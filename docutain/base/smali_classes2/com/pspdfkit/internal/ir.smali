.class public final enum Lcom/pspdfkit/internal/ir;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/ir;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lcom/pspdfkit/internal/ir;

.field public static final enum c:Lcom/pspdfkit/internal/ir;

.field public static final enum d:Lcom/pspdfkit/internal/ir;

.field public static final enum e:Lcom/pspdfkit/internal/ir;

.field private static final synthetic f:[Lcom/pspdfkit/internal/ir;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ir;

    const-string v1, "RECTANGLE"

    const/4 v2, 0x0

    const-string v3, "PSPDFShapeTemplateIdentifierRectangle"

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 4
    new-instance v1, Lcom/pspdfkit/internal/ir;

    const-string v3, "CIRCLE"

    const/4 v4, 0x1

    const-string v5, "PSPDFShapeTemplateIdentifierCircle"

    invoke-direct {v1, v3, v4, v5}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 7
    new-instance v3, Lcom/pspdfkit/internal/ir;

    const-string v5, "LINE"

    const/4 v6, 0x2

    const-string v7, "PSPDFShapeTemplateIdentifierLine"

    invoke-direct {v3, v5, v6, v7}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 10
    new-instance v5, Lcom/pspdfkit/internal/ir;

    const-string v7, "LINE_ARROW_START"

    const/4 v8, 0x3

    const-string v9, "PSPDFShapeTemplateIdentifierLineArrowStart"

    invoke-direct {v5, v7, v8, v9}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/pspdfkit/internal/ir;->b:Lcom/pspdfkit/internal/ir;

    .line 13
    new-instance v7, Lcom/pspdfkit/internal/ir;

    const-string v9, "LINE_ARROW_END"

    const/4 v10, 0x4

    const-string v11, "PSPDFShapeTemplateIdentifierLineArrowEnd"

    invoke-direct {v7, v9, v10, v11}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/pspdfkit/internal/ir;->c:Lcom/pspdfkit/internal/ir;

    .line 19
    new-instance v9, Lcom/pspdfkit/internal/ir;

    const-string v11, "CURVE"

    const/4 v12, 0x5

    const-string v13, "PSPDFShapeTemplateIdentifierCurve"

    invoke-direct {v9, v11, v12, v13}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/pspdfkit/internal/ir;->d:Lcom/pspdfkit/internal/ir;

    .line 22
    new-instance v11, Lcom/pspdfkit/internal/ir;

    const-string v13, "NO_TEMPLATE"

    const/4 v14, 0x6

    const-string v15, ""

    invoke-direct {v11, v13, v14, v15}, Lcom/pspdfkit/internal/ir;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/pspdfkit/internal/ir;->e:Lcom/pspdfkit/internal/ir;

    const/4 v13, 0x7

    new-array v13, v13, [Lcom/pspdfkit/internal/ir;

    aput-object v0, v13, v2

    aput-object v1, v13, v4

    aput-object v3, v13, v6

    aput-object v5, v13, v8

    aput-object v7, v13, v10

    aput-object v9, v13, v12

    aput-object v11, v13, v14

    .line 23
    sput-object v13, Lcom/pspdfkit/internal/ir;->f:[Lcom/pspdfkit/internal/ir;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/ir;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/pspdfkit/internal/ir;
    .locals 5

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ir;->values()[Lcom/pspdfkit/internal/ir;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 2
    iget-object v4, v3, Lcom/pspdfkit/internal/ir;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6
    :cond_1
    sget-object p0, Lcom/pspdfkit/internal/ir;->e:Lcom/pspdfkit/internal/ir;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/ir;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/ir;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ir;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/ir;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ir;->f:[Lcom/pspdfkit/internal/ir;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/ir;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/ir;

    return-object v0
.end method

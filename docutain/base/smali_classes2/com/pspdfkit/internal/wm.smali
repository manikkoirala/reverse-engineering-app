.class public final Lcom/pspdfkit/internal/wm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/lg;


# instance fields
.field private final a:Lcom/pspdfkit/ui/PdfFragment;

.field private b:Lcom/pspdfkit/annotations/WidgetAnnotation;

.field private c:Lcom/pspdfkit/forms/PushButtonFormElement;

.field private final d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;


# direct methods
.method public static synthetic $r8$lambda$IS0YqQvR4oRsVeivYi_-cVzz5e4(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/wm;->a(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$ivj0tQm-lmzpZRL37kdfM0nH0XA(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/wm;->a(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 10
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wm;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    return-void
.end method

.method private static final a(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 25
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/wm;)V
    .locals 1

    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lcom/pspdfkit/internal/wm;->c:Lcom/pspdfkit/forms/PushButtonFormElement;

    .line 61
    iput-object v0, p0, Lcom/pspdfkit/internal/wm;->b:Lcom/pspdfkit/annotations/WidgetAnnotation;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/wm;->b:Lcom/pspdfkit/annotations/WidgetAnnotation;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/forms/PushButtonFormElement;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/wm;->c:Lcom/pspdfkit/forms/PushButtonFormElement;

    return-void
.end method

.method private static final a(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/16 p2, 0x42

    if-ne p1, p2, :cond_0

    .line 26
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/wm;)Lio/reactivex/rxjava3/disposables/CompositeDisposable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/wm;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/jg;
    .locals 2

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object p1, Lcom/pspdfkit/internal/jg;->b:Lcom/pspdfkit/internal/jg;

    return-object p1

    .line 5
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 7
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__ok:I

    const/4 v1, 0x0

    .line 9
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p2

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/wm$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/wm$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/wm$$ExternalSyntheticLambda1;

    invoke-direct {p2}, Lcom/pspdfkit/internal/wm$$ExternalSyntheticLambda1;-><init>()V

    .line 13
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 21
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 24
    sget-object p1, Lcom/pspdfkit/internal/jg;->a:Lcom/pspdfkit/internal/jg;

    return-object p1
.end method

.method public final a()Ljava/lang/Integer;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(IZ)V

    return v1
.end method

.method public final a(II)Z
    .locals 3

    .line 39
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_1

    return v1

    .line 41
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_2

    return v1

    .line 43
    :cond_2
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotationAsync(II)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 44
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 45
    new-instance p2, Lcom/pspdfkit/internal/wm$a;

    invoke-direct {p2, p0, v2}, Lcom/pspdfkit/internal/wm$a;-><init>(Lcom/pspdfkit/internal/wm;Landroid/content/Context;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 59
    iget-object p2, p0, Lcom/pspdfkit/internal/wm;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {p2, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    const/4 p1, 0x1

    return p1
.end method

.method public final a(Lcom/pspdfkit/internal/kg;)Z
    .locals 3

    const-string v0, "jsMailParams"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    goto :goto_0

    .line 35
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/vh;

    invoke-direct {v2, v1, p1}, Lcom/pspdfkit/internal/vh;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/internal/kg;)V

    .line 37
    new-instance p1, Lcom/pspdfkit/document/sharing/SharingOptions;

    sget-object v1, Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;->FLATTEN:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    invoke-direct {p1, v1}, Lcom/pspdfkit/document/sharing/SharingOptions;-><init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;)V

    .line 38
    invoke-static {v2, v0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareDocument(Lcom/pspdfkit/document/sharing/DocumentSharingController;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/sharing/SharingOptions;)Lcom/pspdfkit/document/sharing/DocumentSharingController;

    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public final a(Lcom/pspdfkit/internal/mg;)Z
    .locals 1

    const-string v0, "jsPrintParams"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    new-instance v1, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-direct {v1, p1}, Lcom/pspdfkit/annotations/actions/UriAction;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->executeAction(Lcom/pspdfkit/annotations/actions/Action;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final b()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public final d()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 3
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/wm;->c:Lcom/pspdfkit/forms/PushButtonFormElement;

    if-nez v1, :cond_2

    return-void

    .line 4
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/wm;->b:Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-nez v2, :cond_3

    return-void

    .line 6
    :cond_3
    iget-object v3, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v3}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v4, "JavaScript.IMAGE_PICKER_FRAGMENT_TAG"

    invoke-virtual {v3, v4}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 7
    new-instance v3, Lcom/pspdfkit/document/image/ImagePicker;

    iget-object v5, p0, Lcom/pspdfkit/internal/wm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v5}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v5

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/document/image/ImagePicker;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 8
    new-instance v4, Lcom/pspdfkit/internal/vm;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/pspdfkit/internal/vm;-><init>(Lcom/pspdfkit/internal/wm;Landroid/content/Context;Lcom/pspdfkit/forms/PushButtonFormElement;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    .line 9
    invoke-virtual {v3, v4}, Lcom/pspdfkit/document/image/ImagePicker;->setOnImagePickedListener(Lcom/pspdfkit/document/image/ImagePicker$OnImagePickedListener;)V

    :cond_4
    return-void
.end method

.class final Lcom/pspdfkit/internal/n2$a;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/n2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/pspdfkit/internal/o0;

.field final synthetic b:Lcom/pspdfkit/internal/n2;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/n2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/n2;Lcom/pspdfkit/internal/n2$a-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;-><init>(Lcom/pspdfkit/internal/n2;)V

    return-void
.end method

.method private i(Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetk(Lcom/pspdfkit/internal/n2;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->j(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v1}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetm(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/a1;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p1, v0, v4}, Lcom/pspdfkit/internal/a1;->a(FFLandroid/graphics/Matrix;Z)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    goto :goto_0

    .line 6
    :cond_1
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 7
    :goto_0
    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    :cond_2
    return-object v2
.end method

.method private j(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/o0;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    iget-object v0, v0, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    iget-object v1, v1, Lcom/pspdfkit/internal/n2;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/o0;

    .line 4
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o0;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 5
    iget-object v4, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    .line 6
    invoke-static {v4}, Lcom/pspdfkit/internal/n2;->i(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xa

    int-to-float v5, v5

    .line 7
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    const/4 v6, 0x1

    .line 8
    invoke-static {v6, v5, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 9
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 10
    invoke-virtual {v3, v5}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 12
    iget v3, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v5, Landroid/graphics/Rect;->left:I

    .line 13
    iget v3, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iput v3, v5, Landroid/graphics/Rect;->right:I

    .line 14
    iget v3, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v5, Landroid/graphics/Rect;->top:I

    .line 15
    iget v3, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    iput v3, v5, Landroid/graphics/Rect;->bottom:I

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 21
    :goto_0
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->f()V

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {p1}, Lcom/pspdfkit/internal/n2;->e(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->e()V

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {p1}, Lcom/pspdfkit/internal/n2;->f(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->j(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/o0;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o0;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    goto :goto_0

    .line 4
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->i(Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 7
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 8
    new-instance v4, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v3}, Lcom/pspdfkit/internal/n2;->g(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v3

    iget-object v5, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v5}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v3}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    .line 14
    invoke-static {v3}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgeth(Lcom/pspdfkit/internal/n2;)Ljava/util/ArrayList;

    move-result-object v3

    .line 15
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 16
    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v3}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgete(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/n2$b;

    move-result-object v3

    invoke-interface {v3, v1, p1, v4}, Lcom/pspdfkit/internal/n2$b;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/view/MotionEvent;Landroid/graphics/PointF;)Z

    move-result p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    if-nez p1, :cond_3

    if-eqz v0, :cond_3

    .line 24
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o0;->c()V

    :cond_3
    return v2
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->i(Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o0;->e()V

    .line 2
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->j(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/o0;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    if-eqz p1, :cond_3

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->d()V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {p1}, Lcom/pspdfkit/internal/n2;->a(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/utils/PageRect;->updatePageRect(Landroid/graphics/Matrix;)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object p1

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    .line 13
    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->b(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/n2$a;->a:Lcom/pspdfkit/internal/o0;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o0;->b()Lcom/pspdfkit/utils/PageRect;

    move-result-object p1

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object p1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    .line 18
    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->c(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 22
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v0}, Lcom/pspdfkit/internal/n2;->d(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v0

    if-eqz p1, :cond_2

    const-wide/16 v1, 0x64

    goto :goto_1

    :cond_2
    const-wide/16 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->postInvalidateDelayed(J)V

    :cond_3
    return-void
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->j(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/o0;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o0;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    goto :goto_0

    .line 4
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n2$a;->i(Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 7
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 8
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v2}, Lcom/pspdfkit/internal/n2;->h(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/im;

    move-result-object v2

    iget-object v4, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v4}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v2}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetn(Lcom/pspdfkit/internal/n2;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    .line 12
    invoke-static {v2}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgeth(Lcom/pspdfkit/internal/n2;)Ljava/util/ArrayList;

    move-result-object v2

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 14
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/n2$a;->b:Lcom/pspdfkit/internal/n2;

    invoke-static {v1}, Lcom/pspdfkit/internal/n2;->-$$Nest$fgetf(Lcom/pspdfkit/internal/n2;)Lcom/pspdfkit/internal/n2$c;

    move-result-object v1

    invoke-interface {v1, v0, p1, v3}, Lcom/pspdfkit/internal/n2$c;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/view/MotionEvent;Landroid/graphics/PointF;)Z

    move-result v1

    :cond_2
    return v1
.end method

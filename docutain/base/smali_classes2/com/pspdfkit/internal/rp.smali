.class final Lcom/pspdfkit/internal/rp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zf;

.field final synthetic b:Lcom/pspdfkit/annotations/actions/ResetFormAction;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/actions/ResetFormAction;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/rp;->a:Lcom/pspdfkit/internal/zf;

    iput-object p2, p0, Lcom/pspdfkit/internal/rp;->b:Lcom/pspdfkit/annotations/actions/ResetFormAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Ljava/util/List;

    const-string v0, "formFields"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/rp;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/rp;->b:Lcom/pspdfkit/annotations/actions/ResetFormAction;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/actions/ResetFormAction;->shouldExcludeFormFields()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/internal/uf;->resetFormFields(Ljava/util/List;Z)V

    return-void
.end method

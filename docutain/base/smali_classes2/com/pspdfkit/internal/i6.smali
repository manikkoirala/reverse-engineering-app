.class public final Lcom/pspdfkit/internal/i6;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

.field private final d:Lcom/pspdfkit/internal/mt;


# direct methods
.method public static synthetic $r8$lambda$byXkAhbtHWy7Vj9WXtaBJArml1Y(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/i6;->a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pwK4O-7DvHHEalkKR6BFtLLhHj8(Lcom/pspdfkit/internal/h6;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/i6;->a(Lcom/pspdfkit/internal/h6;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$uY5B1rEsY_81zbZ-VD3H8_A6PPQ(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/i6;->a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/i6;->a:Lcom/pspdfkit/ui/special_mode/controller/ContentEditingController;

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "controller.fragment.requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getContentEditingConfiguration()Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

    move-result-object p1

    const-string v0, "controller.fragment.contentEditingConfiguration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/i6;->c:Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object p1

    const-string v0, "getSystemFontManager()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/i6;->d:Lcom/pspdfkit/internal/mt;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/h6;I)V
    .locals 1

    const-string v0, "$currentFormatter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p1, p1

    .line 298
    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/h6;->setFontSize(F)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "$currentFormatter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "it.name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/h6;->setFaceName(Ljava/lang/String;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "$currentFormatter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    invoke-interface {p0, p2}, Lcom/pspdfkit/internal/h6;->setFontColor(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Landroid/view/View;
    .locals 4

    const-string v0, "currentFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/pspdfkit/internal/i6;->c:Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

    invoke-interface {v0}, Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;->customColorPickerEnabled()Z

    move-result v0

    .line 300
    iget-object v1, p0, Lcom/pspdfkit/internal/i6;->c:Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

    invoke-interface {v1}, Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v1

    const-string v2, "configuration.availableFillColors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 301
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jt;->c()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/i6;->c:Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;

    invoke-interface {p2}, Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;->getDefaultFillColor()I

    move-result p2

    :goto_0
    if-eqz v0, :cond_1

    .line 302
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    invoke-direct {v0, v2, v1, p2}, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    goto :goto_1

    .line 304
    :cond_1
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, p2, v3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    .line 305
    :goto_1
    new-instance p2, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda2;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/h6;)V

    invoke-interface {v0, p2}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;->setOnColorPickedListener(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 309
    invoke-interface {v0}, Lcom/pspdfkit/ui/inspector/PropertyInspectorView;->getView()Landroid/view/View;

    move-result-object p1

    const-string p2, "colorView.view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Ljava/util/List;Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/ui/inspector/views/ContentEditingFontNamesPickerView;
    .locals 9

    const-string v0, "faceListEntries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda1;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/h6;)V

    .line 2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/internal/db;

    .line 4
    sget v3, Lcom/pspdfkit/internal/mt;->g:I

    invoke-virtual {v2}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/mt$a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 49
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_d

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 50
    check-cast v1, Lcom/pspdfkit/internal/db;

    .line 51
    invoke-virtual {v1}, Lcom/pspdfkit/internal/db;->b()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/internal/fb;

    .line 52
    iget-object v7, p0, Lcom/pspdfkit/internal/i6;->d:Lcom/pspdfkit/internal/mt;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/fb;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/pspdfkit/internal/mt;->a(Ljava/lang/String;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    if-nez v7, :cond_3

    const/4 v7, 0x1

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    :goto_2
    if-nez v7, :cond_4

    goto :goto_3

    :cond_4
    move-object v6, v3

    :goto_3
    if-eqz v6, :cond_6

    .line 53
    new-instance v7, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    if-nez v6, :cond_5

    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :cond_5
    invoke-direct {v7, v8, v6}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    goto :goto_4

    :cond_6
    move-object v7, v3

    :goto_4
    if-eqz v7, :cond_2

    goto :goto_5

    :cond_7
    move-object v7, v3

    :goto_5
    if-nez v7, :cond_c

    .line 60
    invoke-virtual {v1}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v2

    const-string v5, "fontName"

    .line 61
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object v5, Lcom/pspdfkit/internal/sf$b;->b:Lcom/pspdfkit/internal/sf$b;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "name"

    .line 118
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-virtual {v5}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7, v4}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_8

    :goto_6
    move-object v3, v5

    goto :goto_7

    .line 163
    :cond_8
    sget-object v5, Lcom/pspdfkit/internal/sf$a;->b:Lcom/pspdfkit/internal/sf$a;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 164
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v5}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7, v4}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_9

    goto :goto_6

    .line 209
    :cond_9
    sget-object v5, Lcom/pspdfkit/internal/sf$c;->b:Lcom/pspdfkit/internal/sf$c;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 210
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-virtual {v5}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v4}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    goto :goto_6

    :cond_a
    :goto_7
    if-eqz v3, :cond_b

    move-object v7, v3

    goto :goto_8

    .line 255
    :cond_b
    new-instance v7, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/db;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-direct {v7, v1, v2}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    .line 285
    :cond_c
    :goto_8
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_d
    if-eqz p3, :cond_e

    .line 286
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jt;->j()Z

    move-result p2

    if-nez p2, :cond_e

    const/4 v2, 0x1

    :cond_e
    if-eqz v2, :cond_f

    .line 287
    new-instance p2, Lcom/pspdfkit/ui/fonts/Font;

    iget-object v1, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    invoke-virtual {p3, v1}, Lcom/pspdfkit/internal/jt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-static {p3, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    goto :goto_9

    :cond_f
    if-eqz p3, :cond_12

    .line 291
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_10
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    invoke-virtual {p3, v4}, Lcom/pspdfkit/internal/jt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object v3, v1

    :cond_11
    move-object p2, v3

    check-cast p2, Lcom/pspdfkit/ui/fonts/Font;

    if-nez p2, :cond_13

    :cond_12
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/fonts/Font;

    .line 294
    :cond_13
    :goto_9
    new-instance p3, Lcom/pspdfkit/ui/inspector/views/ContentEditingFontNamesPickerView;

    .line 295
    iget-object v1, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    .line 296
    invoke-direct {p3, v1, p1, p2, v0}, Lcom/pspdfkit/ui/inspector/views/ContentEditingFontNamesPickerView;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V

    return-object p3
.end method

.method public final b(Lcom/pspdfkit/internal/h6;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;
    .locals 7

    const-string v0, "currentFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/internal/h6;->getAvailableFontSizes()Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jt;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 101
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    .line 102
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_1
    move-object v4, v0

    .line 199
    :goto_0
    check-cast v4, Ljava/lang/Integer;

    goto :goto_1

    :cond_2
    move-object v4, v0

    :goto_1
    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_4

    .line 200
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jt;->e()Ljava/lang/String;

    move-result-object p2

    move-object v5, p2

    goto :goto_3

    :cond_4
    :goto_2
    move-object v5, v0

    .line 202
    :goto_3
    new-instance p2, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;

    .line 203
    iget-object v2, p0, Lcom/pspdfkit/internal/i6;->b:Landroid/content/Context;

    .line 207
    new-instance v6, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda0;

    invoke-direct {v6, p1}, Lcom/pspdfkit/internal/i6$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/h6;)V

    move-object v1, p2

    .line 208
    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/ui/inspector/ContentEditingFontSizesPickerView;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontSizePickerListener;)V

    return-object p2
.end method

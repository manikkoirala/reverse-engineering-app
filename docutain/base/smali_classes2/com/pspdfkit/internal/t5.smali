.class public final Lcom/pspdfkit/internal/t5;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Ljava/util/ArrayList;
    .locals 12

    const/4 v0, 0x3

    new-array v1, v0, [F

    .line 2
    invoke-static {p0, v1}, Landroidx/core/graphics/ColorUtils;->colorToHSL(I[F)V

    const/4 p0, 0x1

    aget v2, v1, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    cmpg-float v2, v2, v3

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    const/4 v2, 0x2

    const/16 v5, 0x9

    if-eqz p0, :cond_2

    .line 6
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    const/16 v1, 0x8

    int-to-float v1, v1

    const/high16 v6, 0x3f800000    # 1.0f

    div-float/2addr v6, v1

    :goto_1
    if-ge v4, v5, :cond_1

    new-array v1, v0, [F

    aput v3, v1, v2

    .line 14
    invoke-static {v1}, Landroidx/core/graphics/ColorUtils;->HSLToColor([F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    return-object p0

    .line 15
    :cond_2
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    aget v3, v1, v2

    const v6, 0x3f666666    # 0.9f

    const v7, 0x3dcccccd    # 0.1f

    cmpg-float v8, v3, v7

    if-lez v8, :cond_3

    cmpl-float v8, v3, v6

    if-ltz v8, :cond_4

    :cond_3
    const/high16 v3, 0x3f000000    # 0.5f

    :cond_4
    const/4 v8, 0x4

    sub-float v9, v3, v7

    int-to-float v10, v8

    div-float/2addr v9, v10

    sub-float/2addr v6, v3

    div-float/2addr v6, v10

    const/4 v3, 0x0

    :goto_2
    const-string v10, "copyOf(this, size)"

    if-ge v3, v5, :cond_6

    .line 33
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v11

    invoke-static {v11, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    aput v7, v11, v2

    .line 35
    invoke-static {v11}, Landroidx/core/graphics/ColorUtils;->HSLToColor([F)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ge v3, v8, :cond_5

    move v10, v9

    goto :goto_3

    :cond_5
    move v10, v6

    :goto_3
    add-float/2addr v7, v10

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    int-to-float v2, v5

    const/high16 v3, 0x43200000    # 160.0f

    div-float/2addr v3, v2

    aget v2, v1, v4

    const/high16 v6, 0x42a00000    # 80.0f

    sub-float/2addr v2, v6

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v5, :cond_7

    .line 52
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v7

    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v8, 0x168

    int-to-float v8, v8

    add-float v9, v2, v8

    rem-float/2addr v9, v8

    .line 53
    aput v9, v7, v4

    .line 54
    invoke-static {v7}, Landroidx/core/graphics/ColorUtils;->HSLToColor([F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v2, v3

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_7
    return-object p0
.end method

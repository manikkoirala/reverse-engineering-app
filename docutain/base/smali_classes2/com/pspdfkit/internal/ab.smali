.class public final Lcom/pspdfkit/internal/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static final c:I

.field private static final d:I


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__EraserTool:[I

    sput-object v0, Lcom/pspdfkit/internal/ab;->b:[I

    .line 4
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__eraserStyle:I

    sput v0, Lcom/pspdfkit/internal/ab;->c:I

    .line 7
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_EraserTool:I

    sput v0, Lcom/pspdfkit/internal/ab;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ab;->b:[I

    sget v2, Lcom/pspdfkit/internal/ab;->c:I

    sget v3, Lcom/pspdfkit/internal/ab;->d:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.theme.obtainStyl\u2026, DEFAULT_STYLE_RESOURCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__EraserTool_pspdf__eraserOutlineColor:I

    .line 12
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray_dark:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 13
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ab;->a:I

    .line 18
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ab;->a:I

    return v0
.end method

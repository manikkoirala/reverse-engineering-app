.class public final Lcom/pspdfkit/internal/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/an;-><init>(I)V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/pspdfkit/internal/an;->a:F

    iput p2, p0, Lcom/pspdfkit/internal/an;->b:F

    iput p3, p0, Lcom/pspdfkit/internal/an;->c:F

    iput p4, p0, Lcom/pspdfkit/internal/an;->d:F

    return-void
.end method

.method public synthetic constructor <init>(I)V
    .locals 0

    const/4 p1, 0x0

    .line 2
    invoke-direct {p0, p1, p1, p1, p1}, Lcom/pspdfkit/internal/an;-><init>(FFFF)V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->d:F

    return v0
.end method

.method public final a(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/an;->d:F

    return-void
.end method

.method public final b()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->a:F

    return v0
.end method

.method public final b(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/an;->a:F

    return-void
.end method

.method public final c()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->c:F

    return v0
.end method

.method public final c(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/an;->c:F

    return-void
.end method

.method public final d()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->b:F

    return v0
.end method

.method public final d(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/an;->b:F

    return-void
.end method

.method public final e()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->b:F

    iget v1, p0, Lcom/pspdfkit/internal/an;->d:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/an;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/an;

    iget v1, p0, Lcom/pspdfkit/internal/an;->a:F

    iget v3, p1, Lcom/pspdfkit/internal/an;->a:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_2

    return v2

    :cond_2
    iget v1, p0, Lcom/pspdfkit/internal/an;->b:F

    iget v3, p1, Lcom/pspdfkit/internal/an;->b:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_3

    return v2

    :cond_3
    iget v1, p0, Lcom/pspdfkit/internal/an;->c:F

    iget v3, p1, Lcom/pspdfkit/internal/an;->c:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_4

    return v2

    :cond_4
    iget v1, p0, Lcom/pspdfkit/internal/an;->d:F

    iget p1, p1, Lcom/pspdfkit/internal/an;->d:F

    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-eqz p1, :cond_5

    return v2

    :cond_5
    return v0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/an;->a:F

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/an;->b:F

    .line 3
    iput v0, p0, Lcom/pspdfkit/internal/an;->c:F

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/an;->d:F

    return-void
.end method

.method public final g()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/an;->c:F

    iget v1, p0, Lcom/pspdfkit/internal/an;->a:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/pspdfkit/internal/an;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/pspdfkit/internal/an;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/pspdfkit/internal/an;->c:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/pspdfkit/internal/an;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    iget v0, p0, Lcom/pspdfkit/internal/an;->a:F

    iget v1, p0, Lcom/pspdfkit/internal/an;->b:F

    iget v2, p0, Lcom/pspdfkit/internal/an;->c:F

    iget v3, p0, Lcom/pspdfkit/internal/an;->d:F

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PdfRect(left="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", top="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", right="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", bottom="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

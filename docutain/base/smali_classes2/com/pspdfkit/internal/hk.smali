.class public final Lcom/pspdfkit/internal/hk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ik;


# instance fields
.field private final a:Lcom/pspdfkit/annotations/Annotation;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/pspdfkit/internal/kk;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private final j:Lcom/pspdfkit/annotations/AnnotationType;

.field private k:Ljava/lang/String;

.field private l:Z

.field private final m:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-class v0, Lcom/pspdfkit/internal/kk;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    const/4 v1, 0x0

    .line 14
    iput-boolean v1, p0, Lcom/pspdfkit/internal/hk;->l:Z

    .line 22
    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->a:Lcom/pspdfkit/annotations/Annotation;

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/hk;->b:I

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getSubject()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/hk;->c:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/hk;->d:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/hk;->i:I

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/hk;->j:Lcom/pspdfkit/annotations/AnnotationType;

    .line 29
    instance-of v1, p1, Lcom/pspdfkit/annotations/NoteAnnotation;

    if-eqz v1, :cond_0

    .line 30
    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/NoteAnnotation;->getIconName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    .line 33
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getCreatedDate()Ljava/util/Date;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    const/4 v1, 0x3

    .line 35
    invoke-static {v0, v1}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_1
    iput-object v0, p0, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    .line 41
    :goto_0
    iput-object p2, p0, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    .line 42
    iput-boolean p3, p0, Lcom/pspdfkit/internal/hk;->m:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/pspdfkit/internal/kk;",
            ">;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .line 6
    iput p1, p0, Lcom/pspdfkit/internal/hk;->i:I

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/hk;->l:Z

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hk;->m:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->j:Lcom/pspdfkit/annotations/AnnotationType;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/hk;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/hk;

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/hk;->b:I

    iget v3, p1, Lcom/pspdfkit/internal/hk;->b:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/pspdfkit/internal/hk;->i:I

    iget v3, p1, Lcom/pspdfkit/internal/hk;->i:I

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/pspdfkit/internal/hk;->l:Z

    iget-boolean v3, p1, Lcom/pspdfkit/internal/hk;->l:Z

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/pspdfkit/internal/hk;->m:Z

    iget-boolean v3, p1, Lcom/pspdfkit/internal/hk;->m:Z

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->c:Ljava/lang/String;

    .line 7
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->d:Ljava/lang/String;

    .line 8
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    .line 9
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    .line 10
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    .line 11
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    .line 12
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->j:Lcom/pspdfkit/annotations/AnnotationType;

    iget-object v3, p1, Lcom/pspdfkit/internal/hk;->j:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    iget-object p1, p1, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    .line 14
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final getColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/hk;->i:I

    return v0
.end method

.method public final getId()J
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/hk;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget v1, p0, Lcom/pspdfkit/internal/hk;->b:I

    .line 2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->c:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->d:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->f:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->h:Ljava/util/Set;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget v1, p0, Lcom/pspdfkit/internal/hk;->i:I

    .line 9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->j:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/pspdfkit/internal/hk;->l:Z

    .line 12
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/pspdfkit/internal/hk;->m:Z

    .line 13
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 14
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hk;->l:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hk;->g:Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    return-object v0
.end method

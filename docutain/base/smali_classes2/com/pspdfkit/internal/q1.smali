.class public final Lcom/pspdfkit/internal/q1;
.super Lcom/pspdfkit/internal/g2;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g2<",
        "Lcom/pspdfkit/internal/n1;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/qf;",
            "Landroid/util/SparseIntArray;",
            "Lcom/pspdfkit/ui/PdfFragment;",
            "Lcom/pspdfkit/internal/k4$a<",
            "-",
            "Lcom/pspdfkit/internal/n1;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/n1;

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/pspdfkit/internal/g2;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/q1;->e:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/n1;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/n1;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final f(Lcom/pspdfkit/internal/ja;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/n1;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/n1;->c:I

    iget-object p1, p1, Lcom/pspdfkit/internal/n1;->e:Ljava/lang/Object;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v2

    .line 4
    invoke-virtual {v2, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/pf;->setProperties(Lcom/pspdfkit/internal/p1;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/q1;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 11
    new-instance v0, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;

    const-string v1, "Could not perform redo operation."

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final g(Lcom/pspdfkit/internal/ja;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/n1;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/n1;->c:I

    iget-object p1, p1, Lcom/pspdfkit/internal/n1;->d:Ljava/lang/Object;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v2

    .line 4
    invoke-virtual {v2, v1, p1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/pf;->setProperties(Lcom/pspdfkit/internal/p1;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/q1;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 11
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v1, "Could not perform undo operation."

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

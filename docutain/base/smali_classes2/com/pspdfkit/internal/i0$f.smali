.class public final Lcom/pspdfkit/internal/i0$f;
.super Lcom/pspdfkit/internal/yg;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/i0;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$-CC;->builder(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$Builder;

    move-result-object p1

    .line 2
    new-instance v0, Landroidx/core/util/Pair;

    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->BUTT:Lcom/pspdfkit/annotations/LineEndType;

    sget-object v2, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {v0, v1, v2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration$Builder;->setDefaultLineEnds(Landroidx/core/util/Pair;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$Builder;

    const/high16 v0, 0x40000000    # 2.0f

    .line 3
    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder;->setDefaultThickness(F)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$Builder;

    .line 4
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder;->setDefaultColor(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$Builder;

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration$Builder;->build()Lcom/pspdfkit/annotations/configuration/MeasurementPerimeterAnnotationConfiguration;

    move-result-object p1

    const-string v0, "builder(context)\n       \u2026                 .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

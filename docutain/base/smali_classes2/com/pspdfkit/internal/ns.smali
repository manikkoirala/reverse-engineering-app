.class public final Lcom/pspdfkit/internal/ns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/internal/ns;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field private final b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private final c:Ljava/util/ArrayList;

.field private final d:Lcom/pspdfkit/internal/nm;

.field private final e:Lcom/pspdfkit/datastructures/TextSelection;

.field private final f:Lcom/pspdfkit/internal/w6;


# direct methods
.method public static synthetic $r8$lambda$BGOBWQgz6nS0r9LeZn1x3w9rc3A(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ns;->a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$zsbsSAU41l0fNKCU9nT4IwVD71E(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/nm;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ns;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/nm;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ns$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ns$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/ns;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 16
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/pspdfkit/internal/ns;->a:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 17
    const-class v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    iput-object v0, p0, Lcom/pspdfkit/internal/ns;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 18
    sget-object v0, Lcom/pspdfkit/internal/nm;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    .line 19
    const-class v0, Lcom/pspdfkit/internal/nm;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nm;

    iput-object v0, p0, Lcom/pspdfkit/internal/ns;->d:Lcom/pspdfkit/internal/nm;

    .line 20
    const-class v0, Lcom/pspdfkit/datastructures/TextSelection;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/datastructures/TextSelection;

    iput-object v0, p0, Lcom/pspdfkit/internal/ns;->e:Lcom/pspdfkit/datastructures/TextSelection;

    .line 21
    const-class v0, Lcom/pspdfkit/internal/w6;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/w6;

    iput-object p1, p0, Lcom/pspdfkit/internal/ns;->f:Lcom/pspdfkit/internal/w6;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/util/List;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/w6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/forms/FormElement;",
            "Lcom/pspdfkit/datastructures/TextSelection;",
            "Lcom/pspdfkit/internal/w6;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ns;->a:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ns;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    if-eqz p4, :cond_0

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/nm;

    invoke-virtual {p4}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ns;->d:Lcom/pspdfkit/internal/nm;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/ns;->e:Lcom/pspdfkit/datastructures/TextSelection;

    .line 8
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    .line 9
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/annotations/Annotation;

    .line 10
    iget-object p3, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    new-instance p4, Lcom/pspdfkit/internal/nm;

    invoke-direct {p4, p2}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 13
    :cond_1
    iput-object p6, p0, Lcom/pspdfkit/internal/ns;->f:Lcom/pspdfkit/internal/w6;

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    .line 12
    check-cast p0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElementAsync()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0

    .line 14
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/nm;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-nez p0, :cond_0

    .line 10
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->a:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 5
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/ns$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/ns$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/zf;)V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->flatMapMaybe(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 9
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final b()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final b(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->d:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/ns$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ns$$ExternalSyntheticLambda0;-><init>()V

    .line 5
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 6
    :cond_1
    :goto_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lcom/pspdfkit/internal/w6;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->f:Lcom/pspdfkit/internal/w6;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->e:Lcom/pspdfkit/datastructures/TextSelection;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final f()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->d:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->a:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->d:Lcom/pspdfkit/internal/nm;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->e:Lcom/pspdfkit/datastructures/TextSelection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ns;->f:Lcom/pspdfkit/internal/w6;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

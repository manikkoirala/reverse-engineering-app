.class public Lcom/pspdfkit/internal/d0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/h0;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/h0;)V
    .locals 1

    const-string v0, "properties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    return-void
.end method


# virtual methods
.method public final customColorPickerEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->j:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getAnnotationAggregationStrategy()Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->u:Lcom/pspdfkit/internal/g0;

    sget-object v2, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->AUTOMATIC:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    return-object v0
.end method

.method public final getAudioRecordingTimeLimit()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->G:Lcom/pspdfkit/internal/g0;

    const v2, 0x493e0

    .line 3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getAvailableColors()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->e:Lcom/pspdfkit/internal/g0;

    .line 3
    sget-object v2, Lcom/pspdfkit/internal/ao;->c:Ljava/util/List;

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getAvailableFillColors()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->g:Lcom/pspdfkit/internal/g0;

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/ao;->a()Ljava/util/List;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getAvailableFonts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->A:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/mt;->getAvailableFonts()Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final getAvailableIconNames()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->F:Lcom/pspdfkit/internal/g0;

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/ao;->d()Ljava/util/List;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getAvailableLineEnds()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->y:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->SQUARE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->CIRCLE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->DIAMOND:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->CLOSED_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->BUTT:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 10
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->REVERSE_OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->REVERSE_CLOSED_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->SLASH:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public final getAvailableOutlineColors()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->i:Lcom/pspdfkit/internal/g0;

    .line 3
    sget-object v2, Lcom/pspdfkit/internal/ao;->c:Ljava/util/List;

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getBorderStylePresets()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->w:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getDefaultAlpha()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->q:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getDefaultBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->v:Lcom/pspdfkit/internal/g0;

    sget-object v2, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->NONE:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    const-string v3, "NONE"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    return-object v0
.end method

.method public final getDefaultColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->d:Lcom/pspdfkit/internal/g0;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getDefaultFillColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->f:Lcom/pspdfkit/internal/g0;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getDefaultFont()Lcom/pspdfkit/ui/fonts/Font;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->z:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "getSystemFontManager().d\u2026otationFont.blockingGet()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    :cond_0
    return-object v0
.end method

.method public final getDefaultIconName()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->E:Lcom/pspdfkit/internal/g0;

    const-string v2, "Note"

    .line 3
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultLineEnds()Landroidx/core/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->x:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/util/Pair;

    if-nez v0, :cond_0

    new-instance v0, Landroidx/core/util/Pair;

    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {v0, v1, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method public final getDefaultOutlineColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->h:Lcom/pspdfkit/internal/g0;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getDefaultOverlayText()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->D:Lcom/pspdfkit/internal/g0;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->L:Lcom/pspdfkit/internal/g0;

    sget-object v2, Lcom/pspdfkit/internal/ao;->b:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    return-object v0
.end method

.method public final getDefaultRepeatOverlayTextSetting()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->C:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getDefaultScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->K:Lcom/pspdfkit/internal/g0;

    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale;->defaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v2

    const-string v3, "defaultScale()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/measurements/Scale;

    return-object v0
.end method

.method public final getDefaultTextSize()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->n:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getDefaultThickness()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->k:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getForceDefaults()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->b:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getMaxAlpha()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->s:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getMaxTextSize()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->p:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x42700000    # 60.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getMaxThickness()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->m:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getMinAlpha()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->r:Lcom/pspdfkit/internal/g0;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getMinTextSize()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->o:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getMinThickness()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->l:Lcom/pspdfkit/internal/g0;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final getRecordingSampleRate()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->H:Lcom/pspdfkit/internal/g0;

    const/16 v2, 0x5622

    .line 3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getStampsForPicker()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->B:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getSupportedProperties()Ljava/util/EnumSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/g0;->a:Lcom/pspdfkit/internal/g0;

    .line 3
    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    const-string v3, "noneOf(AnnotationProperty::class.java)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    return-object v0
.end method

.method public final isHorizontalResizingEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->J:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final isPreviewEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->t:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final isVerticalResizingEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->I:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final isZIndexEditingEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->c:Lcom/pspdfkit/internal/g0;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

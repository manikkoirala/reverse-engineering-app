.class public Lcom/pspdfkit/internal/ys;
.super Landroidx/appcompat/app/AppCompatDialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ys$b;
    }
.end annotation


# static fields
.field public static final synthetic k:I


# instance fields
.field private b:Z

.field private c:Lcom/pspdfkit/internal/ys$b;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

.field private f:I

.field private g:Landroid/graphics/PointF;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Lcom/pspdfkit/internal/zs;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ys;)Lcom/pspdfkit/internal/ys$b;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ys;->c:Lcom/pspdfkit/internal/ys$b;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/ys;)Lcom/pspdfkit/internal/zs;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ys;->b:Z

    const/4 v0, -0x1

    .line 68
    iput v0, p0, Lcom/pspdfkit/internal/ys;->f:I

    return-void
.end method

.method public static a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/ys$b;)Lcom/pspdfkit/internal/ys;
    .locals 1

    const-string v0, "com.pspdfkit.ui.dialog.stamps.StampPickerDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ys;

    if-eqz p0, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ys;->c:Lcom/pspdfkit/internal/ys$b;

    :cond_0
    return-object p0
.end method

.method public static b(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/ys$b;)Lcom/pspdfkit/internal/ys;
    .locals 3

    const-string v0, "com.pspdfkit.ui.dialog.stamps.StampPickerDialog.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ys;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ys;

    invoke-direct {v1}, Lcom/pspdfkit/internal/ys;-><init>()V

    .line 3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 4
    :cond_0
    iput-object p1, v1, Lcom/pspdfkit/internal/ys;->c:Lcom/pspdfkit/internal/ys$b;

    .line 5
    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatDialogFragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    .line 6
    invoke-virtual {v1, p0, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/ys;->f:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .line 10
    iput p1, p0, Lcom/pspdfkit/internal/ys;->f:I

    return-void
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/ys;->g:Landroid/graphics/PointF;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 1

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ys;->e:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zs;->setCustomStampAnnotation(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;)V"
        }
    .end annotation

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/ys;->d:Ljava/util/List;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zs;->setItems(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->g:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->c()V

    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    const-string v1, "STATE_PAGE_INDEX"

    .line 1
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ys;->f:I

    const-string v0, "STATE_TOUCH_POINT"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/pspdfkit/internal/ys;->g:Landroid/graphics/PointF;

    const-string v0, "STATE_CUSTOM_STAMP"

    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    iput-object v0, p0, Lcom/pspdfkit/internal/ys;->e:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    const-string v0, "STATE_DATE_SWITCH"

    .line 5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ys;->h:Ljava/lang/Boolean;

    const-string v0, "STATE_TIME_SWITCH"

    .line 6
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ys;->i:Ljava/lang/Boolean;

    const-string v0, "STATE_STAMPS_LIST"

    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ys;->d:Ljava/util/List;

    const-string v0, "STATE_STAMP_CREATOR_OPEN"

    .line 10
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/ys;->b:Z

    .line 11
    :cond_0
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_Dialog_Light_Panel_Dim:I

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->setStyle(II)V

    .line 12
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    const/4 v0, 0x1

    .line 13
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/ys;->f:I

    const-string v1, "STATE_PAGE_INDEX"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->g:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    const-string v1, "STATE_TOUCH_POINT"

    .line 4
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    const-string v1, "STATE_STAMPS_LIST"

    const-string v2, "STATE_CUSTOM_STAMP"

    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->getDateSwitchState()Z

    move-result v0

    const-string v3, "STATE_DATE_SWITCH"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->getTimeSwitchState()Z

    move-result v0

    const-string v3, "STATE_TIME_SWITCH"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->b()Z

    move-result v0

    const-string v3, "STATE_STAMP_CREATOR_OPEN"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->getCustomStampAnnotation()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zs;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->e:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz v0, :cond_2

    .line 16
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 19
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/ys;->d:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final onStart()V
    .locals 7

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 3
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 4
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/zs;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 7
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__maxHeight:I

    .line 8
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x230

    invoke-static {v3, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v3

    .line 9
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 11
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__maxWidth:I

    .line 12
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x1e0

    invoke-static {v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v4

    .line 13
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 15
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 18
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 19
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    .line 22
    :goto_1
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v6, -0x1

    if-eqz v1, :cond_3

    const/4 v3, -0x1

    :cond_3
    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    if-eqz v5, :cond_5

    :goto_2
    const/4 v2, -0x1

    .line 23
    :cond_5
    invoke-virtual {v4, v3, v2}, Landroid/view/Window;->setLayout(II)V

    .line 28
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 31
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 32
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v2, 0x106000d

    invoke-virtual {v0, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    if-eqz v0, :cond_6

    .line 34
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zs;->setFullscreen(Z)V

    :cond_6
    :goto_3
    return-void
.end method

.method public final setupDialog(Landroid/app/Dialog;I)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 3
    new-instance p2, Lcom/pspdfkit/internal/zs;

    .line 4
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ys;->b:Z

    new-instance v2, Lcom/pspdfkit/internal/ys$a;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/ys$a;-><init>(Lcom/pspdfkit/internal/ys;)V

    invoke-direct {p2, v0, v1, v2}, Lcom/pspdfkit/internal/zs;-><init>(Landroid/content/Context;ZLcom/pspdfkit/internal/zs$a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zs;->setDateSwitchState(Z)V

    .line 28
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/ys;->i:Ljava/lang/Boolean;

    if-eqz p2, :cond_1

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/zs;->setTimeSwitchState(Z)V

    .line 31
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/ys;->d:Ljava/util/List;

    if-eqz p2, :cond_2

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/zs;->setItems(Ljava/util/List;)V

    .line 35
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/internal/ys;->e:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz p2, :cond_3

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/zs;->setCustomStampAnnotation(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    .line 38
    :cond_3
    iget-object p2, p0, Lcom/pspdfkit/internal/ys;->j:Lcom/pspdfkit/internal/zs;

    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/og;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/og$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/og$a;

.field private final b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/og$a;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityConfiguration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/og;->a:Lcom/pspdfkit/internal/og$a;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/og;->b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/KeyEvent;)Z
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x18

    if-eq v0, v2, :cond_6

    const/16 v2, 0x19

    if-eq v0, v2, :cond_4

    const/16 v2, 0x22

    if-eq v0, v2, :cond_2

    const/16 v3, 0x2c

    if-eq v0, v3, :cond_0

    const/16 v3, 0x54

    if-eq v0, v3, :cond_2

    goto :goto_0

    .line 2
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 6
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_8

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/og;->a:Lcom/pspdfkit/internal/og$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/og$a;->attemptPrinting()Z

    move-result v1

    goto :goto_1

    .line 8
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 12
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_8

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/og;->a:Lcom/pspdfkit/internal/og$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/og$a;->showSearchView()V

    goto :goto_1

    .line 14
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/og;->b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isVolumeButtonsNavigationEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_0

    .line 18
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_8

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/og;->a:Lcom/pspdfkit/internal/og$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/og$a;->navigatePreviousPage()V

    goto :goto_1

    .line 20
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/og;->b:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isVolumeButtonsNavigationEnabled()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    .line 24
    :cond_7
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_8

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/og;->a:Lcom/pspdfkit/internal/og$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/og$a;->navigateNextPage()V

    :cond_8
    :goto_1
    return v1
.end method

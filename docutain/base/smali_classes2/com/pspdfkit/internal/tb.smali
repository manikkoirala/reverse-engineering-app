.class public final Lcom/pspdfkit/internal/tb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/d$a;
.implements Lcom/pspdfkit/internal/views/forms/c$a;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/tb$b;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/dm;

.field private final c:Lcom/pspdfkit/internal/yb;

.field private final d:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final e:Lcom/pspdfkit/internal/pr;

.field private final f:Lcom/pspdfkit/internal/ic;

.field private final g:Lcom/pspdfkit/internal/tb$b;

.field private final h:Lcom/pspdfkit/internal/na;

.field private final i:Lcom/pspdfkit/internal/zb;

.field private final j:Ljava/util/ArrayList;

.field private k:Lcom/pspdfkit/internal/zf;

.field private l:Z

.field private final m:Landroid/graphics/Matrix;

.field private n:Lcom/pspdfkit/forms/FormElement;

.field private o:Z

.field private final p:Lcom/pspdfkit/annotations/actions/ActionResolver;

.field private q:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$4e491XiEZVgLrA-BhaNmRV3cDfc(Lcom/pspdfkit/internal/tb;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$FlRQVPPdJ9rERnUxKMrOHQsHb4Q(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/xb;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb;->b(Lcom/pspdfkit/internal/xb;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ZRrK18M-6I4sSA9ZjKyrgFhfLQY(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/tb;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$fmP1vEgtvVMnAvTqfOgstFccHhU(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/xb;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb;->a(Lcom/pspdfkit/internal/xb;)V

    return-void
.end method

.method public static synthetic $r8$lambda$lO9PQ6ODGJ1w4nIe9T3ugEUFUSk()V
    .locals 0

    invoke-static {}, Lcom/pspdfkit/internal/tb;->g()V

    return-void
.end method

.method public static synthetic $r8$lambda$lRPe0tPXRKDVSL31FaMeUPyWK5k(Lcom/pspdfkit/internal/tb;Landroid/graphics/RectF;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb;->b(Landroid/graphics/RectF;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qgBDm8ot2VRfdapPGp2v8u00d1k(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/internal/xb;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/tb;->a(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/internal/xb;Ljava/lang/Boolean;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/dm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/yb;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->i:Lcom/pspdfkit/internal/zb;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/tb;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/tb;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/tb;->l:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/tb;)Landroid/graphics/Matrix;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->m:Landroid/graphics/Matrix;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetp(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/annotations/actions/ActionResolver;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    .line 10
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/tb;->m:Landroid/graphics/Matrix;

    .line 39
    iput-object p1, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 40
    iput-object p5, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    .line 41
    iput-object p4, p0, Lcom/pspdfkit/internal/tb;->e:Lcom/pspdfkit/internal/pr;

    .line 42
    iput-object p3, p0, Lcom/pspdfkit/internal/tb;->d:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 43
    iput-object p6, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    .line 45
    new-instance p3, Lcom/pspdfkit/internal/tb$b;

    const/4 p4, 0x0

    invoke-direct {p3, p0, p4}, Lcom/pspdfkit/internal/tb$b;-><init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/tb$b-IA;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/tb;->g:Lcom/pspdfkit/internal/tb$b;

    .line 47
    new-instance p3, Lcom/pspdfkit/internal/na;

    invoke-direct {p3, p7}, Lcom/pspdfkit/internal/na;-><init>(Lcom/pspdfkit/internal/a1;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/tb;->h:Lcom/pspdfkit/internal/na;

    .line 48
    sget-object p4, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/pspdfkit/internal/na;->a(Ljava/util/EnumSet;)V

    .line 50
    invoke-static {}, Lcom/pspdfkit/internal/x5;->c()Lcom/pspdfkit/internal/ic;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/tb;->f:Lcom/pspdfkit/internal/ic;

    .line 51
    new-instance p4, Lcom/pspdfkit/internal/zb;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget p3, p3, Lcom/pspdfkit/internal/ic;->c:I

    invoke-direct {p4, p1, p3}, Lcom/pspdfkit/internal/zb;-><init>(Landroid/content/Context;I)V

    iput-object p4, p0, Lcom/pspdfkit/internal/tb;->i:Lcom/pspdfkit/internal/zb;

    .line 53
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/tb;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/forms/FormElement;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/FormElement;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/xb<",
            "+",
            "Lcom/pspdfkit/forms/FormElement;",
            ">;>;"
        }
    .end annotation

    .line 28
    sget-object v0, Lcom/pspdfkit/internal/tb$a;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 51
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    .line 52
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    new-instance v0, Lcom/pspdfkit/internal/views/forms/a;

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 54
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/tb;->d:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->f:Lcom/pspdfkit/internal/ic;

    iget v5, v1, Lcom/pspdfkit/internal/ic;->c:I

    move-object v1, v0

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/views/forms/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/views/forms/c$a;)V

    .line 59
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/a;->setFormElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 60
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 97
    :cond_1
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/views/forms/c;

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 98
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->f:Lcom/pspdfkit/internal/ic;

    iget v2, v2, Lcom/pspdfkit/internal/ic;->c:I

    invoke-direct {v0, v1, v2, p0}, Lcom/pspdfkit/internal/views/forms/c;-><init>(Landroid/content/Context;ILcom/pspdfkit/internal/views/forms/c$a;)V

    .line 99
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/c;->setFormElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 100
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 61
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    const/4 v5, 0x0

    goto :goto_1

    .line 64
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageRotation(I)I

    move-result v0

    move v5, v0

    .line 65
    :goto_1
    new-instance v0, Lcom/pspdfkit/internal/views/forms/e;

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 66
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/tb;->d:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/tb;->f:Lcom/pspdfkit/internal/ic;

    iget-object v6, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/views/forms/e;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/ic;ILcom/pspdfkit/internal/yb;)V

    .line 67
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/annotations/d;->setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V

    .line 68
    check-cast p1, Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/forms/e;->setFormElement(Lcom/pspdfkit/forms/TextFormElement;)V

    .line 69
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/internal/xb;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 158
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 170
    iget-object p3, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 171
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p3

    .line 172
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/pspdfkit/internal/w1;->e(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 174
    invoke-interface {p3}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    .line 177
    :cond_0
    iget-object p3, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 178
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p3

    .line 180
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/xb;)V

    const/4 p2, 0x0

    .line 181
    invoke-virtual {p3, p1, p2, v0}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V

    goto :goto_0

    .line 186
    :cond_1
    new-instance p1, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda6;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/xb;)V

    .line 188
    iget-object p2, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p2, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 189
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_2
    :goto_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/xb;)V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    invoke-interface {p1}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->d:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/tb;->l:Z

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->h:Lcom/pspdfkit/internal/na;

    new-instance v1, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/zf;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/internal/na$a;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string v0, "PSPDFKit.Forms"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "Exception while loading form elements on page: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 10
    iget-object v4, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 14
    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    .line 15
    invoke-static {v0, p1, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Exception while loading form elements."

    .line 24
    invoke-static {v0, p1, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/tb;->l:Z

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, v0, :cond_0

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/uf;->hasFieldsCache()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private synthetic b(Landroid/graphics/RectF;)V
    .locals 7

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v1

    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v3

    const-wide/16 v4, 0xc8

    const/4 v6, 0x0

    move-object v2, p1

    .line 10
    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/internal/xb;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    invoke-interface {p1}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private static synthetic g()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    new-instance v1, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/tb;Landroid/graphics/RectF;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 26
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 27
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final a(Z)Z
    .locals 5

    .line 101
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 102
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/tb;->o:Z

    const/4 v1, 0x0

    .line 105
    iput-object v1, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    .line 108
    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/ac;->a(Lcom/pspdfkit/forms/FormElement;Z)V

    .line 110
    iget-object p1, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 111
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v2

    .line 113
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->j()Lio/reactivex/rxjava3/core/Single;

    move-result-object v3

    .line 114
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda2;

    invoke-direct {v4, p0, v2, v1}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/internal/xb;)V

    .line 115
    invoke-virtual {v3, v4}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 149
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->d()V

    goto :goto_0

    .line 151
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 154
    sget-object p1, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->LOOSE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 155
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 157
    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    new-instance v2, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v2, v0}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    invoke-interface {v1, p1, v2}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public final b(Landroid/view/MotionEvent;)Lcom/pspdfkit/forms/FormElement;
    .locals 3

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->h:Lcom/pspdfkit/internal/na;

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->m:Landroid/graphics/Matrix;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/uf;->hasFieldsCache()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    .line 16
    instance-of v0, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v0, :cond_1

    .line 18
    check-cast p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormElement;)V
    .locals 3

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 12
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 13
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->h()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final c()Lcom/pspdfkit/internal/tb$b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->g:Lcom/pspdfkit/internal/tb$b;

    return-object v0
.end method

.method public final c(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ac;->a(Lcom/pspdfkit/forms/FormElement;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 7
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 8
    instance-of v0, p1, Lcom/pspdfkit/forms/SignatureFormElement;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/forms/SignatureFormElement;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/forms/SignatureFormElement;->isSigned()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 14
    :goto_0
    sget v3, Lcom/pspdfkit/internal/ao;->a:I

    if-eqz p1, :cond_2

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v3

    if-nez v3, :cond_2

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_3

    if-nez v0, :cond_3

    return v2

    .line 18
    :cond_3
    sget-object v0, Lcom/pspdfkit/internal/tb$a;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    return v2

    .line 23
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/tb;->d(Lcom/pspdfkit/forms/FormElement;)V

    .line 24
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/forms/RadioButtonFormElement;

    invoke-static {v0}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/RadioButtonFormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 26
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    goto :goto_2

    .line 27
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/tb;->d(Lcom/pspdfkit/forms/FormElement;)V

    .line 28
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-static {v0}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/CheckBoxFormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_2

    .line 37
    :pswitch_3
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->e:Lcom/pspdfkit/internal/pr;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/pr;->onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z

    goto :goto_2

    .line 38
    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/tb;->d(Lcom/pspdfkit/forms/FormElement;)V

    .line 50
    :goto_2
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 52
    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    new-instance v3, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v3, p1}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    invoke-interface {v2, v0, v3}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    goto :goto_3

    .line 56
    :cond_4
    sget-object v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_UP:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    new-instance v3, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v3, p1}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    invoke-interface {v2, v0, v3}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    :cond_5
    :goto_3
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Lcom/pspdfkit/forms/FormElement;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/tb;->l:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    if-eq v0, p1, :cond_6

    .line 3
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v2

    if-nez v2, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    goto :goto_3

    .line 7
    :cond_1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ac;->e(Lcom/pspdfkit/forms/FormElement;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 12
    :cond_2
    iput-object p1, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    .line 15
    sget-object v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->RECEIVE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    if-nez p1, :cond_3

    goto :goto_1

    .line 16
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 18
    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->p:Lcom/pspdfkit/annotations/actions/ActionResolver;

    new-instance v3, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v3, p1}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    invoke-interface {v2, v0, v3}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    .line 19
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 23
    :try_start_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb;->a(Lcom/pspdfkit/forms/FormElement;)Ljava/util/List;

    move-result-object p1

    .line 25
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xb;

    .line 26
    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    invoke-interface {v0}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 28
    invoke-interface {v0}, Lcom/pspdfkit/internal/xb;->n()V

    goto :goto_2

    .line 32
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ac;->b(Lcom/pspdfkit/forms/FormElement;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 33
    :catch_0
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    :cond_6
    :goto_3
    return-void
.end method

.method public final e()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/tb;->o:Z

    return v0
.end method

.method public final i()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/tb;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->k:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/internal/uf;->prepareFieldsCache()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda3;

    invoke-direct {v1}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda3;-><init>()V

    new-instance v2, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/tb$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/tb;)V

    .line 7
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/tb;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

.method public synthetic isFormElementClickable(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener$-CC;->$default$isFormElementClickable(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    return p1
.end method

.method public final k()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->c:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V

    return-void
.end method

.method public final onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 3

    const/4 v0, 0x0

    .line 1
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/tb;->b:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/tb;->n:Lcom/pspdfkit/forms/FormElement;

    if-eq v1, p1, :cond_2

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p1

    sget-object v1, Lcom/pspdfkit/forms/FormType;->PUSHBUTTON:Lcom/pspdfkit/forms/FormType;

    if-eq p1, v1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/tb;->a(Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 5
    :catch_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    :cond_2
    :goto_1
    return v0
.end method

.class public final Lcom/pspdfkit/internal/j4$a;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/j4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/graphics/Point;

.field final synthetic b:Lcom/pspdfkit/internal/j4;


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/j4;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/j4$a;->b:Lcom/pspdfkit/internal/j4;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/MotionEvent;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/j4$a;->a:Landroid/graphics/Point;

    return-void
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4$a;->a:Landroid/graphics/Point;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/j4$a;->b:Lcom/pspdfkit/internal/j4;

    iget-object v1, v1, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    .line 4
    invoke-static {v1, v2, v0, v3, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/j4$a;->b:Lcom/pspdfkit/internal/j4;

    iget-object v0, v0, Lcom/pspdfkit/internal/j4;->f:Lcom/pspdfkit/internal/dm;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/j4$a;->b:Lcom/pspdfkit/internal/j4;

    iget-object v0, v0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/k1;

    .line 10
    instance-of v2, v1, Lcom/pspdfkit/internal/j4;

    if-eqz v2, :cond_0

    .line 11
    check-cast v1, Lcom/pspdfkit/internal/j4;

    .line 13
    invoke-virtual {v1}, Lcom/pspdfkit/internal/j4;->f()V

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4$a;->b:Lcom/pspdfkit/internal/j4;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/j4;->a(FF)V

    const/4 p1, 0x0

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/j4$a;->a:Landroid/graphics/Point;

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/j4$a;->a:Landroid/graphics/Point;

    return-void
.end method

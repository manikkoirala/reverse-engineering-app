.class public final Lcom/pspdfkit/internal/ur;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private b:Z

.field private c:I

.field private d:I

.field private e:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureView;

.field private f:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ur;->b:Z

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/ur;->a()V

    return-void
.end method

.method private a()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__signature_list_item:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const/16 v1, 0x10

    .line 3
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    const/high16 v1, 0x40000000    # 2.0f

    .line 4
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    const/4 v1, 0x0

    .line 5
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 6
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signature_view:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureView;

    iput-object v2, p0, Lcom/pspdfkit/internal/ur;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureView;

    .line 7
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v4, v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 9
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signer_chip:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    iput-object v2, p0, Lcom/pspdfkit/internal/ur;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    .line 10
    sget v2, Lcom/pspdfkit/R$id;->pspdf__signer_chip_container:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 11
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 13
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 14
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x101030e

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 15
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lcom/pspdfkit/internal/ur;->c:I

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$attr;->pspdf__signatureListSelectedItemBackground:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_translucent:I

    .line 18
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ur;->d:I

    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ur;->b:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ur;->b:Z

    if-eqz p1, :cond_0

    .line 2
    iget p1, p0, Lcom/pspdfkit/internal/ur;->d:I

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 4
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ur;->c:I

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method public setSignature(Lcom/pspdfkit/signatures/Signature;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ur;->e:Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignatureView;->setSignature(Lcom/pspdfkit/signatures/Signature;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    if-eqz p1, :cond_1

    .line 5
    invoke-static {}, Lcom/pspdfkit/signatures/SignatureManager;->getSigners()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/signatures/signers/Signer;

    :cond_1
    if-eqz v0, :cond_2

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ur;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;->setSigner(Lcom/pspdfkit/signatures/signers/Signer;)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/ur;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 10
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ur;->f:Lcom/pspdfkit/internal/ui/dialog/signatures/SignerChip;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method public final toggle()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ur;->b:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ur;->setChecked(Z)V

    return-void
.end method

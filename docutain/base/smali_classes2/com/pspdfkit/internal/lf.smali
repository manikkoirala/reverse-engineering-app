.class public final Lcom/pspdfkit/internal/lf;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/lf$a;,
        Lcom/pspdfkit/internal/lf$b;,
        Lcom/pspdfkit/internal/lf$c;,
        Lcom/pspdfkit/internal/lf$d;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\u0007\u00a2\u0006\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/pspdfkit/internal/lf;",
        "Landroidx/fragment/app/Fragment;",
        "<init>",
        "()V",
        "a",
        "b",
        "c",
        "d",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final synthetic l:I


# instance fields
.field private b:Z

.field private c:Lcom/pspdfkit/internal/lf$c;

.field private d:Lcom/pspdfkit/internal/lf$a;

.field private e:Landroid/content/Intent;

.field private f:Landroid/content/Intent;

.field private g:Ljava/lang/String;

.field private h:Landroid/net/Uri;

.field private final i:Lcom/pspdfkit/internal/of;

.field private j:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 70
    invoke-static {}, Lcom/pspdfkit/internal/gj;->k()Lcom/pspdfkit/internal/of;

    move-result-object v0

    const-string v1, "getIntentCreator()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->i:Lcom/pspdfkit/internal/of;

    return-void
.end method

.method private final a(Z)Landroid/content/Intent;
    .locals 3

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_3

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->i:Lcom/pspdfkit/internal/of;

    new-instance v1, Lcom/pspdfkit/internal/mf;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/mf;-><init>(Lcom/pspdfkit/internal/lf;)V

    check-cast p1, Lcom/pspdfkit/internal/r;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "imageFileCreator"

    .line 8
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/pspdfkit/internal/r;->a()Z

    move-result p1

    if-nez p1, :cond_0

    .line 44
    sget-object p1, Lcom/pspdfkit/internal/f5$b;->a:Lcom/pspdfkit/internal/f5$b;

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {v1}, Lcom/pspdfkit/internal/mf;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    if-nez p1, :cond_1

    sget-object p1, Lcom/pspdfkit/internal/f5$a;->a:Lcom/pspdfkit/internal/f5$a;

    goto :goto_0

    .line 50
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/f5$c;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/f5$c;-><init>(Landroid/net/Uri;)V

    move-object p1, v1

    .line 51
    :goto_0
    nop

    instance-of v1, p1, Lcom/pspdfkit/internal/f5$c;

    if-eqz v1, :cond_2

    .line 52
    check-cast p1, Lcom/pspdfkit/internal/f5$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/f5$c;->b()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    .line 53
    invoke-virtual {p1}, Lcom/pspdfkit/internal/f5$c;->a()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 55
    :cond_2
    sget-object v1, Lcom/pspdfkit/internal/f5$a;->a:Lcom/pspdfkit/internal/f5$a;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 56
    sget-object v1, Lcom/pspdfkit/internal/f5$b;->a:Lcom/pspdfkit/internal/f5$b;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ElectronicSignatures"

    const-string v2, "The device doesn\'t have a camera."

    .line 59
    invoke-static {v1, v2, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :cond_3
    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "requireContext()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/lf;->g:Ljava/lang/String;

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/internal/lf$b;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/util/List;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method private static a(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3

    .line 1379
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Signature_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".jpg"

    .line 1383
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->createTemporaryFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/lf;Landroid/content/Context;)Landroid/net/Uri;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/lf;)Lcom/pspdfkit/internal/lf$c;
    .locals 0

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/lf;->c:Lcom/pspdfkit/internal/lf$c;

    return-object p0
.end method

.method private final a(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 3

    .line 61
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "capturing images from camera"

    .line 62
    invoke-static {v0, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->checkProviderConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "imagePickerLauncher"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->i:Lcom/pspdfkit/internal/of;

    check-cast v0, Lcom/pspdfkit/internal/r;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r;->a()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 71
    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->k:Landroidx/activity/result/ActivityResultLauncher;

    if-nez p1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, p1

    :goto_0
    invoke-virtual {v1, p2}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    goto :goto_2

    .line 72
    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/pspdfkit/internal/lf;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 73
    iget-object p2, p0, Lcom/pspdfkit/internal/lf;->k:Landroidx/activity/result/ActivityResultLauncher;

    if-nez p2, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v1, p2

    :goto_1
    invoke-virtual {v1, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    goto :goto_2

    .line 75
    :cond_4
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    .line 76
    iput-object p2, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    :goto_2
    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/lf$a;)V
    .locals 6

    .line 77
    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->c:Lcom/pspdfkit/internal/lf$c;

    if-eqz v0, :cond_5

    .line 79
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lf$a;->a()I

    move-result v1

    .line 80
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lf$a;->b()Landroid/net/Uri;

    move-result-object p1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const-string v4, "context"

    const-string v5, "requireContext()"

    if-eq v1, v2, :cond_1

    if-eqz v1, :cond_0

    .line 104
    invoke-interface {v0}, Lcom/pspdfkit/internal/lf$c;->onImagePickerUnknownError()V

    .line 105
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    .line 106
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    .line 524
    invoke-static {p1, v0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z

    goto :goto_0

    .line 525
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/internal/lf$c;->onImagePickerCancelled()V

    .line 526
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    .line 527
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    .line 945
    invoke-static {p1, v0}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z

    goto :goto_0

    .line 946
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    if-eqz p1, :cond_2

    .line 949
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/lf$c;->onImagePicked(Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 952
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/lf$c;->onImagePicked(Landroid/net/Uri;)V

    .line 953
    iput-object v3, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    goto :goto_0

    .line 956
    :cond_3
    invoke-interface {v0}, Lcom/pspdfkit/internal/lf$c;->onImagePickerUnknownError()V

    .line 957
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 958
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_4

    .line 1376
    invoke-static {p1, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z

    .line 1377
    :cond_4
    :goto_0
    iput-object v3, p0, Lcom/pspdfkit/internal/lf;->d:Lcom/pspdfkit/internal/lf$a;

    .line 1378
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "parentFragmentManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Z)V

    :cond_5
    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/lf;Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/lf;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/lf;Lcom/pspdfkit/internal/lf$a;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf$a;)V

    return-void
.end method

.method private final a()Z
    .locals 7

    const-string v0, "android.permission.CAMERA"

    .line 1387
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/16 v3, 0x17

    if-lt v1, v3, :cond_4

    const/4 v1, 0x0

    .line 1388
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "packageManager"

    .line 1389
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1390
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "requireContext().packageName"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v5, 0x1000

    .line 1391
    invoke-static {v3, v4, v5}, Lcom/pspdfkit/utils/PackageManagerExtensions;->getSupportPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 1395
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const-string v4, "packageInfo.requestedPermissions"

    .line 1396
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v3, v5

    .line 1397
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_1

    const/4 v3, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :catch_0
    nop

    :cond_2
    :goto_1
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_4

    .line 1398
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    .line 1401
    iget-boolean v3, p0, Lcom/pspdfkit/internal/lf;->b:Z

    if-nez v3, :cond_4

    .line 1403
    iput-boolean v2, p0, Lcom/pspdfkit/internal/lf;->b:Z

    .line 1405
    iget-object v2, p0, Lcom/pspdfkit/internal/lf;->j:Landroidx/activity/result/ActivityResultLauncher;

    if-nez v2, :cond_3

    const-string v2, "requiredPermissionsCheckLauncher"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v2, 0x0

    :cond_3
    invoke-virtual {v2, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    return v1

    :cond_4
    return v2
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    return-object p0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/lf;Lcom/pspdfkit/internal/lf$a;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->d:Lcom/pspdfkit/internal/lf$a;

    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    return-object p0
.end method

.method public static final d(Lcom/pspdfkit/internal/lf;)V
    .locals 2

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->d:Lcom/pspdfkit/internal/lf$a;

    .line 2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Z)V

    return-void
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/lf;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    return-void
.end method

.method public static final synthetic f(Lcom/pspdfkit/internal/lf;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    return-void
.end method

.method public static final synthetic g(Lcom/pspdfkit/internal/lf;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/lf;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/lf$c;)V
    .locals 0

    .line 1384
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->c:Lcom/pspdfkit/internal/lf$c;

    .line 1385
    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->d:Lcom/pspdfkit/internal/lf$a;

    if-eqz p1, :cond_0

    .line 1386
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf$a;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->g:Ljava/lang/String;

    return-void
.end method

.method public final b()Z
    .locals 5

    const-string v0, "PSPDFKit.ElectronicSignatures"

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3
    :try_start_0
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/lf;->a(Z)Landroid/content/Intent;

    move-result-object v3

    .line 4
    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/lf;->a(Z)Landroid/content/Intent;

    move-result-object v4

    if-nez v3, :cond_1

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "Failed to capture image because the device does not support any intent action."

    new-array v4, v2, [Ljava/lang/Object;

    .line 12
    invoke-static {v0, v3, v4}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 13
    :cond_1
    :goto_0
    invoke-direct {p0, v3, v4}, Lcom/pspdfkit/internal/lf;->a(Landroid/content/Intent;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception v3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    const-string v3, "Failed to capture image due to security exception!"

    .line 26
    invoke-static {v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return v2
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "capturing images from camera"

    .line 6
    invoke-static {v0, v1}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->checkProviderConfiguration(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 12
    const-class v0, Landroid/content/Intent;

    const-string v1, "PENDING_INTENT_FOR_RESULT"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelable(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    .line 14
    const-class v0, Landroid/content/Intent;

    const-string v1, "PENDING_INTENT_NO_CAMERA_FOR_RESULT"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelable(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    .line 17
    const-class v0, Landroid/net/Uri;

    const-string v1, "TEMP_IMAGE_URI"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelable(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    .line 21
    :cond_0
    new-instance p1, Landroidx/activity/result/contract/ActivityResultContracts$RequestPermission;

    invoke-direct {p1}, Landroidx/activity/result/contract/ActivityResultContracts$RequestPermission;-><init>()V

    new-instance v0, Lcom/pspdfkit/internal/lf$e;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/lf$e;-><init>(Lcom/pspdfkit/internal/lf;)V

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    move-result-object p1

    const-string v0, "override fun onCreate(sa\u2026s = false\n        }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->j:Landroidx/activity/result/ActivityResultLauncher;

    .line 44
    new-instance p1, Lcom/pspdfkit/internal/lf$d;

    invoke-direct {p1}, Lcom/pspdfkit/internal/lf$d;-><init>()V

    new-instance v1, Lcom/pspdfkit/internal/lf$f;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/lf$f;-><init>(Lcom/pspdfkit/internal/lf;)V

    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->k:Landroidx/activity/result/ActivityResultLauncher;

    .line 49
    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    if-eqz p1, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/lf;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 50
    iget-object p1, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/lf;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    const/4 p1, 0x0

    .line 51
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    .line 52
    iput-object p1, p0, Lcom/pspdfkit/internal/lf;->f:Landroid/content/Intent;

    const/4 p1, 0x0

    .line 53
    iput-boolean p1, p0, Lcom/pspdfkit/internal/lf;->b:Z

    :cond_2
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->h:Landroid/net/Uri;

    const-string v1, "TEMP_IMAGE_URI"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/lf;->e:Landroid/content/Intent;

    const-string v1, "PENDING_INTENT_FOR_RESULT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/um;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/h7;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/ui/PdfFragment;

.field private final c:Lcom/pspdfkit/internal/i7;


# direct methods
.method public static synthetic $r8$lambda$hmLRr_pXduL5-LKPWu-pQFwnbss(Lcom/pspdfkit/internal/um;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/um;->c(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xU8LxtvB1ZDFBe6aLchx4ALI5ws(Lcom/pspdfkit/internal/um;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/um;->c()V

    return-void
.end method

.method public static synthetic $r8$lambda$xXMvxlWoFR7mltp1eRRWrfdFcB8(Lcom/pspdfkit/internal/um;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/um;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/um;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/um;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/i7;

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p2

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/i7;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/vu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    return-void
.end method

.method private synthetic b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_copied:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private synthetic c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_cut:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private c(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/um;->a:Landroid/content/Context;

    sget v0, Lcom/pspdfkit/R$string;->pspdf__annotation_pasted:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    .line 6
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/i7;->a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/um;)V

    .line 5
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a(I)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    .line 8
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/i7;->a(I)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/um;)V

    .line 10
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/PointF;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    .line 12
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/i7;->a(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/um;)V

    .line 14
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/i7;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/i7;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/um;->c:Lcom/pspdfkit/internal/i7;

    .line 4
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/i7;->b(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/um$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/um;)V

    .line 6
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

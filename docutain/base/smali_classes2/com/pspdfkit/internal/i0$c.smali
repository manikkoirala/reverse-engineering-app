.class public final Lcom/pspdfkit/internal/i0$c;
.super Lcom/pspdfkit/internal/yg;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/i0;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$-CC;->builder(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;

    move-result-object v0

    const v1, 0x3eb33333    # 0.35f

    .line 2
    invoke-interface {v0, v1}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration$Builder;->setDefaultAlpha(F)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;

    .line 3
    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->HIGHLIGHTER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    invoke-static {v2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    const-string v3, "fromPreset(Preset.HIGHLIGHTER)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder;->setDefaultColor(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;

    const/high16 v0, 0x41f00000    # 30.0f

    .line 4
    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder;->setDefaultThickness(F)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;->build()Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration;

    move-result-object p1

    const-string v0, "builder(context)\n       \u2026                 .build()"

    .line 6
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

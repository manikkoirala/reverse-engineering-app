.class public final Lcom/pspdfkit/internal/hi;
.super Lcom/pspdfkit/internal/hv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/hv<",
        "Lcom/pspdfkit/internal/xr;",
        ">;"
    }
.end annotation


# instance fields
.field private final E:Lcom/pspdfkit/internal/ds$a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/ds$a;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hv;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/hi;->E:Lcom/pspdfkit/internal/ds$a;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hi;->E:Lcom/pspdfkit/internal/ds$a;

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x11

    goto :goto_0

    :cond_0
    const/16 v0, 0x10

    :goto_0
    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hi;->E:Lcom/pspdfkit/internal/ds$a;

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_0

    .line 2
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_0

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    :goto_0
    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/b2;
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/ds;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v4

    .line 6
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 7
    iget-object v6, p0, Lcom/pspdfkit/internal/hi;->E:Lcom/pspdfkit/internal/ds$a;

    move-object v0, v7

    .line 8
    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/ds;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/internal/ds$a;)V

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/ri;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ri;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;)V

    invoke-virtual {v7, v0}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    .line 19
    new-instance v0, Lcom/pspdfkit/internal/xr;

    invoke-direct {v0, v7}, Lcom/pspdfkit/internal/xr;-><init>(Lcom/pspdfkit/internal/ds;)V

    return-object v0
.end method

.method protected final q()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hi;->E:Lcom/pspdfkit/internal/ds$a;

    sget-object v1, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/pspdfkit/internal/i4;->q()Z

    move-result v0

    :goto_0
    return v0
.end method

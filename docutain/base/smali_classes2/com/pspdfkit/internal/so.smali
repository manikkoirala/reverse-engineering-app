.class public final Lcom/pspdfkit/internal/so;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/redaction/RedactionView$RedactionViewListener;


# instance fields
.field private final a:Landroidx/appcompat/app/AppCompatActivity;

.field private final b:Lcom/pspdfkit/internal/fl;

.field private final c:Lcom/pspdfkit/internal/zf;

.field private final d:Lcom/pspdfkit/annotations/AnnotationProvider;

.field private final e:Lcom/pspdfkit/document/editor/FilePicker;

.field private final f:Lcom/pspdfkit/ui/PdfUi;


# direct methods
.method public static synthetic $r8$lambda$0ByPwn4lKkTkTrvSmmNmFnDPFYM(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/so;->b(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$A54iWx0dSXWqzRZ1_6hALPGtXx8()V
    .locals 0

    invoke-static {}, Lcom/pspdfkit/internal/so;->b()V

    return-void
.end method

.method public static synthetic $r8$lambda$ssP3VbFGBnd1jaU96qyFkAVq7Lc(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/so;->a(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/vu;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/AnnotationProvider;Lcom/pspdfkit/internal/x7;Lcom/pspdfkit/ui/PdfUi;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filePicker"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfUi"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/so;->a:Landroidx/appcompat/app/AppCompatActivity;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/so;->b:Lcom/pspdfkit/internal/fl;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/so;->d:Lcom/pspdfkit/annotations/AnnotationProvider;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/so;->e:Lcom/pspdfkit/document/editor/FilePicker;

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/internal/so;->f:Lcom/pspdfkit/ui/PdfUi;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/so;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/so;->a:Landroidx/appcompat/app/AppCompatActivity;

    return-object p0
.end method

.method private final a()V
    .locals 6

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/so;->e:Lcom/pspdfkit/document/editor/FilePicker;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/so;->a:Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    sget v2, Lcom/pspdfkit/R$string;->pspdf__filename_redacted:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 7
    iget-object v4, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    invoke-static {v1, v4}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x0

    .line 8
    invoke-static {v1, v2, v4, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/internal/kb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sanitizeFileName(\n      \u2026)\n            )\n        )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "android.intent.action.CREATE_DOCUMENT"

    .line 10
    invoke-interface {v0, v2, v1}, Lcom/pspdfkit/document/editor/FilePicker;->getDestinationUri(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 12
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 13
    new-instance v1, Lcom/pspdfkit/internal/so$c;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/so$c;-><init>(Lcom/pspdfkit/internal/so;)V

    sget-object v2, Lcom/pspdfkit/internal/so$d;->a:Lcom/pspdfkit/internal/so$d;

    new-instance v3, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda2;

    invoke-direct {v3}, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/so;->a()V

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/annotations/AnnotationProvider;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/so;->d:Lcom/pspdfkit/annotations/AnnotationProvider;

    return-object p0
.end method

.method private static final b()V
    .locals 0

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/so;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 3
    sget p1, Lcom/pspdfkit/internal/uo;->g:I

    iget-object p1, p0, Lcom/pspdfkit/internal/so;->a:Landroidx/appcompat/app/AppCompatActivity;

    iget-object p0, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/uo$a;->a(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/internal/zf;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/internal/fl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/so;->b:Lcom/pspdfkit/internal/fl;

    return-object p0
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/ui/PdfUi;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/so;->f:Lcom/pspdfkit/ui/PdfUi;

    return-object p0
.end method


# virtual methods
.method public final onPreviewModeChanged(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/so;->f:Lcom/pspdfkit/ui/PdfUi;

    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/so;->f:Lcom/pspdfkit/ui/PdfUi;

    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getPSPDFKitViews()Lcom/pspdfkit/ui/PSPDFKitViews;

    move-result-object v0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews;->getThumbnailBarView()Lcom/pspdfkit/ui/PdfThumbnailBar;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfThumbnailBar;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 5
    :goto_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews;->getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 6
    :goto_1
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews;->getOutlineView()Lcom/pspdfkit/ui/PdfOutlineView;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 7
    :cond_2
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/redaction/RedactionView;->setRedactionAnnotationPreviewEnabled(Z)V

    .line 8
    :goto_2
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews;->getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;

    move-result-object p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/redaction/RedactionView;->collapseRedactionOptions(Z)V

    :cond_4
    return-void
.end method

.method public final onRedactionsApplied()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/so;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->isValidForEditing()Z

    move-result v0

    .line 3
    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/pspdfkit/internal/so;->a:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    sget v2, Lcom/pspdfkit/R$string;->pspdf__redaction_apply_redactions:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 5
    sget v2, Lcom/pspdfkit/R$string;->pspdf__redaction_apply_dialog_message:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 6
    sget v2, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 7
    sget v2, Lcom/pspdfkit/R$string;->pspdf__redaction_apply_dialog_new_file:I

    new-instance v3, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/so;)V

    invoke-virtual {v1, v2, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    if-eqz v0, :cond_0

    .line 10
    sget v0, Lcom/pspdfkit/R$string;->pspdf__redaction_apply_dialog_overwrite_file:I

    new-instance v2, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/so$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/so;)V

    invoke-virtual {v1, v0, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    .line 13
    :cond_0
    invoke-virtual {v1}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    return-void
.end method

.method public final onRedactionsCleared()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/so;->d:Lcom/pspdfkit/annotations/AnnotationProvider;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/pspdfkit/internal/so$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/so$a;-><init>(Lcom/pspdfkit/internal/so;)V

    sget-object v2, Lcom/pspdfkit/internal/so$b;->a:Lcom/pspdfkit/internal/so$b;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

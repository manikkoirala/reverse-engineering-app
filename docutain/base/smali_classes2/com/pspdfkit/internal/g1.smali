.class final Lcom/pspdfkit/internal/g1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/document/PdfDocument;

.field final synthetic b:Lcom/pspdfkit/annotations/Annotation;

.field final synthetic c:I


# direct methods
.method constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;I)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/g1;->a:Lcom/pspdfkit/document/PdfDocument;

    iput-object p2, p0, Lcom/pspdfkit/internal/g1;->b:Lcom/pspdfkit/annotations/Annotation;

    iput p3, p0, Lcom/pspdfkit/internal/g1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 1
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g1;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/g1;->b:Lcom/pspdfkit/annotations/Annotation;

    iget v2, p0, Lcom/pspdfkit/internal/g1;->c:I

    add-int/2addr p1, v2

    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;I)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/pspdfkit/internal/al;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/al$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method public static synthetic $r8$lambda$95Mfg-QDzwOaQ6LOyaqGcUokI0I(Lcom/pspdfkit/internal/al$a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/al;->a(Lcom/pspdfkit/internal/al$a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$XhN2oWorEuC9DTRAvfzX5HaRTpY(Lcom/pspdfkit/internal/al$a;Landroid/view/MenuItem;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/al;->a(Lcom/pspdfkit/internal/al$a;Landroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroidx/appcompat/widget/Toolbar;Lcom/pspdfkit/internal/al$a;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    .line 4
    sget v0, Lcom/pspdfkit/R$menu;->pspdf__menu_note_annotation_editor_toolbar:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->inflateMenu(I)V

    .line 6
    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_arrow_back:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    const/4 v1, -0x1

    .line 10
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 11
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__toolbar_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setElevation(F)V

    .line 12
    new-instance v0, Lcom/pspdfkit/internal/al$$ExternalSyntheticLambda0;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/al$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/al$a;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/al$$ExternalSyntheticLambda1;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/al$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/al$a;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setOnMenuItemClickListener(Landroidx/appcompat/widget/Toolbar$OnMenuItemClickListener;)V

    .line 25
    new-instance p2, Lcom/pspdfkit/internal/bl;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/bl;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 27
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 28
    invoke-virtual {p2}, Lcom/pspdfkit/internal/bl;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_1
    const/4 p1, 0x2

    .line 30
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 31
    invoke-virtual {p2}, Lcom/pspdfkit/internal/bl;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_2
    const/4 p1, 0x3

    .line 33
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 34
    invoke-virtual {p2}, Lcom/pspdfkit/internal/bl;->a()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_3
    return-void
.end method

.method private a(I)Landroid/view/MenuItem;
    .locals 3

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    sub-int/2addr p1, v2

    if-eqz p1, :cond_2

    if-eq p1, v2, :cond_1

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    return-object v1

    .line 30
    :cond_0
    sget p1, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_delete:I

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1

    .line 31
    :cond_1
    sget p1, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_redo:I

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1

    .line 32
    :cond_2
    sget p1, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_undo:I

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1

    .line 33
    :cond_3
    throw v1
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/al$a;Landroid/view/View;)V
    .locals 0

    .line 1
    check-cast p0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cl;->f()V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/al$a;Landroid/view/MenuItem;)Z
    .locals 2

    .line 2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_undo:I

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    .line 4
    check-cast p0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/cl;->a(I)V

    goto :goto_0

    .line 5
    :cond_0
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_redo:I

    if-ne p1, v0, :cond_1

    .line 6
    check-cast p0, Lcom/pspdfkit/internal/cl;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cl;->a(I)V

    goto :goto_0

    .line 7
    :cond_1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar_item_delete:I

    if-ne p1, v0, :cond_2

    .line 8
    check-cast p0, Lcom/pspdfkit/internal/cl;

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cl;->a(I)V

    :cond_2
    :goto_0
    return v1
.end method


# virtual methods
.method public final a(IZ)V
    .locals 5

    if-eqz p2, :cond_1

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/16 p1, 0x12c

    .line 10
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 11
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 12
    :cond_0
    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v4, v2

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-direct {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 13
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 14
    invoke-static {p2, v3}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 15
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 3

    const/4 v0, 0x3

    .line 17
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 19
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v2, 0xff

    goto :goto_0

    :cond_0
    const/16 v2, 0x64

    .line 20
    :goto_0
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 21
    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 22
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0}, Landroidx/appcompat/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3
    invoke-static {v1, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const/4 v0, 0x1

    .line 7
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 8
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 9
    invoke-static {v1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 10
    invoke-static {v1, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 11
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_1
    const/4 v0, 0x2

    .line 13
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 14
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 15
    invoke-static {v1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 16
    invoke-static {v1, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 17
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_2
    const/4 v0, 0x3

    .line 19
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 20
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 21
    invoke-static {v1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 22
    invoke-static {v1, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 23
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 25
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setTitleTextColor(I)V

    return-void
.end method

.method public final c(I)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/al;->a(I)Landroid/view/MenuItem;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 2
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/al;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    return-void
.end method

.class final Lcom/pspdfkit/internal/u2;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/z2;

.field final synthetic b:Lcom/pspdfkit/ui/audio/AudioPlaybackController;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/u2;->a:Lcom/pspdfkit/internal/z2;

    iput-object p2, p0, Lcom/pspdfkit/internal/u2;->b:Lcom/pspdfkit/ui/audio/AudioPlaybackController;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/u2;->a:Lcom/pspdfkit/internal/z2;

    invoke-static {v0}, Lcom/pspdfkit/internal/z2;->a(Lcom/pspdfkit/internal/z2;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/u2;->b:Lcom/pspdfkit/ui/audio/AudioPlaybackController;

    .line 36
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;

    .line 37
    invoke-interface {v2, v1}, Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;->onEnterAudioPlaybackMode(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    goto :goto_0

    .line 38
    :cond_0
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

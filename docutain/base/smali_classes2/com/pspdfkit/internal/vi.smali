.class public final Lcom/pspdfkit/internal/vi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 10
    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__MeasurementTool:[I

    .line 11
    sget v2, Lcom/pspdfkit/R$attr;->pspdf__measurementToolsStyle:I

    .line 12
    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_MeasurementTools:I

    const/4 v4, 0x0

    .line 13
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.theme\n        .o\u2026easurementTools\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__MeasurementTool_pspdf__measurementValuePopupBackgroundColor:I

    .line 25
    sget v2, Lcom/pspdfkit/R$color;->pspdf__measurement_popup_value_background:I

    .line 26
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 27
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/vi;->a:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/vi;->a:I

    return v0
.end method

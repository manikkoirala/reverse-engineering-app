.class public final Lcom/pspdfkit/internal/ta;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/files/EmbeddedFilesProvider;


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;


# direct methods
.method public static synthetic $r8$lambda$0CuJz_2ywA976esfvFW_PQDdDVc(Lcom/pspdfkit/internal/ta;ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ta;->a(ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$FZl1HUwhC5yuGc3zjifNbr7rSRI(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ta;->a(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$_8WjTDUVNHNpFOhAuMWFPiqNXC0(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ta;->b(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$t4Ug42q6uPmc5fFnRi-xa6OyO78(Lcom/pspdfkit/internal/ta;Z)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ta;->a(Z)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$tuAu33BNT2q6DAIHdOABM3YQPL0(Lcom/pspdfkit/internal/ta;ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ta;->b(ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method private synthetic a(ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda0;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/pspdfkit/internal/ta;->a(ZLio/reactivex/rxjava3/functions/Predicate;Z)Ljava/util/ArrayList;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/files/EmbeddedFile;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private synthetic a(Z)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ta;->getEmbeddedFiles(Z)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private a(ZLio/reactivex/rxjava3/functions/Predicate;Z)Ljava/util/ArrayList;
    .locals 5

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->j()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findEmbeddedFiles(Lcom/pspdfkit/internal/jni/NativeDocument;)Ljava/util/ArrayList;

    move-result-object v0

    .line 8
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 10
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 11
    new-instance v3, Lcom/pspdfkit/internal/sa;

    iget-object v4, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    invoke-direct {v3, v4, v2}, Lcom/pspdfkit/internal/sa;-><init>(Lcom/pspdfkit/internal/zf;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 13
    :try_start_0
    invoke-interface {p2, v3}, Lio/reactivex/rxjava3/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 14
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p3, :cond_0

    return-object v1

    :catchall_0
    nop

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_6

    const/4 p1, 0x0

    .line 23
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_6

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 26
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_3

    .line 27
    check-cast v2, Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/FileAnnotation;->getFile()Lcom/pspdfkit/document/files/EmbeddedFile;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz p2, :cond_4

    .line 29
    :try_start_1
    invoke-interface {p2, v2}, Lio/reactivex/rxjava3/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 30
    :cond_4
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p3, :cond_3

    return-object v1

    :catchall_1
    nop

    goto :goto_2

    :cond_5
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_6
    return-object v1
.end method

.method private static synthetic a(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/document/files/EmbeddedFile;->getFileName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private synthetic b(ZLjava/lang/String;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda2;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda2;-><init>(Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/pspdfkit/internal/ta;->a(ZLio/reactivex/rxjava3/functions/Predicate;Z)Ljava/util/ArrayList;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/files/EmbeddedFile;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private static synthetic b(Ljava/lang/String;Lcom/pspdfkit/document/files/EmbeddedFile;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/document/files/EmbeddedFile;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final getEmbeddedFileWithFileNameAsync(Ljava/lang/String;Z)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/document/files/EmbeddedFile;",
            ">;"
        }
    .end annotation

    const-string v0, "fileName"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p2, p1}, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ta;ZLjava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 59
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final getEmbeddedFileWithIdAsync(Ljava/lang/String;Z)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/document/files/EmbeddedFile;",
            ">;"
        }
    .end annotation

    const-string v0, "id"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p2, p1}, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/ta;ZLjava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 59
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final getEmbeddedFiles(Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/files/EmbeddedFile;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/internal/ta;->a(ZLio/reactivex/rxjava3/functions/Predicate;Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final getEmbeddedFilesAsync(Z)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/files/EmbeddedFile;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ta$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/ta;Z)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ta;->a:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

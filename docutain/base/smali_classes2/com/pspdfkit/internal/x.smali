.class public final Lcom/pspdfkit/internal/x;
.super Lcom/pspdfkit/internal/p0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/x$a;
    }
.end annotation


# instance fields
.field final c:Lcom/pspdfkit/internal/p1;

.field final d:Lcom/pspdfkit/annotations/AnnotationType;

.field final e:Lcom/pspdfkit/internal/x$a;

.field final f:Landroid/graphics/Bitmap;

.field final g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

.field final h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/x$a;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/p0;-><init>(II)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    .line 3
    new-instance p2, Lcom/pspdfkit/internal/p1;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/pspdfkit/internal/p1;-><init>(Lcom/pspdfkit/internal/p1;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/x;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    .line 9
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    .line 10
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 12
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    .line 14
    :goto_0
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p2, v0, :cond_2

    .line 15
    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioData()[B

    move-result-object v3

    if-eqz v3, :cond_1

    .line 18
    new-instance p2, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object v4

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleRate()I

    move-result v5

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleSize()I

    move-result v6

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getChannels()I

    move-result v7

    const/4 v8, 0x0

    move-object v2, p2

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;-><init>([BLcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    goto :goto_1

    .line 26
    :cond_1
    iput-object v1, p0, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    goto :goto_1

    .line 29
    :cond_2
    iput-object v1, p0, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    :goto_1
    return-void
.end method

.method public static a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/x;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/x;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/x$a;)V

    return-object v0
.end method

.method public static b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/x;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->b:Lcom/pspdfkit/internal/x$a;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/x;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/x$a;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/x;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/x;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    iget-object v3, p1, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    iget-object v3, p1, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    iget-object v3, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    iget-object v3, p1, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    .line 6
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    iget-object v3, p1, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    .line 7
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    iget-object p1, p1, Lcom/pspdfkit/internal/x;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    .line 8
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.class public final Lcom/pspdfkit/internal/m1;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# static fields
.field public static final synthetic e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;[I)I
    .locals 2

    .line 7
    array-length v0, p1

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0, v1}, Lcom/pspdfkit/internal/mb;->a(III)V

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result p0

    return p0
.end method

.method public static a(Ljava/nio/ByteBuffer;)Lcom/pspdfkit/internal/m1;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/m1;

    invoke-direct {v0}, Lcom/pspdfkit/internal/m1;-><init>()V

    .line 2
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    move-result v2

    add-int/2addr v2, v1

    .line 3
    iput v2, v0, Lcom/pspdfkit/internal/ot;->a:I

    .line 4
    iput-object p0, v0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    return-object v0
.end method


# virtual methods
.method public final A()I
    .locals 3

    const/16 v0, 0x8a

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x88

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final C()Z
    .locals 3

    const/16 v0, 0x70

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final D()Lcom/pspdfkit/internal/u7;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/u7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/u7;-><init>()V

    const/16 v1, 0x68

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/u7;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/u7;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final E()I
    .locals 1

    const/16 v0, 0x4c

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final F()B
    .locals 3

    const/16 v0, 0x50

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final G()F
    .locals 3

    const/16 v0, 0x2c

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final H()I
    .locals 1

    const/16 v0, 0x1a

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final I()S
    .locals 3

    const/16 v0, 0x98

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final J()Lcom/pspdfkit/internal/si;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/si;

    invoke-direct {v0}, Lcom/pspdfkit/internal/si;-><init>()V

    const/16 v1, 0x96

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/si;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/si;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final K()I
    .locals 3

    const/16 v0, 0x84

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final L()I
    .locals 3

    const/16 v0, 0x82

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x64

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final N()Z
    .locals 3

    const/16 v0, 0x74

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final O()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0x90

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x92

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final Q()I
    .locals 3

    const/4 v0, 0x6

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final R()J
    .locals 3

    const/16 v0, 0xa

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final S()I
    .locals 1

    const/16 v0, 0x18

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final T()B
    .locals 3

    const/16 v0, 0x52

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final U()I
    .locals 1

    const/16 v0, 0x16

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final V()Z
    .locals 3

    const/16 v0, 0x94

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final W()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x5c

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final X()I
    .locals 3

    const/16 v0, 0x8

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final Y()I
    .locals 3

    const/16 v0, 0x7a

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final Z()I
    .locals 3

    const/16 v0, 0x78

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a()Lcom/pspdfkit/internal/b;
    .locals 3

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/b;-><init>()V

    const/16 v1, 0x1c

    .line 6
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/b;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final a0()I
    .locals 3

    const/16 v0, 0x7c

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x1e

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b0()I
    .locals 3

    const/16 v0, 0x76

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c()Lcom/pspdfkit/internal/ob;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ob;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ob;-><init>()V

    const/16 v1, 0x2e

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ob;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ob;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final c0()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x60

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x7e

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final d0()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x62

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x80

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final e0()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x5e

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final f(I)Lcom/pspdfkit/internal/w;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/w;

    invoke-direct {v0}, Lcom/pspdfkit/internal/w;-><init>()V

    const/16 v1, 0x1e

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/w;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/w;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final f()S
    .locals 3

    const/16 v0, 0x8c

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f0()Lcom/pspdfkit/internal/ha;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ha;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ha;-><init>()V

    const/16 v1, 0x4a

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ha;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ha;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final g(I)I
    .locals 2

    const/16 v0, 0x32

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v0

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v0

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final g()Lcom/pspdfkit/internal/ko;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ko;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ko;-><init>()V

    const/16 v1, 0x12

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ko;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ko;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final g0()B
    .locals 3

    const/16 v0, 0x44

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h()S
    .locals 3

    const/16 v0, 0x30

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h(I)S
    .locals 2

    const/16 v0, 0x4c

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v0

    mul-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v0

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final h0()B
    .locals 3

    const/16 v0, 0x42

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i(I)Lcom/pspdfkit/internal/bh;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/bh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/bh;-><init>()V

    const/16 v1, 0x1a

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/bh;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/bh;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final i()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0x24

    .line 4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final i0()S
    .locals 3

    const/4 v0, 0x4

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final j(I)Lcom/pspdfkit/internal/ln;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ln;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ln;-><init>()V

    const/16 v1, 0x18

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x8

    add-int/2addr p1, v1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ln;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ln;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final j()S
    .locals 3

    const/16 v0, 0x36

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final j0()Ljava/lang/String;
    .locals 2

    const/16 v0, 0xc

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final k()F
    .locals 3

    const/16 v0, 0x38

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final k(I)Lcom/pspdfkit/internal/io;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/io;

    invoke-direct {v0}, Lcom/pspdfkit/internal/io;-><init>()V

    const/16 v1, 0x16

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x20

    add-int/2addr p1, v1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/io;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/io;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final k0()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x54

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final l()S
    .locals 3

    const/16 v0, 0x34

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0x22

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final n()Lcom/pspdfkit/internal/ko;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ko;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ko;-><init>()V

    const/16 v1, 0x14

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ko;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ko;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x5a

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final p()Lcom/pspdfkit/internal/u7;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/u7;

    invoke-direct {v0}, Lcom/pspdfkit/internal/u7;-><init>()V

    const/16 v1, 0x66

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/u7;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/u7;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x6c

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final r()Lcom/pspdfkit/internal/ng;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ng;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ng;-><init>()V

    const/16 v1, 0xe

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ng;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ng;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final s()I
    .locals 1

    const/16 v0, 0x32

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final t()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0x26

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final u()J
    .locals 3

    const/16 v0, 0x6a

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x3e

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final w()F
    .locals 3

    const/16 v0, 0x40

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final x()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0x2a

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final y()B
    .locals 3

    const/16 v0, 0x72

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x20

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

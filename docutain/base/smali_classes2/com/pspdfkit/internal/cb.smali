.class public final Lcom/pspdfkit/internal/cb;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/cb$a;,
        Lcom/pspdfkit/internal/cb$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/cb$b;


# instance fields
.field private final a:Ljava/lang/Float;

.field private final b:Lcom/pspdfkit/internal/p;

.field private final c:Lcom/pspdfkit/internal/fj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/cb$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/cb$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/cb;->Companion:Lcom/pspdfkit/internal/cb$b;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/Float;Lcom/pspdfkit/internal/p;Lcom/pspdfkit/internal/fj;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x6

    const/4 v1, 0x6

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/cb$a;->a:Lcom/pspdfkit/internal/cb$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cb$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    and-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/pspdfkit/internal/cb;->a:Ljava/lang/Float;

    goto :goto_0

    :cond_1
    iput-object p2, p0, Lcom/pspdfkit/internal/cb;->a:Ljava/lang/Float;

    :goto_0
    iput-object p3, p0, Lcom/pspdfkit/internal/cb;->b:Lcom/pspdfkit/internal/p;

    iput-object p4, p0, Lcom/pspdfkit/internal/cb;->c:Lcom/pspdfkit/internal/fj;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p;Lcom/pspdfkit/internal/fj;)V
    .locals 1

    const-string v0, "alignment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modificationsCharacterStyle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/cb;->a:Ljava/lang/Float;

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/cb;->b:Lcom/pspdfkit/internal/p;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/cb;->c:Lcom/pspdfkit/internal/fj;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/cb;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1
    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/cb;->a:Ljava/lang/Float;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    sget-object v1, Lkotlinx/serialization/internal/FloatSerializer;->INSTANCE:Lkotlinx/serialization/internal/FloatSerializer;

    iget-object v3, p0, Lcom/pspdfkit/internal/cb;->a:Ljava/lang/Float;

    invoke-interface {p1, p2, v0, v1, v3}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_2
    sget-object v0, Lcom/pspdfkit/internal/p;->Companion:Lcom/pspdfkit/internal/p$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/cb;->b:Lcom/pspdfkit/internal/p;

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/fj$a;->a:Lcom/pspdfkit/internal/fj$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/cb;->c:Lcom/pspdfkit/internal/fj;

    const/4 v1, 0x2

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/n9;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

.field private final b:Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocumentProvider;Ljava/lang/String;Lcom/pspdfkit/internal/vj;)V
    .locals 1

    const-string v0, "documentProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nativePlatformDelegate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/n9;->a:Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    .line 10
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->configureDocumentScriptExecutor(Ljava/lang/String;)V

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->getDocumentScriptExecutor()Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object p1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/pspdfkit/internal/n9;->b:Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    .line 13
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->setPlatformDelegate(Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;)V

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Document script executor could not be initialized!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeDocumentProvider;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/n9;->a:Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    return-object p0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/n9;->b:Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    return-object p0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/n9;->a:Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->executeDocumentLevelJavascripts()V

    return-void
.end method

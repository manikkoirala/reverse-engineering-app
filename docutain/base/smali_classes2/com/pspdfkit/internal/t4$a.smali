.class final Lcom/pspdfkit/internal/t4$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/s4$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/t4;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/t4;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/t4;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/t4$a;->a:Lcom/pspdfkit/internal/t4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/bookmarks/Bookmark;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4$a;->a:Lcom/pspdfkit/internal/t4;

    invoke-static {v0}, Lcom/pspdfkit/internal/t4;->-$$Nest$fgetm(Lcom/pspdfkit/internal/t4;)Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkPositionSet(Lcom/pspdfkit/bookmarks/Bookmark;I)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/bookmarks/Bookmark;)Z
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/t4$a;->a:Lcom/pspdfkit/internal/t4;

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/t4;->-$$Nest$fgetm(Lcom/pspdfkit/internal/t4;)Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkRemove(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b(Lcom/pspdfkit/bookmarks/Bookmark;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4$a;->a:Lcom/pspdfkit/internal/t4;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/t4;->-$$Nest$fgeto(Lcom/pspdfkit/internal/t4;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/t4;->-$$Nest$fgetr(Lcom/pspdfkit/internal/t4;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4
    invoke-static {v0, p1, p2}, Lcom/pspdfkit/internal/t4;->-$$Nest$ma(Lcom/pspdfkit/internal/t4;Lcom/pspdfkit/bookmarks/Bookmark;I)V

    goto :goto_0

    .line 7
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/t4;->-$$Nest$fgetm(Lcom/pspdfkit/internal/t4;)Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 8
    invoke-interface {p2, p1}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkClicked(Lcom/pspdfkit/bookmarks/Bookmark;)V

    .line 10
    :cond_1
    iget-object p1, v0, Lcom/pspdfkit/internal/ql;->b:Lcom/pspdfkit/internal/ql$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ql$a;->hide()V

    :cond_2
    :goto_0
    return-void
.end method

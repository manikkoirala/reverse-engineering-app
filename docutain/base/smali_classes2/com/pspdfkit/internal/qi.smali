.class public final Lcom/pspdfkit/internal/qi;
.super Lcom/pspdfkit/internal/e4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/e4<",
        "Lcom/pspdfkit/internal/rn;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/e4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x13

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final t()Lcom/pspdfkit/internal/c4;
    .locals 9

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/sn;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v4

    .line 6
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 7
    new-instance v6, Landroidx/core/util/Pair;

    sget-object v0, Lcom/pspdfkit/annotations/LineEndType;->BUTT:Lcom/pspdfkit/annotations/LineEndType;

    sget-object v8, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {v6, v0, v8}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v7

    .line 8
    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/sn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/ri;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->PERIMETER:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ri;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;)V

    invoke-virtual {v7, v0}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    .line 18
    new-instance v0, Lcom/pspdfkit/internal/rn;

    invoke-direct {v0, v7}, Lcom/pspdfkit/internal/rn;-><init>(Lcom/pspdfkit/internal/sn;)V

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/k6$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;Lcom/pspdfkit/utils/Size;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/k6;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/k6;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/k6$c;->a:Lcom/pspdfkit/internal/k6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Ljava/util/HashMap;

    const-string v0, "blocks"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$c;->a:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->d(Lcom/pspdfkit/internal/k6;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/util/Map;)V

    return-void
.end method

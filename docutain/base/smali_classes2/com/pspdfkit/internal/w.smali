.class public final Lcom/pspdfkit/internal/w;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;)I
    .locals 0

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method

.method public static a(Lcom/pspdfkit/internal/mb;I)V
    .locals 1

    const/4 v0, 0x1

    .line 5
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/mb;->b(II)V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;S)V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/mb;->a(IS)V

    return-void
.end method

.method public static b(Lcom/pspdfkit/internal/mb;)V
    .locals 1

    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/mb;->e(I)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/b;
    .locals 3

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/b;-><init>()V

    const/4 v1, 0x6

    .line 3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/b;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/w;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ot;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method public final b()S
    .locals 3

    const/4 v0, 0x4

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

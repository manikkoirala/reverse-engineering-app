.class public final Lcom/pspdfkit/internal/hf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/listeners/InstantDocumentListener;


# instance fields
.field private final b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

.field private final c:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$-KfWvR8Am7bWMp4bVjjd3X7veKY(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hf;->b(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    return-void
.end method

.method public static synthetic $r8$lambda$34NYh9XtMCMNrbnaQOXVl_TsgFI(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hf;->a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V

    return-void
.end method

.method public static synthetic $r8$lambda$6BDm92zJQAw-YJ7NTUIkWphNWgI(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hf;->a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VBhaqypgL-Q3GGnAUA13UShah_4(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hf;->d(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method public static synthetic $r8$lambda$WggXo3TQn5HQx2CLQ6D7XUHT1tI(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hf;->c(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_qUeykKDY5AP97z2VSxADaeyz88(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hf;->b(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oE77Pn7gWPnabArPSB4QxpXKzUY(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hf;->a(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qmEFGxx35kPNmaKuqhbZ96Sl5UI(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hf;->a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    .line 3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    return-void
.end method

.method private synthetic c(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method

.method private synthetic d(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-interface {v0, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 1
    :cond_1
    instance-of v1, p1, Lcom/pspdfkit/internal/hf;

    if-eqz v1, :cond_2

    .line 2
    check-cast p1, Lcom/pspdfkit/internal/hf;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    iget-object p1, p1, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 4
    :cond_2
    instance-of v1, p1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    if-eqz v1, :cond_3

    .line 5
    check-cast p1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->b:Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hf;->c:Landroid/os/Handler;

    new-instance v1, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/hf$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/hf;Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.class final Lcom/pspdfkit/internal/xa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ya;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ya;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/xa;->a:Lcom/pspdfkit/internal/ya;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Ljava/util/List;

    const-string v0, "annotations"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/pspdfkit/internal/xa;->a:Lcom/pspdfkit/internal/ya;

    invoke-static {v0}, Lcom/pspdfkit/internal/ya;->a(Lcom/pspdfkit/internal/ya;)Lcom/pspdfkit/internal/views/annotations/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/annotations/n;->setAnnotations(Ljava/util/List;)V

    .line 193
    iget-object v0, p0, Lcom/pspdfkit/internal/xa;->a:Lcom/pspdfkit/internal/ya;

    invoke-static {v0}, Lcom/pspdfkit/internal/ya;->a(Lcom/pspdfkit/internal/ya;)Lcom/pspdfkit/internal/views/annotations/n;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/pspdfkit/internal/xa;->a:Lcom/pspdfkit/internal/ya;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ya;->a(Lcom/pspdfkit/internal/ya;Ljava/util/List;)V

    return-void
.end method

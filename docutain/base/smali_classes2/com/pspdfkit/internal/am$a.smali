.class final Lcom/pspdfkit/internal/am$a;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private a:Z

.field private b:Lcom/pspdfkit/internal/o1;

.field private c:Ljava/lang/Runnable;

.field private final d:Landroid/os/Handler;

.field private final e:J

.field final synthetic f:Lcom/pspdfkit/internal/am;


# direct methods
.method public static synthetic $r8$lambda$bwaw8QjWVPP9eUHYRszNfXgsH3c(Lcom/pspdfkit/internal/uh;Lkotlin/Pair;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am$a;->a(Lcom/pspdfkit/internal/uh;Lkotlin/Pair;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    .line 8
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/am$a;->d:Landroid/os/Handler;

    const-wide/16 v0, 0x64

    .line 11
    iput-wide v0, p0, Lcom/pspdfkit/internal/am$a;->e:J

    return-void
.end method

.method private final a(FFLcom/pspdfkit/internal/uh;)Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Lcom/pspdfkit/internal/uh;",
            ")",
            "Lkotlin/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 26
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v2}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    new-array v0, v0, [I

    .line 28
    invoke-virtual {p3}, Lcom/pspdfkit/internal/uh;->e()Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 p3, 0x0

    aget v2, v1, p3

    int-to-float v2, v2

    add-float/2addr p1, v2

    aget p3, v0, p3

    int-to-float p3, p3

    sub-float/2addr p1, p3

    const/4 p3, 0x1

    aget v1, v1, p3

    int-to-float v1, v1

    add-float/2addr p2, v1

    aget p3, v0, p3

    int-to-float p3, p3

    sub-float/2addr p2, p3

    .line 32
    new-instance p3, Lkotlin/Pair;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p3
.end method

.method private final a()V
    .locals 5

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Z)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->d:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getMagnifierManager()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g()V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/internal/la;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v2, v4, v3, v1}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;II)V

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/internal/la;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/y1;->b(Z)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->b()Z

    move-result v0

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/z1;->e()Z

    move-result v2

    or-int/2addr v0, v2

    if-eqz v0, :cond_2

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    .line 24
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v2}, Lcom/pspdfkit/internal/am;->j(Lcom/pspdfkit/internal/am;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v4}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V

    :cond_2
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/uh;Lkotlin/Pair;)V
    .locals 1

    const-string v0, "$magnifierManager"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$showPos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/uh;->a(FF)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    const-string v0, "upEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/am$a;->a()V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/y1;->m()V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->b:Lcom/pspdfkit/internal/o1;

    if-eqz p1, :cond_0

    .line 8
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o1;->b()V

    const/4 p1, 0x0

    .line 9
    iput-object p1, p0, Lcom/pspdfkit/internal/am$a;->b:Lcom/pspdfkit/internal/o1;

    :cond_0
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    const-string v0, "cancelEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/am$a;->a()V

    return-void
.end method

.method public final c(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "downEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/y1;->a(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/ka;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 6

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/ka;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/y1;->q()Z

    move-result p1

    if-nez p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 6
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    :cond_0
    return v3

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 15
    iget-object v4, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    if-eqz v0, :cond_2

    .line 17
    invoke-static {v4, v0}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    .line 18
    :goto_0
    invoke-static {v4, v5}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Z)Z

    move-result v4

    if-eqz v0, :cond_7

    .line 28
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v4, v5, :cond_4

    .line 29
    iget-object v4, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v4, v0}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 31
    :cond_3
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {v2, v4, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/u0;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/views/document/a;

    invoke-virtual {p1, v0, v3}, Lcom/pspdfkit/internal/views/document/a;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    return v3

    .line 37
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    .line 38
    :cond_5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v1}, Lcom/pspdfkit/internal/am;->d(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 39
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    new-array v1, v3, [Lcom/pspdfkit/annotations/Annotation;

    aput-object v0, v1, v2

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    :cond_6
    const/4 v2, 0x1

    goto :goto_1

    :cond_7
    move v2, v4

    :goto_1
    return v2
.end method

.method public final e(Landroid/view/MotionEvent;)Z
    .locals 2

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/am$a;->a:Z

    return p1
.end method

.method public final g(Landroid/view/MotionEvent;)Z
    .locals 4

    const-string v0, "downEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/ka;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/y1;->a(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/ka;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v3, v0}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/ka;)V

    if-eqz v0, :cond_1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->b:Lcom/pspdfkit/internal/o1;

    if-nez v0, :cond_0

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v1}, Lcom/pspdfkit/internal/am;->g(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/fl;

    move-result-object v1

    .line 15
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/o1;->a(Ljava/util/List;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/am$a;->b:Lcom/pspdfkit/internal/o1;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->a()V

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v1}, Lcom/pspdfkit/internal/am;->k(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/ka;

    move-result-object v1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, p1, v1}, Lcom/pspdfkit/internal/y1;->a(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 25
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am$a;->a:Z

    if-eqz v0, :cond_2

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->c(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/na;

    move-result-object v0

    .line 28
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v3}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v3

    .line 29
    invoke-virtual {v0, p1, v3, v1}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_2

    :goto_0
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/pspdfkit/internal/am$a;->a:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->e(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/y1;->q()Z

    move-result p1

    if-nez p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    instance-of p1, p1, Lcom/pspdfkit/internal/views/annotations/h;

    if-eqz p1, :cond_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/u0;

    move-result-object p1

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 9
    check-cast p1, Lcom/pspdfkit/internal/views/document/a;

    invoke-virtual {p1, v2, v1}, Lcom/pspdfkit/internal/views/document/a;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    return v0

    :cond_0
    return v1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 6

    const-string v0, "downEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v2}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->c(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/na;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v2}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, p1, v2, v1}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 4
    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/am$a;->a:Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getMagnifierManager()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 10
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/y1;->a(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/ka;

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    .line 11
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v3

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/internal/la;)V

    .line 12
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ka;->i()Z

    move-result v2

    if-nez v2, :cond_3

    return-void

    .line 15
    :cond_3
    iget-object v2, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v2

    .line 16
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-eq v3, v1, :cond_4

    return-void

    .line 17
    :cond_4
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 18
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v3, v4, :cond_5

    goto :goto_1

    .line 20
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {p0, v3, p1, v0}, Lcom/pspdfkit/internal/am$a;->a(FFLcom/pspdfkit/internal/uh;)Lkotlin/Pair;

    move-result-object p1

    const/high16 v3, 0x40000000    # 2.0f

    .line 21
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/uh;->a(F)V

    .line 25
    new-instance v3, Lcom/pspdfkit/internal/am$a$$ExternalSyntheticLambda0;

    invoke-direct {v3, v0, p1}, Lcom/pspdfkit/internal/am$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/uh;Lkotlin/Pair;)V

    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->d:Landroid/os/Handler;

    iget-wide v4, p0, Lcom/pspdfkit/internal/am$a;->e:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 26
    iput-object v3, p0, Lcom/pspdfkit/internal/am$a;->c:Ljava/lang/Runnable;

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/y1;->b(Z)V

    .line 34
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getMeasurementInfo()Lcom/pspdfkit/annotations/measurements/MeasurementInfo;

    move-result-object p1

    if-eqz p1, :cond_6

    iget-object p1, p1, Lcom/pspdfkit/annotations/measurements/MeasurementInfo;->label:Ljava/lang/String;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    .line 35
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Ljava/lang/String;)Z

    :cond_6
    :goto_1
    return-void
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)Z
    .locals 6

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->c(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/na;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v1}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    const-string v0, "annotation"

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v4}, Lcom/pspdfkit/internal/am;->d(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v3, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/y1;->i()Z

    move-result v3

    if-nez v3, :cond_0

    .line 5
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lcom/pspdfkit/internal/am;->a()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v3, v1, v1}, Lcom/pspdfkit/internal/am;->a(ZZ)Z

    .line 160
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    new-array v4, v1, [Lcom/pspdfkit/annotations/Annotation;

    aput-object p1, v4, v2

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    .line 161
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {v3}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 162
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/y1;->isDraggingEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/y1;->n()Z

    move-result v3

    if-nez v3, :cond_0

    .line 163
    iget-object v3, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    .line 164
    new-instance v4, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/pspdfkit/internal/ka;-><init>(Ljava/lang/Object;)V

    .line 165
    invoke-static {v3, v4}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/ka;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 166
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/pspdfkit/internal/am;->a()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    const-string v0, "e1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "e2"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    invoke-static {p1}, Lcom/pspdfkit/internal/am;->k(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/ka;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/am$a;->f:Lcom/pspdfkit/internal/am;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result p3

    neg-float p3, p3

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {p4, v1}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result p4

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v1

    invoke-virtual {v1, p3, p4, p2, p1}, Lcom/pspdfkit/internal/y1;->a(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V

    .line 7
    iget-boolean p1, p0, Lcom/pspdfkit/internal/am$a;->a:Z

    const/4 p3, 0x1

    if-eqz p1, :cond_0

    .line 8
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getMagnifierManager()Lcom/pspdfkit/internal/uh;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/uh;->h()Z

    move-result p4

    if-eqz p4, :cond_0

    .line 10
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    invoke-direct {p0, p4, p2, p1}, Lcom/pspdfkit/internal/am$a;->a(FFLcom/pspdfkit/internal/uh;)Lkotlin/Pair;

    move-result-object p2

    .line 11
    invoke-virtual {p2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Number;

    invoke-virtual {p4}, Ljava/lang/Number;->floatValue()F

    move-result p4

    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result p2

    invoke-virtual {p1, p4, p2}, Lcom/pspdfkit/internal/uh;->a(FF)V

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object p1

    .line 14
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-ne p2, p3, :cond_0

    .line 15
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getMeasurementInfo()Lcom/pspdfkit/annotations/measurements/MeasurementInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/pspdfkit/annotations/measurements/MeasurementInfo;->label:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 16
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Ljava/lang/String;)Z

    :cond_0
    return p3

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

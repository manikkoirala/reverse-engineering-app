.class public final Lcom/pspdfkit/internal/nr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;
.implements Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;
.implements Lcom/pspdfkit/internal/ne;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/nr$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final c:Landroid/content/Context;

.field private d:Lcom/pspdfkit/internal/lq;

.field private final e:Lcom/pspdfkit/internal/wc;

.field private f:Landroid/graphics/PointF;

.field private g:Lcom/pspdfkit/internal/zf;

.field private h:Lcom/pspdfkit/internal/dm;

.field private i:I

.field private final j:Landroid/graphics/Matrix;

.field private final k:Lcom/pspdfkit/internal/na;

.field private l:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final m:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private n:Lcom/pspdfkit/listeners/DocumentListener;


# direct methods
.method public static synthetic $r8$lambda$-49rHH2dqbRq39rDH352j-DjLzU()V
    .locals 0

    invoke-static {}, Lcom/pspdfkit/internal/nr;->f()V

    return-void
.end method

.method public static synthetic $r8$lambda$STk3dvyuszymrrUMIodDAZ3ifiw(Lcom/pspdfkit/internal/nr;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/nr;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/specialMode/handler/a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/nr;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->c:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/lq;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->d:Lcom/pspdfkit/internal/lq;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/nr;)Landroid/graphics/PointF;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/dm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->h:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/nr;)Landroid/graphics/Matrix;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->j:Landroid/graphics/Matrix;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/internal/nr;)Lcom/pspdfkit/internal/na;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/nr;->k:Lcom/pspdfkit/internal/na;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/nr;Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/a1;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/nr;->j:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    .line 39
    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 40
    iput-object p2, p0, Lcom/pspdfkit/internal/nr;->m:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 41
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->c:Landroid/content/Context;

    .line 43
    new-instance p2, Lcom/pspdfkit/internal/wc;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/wc;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/nr;->e:Lcom/pspdfkit/internal/wc;

    .line 44
    sget-object p1, Lcom/pspdfkit/internal/vc;->a:Lcom/pspdfkit/internal/vc;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/pspdfkit/internal/xc;

    new-instance v2, Lcom/pspdfkit/internal/nr$a;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/nr$a;-><init>(Lcom/pspdfkit/internal/nr;Lcom/pspdfkit/internal/nr$a-IA;)V

    const/4 v0, 0x0

    aput-object v2, v1, v0

    invoke-virtual {p2, p1, v1}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    .line 46
    new-instance p1, Lcom/pspdfkit/internal/na;

    invoke-direct {p1, p3}, Lcom/pspdfkit/internal/na;-><init>(Lcom/pspdfkit/internal/a1;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->k:Lcom/pspdfkit/internal/na;

    .line 47
    sget-object p2, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/na;->a(Ljava/util/EnumSet;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string v0, "PSPDFKit.Forms"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "Exception while loading form elements on page: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 19
    iget-object v4, p0, Lcom/pspdfkit/internal/nr;->h:Lcom/pspdfkit/internal/dm;

    .line 23
    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    .line 24
    invoke-static {v0, p1, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Exception while loading form elements."

    .line 33
    invoke-static {v0, p1, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static synthetic f()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-void
.end method

.method private g()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/pspdfkit/internal/uf;->prepareFieldsCache()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/nr$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/internal/nr$$ExternalSyntheticLambda0;-><init>()V

    new-instance v2, Lcom/pspdfkit/internal/nr$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/nr$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/nr;)V

    .line 12
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/nr;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->h:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/nr;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/nr;->i:I

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/nr;->g()V

    .line 8
    new-instance p1, Lcom/pspdfkit/internal/lq;

    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.pspdfkit.internal.SignatureAnnotationCreationMode.SAVED_STATE_FRAGMENT_TAG"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/pspdfkit/internal/nr;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, p0}, Lcom/pspdfkit/internal/lq;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/internal/ne;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->d:Lcom/pspdfkit/internal/lq;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lq;->b()V

    .line 11
    new-instance p1, Lcom/pspdfkit/internal/mr;

    invoke-direct {p1}, Lcom/pspdfkit/internal/mr;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->e:Lcom/pspdfkit/internal/wc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wc;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final b()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 3
    iput-object v1, p0, Lcom/pspdfkit/internal/nr;->n:Lcom/pspdfkit/listeners/DocumentListener;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->dismiss(Landroidx/fragment/app/FragmentManager;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->dismiss(Landroidx/fragment/app/FragmentManager;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 10
    iput-object v1, p0, Lcom/pspdfkit/internal/nr;->l:Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->m:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/nr;->b()Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final onDismiss()V
    .locals 0

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)Z
    .locals 6

    const-string v0, "STATE_PAGE_INDEX"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/nr;->i:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    const-string v1, "pdfFragment"

    .line 4
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "onSignaturePickedListener"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    .line 38
    monitor-enter v1

    .line 39
    :try_start_0
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hb;->b()Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    monitor-exit v1

    if-eqz v2, :cond_1

    .line 40
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 42
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object v0

    .line 43
    invoke-static {v1, p0, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 52
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object v0

    .line 53
    invoke-static {v1, p0, v0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    :goto_0
    const-string v0, "STATE_TOUCH_POINT"

    .line 54
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/graphics/PointF;

    iput-object p1, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    return v5

    :catchall_0
    move-exception p1

    .line 55
    monitor-exit v1

    throw p1

    :cond_2
    return v2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/nr;->i:I

    const-string v1, "STATE_PAGE_INDEX"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    const-string v1, "STATE_TOUCH_POINT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public synthetic onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener$-CC;->$default$onSignatureCreated(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/Signature;Z)V

    return-void
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->d:Lcom/pspdfkit/internal/lq;

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/lq;->a()V

    .line 7
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_2

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    iget v1, p0, Lcom/pspdfkit/internal/nr;->i:I

    iget-object v2, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    invoke-virtual {p1, v0, v1, v2}, Lcom/pspdfkit/signatures/Signature;->toInkAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/InkAnnotation;

    move-result-object p1

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    iget v1, p0, Lcom/pspdfkit/internal/nr;->i:I

    iget-object v2, p0, Lcom/pspdfkit/internal/nr;->f:Landroid/graphics/PointF;

    invoke-virtual {p1, v0, v1, v2}, Lcom/pspdfkit/signatures/Signature;->toStampAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p1

    .line 13
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->exitCurrentlyActiveMode()V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->g:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/nr;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public synthetic onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener$-CC;->$default$onSignatureUiDataCollected(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    return-void
.end method

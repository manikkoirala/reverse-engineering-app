.class public final Lcom/pspdfkit/internal/tg;
.super Lcom/pspdfkit/internal/a6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/tg$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/a6<",
        "Lcom/pspdfkit/internal/tg$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final e:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/tg$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/tg$a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/pt;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V
    .locals 2

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/a6;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->LAYOUT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/tg;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 12
    sget-object v0, Lcom/pspdfkit/internal/tg$a;->Companion:Lcom/pspdfkit/internal/tg$a$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/tg$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/tg;->e:Lkotlinx/serialization/KSerializer;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/tg$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object p1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/pspdfkit/internal/tg$a;-><init>(Ljava/util/UUID;Lcom/pspdfkit/internal/cb;Ljava/lang/Integer;Lcom/pspdfkit/internal/l5;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/tg;->f:Lcom/pspdfkit/internal/tg$a;

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tg;->f:Lcom/pspdfkit/internal/tg$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tg;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tg;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

.class public abstract Lcom/pspdfkit/internal/ug;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ug$a;
    }
.end annotation


# static fields
.field static final synthetic t:Z = true


# instance fields
.field protected final a:Lcom/pspdfkit/internal/views/document/DocumentView;

.field protected final b:Lcom/pspdfkit/internal/zf;

.field protected final c:F

.field protected final d:F

.field protected final e:F

.field protected final f:I

.field private final g:Lio/reactivex/rxjava3/core/Completable;

.field private final h:Lio/reactivex/rxjava3/functions/Action;

.field protected i:I

.field protected j:I

.field protected k:I

.field protected l:I

.field protected m:I

.field protected n:I

.field protected o:Z

.field protected p:Lcom/pspdfkit/internal/ug$a;

.field private q:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final r:Ljava/util/ArrayList;

.field protected final s:Lcom/pspdfkit/internal/cm;


# direct methods
.method public static synthetic $r8$lambda$abrlkw-pDsaWYXLCIUba0ArKwDk(Lcom/pspdfkit/internal/ug;Lcom/pspdfkit/internal/ug$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ug;->a(Lcom/pspdfkit/internal/ug$a;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFILcom/pspdfkit/internal/cm;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-static {v2, v3, v0, v1}, Lio/reactivex/rxjava3/core/Completable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ug;->g:Lio/reactivex/rxjava3/core/Completable;

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/ug$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ug$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ug;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ug;->h:Lio/reactivex/rxjava3/functions/Action;

    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/pspdfkit/internal/ug;->k:I

    .line 14
    iput v0, p0, Lcom/pspdfkit/internal/ug;->l:I

    .line 17
    iput v0, p0, Lcom/pspdfkit/internal/ug;->m:I

    .line 19
    iput v0, p0, Lcom/pspdfkit/internal/ug;->n:I

    .line 21
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/pspdfkit/internal/ug;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ug;->r:Ljava/util/ArrayList;

    .line 57
    sget-boolean v0, Lcom/pspdfkit/internal/ug;->t:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 59
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 60
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    .line 61
    iput p2, p0, Lcom/pspdfkit/internal/ug;->i:I

    .line 62
    iput p3, p0, Lcom/pspdfkit/internal/ug;->j:I

    const/high16 p1, 0x3f800000    # 1.0f

    .line 63
    iput p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    .line 64
    iput p4, p0, Lcom/pspdfkit/internal/ug;->d:F

    .line 65
    iput p5, p0, Lcom/pspdfkit/internal/ug;->e:F

    .line 66
    iput p6, p0, Lcom/pspdfkit/internal/ug;->f:I

    .line 67
    iput-object p7, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    return-void
.end method

.method public static a(IFI)I
    .locals 4

    int-to-float v0, p0

    .line 36
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    int-to-float v1, p2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 37
    :goto_0
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    int-to-float p0, p0

    const/high16 v1, 0x42000000    # 32.0f

    mul-float p1, p1, v1

    cmpg-float p0, p0, p1

    if-gez p0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-nez v0, :cond_3

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    return p2

    :cond_3
    :goto_2
    return v3
.end method

.method private a(Lcom/pspdfkit/internal/ug$a;)V
    .locals 8

    .line 13
    iget-object v0, p1, Lcom/pspdfkit/internal/ug$a;->a:Landroid/graphics/RectF;

    .line 14
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 15
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    const/4 v2, 0x0

    .line 16
    invoke-virtual {p0, v0, v2}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 17
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 21
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v3, p1, Lcom/pspdfkit/internal/ug$a;->b:F

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 22
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v4, v4

    div-float/2addr v4, v3

    float-to-int v3, v4

    .line 23
    new-instance v4, Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    int-to-float v0, v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    sub-float v7, v5, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    int-to-float v3, v3

    div-float/2addr v3, v6

    sub-float v6, v1, v3

    add-float/2addr v5, v0

    add-float/2addr v1, v3

    invoke-direct {v4, v7, v6, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 30
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ug;->a(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    const-wide/16 v3, 0x0

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/pspdfkit/internal/ug;->b(Landroid/graphics/RectF;IJ)V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    if-ne v0, p1, :cond_0

    .line 33
    iput-object v2, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->s()V

    :cond_0
    return-void
.end method


# virtual methods
.method public abstract A()V
.end method

.method public abstract a(I)I
.end method

.method protected abstract a(II)I
.end method

.method public final a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 7
    :goto_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->a(I)I

    move-result v0

    int-to-float v0, v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    div-float p1, v0, p1

    neg-float v1, p1

    .line 11
    invoke-virtual {p2, p1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    const/4 p1, 0x0

    .line 12
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-object p2
.end method

.method public abstract a(Landroid/graphics/RectF;)Landroid/graphics/RectF;
.end method

.method public abstract a(F)V
.end method

.method public abstract a(III)V
.end method

.method public abstract a(IIIFJ)V
.end method

.method protected abstract a(IIIFJJ)V
.end method

.method public abstract a(IZ)V
.end method

.method public abstract a(Landroid/graphics/RectF;IJ)V
.end method

.method public abstract a(Landroid/graphics/RectF;IJZ)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/dm;)V
.end method

.method protected final a(Ljava/lang/Runnable;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract a(FFF)Z
.end method

.method public abstract b(I)I
.end method

.method public abstract b(II)I
.end method

.method public final b()V
    .locals 2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 12
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public b(IIIFJ)V
    .locals 9

    const-wide/16 v7, 0x1f4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    .line 1
    invoke-virtual/range {v0 .. v8}, Lcom/pspdfkit/internal/ug;->a(IIIFJJ)V

    return-void
.end method

.method protected abstract b(Landroid/graphics/RectF;)V
.end method

.method protected abstract b(Landroid/graphics/RectF;IJ)V
.end method

.method public abstract b(Lcom/pspdfkit/internal/dm;)V
.end method

.method public b(Lcom/pspdfkit/internal/ug$a;)V
    .locals 1

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    .line 5
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->k(I)V

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/ug$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/ug$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ug;Lcom/pspdfkit/internal/ug$a;)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public abstract b(FFF)Z
.end method

.method public abstract c()I
.end method

.method public abstract c(I)I
.end method

.method public abstract c(II)Z
.end method

.method public final d()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->c:F

    return v0
.end method

.method public abstract d(I)I
.end method

.method public abstract d(II)Z
.end method

.method public final e()Lcom/pspdfkit/internal/views/document/DocumentView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-object v0
.end method

.method public abstract e(I)Lcom/pspdfkit/utils/Size;
.end method

.method public abstract e(II)V
.end method

.method public abstract f()I
.end method

.method protected abstract f(I)I
.end method

.method public final f(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ug;->k:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/ug;->m:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ug;->l:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/ug;->n:I

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ug;->k:I

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ug;->l:I

    .line 7
    iput p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    .line 8
    iput p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->s()Landroid/graphics/RectF;

    move-result-object p1

    .line 12
    iget p2, p1, Landroid/graphics/RectF;->left:F

    iget v0, p0, Lcom/pspdfkit/internal/ug;->m:I

    int-to-float v0, v0

    add-float/2addr p2, v0

    iput p2, p1, Landroid/graphics/RectF;->left:F

    .line 13
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->n:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 14
    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v1, v1

    add-float/2addr p2, v1

    iput p2, p1, Landroid/graphics/RectF;->right:F

    .line 15
    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float p2, p2

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 18
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->b(Landroid/graphics/RectF;)V

    const/4 p1, 0x0

    .line 21
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    return-void
.end method

.method public abstract g()I
.end method

.method protected abstract g(I)I
.end method

.method public abstract h()I
.end method

.method protected final h(I)Landroid/graphics/RectF;
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result p1

    sub-int/2addr v1, p1

    int-to-float p1, v1

    iput p1, v0, Landroid/graphics/RectF;->top:F

    .line 4
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 5
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v1, v1

    add-float/2addr p1, v1

    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method public abstract i(I)F
.end method

.method public abstract i()I
.end method

.method public final j()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->e:F

    return v0
.end method

.method public abstract j(I)V
.end method

.method public final k()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->d:F

    return v0
.end method

.method protected k(I)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/ug;->a(IZ)V

    return-void
.end method

.method public final l()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    return v0
.end method

.method public final m()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    return v0
.end method

.method public abstract n()I
.end method

.method public abstract o()I
.end method

.method public final p()Lcom/pspdfkit/internal/ug$a;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ug$a;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->r()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ug;->i(I)F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ug$a;-><init>(Landroid/graphics/RectF;IF)V

    :goto_0
    return-object v0
.end method

.method public final q()Landroid/graphics/RectF;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->h(I)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final r()Landroid/graphics/RectF;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->h(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 3
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    return-object v0
.end method

.method public s()Landroid/graphics/RectF;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->h(I)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final t()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->w()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->r:Ljava/util/ArrayList;

    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final u()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final v()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    return v0
.end method

.method public abstract w()Z
.end method

.method public abstract x()V
.end method

.method protected final y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->g:Lio/reactivex/rxjava3/core/Completable;

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->h:Lio/reactivex/rxjava3/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ug;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public abstract z()V
.end method

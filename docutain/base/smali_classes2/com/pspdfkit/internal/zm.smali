.class public final Lcom/pspdfkit/internal/zm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/projection/PdfProjection;


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "document"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/zm;->a:Lcom/pspdfkit/internal/zf;

    return-void
.end method


# virtual methods
.method public final getNormalizedToRawTransformation(I)Landroid/graphics/Matrix;
    .locals 4

    if-ltz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zm;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/zm;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPage(I)Lcom/pspdfkit/internal/jni/NativePage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePage;->getPageInfo()Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getReversePageMatrix()Landroid/graphics/Matrix;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 8
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string p1, "Transformation failed because of invalid page: %d"

    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRawToNormalizedTransformation(I)Landroid/graphics/Matrix;
    .locals 4

    if-ltz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zm;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/zm;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPage(I)Lcom/pspdfkit/internal/jni/NativePage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePage;->getPageInfo()Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getPageMatrix()Landroid/graphics/Matrix;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 8
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string p1, "Transformation failed because of invalid page: %d"

    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toNormalizedPoint(Landroid/graphics/PointF;I)Landroid/graphics/PointF;
    .locals 2

    const-string v0, "point"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zm;->getRawToNormalizedTransformation(I)Landroid/graphics/Matrix;

    move-result-object p2

    .line 55
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(Landroid/graphics/PointF;Landroid/graphics/Matrix;)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1
.end method

.method public final toPdfRect(Landroid/graphics/RectF;I)Landroid/graphics/RectF;
    .locals 3

    const-string v0, "rect"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zm;->getRawToNormalizedTransformation(I)Landroid/graphics/Matrix;

    move-result-object p2

    .line 54
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 61
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v2, p1, v1

    if-gez v2, :cond_0

    .line 63
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 64
    iput p1, v0, Landroid/graphics/RectF;->top:F

    .line 66
    :cond_0
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 67
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 68
    iget p2, v0, Landroid/graphics/RectF;->top:F

    iput p2, v0, Landroid/graphics/RectF;->bottom:F

    .line 69
    iput p1, v0, Landroid/graphics/RectF;->top:F

    return-object v0
.end method

.method public final toRawPoint(Landroid/graphics/PointF;I)Landroid/graphics/PointF;
    .locals 2

    const-string v0, "point"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zm;->getNormalizedToRawTransformation(I)Landroid/graphics/Matrix;

    move-result-object p2

    .line 55
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(Landroid/graphics/PointF;Landroid/graphics/Matrix;)Landroid/graphics/PointF;

    move-result-object p1

    return-object p1
.end method

.method public final toRawRect(Landroid/graphics/RectF;I)Landroid/graphics/RectF;
    .locals 3

    const-string v0, "rect"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zm;->getNormalizedToRawTransformation(I)Landroid/graphics/Matrix;

    move-result-object p2

    .line 54
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 61
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v2, p1, v1

    if-gez v2, :cond_0

    .line 63
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 64
    iput p1, v0, Landroid/graphics/RectF;->top:F

    .line 66
    :cond_0
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 67
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 68
    iget p2, v0, Landroid/graphics/RectF;->top:F

    iput p2, v0, Landroid/graphics/RectF;->bottom:F

    .line 69
    iput p1, v0, Landroid/graphics/RectF;->top:F

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/pd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/HideAction;)I
    .locals 13

    const-string v0, "builder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->getAnnotationReferences(Lcom/pspdfkit/annotations/actions/HideAction;)Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [I

    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x4

    const/4 v8, 0x1

    if-eqz v5, :cond_1

    add-int/lit8 v5, v4, 0x1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/pspdfkit/internal/u1;

    .line 4
    invoke-virtual {v9}, Lcom/pspdfkit/internal/u1;->a()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 7
    invoke-virtual {p0, v10}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    :cond_0
    const/4 v10, 0x0

    .line 8
    :goto_1
    invoke-virtual {v9}, Lcom/pspdfkit/internal/u1;->c()I

    move-result v11

    .line 9
    invoke-virtual {v9}, Lcom/pspdfkit/internal/u1;->b()I

    move-result v9

    .line 10
    sget v12, Lcom/pspdfkit/internal/t1;->e:I

    .line 11
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/mb;->e(I)V

    const/4 v7, 0x3

    .line 12
    invoke-virtual {p0, v7, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 13
    invoke-virtual {p0, v6, v9}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 14
    invoke-virtual {p0, v8, v11}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 15
    invoke-virtual {p0, v3, v10}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v6

    .line 17
    aput v6, v2, v4

    move v4, v5

    goto :goto_0

    .line 28
    :cond_1
    sget v0, Lcom/pspdfkit/internal/ld;->e:I

    .line 29
    invoke-virtual {p0, v7, v1, v7}, Lcom/pspdfkit/internal/mb;->a(III)V

    sub-int/2addr v1, v8

    :goto_2
    if-ltz v1, :cond_2

    aget v0, v2, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/HideAction;->shouldHide()Z

    move-result p1

    .line 31
    invoke-virtual {p0, v6}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 32
    invoke-virtual {p0, v3, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 33
    invoke-virtual {p0, v8, p1, v8}, Lcom/pspdfkit/internal/mb;->a(IZZ)V

    .line 34
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method

.method public static final a(Lcom/pspdfkit/internal/ld;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/HideAction;
    .locals 7

    const-string v0, "action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ld;->a()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ld;->a()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 37
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ld;->f(I)Lcom/pspdfkit/internal/t1;

    move-result-object v3

    const-string v4, "action.annotationReference(i)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v4, Lcom/pspdfkit/internal/u1;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->c()I

    move-result v6

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->b()I

    move-result v3

    invoke-direct {v4, v5, v6, v3}, Lcom/pspdfkit/internal/u1;-><init>(Ljava/lang/String;II)V

    .line 39
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :cond_0
    sget-object v1, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ld;->b()Z

    move-result p0

    invoke-virtual {v1, v0, p0, p1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->createHideAction(Ljava/util/List;ZLjava/util/List;)Lcom/pspdfkit/annotations/actions/HideAction;

    move-result-object p0

    return-object p0
.end method

.class public final Lcom/pspdfkit/internal/kv;
.super Lcom/pspdfkit/internal/g7;
.source "SourceFile"


# instance fields
.field private final L:Landroid/graphics/PointF;

.field private final M:Landroid/graphics/PointF;

.field private final N:Landroid/graphics/Matrix;

.field private final O:[I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 1

    .line 1
    invoke-direct/range {p0 .. p8}, Lcom/pspdfkit/internal/g7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    .line 2
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/kv;->L:Landroid/graphics/PointF;

    .line 6
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/kv;->M:Landroid/graphics/PointF;

    .line 8
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/utils/Size;

    .line 35
    iget p3, p3, Lcom/pspdfkit/utils/Size;->width:F

    iget p4, p0, Lcom/pspdfkit/internal/g7;->z:F

    cmpl-float p4, p3, p4

    if-lez p4, :cond_0

    .line 36
    iput p3, p0, Lcom/pspdfkit/internal/g7;->z:F

    goto :goto_0

    .line 39
    :cond_1
    iget p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    const/high16 p3, 0x3f800000    # 1.0f

    mul-float p1, p1, p3

    iput p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    float-to-int p1, p1

    sub-int/2addr p2, p1

    .line 41
    div-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p1

    .line 46
    new-array p2, p1, [I

    iput-object p2, p0, Lcom/pspdfkit/internal/kv;->O:[I

    const/4 p4, 0x0

    .line 47
    aput p4, p2, p4

    const/4 p2, 0x1

    const/4 p4, 0x1

    :goto_1
    if-ge p4, p1, :cond_2

    .line 49
    iget-object p5, p0, Lcom/pspdfkit/internal/kv;->O:[I

    add-int/lit8 p7, p4, -0x1

    aget p8, p5, p7

    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Lcom/pspdfkit/utils/Size;

    iget p7, p7, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int p7, p7

    add-int/2addr p8, p7

    add-int/2addr p8, p6

    aput p8, p5, p4

    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    :cond_2
    sub-int/2addr p1, p2

    .line 54
    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->O:[I

    aget p2, p2, p1

    int-to-float p2, p2

    iget-object p4, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {p4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    add-float/2addr p2, p1

    mul-float p2, p2, p3

    iput p2, p0, Lcom/pspdfkit/internal/g7;->A:F

    iput p2, p0, Lcom/pspdfkit/internal/g7;->C:F

    .line 55
    iget p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    iput p1, p0, Lcom/pspdfkit/internal/g7;->B:F

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 9

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v2, v1

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_0

    float-to-int v0, v0

    sub-int/2addr v1, v0

    .line 2
    div-int/lit8 v1, v1, 0x2

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    if-eq v1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v4, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v5, p0, Lcom/pspdfkit/internal/g7;->G:I

    sub-int v6, v1, v4

    const/4 v7, 0x0

    const/16 v8, 0x190

    invoke-virtual/range {v3 .. v8}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final C()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    iget v1, p0, Lcom/pspdfkit/internal/g7;->z:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final D()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->A:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v1, v1

    cmpl-float v2, v0, v1

    sub-float/2addr v1, v0

    if-lez v2, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v1, v0

    :goto_0
    float-to-int v0, v1

    return v0
.end method

.method public final a(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5

    .line 41
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 42
    iget p1, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 43
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result p1

    .line 46
    iget v1, p0, Lcom/pspdfkit/internal/g7;->A:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    const/4 v3, 0x0

    cmpg-float v4, v1, v2

    if-gez v4, :cond_0

    sub-float/2addr v1, v2

    goto :goto_0

    .line 49
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    neg-float v1, v1

    :goto_0
    int-to-float p1, p1

    .line 53
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_1

    .line 54
    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v3, p1

    float-to-int p1, v3

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    add-float/2addr v2, p1

    neg-float p1, v2

    goto :goto_1

    .line 56
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, p1

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    neg-float p1, p1

    .line 59
    :goto_1
    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 60
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 61
    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 62
    iget v1, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    return-object v0
.end method

.method public final a(IZ)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v4, p0, Lcom/pspdfkit/internal/g7;->G:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result p1

    neg-int v6, p1

    if-eqz p2, :cond_0

    const/16 p1, 0x190

    const/16 v7, 0x190

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v7, 0x0

    :goto_0
    const/4 v5, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final a(FFF)Z
    .locals 7

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 6
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v1

    iget v1, p0, Lcom/pspdfkit/internal/ug;->d:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->e:F

    .line 7
    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 8
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    cmpl-float v2, p1, v1

    if-nez v2, :cond_0

    return v0

    :cond_0
    div-float v1, p1, v1

    .line 11
    iput p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    .line 14
    iget p1, p0, Lcom/pspdfkit/internal/g7;->A:F

    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v2

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int v2, v2, v3

    int-to-float v2, v2

    sub-float/2addr p1, v2

    .line 15
    iget v2, p0, Lcom/pspdfkit/internal/g7;->z:F

    mul-float v2, v2, v1

    iput v2, p0, Lcom/pspdfkit/internal/g7;->z:F

    mul-float p1, p1, v1

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v1

    sub-int/2addr v1, v0

    iget v2, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int v1, v1, v2

    int-to-float v1, v1

    add-float/2addr p1, v1

    iput p1, p0, Lcom/pspdfkit/internal/g7;->A:F

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/kv;->M:Landroid/graphics/PointF;

    .line 21
    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 22
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/g7;->E:I

    iget-object v1, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 23
    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 26
    iget p2, p1, Landroid/graphics/PointF;->x:F

    iget-object p3, p0, Lcom/pspdfkit/internal/kv;->L:Landroid/graphics/PointF;

    iget p3, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p3

    iget-object p3, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    .line 27
    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p2

    float-to-int v4, p2

    .line 28
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->L:Landroid/graphics/PointF;

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    .line 29
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p1

    neg-float p1, p1

    float-to-int v5, p1

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {p1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v2, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    const/4 p1, 0x0

    .line 36
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    :goto_0
    if-ge p1, p2, :cond_1

    .line 37
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(I)Lcom/pspdfkit/internal/dm;

    move-result-object p3

    .line 38
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/g7;->b(Lcom/pspdfkit/internal/dm;)V

    .line 39
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/g7;->a(Lcom/pspdfkit/internal/dm;)V

    .line 40
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p3}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final b(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/g7;->z:F

    sub-float/2addr v0, p1

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr v0, p1

    const/4 p1, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iget v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    add-int/2addr p1, v0

    return p1
.end method

.method public final b(II)I
    .locals 2

    const/4 p1, 0x0

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result v0

    if-ltz v0, :cond_0

    return p1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kv;->O:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/kv;->O:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/g7;->a(I)I

    move-result v1

    add-int/2addr v1, v0

    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-gt v1, v0, :cond_1

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/kv;->O:[I

    array-length p1, p1

    :goto_0
    add-int/lit8 p1, p1, -0x1

    return p1

    .line 13
    :cond_1
    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    .line 14
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result p2

    if-ge v0, p2, :cond_2

    return p1

    .line 18
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->O:[I

    array-length p2, p2

    :goto_1
    add-int/lit8 v1, p2, -0x1

    if-ge p1, v1, :cond_4

    .line 19
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result v1

    if-gt v1, v0, :cond_3

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    return p1

    :cond_3
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 25
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/kv;->O:[I

    array-length p1, p1

    goto :goto_0
.end method

.method public final b(FFF)Z
    .locals 1

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->K:Z

    .line 27
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 29
    iput p2, p0, Lcom/pspdfkit/internal/g7;->H:F

    .line 30
    iput p3, p0, Lcom/pspdfkit/internal/g7;->I:F

    .line 31
    iget v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 32
    iput v0, p0, Lcom/pspdfkit/internal/g7;->E:I

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/kv;->L:Landroid/graphics/PointF;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 39
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/g7;->E:I

    iget-object v0, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 40
    iget-object p2, p0, Lcom/pspdfkit/internal/kv;->L:Landroid/graphics/PointF;

    iget-object p3, p0, Lcom/pspdfkit/internal/kv;->N:Landroid/graphics/Matrix;

    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    return p1
.end method

.method public final c(I)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kv;->O:[I

    aget v0, v0, p1

    iget v1, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int p1, p1, v1

    sub-int/2addr v0, p1

    int-to-float v0, v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float v0, v0, v1

    int-to-float p1, p1

    add-float/2addr v0, p1

    float-to-int p1, v0

    iget v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    add-int/2addr p1, v0

    return p1
.end method

.method public final c(II)Z
    .locals 5

    .line 3
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/g7;->c(II)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 5
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 6
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    iget v2, p0, Lcom/pspdfkit/internal/g7;->z:F

    div-float v0, v2, v0

    float-to-int v0, v0

    sub-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    add-int/2addr v0, p1

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    float-to-int v2, v2

    add-int/2addr v2, v3

    invoke-static {p1, v0, v3, v2}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    int-to-float p2, p2

    iget v2, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    goto :goto_1

    :cond_1
    const/high16 v2, 0x40200000    # 2.5f

    mul-float v0, v0, v2

    .line 16
    iget v2, p0, Lcom/pspdfkit/internal/g7;->F:I

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v0, v3

    div-float v3, v0, v3

    mul-float v3, v3, v2

    float-to-int v2, v3

    .line 17
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int v4, v3, v2

    if-lt v2, v4, :cond_2

    .line 19
    div-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 20
    :cond_2
    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 21
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    int-to-float v2, v3

    int-to-float p2, p2

    iget v3, p0, Lcom/pspdfkit/internal/g7;->y:F

    invoke-virtual {p1, v2, p2, v3, v0}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    :goto_1
    return v1
.end method

.method public final d(II)Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/g7;->K:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v3, p0, Lcom/pspdfkit/internal/g7;->z:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 p1, 0x0

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    .line 13
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v2
.end method

.method public final e(II)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/g7;->z:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 p1, 0x0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int v3, p1

    neg-int v4, p2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final h()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    iget v1, p0, Lcom/pspdfkit/internal/g7;->z:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->A:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v1, v1

    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sub-float/2addr v1, v0

    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v1, v0

    :goto_0
    float-to-int v0, v0

    return v0
.end method

.method protected final k(I)V
    .locals 8

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/kv;->c(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g7;->a(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr p1, v1

    div-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v4, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int v6, p1

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

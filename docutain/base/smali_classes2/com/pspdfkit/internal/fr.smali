.class public final Lcom/pspdfkit/internal/fr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static b:Ljava/lang/Boolean;


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/fr;->a(Landroid/content/Context;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/fr;-><init>([B)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;->createFromTemplatesData([B)Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/fr;->a:Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;

    return-void

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Could not parse magic ink shape templates data"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static declared-synchronized a()Z
    .locals 7

    const-class v0, Lcom/pspdfkit/internal/fr;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v2, 0x0

    if-nez v1, :cond_0

    monitor-exit v0

    return v2

    :cond_0
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 6
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v5, "pspdfkit"

    invoke-virtual {v1, v5}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 8
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v5, "PSPDFShapeTemplates.data"

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 9
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    .line 18
    :try_start_2
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    .line 20
    :cond_2
    sget-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_6

    new-array v1, v3, [Ljava/lang/Object;

    const-string v3, "pspdfkit"

    aput-object v3, v1, v2

    const-string v2, "PSPDFShapeTemplates.data"

    aput-object v2, v1, v4

    const-string v2, "PSPDFKit.Internal.ShapeDetector"

    const-string v3, "The shape templates data (%s/%s) could not be found in assets. Magic ink will be disabled."

    .line 21
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catchall_0
    :try_start_3
    const-string v1, "PSPDFKit.Internal.ShapeDetector"

    const-string v5, "Failed to check whether or not SHAPE_TEMPLATES_DATA_ASSET_NAME is in the assets list and the exception was ignored."

    new-array v6, v2, [Ljava/lang/Object;

    .line 22
    invoke-static {v1, v5, v6}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 27
    :try_start_4
    sget-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 28
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    .line 30
    :cond_3
    sget-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_6

    new-array v1, v3, [Ljava/lang/Object;

    const-string v3, "pspdfkit"

    aput-object v3, v1, v2

    const-string v2, "PSPDFShapeTemplates.data"

    aput-object v2, v1, v4

    const-string v2, "PSPDFKit.Internal.ShapeDetector"

    const-string v3, "The shape templates data (%s/%s) could not be found in assets. Magic ink will be disabled."

    .line 31
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catchall_1
    move-exception v1

    .line 32
    sget-object v5, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    if-nez v5, :cond_4

    .line 33
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v5, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    .line 35
    :cond_4
    sget-object v5, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_5

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "pspdfkit"

    aput-object v5, v3, v2

    const-string v2, "PSPDFShapeTemplates.data"

    aput-object v2, v3, v4

    const-string v2, "PSPDFKit.Internal.ShapeDetector"

    const-string v4, "The shape templates data (%s/%s) could not be found in assets. Magic ink will be disabled."

    .line 36
    invoke-static {v2, v4, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    :cond_5
    throw v1

    .line 44
    :cond_6
    :goto_1
    sget-object v1, Lcom/pspdfkit/internal/fr;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    monitor-exit v0

    return v1

    :catchall_2
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Landroid/content/Context;)[B
    .locals 4

    .line 45
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const-string v0, "PSPDFShapeTemplates.data"

    .line 46
    invoke-static {v0}, Lcom/pspdfkit/internal/kb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object p0

    .line 47
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const v1, 0x19000

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const v1, 0xffff

    new-array v1, v1, [B

    .line 51
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v3, 0x0

    .line 52
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 56
    :catch_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Could not read shape templates data (PSPDFShapeTemplates.data) from assets."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/gr;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/fr;->a:Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;

    new-instance v1, Lcom/pspdfkit/internal/mn;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/mn;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeNativeShapeDetector;->detectShape(Lcom/pspdfkit/internal/jni/NativePointsPager;)Lcom/pspdfkit/internal/jni/NativeShapeDetectorResult;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 59
    new-instance v0, Lcom/pspdfkit/internal/gr;

    .line 60
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeShapeDetectorResult;->getMatchingTemplateIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeShapeDetectorResult;->getMatchConfidence()I

    move-result p1

    .line 61
    invoke-static {v1}, Lcom/pspdfkit/internal/ir;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/ir;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/gr;-><init>(Lcom/pspdfkit/internal/ir;I)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

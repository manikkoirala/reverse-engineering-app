.class public abstract Lcom/pspdfkit/internal/y6;
.super Lcom/pspdfkit/internal/k4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/internal/e6;",
        ">",
        "Lcom/pspdfkit/internal/k4<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/k4$a;)V
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/l6;

    const-string v1, "editClass"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    const-class v0, Lcom/pspdfkit/internal/l6;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/k4;-><init>(Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    return-void
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/e6;

    const-string v0, "edit"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/e6;

    const-string v0, "edit"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

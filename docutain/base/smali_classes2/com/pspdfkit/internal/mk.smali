.class public interface abstract Lcom/pspdfkit/internal/mk;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/hk;I)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/hk;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/hk;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/uk;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/pspdfkit/internal/ik;)Z
.end method

.method public abstract b()Z
.end method

.method public abstract b(Lcom/pspdfkit/internal/ik;)Z
.end method

.method public abstract c(Lcom/pspdfkit/internal/ik;)V
.end method

.method public abstract c()Z
.end method

.method public abstract d(Lcom/pspdfkit/internal/ik;)V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()Lcom/pspdfkit/internal/ik;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract h()Z
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Z
.end method

.method public abstract l()Z
.end method

.method public abstract m()V
.end method

.method public abstract n()Lcom/pspdfkit/internal/ik;
.end method

.method public abstract o()Z
.end method

.method public abstract p()Z
.end method

.method public abstract q()I
.end method

.method public abstract r()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract s()V
.end method

.method public abstract t()Z
.end method

.method public abstract u()Lio/reactivex/rxjava3/core/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;>;"
        }
    .end annotation
.end method

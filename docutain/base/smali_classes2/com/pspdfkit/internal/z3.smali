.class Lcom/pspdfkit/internal/z3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/b2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DrawingShape:",
        "Lcom/pspdfkit/internal/h4;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/b2;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/pspdfkit/internal/h4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDrawingShape;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/h4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDrawingShape;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    return-void
.end method

.method protected a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->g()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v1

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 5
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/h4;->j()I

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v3

    if-eq v1, v3, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->j()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    const/4 v0, 0x1

    .line 9
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/h4;->f()F

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    const/4 v0, 0x1

    .line 13
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_3

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    goto :goto_1

    :cond_3
    move v2, v0

    :goto_1
    return v2
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 1

    const/4 v0, 0x1

    .line 38
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/pspdfkit/internal/z3;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p1

    return p1
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 2

    .line 15
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/h4;->g()I

    move-result p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result p3

    const/4 p4, 0x1

    if-eq p2, p3, :cond_0

    .line 16
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/h4;->a(I)V

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 19
    :goto_0
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/h4;->j()I

    move-result p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v0

    if-eq p3, v0, :cond_1

    .line 20
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/h4;->b(I)V

    const/4 p2, 0x1

    .line 23
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v0

    cmpl-float p3, p3, v0

    if-eqz p3, :cond_2

    .line 24
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/h4;->a(F)V

    const/4 p2, 0x1

    .line 27
    :cond_2
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/h4;->m()F

    move-result p3

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_3

    .line 29
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/InkAnnotation;->getLineWidth()F

    move-result v0

    goto :goto_1

    .line 31
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v0

    :goto_1
    cmpl-float p3, p3, v0

    if-eqz p3, :cond_5

    .line 32
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p3

    if-ne p3, v1, :cond_4

    .line 34
    check-cast p1, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLineWidth()F

    move-result p1

    goto :goto_2

    .line 36
    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result p1

    .line 37
    :goto_2
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/h4;->b(F)V

    goto :goto_3

    :cond_5
    move p4, p2

    :goto_3
    return p4
.end method

.method public synthetic a(Z)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/b2$-CC;->$default$a(Lcom/pspdfkit/internal/b2;Z)Z

    move-result p1

    return p1
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/h4;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    return-void
.end method

.method public synthetic b()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/b2$-CC;->$default$b(Lcom/pspdfkit/internal/b2;)Z

    move-result v0

    return v0
.end method

.method public final c()Lcom/pspdfkit/internal/br$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/internal/b2$-CC;->$default$d(Lcom/pspdfkit/internal/b2;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/ig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/xf;


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:Lcom/pspdfkit/internal/vj;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/n9;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public static synthetic $r8$lambda$YReEV4UhIJS3xQELEfWKrxCwHWA(Lcom/pspdfkit/internal/ig;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/internal/ig;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/vj;

    invoke-direct {p1}, Lcom/pspdfkit/internal/vj;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ig;->b:Lcom/pspdfkit/internal/vj;

    .line 4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ig;->d:Z

    return-void
.end method

.method private final a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/n9;
    .locals 3

    .line 282
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    .line 283
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    if-gez v0, :cond_0

    goto :goto_0

    .line 287
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result p1

    if-ltz p1, :cond_3

    .line 288
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    goto :goto_1

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/n9;

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.JavaScript"

    const-string v1, "Error executing javascript action for annotation %s. Annotation was not attached to document."

    .line 292
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-object v2
.end method

.method private static final a(Lcom/pspdfkit/internal/ig;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 29
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n9;

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/internal/n9;->a()V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/lg;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->b:Lcom/pspdfkit/internal/vj;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vj;->a(Lcom/pspdfkit/internal/lg;)V

    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 8

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 2
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ig;->d:Z

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ig;->d:Z

    if-eqz v1, :cond_0

    .line 7
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;->ENABLED:Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;

    goto :goto_0

    .line 10
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;->DISABLED:Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;

    .line 11
    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->setJavascriptStatus(Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    .line 12
    :try_start_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "dist"

    .line 16
    invoke-static {v2}, Lcom/pspdfkit/internal/kb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "dist"

    .line 18
    invoke-static {v1, v2, v0, v3}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashSet;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    nop

    :cond_1
    move-object v1, v0

    :goto_1
    if-nez v1, :cond_2

    const/4 v2, 0x0

    :try_start_2
    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.JavaScript"

    const-string v4, "The JavaScript API minified bundle is not available on this platform. PDFs containing JavaScript may not work correctly."

    .line 19
    invoke-static {v3, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getDocumentProviders()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    .line 23
    iget-object v4, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    new-instance v5, Lcom/pspdfkit/internal/n9;

    const-string v6, "documentProvider"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    :cond_3
    move-object v6, v0

    :goto_3
    iget-object v7, p0, Lcom/pspdfkit/internal/ig;->b:Lcom/pspdfkit/internal/vj;

    invoke-direct {v5, v3, v6, v7}, Lcom/pspdfkit/internal/n9;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentProvider;Ljava/lang/String;Lcom/pspdfkit/internal/vj;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 26
    :cond_4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ig;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/pspdfkit/annotations/LinkAnnotation;)Z
    .locals 3

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 173
    :cond_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/n9;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 174
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    new-instance v0, Lcom/pspdfkit/internal/l9;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/l9;-><init>(Lcom/pspdfkit/internal/n9;Lcom/pspdfkit/annotations/LinkAnnotation;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/o9;->a(Lkotlin/jvm/functions/Function0;)Z

    move-result v2

    :cond_1
    return v2
.end method

.method public final a(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Z
    .locals 5

    const-string v0, "formElement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotationTriggerEvent"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    return v3

    .line 224
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    const-string v4, "formElement.annotation"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/n9;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 225
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    new-instance v0, Lcom/pspdfkit/internal/k9;

    invoke-direct {v0, v2, p1, p2}, Lcom/pspdfkit/internal/k9;-><init>(Lcom/pspdfkit/internal/n9;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/o9;->a(Lkotlin/jvm/functions/Function0;)Z

    move-result v3

    :cond_1
    return v3
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    const-string v0, "script"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/n9;

    :goto_0
    if-eqz v1, :cond_2

    .line 129
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    new-instance v0, Lcom/pspdfkit/internal/m9;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/m9;-><init>(Lcom/pspdfkit/internal/n9;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/o9;->a(Lkotlin/jvm/functions/Function0;)Z

    move-result v2

    :cond_2
    return v2
.end method

.method public final a(Ljava/lang/String;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 3

    const-string v0, "script"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 33
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/ActionSender;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    .line 34
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/ActionSender;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object p2

    if-eqz v1, :cond_1

    .line 37
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/n9;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 38
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v0, Lcom/pspdfkit/internal/m9;

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/internal/m9;-><init>(Lcom/pspdfkit/internal/n9;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/o9;->a(Lkotlin/jvm/functions/Function0;)Z

    move-result v2

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 81
    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p2

    const-string v1, "formElement.annotation"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/n9;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 82
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/pspdfkit/internal/m9;

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/internal/m9;-><init>(Lcom/pspdfkit/internal/n9;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/o9;->a(Lkotlin/jvm/functions/Function0;)Z

    move-result v2

    goto :goto_0

    :cond_2
    new-array p1, v2, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.JavaScript"

    const-string v0, "Trying to execute a JavaScript action on something that is not a form element is not supported yet."

    .line 125
    invoke-static {p2, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    :goto_0
    return v2
.end method

.method public final b()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->b:Lcom/pspdfkit/internal/vj;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vj;->a()V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/lg;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->b:Lcom/pspdfkit/internal/vj;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vj;->b(Lcom/pspdfkit/internal/lg;)V

    return-void
.end method

.method public final executeDocumentLevelScripts()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ig;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/n9;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/n9;->a()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final executeDocumentLevelScriptsAsync()Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ig$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ig$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ig;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    const-string v1, "fromAction {\n           \u2026heduler.PRIORITY_NORMAL))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final isJavaScriptEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ig;->d:Z

    return v0
.end method

.method public final declared-synchronized setJavaScriptEnabled(Z)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ig;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    monitor-exit p0

    return-void

    .line 2
    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ig;->d:Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ig;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ig;->d:Z

    if-eqz v0, :cond_1

    .line 7
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;->ENABLED:Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;

    goto :goto_0

    .line 10
    :cond_1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;->DISABLED:Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;

    .line 11
    :goto_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->setJavascriptStatus(Lcom/pspdfkit/internal/jni/NativeDocumentJavaScriptStatus;)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ig;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

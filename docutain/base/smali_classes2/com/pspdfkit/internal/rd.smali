.class public final Lcom/pspdfkit/internal/rd;
.super Lcom/pspdfkit/internal/g7;
.source "SourceFile"


# instance fields
.field private final L:Landroid/graphics/PointF;

.field private final M:Landroid/graphics/PointF;

.field private final N:Landroid/graphics/Matrix;

.field private final O:[I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 6

    .line 1
    invoke-direct/range {p0 .. p8}, Lcom/pspdfkit/internal/g7;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    .line 2
    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/rd;->L:Landroid/graphics/PointF;

    .line 6
    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/rd;->M:Landroid/graphics/PointF;

    .line 8
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    .line 39
    iget-object p2, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    const/high16 p5, 0x3f800000    # 1.0f

    if-eqz p4, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/pspdfkit/utils/Size;

    .line 40
    iget p7, p4, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/internal/g7;->A:F

    cmpl-float p7, p7, v0

    if-lez p7, :cond_0

    .line 41
    iget p4, p4, Lcom/pspdfkit/utils/Size;->height:F

    mul-float p4, p4, p5

    iput p4, p0, Lcom/pspdfkit/internal/g7;->A:F

    goto :goto_0

    .line 44
    :cond_1
    iget p2, p0, Lcom/pspdfkit/internal/g7;->z:F

    mul-float p2, p2, p5

    iput p2, p0, Lcom/pspdfkit/internal/g7;->z:F

    .line 46
    iget p2, p0, Lcom/pspdfkit/internal/g7;->A:F

    float-to-int p2, p2

    sub-int/2addr p3, p2

    div-int/lit8 p3, p3, 0x2

    iput p3, p0, Lcom/pspdfkit/internal/g7;->G:I

    .line 50
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    .line 51
    new-array p3, p2, [I

    iput-object p3, p0, Lcom/pspdfkit/internal/rd;->O:[I

    const/4 p4, 0x0

    .line 52
    aput p4, p3, p4

    const/4 p3, 0x1

    const/4 p7, 0x1

    :goto_1
    if-ge p7, p2, :cond_2

    add-int/lit8 v0, p7, -0x1

    .line 54
    invoke-interface {p8, v0}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v1

    .line 55
    iget-object v2, p0, Lcom/pspdfkit/internal/rd;->O:[I

    aget v0, v2, v0

    int-to-float v0, v0

    iget-object v3, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    .line 56
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/Size;

    iget v1, v1, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr v0, v1

    int-to-float v1, p6

    add-float/2addr v0, v1

    float-to-int v0, v0

    aput v0, v2, p7

    add-int/lit8 p7, p7, 0x1

    goto :goto_1

    :cond_2
    sub-int/2addr p2, p3

    .line 61
    iget p3, p0, Lcom/pspdfkit/internal/g7;->A:F

    iput p3, p0, Lcom/pspdfkit/internal/g7;->C:F

    .line 62
    iget-object p3, p0, Lcom/pspdfkit/internal/rd;->O:[I

    aget p3, p3, p2

    int-to-float p3, p3

    iget-object p6, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {p6, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/utils/Size;

    iget p2, p2, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr p3, p2

    mul-float p3, p3, p5

    iput p3, p0, Lcom/pspdfkit/internal/g7;->z:F

    iput p3, p0, Lcom/pspdfkit/internal/g7;->B:F

    .line 67
    invoke-virtual {p0, p4}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result p2

    neg-int p2, p2

    invoke-virtual {p0, p4}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result p3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    sub-int/2addr p3, p1

    div-int/lit8 p3, p3, 0x2

    add-int v1, p3, p2

    iput v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 9

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->A:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v2, v1

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_0

    float-to-int v0, v0

    sub-int/2addr v1, v0

    .line 2
    div-int/lit8 v1, v1, 0x2

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    if-eq v1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v4, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v5, p0, Lcom/pspdfkit/internal/g7;->G:I

    const/4 v6, 0x0

    sub-int v7, v1, v5

    const/16 v8, 0x190

    invoke-virtual/range {v3 .. v8}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final C()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v1, v1

    cmpl-float v2, v0, v1

    sub-float/2addr v1, v0

    if-lez v2, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v1, v0

    :goto_0
    float-to-int v0, v1

    return v0
.end method

.method public final D()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    iget v1, p0, Lcom/pspdfkit/internal/g7;->A:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final a(II)I
    .locals 4

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result p2

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    iget-object v2, p0, Lcom/pspdfkit/internal/rd;->O:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v1

    .line 13
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result v2

    if-ltz v2, :cond_0

    return v0

    .line 18
    :cond_0
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result v1

    add-int/2addr v1, v2

    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-gt v1, v2, :cond_1

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/rd;->O:[I

    array-length p1, p1

    :goto_0
    add-int/lit8 p1, p1, -0x1

    return p1

    .line 23
    :cond_1
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p1

    .line 24
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result p1

    if-ge v2, p1, :cond_2

    return v0

    .line 28
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/rd;->O:[I

    array-length p1, p1

    :goto_1
    add-int/lit8 p2, p1, -0x1

    if-ge v0, p2, :cond_4

    .line 29
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {p2, v0}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result p2

    .line 30
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v3}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v1

    .line 31
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result p2

    if-gt p2, v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result p2

    if-ge v2, p2, :cond_3

    return v0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 37
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/rd;->O:[I

    array-length p1, p1

    goto :goto_0
.end method

.method public final a(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5

    .line 75
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 76
    iget p1, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 77
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g7;->a(I)I

    move-result p1

    int-to-float p1, p1

    .line 80
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    .line 81
    iget v1, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v3, p1

    float-to-int p1, v3

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    add-float/2addr v1, p1

    neg-float p1, v1

    goto :goto_0

    .line 83
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, p1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    neg-float p1, p1

    .line 87
    :goto_0
    iget v1, p0, Lcom/pspdfkit/internal/g7;->z:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v1, v3

    if-gez v4, :cond_1

    sub-float/2addr v1, v3

    goto :goto_1

    .line 90
    :cond_1
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    int-to-float v3, v3

    sub-float/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    neg-float v1, v1

    .line 93
    :goto_1
    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, p1

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 94
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, p1

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 95
    iget p1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr p1, v1

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 96
    iget p1, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr p1, v1

    iput p1, v0, Landroid/graphics/RectF;->right:F

    return-object v0
.end method

.method public final a(IZ)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v4, p0, Lcom/pspdfkit/internal/g7;->G:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result p1

    neg-int v5, p1

    if-eqz p2, :cond_0

    const/16 p1, 0x190

    const/16 v7, 0x190

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v7, 0x0

    :goto_0
    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    if-nez p2, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalX()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final a(FFF)Z
    .locals 7

    const/4 v0, 0x1

    .line 38
    iput-boolean v0, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 40
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v1

    iget v1, p0, Lcom/pspdfkit/internal/ug;->d:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->e:F

    .line 41
    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 42
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    cmpl-float v2, p1, v1

    if-nez v2, :cond_0

    return v0

    :cond_0
    div-float v1, p1, v1

    .line 45
    iput p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    .line 48
    iget p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v2

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int v2, v2, v3

    int-to-float v2, v2

    sub-float/2addr p1, v2

    mul-float p1, p1, v1

    .line 49
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v2

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int v2, v2, v3

    int-to-float v2, v2

    add-float/2addr p1, v2

    iput p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    .line 50
    iget p1, p0, Lcom/pspdfkit/internal/g7;->A:F

    mul-float p1, p1, v1

    iput p1, p0, Lcom/pspdfkit/internal/g7;->A:F

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/internal/rd;->M:Landroid/graphics/PointF;

    .line 55
    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 56
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/g7;->E:I

    iget-object v1, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 57
    iget-object p2, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 60
    iget p2, p1, Landroid/graphics/PointF;->x:F

    iget-object p3, p0, Lcom/pspdfkit/internal/rd;->L:Landroid/graphics/PointF;

    iget p3, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p3

    iget-object p3, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    .line 61
    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p2

    float-to-int v4, p2

    .line 62
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget-object p2, p0, Lcom/pspdfkit/internal/rd;->L:Landroid/graphics/PointF;

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    .line 63
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p1

    neg-float p1, p1

    float-to-int v5, p1

    .line 65
    iget-object p1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {p1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 66
    iget-object v1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v2, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    const/4 p1, 0x0

    .line 70
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    :goto_0
    if-ge p1, p2, :cond_1

    .line 71
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(I)Lcom/pspdfkit/internal/dm;

    move-result-object p3

    .line 72
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/g7;->b(Lcom/pspdfkit/internal/dm;)V

    .line 73
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/g7;->a(Lcom/pspdfkit/internal/dm;)V

    .line 74
    invoke-static {p3}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final b(I)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/rd;->O:[I

    aget v0, v0, p1

    iget v1, p0, Lcom/pspdfkit/internal/ug;->f:I

    mul-int p1, p1, v1

    sub-int/2addr v0, p1

    int-to-float v0, v0

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float v0, v0, v1

    int-to-float p1, p1

    add-float/2addr v0, p1

    float-to-int p1, v0

    iget v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    add-int/2addr p1, v0

    return p1
.end method

.method public final b(II)I
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/rd;->a(II)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result p1

    return p1
.end method

.method public final b(FFF)Z
    .locals 1

    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->K:Z

    .line 7
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 9
    iput p2, p0, Lcom/pspdfkit/internal/g7;->H:F

    .line 10
    iput p3, p0, Lcom/pspdfkit/internal/g7;->I:F

    .line 11
    iget v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 12
    iput v0, p0, Lcom/pspdfkit/internal/g7;->E:I

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/rd;->L:Landroid/graphics/PointF;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 19
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/g7;->E:I

    iget-object v0, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 20
    iget-object p2, p0, Lcom/pspdfkit/internal/rd;->L:Landroid/graphics/PointF;

    iget-object p3, p0, Lcom/pspdfkit/internal/rd;->N:Landroid/graphics/Matrix;

    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    return p1
.end method

.method public final c(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/g7;->A:F

    sub-float/2addr v0, p1

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr v0, p1

    const/4 p1, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iget v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    add-int/2addr p1, v0

    return p1
.end method

.method public final c(II)Z
    .locals 5

    .line 3
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/g7;->c(II)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 5
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 6
    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    iget v2, p0, Lcom/pspdfkit/internal/g7;->A:F

    div-float v0, v2, v0

    float-to-int v0, v0

    sub-int/2addr p2, v0

    div-int/lit8 p2, p2, 0x2

    add-int/2addr v0, p2

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    float-to-int v2, v2

    add-int/2addr v2, v3

    invoke-static {p2, v0, v3, v2}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    int-to-float p2, p2

    iget v2, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    goto :goto_1

    :cond_1
    const/high16 v2, 0x40200000    # 2.5f

    mul-float v0, v0, v2

    .line 16
    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v0, v3

    div-float v3, v0, v3

    mul-float v3, v3, v2

    float-to-int v2, v3

    .line 17
    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int v4, v3, v2

    if-lt v2, v4, :cond_2

    .line 19
    div-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 20
    :cond_2
    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 21
    :goto_0
    iget-object p2, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    int-to-float v2, v3

    iget v3, p0, Lcom/pspdfkit/internal/g7;->y:F

    invoke-virtual {p2, p1, v2, v3, v0}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    :goto_1
    return v1
.end method

.method public final d(II)Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/g7;->K:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v0, v0

    iget v3, p0, Lcom/pspdfkit/internal/g7;->A:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    .line 13
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v2
.end method

.method public final e(II)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/g7;->A:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 p2, 0x0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int v3, p1

    neg-int v4, p2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final h()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v1, v1

    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sub-float/2addr v1, v0

    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v1, v0

    :goto_0
    float-to-int v0, v0

    return v0
.end method

.method public final i()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    iget v1, p0, Lcom/pspdfkit/internal/g7;->A:F

    float-to-int v1, v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected final k(I)V
    .locals 8

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/rd;->b(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr p1, v1

    div-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v4, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int v5, p1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalX()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

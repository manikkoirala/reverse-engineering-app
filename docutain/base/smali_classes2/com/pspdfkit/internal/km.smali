.class abstract Lcom/pspdfkit/internal/km;
.super Lcom/pspdfkit/internal/ca;
.source "SourceFile"


# instance fields
.field private final A:Landroid/graphics/Matrix;

.field protected B:I

.field C:F

.field D:I

.field E:I

.field F:Landroid/widget/OverScroller;

.field G:Lcom/pspdfkit/internal/qq;

.field H:I

.field I:I

.field J:Z

.field K:Z

.field private final L:Lcom/pspdfkit/internal/rv;

.field private M:Z

.field private N:Z

.field private final y:Landroid/graphics/PointF;

.field private final z:Landroid/graphics/PointF;


# direct methods
.method public static synthetic $r8$lambda$Txw5PDtY72yIt4YL1RfCRKt2kTw(Lcom/pspdfkit/internal/km;IIIFJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/km;->c(IIIFJ)V

    return-void
.end method

.method public static synthetic $r8$lambda$iBumjaOVn_NLsQqRumSDR7zamOw(Lcom/pspdfkit/internal/km;Landroid/graphics/RectF;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/km;->c(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method public static synthetic $r8$lambda$ijgheLrzaemExDBoZcD9EwrVNfg(Lcom/pspdfkit/internal/km;Lcom/pspdfkit/internal/ug$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->c(Lcom/pspdfkit/internal/ug$a;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p10}, Lcom/pspdfkit/internal/ca;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V

    .line 2
    new-instance p4, Landroid/graphics/PointF;

    invoke-direct {p4}, Landroid/graphics/PointF;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/km;->y:Landroid/graphics/PointF;

    .line 7
    new-instance p4, Landroid/graphics/PointF;

    invoke-direct {p4}, Landroid/graphics/PointF;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/km;->z:Landroid/graphics/PointF;

    .line 12
    new-instance p4, Landroid/graphics/Matrix;

    invoke-direct {p4}, Landroid/graphics/Matrix;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    const/4 p4, 0x0

    .line 20
    iput p4, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 22
    iget p5, p0, Lcom/pspdfkit/internal/ug;->c:F

    iput p5, p0, Lcom/pspdfkit/internal/km;->C:F

    const/4 p5, 0x1

    .line 56
    iput-boolean p5, p0, Lcom/pspdfkit/internal/km;->J:Z

    .line 67
    iput-boolean p4, p0, Lcom/pspdfkit/internal/km;->M:Z

    .line 72
    iput-boolean p4, p0, Lcom/pspdfkit/internal/km;->N:Z

    .line 113
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p5

    .line 114
    new-instance p6, Landroid/widget/OverScroller;

    invoke-direct {p6, p5}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object p6, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    .line 115
    new-instance p6, Lcom/pspdfkit/internal/qq;

    invoke-direct {p6, p5}, Lcom/pspdfkit/internal/qq;-><init>(Landroid/content/Context;)V

    iput-object p6, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    .line 116
    new-instance p5, Lcom/pspdfkit/internal/rv;

    invoke-direct {p5, p1, p0}, Lcom/pspdfkit/internal/rv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/ug;)V

    iput-object p5, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    .line 119
    iget p1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 120
    invoke-interface {p10, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result p1

    .line 121
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->l(I)I

    move-result p1

    invoke-static {p1}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result p1

    const/high16 p5, 0x3f800000    # 1.0f

    if-eqz p1, :cond_0

    .line 130
    iget p1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->t(I)I

    move-result p1

    sub-int/2addr p2, p1

    div-int/lit8 p2, p2, 0x2

    invoke-static {p2, p4}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/km;->D:I

    goto :goto_0

    :cond_0
    int-to-float p1, p2

    .line 131
    iget p2, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 132
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result p2

    int-to-float p2, p2

    mul-float p2, p2, p5

    sub-float/2addr p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ca;->w:I

    int-to-float p2, p2

    sub-float/2addr p1, p2

    const/high16 p2, 0x40000000    # 2.0f

    div-float/2addr p1, p2

    const/4 p2, 0x0

    .line 133
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 144
    :goto_0
    iget p1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result p1

    int-to-float p1, p1

    mul-float p1, p1, p5

    float-to-int p1, p1

    .line 145
    iget p2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/km;->a(I)I

    move-result p2

    sub-int p2, p1, p2

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr p3, p1

    .line 146
    div-int/lit8 p3, p3, 0x2

    .line 147
    invoke-static {p4, p3}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/2addr p1, p2

    iput p1, p0, Lcom/pspdfkit/internal/km;->E:I

    return-void
.end method

.method private C()I
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->p(I)I

    move-result v0

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 8
    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->d()F

    move-result v3

    const v4, 0x3c23d70a    # 0.01f

    add-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 10
    iget v2, p0, Lcom/pspdfkit/internal/km;->D:I

    if-lez v2, :cond_0

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->t(I)I

    move-result v2

    iget v3, p0, Lcom/pspdfkit/internal/km;->D:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_1

    goto :goto_0

    .line 15
    :cond_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->t(I)I

    move-result v2

    iget v3, p0, Lcom/pspdfkit/internal/km;->D:I

    neg-int v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v3

    if-le v2, v4, :cond_1

    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    move v0, v1

    :cond_3
    :goto_2
    return v0
.end method

.method private b(Landroid/graphics/RectF;IJZ)V
    .locals 7

    .line 89
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p2}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 90
    new-instance v2, Landroid/graphics/RectF;

    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-direct {v2, v4, v4, v1, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 92
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/km;->b(I)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v3

    sub-int/2addr v1, v3

    .line 93
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/km;->c(I)I

    move-result v3

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result v0

    sub-int/2addr v3, v0

    .line 95
    iget v0, p1, Landroid/graphics/RectF;->left:F

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 96
    iget v0, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 97
    iget v0, p1, Landroid/graphics/RectF;->top:F

    int-to-float v1, v3

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 98
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    if-eqz p5, :cond_0

    .line 99
    iget p5, p0, Lcom/pspdfkit/internal/km;->D:I

    const/4 v0, 0x0

    invoke-static {p5, v0}, Ljava/lang/Math;->min(II)I

    move-result p5

    .line 100
    iget v1, p0, Lcom/pspdfkit/internal/km;->E:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 103
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 105
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result p2

    int-to-float p2, p2

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float p2, p2, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v3, v3

    invoke-static {p2, v3}, Ljava/lang/Math;->max(FF)F

    move-result p2

    float-to-int p2, p2

    .line 107
    new-instance v3, Landroid/graphics/RectF;

    int-to-float p5, p5

    int-to-float v0, v0

    int-to-float v1, v1

    int-to-float p2, p2

    invoke-direct {v3, p5, v0, v1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p1, v3}, Lcom/pspdfkit/internal/ga;->b(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    iget v4, p0, Lcom/pspdfkit/internal/km;->C:F

    move-object v3, p1

    move-wide v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/rv;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;FJ)V

    return-void
.end method

.method private b(III)Z
    .locals 5

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 113
    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(I)I

    move-result p2

    sub-int/2addr v2, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/View;->getScrollY()I

    move-result p2

    add-int/2addr p2, p3

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->c(I)I

    move-result p3

    sub-int/2addr p2, p3

    .line 114
    invoke-virtual {v0, v2, p2}, Lcom/pspdfkit/internal/dm;->b(II)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 115
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {p3, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p3

    .line 116
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/ca;->l(I)I

    move-result v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 119
    :goto_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(I)I

    move-result v2

    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v4

    sub-int/2addr v2, v4

    if-eqz v0, :cond_2

    iget v4, p0, Lcom/pspdfkit/internal/ca;->w:I

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    sub-int/2addr v2, v4

    .line 120
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->c(I)I

    move-result p1

    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result p3

    sub-int/2addr p1, p3

    .line 122
    iget p3, p2, Landroid/graphics/RectF;->left:F

    float-to-int p3, p3

    add-int/2addr p3, v2

    iget v4, p2, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    add-int/2addr v4, v2

    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-static {p3, v4, v1, v2}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p3

    .line 124
    iget v2, p2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    add-int/2addr v2, p1

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    add-int/2addr v4, p1

    iget p1, p0, Lcom/pspdfkit/internal/ug;->j:I

    invoke-static {v2, v4, v1, p1}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    .line 126
    iget v1, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v2, v2

    mul-float v1, v1, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result p2

    div-float/2addr v1, p2

    .line 127
    iget-object p2, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    int-to-float p3, p3

    if-eqz v0, :cond_3

    .line 128
    iget v0, p0, Lcom/pspdfkit/internal/ca;->w:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    add-float/2addr p3, v0

    int-to-float p1, p1

    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    .line 129
    invoke-virtual {p2, p3, p1, v0, v1}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    return v3

    :cond_4
    return v1
.end method

.method private b(IZ)Z
    .locals 8

    .line 130
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int v5, v0, v1

    .line 131
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    sub-int v6, p1, v0

    if-nez v5, :cond_1

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 135
    iget-object v2, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v3

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    const/16 v7, 0x190

    .line 137
    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 139
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private b(Z)Z
    .locals 9

    .line 140
    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 141
    iget v2, p0, Lcom/pspdfkit/internal/km;->E:I

    .line 144
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v0

    const/4 v6, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/ca;->w:I

    .line 146
    :goto_0
    iget v3, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 147
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v3, v3, v4

    float-to-int v3, v3

    add-int/2addr v3, v0

    .line 148
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v0, v0, v4

    float-to-int v0, v0

    .line 151
    iget v5, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-gt v3, v5, :cond_1

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    goto :goto_1

    :cond_1
    move v3, v1

    .line 152
    :goto_1
    iget v7, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-gt v0, v7, :cond_2

    sub-int v0, v7, v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_2
    move v0, v2

    :goto_2
    const v8, 0x3c23d70a    # 0.01f

    add-float/2addr v4, v8

    .line 154
    iget v8, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpg-float v4, v4, v8

    if-gez v4, :cond_4

    if-eqz p1, :cond_3

    .line 156
    div-int/lit8 v5, v5, 0x2

    div-int/lit8 v7, v7, 0x2

    invoke-virtual {p0, v5, v7}, Lcom/pspdfkit/internal/km;->c(II)Z

    :cond_3
    return v6

    :cond_4
    if-ne v1, v3, :cond_6

    if-eq v2, v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 p1, 0x1

    return p1

    :cond_6
    :goto_3
    if-eqz p1, :cond_7

    .line 162
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    sub-int/2addr v3, v1

    sub-int v4, v0, v2

    const/16 v5, 0x190

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 164
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_7
    return v6
.end method

.method private c(IIIFJ)V
    .locals 7

    .line 75
    new-instance v0, Landroid/graphics/PointF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 p1, 0x0

    .line 77
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    .line 78
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 80
    iget p1, p0, Lcom/pspdfkit/internal/km;->C:F

    div-float/2addr p4, p1

    .line 81
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float p1, p1

    div-float/2addr p1, p4

    float-to-int p1, p1

    .line 82
    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float p2, p2

    div-float/2addr p2, p4

    float-to-int p2, p2

    .line 83
    new-instance v2, Landroid/graphics/RectF;

    iget p4, v0, Landroid/graphics/PointF;->x:F

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    sub-float v1, p4, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    sub-float v3, v0, p2

    add-float/2addr p4, p1

    add-float/2addr v0, p2

    invoke-direct {v2, v1, v3, p4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v6, 0x1

    move-object v1, p0

    move v3, p3

    move-wide v4, p5

    .line 84
    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/km;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method private c(Landroid/graphics/RectF;IJ)V
    .locals 6

    .line 85
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    const/4 v0, 0x0

    .line 87
    invoke-virtual {p0, p2, v0}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 88
    invoke-virtual {v1, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 89
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    const/4 v5, 0x1

    move-object v0, p0

    move v2, p2

    move-wide v3, p3

    .line 90
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/km;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method private c(Lcom/pspdfkit/internal/ug$a;)V
    .locals 8

    .line 91
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->b:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p1, Lcom/pspdfkit/internal/ug$a;->a:Landroid/graphics/RectF;

    .line 96
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-direct {v1, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 97
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    .line 98
    invoke-virtual {p0, v0, v2}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 99
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 103
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v3, p1, Lcom/pspdfkit/internal/ug$a;->b:F

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 104
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v4, v4

    div-float/2addr v4, v3

    float-to-int v3, v4

    .line 105
    new-instance v4, Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/PointF;->x:F

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float v6, v5, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float v7, v1, v3

    add-float/2addr v5, v0

    add-float/2addr v1, v3

    invoke-direct {v4, v6, v7, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 112
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/km;->a(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->b(Landroid/graphics/RectF;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    if-ne v0, p1, :cond_1

    .line 116
    iput-object v2, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    .line 117
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->s()V

    :cond_1
    return-void
.end method

.method private u(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    if-eq p1, v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public A()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->a()Z

    move-result v0

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/km;->b(Z)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    if-nez v0, :cond_2

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    if-eqz v0, :cond_1

    return-void

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ug;->a(II)I

    move-result v0

    .line 8
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result v0

    .line 11
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/km;->b(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/km;->J:Z

    .line 14
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    xor-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/km;->b(Z)Z

    :cond_2
    return-void
.end method

.method final D()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_0
    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method final E()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method final F()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_0
    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method final G()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public a(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/pspdfkit/internal/km;->C:F

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    mul-float v0, v0, p1

    float-to-int p1, v0

    return p1
.end method

.method public a(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 6

    .line 168
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 170
    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->c()I

    move-result v1

    .line 172
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v2, v2, v3

    float-to-int v2, v2

    .line 173
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v1, v1, v3

    float-to-int v1, v1

    int-to-float v2, v2

    .line 176
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/4 v4, 0x0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    .line 177
    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v5

    sub-float/2addr v5, v2

    float-to-int v2, v5

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v3, v2

    neg-float v2, v3

    goto :goto_0

    .line 179
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    neg-float v2, v2

    :goto_0
    int-to-float v1, v1

    .line 183
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v3

    cmpg-float v3, v1, v3

    if-gez v3, :cond_1

    .line 184
    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    sub-float/2addr p1, v1

    float-to-int p1, p1

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    add-float/2addr v3, p1

    neg-float p1, v3

    goto :goto_1

    .line 186
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget p1, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr p1, v1

    invoke-static {p1, v4}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    neg-float p1, p1

    .line 189
    :goto_1
    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 190
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 191
    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 192
    iget v1, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    return-object v0
.end method

.method public a(F)V
    .locals 0

    const/4 p1, 0x0

    .line 122
    iput-boolean p1, p0, Lcom/pspdfkit/internal/km;->M:Z

    const/4 p1, 0x1

    .line 123
    iput-boolean p1, p0, Lcom/pspdfkit/internal/km;->N:Z

    .line 124
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    return-void
.end method

.method public a(III)V
    .locals 6

    .line 138
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    iget v2, p0, Lcom/pspdfkit/internal/km;->E:I

    neg-int p1, p1

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p1

    neg-int p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    div-int/lit8 p2, p2, 0x2

    add-int v4, p2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 144
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method protected final a(IIIFJJ)V
    .locals 13

    move-object v8, p0

    move/from16 v4, p3

    .line 125
    invoke-direct {p0, v4}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 126
    invoke-virtual {p0, v4, v0}, Lcom/pspdfkit/internal/km;->a(IZ)V

    move-wide/from16 v9, p7

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    move-wide v9, v0

    .line 130
    :goto_0
    iget-object v11, v8, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v12, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda2;

    move-object v0, v12

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/km;IIIFJ)V

    invoke-virtual {v11, v12, v9, v10}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(IZ)V
    .locals 8

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->v(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    .line 7
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result p1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/km;->b(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v3

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v4

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->h()I

    move-result v2

    .line 12
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->i()I

    move-result v2

    .line 14
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 15
    iget-object v2, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    sub-int v5, v1, v3

    sub-int v6, p1, v4

    if-eqz p2, :cond_0

    const/16 v0, 0x190

    const/16 v7, 0x190

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    .line 18
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->b(Z)Z

    :goto_1
    return-void
.end method

.method public a(Landroid/graphics/RectF;IJ)V
    .locals 10

    .line 131
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    .line 132
    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    if-eq v1, p2, :cond_0

    if-eq p2, v0, :cond_0

    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0, p2, v0}, Lcom/pspdfkit/internal/km;->a(IZ)V

    const-wide/16 v0, 0x1f4

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 137
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v9, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda1;

    move-object v3, v9

    move-object v4, p0

    move-object v5, p1

    move v6, p2

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/km;Landroid/graphics/RectF;IJ)V

    invoke-virtual {v2, v9, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Landroid/graphics/RectF;IJZ)V
    .locals 10

    .line 145
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 147
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 148
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->h(I)Landroid/graphics/RectF;

    move-result-object v1

    if-nez p5, :cond_0

    .line 150
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result p5

    if-eqz p5, :cond_0

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result p5

    if-nez p5, :cond_1

    .line 152
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result p5

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr p5, v2

    .line 153
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v1, v0

    .line 154
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/km;->i(I)F

    move-result v0

    mul-float p5, p5, v0

    mul-float v1, v1, v0

    .line 158
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 159
    invoke-static {v0, p5}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 162
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->k()F

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 163
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->j()F

    move-result v1

    .line 164
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    invoke-static {v0, p5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 165
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p5

    float-to-int v2, p5

    .line 166
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    float-to-int v3, p1

    const-wide/16 v8, 0x64

    move-object v1, p0

    move v4, p2

    move-wide v6, p3

    .line 167
    invoke-virtual/range {v1 .. v9}, Lcom/pspdfkit/internal/km;->a(IIIFJJ)V

    :cond_1
    return-void
.end method

.method public abstract a(Lcom/pspdfkit/internal/dm;)V
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/rv;->a()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/km;->M:Z

    const/4 p1, 0x0

    .line 20
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 21
    iput-boolean v0, p0, Lcom/pspdfkit/internal/km;->N:Z

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 7

    .line 24
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->N:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    if-eqz v0, :cond_1

    .line 25
    iput-boolean v1, p0, Lcom/pspdfkit/internal/km;->N:Z

    .line 26
    invoke-direct {p0}, Lcom/pspdfkit/internal/km;->C()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->h()I

    move-result v3

    .line 32
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 33
    iget-object v3, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->i()I

    move-result v4

    .line 34
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 35
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->scrollTo(II)V

    .line 38
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g(I)V

    return v2

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    .line 45
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v3, v0}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result v3

    .line 46
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v4

    .line 48
    iget v5, p0, Lcom/pspdfkit/internal/km;->B:I

    if-ne v5, v0, :cond_3

    const/4 v6, 0x1

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    :goto_0
    if-ne v5, v4, :cond_4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    .line 49
    :goto_1
    invoke-direct {p0, v3, v1}, Lcom/pspdfkit/internal/km;->b(IZ)Z

    move-result v3

    .line 50
    iput-boolean v3, p0, Lcom/pspdfkit/internal/km;->J:Z

    if-eqz v3, :cond_5

    if-nez v6, :cond_5

    if-nez v4, :cond_5

    .line 52
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->v(I)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->o()V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v1

    .line 58
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->o()V

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->J:Z

    if-eqz v0, :cond_9

    .line 62
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    if-eqz v0, :cond_6

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->E:I

    goto :goto_2

    .line 66
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    .line 67
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->F()I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->D()I

    move-result v3

    .line 68
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 69
    iput v0, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    .line 71
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->G()I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->E()I

    move-result v3

    .line 72
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 73
    iput v0, p0, Lcom/pspdfkit/internal/km;->E:I

    .line 74
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 76
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    .line 80
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 82
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    :cond_8
    return v2

    .line 86
    :cond_9
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->N:Z

    if-eqz v0, :cond_a

    .line 87
    iput-boolean v1, p0, Lcom/pspdfkit/internal/km;->N:Z

    .line 88
    invoke-direct {p0}, Lcom/pspdfkit/internal/km;->C()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->B:I

    :cond_a
    return v1
.end method

.method public a(FFF)Z
    .locals 8

    .line 89
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float p1, p1, v0

    iget v0, p0, Lcom/pspdfkit/internal/ug;->d:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->e:F

    .line 90
    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 91
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    const/4 v1, 0x1

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    return v1

    .line 92
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/km;->C:F

    .line 96
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->z:Landroid/graphics/PointF;

    .line 97
    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 98
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/km;->B:I

    iget-object v0, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 99
    iget-object p2, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 102
    iget p2, p1, Landroid/graphics/PointF;->x:F

    iget-object p3, p0, Lcom/pspdfkit/internal/km;->y:Landroid/graphics/PointF;

    iget p3, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p3

    iget-object p3, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    .line 103
    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p2

    float-to-int v5, p2

    .line 104
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget-object p2, p0, Lcom/pspdfkit/internal/km;->y:Landroid/graphics/PointF;

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    .line 105
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p1

    neg-float p1, p1

    float-to-int v6, p1

    .line 107
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 109
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(Lcom/pspdfkit/internal/dm;)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    .line 111
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 114
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 116
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(Lcom/pspdfkit/internal/dm;)V

    .line 117
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    .line 118
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 121
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/km;->D:I

    iget v4, p0, Lcom/pspdfkit/internal/km;->E:I

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    return v1
.end method

.method public b(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result v1

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->l(I)I

    move-result v2

    invoke-static {v2}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    if-eqz v1, :cond_0

    .line 29
    iget p1, p0, Lcom/pspdfkit/internal/km;->D:I

    goto :goto_0

    .line 30
    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->t(I)I

    move-result p1

    sub-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x2

    const/4 p1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0

    :cond_1
    add-int/lit8 p1, v0, -0x1

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v1

    .line 32
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/km;->b(I)I

    move-result v2

    .line 33
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result p1

    sub-int/2addr v2, p1

    .line 34
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/km;->t(I)I

    move-result p1

    add-int/2addr v2, p1

    iget p1, p0, Lcom/pspdfkit/internal/ca;->w:I

    add-int/2addr p1, v2

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 35
    iget p1, p0, Lcom/pspdfkit/internal/km;->D:I

    goto :goto_0

    .line 36
    :cond_3
    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v1, v1

    .line 38
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result p1

    int-to-float p1, p1

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    mul-float p1, p1, v2

    sub-float/2addr v1, p1

    iget p1, p0, Lcom/pspdfkit/internal/ca;->w:I

    int-to-float p1, p1

    sub-float/2addr v1, p1

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr v1, p1

    const/4 p1, 0x0

    .line 39
    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    .line 62
    :goto_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method protected final b(Landroid/graphics/RectF;)V
    .locals 6

    .line 80
    iget v2, p0, Lcom/pspdfkit/internal/km;->B:I

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/km;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method protected final b(Landroid/graphics/RectF;IJ)V
    .locals 6

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    .line 79
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/km;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method public abstract b(Lcom/pspdfkit/internal/dm;)V
.end method

.method public b(Lcom/pspdfkit/internal/ug$a;)V
    .locals 2

    .line 81
    iput-object p1, p0, Lcom/pspdfkit/internal/ug;->p:Lcom/pspdfkit/internal/ug$a;

    .line 83
    iget v0, p1, Lcom/pspdfkit/internal/ug$a;->c:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/km;->a(IZ)V

    .line 88
    new-instance v0, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/km$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/km;Lcom/pspdfkit/internal/ug$a;)V

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(FFF)Z
    .locals 1

    const/4 p1, 0x1

    .line 63
    iput-boolean p1, p0, Lcom/pspdfkit/internal/km;->K:Z

    .line 65
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 66
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result p1

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/km;->b(IZ)Z

    move-result p1

    .line 68
    iput-boolean p1, p0, Lcom/pspdfkit/internal/km;->M:Z

    .line 74
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->y:Landroid/graphics/PointF;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 75
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p2, p0, Lcom/pspdfkit/internal/km;->B:I

    iget-object p3, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 76
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->y:Landroid/graphics/PointF;

    iget-object p2, p0, Lcom/pspdfkit/internal/km;->A:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 78
    iget-boolean p1, p0, Lcom/pspdfkit/internal/km;->M:Z

    return p1
.end method

.method public abstract c()I
.end method

.method public c(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result v1

    .line 3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v2

    int-to-float v2, v2

    if-eqz v1, :cond_0

    .line 4
    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    mul-float v2, v2, v3

    float-to-int v2, v2

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->a(I)I

    move-result p1

    sub-int p1, v2, p1

    div-int/lit8 p1, p1, 0x2

    if-eqz v1, :cond_1

    .line 9
    iget v1, p0, Lcom/pspdfkit/internal/km;->E:I

    goto :goto_1

    .line 11
    :cond_1
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_1
    add-int/2addr v1, p1

    .line 14
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result p1

    add-int/2addr p1, v1

    return p1
.end method

.method public c(II)Z
    .locals 9

    .line 15
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->J:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 16
    :cond_0
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 18
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    .line 21
    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    const/4 v4, 0x1

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 22
    iget v2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-direct {p0, v2, p1, p2}, Lcom/pspdfkit/internal/km;->b(III)Z

    move-result v2

    if-eqz v2, :cond_1

    return v4

    :cond_1
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 24
    invoke-direct {p0, v0, p1, p2}, Lcom/pspdfkit/internal/km;->b(III)Z

    move-result v0

    if-eqz v0, :cond_2

    return v4

    .line 30
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_5

    .line 31
    iget p1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result p1

    .line 32
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_0
    add-int/2addr p1, v1

    int-to-float p1, p1

    .line 33
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v0

    int-to-float v0, v0

    .line 35
    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 36
    iget v2, p0, Lcom/pspdfkit/internal/km;->E:I

    .line 38
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v3, v3

    sub-float/2addr v3, p1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    .line 39
    iget v6, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v6, v6

    sub-float/2addr v6, v0

    div-float/2addr v6, v5

    float-to-int v5, v3

    add-float/2addr v3, p1

    float-to-int v3, v3

    int-to-float v7, v1

    .line 42
    iget v8, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float p1, p1, v8

    add-float/2addr p1, v7

    float-to-int p1, p1

    invoke-static {v5, v3, v1, p1}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    float-to-int v1, v6

    add-float/2addr v6, v0

    float-to-int v3, v6

    int-to-float v5, v2

    .line 44
    iget v6, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v6, v6, v0

    add-float/2addr v6, v5

    float-to-int v5, v6

    invoke-static {v1, v3, v2, v5}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result v1

    .line 48
    iget-object v2, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    .line 50
    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    int-to-float p2, p2

    goto :goto_1

    :cond_4
    int-to-float p2, v1

    :goto_1
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    .line 51
    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    goto :goto_4

    :cond_5
    const/high16 v1, 0x40200000    # 2.5f

    mul-float v0, v0, v1

    .line 64
    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v0, v2

    div-float v2, v0, v2

    mul-float v1, v1, v2

    float-to-int v1, v1

    .line 65
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int v5, v3, v1

    if-lt v1, v5, :cond_6

    .line 67
    div-int/lit8 v3, v3, 0x2

    goto :goto_2

    .line 68
    :cond_6
    invoke-static {p1, v5}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 69
    :goto_2
    iget p1, p0, Lcom/pspdfkit/internal/km;->E:I

    int-to-float p1, p1

    mul-float p1, p1, v2

    float-to-int p1, p1

    .line 70
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int v2, v1, p1

    if-lt p1, v2, :cond_7

    .line 72
    div-int/lit8 v1, v1, 0x2

    goto :goto_3

    .line 73
    :cond_7
    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 74
    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/internal/km;->L:Lcom/pspdfkit/internal/rv;

    int-to-float p2, v3

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    :goto_4
    return v4
.end method

.method final c(Z)Z
    .locals 0

    const/4 p1, 0x0

    .line 118
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->b(Z)Z

    move-result p1

    return p1
.end method

.method public i(I)F
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/pspdfkit/internal/km;->C:F

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    return p1
.end method

.method public t(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/pspdfkit/internal/km;->C:F

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    mul-float v0, v0, p1

    float-to-int p1, v0

    return p1
.end method

.method final v(I)V
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/km;->u(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5
    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    iput v1, p0, Lcom/pspdfkit/internal/km;->C:F

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result v1

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v2

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->r(I)I

    move-result v3

    .line 12
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/km;->b(I)I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v1

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/pspdfkit/internal/km;->D:I

    .line 13
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/km;->c(I)I

    move-result v1

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/pspdfkit/internal/km;->E:I

    .line 16
    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/km;->B:I

    .line 17
    iput p1, p0, Lcom/pspdfkit/internal/km;->B:I

    if-eqz v0, :cond_2

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 23
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(Lcom/pspdfkit/internal/dm;)V

    .line 24
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    .line 28
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 29
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 31
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->b(Lcom/pspdfkit/internal/dm;)V

    .line 32
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->a(Lcom/pspdfkit/internal/dm;)V

    :cond_2
    return-void
.end method

.method public w()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->M:Z

    return v0
.end method

.method public x()V
    .locals 2

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/km;->K:Z

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v1, v0}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->H:I

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/km;->I:I

    return-void
.end method

.class final Lcom/pspdfkit/internal/yu$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field a:I

.field b:Z

.field final synthetic c:Lcom/pspdfkit/internal/yu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/yu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/yu$f;->a:I

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu$f;->b:Z

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;

    move-result-object p1

    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->getDuration()I

    move-result p1

    int-to-long v0, p1

    int-to-long p1, p2

    mul-long v0, v0, p1

    const-wide/16 p1, 0x3e8

    .line 8
    div-long/2addr v0, p1

    long-to-int p1, v0

    .line 9
    iput p1, p0, Lcom/pspdfkit/internal/yu$f;->a:I

    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu$f;->b:Z

    :cond_1
    :goto_0
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x36ee80

    .line 4
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fputm(Lcom/pspdfkit/internal/yu;Z)V

    .line 7
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetw(Lcom/pspdfkit/internal/yu;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yu$f;->b:Z

    if-eqz v0, :cond_1

    .line 5
    iget v0, p0, Lcom/pspdfkit/internal/yu$f;->a:I

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zu;->b(I)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetj(Lcom/pspdfkit/internal/yu;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7
    iget v1, p0, Lcom/pspdfkit/internal/yu$f;->a:I

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/yu;->-$$Nest$mc(Lcom/pspdfkit/internal/yu;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fputm(Lcom/pspdfkit/internal/yu;Z)V

    .line 11
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$md(Lcom/pspdfkit/internal/yu;)I

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$mh(Lcom/pspdfkit/internal/yu;)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    const/16 v0, 0xbb8

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$f;->c:Lcom/pspdfkit/internal/yu;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fputl(Lcom/pspdfkit/internal/yu;Z)V

    .line 19
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetw(Lcom/pspdfkit/internal/yu;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

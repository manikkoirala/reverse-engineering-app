.class final Lcom/pspdfkit/internal/wc$a;
.super Lcom/pspdfkit/internal/o7$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/wc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/wc;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/wc;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-direct {p0}, Lcom/pspdfkit/internal/o7$c;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/wc;Lcom/pspdfkit/internal/wc$a-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wc$a;-><init>(Lcom/pspdfkit/internal/wc;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->a(Landroid/view/MotionEvent;)V

    return-void

    .line 5
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgeta(Lcom/pspdfkit/internal/wc;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->b(Landroid/view/MotionEvent;)V

    return-void

    .line 5
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgeta(Lcom/pspdfkit/internal/wc;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/vc;->b:Lcom/pspdfkit/internal/vc;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {p1}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/internal/vc;->b:Lcom/pspdfkit/internal/vc;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return v1
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/wc;->-$$Nest$fpute(Lcom/pspdfkit/internal/wc;Lcom/pspdfkit/internal/xc;)V

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgeta(Lcom/pspdfkit/internal/wc;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->c(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/wc;->-$$Nest$fpute(Lcom/pspdfkit/internal/wc;Lcom/pspdfkit/internal/xc;)V

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->onDown(Landroid/view/MotionEvent;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetc(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/vc;

    .line 13
    iget-object v4, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v4}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 14
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 16
    iget-object v5, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v5}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetc(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v5

    .line 17
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/yc;

    .line 18
    invoke-interface {v5}, Lcom/pspdfkit/internal/yc;->a()Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v6}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v6

    .line 19
    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v5}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v5

    .line 20
    invoke-interface {v5, v1, p1}, Lcom/pspdfkit/internal/xc;->a(Lcom/pspdfkit/internal/vc;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v1}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    return v3

    .line 29
    :cond_4
    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgeta(Lcom/pspdfkit/internal/wc;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 30
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->onDown(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 34
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetc(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/vc;

    .line 35
    iget-object v4, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v4}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 36
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 39
    iget-object v5, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v5}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetc(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/yc;

    invoke-interface {v5}, Lcom/pspdfkit/internal/yc;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/internal/xc;

    .line 40
    invoke-interface {v6, v1, p1}, Lcom/pspdfkit/internal/xc;->a(Lcom/pspdfkit/internal/vc;Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 41
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    goto :goto_2

    :cond_8
    return v3
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/vc;->c:Lcom/pspdfkit/internal/vc;

    .line 3
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v3}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/vc;->c:Lcom/pspdfkit/internal/vc;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/xc;

    invoke-interface {v3, p1}, Lcom/pspdfkit/internal/xc;->onLongPress(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    if-ge v1, v0, :cond_2

    if-ne v1, v2, :cond_0

    goto :goto_2

    .line 12
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v3}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/vc;->c:Lcom/pspdfkit/internal/vc;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/xc;

    invoke-interface {v3, p1}, Lcom/pspdfkit/internal/xc;->b(Landroid/view/MotionEvent;)V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/vc;->d:Lcom/pspdfkit/internal/vc;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/xc;

    .line 5
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/pspdfkit/internal/xc;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v3, :cond_2

    .line 9
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 10
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 18
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_1
    return v3
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {v0}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/vc;->a:Lcom/pspdfkit/internal/vc;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xc;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xc;->d(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/wc$a;->a:Lcom/pspdfkit/internal/wc;

    invoke-static {p1}, Lcom/pspdfkit/internal/wc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/internal/vc;->a:Lcom/pspdfkit/internal/vc;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return v1
.end method

.class public final Lcom/pspdfkit/internal/cn$e$a;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/cn$e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/dn;

.field private final b:Landroid/widget/RelativeLayout;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/View;

.field private f:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

.field private final g:I

.field private final h:I

.field final synthetic i:Lcom/pspdfkit/internal/cn$e;


# direct methods
.method public static synthetic $r8$lambda$pd4sF2PLlcoyh4lieU7opj0-EKU(Lcom/pspdfkit/internal/cn$e$a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cn$e$a;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xL7GLMkVuWGz-PjBAZ-RM8uZB-g(Lcom/pspdfkit/internal/cn$e$a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cn$e$a;->b(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e$a;->a:Lcom/pspdfkit/internal/dn;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/RelativeLayout;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e$a;->b:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e$a;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e$a;->d:Landroid/widget/ImageView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/cn$e$a;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e$a;->e:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/cn$e$a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/cn$e$a;->g:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/cn$e$a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/cn$e$a;->h:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/cn$e$a;Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/cn$e$a;->f:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/cn$e;Landroid/view/View;Lcom/pspdfkit/internal/dn;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    .line 2
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/internal/cn$e$a;->a:Lcom/pspdfkit/internal/dn;

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__tab_item_container:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/cn$e$a;->b:Landroid/widget/RelativeLayout;

    .line 6
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dn;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p3}, Lcom/pspdfkit/internal/dn;->b()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 9
    sget v0, Lcom/pspdfkit/R$id;->pspdf__tab_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/cn$e$a;->c:Landroid/widget/TextView;

    .line 10
    new-instance v1, Lcom/pspdfkit/internal/cn$e$a$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/cn$e$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/cn$e$a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 15
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dn;->f()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 17
    sget v0, Lcom/pspdfkit/R$id;->pspdf__tab_close:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/cn$e$a;->d:Landroid/widget/ImageView;

    .line 18
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e;)Landroid/content/Context;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_close:I

    .line 19
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dn;->h()I

    move-result v2

    .line 20
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 22
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/cn$e$a;->h:I

    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout$LayoutParams;->getMarginEnd()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/cn$e$a;->g:I

    .line 24
    new-instance p1, Lcom/pspdfkit/internal/cn$e$a$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/cn$e$a$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/cn$e$a;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    sget p1, Lcom/pspdfkit/R$id;->pspdf__tab_selection_indicator:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/cn$e$a;->e:Landroid/view/View;

    .line 31
    invoke-virtual {p3}, Lcom/pspdfkit/internal/dn;->i()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/cn$e$a;->f:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-eqz p1, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v0, v0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/cn$c;->shouldSelectTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_1
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/cn$e$a;->f:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-eqz p1, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v0, v0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/cn$c;->shouldCloseTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cn;->c(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_1
    return-void
.end method

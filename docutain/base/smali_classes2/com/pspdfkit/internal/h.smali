.class final Lcom/pspdfkit/internal/h;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# static fields
.field public static final l:[I

.field public static final m:I

.field public static final n:I


# instance fields
.field private final b:Lcom/pspdfkit/internal/f;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private h:Landroidx/recyclerview/widget/RecyclerView;

.field private i:Lcom/pspdfkit/internal/e;

.field private j:Landroidx/recyclerview/widget/RecyclerView;

.field private k:Lcom/pspdfkit/internal/e;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/h;)Lcom/pspdfkit/internal/f;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/h;->b:Lcom/pspdfkit/internal/f;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__ActionMenu:[I

    sput-object v0, Lcom/pspdfkit/internal/h;->l:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__actionMenuStyle:I

    sput v0, Lcom/pspdfkit/internal/h;->m:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_ActionMenu:I

    sput v0, Lcom/pspdfkit/internal/h;->n:I

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/f;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/h;->a(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/h;->b:Lcom/pspdfkit/internal/f;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/h;->b()V

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/internal/h;->m:I

    sget v1, Lcom/pspdfkit/internal/h;->n:I

    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result p0

    return p0
.end method

.method private b()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__action_menu_layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/f;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 7
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__ActionMenu_pspdf__backgroundColor:I

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/pspdfkit/internal/h;->c:I

    .line 8
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__ActionMenu_pspdf__labelColor:I

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/pspdfkit/R$color;->pspdf__action_menu_label_color:I

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    .line 11
    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/pspdfkit/internal/h;->d:I

    .line 14
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__ActionMenu_pspdf__fixedActionsPanelBackgroundColor:I

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/pspdfkit/R$color;->pspdf__action_menu_fixed_items_background:I

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    .line 17
    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/pspdfkit/internal/h;->e:I

    .line 20
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__ActionMenu_pspdf__fixedActionsIconBackground:I

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Landroidx/appcompat/R$attr;->colorPrimary:I

    sget v7, Lcom/pspdfkit/R$color;->pspdf__color:I

    invoke-static {v5, v6, v7}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v5

    .line 23
    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/pspdfkit/internal/h;->f:I

    .line 26
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 27
    new-instance v1, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v4, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v4, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 29
    invoke-virtual {v0, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 30
    iget-object v4, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__share:I

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 33
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getCornerRadius()I

    move-result v1

    const/4 v4, 0x2

    add-int/2addr v1, v4

    .line 34
    iget v5, p0, Lcom/pspdfkit/internal/h;->c:I

    const/16 v6, 0x8

    new-array v6, v6, [F

    int-to-float v1, v1

    aput v1, v6, v2

    const/4 v7, 0x1

    aput v1, v6, v7

    aput v1, v6, v4

    const/4 v4, 0x3

    aput v1, v6, v4

    const/4 v1, 0x4

    const/4 v4, 0x0

    aput v4, v6, v1

    const/4 v1, 0x5

    aput v4, v6, v1

    const/4 v1, 0x6

    aput v4, v6, v1

    const/4 v1, 0x7

    aput v4, v6, v1

    invoke-static {v0, v5, v6}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;I[F)V

    .line 35
    new-instance v1, Lcom/pspdfkit/internal/e;

    .line 36
    new-instance v4, Lcom/pspdfkit/internal/g;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/g;-><init>(Lcom/pspdfkit/internal/h;)V

    .line 37
    iget v5, p0, Lcom/pspdfkit/internal/h;->f:I

    iget v6, p0, Lcom/pspdfkit/internal/h;->d:I

    invoke-direct {v1, v4, v5, v6}, Lcom/pspdfkit/internal/e;-><init>(Lcom/pspdfkit/internal/e$a;II)V

    iput-object v1, p0, Lcom/pspdfkit/internal/h;->i:Lcom/pspdfkit/internal/e;

    .line 38
    sget v4, Lcom/pspdfkit/R$id;->pspdf__fixed_menu_recycler_view:I

    .line 39
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroidx/recyclerview/widget/RecyclerView;

    .line 40
    invoke-virtual {v4, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 41
    new-instance v5, Lcom/pspdfkit/internal/views/utils/recyclerview/AutoSpanGridLayoutManager;

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    const/16 v9, 0x78

    invoke-static {v8, v9}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v8

    invoke-direct {v5, v6, v8}, Lcom/pspdfkit/internal/views/utils/recyclerview/AutoSpanGridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 43
    invoke-virtual {v4, v5}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 45
    invoke-virtual {v4, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 46
    iput-object v4, p0, Lcom/pspdfkit/internal/h;->h:Landroidx/recyclerview/widget/RecyclerView;

    .line 47
    iget v1, p0, Lcom/pspdfkit/internal/h;->e:I

    invoke-virtual {v4, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 48
    new-instance v1, Lcom/pspdfkit/internal/e;

    .line 49
    new-instance v4, Lcom/pspdfkit/internal/g;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/g;-><init>(Lcom/pspdfkit/internal/h;)V

    .line 50
    iget v5, p0, Lcom/pspdfkit/internal/h;->d:I

    invoke-direct {v1, v4, v2, v5}, Lcom/pspdfkit/internal/e;-><init>(Lcom/pspdfkit/internal/e$a;II)V

    iput-object v1, p0, Lcom/pspdfkit/internal/h;->k:Lcom/pspdfkit/internal/e;

    .line 51
    sget v4, Lcom/pspdfkit/R$id;->pspdf__standard_menu_recycler_view:I

    .line 52
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 53
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 54
    new-instance v2, Lcom/pspdfkit/internal/views/utils/recyclerview/AutoSpanGridLayoutManager;

    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v2, v4, v5}, Lcom/pspdfkit/internal/views/utils/recyclerview/AutoSpanGridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 56
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 58
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 59
    iput-object v0, p0, Lcom/pspdfkit/internal/h;->j:Landroidx/recyclerview/widget/RecyclerView;

    .line 60
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    invoke-virtual {p0, v7}, Landroid/widget/FrameLayout;->setFitsSystemWindows(Z)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getTitleHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/h;->g:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;",
            ">;)V"
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;

    .line 5
    invoke-virtual {v2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->getItemType()Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;->FIXED:Lcom/pspdfkit/ui/actionmenu/ActionMenuItem$MenuItemType;

    if-ne v3, v4, :cond_0

    .line 6
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/h;->i:Lcom/pspdfkit/internal/e;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/e;->a(Ljava/util/ArrayList;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/h;->h:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/h;->k:Lcom/pspdfkit/internal/e;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/e;->a(Ljava/util/ArrayList;)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/h;->j:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.class public Lcom/pspdfkit/internal/rk;
.super Lcom/pspdfkit/internal/v0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/cl$b;
.implements Lcom/pspdfkit/internal/cl$c;


# instance fields
.field private j:Lcom/pspdfkit/internal/cl;

.field private k:Lcom/pspdfkit/internal/uk;

.field private l:Lcom/pspdfkit/internal/yk;

.field private m:Lcom/pspdfkit/annotations/Annotation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/v0;-><init>()V

    return-void
.end method

.method private h()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/v0;->e:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    :cond_2
    move-object v5, v0

    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->d()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v8

    .line 15
    instance-of v0, v8, Lcom/pspdfkit/internal/qe;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/cf;

    iget-object v2, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v4

    check-cast v8, Lcom/pspdfkit/internal/qe;

    invoke-direct {v0, v3, v2, v4, v8}, Lcom/pspdfkit/internal/cf;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/internal/qe;)V

    goto :goto_1

    .line 21
    :cond_3
    new-instance v0, Lcom/pspdfkit/internal/tk;

    iget-object v4, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->c()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v6

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v7

    iget-object v9, p0, Lcom/pspdfkit/internal/v0;->f:Lcom/pspdfkit/internal/fl;

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v10

    move-object v2, v0

    invoke-direct/range {v2 .. v10}, Lcom/pspdfkit/internal/tk;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/internal/qf;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V

    .line 31
    :goto_1
    new-instance v2, Lcom/pspdfkit/internal/uk;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/uk;-><init>(Lcom/pspdfkit/internal/mk;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    .line 32
    invoke-virtual {v2}, Lcom/pspdfkit/internal/uk;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    if-eqz v0, :cond_4

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    iget-object v3, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    invoke-virtual {v2, v0, v3}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/cl;Lcom/pspdfkit/internal/pk;)V

    .line 34
    iput-object v1, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    :cond_4
    :goto_2
    return-void
.end method


# virtual methods
.method protected final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/rk;->h()V

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onAttach(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/rk;->h()V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/cl;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    .line 2
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/cl;->setOnDismissViewListener(Lcom/pspdfkit/internal/cl$b;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/cl;->setStatusBarColorCallback(Lcom/pspdfkit/internal/cl$c;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/cl;->setFragmentManager(Landroidx/fragment/app/FragmentManager;)V

    if-eqz p3, :cond_0

    const-string p1, "NoteEditorFragment.PresenterState"

    .line 7
    invoke-virtual {p3, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 8
    instance-of p2, p1, Lcom/pspdfkit/internal/yk;

    if-eqz p2, :cond_0

    .line 9
    check-cast p1, Lcom/pspdfkit/internal/yk;

    iput-object p1, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    .line 13
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    return-object p1
.end method

.method public final onDestroy()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->d()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    .line 4
    instance-of v1, v0, Lcom/pspdfkit/internal/qe;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/rk;->m:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v1, :cond_0

    .line 5
    check-cast v0, Lcom/pspdfkit/internal/qe;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/qe;->m(Lcom/pspdfkit/annotations/Annotation;)V

    .line 7
    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/v0;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/v0;->e()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->a()Lcom/pspdfkit/internal/yk;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    instance-of v1, v0, Lcom/pspdfkit/internal/yk;

    if-eqz v1, :cond_1

    const-string v1, "NoteEditorFragment.PresenterState"

    .line 10
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    :cond_1
    return-void
.end method

.method public final onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/v0;->onStart()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->j:Lcom/pspdfkit/internal/cl;

    if-eqz v0, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    iget-object v2, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/cl;Lcom/pspdfkit/internal/pk;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/rk;->h()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/v0;->onStop()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->a()Lcom/pspdfkit/internal/yk;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/rk;->l:Lcom/pspdfkit/internal/yk;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->i()V

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/rk;->k:Lcom/pspdfkit/internal/uk;

    :cond_0
    return-void
.end method

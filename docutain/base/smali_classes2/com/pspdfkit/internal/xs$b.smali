.class public final Lcom/pspdfkit/internal/xs$b;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/xs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/xs$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/xs$b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field final synthetic b:Lcom/pspdfkit/internal/xs;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/xs;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/xs$b;->b:Lcom/pspdfkit/internal/xs;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/xs$b;->a:Landroid/content/Context;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/xs$b;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/xs$b;->a:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Lcom/pspdfkit/internal/xs$b$a;
    .locals 4

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/xs$b$a;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/xs$b;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 4
    sget v2, Lcom/pspdfkit/R$layout;->pspdf__stamps_picker_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string v1, "from(context)\n          \u2026list_item, parent, false)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/xs$b$a;-><init>(Lcom/pspdfkit/internal/xs$b;Landroid/view/View;)V

    return-object v0
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b;->b:Lcom/pspdfkit/internal/xs;

    invoke-static {v0}, Lcom/pspdfkit/internal/xs;->b(Lcom/pspdfkit/internal/xs;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/xs$b$a;

    const-string v0, "holder"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b;->b:Lcom/pspdfkit/internal/xs;

    invoke-static {v0}, Lcom/pspdfkit/internal/xs;->b(Lcom/pspdfkit/internal/xs;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    return-void
.end method

.method public final bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/xs$b;->a(Landroid/view/ViewGroup;)Lcom/pspdfkit/internal/xs$b$a;

    move-result-object p1

    return-object p1
.end method

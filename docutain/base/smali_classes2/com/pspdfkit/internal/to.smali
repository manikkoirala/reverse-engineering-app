.class public final Lcom/pspdfkit/internal/to;
.super Lcom/pspdfkit/internal/bi;
.source "SourceFile"


# instance fields
.field private final n:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final o:Landroid/graphics/RectF;

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/bi;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/to;->o:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x16

    return v0
.end method

.method protected final a(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;
    .locals 2

    const-string v0, "selectedTextRects"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance p1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    iget v0, p0, Lcom/pspdfkit/internal/bi;->f:I

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/pspdfkit/annotations/RedactionAnnotation;-><init>(ILjava/util/List;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getOutlineColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOutlineColor(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getOverlayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOverlayText(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->n:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getRepeatOverlayText()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setRepeatOverlayText(Z)V

    return-object p1
.end method

.method protected final a(Landroid/graphics/RectF;)V
    .locals 3

    const-string v0, "touchRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/to;->o:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/to;->o:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    const-string v0, "pageLayout.getPdfToViewTransformation(null)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/to;->o:Landroid/graphics/RectF;

    .line 15
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 16
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    .line 17
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    iget v1, p0, Lcom/pspdfkit/internal/bi;->f:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/to;->j()Z

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/RectF;Z)Ljava/util/List;

    move-result-object p1

    const-string v0, "document.getPageTextRect\u2026, onlyIncludeFullWords())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/to;->p:Z

    return-void
.end method

.method protected final a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/ArrayList;Landroid/graphics/RectF;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTextRects"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedScreenRect"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-boolean v0, p0, Lcom/pspdfkit/internal/to;->p:Z

    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-lez p3, :cond_1

    .line 25
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/List;)V

    goto :goto_0

    .line 26
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p2

    const-string v0, "pageLayout.getPdfToViewTransformation(null)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 28
    invoke-virtual {v0, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 29
    invoke-static {v0, p2}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    .line 30
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 31
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->setRects(Ljava/util/List;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method protected final f()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-eqz v0, :cond_4

    const-string v1, "annotation"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x41a00000    # 20.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->getRects()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x3

    if-le v1, v3, :cond_1

    goto :goto_0

    .line 13
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->getRects()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    .line 14
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v2

    if-lez v3, :cond_2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_4

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->removeAnnotationFromPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 16
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 21
    :cond_4
    invoke-super {p0}, Lcom/pspdfkit/internal/bi;->f()V

    return-void
.end method

.method protected final g()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/to;->p:Z

    return v0
.end method

.method protected final i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

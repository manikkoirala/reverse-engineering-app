.class public final Lcom/pspdfkit/internal/fj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/fj$a;,
        Lcom/pspdfkit/internal/fj$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/fj$b;


# instance fields
.field private final a:I

.field private final b:Lcom/pspdfkit/internal/ph;

.field private final c:Lcom/pspdfkit/internal/y3;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/fj$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/fj$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/fj;->Companion:Lcom/pspdfkit/internal/fj$b;

    return-void
.end method

.method public synthetic constructor <init>(IILcom/pspdfkit/internal/ph;Lcom/pspdfkit/internal/y3;)V
    .locals 2
    .param p2    # I
        .annotation runtime Lkotlinx/serialization/Serializable;
            with = Lcom/pspdfkit/internal/r5;
        .end annotation
    .end param
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x7

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/fj$a;->a:Lcom/pspdfkit/internal/fj$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/pspdfkit/internal/fj;->a:I

    iput-object p3, p0, Lcom/pspdfkit/internal/fj;->b:Lcom/pspdfkit/internal/ph;

    iput-object p4, p0, Lcom/pspdfkit/internal/fj;->c:Lcom/pspdfkit/internal/y3;

    return-void
.end method

.method public constructor <init>(ILcom/pspdfkit/internal/ph;Lcom/pspdfkit/internal/y3;)V
    .locals 1

    const-string v0, "effects"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fontRef"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput p1, p0, Lcom/pspdfkit/internal/fj;->a:I

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/fj;->b:Lcom/pspdfkit/internal/ph;

    .line 7
    iput-object p3, p0, Lcom/pspdfkit/internal/fj;->c:Lcom/pspdfkit/internal/y3;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/fj;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/r5;->a:Lcom/pspdfkit/internal/r5;

    iget v1, p0, Lcom/pspdfkit/internal/fj;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/ph$a;->a:Lcom/pspdfkit/internal/ph$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/fj;->b:Lcom/pspdfkit/internal/ph;

    const/4 v2, 0x1

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/y3$a;->a:Lcom/pspdfkit/internal/y3$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/fj;->c:Lcom/pspdfkit/internal/y3;

    const/4 v1, 0x2

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/fj;->a:I

    return v0
.end method

.method public final b()Lcom/pspdfkit/internal/ph;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fj;->b:Lcom/pspdfkit/internal/ph;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/y3;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fj;->c:Lcom/pspdfkit/internal/y3;

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/hr$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/hr;->a(Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/graphics/Rect;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:F

.field final synthetic d:Landroid/graphics/Matrix;

.field final synthetic e:Lcom/pspdfkit/internal/hr;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/hr;Landroid/graphics/Rect;Ljava/util/ArrayList;FLandroid/graphics/Matrix;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    iput-object p2, p0, Lcom/pspdfkit/internal/hr$b;->a:Landroid/graphics/Rect;

    iput-object p3, p0, Lcom/pspdfkit/internal/hr$b;->b:Ljava/util/List;

    iput p4, p0, Lcom/pspdfkit/internal/hr$b;->c:F

    iput-object p5, p0, Lcom/pspdfkit/internal/hr$b;->d:Landroid/graphics/Matrix;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    const/16 v2, 0x800

    .line 3
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 5
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 6
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    int-to-float v0, v0

    div-float/2addr v3, v0

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    .line 7
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    .line 8
    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Lcom/pspdfkit/internal/di;->a(I)I

    move-result v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/2addr v1, v0

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/hr$b;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/2addr v2, v0

    .line 13
    monitor-enter p0

    .line 14
    :try_start_0
    iget-object v3, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v3}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgeth(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 15
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    .line 20
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v5, v1, :cond_1

    .line 21
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-eq v5, v2, :cond_0

    goto :goto_0

    .line 29
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 30
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    .line 31
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 32
    :cond_2
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 33
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 34
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 44
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 46
    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v1}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v1

    int-to-float v0, v0

    const/high16 v2, 0x3f800000    # 1.0f

    div-float/2addr v2, v0

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 48
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v0}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/hr$b;->a:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/hr$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/br;

    .line 52
    invoke-interface {v1}, Lcom/pspdfkit/internal/br;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v2

    sget-object v4, Lcom/pspdfkit/internal/br$a;->a:Lcom/pspdfkit/internal/br$a;

    if-eq v2, v4, :cond_4

    .line 53
    iget v2, p0, Lcom/pspdfkit/internal/hr$b;->c:F

    iget-object v4, p0, Lcom/pspdfkit/internal/hr$b;->d:Landroid/graphics/Matrix;

    invoke-interface {v1, v2, v4}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    .line 54
    iget-object v2, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v2}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v4

    invoke-static {v2}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetb(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-static {v2}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetc(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-interface {v1, v4, v5, v2}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 58
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/hr$b;->e:Lcom/pspdfkit/internal/hr;

    invoke-static {v0}, Lcom/pspdfkit/internal/hr;->-$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    return-object v3

    :catchall_0
    move-exception v0

    .line 59
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

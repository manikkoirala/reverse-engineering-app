.class public final Lcom/pspdfkit/internal/ya;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final c:Landroid/graphics/Paint;

.field private final d:Lcom/pspdfkit/internal/views/annotations/n;

.field private final e:Landroid/graphics/Matrix;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Path;

.field private h:Lcom/pspdfkit/internal/zf;

.field private i:I

.field private j:F

.field private k:Lcom/pspdfkit/internal/dm;

.field private l:Lcom/pspdfkit/internal/os;

.field private m:F

.field private n:F

.field private o:Z

.field private p:Z

.field private q:F

.field private r:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$JhyNZ2FqqMVKtirezr4pd-Nw3LI(Lcom/pspdfkit/internal/ya;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ya;->b(Lcom/pspdfkit/internal/ya;)V

    return-void
.end method

.method public static synthetic $r8$lambda$lskmSvCBSiff6G9VDd2QOGQRujY(Lcom/pspdfkit/internal/ya;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/ya;->c(Lcom/pspdfkit/internal/ya;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Landroid/graphics/Paint;Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eraserCirclePaint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extractedAnnotationsView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ya;->c:Landroid/graphics/Paint;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    .line 7
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    .line 8
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ya;->f:Landroid/graphics/Rect;

    .line 11
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/ya;)Lcom/pspdfkit/internal/views/annotations/n;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    return-object p0
.end method

.method private final a(FF)V
    .locals 7

    const/4 v0, 0x0

    .line 106
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ya;->o:Z

    const/4 v1, 0x1

    .line 107
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ya;->p:Z

    .line 108
    iput p1, p0, Lcom/pspdfkit/internal/ya;->m:F

    .line 109
    iput p2, p0, Lcom/pspdfkit/internal/ya;->n:F

    .line 110
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 111
    iget-object v3, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    mul-float v3, v3, v2

    const/4 v4, 0x3

    int-to-float v4, v4

    mul-float v4, v4, v2

    int-to-float v2, v1

    add-float/2addr v2, v4

    .line 115
    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 117
    iget v3, p0, Lcom/pspdfkit/internal/ya;->q:F

    cmpg-float v3, v2, v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    .line 118
    iput v2, p0, Lcom/pspdfkit/internal/ya;->q:F

    .line 121
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 122
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 123
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    iget v3, p0, Lcom/pspdfkit/internal/ya;->q:F

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    const/4 v6, 0x0

    invoke-virtual {v2, v6, v6, v3, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 124
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    .line 127
    iget v3, p0, Lcom/pspdfkit/internal/ya;->q:F

    sub-float/2addr v3, v4

    .line 128
    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 129
    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 138
    :cond_1
    iget v2, p0, Lcom/pspdfkit/internal/ya;->j:F

    const/high16 v3, 0x40400000    # 3.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 139
    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/views/annotations/n;->setForceHighQualityDrawing(Z)V

    .line 141
    :cond_2
    iget v1, p0, Lcom/pspdfkit/internal/ya;->j:F

    div-float/2addr p1, v1

    div-float/2addr p2, v1

    iget v2, p0, Lcom/pspdfkit/internal/ya;->q:F

    div-float/2addr v2, v1

    .line 142
    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/annotations/n;->getShapes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/b2;

    .line 143
    instance-of v4, v3, Lcom/pspdfkit/internal/ua;

    if-eqz v4, :cond_3

    .line 144
    check-cast v3, Lcom/pspdfkit/internal/ua;

    invoke-interface {v3, p1, p2, v2}, Lcom/pspdfkit/internal/ua;->a(FFF)Z

    move-result v3

    or-int/2addr v0, v3

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    .line 148
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/n;->c()V

    :cond_5
    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/ya;Ljava/util/List;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ya;->a(Ljava/util/List;)V

    return-void
.end method

.method private final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/pspdfkit/internal/ya$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/ya$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/ya;)V

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    :cond_0
    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/ya;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1

    .line 3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p0, v0, :cond_0

    .line 5
    sget-object p0, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result p0

    if-nez p0, :cond_0

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result p0

    if-nez p0, :cond_0

    .line 8
    sget-object p0, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 9
    sget-object p0, Lcom/pspdfkit/annotations/AnnotationFlags;->NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isReply()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final b(Lcom/pspdfkit/internal/ya;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iget-object p0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/ya;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm;->a(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->c()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object p0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->c()V

    .line 14
    iget-object p0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/dm;->a(ZLcom/pspdfkit/internal/im$c;)V

    :goto_0
    return-void
.end method

.method private final f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pspdfkit/internal/ya;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/n;->getAnnotations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/annotations/n;->getAnnotations()Ljava/util/List;

    move-result-object v1

    .line 14
    new-instance v2, Lcom/pspdfkit/internal/ya$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/ya$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ya;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/w1;->b(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    :cond_1
    return-void
.end method

.method private final i()V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/n;->getAnnotations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/annotations/n;->getAnnotations()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/annotations/n;->getShapes()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v4, :cond_6

    .line 7
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v7, :cond_5

    .line 8
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Lcom/pspdfkit/internal/ge;

    if-nez v7, :cond_1

    goto :goto_2

    .line 12
    :cond_1
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    const-string v8, "null cannot be cast to non-null type com.pspdfkit.annotations.InkAnnotation"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/pspdfkit/annotations/InkAnnotation;

    .line 13
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    const-string v9, "null cannot be cast to non-null type com.pspdfkit.internal.annotations.shapes.annotations.InkAnnotationShape"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/pspdfkit/internal/ge;

    .line 14
    invoke-virtual {v8}, Lcom/pspdfkit/internal/ge;->m()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 15
    iget-object v9, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    iget v10, p0, Lcom/pspdfkit/internal/ya;->j:F

    invoke-virtual {v8, v10, v9}, Lcom/pspdfkit/internal/ge;->b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object v8

    const-string v9, "shape.getLinesInPdfCoord\u2026sformation, currentScale)"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 17
    iget-object v8, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v8

    .line 18
    invoke-static {v7}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 19
    iget-object v8, p0, Lcom/pspdfkit/internal/ya;->h:Lcom/pspdfkit/internal/zf;

    if-eqz v8, :cond_4

    invoke-interface {v8}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-interface {v8, v7}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 21
    :cond_2
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-static {v9, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 23
    new-instance v9, Lcom/pspdfkit/internal/n1;

    .line 26
    invoke-virtual {v7}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v10

    const/16 v11, 0x64

    .line 27
    invoke-direct {v9, v7, v11, v10, v8}, Lcom/pspdfkit/internal/n1;-><init>(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V

    .line 28
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_3
    invoke-virtual {v7, v8}, Lcom/pspdfkit/annotations/InkAnnotation;->setLines(Ljava/util/List;)V

    .line 39
    :cond_4
    :goto_1
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 42
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/v5;

    invoke-direct {v1, v3}, Lcom/pspdfkit/internal/v5;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 45
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_8

    .line 46
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v2, v5, v1}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V

    :cond_8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x15

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ya;->p:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 102
    iget v0, p0, Lcom/pspdfkit/internal/ya;->m:F

    iget v1, p0, Lcom/pspdfkit/internal/ya;->n:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->g:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 104
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 2

    const-string v0, "drawMatrix"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/ya;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 95
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/ya;->j:F

    .line 96
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 99
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/pspdfkit/internal/ya;->j:F

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/views/annotations/n;->a(FLandroid/graphics/Matrix;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 3

    const-string v0, "specialModeView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_0

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/pspdfkit/internal/ya;->i:I

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/ya;->h:Lcom/pspdfkit/internal/zf;

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 17
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 19
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    iput v0, p0, Lcom/pspdfkit/internal/ya;->j:F

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 27
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 28
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->h:Lcom/pspdfkit/internal/zf;

    if-eqz p1, :cond_7

    .line 31
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    .line 32
    iget v0, p0, Lcom/pspdfkit/internal/ya;->i:I

    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 33
    sget-object v0, Lcom/pspdfkit/internal/va;->a:Lcom/pspdfkit/internal/va;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->flatMapIterable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 36
    new-instance v0, Lcom/pspdfkit/internal/wa;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/wa;-><init>(Lcom/pspdfkit/internal/ya;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 38
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 39
    new-instance v0, Lcom/pspdfkit/internal/xa;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/xa;-><init>(Lcom/pspdfkit/internal/ya;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v1

    .line 40
    :cond_7
    iput-object v1, p0, Lcom/pspdfkit/internal/ya;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_a

    const/4 v2, 0x0

    if-eq v0, v1, :cond_9

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 p1, 0x3

    if-eq v0, p1, :cond_9

    goto/16 :goto_2

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 44
    iget-object v3, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 45
    iget-object v3, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-static {p1, v3}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 52
    :cond_1
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ya;->o:Z

    if-eqz v3, :cond_4

    .line 53
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/ya;->a(FF)V

    goto/16 :goto_2

    .line 54
    :cond_2
    :goto_0
    iget-boolean v3, p0, Lcom/pspdfkit/internal/ya;->o:Z

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_3
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ya;->o:Z

    .line 64
    :cond_4
    iget v3, p0, Lcom/pspdfkit/internal/ya;->q:F

    iget-object v4, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/pspdfkit/internal/ya;->q:F

    sub-float/2addr v4, v5

    .line 65
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 66
    iget v3, p0, Lcom/pspdfkit/internal/ya;->q:F

    iget-object v4, p0, Lcom/pspdfkit/internal/ya;->k:Lcom/pspdfkit/internal/dm;

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/pspdfkit/internal/ya;->q:F

    sub-float/2addr v4, v5

    .line 67
    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v3, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 68
    iget v3, p0, Lcom/pspdfkit/internal/ya;->m:F

    sub-float v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 69
    iget v4, p0, Lcom/pspdfkit/internal/ya;->n:F

    sub-float v4, p1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 71
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ya;->o:Z

    if-nez v5, :cond_5

    const/high16 v5, 0x40800000    # 4.0f

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_5

    cmpl-float v3, v4, v5

    if-lez v3, :cond_b

    .line 72
    :cond_5
    iput v0, p0, Lcom/pspdfkit/internal/ya;->m:F

    .line 73
    iput p1, p0, Lcom/pspdfkit/internal/ya;->n:F

    .line 74
    iget v3, p0, Lcom/pspdfkit/internal/ya;->j:F

    div-float/2addr v0, v3

    div-float/2addr p1, v3

    iget v4, p0, Lcom/pspdfkit/internal/ya;->q:F

    div-float/2addr v4, v3

    .line 75
    iget-object v3, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/views/annotations/n;->getShapes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/internal/b2;

    .line 76
    instance-of v7, v6, Lcom/pspdfkit/internal/ua;

    if-eqz v7, :cond_6

    .line 77
    check-cast v6, Lcom/pspdfkit/internal/ua;

    invoke-interface {v6, v0, p1, v4}, Lcom/pspdfkit/internal/ua;->a(FFF)Z

    move-result v6

    or-int/2addr v5, v6

    goto :goto_1

    :cond_7
    if-eqz v5, :cond_8

    .line 81
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/annotations/n;->c()V

    .line 82
    :cond_8
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ya;->o:Z

    if-eqz p1, :cond_b

    .line 83
    iput-boolean v2, p0, Lcom/pspdfkit/internal/ya;->p:Z

    goto :goto_2

    .line 84
    :cond_9
    iput-boolean v2, p0, Lcom/pspdfkit/internal/ya;->p:Z

    .line 85
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/views/annotations/n;->setForceHighQualityDrawing(Z)V

    .line 86
    invoke-direct {p0}, Lcom/pspdfkit/internal/ya;->i()V

    goto :goto_2

    .line 87
    :cond_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/ya;->a(FF)V

    .line 93
    :cond_b
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    return v1
.end method

.method public final b()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ya;->i()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/os;->setPageModeHandlerViewHolder(Lcom/pspdfkit/internal/em;)V

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/ya;->f()V

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 2

    .line 15
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    const-string v1, "defaultVariant()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ya;->b()Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->ERASER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ya;->i()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->c()V

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/ya;->f()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ya;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ya;->i:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/pspdfkit/annotations/Annotation;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/n;->b([Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    :cond_0
    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 4

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/ya;->i:I

    if-ne v0, v1, :cond_1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 3
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isReply()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ya;->d:Lcom/pspdfkit/internal/views/annotations/n;

    new-array v1, v2, [Lcom/pspdfkit/annotations/Annotation;

    aput-object p1, v1, v3

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/n;->a([Lcom/pspdfkit/annotations/Annotation;)V

    .line 10
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ya;->a(Ljava/util/List;)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/ya;->l:Lcom/pspdfkit/internal/os;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    :cond_1
    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldOrder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newOrder"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

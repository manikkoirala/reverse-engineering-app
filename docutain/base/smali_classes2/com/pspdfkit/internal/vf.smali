.class public final Lcom/pspdfkit/internal/vf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

.field private final b:Lcom/pspdfkit/instant/client/InstantClient;

.field private final c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/pspdfkit/internal/wf;

.field private h:Lcom/pspdfkit/internal/ef;

.field private i:Lcom/pspdfkit/internal/re;

.field private j:Lcom/pspdfkit/internal/xe;

.field private k:Lio/reactivex/rxjava3/core/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/pspdfkit/internal/ze;


# direct methods
.method public static synthetic $r8$lambda$PRVPiTz3x0AbjEtB5vfvd1SIy7g(Lcom/pspdfkit/internal/vf;Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/vf;->b(Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$RPPmO-sU4l5b8W2XopXFTdFBa9E(Lcom/pspdfkit/internal/gf;Lcom/pspdfkit/internal/kf;Ljava/lang/Throwable;)Lorg/reactivestreams/Publisher;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/vf;->a(Lcom/pspdfkit/internal/gf;Lcom/pspdfkit/internal/kf;Ljava/lang/Throwable;)Lorg/reactivestreams/Publisher;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->a:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/vf;->b:Lcom/pspdfkit/instant/client/InstantClient;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 5
    invoke-virtual {p3}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getDocumentIdentifier()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->d:Ljava/lang/String;

    .line 6
    invoke-virtual {p3}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getLayerName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->e:Ljava/lang/String;

    .line 8
    new-instance p1, Lcom/pspdfkit/internal/ze;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ze;-><init>(Lcom/pspdfkit/internal/vf;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->l:Lcom/pspdfkit/internal/ze;

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/gf;Lcom/pspdfkit/internal/kf;Ljava/lang/Throwable;)Lorg/reactivestreams/Publisher;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 641
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/gf;->a(Lcom/pspdfkit/internal/kf;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p0

    return-object p0
.end method

.method private b(Ljava/lang/String;)Lcom/pspdfkit/instant/document/InstantPdfDocument;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->g:Lcom/pspdfkit/internal/wf;

    if-nez v0, :cond_3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->isDownloaded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getDocument()Lcom/pspdfkit/instant/internal/jni/NativeDocumentResult;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeDocumentResult;->isError()Z

    move-result v1

    if-nez v1, :cond_1

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeDocumentResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->getDocument()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 14
    new-instance v2, Lcom/pspdfkit/internal/ef;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/ef;-><init>(Lcom/pspdfkit/internal/vf;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/vf;->h:Lcom/pspdfkit/internal/ef;

    .line 17
    new-instance v2, Lcom/pspdfkit/internal/xe;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/xe;-><init>(Lcom/pspdfkit/internal/vf;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/vf;->j:Lcom/pspdfkit/internal/xe;

    .line 20
    iget-object v2, p0, Lcom/pspdfkit/internal/vf;->b:Lcom/pspdfkit/instant/client/InstantClient;

    iget-object v3, p0, Lcom/pspdfkit/internal/vf;->a:Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    .line 23
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeLayerDocumentContainer;->getLayerCapabilities()Ljava/util/EnumSet;

    move-result-object v0

    iget-object v4, p0, Lcom/pspdfkit/internal/vf;->j:Lcom/pspdfkit/internal/xe;

    .line 24
    invoke-static {v2, v3, v0, v4, v1}, Lcom/pspdfkit/internal/wf;->a(Lcom/pspdfkit/instant/client/InstantClient;Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;Ljava/util/EnumSet;Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/wf;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/vf;->g:Lcom/pspdfkit/internal/wf;

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/re;

    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->g:Lcom/pspdfkit/internal/wf;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/re;-><init>(Lcom/pspdfkit/internal/wf;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/vf;->i:Lcom/pspdfkit/internal/re;

    goto :goto_0

    .line 33
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Instant document could not be opened"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeDocumentResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1

    .line 35
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Document must be downloaded before opening!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 64
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->g:Lcom/pspdfkit/internal/wf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-eqz p1, :cond_4

    .line 65
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vf;->e(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V
    :try_end_1
    .catch Lcom/pspdfkit/instant/exceptions/InstantException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Instant"

    const-string v3, "Can\'t update authentication token"

    .line 69
    invoke-static {v2, p1, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_1
    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/pspdfkit/internal/re;
    .locals 2

    monitor-enter p0

    .line 642
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->i:Lcom/pspdfkit/internal/re;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-object v0

    .line 643
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getAnnotationSyncManager() must be called only after InstantPdfDocument has been opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "jwt"

    const-string v1, "argumentName"

    .line 230
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jwt"

    const/4 v1, 0x0

    .line 281
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 282
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->e:Ljava/lang/String;

    .line 283
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    .line 284
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->isDownloaded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/pspdfkit/instant/client/InstantProgress;

    .line 285
    sget-object v0, Lcom/pspdfkit/internal/gf;->e:Lcom/pspdfkit/instant/client/InstantProgress;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Flowable;->fromArray([Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 289
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    move-result-object p1

    .line 290
    new-instance v0, Lcom/pspdfkit/internal/gf;

    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/gf;-><init>(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V

    .line 292
    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->k:Lio/reactivex/rxjava3/core/Flowable;

    if-nez v1, :cond_1

    .line 294
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/gf;->a(Lcom/pspdfkit/internal/kf;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Flowable;->share()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->k:Lio/reactivex/rxjava3/core/Flowable;

    goto :goto_0

    .line 296
    :cond_1
    new-instance v2, Lcom/pspdfkit/internal/vf$$ExternalSyntheticLambda1;

    invoke-direct {v2, v0, p1}, Lcom/pspdfkit/internal/vf$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/gf;Lcom/pspdfkit/internal/kf;)V

    .line 297
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Flowable;->onErrorResumeNext(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    .line 298
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Flowable;->share()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->k:Lio/reactivex/rxjava3/core/Flowable;
    :try_end_1
    .catch Lcom/pspdfkit/instant/exceptions/InstantException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 303
    :goto_0
    :try_start_2
    iget-object p1, p0, Lcom/pspdfkit/internal/vf;->k:Lio/reactivex/rxjava3/core/Flowable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    .line 304
    :try_start_3
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Flowable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/instant/exceptions/InstantException;
        }
    .end annotation

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 305
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 356
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 357
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 358
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    .line 359
    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->commentsForAnnotation(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;

    move-result-object p1

    const-string v0, "commentThreadResult"

    .line 360
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 411
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 412
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->isError()Z

    move-result v0

    if-nez v0, :cond_1

    .line 416
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->value()Ljava/util/ArrayList;

    move-result-object p1

    const-string v0, "rawThread"

    .line 417
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 468
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 469
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 470
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/internal/jni/NativeComment;

    .line 471
    new-instance v2, Lcom/pspdfkit/internal/bf;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/bf;-><init>(Lcom/pspdfkit/instant/internal/jni/NativeComment;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0

    .line 472
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 3

    const-string v0, "comment"

    const-string v1, "argumentName"

    .line 473
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 524
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 525
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 526
    invoke-virtual {p1}, Lcom/pspdfkit/internal/bf;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p2

    .line 527
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->removeCommentWithId(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;

    move-result-object p1

    const-string p2, "commentThreadResult"

    .line 528
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 579
    invoke-static {p1, p2, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 580
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->isError()Z

    move-result p2

    if-nez p2, :cond_1

    .line 584
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->value()Ljava/util/ArrayList;

    move-result-object p1

    const-string p2, "rawThread"

    .line 585
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 636
    invoke-static {p1, p2, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 637
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 638
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/instant/internal/jni/NativeComment;

    .line 639
    new-instance v1, Lcom/pspdfkit/internal/bf;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/bf;-><init>(Lcom/pspdfkit/instant/internal/jni/NativeComment;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p2

    .line 640
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentThreadResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 3

    const-string v0, "contentText"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "author"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "annotation"

    .line 105
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 157
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 162
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p3

    invoke-interface {p3}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p3

    .line 163
    invoke-virtual {v0, p1, p2, v2, p3}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->createComment(Ljava/lang/String;Ljava/lang/String;[BLcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertionResult;

    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertionResult;->isError()Z

    move-result p2

    if-nez p2, :cond_1

    .line 173
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertionResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertion;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertion;->getUpdatedThread()Ljava/util/ArrayList;

    move-result-object p1

    const-string p2, "rawThread"

    .line 174
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-static {p1, p2, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 226
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 227
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/instant/internal/jni/NativeComment;

    .line 228
    new-instance v0, Lcom/pspdfkit/internal/bf;

    invoke-direct {v0, p3}, Lcom/pspdfkit/internal/bf;-><init>(Lcom/pspdfkit/instant/internal/jni/NativeComment;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p2

    .line 229
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeCommentInsertionResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
.end method

.method public final declared-synchronized b()Lcom/pspdfkit/internal/xe;
    .locals 2

    monitor-enter p0

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->j:Lcom/pspdfkit/internal/xe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-object v0

    .line 71
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getAssetProvider() must be called only after InstantPdfDocument has been opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 73
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 124
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 125
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 128
    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->softDeleteCommentRootWithoutChildren(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final c(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/instant/document/InstantPdfDocument;",
            ">;"
        }
    .end annotation

    const-string v0, "jwt"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->e:Ljava/lang/String;

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->isDownloaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vf;->d(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 58
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vf;->a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vf;->d(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getCreatorName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized d()Lcom/pspdfkit/internal/ef;
    .locals 2

    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->h:Lcom/pspdfkit/internal/ef;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-object v0

    .line 3
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getDocumentDelegate must be called only after InstantPdfDocument has been opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/instant/document/InstantPdfDocument;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/vf$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/vf$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/vf;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final e(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "jwt"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/pspdfkit/internal/vf;->e:Ljava/lang/String;

    .line 54
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    .line 55
    iput-object p1, p0, Lcom/pspdfkit/internal/vf;->f:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->l:Lcom/pspdfkit/internal/ze;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ze;->a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized f()Lcom/pspdfkit/internal/wf;
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->g:Lcom/pspdfkit/internal/wf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Lcom/pspdfkit/instant/document/InstantDocumentState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getCurrentState()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;

    move-result-object v0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerState;)Lcom/pspdfkit/instant/document/InstantDocumentState;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->isDownloaded()Z

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->invalidate()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vf;->c:Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->removeLayerStorage()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    return-void
.end method

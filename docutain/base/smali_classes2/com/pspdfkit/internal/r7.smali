.class public final Lcom/pspdfkit/internal/r7;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/r7$b;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/r7$b;

.field private c:Lcom/pspdfkit/annotations/StampAnnotation;

.field private d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/ImageView;

.field private g:I

.field private h:Lcom/pspdfkit/internal/views/picker/a;

.field private i:Landroid/widget/Switch;

.field private j:Landroid/widget/Switch;

.field private k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;


# direct methods
.method public static synthetic $r8$lambda$AOmJ4RHtpgvD9G8ffdQtwbjV1zY(Lcom/pspdfkit/internal/r7;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/r7;->a(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$IT9sddYgkrMmimC8rzl300MLfAA(Lcom/pspdfkit/internal/r7;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r7;->b(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$MGVh0AdNjbIivpnnNWWwFWU9V_Y(Lcom/pspdfkit/internal/r7;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r7;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$OpddCTiqpSC7Bm9gDJZ98CLiKA0(Lcom/pspdfkit/internal/r7;Landroid/widget/EditText;Lio/reactivex/rxjava3/core/ObservableEmitter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r7;->a(Landroid/widget/EditText;Lio/reactivex/rxjava3/core/ObservableEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$R2pHNKAHuPZBAbZK2Ey1YS34umE(Lcom/pspdfkit/internal/r7;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Vsuhhw9BM-SuVk1Va2eO4ku5K8I(Lcom/pspdfkit/internal/r7;Lcom/pspdfkit/internal/views/picker/a;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r7;->a(Lcom/pspdfkit/internal/views/picker/a;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$ZhiFk80eMT--j7YubOCjOLR0Tc8(Lcom/pspdfkit/internal/r7;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r7;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$n-OgucWG-dzPV3zRqNCJJ9jMR5E(Lcom/pspdfkit/internal/r7;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r7;->a(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/r7$b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/r7;->b:Lcom/pspdfkit/internal/r7$b;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->a()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/pspdfkit/internal/vs;
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v1, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    const/4 p3, 0x0

    .line 174
    invoke-virtual {v0, p3}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->createStampAnnotation(I)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    .line 176
    :cond_2
    iget-object p3, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p3, p1}, Lcom/pspdfkit/annotations/StampAnnotation;->setTitle(Ljava/lang/String;)V

    .line 177
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/StampAnnotation;->setSubtitle(Ljava/lang/String;)V

    .line 178
    new-instance p1, Lcom/pspdfkit/internal/vs;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object p3, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-direct {p1, p2, p3}, Lcom/pspdfkit/internal/vs;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/StampAnnotation;)V

    .line 181
    iget-object p2, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p2

    .line 182
    invoke-virtual {p2}, Landroid/graphics/RectF;->sort()V

    .line 183
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-static {p3, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p3

    float-to-int p3, p3

    .line 184
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    invoke-static {v0, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    float-to-int p2, p2

    .line 185
    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/internal/vs;->a(II)V

    return-object p1
.end method

.method private a(Landroid/widget/EditText;)Lio/reactivex/rxjava3/core/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/EditText;",
            ")",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/r7;Landroid/widget/EditText;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->create(Lio/reactivex/rxjava3/core/ObservableOnSubscribe;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/zs;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__backgroundColor:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/r7;->g:I

    .line 3
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__textColor:I

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_gray_dark:I

    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 6
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 9
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__hintColor:I

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 12
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 15
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__acceptCustomStampIconColor:I

    .line 16
    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 17
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__acceptCustomStampIconBackgroundColor:I

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Landroidx/appcompat/R$attr;->colorAccent:I

    invoke-static {v6, v7}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result v6

    .line 20
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__acceptCustomStampIcon:I

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    .line 22
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 23
    invoke-static {v6, v7}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-nez v6, :cond_0

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-static {v6, v7, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_0

    .line 30
    :cond_0
    invoke-static {v6}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 31
    invoke-static {v6, v4}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    move-object v4, v6

    .line 32
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 33
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    iget v0, p0, Lcom/pspdfkit/internal/r7;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__custom_stamp_creator_layout:I

    const/4 v6, 0x1

    invoke-virtual {v0, v2, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 38
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_image:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->f:Landroid/widget/ImageView;

    .line 40
    invoke-direct {p0, v1, v3}, Lcom/pspdfkit/internal/r7;->a(II)V

    .line 42
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_linear_container:I

    .line 43
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 44
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/r7;->a(Landroid/widget/LinearLayout;)V

    .line 46
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/r7;->a(I)V

    .line 47
    invoke-direct {p0, v4, v5}, Lcom/pspdfkit/internal/r7;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Z)V

    .line 50
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->c()V

    return-void
.end method

.method private a(I)V
    .locals 9

    .line 57
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_color_date_switch:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    const/4 v1, 0x1

    .line 58
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    new-instance v2, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/r7;)V

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    .line 61
    new-instance v2, Landroid/content/res/ColorStateList;

    const/4 v3, 0x2

    new-array v4, v3, [[I

    new-array v5, v1, [I

    const/4 v6, 0x0

    const v7, 0x101009e

    aput v7, v5, v6

    aput-object v5, v4, v6

    sget-object v5, Landroid/widget/RelativeLayout;->EMPTY_STATE_SET:[I

    aput-object v5, v4, v1

    new-array v8, v3, [I

    aput p1, v8, v6

    aput p1, v8, v1

    invoke-direct {v2, v4, v8}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 64
    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 65
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_color_time_switch:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    .line 66
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 67
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    new-instance v2, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/r7;)V

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    .line 69
    new-instance v2, Landroid/content/res/ColorStateList;

    new-array v4, v3, [[I

    new-array v8, v1, [I

    aput v7, v8, v6

    aput-object v8, v4, v6

    aput-object v5, v4, v1

    new-array v3, v3, [I

    aput p1, v3, v6

    aput p1, v3, v1

    invoke-direct {v2, v4, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 72
    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method private a(II)V
    .locals 4

    .line 99
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_text:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    .line 102
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/r7;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/r7;->a(Landroid/widget/EditText;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 112
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/r7;)V

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc8

    .line 122
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/core/Observable;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/r7;)V

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 125
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 126
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHintTextColor(I)V

    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .line 51
    sget v0, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_floating_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 52
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 53
    iget-object p2, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p2, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance p2, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda6;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/r7;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 1

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->b:Lcom/pspdfkit/internal/r7$b;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->b:Lcom/pspdfkit/internal/r7$b;

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->mutate()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/r7$b;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    :cond_0
    return-void
.end method

.method private synthetic a(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->c()V

    return-void
.end method

.method private synthetic a(Landroid/widget/EditText;Lio/reactivex/rxjava3/core/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 133
    new-instance v0, Lcom/pspdfkit/internal/r7$a;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/r7$a;-><init>(Lio/reactivex/rxjava3/core/ObservableEmitter;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private a(Landroid/widget/LinearLayout;)V
    .locals 5

    .line 74
    new-instance v0, Lcom/pspdfkit/internal/views/picker/a;

    .line 75
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/ao;->h:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/views/picker/a;-><init>(Landroid/content/Context;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    .line 76
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_stamp_creator_dialog_color_picker:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 78
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTextColor()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTextColor()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/picker/a;->b(I)Z

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/picker/a;->setShowSelectionIndicator(Z)V

    .line 83
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    new-instance v2, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/r7$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/r7;)V

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/picker/a;->setOnColorPickedListener(Lcom/pspdfkit/internal/views/picker/a$a;)V

    .line 89
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 91
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0x10

    invoke-static {v2, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    .line 92
    invoke-virtual {v0, v2, v2, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 93
    iget-object v2, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/views/picker/a;I)V
    .locals 0

    .line 96
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    if-nez p1, :cond_0

    return-void

    .line 97
    :cond_0
    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 98
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getSubtitle()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 131
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    if-nez v0, :cond_0

    goto :goto_0

    .line 162
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->CUSTOM:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    invoke-static {v0, v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->fromPredefinedType(Landroid/content/Context;Lcom/pspdfkit/annotations/stamps/PredefinedStampType;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object v0

    .line 163
    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withTitle(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object v0

    .line 164
    invoke-virtual {v0, p2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withSubtitle(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 165
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfWidth()F

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withSize(FF)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->c:Lcom/pspdfkit/annotations/StampAnnotation;

    .line 166
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withTextColor(Ljava/lang/Integer;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->build()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 168
    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/pspdfkit/internal/vs;

    move-result-object p1

    .line 170
    iget-object p2, p0, Lcom/pspdfkit/internal/r7;->f:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .line 138
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const-wide/16 v0, 0x64

    if-eqz p1, :cond_2

    .line 139
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    const/16 v2, 0x8

    if-ne p1, v2, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 143
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 144
    new-instance p2, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x1

    invoke-direct {p2, p1, v2, v0, v1}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {p2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 145
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 146
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    .line 148
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 149
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    goto :goto_0

    .line 150
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    .line 154
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 155
    new-instance p2, Lcom/pspdfkit/internal/mq;

    const/4 v2, 0x2

    invoke-direct {p2, p1, v2, v0, v1}, Lcom/pspdfkit/internal/mq;-><init>(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;IJ)V

    invoke-static {p2}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 156
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 157
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    .line 159
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 160
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->k:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private synthetic a(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 127
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 128
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    const/4 p1, 0x1

    return p1
.end method

.method private synthetic b(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->c()V

    return-void
.end method

.method private synthetic b(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private c()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->getDate()Ljava/lang/String;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 6
    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/pspdfkit/internal/vs;

    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/r7;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method private getDate()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/rh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/rh;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 13
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/rh;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 3

    if-eqz p2, :cond_0

    .line 134
    iget p1, p0, Lcom/pspdfkit/internal/r7;->g:I

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 137
    :cond_0
    iget p2, p0, Lcom/pspdfkit/internal/r7;->g:I

    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v2, v0, v1

    int-to-float p1, p1

    const/4 v1, 0x4

    aput p1, v0, v1

    const/4 v1, 0x5

    aput p1, v0, v1

    const/4 v1, 0x6

    aput p1, v0, v1

    const/4 v1, 0x7

    aput p1, v0, v1

    invoke-static {p0, p2, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;I[F)V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    .line 5
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)V

    return-void
.end method

.method public getCustomStamp()Lcom/pspdfkit/annotations/stamps/StampPickerItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    return-object v0
.end method

.method public getDateSwitchState()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    return v0
.end method

.method public getTimeSwitchState()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setCustomStamp(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTextColor()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->h:Lcom/pspdfkit/internal/views/picker/a;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTextColor()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/picker/a;->b(I)Z

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getSubtitle()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/pspdfkit/internal/vs;

    move-result-object p1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/r7;->c()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->d:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 11
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {p1, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {p1, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/r7;->e:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/r7;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public setDateSwitchState(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->i:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    return-void
.end method

.method public setTimeSwitchState(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r7;->j:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    return-void
.end method

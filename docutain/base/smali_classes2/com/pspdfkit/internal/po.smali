.class public final Lcom/pspdfkit/internal/po;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/po$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/po$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/hl;

.field private c:I

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:I

.field private g:I


# direct methods
.method public static synthetic $r8$lambda$EK4lTrgcymIeUsoq6AHs8gPoMGc(Lcom/pspdfkit/internal/po;ILandroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/po;->a(Lcom/pspdfkit/internal/po;ILandroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/pspdfkit/internal/hl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;",
            "Lcom/pspdfkit/internal/hl;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fonts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onFontSelectionListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/internal/po;->b:Lcom/pspdfkit/internal/hl;

    .line 6
    sget p2, Lcom/pspdfkit/R$string;->pspdf__signature:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "context.getString(R.string.pspdf__signature)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/po;->d:Ljava/lang/String;

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/internal/po;->e:Ljava/lang/String;

    .line 8
    sget p2, Lcom/pspdfkit/R$color;->pspdf__electronic_signature_font_select_bg_color:I

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/po;->f:I

    .line 11
    sget p2, Lcom/pspdfkit/R$color;->pspdf__color_electronic_signature_drawing_primary:I

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/po;->g:I

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/po;ILandroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget p2, p0, Lcom/pspdfkit/internal/po;->c:I

    if-eq p1, p2, :cond_0

    .line 24
    iput p1, p0, Lcom/pspdfkit/internal/po;->c:I

    .line 25
    invoke-virtual {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 26
    iget p1, p0, Lcom/pspdfkit/internal/po;->c:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/po;->b:Lcom/pspdfkit/internal/hl;

    iget-object p2, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    iget v0, p0, Lcom/pspdfkit/internal/po;->c:I

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    const-string v0, "fonts[selectedFontIndex]"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/ui/fonts/Font;

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/hl;->a(Lcom/pspdfkit/ui/fonts/Font;)V

    .line 28
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .line 29
    iput p1, p0, Lcom/pspdfkit/internal/po;->g:I

    .line 30
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/po$a;I)V
    .locals 5

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/po;->c:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "fonts[position]"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/ui/fonts/Font;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->c()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->c()Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/po;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->c()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/pspdfkit/internal/po;->g:I

    goto :goto_1

    :cond_1
    iget v2, p0, Lcom/pspdfkit/internal/po;->f:I

    iget v3, p0, Lcom/pspdfkit/internal/po;->g:I

    const v4, 0x3e99999a    # 0.3f

    invoke-static {v2, v3, v4}, Landroidx/core/graphics/ColorUtils;->blendARGB(IIF)I

    move-result v2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->c()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->b()Landroid/widget/RadioButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/po$a;->a()Landroid/widget/LinearLayout;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/po$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/po$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/po;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/po;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_1

    .line 12
    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    .line 15
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/po;->d:Ljava/lang/String;

    .line 16
    :goto_1
    iput-object p1, p0, Lcom/pspdfkit/internal/po;->e:Ljava/lang/String;

    .line 21
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public final b(I)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/po;->c:I

    goto :goto_3

    .line 3
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    .line 34
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 35
    check-cast v4, Lcom/pspdfkit/ui/fonts/Font;

    .line 36
    invoke-virtual {v4}, Lcom/pspdfkit/ui/fonts/Font;->hashCode()I

    move-result v4

    if-ne v4, p1, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, -0x1

    :goto_2
    if-eq v3, v1, :cond_4

    .line 37
    iget p1, p0, Lcom/pspdfkit/internal/po;->c:I

    if-eq v3, p1, :cond_4

    .line 39
    iput v3, p0, Lcom/pspdfkit/internal/po;->c:I

    .line 40
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 41
    iget p1, p0, Lcom/pspdfkit/internal/po;->c:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 42
    iget-object p1, p0, Lcom/pspdfkit/internal/po;->b:Lcom/pspdfkit/internal/hl;

    iget-object v0, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/po;->c:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "fonts[selectedFontIndex]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/hl;->a(Lcom/pspdfkit/ui/fonts/Font;)V

    .line 43
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 44
    :cond_4
    :goto_3
    iget p1, p0, Lcom/pspdfkit/internal/po;->c:I

    return p1
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/po;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/po$a;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/po;->a(Lcom/pspdfkit/internal/po$a;I)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const-string p2, "parent"

    .line 1
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 69
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__electronic_signature_typing_font_view_holder:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 70
    new-instance p2, Lcom/pspdfkit/internal/po$a;

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/po$a;-><init>(Landroid/view/View;)V

    return-object p2
.end method

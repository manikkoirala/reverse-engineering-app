.class public final Lcom/pspdfkit/internal/i0$a;
.super Lcom/pspdfkit/internal/yg;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/i0;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/annotations/AnnotationType;


# direct methods
.method constructor <init>(Lcom/pspdfkit/annotations/AnnotationType;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/i0$a;->a:Lcom/pspdfkit/annotations/AnnotationType;

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i0$a;->a:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$-CC;->builder(Landroid/content/Context;Lcom/pspdfkit/annotations/AnnotationType;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;->build()Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p1

    const-string v0, "builder(context, it).build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

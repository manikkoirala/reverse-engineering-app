.class public final Lcom/pspdfkit/internal/w9$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/w9;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field final a:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

.field final b:I

.field final c:I

.field private d:Landroid/content/Context;


# direct methods
.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/w9$a;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/w9$a;->d:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/w9$a;->a:Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/w9$a;->b:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/w9$a;->c:I

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w9$a;->d:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/w9$a;->b:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/li;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/li$a;
    }
.end annotation


# direct methods
.method public static final a(Lcom/pspdfkit/internal/ri;FF)Lcom/pspdfkit/internal/mi;
    .locals 3

    const-string v0, "measurementProperties"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    float-to-double v0, p1

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    float-to-double p1, p2

    .line 31
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object v2

    .line 33
    invoke-static {v0, v1, p1, p2, v2}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalculator;->getMeasurementCircularArea(DDLcom/pspdfkit/internal/jni/NativeMeasurementScale;)D

    move-result-wide p1

    .line 39
    new-instance v0, Lcom/pspdfkit/internal/mi;

    .line 40
    sget v1, Lcom/pspdfkit/internal/jc;->f:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v1

    double-to-float p1, p1

    invoke-static {p0}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p1, p0}, Lcom/pspdfkit/internal/jc$a;->a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;FLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 41
    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/mi;-><init>(Ljava/lang/String;F)V

    return-object v0
.end method

.method public static final a(Lcom/pspdfkit/internal/ri;Ljava/util/List;)Lcom/pspdfkit/internal/mi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/ri;",
            "Ljava/util/List<",
            "+",
            "Landroid/graphics/PointF;",
            ">;)",
            "Lcom/pspdfkit/internal/mi;"
        }
    .end annotation

    const-string v0, "measurementProperties"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfPoints"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "<this>"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object p1

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->a()Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/li$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 20
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalculator;->getMeasurementArea(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeMeasurementScale;)D

    move-result-wide v0

    goto :goto_0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 21
    :cond_2
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalculator;->getMeasurementDistance(Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeMeasurementScale;)D

    move-result-wide v0

    .line 25
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result p1

    if-eqz p1, :cond_3

    const-wide/16 v0, 0x0

    .line 27
    :cond_3
    new-instance p1, Lcom/pspdfkit/internal/mi;

    .line 28
    sget v2, Lcom/pspdfkit/internal/jc;->f:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    double-to-float v0, v0

    invoke-static {p0}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v0, p0}, Lcom/pspdfkit/internal/jc$a;->a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;FLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 29
    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/mi;-><init>(Ljava/lang/String;F)V

    return-object p1
.end method

.method public static final a(FLcom/pspdfkit/internal/ri;)Ljava/lang/String;
    .locals 4

    const-string v0, "measurementProperties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    float-to-double v0, p0

    const/16 p0, 0xa

    int-to-double v2, p0

    mul-double v0, v0, v2

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int p0, v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    .line 43
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-lez v0, :cond_0

    const-string v1, ". "

    goto :goto_0

    :cond_0
    const-string v1, " "

    :goto_0
    add-int/2addr p0, v0

    const-string v0, "0"

    .line 45
    invoke-static {v0, p0}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/ri;)Ljava/lang/String;
    .locals 2

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->a()Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/li$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p0

    iget-object p0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sq "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p0

    iget-object p0, p0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "measurementProperties.scale.unitTo.toString()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static final a(Ljava/lang/String;Lcom/pspdfkit/internal/ri;)Ljava/lang/String;
    .locals 7

    const-string v0, "valueString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "measurementProperties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v2, v0, [C

    .line 46
    fill-array-data v2, :array_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->indexOfAny$default(Ljava/lang/CharSequence;[CIZILjava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 49
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unable to work out the largest width string based on input "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "MEASUREMENTS"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p0

    .line 53
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    int-to-float p0, v1

    .line 54
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/li;->a(FLcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :array_0
    .array-data 2
        0x2es
        0x20s
    .end array-data
.end method

.class public final Lcom/pspdfkit/internal/b0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;


# instance fields
.field private a:Landroid/content/ClipboardManager;

.field private b:Lcom/pspdfkit/internal/c0;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public static synthetic $r8$lambda$d_vI108BEU5TT0Wn_iY3nD_D_Yk(Lcom/pspdfkit/internal/b0;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/b0;->d()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b0;->d:Z

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/b0;->a()Landroid/content/ClipboardManager;

    return-void
.end method

.method private a()Landroid/content/ClipboardManager;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->a:Landroid/content/ClipboardManager;

    if-nez v0, :cond_0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/b0$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/b0$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/b0;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->a:Landroid/content/ClipboardManager;

    return-object v0
.end method

.method public static a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v1, :cond_1

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private synthetic d()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    monitor-exit p0

    return-void

    :cond_0
    const-string v1, "clipboard"

    .line 5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    if-nez v0, :cond_1

    .line 6
    monitor-exit p0

    return-void

    .line 7
    :cond_1
    iput-object v0, p0, Lcom/pspdfkit/internal/b0;->a:Landroid/content/ClipboardManager;

    .line 8
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    invoke-virtual {v0, p0}, Landroid/content/ClipboardManager;->addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V

    return-void

    :catchall_0
    move-exception v0

    .line 11
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
    .locals 5

    const-string v0, "PSPDFKit.Clipboard"

    .line 48
    iget-boolean v1, p0, Lcom/pspdfkit/internal/b0;->d:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 50
    iput-boolean v1, p0, Lcom/pspdfkit/internal/b0;->d:Z

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/internal/b0;->a()Landroid/content/ClipboardManager;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "Got runtime exception when reading clipboard. Probably too much data on the clipboard."

    .line 63
    invoke-static {v0, v3, v4, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "Got security exception when reading clipboard."

    .line 64
    invoke-static {v0, v3, v4, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_1

    .line 65
    iget-object v1, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    .line 66
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/c0;->a(Landroid/content/ClipData;Lcom/pspdfkit/internal/c0;)Lcom/pspdfkit/internal/c0;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 68
    iput-object v0, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    .line 70
    iput-object v2, p0, Lcom/pspdfkit/internal/b0;->c:Ljava/lang/String;

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    if-eqz v0, :cond_2

    .line 76
    invoke-virtual {v0}, Lcom/pspdfkit/internal/c0;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 78
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getCopy()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 81
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setModifiedDate(Ljava/util/Date;)V

    .line 82
    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->setCreator(Ljava/lang/String;)V

    return-object v0

    :cond_2
    return-object v2
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Z
    .locals 1

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getCopy()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    const/4 v0, 0x0

    .line 32
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b0;->d:Z

    if-nez p1, :cond_0

    return v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {v0}, Lcom/pspdfkit/internal/c0;->c()V

    .line 43
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/c0;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/c0;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    .line 44
    iput-object p2, p0, Lcom/pspdfkit/internal/b0;->c:Ljava/lang/String;

    .line 45
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object p1

    sget-object p2, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->ANNOTATION_COPY_PASTE_SYSTEM_INTEGRATION:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 46
    invoke-virtual {p1, p2}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 47
    iget-object p1, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/c0;->d()V

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c0;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/b0;->d:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/b0;->b:Lcom/pspdfkit/internal/c0;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b0;->d:Z

    return-void
.end method

.method public final onPrimaryClipChanged()V
    .locals 2

    .line 1
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;->ANNOTATION_COPY_PASTE_SYSTEM_INTEGRATION:Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;

    .line 3
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/policy/ApplicationPolicy;->hasPermissionForEvent(Lcom/pspdfkit/configuration/policy/ApplicationPolicy$PolicyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/b0;->a()Landroid/content/ClipboardManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 5
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, "text/plain"

    .line 10
    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    :cond_4
    :goto_0
    const/4 v0, 0x0

    .line 11
    :goto_1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b0;->d:Z

    return-void
.end method

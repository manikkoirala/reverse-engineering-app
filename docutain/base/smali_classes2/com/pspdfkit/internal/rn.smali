.class public final Lcom/pspdfkit/internal/rn;
.super Lcom/pspdfkit/internal/f4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/f4<",
        "Lcom/pspdfkit/internal/sn;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/sn;

    invoke-direct {v0}, Lcom/pspdfkit/internal/sn;-><init>()V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/f4;-><init>(Lcom/pspdfkit/internal/sn;)V

    return-void
.end method

.method public constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIFF",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)V"
        }
    .end annotation

    .line 2
    new-instance v7, Lcom/pspdfkit/internal/sn;

    move-object v0, v7

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/sn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V

    invoke-direct {p0, v7}, Lcom/pspdfkit/internal/f4;-><init>(Lcom/pspdfkit/internal/sn;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/sn;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/f4;-><init>(Lcom/pspdfkit/internal/sn;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/graphics/Matrix;F)Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    invoke-virtual {p0, p3, p2}, Lcom/pspdfkit/internal/c4;->b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object p2

    .line 2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    const/4 v0, 0x2

    if-ge p3, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 4
    :cond_0
    new-instance p3, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-direct {p3, p1, p2}, Lcom/pspdfkit/annotations/PolylineAnnotation;-><init>(ILjava/util/List;)V

    .line 5
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/f4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-object p1, p3

    :goto_0
    return-object p1
.end method

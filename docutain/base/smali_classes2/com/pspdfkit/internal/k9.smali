.class final Lcom/pspdfkit/internal/k9;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/k9$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/pspdfkit/internal/jni/NativeJSResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/n9;

.field final synthetic b:Lcom/pspdfkit/forms/FormElement;

.field final synthetic c:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/n9;Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    iput-object p2, p0, Lcom/pspdfkit/internal/k9;->b:Lcom/pspdfkit/forms/FormElement;

    iput-object p3, p0, Lcom/pspdfkit/internal/k9;->c:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/jni/NativeJSResult;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;

    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->a(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/k9;->b:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v2}, Lcom/pspdfkit/forms/FormElement;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentProvider;Ljava/lang/String;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->c:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    sget-object v2, Lcom/pspdfkit/internal/k9$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const-string v2, "{\n                    na\u2026etInfo)\n                }"

    packed-switch v1, :pswitch_data_0

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/k9;->c:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JavaScript execution for event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " is not supported"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.JavaScript"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    new-instance v0, Lcom/pspdfkit/internal/jni/NativeJSResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1}, Lcom/pspdfkit/internal/jni/NativeJSResult;-><init>(Lcom/pspdfkit/internal/jni/NativeJSValue;Lcom/pspdfkit/internal/jni/NativeJSEvent;Lcom/pspdfkit/internal/jni/NativeJSError;)V

    goto :goto_0

    .line 27
    :pswitch_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldFormat(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 28
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :pswitch_1
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldBlur(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 30
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 31
    :pswitch_2
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldFocus(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 32
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :pswitch_3
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldMouseUp(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 34
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :pswitch_4
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldMouseDown(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 36
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :pswitch_5
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldMouseExit(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 38
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :pswitch_6
    iget-object v1, p0, Lcom/pspdfkit/internal/k9;->a:Lcom/pspdfkit/internal/n9;

    invoke-static {v1}, Lcom/pspdfkit/internal/n9;->b(Lcom/pspdfkit/internal/n9;)Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeJSDocumentScriptExecutor;->onFieldMouseEnter(Lcom/pspdfkit/internal/jni/NativeJSEventSourceTargetInfo;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    .line 40
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k9;->a()Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/pv$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/pv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/pspdfkit/annotations/SoundAnnotation;)Lcom/pspdfkit/internal/pv;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/sound/AudioEncoding;->SIGNED:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    if-ne v0, v1, :cond_1

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioData()[B

    move-result-object v3

    if-eqz v3, :cond_0

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/pv;

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleRate()I

    move-result v4

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleSize()I

    move-result v5

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getChannels()I

    move-result v6

    .line 13
    sget-object v7, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    const-string p0, "BIG_ENDIAN"

    invoke-static {v7, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    .line 14
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/pv;-><init>([BIIILjava/nio/ByteOrder;)V

    return-object v0

    .line 15
    :cond_0
    new-instance p0, Ljava/io/IOException;

    const-string v0, "Can\'t read audio data from annotation"

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 16
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported audio encoding: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "No audio data is attached to sound annotation."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

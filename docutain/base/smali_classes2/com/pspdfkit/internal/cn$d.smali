.class final Lcom/pspdfkit/internal/cn$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$ItemAnimator$ItemAnimatorFinishedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/cn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

.field final synthetic c:Lcom/pspdfkit/internal/cn;


# direct methods
.method public static synthetic $r8$lambda$UYRUOMqrzFQf8xmjvXYniZq3gHc(Lcom/pspdfkit/internal/cn$d;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/cn$d;->a()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/cn$d;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$d;->a:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputb(Lcom/pspdfkit/internal/cn$d;Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/cn$d;->b:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-void
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/cn$d;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/cn$d;->b()V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/cn;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/cn$d;->a:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/cn$d;->b:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/cn;Lcom/pspdfkit/internal/cn$d-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cn$d;-><init>(Lcom/pspdfkit/internal/cn;)V

    return-void
.end method

.method private a()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->isRunning(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator$ItemAnimatorFinishedListener;)Z

    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    new-instance v1, Lcom/pspdfkit/internal/cn$d$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/cn$d$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/cn$d;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public final onAnimationsFinished()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/cn$d;->b()V

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-static {v2}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/cn$c;->onTabClosed(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    goto :goto_0

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$d;->b:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/cn$d;->c:Lcom/pspdfkit/internal/cn;

    invoke-static {v1}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 14
    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/cn$c;->onTabSelected(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    :cond_2
    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/pspdfkit/internal/cn$d;->b:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-void
.end method

.class public final Lcom/pspdfkit/internal/am$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/am;->a(Z[Lcom/pspdfkit/annotations/Annotation;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/am;

.field final synthetic b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;>;"
        }
    .end annotation
.end field

.field final synthetic c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic d:Z


# direct methods
.method public static synthetic $r8$lambda$sC1DIxPNDmMAfRrEQRv1vpZICls(Lcom/pspdfkit/internal/am$d;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/am$d;->a(Lcom/pspdfkit/internal/am$d;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/am;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    iput-object p2, p0, Lcom/pspdfkit/internal/am$d;->b:Ljava/util/List;

    iput-object p3, p0, Lcom/pspdfkit/internal/am$d;->c:Ljava/util/List;

    iput-boolean p4, p0, Lcom/pspdfkit/internal/am$d;->d:Z

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/am$d;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    .line 19
    invoke-static {v0}, Lcom/pspdfkit/internal/am;->l(Lcom/pspdfkit/internal/am;)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 24
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 25
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->o()V

    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->f(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/y1;->c(Z)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->k(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/ka;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v1}, Lcom/pspdfkit/internal/am;->k(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/ka;

    move-result-object v1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v2, v1}, Lcom/pspdfkit/internal/y1;->a(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V

    .line 39
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->j(Lcom/pspdfkit/internal/am;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 40
    iget-object v2, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v2}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/w0;

    move-result-object v2

    .line 42
    iget-boolean v3, p0, Lcom/pspdfkit/internal/am$d;->d:Z

    .line 43
    check-cast v2, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v2, v1, v3}, Lcom/pspdfkit/internal/i1;->b(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_1

    .line 50
    :cond_3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am$d;->d:Z

    if-eqz v0, :cond_4

    .line 51
    iget-object p0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->e()Lcom/pspdfkit/internal/y1;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->q()Z

    :cond_4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->f(Lcom/pspdfkit/internal/am;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    .line 4
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 5
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/am;->c(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 9
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am$d;->a:Lcom/pspdfkit/internal/am;

    invoke-static {v0}, Lcom/pspdfkit/internal/am;->h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/am$d;->c:Ljava/util/List;

    .line 17
    new-instance v2, Lcom/pspdfkit/internal/am$d$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/am$d$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/am$d;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/nb;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/pspdfkit/internal/si;)Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 16
    :cond_0
    new-instance v0, Lcom/pspdfkit/annotations/measurements/Scale;

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/si;->a()F

    move-result v1

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/si;->c()S

    move-result v2

    const-class v3, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 19
    invoke-virtual {v3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Enum;

    aget-object v2, v3, v2

    .line 20
    check-cast v2, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/si;->b()F

    move-result v3

    .line 22
    invoke-virtual {p0}, Lcom/pspdfkit/internal/si;->d()S

    move-result p0

    const-class v4, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 23
    invoke-virtual {v4}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Enum;

    aget-object p0, v4, p0

    .line 24
    check-cast p0, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    return-object v0
.end method

.method public static a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;
    .locals 5

    if-eqz p0, :cond_0

    .line 7
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Landroid/graphics/RectF;->right:F

    iget p0, p0, Landroid/graphics/RectF;->top:F

    const/4 v3, 0x4

    const/16 v4, 0x10

    .line 8
    invoke-virtual {p1, v3, v4}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 9
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 10
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 11
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 12
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result p0

    .line 14
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;
    .locals 2

    if-eqz p0, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/n5;->a()J

    move-result-wide v0

    long-to-int p0, v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;
    .locals 2

    if-eqz p0, :cond_0

    .line 2
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v0, p0

    const/4 p0, 0x4

    .line 3
    invoke-virtual {p1, p0, p0}, Lcom/pspdfkit/internal/mb;->d(II)V

    long-to-int p0, v0

    .line 4
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/mb;->c(I)V

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result p0

    .line 6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static a(Ljava/lang/Enum;)Ljava/lang/Short;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(TT;)",
            "Ljava/lang/Short;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    int-to-short p0, p0

    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

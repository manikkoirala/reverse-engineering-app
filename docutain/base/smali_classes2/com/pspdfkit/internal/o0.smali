.class public Lcom/pspdfkit/internal/o0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/pspdfkit/annotations/Annotation;

.field protected final b:Lcom/pspdfkit/utils/PageRect;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/o0;->a:Lcom/pspdfkit/annotations/Annotation;

    .line 3
    new-instance v0, Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/pspdfkit/utils/PageRect;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/o0;->b:Lcom/pspdfkit/utils/PageRect;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->b:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    return-void
.end method

.method public final b()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->b:Lcom/pspdfkit/utils/PageRect;

    return-object v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

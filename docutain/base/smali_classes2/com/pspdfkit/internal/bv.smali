.class public abstract Lcom/pspdfkit/internal/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/bv$a;
    }
.end annotation


# static fields
.field private static b:Ljava/lang/Float;


# instance fields
.field private final transient a:Lcom/pspdfkit/utils/PageRect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Lcom/pspdfkit/utils/PageRect;

    invoke-direct {v0}, Lcom/pspdfkit/utils/PageRect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/bv;->a:Lcom/pspdfkit/utils/PageRect;

    return-void
.end method

.method public static final synthetic a()Ljava/lang/Float;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/bv;->b:Ljava/lang/Float;

    return-object v0
.end method

.method public static final synthetic a(Ljava/lang/Float;)V
    .locals 0

    .line 2
    sput-object p0, Lcom/pspdfkit/internal/bv;->b:Ljava/lang/Float;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/iv;)V
    .locals 4

    const-string v0, "pageSize"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "anchor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bv;->a:Lcom/pspdfkit/utils/PageRect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    invoke-virtual {p0}, Lcom/pspdfkit/internal/bv;->b()Lcom/pspdfkit/internal/c7;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/c7;->b()Lcom/pspdfkit/internal/iv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v2

    const/4 v3, 0x0

    aput v2, v1, v3

    .line 4
    sget-object v2, Lcom/pspdfkit/internal/bv;->b:Ljava/lang/Float;

    if-eqz v2, :cond_0

    .line 5
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    goto :goto_0

    :cond_0
    const/high16 v2, 0x41a00000    # 20.0f

    :goto_0
    const/4 v3, 0x1

    aput v2, v1, v3

    .line 6
    invoke-static {v1}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/bv;->b()Lcom/pspdfkit/internal/c7;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/c7;->b()Lcom/pspdfkit/internal/iv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/iv;->b()F

    move-result v2

    neg-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/pspdfkit/utils/PageRect;->set(FFFF)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/bv;->a:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 8
    invoke-virtual {p2}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/bv;->b()Lcom/pspdfkit/internal/c7;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/c7;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v2

    add-float/2addr v2, v1

    .line 9
    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    invoke-virtual {p2}, Lcom/pspdfkit/internal/iv;->b()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/bv;->b()Lcom/pspdfkit/internal/c7;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/c7;->a()Lcom/pspdfkit/internal/iv;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/iv;->b()F

    move-result p2

    sub-float/2addr p1, p2

    .line 10
    invoke-virtual {v0, v2, p1}, Landroid/graphics/RectF;->offset(FF)V

    return-void
.end method

.method public abstract b()Lcom/pspdfkit/internal/c7;
.end method

.method public final c()Lcom/pspdfkit/utils/PageRect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bv;->a:Lcom/pspdfkit/utils/PageRect;

    return-object v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

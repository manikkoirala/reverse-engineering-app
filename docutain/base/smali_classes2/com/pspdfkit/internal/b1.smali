.class public Lcom/pspdfkit/internal/b1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

.field private final c:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationPreferences"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationConfiguration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/b1;->b:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/b1;->c:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    return-void
.end method

.method private static a(ILjava/util/List;)V
    .locals 5

    .line 138
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/16 v1, 0xff

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 139
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    if-eq v4, v1, :cond_2

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_2

    :cond_3
    const/4 p1, 0x1

    :goto_2
    if-eqz p1, :cond_6

    .line 140
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result p1

    if-eq p1, v1, :cond_5

    if-nez p0, :cond_4

    goto :goto_3

    :cond_4
    const/4 p0, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 p0, 0x1

    :goto_4
    if-eqz p0, :cond_6

    const/4 v2, 0x1

    :cond_6
    if-eqz v2, :cond_7

    return-void

    .line 141
    :cond_7
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Annotation inspector does not support transparent colors other than android.graphics.Color.TRANSPARENT"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/b1;->c:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;)Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;
    .locals 8

    const-string v0, "defaultBorderStylePreset"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 219
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;->getBorderStylePresets()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 224
    :cond_0
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;->getBorderStylePresets()Ljava/util/List;

    move-result-object v5

    const-string p1, "borderStyleConfiguration.borderStylePresets"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 226
    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 227
    :goto_0
    check-cast v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    if-nez v1, :cond_3

    const/4 p1, 0x0

    .line 229
    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-object v6, p1

    goto :goto_1

    :cond_3
    move-object v6, v1

    .line 232
    :goto_1
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;

    .line 233
    iget-object v3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 234
    sget p2, Lcom/pspdfkit/R$string;->pspdf__picker_line_style:I

    .line 235
    invoke-static {v3, p2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v7, p3

    .line 236
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;)V

    .line 243
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_border_style_picker:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    return-object p1

    :cond_4
    :goto_2
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;
    .locals 3

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_2

    .line 244
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 245
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    .line 246
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object v0

    const-string v1, "colorConfiguration.availableColors"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/b1;->a(ILjava/util/List;)V

    .line 247
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    .line 248
    invoke-virtual {v0, p3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;->setOnColorPickedListener(Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 249
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_foreground_color_picker:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    return-object v0

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;IZLcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;
    .locals 11

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 44
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 45
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    goto :goto_3

    .line 46
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object v1

    const-string v2, "colorConfiguration.availableColors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/pspdfkit/internal/b1;->a(ILjava/util/List;)V

    if-eqz p3, :cond_2

    .line 48
    iget-object p3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__edit_menu_text_color:I

    .line 49
    invoke-static {p3, v1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "{\n            Localizati\u2026enu_text_color)\n        }"

    .line 50
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 53
    :cond_2
    iget-object p3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__edit_menu_color:I

    .line 54
    invoke-static {p3, v1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "{\n            Localizati\u2026dit_menu_color)\n        }"

    .line 55
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    move-object v6, p3

    .line 58
    new-instance p3, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    .line 59
    iget-object v5, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 61
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object v7

    .line 64
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->customColorPickerEnabled()Z

    move-result v0

    .line 65
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_3

    .line 66
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;

    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    goto :goto_2

    .line 68
    :cond_3
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, v3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    :goto_2
    move-object v9, v0

    move-object v4, p3

    move v8, p2

    move-object v10, p4

    .line 69
    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 81
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_foreground_color_picker:I

    invoke-virtual {p3, p1}, Landroid/view/View;->setId(I)V

    return-object p3

    :cond_4
    :goto_3
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;
    .locals 11

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 82
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 83
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    goto :goto_2

    .line 84
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v1

    const-string v2, "colorConfiguration.availableFillColors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/pspdfkit/internal/b1;->a(ILjava/util/List;)V

    .line 85
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    .line 86
    iget-object v5, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 87
    sget v4, Lcom/pspdfkit/R$string;->pspdf__edit_menu_fill_color:I

    .line 88
    invoke-static {v5, v4, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v7

    .line 92
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->customColorPickerEnabled()Z

    move-result v0

    .line 93
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 94
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2}, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    goto :goto_1

    .line 96
    :cond_2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2, v3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    :goto_1
    move-object v9, v0

    move-object v4, v1

    move v8, p2

    move-object v10, p3

    .line 97
    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 109
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_fill_color_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_3
    :goto_2
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;
    .locals 11

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 110
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getAvailableOutlineColors()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 111
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    goto :goto_2

    .line 112
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getAvailableOutlineColors()Ljava/util/List;

    move-result-object v1

    const-string v2, "colorConfiguration.availableOutlineColors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/pspdfkit/internal/b1;->a(ILjava/util/List;)V

    .line 113
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    .line 114
    iget-object v5, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 115
    sget v4, Lcom/pspdfkit/R$string;->pspdf__edit_menu_outline_color:I

    .line 116
    invoke-static {v5, v4, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    .line 117
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getAvailableOutlineColors()Ljava/util/List;

    move-result-object v7

    .line 120
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->customColorPickerEnabled()Z

    move-result v0

    .line 121
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getAvailableOutlineColors()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 122
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2}, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    goto :goto_1

    .line 124
    :cond_2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2, v3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    :goto_1
    move-object v9, v0

    move-object v4, v1

    move v8, p2

    move-object v10, p3

    .line 125
    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 137
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_outline_color_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_3
    :goto_2
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView;
    .locals 2

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 290
    :cond_0
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;->getAvailableFonts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v0

    :cond_1
    if-nez p2, :cond_2

    .line 291
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;->getDefaultFont()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p2

    const-string v0, "fontConfiguration.defaultFont"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    :cond_2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView;

    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;->getAvailableFonts()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V

    .line 293
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_font_picker:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;Lcom/pspdfkit/annotations/LineEndType;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;
    .locals 8

    const-string v0, "defaultType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_2

    .line 250
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;->getAvailableLineEnds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 255
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    .line 256
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 258
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;->getAvailableLineEnds()Ljava/util/List;

    move-result-object v4

    move-object v1, v0

    move-object v3, p3

    move-object v5, p2

    move v6, p4

    move-object v7, p5

    .line 259
    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/pspdfkit/annotations/LineEndType;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)V

    if-eqz p4, :cond_1

    .line 269
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_line_start_picker:I

    goto :goto_0

    .line 271
    :cond_1
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_line_end_picker:I

    .line 272
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    return-object v0

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;)Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 294
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    .line 295
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 296
    sget v3, Lcom/pspdfkit/R$string;->pspdf__picker_precision:I

    .line 297
    invoke-static {v2, v3, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    if-nez p2, :cond_1

    .line 298
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;->getDefaultPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p2

    const-string p1, "precisionConfiguration.defaultPrecision"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    :cond_1
    invoke-direct {v1, v2, v0, p2, p3}, Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;)V

    .line 305
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_precision_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1
.end method

.method protected final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;)Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;
    .locals 8

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    instance-of v0, p1, Lcom/pspdfkit/annotations/LineAnnotation;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/pspdfkit/annotations/LineAnnotation;

    move-object v3, p1

    goto :goto_0

    :cond_0
    move-object v3, v1

    :goto_0
    if-nez v3, :cond_1

    return-object v1

    .line 319
    :cond_1
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;

    .line 321
    iget-object v4, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 322
    sget v0, Lcom/pspdfkit/R$string;->pspdf__picker_calibrate:I

    .line 323
    invoke-static {v4, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    if-nez p2, :cond_2

    .line 324
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale;->defaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p2

    iget-object p2, p2, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    const-string v0, "defaultScale().unitTo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    move-object v6, p2

    move-object v2, p1

    move-object v7, p3

    .line 325
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;-><init>(Lcom/pspdfkit/annotations/LineAnnotation;Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;)V

    .line 332
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_scale_calibration_picker:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;)Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 306
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    .line 307
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 308
    sget v3, Lcom/pspdfkit/R$string;->pspdf__picker_scale:I

    .line 309
    invoke-static {v2, v3, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    if-nez p2, :cond_1

    .line 310
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;->getDefaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p2

    const-string p1, "scaleConfiguration.defaultScale"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    :cond_1
    invoke-direct {v1, v2, v0, p2, p3}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;)V

    .line 317
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_scale_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 273
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getMinAlpha()F

    move-result v1

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getMaxAlpha()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    goto :goto_0

    .line 274
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    .line 275
    iget-object v3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 276
    sget v2, Lcom/pspdfkit/R$string;->pspdf__picker_opacity:I

    .line 277
    invoke-static {v3, v2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 278
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getMinAlpha()F

    move-result v0

    const/16 v2, 0x64

    int-to-float v2, v2

    mul-float v0, v0, v2

    float-to-int v6, v0

    .line 279
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getMaxAlpha()F

    move-result p1

    mul-float p1, p1, v2

    float-to-int v7, p1

    mul-float p2, p2, v2

    float-to-int v8, p2

    const-string v5, "%1$s %%"

    move-object v2, v1

    move-object v9, p3

    .line 280
    invoke-direct/range {v2 .. v9}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)V

    .line 289
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_alpha_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_1
    :goto_0
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 200
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getMinTextSize()F

    move-result v1

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getMaxTextSize()F

    move-result v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    goto :goto_0

    .line 201
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    .line 202
    iget-object v3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 203
    sget v2, Lcom/pspdfkit/R$string;->pspdf__size:I

    .line 204
    invoke-static {v3, v2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 205
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__unit_pt:I

    .line 206
    invoke-static {v2, v5, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 207
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getMinTextSize()F

    move-result v0

    float-to-int v6, v0

    .line 208
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getMaxTextSize()F

    move-result p1

    float-to-int v7, p1

    float-to-int v8, p2

    move-object v2, v1

    move-object v9, p3

    .line 209
    invoke-direct/range {v2 .. v9}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)V

    .line 218
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_text_size_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_1
    :goto_0
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 181
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getMinThickness()F

    move-result v1

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getMaxThickness()F

    move-result v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    goto :goto_0

    .line 182
    :cond_0
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    .line 183
    iget-object v3, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 184
    sget v2, Lcom/pspdfkit/R$string;->pspdf__picker_thickness:I

    .line 185
    invoke-static {v3, v2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 186
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__unit_pt:I

    .line 187
    invoke-static {v2, v5, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 188
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getMinThickness()F

    move-result v0

    float-to-int v6, v0

    .line 189
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getMaxThickness()F

    move-result p1

    float-to-int v7, p1

    float-to-int v8, p2

    move-object v2, v1

    move-object v9, p3

    .line 190
    invoke-direct/range {v2 .. v9}, Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)V

    .line 199
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_thickness_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_1
    :goto_0
    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;ZLcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;)Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;
    .locals 3

    const-string v0, "annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tool"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/pspdfkit/internal/jr;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    const/4 p1, 0x0

    if-nez v0, :cond_1

    return-object p1

    .line 32
    :cond_1
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;

    .line 33
    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 34
    sget v2, Lcom/pspdfkit/R$string;->pspdf__picker_snapping:I

    .line 35
    invoke-static {v1, v2, p1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    .line 36
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;)V

    .line 42
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_snapping_picker:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;
    .locals 3

    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return-object v0

    .line 146
    :cond_1
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    .line 147
    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 148
    sget v2, Lcom/pspdfkit/R$string;->pspdf__edit_menu_overlay_text:I

    .line 149
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-direct {p1, v1, v0, p2, p3}, Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)V

    .line 156
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_overlay_text_picker:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;ZLcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;)Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;
    .locals 8

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 157
    :cond_0
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;

    .line 158
    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 159
    sget v1, Lcom/pspdfkit/R$string;->pspdf__edit_menu_repeat_overlay_text:I

    .line 160
    invoke-static {v2, v1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    move-object v1, p1

    move v6, p2

    move-object v7, p3

    .line 161
    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;)V

    .line 169
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_repeat_overlay_text_picker:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;)Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 170
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->isZIndexEditingEnabled()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 171
    :cond_0
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;

    .line 172
    iget-object v1, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 173
    sget v2, Lcom/pspdfkit/R$string;->pspdf__z_index_order:I

    .line 174
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-direct {p1, v1, v0, p2}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;)V

    .line 180
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_z_index_picker:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    return-object p1

    :cond_1
    :goto_0
    return-object v0
.end method

.method protected final b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b1;->b:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    return-object v0
.end method

.method protected final b(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;
    .locals 11

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    goto :goto_2

    .line 4
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v1

    const-string v2, "colorConfiguration.availableFillColors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/pspdfkit/internal/b1;->a(ILjava/util/List;)V

    .line 5
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    .line 6
    iget-object v5, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    .line 7
    sget v4, Lcom/pspdfkit/R$string;->pspdf__picker_line_ends_fill_color:I

    .line 8
    invoke-static {v5, v4, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    .line 9
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object v7

    .line 12
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->customColorPickerEnabled()Z

    move-result v0

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getAvailableFillColors()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 14
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2}, Lcom/pspdfkit/ui/inspector/views/CustomColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    goto :goto_1

    .line 16
    :cond_2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    iget-object v2, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, p2, v3}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;-><init>(Landroid/content/Context;Ljava/util/List;IZ)V

    :goto_1
    move-object v9, v0

    move-object v4, v1

    move v8, p2

    move-object v10, p3

    .line 17
    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerDetailView;Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)V

    .line 29
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_line_end_fill_color_picker:I

    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    return-object v1

    :cond_3
    :goto_2
    return-object v0
.end method

.method protected final c()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b1;->a:Landroid/content/Context;

    return-object v0
.end method

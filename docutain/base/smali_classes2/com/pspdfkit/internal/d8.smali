.class public final Lcom/pspdfkit/internal/d8;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/d8$a;,
        Lcom/pspdfkit/internal/d8$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/d8$b;


# instance fields
.field private a:Lcom/pspdfkit/internal/fj;

.field private final b:Lcom/pspdfkit/internal/eb;

.field private final c:Lcom/pspdfkit/internal/jt;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/d8$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/d8$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/d8;->Companion:Lcom/pspdfkit/internal/d8$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/d8;-><init>(Lcom/pspdfkit/internal/fj;I)V

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/fj;Lcom/pspdfkit/internal/eb;Lcom/pspdfkit/internal/jt;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x0

    if-eqz v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/d8$a;->a:Lcom/pspdfkit/internal/d8$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d8$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iput-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    goto :goto_0

    :cond_1
    iput-object p2, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    :goto_0
    and-int/lit8 p2, p1, 0x2

    if-nez p2, :cond_2

    iput-object v1, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    goto :goto_1

    :cond_2
    iput-object p3, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    :goto_1
    and-int/lit8 p1, p1, 0x4

    if-nez p1, :cond_3

    iput-object v1, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    goto :goto_2

    :cond_3
    iput-object p4, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    :goto_2
    return-void
.end method

.method public synthetic constructor <init>(Lcom/pspdfkit/internal/fj;I)V
    .locals 1

    and-int/lit8 p2, p2, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    move-object p1, v0

    .line 10
    :cond_0
    invoke-direct {p0, p1, v0, v0}, Lcom/pspdfkit/internal/d8;-><init>(Lcom/pspdfkit/internal/fj;Lcom/pspdfkit/internal/eb;Lcom/pspdfkit/internal/jt;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/fj;Lcom/pspdfkit/internal/eb;Lcom/pspdfkit/internal/jt;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    .line 9
    iput-object p3, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/d8;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1
    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    sget-object v1, Lcom/pspdfkit/internal/fj$a;->a:Lcom/pspdfkit/internal/fj$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    invoke-interface {p1, p2, v0, v1, v3}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_2
    invoke-interface {p1, p2, v2}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    if-eqz v1, :cond_4

    :goto_2
    const/4 v1, 0x1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_5

    sget-object v1, Lcom/pspdfkit/internal/eb$a;->a:Lcom/pspdfkit/internal/eb$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    invoke-interface {p1, p2, v2, v1, v3}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_5
    const/4 v1, 0x2

    invoke-interface {p1, p2, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_4

    :cond_6
    iget-object v3, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    if-eqz v3, :cond_7

    :goto_4
    const/4 v0, 0x1

    :cond_7
    if-eqz v0, :cond_8

    sget-object v0, Lcom/pspdfkit/internal/jt$a;->a:Lcom/pspdfkit/internal/jt$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_8
    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/fj;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/fj;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    return-void
.end method

.method public final b()Lcom/pspdfkit/internal/eb;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/jt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jt;
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d8;->c:Lcom/pspdfkit/internal/jt;

    if-nez v0, :cond_7

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/jt;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/w3;->a()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 5
    :goto_0
    iget-object v4, p0, Lcom/pspdfkit/internal/d8;->b:Lcom/pspdfkit/internal/eb;

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fb;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object v5, v2

    .line 7
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fb;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v6, v1

    goto :goto_2

    :cond_2
    move-object v6, v2

    .line 8
    :goto_2
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y3;->b()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    move-object v7, v1

    goto :goto_3

    :cond_3
    move-object v7, v2

    .line 9
    :goto_3
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v8, v1

    goto :goto_4

    :cond_4
    move-object v8, v2

    .line 10
    :goto_4
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ph;->b()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    move-object v9, v1

    goto :goto_5

    :cond_5
    move-object v9, v2

    .line 11
    :goto_5
    iget-object v1, p0, Lcom/pspdfkit/internal/d8;->a:Lcom/pspdfkit/internal/fj;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ph;->a()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    move-object v10, v1

    goto :goto_6

    :cond_6
    move-object v10, v2

    :goto_6
    move-object v1, v0

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    .line 12
    invoke-direct/range {v1 .. v9}, Lcom/pspdfkit/internal/jt;-><init>(Ljava/lang/String;Lcom/pspdfkit/internal/eb;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;)V

    :cond_7
    return-object v0
.end method

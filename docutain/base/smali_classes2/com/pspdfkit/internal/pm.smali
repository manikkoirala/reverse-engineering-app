.class public final Lcom/pspdfkit/internal/pm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pm$c;,
        Lcom/pspdfkit/internal/pm$b;,
        Lcom/pspdfkit/internal/pm$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/pm$a;

.field private final b:Lcom/pspdfkit/internal/pm$b;

.field private final c:Lcom/pspdfkit/internal/pm$c;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/qm;Lcom/pspdfkit/ui/PdfUi;Lcom/pspdfkit/ui/PdfUi;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pm;->d:Z

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/pm;->b:Lcom/pspdfkit/internal/pm$b;

    .line 10
    iput-object p3, p0, Lcom/pspdfkit/internal/pm;->c:Lcom/pspdfkit/internal/pm$c;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pm;->d:Z

    return-void
.end method

.method public final a(Landroid/view/Menu;)V
    .locals 4

    .line 1
    instance-of v0, p1, Landroidx/appcompat/view/menu/MenuBuilder;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroidx/appcompat/view/menu/MenuBuilder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/appcompat/view/menu/MenuBuilder;->setOptionalIconsVisible(Z)V

    .line 3
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/pm;->b:Lcom/pspdfkit/internal/pm$b;

    iget-object v1, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    .line 7
    check-cast v1, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/qm;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pm$b;->onGenerateMenuItemIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 10
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_1

    goto :goto_0

    .line 12
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    const-string v3, ""

    invoke-interface {p1, v2, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0

    .line 16
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    check-cast v0, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qm;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 17
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 20
    iget-object v3, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    check-cast v3, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/qm;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_1

    :cond_4
    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    .line 21
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pm;->d:Z

    return-void
.end method

.method public final b(Landroid/view/Menu;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    check-cast v0, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qm;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 5
    iget-object v4, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    check-cast v4, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/qm;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 6
    iget-object v4, p0, Lcom/pspdfkit/internal/pm;->c:Lcom/pspdfkit/internal/pm$c;

    .line 7
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    check-cast v6, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 8
    sget v6, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    if-eq v7, v6, :cond_3

    sget v6, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    if-ne v7, v6, :cond_1

    goto :goto_1

    .line 10
    :cond_1
    sget v2, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    if-ne v7, v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x2

    .line 11
    :cond_3
    :goto_1
    invoke-interface {v4, v5, v2}, Lcom/pspdfkit/internal/pm$c;->onGetShowAsAction(II)I

    move-result v2

    .line 12
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/pm;->a:Lcom/pspdfkit/internal/pm$a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    check-cast v2, Lcom/pspdfkit/internal/qm;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/qm;->d(I)Z

    move-result v1

    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 19
    :cond_4
    :goto_2
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 20
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/pspdfkit/internal/pm;->d:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method

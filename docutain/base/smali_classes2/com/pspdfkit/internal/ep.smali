.class public final Lcom/pspdfkit/internal/ep;
.super Lcom/pspdfkit/internal/jni/NativeReflowProcessorDelegate;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/fp;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/fp;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeReflowProcessorDelegate;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ep;->a:Lcom/pspdfkit/internal/fp;

    return-void
.end method


# virtual methods
.method public final isCanceled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ep;->a:Lcom/pspdfkit/internal/fp;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/internal/fp;->isCanceled()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final progress(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ep;->a:Lcom/pspdfkit/internal/fp;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/fp;->progress(II)V

    :cond_0
    return-void
.end method

.class abstract Lcom/pspdfkit/internal/f4;
.super Lcom/pspdfkit/internal/c4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ShapeDelegate:",
        "Lcom/pspdfkit/internal/sn;",
        ">",
        "Lcom/pspdfkit/internal/c4<",
        "TShapeDelegate;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/sn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TShapeDelegate;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/c4;-><init>(Lcom/pspdfkit/internal/d4;)V

    return-void
.end method


# virtual methods
.method public a(IIFLcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;FLandroidx/core/util/Pair;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIF",
            "Lcom/pspdfkit/annotations/BorderStyle;",
            "Lcom/pspdfkit/annotations/BorderEffect;",
            "F",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;F",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)Z"
        }
    .end annotation

    .line 18
    invoke-super/range {p0 .. p9}, Lcom/pspdfkit/internal/c4;->a(IIFLcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;FLandroidx/core/util/Pair;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/sn;

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/internal/sn;->z()Landroidx/core/util/Pair;

    move-result-object p1

    invoke-static {p1, p9}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v2, Lcom/pspdfkit/internal/sn;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/sn;->z()Landroidx/core/util/Pair;

    move-result-object v2

    .line 6
    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 9
    iget-object v1, v2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_0

    check-cast v1, Lcom/pspdfkit/annotations/LineEndType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    .line 10
    :goto_0
    iget-object v2, v2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_1

    check-cast v2, Lcom/pspdfkit/annotations/LineEndType;

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    .line 11
    :goto_1
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)Z

    move-result p1

    or-int/2addr v0, p1

    :cond_2
    return v0
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 0

    .line 12
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/c4;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p2

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p1

    .line 15
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p3, Lcom/pspdfkit/internal/sn;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/sn;->z()Landroidx/core/util/Pair;

    move-result-object p3

    .line 16
    invoke-static {p1, p3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    if-eqz p1, :cond_0

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/sn;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/sn;->a(Landroidx/core/util/Pair;)V

    const/4 p2, 0x1

    :cond_0
    return p2
.end method

.method public b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/annotations/PolylineAnnotation;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {p0, p3, p2}, Lcom/pspdfkit/internal/c4;->b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object p2

    .line 9
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-ge p3, v0, :cond_0

    return v1

    .line 10
    :cond_0
    move-object p3, p1

    check-cast p3, Lcom/pspdfkit/annotations/PolylineAnnotation;

    .line 11
    invoke-virtual {p3}, Lcom/pspdfkit/annotations/PolylineAnnotation;->getPoints()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12
    invoke-virtual {p3, p2}, Lcom/pspdfkit/annotations/PolylineAnnotation;->setPoints(Ljava/util/List;)V

    const/4 v1, 0x1

    .line 17
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/f4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    or-int/2addr p1, v1

    return p1

    .line 18
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "You need to pass a PolylineAnnotation to this shape."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

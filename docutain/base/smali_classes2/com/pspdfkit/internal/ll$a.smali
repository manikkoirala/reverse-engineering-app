.class final Lcom/pspdfkit/internal/ll$a;
.super Landroid/view/OrientationEventListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ll;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ll;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ll;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    const/4 p1, 0x2

    invoke-direct {p0, p2, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public final onOrientationChanged(I)V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x3

    const/4 v1, 0x4

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/16 v4, 0x14

    const/4 v5, 0x0

    if-le p1, v4, :cond_4

    const/16 v6, 0x154

    if-lt p1, v6, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit16 v6, p1, -0xb4

    .line 2
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v6, v4, :cond_1

    const/4 p1, 0x2

    goto :goto_1

    :cond_1
    add-int/lit8 v6, p1, -0x5a

    .line 4
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v6, v4, :cond_2

    const/4 p1, 0x4

    goto :goto_1

    :cond_2
    add-int/lit16 p1, p1, -0x10e

    .line 6
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    if-gt p1, v4, :cond_3

    const/4 p1, 0x3

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_5

    return-void

    .line 7
    :cond_5
    iget-object v4, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgete(Lcom/pspdfkit/internal/ll;)I

    move-result v6

    const-wide/16 v7, 0x0

    if-eq p1, v6, :cond_6

    .line 8
    invoke-static {v4, v7, v8}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputd(Lcom/pspdfkit/internal/ll;J)V

    .line 9
    invoke-static {v4, v7, v8}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputc(Lcom/pspdfkit/internal/ll;J)V

    .line 10
    invoke-static {v4, p1}, Lcom/pspdfkit/internal/ll;->-$$Nest$fpute(Lcom/pspdfkit/internal/ll;I)V

    goto/16 :goto_2

    .line 11
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 12
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ll;)J

    move-result-wide v11

    cmp-long v6, v11, v7

    if-nez v6, :cond_7

    .line 13
    invoke-static {v4, v9, v10}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputd(Lcom/pspdfkit/internal/ll;J)V

    .line 15
    :cond_7
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ll;)J

    move-result-wide v6

    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ll;)J

    move-result-wide v11

    sub-long v11, v9, v11

    add-long/2addr v11, v6

    invoke-static {v4, v11, v12}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputc(Lcom/pspdfkit/internal/ll;J)V

    .line 16
    invoke-static {v4, v9, v10}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputd(Lcom/pspdfkit/internal/ll;J)V

    .line 17
    iget-object v4, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ll;)J

    move-result-wide v6

    const-wide/16 v8, 0x5dc

    cmp-long v10, v6, v8

    if-lez v10, :cond_b

    const-string v6, "PSPDFKit.OrientationDetector"

    if-ne p1, v0, :cond_8

    .line 19
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ll;)I

    move-result v0

    if-eqz v0, :cond_b

    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, "switch to SCREEN_ORIENTATION_LANDSCAPE"

    .line 20
    invoke-static {v6, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v0, v5}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputf(Lcom/pspdfkit/internal/ll;I)V

    .line 22
    invoke-static {v0}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ll;)Lcom/pspdfkit/internal/ll$b;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 23
    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->a(I)V

    goto :goto_2

    :cond_8
    if-ne p1, v3, :cond_9

    .line 29
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ll;)I

    move-result v0

    if-eq v0, v3, :cond_b

    new-array v0, v5, [Ljava/lang/Object;

    const-string v1, "switch to SCREEN_ORIENTATION_PORTRAIT"

    .line 30
    invoke-static {v6, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputf(Lcom/pspdfkit/internal/ll;I)V

    .line 32
    invoke-static {v0}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ll;)Lcom/pspdfkit/internal/ll$b;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 33
    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->a(I)V

    goto :goto_2

    :cond_9
    if-ne p1, v2, :cond_a

    .line 39
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ll;)I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_b

    new-array v0, v5, [Ljava/lang/Object;

    const-string v2, "switch to SCREEN_ORIENTATION_REVERSE_PORTRAIT"

    .line 40
    invoke-static {v6, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputf(Lcom/pspdfkit/internal/ll;I)V

    .line 42
    invoke-static {v0}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ll;)Lcom/pspdfkit/internal/ll$b;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 43
    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->a(I)V

    goto :goto_2

    :cond_a
    if-ne p1, v1, :cond_b

    .line 49
    invoke-static {v4}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetf(Lcom/pspdfkit/internal/ll;)I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_b

    new-array v0, v5, [Ljava/lang/Object;

    const-string v2, "switch to SCREEN_ORIENTATION_REVERSE_LANDSCAPE"

    .line 50
    invoke-static {v6, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/ll$a;->a:Lcom/pspdfkit/internal/ll;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ll;->-$$Nest$fputf(Lcom/pspdfkit/internal/ll;I)V

    .line 52
    invoke-static {v0}, Lcom/pspdfkit/internal/ll;->-$$Nest$fgetg(Lcom/pspdfkit/internal/ll;)Lcom/pspdfkit/internal/ll$b;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 53
    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->a(I)V

    :cond_b
    :goto_2
    return-void
.end method

.class final Lcom/pspdfkit/internal/m8$a;
.super Lcom/pspdfkit/internal/cs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/m8;->a(Landroid/content/Context;Lcom/pspdfkit/document/editor/PdfDocumentEditor;Ljava/util/HashSet;Lio/reactivex/rxjava3/core/Maybe;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/cs<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/pspdfkit/internal/m8;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/m8;Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/m8$a;->c:Lcom/pspdfkit/internal/m8;

    iput-object p2, p0, Lcom/pspdfkit/internal/m8$a;->a:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    iput-object p3, p0, Lcom/pspdfkit/internal/m8$a;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/pspdfkit/internal/cs;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8$a;->a:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->dismiss()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.DocumentEditor"

    const-string v2, "Document saving was canceled."

    .line 2
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/m8$a;->a:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    iget-object v1, p0, Lcom/pspdfkit/internal/m8$a;->b:Landroid/content/Context;

    sget v2, Lcom/pspdfkit/R$string;->pspdf__document_could_not_be_saved:I

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->showErrorDialog(Landroid/content/Context;I)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.DocumentEditor"

    const-string v2, "Document couldn\'t be saved."

    .line 2
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Landroid/net/Uri;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/m8$a;->a:Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/document/editor/DocumentEditorProgressDialog;->dismiss()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/m8$a;->c:Lcom/pspdfkit/internal/m8;

    invoke-static {v0}, Lcom/pspdfkit/internal/m8;->-$$Nest$fgetb(Lcom/pspdfkit/internal/m8;)Lcom/pspdfkit/internal/gl;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/gl;->onDocumentExported(Landroid/net/Uri;)V

    return-void
.end method

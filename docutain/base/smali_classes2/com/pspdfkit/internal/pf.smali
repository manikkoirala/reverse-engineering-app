.class public interface abstract Lcom/pspdfkit/internal/pf;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V
.end method

.method public abstract addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

.method public abstract adjustBoundsForRotation(F)V
.end method

.method public abstract clearModified()V
.end method

.method public abstract clearTextShouldFit()V
.end method

.method public abstract ensureAnnotationCanBeAttachedToDocument(Lcom/pspdfkit/internal/zf;)V
.end method

.method public abstract getAction()Lcom/pspdfkit/annotations/actions/Action;
.end method

.method public abstract getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;
.end method

.method public abstract getAdditionalActions()Lcom/pspdfkit/internal/o;
.end method

.method public abstract getAdditionalData(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getAnnotationResource()Lcom/pspdfkit/internal/x1;
.end method

.method public abstract getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;
.end method

.method public abstract getCopy()Lcom/pspdfkit/annotations/Annotation;
.end method

.method public abstract getDetachedAnnotationLookupKey()Ljava/lang/Integer;
.end method

.method public abstract getEdgeInsets()Lcom/pspdfkit/utils/EdgeInsets;
.end method

.method public abstract getInReplyToUuid()Ljava/lang/String;
.end method

.method public abstract getInternalDocument()Lcom/pspdfkit/internal/zf;
.end method

.method public abstract getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
.end method

.method public abstract getMeasurementProperties()Lcom/pspdfkit/internal/ri;
.end method

.method public abstract getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
.end method

.method public abstract getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;
.end method

.method public abstract getNativeAnnotationManager()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;
.end method

.method public abstract getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;
.end method

.method public abstract getPageRotation()I
.end method

.method public abstract getProperties()Lcom/pspdfkit/internal/p1;
.end method

.method public abstract getQuadrilaterals()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ho;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRotation()I
.end method

.method public abstract getSoundAnnotationState()Lcom/pspdfkit/internal/ls;
.end method

.method public abstract getTextShouldFit()Z
.end method

.method public abstract getUuid()Ljava/lang/String;
.end method

.method public abstract getVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
.end method

.method public abstract hasInstantComments()Z
.end method

.method public abstract markAsInstantCommentRoot()V
.end method

.method public abstract needsFlippedContentSize()Z
.end method

.method public abstract needsSyncingWithCore()Z
.end method

.method public abstract notifyAnnotationCreated()V
.end method

.method public abstract notifyAnnotationRemoved()V
.end method

.method public abstract notifyAnnotationUpdated()V
.end method

.method public abstract onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V
.end method

.method public abstract onDetachedFromDocument()V
.end method

.method public abstract prepareForCopy()V
.end method

.method public abstract removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V
.end method

.method public abstract removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
.end method

.method public abstract requireNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;
.end method

.method public abstract setAction(Lcom/pspdfkit/annotations/actions/Action;)V
.end method

.method public abstract setAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V
.end method

.method public abstract setAdditionalData(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract setAnnotationResource(Lcom/pspdfkit/internal/x1;)V
.end method

.method public abstract setContentSize(Landroid/graphics/RectF;Z)V
.end method

.method public abstract setDetachedAnnotationLookupKey(Ljava/lang/Integer;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V
.end method

.method public abstract setEdgeInsets(Lcom/pspdfkit/utils/EdgeInsets;)V
.end method

.method public abstract setFontName(Ljava/lang/String;)V
.end method

.method public abstract setInReplyToUuid(Ljava/lang/String;)V
.end method

.method public abstract setIsSignature(Z)V
.end method

.method public abstract setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
.end method

.method public abstract setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
.end method

.method public abstract setPageIndex(I)V
.end method

.method public abstract setPointsWithoutCoreSync(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setProperties(Lcom/pspdfkit/internal/p1;)V
.end method

.method public abstract setQuadrilaterals(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ho;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setRotation(I)V
.end method

.method public abstract setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V
.end method

.method public abstract setTextShouldFit(Z)V
.end method

.method public abstract setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
.end method

.method public abstract synchronizeFromNativeObjectIfAttached()V
.end method

.method public abstract synchronizeToNativeObjectIfAttached()Z
.end method

.method public abstract synchronizeToNativeObjectIfAttached(Z)Z
.end method

.method public abstract synchronizeToNativeObjectIfAttached(ZZ)Z
.end method

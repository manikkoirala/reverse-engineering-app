.class final Lcom/pspdfkit/internal/h3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/b3;

.field final synthetic b:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/b3;Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/b3;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/internal/h3;->a:Lcom/pspdfkit/internal/b3;

    iput-object p2, p0, Lcom/pspdfkit/internal/h3;->b:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/j3;

    const-string v0, "audioPlayer"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/pspdfkit/internal/h3;->a:Lcom/pspdfkit/internal/b3;

    invoke-static {v0}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/annotations/SoundAnnotation;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/h3;->a:Lcom/pspdfkit/internal/b3;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/b3;Lcom/pspdfkit/internal/j3;)V

    .line 254
    iget-object v0, p0, Lcom/pspdfkit/internal/h3;->a:Lcom/pspdfkit/internal/b3;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/j3;->a(Lcom/pspdfkit/internal/b3;)V

    .line 257
    iget-object p1, p0, Lcom/pspdfkit/internal/h3;->a:Lcom/pspdfkit/internal/b3;

    .line 258
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 259
    new-instance v0, Lcom/pspdfkit/internal/f3;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/f3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    .line 260
    iget-object p1, p0, Lcom/pspdfkit/internal/h3;->b:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

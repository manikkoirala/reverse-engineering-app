.class public final Lcom/pspdfkit/internal/hr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Canvas;

.field private final e:Landroid/graphics/Matrix;

.field private final f:Lcom/pspdfkit/internal/do;

.field private g:Z

.field private h:Landroid/graphics/Bitmap;

.field private i:Landroid/graphics/Rect;

.field private j:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$5359gqe0Tx2wGD8KKNV3d2C8hjM(Ljava/util/List;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/hr;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$QIUWURiBN5HqbKDZft7BPFPGlaE(Lcom/pspdfkit/internal/hr;Ljava/util/List;Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/hr;->a(Ljava/util/List;Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YJhFkqGV7U2x9x9s4IFbMOh8n4A(Lcom/pspdfkit/internal/hr;Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/internal/hr;->a(Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJLio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Paint;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/hr;->b:Landroid/graphics/Paint;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Paint;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/hr;->c:Landroid/graphics/Paint;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Canvas;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/hr;->d:Landroid/graphics/Canvas;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/hr;)Landroid/graphics/Bitmap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/hr;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/hr;->j:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->e:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hr;->g:Z

    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    .line 16
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->i:Landroid/graphics/Rect;

    .line 23
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->d:Landroid/graphics/Canvas;

    .line 24
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->b:Landroid/graphics/Paint;

    .line 26
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/hr;->c:Landroid/graphics/Paint;

    .line 30
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u;

    const/4 p2, 0x1

    const-string v0, "pspdfkit-shape-render"

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/u;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/hr;->f:Lcom/pspdfkit/internal/do;

    return-void
.end method

.method private a(Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/br;",
            ">;",
            "Landroid/graphics/Matrix;",
            "FJ)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 5
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hr;->g:Z

    .line 15
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 16
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/internal/hr;->e:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 19
    new-instance p2, Lcom/pspdfkit/internal/hr$b;

    move-object v1, p2

    move-object v2, p0

    move-object v3, v0

    move-object v4, p1

    move v5, p4

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/hr$b;-><init>(Lcom/pspdfkit/internal/hr;Landroid/graphics/Rect;Ljava/util/ArrayList;FLandroid/graphics/Matrix;)V

    invoke-static {p2}, Lio/reactivex/rxjava3/internal/operators/single/SingleJust;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    iget-object p3, p0, Lcom/pspdfkit/internal/hr;->f:Lcom/pspdfkit/internal/do;

    const/4 p4, 0x5

    .line 69
    invoke-virtual {p3, p4}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    sget-object p3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 70
    invoke-virtual {p2, p5, p6, p3}, Lio/reactivex/rxjava3/core/Single;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 71
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda1;

    invoke-direct {p3, p0, p1, v0}, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/hr;Ljava/util/List;Landroid/graphics/Rect;)V

    .line 72
    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda2;

    invoke-direct {p3, p1}, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda2;-><init>(Ljava/util/List;)V

    .line 86
    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Single;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 87
    :cond_1
    :goto_0
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/hr;->i:Landroid/graphics/Rect;

    .line 88
    invoke-static {}, Lio/reactivex/rxjava3/core/Single;->never()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hr;->a()V

    .line 3
    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/hr;->a(Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/hr$a;

    invoke-direct {p2, p0, p7}, Lcom/pspdfkit/internal/hr$a;-><init>(Lcom/pspdfkit/internal/hr;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    .line 4
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeWith(Lio/reactivex/rxjava3/core/SingleObserver;)Lio/reactivex/rxjava3/core/SingleObserver;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object p1, p0, Lcom/pspdfkit/internal/hr;->j:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static synthetic a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 99
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/br;

    .line 100
    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/br$a;->c:Lcom/pspdfkit/internal/br$a;

    if-eq v1, v2, :cond_0

    .line 101
    sget-object v1, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/br;->a(Lcom/pspdfkit/internal/br$a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private synthetic a(Ljava/util/List;Landroid/graphics/Rect;Landroid/graphics/Bitmap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 89
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/br;

    .line 90
    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    if-ne v1, v2, :cond_0

    .line 91
    sget-object v1, Lcom/pspdfkit/internal/br$a;->c:Lcom/pspdfkit/internal/br$a;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/br;->a(Lcom/pspdfkit/internal/br$a;)V

    goto :goto_0

    .line 96
    :cond_1
    iput-object p3, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    .line 97
    iput-object p2, p0, Lcom/pspdfkit/internal/hr;->i:Landroid/graphics/Rect;

    const/4 p1, 0x1

    .line 98
    iput-boolean p1, p0, Lcom/pspdfkit/internal/hr;->g:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Ljava/util/ArrayList;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Completable;
    .locals 9

    .line 1
    new-instance v8, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda0;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/hr$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/hr;Landroid/graphics/Rect;Ljava/util/List;Landroid/graphics/Matrix;FJ)V

    invoke-static {v8}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 102
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hr;->g:Z

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->j:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 104
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->j:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final c()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->i:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hr;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->f:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/do;->c()V

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public final recycle()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hr;->a()V

    .line 2
    monitor-enter p0

    .line 3
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/hr;->h:Landroid/graphics/Bitmap;

    .line 7
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

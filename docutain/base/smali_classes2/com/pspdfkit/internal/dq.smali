.class public final Lcom/pspdfkit/internal/dq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private b:Landroid/renderscript/RenderScript;

.field private c:Landroid/renderscript/ScriptIntrinsicBlur;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:F

.field private g:F

.field private final h:Landroid/graphics/Paint;

.field private final i:F

.field private final j:F

.field private final k:F


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dq;->a:Landroid/view/View;

    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/high16 v0, 0x40800000    # 4.0f

    .line 21
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/dq;->f:F

    const/high16 v0, 0x42c80000    # 100.0f

    .line 23
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/dq;->j:F

    const/high16 v1, 0x42400000    # 48.0f

    .line 24
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dq;->k:F

    const/high16 v2, 0x42180000    # 38.0f

    .line 25
    invoke-static {p1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/dq;->i:F

    const/high16 v2, 0x41c00000    # 24.0f

    .line 27
    invoke-static {p1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p1

    .line 28
    iget v2, p0, Lcom/pspdfkit/internal/dq;->f:F

    div-float/2addr v2, p1

    const/high16 p1, 0x41c80000    # 25.0f

    mul-float v2, v2, p1

    iput v2, p0, Lcom/pspdfkit/internal/dq;->g:F

    .line 30
    new-instance p1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {p1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/dq;->h:Landroid/graphics/Paint;

    .line 31
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    const/high16 v4, -0x1000000

    invoke-direct {v2, v4, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    const/16 v2, 0x66

    .line 34
    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 36
    new-instance p1, Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-direct {p1, v2, v2, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 37
    invoke-direct {p0}, Lcom/pspdfkit/internal/dq;->a()V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dq;->j:F

    iget v1, p0, Lcom/pspdfkit/internal/dq;->g:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 2
    iget v2, p0, Lcom/pspdfkit/internal/dq;->k:F

    add-float/2addr v2, v1

    float-to-int v0, v0

    float-to-int v1, v2

    .line 3
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v1, "createBitmap(extWidth.to\u2026 Bitmap.Config.ARGB_8888)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/dq;->d:Landroid/graphics/Bitmap;

    .line 4
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/pspdfkit/internal/dq;->d:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    const-string v1, "blurredBitmap"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 5
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/dq;->g:F

    iget v3, p0, Lcom/pspdfkit/internal/dq;->j:F

    add-float/2addr v3, v2

    iget v4, p0, Lcom/pspdfkit/internal/dq;->k:F

    add-float/2addr v4, v2

    invoke-direct {v1, v2, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 6
    iget v2, p0, Lcom/pspdfkit/internal/dq;->i:F

    iget-object v3, p0, Lcom/pspdfkit/internal/dq;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;FF)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->e:Landroid/graphics/Bitmap;

    const-string v1, "blurredBitmap"

    const/4 v2, 0x0

    if-nez v0, :cond_7

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    .line 9
    :cond_0
    iget v3, p0, Lcom/pspdfkit/internal/dq;->g:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-nez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    goto :goto_1

    .line 10
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/dq;->b:Landroid/renderscript/RenderScript;

    if-nez v3, :cond_3

    move-object v0, v2

    goto :goto_1

    .line 12
    :cond_3
    invoke-static {v3, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v3

    .line 13
    iget-object v4, p0, Lcom/pspdfkit/internal/dq;->b:Landroid/renderscript/RenderScript;

    invoke-virtual {v3}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;

    move-result-object v4

    .line 15
    iget-object v5, p0, Lcom/pspdfkit/internal/dq;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    if-eqz v5, :cond_4

    iget v6, p0, Lcom/pspdfkit/internal/dq;->g:F

    invoke-virtual {v5, v6}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    .line 17
    :cond_4
    iget-object v5, p0, Lcom/pspdfkit/internal/dq;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    if-eqz v5, :cond_5

    invoke-virtual {v5, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    .line 18
    :cond_5
    iget-object v5, p0, Lcom/pspdfkit/internal/dq;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    if-eqz v5, :cond_6

    invoke-virtual {v5, v4}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    .line 20
    :cond_6
    invoke-virtual {v4, v0}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    .line 22
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 23
    invoke-virtual {v4}, Landroid/renderscript/Allocation;->destroy()V

    .line 24
    :goto_1
    iput-object v0, p0, Lcom/pspdfkit/internal/dq;->e:Landroid/graphics/Bitmap;

    .line 28
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_8
    iget v1, p0, Lcom/pspdfkit/internal/dq;->g:F

    sub-float/2addr p2, v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float/2addr p3, v1

    invoke-virtual {p1, v0, p2, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/dq;->b:Landroid/renderscript/RenderScript;

    .line 2
    invoke-static {v0}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/dq;->b:Landroid/renderscript/RenderScript;

    invoke-static {v1, v0}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/dq;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->c:Landroid/renderscript/ScriptIntrinsicBlur;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dq;->b:Landroid/renderscript/RenderScript;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->destroy()V

    :cond_1
    return-void
.end method

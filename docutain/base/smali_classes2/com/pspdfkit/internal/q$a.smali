.class public final Lcom/pspdfkit/internal/q$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/os/Bundle;

.field final synthetic c:Lcom/pspdfkit/internal/q;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/q;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/q$a;->c:Lcom/pspdfkit/internal/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/q$a;->a:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/q;Ljava/lang/String;Lcom/pspdfkit/internal/q$a-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;-><init>(Lcom/pspdfkit/internal/q;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;
    .locals 3

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    const-string v2, "annotation_type"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    const-string v1, "page_index"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final a(Lcom/pspdfkit/bookmarks/Bookmark;)Lcom/pspdfkit/internal/q$a;
    .locals 3

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 12
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    const-string v2, "page_index"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getSortKey()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    const-string v1, "sort"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public final a()V
    .locals 3

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/q$a;->c:Lcom/pspdfkit/internal/q;

    iget-object v1, p0, Lcom/pspdfkit/internal/q$a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/pspdfkit/internal/q$a;->b:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/q;->-$$Nest$ma(Lcom/pspdfkit/internal/q;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

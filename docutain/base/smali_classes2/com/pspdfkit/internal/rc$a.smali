.class public final Lcom/pspdfkit/internal/rc$a;
.super Lcom/pspdfkit/internal/g4$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/rc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g4$a<",
        "Lcom/pspdfkit/internal/rc;",
        "Lcom/pspdfkit/internal/rc$a;",
        ">;"
    }
.end annotation


# instance fields
.field t:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/zf;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/g4$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/rc$a;->t:Z

    return-void
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/internal/g4$a;
    .locals 0

    return-object p0
.end method

.method public final b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g4$a;->a(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    iget-boolean p1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->useCache:Z

    .line 2
    iput-boolean p1, v0, Lcom/pspdfkit/internal/rc$a;->t:Z

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/rc;
    .locals 25

    move-object/from16 v0, p0

    .line 3
    new-instance v23, Lcom/pspdfkit/internal/rc;

    move-object/from16 v1, v23

    iget-object v2, v0, Lcom/pspdfkit/internal/g4$a;->a:Lcom/pspdfkit/internal/zf;

    iget v3, v0, Lcom/pspdfkit/internal/g4$a;->b:I

    iget v4, v0, Lcom/pspdfkit/internal/g4$a;->e:I

    iget-object v5, v0, Lcom/pspdfkit/internal/g4$a;->f:Landroid/graphics/Bitmap;

    iget v6, v0, Lcom/pspdfkit/internal/g4$a;->g:I

    iget v7, v0, Lcom/pspdfkit/internal/g4$a;->h:I

    iget v8, v0, Lcom/pspdfkit/internal/g4$a;->i:I

    iget-object v9, v0, Lcom/pspdfkit/internal/g4$a;->j:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/pspdfkit/internal/g4$a;->k:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/pspdfkit/internal/g4$a;->m:Ljava/lang/Integer;

    iget-object v12, v0, Lcom/pspdfkit/internal/g4$a;->l:Ljava/lang/Integer;

    iget-boolean v13, v0, Lcom/pspdfkit/internal/g4$a;->n:Z

    iget-boolean v14, v0, Lcom/pspdfkit/internal/g4$a;->o:Z

    iget-object v15, v0, Lcom/pspdfkit/internal/g4$a;->q:Ljava/util/ArrayList;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/pspdfkit/internal/g4$a;->c:Ljava/util/ArrayList;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/pspdfkit/internal/g4$a;->d:Ljava/util/ArrayList;

    move-object/from16 v17, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->r:Z

    move/from16 v18, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->s:Z

    move/from16 v19, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/rc$a;->t:Z

    move/from16 v20, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->p:Z

    move/from16 v21, v1

    const/16 v22, 0x0

    move-object/from16 v1, v24

    invoke-direct/range {v1 .. v22}, Lcom/pspdfkit/internal/rc;-><init>(Lcom/pspdfkit/internal/zf;IILandroid/graphics/Bitmap;IIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZZZLcom/pspdfkit/internal/rc-IA;)V

    return-object v23
.end method

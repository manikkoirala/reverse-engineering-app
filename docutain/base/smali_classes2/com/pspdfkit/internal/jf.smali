.class public final Lcom/pspdfkit/internal/jf;
.super Lcom/pspdfkit/internal/qd;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/qd;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/qd;->b(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/HighlightAnnotation;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->markAsInstantCommentRoot()V

    :goto_0
    return-object p1
.end method

.method protected final a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 0

    .line 9
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/bi;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/specialMode/handler/a;)V

    .line 12
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method protected final b(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/HighlightAnnotation;
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/qd;->b(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/HighlightAnnotation;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->markAsInstantCommentRoot()V

    return-object p1
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

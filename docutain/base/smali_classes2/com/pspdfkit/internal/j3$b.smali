.class public final enum Lcom/pspdfkit/internal/j3$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/j3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/j3$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/j3$b;

.field public static final enum b:Lcom/pspdfkit/internal/j3$b;

.field public static final enum c:Lcom/pspdfkit/internal/j3$b;

.field public static final enum d:Lcom/pspdfkit/internal/j3$b;

.field private static final synthetic e:[Lcom/pspdfkit/internal/j3$b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/j3$b;

    const-string v1, "PLAYING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/j3$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/j3$b;->a:Lcom/pspdfkit/internal/j3$b;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/j3$b;

    const-string v3, "PAUSED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/j3$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/j3$b;->b:Lcom/pspdfkit/internal/j3$b;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/j3$b;

    const-string v5, "STOPPED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/j3$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/j3$b;->c:Lcom/pspdfkit/internal/j3$b;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/j3$b;

    const-string v7, "RELEASED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/j3$b;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/j3$b;->d:Lcom/pspdfkit/internal/j3$b;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/pspdfkit/internal/j3$b;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    .line 6
    sput-object v7, Lcom/pspdfkit/internal/j3$b;->e:[Lcom/pspdfkit/internal/j3$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/j3$b;
    .locals 1

    const-class v0, Lcom/pspdfkit/internal/j3$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/j3$b;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/j3$b;
    .locals 1

    sget-object v0, Lcom/pspdfkit/internal/j3$b;->e:[Lcom/pspdfkit/internal/j3$b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/j3$b;

    return-object v0
.end method

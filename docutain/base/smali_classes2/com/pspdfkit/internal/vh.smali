.class final Lcom/pspdfkit/internal/vh;
.super Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/kg;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/internal/kg;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mailParams"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/document/sharing/DefaultDocumentSharingController;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    return-void
.end method


# virtual methods
.method public final onDocumentPrepared(Landroid/net/Uri;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "shareUri"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-static {v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v3

    if-nez v3, :cond_1

    return-void

    .line 5
    :cond_1
    new-instance v4, Landroidx/core/app/ShareCompat$IntentBuilder;

    invoke-direct {v4, v3}, Landroidx/core/app/ShareCompat$IntentBuilder;-><init>(Landroid/content/Context;)V

    const-string v3, "application/pdf"

    .line 6
    invoke-virtual {v4, v3}, Landroidx/core/app/ShareCompat$IntentBuilder;->setType(Ljava/lang/String;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v3

    .line 7
    invoke-virtual {v3, v1}, Landroidx/core/app/ShareCompat$IntentBuilder;->addStream(Landroid/net/Uri;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 8
    iget-object v3, v0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/kg;->e()Ljava/lang/String;

    move-result-object v4

    const-string v3, ";"

    const/4 v10, 0x0

    if-eqz v4, :cond_2

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    new-array v5, v10, [Ljava/lang/String;

    .line 26
    invoke-interface {v4, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    .line 27
    check-cast v4, [Ljava/lang/String;

    if-nez v4, :cond_3

    :cond_2
    new-array v4, v10, [Ljava/lang/String;

    .line 47
    :cond_3
    invoke-virtual {v1, v4}, Landroidx/core/app/ShareCompat$IntentBuilder;->addEmailTo([Ljava/lang/String;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 48
    iget-object v4, v0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/kg;->a()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_4

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    new-array v5, v10, [Ljava/lang/String;

    .line 68
    invoke-interface {v4, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    .line 69
    check-cast v4, [Ljava/lang/String;

    if-nez v4, :cond_5

    :cond_4
    new-array v4, v10, [Ljava/lang/String;

    .line 91
    :cond_5
    invoke-virtual {v1, v4}, Landroidx/core/app/ShareCompat$IntentBuilder;->addEmailBcc([Ljava/lang/String;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 92
    iget-object v4, v0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/kg;->b()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_6

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_6

    new-array v4, v10, [Ljava/lang/String;

    .line 114
    invoke-interface {v3, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .line 115
    check-cast v3, [Ljava/lang/String;

    if-nez v3, :cond_7

    :cond_6
    new-array v3, v10, [Ljava/lang/String;

    .line 139
    :cond_7
    invoke-virtual {v1, v3}, Landroidx/core/app/ShareCompat$IntentBuilder;->addEmailCc([Ljava/lang/String;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 140
    iget-object v3, v0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/kg;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    if-nez v3, :cond_8

    move-object v3, v4

    :cond_8
    invoke-virtual {v1, v3}, Landroidx/core/app/ShareCompat$IntentBuilder;->setSubject(Ljava/lang/String;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 141
    iget-object v3, v0, Lcom/pspdfkit/internal/vh;->a:Lcom/pspdfkit/internal/kg;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/kg;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    goto :goto_0

    :cond_9
    move-object v4, v3

    :goto_0
    invoke-virtual {v1, v4}, Landroidx/core/app/ShareCompat$IntentBuilder;->setText(Ljava/lang/CharSequence;)Landroidx/core/app/ShareCompat$IntentBuilder;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Landroidx/core/app/ShareCompat$IntentBuilder;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "IntentBuilder(activity)\n\u2026: \"\")\n            .intent"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "android.intent.action.SENDTO"

    .line 146
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "mailto:"

    .line 147
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v3, 0x0

    .line 149
    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 150
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

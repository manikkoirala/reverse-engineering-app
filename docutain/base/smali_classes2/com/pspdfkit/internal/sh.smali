.class public final Lcom/pspdfkit/internal/sh;
.super Lcom/pspdfkit/internal/im$h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/sh$b;
    }
.end annotation


# instance fields
.field private final d:I

.field private e:Lio/reactivex/rxjava3/disposables/Disposable;

.field private f:Lcom/pspdfkit/internal/yh;

.field private g:Lcom/pspdfkit/internal/yh;

.field private final h:Landroid/graphics/Paint;

.field private i:Z

.field private j:Z

.field private final k:I

.field private final l:Landroid/graphics/Rect;


# direct methods
.method public static synthetic $r8$lambda$6EPoyIT7TZ2q7e8yLcArDM_tGCk(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/rc;)Lorg/reactivestreams/Publisher;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/sh;->c(Lcom/pspdfkit/internal/rc;)Lorg/reactivestreams/Publisher;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$96ef5B-t3FQU2RwJNe2sUsMeujE(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/sh$b;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/sh;->b(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/sh$b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$NFIfj2p7uHKVWZYvnuL55BwxozE(Lcom/pspdfkit/internal/sh;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/sh;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$O9yH8f7q2BrCexd47xsQrmWZh7Y(Lcom/pspdfkit/internal/rc;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/sh;->b(Lcom/pspdfkit/internal/rc;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$f5q7zlXeXBU8j8XMiCXvLeQwAvE(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lorg/reactivestreams/Publisher;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/sh;->a(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lorg/reactivestreams/Publisher;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$fROIvea7dQlfk7XMrHHnoyBR5Jo(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/sh;->c(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$tioJrr29JN_BHFghoGusZK_yTi0(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)Lcom/pspdfkit/internal/rc;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/sh;->b(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)Lcom/pspdfkit/internal/rc;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/sh;)Lcom/pspdfkit/internal/yh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/sh;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/sh;->i:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/yh;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputg(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/yh;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputi(Lcom/pspdfkit/internal/sh;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/sh;->i:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/sh;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/sh;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im$h;-><init>(Lcom/pspdfkit/internal/im;)V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->h:Landroid/graphics/Paint;

    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sh;->i:Z

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->l:Landroid/graphics/Rect;

    .line 29
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v1

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v2

    .line 30
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ga;->a(IZZ)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/sh;->d:I

    .line 33
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getFixedLowResRenderPixelCount()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 35
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    .line 37
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 38
    invoke-static {p1}, Lcom/pspdfkit/internal/e8;->b(Landroid/content/Context;)I

    move-result p1

    :goto_0
    iput p1, p0, Lcom/pspdfkit/internal/sh;->k:I

    return-void
.end method

.method static synthetic a(Lcom/pspdfkit/internal/sh;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method private static a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1

    .line 40
    new-instance v0, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/rc;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)Ljava/util/concurrent/Callable;
    .locals 1

    .line 39
    new-instance v0, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)V

    return-object v0
.end method

.method private synthetic a(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lorg/reactivestreams/Publisher;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 34
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/sh;->d(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/Runnable;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    if-eqz p1, :cond_0

    .line 38
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private static synthetic b(Lcom/pspdfkit/internal/rc;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yl;->b()Lcom/pspdfkit/internal/sc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/sc;->a(Lcom/pspdfkit/internal/rc;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic b(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)Lcom/pspdfkit/internal/rc;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cg;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cg;->b()I

    move-result p0

    invoke-virtual {v0, v1, p0}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 18
    new-instance v0, Lcom/pspdfkit/internal/rc$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->d()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object v0

    if-eqz p2, :cond_0

    const/16 p2, 0xf

    goto :goto_0

    :cond_0
    const/4 p2, 0x5

    .line 20
    :goto_0
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 21
    invoke-virtual {p2, p0}, Lcom/pspdfkit/internal/g4$a;->a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 22
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 23
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p0

    invoke-virtual {p2, p0}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->f()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/g4$a;->b(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->e()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->i()Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->b()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g4$a;->a(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic b(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/sh$b;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/pspdfkit/internal/yh;

    iget-object v1, p0, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/pspdfkit/internal/g4;->e:I

    iget v3, p0, Lcom/pspdfkit/internal/g4;->f:I

    invoke-direct {v0, v2, v3, v1}, Lcom/pspdfkit/internal/yh;-><init>(IILandroid/graphics/Bitmap;)V

    .line 32
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 33
    monitor-enter v1

    .line 34
    :try_start_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 35
    new-instance v3, Landroid/graphics/Rect;

    .line 36
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x0

    .line 37
    invoke-virtual {v2, p1, v4, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 39
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    new-instance p1, Lcom/pspdfkit/internal/sh$b;

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/sh$b;-><init>(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yh;)V

    return-object p1

    :catchall_0
    move-exception p0

    .line 41
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/sh;Ljava/lang/Runnable;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {p1, v0}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static synthetic c(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 135
    new-instance v0, Lcom/pspdfkit/internal/sh$b;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/sh$b;-><init>(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method private synthetic c(Lcom/pspdfkit/internal/rc;)Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 132
    invoke-static {p1}, Lcom/pspdfkit/internal/sh;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/rc;)V

    .line 133
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->flatMapPublisher(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 134
    invoke-static {p1}, Lcom/pspdfkit/internal/sh;->d(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->toFlowable()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Flowable;->switchIfEmpty(Lorg/reactivestreams/Publisher;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method private static d(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/Flowable;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/rc;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->toFlowable()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/pspdfkit/internal/g4;->e:I

    if-ne v1, v2, :cond_0

    .line 16
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    iget v1, p0, Lcom/pspdfkit/internal/g4;->f:I

    if-eq p1, v1, :cond_1

    .line 17
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/internal/sh;->d(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-virtual {v0, p0}, Lio/reactivex/rxjava3/core/Flowable;->concatWith(Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private static d(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;
    .locals 2

    .line 18
    invoke-static {p0}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/rc;)V

    .line 19
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_3

    .line 8
    monitor-enter p0

    .line 11
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sh;->j:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 19
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 24
    iget-object v3, p0, Lcom/pspdfkit/internal/sh;->l:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v3, v2, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/sh;->l:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/pspdfkit/internal/sh;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 p1, 0x1

    return p1

    .line 29
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/sh;->d:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    return v2

    :catchall_0
    move-exception p1

    .line 30
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 31
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Trying to draw the LowResSubview while the State is not initialized, meaning that the view was never bound to the page, or already recycled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sh;->j:Z

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/sh;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x0

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 10
    new-instance v2, Lcom/pspdfkit/internal/cg;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v3

    iget v3, v3, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v3, v3

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v4

    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Lcom/pspdfkit/internal/cg;-><init>(II)V

    .line 12
    iget v3, p0, Lcom/pspdfkit/internal/sh;->k:I

    const-string v4, "pageViewSize"

    .line 13
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "<this>"

    .line 14
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->c()I

    move-result v4

    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->b()I

    move-result v5

    mul-int v5, v5, v4

    if-eqz v5, :cond_3

    if-nez v3, :cond_1

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->c()I

    move-result v4

    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->b()I

    move-result v5

    mul-int v5, v5, v4

    if-gt v5, v3, :cond_2

    goto :goto_1

    :cond_2
    int-to-float v3, v3

    int-to-float v4, v5

    div-float/2addr v3, v4

    float-to-double v3, v3

    .line 64
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v3, v3

    const-string v4, "<this>"

    .line 65
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v4, Lcom/pspdfkit/internal/cg;

    .line 100
    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->c()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v3

    float-to-int v5, v5

    .line 101
    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->b()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v3

    float-to-int v2, v2

    .line 102
    invoke-direct {v4, v5, v2}, Lcom/pspdfkit/internal/cg;-><init>(II)V

    move-object v2, v4

    goto :goto_1

    .line 103
    :cond_3
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/cg;->a()Lcom/pspdfkit/internal/cg;

    move-result-object v2

    .line 104
    :goto_1
    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->c()I

    move-result v3

    if-lez v3, :cond_7

    invoke-virtual {v2}, Lcom/pspdfkit/internal/cg;->b()I

    move-result v3

    if-gtz v3, :cond_4

    goto :goto_3

    .line 110
    :cond_4
    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v3, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    if-eqz v3, :cond_5

    .line 112
    iput-object v3, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;

    .line 113
    iput-object v1, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    .line 115
    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v3, 0xf

    goto :goto_2

    :cond_6
    const/4 v3, 0x5

    .line 121
    :goto_2
    invoke-static {v2, v0, v1}, Lcom/pspdfkit/internal/sh;->a(Lcom/pspdfkit/internal/cg;Lcom/pspdfkit/internal/dm$e;Z)Ljava/util/concurrent/Callable;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 124
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/sh$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/sh;)V

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->flatMapPublisher(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 129
    new-instance v1, Lcom/pspdfkit/internal/sh$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/sh$a;-><init>(Lcom/pspdfkit/internal/sh;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->subscribeWith(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object v0

    check-cast v0, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void

    :catchall_0
    move-exception v0

    .line 130
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    :goto_3
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Failed to render low-res page image: 0 size bitmap is not possible."

    .line 131
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized recycle()V
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-super {p0}, Lcom/pspdfkit/internal/im$h;->recycle()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sh;->i:Z

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sh;->j:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yh;->c()V

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->f:Lcom/pspdfkit/internal/yh;

    .line 13
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;

    if-eqz v1, :cond_1

    .line 14
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yh;->c()V

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/internal/sh;->g:Lcom/pspdfkit/internal/yh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

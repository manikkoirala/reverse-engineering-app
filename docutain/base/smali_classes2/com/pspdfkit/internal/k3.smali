.class public final Lcom/pspdfkit/internal/k3;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/k3$a;,
        Lcom/pspdfkit/internal/k3$b;
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private c:Lcom/pspdfkit/internal/k3$b;

.field private final d:F

.field private final e:I

.field private f:Z

.field private g:Lcom/pspdfkit/internal/k3$a;

.field private h:J

.field private i:Ljava/lang/Thread;

.field private j:Ljava/nio/ByteBuffer;

.field private final k:Lio/reactivex/rxjava3/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/PublishSubject<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$UmQPqa2fwDNu79VkjrcZ2duuyhQ(Lcom/pspdfkit/internal/k3;Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3;Lcom/pspdfkit/annotations/SoundAnnotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fjxPl61M70SwCdvMaoz2UP-eGyE(Lcom/pspdfkit/internal/k3;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3;)V

    return-void
.end method

.method public synthetic constructor <init>(I)V
    .locals 1

    const/16 p1, 0x5622

    const v0, 0x493e0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/k3;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/k3;->a:I

    .line 4
    iput p2, p0, Lcom/pspdfkit/internal/k3;->b:I

    int-to-float p2, p1

    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p2, v0

    const/4 v0, 0x2

    int-to-float v1, v0

    mul-float p2, p2, v1

    .line 45
    iput p2, p0, Lcom/pspdfkit/internal/k3;->d:F

    const/16 p2, 0x10

    .line 48
    invoke-static {p1, p2, v0}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result p2

    const/4 v1, -0x2

    if-eq p2, v1, :cond_0

    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    goto :goto_0

    :cond_0
    mul-int/lit8 p2, p1, 0x2

    .line 49
    :goto_0
    iput p2, p0, Lcom/pspdfkit/internal/k3;->e:I

    .line 62
    sget-object p1, Lcom/pspdfkit/internal/k3$a;->b:Lcom/pspdfkit/internal/k3$a;

    iput-object p1, p0, Lcom/pspdfkit/internal/k3;->g:Lcom/pspdfkit/internal/k3$a;

    .line 66
    invoke-direct {p0}, Lcom/pspdfkit/internal/k3;->a()Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    .line 68
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object p1

    const-string p2, "create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/k3;->k:Lio/reactivex/rxjava3/subjects/PublishSubject;

    return-void
.end method

.method private final a()Ljava/nio/ByteBuffer;
    .locals 3

    .line 146
    iget v0, p0, Lcom/pspdfkit/internal/k3;->d:F

    iget v1, p0, Lcom/pspdfkit/internal/k3;->b:I

    int-to-float v1, v1

    mul-float v0, v0, v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 147
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    const-string v2, "nativeOrder()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    const-string v1, "allocateDirect((bytesPer\u2026tRecordedDataByteOrder())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final declared-synchronized a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V
    .locals 1

    monitor-enter p0

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->g:Lcom/pspdfkit/internal/k3$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    monitor-exit p0

    return-void

    .line 144
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/pspdfkit/internal/k3;->g:Lcom/pspdfkit/internal/k3$a;

    .line 145
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->c:Lcom/pspdfkit/internal/k3$b;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/k3$b;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private static final a(Lcom/pspdfkit/internal/k3;)V
    .locals 8

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, -0x10

    .line 3
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 6
    iget v0, p0, Lcom/pspdfkit/internal/k3;->e:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 7
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    const-string v2, "nativeOrder()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 12
    :try_start_0
    new-instance v7, Landroid/media/AudioRecord;

    const/4 v2, 0x0

    .line 14
    iget v3, p0, Lcom/pspdfkit/internal/k3;->a:I

    const/16 v4, 0x10

    const/4 v5, 0x2

    .line 17
    iget v6, p0, Lcom/pspdfkit/internal/k3;->e:I

    move-object v1, v7

    .line 18
    invoke-direct/range {v1 .. v6}, Landroid/media/AudioRecord;-><init>(IIIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 29
    invoke-virtual {v7}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 30
    sget-object v0, Lcom/pspdfkit/internal/k3$a;->d:Lcom/pspdfkit/internal/k3$a;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not initialize audio recording"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 35
    :cond_0
    invoke-virtual {v7}, Landroid/media/AudioRecord;->startRecording()V

    .line 36
    invoke-virtual {v7}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_1

    .line 37
    sget-object v0, Lcom/pspdfkit/internal/k3$a;->d:Lcom/pspdfkit/internal/k3$a;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not start audio recording"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 42
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/pspdfkit/internal/k3;->c()I

    move-result v1

    int-to-long v5, v1

    sub-long/2addr v3, v5

    iput-wide v3, p0, Lcom/pspdfkit/internal/k3;->h:J

    .line 45
    sget-object v1, Lcom/pspdfkit/internal/k3$a;->a:Lcom/pspdfkit/internal/k3$a;

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0, v1, v3}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    .line 47
    iget-object v1, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    .line 51
    :cond_2
    :goto_0
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 52
    :try_start_2
    iget-boolean v4, p0, Lcom/pspdfkit/internal/k3;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit p0

    const/4 v5, 0x0

    if-eqz v4, :cond_6

    .line 53
    invoke-virtual {v1}, Ljava/nio/Buffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_2

    .line 59
    :cond_3
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 60
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x17

    if-lt v4, v6, :cond_4

    const/4 v5, 0x1

    :cond_4
    if-eqz v5, :cond_5

    .line 61
    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v4

    invoke-virtual {v7, v0, v4, v2}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;II)I

    move-result v4

    goto :goto_1

    .line 63
    :cond_5
    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v4

    invoke-virtual {v7, v0, v4}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v4

    .line 66
    :goto_1
    invoke-virtual {v1}, Ljava/nio/Buffer;->remaining()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-lez v4, :cond_2

    .line 68
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 71
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 72
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 75
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 76
    iget-object v4, p0, Lcom/pspdfkit/internal/k3;->k:Lio/reactivex/rxjava3/subjects/PublishSubject;

    invoke-virtual {v4, v0}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    .line 80
    :goto_2
    invoke-virtual {v7}, Landroid/media/AudioRecord;->stop()V

    .line 81
    invoke-virtual {v7}, Landroid/media/AudioRecord;->release()V

    if-eqz v2, :cond_7

    .line 85
    sget-object v0, Lcom/pspdfkit/internal/k3$a;->c:Lcom/pspdfkit/internal/k3$a;

    .line 86
    invoke-direct {p0, v0, v3}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 87
    :cond_7
    sget-object v0, Lcom/pspdfkit/internal/k3$a;->b:Lcom/pspdfkit/internal/k3$a;

    .line 88
    invoke-direct {p0, v0, v3}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p0

    .line 89
    invoke-virtual {v7}, Landroid/media/AudioRecord;->stop()V

    .line 90
    invoke-virtual {v7}, Landroid/media/AudioRecord;->release()V

    throw p0

    :catchall_2
    move-exception v0

    .line 91
    sget-object v1, Lcom/pspdfkit/internal/k3$a;->d:Lcom/pspdfkit/internal/k3$a;

    invoke-direct {p0, v1, v0}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    :goto_3
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/k3;Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 9

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$soundAnnotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->position()I

    move-result v0

    if-lez v0, :cond_2

    .line 100
    monitor-enter p0

    const/4 v0, 0x0

    .line 101
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 102
    iget-object v1, p0, Lcom/pspdfkit/internal/k3;->i:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v2, v3}, Ljava/lang/Thread;->join(J)V

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 108
    iget-object v1, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->limit()I

    move-result v1

    new-array v3, v1, [B

    .line 109
    iget-object v2, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 110
    iget-object v2, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 111
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    const-string v4, "nativeOrder()"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "byteArray"

    .line 113
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_1

    .line 133
    aget-byte v2, v3, v0

    add-int/lit8 v4, v0, 0x1

    .line 134
    aget-byte v5, v3, v4

    .line 135
    aput-byte v5, v3, v0

    .line 136
    aput-byte v2, v3, v4

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 137
    :cond_1
    new-instance v0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    sget-object v4, Lcom/pspdfkit/annotations/sound/AudioEncoding;->SIGNED:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    iget v5, p0, Lcom/pspdfkit/internal/k3;->a:I

    const/16 v6, 0x10

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;-><init>([BLcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V

    .line 138
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/SoundAnnotation;->setAudioSource(Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    .line 139
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 140
    monitor-exit p0

    throw p1

    .line 141
    :cond_2
    :goto_1
    sget-object p1, Lcom/pspdfkit/internal/k3$a;->e:Lcom/pspdfkit/internal/k3$a;

    const/4 v0, 0x0

    .line 142
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/k3;->a(Lcom/pspdfkit/internal/k3$a;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/SoundAnnotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    const-string v0, "soundAnnotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/pspdfkit/internal/k3$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/k3$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/k3;Lcom/pspdfkit/annotations/SoundAnnotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 98
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v0, "fromAction {\n           \u2026Scheduler.PRIORITY_HIGH))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/k3$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/k3;->c:Lcom/pspdfkit/internal/k3$b;

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 1
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/k3;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 4

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->g:Lcom/pspdfkit/internal/k3$a;

    sget-object v1, Lcom/pspdfkit/internal/k3$a;->a:Lcom/pspdfkit/internal/k3$a;

    if-ne v0, v1, :cond_0

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/pspdfkit/internal/k3;->h:J

    sub-long/2addr v0, v2

    long-to-int v1, v0

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->position()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/k3;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    div-float/2addr v0, v1

    float-to-int v1, v0

    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/k3;->b:I

    return v0
.end method

.method public final e()Lio/reactivex/rxjava3/core/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->k:Lio/reactivex/rxjava3/subjects/PublishSubject;

    sget-object v1, Lio/reactivex/rxjava3/core/BackpressureStrategy;->LATEST:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->toFlowable(Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    const-string v1, "visualizerSubject.toFlow\u2026kpressureStrategy.LATEST)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 1
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/k3;->g:Lcom/pspdfkit/internal/k3$a;

    sget-object v1, Lcom/pspdfkit/internal/k3$a;->b:Lcom/pspdfkit/internal/k3$a;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/k3;->f:Z

    .line 4
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/pspdfkit/internal/k3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/k3$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/k3;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/k3;->i:Ljava/lang/Thread;

    .line 5
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

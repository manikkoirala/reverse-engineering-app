.class public final Lcom/pspdfkit/internal/dm;
.super Lcom/pspdfkit/internal/en;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/dm$d;,
        Lcom/pspdfkit/internal/dm$b;,
        Lcom/pspdfkit/internal/dm$e;,
        Lcom/pspdfkit/internal/dm$c;
    }
.end annotation


# instance fields
.field private A:Lio/reactivex/rxjava3/disposables/Disposable;

.field private B:Z

.field private C:Z

.field private D:Lcom/pspdfkit/internal/a1;

.field private E:Landroid/view/View$OnKeyListener;

.field private F:Lcom/pspdfkit/internal/ul;

.field private G:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

.field private e:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private f:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private g:Lcom/pspdfkit/internal/w0;

.field private h:Lcom/pspdfkit/internal/yb;

.field private i:Lcom/pspdfkit/internal/dm$c;

.field private j:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/overlay/OverlayViewProvider;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/pspdfkit/internal/am;

.field private m:Lcom/pspdfkit/internal/tb;

.field private n:Lcom/pspdfkit/internal/yi;

.field private o:Lcom/pspdfkit/internal/wc;

.field private p:Lcom/pspdfkit/internal/w1;

.field private q:Lcom/pspdfkit/internal/il;

.field private final r:Lcom/pspdfkit/internal/dm$d;

.field private final s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private final t:Landroid/graphics/Rect;

.field private u:Z

.field private v:Lcom/pspdfkit/internal/dm$e;

.field private w:Lcom/pspdfkit/internal/oh;

.field private x:Lcom/pspdfkit/internal/im;

.field private y:Lcom/pspdfkit/internal/os;

.field private z:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$0xd-6SQ5c9ZcDNcbIDHczIASZ-s(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/forms/SignatureFormElement;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/forms/SignatureFormElement;)V

    return-void
.end method

.method public static synthetic $r8$lambda$59Gtsv7E1QPsyyXVgXbri3hVncM(Lcom/pspdfkit/internal/dm;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->b(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$8qEQuL3-3PaDczwovTDSb2PR7UE(Ljava/util/List;)Ljava/lang/Iterable;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/dm;->c(Ljava/util/List;)Ljava/lang/Iterable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$HVvRQEgoVhZzHHMnZiK_u53sKWE(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->b(Landroid/view/MotionEvent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$N1oJCDq7jMMofzkM-KcGmYmL2Yw(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/dm;->a(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$hFHacdo9Ja7MCZ2r6_vHM40EPNU(Lcom/pspdfkit/internal/dm;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$m6lRKeF2KDHKEnZDfOcsMQRhwTw(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/dm;->b(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$va1WioIpmRAiHK1eopLtrtcoSbA(Lcom/pspdfkit/internal/dm;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->d(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$x4dkbVZGTYo325jzQTHKoMTii0E(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/forms/FormElement;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/forms/FormElement;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetC(Lcom/pspdfkit/internal/dm;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/dm;->C:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/dm$c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm;->i:Lcom/pspdfkit/internal/dm$c;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/am;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/tb;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetp(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/w1;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetw(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/oh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputB(Lcom/pspdfkit/internal/dm;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/dm;->B:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    const/4 p2, 0x0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/dm$d;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/dm$d;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/dm$d-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->r:Lcom/pspdfkit/internal/dm$d;

    .line 9
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 13
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->t:Landroid/graphics/Rect;

    const/4 p1, 0x0

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/internal/dm;->u:Z

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 194
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isSignature()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->h()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/dm;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/forms/SignatureFormElement;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 201
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 202
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->onFormElementUpdated(Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    .line 173
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->setDrawableProviders(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 195
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    if-ne v0, v1, :cond_0

    .line 196
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private static synthetic b(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    return-object p0
.end method

.method private b(Landroid/view/MotionEvent;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 8
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 p1, 0x0

    .line 9
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 10
    iget v1, v0, Landroid/graphics/PointF;->x:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, p1, v2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 11
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "PageLayout touched at (%.2f, %.2f)"

    invoke-static {v0, v1, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private synthetic b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ul;->setCurrentOverlayViewProviders(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private static synthetic c(Ljava/util/List;)Ljava/lang/Iterable;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-object p0
.end method

.method private synthetic d(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/a1;->a(Ljava/util/List;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/yi;->a(Ljava/util/List;)V

    return-void
.end method

.method private f()Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    goto :goto_0

    .line 8
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/dm$e;)Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-static {v1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v1

    .line 10
    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda3;

    invoke-direct {v1}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda3;-><init>()V

    .line 11
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->onErrorResumeNext(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->l()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private h()Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Lcom/pspdfkit/forms/SignatureFormElement;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/dm$e;)Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/forms/FormProvider;->getFormElementsAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda0;-><init>()V

    .line 5
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->flattenAsObservable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/dm;)V

    .line 6
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 8
    const-class v1, Lcom/pspdfkit/forms/SignatureFormElement;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda2;-><init>()V

    .line 9
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->onErrorResumeNext(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 10
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0

    .line 12
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method private l()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "-",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/dm;)V

    return-object v0
.end method

.method private setDrawableProviders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/im;->setDrawableProviders(Ljava/util/List;)V

    return-void

    .line 3
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "State can only be accessed after the page has been bound using bindPage()."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->e:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v0

    invoke-virtual {v1, v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    :goto_0
    return-object p1
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 7

    .line 227
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v3

    .line 228
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v1

    const-wide/16 v4, 0xc8

    const/4 v6, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/os;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/os;->a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/os;->a(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/internal/i2;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/dm$c;Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/il;)V
    .locals 14

    move-object v9, p0

    move-object/from16 v10, p2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v11

    if-eqz v11, :cond_0

    move-object v0, p1

    .line 5
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->e:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 6
    iput-object v10, v9, Lcom/pspdfkit/internal/dm;->f:Lcom/pspdfkit/configuration/PdfConfiguration;

    move-object/from16 v4, p3

    .line 7
    iput-object v4, v9, Lcom/pspdfkit/internal/dm;->g:Lcom/pspdfkit/internal/w0;

    move-object/from16 v12, p5

    .line 8
    iput-object v12, v9, Lcom/pspdfkit/internal/dm;->h:Lcom/pspdfkit/internal/yb;

    move-object/from16 v0, p9

    .line 9
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->i:Lcom/pspdfkit/internal/dm$c;

    move-object/from16 v0, p10

    .line 10
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->j:Lcom/pspdfkit/internal/fm;

    move-object/from16 v0, p11

    .line 11
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->k:Lcom/pspdfkit/internal/fm;

    move-object/from16 v0, p13

    .line 12
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->q:Lcom/pspdfkit/internal/il;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/a1;

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static/range {p2 .. p2}, Lcom/pspdfkit/internal/x5;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Ljava/util/EnumSet;

    move-result-object v2

    .line 15
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__min_editable_annotation_touch_size:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 16
    invoke-direct {v0, v11, v1, v2}, Lcom/pspdfkit/internal/a1;-><init>(Lcom/pspdfkit/internal/zf;ILjava/util/EnumSet;)V

    .line 17
    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    .line 19
    new-instance v13, Lcom/pspdfkit/internal/am;

    iget-object v7, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    .line 27
    invoke-static {}, Lcom/pspdfkit/internal/x5;->a()Lcom/pspdfkit/internal/f2;

    move-result-object v8

    move-object v0, v13

    move-object v1, p0

    move-object v2, v11

    move-object/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p7

    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/am;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/a1;Lcom/pspdfkit/internal/f2;)V

    iput-object v13, v9, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    .line 28
    new-instance v8, Lcom/pspdfkit/internal/tb;

    iget-object v7, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    move-object v0, v8

    move-object/from16 v4, p8

    move-object/from16 v5, p5

    move-object/from16 v6, p12

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/tb;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/ac;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V

    iput-object v8, v9, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    .line 36
    new-instance v6, Lcom/pspdfkit/internal/yi;

    iget-object v5, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    move-object v0, v6

    move-object/from16 v4, p12

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/yi;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V

    iput-object v6, v9, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/wc;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/wc;-><init>(Landroid/content/Context;)V

    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    .line 38
    new-instance v0, Lcom/pspdfkit/internal/w1;

    move-object/from16 v1, p6

    invoke-direct {v0, p0, v10, v1}, Lcom/pspdfkit/internal/w1;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i2;)V

    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    .line 40
    new-instance v0, Lcom/pspdfkit/internal/ul;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/internal/ul;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/dm;)V

    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    .line 42
    invoke-static {v11, v10}, Lcom/pspdfkit/internal/x5;->c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object v0

    iput-object v0, v9, Lcom/pspdfkit/internal/dm;->G:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    const/4 v0, 0x0

    .line 45
    invoke-virtual {p0, v0}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 46
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setSaveFromParentEnabled(Z)V

    .line 50
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 54
    new-instance v1, Lcom/pspdfkit/ui/RecyclableFrameLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/ui/RecyclableFrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, -0x1

    .line 55
    invoke-virtual {p0, v1, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->j()V

    .line 59
    new-instance v3, Lcom/pspdfkit/internal/im;

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->r:Lcom/pspdfkit/internal/dm$d;

    iget-object v5, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    move-object/from16 p3, v3

    move-object/from16 p4, p0

    move-object/from16 p5, v4

    move-object/from16 p6, p2

    move-object/from16 p7, p12

    move-object/from16 p8, v5

    invoke-direct/range {p3 .. p8}, Lcom/pspdfkit/internal/im;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/im$e;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V

    iput-object v3, v9, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    .line 60
    invoke-virtual {v1, v3, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 64
    new-instance v1, Lcom/pspdfkit/internal/os;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->D:Lcom/pspdfkit/internal/a1;

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/os;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/a1;)V

    iput-object v1, v9, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    .line 65
    invoke-virtual {p0, v1, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 68
    new-instance v1, Lcom/pspdfkit/internal/oh;

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 70
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v4

    .line 71
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result v5

    .line 72
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v6

    .line 73
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v7

    move-object p1, v1

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    invoke-direct/range {p1 .. p6}, Lcom/pspdfkit/internal/oh;-><init>(Landroid/content/Context;Ljava/lang/Integer;IZZ)V

    iput-object v1, v9, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    .line 74
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/oh;->b()V

    .line 77
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 80
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    sget-object v2, Lcom/pspdfkit/internal/vc;->a:Lcom/pspdfkit/internal/vc;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/pspdfkit/internal/xc;

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    .line 82
    invoke-virtual {v4}, Lcom/pspdfkit/internal/im;->getGestureReceiver()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    .line 83
    invoke-virtual {v4}, Lcom/pspdfkit/internal/tb;->c()Lcom/pspdfkit/internal/tb$b;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    .line 84
    invoke-virtual {v4}, Lcom/pspdfkit/internal/am;->c()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    const/4 v6, 0x2

    aput-object v4, v3, v6

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    .line 85
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yi;->b()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    const/4 v7, 0x3

    aput-object v4, v3, v7

    new-instance v4, Lcom/pspdfkit/internal/dm$b;

    const/4 v8, 0x0

    invoke-direct {v4, p0, v8}, Lcom/pspdfkit/internal/dm$b;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/dm$b-IA;)V

    const/4 v8, 0x4

    aput-object v4, v3, v8

    .line 86
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    .line 93
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    sget-object v2, Lcom/pspdfkit/internal/vc;->b:Lcom/pspdfkit/internal/vc;

    new-array v3, v5, [Lcom/pspdfkit/internal/xc;

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/am;->c()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    .line 94
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    sget-object v2, Lcom/pspdfkit/internal/vc;->c:Lcom/pspdfkit/internal/vc;

    new-array v3, v7, [Lcom/pspdfkit/internal/xc;

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    .line 96
    invoke-virtual {v4}, Lcom/pspdfkit/internal/im;->getGestureReceiver()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    .line 97
    invoke-virtual {v4}, Lcom/pspdfkit/internal/tb;->c()Lcom/pspdfkit/internal/tb$b;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    .line 98
    invoke-virtual {v4}, Lcom/pspdfkit/internal/am;->c()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    aput-object v4, v3, v6

    .line 99
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    .line 104
    iget-object v1, v9, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    sget-object v2, Lcom/pspdfkit/internal/vc;->d:Lcom/pspdfkit/internal/vc;

    new-array v3, v5, [Lcom/pspdfkit/internal/xc;

    iget-object v4, v9, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/am;->c()Lcom/pspdfkit/internal/xc;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    return-void

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Document may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/pspdfkit/utils/Size;)V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 177
    :cond_0
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fputb(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/utils/Size;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/utils/Size;IF)V
    .locals 9

    .line 106
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->e:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-nez v1, :cond_4

    if-eqz v0, :cond_3

    .line 113
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->G:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "PageRenderConfiguration may not be null"

    invoke-static {v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 114
    new-instance v8, Lcom/pspdfkit/internal/dm$e;

    iget-object v6, p0, Lcom/pspdfkit/internal/dm;->f:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v7, p0, Lcom/pspdfkit/internal/dm;->G:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-object v1, v8

    move-object v2, v0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/dm$e;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;IFLcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)V

    iput-object v8, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    .line 117
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    const/16 p3, 0x32

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/oh;->a(I)V

    .line 119
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/tb;->b()V

    .line 120
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    iget-object p3, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/im;->a(Lcom/pspdfkit/internal/dm$e;)V

    .line 121
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    iget-object p3, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/dm$e;)V

    .line 123
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 124
    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->f()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p3

    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->l()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-virtual {p3, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p3

    invoke-virtual {p3}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p3

    .line 125
    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 129
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p3

    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->q:Lcom/pspdfkit/internal/il;

    invoke-virtual {p1, p3, v1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/internal/il;)V

    .line 131
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->j:Lcom/pspdfkit/internal/fm;

    if-eqz p1, :cond_1

    .line 133
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/fm;->b(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 134
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda5;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/dm;)V

    .line 135
    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 147
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->k:Lcom/pspdfkit/internal/fm;

    if-eqz p1, :cond_2

    iget-object p3, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    if-eqz p3, :cond_2

    .line 149
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/fm;->b(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 150
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda6;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/dm;)V

    .line 151
    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->A:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 157
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    iget-object p3, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/ul;->a(Lcom/pspdfkit/internal/dm$e;)V

    .line 161
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->g:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 162
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->g:Lcom/pspdfkit/internal/w0;

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 165
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->h:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    .line 166
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->h:Lcom/pspdfkit/internal/yb;

    check-cast p1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ac;->addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    .line 167
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 168
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 169
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/zf;I)V

    return-void

    .line 170
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "documentView.getDocument() may not return null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 171
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "You must call recycle() on this view before binding another page."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Z)V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_3

    .line 180
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->j()V

    if-nez p1, :cond_0

    .line 181
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm;->u:Z

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/im;->b(Z)V

    .line 183
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    .line 184
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->l()V

    .line 185
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    .line 186
    iget-object p1, p1, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    .line 187
    invoke-virtual {p1}, Lcom/pspdfkit/internal/j1;->d()V

    .line 188
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ul;->c()V

    .line 189
    :cond_1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/dm;->u:Z

    if-nez p1, :cond_2

    .line 190
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yi;->f()V

    goto :goto_0

    .line 192
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yi;->g()V

    :goto_0
    return-void

    .line 193
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "State can only be accessed after the page has been bound using bindPage()."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(ZLcom/pspdfkit/internal/im$c;)V
    .locals 2

    if-eqz p2, :cond_0

    .line 211
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    new-instance v1, Lcom/pspdfkit/internal/dm$a;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/dm$a;-><init>(Lcom/pspdfkit/internal/im$c;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/im;->a(Lcom/pspdfkit/internal/im$c;)V

    .line 222
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/im;->a(Z)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final a(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 3

    .line 203
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    .line 204
    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v0

    .line 205
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    .line 206
    invoke-static {v1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    .line 207
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-virtual {v0, v1, p3}, Lcom/pspdfkit/internal/am;->a(ZZ)Z

    move-result p3

    if-eqz p2, :cond_1

    .line 209
    iget-object p1, p1, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/tb;->b(Landroid/view/MotionEvent;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 210
    :goto_1
    iget-object p2, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    move-result p1

    or-int/2addr p1, p3

    return p1

    :cond_3
    return v2
.end method

.method public final b(II)Landroid/graphics/RectF;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/im;->a(II)Landroid/graphics/RectF;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final b()Z
    .locals 3

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->b()Z

    move-result v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    const/4 v2, 0x0

    .line 6
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    move-result v1

    or-int/2addr v0, v1

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->i:Lcom/pspdfkit/internal/dm$c;

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x0

    :cond_0
    return v0
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->a()V

    return-void
.end method

.method public final d()Z
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->E:Landroid/view/View$OnKeyListener;

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-interface {v1, p0, v0, p1}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm;->u:Z

    return v0
.end method

.method public final focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/im;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    .line 3
    iget-object v1, v1, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    const/4 v2, 0x0

    .line 4
    invoke-virtual {v0, v1, v2, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 10
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Annotation rendering coordinator can only be accessed after the views have been created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFormEditor()Lcom/pspdfkit/internal/tb;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    return-object v0
.end method

.method public getLocalVisibleRect()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->t:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getMediaPlayer()Lcom/pspdfkit/internal/yi;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    return-object v0
.end method

.method public getPageEditor()Lcom/pspdfkit/internal/am;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    return-object v0
.end method

.method public getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->e:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-object v0
.end method

.method public getPdfConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->f:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public getPdfRect()Landroid/graphics/RectF;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/dm$e;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getSpecialModeView()Lcom/pspdfkit/internal/os;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    return-object v0
.end method

.method public getState()Lcom/pspdfkit/internal/dm$e;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State can only be accessed after the page has been bound using bindPage()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->getCurrentMode()Lcom/pspdfkit/internal/em;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/internal/zt;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/zt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zt;->f()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getZoomScale()F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgeth(Lcom/pspdfkit/internal/dm$e;)F

    move-result v0

    return v0
.end method

.method final i()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm;->C:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/im;->b()V

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm;->B:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm;->C:Z

    if-eqz v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/oh;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->i()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/w1;->f()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->i:Lcom/pspdfkit/internal/dm$c;

    if-eqz v0, :cond_0

    .line 8
    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/dm$c;->a(Lcom/pspdfkit/internal/dm;)V

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/tb;->i()V

    :cond_1
    return-void
.end method

.method public final j()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->t:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm;->u:Z

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/ul;->a(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    if-eqz v0, :cond_1

    .line 4
    iget-boolean v1, p0, Lcom/pspdfkit/internal/dm;->u:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/dm;->u:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x20000

    .line 6
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    goto :goto_0

    :cond_0
    const/high16 v0, 0x60000

    .line 8
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/am;->onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/tb;->a(Z)Z

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    .line 2
    invoke-static {v1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->f()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    invoke-direct {p0}, Lcom/pspdfkit/internal/dm;->l()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v1

    .line 5
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->h(Lcom/pspdfkit/annotations/Annotation;)V

    .line 13
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    .line 16
    iget-object v0, v0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    .line 17
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/n2;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p2

    .line 2
    invoke-static {p2}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result p2

    if-ne p1, p2, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->h()Z

    move-result p1

    if-nez p1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/w1;->c(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final onFormElementSelected(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/pspdfkit/internal/am;->a(ZZ)Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/tb;->onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z

    return-void
.end method

.method public final onFormElementUpdated(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/tb;->b(Lcom/pspdfkit/forms/FormElement;)V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/dm$e;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->h(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected final onLayout(ZIIII)V
    .locals 3

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    sub-int v1, p4, p2

    int-to-float v1, v1

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgetb(Lcom/pspdfkit/internal/dm$e;)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    iget v2, v2, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v1, v2

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fgeth(Lcom/pspdfkit/internal/dm$e;)F

    move-result v0

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3727c5ac    # 1.0E-5f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fputh(Lcom/pspdfkit/internal/dm$e;F)V

    .line 7
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/pspdfkit/internal/en;->onLayout(ZIIII)V

    return-void
.end method

.method public final onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public synthetic onPrepareFormElementSelection(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener$-CC;->$default$onPrepareFormElementSelection(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    return p1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 4
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/dm$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;)V

    const-string v2, "PSPDFKit.PdfView"

    invoke-static {v2, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getScaleHandleRadius()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float v0, v0, v2

    float-to-int v0, v0

    .line 14
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-nez v2, :cond_2

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    int-to-float v0, v0

    add-float/2addr v2, v0

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-gez v2, :cond_1

    .line 17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    add-float/2addr v2, v0

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 18
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-ltz v0, :cond_2

    :cond_1
    return v1

    .line 23
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    .line 24
    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->getCurrentMode()Lcom/pspdfkit/internal/em;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    .line 25
    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->getCurrentMode()Lcom/pspdfkit/internal/em;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->a()I

    move-result v0

    const/16 v2, 0x14

    if-eq v0, v2, :cond_3

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 27
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    if-eqz v0, :cond_4

    return v1

    .line 32
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/yi;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    .line 33
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/tb;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    .line 34
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->o:Lcom/pspdfkit/internal/wc;

    .line 35
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wc;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    :goto_0
    return v1
.end method

.method public final performClick()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->r:Lcom/pspdfkit/internal/dm$d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1}, Lcom/pspdfkit/internal/dm$d;->a(Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final recycle()V
    .locals 5

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm;->B:Z

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/dm;->C:Z

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->w:Lcom/pspdfkit/internal/oh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/oh;->b()V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->y:Lcom/pspdfkit/internal/os;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/os;->recycle()V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->m:Lcom/pspdfkit/internal/tb;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/tb;->k()V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/am;->recycle()V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->n:Lcom/pspdfkit/internal/yi;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/yi;->recycle()V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->p:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/w1;->recycle()V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/dm;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 16
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lcom/pspdfkit/internal/dm;->z:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 19
    iget-object v2, p0, Lcom/pspdfkit/internal/dm;->A:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 20
    invoke-static {v2}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 21
    iput-object v1, p0, Lcom/pspdfkit/internal/dm;->A:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 24
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_1

    .line 25
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 26
    instance-of v4, v3, Lcom/pspdfkit/internal/mo;

    if-eqz v4, :cond_0

    .line 27
    check-cast v3, Lcom/pspdfkit/internal/mo;

    invoke-interface {v3}, Lcom/pspdfkit/internal/mo;->recycle()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->F:Lcom/pspdfkit/internal/ul;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ul;->recycle()V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->g:Lcom/pspdfkit/internal/w0;

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->g:Lcom/pspdfkit/internal/w0;

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->h:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->h:Lcom/pspdfkit/internal/yb;

    check-cast v0, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/ac;->removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V

    .line 41
    iput-object v1, p0, Lcom/pspdfkit/internal/dm;->v:Lcom/pspdfkit/internal/dm$e;

    return-void
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/dm;->E:Landroid/view/View$OnKeyListener;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->x:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dm;->l:Lcom/pspdfkit/internal/am;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->g()Lcom/pspdfkit/internal/z1;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    .line 2
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/dm$e;->-$$Nest$fputj(Lcom/pspdfkit/internal/dm$e;Z)V

    return-void
.end method

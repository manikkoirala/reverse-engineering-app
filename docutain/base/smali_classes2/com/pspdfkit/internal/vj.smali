.class public final Lcom/pspdfkit/internal/vj;
.super Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;
.source "SourceFile"


# instance fields
.field private a:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/lg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeJSPlatformDelegate;-><init>()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/lg;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/lg;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final buttonImportIcon(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconParams;Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconFormElementInfo;)Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconResult;
    .locals 1

    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "params"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formElementInfo"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lg;

    .line 2
    invoke-virtual {p4}, Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconFormElementInfo;->getFormPageIndex()I

    move-result p3

    invoke-virtual {p4}, Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconFormElementInfo;->getFormAnnotationId()I

    move-result v0

    invoke-interface {p2, p3, v0}, Lcom/pspdfkit/internal/lg;->a(II)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 3
    sget-object p1, Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconResult;->NO_ERROR:Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconResult;

    return-object p1

    .line 6
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconResult;->CANCELLED:Lcom/pspdfkit/internal/jni/NativeJSButtonImportIconResult;

    return-object p1
.end method

.method public final getPageNumber(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;)I
    .locals 1

    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lg;

    .line 2
    invoke-interface {p2}, Lcom/pspdfkit/internal/lg;->a()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 4
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_1
    const/high16 p1, -0x80000000

    return p1
.end method

.method public final launchUrl(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    const-string p4, "api"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "url"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lg;

    .line 2
    invoke-interface {p2, p3}, Lcom/pspdfkit/internal/lg;->a(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    :cond_1
    return-void
.end method

.method public final mailDoc(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeJSMail;)V
    .locals 6

    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "params"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/kg;

    .line 2
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSMail;->getTo()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSMail;->getCc()Ljava/lang/String;

    move-result-object v2

    .line 4
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSMail;->getBcc()Ljava/lang/String;

    move-result-object v3

    .line 5
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSMail;->getSubject()Ljava/lang/String;

    move-result-object v4

    .line 6
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSMail;->getMessage()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    .line 7
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/kg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    iget-object p2, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/lg;

    .line 15
    invoke-interface {p3, p1}, Lcom/pspdfkit/internal/lg;->a(Lcom/pspdfkit/internal/kg;)Z

    move-result p3

    if-eqz p3, :cond_0

    :cond_1
    return-void
.end method

.method public final print(Lcom/pspdfkit/internal/jni/NativeJSPrintParams;)V
    .locals 5

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeJSPrintParams;->getStart()Ljava/lang/Integer;

    move-result-object v0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeJSPrintParams;->getEnd()Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    new-instance v3, Lcom/pspdfkit/datastructures/Range;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_2
    sub-int/2addr v1, v2

    invoke-direct {v3, v4, v1}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    goto :goto_2

    .line 7
    :cond_3
    :goto_1
    new-instance v3, Lcom/pspdfkit/datastructures/Range;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_4
    const v0, 0x7fffffff

    invoke-direct {v3, v2, v0}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    .line 11
    :goto_2
    new-instance v0, Lcom/pspdfkit/internal/mg;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeJSPrintParams;->getUi()Ljava/lang/Boolean;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeJSPrintParams;->getPrintAnnotations()Z

    move-result p1

    .line 15
    invoke-direct {v0, v3, v1, p1}, Lcom/pspdfkit/internal/mg;-><init>(Lcom/pspdfkit/datastructures/Range;ZZ)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/lg;

    .line 22
    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/lg;->a(Lcom/pspdfkit/internal/mg;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_7
    return-void
.end method

.method public final setPageNumber(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lg;

    .line 2
    invoke-interface {p2, p3}, Lcom/pspdfkit/internal/lg;->a(I)Z

    move-result p2

    if-eqz p2, :cond_0

    :cond_1
    return-void
.end method

.method public final showAlert(Lcom/pspdfkit/internal/jni/NativeJavaScriptAPI;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeJSAlert;)Lcom/pspdfkit/internal/jni/NativeJSAlertResult;
    .locals 3

    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "scriptUuid"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "alert"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/vj;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/lg;

    .line 2
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "alert.title"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSAlert;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "alert.message"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0, v1}, Lcom/pspdfkit/internal/lg;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/jg;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 3
    const-class p1, Lcom/pspdfkit/internal/jni/NativeJSAlertResult;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    const-string p2, "mapEnum(result, NativeJSAlertResult::class.java)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/internal/jni/NativeJSAlertResult;

    return-object p1

    .line 4
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/jni/NativeJSAlertResult;->CANCEL:Lcom/pspdfkit/internal/jni/NativeJSAlertResult;

    return-object p1
.end method

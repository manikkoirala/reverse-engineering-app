.class final Lcom/pspdfkit/internal/gp$b;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/gp;->e()Lkotlin/jvm/functions/Function2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/pspdfkit/internal/ip;",
        "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/gp;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/gp;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/gp$b;->a:Lcom/pspdfkit/internal/gp;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/ip;

    check-cast p2, Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    const-string v0, "result"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "native"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/gp$b;->a:Lcom/pspdfkit/internal/gp;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/gp;->i()Lcom/pspdfkit/internal/pt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ip;->a(Ljava/util/UUID;)V

    .line 104
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeContentEditingResult;->getBinaryData()[B

    move-result-object p2

    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/gp$b;->a:Lcom/pspdfkit/internal/gp;

    .line 105
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 106
    array-length p2, p2

    div-int/lit8 p2, p2, 0x4

    new-array v2, p2, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, p2, :cond_2

    .line 108
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    goto :goto_1

    :cond_0
    ushr-int/lit8 v6, v5, 0x8

    shl-int/lit8 v5, v5, 0x18

    or-int/2addr v5, v6

    .line 121
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/gp;->h()Z

    move-result v6

    if-eqz v6, :cond_1

    const v6, 0xffffff

    xor-int/2addr v5, v6

    .line 124
    :cond_1
    aput v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ip;->a()Lcom/pspdfkit/internal/d7;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/d7;->b()Lcom/pspdfkit/internal/jv;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jv;->a()I

    move-result p2

    .line 130
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ip;->a()Lcom/pspdfkit/internal/d7;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d7;->b()Lcom/pspdfkit/internal/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jv;->b()I

    move-result v0

    .line 131
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 132
    invoke-static {v2, p2, v0, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    const-string v0, "createBitmap(\n          \u2026GB_8888\n                )"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<set-?>"

    .line 133
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    iput-object p2, p1, Lcom/pspdfkit/internal/ip;->b:Landroid/graphics/Bitmap;

    .line 294
    :cond_3
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.class public Lcom/pspdfkit/internal/me;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# static fields
.field public static final synthetic d:I


# instance fields
.field private b:Lcom/pspdfkit/internal/ne;

.field private c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/ne;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/me;->b:Lcom/pspdfkit/internal/ne;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    if-eqz p1, :cond_0

    .line 4
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/ne;->onRestoreInstanceState(Landroid/os/Bundle;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/me;->b:Lcom/pspdfkit/internal/ne;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ne;->onRestoreInstanceState(Landroid/os/Bundle;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/me;->b:Lcom/pspdfkit/internal/ne;

    if-eqz v0, :cond_1

    .line 6
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/ne;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 8
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/me;->c:Landroid/os/Bundle;

    return-void
.end method

.class public final Lcom/pspdfkit/internal/j8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager;
.implements Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j8;->b:Lcom/pspdfkit/internal/nh;

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j8;->c:Lcom/pspdfkit/internal/nh;

    return-void
.end method


# virtual methods
.method public final addOnDocumentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnDocumentEditingPageSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final onDocumentEditingPageSelectionChanged(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Document Editing listeners touched on non ui thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;->onDocumentEditingPageSelectionChanged(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onEnterDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Document Editing listeners touched on non ui thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;->onEnterDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onExitDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Document Editing listeners touched on non ui thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;->onExitDocumentEditingMode(Lcom/pspdfkit/ui/special_mode/controller/DocumentEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final removeOnDocumentEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnDocumentEditingPageSelectionChangeListener(Lcom/pspdfkit/ui/special_mode/manager/DocumentEditingManager$OnDocumentEditingPageSelectionChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/vb;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static synthetic $r8$lambda$m_UgDdjo3FWQE_-gSnZCIiKnfUc(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/vb;->c(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$uMJ2lECEDsQZTkBcK2qelahMrxg(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/vb;->d(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/forms/FormElement;)Lcom/pspdfkit/forms/TextInputFormat;
    .locals 4

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_CHANGED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-interface {p0, v0}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p0

    instance-of v0, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    goto :goto_0

    :cond_0
    move-object p0, v1

    :goto_0
    if-nez p0, :cond_1

    .line 555
    sget-object p0, Lcom/pspdfkit/forms/TextInputFormat;->NORMAL:Lcom/pspdfkit/forms/TextInputFormat;

    return-object p0

    .line 556
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->getScript()Ljava/lang/String;

    move-result-object p0

    const-string v0, "action.script"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "AFNumber_Keystroke"

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 559
    invoke-static {p0, v0, v2, v3, v1}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lcom/pspdfkit/forms/TextInputFormat;->NUMBER:Lcom/pspdfkit/forms/TextInputFormat;

    goto :goto_1

    :cond_2
    const-string v0, "AFDate_Keystroke"

    .line 560
    invoke-static {p0, v0, v2, v3, v1}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p0, Lcom/pspdfkit/forms/TextInputFormat;->DATE:Lcom/pspdfkit/forms/TextInputFormat;

    goto :goto_1

    :cond_3
    const-string v0, "AFTime_Keystroke"

    .line 561
    invoke-static {p0, v0, v2, v3, v1}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    sget-object p0, Lcom/pspdfkit/forms/TextInputFormat;->TIME:Lcom/pspdfkit/forms/TextInputFormat;

    goto :goto_1

    .line 562
    :cond_4
    sget-object p0, Lcom/pspdfkit/forms/TextInputFormat;->NORMAL:Lcom/pspdfkit/forms/TextInputFormat;

    :goto_1
    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)Lcom/pspdfkit/internal/qg;
    .locals 2

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contents"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 563
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 564
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ig;->isJavaScriptEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_5

    .line 567
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getFormField()Lcom/pspdfkit/forms/ChoiceFormField;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/jni/NativeFormControl;->executeKeystrokeEventForComboOrListFields(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object p0

    const-string p1, "this.formField.internal.\u2026mboOrListFields(contents)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 569
    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getError()Lcom/pspdfkit/internal/jni/NativeJSError;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getEvent()Lcom/pspdfkit/internal/jni/NativeJSEvent;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeJSEvent;->getRc()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    goto :goto_2

    .line 572
    :cond_2
    new-instance p1, Lcom/pspdfkit/internal/qg;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getValue()Lcom/pspdfkit/internal/jni/NativeJSValue;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSValue;->getStringValue()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_3
    move-object p0, v1

    :goto_1
    invoke-direct {p1, p0, v1}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 573
    :cond_4
    :goto_2
    new-instance p1, Lcom/pspdfkit/internal/qg;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getError()Lcom/pspdfkit/internal/jni/NativeJSError;

    move-result-object p0

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jni/NativeJSError;->getMessage()Ljava/lang/String;

    move-result-object p0

    goto :goto_3

    :cond_5
    move-object p0, v1

    :goto_3
    invoke-direct {p1, v1, p0}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    return-object p1

    .line 574
    :cond_6
    :goto_5
    new-instance p0, Lcom/pspdfkit/internal/qg;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/util/List;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/ChoiceFormElement;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Completable;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedIndexes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 583
    new-instance v0, Lcom/pspdfkit/internal/vb$c;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/vb$c;-><init>(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/util/List;)V

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/vb;->a(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 575
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-nez v0, :cond_0

    .line 576
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Can\'t set value to form elements that are not attached to a document!"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    const-string p1, "error(IllegalStateExcept\u2026ttached to a document!\"))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 578
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/vb$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/vb$$ExternalSyntheticLambda0;-><init>(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    const/4 p1, 0x5

    .line 579
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    const-string p1, "fromAction { block() }\n \u2026heduler.PRIORITY_NORMAL))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/CheckBoxFormElement;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/CheckBoxFormElement;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 582
    new-instance v0, Lcom/pspdfkit/internal/vb$e;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/vb$e;-><init>(Lcom/pspdfkit/forms/CheckBoxFormElement;)V

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/vb;->b(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/ComboBoxFormElement;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/ComboBoxFormElement;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 584
    new-instance v0, Lcom/pspdfkit/internal/vb$b;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/vb$b;-><init>(Lcom/pspdfkit/forms/ComboBoxFormElement;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/vb;->b(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/RadioButtonFormElement;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/RadioButtonFormElement;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 581
    new-instance v0, Lcom/pspdfkit/internal/vb$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/vb$a;-><init>(Lcom/pspdfkit/forms/RadioButtonFormElement;)V

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/vb;->b(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/TextFormElement;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 580
    new-instance v0, Lcom/pspdfkit/internal/vb$d;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/vb$d;-><init>(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/pspdfkit/internal/vb;->b(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/forms/ChoiceFormElement;)V
    .locals 12

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    instance-of v1, p0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    if-eqz v1, :cond_0

    .line 151
    move-object v2, p0

    check-cast v2, Lcom/pspdfkit/forms/ComboBoxFormElement;

    invoke-virtual {v2}, Lcom/pspdfkit/forms/ComboBoxFormElement;->getCustomText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getSelectedIndexes()Ljava/util/List;

    move-result-object v3

    const-string v2, "this.selectedIndexes"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Lcom/pspdfkit/internal/ub;

    invoke-direct {v9, p0}, Lcom/pspdfkit/internal/ub;-><init>(Lcom/pspdfkit/forms/ChoiceFormElement;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    const-string v4, ","

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 157
    :goto_0
    invoke-static {p0, v2}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)Lcom/pspdfkit/internal/qg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/qg;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/pspdfkit/internal/qg;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    .line 158
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v9, :cond_2

    if-eqz v1, :cond_1

    .line 322
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/forms/ComboBoxFormElement;->setCustomText(Ljava/lang/String;)Z

    .line 324
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    goto/16 :goto_5

    .line 327
    :cond_2
    invoke-static {p0, v9}, Lcom/pspdfkit/internal/vb;->b(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    .line 329
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    goto/16 :goto_5

    :cond_3
    const-string v0, ","

    .line 332
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v3, v9

    invoke-static/range {v3 .. v8}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 436
    instance-of v2, v0, Ljava/util/Collection;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_2

    .line 437
    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 438
    invoke-static {p0, v5}, Lcom/pspdfkit/internal/vb;->b(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_6

    const/4 v5, 0x1

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    :goto_1
    if-nez v5, :cond_5

    goto :goto_3

    :cond_7
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_9

    .line 439
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 440
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 441
    check-cast v2, Ljava/lang/String;

    .line 442
    invoke-static {p0, v2}, Lcom/pspdfkit/internal/vb;->b(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 548
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 549
    :cond_8
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->sorted(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    goto :goto_5

    :cond_9
    if-eqz v1, :cond_a

    .line 553
    check-cast p0, Lcom/pspdfkit/forms/ComboBoxFormElement;

    invoke-virtual {p0, v9}, Lcom/pspdfkit/forms/ComboBoxFormElement;->setCustomText(Ljava/lang/String;)Z

    :cond_a
    :goto_5
    return-void
.end method

.method private static final b(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/lang/String;)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ChoiceFormElement;->getOptions()Ljava/util/List;

    move-result-object p0

    const-string v0, "options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 95
    check-cast v1, Lcom/pspdfkit/forms/FormOption;

    .line 96
    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormOption;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    return v0
.end method

.method public static final b(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Lio/reactivex/rxjava3/core/Single;
    .locals 2

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-nez v0, :cond_0

    .line 98
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Can\'t set value to form elements that are not attached to a document!"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lio/reactivex/rxjava3/core/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string p1, "error(IllegalStateExcept\u2026ttached to a document!\"))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 100
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/vb$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/vb$$ExternalSyntheticLambda1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const/4 p1, 0x5

    .line 101
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string p1, "fromCallable { block() }\u2026heduler.PRIORITY_NORMAL))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final c(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)Ljava/lang/Object;
    .locals 1

    const-string v0, "$block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$this_executeAsync"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static final d(Lkotlin/jvm/functions/Function1;Lcom/pspdfkit/forms/FormElement;)V
    .locals 1

    const-string v0, "$block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$this_executeAsync"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

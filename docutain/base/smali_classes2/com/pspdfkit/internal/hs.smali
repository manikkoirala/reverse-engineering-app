.class public final Lcom/pspdfkit/internal/hs;
.super Lcom/pspdfkit/internal/e0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/e0<",
        "Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 1
    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/e0;-><init>([Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hs;->build()Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration;
    .locals 2

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/is;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/is;-><init>(Lcom/pspdfkit/internal/h0;)V

    return-object v0
.end method

.method public final setAudioRecordingSampleRate(I)Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->H:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    return-object p0
.end method

.method public final setAudioRecordingTimeLimit(I)Lcom/pspdfkit/annotations/configuration/SoundAnnotationConfiguration$Builder;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->G:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    return-object p0
.end method

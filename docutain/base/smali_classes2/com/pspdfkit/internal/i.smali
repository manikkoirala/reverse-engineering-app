.class public final Lcom/pspdfkit/internal/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/actions/ActionResolver;


# instance fields
.field private final b:Ljava/util/EnumMap;

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/document/DocumentActionListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/pspdfkit/annotations/actions/ActionType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/i;->b:Ljava/util/EnumMap;

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i;->c:Lcom/pspdfkit/internal/nh;

    .line 17
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v1, Lcom/pspdfkit/internal/dd;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/dd;-><init>(Lcom/pspdfkit/ui/navigation/PageNavigator;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 18
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO_EMBEDDED:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v1, Lcom/pspdfkit/internal/hd;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/hd;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 19
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->NAMED:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v1, Lcom/pspdfkit/internal/kj;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/kj;-><init>(Lcom/pspdfkit/ui/navigation/PageNavigator;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 20
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionType;->URI:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v1, Lcom/pspdfkit/internal/dv;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-direct {v1, p2, p1}, Lcom/pspdfkit/internal/dv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 21
    sget-object p1, Lcom/pspdfkit/annotations/actions/ActionType;->RESET_FORM:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v0, Lcom/pspdfkit/internal/tp;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/tp;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 22
    sget-object p1, Lcom/pspdfkit/annotations/actions/ActionType;->HIDE:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v0, Lcom/pspdfkit/internal/od;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/od;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 23
    sget-object p1, Lcom/pspdfkit/annotations/actions/ActionType;->RENDITION:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v0, Lcom/pspdfkit/internal/pp;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/pp;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 24
    sget-object p1, Lcom/pspdfkit/annotations/actions/ActionType;->RICH_MEDIA_EXECUTE:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v0, Lcom/pspdfkit/internal/bq;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/bq;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    .line 25
    sget-object p1, Lcom/pspdfkit/annotations/actions/ActionType;->JAVASCRIPT:Lcom/pspdfkit/annotations/actions/ActionType;

    new-instance v0, Lcom/pspdfkit/internal/gg;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/gg;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/actions/ActionType;Lcom/pspdfkit/internal/c;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final addDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/i;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;)V
    .locals 1

    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/i;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    return-void
.end method

.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V
    .locals 6

    const-string v0, "action"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 53
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "PSPDFKit.ActionResolver"

    const-string v4, "Execute action %s."

    invoke-static {v2, v4, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/i;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/document/DocumentActionListener;

    if-nez v4, :cond_1

    .line 58
    invoke-interface {v5, p1}, Lcom/pspdfkit/document/DocumentActionListener;->onExecuteAction(Lcom/pspdfkit/annotations/actions/Action;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_3

    return-void

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/i;->b:Ljava/util/EnumMap;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/c;

    if-eqz v0, :cond_4

    .line 65
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/c;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z

    goto :goto_1

    .line 67
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown action "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " of type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :goto_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/Action;->getSubActions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/actions/Action;

    .line 72
    invoke-virtual {p0, v0, p2}, Lcom/pspdfkit/internal/i;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    goto :goto_2

    :cond_5
    return-void
.end method

.method public final removeDocumentActionListener(Lcom/pspdfkit/document/DocumentActionListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/i;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

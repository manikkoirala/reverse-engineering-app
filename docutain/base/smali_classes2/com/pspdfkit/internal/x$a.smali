.class public final enum Lcom/pspdfkit/internal/x$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/x$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/x$a;

.field public static final enum b:Lcom/pspdfkit/internal/x$a;

.field private static final synthetic c:[Lcom/pspdfkit/internal/x$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/x$a;

    const-string v1, "ADD_ANNOTATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/x$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    .line 4
    new-instance v1, Lcom/pspdfkit/internal/x$a;

    const-string v3, "REMOVE_ANNOTATION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/x$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/x$a;->b:Lcom/pspdfkit/internal/x$a;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/pspdfkit/internal/x$a;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    .line 5
    sput-object v3, Lcom/pspdfkit/internal/x$a;->c:[Lcom/pspdfkit/internal/x$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/x$a;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/x$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/x$a;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/x$a;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/x$a;->c:[Lcom/pspdfkit/internal/x$a;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/x$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/x$a;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/pq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:[I

.field private static final g:I

.field private static final h:I


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar:[I

    sput-object v0, Lcom/pspdfkit/internal/pq;->f:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__scrollableThumbnailBarStyle:I

    sput v0, Lcom/pspdfkit/internal/pq;->g:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_ScrollableThumbnailBar:I

    sput v0, Lcom/pspdfkit/internal/pq;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/pq;->f:[I

    sget v2, Lcom/pspdfkit/internal/pq;->g:I

    sget v3, Lcom/pspdfkit/internal/pq;->h:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 4
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar_pspdf__thumbnailBorderColor:I

    const v2, 0x106000c

    .line 6
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 7
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/pq;->a:I

    .line 10
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar_pspdf__thumbnailSelectedBorderColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 12
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/pq;->b:I

    .line 16
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar_pspdf__thumbnailWidth:I

    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__scrollable_thumbnail_width:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/pq;->c:I

    .line 22
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar_pspdf__thumbnailHeight:I

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__scrollable_thumbnail_height:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 25
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/pq;->d:I

    .line 28
    sget p1, Lcom/pspdfkit/R$styleable;->pspdf__ScrollableThumbnailBar_pspdf__usePageAspectRatio:I

    const/4 v1, 0x1

    .line 29
    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/pq;->e:Z

    .line 30
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ao$a;
    }
.end annotation


# static fields
.field public static final a:I

.field public static final b:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:I

.field private static final l:I

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 29

    const/16 v0, 0xc0

    const/16 v1, 0x27

    const/16 v2, 0x4c

    .line 1
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/pspdfkit/internal/ao;->a:I

    .line 20
    sget-object v0, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->TWO_DP:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    sput-object v0, Lcom/pspdfkit/internal/ao;->b:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0xff

    .line 27
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const/16 v2, 0xbc

    const/16 v4, 0xc6

    const/16 v5, 0xcb

    .line 28
    invoke-static {v2, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v0, v4

    const/16 v2, 0x82

    const/16 v5, 0x8d

    const/16 v6, 0x93

    .line 29
    invoke-static {v2, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v0, v5

    const/16 v2, 0x55

    const/16 v7, 0x5d

    const/16 v8, 0x61

    .line 30
    invoke-static {v2, v7, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v7, 0x3

    aput-object v2, v0, v7

    .line 31
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v8, 0x4

    aput-object v2, v0, v8

    const/16 v2, 0x6d

    const/16 v9, 0x50

    const/16 v10, 0x34

    .line 32
    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v9, 0x5

    aput-object v2, v0, v9

    const/16 v2, 0xc0

    const/16 v10, 0x27

    const/16 v11, 0x4c

    .line 33
    invoke-static {v2, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v10, 0x6

    aput-object v2, v0, v10

    const/16 v2, 0xdf

    const/16 v11, 0x47

    const/16 v12, 0x4f

    .line 34
    invoke-static {v2, v11, v12}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x7

    aput-object v2, v0, v11

    const/16 v2, 0xf5

    const/16 v12, 0xa4

    const/16 v13, 0x2a

    .line 35
    invoke-static {v2, v12, v13}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v12, 0x8

    aput-object v2, v0, v12

    const/16 v2, 0xfe

    const/16 v13, 0xe8

    const/16 v14, 0x31

    .line 36
    invoke-static {v2, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v13, 0x9

    aput-object v2, v0, v13

    const/16 v2, 0x9e

    const/16 v14, 0xd6

    .line 37
    invoke-static {v2, v14, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/16 v15, 0xa

    aput-object v14, v0, v15

    const/16 v14, 0x3f

    const/16 v15, 0xb3

    const/16 v2, 0x3c

    .line 38
    invoke-static {v14, v15, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xb

    aput-object v2, v0, v14

    const/16 v2, 0xcc

    const/16 v15, 0xb4

    .line 39
    invoke-static {v12, v2, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v15, 0xc

    aput-object v2, v0, v15

    const/16 v2, 0x22

    const/16 v15, 0xfb

    .line 40
    invoke-static {v2, v6, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v15, 0xd

    aput-object v2, v0, v15

    const/16 v2, 0x4b

    const/16 v15, 0x64

    const/16 v14, 0xe3

    .line 41
    invoke-static {v2, v15, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xe

    aput-object v2, v0, v14

    const/16 v2, 0x8f

    const/16 v14, 0x5b

    .line 42
    invoke-static {v2, v14, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xf

    aput-object v2, v0, v14

    const/16 v2, 0xe2

    const/16 v14, 0x43

    const/16 v15, 0xfc

    .line 43
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x10

    aput-object v2, v0, v16

    const/16 v2, 0xfd

    const/16 v14, 0x63

    const/16 v15, 0x91

    .line 44
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0x11

    aput-object v2, v0, v14

    .line 45
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 46
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v2, "unmodifiableList( // The\u2026 99, 145)\n        )\n    )"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->c:Ljava/util/List;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Integer;

    .line 73
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    .line 74
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    const/16 v2, 0xbc

    const/16 v14, 0xc6

    const/16 v15, 0xcb

    .line 75
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v5

    const/16 v2, 0x82

    const/16 v14, 0x8d

    .line 76
    invoke-static {v2, v14, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v7

    const/16 v2, 0x55

    const/16 v14, 0x5d

    const/16 v15, 0x61

    .line 77
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v8

    .line 78
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v9

    const/16 v2, 0xc0

    const/16 v14, 0x27

    const/16 v15, 0x4c

    .line 79
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v10

    const/16 v2, 0xdf

    const/16 v14, 0x47

    const/16 v15, 0x4f

    .line 80
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v11

    const/16 v2, 0xf5

    const/16 v14, 0xa4

    const/16 v15, 0x2a

    .line 81
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v12

    const/16 v2, 0xfe

    const/16 v14, 0xe8

    const/16 v15, 0x31

    .line 82
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v13

    const/16 v2, 0xd6

    const/16 v14, 0x9e

    .line 83
    invoke-static {v14, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xa

    aput-object v2, v0, v14

    const/16 v2, 0x3f

    const/16 v14, 0xb3

    const/16 v15, 0x3c

    .line 84
    invoke-static {v2, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xb

    aput-object v2, v0, v14

    const/16 v2, 0xcc

    const/16 v14, 0xb4

    .line 85
    invoke-static {v12, v2, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v14, 0xc

    aput-object v2, v0, v14

    const/16 v2, 0x22

    const/16 v14, 0xfb

    .line 86
    invoke-static {v2, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v6, 0xd

    aput-object v2, v0, v6

    const/16 v2, 0x4b

    const/16 v6, 0x64

    const/16 v14, 0xe3

    .line 87
    invoke-static {v2, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v6, 0xe

    aput-object v2, v0, v6

    const/16 v2, 0x8f

    const/16 v6, 0x5b

    .line 88
    invoke-static {v2, v6, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v6, 0xf

    aput-object v2, v0, v6

    const/16 v2, 0xe2

    const/16 v6, 0xfc

    const/16 v14, 0x43

    .line 89
    invoke-static {v2, v14, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v6, 0x10

    aput-object v2, v0, v6

    const/16 v2, 0xfd

    const/16 v6, 0x63

    const/16 v14, 0x91

    .line 90
    invoke-static {v2, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v6, 0x11

    aput-object v2, v0, v6

    .line 91
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 92
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v2, "unmodifiableList(\n      \u2026 99, 145)\n        )\n    )"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->d:Ljava/util/List;

    .line 117
    sput-object v0, Lcom/pspdfkit/internal/ao;->e:Ljava/util/List;

    new-array v0, v10, [Ljava/lang/Integer;

    const/16 v2, 0xee

    const/16 v6, 0x58

    .line 123
    invoke-static {v1, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    const/16 v2, 0xa7

    const/16 v6, 0x26

    .line 124
    invoke-static {v1, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/16 v1, 0xef

    const/16 v2, 0x53

    const/16 v6, 0x50

    .line 125
    invoke-static {v1, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0xec

    const/16 v2, 0x40

    const/16 v6, 0x7a

    .line 126
    invoke-static {v1, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/16 v1, 0x42

    const/16 v2, 0xa5

    const/16 v6, 0xf5

    .line 127
    invoke-static {v1, v2, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v8

    const/16 v2, 0x66

    const/16 v6, 0xbb

    const/16 v14, 0x6a

    .line 128
    invoke-static {v2, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v9

    .line 129
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 130
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v2, "unmodifiableList(\n      \u2026187, 106)\n        )\n    )"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->f:Ljava/util/List;

    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/Integer;

    const/16 v0, 0xf4

    const/16 v6, 0x36

    const/16 v14, 0x43

    .line 144
    invoke-static {v0, v14, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/16 v0, 0x8b

    const/16 v6, 0xc3

    const/16 v14, 0x4a

    .line 145
    invoke-static {v0, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    const/16 v0, 0x21

    const/16 v6, 0x96

    const/16 v14, 0xf3

    .line 146
    invoke-static {v0, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v5

    const/16 v6, 0xed

    const/16 v14, 0x8c

    const/16 v15, 0xfc

    .line 147
    invoke-static {v15, v6, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v7

    const/16 v6, 0xe9

    const/16 v14, 0x1e

    const/16 v15, 0x63

    .line 148
    invoke-static {v6, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v8

    const/16 v6, 0xfa

    const/16 v14, 0xfa

    const/16 v15, 0xfa

    .line 150
    invoke-static {v6, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v9

    const/16 v6, 0xe0

    .line 151
    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v10

    const/16 v14, 0x9e

    .line 152
    invoke-static {v14, v14, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v11

    .line 153
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v12

    .line 154
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v13

    .line 155
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 156
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const-string v14, "unmodifiableList(\n      \u2026, 33, 33)\n        )\n    )"

    invoke-static {v2, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/internal/ao;->g:Ljava/util/List;

    const/16 v2, 0xf

    new-array v2, v2, [Ljava/lang/Integer;

    const v14, -0x85fff2

    .line 176
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v3

    const v14, -0xcaa7e6

    .line 177
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v4

    const v14, -0xebe4b1

    .line 178
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v5

    const v14, -0xcddb93

    .line 179
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v7

    const v14, -0x365fbd

    .line 180
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v2, v8

    const/16 v14, 0xf4

    const/16 v15, 0x36

    const/16 v8, 0x43

    .line 181
    invoke-static {v14, v8, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v9

    const/16 v8, 0x8b

    const/16 v14, 0xc3

    const/16 v15, 0x4a

    .line 182
    invoke-static {v8, v14, v15}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v10

    const/16 v8, 0x96

    const/16 v14, 0xf3

    .line 183
    invoke-static {v0, v8, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v11

    const/16 v0, 0xed

    const/16 v8, 0x8c

    const/16 v14, 0xfc

    .line 184
    invoke-static {v14, v0, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v12

    const/16 v0, 0xe9

    const/16 v8, 0x1e

    const/16 v14, 0x63

    .line 185
    invoke-static {v0, v8, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v13

    const v0, -0xa7fff6

    .line 186
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v8, 0xa

    aput-object v0, v2, v8

    .line 187
    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v6, 0xb

    aput-object v0, v2, v6

    const/16 v0, 0x9e

    .line 188
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v6, 0xc

    aput-object v0, v2, v6

    .line 189
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0xd

    aput-object v0, v2, v1

    .line 190
    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0xe

    aput-object v0, v2, v1

    .line 191
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 192
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v1, "unmodifiableList(\n      \u2026(0, 0, 0)\n        )\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->h:Ljava/util/List;

    const-string v17, "Comment"

    const-string v18, "RightPointer"

    const-string v19, "RightArrow"

    const-string v20, "Check"

    const-string v21, "Circle"

    const-string v22, "Cross"

    const-string v23, "Insert"

    const-string v24, "NewParagraph"

    const-string v25, "Note"

    const-string v26, "Paragraph"

    const-string v27, "Help"

    const-string v28, "Star"

    .line 232
    filled-new-array/range {v17 .. v28}, [Ljava/lang/String;

    move-result-object v0

    .line 233
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 234
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v1, "unmodifiableList(\n      \u2026tion.STAR\n        )\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->i:Ljava/util/List;

    const/16 v0, 0xd

    new-array v1, v0, [Lcom/pspdfkit/internal/fv$a;

    .line 256
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_comment:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 257
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Comment"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v3

    .line 259
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_right_pointer:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 260
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "RightPointer"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v4

    .line 262
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_right_arrow:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 263
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "RightArrow"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v5

    .line 265
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_check:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 266
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Check"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v7

    .line 268
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_circle:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 269
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Circle"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v0, 0x4

    aput-object v2, v1, v0

    .line 271
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_cross:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 272
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Cross"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v9

    .line 274
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_insert:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 275
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Insert"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v10

    .line 277
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_new_paragraph:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 278
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "NewParagraph"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v11

    .line 280
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_note:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 281
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Note"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v12

    .line 283
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_paragraph:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 284
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Paragraph"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v13

    .line 286
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_help:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 287
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Help"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    const/16 v0, 0xa

    aput-object v2, v1, v0

    .line 289
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_star:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 290
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Star"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    const/16 v0, 0xb

    aput-object v2, v1, v0

    .line 292
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_key:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 293
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Key"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    const/16 v0, 0xc

    aput-object v2, v1, v0

    .line 295
    new-instance v0, Ljava/util/HashMap;

    const/16 v2, 0xd

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_0

    .line 296
    aget-object v8, v1, v6

    iget-object v9, v8, Lcom/pspdfkit/internal/fv$a;->a:Ljava/lang/Object;

    iget-object v8, v8, Lcom/pspdfkit/internal/fv$a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 297
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "unmodifiableMap(\n       \u2026pdf__note_icon_key)\n    )"

    .line 298
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->j:Ljava/util/Map;

    .line 315
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_note:I

    sput v0, Lcom/pspdfkit/internal/ao;->k:I

    .line 318
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__note_icon_instant_comment:I

    sput v0, Lcom/pspdfkit/internal/ao;->l:I

    const/4 v0, 0x4

    new-array v1, v0, [Lcom/pspdfkit/internal/fv$a;

    .line 322
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__file_icon_graph:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 323
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Graph"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v3

    .line 325
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__file_icon_paperclip:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 326
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v6, "Paperclip"

    invoke-direct {v2, v6, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v4

    .line 328
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__file_icon_push_pin:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 329
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v4, "PushPin"

    invoke-direct {v2, v4, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v5

    .line 331
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__file_icon_tag:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 332
    new-instance v2, Lcom/pspdfkit/internal/fv$a;

    const-string v4, "Tag"

    invoke-direct {v2, v4, v0}, Lcom/pspdfkit/internal/fv$a;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v2, v1, v7

    .line 334
    new-instance v0, Ljava/util/HashMap;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    :goto_1
    if-ge v3, v2, :cond_1

    .line 335
    aget-object v4, v1, v3

    iget-object v5, v4, Lcom/pspdfkit/internal/fv$a;->a:Ljava/lang/Object;

    iget-object v4, v4, Lcom/pspdfkit/internal/fv$a;->b:Ljava/lang/Object;

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 336
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "unmodifiableMap(\n       \u2026pdf__file_icon_tag)\n    )"

    .line 337
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/ao;->m:Ljava/util/Map;

    .line 345
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__file_icon_paperclip:I

    sput v0, Lcom/pspdfkit/internal/ao;->n:I

    return-void
.end method

.method public static final a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    sget-object v0, Lcom/pspdfkit/internal/ao$a;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    .line 505
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_highlight:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 506
    :pswitch_0
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_redaction:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 507
    :pswitch_1
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_ink:I

    .line 508
    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 509
    :pswitch_2
    sget-object p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;->HIGHLIGHTER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;

    invoke-static {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->fromPreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant$Preset;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 510
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_highlight:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 512
    :cond_0
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_ink:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 513
    :pswitch_3
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_strikeout:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 514
    :pswitch_4
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_squiggle:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 515
    :pswitch_5
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_underline:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 516
    :pswitch_6
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_freetext:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    .line 522
    :cond_1
    :pswitch_7
    sget p1, Lcom/pspdfkit/R$color;->pspdf__color_default_highlight:I

    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final a(Lcom/pspdfkit/annotations/Annotation;)I
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    instance-of v0, p0, Lcom/pspdfkit/annotations/NoteAnnotation;

    const-string v1, "annotation.iconName"

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    sget p0, Lcom/pspdfkit/internal/ao;->l:I

    goto :goto_0

    .line 6
    :cond_0
    check-cast p0, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/NoteAnnotation;->getIconName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->b(Ljava/lang/String;)I

    move-result p0

    goto :goto_0

    .line 8
    :cond_1
    instance-of v0, p0, Lcom/pspdfkit/annotations/FileAnnotation;

    if-eqz v0, :cond_3

    .line 9
    check-cast p0, Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FileAnnotation;->getIconName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    sget-object v0, Lcom/pspdfkit/internal/ao;->m:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    if-eqz p0, :cond_2

    .line 11
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0

    :cond_2
    sget p0, Lcom/pspdfkit/internal/ao;->n:I

    goto :goto_0

    .line 12
    :cond_3
    instance-of p0, p0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz p0, :cond_4

    .line 13
    sget p0, Lcom/pspdfkit/R$drawable;->pspdf__ic_sound:I

    :goto_0
    return p0

    .line 14
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Only note and file annotations are supported."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotationTool"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/pspdfkit/internal/ao$a;->b:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    const-string v0, "annotationTool.toAnnotationType()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 24
    :pswitch_0
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_rectangular_area:I

    goto :goto_1

    .line 25
    :pswitch_1
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_polygonal_area:I

    goto :goto_1

    .line 26
    :pswitch_2
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_elliptical_area:I

    goto :goto_1

    .line 27
    :pswitch_3
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_perimeter:I

    goto :goto_1

    .line 28
    :pswitch_4
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_distance:I

    goto :goto_1

    .line 29
    :pswitch_5
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    goto :goto_1

    .line 30
    :pswitch_6
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_eraser:I

    goto :goto_1

    :goto_0
    const-string v0, "annotationType"

    .line 31
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 441
    sget-object v0, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_1

    .line 455
    :pswitch_7
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotations:I

    goto :goto_1

    .line 456
    :pswitch_8
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_redaction:I

    goto :goto_1

    .line 457
    :pswitch_9
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polyline:I

    goto :goto_1

    .line 458
    :pswitch_a
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polygon:I

    goto :goto_1

    .line 459
    :pswitch_b
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_square:I

    goto :goto_1

    .line 463
    :pswitch_c
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_stamp:I

    goto :goto_1

    .line 464
    :pswitch_d
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_line:I

    goto :goto_1

    .line 466
    :pswitch_e
    sget p0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_circle:I

    goto :goto_1

    .line 467
    :pswitch_f
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_ink:I

    goto :goto_1

    .line 476
    :pswitch_10
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_squiggly:I

    goto :goto_1

    .line 478
    :pswitch_11
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_freetext:I

    goto :goto_1

    .line 479
    :pswitch_12
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_underline:I

    goto :goto_1

    .line 480
    :pswitch_13
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_strikeout:I

    goto :goto_1

    .line 483
    :pswitch_14
    sget p0, Lcom/pspdfkit/R$string;->pspdf__edit_menu_highlight:I

    :goto_1
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_7
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_b
        :pswitch_7
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public static final a(Ljava/lang/String;)I
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "iconName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 815
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "Circle"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto/16 :goto_0

    .line 817
    :cond_0
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_circle:I

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "RightPointer"

    .line 818
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    goto/16 :goto_0

    .line 830
    :cond_1
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_right_pointer:I

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "RightArrow"

    .line 831
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    goto/16 :goto_0

    .line 842
    :cond_2
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_right_arrow:I

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "Cross"

    .line 843
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    goto/16 :goto_0

    .line 847
    :cond_3
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_cross:I

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "Check"

    .line 848
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto/16 :goto_0

    .line 849
    :cond_4
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_checkmark:I

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "Star"

    .line 850
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    goto :goto_0

    .line 863
    :cond_5
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_star:I

    goto :goto_1

    :sswitch_6
    const-string v0, "Note"

    .line 864
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    goto :goto_0

    .line 873
    :cond_6
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_text_note:I

    goto :goto_1

    :sswitch_7
    const-string v0, "Help"

    .line 874
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_7

    goto :goto_0

    .line 879
    :cond_7
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_help:I

    goto :goto_1

    :sswitch_8
    const-string v0, "Key"

    .line 880
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    goto :goto_0

    .line 887
    :cond_8
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_key:I

    goto :goto_1

    :sswitch_9
    const-string v0, "Paragraph"

    .line 888
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_9

    goto :goto_0

    .line 898
    :cond_9
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_paragraph:I

    goto :goto_1

    :sswitch_a
    const-string v0, "Comment"

    .line 899
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_a

    goto :goto_0

    .line 902
    :cond_a
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_comment:I

    goto :goto_1

    :sswitch_b
    const-string v0, "NewParagraph"

    .line 903
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_b

    goto :goto_0

    .line 911
    :cond_b
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_new_paragraph:I

    goto :goto_1

    :sswitch_c
    const-string v0, "Insert"

    .line 912
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_c

    goto :goto_0

    .line 918
    :cond_c
    sget p0, Lcom/pspdfkit/R$string;->pspdf__note_icon_insert_text:I

    goto :goto_1

    :goto_0
    const/4 p0, 0x0

    :goto_1
    return p0

    :sswitch_data_0
    .sparse-switch
        -0x7d2a5127 -> :sswitch_c
        -0x6fdf9832 -> :sswitch_b
        -0x642179c1 -> :sswitch_a
        -0x145e16b2 -> :sswitch_9
        0x1263f -> :sswitch_8
        0x224361 -> :sswitch_7
        0x252412 -> :sswitch_6
        0x277a72 -> :sswitch_5
        0x3e0f4e8 -> :sswitch_4
        0x3e5a820 -> :sswitch_3
        0x22748f6d -> :sswitch_2
        0x6b6e10a1 -> :sswitch_1
        0x7851a8f0 -> :sswitch_0
    .end sparse-switch
.end method

.method public static final a(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    const-string v1, "annotation.type"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 527
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v1

    const-string v2, ""

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 528
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    .line 529
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "{\n            Localizati\u2026s\n            )\n        }"

    .line 530
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 536
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 585
    :pswitch_0
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_redaction:I

    .line 586
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 587
    :pswitch_1
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polyline:I

    .line 588
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 589
    :pswitch_2
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_polygon:I

    .line 590
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 591
    :pswitch_3
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_sound:I

    .line 592
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 593
    :pswitch_4
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_square:I

    .line 594
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 595
    :pswitch_5
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_file:I

    .line 596
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 597
    :pswitch_6
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_screen:I

    .line 598
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 599
    :pswitch_7
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_rich_media:I

    .line 600
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :goto_0
    :pswitch_8
    move-object v1, v2

    goto :goto_1

    .line 602
    :pswitch_9
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_stamp:I

    .line 603
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 604
    :pswitch_a
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_line:I

    .line 605
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 606
    :pswitch_b
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_circle:I

    .line 607
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 608
    :pswitch_c
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_link:I

    .line 609
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 610
    :pswitch_d
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_ink:I

    .line 611
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 612
    :pswitch_e
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_squiggly:I

    .line 613
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 614
    :pswitch_f
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_freetext:I

    .line 615
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 616
    :pswitch_10
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_underline:I

    .line 617
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 618
    :pswitch_11
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_strikeout:I

    .line 619
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 620
    :pswitch_12
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_highlight:I

    .line 621
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 622
    :pswitch_13
    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_note:I

    .line 623
    invoke-static {p0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const-string v4, "{\n            when (anno\u2026\"\n            }\n        }"

    .line 624
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 625
    :goto_2
    sget-object v4, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v4, v0

    const-string v4, "stringBuilder.toString()"

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_15

    .line 646
    :pswitch_14
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 647
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_1

    goto :goto_3

    :cond_1
    const/4 v5, 0x0

    :cond_2
    :goto_3
    if-nez v5, :cond_3

    goto :goto_4

    :cond_3
    move-object p0, v2

    :goto_4
    if-nez p0, :cond_4

    goto/16 :goto_18

    :cond_4
    :goto_5
    move-object v2, p0

    goto/16 :goto_18

    .line 649
    :pswitch_15
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 653
    instance-of v0, p1, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v0, :cond_7

    .line 654
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 655
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_6

    :cond_5
    const/4 v0, 0x0

    goto :goto_7

    :cond_6
    :goto_6
    const/4 v0, 0x1

    :goto_7
    if-nez v0, :cond_7

    .line 656
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    .line 662
    :goto_8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 663
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    goto :goto_9

    :cond_8
    const/4 v2, 0x0

    goto :goto_a

    :cond_9
    :goto_9
    const/4 v2, 0x1

    :goto_a
    if-nez v2, :cond_b

    if-eqz v0, :cond_a

    const-string v0, ": "

    .line 665
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    :cond_a
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669
    :cond_b
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 670
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_c

    goto :goto_b

    :cond_c
    const/4 v5, 0x0

    :goto_b
    if-nez v5, :cond_1b

    goto :goto_5

    .line 671
    :pswitch_16
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_e

    .line 672
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_d

    goto :goto_c

    :cond_d
    const/4 v5, 0x0

    :cond_e
    :goto_c
    if-nez v5, :cond_f

    move-object v1, p0

    :cond_f
    if-nez v1, :cond_1b

    goto/16 :goto_18

    .line 674
    :pswitch_17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 677
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 678
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_10

    goto :goto_d

    :cond_10
    const/4 v7, 0x0

    goto :goto_e

    :cond_11
    :goto_d
    const/4 v7, 0x1

    :goto_e
    if-nez v7, :cond_12

    .line 679
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string p0, "stringBuilder.append(contents).toString()"

    invoke-static {v2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 681
    :cond_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    instance-of v1, p1, Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v1, :cond_13

    .line 685
    check-cast p1, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const-string v1, ", "

    .line 687
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    sget v1, Lcom/pspdfkit/R$plurals;->pspdf__lines_number:I

    new-array v2, v5, [Ljava/lang/Object;

    .line 694
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v6

    .line 695
    invoke-static {p0, v1, v3, p1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/widget/TextView;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 696
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 706
    :cond_13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 707
    :pswitch_18
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p0

    .line 708
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v0

    if-nez v0, :cond_16

    if-eqz p0, :cond_15

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_14

    goto :goto_f

    :cond_14
    const/4 v0, 0x0

    goto :goto_10

    :cond_15
    :goto_f
    const/4 v0, 0x1

    :goto_10
    if-nez v0, :cond_16

    :goto_11
    move-object v1, p0

    goto :goto_13

    .line 713
    :cond_16
    instance-of p0, p1, Lcom/pspdfkit/annotations/TextMarkupAnnotation;

    if-eqz p0, :cond_18

    .line 714
    check-cast p1, Lcom/pspdfkit/annotations/TextMarkupAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/TextMarkupAnnotation;->getHighlightedText()Ljava/lang/String;

    move-result-object p0

    const-string p1, "annotation.highlightedText"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 715
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_17

    goto :goto_12

    :cond_17
    const/4 v5, 0x0

    :goto_12
    if-nez v5, :cond_18

    goto :goto_11

    :cond_18
    :goto_13
    if-nez v1, :cond_1b

    goto :goto_18

    .line 723
    :pswitch_19
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1a

    .line 724
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_19

    goto :goto_14

    :cond_19
    const/4 v5, 0x0

    :cond_1a
    :goto_14
    if-eqz v5, :cond_4

    :cond_1b
    :pswitch_1a
    move-object v2, v1

    goto :goto_18

    .line 725
    :goto_15
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1d

    .line 726
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_1c

    goto :goto_16

    :cond_1c
    const/4 v5, 0x0

    :cond_1d
    :goto_16
    if-nez v5, :cond_1e

    goto :goto_17

    :cond_1e
    move-object p0, v2

    :goto_17
    if-nez p0, :cond_4

    :goto_18
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_19
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_19
        :pswitch_19
        :pswitch_15
        :pswitch_14
        :pswitch_1a
        :pswitch_1a
        :pswitch_14
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
    .end packed-switch
.end method

.method public static final a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 728
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/zf;

    .line 729
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-nez v1, :cond_3

    .line 731
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getTitle()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 732
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 735
    :cond_1
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_title_unnamed_document:I

    .line 736
    invoke-static {p0, p1, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    :cond_2
    const-string p0, "{\n            // Pdf doc\u2026e\n            }\n        }"

    .line 737
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 750
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object p1

    if-eqz p1, :cond_9

    const-string v0, "<this>"

    .line 751
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 777
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p1

    .line 778
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 779
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    goto :goto_1

    .line 780
    :cond_4
    invoke-static {p1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    :goto_1
    move-object p1, v4

    :goto_2
    if-nez p1, :cond_6

    goto :goto_4

    .line 781
    :cond_6
    invoke-static {p1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x2e

    .line 782
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ge v0, v2, :cond_7

    goto :goto_3

    .line 783
    :cond_7
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 784
    :goto_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    goto :goto_4

    .line 785
    :cond_8
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/document/providers/DataProvider;->getTitle()Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    :cond_9
    :goto_4
    move-object p1, v4

    :cond_a
    :goto_5
    if-nez p1, :cond_b

    .line 786
    sget p1, Lcom/pspdfkit/R$string;->pspdf__unnamed_image_document:I

    .line 787
    invoke-static {p0, p1, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    const-string p0, "getString(context, R.str\u2026__unnamed_image_document)"

    .line 788
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_b
    :goto_6
    return-object p1
.end method

.method public static a()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ao;->d:Ljava/util/List;

    return-object v0
.end method

.method public static final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)Z
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineEndType1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineEndType2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 795
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_1

    const/16 v2, 0x14

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 801
    :cond_0
    check-cast p0, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/annotations/PolylineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    goto :goto_1

    .line 802
    :cond_1
    check-cast p0, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/annotations/LineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    goto :goto_1

    .line 810
    :cond_2
    check-cast p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 811
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object p1

    .line 812
    sget-object v0, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne p1, v0, :cond_3

    .line 814
    invoke-virtual {p0, p2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setLineEnd(Lcom/pspdfkit/annotations/LineEndType;)V

    return v1

    :cond_3
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public static final b(Ljava/lang/String;)I
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "iconName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/ao;->j:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    if-eqz p0, :cond_0

    .line 3
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0

    :cond_0
    sget p0, Lcom/pspdfkit/internal/ao;->k:I

    :goto_0
    return p0
.end method

.method public static final b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NONE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_b

    const/16 v2, 0x11

    if-eq v1, v2, :cond_9

    const/16 v2, 0x9

    if-eq v1, v2, :cond_7

    const/16 v2, 0xa

    if-eq v1, v2, :cond_5

    const/16 v2, 0x13

    if-eq v1, v2, :cond_3

    const/16 v2, 0x14

    if-eq v1, v2, :cond_1

    .line 27
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->values()[Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_d

    aget-object v4, v1, v2

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v5

    invoke-virtual {v4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v6

    if-ne v5, v6, :cond_0

    move-object v0, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_PERIMETER:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 32
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->POLYGON:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 33
    :cond_5
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 42
    :cond_7
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_ELLIPSE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->CIRCLE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 45
    :cond_9
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_AREA_RECT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_a
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SQUARE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 46
    :cond_b
    instance-of v1, p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v1, :cond_d

    .line 47
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getCallOutPoints()Ljava/util/List;

    move-result-object v0

    const-string v1, "annotation.callOutPoints"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_c

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    :cond_c
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 75
    :cond_d
    :goto_1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/internal/pf;->getVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object p0

    const-string v1, "annotation.internal.variant"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v1, Landroidx/core/util/Pair;

    invoke-direct {v1, v0, p0}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public static b()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ao;->g:Ljava/util/List;

    return-object v0
.end method

.method public static final c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    check-cast p0, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/PolylineAnnotation;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p0

    goto :goto_1

    .line 5
    :cond_1
    check-cast p0, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/LineAnnotation;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p0

    goto :goto_1

    .line 8
    :cond_2
    check-cast p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v0

    .line 10
    sget-object v1, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v0, v1, :cond_3

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getLineEnd()Lcom/pspdfkit/annotations/LineEndType;

    move-result-object p0

    const-string v0, "freeTextAnnotation.lineEnd"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Landroidx/core/util/Pair;

    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {v0, p0, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_3
    :goto_0
    const/4 p0, 0x0

    :goto_1
    return-object p0
.end method

.method public static c()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ao;->e:Ljava/util/List;

    return-object v0
.end method

.method public static d()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ao;->i:Ljava/util/List;

    return-object v0
.end method

.method public static final d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    const-string v2, "{\n                (annot\u2026ion).points\n            }"

    if-eq v0, v1, :cond_1

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    .line 16
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 17
    :cond_0
    check-cast p0, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/PolylineAnnotation;->getPoints()Ljava/util/List;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 18
    :cond_1
    check-cast p0, Lcom/pspdfkit/annotations/PolygonAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/PolygonAnnotation;->getPoints()Ljava/util/List;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_2
    check-cast p0, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object p0

    const-string v0, "annotation as LineAnnotation).points"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/PointF;

    .line 20
    iget-object v1, p0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object p0, p0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Landroid/graphics/PointF;

    const/4 v1, 0x1

    aput-object p0, v0, v1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 29
    :cond_3
    check-cast p0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getCallOutPoints()Ljava/util/List;

    move-result-object p0

    const-string v0, "{\n                (annot\u2026llOutPoints\n            }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static final e(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v3, 0x0

    if-eq v1, v2, :cond_6

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    goto :goto_4

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    goto :goto_3

    .line 8
    :cond_1
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 9
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_5

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_6

    .line 11
    :cond_5
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1085
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object p0

    const-string v0, "AutoCAD SHX Text"

    invoke-static {v0, p0, v2}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p0

    if-nez p0, :cond_6

    :goto_3
    const/4 v3, 0x1

    :cond_6
    :goto_4
    return v3
.end method

.method public static final f(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->isReply()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.class public Lcom/pspdfkit/internal/sn;
.super Lcom/pspdfkit/internal/d4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/sn$a;
    }
.end annotation


# instance fields
.field protected A:F

.field private B:F

.field protected final w:Landroid/graphics/Path;

.field private final x:Landroid/graphics/Path;

.field private y:Landroidx/core/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation
.end field

.field protected z:F


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    sget-object v0, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    .line 7
    invoke-static {v0, v0}, Landroidx/core/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroidx/core/util/Pair;

    move-result-object v6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    .line 8
    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/sn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V

    return-void
.end method

.method public constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIFF",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)V"
        }
    .end annotation

    .line 9
    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/d4;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 10
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    .line 14
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    const/4 p1, 0x0

    .line 22
    iput p1, p0, Lcom/pspdfkit/internal/sn;->z:F

    .line 24
    iput p1, p0, Lcom/pspdfkit/internal/sn;->A:F

    const/high16 p1, 0x41400000    # 12.0f

    .line 33
    iput p1, p0, Lcom/pspdfkit/internal/sn;->B:F

    .line 62
    iput-object p6, p0, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/pspdfkit/internal/sn$a;
    .locals 9

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/sn$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/sn$a;-><init>(Lcom/pspdfkit/internal/sn$a-IA;)V

    .line 3
    iget v1, p2, Landroid/graphics/PointF;->x:F

    iput v1, v0, Lcom/pspdfkit/internal/sn$a;->a:F

    .line 4
    iget v1, p2, Landroid/graphics/PointF;->y:F

    iput v1, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    .line 5
    iget v1, p3, Landroid/graphics/PointF;->x:F

    .line 6
    iget v2, p3, Landroid/graphics/PointF;->y:F

    .line 8
    sget-object v3, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    if-ne p1, v3, :cond_0

    return-object v0

    .line 9
    :cond_0
    iget v3, p0, Lcom/pspdfkit/internal/y4;->q:F

    const/high16 v4, 0x3fe00000    # 1.75f

    iget v5, p0, Lcom/pspdfkit/internal/sn;->B:F

    .line 10
    invoke-virtual {p2, p3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    goto :goto_0

    :cond_1
    mul-float v3, v3, v4

    add-float p3, v3, v5

    .line 11
    :goto_0
    iget v3, v0, Lcom/pspdfkit/internal/sn$a;->a:F

    const v4, 0x3c23d70a    # 0.01f

    cmpl-float v5, v3, v1

    if-nez v5, :cond_2

    add-float/2addr v1, v4

    .line 12
    :cond_2
    iget v5, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    cmpl-float v5, v5, v2

    if-nez v5, :cond_3

    add-float/2addr v2, v4

    :cond_3
    sub-float v3, v1, v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    .line 16
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    iget v7, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    sub-float v7, v2, v7

    float-to-double v7, v7

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    add-double/2addr v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v3, v3

    const v4, 0x3a83126f    # 0.001f

    cmpg-float v5, v3, v4

    if-gtz v5, :cond_4

    const v3, 0x3a83126f    # 0.001f

    .line 22
    :cond_4
    iget v4, v0, Lcom/pspdfkit/internal/sn$a;->a:F

    sub-float/2addr v1, v4

    div-float v4, v1, v3

    .line 23
    iget v5, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    sub-float v6, v2, v5

    div-float/2addr v6, v3

    sub-float/2addr v5, v2

    float-to-double v2, v5

    float-to-double v7, v1

    .line 24
    invoke-static {v2, v3, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    const-wide v7, 0x400921fb54442d18L    # Math.PI

    sub-double/2addr v7, v1

    double-to-float v1, v7

    .line 26
    iput v1, v0, Lcom/pspdfkit/internal/sn$a;->c:F

    .line 27
    sget-object v1, Lcom/pspdfkit/internal/hh$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_5

    const/4 v2, 0x2

    if-eq p1, v2, :cond_5

    const/4 v2, 0x3

    if-eq p1, v2, :cond_5

    const/4 v2, 0x4

    if-eq p1, v2, :cond_5

    const/4 v1, 0x0

    :cond_5
    if-eqz v1, :cond_6

    .line 28
    iget p1, p2, Landroid/graphics/PointF;->x:F

    iput p1, v0, Lcom/pspdfkit/internal/sn$a;->a:F

    .line 29
    iget p1, p2, Landroid/graphics/PointF;->y:F

    iput p1, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    goto :goto_1

    .line 31
    :cond_6
    iget p1, p2, Landroid/graphics/PointF;->x:F

    mul-float v4, v4, p3

    add-float/2addr v4, p1

    iput v4, v0, Lcom/pspdfkit/internal/sn$a;->a:F

    .line 32
    iget p1, p2, Landroid/graphics/PointF;->y:F

    mul-float v6, v6, p3

    add-float/2addr v6, p1

    iput v6, v0, Lcom/pspdfkit/internal/sn$a;->b:F

    :goto_1
    return-object v0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;FLcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;FF)V
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 36
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/y4;->q:F

    invoke-static {p5, v0, p7}, Lcom/pspdfkit/internal/hh;->a(Lcom/pspdfkit/annotations/LineEndType;FF)Landroid/graphics/Path;

    move-result-object p7

    .line 39
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 40
    invoke-virtual {v0, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    float-to-double v1, p8

    .line 41
    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float p8, v1

    invoke-virtual {v0, p8}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 42
    iget p8, p6, Landroid/graphics/PointF;->x:F

    mul-float p8, p8, p4

    iget p6, p6, Landroid/graphics/PointF;->y:F

    mul-float p6, p6, p4

    invoke-virtual {v0, p8, p6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 43
    iget-object p4, p0, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    .line 44
    invoke-virtual {p4, p7}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 45
    invoke-virtual {p4, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 46
    invoke-virtual {p2}, Landroid/graphics/Paint;->getPathEffect()Landroid/graphics/PathEffect;

    move-result-object p4

    .line 47
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeCap()Landroid/graphics/Paint$Cap;

    move-result-object p6

    const/4 p7, 0x0

    .line 48
    invoke-virtual {p2, p7}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 49
    sget-object p7, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {p2, p7}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    if-eqz p3, :cond_1

    .line 50
    sget-object p7, Lcom/pspdfkit/internal/hh$a;->a:[I

    invoke-virtual {p5}, Ljava/lang/Enum;->ordinal()I

    move-result p5

    aget p5, p7, p5

    packed-switch p5, :pswitch_data_0

    const/4 p5, 0x0

    goto :goto_0

    :pswitch_0
    const/4 p5, 0x1

    :goto_0
    if-eqz p5, :cond_1

    .line 51
    invoke-virtual {p3}, Landroid/graphics/Paint;->getColor()I

    move-result p5

    if-eqz p5, :cond_1

    .line 52
    iget-object p5, p0, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    invoke-virtual {p1, p5, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 56
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 59
    invoke-virtual {p2, p4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 60
    invoke-virtual {p2, p6}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 28

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move/from16 v12, p4

    .line 61
    iget-object v0, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 63
    :cond_0
    iget-object v0, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/graphics/PointF;

    .line 64
    iget-object v0, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 66
    iget-object v4, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Landroid/graphics/PointF;

    .line 67
    iget-object v4, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v3

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Landroid/graphics/PointF;

    .line 70
    iget-object v4, v9, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    iget-object v4, v4, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/pspdfkit/annotations/LineEndType;

    .line 71
    invoke-direct {v9, v4, v6, v0}, Lcom/pspdfkit/internal/sn;->a(Lcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/pspdfkit/internal/sn$a;

    move-result-object v4

    .line 73
    iget v5, v4, Lcom/pspdfkit/internal/sn$a;->c:F

    iput v5, v9, Lcom/pspdfkit/internal/sn;->z:F

    .line 76
    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    iget-object v5, v5, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/pspdfkit/annotations/LineEndType;

    invoke-direct {v9, v5, v14, v13}, Lcom/pspdfkit/internal/sn;->a(Lcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/pspdfkit/internal/sn$a;

    move-result-object v15

    .line 78
    iget v5, v15, Lcom/pspdfkit/internal/sn$a;->c:F

    iput v5, v9, Lcom/pspdfkit/internal/sn;->A:F

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/y4;->u()Z

    move-result v5

    const/4 v7, 0x3

    if-nez v5, :cond_3

    .line 84
    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 87
    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget v8, v4, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v1, v4, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-virtual {v5, v8, v1}, Landroid/graphics/Path;->moveTo(FF)V

    if-ne v0, v14, :cond_1

    .line 89
    iget-object v1, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget v5, v15, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v8, v15, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-virtual {v1, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object v1, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 94
    :goto_0
    iget-object v1, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v7, :cond_2

    const/4 v1, 0x2

    .line 95
    :goto_1
    iget-object v5, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v3

    if-ge v1, v5, :cond_2

    .line 96
    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget-object v7, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-eq v0, v14, :cond_7

    .line 101
    iget-object v1, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget v5, v15, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v7, v15, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-virtual {v1, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_4

    .line 105
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 107
    new-instance v5, Landroid/graphics/PointF;

    iget v8, v4, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v2, v4, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-direct {v5, v8, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v14, :cond_4

    .line 109
    new-instance v2, Landroid/graphics/PointF;

    iget v5, v15, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v8, v15, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-direct {v2, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 111
    :cond_4
    new-instance v2, Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    :goto_2
    iget-object v2, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v7, :cond_5

    const/4 v2, 0x2

    .line 115
    :goto_3
    iget-object v5, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v3

    if-ge v2, v5, :cond_5

    .line 116
    iget-object v5, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    if-eq v0, v14, :cond_6

    .line 121
    new-instance v2, Landroid/graphics/PointF;

    iget v5, v15, Lcom/pspdfkit/internal/sn$a;->a:F

    iget v7, v15, Lcom/pspdfkit/internal/sn$a;->b:F

    invoke-direct {v2, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_6
    iget v2, v9, Lcom/pspdfkit/internal/y4;->r:F

    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    const/4 v7, 0x0

    invoke-static {v1, v2, v5, v7}, Lcom/pspdfkit/internal/k5;->a(Ljava/util/ArrayList;FLandroid/graphics/Path;Z)Landroid/graphics/Path;

    :cond_7
    :goto_4
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v12, v1

    if-eqz v1, :cond_8

    .line 131
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 132
    invoke-virtual {v2, v12, v12}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 133
    iget-object v5, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    iget-object v7, v9, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    .line 134
    invoke-virtual {v7, v5}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 135
    invoke-virtual {v7, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 136
    iget-object v2, v9, Lcom/pspdfkit/internal/sn;->x:Landroid/graphics/Path;

    invoke-virtual {v10, v2, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_5

    .line 138
    :cond_8
    iget-object v2, v9, Lcom/pspdfkit/internal/sn;->w:Landroid/graphics/Path;

    invoke-virtual {v10, v2, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 139
    :goto_5
    iget-object v2, v9, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz v2, :cond_11

    iget-object v5, v9, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-eqz v5, :cond_11

    iget-object v5, v9, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    if-nez v5, :cond_9

    goto/16 :goto_9

    .line 141
    :cond_9
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ri;->a()Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    move-result-object v2

    sget-object v5, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    const-wide v18, 0x400921fb54442d18L    # Math.PI

    if-ne v2, v5, :cond_e

    .line 142
    iget-object v2, v9, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    .line 143
    iget-object v5, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 144
    iget-object v8, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    .line 149
    iget v3, v9, Lcom/pspdfkit/internal/sn;->z:F

    move-object/from16 v21, v8

    float-to-double v7, v3

    const-wide v22, 0x4012d97c7f3321d2L    # 4.71238898038469

    const-wide v24, 0x3ff921fb54442d18L    # 1.5707963267948966

    cmpg-double v3, v7, v22

    if-gez v3, :cond_a

    cmpl-double v3, v7, v24

    if-lez v3, :cond_a

    const/4 v3, 0x1

    goto :goto_6

    :cond_a
    const/4 v3, 0x0

    :goto_6
    if-eqz v3, :cond_c

    cmpl-double v3, v7, v18

    if-lez v3, :cond_b

    sub-double v7, v7, v18

    goto :goto_7

    :cond_b
    add-double v7, v7, v18

    :cond_c
    :goto_7
    move-object/from16 v22, v13

    move-object/from16 v23, v14

    .line 156
    invoke-static {v7, v8}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v13

    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v3

    const/high16 v18, 0x41600000    # 14.0f

    add-float v3, v3, v18

    iget-object v11, v9, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    move-object/from16 v26, v15

    iget v15, v9, Lcom/pspdfkit/internal/h4;->b:F

    move-object/from16 v27, v4

    const/16 v4, 0x9

    new-array v4, v4, [F

    .line 159
    invoke-virtual {v11, v4}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v11, 0x0

    aget v4, v4, v11

    div-float/2addr v4, v15

    mul-float v4, v4, v3

    float-to-double v3, v4

    sub-double v15, v7, v24

    .line 161
    iget v11, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v24, v0

    move-object/from16 v0, v21

    move-object/from16 v21, v6

    iget v6, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v11, v6

    move-wide/from16 v17, v7

    float-to-double v6, v11

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->cos(D)D

    move-result-wide v19

    mul-double v19, v19, v3

    add-double v6, v19, v6

    .line 162
    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v0

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v5, v0

    float-to-double v8, v5

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    mul-double v15, v15, v3

    add-double v3, v15, v8

    .line 163
    invoke-static {v3, v4, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    mul-double v6, v6, v6

    mul-double v3, v3, v3

    add-double/2addr v3, v6

    .line 164
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    sub-double v8, v8, v17

    .line 167
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double v5, v5, v3

    double-to-float v0, v5

    .line 168
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double v5, v5, v3

    double-to-float v3, v5

    .line 170
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    double-to-float v4, v13

    .line 172
    invoke-virtual {v10, v4}, Landroid/graphics/Canvas;->rotate(F)V

    if-eqz v1, :cond_d

    .line 173
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 174
    invoke-virtual {v1, v12, v12}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 175
    invoke-virtual {v10, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_d
    move-object/from16 v9, p0

    .line 178
    iget-object v1, v9, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {v10, v2, v0, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 179
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_a

    :cond_e
    move-object/from16 v24, v0

    move-object/from16 v27, v4

    move-object/from16 v21, v6

    move-object/from16 v22, v13

    move-object/from16 v23, v14

    move-object/from16 v26, v15

    .line 180
    iget-object v0, v9, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    .line 181
    iget-object v2, v9, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 183
    iget v3, v9, Lcom/pspdfkit/internal/sn;->A:F

    float-to-double v3, v3

    cmpl-double v5, v3, v18

    if-lez v5, :cond_f

    const-wide v5, 0x401921fb54442d18L    # 6.283185307179586

    cmpg-double v7, v3, v5

    if-gez v7, :cond_f

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v3

    const/high16 v4, 0x41600000    # 14.0f

    add-float/2addr v3, v4

    iget-object v4, v9, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget v5, v9, Lcom/pspdfkit/internal/h4;->b:F

    const/16 v6, 0x9

    new-array v6, v6, [F

    .line 186
    invoke-virtual {v4, v6}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v4, 0x0

    aget v4, v6, v4

    div-float/2addr v4, v5

    mul-float v4, v4, v3

    neg-float v3, v4

    goto :goto_8

    .line 188
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v3

    const/high16 v4, 0x41000000    # 8.0f

    add-float/2addr v3, v4

    iget-object v4, v9, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget v5, v9, Lcom/pspdfkit/internal/h4;->b:F

    const/16 v6, 0x9

    new-array v6, v6, [F

    .line 190
    invoke-virtual {v4, v6}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v4, 0x0

    aget v4, v6, v4

    div-float/2addr v4, v5

    mul-float v4, v4, v3

    .line 192
    iget-object v3, v9, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    .line 193
    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    add-float/2addr v3, v4

    .line 195
    :goto_8
    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 196
    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    .line 198
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    if-eqz v1, :cond_10

    .line 199
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 200
    invoke-virtual {v1, v12, v12}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 201
    invoke-virtual {v10, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 204
    :cond_10
    iget-object v1, v9, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {v10, v0, v4, v2, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 205
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_a

    :cond_11
    :goto_9
    move-object/from16 v24, v0

    move-object/from16 v27, v4

    move-object/from16 v21, v6

    move-object/from16 v22, v13

    move-object/from16 v23, v14

    move-object/from16 v26, v15

    .line 206
    :goto_a
    iget-object v0, v9, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    sget-object v11, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    const/4 v13, 0x0

    const/high16 v14, 0x3fe00000    # 1.75f

    if-eq v0, v11, :cond_13

    .line 210
    move-object v5, v0

    check-cast v5, Lcom/pspdfkit/annotations/LineEndType;

    .line 211
    iget v0, v9, Lcom/pspdfkit/internal/y4;->q:F

    iget v1, v9, Lcom/pspdfkit/internal/sn;->B:F

    move-object/from16 v6, v21

    move-object/from16 v2, v24

    .line 212
    invoke-virtual {v6, v2}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, v27

    const/4 v7, 0x0

    goto :goto_b

    :cond_12
    mul-float v0, v0, v14

    add-float/2addr v0, v1

    move v7, v0

    move-object/from16 v0, v27

    .line 213
    :goto_b
    iget v8, v0, Lcom/pspdfkit/internal/sn$a;->c:F

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    .line 214
    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/sn;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;FLcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;FF)V

    .line 224
    :cond_13
    iget-object v0, v9, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    if-eq v0, v11, :cond_15

    .line 228
    move-object v5, v0

    check-cast v5, Lcom/pspdfkit/annotations/LineEndType;

    .line 229
    iget v0, v9, Lcom/pspdfkit/internal/y4;->q:F

    iget v1, v9, Lcom/pspdfkit/internal/sn;->B:F

    move-object/from16 v4, v22

    move-object/from16 v6, v23

    .line 230
    invoke-virtual {v4, v6}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    move-object/from16 v0, v26

    const/4 v7, 0x0

    goto :goto_c

    :cond_14
    mul-float v0, v0, v14

    add-float/2addr v0, v1

    move v7, v0

    move-object/from16 v0, v26

    .line 231
    :goto_c
    iget v8, v0, Lcom/pspdfkit/internal/sn$a;->c:F

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    .line 232
    invoke-direct/range {v0 .. v8}, Lcom/pspdfkit/internal/sn;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;FLcom/pspdfkit/annotations/LineEndType;Landroid/graphics/PointF;FF)V

    :cond_15
    return-void
.end method

.method public final a(Landroidx/core/util/Pair;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    return-void
.end method

.method protected final e()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/y4;->e()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/sn;->B:F

    return-void
.end method

.method public final z()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sn;->y:Landroidx/core/util/Pair;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/hd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/hd;->a:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/hd;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/hd;->a:Lcom/pspdfkit/ui/PdfFragment;

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/hd;Lcom/pspdfkit/document/files/EmbeddedFile;I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/hd;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 3
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor;->prepareEmbeddedFileForSharing(Landroid/content/Context;Lcom/pspdfkit/document/files/EmbeddedFile;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/gd;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/gd;-><init>(Lcom/pspdfkit/internal/hd;I)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    const-string p2, "action"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPdfPath()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v0, 0x1

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/pspdfkit/internal/hd;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 34
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/hd;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getEmbeddedFilesProvider()Lcom/pspdfkit/document/files/EmbeddedFilesProvider;

    move-result-object p2

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPdfPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {p2, v1, v0}, Lcom/pspdfkit/document/files/EmbeddedFilesProvider;->getEmbeddedFileWithFileNameAsync(Ljava/lang/String;Z)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 36
    new-instance v1, Lcom/pspdfkit/internal/fd;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/fd;-><init>(Lcom/pspdfkit/internal/hd;Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;)V

    invoke-virtual {p2, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

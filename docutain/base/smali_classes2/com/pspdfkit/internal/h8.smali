.class public final Lcom/pspdfkit/internal/h8;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeDocumentData;

.field private final b:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;Lcom/pspdfkit/internal/jni/NativeDocumentData;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/h8;->b:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/h8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentData;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)I
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    const-string v1, "argumentName"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "PSPDFKit.lastViewedPage"

    const-string v1, "key"

    const/4 v2, 0x0

    .line 105
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/pspdfkit/internal/h8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentData;

    const-string v1, "PSPDFKit.lastViewedPage"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentData;->getInt(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "PSPDFKit.lastViewedPage"

    const-string v1, "key"

    const/4 v2, 0x0

    .line 52
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/h8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentData;

    const-string v1, "PSPDFKit.lastViewedPage"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentData;->clearKey(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "key"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "PSPDFKit.lastViewedPage"

    const-string v1, "key"

    const/4 v2, 0x0

    .line 52
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/h8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentData;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "PSPDFKit.lastViewedPage"

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentData;->putInt(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

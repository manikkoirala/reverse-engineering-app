.class final Lcom/pspdfkit/internal/do$c;
.super Lio/reactivex/rxjava3/core/Scheduler$Worker;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/do;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private final b:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lcom/pspdfkit/internal/do$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public static synthetic $r8$lambda$FdTYWvQeTXPPYyLDPNzRXw0KESU(Lcom/pspdfkit/internal/do$c;Lcom/pspdfkit/internal/do$b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/do$c;->a(Lcom/pspdfkit/internal/do$b;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/PriorityBlockingQueue;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lcom/pspdfkit/internal/do$b;",
            ">;I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lio/reactivex/rxjava3/core/Scheduler$Worker;-><init>()V

    .line 2
    new-instance v0, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/do$c;->a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/do$c;->b:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 13
    iput p2, p0, Lcom/pspdfkit/internal/do$c;->c:I

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/do$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/do$c;->b:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/do$c;->a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public final isDisposed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/do$c;->a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public final schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/do$b;

    iget v1, p0, Lcom/pspdfkit/internal/do$c;->c:I

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/pspdfkit/internal/do$b;-><init>(Ljava/lang/Runnable;ILcom/pspdfkit/internal/do$b-IA;)V

    .line 2
    new-instance v1, Lio/reactivex/rxjava3/internal/schedulers/ScheduledRunnable;

    iget-object v2, p0, Lcom/pspdfkit/internal/do$c;->a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v1, p1, v2}, Lio/reactivex/rxjava3/internal/schedulers/ScheduledRunnable;-><init>(Ljava/lang/Runnable;Lio/reactivex/rxjava3/disposables/DisposableContainer;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/do$c;->a:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    new-instance v2, Lcom/pspdfkit/internal/do$c$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/do$c$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/do$c;Lcom/pspdfkit/internal/do$b;)V

    invoke-static {v2}, Lio/reactivex/rxjava3/disposables/Disposable$-CC;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v2

    invoke-virtual {p1, v2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/do$c;->b:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v0, p2, p3, p4}, Ljava/util/concurrent/PriorityBlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    return-object v1
.end method

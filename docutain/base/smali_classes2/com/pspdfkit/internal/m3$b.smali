.class final Lcom/pspdfkit/internal/m3$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/m3;->a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/m3;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/pspdfkit/internal/v3;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/m3;Landroid/content/Context;Lcom/pspdfkit/internal/v3;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/m3$b;->a:Lcom/pspdfkit/internal/m3;

    iput-object p2, p0, Lcom/pspdfkit/internal/m3$b;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/pspdfkit/internal/m3$b;->c:Lcom/pspdfkit/internal/v3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/pspdfkit/internal/m3$b;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/annotations/SoundAnnotation;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$b;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {p1}, Lcom/pspdfkit/internal/m3;->b(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/internal/a3;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$b;->a:Lcom/pspdfkit/internal/m3;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/a3;->b(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    goto :goto_0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/m3$b;->a:Lcom/pspdfkit/internal/m3;

    iget-object v1, p0, Lcom/pspdfkit/internal/m3$b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/m3$b;->c:Lcom/pspdfkit/internal/v3;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/v3;->c()Z

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/pspdfkit/internal/m3;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V

    :goto_0
    return-void
.end method

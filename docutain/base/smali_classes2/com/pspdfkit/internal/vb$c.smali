.class final Lcom/pspdfkit/internal/vb$c;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/util/List;)Lio/reactivex/rxjava3/core/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/pspdfkit/forms/FormElement;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/forms/ChoiceFormElement;

.field final synthetic b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/ChoiceFormElement;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/forms/ChoiceFormElement;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/pspdfkit/internal/vb$c;->a:Lcom/pspdfkit/forms/ChoiceFormElement;

    iput-object p2, p0, Lcom/pspdfkit/internal/vb$c;->b:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/FormElement;

    const-string v0, "$this$executeAsync"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iget-object p1, p0, Lcom/pspdfkit/internal/vb$c;->a:Lcom/pspdfkit/forms/ChoiceFormElement;

    iget-object v0, p0, Lcom/pspdfkit/internal/vb$c;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/forms/ChoiceFormElement;->setSelectedIndexes(Ljava/util/List;)V

    .line 263
    iget-object p1, p0, Lcom/pspdfkit/internal/vb$c;->a:Lcom/pspdfkit/forms/ChoiceFormElement;

    invoke-static {p1}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/ChoiceFormElement;)V

    .line 264
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

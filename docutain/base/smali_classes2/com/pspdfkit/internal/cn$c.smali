.class public interface abstract Lcom/pspdfkit/internal/cn$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/cn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "c"
.end annotation


# virtual methods
.method public abstract onMoveTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;I)Z
.end method

.method public abstract onTabClosed(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
.end method

.method public abstract onTabSelected(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
.end method

.method public abstract onTabsChanged()V
.end method

.method public abstract shouldCloseTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)Z
.end method

.method public abstract shouldSelectTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)Z
.end method

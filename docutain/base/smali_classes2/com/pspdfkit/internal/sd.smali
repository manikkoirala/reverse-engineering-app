.class public final Lcom/pspdfkit/internal/sd;
.super Lcom/pspdfkit/internal/km;
.source "SourceFile"


# instance fields
.field private final O:[I

.field private P:Z

.field private Q:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p10}, Lcom/pspdfkit/internal/km;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/sd;->P:Z

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/sd;->Q:Z

    .line 46
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p3

    .line 47
    new-array p4, p3, [I

    iput-object p4, p0, Lcom/pspdfkit/internal/sd;->O:[I

    if-lez p3, :cond_2

    .line 51
    aput p1, p4, p1

    const/4 p5, 0x1

    if-le p3, p5, :cond_1

    if-eqz p8, :cond_0

    .line 57
    aput p1, p4, p5

    const/4 p5, 0x2

    :cond_0
    :goto_0
    if-ge p5, p3, :cond_1

    .line 63
    iget-object p4, p0, Lcom/pspdfkit/internal/sd;->O:[I

    add-int/lit8 p7, p5, -0x2

    invoke-static {p7, p1}, Ljava/lang/Math;->max(II)I

    move-result p7

    aget p7, p4, p7

    add-int/2addr p7, p2

    add-int/2addr p7, p6

    aput p7, p4, p5

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    .line 67
    :cond_1
    iget p1, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/km;->v(I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    .line 1
    iget p2, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p2, p1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/sd;->O:[I

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_0

    move p1, v1

    goto :goto_1

    .line 7
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/sd;->O:[I

    aget v3, v2, v1

    if-gt v3, p2, :cond_1

    add-int/lit8 v3, v1, 0x1

    aget v2, v2, v3

    if-ge p2, v2, :cond_1

    move p1, v1

    goto :goto_2

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13
    :cond_2
    :goto_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->o(I)I

    move-result p1

    return p1
.end method

.method public final a(IIIFJ)V
    .locals 8

    .line 20
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v5, v0, p4

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/pspdfkit/internal/ug;->b(IIIFJ)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm;)V
    .locals 4

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 15
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->b(I)I

    move-result v1

    .line 16
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->c(I)I

    move-result v2

    .line 17
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->t(I)I

    move-result v3

    add-int/2addr v3, v1

    .line 18
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->a(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 19
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public final b(II)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/sd;->a(II)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->p(I)I

    move-result p1

    return p1
.end method

.method public final b(Lcom/pspdfkit/internal/dm;)V
    .locals 3

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->t(I)I

    move-result v1

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sd;->a(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    .line 6
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 7
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 8
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    return v0
.end method

.method public final d(II)Z
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->K:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sd;->P:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-ltz p1, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sd;->Q:Z

    if-eqz v0, :cond_3

    if-lez p1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->J:Z

    if-nez v0, :cond_5

    .line 6
    :cond_4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/km;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 7
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v3, 0x7d0

    if-lt v0, v3, :cond_5

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/km;->H:I

    iget v3, p0, Lcom/pspdfkit/internal/km;->I:I

    invoke-virtual {p0, v0, v3}, Lcom/pspdfkit/internal/sd;->a(II)I

    move-result v0

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/sd;->O:[I

    aget v3, v3, v0

    .line 11
    iget-object v4, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    invoke-virtual {v4}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v4

    sub-int/2addr v4, v3

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->e()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 15
    invoke-static {v4, v3, p1}, Lcom/pspdfkit/internal/ug;->a(IFI)I

    move-result p1

    int-to-float p1, p1

    .line 19
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result p1

    float-to-int p1, p1

    mul-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v0

    sub-int/2addr p2, v2

    .line 20
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/internal/sd;->O:[I

    aget p1, p2, p1

    .line 22
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/View;->getScrollX()I

    move-result p2

    sub-int v6, p1, p2

    .line 23
    iget-object v3, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v4

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v5

    const/4 v7, 0x0

    const/16 v8, 0x190

    invoke-virtual/range {v3 .. v8}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_2

    .line 25
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 27
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 28
    iget v3, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    iget v3, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_1
    add-int/2addr v0, v3

    .line 29
    iget v3, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v3, v3, v4

    float-to-int v3, v3

    .line 35
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-ge v3, v4, :cond_7

    const/4 p2, 0x0

    .line 36
    :cond_7
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-ge v0, v3, :cond_8

    const/4 p1, 0x0

    .line 38
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    iget v3, p0, Lcom/pspdfkit/internal/km;->E:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    .line 42
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v2
.end method

.method public final e(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    return-object p1
.end method

.method public final e(II)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sd;->P:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/sd;->Q:Z

    if-eqz v0, :cond_2

    if-lez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/km;->J:Z

    if-nez v0, :cond_4

    :cond_3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/km;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/km;->F:Landroid/widget/OverScroller;

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v3

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v4

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_2

    .line 6
    :cond_4
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v0, v0, v2

    float-to-int v0, v0

    .line 7
    iget v2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_1
    add-int/2addr v0, v2

    .line 8
    iget v2, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ca;->m(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v2, v2, v3

    float-to-int v2, v2

    .line 14
    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-ge v2, v3, :cond_6

    const/4 p2, 0x0

    .line 15
    :cond_6
    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-ge v0, v2, :cond_7

    const/4 p1, 0x0

    .line 17
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/km;->D:I

    iget v2, p0, Lcom/pspdfkit/internal/km;->E:I

    neg-int v3, p1

    neg-int v4, p2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 21
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final f()I
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final f(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sd;->O:[I

    aget p1, v0, p1

    return p1
.end method

.method public final g()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/sd;->h()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public final g(I)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final h()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/sd;->O:[I

    aget v0, v1, v0

    return v0
.end method

.method public final i()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final j(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/sd;->a(IZ)V

    return-void
.end method

.method public final n()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/km;->E:I

    neg-int v0, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final o()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/km;->C:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->E()I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/km;->G()I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final x()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/km;->x()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/km;->G:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 6
    iget v0, p0, Lcom/pspdfkit/internal/km;->D:I

    const/4 v2, 0x0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/sd;->P:Z

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->n(I)I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/pspdfkit/internal/km;->C:F

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 10
    iget v3, p0, Lcom/pspdfkit/internal/km;->B:I

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ca;->s(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    iget v3, p0, Lcom/pspdfkit/internal/ca;->w:I

    :goto_1
    add-int/2addr v0, v3

    .line 11
    iget v3, p0, Lcom/pspdfkit/internal/km;->D:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-gt v0, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iput-boolean v1, p0, Lcom/pspdfkit/internal/sd;->Q:Z

    return-void
.end method

.method public final z()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/km;->A()V

    return-void
.end method

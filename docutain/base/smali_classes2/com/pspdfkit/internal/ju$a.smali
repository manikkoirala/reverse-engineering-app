.class final Lcom/pspdfkit/internal/ju$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ju;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ju$a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ju$a;->a:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ju$a;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ju$a;->b:I

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid:[I

    .line 3
    sget v1, Lcom/pspdfkit/R$attr;->pspdf__thumbnailGridStyle:I

    .line 4
    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_ThumbnailGrid:I

    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 15
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf__selectionCheckBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 17
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ju$a;->a:I

    .line 21
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ThumbnailGrid_pspdf__itemRippleBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__annotation_selection_border:I

    .line 23
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 24
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ju$a;->b:I

    .line 28
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

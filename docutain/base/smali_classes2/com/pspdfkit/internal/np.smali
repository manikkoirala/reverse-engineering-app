.class final Lcom/pspdfkit/internal/np;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/pp;

.field final synthetic b:Lcom/pspdfkit/annotations/actions/RenditionAction;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/pp;Lcom/pspdfkit/annotations/actions/RenditionAction;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/np;->a:Lcom/pspdfkit/internal/pp;

    iput-object p2, p0, Lcom/pspdfkit/internal/np;->b:Lcom/pspdfkit/annotations/actions/RenditionAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/ScreenAnnotation;

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/np;->a:Lcom/pspdfkit/internal/pp;

    invoke-static {v0}, Lcom/pspdfkit/internal/pp;->a(Lcom/pspdfkit/internal/pp;)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getMediaPlayer()Lcom/pspdfkit/internal/yi;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/np;->b:Lcom/pspdfkit/annotations/actions/RenditionAction;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/annotations/actions/RenditionAction;)V

    :cond_0
    return-void
.end method

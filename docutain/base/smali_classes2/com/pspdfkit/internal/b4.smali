.class abstract Lcom/pspdfkit/internal/b4;
.super Lcom/pspdfkit/internal/j4;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/image/ImagePicker$OnImagePickedListener;
.implements Lcom/pspdfkit/internal/ne;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/b4$a;
    }
.end annotation


# instance fields
.field private final j:Lcom/pspdfkit/internal/be;

.field protected k:Lcom/pspdfkit/document/image/ImagePicker;

.field private l:Landroid/graphics/PointF;

.field private m:Z

.field private final n:Lcom/pspdfkit/internal/lq;

.field private final o:Lcom/pspdfkit/internal/xp;

.field private p:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$1l9nToDamk6JNN7k_UIrUtiBy3M(Lcom/pspdfkit/internal/b4;Landroid/net/Uri;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/b4;->a(Landroid/net/Uri;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$7zYDfFb9YoZri8d2O4LF7SQks6k(Lcom/pspdfkit/internal/b4;Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b4;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$dW5pJeyhHLVX4ELmGP-4Kv2uYJ8(Lcom/pspdfkit/internal/b4;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b4;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wgRNPwJ9Um9mo3LCfHByf1pEsAU(Lcom/pspdfkit/internal/b4;Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b4;->c(Lcom/pspdfkit/annotations/StampAnnotation;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/j4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    const/4 p2, 0x0

    .line 2
    iput-boolean p2, p0, Lcom/pspdfkit/internal/b4;->m:Z

    .line 18
    new-instance p2, Lcom/pspdfkit/internal/lq;

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "com.pspdfkit.internal.ImageStampAnnotationCreationMode.SAVED_STATE_FRAGMENT_TAG"

    invoke-direct {p2, v0, v1, p0}, Lcom/pspdfkit/internal/lq;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/internal/ne;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/b4;->n:Lcom/pspdfkit/internal/lq;

    .line 20
    new-instance p2, Lcom/pspdfkit/internal/xp;

    .line 21
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "com.pspdfkit.internal.ImageStampAnnotationCreationMode.IMAGE_SINGLE_SAVED_STATE_FRAGMENT_TAG"

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/xp;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/b4;->o:Lcom/pspdfkit/internal/xp;

    .line 22
    new-instance p1, Lcom/pspdfkit/internal/be;

    iget-object p2, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/be;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/pspdfkit/internal/be;->a()Lcom/pspdfkit/internal/be;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->j:Lcom/pspdfkit/internal/be;

    return-void
.end method

.method private synthetic a(Landroid/net/Uri;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 30
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j4;->g()V

    .line 31
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/b4;->b(Landroid/net/Uri;)V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/b4;->o:Lcom/pspdfkit/internal/xp;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/xp;->a(Ljava/lang/Object;)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/b4;->o:Lcom/pspdfkit/internal/xp;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xp;->a()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    sget v0, Lcom/pspdfkit/R$string;->pspdf__file_not_available:I

    const/4 v1, 0x1

    invoke-static {p2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p2

    .line 35
    invoke-virtual {p2}, Landroid/widget/Toast;->show()V

    .line 36
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/b4;->b(Landroid/net/Uri;)V

    return-void
.end method

.method private a(Lio/reactivex/rxjava3/core/Single;Landroid/net/Uri;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/annotations/StampAnnotation;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .line 19
    new-instance v0, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/b4;)V

    .line 20
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/b4;)V

    .line 21
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/b4;Landroid/net/Uri;)V

    .line 22
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doAfterTerminate(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/b4;)V

    new-instance v1, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/b4$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/b4;Landroid/net/Uri;)V

    .line 28
    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private synthetic a(Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j4;->i()V

    return-void
.end method

.method private synthetic c(Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/j4;->a(Lcom/pspdfkit/annotations/StampAnnotation;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(FF)V
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/pspdfkit/internal/b4;->m:Z

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->f:Lcom/pspdfkit/internal/dm;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/b4;->n:Lcom/pspdfkit/internal/lq;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/lq;->c()Lcom/pspdfkit/internal/me;

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/pspdfkit/internal/b4;->m:Z

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b4;->k()V

    :cond_0
    return-void
.end method

.method public a(Lcom/pspdfkit/internal/os;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/j4;->a(Lcom/pspdfkit/internal/os;)V

    .line 2
    new-instance p1, Lcom/pspdfkit/document/image/ImagePicker;

    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b4;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/pspdfkit/document/image/ImagePicker;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->k:Lcom/pspdfkit/document/image/ImagePicker;

    .line 3
    invoke-virtual {p1, p0}, Lcom/pspdfkit/document/image/ImagePicker;->setOnImagePickedListener(Lcom/pspdfkit/document/image/ImagePicker$OnImagePickedListener;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/b4;->o:Lcom/pspdfkit/internal/xp;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/xp;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/b4$a;

    if-eqz p1, :cond_0

    .line 8
    iget v0, p1, Lcom/pspdfkit/internal/b4$a;->d:I

    iget v1, p0, Lcom/pspdfkit/internal/j4;->g:I

    if-ne v0, v1, :cond_0

    .line 9
    iget-object v0, p1, Lcom/pspdfkit/internal/b4$a;->b:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 10
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 11
    iget-object v0, p1, Lcom/pspdfkit/internal/b4$a;->a:Lio/reactivex/rxjava3/core/Single;

    iget-object p1, p1, Lcom/pspdfkit/internal/b4$a;->c:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/b4;->a(Lio/reactivex/rxjava3/core/Single;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected abstract b(Landroid/net/Uri;)V
.end method

.method public d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    invoke-super {p0}, Lcom/pspdfkit/internal/j4;->d()Z

    const/4 v0, 0x0

    return v0
.end method

.method public h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method protected abstract j()Ljava/lang/String;
.end method

.method protected abstract k()V
.end method

.method public onCameraPermissionDeclined(Z)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/b4;->m:Z

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 5

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b4;->m:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->n:Lcom/pspdfkit/internal/lq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lq;->b()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->n:Lcom/pspdfkit/internal/lq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lq;->a()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/b4;->j:Lcom/pspdfkit/internal/be;

    iget-object v2, p0, Lcom/pspdfkit/internal/j4;->e:Lcom/pspdfkit/internal/zf;

    iget v3, p0, Lcom/pspdfkit/internal/j4;->g:I

    .line 8
    invoke-virtual {v1, v2, v3, v0, p1}, Lcom/pspdfkit/internal/be;->a(Lcom/pspdfkit/internal/zf;ILandroid/graphics/PointF;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->cache()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 12
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 13
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/b4;->a(Lio/reactivex/rxjava3/core/Single;Landroid/net/Uri;)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/b4;->o:Lcom/pspdfkit/internal/xp;

    new-instance v2, Lcom/pspdfkit/internal/b4$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/b4;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    iget v4, p0, Lcom/pspdfkit/internal/j4;->g:I

    invoke-direct {v2, v0, p1, v3, v4}, Lcom/pspdfkit/internal/b4$a;-><init>(Lio/reactivex/rxjava3/core/Single;Landroid/net/Uri;Lio/reactivex/rxjava3/disposables/Disposable;I)V

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/xp;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    .line 16
    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    :cond_0
    return-void
.end method

.method public onImagePickerCancelled()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/b4;->m:Z

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    return-void
.end method

.method public onImagePickerUnknownError()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b4;->onImagePickerCancelled()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__file_not_available:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)Z
    .locals 2

    const-string v0, "STATE_PAGE_INDEX"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/j4;->g:I

    if-ne v0, v1, :cond_0

    const-string v0, "STATE_TOUCH_POINT"

    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/graphics/PointF;

    iput-object p1, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/j4;->g:I

    const-string v1, "STATE_PAGE_INDEX"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/b4;->l:Landroid/graphics/PointF;

    const-string v1, "STATE_TOUCH_POINT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

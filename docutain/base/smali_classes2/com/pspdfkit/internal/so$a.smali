.class final Lcom/pspdfkit/internal/so$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/so;->onRedactionsCleared()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/so;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/so;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/so$a;->a:Lcom/pspdfkit/internal/so;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 5

    .line 1
    check-cast p1, Ljava/util/List;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/pspdfkit/internal/so$a;->a:Lcom/pspdfkit/internal/so;

    .line 134
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 135
    invoke-static {v2}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v3

    const-string v4, "remove(annotation)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-static {v1}, Lcom/pspdfkit/internal/so;->b(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 137
    invoke-static {v1}, Lcom/pspdfkit/internal/so;->e(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/ui/PdfUi;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/ui/PdfUi;->getPdfFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v2}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/so$a;->a:Lcom/pspdfkit/internal/so;

    invoke-static {p1}, Lcom/pspdfkit/internal/so;->d(Lcom/pspdfkit/internal/so;)Lcom/pspdfkit/internal/fl;

    move-result-object p1

    if-eqz p1, :cond_2

    new-instance v1, Lcom/pspdfkit/internal/v5;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/v5;-><init>(Ljava/util/List;)V

    invoke-interface {p1, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_2
    return-void
.end method

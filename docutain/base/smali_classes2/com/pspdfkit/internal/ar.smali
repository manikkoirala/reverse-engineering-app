.class public final Lcom/pspdfkit/internal/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

.field private b:Lcom/pspdfkit/configuration/page/PageScrollMode;

.field private c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

.field private d:Lcom/pspdfkit/configuration/theming/ThemeMode;

.field private e:J

.field private final f:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/theming/ThemeMode;JLjava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/configuration/page/PageScrollDirection;",
            "Lcom/pspdfkit/configuration/page/PageScrollMode;",
            "Lcom/pspdfkit/configuration/page/PageLayoutMode;",
            "Lcom/pspdfkit/configuration/theming/ThemeMode;",
            "J",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "scrollDirection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrollMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "layoutMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themeMode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visibleItems"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    .line 6
    iput-wide p5, p0, Lcom/pspdfkit/internal/ar;->e:J

    .line 7
    iput-object p7, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/ar;)Lcom/pspdfkit/internal/ar;
    .locals 8

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    iget-object v2, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iget-object v3, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iget-object v4, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    iget-wide v5, p0, Lcom/pspdfkit/internal/ar;->e:J

    iget-object v7, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p0, "scrollDirection"

    .line 1
    invoke-static {v1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "scrollMode"

    invoke-static {v2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "layoutMode"

    invoke-static {v3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "themeMode"

    invoke-static {v4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "visibleItems"

    invoke-static {v7, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lcom/pspdfkit/internal/ar;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/ar;-><init>(Lcom/pspdfkit/configuration/page/PageScrollDirection;Lcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/theming/ThemeMode;JLjava/util/EnumSet;)V

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/configuration/page/PageLayoutMode;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .line 7
    iput-wide p1, p0, Lcom/pspdfkit/internal/ar;->e:J

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/page/PageScrollMode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/theming/ThemeMode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    return-void
.end method

.method public final b()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ar;->e:J

    return-wide v0
.end method

.method public final c()Lcom/pspdfkit/configuration/page/PageScrollDirection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/configuration/page/PageScrollMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/configuration/theming/ThemeMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/ar;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/ar;

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    iget-object v3, p1, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iget-object v3, p1, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-eq v1, v3, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iget-object v3, p1, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-eq v1, v3, :cond_4

    return v2

    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    iget-object v3, p1, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-eq v1, v3, :cond_5

    return v2

    :cond_5
    iget-wide v3, p0, Lcom/pspdfkit/internal/ar;->e:J

    iget-wide v5, p1, Lcom/pspdfkit/internal/ar;->e:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_6

    return v2

    :cond_6
    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    iget-object p1, p1, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    return v2

    :cond_7
    return v0
.end method

.method public final f()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lcom/pspdfkit/internal/ar;->e:J

    invoke-static {v2, v3}, Landroidx/work/impl/model/WorkSpec$$ExternalSyntheticBackport0;->m(J)I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lcom/pspdfkit/internal/ar;->a:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    iget-object v1, p0, Lcom/pspdfkit/internal/ar;->b:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iget-object v2, p0, Lcom/pspdfkit/internal/ar;->c:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iget-object v3, p0, Lcom/pspdfkit/internal/ar;->d:Lcom/pspdfkit/configuration/theming/ThemeMode;

    iget-wide v4, p0, Lcom/pspdfkit/internal/ar;->e:J

    iget-object v6, p0, Lcom/pspdfkit/internal/ar;->f:Ljava/util/EnumSet;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SettingsOptions(scrollDirection="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", scrollMode="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", layoutMode="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", themeMode="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", screenTimeoutMillis="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", visibleItems="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

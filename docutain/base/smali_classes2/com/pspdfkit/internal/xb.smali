.class public interface abstract Lcom/pspdfkit/internal/xb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/forms/FormElement;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;"
    }
.end annotation


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public abstract d()V
.end method

.method public abstract getFormElement()Lcom/pspdfkit/forms/FormElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract h()V
.end method

.method public abstract j()Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract n()V
.end method

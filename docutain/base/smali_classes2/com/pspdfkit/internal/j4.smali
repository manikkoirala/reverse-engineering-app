.class public abstract Lcom/pspdfkit/internal/j4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/j4$a;
    }
.end annotation


# instance fields
.field protected final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final c:Lcom/pspdfkit/internal/wc;

.field protected d:Landroid/content/Context;

.field protected e:Lcom/pspdfkit/internal/zf;

.field protected f:Lcom/pspdfkit/internal/dm;

.field protected g:I

.field private h:Lcom/pspdfkit/internal/eo;

.field private final i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;


# direct methods
.method public static synthetic $r8$lambda$39k1Hub5xFlcDM_TIuujIadKqtI(Lcom/pspdfkit/internal/j4;Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/j4;->b(Lcom/pspdfkit/annotations/StampAnnotation;)V

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/j4;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 6
    new-instance p1, Lcom/pspdfkit/internal/wc;

    invoke-direct {p1, v0}, Lcom/pspdfkit/internal/wc;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j4;->c:Lcom/pspdfkit/internal/wc;

    .line 7
    sget-object p2, Lcom/pspdfkit/internal/vc;->a:Lcom/pspdfkit/internal/vc;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/pspdfkit/internal/xc;

    new-instance v1, Lcom/pspdfkit/internal/j4$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/j4$a;-><init>(Lcom/pspdfkit/internal/j4;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/wc;->a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method


# virtual methods
.method protected abstract a(FF)V
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method protected final a(Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 3

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/j4$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/j4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/j4;Lcom/pspdfkit/annotations/StampAnnotation;)V

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/pspdfkit/internal/os;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/j4;->f:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/j4;->e:Lcom/pspdfkit/internal/zf;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/j4;->g:I

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->c:Lcom/pspdfkit/internal/wc;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wc;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final b()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j4;->f()V

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j4;->f()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method protected final g()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method protected final i()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/eo;

    iget-object v1, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/eo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    const/4 v1, 0x1

    .line 3
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->a(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->c(I)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    iget-object v1, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    sget v2, Lcom/pspdfkit/R$string;->pspdf__loading:I

    const/4 v3, 0x0

    .line 8
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/eo;->setMessage(Ljava/lang/CharSequence;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->h:Lcom/pspdfkit/internal/eo;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

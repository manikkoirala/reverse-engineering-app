.class public final Lcom/pspdfkit/internal/pg;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pg$b;,
        Lcom/pspdfkit/internal/pg$a;,
        Lcom/pspdfkit/internal/pg$c;,
        Lcom/pspdfkit/internal/pg$d;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/WeakHashMap;


# direct methods
.method public static synthetic $r8$lambda$gBqq9uaAveUdUhJMenRv0wjFOQk(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->c(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$zfjk8F_tCSFgWysbnNHcM7kSWj4(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/pg;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 0

    .line 17
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 21
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p0

    iget p0, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    and-int/lit16 p0, p0, 0xf0

    return p0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 2

    .line 10
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 11
    :cond_0
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    if-nez p0, :cond_1

    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p0

    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 16
    :goto_0
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/view/Window;->setSoftInputMode(I)V

    return v1
.end method

.method private static declared-synchronized a(Landroid/view/View;)Lcom/pspdfkit/internal/pg$b;
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/pg;

    monitor-enter v0

    .line 25
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_0

    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    check-cast p0, Landroid/app/Activity;

    .line 29
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 30
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.KeyboardUtils"

    const-string v2, "Can\'t retrieve keyboard lock for detached view!"

    .line 31
    invoke-static {v1, v2, p0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x0

    monitor-exit v0

    return-object p0

    .line 35
    :cond_1
    :try_start_1
    sget-object p0, Lcom/pspdfkit/internal/pg;->a:Ljava/util/WeakHashMap;

    invoke-virtual {p0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/pg$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;
    .locals 2

    .line 22
    new-instance v0, Lcom/pspdfkit/internal/pg$c;

    .line 23
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p0

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/pspdfkit/internal/pg$c;-><init>(Landroid/app/Activity;Lcom/pspdfkit/internal/pg$d;Lcom/pspdfkit/internal/pg$c-IA;)V

    return-object v0
.end method

.method public static a(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/pg$d;)Lcom/pspdfkit/internal/pg$c;
    .locals 2

    .line 24
    new-instance v0, Lcom/pspdfkit/internal/pg$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/pspdfkit/internal/pg$c;-><init>(Landroid/app/Activity;Lcom/pspdfkit/internal/pg$d;Lcom/pspdfkit/internal/pg$c-IA;)V

    return-object v0
.end method

.method private static a(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-string v1, "Input method manager is not available."

    const-string v2, "message"

    .line 5
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    .line 6
    invoke-virtual {v0, p0, p2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 8
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/pg$a;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/pspdfkit/internal/pg$a;-><init>(Lcom/pspdfkit/internal/pg$d;Lcom/pspdfkit/internal/pg$a-IA;)V

    invoke-virtual {v0, p0, p2, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    :goto_0
    return-void

    .line 9
    :cond_2
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static b(Landroid/view/View;)V
    .locals 3

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/pg$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/pg$$ExternalSyntheticLambda1;-><init>(Landroid/view/View;)V

    const-class v1, Lcom/pspdfkit/internal/pg;

    monitor-enter v1

    .line 10
    :try_start_0
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;)Lcom/pspdfkit/internal/pg$b;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/pg$b;->a:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    goto :goto_0

    .line 12
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/pg$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 13
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v1

    throw p0
.end method

.method public static b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/e8;->d(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0x21c

    .line 2
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x2

    .line 3
    :cond_1
    invoke-static {p0, p1, v1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V

    return-void
.end method

.method public static b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V
    .locals 1

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/pg$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/pg$$ExternalSyntheticLambda0;-><init>(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;I)V

    const-class p1, Lcom/pspdfkit/internal/pg;

    monitor-enter p1

    .line 5
    :try_start_0
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;)Lcom/pspdfkit/internal/pg$b;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 6
    iget-object p2, p0, Lcom/pspdfkit/internal/pg$b;->a:Ljava/util/HashSet;

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-nez p2, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/pg$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 8
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p0

    monitor-exit p1

    throw p0
.end method

.method private static c(Landroid/view/View;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 5
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 6
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    if-nez v0, :cond_1

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.KeyboardUtils"

    const-string v4, "KeyboardUtils#hideKeyboard was called with a detached view. Hiding keyboard will not work on some device."

    .line 7
    invoke-static {v3, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/inputmethod/InputMethodManager;

    const-string v2, "Input method manager is not available."

    const-string v3, "message"

    .line 10
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_2

    .line 11
    invoke-virtual {p0, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void

    .line 12
    :cond_2
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static declared-synchronized d(Landroid/view/View;)V
    .locals 5

    const-class v0, Lcom/pspdfkit/internal/pg;

    monitor-enter v0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 5
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 6
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.KeyboardUtils"

    const-string v2, "Can\'t lock the keyboard for detached view!"

    .line 7
    invoke-static {v1, v2, p0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    .line 11
    :cond_1
    :try_start_1
    sget-object v2, Lcom/pspdfkit/internal/pg;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/pg$b;

    if-nez v3, :cond_2

    .line 13
    new-instance v3, Lcom/pspdfkit/internal/pg$b;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/pspdfkit/internal/pg$b;-><init>(Lcom/pspdfkit/internal/pg$b-IA;)V

    .line 14
    invoke-virtual {v2, v1, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    :cond_2
    iget-object v1, v3, Lcom/pspdfkit/internal/pg$b;->a:Ljava/util/HashSet;

    invoke-virtual {v1, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized e(Landroid/view/View;)V
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/pg;

    monitor-enter v0

    .line 1
    :try_start_0
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->a(Landroid/view/View;)Lcom/pspdfkit/internal/pg$b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3
    iget-object v2, v1, Lcom/pspdfkit/internal/pg$b;->a:Ljava/util/HashSet;

    invoke-virtual {v2, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4
    iget-object p0, v1, Lcom/pspdfkit/internal/pg$b;->a:Ljava/util/HashSet;

    invoke-virtual {p0}, Ljava/util/HashSet;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    if-nez p0, :cond_0

    .line 5
    iget-object p0, v1, Lcom/pspdfkit/internal/pg$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 6
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

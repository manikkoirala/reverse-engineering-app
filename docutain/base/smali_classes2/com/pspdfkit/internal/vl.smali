.class public final Lcom/pspdfkit/internal/vl;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static synthetic $r8$lambda$NLqZlVXqzo8rV7bWD3nqfJ7wU74(Landroid/app/Activity;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/vl;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private static final a(Landroid/app/Activity;)V
    .locals 2

    .line 4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "PSPDFKit for Android 8.7.3"

    .line 5
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeLicense;->rawJsonString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 7
    sget v1, Lcom/pspdfkit/R$string;->pspdf__ok:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p0

    .line 8
    invoke-virtual {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    .line 9
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public static final a()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->a()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->c()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/vl$$ExternalSyntheticLambda0;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/vl$$ExternalSyntheticLambda0;-><init>(Landroid/app/Activity;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

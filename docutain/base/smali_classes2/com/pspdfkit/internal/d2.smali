.class public final Lcom/pspdfkit/internal/d2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;
.implements Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/d2$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/wf;

.field private final c:Lcom/pspdfkit/internal/vf;

.field private final d:Ljava/util/Random;

.field e:Lio/reactivex/rxjava3/core/Scheduler;

.field private f:Z

.field private g:Lio/reactivex/rxjava3/disposables/Disposable;

.field private h:Lio/reactivex/rxjava3/disposables/Disposable;

.field private i:J

.field private j:Z

.field private k:J


# direct methods
.method public static synthetic $r8$lambda$XI2zh7SoogT887Iv6DsjdTTGKJg(Lcom/pspdfkit/internal/d2;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->f()V

    return-void
.end method

.method public static synthetic $r8$lambda$ceYeZXScMlhdZl1Ii4n1wl88QoM(Lcom/pspdfkit/internal/d2;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/d2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$miLEz-CshCIXF0n4gThFLTQtl_c(Lcom/pspdfkit/internal/d2;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->h()V

    return-void
.end method

.method public static synthetic $r8$lambda$wCEn8AomYoe0c0njBplrkZSFUUw(Lcom/pspdfkit/internal/d2;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->e()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->d:Ljava/util/Random;

    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/internal/d2;->f:Z

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z

    const-wide/high16 v0, -0x8000000000000000L

    .line 28
    iput-wide v0, p0, Lcom/pspdfkit/internal/d2;->k:J

    .line 36
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/wf;

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->b:Lcom/pspdfkit/internal/wf;

    .line 37
    invoke-interface {p1}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->c:Lcom/pspdfkit/internal/vf;

    .line 38
    new-instance v0, Lcom/pspdfkit/internal/d2$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/d2$a;-><init>(Lcom/pspdfkit/internal/d2$a-IA;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/schedulers/Schedulers;->from(Ljava/util/concurrent/Executor;)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->e:Lio/reactivex/rxjava3/core/Scheduler;

    const-wide/16 v0, 0x3e8

    .line 39
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/d2;->a(J)V

    .line 40
    invoke-interface {p1}, Lcom/pspdfkit/instant/document/InstantPdfDocument;->getAnnotationProvider()Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;->addNonAnnotationChangeListener(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;)V

    return-void
.end method

.method private declared-synchronized a()V
    .locals 1

    monitor-enter p0

    .line 85
    :try_start_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->b()V

    .line 86
    iget-object v0, p0, Lcom/pspdfkit/internal/d2;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 87
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->g:Lio/reactivex/rxjava3/disposables/Disposable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lio/reactivex/rxjava3/functions/Action;J)V
    .locals 3

    monitor-enter p0

    .line 80
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 81
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->b()V

    .line 82
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/pspdfkit/internal/d2;->e:Lio/reactivex/rxjava3/core/Scheduler;

    .line 83
    invoke-virtual {v0, p2, p3, v1, v2}, Lio/reactivex/rxjava3/core/Completable;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    .line 84
    invoke-virtual {p2, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/d2;->h:Lio/reactivex/rxjava3/disposables/Disposable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 22
    check-cast p1, Lcom/pspdfkit/instant/exceptions/InstantSyncException;

    if-eqz p1, :cond_0

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object p1

    const-string v0, "errorCode"

    .line 25
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/pspdfkit/internal/kd;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 69
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x0

    .line 70
    :goto_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/d2;->a(Z)V

    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized a(Z)V
    .locals 4

    monitor-enter p0

    .line 71
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->f:Z

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    if-nez p1, :cond_1

    const-wide/16 v0, 0x64

    .line 73
    iput-wide v0, p0, Lcom/pspdfkit/internal/d2;->i:J

    goto :goto_0

    .line 77
    :cond_1
    iget-wide v0, p0, Lcom/pspdfkit/internal/d2;->i:J

    const-wide/16 v2, 0x3e8

    add-long/2addr v2, v0

    iget-object p1, p0, Lcom/pspdfkit/internal/d2;->d:Ljava/util/Random;

    long-to-int v1, v0

    invoke-virtual {p1, v1}, Ljava/util/Random;->nextInt(I)I

    move-result p1

    int-to-long v0, p1

    add-long/2addr v2, v0

    const-wide/32 v0, 0xea60

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/pspdfkit/internal/d2;->i:J

    .line 79
    :goto_0
    new-instance p1, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/d2;)V

    iget-wide v0, p0, Lcom/pspdfkit/internal/d2;->i:J

    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/internal/d2;->a(Lio/reactivex/rxjava3/functions/Action;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized b()V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/d2;->h:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->h:Lio/reactivex/rxjava3/disposables/Disposable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/d2;->a(ZZ)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yr;

    invoke-direct {v1}, Lcom/pspdfkit/internal/yr;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeWith(Lio/reactivex/rxjava3/core/CompletableObserver;)Lio/reactivex/rxjava3/core/CompletableObserver;

    move-result-object v0

    check-cast v0, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->g:Lio/reactivex/rxjava3/disposables/Disposable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/d2;->a(ZZ)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->onErrorComplete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yr;

    invoke-direct {v1}, Lcom/pspdfkit/internal/yr;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeWith(Lio/reactivex/rxjava3/core/CompletableObserver;)Lio/reactivex/rxjava3/core/CompletableObserver;

    move-result-object v0

    check-cast v0, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object v0, p0, Lcom/pspdfkit/internal/d2;->g:Lio/reactivex/rxjava3/disposables/Disposable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private synthetic h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d2;->b:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qe;->a()V

    return-void
.end method

.method private i()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/d2;->k:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/d2;)V

    iget-wide v1, p0, Lcom/pspdfkit/internal/d2;->k:J

    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/d2;->a(Lio/reactivex/rxjava3/functions/Action;J)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ZZ)Lio/reactivex/rxjava3/core/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->a()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/d2;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/d2;->c:Lcom/pspdfkit/internal/vf;

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/vf;->a()Lcom/pspdfkit/internal/re;

    move-result-object v1

    .line 5
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/re;->c(ZZ)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 6
    invoke-virtual {v0, p2}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lorg/reactivestreams/Publisher;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    iget-object v0, p0, Lcom/pspdfkit/internal/d2;->e:Lio/reactivex/rxjava3/core/Scheduler;

    .line 9
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Flowable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/d2$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/d2;)V

    .line 10
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Flowable;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x4e20

    goto :goto_0

    :cond_0
    const-wide/32 v0, 0xdbba0

    .line 20
    :goto_0
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 21
    invoke-virtual {p2, v0, v1, p1}, Lio/reactivex/rxjava3/core/Flowable;->timeout(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    monitor-enter p0

    .line 89
    :try_start_0
    iget-wide v0, p0, Lcom/pspdfkit/internal/d2;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    monitor-exit p0

    return-void

    .line 90
    :cond_0
    :try_start_1
    iput-wide p1, p0, Lcom/pspdfkit/internal/d2;->k:J

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v2, p1, v0

    if-eqz v2, :cond_1

    .line 95
    iget-object p1, p0, Lcom/pspdfkit/internal/d2;->b:Lcom/pspdfkit/internal/wf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/r1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    goto :goto_0

    .line 98
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/d2;->b:Lcom/pspdfkit/internal/wf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/r1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b(Z)V
    .locals 1

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->f:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 5
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/d2;->f:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 9
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/d2;->a(Z)V

    goto :goto_0

    .line 12
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->b()V

    :goto_0
    return-void
.end method

.method public final c()J
    .locals 2

    .line 10
    iget-wide v0, p0, Lcom/pspdfkit/internal/d2;->k:J

    return-wide v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    monitor-exit p0

    return-void

    .line 2
    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/d2;->j:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/d2;->a(Z)V

    goto :goto_0

    .line 9
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d2;->j:Z

    return v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isModified()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->i()V

    :cond_0
    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isModified()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->i()V

    :cond_0
    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isModified()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->i()V

    :cond_0
    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->i()V

    return-void
.end method

.method public final onNonAnnotationChange(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/d2;->i()V

    return-void
.end method

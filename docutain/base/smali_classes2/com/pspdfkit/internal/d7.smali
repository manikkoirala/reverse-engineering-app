.class public final Lcom/pspdfkit/internal/d7;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/d7$a;,
        Lcom/pspdfkit/internal/d7$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/d7$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/jv;

.field private final b:Lcom/pspdfkit/internal/jv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/d7$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/d7$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/d7;->Companion:Lcom/pspdfkit/internal/d7$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/jv;Lcom/pspdfkit/internal/jv;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x3

    const/4 v1, 0x3

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/d7$a;->a:Lcom/pspdfkit/internal/d7$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d7$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/d7;->a:Lcom/pspdfkit/internal/jv;

    iput-object p3, p0, Lcom/pspdfkit/internal/d7;->b:Lcom/pspdfkit/internal/jv;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/d7;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jv$a;->a:Lcom/pspdfkit/internal/jv$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/d7;->a:Lcom/pspdfkit/internal/jv;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget-object p0, p0, Lcom/pspdfkit/internal/d7;->b:Lcom/pspdfkit/internal/jv;

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/jv;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/d7;->a:Lcom/pspdfkit/internal/jv;

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/jv;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d7;->b:Lcom/pspdfkit/internal/jv;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/cn;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/cn$d;,
        Lcom/pspdfkit/internal/cn$e;,
        Lcom/pspdfkit/internal/cn$c;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/dn;

.field private c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

.field private final d:Lcom/pspdfkit/internal/cn$e;

.field private final e:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final f:Ljava/util/ArrayList;

.field private g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

.field private h:Lcom/pspdfkit/internal/cn$c;

.field private final i:Lcom/pspdfkit/internal/cn$d;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/dn;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn;->b:Lcom/pspdfkit/internal/dn;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn;->c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/cn;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/cn$c;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/dn;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 2
    sget-object p1, Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;->CLOSE_ONLY_SELECTED_TAB:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/cn$e;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/cn$e;-><init>(Lcom/pspdfkit/internal/cn;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    .line 8
    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 11
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/cn$d;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/cn$d;-><init>(Lcom/pspdfkit/internal/cn;Lcom/pspdfkit/internal/cn$d-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/cn;->i:Lcom/pspdfkit/internal/cn$d;

    const-string p1, "themeConfiguration"

    .line 26
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iput-object p2, p0, Lcom/pspdfkit/internal/cn;->b:Lcom/pspdfkit/internal/dn;

    .line 28
    invoke-direct {p0}, Lcom/pspdfkit/internal/cn;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__tabs_bar_list:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setSmoothScrollbarEnabled(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 7
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance v1, Lcom/pspdfkit/internal/cn$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/cn$a;-><init>(Lcom/pspdfkit/internal/cn;)V

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 38
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_1

    .line 42
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    sget-object v1, Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;->CLOSE_DISABLED:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    if-eq p1, v1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 47
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    .line 50
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    if-eqz p1, :cond_1

    .line 51
    invoke-interface {p1}, Lcom/pspdfkit/internal/cn$c;->onTabsChanged()V

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;I)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    if-eq v0, p2, :cond_0

    .line 54
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 55
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p1, v0, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)I
    .locals 1

    if-eqz p1, :cond_0

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public final b()V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cn;->setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    if-eqz p1, :cond_2

    .line 10
    invoke-interface {p1}, Lcom/pspdfkit/internal/cn$c;->onTabsChanged()V

    :cond_2
    return-void
.end method

.method public final c()V
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    if-eqz v0, :cond_0

    .line 21
    invoke-interface {v0}, Lcom/pspdfkit/internal/cn$c;->onTabsChanged()V

    :cond_0
    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-eqz v0, :cond_3

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    if-eqz v1, :cond_0

    .line 4
    invoke-interface {v1}, Lcom/pspdfkit/internal/cn$c;->onTabsChanged()V

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    sub-int/2addr p1, v2

    :goto_0
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cn;->setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    .line 13
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->i:Lcom/pspdfkit/internal/cn$d;

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$d;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$d;)Ljava/util/ArrayList;

    move-result-object v1

    .line 15
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 16
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$d;->-$$Nest$mb(Lcom/pspdfkit/internal/cn$d;)V

    :cond_3
    return-void
.end method

.method public getSelectedTab()Lcom/pspdfkit/ui/tabs/PdfTabBarItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    return-object v0
.end method

.method public getTabs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/tabs/PdfTabBarItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setCloseMode(Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->c:Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setDelegate(Lcom/pspdfkit/internal/cn$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->h:Lcom/pspdfkit/internal/cn$c;

    return-void
.end method

.method public setSelectedTab(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cn;->b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)I

    move-result v0

    if-ltz v0, :cond_4

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->f:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    if-ltz v1, :cond_1

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 12
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->d:Lcom/pspdfkit/internal/cn$e;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/cn;->b(Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)I

    move-result p1

    if-ltz p1, :cond_2

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 15
    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v1

    if-lt p1, v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/cn;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 16
    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v1

    if-gt p1, v1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_3

    .line 17
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 21
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/cn;->i:Lcom/pspdfkit/internal/cn$d;

    iget-object v0, p0, Lcom/pspdfkit/internal/cn;->g:Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    .line 22
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/cn$d;->-$$Nest$fputb(Lcom/pspdfkit/internal/cn$d;Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    .line 23
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$d;->-$$Nest$mb(Lcom/pspdfkit/internal/cn$d;)V

    :cond_4
    return-void
.end method

.class final Lcom/pspdfkit/internal/zu$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$c;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$c;->a:Lcom/pspdfkit/internal/zu;

    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputc(Lcom/pspdfkit/internal/zu;I)V

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputd(Lcom/pspdfkit/internal/zu;I)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object p1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result p1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$c;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetc(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    .line 6
    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->e()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 7
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v0, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.VideoView"

    const-string v1, "a=%s,b=%d"

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$c;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetr(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 10
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object p1

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_1
    return-void
.end method

.class final Lcom/pspdfkit/internal/sq$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/sq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/pspdfkit/internal/sq$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/sq$b;->b:Landroid/widget/TextView;

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/sq$b;->a:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 6
    invoke-static {p3}, Lcom/pspdfkit/internal/sq$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/sq$a;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    if-eqz p1, :cond_1

    .line 7
    invoke-static {p3}, Lcom/pspdfkit/internal/sq$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/sq$a;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/pspdfkit/internal/sq$a;Lcom/pspdfkit/internal/sq$b-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/sq$b;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/pspdfkit/internal/sq$a;)V

    return-void
.end method

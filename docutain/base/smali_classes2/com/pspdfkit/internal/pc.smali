.class public final Lcom/pspdfkit/internal/pc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pc$b;
    }
.end annotation


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

.field private final d:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final e:Lcom/pspdfkit/internal/o7;

.field private f:Lcom/pspdfkit/internal/dm;

.field private g:Lcom/pspdfkit/internal/zf;

.field private h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

.field private i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

.field private j:Landroid/graphics/Point;

.field private final k:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;


# direct methods
.method public static synthetic $r8$lambda$0Ht2LrWgj1gqe_GttqkDHNrlmlI(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pc;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/pc;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pc;->b:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/specialMode/handler/a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/dm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/annotations/FreeTextAnnotation;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/pc;)Landroid/graphics/Point;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pc;->j:Landroid/graphics/Point;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputi(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/annotations/FreeTextAnnotation;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/pc;Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->j:Landroid/graphics/Point;

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/pc;FF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/pc;->a(FF)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 16
    iput-object p3, p0, Lcom/pspdfkit/internal/pc;->k:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/pc;->b:Landroid/content/Context;

    .line 18
    iput-object p2, p0, Lcom/pspdfkit/internal/pc;->c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 20
    new-instance p2, Lcom/pspdfkit/internal/o7;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/pc$b;

    invoke-direct {p3, p0, v0}, Lcom/pspdfkit/internal/pc$b;-><init>(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/internal/pc$b-IA;)V

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/pc;->e:Lcom/pspdfkit/internal/o7;

    return-void
.end method

.method private a(FF)V
    .locals 11

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->g:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 31
    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/a;->getTextSize()F

    move-result v3

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/oc;->a(FF)F

    move-result v2

    .line 32
    iget-object v3, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x50

    invoke-static {v3, v4}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    .line 33
    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v4

    mul-float v4, v4, v3

    .line 35
    invoke-static {v4, v0}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result v3

    .line 36
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 39
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 40
    invoke-static {v3, v0}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 41
    new-instance p1, Landroid/graphics/RectF;

    iget p2, v3, Landroid/graphics/PointF;->x:F

    iget v0, v3, Landroid/graphics/PointF;->y:F

    add-float v3, p2, v2

    sub-float v2, v0, v2

    invoke-direct {p1, p2, v0, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 45
    iget-object p2, p0, Lcom/pspdfkit/internal/pc;->g:Lcom/pspdfkit/internal/zf;

    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p2

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p2, Lcom/pspdfkit/utils/Size;->height:F

    iget v3, p2, Lcom/pspdfkit/utils/Size;->width:F

    const/4 v10, 0x0

    invoke-direct {v0, v10, v2, v3, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 47
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ga;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    .line 51
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v4

    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v6

    const-wide/16 v7, 0xc8

    const/4 v9, 0x0

    move-object v5, p1

    .line 55
    invoke-virtual/range {v4 .. v9}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;IJZ)V

    .line 61
    new-instance v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    const-string v3, ""

    invoke-direct {v0, v2, p1, v3}, Lcom/pspdfkit/annotations/FreeTextAnnotation;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 62
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getTextSize()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setTextSize(F)V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v0

    .line 69
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/Annotation;->setBorderStyle(Lcom/pspdfkit/annotations/BorderStyle;)V

    .line 70
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffect(Lcom/pspdfkit/annotations/BorderEffect;)V

    .line 71
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffectIntensity(F)V

    .line 72
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/Annotation;->setBorderDashArray(Ljava/util/List;)V

    .line 73
    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->hasBorder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFont()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setFontName(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v0, v2, :cond_2

    .line 87
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    sget-object v2, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setIntent(Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;)V

    .line 88
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v2

    iget-object v2, v2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setLineEnd(Lcom/pspdfkit/annotations/LineEndType;)V

    .line 91
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    sget-object v2, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    invoke-static {v0, p2, v2, v2, v1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V

    .line 93
    iget-object p2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    .line 94
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 97
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    const/high16 v2, 0x42c80000    # 100.0f

    sub-float/2addr v1, v2

    invoke-static {v10, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    const/high16 v2, 0x42480000    # 50.0f

    sub-float/2addr p1, v2

    invoke-static {v10, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setCallOutPoints(Ljava/util/List;)V

    .line 101
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object p2, p0, Lcom/pspdfkit/internal/pc;->g:Lcom/pspdfkit/internal/zf;

    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    .line 103
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->getPageRotation(I)I

    move-result p2

    .line 104
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;I)V

    goto :goto_1

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v3, p0, Lcom/pspdfkit/internal/pc;->k:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 110
    const-class v4, Lcom/pspdfkit/annotations/configuration/FreeTextAnnotationConfiguration;

    invoke-interface {v0, v2, v3, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/FreeTextAnnotationConfiguration;

    .line 112
    iget-object v2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    new-instance v3, Lcom/pspdfkit/utils/Size;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    invoke-direct {v3, v4, p1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    const/4 p1, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setRotation(ILcom/pspdfkit/utils/Size;)V

    if-eqz v0, :cond_4

    .line 114
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration;->isHorizontalResizingEnabled()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 116
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    invoke-static {p1, p2, v0, v0, v1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V

    goto :goto_1

    .line 118
    :cond_3
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration;->isVerticalResizingEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 121
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->FIXED:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    sget-object v2, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    invoke-static {p1, p2, v0, v2, v1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V

    .line 128
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 129
    iget-object p2, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/pc$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/pc$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/annotations/Annotation;)V

    const/4 v1, 0x1

    invoke-virtual {p2, p1, v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    .line 131
    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->FREETEXT_CALLOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x6

    return v0

    :cond_0
    const/4 v0, 0x5

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->g:Lcom/pspdfkit/internal/zf;

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    .line 8
    new-instance p1, Lcom/pspdfkit/internal/pc$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/pc$a;-><init>(Lcom/pspdfkit/internal/pc;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->e:Lcom/pspdfkit/internal/o7;

    .line 23
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->k:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->c:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/pc;->h:Lcom/pspdfkit/listeners/SimpleDocumentListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->d:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setTextSize(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAlpha()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/pc;->f:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->j()V

    :cond_0
    return-void
.end method

.method public final onAnnotationDeselected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/pc;->i:Lcom/pspdfkit/annotations/FreeTextAnnotation;

    :cond_0
    return-void
.end method

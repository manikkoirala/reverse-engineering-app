.class public final Lcom/pspdfkit/internal/ol;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ol;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ol;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ol;-><init>()V

    .line 2
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    move-result v2

    add-int/2addr v2, v1

    .line 3
    iput v2, v0, Lcom/pspdfkit/internal/ot;->a:I

    .line 4
    iput-object p0, v0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/b;
    .locals 3

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/b;-><init>()V

    const/16 v1, 0x12

    .line 6
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/b;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0xa

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c()Lcom/pspdfkit/internal/n5;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/n5;

    invoke-direct {v0}, Lcom/pspdfkit/internal/n5;-><init>()V

    const/16 v1, 0xc

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/n5;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/n5;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final d()Z
    .locals 3

    const/16 v0, 0xe

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final e()Z
    .locals 3

    const/16 v0, 0x14

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f(I)Lcom/pspdfkit/internal/ol;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ol;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ol;-><init>()V

    const/16 v1, 0xa

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    .line 3
    iput p1, v0, Lcom/pspdfkit/internal/ot;->a:I

    .line 4
    iput-object v1, v0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final f()Z
    .locals 3

    const/16 v0, 0x10

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    const/16 v0, 0x8

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

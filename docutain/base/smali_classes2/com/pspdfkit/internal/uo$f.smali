.class final Lcom/pspdfkit/internal/uo$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/document/PdfDocument;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/uo;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/pspdfkit/document/PdfDocument;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/uo;Landroid/net/Uri;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/uo$f;->a:Lcom/pspdfkit/internal/uo;

    iput-object p2, p0, Lcom/pspdfkit/internal/uo$f;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/pspdfkit/internal/uo$f;->c:Lcom/pspdfkit/document/PdfDocument;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uo$f;->a:Lcom/pspdfkit/internal/uo;

    invoke-static {v0}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/uo;)Lcom/pspdfkit/ui/PdfUi;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/uo$f;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/pspdfkit/internal/uo$f;->c:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object p1

    const-string v1, "fromUri(uri, document.documentSource.password)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->addDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/DocumentCoordinator;->setVisibleDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/uo$f;->a:Lcom/pspdfkit/internal/uo;

    invoke-static {p1}, Lcom/pspdfkit/internal/uo;->d(Lcom/pspdfkit/internal/uo;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/y9;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/document/PdfDocument;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/pspdfkit/document/sharing/ShareTarget;

.field private final e:Lcom/pspdfkit/document/sharing/ShareAction;

.field private f:Landroidx/fragment/app/FragmentActivity;

.field private g:Lcom/pspdfkit/document/sharing/DocumentSharingController;

.field private final h:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

.field private final i:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

.field private j:Z


# direct methods
.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/y9;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/y9;->j:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/y9;Lcom/pspdfkit/document/sharing/SharingOptions;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/y9;->a(Lcom/pspdfkit/document/sharing/SharingOptions;)V

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;Lcom/pspdfkit/document/sharing/ShareTarget;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/y9;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/y9;->h:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/y9;->i:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/y9;->d:Lcom/pspdfkit/document/sharing/ShareTarget;

    .line 7
    invoke-virtual {p5}, Lcom/pspdfkit/document/sharing/ShareTarget;->getShareAction()Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y9;->e:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 8
    iput p6, p0, Lcom/pspdfkit/internal/y9;->b:I

    .line 9
    iput-object p7, p0, Lcom/pspdfkit/internal/y9;->c:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/pspdfkit/document/sharing/SharingOptions;)V
    .locals 5

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    if-nez v0, :cond_0

    return-void

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->d:Lcom/pspdfkit/document/sharing/ShareTarget;

    const-string v2, "action"

    const-string v3, "share"

    if-eqz v1, :cond_1

    .line 11
    iget-object v4, p0, Lcom/pspdfkit/internal/y9;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 12
    invoke-static {v0, v4, v1, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareDocument(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/sharing/ShareTarget;Lcom/pspdfkit/document/sharing/SharingOptions;)Lcom/pspdfkit/document/sharing/DocumentSharingController;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y9;->g:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 14
    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->d:Lcom/pspdfkit/document/sharing/ShareTarget;

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/ShareTarget;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "package_name"

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->d:Lcom/pspdfkit/document/sharing/ShareTarget;

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/ShareTarget;->getShareAction()Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    .line 18
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_0

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget-object v4, p0, Lcom/pspdfkit/internal/y9;->e:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 23
    invoke-static {v0, v1, v4, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareDocument(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/sharing/SharingOptions;)Lcom/pspdfkit/document/sharing/DocumentSharingController;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y9;->g:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    .line 25
    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->e:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 26
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    if-nez v0, :cond_0

    return-void

    .line 31
    :cond_0
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->hide(Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method public final a(Landroidx/fragment/app/FragmentActivity;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->g:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onAttach(Landroid/content/Context;)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->isVisible(Landroidx/fragment/app/FragmentManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/x9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/x9;-><init>(Lcom/pspdfkit/internal/y9;)V

    .line 7
    invoke-static {p1, v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y9;->j:Z

    :cond_1
    :goto_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y9;->j:Z

    return v0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->g:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onDetach()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    if-eqz v0, :cond_5

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDF_CREATION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    new-instance v0, Lcom/pspdfkit/document/sharing/SharingOptions;

    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-direct {v0, v1}, Lcom/pspdfkit/document/sharing/SharingOptions;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/y9;->a(Lcom/pspdfkit/document/sharing/SharingOptions;)V

    goto :goto_0

    .line 6
    :cond_1
    new-instance v0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/y9;->e:Lcom/pspdfkit/document/sharing/ShareAction;

    iget-object v3, p0, Lcom/pspdfkit/internal/y9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget v4, p0, Lcom/pspdfkit/internal/y9;->b:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/PdfDocument;I)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName(Ljava/lang/String;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    .line 13
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/y9;->i:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    if-eqz v1, :cond_3

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/y9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget v3, p0, Lcom/pspdfkit/internal/y9;->b:I

    .line 15
    invoke-interface {v1, v2, v3}, Lcom/pspdfkit/document/sharing/SharingOptionsProvider;->createSharingOptions(Lcom/pspdfkit/document/PdfDocument;I)Lcom/pspdfkit/document/sharing/SharingOptions;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 17
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/y9;->a(Lcom/pspdfkit/document/sharing/SharingOptions;)V

    return-void

    :cond_3
    const/4 v1, 0x0

    .line 24
    iget-object v2, p0, Lcom/pspdfkit/internal/y9;->h:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    if-eqz v2, :cond_4

    .line 25
    invoke-interface {v2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;->createDocumentSharingDialog()Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    move-result-object v1

    :cond_4
    const/4 v2, 0x1

    .line 29
    iput-boolean v2, p0, Lcom/pspdfkit/internal/y9;->j:Z

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/internal/y9;->f:Landroidx/fragment/app/FragmentActivity;

    .line 32
    invoke-virtual {v2}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    .line 33
    invoke-virtual {v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->build()Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    move-result-object v0

    .line 34
    new-instance v3, Lcom/pspdfkit/internal/x9;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/x9;-><init>(Lcom/pspdfkit/internal/y9;)V

    .line 35
    invoke-static {v1, v2, v0, v3}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->show(Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V

    :cond_5
    :goto_0
    return-void
.end method

.class public Lcom/pspdfkit/internal/j2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/i2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/j2$a;,
        Lcom/pspdfkit/internal/j2$b;
    }
.end annotation


# static fields
.field private static final n:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

.field public static final synthetic o:I


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/ui/PdfFragment;

.field private final c:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

.field private final f:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/h;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/o;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/k;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/j;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/m;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/n;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/pspdfkit/internal/no;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/no<",
            "Lcom/pspdfkit/internal/views/annotations/l;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/ArrayList;


# direct methods
.method public static synthetic $r8$lambda$3HWQhyufeYKLQnVDiL-Mkmo9AqA(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$5wSClPtr58VgUOCNeKonLdWzaMs(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->b(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$6lTvRBy_BGEgU8yvzqcGK78sOpo(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$C2Rt1l5-Dx0WqI4h49sH5_dEJb4(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->b(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$JIIEg475lCgw6af4YRvghG_NTG4(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/n;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->f(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/n;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$O0gseNHoi1fzfFeKmodUeTzf_w8(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/j2;)Ljava/lang/Boolean;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/j2;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$RvpVbm4mDgEA7Y8tNxDyjgBaBZM(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/j;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->d(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/j;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$YfqnMr8-iBGTNROOpNPLityczMY(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/m;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->c(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/m;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$YonfVP1nF2XrLlyT0Xns2uSK6hU(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/h;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->c(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/h;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$k5Cf2cmBlrU1o0ovFvSorU0Tz3Y(Lcom/pspdfkit/internal/j2;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/internal/j2;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qKzAPM_vDKnZfxqAESzFzhqvIGE(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$uCJjxzoujp2BZjMN9GLFZWPsim4(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->d(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$vaHZY_pgxXJWoKnM7iaT0NKZnIM(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/j2;->e(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/j2$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/j2$a;-><init>(I)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda11;

    invoke-direct {v0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda11;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/j2;->n:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfFragment"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 10
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 12
    sget-object p1, Lcom/pspdfkit/internal/j2;->n:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    .line 15
    new-instance p1, Lcom/pspdfkit/internal/no;

    const/4 p2, 0x3

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->f:Lcom/pspdfkit/internal/no;

    .line 16
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->g:Lcom/pspdfkit/internal/no;

    .line 17
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->h:Lcom/pspdfkit/internal/no;

    .line 18
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->i:Lcom/pspdfkit/internal/no;

    .line 19
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->j:Lcom/pspdfkit/internal/no;

    .line 20
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->k:Lcom/pspdfkit/internal/no;

    .line 21
    new-instance p1, Lcom/pspdfkit/internal/no;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/no;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->l:Lcom/pspdfkit/internal/no;

    .line 23
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->m:Ljava/util/ArrayList;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 421
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/k;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {v0, v1, p0, p1}, Lcom/pspdfkit/internal/views/annotations/k;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    return-object v0
.end method

.method private static final a(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 419
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/o;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    const/4 v1, 0x0

    .line 420
    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/views/annotations/o;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private static final a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;
    .locals 1

    const-string v0, "it"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 518
    sget-object p0, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->PLATFORM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    return-object p0
.end method

.method private final a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 423
    new-instance v0, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda12;

    invoke-direct {v0, p2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/j2;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    const-string v0, "<this>"

    .line 424
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    check-cast p1, Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x3

    .line 445
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p2, p1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const-string p2, "fromCallable {\n         \u2026yScheduler.PRIORITY_LOW))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private static final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/j2;)Ljava/lang/Boolean;
    .locals 4

    const-string v0, "$annotation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    .line 519
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 520
    sget-object v2, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p0, v2}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    .line 521
    :cond_0
    sget v2, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 522
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    iget-object v3, p1, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v2, v3, p0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    .line 523
    :goto_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v3

    iget-object p1, p1, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v3, p1}, Lcom/pspdfkit/internal/hb;->c(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p1

    .line 525
    instance-of v3, p0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v3, :cond_6

    if-eqz v2, :cond_5

    if-eqz p1, :cond_5

    .line 526
    check-cast p0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 527
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 529
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p1

    sget-object v2, Lcom/pspdfkit/forms/FormType;->PUSHBUTTON:Lcom/pspdfkit/forms/FormType;

    if-ne p1, v2, :cond_3

    .line 531
    check-cast p0, Lcom/pspdfkit/forms/PushButtonFormElement;

    invoke-virtual {p0}, Lcom/pspdfkit/forms/PushButtonFormElement;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p0

    if-eqz p0, :cond_4

    goto :goto_2

    .line 534
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p0

    sget-object p1, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    if-eq p0, p1, :cond_4

    :goto_2
    const/4 p0, 0x1

    goto :goto_3

    :cond_4
    const/4 p0, 0x0

    :goto_3
    if-eqz p0, :cond_5

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    move v0, v2

    .line 535
    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/j2;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$disposable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 422
    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object p1, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-nez p1, :cond_0

    const-string p1, "disposable"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->remove(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/k;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/k;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {v0, v1, p0, p1}, Lcom/pspdfkit/internal/views/annotations/k;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    return-object v0
.end method

.method private static final b(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/l;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {v0, v1, v2, p0}, Lcom/pspdfkit/internal/views/annotations/l;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V

    return-object v0
.end method

.method private static final c(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/h;
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/h;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    const/4 v2, 0x0

    .line 3
    invoke-direct {v0, v1, p0, v2}, Lcom/pspdfkit/internal/views/annotations/h;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;I)V

    return-object v0
.end method

.method private static final c(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/m;
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/m;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object p0

    invoke-direct {v0, v1, p1, v2, p0}, Lcom/pspdfkit/internal/views/annotations/m;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V

    return-object v0
.end method

.method private static final d(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/views/annotations/j;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/j;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-direct {v0, v1, p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    return-object v0
.end method

.method private static final d(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/o;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/o;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    const/4 v1, 0x0

    .line 3
    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/views/annotations/o;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private static final e(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/l;
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/l;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {v0, v1, v2, p0}, Lcom/pspdfkit/internal/views/annotations/l;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V

    return-object v0
.end method

.method private static final f(Lcom/pspdfkit/internal/j2;)Lcom/pspdfkit/internal/views/annotations/n;
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/n;

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object p0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getInternal()Lcom/pspdfkit/internal/ag;

    move-result-object p0

    invoke-interface {p0}, Lcom/pspdfkit/internal/ag;->getViewCoordinator()Lcom/pspdfkit/internal/xm;

    move-result-object p0

    const/4 v3, 0x0

    .line 2
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/xm;->a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p0

    .line 3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v3, v2, p0}, Lcom/pspdfkit/internal/views/annotations/n;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object v0
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            "Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;",
            ")",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;"
        }
    .end annotation

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationRenderStrategy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 5
    new-instance v1, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 9
    sget-object v2, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->AP_STREAM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    if-ne p2, v2, :cond_0

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v2, Lcom/pspdfkit/internal/j2$b;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v2, p2

    packed-switch p2, :pswitch_data_0

    move-object p2, v3

    goto :goto_0

    .line 15
    :pswitch_0
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->l:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_0

    .line 16
    :pswitch_1
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->h:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_0

    .line 17
    :pswitch_2
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->g:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 18
    :goto_0
    iput-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_2

    .line 25
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v2, Lcom/pspdfkit/internal/j2$b;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v2, p2

    packed-switch p2, :pswitch_data_1

    move-object p2, v3

    goto :goto_1

    .line 31
    :pswitch_3
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->k:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_1

    .line 32
    :pswitch_4
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->j:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_1

    .line 33
    :pswitch_5
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->f:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_1

    .line 36
    :pswitch_6
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->l:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda7;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_1

    .line 37
    :pswitch_7
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->h:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda8;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    goto :goto_1

    .line 38
    :pswitch_8
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->g:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda9;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/j2;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 39
    :goto_1
    iput-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 54
    :cond_1
    :goto_2
    iget-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-nez p2, :cond_2

    .line 55
    iget-object p2, p0, Lcom/pspdfkit/internal/j2;->i:Lcom/pspdfkit/internal/no;

    new-instance v2, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda10;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/j2;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/no$a;)Lcom/pspdfkit/internal/mo;

    move-result-object p2

    iput-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 61
    :cond_2
    iget-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const-string v2, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<com.pspdfkit.annotations.Annotation>"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/views/annotations/a;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 65
    new-instance p2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 66
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 67
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 68
    new-instance v0, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/j2$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/j2;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doAfterTerminate(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 76
    new-instance v0, Lcom/pspdfkit/internal/j2$c;

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/j2$c;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    const-string v0, "annotationView: Annotati\u2026BeFocusable\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iput-object p1, p2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-nez p1, :cond_3

    const-string p1, "disposable"

    .line 90
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    move-object v3, p1

    :goto_3
    iget-object p1, p0, Lcom/pspdfkit/internal/j2;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    const-string p2, "<this>"

    .line 91
    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "compositeDisposable"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p1, v3}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 129
    iget-object p1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/internal/views/annotations/a;

    const-string p2, "annotationView"

    .line 130
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    instance-of p2, p1, Lcom/pspdfkit/internal/views/annotations/j;

    if-nez p2, :cond_5

    instance-of p1, p1, Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 p1, 0x1

    :goto_5
    if-eqz p1, :cond_6

    .line 414
    iget-object p1, p0, Lcom/pspdfkit/internal/j2;->m:Ljava/util/ArrayList;

    iget-object p2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_6
    iget-object p1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/internal/views/annotations/a;

    return-object p1

    .line 418
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Annotation view can be created only while document is loaded!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 446
    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    goto :goto_0

    .line 448
    :cond_0
    sget-object p1, Lcom/pspdfkit/internal/j2;->n:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    iput-object p1, p0, Lcom/pspdfkit/internal/j2;->e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    :goto_0
    return-void
.end method

.method public a(Lcom/pspdfkit/internal/views/annotations/a;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "annotationView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 449
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    invoke-interface {v1, v0}, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;->getOverlayRenderStrategy(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    move-result-object v1

    const-string v2, "annotationOverlayRenderS\u2026enderStrategy(annotation)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Overlay render strategy my not be null!"

    const/4 v3, 0x1

    .line 452
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 453
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v2, Lcom/pspdfkit/internal/j2$b;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 476
    :pswitch_0
    sget-object v0, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->AP_STREAM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    if-ne v1, v0, :cond_1

    .line 477
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/j;

    goto :goto_0

    .line 479
    :cond_1
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/n;

    goto :goto_0

    .line 480
    :pswitch_1
    sget-object v0, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->AP_STREAM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    if-ne v1, v0, :cond_2

    .line 481
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/j;

    goto :goto_0

    .line 483
    :cond_2
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/m;

    goto :goto_0

    .line 484
    :pswitch_2
    sget-object v0, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->AP_STREAM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    if-ne v1, v0, :cond_3

    .line 485
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/j;

    goto :goto_0

    .line 487
    :cond_3
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/h;

    goto :goto_0

    .line 512
    :pswitch_3
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/l;

    goto :goto_0

    .line 513
    :pswitch_4
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/k;

    goto :goto_0

    .line 514
    :pswitch_5
    sget-object v0, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->AP_STREAM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    if-ne v1, v0, :cond_4

    .line 515
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/j;

    goto :goto_0

    .line 517
    :cond_4
    instance-of v3, p1, Lcom/pspdfkit/internal/views/annotations/o;

    :goto_0
    return v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final b()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->a:Landroid/content/Context;

    return-object v0
.end method

.method public b(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "annotationView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 9
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/h;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->f:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 10
    :cond_1
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/o;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->g:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 11
    :cond_2
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/m;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->j:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 12
    :cond_3
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->k:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 13
    :cond_4
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/k;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->h:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 14
    :cond_5
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/l;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->l:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    goto :goto_0

    .line 15
    :cond_6
    instance-of v1, p1, Lcom/pspdfkit/internal/views/annotations/j;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/pspdfkit/internal/j2;->i:Lcom/pspdfkit/internal/no;

    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/mo;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/no;->a(Lcom/pspdfkit/internal/mo;)V

    .line 16
    :cond_7
    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/j;

    if-nez v0, :cond_9

    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz v0, :cond_8

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    :cond_9
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_a

    .line 300
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_a
    return-void
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 6

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    invoke-static {p1}, Lcom/pspdfkit/internal/j2$a;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 302
    iget-object p1, p0, Lcom/pspdfkit/internal/j2;->m:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 303
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getApproximateMemoryUsage()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 304
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->q()Lcom/pspdfkit/internal/lp;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 305
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x2

    .line 306
    div-long/2addr v2, v4

    const-wide/32 v4, 0x20000000

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int p1, v2

    if-ge v1, p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public final c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/Annotation;",
            ")",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;"
        }
    .end annotation

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->e:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;->getOverlayRenderStrategy(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    move-result-object v0

    const-string v1, "annotationOverlayRenderS\u2026enderStrategy(annotation)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "Overlay render strategy my not be null!"

    const/4 v2, 0x1

    .line 7
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 8
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    return-object p1
.end method

.method protected final c()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->b:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method protected final d()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j2;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    return-void
.end method

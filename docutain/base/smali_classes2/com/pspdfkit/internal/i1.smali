.class public final Lcom/pspdfkit/internal/i1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/w0;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/du;

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/pspdfkit/document/PdfDocument;

.field private final i:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$NBu2_ztJZCITr1H2SAyO5AMsHW0(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i1;->c(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$hC6Go3aXk8AQvtl4AE_E6LaHJZA(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$j1Rp1hWlyxwvGGFy0qEvOF1sEPE(Lcom/pspdfkit/internal/i1;ILjava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/i1;->a(ILjava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ljot2Gv1CWNUkOXQH680CNRa8vc(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/nh;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/internal/nh;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pXQ26jzmomwjMXorTWJ7D7ts0jc(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/du;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->d:Lcom/pspdfkit/internal/nh;

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    .line 14
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->f:Lcom/pspdfkit/internal/nh;

    .line 18
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    .line 25
    new-instance v0, Lcom/pspdfkit/internal/nh;

    new-instance v1, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/i1;)V

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/nh;-><init>(Lcom/pspdfkit/internal/nh$a;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    .line 30
    iput-object p1, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    return-void
.end method

.method private synthetic a(ILjava/util/List;Ljava/util/List;)V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 32
    invoke-interface {v1, p1, p2, p3}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 30
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/nh;)V
    .locals 0

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-nez p1, :cond_0

    goto :goto_0

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    goto :goto_0

    .line 28
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    :goto_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Annotation listeners touched on non ui thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 12
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic c(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;

    .line 3
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;->onAnnotationDeselected(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 17
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-nez p1, :cond_1

    goto :goto_0

    .line 20
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    goto :goto_0

    .line 23
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/i1;->h:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 2

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;->onEnterAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;->onChangeAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 2

    .line 10
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 12
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;->onChangeAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/y1;Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 2

    .line 13
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;

    .line 15
    invoke-interface {v1, p1, p2, p3}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;->onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final addOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;

    .line 3
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;->onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;->onExitAnnotationCreationMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 2

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;->onEnterAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;->onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 2

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/i1;->b()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;

    .line 7
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;->onExitAnnotationEditingMode(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    new-instance v1, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    new-instance v1, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    new-instance v1, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/annotations/Annotation;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->b:Lcom/pspdfkit/internal/du;

    new-instance v1, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/pspdfkit/internal/i1$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/i1;ILjava/util/List;Ljava/util/List;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final removeOnAnnotationCreationModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnAnnotationDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationDeselectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnAnnotationEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnAnnotationSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i1;->i:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

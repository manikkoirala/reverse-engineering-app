.class public final Lcom/pspdfkit/internal/la;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/z1$b;

.field private final b:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;II)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/z1$b;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/la;->b:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/pspdfkit/internal/z1$b;II)V
    .locals 0

    and-int/lit8 p3, p2, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_1

    const/4 p2, -0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 4
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;I)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/la;->b:I

    return v0
.end method

.method public final b()Lcom/pspdfkit/internal/z1$b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/la;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/la;

    iget-object v1, p0, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    iget-object v3, p1, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    iget v1, p0, Lcom/pspdfkit/internal/la;->b:I

    iget p1, p1, Lcom/pspdfkit/internal/la;->b:I

    if-eq v1, p1, :cond_3

    return v2

    :cond_3
    return v0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/pspdfkit/internal/la;->b:I

    add-int/2addr v1, v0

    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/pspdfkit/internal/la;->a:Lcom/pspdfkit/internal/z1$b;

    iget v1, p0, Lcom/pspdfkit/internal/la;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EditModeHandle(scaleHandle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", editHandle="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

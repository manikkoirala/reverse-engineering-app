.class public final Lcom/pspdfkit/internal/vs;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field private static final t:Ljava/util/HashSet;

.field private static final u:Landroid/graphics/Typeface;

.field private static final v:Landroid/graphics/Typeface;


# instance fields
.field private final a:Lcom/pspdfkit/annotations/StampAnnotation;

.field private final b:I

.field private final c:Landroid/util/DisplayMetrics;

.field private d:Landroid/graphics/Path;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Path;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/RectF;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/RectF;

.field private k:Landroid/graphics/Paint;

.field private final l:F

.field private final m:F

.field private final n:F

.field private final o:F

.field private final p:F

.field private final q:F

.field private r:I

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v2, v1, [Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    sget-object v3, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->INITIAL_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->SIGN_HERE:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->WITNESS:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    const/4 v5, 0x2

    aput-object v3, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/pspdfkit/internal/vs;->t:Ljava/util/HashSet;

    .line 19
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/vs;->u:Landroid/graphics/Typeface;

    .line 22
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/vs;->v:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/vs;->r:I

    .line 3
    iput v0, p0, Lcom/pspdfkit/internal/vs;->s:I

    .line 6
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/StampAnnotation;->getStampType()Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/StampAnnotation;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "StampDrawable can\'t be used with image stamps."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 9
    :cond_1
    :goto_0
    iput-object p2, p0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    .line 10
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/StampAnnotation;->getStampType()Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->fromStampType(Lcom/pspdfkit/annotations/stamps/StampType;)Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    move-result-object v0

    .line 12
    sget-object v1, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->ACCEPTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    if-ne v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/StampAnnotation;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 p2, 0x1

    .line 13
    iput p2, p0, Lcom/pspdfkit/internal/vs;->b:I

    goto :goto_1

    .line 14
    :cond_2
    sget-object v1, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->REJECTED:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    if-ne v0, v1, :cond_3

    invoke-virtual {p2}, Lcom/pspdfkit/annotations/StampAnnotation;->getSubtitle()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_3

    const/4 p2, 0x2

    .line 15
    iput p2, p0, Lcom/pspdfkit/internal/vs;->b:I

    goto :goto_1

    .line 16
    :cond_3
    sget-object p2, Lcom/pspdfkit/internal/vs;->t:Ljava/util/HashSet;

    invoke-virtual {p2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 p2, 0x3

    .line 17
    iput p2, p0, Lcom/pspdfkit/internal/vs;->b:I

    goto :goto_1

    :cond_4
    const/4 p2, 0x4

    .line 19
    iput p2, p0, Lcom/pspdfkit/internal/vs;->b:I

    :goto_1
    const/high16 p2, 0x40c00000    # 6.0f

    .line 22
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/vs;->p:F

    const/high16 v0, 0x3f800000    # 1.0f

    .line 23
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/vs;->q:F

    .line 25
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/vs;->l:F

    const/high16 p2, 0x40800000    # 4.0f

    .line 26
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/vs;->m:F

    const/high16 p2, 0x40000000    # 2.0f

    .line 27
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/vs;->n:F

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/vs;->c:Landroid/util/DisplayMetrics;

    const/high16 p2, 0x41d00000    # 26.0f

    .line 30
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/vs;->o:F

    return-void
.end method

.method private a(Landroid/graphics/Path;I)V
    .locals 4

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    .line 4
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    .line 5
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float p1, p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p1, v1

    int-to-float v0, v0

    div-float v1, v0, v1

    .line 9
    invoke-static {p1, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 10
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    neg-float v1, v1

    .line 11
    invoke-virtual {v3, p1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    const/4 p1, 0x0

    .line 12
    invoke-virtual {v3, p1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    invoke-virtual {p1, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 15
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    .line 16
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    const v0, 0x3eb33333    # 0.35f

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/s5;->a(IF)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 21
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    .line 22
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .line 27
    iput p1, p0, Lcom/pspdfkit/internal/vs;->r:I

    .line 28
    iput p2, p0, Lcom/pspdfkit/internal/vs;->s:I

    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    if-eqz v1, :cond_1

    .line 7
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->i:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    .line 13
    invoke-static {v0}, Lcom/pspdfkit/internal/ws;->b(Lcom/pspdfkit/annotations/StampAnnotation;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/pspdfkit/internal/vs;->i:Landroid/graphics/Paint;

    .line 14
    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->k:Landroid/graphics/Paint;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/pspdfkit/internal/vs;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_3
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/vs;->s:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/vs;->r:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 16

    move-object/from16 v0, p0

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_e

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gtz v1, :cond_0

    goto/16 :goto_6

    .line 2
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_e

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gtz v1, :cond_1

    goto/16 :goto_6

    .line 3
    :cond_1
    iget v1, v0, Lcom/pspdfkit/internal/vs;->b:I

    invoke-static {v1}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v1

    if-eqz v1, :cond_d

    const/4 v2, 0x1

    if-eq v1, v2, :cond_c

    .line 4
    iget v1, v0, Lcom/pspdfkit/internal/vs;->b:I

    const/4 v3, 0x3

    const/4 v4, 0x0

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 6
    :goto_0
    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v5}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v5

    const/high16 v6, 0x3f000000    # 0.5f

    .line 7
    invoke-static {v5, v6}, Lcom/pspdfkit/internal/s5;->a(IF)I

    move-result v6

    .line 8
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 9
    sget-object v8, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 10
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 11
    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 12
    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 13
    iput-object v7, v0, Lcom/pspdfkit/internal/vs;->e:Landroid/graphics/Paint;

    .line 14
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 15
    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 16
    invoke-virtual {v6, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 17
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 18
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 19
    iput-object v6, v0, Lcom/pspdfkit/internal/vs;->g:Landroid/graphics/Paint;

    .line 22
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/high16 v6, 0x40400000    # 3.0f

    const/4 v7, 0x0

    if-eqz v1, :cond_3

    .line 24
    iget v1, v0, Lcom/pspdfkit/internal/vs;->p:F

    iget v8, v0, Lcom/pspdfkit/internal/vs;->q:F

    div-float v6, v8, v6

    add-float/2addr v6, v1

    .line 25
    invoke-static {v5, v6, v6, v7}, Lcom/pspdfkit/internal/ws;->a(Landroid/graphics/RectF;FFF)Landroid/graphics/Path;

    move-result-object v6

    iput-object v6, v0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    .line 28
    invoke-static {v5, v1, v1, v8}, Lcom/pspdfkit/internal/ws;->a(Landroid/graphics/RectF;FFF)Landroid/graphics/Path;

    move-result-object v1

    iput-object v1, v0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    goto :goto_1

    .line 29
    :cond_3
    iget v1, v0, Lcom/pspdfkit/internal/vs;->p:F

    iget v8, v0, Lcom/pspdfkit/internal/vs;->q:F

    .line 30
    new-instance v9, Landroid/graphics/Path;

    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    iput-object v9, v0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    div-float v6, v8, v6

    add-float/2addr v6, v1

    .line 31
    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v9, v5, v6, v6, v10}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 32
    iget-object v6, v0, Lcom/pspdfkit/internal/vs;->d:Landroid/graphics/Path;

    sget-object v9, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v6, v9}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 35
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, v0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    .line 36
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v6, v5, v1, v1, v9}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 37
    iget-object v6, v0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    sget-object v9, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v6, v9}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 39
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 40
    invoke-virtual {v6, v8, v8}, Landroid/graphics/RectF;->inset(FF)V

    .line 41
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v9

    cmpl-float v9, v9, v7

    if-lez v9, :cond_4

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v9

    cmpl-float v9, v9, v7

    if-lez v9, :cond_4

    .line 42
    iget-object v9, v0, Lcom/pspdfkit/internal/vs;->f:Landroid/graphics/Path;

    sub-float/2addr v1, v8

    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v9, v6, v1, v1, v8}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 43
    :cond_4
    :goto_1
    iget v1, v0, Lcom/pspdfkit/internal/vs;->q:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v1, v6

    invoke-virtual {v5, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 44
    iget v1, v0, Lcom/pspdfkit/internal/vs;->b:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    .line 45
    :goto_2
    iget-object v3, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v3}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v3

    .line 47
    iget-object v8, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v8}, Lcom/pspdfkit/internal/ws;->b(Lcom/pspdfkit/annotations/StampAnnotation;)Ljava/lang/String;

    move-result-object v8

    .line 48
    iget-object v9, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v9}, Lcom/pspdfkit/annotations/StampAnnotation;->getSubtitle()Ljava/lang/String;

    move-result-object v9

    .line 50
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v10, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    const/high16 v11, 0x40a00000    # 5.0f

    if-eqz v1, :cond_6

    .line 54
    iget v12, v10, Landroid/graphics/RectF;->left:F

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v13

    div-float/2addr v13, v11

    add-float/2addr v13, v12

    iput v13, v10, Landroid/graphics/RectF;->left:F

    .line 56
    :cond_6
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v10, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    if-eqz v1, :cond_7

    .line 60
    iget v1, v10, Landroid/graphics/RectF;->left:F

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v12

    div-float/2addr v12, v11

    add-float/2addr v12, v1

    iput v12, v10, Landroid/graphics/RectF;->left:F

    :cond_7
    if-eqz v9, :cond_8

    .line 66
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v1, v6

    iget v5, v0, Lcom/pspdfkit/internal/vs;->o:F

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 69
    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    iget v10, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v10, v1

    iput v10, v5, Landroid/graphics/RectF;->bottom:F

    .line 72
    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    iget v10, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v10, v1

    iput v10, v5, Landroid/graphics/RectF;->top:F

    .line 75
    iget v1, v0, Lcom/pspdfkit/internal/vs;->l:F

    iget v10, v0, Lcom/pspdfkit/internal/vs;->n:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/RectF;->inset(FF)V

    .line 76
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    .line 77
    sget-object v1, Lcom/pspdfkit/internal/vs;->u:Landroid/graphics/Typeface;

    invoke-virtual {v11, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 78
    invoke-virtual {v11, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    invoke-virtual {v11, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 80
    invoke-virtual {v11, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 81
    iput-object v11, v0, Lcom/pspdfkit/internal/vs;->k:Landroid/graphics/Paint;

    .line 82
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    .line 85
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    .line 86
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    const/high16 v10, 0x3f400000    # 0.75f

    mul-float v5, v5, v10

    iget-object v15, v0, Lcom/pspdfkit/internal/vs;->c:Landroid/util/DisplayMetrics;

    .line 87
    iget v10, v15, Landroid/util/DisplayMetrics;->density:F

    div-float v12, v1, v10

    div-float v13, v5, v10

    const/4 v14, 0x0

    const/4 v1, 0x0

    move-object v10, v9

    move-object v5, v15

    move v15, v1

    .line 88
    invoke-static/range {v10 .. v15}, Lcom/pspdfkit/internal/vt;->a(Ljava/lang/String;Landroid/graphics/Paint;FFZZ)F

    move-result v1

    .line 89
    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v1, v5

    .line 90
    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->k:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 93
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    iget-object v5, v0, Lcom/pspdfkit/internal/vs;->k:Landroid/graphics/Paint;

    .line 94
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 95
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v5, v9, v4, v11, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 97
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v5, v11

    div-float/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    float-to-int v5, v5

    .line 98
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v11, v10

    div-float/2addr v11, v6

    invoke-static {v7, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    float-to-int v10, v10

    int-to-float v5, v5

    int-to-float v10, v10

    .line 99
    invoke-virtual {v1, v5, v10}, Landroid/graphics/RectF;->inset(FF)V

    .line 100
    :cond_8
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    iget v5, v0, Lcom/pspdfkit/internal/vs;->l:F

    iget v10, v0, Lcom/pspdfkit/internal/vs;->m:F

    invoke-virtual {v1, v5, v10}, Landroid/graphics/RectF;->inset(FF)V

    .line 102
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    .line 103
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/StampAnnotation;->getStampType()Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->a:Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/StampAnnotation;->getStampType()Lcom/pspdfkit/annotations/stamps/StampType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampType;->isStandard()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    .line 104
    :goto_3
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    if-eqz v1, :cond_a

    .line 105
    sget-object v1, Lcom/pspdfkit/internal/vs;->v:Landroid/graphics/Typeface;

    goto :goto_4

    :cond_a
    sget-object v1, Lcom/pspdfkit/internal/vs;->u:Landroid/graphics/Typeface;

    :goto_4
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 106
    invoke-virtual {v10, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 108
    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 109
    iput-object v10, v0, Lcom/pspdfkit/internal/vs;->i:Landroid/graphics/Paint;

    .line 111
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    .line 114
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    .line 115
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    const v3, 0x3f2aaaab

    mul-float v2, v2, v3

    iget-object v3, v0, Lcom/pspdfkit/internal/vs;->c:Landroid/util/DisplayMetrics;

    .line 116
    iget v5, v3, Landroid/util/DisplayMetrics;->density:F

    div-float v11, v1, v5

    div-float v12, v2, v5

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, v9

    move-object v9, v8

    .line 117
    invoke-static/range {v9 .. v14}, Lcom/pspdfkit/internal/vt;->a(Ljava/lang/String;Landroid/graphics/Paint;FFZZ)F

    move-result v2

    .line 118
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float v2, v2, v3

    .line 119
    iget-object v3, v0, Lcom/pspdfkit/internal/vs;->i:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    if-eqz v1, :cond_b

    .line 123
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    iget-object v2, v0, Lcom/pspdfkit/internal/vs;->j:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, v0, Lcom/pspdfkit/internal/vs;->m:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 127
    :cond_b
    iget-object v1, v0, Lcom/pspdfkit/internal/vs;->h:Landroid/graphics/RectF;

    iget-object v2, v0, Lcom/pspdfkit/internal/vs;->i:Landroid/graphics/Paint;

    .line 128
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 129
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v8, v4, v5, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 131
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v2, v4

    div-float/2addr v2, v6

    invoke-static {v7, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    .line 132
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v4, v3

    div-float/2addr v4, v6

    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    int-to-float v2, v2

    int-to-float v3, v3

    .line 133
    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    goto/16 :goto_5

    .line 134
    :cond_c
    sget v1, Lcom/pspdfkit/internal/ws;->b:I

    .line 135
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    const/high16 v2, 0x40d00000    # 6.5f

    const v3, 0x41a0147b    # 20.01f

    .line 136
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    const v4, 0x4214d70a    # 37.21f

    const/high16 v5, 0x42460000    # 49.5f

    .line 137
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    const v6, 0x42a06b85    # 80.21f

    .line 138
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    const v7, 0x419651ec    # 18.79f

    const/high16 v8, 0x42b90000    # 92.5f

    .line 139
    invoke-virtual {v1, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    const v9, 0x427728f6    # 61.79f

    .line 140
    invoke-virtual {v1, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    invoke-virtual {v1, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    invoke-virtual {v1, v8, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 143
    invoke-virtual {v1, v9, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    invoke-virtual {v1, v8, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    invoke-virtual {v1, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    invoke-virtual {v1, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 147
    invoke-virtual {v1, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 148
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    const v2, -0x85fff2

    .line 149
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/vs;->a(Landroid/graphics/Path;I)V

    goto :goto_5

    .line 150
    :cond_d
    sget v1, Lcom/pspdfkit/internal/ws;->b:I

    .line 151
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    const v2, 0x417828f6    # 15.51f

    const v3, 0x424851ec    # 50.08f

    .line 152
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    const/high16 v2, 0x40b00000    # 5.5f

    const v3, 0x422de148    # 43.47f

    .line 153
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x40d0a3d7    # 6.52f

    const v3, 0x41f5999a    # 30.7f

    .line 154
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x427047ae    # 60.07f

    const v4, 0x4236ae14    # 45.67f

    .line 155
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x42befae1    # 95.49f

    const v4, 0x42a8570a    # 84.17f

    .line 156
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x429d6148    # 78.69f

    const v4, 0x42bac28f    # 93.38f

    .line 157
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x42035c29    # 32.84f

    .line 158
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    const v2, 0x41a8cccd    # 21.1f

    const v3, 0x42621eb8    # 56.53f

    .line 159
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    const v2, -0xcaa7e6

    .line 161
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/vs;->a(Landroid/graphics/Path;I)V

    .line 173
    :goto_5
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_e
    :goto_6
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

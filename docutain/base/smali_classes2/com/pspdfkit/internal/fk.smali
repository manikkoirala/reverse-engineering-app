.class public final Lcom/pspdfkit/internal/fk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ik;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/pspdfkit/annotations/Annotation;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/EnumSet;

.field private final e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;J)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Note"

    const-string v1, "iconName"

    .line 2
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rootAnnotation"

    .line 3
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "authorName"

    .line 4
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/fk;->a:Ljava/lang/String;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/fk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/internal/fk;->c:Ljava/lang/String;

    .line 8
    iput-wide p3, p0, Lcom/pspdfkit/internal/fk;->e:J

    .line 9
    const-class p1, Lcom/pspdfkit/internal/kk;

    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/fk;->d:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/pspdfkit/internal/kk;",
            ">;"
        }
    .end annotation

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->d:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/fk;->f:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->d:Ljava/util/EnumSet;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->d:Ljava/util/EnumSet;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->b:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final getColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    return v0
.end method

.method public final getId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/fk;->e:J

    return-wide v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x3

    .line 1
    invoke-static {v0, v1}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 2
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

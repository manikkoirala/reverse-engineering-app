.class public final Lcom/pspdfkit/internal/bb;
.super Lcom/pspdfkit/internal/e0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/e0<",
        "Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$Builder;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 1
    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/e0;-><init>([Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bb;->build()Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration;
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->k:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    const/high16 v2, 0x41500000    # 13.0f

    .line 4
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 5
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->l:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 11
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 17
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/za;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/za;-><init>(Lcom/pspdfkit/internal/h0;)V

    return-object v0
.end method

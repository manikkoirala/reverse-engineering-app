.class public final Lcom/pspdfkit/internal/qe;
.super Lcom/pspdfkit/internal/r1;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider;


# instance fields
.field private final j:Lcom/pspdfkit/internal/vf;

.field private final k:Lcom/pspdfkit/internal/xe;

.field private final l:Ljava/util/HashMap;

.field private final m:Ljava/util/ArrayList;


# direct methods
.method public static synthetic $r8$lambda$WKw8EUnblP8Akz6Z6XCGRn5flU4(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/Annotation;)I
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/qe;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/Annotation;)I

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/vf;Lcom/pspdfkit/internal/xe;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/r1;-><init>(Lcom/pspdfkit/internal/zf;)V

    .line 2
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    .line 13
    iput-object p2, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    .line 14
    iput-object p3, p0, Lcom/pspdfkit/internal/qe;->k:Lcom/pspdfkit/internal/xe;

    .line 15
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/r1;->f()V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/Annotation;)I
    .locals 0

    .line 130
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result p0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method private static a(Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/annotations/Annotation;
    .locals 6

    .line 147
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 148
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v1

    .line 150
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private l(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;
    .locals 2

    .line 1
    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantIdentifier(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 8
    :goto_1
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)Lcom/pspdfkit/annotations/Annotation;
    .locals 5

    .line 158
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationType()Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeAnnotationType;->STAMP:Lcom/pspdfkit/internal/jni/NativeAnnotationType;

    if-ne v0, v1, :cond_5

    .line 159
    iget-object p2, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getProperties(Lcom/pspdfkit/internal/jni/NativeAnnotation;)[B

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    .line 160
    array-length p2, p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 164
    :cond_0
    new-instance p2, Lcom/pspdfkit/internal/p1;

    invoke-direct {p2}, Lcom/pspdfkit/internal/p1;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {p2, p1, v1}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V

    .line 166
    new-instance v1, Lcom/pspdfkit/annotations/StampAnnotation;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2, v0}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLandroid/graphics/Bitmap;)V

    const-string p2, "imageAttachmentId"

    .line 169
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAdditionalDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 173
    new-instance v0, Lcom/pspdfkit/internal/oe;

    iget-object v3, p0, Lcom/pspdfkit/internal/qe;->k:Lcom/pspdfkit/internal/xe;

    invoke-direct {v0, v3, v1, p2}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    invoke-interface {p2, v0}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 178
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p2

    const/4 v0, 0x1

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    .line 181
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p2

    iget-object v2, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    iget-object v4, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 184
    invoke-interface {v3, p1, v4}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object p1

    .line 185
    invoke-interface {p2, v2, p1, v0}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 189
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeFromNativeObjectIfAttached()V

    :cond_3
    return-object v1

    :cond_4
    :goto_0
    return-object v0

    .line 195
    :cond_5
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Z)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/instant/exceptions/InstantException;
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    .line 132
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/vf;->a(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object p1

    .line 133
    sget-object p2, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;->COMMENT_DELETED:Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    monitor-enter v0

    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;

    .line 136
    invoke-interface {v2, p2}, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;->onNonAnnotationChange(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;)V

    goto :goto_0

    .line 138
    :cond_0
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/instant/exceptions/InstantException;
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    .line 140
    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/vf;->a(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object p1

    .line 141
    sget-object p2, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;->COMMENT_CREATED:Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;

    .line 142
    iget-object p3, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    monitor-enter p3

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;

    .line 144
    invoke-interface {v1, p2}, Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;->onNonAnnotationChange(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$NonAnnotationChange;)V

    goto :goto_0

    .line 146
    :cond_0
    monitor-exit p3

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 16

    move-object/from16 v1, p0

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 8
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/pspdfkit/internal/r1;->a(I)Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_0

    .line 10
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    goto :goto_1

    .line 13
    :cond_0
    iget-object v7, v1, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Landroidx/collection/SparseArrayCompat;->remove(I)V

    .line 17
    :goto_1
    iget-object v7, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 18
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->refreshCacheForPage(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 20
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/pspdfkit/internal/qe;->getAnnotations(I)Ljava/util/List;

    move-result-object v8

    .line 23
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;

    .line 24
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getFirst()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    if-nez v10, :cond_3

    .line 26
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    if-nez v10, :cond_2

    goto :goto_2

    .line 28
    :cond_2
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/pspdfkit/internal/qe;->a(Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 30
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    iget-object v11, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 32
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v9

    invoke-virtual {v11, v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantIdentifier(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 34
    iget-object v11, v1, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 37
    :cond_3
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    if-nez v10, :cond_5

    .line 40
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getFirst()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/pspdfkit/internal/qe;->a(Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 42
    iget-object v11, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 43
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getFirst()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v9

    invoke-virtual {v11, v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantIdentifier(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 45
    iget-object v11, v1, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {v11, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_4
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v9

    invoke-interface {v9}, Lcom/pspdfkit/internal/pf;->onDetachedFromDocument()V

    goto :goto_2

    .line 53
    :cond_5
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getFirst()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/pspdfkit/internal/qe;->a(Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v10

    .line 55
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/pspdfkit/internal/qe;->a(Ljava/util/List;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v11

    if-eqz v10, :cond_1

    if-eqz v11, :cond_1

    .line 57
    invoke-interface {v6, v10}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 58
    move-object v12, v8

    check-cast v12, Ljava/util/ArrayList;

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {v10, v11}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 64
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v13

    .line 66
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v14

    invoke-interface {v14}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v14

    .line 67
    invoke-interface {v13, v14}, Lcom/pspdfkit/internal/pf;->setProperties(Lcom/pspdfkit/internal/p1;)V

    .line 69
    invoke-virtual {v9}, Lcom/pspdfkit/internal/jni/NativeAnnotationMapping;->getSecond()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v9

    .line 71
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v13

    iget-object v14, v1, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    iget-object v15, v1, Lcom/pspdfkit/internal/r1;->c:Lcom/pspdfkit/internal/mj;

    move-object/from16 p1, v2

    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 74
    invoke-interface {v15, v9, v2}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)Lcom/pspdfkit/internal/oj;

    move-result-object v2

    const/4 v15, 0x0

    .line 75
    invoke-interface {v13, v14, v2, v15}, Lcom/pspdfkit/internal/pf;->onAttachToDocument(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/oj;Z)V

    .line 81
    invoke-virtual {v10}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    .line 83
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v11

    invoke-interface {v11}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v11

    .line 84
    invoke-interface {v2, v11}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    .line 86
    invoke-static {v10, v9}, Lcom/pspdfkit/internal/kn;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V

    .line 87
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    move-object/from16 p1, v2

    .line 89
    :goto_3
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v2, p1

    goto/16 :goto_2

    :cond_7
    move-object/from16 p1, v2

    .line 96
    new-instance v2, Lcom/pspdfkit/internal/qe$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/pspdfkit/internal/qe$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {v8, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 97
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v8}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 100
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 101
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 102
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 105
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 106
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->clearModified()V

    goto :goto_4

    .line 110
    :cond_8
    iget-object v2, v1, Lcom/pspdfkit/internal/r1;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;

    .line 112
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/annotations/Annotation;

    .line 113
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v9

    invoke-interface {v9}, Lcom/pspdfkit/internal/pf;->notifyAnnotationCreated()V

    .line 114
    invoke-interface {v3, v8}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_5

    .line 117
    :cond_a
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/annotations/Annotation;

    .line 118
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v9

    invoke-interface {v9}, Lcom/pspdfkit/internal/pf;->notifyAnnotationUpdated()V

    .line 119
    invoke-interface {v3, v8}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_6

    .line 122
    :cond_b
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/annotations/Annotation;

    .line 123
    invoke-virtual {v8}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v9

    invoke-interface {v9}, Lcom/pspdfkit/internal/pf;->notifyAnnotationRemoved()V

    .line 124
    invoke-interface {v3, v8}, Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;->onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_7

    :cond_c
    move-object/from16 v2, p1

    goto/16 :goto_0

    .line 128
    :cond_d
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 129
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 1

    .line 151
    monitor-enter p0

    .line 152
    :try_start_0
    invoke-super {p0}, Lcom/pspdfkit/internal/r1;->a()V

    .line 156
    invoke-virtual {p0}, Lcom/pspdfkit/internal/r1;->e()V

    .line 157
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V
    .locals 5

    .line 196
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v1, :cond_1

    .line 197
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    const-string v1, "stampAnnotation"

    .line 198
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotationProvider"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "nativeAnnotation"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/StampAnnotation;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 251
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v0

    const/16 v2, 0xfa0

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    goto :goto_0

    .line 255
    :cond_0
    new-instance v3, Lcom/pspdfkit/internal/p1;

    invoke-direct {v3}, Lcom/pspdfkit/internal/p1;-><init>()V

    const/16 v4, 0x1772

    .line 256
    invoke-virtual {v3, v4, v1}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 257
    invoke-virtual {v3, v2, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 258
    invoke-virtual {v3, p0, p2}, Lcom/pspdfkit/internal/p1;->a(Lcom/pspdfkit/internal/qf;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Z

    .line 259
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 260
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->markAsInstantCommentRoot()V

    :cond_2
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 5

    .line 261
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_7

    .line 262
    check-cast p1, Lcom/pspdfkit/annotations/StampAnnotation;

    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->k:Lcom/pspdfkit/internal/xe;

    const-string v1, "stampAnnotation"

    .line 263
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "assetProvider"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/StampAnnotation;->hasBitmap()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v4

    instance-of v4, v4, Lcom/pspdfkit/internal/oe;

    if-nez v4, :cond_6

    .line 338
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v1

    .line 369
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/StampAnnotation;->hasBitmap()Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v1, :cond_5

    .line 373
    check-cast v1, Lcom/pspdfkit/internal/a0;

    const-string v4, "annotationResource"

    .line 374
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    instance-of v3, v1, Lcom/pspdfkit/internal/oe;

    if-eqz v3, :cond_0

    check-cast v1, Lcom/pspdfkit/internal/oe;

    goto :goto_1

    .line 415
    :cond_0
    invoke-virtual {v1}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    .line 416
    invoke-virtual {v1}, Lcom/pspdfkit/internal/a0;->l()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v1, Lcom/pspdfkit/internal/oe;

    invoke-direct {v1, v0, v3, v4}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V

    goto :goto_1

    .line 417
    :cond_1
    invoke-virtual {v1}, Lcom/pspdfkit/internal/a0;->n()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_2

    new-instance v1, Lcom/pspdfkit/internal/oe;

    invoke-direct {v1, v0, v3, v4}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 418
    :cond_2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/a0;->m()[B

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v4, Lcom/pspdfkit/internal/oe;

    invoke-direct {v4, v0, v3, v1}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;[B)V

    move-object v1, v4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_4

    .line 419
    :goto_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/pspdfkit/internal/pf;->setAnnotationResource(Lcom/pspdfkit/internal/x1;)V

    const/4 p1, 0x1

    goto :goto_2

    .line 420
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "AnnotationBitmapResource was not initialized correctly!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 421
    :cond_5
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Instant does not support standard stamps, only image stamps are supported."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    const/4 p1, 0x0

    :goto_2
    or-int/2addr v2, p1

    :cond_7
    return v2
.end method

.method public final addNonAnnotationChangeListener(Lcom/pspdfkit/instant/annotations/InstantAnnotationProvider$OnNonAnnotationChangeListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/qe;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final getAnnotationForIdentifier(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;
    .locals 6

    const-string v0, "identifier"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "identifier"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 56
    monitor-exit p0

    return-object v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 61
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->d:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v2}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 62
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/qe;->getAnnotations(I)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    .line 63
    invoke-direct {p0, v4}, Lcom/pspdfkit/internal/qe;->l(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 64
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 65
    monitor-exit p0

    return-object v4

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 71
    :cond_3
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    .line 72
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final getAnnotations(I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/r1;->getAnnotations(I)Ljava/util/List;

    move-result-object p1

    .line 3
    move-object v0, p1

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 9
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantIdentifier(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 11
    iget-object v4, p0, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantCommentCount(Lcom/pspdfkit/internal/jni/NativeAnnotation;)I

    move-result v2

    if-lez v2, :cond_0

    .line 16
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->markAsInstantCommentRoot()V

    goto :goto_0

    .line 19
    :cond_3
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 20
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final getIdentifierForAnnotation(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;
    .locals 2

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qe;->l(Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 56
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The given annotation is not managed by this document"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final hasUnsavedChanges()Z
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Lcom/pspdfkit/internal/r1;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->g()Lcom/pspdfkit/instant/document/InstantDocumentState;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/instant/document/InstantDocumentState;->CLEAN:Lcom/pspdfkit/instant/document/InstantDocumentState;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final invalidateCache()V
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Lcom/pspdfkit/internal/r1;->invalidateCache()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final k(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/instant/exceptions/InstantException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vf;->a(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final m(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qe;->j:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vf;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/qe;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    :cond_0
    return-void
.end method

.method public final removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 1
    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/r1;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getInstantIdentifier(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/r1;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    if-eqz v0, :cond_1

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/qe;->l:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

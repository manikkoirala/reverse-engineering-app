.class public final Lcom/pspdfkit/internal/dn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final l:[I

.field private static final m:I

.field private static final n:I


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__TabBar:[I

    sput-object v0, Lcom/pspdfkit/internal/dn;->l:[I

    .line 4
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__tabBarStyle:I

    sput v0, Lcom/pspdfkit/internal/dn;->m:I

    .line 7
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_TabBar:I

    sput v0, Lcom/pspdfkit/internal/dn;->n:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/pspdfkit/internal/dn;->l:[I

    .line 52
    sget v2, Lcom/pspdfkit/internal/dn;->m:I

    .line 53
    sget v3, Lcom/pspdfkit/internal/dn;->n:I

    const/4 v4, 0x0

    .line 54
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.theme.obtainStyl\u2026_STYLE_RESOURCE\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__backgroundColor:I

    .line 63
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 64
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->a:I

    .line 70
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabColor:I

    const/4 v2, 0x0

    .line 71
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->b:I

    .line 77
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabIndicatorColor:I

    .line 78
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white_quarter_translucent:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 79
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->c:I

    .line 85
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabTextColor:I

    .line 86
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 87
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->d:I

    .line 93
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabTextColorSelected:I

    .line 94
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 95
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->e:I

    .line 101
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabIconColor:I

    .line 102
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 103
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 109
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabIconColorSelected:I

    .line 110
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 111
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->f:I

    .line 117
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabBarHeight:I

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__tab_bar_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 119
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->g:I

    .line 125
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabBarMinimumWidth:I

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__tab_bar_minimum_width:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 127
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->i:I

    .line 133
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabBarMaximumWidth:I

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__tab_bar_maximum_width:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 135
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->j:I

    .line 141
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__TabBar_pspdf__tabBarTextSize:I

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__tab_bar_text_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 143
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/dn;->k:I

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__tab_bar_item_margin_width:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/dn;->h:I

    .line 150
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->g:I

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->h:I

    return v0
.end method

.method public final d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->j:I

    return v0
.end method

.method public final e()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->i:I

    return v0
.end method

.method public final f()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->k:I

    return v0
.end method

.method public final g()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->b:I

    return v0
.end method

.method public final h()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->f:I

    return v0
.end method

.method public final i()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->c:I

    return v0
.end method

.method public final j()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->d:I

    return v0
.end method

.method public final k()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/dn;->e:I

    return v0
.end method

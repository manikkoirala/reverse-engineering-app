.class final Lcom/pspdfkit/internal/md;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/od;

.field final synthetic b:Lcom/pspdfkit/annotations/actions/HideAction;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/od;Lcom/pspdfkit/annotations/actions/HideAction;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/md;->a:Lcom/pspdfkit/internal/od;

    iput-object p2, p0, Lcom/pspdfkit/internal/md;->b:Lcom/pspdfkit/annotations/actions/HideAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 5

    .line 1
    check-cast p1, Ljava/util/List;

    const-string v0, "annotations"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/md;->b:Lcom/pspdfkit/annotations/actions/HideAction;

    .line 55
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 56
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getFlags()Ljava/util/EnumSet;

    move-result-object v3

    const-string v4, "annotation.flags"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationFlags;->INVISIBLE:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v3, v4}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 58
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationFlags;->NOVIEW:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v3, v4}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/HideAction;->shouldHide()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 61
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v3, v4}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 63
    :cond_0
    sget-object v4, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v3, v4}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    .line 65
    :goto_1
    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/Annotation;->setFlags(Ljava/util/EnumSet;)V

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/md;->a:Lcom/pspdfkit/internal/od;

    invoke-static {v0}, Lcom/pspdfkit/internal/od;->a(Lcom/pspdfkit/internal/od;)Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Ljava/util/List;)V

    return-void
.end method

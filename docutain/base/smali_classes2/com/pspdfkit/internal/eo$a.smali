.class final Lcom/pspdfkit/internal/eo$a;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/eo;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/eo;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/eo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .line 1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {p1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgetb(Lcom/pspdfkit/internal/eo;)Landroid/widget/ProgressBar;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result p1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {v0}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgetb(Lcom/pspdfkit/internal/eo;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {v1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgetf(Lcom/pspdfkit/internal/eo;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, ""

    if-eqz v2, :cond_0

    .line 8
    invoke-static {v1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgete(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 10
    :cond_0
    invoke-static {v1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgete(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {v1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgeth(Lcom/pspdfkit/internal/eo;)Ljava/text/NumberFormat;

    move-result-object v2

    if-eqz v2, :cond_1

    int-to-double v1, p1

    int-to-double v5, v0

    div-double/2addr v1, v5

    .line 14
    new-instance p1, Landroid/text/SpannableString;

    iget-object v0, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {v0}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgeth(Lcom/pspdfkit/internal/eo;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 15
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 18
    invoke-virtual {p1}, Landroid/text/SpannableString;->length()I

    move-result v1

    const/16 v2, 0x21

    .line 19
    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/eo$a;->a:Lcom/pspdfkit/internal/eo;

    invoke-static {v0}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgetg(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 26
    :cond_1
    invoke-static {v1}, Lcom/pspdfkit/internal/eo;->-$$Nest$fgetg(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

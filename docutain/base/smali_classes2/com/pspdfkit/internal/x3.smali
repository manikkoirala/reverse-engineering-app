.class public final Lcom/pspdfkit/internal/x3;
.super Lcom/pspdfkit/internal/z5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/z5<",
        "Lkotlin/Unit;",
        "Ljava/util/List<",
        "+",
        "Lcom/pspdfkit/internal/db;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final d:Lkotlin/Unit;

.field private final e:Lkotlinx/serialization/KSerializer;

.field private final f:Lkotlinx/serialization/KSerializer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/z5;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->AVAILABLE_FACES:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/x3;->c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 3
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iput-object v0, p0, Lcom/pspdfkit/internal/x3;->d:Lkotlin/Unit;

    .line 4
    invoke-static {v0}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->serializer(Lkotlin/Unit;)Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/x3;->e:Lkotlinx/serialization/KSerializer;

    .line 6
    sget-object v0, Lcom/pspdfkit/internal/db;->Companion:Lcom/pspdfkit/internal/db$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/db$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    invoke-static {v0}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->ListSerializer(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/x3;->f:Lkotlinx/serialization/KSerializer;

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x3;->d:Lkotlin/Unit;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x3;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x3;->c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

.method public final g()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x3;->f:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

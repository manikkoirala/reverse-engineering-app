.class final Lcom/pspdfkit/internal/t9;
.super Lcom/pspdfkit/internal/fs;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/w9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/w9;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-direct {p0}, Lcom/pspdfkit/internal/fs;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p2}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;

    move-result-object p2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p3, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p3}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgete(Lcom/pspdfkit/internal/w9;)I

    move-result p3

    invoke-static {p1, p3}, Lcom/pspdfkit/document/sharing/SharingOptions;->parsePageRange(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p1

    iput-object p1, p2, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p2

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;

    move-result-object p1

    .line 3
    iget-object p1, p1, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    const/4 p3, 0x1

    xor-int/2addr p1, p3

    if-eqz p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetp(Lcom/pspdfkit/internal/w9;)I

    move-result p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetq(Lcom/pspdfkit/internal/w9;)I

    move-result p1

    :goto_0
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/widget/EditText;I)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/t9;->b:Lcom/pspdfkit/internal/w9;

    .line 7
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeto(Lcom/pspdfkit/internal/w9;)Landroid/widget/TextView;

    move-result-object p2

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetj(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;

    move-result-object p4

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeti(Lcom/pspdfkit/internal/w9;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/pspdfkit/internal/w9$c;

    iget p4, p4, Lcom/pspdfkit/internal/w9$c;->a:I

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-ne p4, v0, :cond_1

    const/4 p4, 0x1

    goto :goto_1

    :cond_1
    const/4 p4, 0x0

    :goto_1
    if-eqz p4, :cond_2

    .line 9
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;

    move-result-object p4

    .line 10
    iget-object p4, p4, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 11
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result p4

    xor-int/2addr p4, p3

    if-eqz p4, :cond_5

    .line 12
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p4

    invoke-virtual {p4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p4

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_4

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 14
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_3

    const-string p4, "[:\\\\/*\"?|<>\']"

    const-string v0, ""

    .line 15
    invoke-virtual {p1, p4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 16
    invoke-virtual {p4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_4

    const/4 p1, 0x1

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 p3, 0x0

    .line 17
    :goto_4
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

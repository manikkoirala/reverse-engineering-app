.class final Lcom/pspdfkit/internal/im$f;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/im;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/im;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/im;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/im$f;->a:Lcom/pspdfkit/internal/im;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$f-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im$f;-><init>(Lcom/pspdfkit/internal/im;)V

    return-void
.end method


# virtual methods
.method public final f(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$f;->a:Lcom/pspdfkit/internal/im;

    invoke-static {v0}, Lcom/pspdfkit/internal/im;->-$$Nest$fgetl(Lcom/pspdfkit/internal/im;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 2
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/im;->-$$Nest$fputl(Lcom/pspdfkit/internal/im;Z)V

    return v2

    .line 7
    :cond_0
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    .line 11
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/im$f;->a:Lcom/pspdfkit/internal/im;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/im;->getPdfToPageViewTransformation()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/im$f;->a:Lcom/pspdfkit/internal/im;

    invoke-static {v1}, Lcom/pspdfkit/internal/im;->-$$Nest$fgete(Lcom/pspdfkit/internal/im;)Lcom/pspdfkit/internal/im$e;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/dm$d;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, v2}, Lcom/pspdfkit/internal/dm$d;->b(Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

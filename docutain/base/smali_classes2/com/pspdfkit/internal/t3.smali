.class public final Lcom/pspdfkit/internal/t3;
.super Lcom/pspdfkit/internal/p0;
.source "SourceFile"


# instance fields
.field public final c:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/p0;-><init>(II)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioData()[B

    move-result-object v3

    if-eqz v3, :cond_0

    .line 5
    new-instance v0, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object v4

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleRate()I

    move-result v5

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getSampleSize()I

    move-result v6

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getChannels()I

    move-result v7

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;-><init>([BLcom/pspdfkit/annotations/sound/AudioEncoding;IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/t3;->c:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/t3;->c:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/l6;
.super Lcom/pspdfkit/internal/e6;
.source "SourceFile"


# instance fields
.field private final c:Lcom/pspdfkit/internal/su;

.field private final d:Lcom/pspdfkit/internal/su;

.field private final e:Lcom/pspdfkit/internal/cb;


# direct methods
.method public constructor <init>(ILjava/util/UUID;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/su;Lcom/pspdfkit/internal/cb;)V
    .locals 1

    const-string v0, "textBlockId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "undoData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redoData"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalControlState"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/e6;-><init>(ILjava/util/UUID;)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/l6;->c:Lcom/pspdfkit/internal/su;

    .line 3
    iput-object p4, p0, Lcom/pspdfkit/internal/l6;->d:Lcom/pspdfkit/internal/su;

    .line 4
    iput-object p5, p0, Lcom/pspdfkit/internal/l6;->e:Lcom/pspdfkit/internal/cb;

    return-void
.end method


# virtual methods
.method public final a(Z)Ljava/lang/Integer;
    .locals 0

    if-eqz p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->c:Lcom/pspdfkit/internal/su;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->d:Lcom/pspdfkit/internal/su;

    .line 2
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/su;->b()Lcom/pspdfkit/internal/vq;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vq;->b()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public final b()Lcom/pspdfkit/internal/cb;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l6;->e:Lcom/pspdfkit/internal/cb;

    return-object v0
.end method

.method public final b(Z)Ljava/lang/Integer;
    .locals 1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->c:Lcom/pspdfkit/internal/su;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->d:Lcom/pspdfkit/internal/su;

    .line 3
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/su;->b()Lcom/pspdfkit/internal/vq;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vq;->a()I

    move-result p1

    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/su;->a()Lcom/pspdfkit/internal/l7;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/l7;->a()I

    move-result p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method public final c(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->c:Lcom/pspdfkit/internal/su;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/l6;->d:Lcom/pspdfkit/internal/su;

    .line 2
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/su;->c()I

    move-result p1

    return p1
.end method

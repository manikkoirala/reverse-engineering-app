.class public final Lcom/pspdfkit/internal/qa;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/qa$a;,
        Lcom/pspdfkit/internal/qa$b;,
        Lcom/pspdfkit/internal/qa$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/qa$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/pa;

.field private final b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

.field private c:Ljava/util/LinkedHashMap;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/pa;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/qa;->a:Lcom/pspdfkit/internal/pa;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    .line 13
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    return-void
.end method


# virtual methods
.method public final a()Landroid/util/SparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation

    .line 5
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 7
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/qa$b;

    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/qa$b;->a(Landroid/util/SparseArray;)V

    .line 8
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/qa$b;

    .line 9
    iget-object v3, v3, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    const-string v3, "layout"

    .line 10
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 11
    :goto_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/qa$b;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/qa$b;->a()Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 12
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 13
    iget-object v4, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v4}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 14
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/qa$b;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/qa$b;->a()Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final a(I)Lcom/pspdfkit/internal/ui/dialog/signatures/f;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "signatureOptions.signatureCreationModes[viewType]"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 3
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/qa$b;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p1, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    if-eqz p1, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    const-string p1, "layout"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object v0
.end method

.method public final a(Landroid/util/SparseArray;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;>;)V"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 16
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 17
    iget-object v4, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v4}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 18
    invoke-virtual {p1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    goto :goto_1

    :cond_0
    move-object v3, v2

    .line 19
    :goto_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/qa$b;

    invoke-virtual {v4, v3}, Lcom/pspdfkit/internal/qa$b;->a(Landroid/util/SparseArray;)V

    .line 20
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qa$b;

    .line 21
    iget-object v1, v1, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    if-eqz v1, :cond_1

    move-object v2, v1

    goto :goto_2

    :cond_1
    const-string v1, "layout"

    .line 22
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 23
    :goto_2
    invoke-virtual {v2, v3}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final b(I)I
    .locals 2

    if-ltz p1, :cond_5

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_5

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    const/4 v0, -0x1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    .line 57
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/qa$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v1, p1

    :goto_0
    if-eq p1, v0, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 60
    sget p1, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_type_tab:I

    goto :goto_1

    .line 63
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 64
    :cond_2
    sget p1, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_image_tab:I

    goto :goto_1

    .line 65
    :cond_3
    sget p1, Lcom/pspdfkit/R$string;->pspdf__electronic_signature_draw_tab:I

    :goto_1
    return p1

    .line 70
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "SignatureCreationModes should never be null in getTabPositionTitleRes."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 71
    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Tab position outside range of available signature creation modes."

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    .line 47
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 48
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qa$b;

    .line 49
    iget-object v1, v1, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const-string v1, "layout"

    .line 50
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 51
    :goto_1
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->f()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 0

    return p1
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/qa$a;

    const-string p2, "holder"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 3

    const-string v0, "parent"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_6

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    const-string v0, "signatureOptions.signatureCreationModes[viewType]"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/configuration/signatures/SignatureCreationMode;

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->c:Ljava/util/LinkedHashMap;

    .line 155
    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 156
    new-instance v1, Lcom/pspdfkit/internal/qa$b;

    invoke-direct {v1}, Lcom/pspdfkit/internal/qa$b;-><init>()V

    .line 262
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    :cond_0
    check-cast v1, Lcom/pspdfkit/internal/qa$b;

    .line 265
    sget-object v0, Lcom/pspdfkit/internal/qa$c;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    const-string v2, "parent.context"

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 272
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/q;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/q;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 273
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qa$b;->a()Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p2, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 274
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 275
    :cond_2
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/g;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/g;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 276
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qa$b;->a()Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p2, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 277
    :cond_3
    new-instance p2, Lcom/pspdfkit/internal/ui/dialog/signatures/c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->b:Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/ui/dialog/signatures/c;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;)V

    .line 278
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qa$b;->a()Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p2, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_4
    :goto_0
    const-string p1, "<set-?>"

    .line 279
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    iput-object p2, v1, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    .line 316
    new-instance p1, Lcom/pspdfkit/internal/qa$a;

    .line 317
    iget-object p2, v1, Lcom/pspdfkit/internal/qa$b;->b:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    if-eqz p2, :cond_5

    goto :goto_1

    :cond_5
    const-string p2, "layout"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 318
    :goto_1
    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/qa$a;-><init>(Lcom/pspdfkit/internal/ui/dialog/signatures/f;)V

    return-object p1

    .line 319
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Tab outside of range for the available signature creation modes."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final onViewAttachedToWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/qa$a;

    const-string v0, "holder"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/pspdfkit/internal/qa;->a:Lcom/pspdfkit/internal/pa;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/qa$a;->a(Lcom/pspdfkit/internal/pa;)V

    return-void
.end method

.method public final onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/qa$a;

    const-string v0, "holder"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 90
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/qa$a;->a(Lcom/pspdfkit/internal/pa;)V

    return-void
.end method

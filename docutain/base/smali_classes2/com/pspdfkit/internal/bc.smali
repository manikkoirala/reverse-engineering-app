.class public final Lcom/pspdfkit/internal/bc;
.super Lcom/pspdfkit/internal/jni/NativeFormObserver;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/uf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/zf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$L47_bcS0TALdN9tabAHTKrNIiBk(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/bc;->b(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$Lajdy41foEXlVECahs3W2_4-7C8(Lcom/pspdfkit/internal/bc;Lcom/pspdfkit/forms/FormField;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/internal/bc;Lcom/pspdfkit/forms/FormField;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SMBSaDDTTVpn-1yVNH5LjQ6UWGU(Lcom/pspdfkit/internal/bc;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/bc;->e(Lcom/pspdfkit/internal/bc;)V

    return-void
.end method

.method public static synthetic $r8$lambda$XNPznJW3BpLLhHQGeSgE0BfcNcQ(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/bc;->d(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/uf;Lcom/pspdfkit/internal/zf;)V
    .locals 1

    const-string v0, "provider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeFormObserver;-><init>()V

    .line 3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    .line 4
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->b:Ljava/lang/ref/WeakReference;

    .line 7
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->c:Lcom/pspdfkit/internal/nh;

    .line 8
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    .line 9
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    .line 10
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    .line 11
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bc;->g:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->c:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method private final a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/forms/FormField;",
            ">;"
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/uf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string p2, "empty()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 246
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/bc$l;

    invoke-direct {v1, v0, p1, p2}, Lcom/pspdfkit/internal/bc$l;-><init>(Lcom/pspdfkit/internal/uf;ILjava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/bc;->a(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 249
    new-instance v1, Lcom/pspdfkit/internal/bc$m;

    invoke-direct {v1, p2, p1}, Lcom/pspdfkit/internal/bc$m;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->onErrorResumeNext(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 253
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string p2, "providerIndex: Int, form\u2026dSchedulers.mainThread())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final a(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "TT;>;"
        }
    .end annotation

    .line 254
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string v0, "empty()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 255
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const/16 v1, 0xf

    .line 258
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string v0, "defer {\n            val \u2026heduler.PRIORITY_HIGHER))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private static final a(Lcom/pspdfkit/internal/bc;Lcom/pspdfkit/forms/FormField;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$formField"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    .line 243
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    .line 244
    invoke-interface {v0, p1}, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;->onFormFieldUpdated(Lcom/pspdfkit/forms/FormField;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method private static final b(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 1

    const-string v0, "$action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0

    .line 8
    :cond_0
    invoke-static {p0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method private final c(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v0, "complete()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 3
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda0;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const/4 v1, 0x5

    .line 4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const-string v0, "fromAction(action)\n     \u2026aScheduler(taskPriority))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method private static final d(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    const-string v0, "$tmp0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private static final e(Lcom/pspdfkit/internal/bc;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bc;->g:Lcom/pspdfkit/internal/nh;

    .line 128
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;

    .line 129
    invoke-interface {v0}, Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;->onFormTabOrderUpdated()V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/forms/FormField;)V
    .locals 2

    const-string v0, "formField"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/bc;Lcom/pspdfkit/forms/FormField;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormListeners$OnChoiceFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormListeners$OnFormTabOrderUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormListeners$OnTextFormFieldUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final formDidAddFormField(Lcom/pspdfkit/internal/jni/NativeDocument;ILcom/pspdfkit/internal/jni/NativeFormField;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "nativeFormField"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/bc$a;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/bc$a;-><init>(Lcom/pspdfkit/internal/uf;ILcom/pspdfkit/internal/jni/NativeFormField;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/bc;->a(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 5
    new-instance p2, Lcom/pspdfkit/internal/bc$b;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/bc$b;-><init>(Lcom/pspdfkit/internal/bc;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidChange(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 4
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/uf;->setDirty(Z)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 8
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/cc;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/cc;-><init>(Lcom/pspdfkit/internal/bc;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method public final formDidChangeAction(Lcom/pspdfkit/internal/jni/NativeDocument;II)V
    .locals 2

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/dc;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/dc;-><init>(Lcom/pspdfkit/internal/uf;II)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/bc;->a(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 5
    sget-object v0, Lcom/pspdfkit/internal/ec;->a:Lcom/pspdfkit/internal/ec;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/fc;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/fc;-><init>(Lcom/pspdfkit/internal/bc;)V

    new-instance v1, Lcom/pspdfkit/internal/gc;

    invoke-direct {v1, p3, p2}, Lcom/pspdfkit/internal/gc;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method public final formDidChangeButtonSelection(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;IZ)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$c;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$c;-><init>(ILcom/pspdfkit/internal/bc;Z)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidChangeFlags(Lcom/pspdfkit/internal/jni/NativeDocument;II)V
    .locals 2

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/dc;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/dc;-><init>(Lcom/pspdfkit/internal/uf;II)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/bc;->a(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 5
    sget-object v0, Lcom/pspdfkit/internal/ec;->a:Lcom/pspdfkit/internal/ec;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/fc;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/fc;-><init>(Lcom/pspdfkit/internal/bc;)V

    new-instance v1, Lcom/pspdfkit/internal/gc;

    invoke-direct {v1, p3, p2}, Lcom/pspdfkit/internal/gc;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method public final formDidReset(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;I)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$d;

    invoke-direct {p2, p4, p0}, Lcom/pspdfkit/internal/bc$d;-><init>(ILcom/pspdfkit/internal/bc;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSelectOption(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;ILjava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeDocument;",
            "I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "selectedOption"

    invoke-static {p5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$e;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$e;-><init>(ILcom/pspdfkit/internal/bc;Ljava/util/ArrayList;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSetCustomOption(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;ILjava/lang/String;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$f;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$f;-><init>(ILcom/pspdfkit/internal/bc;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSetMaxLength(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;II)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$g;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$g;-><init>(ILcom/pspdfkit/internal/bc;I)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSetRichText(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;ILjava/lang/String;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$h;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$h;-><init>(ILcom/pspdfkit/internal/bc;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSetText(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;ILjava/lang/String;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/bc$i;

    invoke-direct {p2, p4, p0, p5}, Lcom/pspdfkit/internal/bc$i;-><init>(ILcom/pspdfkit/internal/bc;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final formDidSetValue(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "formFieldFQN"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 4
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/uf;->setDirty(Z)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 8
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/bc;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/cc;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/cc;-><init>(Lcom/pspdfkit/internal/bc;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

.method public final formTabOrderDidRecalculate(Lcom/pspdfkit/internal/jni/NativeDocument;I)V
    .locals 1

    const-string v0, "nativeDocument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/uf;

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/bc$j;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/bc$j;-><init>(Lcom/pspdfkit/internal/uf;I)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/bc;->c(Lkotlin/jvm/functions/Function0;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 5
    new-instance p2, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/bc$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/bc;)V

    sget-object v0, Lcom/pspdfkit/internal/bc$k;->a:Lcom/pspdfkit/internal/bc$k;

    invoke-virtual {p1, p2, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

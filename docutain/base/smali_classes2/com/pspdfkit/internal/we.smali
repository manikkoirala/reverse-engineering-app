.class public final Lcom/pspdfkit/internal/we;
.super Lcom/pspdfkit/internal/views/annotations/j;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ve$a;


# instance fields
.field private final v:I

.field private w:Lcom/pspdfkit/internal/ve;

.field private x:Landroid/widget/ProgressBar;

.field private y:Landroid/widget/TextView;

.field private z:Ljava/lang/Runnable;


# direct methods
.method public static synthetic $r8$lambda$2pfXE-1wFZarUQWJqQ2yH83krOE(Lcom/pspdfkit/internal/we;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->t()V

    return-void
.end method

.method public static synthetic $r8$lambda$rzNYcJ6lcnR8JlxNKa83hd8uesw(Lcom/pspdfkit/internal/we;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->x()V

    return-void
.end method

.method public static synthetic $r8$lambda$we8-itNAnX0UJJ6vVvUQwxQqHgA(Lcom/pspdfkit/internal/we;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->w()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/views/annotations/j;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/document/PdfDocument;)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$attr;->pspdf__backgroundColor:I

    sget p3, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/we;->v:I

    return-void
.end method

.method private t()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ve;->b(Lcom/pspdfkit/internal/ve$a;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->u()V

    .line 4
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/j;->b()V

    :cond_0
    return-void
.end method

.method private u()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getAnnotationResource()Lcom/pspdfkit/internal/x1;

    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/pspdfkit/internal/ve;

    if-eqz v1, :cond_5

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/ve;

    iput-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/ve;->a()I

    move-result v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x12c

    if-eqz v0, :cond_3

    const/4 v4, 0x1

    if-eq v0, v4, :cond_1

    const/4 v4, 0x2

    if-eq v0, v4, :cond_3

    goto :goto_0

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ve;->a(Lcom/pspdfkit/internal/ve$a;)V

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/we;)V

    .line 11
    iget-object v4, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz v4, :cond_2

    .line 12
    invoke-virtual {p0, v4}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 13
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 14
    :cond_2
    iput-object v0, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 15
    invoke-virtual {p0, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 16
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ve;->a(Lcom/pspdfkit/internal/ve$a;)V

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/we;)V

    .line 18
    iget-object v4, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz v4, :cond_4

    .line 19
    invoke-virtual {p0, v4}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 20
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 21
    :cond_4
    iput-object v0, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 22
    invoke-virtual {p0, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    :goto_0
    return-void
.end method

.method private v()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    .line 2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 3
    :cond_1
    iget v0, p0, Lcom/pspdfkit/internal/we;->v:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method private w()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 6
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    const-string v1, "\u2715"

    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1060000

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroidx/core/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 18
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    return-void
.end method

.method private x()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 6
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    const/4 v3, -0x2

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/internal/ve;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->a(Landroid/graphics/Bitmap;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    if-eqz p1, :cond_1

    .line 5
    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/ve;->b(Lcom/pspdfkit/internal/ve$a;)V

    .line 6
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz p1, :cond_2

    .line 7
    invoke-virtual {p0, p1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 9
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    if-eqz p1, :cond_3

    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 12
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    if-eqz p1, :cond_4

    .line 14
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 16
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    :cond_5
    return-void
.end method

.method public final b()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->u()V

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/j;->b()V

    return-void
.end method

.method public final c()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/we;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {p0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v1, 0x0

    .line 4
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 5
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    const-wide/16 v1, 0x12c

    .line 6
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final l()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/we;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {p0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v1, 0x0

    .line 4
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 5
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    const-wide/16 v1, 0x12c

    .line 6
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final m()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/we$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/we;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final recycle()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/views/annotations/j;->recycle()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/ve;->b(Lcom/pspdfkit/internal/ve$a;)V

    .line 5
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->w:Lcom/pspdfkit/internal/ve;

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 8
    iput-object v1, p0, Lcom/pspdfkit/internal/we;->z:Ljava/lang/Runnable;

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->x:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    if-eqz v0, :cond_2

    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/we;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->v()V

    return-void
.end method

.method public setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/annotations/j;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/views/annotations/j;->setAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/we;->u()V

    return-void
.end method

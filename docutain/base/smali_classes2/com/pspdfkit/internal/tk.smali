.class public final Lcom/pspdfkit/internal/tk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mk;


# instance fields
.field private final b:Lcom/pspdfkit/annotations/Annotation;

.field private final c:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

.field private final e:Lcom/pspdfkit/internal/qf;

.field private final f:Lcom/pspdfkit/internal/fl;

.field private final g:Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

.field private final h:Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

.field private final i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/util/ArrayList;

.field private final l:I

.field private final m:Ljava/util/ArrayList;

.field private n:Lcom/pspdfkit/internal/hk;

.field private o:Lcom/pspdfkit/annotations/Annotation;

.field private p:Lcom/pspdfkit/internal/o1;


# direct methods
.method public static synthetic $r8$lambda$7cu0hAvcaPmIWl64bXBUk5G4BGU(Lcom/pspdfkit/internal/tk;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tk;->b(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$E96qwnsc0Jys6aCDQyFLxr1nVjc(Lcom/pspdfkit/internal/tk;Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tk;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$sD9HdooIwIQfDaVAF-69eC7CMh0(Lcom/pspdfkit/internal/tk;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tk;->b(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/internal/qf;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editedAnnotation"

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfConfiguration"

    .line 4
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationPreferences"

    .line 5
    invoke-static {p5, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationProvider"

    .line 6
    invoke-static {p6, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationConfiguration"

    .line 7
    invoke-static {p8, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 10
    iput-object p4, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 11
    iput-object p5, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 12
    iput-object p6, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    .line 13
    iput-object p7, p0, Lcom/pspdfkit/internal/tk;->f:Lcom/pspdfkit/internal/fl;

    .line 15
    iput-object p3, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 16
    sget p2, Lcom/pspdfkit/R$string;->pspdf__annotation_type_note:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/tk;->j:Ljava/lang/String;

    .line 18
    sget-object p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 19
    const-class p4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-interface {p8, p2, p3, p4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p4

    check-cast p4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    iput-object p4, p0, Lcom/pspdfkit/internal/tk;->g:Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    .line 21
    const-class p5, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    invoke-interface {p8, p2, p3, p5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p5

    check-cast p5, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    iput-object p5, p0, Lcom/pspdfkit/internal/tk;->h:Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    .line 23
    new-instance p6, Ljava/util/ArrayList;

    invoke-direct {p6}, Ljava/util/ArrayList;-><init>()V

    iput-object p6, p0, Lcom/pspdfkit/internal/tk;->k:Ljava/util/ArrayList;

    if-eqz p4, :cond_0

    .line 25
    invoke-interface {p4}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getAvailableColors()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p6, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 26
    invoke-interface {p4}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getDefaultColor()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/tk;->l:I

    goto :goto_0

    .line 29
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/tk;->l:I

    .line 32
    :goto_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/tk;->m:Ljava/util/ArrayList;

    if-eqz p5, :cond_1

    .line 34
    invoke-interface {p5}, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;->getAvailableIconNames()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/hk;
    .locals 6

    .line 111
    new-instance v0, Lcom/pspdfkit/internal/hk;

    .line 113
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v2

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, p1, v2}, Lcom/pspdfkit/internal/r1;->getReviewSummary(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 115
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v2, :cond_1

    .line 116
    invoke-static {v2}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 117
    sget-object v5, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v2, v5}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    .line 118
    :cond_1
    sget v2, Lcom/pspdfkit/internal/ao;->a:I

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    .line 119
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v5, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v2, v5, :cond_4

    .line 120
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    :goto_3
    invoke-direct {v0, p1, v1, v3}, Lcom/pspdfkit/internal/hk;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;Z)V

    return-object v0
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    check-cast v0, Lcom/pspdfkit/internal/r1;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private synthetic b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 14
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    invoke-interface {v1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic c(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 14
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/tk;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/hk;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->o:Lcom/pspdfkit/annotations/Annotation;

    if-ne v0, p1, :cond_0

    return-void

    .line 19
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->b()V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    .line 26
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/tk;->o:Lcom/pspdfkit/annotations/Annotation;

    if-eqz p1, :cond_2

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->f:Lcom/pspdfkit/internal/fl;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o1;->a()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/hk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hk;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, v2, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;I)V
    .locals 1

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/hk;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 30
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 31
    invoke-virtual {v0, p2}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 32
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/hk;->a(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
    .locals 2

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/hk;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 12
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0, p2}, Lcom/pspdfkit/internal/r1;->appendAnnotationState(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    .line 14
    iget-object p2, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 15
    invoke-interface {v1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p2, v0, v1}, Lcom/pspdfkit/internal/r1;->getReviewSummary(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object p2

    .line 16
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/hk;->a(Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;Ljava/lang/String;)V
    .locals 2

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/internal/hk;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 23
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 24
    instance-of v1, v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    if-eqz v1, :cond_0

    .line 25
    check-cast v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    .line 26
    invoke-virtual {v0, p2}, Lcom/pspdfkit/annotations/NoteAnnotation;->setIconName(Ljava/lang/String;)V

    .line 28
    :cond_0
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/hk;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V
    .locals 1

    .line 17
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 19
    :cond_0
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 20
    invoke-virtual {v0, p2}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 21
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/ik;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/uk;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, v2, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 35
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ik;

    const-string v2, "contentCard"

    const-string v3, "argumentName"

    .line 37
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 89
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 91
    :cond_2
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 92
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 93
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 94
    :cond_3
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    .line 96
    :cond_4
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v2, v3}, Lcom/pspdfkit/internal/pf;->setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 97
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v2

    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->getColor()I

    move-result v3

    if-eq v2, v3, :cond_5

    .line 98
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 100
    :cond_5
    instance-of v2, v1, Lcom/pspdfkit/annotations/NoteAnnotation;

    if-eqz v2, :cond_1

    .line 101
    check-cast v1, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/annotations/NoteAnnotation;->setIconName(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    if-eqz p1, :cond_7

    .line 106
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o1;->b()V

    .line 107
    iput-object v1, p0, Lcom/pspdfkit/internal/tk;->p:Lcom/pspdfkit/internal/o1;

    .line 110
    :cond_7
    iget-object p1, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    invoke-interface {p1}, Lcom/pspdfkit/internal/qf;->a()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;)Z
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->ENABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result p1

    if-nez p1, :cond_3

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->l()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v3, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1, v0, v3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz p1, :cond_0

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    .line 7
    :cond_0
    sget p1, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final b()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3
    sget-object v3, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 4
    :cond_0
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v0, v3, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final b(Lcom/pspdfkit/internal/ik;)Z
    .locals 1

    .line 8
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    invoke-interface {v0, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->f:Lcom/pspdfkit/internal/fl;

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final c(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 30
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 32
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/tk;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/tk;)V

    .line 33
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final c()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->ENABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public final d(Lcom/pspdfkit/internal/ik;)V
    .locals 0

    .line 1
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/tk;->a(Ljava/util/List;)V

    return-void
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/ik;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->n:Lcom/pspdfkit/internal/hk;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/hk;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/tk;->n:Lcom/pspdfkit/internal/hk;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->n:Lcom/pspdfkit/internal/hk;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getSubject()Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->j:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public final h()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->g:Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getSupportedProperties()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public final j()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3
    sget-object v3, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 4
    :cond_0
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_3

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :cond_4
    :goto_2
    return v1
.end method

.method public final l()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    monitor-enter v0

    :try_start_0
    const-string v2, "configuration"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_REPLIES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->DISABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final n()Lcom/pspdfkit/internal/ik;
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/annotations/NoteAnnotation;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setInReplyTo(Lcom/pspdfkit/annotations/Annotation;)V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setCreator(Ljava/lang/String;)V

    .line 5
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setCreatedDate(Ljava/util/Date;)V

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getFlags()Ljava/util/EnumSet;

    move-result-object v1

    .line 9
    sget-object v2, Lcom/pspdfkit/annotations/AnnotationFlags;->HIDDEN:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 10
    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setFlags(Ljava/util/EnumSet;)V

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/tk;->i:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/pf;->setVariant(Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 15
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->f:Lcom/pspdfkit/internal/fl;

    invoke-static {v0}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 17
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/tk;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/hk;

    move-result-object v0

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->h:Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getSupportedProperties()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->NOTE_ICON:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final p()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getAnnotationReplyFeatures()Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;->ENABLED:Lcom/pspdfkit/configuration/annotations/AnnotationReplyFeatures;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6
    sget-object v1, Lcom/pspdfkit/annotations/AnnotationFlags;->READONLY:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public final q()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    sget v1, Lcom/pspdfkit/internal/ao;->a:I

    const-string v1, "annotation"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 697
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    .line 698
    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v0}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v0

    goto :goto_0

    .line 700
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    .line 701
    iget v0, p0, Lcom/pspdfkit/internal/tk;->l:I

    :cond_1
    return v0
.end method

.method public final r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final s()V
    .locals 0

    return-void
.end method

.method public final t()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->NOTE_EDITOR_CONTENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final u()Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;>;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/tk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Observable;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/tk;->e:Lcom/pspdfkit/internal/qf;

    iget-object v1, p0, Lcom/pspdfkit/internal/tk;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->getFlattenedAnnotationRepliesAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/tk$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/tk;)V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->toObservable()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

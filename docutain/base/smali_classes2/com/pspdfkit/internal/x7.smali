.class public final Lcom/pspdfkit/internal/x7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/editor/FilePicker;


# instance fields
.field private final a:Landroidx/appcompat/app/AppCompatActivity;

.field private final b:Lcom/pspdfkit/internal/t;

.field private final c:[Ljava/lang/String;

.field private d:Lio/reactivex/rxjava3/subjects/MaybeSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/MaybeSubject<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/internal/t;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalStorageAccessPermissionHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/x7;->a:Landroidx/appcompat/app/AppCompatActivity;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/x7;->b:Lcom/pspdfkit/internal/t;

    const-string p1, "application/pdf"

    .line 6
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/x7;->c:[Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/x7;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/x7;->a:Landroidx/appcompat/app/AppCompatActivity;

    return-object p0
.end method

.method private static a(Landroid/net/Uri;)Z
    .locals 2

    .line 3
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.externalstorage.documents"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.providers.downloads.documents"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object p0

    const-string v0, "com.android.providers.media.documents"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/x7;Landroid/net/Uri;)Z
    .locals 0

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/pspdfkit/internal/x7;->a(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/x7;)Lcom/pspdfkit/internal/t;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/x7;->b:Lcom/pspdfkit/internal/t;

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/x7;)Lio/reactivex/rxjava3/subjects/MaybeSubject;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/x7;->d:Lio/reactivex/rxjava3/subjects/MaybeSubject;

    return-object p0
.end method


# virtual methods
.method public synthetic getDestinationUri(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/document/editor/FilePicker$-CC;->$default$getDestinationUri(Lcom/pspdfkit/document/editor/FilePicker;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final getDestinationUri(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "android.intent.action.OPEN_DOCUMENT"

    .line 1
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.action.CREATE_DOCUMENT"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_7

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/x7;->a:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v1}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 10
    invoke-static {}, Lio/reactivex/rxjava3/subjects/MaybeSubject;->create()Lio/reactivex/rxjava3/subjects/MaybeSubject;

    move-result-object v2

    const-string v3, "create()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/x7;->d:Lio/reactivex/rxjava3/subjects/MaybeSubject;

    .line 12
    sget v2, Lcom/pspdfkit/internal/y7;->g:I

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/internal/x7;->c:[Ljava/lang/String;

    new-instance v3, Lcom/pspdfkit/internal/x7$a;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/x7$a;-><init>(Lcom/pspdfkit/internal/x7;)V

    const-string v4, "fragmentManager"

    .line 14
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "supportedDocumentTypes"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "com.pspdfkit.internal.document.editor.DefaultFilePickerHandler.FRAGMENT_TAG"

    .line 122
    invoke-virtual {v1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_2

    .line 123
    new-instance v4, Lcom/pspdfkit/internal/y7;

    invoke-direct {v4}, Lcom/pspdfkit/internal/y7;-><init>()V

    .line 124
    :cond_2
    check-cast v4, Lcom/pspdfkit/internal/y7;

    .line 129
    invoke-static {v4, p2}, Lcom/pspdfkit/internal/y7;->b(Lcom/pspdfkit/internal/y7;Ljava/lang/String;)V

    .line 130
    invoke-static {v4, v3}, Lcom/pspdfkit/internal/y7;->a(Lcom/pspdfkit/internal/y7;Lkotlin/jvm/functions/Function1;)V

    .line 131
    invoke-static {v4, p1}, Lcom/pspdfkit/internal/y7;->a(Lcom/pspdfkit/internal/y7;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v4}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_3

    .line 135
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 136
    invoke-virtual {p1, v4, v0}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitNow()V

    .line 140
    :cond_3
    invoke-static {v4}, Lcom/pspdfkit/internal/y7;->c(Lcom/pspdfkit/internal/y7;)Landroidx/activity/result/ActivityResultLauncher;

    move-result-object p1

    const/4 p2, 0x0

    if-nez p1, :cond_4

    const-string p1, "filePickerLauncher"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, p2

    :cond_4
    invoke-virtual {p1, v2}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 141
    iget-object p1, p0, Lcom/pspdfkit/internal/x7;->d:Lio/reactivex/rxjava3/subjects/MaybeSubject;

    if-nez p1, :cond_5

    const-string p1, "maybeSubject"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object p2, p1

    :goto_2
    return-object p2

    .line 142
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "PSPDFKit.FilePicker: Failed to get the FragmentManager."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 143
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "PSPDFKit.FilePicker: Unsupported intent, action may be equal to Intent.ACTION_OPEN_DOCUMENT or Intent.ACTION_CREATE_DOCUMENT."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

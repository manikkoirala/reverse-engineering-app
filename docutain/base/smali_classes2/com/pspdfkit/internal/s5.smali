.class public final Lcom/pspdfkit/internal/s5;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 1

    const/16 v0, 0x64

    .line 16
    invoke-static {p0, v0}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result p0

    const/4 v0, -0x1

    invoke-static {p0, v0}, Landroidx/core/graphics/ColorUtils;->compositeColors(II)I

    move-result p0

    return p0
.end method

.method public static a(IF)I
    .locals 5

    .line 17
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 18
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    add-float/2addr v1, p1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    mul-float v1, v1, v2

    float-to-int v1, v1

    .line 19
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v2

    add-float/2addr v4, p1

    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float v4, v4, v2

    float-to-int v4, v4

    .line 20
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    int-to-float p0, p0

    div-float/2addr p0, v2

    add-float/2addr p0, p1

    invoke-static {p0, v3}, Ljava/lang/Math;->min(FF)F

    move-result p0

    mul-float p0, p0, v2

    float-to-int p0, p0

    .line 21
    invoke-static {v0, v1, v4, p0}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    return p0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 2
    invoke-virtual {p0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p0

    const/4 v0, 0x0

    const/high16 v1, -0x1000000

    .line 4
    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 5
    invoke-virtual {p0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 6
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    .line 10
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result p0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    const/16 v3, 0xff

    invoke-static {v3, p0, v2, p1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    .line 11
    invoke-static {v0, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v2

    invoke-static {v1, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v4

    cmpl-double p1, v2, v4

    if-lez p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const/high16 p1, 0x40e00000    # 7.0f

    .line 12
    invoke-static {v0, p0, p1}, Landroidx/core/graphics/ColorUtils;->calculateMinimumAlpha(IIF)I

    move-result p0

    if-ltz p0, :cond_1

    .line 15
    invoke-static {v0, p0}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v0

    :cond_1
    return v0

    nop

    :array_0
    .array-data 4
        0x1010030
        0x1010206
    .end array-data
.end method

.method public static a(Landroid/view/Window;I)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/high16 v0, -0x80000000

    .line 22
    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    const/high16 v0, 0x4000000

    .line 23
    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 24
    invoke-virtual {p0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    return-void
.end method

.method public static b(I)I
    .locals 11

    if-nez p0, :cond_0

    const/4 p0, -0x1

    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [D

    .line 2
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    invoke-static {v1, v2, p0, v0}, Landroidx/core/graphics/ColorUtils;->RGBToLAB(III[D)V

    const/4 p0, 0x0

    aget-wide v1, v0, p0

    const v3, 0x3f666666    # 0.9f

    float-to-double v3, v3

    mul-double v5, v1, v3

    aput-wide v5, v0, p0

    const/4 p0, 0x1

    aget-wide v7, v0, p0

    const/4 p0, 0x2

    aget-wide v9, v0, p0

    .line 4
    invoke-static/range {v5 .. v10}, Landroidx/core/graphics/ColorUtils;->LABToColor(DDD)I

    move-result p0

    return p0
.end method

.method public static c(I)I
    .locals 6

    .line 1
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    const/16 v2, 0xff

    invoke-static {v2, v0, v1, p0}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    const/high16 v0, -0x1000000

    .line 2
    invoke-static {v0, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v1

    const/4 v3, -0x1

    invoke-static {v3, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v4

    cmpl-double p0, v1, v4

    if-lez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public static d(I)I
    .locals 7

    .line 1
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    const/16 v2, 0xff

    invoke-static {v2, v0, v1, p0}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    const/high16 v0, -0x1000000

    .line 2
    invoke-static {v0, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v1

    const/4 v3, -0x1

    invoke-static {v3, p0}, Landroidx/core/graphics/ColorUtils;->calculateContrast(II)D

    move-result-wide v4

    cmpl-double v6, v1, v4

    if-lez v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    const/high16 v1, 0x40e00000    # 7.0f

    .line 3
    invoke-static {v0, p0, v1}, Landroidx/core/graphics/ColorUtils;->calculateMinimumAlpha(IIF)I

    move-result p0

    if-ltz p0, :cond_1

    .line 6
    invoke-static {v0, p0}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v0

    :cond_1
    return v0
.end method

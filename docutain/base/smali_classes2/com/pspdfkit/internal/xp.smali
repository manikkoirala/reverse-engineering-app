.class public final Lcom/pspdfkit/internal/xp;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/xp$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/fragment/app/FragmentManager;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    const-string v2, "fragmentManager"

    .line 8
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentTag"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    const-string v3, "removeFragmentAllowingStateLoss() may only be called from the main thread."

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    .line 122
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;Z)V

    :goto_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xp$a;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/xp$a;->-$$Nest$fputb(Lcom/pspdfkit/internal/xp$a;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/xp;->c()Lcom/pspdfkit/internal/xp$a;

    move-result-object v0

    .line 6
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/xp$a;->-$$Nest$fputb(Lcom/pspdfkit/internal/xp$a;Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final b()Ljava/lang/Object;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xp$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/xp$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/xp$a;)Ljava/lang/Object;

    move-result-object v2

    .line 4
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/xp$a;->-$$Nest$fputb(Lcom/pspdfkit/internal/xp$a;Ljava/lang/Object;)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/xp;->a()V

    return-object v2

    :cond_0
    return-object v1
.end method

.method public final c()Lcom/pspdfkit/internal/xp$a;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xp$a;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/xp$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/xp$a;-><init>()V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/xp;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v2, p0, Lcom/pspdfkit/internal/xp;->b:Ljava/lang/String;

    const-string v3, "fragmentManager"

    .line 5
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragment"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragmentTag"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    const-string v4, "addFragment() may only be called from the main thread."

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "fragmentManager.beginTra\u2026dd(fragment, fragmentTag)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_0
    return-object v0
.end method

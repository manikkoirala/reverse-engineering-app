.class public abstract Lcom/pspdfkit/internal/g4;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/g4$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/pspdfkit/internal/zf;

.field public final b:I

.field public final c:Landroid/graphics/Bitmap;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/Integer;

.field public final i:Ljava/lang/Integer;

.field public final j:Ljava/lang/Integer;

.field public final k:Ljava/lang/Integer;

.field public final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z

.field public final s:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zf;IILandroid/graphics/Bitmap;IIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZZ)V
    .locals 2

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 2
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    move v1, p2

    .line 3
    iput v1, v0, Lcom/pspdfkit/internal/g4;->d:I

    move v1, p3

    .line 4
    iput v1, v0, Lcom/pspdfkit/internal/g4;->b:I

    move-object v1, p4

    .line 5
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    move v1, p5

    .line 6
    iput v1, v0, Lcom/pspdfkit/internal/g4;->e:I

    move v1, p6

    .line 7
    iput v1, v0, Lcom/pspdfkit/internal/g4;->f:I

    move v1, p7

    .line 8
    iput v1, v0, Lcom/pspdfkit/internal/g4;->g:I

    move-object v1, p8

    .line 9
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->h:Ljava/lang/Integer;

    move-object v1, p9

    .line 10
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->i:Ljava/lang/Integer;

    move-object v1, p10

    .line 11
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->j:Ljava/lang/Integer;

    move-object v1, p11

    .line 12
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->k:Ljava/lang/Integer;

    move v1, p12

    .line 13
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4;->o:Z

    move v1, p13

    .line 14
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4;->p:Z

    move-object/from16 v1, p14

    .line 15
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->l:Ljava/util/ArrayList;

    move-object/from16 v1, p15

    .line 16
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->m:Ljava/util/ArrayList;

    move-object/from16 v1, p16

    .line 17
    iput-object v1, v0, Lcom/pspdfkit/internal/g4;->n:Ljava/util/List;

    move/from16 v1, p17

    .line 18
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4;->q:Z

    move/from16 v1, p18

    .line 19
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4;->r:Z

    move/from16 v1, p19

    .line 20
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4;->s:Z

    return-void
.end method

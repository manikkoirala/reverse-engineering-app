.class public final Lcom/pspdfkit/internal/a3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/audio/AudioModeListeners;
.implements Lcom/pspdfkit/ui/audio/AudioModeManager;


# instance fields
.field private final a:Lcom/pspdfkit/ui/PdfFragment;

.field private final synthetic b:Lcom/pspdfkit/internal/z2;

.field private final c:Lcom/pspdfkit/internal/b3;

.field private final d:Lcom/pspdfkit/internal/m3;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V
    .locals 1

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditRecordedListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/z2;

    invoke-direct {p1}, Lcom/pspdfkit/internal/z2;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    .line 5
    new-instance p1, Lcom/pspdfkit/internal/b3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/b3;-><init>(Lcom/pspdfkit/internal/a3;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    .line 6
    new-instance p1, Lcom/pspdfkit/internal/m3;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/m3;-><init>(Lcom/pspdfkit/internal/a3;Lcom/pspdfkit/internal/vu;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    const-string v1, "fragment.annotationConfiguration"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/v3;)V
    .locals 4

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    instance-of v1, v0, Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return-void

    .line 123
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v3;->b()Z

    move-result v1

    const-string v2, "fragment.requireContext()"

    if-eqz v1, :cond_2

    .line 124
    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    iget-object v3, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v3}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v0, p1}, Lcom/pspdfkit/internal/m3;->a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V

    goto :goto_1

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    iget-object v3, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v3}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v0, p1}, Lcom/pspdfkit/internal/b3;->a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V

    :goto_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/pspdfkit/internal/t2;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/t2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 52
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/pspdfkit/internal/w2;

    check-cast p1, Lcom/pspdfkit/internal/m3;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/w2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/internal/m3;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final addAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z2;->addAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V

    return-void
.end method

.method public final addAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z2;->addAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V

    return-void
.end method

.method public final b()Lcom/pspdfkit/internal/v3;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->a()Lcom/pspdfkit/internal/v3;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m3;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m3;->a()Lcom/pspdfkit/internal/v3;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/pspdfkit/internal/u2;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/u2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 46
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/pspdfkit/internal/x2;

    check-cast p1, Lcom/pspdfkit/internal/m3;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/x2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/internal/m3;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final c()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->pause()V

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m3;->pause()V

    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/pspdfkit/internal/v2;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/v2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V
    .locals 2

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 58
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/pspdfkit/internal/y2;

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/y2;-><init>(Lcom/pspdfkit/internal/z2;Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final canPlay(Lcom/pspdfkit/annotations/SoundAnnotation;)Z
    .locals 4

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 85
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/annotations/sound/AudioEncoding;->SIGNED:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.method public final canRecord(Lcom/pspdfkit/annotations/SoundAnnotation;)Z
    .locals 2

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final enterAudioPlaybackMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 4

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m3;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-interface {v0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->exitAudioRecordingMode()V

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/a3;->canPlay(Lcom/pspdfkit/annotations/SoundAnnotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "fragment.requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 7
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/pspdfkit/internal/b3;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;ZI)V

    :cond_1
    return-void
.end method

.method public final enterAudioRecordingMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 3

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->exitAudioPlaybackMode()V

    .line 4
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/a3;->canRecord(Lcom/pspdfkit/annotations/SoundAnnotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    iget-object v1, p0, Lcom/pspdfkit/internal/a3;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "fragment.requireContext()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/pspdfkit/internal/m3;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V

    :cond_1
    return-void
.end method

.method public final exitActiveAudioMode()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->c:Lcom/pspdfkit/internal/b3;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b3;->exitAudioPlaybackMode()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->d:Lcom/pspdfkit/internal/m3;

    invoke-interface {v0}, Lcom/pspdfkit/ui/audio/AudioRecordingController;->exitAudioRecordingMode()V

    return-void
.end method

.method public final removeAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z2;->removeAudioPlaybackModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioPlaybackModeChangeListener;)V

    return-void
.end method

.method public final removeAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/a3;->b:Lcom/pspdfkit/internal/z2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z2;->removeAudioRecordingModeChangeListener(Lcom/pspdfkit/ui/audio/AudioModeListeners$AudioRecordingModeChangeListener;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/im;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/im$b;,
        Lcom/pspdfkit/internal/im$f;,
        Lcom/pspdfkit/internal/im$a;,
        Lcom/pspdfkit/internal/im$d;,
        Lcom/pspdfkit/internal/im$e;,
        Lcom/pspdfkit/internal/im$c;,
        Lcom/pspdfkit/internal/im$h;,
        Lcom/pspdfkit/internal/im$g;
    }
.end annotation


# static fields
.field private static final o:Ljava/lang/Integer;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Lcom/pspdfkit/internal/dm;

.field private d:Lcom/pspdfkit/internal/dm$e;

.field private final e:Lcom/pspdfkit/internal/im$e;

.field private final f:Lcom/pspdfkit/internal/as$a;

.field g:Lcom/pspdfkit/internal/n2;

.field private final h:Lcom/pspdfkit/internal/sh;

.field private final i:Lcom/pspdfkit/internal/mu;

.field private final j:Lcom/pspdfkit/internal/da;

.field private k:Z

.field private l:Z

.field private final m:Lcom/pspdfkit/internal/jq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/jq<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$7mCWJwtSlHQqNmCAgx7kQvkFh_g(Lcom/pspdfkit/internal/im;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic $r8$lambda$G5lGisdRDK_YUDCf8IilgSnZH9A(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/im;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/im;)Lcom/pspdfkit/internal/im$e;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/im;->e:Lcom/pspdfkit/internal/im$e;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/internal/im;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/im;->k:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/im;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/im;->l:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputk(Lcom/pspdfkit/internal/im;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/im;->k:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/internal/im;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/im;->l:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/im;->o:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/im$e;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 44
    iput-boolean v0, p0, Lcom/pspdfkit/internal/im;->k:Z

    .line 47
    iput-boolean v0, p0, Lcom/pspdfkit/internal/im;->l:Z

    .line 50
    new-instance v1, Lcom/pspdfkit/internal/jq;

    invoke-direct {v1}, Lcom/pspdfkit/internal/jq;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/im;->m:Lcom/pspdfkit/internal/jq;

    const/4 v1, 0x0

    .line 54
    iput-object v1, p0, Lcom/pspdfkit/internal/im;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 64
    iput-object p1, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    .line 65
    iput-object p2, p0, Lcom/pspdfkit/internal/im;->e:Lcom/pspdfkit/internal/im$e;

    .line 66
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/im;->a(Lcom/pspdfkit/internal/im$c;)V

    .line 68
    new-instance p1, Lcom/pspdfkit/internal/sh;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/sh;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    .line 69
    new-instance p1, Lcom/pspdfkit/internal/mu;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/mu;-><init>(Lcom/pspdfkit/internal/im;Landroid/util/DisplayMetrics;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    .line 70
    new-instance p1, Lcom/pspdfkit/internal/n2;

    new-instance v5, Lcom/pspdfkit/internal/im$a;

    invoke-direct {v5, p0, v1}, Lcom/pspdfkit/internal/im$a;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$a-IA;)V

    new-instance v6, Lcom/pspdfkit/internal/im$b;

    invoke-direct {v6, p0, v1}, Lcom/pspdfkit/internal/im$b;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$b-IA;)V

    move-object v2, p1

    move-object v3, p0

    move-object v4, p4

    move-object v7, p3

    move-object v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/pspdfkit/internal/n2;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/n2$b;Lcom/pspdfkit/internal/n2$c;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/a1;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    .line 77
    new-instance p1, Lcom/pspdfkit/internal/da;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/da;-><init>(Lcom/pspdfkit/internal/im;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    .line 79
    new-instance p1, Lcom/pspdfkit/internal/as$a;

    const/4 p2, 0x3

    new-array p2, p2, [Lcom/pspdfkit/internal/xc;

    iget-object p3, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    .line 80
    invoke-virtual {p3}, Lcom/pspdfkit/internal/n2;->a()Lcom/pspdfkit/internal/xc;

    move-result-object p3

    aput-object p3, p2, v0

    new-instance p3, Lcom/pspdfkit/internal/im$d;

    invoke-direct {p3, p0, v1}, Lcom/pspdfkit/internal/im$d;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$d-IA;)V

    const/4 p4, 0x1

    aput-object p3, p2, p4

    new-instance p3, Lcom/pspdfkit/internal/im$f;

    invoke-direct {p3, p0, v1}, Lcom/pspdfkit/internal/im$f;-><init>(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$f-IA;)V

    const/4 p5, 0x2

    aput-object p3, p2, p5

    .line 81
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/as$a;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/im;->f:Lcom/pspdfkit/internal/as$a;

    .line 89
    invoke-virtual {p0, p4}, Landroid/view/View;->setFocusable(Z)V

    .line 90
    invoke-static {}, Lcom/pspdfkit/internal/v;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Landroid/view/View;->setDefaultFocusHighlightEnabled(Z)V

    :cond_0
    return-void
.end method

.method private synthetic a(Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sh;->c()V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/mu;->e()V

    :cond_0
    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Exception in rendering queue!"

    .line 33
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->m:Lcom/pspdfkit/internal/jq;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jq;->a()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x32

    .line 3
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/core/Flowable;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Flowable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/im$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/im$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/im;)V

    new-instance v2, Lcom/pspdfkit/internal/im$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/pspdfkit/internal/im$$ExternalSyntheticLambda1;-><init>()V

    .line 5
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Flowable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/im;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a(II)Landroid/graphics/RectF;
    .locals 4

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    return-object p1

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/im;->getPdfToPageViewTransformation()Landroid/graphics/Matrix;

    move-result-object v0

    .line 47
    new-instance v1, Landroid/graphics/PointF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 48
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x4

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    .line 50
    iget-object p2, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p2

    iget-object v2, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    int-to-float v3, p1

    invoke-virtual {p2, v2, v1, v3}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/PointF;F)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 51
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    neg-int p1, p1

    int-to-float p1, p1

    .line 52
    invoke-virtual {p2, p1, p1}, Landroid/graphics/RectF;->inset(FF)V

    :cond_1
    return-object p2
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm$e;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    .line 3
    iput-object p1, v0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    .line 5
    iput-object p1, v0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/n2;->a(Lcom/pspdfkit/internal/dm$e;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    .line 8
    iput-object p1, v0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/jm;

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/im;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/pspdfkit/internal/jm;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/zf;I)V

    .line 13
    invoke-static {p0, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    .line 22
    invoke-direct {p0}, Lcom/pspdfkit/internal/im;->c()V

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/im$c;)V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    monitor-enter v0

    .line 35
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/im$g;)V
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    monitor-enter v0

    .line 38
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/im$c;

    .line 39
    invoke-interface {v2, p0, p1}, Lcom/pspdfkit/internal/im$c;->a(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V

    goto :goto_0

    .line 41
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 42
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, p1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->m:Lcom/pspdfkit/internal/jq;

    sget-object v0, Lcom/pspdfkit/internal/im;->o:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jq;->a(Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sh;->b()V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/im$c;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final b(Z)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/mu;->a(Z)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/n2;->c()V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/da;->b()V

    return-void
.end method

.method public getGestureReceiver()Lcom/pspdfkit/internal/xc;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->f:Lcom/pspdfkit/internal/as$a;

    return-object v0
.end method

.method public getLocalVisibleRect()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getLocalVisibleRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getParentView()Lcom/pspdfkit/internal/dm;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    return-object v0
.end method

.method public getPdfToPageViewTransformation()Landroid/graphics/Matrix;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->c:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/n2;->onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/sh;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/mu;->a(Landroid/graphics/Canvas;)Z

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/n2;->a(Landroid/graphics/Canvas;)Z

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/da;->a(Landroid/graphics/Canvas;)Z

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p2

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sh;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sh;->c()V

    :cond_0
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sh;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sh;->c()V

    :cond_0
    return-void
.end method

.method public final performClick()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->e:Lcom/pspdfkit/internal/im$e;

    check-cast v0, Lcom/pspdfkit/internal/dm$d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1}, Lcom/pspdfkit/internal/dm$d;->a(Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final recycle()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/im;->d:Lcom/pspdfkit/internal/dm$e;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    monitor-enter v1

    .line 4
    :try_start_0
    iget-object v2, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/im;->b:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/pspdfkit/internal/im;->e:Lcom/pspdfkit/internal/im$e;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->h:Lcom/pspdfkit/internal/sh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/sh;->recycle()V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->i:Lcom/pspdfkit/internal/mu;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/mu;->recycle()V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->g:Lcom/pspdfkit/internal/n2;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/n2;->recycle()V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/da;->recycle()V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/im;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 17
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 18
    iput-object v0, p0, Lcom/pspdfkit/internal/im;->n:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void

    :catchall_0
    move-exception v0

    .line 19
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setDrawableProviders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/da;->a(Ljava/util/List;)V

    return-void
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im;->j:Lcom/pspdfkit/internal/da;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/da;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

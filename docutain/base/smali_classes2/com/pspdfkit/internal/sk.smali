.class public final Lcom/pspdfkit/internal/sk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ik;


# instance fields
.field private final a:Lcom/pspdfkit/internal/bf;

.field private final b:Lcom/pspdfkit/annotations/Annotation;

.field private final c:Ljava/util/EnumSet;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-class v0, Lcom/pspdfkit/internal/kk;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/sk;->c:Ljava/util/EnumSet;

    const-string v0, "comment"

    .line 9
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rootAnnotation"

    .line 10
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/sk;->a:Lcom/pspdfkit/internal/bf;

    .line 12
    iput-object p2, p0, Lcom/pspdfkit/internal/sk;->b:Lcom/pspdfkit/annotations/Annotation;

    const/4 p2, 0x2

    const/4 v0, 0x3

    .line 13
    invoke-static {p2, v0}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object p2

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/bf;->c()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/sk;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/pspdfkit/internal/kk;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->c:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 2

    const-string v0, "contextualMenuItems"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->c:Ljava/util/EnumSet;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->c:Ljava/util/EnumSet;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/annotations/AnnotationType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->a:Lcom/pspdfkit/internal/bf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/bf;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getAnnotation()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->b:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final getColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    return v0
.end method

.method public final getId()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->a:Lcom/pspdfkit/internal/bf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/bf;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->b:Lcom/pspdfkit/annotations/Annotation;

    instance-of v1, v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    if-eqz v1, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/NoteAnnotation;->getIconName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->a:Lcom/pspdfkit/internal/bf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/bf;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()Lcom/pspdfkit/internal/bf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sk;->a:Lcom/pspdfkit/internal/bf;

    return-object v0
.end method

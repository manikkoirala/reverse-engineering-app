.class public final Lcom/pspdfkit/internal/yq$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/google/android/material/card/MaterialCardView;

.field private final b:Landroid/widget/RadioButton;

.field private final c:Landroid/webkit/WebView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/material/card/MaterialCardView;Landroid/widget/RadioButton;Landroid/webkit/WebView;Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "radio"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animationWebView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/yq$c;->a:Lcom/google/android/material/card/MaterialCardView;

    iput-object p2, p0, Lcom/pspdfkit/internal/yq$c;->b:Landroid/widget/RadioButton;

    iput-object p3, p0, Lcom/pspdfkit/internal/yq$c;->c:Landroid/webkit/WebView;

    iput-object p4, p0, Lcom/pspdfkit/internal/yq$c;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final a()Landroid/webkit/WebView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$c;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method public final b()Lcom/google/android/material/card/MaterialCardView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$c;->a:Lcom/google/android/material/card/MaterialCardView;

    return-object v0
.end method

.method public final c()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$c;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method public final d()Landroid/widget/RadioButton;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$c;->b:Landroid/widget/RadioButton;

    return-object v0
.end method

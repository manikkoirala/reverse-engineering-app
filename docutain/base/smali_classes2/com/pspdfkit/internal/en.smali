.class public abstract Lcom/pspdfkit/internal/en;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field protected final b:Landroid/graphics/Matrix;

.field protected c:F

.field private final d:Landroid/graphics/Rect;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 0

    const/4 p2, 0x0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    .line 10
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/en;->d:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 13
    instance-of v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 14
    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 18
    iget-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    iget-object v2, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 20
    iget-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 21
    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->layoutPosition:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    sget-object v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;->CENTER:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    if-ne v0, v2, :cond_0

    .line 28
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    float-to-int v0, v0

    .line 29
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    float-to-int v1, v1

    .line 30
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int v3, v0, v2

    sub-int v4, v1, p1

    add-int/2addr v0, v2

    add-int/2addr v1, p1

    move v2, v3

    goto :goto_0

    .line 45
    :cond_0
    iget v0, v1, Landroid/graphics/RectF;->left:F

    float-to-int v2, v0

    .line 46
    iget v0, v1, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v2

    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    add-int/2addr p1, v0

    move v4, v0

    move v0, v1

    move v1, p1

    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v4, 0x0

    :goto_0
    if-nez p2, :cond_2

    .line 60
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2, v2, v4, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1

    .line 62
    :cond_2
    invoke-virtual {p2, v2, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    return-object p2
.end method

.method protected final a()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/en;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/en;->getZoomScale()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/en;->c:F

    return-void
.end method

.method protected final a(II)V
    .locals 6

    const/4 v0, 0x0

    .line 3
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 4
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/en;->d:Landroid/graphics/Rect;

    invoke-virtual {p0, v1, v2}, Lcom/pspdfkit/internal/en;->a(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    .line 6
    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, p1

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, p2

    iget v5, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, p1

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, p2

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract getPdfRect()Landroid/graphics/RectF;
.end method

.method public abstract getZoomScale()F
.end method

.method protected final measureChild(Landroid/view/View;II)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 3
    instance-of v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v1, :cond_6

    .line 4
    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 5
    iget-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/pspdfkit/internal/en;->c:F

    .line 11
    iget-object v4, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->fixedScreenSize:Lcom/pspdfkit/utils/Size;

    if-eqz v4, :cond_0

    goto :goto_0

    .line 15
    :cond_0
    iget-boolean v4, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->noZoom:Z

    if-eqz v4, :cond_1

    .line 16
    invoke-virtual {v1, v2}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    const/high16 v2, 0x3f800000    # 1.0f

    .line 17
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 18
    new-instance v4, Lcom/pspdfkit/utils/Size;

    iget-object v3, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->minSize:Lcom/pspdfkit/utils/Size;

    iget v3, v3, Lcom/pspdfkit/utils/Size;->width:F

    iget-object v5, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    .line 21
    invoke-virtual {v5}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v2

    .line 22
    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget-object v5, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->minSize:Lcom/pspdfkit/utils/Size;

    iget v5, v5, Lcom/pspdfkit/utils/Size;->height:F

    iget-object v6, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    .line 27
    invoke-virtual {v6}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v6, v2

    .line 28
    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-direct {v4, v3, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    .line 29
    iget v1, v4, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v1, v1

    .line 30
    iget v2, v4, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v2, v2

    goto :goto_2

    .line 37
    :cond_2
    sget-object v2, Lcom/pspdfkit/internal/en$a;->a:[I

    iget-object v3, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->sizingMode:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 46
    iget-object v2, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 47
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v3, p0, Lcom/pspdfkit/internal/en;->c:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    .line 48
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v3, p0, Lcom/pspdfkit/internal/en;->c:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 49
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleX(F)V

    .line 50
    iget v3, p0, Lcom/pspdfkit/internal/en;->c:F

    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleY(F)V

    .line 52
    iget-object v3, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->layoutPosition:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    sget-object v4, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;->CENTER:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    if-ne v3, v4, :cond_3

    int-to-float v3, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 53
    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotX(F)V

    int-to-float v3, v1

    div-float/2addr v3, v4

    .line 54
    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotY(F)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    .line 56
    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotX(F)V

    .line 57
    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotY(F)V

    goto :goto_1

    .line 62
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid layout space received."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 63
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 64
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    .line 66
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    :goto_1
    move v7, v2

    move v2, v1

    move v1, v7

    .line 91
    :goto_2
    iget-object v3, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->minSize:Lcom/pspdfkit/utils/Size;

    iget v3, v3, Lcom/pspdfkit/utils/Size;->width:F

    int-to-float v1, v1

    .line 92
    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    const/4 v3, 0x0

    .line 93
    invoke-static {p2, v3, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p2

    .line 95
    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->minSize:Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    int-to-float v1, v2

    .line 96
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    .line 97
    invoke-static {p3, v3, v0}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p3

    .line 99
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->measure(II)V

    goto :goto_3

    .line 103
    :cond_6
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    :goto_3
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    invoke-virtual {p0, p1, p1}, Lcom/pspdfkit/internal/en;->a(II)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/en;->a()V

    .line 2
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->measureChildren(II)V

    .line 3
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    return-void
.end method

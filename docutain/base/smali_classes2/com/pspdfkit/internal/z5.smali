.class public abstract Lcom/pspdfkit/internal/z5;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputType:",
        "Ljava/lang/Object;",
        "ResultType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "TResultType;",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/internal/z5;->a:Ljava/lang/String;

    .line 22
    sget-object v0, Lcom/pspdfkit/internal/z5$a;->a:Lcom/pspdfkit/internal/z5$a;

    iput-object v0, p0, Lcom/pspdfkit/internal/z5;->b:Lkotlin/jvm/functions/Function2;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/jni/NativeContentEditor;)Lcom/pspdfkit/internal/jni/NativeContentEditingResult;
    .locals 4

    const-string v0, "editor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    move-result-object v0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/j6;->a()Lkotlinx/serialization/json/Json;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->c()Lkotlinx/serialization/KSerializer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lkotlinx/serialization/json/Json;->encodeToString(Lkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/jni/NativeContentEditor;->executeCommand(Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    move-result-object p1

    const-string v0, "editor.executeCommand(na\u2026and, inputParametersJSON)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            ")TResultType;"
        }
    .end annotation

    const-string v0, "nativeResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->f()Lcom/pspdfkit/internal/q6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q6;->b(Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z5;->a:Ljava/lang/String;

    return-object v0
.end method

.method public abstract b()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TInputType;"
        }
    .end annotation
.end method

.method public abstract c()Lkotlinx/serialization/KSerializer;
.end method

.method public abstract d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
.end method

.method public e()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "TResultType;",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z5;->b:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public f()Lcom/pspdfkit/internal/q6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/q6<",
            "TResultType;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/q6;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->g()Lkotlinx/serialization/KSerializer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/z5;->e()Lkotlin/jvm/functions/Function2;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/q6;-><init>(Lkotlinx/serialization/DeserializationStrategy;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public abstract g()Lkotlinx/serialization/KSerializer;
.end method

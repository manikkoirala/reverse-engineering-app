.class final Lcom/pspdfkit/internal/bp$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/bp;->onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/bp;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/bp;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/bp$a;->a:Lcom/pspdfkit/internal/bp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/pspdfkit/internal/bp$a;->a:Lcom/pspdfkit/internal/bp;

    invoke-static {v0}, Lcom/pspdfkit/internal/bp;->a(Lcom/pspdfkit/internal/bp;)Ljava/util/ArrayList;

    move-result-object v0

    check-cast p1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object p1, p0, Lcom/pspdfkit/internal/bp$a;->a:Lcom/pspdfkit/internal/bp;

    invoke-static {p1}, Lcom/pspdfkit/internal/bp;->b(Lcom/pspdfkit/internal/bp;)V

    return-void
.end method

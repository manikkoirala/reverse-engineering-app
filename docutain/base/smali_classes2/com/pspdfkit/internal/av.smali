.class public final Lcom/pspdfkit/internal/av;
.super Lcom/pspdfkit/internal/bv;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/av$a;,
        Lcom/pspdfkit/internal/av$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/av$b;


# instance fields
.field private final c:Lcom/pspdfkit/internal/c7;

.field private d:Lcom/pspdfkit/internal/l7;

.field private e:Lcom/pspdfkit/internal/d8;

.field private final f:Lcom/pspdfkit/internal/xg;

.field private final g:I

.field private h:Lcom/pspdfkit/internal/vq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/av$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/av$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/av;->Companion:Lcom/pspdfkit/internal/av$b;

    return-void
.end method

.method private constructor <init>(ILcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/l7;Lcom/pspdfkit/internal/d8;Lcom/pspdfkit/internal/xg;Lkotlin/UInt;Lcom/pspdfkit/internal/vq;)V
    .locals 2

    and-int/lit8 v0, p1, 0x1f

    const/16 v1, 0x1f

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/av$a;->a:Lcom/pspdfkit/internal/av$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/av$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/bv;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/av;->c:Lcom/pspdfkit/internal/c7;

    iput-object p3, p0, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    iput-object p4, p0, Lcom/pspdfkit/internal/av;->e:Lcom/pspdfkit/internal/d8;

    iput-object p5, p0, Lcom/pspdfkit/internal/av;->f:Lcom/pspdfkit/internal/xg;

    invoke-virtual {p6}, Lkotlin/UInt;->unbox-impl()I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/av;->g:I

    and-int/lit8 p1, p1, 0x20

    if-nez p1, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    goto :goto_0

    :cond_1
    iput-object p7, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    :goto_0
    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/l7;Lcom/pspdfkit/internal/d8;Lcom/pspdfkit/internal/xg;Lkotlin/UInt;Lcom/pspdfkit/internal/vq;I)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    invoke-direct/range {p0 .. p7}, Lcom/pspdfkit/internal/av;-><init>(ILcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/l7;Lcom/pspdfkit/internal/d8;Lcom/pspdfkit/internal/xg;Lkotlin/UInt;Lcom/pspdfkit/internal/vq;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/av;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/c7$a;->a:Lcom/pspdfkit/internal/c7$a;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/av;->c:Lcom/pspdfkit/internal/c7;

    const/4 v2, 0x0

    .line 3
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/l7$a;->a:Lcom/pspdfkit/internal/l7$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    const/4 v3, 0x1

    invoke-interface {p1, p2, v3, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/d8$a;->a:Lcom/pspdfkit/internal/d8$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/av;->e:Lcom/pspdfkit/internal/d8;

    const/4 v4, 0x2

    invoke-interface {p1, p2, v4, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/xg$a;->a:Lcom/pspdfkit/internal/xg$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/av;->f:Lcom/pspdfkit/internal/xg;

    const/4 v4, 0x3

    invoke-interface {p1, p2, v4, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lkotlinx/serialization/internal/UIntSerializer;->INSTANCE:Lkotlinx/serialization/internal/UIntSerializer;

    iget v1, p0, Lcom/pspdfkit/internal/av;->g:I

    invoke-static {v1}, Lkotlin/UInt;->box-impl(I)Lkotlin/UInt;

    move-result-object v1

    const/4 v4, 0x4

    invoke-interface {p1, p2, v4, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    const/4 v0, 0x5

    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    sget-object v1, Lcom/pspdfkit/internal/vq$a;->a:Lcom/pspdfkit/internal/vq$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    invoke-interface {p1, p2, v0, v1, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/av;)V
    .locals 1

    const-string v0, "updateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object v0, p1, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    iput-object v0, p0, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    .line 8
    iget-object p1, p1, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    iput-object p1, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/d8;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/av;->e:Lcom/pspdfkit/internal/d8;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/l7;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/vq;)V
    .locals 0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    return-void
.end method

.method public final b()Lcom/pspdfkit/internal/c7;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->c:Lcom/pspdfkit/internal/c7;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->f:Lcom/pspdfkit/internal/xg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/internal/l7;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->d:Lcom/pspdfkit/internal/l7;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/d8;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->e:Lcom/pspdfkit/internal/d8;

    return-object v0
.end method

.method public final g()Lcom/pspdfkit/internal/xg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->f:Lcom/pspdfkit/internal/xg;

    return-object v0
.end method

.method public final h()Lcom/pspdfkit/internal/vq;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/av;->h:Lcom/pspdfkit/internal/vq;

    return-object v0
.end method

.method public final i()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/av;->g:I

    return v0
.end method

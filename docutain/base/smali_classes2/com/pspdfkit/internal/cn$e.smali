.class public final Lcom/pspdfkit/internal/cn$e;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/cn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/cn$e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/cn$e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field final synthetic b:Lcom/pspdfkit/internal/cn;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/cn$e;->a:Landroid/content/Context;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/cn;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/cn$e;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetf(Lcom/pspdfkit/internal/cn;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 5

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/cn$e$a;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetf(Lcom/pspdfkit/internal/cn;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    .line 3
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fputf(Lcom/pspdfkit/internal/cn$e$a;Lcom/pspdfkit/ui/tabs/PdfTabBarItem;)V

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/ui/tabs/PdfTabBarItem;->getDocumentDescriptor()Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v1

    iget-object v2, p1, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v2, v2, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/DocumentDescriptor;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgete(Lcom/pspdfkit/internal/cn$e$a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 10
    iget-object v1, p1, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v1, v1, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v1}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetg(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p2, v1, :cond_0

    .line 11
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 12
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dn;->k()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 13
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    const/4 v1, -0x1

    .line 14
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    .line 16
    :cond_0
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 17
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dn;->j()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 18
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 19
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dn;->c()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 24
    :goto_0
    sget-object v0, Lcom/pspdfkit/internal/cn$b;->a:[I

    iget-object v1, p1, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v1, v1, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v1}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/ui/tabs/PdfTabBarCloseMode;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    goto :goto_1

    .line 30
    :cond_1
    iget-object v0, p1, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v0, v0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetg(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/ui/tabs/PdfTabBarItem;

    move-result-object v0

    if-ne p2, v0, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x0

    .line 36
    :cond_3
    :goto_2
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/ImageView;

    move-result-object p2

    if-eqz v2, :cond_4

    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    const/16 v0, 0x8

    :goto_3
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 37
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/ImageView;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 40
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->forceLayout()V

    .line 42
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object p2

    .line 43
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p1, Lcom/pspdfkit/internal/cn$e$a;->i:Lcom/pspdfkit/internal/cn$e;

    iget-object v1, v1, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    .line 44
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 45
    invoke-virtual {p2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 49
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 50
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    .line 51
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dn;->e()I

    move-result v0

    if-ge p2, v0, :cond_5

    .line 53
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dn;->e()I

    move-result p2

    goto :goto_4

    .line 54
    :cond_5
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dn;->d()I

    move-result v0

    if-le p2, v0, :cond_6

    .line 56
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/cn$e$a;)Lcom/pspdfkit/internal/dn;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dn;->d()I

    move-result p2

    .line 59
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetc(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 61
    :cond_6
    :goto_4
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 62
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgeth(Lcom/pspdfkit/internal/cn$e$a;)I

    move-result v1

    add-int/2addr p2, v1

    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetg(Lcom/pspdfkit/internal/cn$e$a;)I

    move-result v1

    add-int/2addr p2, v1

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 63
    invoke-static {p1}, Lcom/pspdfkit/internal/cn$e$a;->-$$Nest$fgetb(Lcom/pspdfkit/internal/cn$e$a;)Landroid/widget/RelativeLayout;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 3

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/cn$e$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e;->a:Landroid/content/Context;

    .line 2
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__tab_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/cn$e;->b:Lcom/pspdfkit/internal/cn;

    invoke-static {v0}, Lcom/pspdfkit/internal/cn;->-$$Nest$fgetb(Lcom/pspdfkit/internal/cn;)Lcom/pspdfkit/internal/dn;

    move-result-object v0

    invoke-direct {p2, p0, p1, v0}, Lcom/pspdfkit/internal/cn$e$a;-><init>(Lcom/pspdfkit/internal/cn$e;Landroid/view/View;Lcom/pspdfkit/internal/dn;)V

    return-object p2
.end method

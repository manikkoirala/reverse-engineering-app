.class final Lcom/pspdfkit/internal/u9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/w9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/w9;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeto(Lcom/pspdfkit/internal/w9;)Landroid/widget/TextView;

    move-result-object p2

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetj(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;

    move-result-object p3

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeti(Lcom/pspdfkit/internal/w9;)Landroid/widget/Spinner;

    move-result-object p4

    invoke-virtual {p4}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result p4

    invoke-virtual {p3, p4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/w9$c;

    iget p3, p3, Lcom/pspdfkit/internal/w9$c;->a:I

    const/4 p4, 0x2

    const/4 p5, 0x1

    const/4 v0, 0x0

    if-ne p3, p4, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_1

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;

    move-result-object p3

    .line 5
    iget-object p3, p3, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 6
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p3

    xor-int/2addr p3, p5

    if-eqz p3, :cond_4

    .line 7
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p3

    invoke-virtual {p3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_3

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    .line 8
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_2

    const-string p3, "[:\\\\/*\"?|<>\']"

    const-string v1, ""

    .line 10
    invoke-virtual {p1, p3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 11
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_4

    const/4 p1, 0x1

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    .line 12
    :goto_3
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetj(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;

    move-result-object p2

    .line 15
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeti(Lcom/pspdfkit/internal/w9;)Landroid/widget/Spinner;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/w9$c;

    iget p1, p1, Lcom/pspdfkit/internal/w9$c;->a:I

    if-ne p1, p4, :cond_5

    const/4 p1, 0x1

    goto :goto_4

    :cond_5
    const/4 p1, 0x0

    :goto_4
    if-eqz p1, :cond_6

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, p5}, Landroid/view/View;->setEnabled(Z)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_5

    .line 20
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/u9;->a:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetk(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :goto_5
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

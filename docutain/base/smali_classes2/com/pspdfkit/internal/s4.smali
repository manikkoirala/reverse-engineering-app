.class public final Lcom/pspdfkit/internal/s4;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/w4$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/s4$a;,
        Lcom/pspdfkit/internal/s4$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/s4$b;",
        ">;",
        "Lcom/pspdfkit/internal/w4$a;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/pspdfkit/internal/u4;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private final m:Lcom/pspdfkit/internal/s4$a;

.field private n:Z

.field private o:Z

.field private p:I

.field private q:Z


# direct methods
.method public static synthetic $r8$lambda$ARp0bmNzCCENuybetB6nb32oh7E(Lcom/pspdfkit/internal/s4$b;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/s4;->a(Lcom/pspdfkit/internal/s4$b;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$TWSBPgRX0NCuUSBXii0GovUmiv0(Lcom/pspdfkit/internal/s4$b;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/s4;->a(Lcom/pspdfkit/internal/s4$b;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iBRM48W8SCLayqmVJtMS4dddWZ0(Lcom/pspdfkit/internal/s4$b;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/s4;->a(Lcom/pspdfkit/internal/s4$b;Ljava/lang/Throwable;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/s4;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/s4;)Lcom/pspdfkit/internal/s4$a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/s4$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/s4;->l:I

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/internal/s4;->n:Z

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/internal/s4;->o:Z

    const/4 v0, -0x1

    .line 13
    iput v0, p0, Lcom/pspdfkit/internal/s4;->p:I

    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/pspdfkit/internal/s4;->q:Z

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    .line 22
    iput-object p2, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    .line 23
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/s4$b;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 85
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/s4$b;Landroid/graphics/Bitmap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 87
    iget-object v0, p0, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 88
    iget-object v0, p0, Lcom/pspdfkit/internal/s4$b;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 89
    iget-object p0, p0, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/s4$b;Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 7

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->e:Landroid/widget/TextView;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__bookmark:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->e:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    const/16 v3, 0x20

    .line 5
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    :goto_0
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    sget v2, Lcom/pspdfkit/R$string;->pspdf__annotation_list_page:I

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    .line 10
    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 11
    invoke-static {v1, v2, v0, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    iget v0, p0, Lcom/pspdfkit/internal/s4;->l:I

    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 15
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 16
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 17
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    iget v4, p0, Lcom/pspdfkit/internal/s4;->k:I

    iget v5, p0, Lcom/pspdfkit/internal/s4;->f:I

    invoke-static {v1, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 19
    :cond_1
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 20
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 23
    :goto_1
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/s4;->p:I

    if-ne v0, v1, :cond_2

    const/4 v0, -0x1

    .line 27
    iput v0, p0, Lcom/pspdfkit/internal/s4;->p:I

    .line 28
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/pspdfkit/internal/s4;->i:I

    .line 29
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v6

    iget v4, p0, Lcom/pspdfkit/internal/s4;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    iget v3, p0, Lcom/pspdfkit/internal/s4;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v1, v4

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v3, 0x2ee

    .line 30
    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    const-wide/16 v3, 0x3e8

    .line 31
    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 32
    new-instance v1, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/s4$b;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 34
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 37
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    if-eqz v0, :cond_7

    .line 38
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->i:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 41
    iget-boolean v0, p0, Lcom/pspdfkit/internal/s4;->q:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/u4;->b(Lcom/pspdfkit/bookmarks/Bookmark;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 43
    iget-object v1, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/u4;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 48
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/u4;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 50
    :cond_5
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->i:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    .line 51
    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/u4;->c(Lcom/pspdfkit/bookmarks/Bookmark;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    .line 52
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v1, v3}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    .line 53
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v1, v3}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v1

    iget-object v3, p1, Lcom/pspdfkit/internal/s4$b;->g:Landroid/widget/TextView;

    .line 54
    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda1;

    invoke-direct {v4, v3}, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda1;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v1, v4}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 61
    :goto_3
    iget v0, p1, Lcom/pspdfkit/internal/s4$b;->a:I

    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_6

    iget v0, p1, Lcom/pspdfkit/internal/s4$b;->b:I

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    .line 62
    invoke-virtual {v1}, Lcom/pspdfkit/internal/u4;->a()I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    .line 63
    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_7

    .line 65
    :cond_6
    invoke-virtual {p2}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/pspdfkit/internal/s4$b;->a:I

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u4;->a()I

    move-result v0

    iput v0, p1, Lcom/pspdfkit/internal/s4$b;->b:I

    .line 67
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 68
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 69
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    .line 72
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__bookmark_page_image_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    .line 74
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__bookmark_page_image_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 77
    iget-object v2, p1, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 78
    iget-object v2, p1, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 79
    iget-object v2, p1, Lcom/pspdfkit/internal/s4$b;->i:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v3, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    new-instance v4, Lcom/pspdfkit/utils/Size;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-direct {v4, v0, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 80
    invoke-virtual {v3, p2, v4}, Lcom/pspdfkit/internal/u4;->a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/utils/Size;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 81
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 82
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda2;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/s4$b;)V

    new-instance v1, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/s4$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/s4$b;)V

    .line 83
    invoke-virtual {p2, v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 84
    invoke-virtual {v2, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :cond_7
    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/s4$b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 90
    iget-object p1, p0, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const/4 v0, 0x1

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 91
    iget-object p0, p0, Lcom/pspdfkit/internal/s4$b;->d:Landroid/widget/ImageView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .line 130
    iput p1, p0, Lcom/pspdfkit/internal/s4;->p:I

    .line 131
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    return-void
.end method

.method public final a(II)V
    .locals 3

    if-ge p1, p2, :cond_0

    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_1

    .line 109
    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v1, v0, v2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, p1

    :goto_1
    if-le v0, p2, :cond_1

    .line 113
    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-static {v1, v0, v2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 117
    :cond_1
    invoke-virtual {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    const/4 p1, 0x1

    .line 118
    iput-boolean p1, p0, Lcom/pspdfkit/internal/s4;->o:Z

    return-void
.end method

.method public final a(IIIII)V
    .locals 3

    .line 96
    iput p1, p0, Lcom/pspdfkit/internal/s4;->d:I

    .line 97
    invoke-static {p1}, Lcom/pspdfkit/internal/s5;->a(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/s4;->e:I

    .line 98
    iput p2, p0, Lcom/pspdfkit/internal/s4;->f:I

    .line 100
    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result p1

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v0

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    const/16 v2, 0x40

    invoke-static {v2, p1, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/s4;->g:I

    .line 101
    iput p3, p0, Lcom/pspdfkit/internal/s4;->i:I

    .line 102
    iput p5, p0, Lcom/pspdfkit/internal/s4;->k:I

    .line 103
    iput p4, p0, Lcom/pspdfkit/internal/s4;->j:I

    .line 105
    iget-object p1, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    sget p3, Lcom/pspdfkit/R$drawable;->pspdf__arrow_right:I

    invoke-static {p1, p3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 106
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 107
    invoke-static {p1, p2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 108
    iput-object p1, p0, Lcom/pspdfkit/internal/s4;->h:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final a(Ljava/util/List;Lcom/pspdfkit/internal/u4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;",
            "Lcom/pspdfkit/internal/u4;",
            ")V"
        }
    .end annotation

    .line 92
    iput-object p1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    .line 93
    iput-object p2, p0, Lcom/pspdfkit/internal/s4;->b:Lcom/pspdfkit/internal/u4;

    .line 94
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 95
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    .line 119
    iget-boolean v0, p0, Lcom/pspdfkit/internal/s4;->n:Z

    if-eq p1, v0, :cond_1

    .line 120
    iput-boolean p1, p0, Lcom/pspdfkit/internal/s4;->n:Z

    .line 121
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 124
    iput-boolean v0, p0, Lcom/pspdfkit/internal/s4;->o:Z

    goto :goto_1

    .line 125
    :cond_0
    iget-boolean p1, p0, Lcom/pspdfkit/internal/s4;->o:Z

    if-eqz p1, :cond_1

    .line 126
    iget-object p1, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    if-eqz p1, :cond_1

    .line 128
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v0, p1, :cond_1

    .line 129
    iget-object p1, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-interface {p1, v1, v0}, Lcom/pspdfkit/internal/s4$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public final b(I)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/s4;->o:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-interface {v2, v3, v0}, Lcom/pspdfkit/internal/s4$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/s4;->m:Lcom/pspdfkit/internal/s4$a;

    if-eqz v2, :cond_1

    .line 8
    invoke-interface {v2, v0}, Lcom/pspdfkit/internal/s4$a;->a(Lcom/pspdfkit/bookmarks/Bookmark;)Z

    move-result v1

    :cond_1
    if-eqz v1, :cond_2

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 13
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    :cond_2
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 14
    iput-boolean p1, p0, Lcom/pspdfkit/internal/s4;->q:Z

    return-void
.end method

.method public final c(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/s4;->l:I

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getUuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/s4$b;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/bookmarks/Bookmark;

    .line 3
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->i:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 4
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->e:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 5
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 6
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->g:Landroid/widget/TextView;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/s4;->a:Landroid/content/Context;

    iget v1, p0, Lcom/pspdfkit/internal/s4;->k:I

    iget v2, p0, Lcom/pspdfkit/internal/s4;->j:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 8
    iget-object v1, p1, Lcom/pspdfkit/internal/s4$b;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 9
    iget-object v0, p1, Lcom/pspdfkit/internal/s4$b;->h:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/pspdfkit/internal/s4;->n:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/s4;->a(Lcom/pspdfkit/internal/s4$b;Lcom/pspdfkit/bookmarks/Bookmark;)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__outline_bookmarks_list_item:I

    const/4 v1, 0x0

    .line 2
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 3
    new-instance p2, Lcom/pspdfkit/internal/s4$b;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/internal/s4$b;-><init>(Lcom/pspdfkit/internal/s4;Landroid/view/View;)V

    return-object p2
.end method

.class final Lcom/pspdfkit/internal/zu$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputc(Lcom/pspdfkit/internal/zu;I)V

    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputy(Lcom/pspdfkit/internal/zu;Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputz(Lcom/pspdfkit/internal/zu;Z)V

    .line 6
    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->b()V

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgets(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 11
    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 14
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/yu;->setEnabled(Z)V

    .line 16
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputk(Lcom/pspdfkit/internal/zu;I)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputl(Lcom/pspdfkit/internal/zu;I)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetx(Lcom/pspdfkit/internal/zu;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 22
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zu;->b(I)V

    .line 24
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    const/4 v2, 0x3

    if-eqz v1, :cond_6

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    if-eqz v1, :cond_6

    .line 25
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result v3

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    invoke-interface {p1, v3, v1}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetn(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result v3

    if-ne v1, v3, :cond_7

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeto(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result v3

    if-ne v1, v3, :cond_7

    .line 30
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetd(Lcom/pspdfkit/internal/zu;)I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->h()V

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object p1

    if-eqz p1, :cond_7

    const/16 v0, 0xbb8

    .line 33
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    goto :goto_0

    .line 34
    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result p1

    if-nez p1, :cond_7

    if-nez v0, :cond_5

    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    move-result p1

    if-lez p1, :cond_7

    .line 35
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$b;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object p1

    if-eqz p1, :cond_7

    const/4 v0, 0x0

    .line 38
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    goto :goto_0

    .line 45
    :cond_6
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetd(Lcom/pspdfkit/internal/zu;)I

    move-result v0

    if-ne v0, v2, :cond_7

    .line 46
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->h()V

    :cond_7
    :goto_0
    return-void
.end method

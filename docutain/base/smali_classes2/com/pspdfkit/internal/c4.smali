.class public Lcom/pspdfkit/internal/c4;
.super Lcom/pspdfkit/internal/x4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ShapeDelegate:",
        "Lcom/pspdfkit/internal/d4;",
        ">",
        "Lcom/pspdfkit/internal/x4<",
        "TShapeDelegate;>;"
    }
.end annotation


# instance fields
.field private b:I


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/d4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TShapeDelegate;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/x4;-><init>(Lcom/pspdfkit/internal/y4;)V

    return-void
.end method

.method private b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 3

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object p1

    .line 6
    invoke-interface {p1}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 7
    iget v1, p0, Lcom/pspdfkit/internal/c4;->b:I

    const/4 v2, 0x0

    if-ne v1, v0, :cond_0

    return v2

    .line 8
    :cond_0
    iput v0, p0, Lcom/pspdfkit/internal/c4;->b:I

    .line 12
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    const/high16 p2, 0x3f800000    # 1.0f

    div-float/2addr p2, p3

    .line 13
    invoke-virtual {v0, p2, p2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 15
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 16
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/graphics/PointF;

    .line 17
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 18
    invoke-static {p3, v1, v0}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 19
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 22
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/d4;->w()Ljava/util/ArrayList;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p3

    const/4 v0, 0x2

    if-lt p3, v0, :cond_2

    invoke-interface {p2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 24
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    if-eqz p4, :cond_3

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/d4;->v()V

    :cond_3
    const/4 v2, 0x1

    :cond_4
    return v2
.end method


# virtual methods
.method public bridge synthetic a(ILandroid/graphics/Matrix;F)Lcom/pspdfkit/annotations/Annotation;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/c4;->b:I

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/br$a;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/br$a;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ri;)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->a()Z

    move-result v0

    return v0
.end method

.method public final a(FLandroid/graphics/Matrix;)Z
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    move-result p1

    return p1
.end method

.method public a(IIFLcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;FLandroidx/core/util/Pair;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIF",
            "Lcom/pspdfkit/annotations/BorderStyle;",
            "Lcom/pspdfkit/annotations/BorderEffect;",
            "F",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;F",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;)Z"
        }
    .end annotation

    .line 6
    iget-object p9, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p9, Lcom/pspdfkit/internal/d4;

    invoke-virtual {p9}, Lcom/pspdfkit/internal/h4;->g()I

    move-result p9

    if-ne p9, p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/h4;->j()I

    move-result p1

    if-ne p1, p2, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/h4;->m()F

    move-result p1

    cmpl-float p1, p1, p3

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/y4;->r()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p1

    if-ne p1, p4, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/y4;->p()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object p1

    if-ne p1, p5, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/y4;->q()F

    move-result p1

    cmpl-float p1, p1, p6

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/y4;->s()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, p7}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/d4;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p1

    cmpl-float p1, p1, p8

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 1

    .line 3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result v0

    .line 4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/c4;->b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p1

    or-int/2addr p1, v0

    return p1
.end method

.method public final a(Z)Z
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Z)Z

    move-result p1

    return p1
.end method

.method protected final b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;
    .locals 5

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/d4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/d4;->w()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/d4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/d4;->w()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 29
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 31
    iget v4, v2, Landroid/graphics/PointF;->x:F

    mul-float v4, v4, p1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float v2, v2, p1

    invoke-virtual {v3, v4, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 33
    invoke-static {v3, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 34
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/d4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/d4;->b(Z)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->k()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x4;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/d4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d4;->v()V

    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/d4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d4;->y()V

    return-void
.end method

.method public final hide()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->hide()V

    return-void
.end method

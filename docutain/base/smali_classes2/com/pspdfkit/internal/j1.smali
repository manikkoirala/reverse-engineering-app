.class public final Lcom/pspdfkit/internal/j1;
.super Lcom/pspdfkit/internal/en;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;


# instance fields
.field private final e:Lcom/pspdfkit/internal/dm;

.field private final f:Lcom/pspdfkit/internal/w1;

.field private final g:Landroid/graphics/Matrix;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/ArrayList;


# direct methods
.method public static synthetic $r8$lambda$K_AgDndWlndMuxR7gLb04X37Xx0(Lcom/pspdfkit/internal/j1;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/j1;->a(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/w1;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j1;->g:Landroid/graphics/Matrix;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j1;->i:Ljava/util/ArrayList;

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    .line 16
    iput-object p2, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    .line 18
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p1, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 1

    .line 55
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method private synthetic a(Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/pspdfkit/internal/views/annotations/c;

    invoke-direct {v0}, Lcom/pspdfkit/internal/views/annotations/c;-><init>()V

    .line 47
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 48
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 49
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_0

    .line 50
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/views/annotations/c;->a(Lcom/pspdfkit/internal/views/annotations/a;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    :cond_1
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/pspdfkit/internal/j1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/j1$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/c;->a(Lcom/pspdfkit/internal/views/annotations/c$a;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 5
    invoke-virtual {p0, v0, p2}, Lcom/pspdfkit/internal/j1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 5

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->g(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_6

    if-nez v0, :cond_0

    if-eqz v1, :cond_6

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->e(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 14
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eq v4, p0, :cond_1

    return v2

    :cond_1
    if-nez v0, :cond_5

    if-eqz v1, :cond_2

    return v2

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-nez v0, :cond_3

    return v3

    .line 29
    :cond_3
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/j1;->addView(Landroid/view/View;)V

    .line 30
    iget-object v1, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    if-nez p2, :cond_4

    .line 33
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object p2

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/j1;->i:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return v3

    .line 38
    :cond_4
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 41
    :cond_5
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 42
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    :goto_0
    return v2

    .line 45
    :cond_6
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    xor-int/lit8 p1, p2, 0x1

    return p1
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 3
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 6
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 3

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 12
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 14
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/j1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Z

    return-void
.end method

.method public final b(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p2, :cond_1

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 3
    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->f:Lcom/pspdfkit/internal/w1;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    goto :goto_0

    .line 5
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-void
.end method

.method public final c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_4

    .line 2
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3
    instance-of v4, v3, Lcom/pspdfkit/internal/h2;

    if-eqz v4, :cond_2

    .line 5
    move-object v4, v3

    check-cast v4, Lcom/pspdfkit/internal/h2;

    invoke-interface {v4}, Lcom/pspdfkit/internal/h2;->getAnnotations()Ljava/util/List;

    move-result-object v4

    .line 6
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/annotations/Annotation;

    if-ne p1, v5, :cond_1

    .line 8
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    return-object v3

    .line 11
    :cond_2
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_3

    .line 12
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    .line 13
    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    if-ne p1, v4, :cond_3

    return-object v3

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-object v0
.end method

.method public final c()Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0

    .line 15
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/j1$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/j1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/j1;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->g:Landroid/graphics/Matrix;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 4
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_0

    .line 6
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/j1;->getZoomScale()F

    move-result v4

    invoke-interface {v3, v4, v0}, Lcom/pspdfkit/internal/views/annotations/a;->a(FLandroid/graphics/Matrix;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .line 1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 7
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x42

    const/4 v3, 0x0

    if-ne v0, v2, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 9
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 10
    instance-of v2, v0, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v2, :cond_4

    .line 11
    check-cast v0, Lcom/pspdfkit/internal/views/annotations/a;

    .line 12
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_3

    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 14
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 15
    instance-of v0, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v0, :cond_2

    .line 18
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-nez v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    new-array v2, v1, [Lcom/pspdfkit/annotations/Annotation;

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 24
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getFormEditor()Lcom/pspdfkit/internal/tb;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/tb;->c(Lcom/pspdfkit/forms/FormElement;)Z

    goto :goto_0

    .line 27
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    new-array v2, v1, [Lcom/pspdfkit/annotations/Annotation;

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    :cond_3
    :goto_0
    return v1

    :cond_4
    return v3
.end method

.method public final focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 4

    const/4 v0, 0x2

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 2
    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_0

    if-eq p2, v3, :cond_1

    .line 6
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v3

    if-ne v2, v1, :cond_2

    if-ne p2, v0, :cond_2

    .line 7
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 11
    :cond_2
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    return-object v0

    .line 17
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAnnotations()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    .line 3
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4
    instance-of v4, v3, Lcom/pspdfkit/internal/h2;

    if-eqz v4, :cond_0

    .line 6
    check-cast v3, Lcom/pspdfkit/internal/h2;

    invoke-interface {v3}, Lcom/pspdfkit/internal/h2;->getAnnotations()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 7
    :cond_0
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_1

    .line 8
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getPdfRect()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPdfRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getZoomScale()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getZoomScale()F

    move-result v0

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Lcom/pspdfkit/internal/en;->onLayout(ZIIII)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j1;->d()V

    return-void
.end method

.method public final recycle()V
    .locals 2

    .line 1
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3
    instance-of v1, v0, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v1, :cond_0

    .line 4
    check-cast v0, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/j1;->a(Lcom/pspdfkit/internal/views/annotations/a;)V

    goto :goto_0

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/j1;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

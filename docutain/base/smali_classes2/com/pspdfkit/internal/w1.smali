.class public final Lcom/pspdfkit/internal/w1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/w1$b;,
        Lcom/pspdfkit/internal/w1$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/dm;

.field private c:Lcom/pspdfkit/internal/dm$e;

.field private final d:Lcom/pspdfkit/internal/i2;

.field private final e:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;

.field private g:Z

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/HashMap;

.field private final k:Ljava/util/ArrayList;

.field private final l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private m:Lio/reactivex/rxjava3/disposables/Disposable;

.field private n:Lcom/pspdfkit/internal/il;

.field final o:Lcom/pspdfkit/internal/j1;

.field private p:Z

.field private final q:Ljava/util/ArrayList;

.field private r:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$Fk0G1LnZIx8ge9FrkxJuvZJqkqQ(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/w1;->a(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$HMft0BuHFf8TJGRvKrTFjVPmxXo(Lcom/pspdfkit/internal/w1;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->c()V

    return-void
.end method

.method public static synthetic $r8$lambda$IpxozXRKavLiCpm35r78AW_797U(Lcom/pspdfkit/internal/w1;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->b(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JsbyTdURBQvObElSk4sIkSEVzYI(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public static synthetic $r8$lambda$O_G8gEilo3KcRliDt_JUSxsXukA(Lcom/pspdfkit/internal/w1;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->d()V

    return-void
.end method

.method public static synthetic $r8$lambda$RdX5vO0a0w8uT4PtWM-8Et7HWyU(Lcom/pspdfkit/internal/w1;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SsByqI55frqhP8k7RDZlpNuVbic(Lcom/pspdfkit/internal/w1;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->a(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$YKj52I7OjA2_9BOKyRUeZLS6Fn4(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/w1;->a(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fKxeaPyPXwvE0AWxrV-LYRIJAww(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/w1;->b(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nltjoYS5R74Rh90eA5zCL3RRAiM(Lcom/pspdfkit/internal/w1;Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/w1;->c(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qCEm7O5hKJsUsqjiNpZ_7YMpKmA(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/w1$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method public static synthetic $r8$lambda$tNytbqvPDZ3ZWOcg7vA_6ooPieM(Lcom/pspdfkit/internal/w1;ZLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/w1;->a(ZLio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i2;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    .line 20
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    .line 25
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    .line 31
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    .line 38
    new-instance v1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    const/4 v1, 0x0

    .line 59
    iput-boolean v1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/w1;->q:Ljava/util/ArrayList;

    .line 75
    iput-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    .line 76
    invoke-static {p2}, Lcom/pspdfkit/internal/x5;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Ljava/util/EnumSet;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/w1;->e:Ljava/util/EnumSet;

    .line 77
    iput-object p3, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    .line 80
    sget-object p2, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p2}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 83
    new-instance p2, Lcom/pspdfkit/internal/j1;

    invoke-direct {p2, p1, p0}, Lcom/pspdfkit/internal/j1;-><init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/w1;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 304
    invoke-static {}, Lio/reactivex/rxjava3/core/Observable;->empty()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    return-object p0
.end method

.method private a()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "-",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 305
    new-instance v0, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/w1;)V

    return-object v0
.end method

.method private synthetic a(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 283
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/w1$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j1;->b()V

    if-eqz p1, :cond_0

    .line 233
    invoke-interface {p1}, Lcom/pspdfkit/internal/w1$a;->a()V

    :cond_0
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;Z)V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    if-ne v0, p2, :cond_2

    .line 90
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 91
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 92
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 93
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 99
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 100
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 102
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_6

    .line 110
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 111
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/dm$e;->b(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_2

    .line 114
    :cond_5
    iget-object p2, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    if-eqz p3, :cond_6

    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 116
    invoke-direct {p0, p1, v0, p1, p2}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    :cond_6
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;ZZ)V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 63
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-ne v0, p2, :cond_1

    return-void

    .line 64
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 66
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 67
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 68
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_6

    .line 76
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 77
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/dm$e;->a(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_1

    .line 80
    :cond_5
    iget-object p2, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p2, p1, p3}, Lcom/pspdfkit/internal/j1;->a(Ljava/util/List;Z)V

    if-eqz p4, :cond_6

    .line 83
    iget-boolean p1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    const/4 p2, 0x1

    xor-int/2addr p1, p2

    const/4 p3, 0x0

    const/4 p4, 0x0

    invoke-direct {p0, p1, p3, p2, p4}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    :cond_6
    return-void
.end method

.method private static synthetic a(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    .line 230
    invoke-interface {p0}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 306
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 307
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/w1;->i(Lcom/pspdfkit/annotations/Annotation;)Z

    goto :goto_0

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j1;->getAnnotations()Ljava/util/List;

    move-result-object v0

    .line 313
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 314
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    .line 315
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 316
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v5

    or-int/2addr v3, v4

    goto :goto_1

    :cond_2
    if-nez v3, :cond_3

    .line 320
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 321
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->i()V

    return-void

    .line 326
    :cond_3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    xor-int/2addr p1, v5

    .line 327
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    .line 328
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j1;->getAnnotations()Ljava/util/List;

    move-result-object v3

    .line 329
    invoke-interface {v3, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 330
    invoke-virtual {v0, v3, v2}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    .line 333
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/j1;->a(Ljava/util/List;Z)V

    .line 334
    iget-boolean p1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda3;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/dm;)V

    invoke-direct {p0, p1, v2, v5, v1}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method private a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;ZZZ",
            "Lcom/pspdfkit/internal/w1$a;",
            ")V"
        }
    .end annotation

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    .line 122
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 124
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    if-eqz p3, :cond_2

    .line 128
    sget-object v5, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 129
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 130
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x1

    .line 135
    :cond_2
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 137
    iget-boolean v6, p0, Lcom/pspdfkit/internal/w1;->g:Z

    if-nez v6, :cond_3

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/w1;->g(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    if-eqz p2, :cond_1

    if-nez v5, :cond_1

    .line 144
    sget-object v4, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 145
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 151
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    .line 152
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p1, v0, v4}, Lcom/pspdfkit/internal/j1;->a(Ljava/util/List;Z)V

    .line 160
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_7

    goto :goto_2

    .line 162
    :cond_7
    new-instance p1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda13;

    invoke-direct {p1, p0, v0, p5}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/internal/w1;Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    move-object p5, p1

    .line 163
    :goto_2
    invoke-direct {p0, v1, v2, p4, p5}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method private synthetic a(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 213
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->n:Lcom/pspdfkit/internal/il;

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/il;->a(I)V

    :cond_0
    return-void
.end method

.method private a(ZLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    new-instance v1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda11;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda11;-><init>(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    .line 217
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/dm;->a(ZLcom/pspdfkit/internal/im$c;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 218
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x1

    new-instance v2, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda12;

    invoke-direct {v2, p2}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda12;-><init>(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-virtual {p1, v1, v2}, Lcom/pspdfkit/internal/dm;->a(ZLcom/pspdfkit/internal/im$c;)V

    goto :goto_0

    .line 222
    :cond_1
    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.PdfView"

    const-string v1, "Attempted to refresh page render after rebinding..."

    .line 228
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    :goto_0
    return-void
.end method

.method private a(ZZZLcom/pspdfkit/internal/w1$a;)V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    .line 165
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j1;->c()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 169
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 170
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v1

    .line 171
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 172
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 173
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    const/4 v3, 0x5

    .line 174
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    .line 175
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 176
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 177
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/w1;Z)V

    .line 178
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    invoke-virtual {v0, p2}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    :cond_1
    if-eqz p1, :cond_2

    .line 186
    new-instance p1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda5;

    invoke-direct {p1, p0, p3}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/w1;Z)V

    .line 187
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda6;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/w1;)V

    .line 209
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 212
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    new-instance p2, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0, p4}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/w1$a;)V

    invoke-virtual {v0, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method private b()Lcom/pspdfkit/internal/dm$e;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    return-object v0

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State can only be accessed after the page has been bound using bindPage()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static synthetic b(Lio/reactivex/rxjava3/core/CompletableEmitter;Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    .line 12
    invoke-interface {p0}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    return-void
.end method

.method private synthetic b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 30
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 31
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    instance-of v1, v0, Lcom/pspdfkit/internal/views/annotations/k;

    if-nez v1, :cond_0

    .line 36
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->q:Ljava/util/ArrayList;

    .line 14
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda1;

    invoke-direct {v5, v1}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda1;-><init>(Ljava/util/List;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    .line 15
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method private synthetic c(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    if-eqz p2, :cond_0

    .line 12
    invoke-interface {p2}, Lcom/pspdfkit/internal/w1$a;->a()V

    :cond_0
    return-void
.end method

.method private synthetic d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/w1;->g:Z

    return-void
.end method

.method private e()Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda0;-><init>()V

    .line 13
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->onErrorResumeNext(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method private g()Lio/reactivex/rxjava3/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Consumer<",
            "-",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/w1;)V

    return-object v0
.end method

.method private h()V
    .locals 5

    .line 189
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 190
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->remove(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 191
    :cond_0
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 193
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 196
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    .line 197
    const-class v2, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/AnnotationType;

    .line 198
    iget-object v4, p0, Lcom/pspdfkit/internal/w1;->e:Ljava/util/EnumSet;

    invoke-virtual {v4, v3}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 199
    iget-object v4, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {v4, v3}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 200
    invoke-virtual {v0, v3}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_2

    .line 203
    :cond_2
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/dm$e;->b(Lcom/pspdfkit/annotations/AnnotationType;)V

    goto :goto_1

    .line 204
    :cond_3
    :goto_2
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/dm$e;->a(Lcom/pspdfkit/annotations/AnnotationType;)V

    goto :goto_1

    .line 205
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->e()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 206
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->a()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/w1;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 208
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method private i(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->e:Ljava/util/EnumSet;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    .line 3
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_3

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_2

    .line 9
    :cond_2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->c(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 10
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return v3

    .line 11
    :cond_3
    :goto_2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->c(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 12
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return v3

    :cond_4
    return v2
.end method


# virtual methods
.method final a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 5

    .line 284
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    check-cast v0, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j2;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    check-cast v0, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j2;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    return-object p1

    .line 286
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    goto :goto_0

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 288
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 293
    iput-boolean v2, p0, Lcom/pspdfkit/internal/w1;->p:Z

    .line 297
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->h()V

    goto :goto_0

    .line 300
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/pspdfkit/internal/j1;->b(Ljava/util/List;Z)V

    .line 301
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/w1;->i(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 302
    invoke-direct {p0, v2, v4, v2, v1}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    :cond_4
    :goto_0
    return-object v1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 2

    .line 53
    sget-object v0, Lcom/pspdfkit/internal/w1$b;->b:Lcom/pspdfkit/internal/w1$b;

    .line 55
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    xor-int/lit8 v1, p2, 0x1

    .line 56
    invoke-direct {p0, v0, p1, v1, p2}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;ZZ)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V
    .locals 2

    .line 57
    sget-object v0, Lcom/pspdfkit/internal/w1$b;->b:Lcom/pspdfkit/internal/w1$b;

    .line 58
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x1

    .line 59
    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;Z)V

    return-void
.end method

.method final a(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/internal/il;)V
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/pspdfkit/internal/w1;->n:Lcom/pspdfkit/internal/il;

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/w1;->c:Lcom/pspdfkit/internal/dm$e;

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object p2, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    iget-object p2, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 14
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->h()V

    return-void
.end method

.method public final a(Ljava/util/ArrayList;Z)V
    .locals 5

    .line 31
    sget-object v0, Lcom/pspdfkit/internal/w1$b;->a:Lcom/pspdfkit/internal/w1$b;

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    iget-object v2, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    :cond_0
    sget-object v2, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 37
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 38
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    .line 39
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 40
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_2
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    .line 44
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    const/4 v3, 0x0

    if-nez p1, :cond_4

    .line 45
    invoke-direct {p0, v0, v1, v3}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;Z)V

    .line 48
    :cond_4
    iget-boolean p1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    invoke-direct {p0, v0, v2, p1, v3}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/internal/w1$b;Ljava/util/List;ZZ)V

    if-eqz p2, :cond_5

    .line 52
    iget-boolean p1, p0, Lcom/pspdfkit/internal/w1;->p:Z

    const/4 p2, 0x0

    invoke-direct {p0, p1, v3, v1, p2}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    :cond_5
    return-void
.end method

.method public final a(Ljava/util/EnumSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 15
    sget-object v0, Lcom/pspdfkit/internal/tl;->a:Ljava/util/EnumSet;

    .line 16
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 17
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/AnnotationType;

    .line 18
    invoke-static {v1}, Lcom/pspdfkit/internal/tl;->a(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 19
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 22
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    .line 26
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {p1}, Ljava/util/AbstractCollection;->clear()V

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 30
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->h()V

    return-void
.end method

.method public final a(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/w1$a;",
            ")V"
        }
    .end annotation

    .line 117
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 118
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v7, p2

    .line 119
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method final a(Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/views/annotations/a;",
            ">;Z)V"
        }
    .end annotation

    .line 234
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 236
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 238
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 240
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 244
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 246
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 247
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/w1;->g(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 248
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez p2, :cond_2

    .line 251
    iget-object v4, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 252
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v5

    if-nez v5, :cond_1

    .line 253
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/j1;->addView(Landroid/view/View;)V

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 254
    :goto_1
    iget-object v5, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    invoke-interface {v5, v2}, Lcom/pspdfkit/internal/i2;->a(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 259
    iget-object v5, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    check-cast v5, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v5, v3}, Lcom/pspdfkit/internal/j2;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v3

    .line 260
    iget-object v5, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/pspdfkit/internal/j1;->addView(Landroid/view/View;)V

    .line 261
    iget-object v3, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    iget-object v5, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    .line 262
    invoke-virtual {v5}, Lcom/pspdfkit/internal/j1;->c()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v5

    new-instance v6, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda10;

    invoke-direct {v6, p0, v2}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 263
    invoke-virtual {v5, v6}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v5

    .line 264
    invoke-virtual {v3, v5}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :cond_3
    :goto_2
    if-nez v4, :cond_4

    .line 272
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    :cond_4
    or-int/2addr v1, v4

    goto/16 :goto_0

    :cond_5
    if-eqz v1, :cond_6

    .line 280
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 281
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->e()Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->g()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Observable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p2

    .line 282
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :cond_6
    return-void
.end method

.method public final a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;Z",
            "Lcom/pspdfkit/internal/w1$a;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    .line 120
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method final a(Lcom/pspdfkit/annotations/AnnotationType;)Z
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j1;->a(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 7
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->a(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/w1;->g:Z

    :goto_0
    return-void
.end method

.method final b(Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 4

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/i2;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 20
    iget-object v2, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    check-cast v2, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/j2;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 21
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_1

    .line 24
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/pspdfkit/internal/j1;->a(Ljava/util/List;Z)V

    .line 28
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/w1;->i(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    .line 29
    invoke-direct {p0, p1, v3, p1, v1}, Lcom/pspdfkit/internal/w1;->a(ZZZLcom/pspdfkit/internal/w1$a;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public final b(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/w1$a;",
            ")V"
        }
    .end annotation

    .line 9
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 10
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/w1;->c(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v7, p2

    .line 11
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method public final c(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 7
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->b(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/w1;->g:Z

    :goto_0
    return-void
.end method

.method public final c(Ljava/util/List;)V
    .locals 8

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 17
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->g()Lio/reactivex/rxjava3/functions/Consumer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    .line 23
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZZZLcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method final d(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->i()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    if-eqz v0, :cond_3

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 14
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    sget-object v1, Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;->PLATFORM_RENDERING:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;

    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/internal/i2;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy$Strategy;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    .line 19
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    :cond_5
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_6
    return-object v0
.end method

.method public final e(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j1;->c(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 8
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-ne p1, v2, :cond_1

    return-object v1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method final f()V
    .locals 2

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/w1;->p:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final f(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 4

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    goto :goto_0

    .line 11
    :cond_1
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const-string v0, "annotation"

    .line 12
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1086
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AutoCAD SHX Text"

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    .line 1087
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->isSignature()Z

    move-result v0

    if-eqz v0, :cond_3

    return v3

    .line 1089
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    .line 1090
    iget-object v2, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {v2, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    return v3

    .line 1091
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_5

    .line 1092
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    return v3

    :cond_6
    :goto_0
    return v1
.end method

.method public final g(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->e:Ljava/util/EnumSet;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    .line 2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method final h(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    if-eq v0, v1, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "annotation"

    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lcom/pspdfkit/internal/am;->a()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    .line 159
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 161
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->h()V

    goto :goto_0

    .line 165
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->c(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 166
    invoke-direct {p0}, Lcom/pspdfkit/internal/w1;->b()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm$e;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/j1;->a(Lcom/pspdfkit/annotations/Annotation;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 172
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 174
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 175
    iput-object v1, p0, Lcom/pspdfkit/internal/w1;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 176
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x64

    invoke-static {v0, v1, p1}, Lio/reactivex/rxjava3/core/Completable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 178
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 179
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/w1$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/w1;)V

    .line 180
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/w1;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 182
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    goto :goto_0

    .line 187
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    .line 188
    invoke-virtual {p1, v2, v1}, Lcom/pspdfkit/internal/dm;->a(ZLcom/pspdfkit/internal/im$c;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public final recycle()V
    .locals 3

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/w1;->p:Z

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->l:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/w1;->g:Z

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->i:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/internal/tl;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j1;->recycle()V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->b:Lcom/pspdfkit/internal/dm;

    iget-object v1, p0, Lcom/pspdfkit/internal/w1;->o:Lcom/pspdfkit/internal/j1;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/internal/w1;->d:Lcom/pspdfkit/internal/i2;

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/i2;->b(Lcom/pspdfkit/internal/views/annotations/a;)V

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/w1;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

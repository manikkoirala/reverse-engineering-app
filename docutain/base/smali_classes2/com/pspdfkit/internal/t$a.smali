.class public final Lcom/pspdfkit/internal/t$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/pspdfkit/internal/t;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/s;

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result v1

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    if-eqz v1, :cond_0

    .line 3
    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 5
    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v1

    .line 7
    :goto_0
    sget v2, Lcom/pspdfkit/R$string;->pspdf__permission_rationale_local_access_denied_permanently:I

    .line 8
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/s;-><init>([Ljava/lang/String;I)V

    return-object v0
.end method

.method public static b()Lcom/pspdfkit/internal/t;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/s;

    const-string v1, "android.permission.RECORD_AUDIO"

    .line 2
    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    .line 3
    sget v2, Lcom/pspdfkit/R$string;->pspdf__permission_rationale_record_audio_denied_permanently:I

    .line 4
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/s;-><init>([Ljava/lang/String;I)V

    return-object v0
.end method

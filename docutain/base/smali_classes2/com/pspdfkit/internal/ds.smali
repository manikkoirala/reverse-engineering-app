.class public final Lcom/pspdfkit/internal/ds;
.super Lcom/pspdfkit/internal/y4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ds$a;
    }
.end annotation


# instance fields
.field private A:Z

.field private final B:Landroid/graphics/Rect;

.field private C:Z

.field private final s:Landroid/graphics/RectF;

.field private final t:Landroid/graphics/Path;

.field private final u:Landroid/graphics/Path;

.field private final v:Landroid/graphics/Matrix;

.field private final w:Landroid/graphics/RectF;

.field private final x:Lcom/pspdfkit/internal/ds$a;

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/internal/ds$a;)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/y4;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 3
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    .line 6
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    .line 9
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->u:Landroid/graphics/Path;

    .line 12
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->v:Landroid/graphics/Matrix;

    .line 15
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    const/high16 p1, -0x40800000    # -1.0f

    .line 22
    iput p1, p0, Lcom/pspdfkit/internal/ds;->y:F

    .line 24
    iput p1, p0, Lcom/pspdfkit/internal/ds;->z:F

    .line 29
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ds;->B:Landroid/graphics/Rect;

    const/4 p1, 0x0

    .line 34
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ds;->C:Z

    .line 48
    iput-object p6, p0, Lcom/pspdfkit/internal/ds;->x:Lcom/pspdfkit/internal/ds$a;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/ds$a;)V
    .locals 7

    .line 1
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/ds;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/internal/ds$a;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Canvas;F)V
    .locals 5

    .line 185
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/high16 v2, 0x40400000    # 3.0f

    const/16 v3, 0x9

    new-array v3, v3, [F

    .line 189
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v0, 0x0

    aget v0, v3, v0

    div-float/2addr v0, v1

    mul-float v0, v0, v2

    .line 191
    iget-object v1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    .line 192
    iget-object v2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v2

    sub-float/2addr v3, v0

    .line 194
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p3, v0

    if-eqz v0, :cond_1

    .line 198
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 199
    invoke-virtual {v0, p3, p3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 200
    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 203
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {p2, p1, v1, v3, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 205
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, p1, v1, v3, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/high16 v3, 0x40e00000    # 7.0f

    const/16 v4, 0x9

    new-array v4, v4, [F

    .line 8
    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->getValues([F)V

    aget p1, v4, v1

    div-float/2addr p1, v2

    mul-float p1, p1, v3

    .line 10
    iget v2, p0, Lcom/pspdfkit/internal/y4;->q:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v2, v2, v3

    mul-float p1, p1, v3

    add-float/2addr p1, v2

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v2, p1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    .line 12
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v2, p1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result p1

    int-to-float p1, p1

    cmpl-float p1, v2, p1

    if-lez p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ds;->A:Z

    return-void
.end method

.method protected final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 10

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_13

    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    goto/16 :goto_4

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/y4;->q:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v0, v2, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    sub-float/2addr v4, v2

    invoke-virtual {v0, v4, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v4, v2

    invoke-virtual {v0, v1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->x:Lcom/pspdfkit/internal/ds$a;

    sget-object v4, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v4, :cond_4

    .line 52
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    iget v1, p0, Lcom/pspdfkit/internal/y4;->r:F

    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    invoke-static {v0, v1, v3}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/RectF;FLandroid/graphics/Path;)V

    goto :goto_0

    .line 56
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    .line 57
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v3

    iget-object v4, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v4, v3

    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    .line 58
    invoke-static {v0, v1, v4, v3}, Lcom/pspdfkit/internal/fq;->a(Landroid/graphics/RectF;FFLandroid/graphics/Path;)V

    goto :goto_0

    .line 61
    :cond_4
    sget-object v3, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    if-ne v0, v3, :cond_12

    .line 62
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    iget v1, p0, Lcom/pspdfkit/internal/y4;->r:F

    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    const-string v4, "rect"

    .line 64
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v4, 0x40880000    # 4.25f

    mul-float v4, v4, v1

    .line 117
    invoke-virtual {v0, v4, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 120
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 121
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v0, Landroid/graphics/RectF;->top:F

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v0, Landroid/graphics/RectF;->top:F

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v5, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    .line 125
    invoke-static {v4, v1, v3, v0}, Lcom/pspdfkit/internal/k5;->a(Ljava/util/ArrayList;FLandroid/graphics/Path;Z)Landroid/graphics/Path;

    goto :goto_0

    .line 126
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->w:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    invoke-static {v0, v1, v1, v3}, Lcom/pspdfkit/internal/fq;->a(Landroid/graphics/RectF;FFLandroid/graphics/Path;)V

    .line 132
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 135
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_7

    .line 136
    iget-object v1, p0, Lcom/pspdfkit/internal/ds;->B:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 137
    iget-object v1, p0, Lcom/pspdfkit/internal/ds;->B:Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    int-to-float v5, v3

    iget v3, v1, Landroid/graphics/Rect;->top:I

    int-to-float v6, v3

    iget v3, v1, Landroid/graphics/Rect;->right:I

    int-to-float v7, v3

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v1

    const/4 v9, 0x0

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    goto :goto_1

    .line 139
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v6, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v7, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    :cond_7
    :goto_1
    const/4 v1, 0x0

    cmpl-float v3, p4, v2

    if-eqz v3, :cond_b

    .line 145
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->v:Landroid/graphics/Matrix;

    invoke-virtual {v3, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 146
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/pspdfkit/internal/ds;->u:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/pspdfkit/internal/ds;->v:Landroid/graphics/Matrix;

    .line 147
    invoke-virtual {v4, v3}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 148
    invoke-virtual {v4, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    if-eqz p3, :cond_8

    .line 149
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->u:Landroid/graphics/Path;

    invoke-virtual {p1, v3, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 151
    :cond_8
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->u:Landroid/graphics/Path;

    .line 152
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->t()Z

    move-result v4

    if-nez v4, :cond_9

    goto :goto_2

    :cond_9
    if-eqz p3, :cond_a

    .line 155
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p3

    cmpg-float p3, p3, v2

    if-gez p3, :cond_a

    .line 158
    new-instance p3, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p3, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 159
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 160
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 162
    :cond_a
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_b
    if-eqz p3, :cond_c

    .line 163
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    invoke-virtual {p1, v3, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 165
    :cond_c
    iget-object v3, p0, Lcom/pspdfkit/internal/ds;->t:Landroid/graphics/Path;

    .line 166
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->t()Z

    move-result v4

    if-nez v4, :cond_d

    goto :goto_2

    :cond_d
    if-eqz p3, :cond_e

    .line 169
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p3

    cmpg-float p3, p3, v2

    if-gez p3, :cond_e

    .line 172
    new-instance p3, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p3, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 173
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 174
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 176
    :cond_e
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 177
    :goto_2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 178
    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-eqz p2, :cond_11

    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz p2, :cond_11

    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    if-nez p2, :cond_f

    goto :goto_3

    .line 180
    :cond_f
    iget-boolean p3, p0, Lcom/pspdfkit/internal/ds;->C:Z

    if-eqz p3, :cond_10

    .line 181
    invoke-direct {p0, p2, p1, p4}, Lcom/pspdfkit/internal/ds;->a(Ljava/lang/String;Landroid/graphics/Canvas;F)V

    goto :goto_3

    :cond_10
    const-string p2, ""

    .line 183
    invoke-direct {p0, p2, p1, p4}, Lcom/pspdfkit/internal/ds;->a(Ljava/lang/String;Landroid/graphics/Canvas;F)V

    :cond_11
    :goto_3
    return-void

    .line 184
    :cond_12
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Shape type is not implemented: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, p0, Lcom/pspdfkit/internal/ds;->x:Lcom/pspdfkit/internal/ds$a;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_13
    :goto_4
    return-void
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 4

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ds;->y:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/pspdfkit/internal/ds;->z:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    const/16 v0, 0x9

    new-array v1, v0, [F

    .line 6
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v2, 0x0

    aget v1, v1, v2

    const/high16 v3, 0x42000000    # 32.0f

    mul-float v1, v1, v3

    div-float/2addr v1, p3

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v3

    new-array v0, v0, [F

    .line 10
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    aget p2, v0, v2

    mul-float v3, v3, p2

    div-float/2addr v3, p3

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->u()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 14
    iget p2, p0, Lcom/pspdfkit/internal/y4;->r:F

    const/high16 p3, 0x40880000    # 4.25f

    mul-float p2, p2, p3

    add-float/2addr v3, p2

    :cond_1
    const/high16 p2, 0x40000000    # 2.0f

    mul-float v3, v3, p2

    .line 16
    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result p2

    .line 17
    iget p3, p1, Landroid/graphics/PointF;->x:F

    iget v0, p0, Lcom/pspdfkit/internal/ds;->y:F

    sub-float/2addr p3, v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p3

    const/4 v0, 0x1

    cmpl-float p3, p3, p2

    if-ltz p3, :cond_2

    iget p3, p1, Landroid/graphics/PointF;->y:F

    iget v1, p0, Lcom/pspdfkit/internal/ds;->z:F

    sub-float/2addr p3, v1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p3

    cmpl-float p3, p3, p2

    if-ltz p3, :cond_2

    .line 18
    iget-object p2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget p3, p0, Lcom/pspdfkit/internal/ds;->y:F

    iget v1, p0, Lcom/pspdfkit/internal/ds;->z:F

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, p3, v1, v2, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 20
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ds;->A:Z

    goto :goto_0

    .line 21
    :cond_2
    iget-boolean p3, p0, Lcom/pspdfkit/internal/ds;->A:Z

    if-nez p3, :cond_3

    .line 22
    iget p3, p1, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lcom/pspdfkit/internal/ds;->y:F

    sub-float/2addr p3, v1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr p3, v1

    .line 23
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget v1, p0, Lcom/pspdfkit/internal/ds;->z:F

    sub-float/2addr p1, v1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr p1, v1

    .line 24
    iget-object v1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/ds;->y:F

    iget v3, p0, Lcom/pspdfkit/internal/ds;->z:F

    mul-float p3, p3, p2

    add-float/2addr p3, v2

    mul-float p1, p1, p2

    add-float/2addr p1, v3

    invoke-virtual {v1, v2, v3, p3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 26
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ds;->A:Z

    .line 29
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ds;->o()V

    goto :goto_2

    .line 30
    :cond_4
    :goto_1
    iget p2, p1, Landroid/graphics/PointF;->x:F

    iput p2, p0, Lcom/pspdfkit/internal/ds;->y:F

    .line 31
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iput p1, p0, Lcom/pspdfkit/internal/ds;->z:F

    :goto_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 206
    iput-object p1, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    .line 207
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz v0, :cond_0

    .line 209
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/li;->a(Ljava/lang/String;Lcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object p1

    .line 210
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ds;->b(Ljava/lang/String;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 211
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ds;->C:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ds;->A:Z

    return v0
.end method

.method public final o()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    goto/16 :goto_3

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->x:Lcom/pspdfkit/internal/ds$a;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    goto/16 :goto_1

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    const/16 v1, 0x9

    new-array v1, v1, [F

    .line 8
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    aget v0, v1, v3

    .line 10
    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    div-float/2addr v0, v1

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v0

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v0

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;FF)Lcom/pspdfkit/internal/mi;

    move-result-object v2

    goto/16 :goto_1

    :cond_2
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    .line 18
    new-instance v4, Landroid/graphics/PointF;

    iget-object v5, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v4, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v0, v3

    new-instance v4, Landroid/graphics/PointF;

    iget-object v5, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v4, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v0, v1

    new-instance v1, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget v5, v4, Landroid/graphics/RectF;->right:F

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x2

    aput-object v1, v0, v4

    new-instance v1, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    iget v5, v4, Landroid/graphics/RectF;->left:F

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x3

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    iget v4, p0, Lcom/pspdfkit/internal/h4;->b:F

    iget-object v5, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    const-string v6, "measurementProperties"

    .line 24
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "viewPoints"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "viewPointsToPdfPointsTransformationMatrix"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_1

    .line 185
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 187
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 188
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    .line 190
    iget v8, v6, Landroid/graphics/PointF;->x:F

    mul-float v8, v8, v4

    iget v6, v6, Landroid/graphics/PointF;->y:F

    mul-float v6, v6, v4

    invoke-virtual {v7, v8, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 192
    invoke-static {v7, v5}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    const-string v6, "getUnscaledViewPointAsPd\u2026      pageScale\n        )"

    .line 193
    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    :cond_4
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;Ljava/util/List;)Lcom/pspdfkit/internal/mi;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_6

    .line 202
    invoke-virtual {v2}, Lcom/pspdfkit/internal/mi;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    .line 203
    invoke-virtual {v2}, Lcom/pspdfkit/internal/mi;->b()F

    move-result v0

    .line 204
    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-nez v1, :cond_5

    goto :goto_2

    .line 206
    :cond_5
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/li;->a(FLcom/pspdfkit/internal/ri;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/ds;->b(Ljava/lang/String;)Z

    move-result v3

    .line 208
    :goto_2
    iput-boolean v3, p0, Lcom/pspdfkit/internal/ds;->C:Z

    :cond_6
    :goto_3
    return-void
.end method

.method public final v()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->s:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final w()Lcom/pspdfkit/internal/ds$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ds;->x:Lcom/pspdfkit/internal/ds$a;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/xm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/xm$b;,
        Lcom/pspdfkit/internal/xm$c;,
        Lcom/pspdfkit/internal/xm$d;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/ui/PdfFragment;

.field private b:I

.field private final c:Lcom/pspdfkit/internal/fl;

.field private final d:Lcom/pspdfkit/internal/pr;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Lcom/pspdfkit/internal/i2;

.field private g:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

.field private final h:Lcom/pspdfkit/ui/audio/AudioModeManager;

.field private i:Landroid/widget/FrameLayout;

.field private j:Lcom/pspdfkit/internal/oh;

.field private k:Landroid/widget/ImageView;

.field l:Lcom/pspdfkit/internal/zg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/zg<",
            "Lcom/pspdfkit/internal/xm$b;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/pspdfkit/internal/zg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/zg<",
            "Lcom/pspdfkit/internal/views/document/DocumentView;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private o:Z

.field private p:Lio/reactivex/rxjava3/disposables/Disposable;

.field private q:Lcom/pspdfkit/internal/do;

.field private r:Lcom/pspdfkit/internal/l1;

.field private s:Lcom/pspdfkit/internal/n6;


# direct methods
.method public static synthetic $r8$lambda$6euulLcfRI_4a2NzXqCJSqDf9Fw(Lcom/pspdfkit/internal/xm$c;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public static synthetic $r8$lambda$7N8mz3QSk0BB1BaZx7D-T-s1qVI(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public static synthetic $r8$lambda$BpPmwXrxgy8rtwDPMCICp740oJo(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/xm;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$GvwkhyL3CHlvgYzCJObWJ6eh5ZQ(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/xm;->c(ZLcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SDJ7puuGQh1YloXUnJ3quK5ElmQ(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/xm;->b(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public static synthetic $r8$lambda$UzuJuKyVnvB6sPebR6rjfD1kz_Q(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_znsBQ3xSP4fEEV-NXNDnQF6YoY(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/xm;->a(ZLcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fA3hp6MdXLY6WbBEikynka1VIRc(Lcom/pspdfkit/internal/xm;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/xm;->y()V

    return-void
.end method

.method public static synthetic $r8$lambda$g1Vp3s8-f4i5DMnoDFpaYAGjZeA(Lcom/pspdfkit/internal/xm;ILcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(ILcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public static synthetic $r8$lambda$g6z1CgmlsUD3HVTCwZ7pEcfXxVU(Lcom/pspdfkit/internal/xm;ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/xm;->b(ZLcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method public static synthetic $r8$lambda$gH2CQhN2To-VjtYsYyoIDSYKyg0(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iP0wIbBVi025A1Kg0LRSjj0Plhw(Lcom/pspdfkit/internal/xm;Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method public static synthetic $r8$lambda$kWbu4q5jthNJPECPC4dpNlkdJJk(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wJNZSB1znhwS_ObO_BqhfA-UYEo(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/xm;->d(ZLcom/pspdfkit/internal/xm$b;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fputo(Lcom/pspdfkit/internal/xm;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/xm;->o:Z

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/a3;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/xm;->b:I

    .line 33
    new-instance v0, Lcom/pspdfkit/internal/zg;

    invoke-direct {v0}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/zg;

    invoke-direct {v0}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    .line 75
    iput-object p1, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 76
    iput-object p2, p0, Lcom/pspdfkit/internal/xm;->c:Lcom/pspdfkit/internal/fl;

    .line 77
    iput-object p3, p0, Lcom/pspdfkit/internal/xm;->d:Lcom/pspdfkit/internal/pr;

    .line 78
    iput-object p4, p0, Lcom/pspdfkit/internal/xm;->h:Lcom/pspdfkit/ui/audio/AudioModeManager;

    return-void
.end method

.method private synthetic a(ILcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 167
    iget-object p2, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    if-eqz p2, :cond_0

    .line 168
    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 169
    iput p1, p0, Lcom/pspdfkit/internal/xm;->b:I

    :cond_0
    return-void
.end method

.method private synthetic a(Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/internal/xm$b;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/internal/xm$b;-><init>(Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Ljava/lang/Object;)V

    .line 146
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/pspdfkit/internal/do;->c()V

    const/4 p1, 0x0

    .line 148
    iput-object p1, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zg;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/content/Context;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 91
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/pspdfkit/ui/fonts/Font;

    .line 96
    iget-object v2, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v3, p0, Lcom/pspdfkit/internal/xm;->c:Lcom/pspdfkit/internal/fl;

    iget-object v4, p0, Lcom/pspdfkit/internal/xm;->d:Lcom/pspdfkit/internal/pr;

    .line 100
    monitor-enter p0

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/pspdfkit/internal/j2;

    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 103
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v5}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v6

    invoke-direct {v0, v1, v6, v5}, Lcom/pspdfkit/internal/j2;-><init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/PdfFragment;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    .line 104
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->g:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V

    .line 106
    :cond_0
    iget-object v5, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 107
    iget-object v6, p0, Lcom/pspdfkit/internal/xm;->h:Lcom/pspdfkit/ui/audio/AudioModeManager;

    .line 109
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/xm;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/l1;

    move-result-object v7

    new-instance v9, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda11;

    invoke-direct {v9, p0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    move-object v1, p1

    .line 110
    invoke-virtual/range {v1 .. v9}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/i2;Lcom/pspdfkit/ui/audio/AudioModeManager;Lcom/pspdfkit/internal/l1;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/views/document/DocumentView$f;)V

    .line 128
    iget-object p2, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->setDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 129
    iget-object p2, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->setDocumentScrollListener(Lcom/pspdfkit/listeners/scrolling/DocumentScrollListener;)V

    .line 132
    new-instance p2, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda12;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentInteractionListener(Lcom/pspdfkit/internal/views/document/DocumentView$e;)V

    .line 141
    new-instance p2, Lcom/pspdfkit/internal/xm$a;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/internal/xm$a;-><init>(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Lcom/pspdfkit/internal/views/document/DocumentView$g;)V

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private synthetic a(Lcom/pspdfkit/internal/xm$b;)V
    .locals 0

    .line 163
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    if-eqz p1, :cond_0

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-static {p1}, Landroidx/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 165
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/oh;->c()V

    const/4 p1, 0x0

    .line 166
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/xm;->c(Z)V

    :cond_0
    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/xm$c;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 160
    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/xm$c;->a(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/xm$b;)V
    .locals 1

    .line 193
    iget-object p2, p2, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/ui/PdfFragment;)V

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Can\'t initialize fragment contents"

    .line 149
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private static a(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 5

    .line 196
    invoke-static {p1}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetc(Lcom/pspdfkit/internal/xm$b;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p1, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/pspdfkit/R$layout;->pspdf__pdf_fragment_error_view:I

    iget-object v4, p1, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    .line 198
    invoke-virtual {v0, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fputc(Lcom/pspdfkit/internal/xm$b;Landroid/view/View;)V

    .line 199
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 201
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetc(Lcom/pspdfkit/internal/xm$b;)Landroid/view/View;

    move-result-object v0

    if-eqz p0, :cond_2

    .line 202
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-nez p0, :cond_1

    .line 203
    iget-object p0, p1, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 205
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object p0, p1, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 208
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/xm;->c(Z)V

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setOnDocumentInteractionListener(Lcom/pspdfkit/internal/views/document/DocumentView$e;)V

    return-void
.end method

.method private b(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 5

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    return-void

    .line 56
    :cond_0
    invoke-static {p2}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    const/16 v1, 0x8

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/pspdfkit/ui/PdfPasswordView;

    iget-object v2, p2, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/pspdfkit/ui/PdfPasswordView;-><init>(Landroid/content/Context;)V

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fputd(Lcom/pspdfkit/internal/xm$b;Lcom/pspdfkit/ui/PdfPasswordView;)V

    .line 58
    sget v2, Lcom/pspdfkit/R$id;->pspdf__fragment_password_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 59
    invoke-static {p2}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    const/4 v4, -0x2

    invoke-direct {v2, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    invoke-static {p2}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfPasswordView;->setVisibility(I)V

    .line 63
    :cond_1
    invoke-static {p2}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    if-eqz p1, :cond_3

    .line 64
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-nez p1, :cond_2

    .line 65
    iget-object p1, p2, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_2
    const/4 p1, 0x0

    .line 67
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfPasswordView;->setVisibility(I)V

    goto :goto_0

    .line 69
    :cond_3
    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 70
    iget-object p1, p2, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 71
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfPasswordView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private static synthetic c(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    .line 8
    iget-object p1, p1, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setScrollingEnabled(Z)V

    return-void
.end method

.method private static synthetic d(ZLcom/pspdfkit/internal/xm$b;)V
    .locals 0

    .line 5
    iget-object p1, p1, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/views/document/DocumentView;->setZoomingEnabled(Z)V

    return-void
.end method

.method private synthetic y()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/oh;->e()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/oh;->getProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    sget v1, Lcom/pspdfkit/R$id;->pspdf__fragment_progressbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    :cond_0
    return-void
.end method

.method public final a(I)F
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    return p1

    .line 172
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->d(I)F

    move-result p1

    return p1
.end method

.method public final a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final a(Landroid/view/LayoutInflater;)Landroid/widget/FrameLayout;
    .locals 11

    .line 60
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    .line 66
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__document_view:I

    const/4 v3, 0x0

    .line 67
    invoke-virtual {p1, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 68
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 69
    iput-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 70
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getLoadingProgressDrawable()Ljava/lang/Integer;

    move-result-object v9

    .line 73
    new-instance v10, Lcom/pspdfkit/internal/oh;

    .line 74
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 76
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;)I

    move-result v6

    .line 77
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v7

    .line 78
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v8

    move-object v3, v10

    move-object v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/oh;-><init>(Landroid/content/Context;Ljava/lang/Integer;IZZ)V

    iput-object v10, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    .line 80
    sget p1, Lcom/pspdfkit/R$id;->pspdf__fragment_loading_view:I

    invoke-virtual {v10, p1}, Landroid/view/View;->setId(I)V

    .line 81
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/oh;->getThrobber()Landroid/widget/ProgressBar;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$id;->pspdf__fragment_throbber:I

    invoke-virtual {p1, v1}, Landroid/view/View;->setId(I)V

    if-nez v9, :cond_0

    .line 83
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    invoke-virtual {v0, p1, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 87
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__loading_view_background_color:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_loading_view_default:I

    .line 88
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 89
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->e:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    .line 90
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/xm;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-object v0
.end method

.method public final a(Landroid/content/Context;)Lcom/pspdfkit/internal/l1;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->r:Lcom/pspdfkit/internal/l1;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/pspdfkit/internal/l1;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/l1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->r:Lcom/pspdfkit/internal/l1;

    .line 153
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->r:Lcom/pspdfkit/internal/l1;

    return-object p1
.end method

.method public final declared-synchronized a(Z)Lcom/pspdfkit/internal/views/document/DocumentView;
    .locals 1

    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/pspdfkit/internal/xm;->z()V

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(D)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/oh;->setLoadingProgress(D)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .line 178
    iput-object p1, p0, Lcom/pspdfkit/internal/xm;->e:Landroid/graphics/drawable/Drawable;

    .line 179
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->k:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->k:Landroid/widget/ImageView;

    .line 182
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->k:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/internal/se;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "annotationViewsFactory"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationViewsFactory"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    .line 57
    iput-object p1, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->g:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 59
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Custom annotation views factory must be injected before calling createViews()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/xm$c;Z)V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/xm$c;)V

    invoke-virtual {v0, v1, p2}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/xm$d;Z)V
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/xm$d;)V

    invoke-virtual {v0, v1, p2}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 194
    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/zf;)V

    const/4 p1, 0x0

    .line 195
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/PdfPasswordView;)V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->c()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xm$b;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fputd(Lcom/pspdfkit/internal/xm$b;Lcom/pspdfkit/ui/PdfPasswordView;)V

    return-void
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V
    .locals 1

    monitor-enter p0

    .line 190
    :try_start_0
    iput-object p1, p0, Lcom/pspdfkit/internal/xm;->g:Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;

    .line 191
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    if-eqz v0, :cond_0

    .line 192
    check-cast v0, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j2;->a(Lcom/pspdfkit/ui/rendering/AnnotationOverlayRenderStrategy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;)V"
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->setMediaContentStates(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a(Landroid/graphics/RectF;I)Z
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Landroid/graphics/RectF;I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b(Landroid/content/Context;)Lcom/pspdfkit/internal/n6;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->s:Lcom/pspdfkit/internal/n6;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/pspdfkit/internal/n6;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/n6;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->s:Lcom/pspdfkit/internal/n6;

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/xm;->s:Lcom/pspdfkit/internal/n6;

    return-object p1
.end method

.method public final b(I)Lcom/pspdfkit/internal/tb;
    .locals 2

    const/4 v0, 0x0

    if-gez p1, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    goto :goto_1

    :cond_2
    :goto_0
    move-object p1, v0

    :goto_1
    if-nez p1, :cond_3

    return-object v0

    .line 52
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getFormEditor()Lcom/pspdfkit/internal/tb;

    move-result-object p1

    return-object p1
.end method

.method public final b()V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/do;->a()V

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zg;->a()V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zg;->a()V

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v1, :cond_1

    .line 20
    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->u()V

    .line 22
    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->d()V

    .line 25
    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->t()V

    .line 26
    :cond_1
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    if-eqz v1, :cond_2

    .line 29
    check-cast v1, Lcom/pspdfkit/internal/j2;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/j2;->e()V

    .line 30
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->f:Lcom/pspdfkit/internal/i2;

    .line 34
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 35
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 36
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    .line 40
    :cond_3
    new-instance v1, Lcom/pspdfkit/internal/zg;

    invoke-direct {v1}, Lcom/pspdfkit/internal/zg;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    .line 43
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    .line 44
    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->k:Landroid/widget/ImageView;

    const/4 v0, 0x0

    .line 45
    iput-boolean v0, p0, Lcom/pspdfkit/internal/xm;->o:Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/pspdfkit/internal/xm;->s()Z

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda4;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda4;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public final c(I)Lcom/pspdfkit/internal/am;
    .locals 2

    const/4 v0, 0x0

    if-gez p1, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 6
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    goto :goto_1

    :cond_2
    :goto_0
    move-object p1, v0

    :goto_1
    if-nez p1, :cond_3

    return-object v0

    .line 7
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/oh;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public final d(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 2
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->c(I)I

    move-result p1

    return p1
.end method

.method public final d()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getActiveAnnotationToolVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    return-object v0
.end method

.method public final d(Z)V
    .locals 2

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/xm;->v()Z

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    new-instance v1, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda14;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/xm;Z)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;)V

    return-void
.end method

.method public final e()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/xm;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$color;->pspdf__color_gray_light:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    :goto_0
    return v0
.end method

.method public final e(I)V
    .locals 1

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda10;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/xm;I)V

    const/4 p1, 0x0

    .line 5
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$c;Z)V

    return-void
.end method

.method public final e(Z)V
    .locals 1

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda3;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda3;-><init>(Z)V

    const/4 p1, 0x0

    .line 7
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public final f()Lcom/pspdfkit/internal/w6;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getContentEditingState()Lcom/pspdfkit/internal/w6;

    move-result-object v0

    return-object v0
.end method

.method public final f(Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda13;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda13;-><init>(Z)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public final g()D
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->j:Lcom/pspdfkit/internal/oh;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/oh;->getLoadingProgress()D

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0
.end method

.method public final h()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/internal/views/document/DocumentView;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->c()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getMediaContentStates()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0
.end method

.method public final j()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result v0

    return v0
.end method

.method public final k()Lcom/pspdfkit/ui/PdfPasswordView;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->c()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xm$b;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3
    new-instance v1, Lcom/pspdfkit/ui/PdfPasswordView;

    iget-object v2, v0, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/ui/PdfPasswordView;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fputd(Lcom/pspdfkit/internal/xm$b;Lcom/pspdfkit/ui/PdfPasswordView;)V

    .line 4
    sget v2, Lcom/pspdfkit/R$id;->pspdf__fragment_password_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v1

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    const/4 v4, -0x2

    invoke-direct {v2, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/PdfPasswordView;->setVisibility(I)V

    .line 9
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getSelectedAnnotations()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final m()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getSelectedFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final n()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getTextSelection()Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/pspdfkit/internal/ug$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getViewState()Lcom/pspdfkit/internal/ug$a;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getVisiblePages()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0
.end method

.method public final q()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->m:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->e()Z

    move-result v0

    return v0
.end method

.method public final r()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/xm;)V

    const/4 v1, 0x0

    .line 2
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/xm;->a(Lcom/pspdfkit/internal/xm$d;Z)V

    return-void
.end method

.method public final s()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->e()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xm$b;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetc(Lcom/pspdfkit/internal/xm$b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final t()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final u()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/xm;->o:Z

    return v0
.end method

.method public final v()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->e()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/xm$b;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/xm$b;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final w()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final x()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final z()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xm;->n:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/xm;->i:Landroid/widget/FrameLayout;

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/xm;->l:Lcom/pspdfkit/internal/zg;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zg;->e()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/pspdfkit/internal/xm;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "pspdfkit-fragment-initialization"

    const-string v4, "threadName"

    .line 11
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v4, Lcom/pspdfkit/internal/do;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Lcom/pspdfkit/internal/do;-><init>(Ljava/lang/String;I)V

    .line 99
    iput-object v4, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    .line 100
    new-instance v3, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda6;

    invoke-direct {v3, p0, v0, v2}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/xm;Lcom/pspdfkit/internal/views/document/DocumentView;Landroid/content/Context;)V

    invoke-static {v3}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/xm;->q:Lcom/pspdfkit/internal/do;

    const/4 v4, 0x5

    .line 154
    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v2

    .line 155
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda7;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/xm;)V

    .line 156
    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Completable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda8;

    invoke-direct {v3, p0, v1, v0}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/xm;Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    new-instance v0, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda9;

    invoke-direct {v0}, Lcom/pspdfkit/internal/xm$$ExternalSyntheticLambda9;-><init>()V

    .line 157
    invoke-virtual {v2, v3, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/xm;->p:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method

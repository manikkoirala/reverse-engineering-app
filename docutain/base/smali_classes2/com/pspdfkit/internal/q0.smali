.class public final Lcom/pspdfkit/internal/q0;
.super Lcom/pspdfkit/internal/b1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/q0$a;
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

.field private e:Lio/reactivex/rxjava3/disposables/Disposable;

.field private f:I

.field private g:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;


# direct methods
.method public static synthetic $r8$lambda$-rF8y0IDms6cuk_TJZCOXB_nbvY(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.method public static synthetic $r8$lambda$-v8RJInOwkN5N3gW1dNSSmvlqZs(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$2l8dCDfWIGskCFBssq8O1a7F8Pc(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$6DhQMD3b-0WYp9wWRZrCCWJyFiM(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public static synthetic $r8$lambda$93QokTBYqlTUn3jTZ6i89yKh9B0(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->c(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$FAtvvT1-eyUnKmRrqwzc7DDiM5M(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->e(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$GgAsjS6BASPG3wJWWcCIo37_Ne4(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->d(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$He5miANz3BNSGFYQPBjpmgYA5HE(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V

    return-void
.end method

.method public static synthetic $r8$lambda$IqAWtAfbKc6tVI3ocvhMBpsAfBA(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$O1fDTon5kGoL78CQV-2-iOnzNCM(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Okm_HQEp6uWfLAMdjHJnpwbUmCk(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->c(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$TR0qjGYWzfei4j7eddGdP3jV1Rs(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$V4TbHieGl0ALPURCKafHaO0Ec2Q(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method public static synthetic $r8$lambda$hfOL-dBoRSDkWJmiWJkMzk-20Ws(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void
.end method

.method public static synthetic $r8$lambda$hxtAApX_leb3-m70cBc09Z60Bt0(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public static synthetic $r8$lambda$os3vYJlfOZ7msNXbbBPGWXHGrdw(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wBeI7CpiOV3dnaDSUfGxJb_sjEE(Lcom/pspdfkit/internal/q0;Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$yT1dkzMR1zOcoYPvMfnngeEFClU(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$yW208aAKHar1aVJunw4e_iDAjHg(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;)V
    .locals 4

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "controller.fragment.requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    const-string v2, "controller.fragment.annotationPreferences"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    const-string v3, "controller.fragment.annotationConfiguration"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/b1;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    const/4 p1, 0x1

    .line 908
    iput p1, p0, Lcom/pspdfkit/internal/q0;->f:I

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/q0;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;
    .locals 0

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    return-object p0
.end method

.method private final a(I)V
    .locals 2

    .line 4122
    iget v0, p0, Lcom/pspdfkit/internal/q0;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4123
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 4124
    iput p1, p0, Lcom/pspdfkit/internal/q0;->f:I

    goto :goto_0

    :cond_0
    if-eq v0, p1, :cond_1

    .line 4126
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 4127
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 4128
    iput p1, p0, Lcom/pspdfkit/internal/q0;->f:I

    .line 4130
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 4131
    :cond_2
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x12c

    invoke-static {v0, v1, p1}, Lio/reactivex/rxjava3/core/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 4132
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 4133
    new-instance v0, Lcom/pspdfkit/internal/t0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/t0;-><init>(Lcom/pspdfkit/internal/q0;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 4134
    iput-object p1, p0, Lcom/pspdfkit/internal/q0;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 4

    .line 4103
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotations(I)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 4104
    iget-object v2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getZIndex(Lcom/pspdfkit/annotations/Annotation;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    const/4 p1, 0x1

    const/4 v2, 0x0

    if-nez v1, :cond_3

    goto :goto_2

    .line 4107
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_5

    const/4 p1, 0x0

    :cond_4
    :goto_1
    const/4 v1, 0x1

    goto :goto_3

    :cond_5
    :goto_2
    add-int/lit8 v3, v0, -0x1

    if-nez v1, :cond_6

    goto :goto_1

    .line 4109
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_4

    const/4 v1, 0x0

    :goto_3
    const/4 v3, 0x2

    if-ge v0, v3, :cond_7

    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    move v2, p1

    .line 4118
    :goto_4
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->g:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;

    if-eqz p1, :cond_9

    .line 4119
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->enableAllMovements()V

    if-nez v2, :cond_8

    .line 4120
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->disableBackwardMovements()V

    :cond_8
    if-nez v1, :cond_9

    .line 4121
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;->disableForwardMovements()V

    :cond_9
    return-void
.end method

.method private static final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 7

    const-string p2, "$annotation"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "value"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3034
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v0

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "change_property_in_inspector"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v0, v1, :cond_0

    .line 3036
    invoke-direct {p1}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3037
    iget-object v0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3038
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderStyle(Lcom/pspdfkit/annotations/BorderStyle;)V

    .line 3039
    iget-object v0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3040
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    .line 3041
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3042
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    const-string v1, "border_style"

    .line 3043
    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3044
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3045
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 3047
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object v6

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 3049
    :cond_2
    invoke-direct {p1}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3050
    iget-object v0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3051
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object v0

    .line 3052
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderDashArray(Ljava/util/List;)V

    .line 3053
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3054
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    .line 3055
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 3056
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    const-string v6, "border_dash_array"

    .line 3057
    invoke-virtual {v1, v2, v6}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    if-eqz v0, :cond_3

    new-array v6, v4, [Ljava/lang/Integer;

    .line 3681
    invoke-interface {v0, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    const-string v6, ","

    .line 3682
    invoke-static {v6, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v0, "null"

    .line 3685
    :goto_1
    invoke-virtual {v1, p2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3693
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    const/4 v0, 0x1

    .line 3695
    :cond_4
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v1

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v6

    if-ne v1, v6, :cond_6

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result v1

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v6

    cmpg-float v1, v1, v6

    if-nez v1, :cond_5

    const/4 v4, 0x1

    :cond_5
    if-nez v4, :cond_7

    .line 3697
    :cond_6
    invoke-direct {p1}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3698
    iget-object v0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3699
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffect(Lcom/pspdfkit/annotations/BorderEffect;)V

    .line 3700
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffectIntensity(F)V

    .line 3701
    iget-object v0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3702
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    .line 3703
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3704
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    const-string v1, "border_effect"

    .line 3705
    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 3706
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    .line 3707
    invoke-virtual {p2}, Lcom/pspdfkit/internal/q$a;->a()V

    const/4 v0, 0x1

    .line 3709
    :cond_7
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p0

    .line 3710
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object p2

    .line 3711
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/BorderStyle;->DASHED:Lcom/pspdfkit/annotations/BorderStyle;

    if-ne v1, v2, :cond_8

    if-eqz p2, :cond_8

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v5

    if-eqz v1, :cond_8

    .line 3712
    invoke-virtual {p1}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    .line 3714
    iget-object v2, p0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3715
    iget-object p0, p0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 3716
    new-instance v3, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p3

    invoke-direct {v3, p3, p2}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;Ljava/util/List;)V

    .line 3717
    invoke-interface {v1, v2, p0, v3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    goto :goto_2

    .line 3723
    :cond_8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p2

    .line 3725
    iget-object v1, p0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3726
    iget-object p0, p0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 3727
    new-instance v2, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p3

    invoke-direct {v2, p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;)V

    .line 3728
    invoke-interface {p2, v1, p0, v2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    :goto_2
    if-eqz v0, :cond_9

    .line 3735
    iget-object p0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    :cond_9
    return-void
.end method

.method private static final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 5

    const-string p2, "$annotation"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "value"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3736
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v0

    .line 3737
    invoke-direct {p1}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3738
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    if-eqz v0, :cond_0

    .line 3739
    iget-object v1, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    const-string v2, "lineEnds12.second"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-static {p0, p3, v1}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3740
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v1

    .line 3741
    invoke-virtual {p1}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v2

    .line 3743
    iget-object v3, v1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3744
    iget-object v1, v1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 3746
    iget-object v4, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/pspdfkit/annotations/LineEndType;

    .line 3747
    invoke-interface {v2, v3, v1, p3, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 3753
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3754
    iget-object p1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 3755
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v1, "change_property_in_inspector"

    .line 3756
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 3757
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string v1, "line_ends"

    .line 3758
    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3761
    sget-object p1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 p1, 0x2

    new-array v1, p1, [Ljava/lang/Object;

    .line 3763
    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p3

    const/4 v2, 0x0

    aput-object p3, v1, v2

    .line 3764
    iget-object p3, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p3, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p3

    const/4 v0, 0x1

    aput-object p3, v1, v0

    .line 3765
    invoke-static {v1, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string p3, "%s,%s"

    invoke-static {p3, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "format(format, *args)"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3766
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3774
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_0

    .line 3776
    :cond_0
    iget-object p0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    :goto_0
    return-void
.end method

.method private final a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/util/ArrayList;)V
    .locals 7

    .line 3826
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getMeasurementProperties()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 3828
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    .line 3829
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v3, 0x1

    :goto_1
    const/4 v4, 0x0

    if-nez v3, :cond_8

    .line 3830
    sget-object v3, Lcom/pspdfkit/internal/q0$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v3, v3, v5

    const/4 v5, 0x2

    if-eq v3, v2, :cond_4

    if-eq v3, v5, :cond_3

    const/4 v6, 0x3

    if-eq v3, v6, :cond_3

    const/4 v6, 0x4

    if-eq v3, v6, :cond_3

    .line 3835
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_2

    .line 3836
    :cond_3
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->AREA:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    goto :goto_2

    .line 3837
    :cond_4
    sget-object v3, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->PERIMETER:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    .line 3844
    :goto_2
    sget-object v6, Lcom/pspdfkit/annotations/measurements/MeasurementMode;->DISTANCE:Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    if-ne v3, v6, :cond_5

    move-object v3, v4

    goto :goto_4

    .line 3845
    :cond_5
    sget-object v6, Lcom/pspdfkit/internal/q0$a;->b:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v6, v3

    if-eq v3, v2, :cond_7

    if-eq v3, v5, :cond_6

    const-string v2, ""

    goto :goto_3

    .line 3852
    :cond_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__annotation_type_measure_perimeter:I

    .line 3853
    invoke-static {v2, v3, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(context, R.str\u2026n_type_measure_perimeter)"

    .line 3854
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v2, "Area"

    .line 3859
    :goto_3
    new-instance v3, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;

    .line 3860
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v5

    .line 3861
    invoke-direct {v3, v5, v2, v1, p1}, Lcom/pspdfkit/ui/inspector/views/MeasurementValueInspectorView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)V

    .line 3868
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_inspector_view_measurement_value:I

    invoke-virtual {v3, v1}, Landroid/view/View;->setId(I)V

    :goto_4
    if-eqz v3, :cond_8

    .line 3869
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3874
    :cond_8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda15;

    invoke-direct {v2, p0, p1, p2}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)V

    .line 3875
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->SCALE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, p2, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_9

    move-object v1, v4

    goto :goto_5

    .line 3876
    :cond_9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    invoke-interface {v3, p2, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    .line 3877
    invoke-virtual {p0, v3, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;)Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    move-result-object v1

    .line 3878
    :goto_5
    sget-object v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MEASUREMENT_DISTANCE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-ne p2, v2, :cond_c

    .line 3882
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v2

    iget-object v2, v2, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 3883
    new-instance v3, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda16;

    invoke-direct {v3, v1, p1}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda16;-><init>(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {p0, p1, v2, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView$CalibrationPickerListener;)Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 3910
    instance-of v3, v1, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    if-eqz v3, :cond_a

    move-object v3, v1

    goto :goto_6

    :cond_a
    move-object v3, v4

    :goto_6
    if-eqz v3, :cond_b

    invoke-virtual {v3, v2}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setCalibrationPicker(Lcom/pspdfkit/ui/inspector/views/ScaleCalibrationPickerInspectorView;)V

    .line 3911
    :cond_b
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    if-eqz v1, :cond_d

    .line 3916
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3919
    :cond_d
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda17;

    invoke-direct {v1, p0, p1, p2}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda17;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)V

    .line 3920
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FLOAT_PRECISION:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v2, p2, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v2

    if-nez v2, :cond_e

    goto :goto_7

    .line 3921
    :cond_e
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    const-class v3, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    invoke-interface {v2, p2, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    .line 3922
    invoke-virtual {p0, v2, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;)Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    move-result-object v4

    :goto_7
    if-eqz v4, :cond_f

    .line 3923
    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3926
    :cond_f
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->isMeasurementSnappingEnabled()Z

    move-result v0

    new-instance v1, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda18;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda18;-><init>(Lcom/pspdfkit/internal/q0;)V

    invoke-virtual {p0, p2, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;ZLcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;)Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 3929
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3932
    :cond_10
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/util/ArrayList;)V

    .line 3935
    new-instance p1, Lcom/pspdfkit/internal/le;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/le;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 5

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedFont"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/q0;->a(I)V

    .line 4
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 8
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    const-string v3, "annotation"

    .line 9
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "font"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 957
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_2

    .line 959
    move-object v3, p1

    check-cast v3, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 960
    invoke-virtual {p2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setFontName(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 961
    invoke-static {v3, v2, v0, v1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;Lcom/pspdfkit/utils/Size;Landroid/text/TextPaint;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 962
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v0

    .line 963
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    .line 965
    iget-object v2, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 966
    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 967
    invoke-interface {v1, v2, v0, p2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V

    .line 972
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 973
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string v0, "change_property_in_inspector"

    .line 974
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 975
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string v0, "fontName"

    .line 976
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 977
    invoke-virtual {p2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 978
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_3
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3777
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3778
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3779
    invoke-virtual {p1, p3}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 3780
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 3781
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    .line 3783
    iget-object v1, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3784
    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 3785
    invoke-interface {v0, v1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 3790
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3791
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 3792
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 3793
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3794
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "line_ends_fill_color"

    .line 3795
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 3796
    invoke-static {p3, p1, p2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    .line 3797
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3801
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x5

    .line 3802
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/q0;->a(I)V

    int-to-float p2, p3

    const/high16 p3, 0x42c80000    # 100.0f

    div-float/2addr p2, p3

    .line 3804
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result p3

    cmpg-float p3, p3, p2

    if-nez p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-nez p3, :cond_1

    .line 3805
    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 3806
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p1

    .line 3807
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    .line 3809
    iget-object v0, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3810
    iget-object p1, p1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 3811
    invoke-interface {p3, v0, p1, p2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 3817
    :cond_1
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x6

    .line 979
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/q0;->a(I)V

    .line 980
    sget p0, Lcom/pspdfkit/internal/ao;->a:I

    const-string p0, "annotation"

    .line 981
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2005
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object p2, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, p2, p0

    const/16 p2, 0x15

    if-ne p0, p2, :cond_0

    .line 2007
    move-object p0, p1

    check-cast p0, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {p0, p3}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOverlayText(Ljava/lang/String;)V

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_2

    .line 2008
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 2009
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 2010
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "overlay_text"

    .line 2011
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    if-nez p3, :cond_1

    const-string p3, ""

    :cond_1
    const-string p1, "value"

    .line 2012
    invoke-virtual {p0, p1, p3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 2013
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_2
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2014
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 2015
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 2016
    sget p2, Lcom/pspdfkit/internal/ao;->a:I

    const-string p2, "annotation"

    .line 2017
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3021
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/16 v0, 0x15

    if-ne p2, v0, :cond_0

    .line 3023
    move-object p2, p1

    check-cast p2, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {p2, p3}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setRepeatOverlayText(Z)V

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 3024
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3025
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 3026
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 3027
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3028
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "repeat_overlay_text"

    .line 3029
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3030
    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 3031
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_1

    .line 3033
    :cond_1
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    :goto_1
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "executedMove"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3818
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 3819
    :cond_0
    invoke-interface {p2, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getZIndex(Lcom/pspdfkit/annotations/Annotation;)I

    move-result v0

    .line 3821
    invoke-interface {p2, p1, p3}, Lcom/pspdfkit/annotations/AnnotationProvider;->moveAnnotationAsync(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/AnnotationZIndexMove;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p3

    .line 3822
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {p3, v1}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p3

    .line 3824
    new-instance v1, Lcom/pspdfkit/internal/r0;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/pspdfkit/internal/r0;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;ILcom/pspdfkit/annotations/AnnotationProvider;)V

    .line 3825
    invoke-virtual {p3, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4094
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 4095
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 4096
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p3}, Lcom/pspdfkit/internal/pf;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 4097
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 4098
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 4099
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p1

    invoke-interface {p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 4101
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p0, p3}, Lcom/pspdfkit/document/PdfDocument;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    :goto_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3936
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 3937
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3938
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p3}, Lcom/pspdfkit/internal/pf;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 3939
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 3940
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 3941
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p1

    invoke-interface {p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 3943
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p0, p3}, Lcom/pspdfkit/document/PdfDocument;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    :goto_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/q0;Z)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4102
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p0

    invoke-interface {p0, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setMeasurementSnappingEnabled(Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/ui/inspector/PropertyInspectorView;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Float;Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V
    .locals 4

    const-string v0, "$annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "calibrationUnit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 3944
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    .line 3945
    instance-of v0, p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    if-nez v0, :cond_0

    return-void

    .line 3946
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/LineAnnotation;

    if-nez v0, :cond_1

    return-void

    .line 3949
    :cond_1
    check-cast p1, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/PointF;

    .line 3950
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LineAnnotation;->getPoints()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Landroid/graphics/PointF;

    .line 3951
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;

    .line 3952
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    float-to-double v2, p2

    .line 3953
    invoke-static {p3}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;)Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object p2

    .line 3954
    invoke-direct {v1, v2, v3, p2}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;-><init>(DLcom/pspdfkit/internal/jni/NativeUnitTo;)V

    .line 3958
    check-cast p0, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->getCurrentScaleValue()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p2

    const-string p3, "scalePicker.currentScaleValue"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object p2

    .line 3959
    invoke-static {v0, p1, v1, p2}, Lcom/pspdfkit/internal/jni/NativeMeasurementCalculator;->getMeasurementScaleFromCalibration(Landroid/graphics/PointF;Landroid/graphics/PointF;Lcom/pspdfkit/internal/jni/NativeMeasurementCalibration;Lcom/pspdfkit/internal/jni/NativeMeasurementScale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    const-string p2, "nativeScale"

    .line 3960
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4085
    new-instance p2, Lcom/pspdfkit/annotations/measurements/Scale;

    .line 4086
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getFrom()D

    move-result-wide v0

    double-to-float p3, v0

    .line 4087
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getUnitFrom()Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    move-result-object v0

    const-string v1, "nativeScale.unitFrom"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeUnitFrom;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    move-result-object v0

    .line 4088
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getTo()D

    move-result-wide v1

    double-to-float v1, v1

    .line 4089
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getUnitTo()Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object p1

    const-string v2, "nativeScale.unitTo"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeUnitTo;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object p1

    .line 4090
    invoke-direct {p2, p3, v0, v1, p1}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    .line 4091
    invoke-virtual {p0}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->getCurrentScaleValue()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    const/4 p1, 0x1

    const/4 p3, 0x0

    .line 4093
    invoke-virtual {p0, p2, p1, p3}, Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;->setScale(Lcom/pspdfkit/annotations/measurements/Scale;ZZ)V

    :cond_4
    return-void
.end method

.method private static final b(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 5

    const-string p2, "$annotation"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "value"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7952
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v0

    .line 7953
    invoke-direct {p1}, Lcom/pspdfkit/internal/q0;->d()V

    .line 7954
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    if-eqz v0, :cond_0

    .line 7956
    iget-object v1, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    const-string v2, "lineEnds1.first"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-static {p0, v1, p3}, Lcom/pspdfkit/internal/ao;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7958
    invoke-static {p0}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v1

    .line 7959
    invoke-virtual {p1}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v2

    .line 7961
    iget-object v3, v1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 7962
    iget-object v1, v1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 7963
    iget-object v4, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/pspdfkit/annotations/LineEndType;

    .line 7964
    invoke-interface {v2, v3, v1, v4, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 7970
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 7971
    iget-object v1, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 7972
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "change_property_in_inspector"

    .line 7973
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 7974
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string v1, "action"

    const-string v2, "line_ends"

    .line 7975
    invoke-virtual {p0, v1, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 7978
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    .line 7980
    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 7981
    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p3

    const/4 v0, 0x1

    aput-object p3, v2, v0

    .line 7982
    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p3

    const-string v0, "%s,%s"

    invoke-static {v0, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "format(format, *args)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7983
    invoke-virtual {p0, p2, p3}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 7991
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 7993
    :cond_0
    iget-object p0, p1, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    return-void
.end method

.method private final b(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/util/ArrayList;)V
    .locals 3

    .line 7994
    new-instance v0, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda14;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 7995
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    invoke-virtual {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isZIndexEditingSupported(Lcom/pspdfkit/annotations/AnnotationType;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 7996
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    invoke-interface {v1, p2, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p2

    .line 7997
    invoke-virtual {p0, p2, v0}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView$ZIndexChangeListener;)Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;

    move-result-object p2

    :goto_0
    if-eqz p2, :cond_1

    .line 7998
    iput-object p2, p0, Lcom/pspdfkit/internal/q0;->g:Lcom/pspdfkit/ui/inspector/views/ZIndexInspectorView;

    .line 7999
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 8000
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/q0;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/q0;->f:I

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8001
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 8002
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 8003
    invoke-virtual {p1, p3}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 8004
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 8005
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    iget-object v1, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 8006
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 8007
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 8008
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 8009
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 8010
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "foreground_color"

    .line 8011
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 8012
    invoke-static {p3, p1, p2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    .line 8013
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 8014
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 5

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x2

    .line 7242
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/q0;->a(I)V

    .line 7243
    sget p2, Lcom/pspdfkit/internal/ao;->a:I

    .line 7246
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    invoke-interface {p2, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    .line 7247
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-string v2, "annotation"

    .line 7248
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7913
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x5

    const/4 v4, 0x1

    if-eq v2, v3, :cond_3

    const/4 v3, 0x7

    if-eq v2, v3, :cond_2

    const/16 v3, 0x11

    if-eq v2, v3, :cond_3

    const/16 v3, 0x9

    if-eq v2, v3, :cond_3

    const/16 p2, 0xa

    if-eq v2, p2, :cond_1

    const/16 p2, 0x13

    if-eq v2, p2, :cond_1

    const/16 p2, 0x14

    if-eq v2, p2, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    .line 7919
    :cond_1
    move-object p2, p1

    check-cast p2, Lcom/pspdfkit/annotations/BaseLineAnnotation;

    int-to-float v0, p3

    invoke-virtual {p2, v0}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->setLineWidth(F)V

    goto :goto_1

    .line 7920
    :cond_2
    move-object p2, p1

    check-cast p2, Lcom/pspdfkit/annotations/InkAnnotation;

    int-to-float v0, p3

    invoke-virtual {p2, v0}, Lcom/pspdfkit/annotations/InkAnnotation;->setLineWidth(F)V

    goto :goto_1

    :cond_3
    int-to-float v2, p3

    .line 7928
    invoke-virtual {p1, v2}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    .line 7929
    instance-of v2, p1, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    if-eqz v1, :cond_4

    .line 7933
    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 7934
    invoke-static {v2, v1, p2, v0}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;Lcom/pspdfkit/utils/Size;Landroid/text/TextPaint;)V

    :cond_4
    :goto_1
    if-eqz v4, :cond_5

    .line 7935
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 7936
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 7937
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p0

    .line 7939
    iget-object v0, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 7940
    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    int-to-float v1, p3

    .line 7941
    invoke-interface {p0, v0, p2, v1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 7946
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 7947
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 7948
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "thickness"

    .line 7949
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "value"

    .line 7950
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 7951
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_5
    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/q0;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/q0;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1851
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 1852
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 1853
    invoke-virtual {p1, p3}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 1854
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 1855
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    iget-object v1, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 1856
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 1857
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 1858
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 1859
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 1860
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "foreground_color"

    .line 1861
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 1862
    invoke-static {p3, p1, p2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    .line 1863
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 1864
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 4

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x3

    .line 1110
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/q0;->a(I)V

    .line 1111
    sget p2, Lcom/pspdfkit/internal/ao;->a:I

    .line 1114
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    invoke-interface {p2, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    .line 1115
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-string v2, "annotation"

    .line 1116
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1830
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v2, v3, :cond_2

    .line 1831
    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    int-to-float v3, p3

    .line 1832
    invoke-virtual {v2, v3}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setTextSize(F)V

    if-eqz p2, :cond_1

    if-eqz v1, :cond_1

    .line 1833
    invoke-static {v2, v1, p2, v0}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;Lcom/pspdfkit/utils/Size;Landroid/text/TextPaint;)V

    :cond_1
    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_3

    .line 1834
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 1835
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 1836
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p0

    .line 1838
    iget-object v0, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 1839
    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    int-to-float v1, p3

    .line 1840
    invoke-interface {p0, v0, p2, v1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 1845
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 1846
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 1847
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "text_Size"

    .line 1848
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "value"

    .line 1849
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/q$a;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 1850
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    :cond_3
    return-void
.end method

.method private final d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 2
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/q0;->f:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 4
    iput v1, p0, Lcom/pspdfkit/internal/q0;->f:I

    :cond_1
    return-void
.end method

.method private static final d(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 3

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 6
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 7
    sget p2, Lcom/pspdfkit/internal/ao;->a:I

    const-string p2, "annotation"

    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 992
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x15

    if-ne p2, v2, :cond_0

    .line 994
    move-object p2, p1

    check-cast p2, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {p2, p3}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOutlineColor(I)V

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 995
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p2

    const-string v2, "change_property_in_inspector"

    .line 996
    invoke-virtual {p2, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p2

    .line 997
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    const-string p2, "action"

    const-string v2, "outline_color"

    .line 998
    invoke-virtual {p1, p2, v2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 999
    invoke-static {p3, v1, v0}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object p2

    const-string p3, "value"

    .line 1000
    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 1001
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 1003
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 1004
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    return-void
.end method

.method private static final e(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/q0;->d()V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->startRecording()V

    .line 3
    invoke-virtual {p1, p3}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p2

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    iget-object v1, p2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object p2, p2, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 6
    iget-object p2, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p2}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 7
    iget-object p0, p0, Lcom/pspdfkit/internal/q0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->saveCurrentlySelectedAnnotation()V

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p0

    const-string p2, "change_property_in_inspector"

    .line 9
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 10
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const-string p1, "action"

    const-string p2, "fill_color"

    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 12
    invoke-static {p3, p1, p2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object p1

    const-string p2, "value"

    .line 13
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p0

    .line 17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method


# virtual methods
.method public final b(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;
    .locals 20

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    const-string v8, "annotation"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 5
    new-instance v10, Lcom/pspdfkit/internal/s0;

    invoke-direct {v10, v9}, Lcom/pspdfkit/internal/s0;-><init>(Ljava/util/ArrayList;)V

    .line 8
    invoke-static/range {p1 .. p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v11

    .line 10
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    const-string v12, "toolVariantPair.first"

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-direct {v6, v7, v0, v9}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/util/ArrayList;)V

    .line 13
    instance-of v13, v7, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    const/4 v0, 0x5

    if-eqz v13, :cond_5

    .line 16
    iget-object v2, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v2, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 17
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 937
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v4, v3

    if-ne v3, v0, :cond_2

    .line 939
    move-object v3, v7

    check-cast v3, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 940
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getFontName()Ljava/lang/String;

    move-result-object v3

    .line 941
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/pspdfkit/internal/mt;->getFontByName(Ljava/lang/String;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v4

    if-nez v4, :cond_3

    if-eqz v3, :cond_1

    .line 942
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v5, 0x1

    :goto_1
    if-nez v5, :cond_3

    .line 944
    new-instance v4, Lcom/pspdfkit/ui/fonts/Font;

    invoke-direct {v4, v3}, Lcom/pspdfkit/ui/fonts/Font;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    .line 945
    :cond_3
    :goto_2
    new-instance v3, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda0;

    invoke-direct {v3, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 946
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v5

    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FONT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v5, v2, v1}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    goto :goto_3

    .line 947
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    invoke-interface {v1, v2, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    .line 948
    invoke-virtual {v6, v1, v4, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView;

    move-result-object v1

    .line 949
    :goto_3
    invoke-virtual {v10, v1}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    :cond_5
    iget-object v1, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 985
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1999
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/16 v4, 0x15

    if-ne v2, v4, :cond_6

    .line 2000
    move-object v2, v7

    check-cast v2, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/RedactionAnnotation;->getOverlayText()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    .line 2001
    :goto_4
    new-instance v5, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda5;

    invoke-direct {v5, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 2002
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v14

    sget-object v15, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v14, v1, v15}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v14

    if-nez v14, :cond_7

    const/4 v1, 0x0

    goto :goto_5

    .line 2003
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v14

    const-class v15, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-interface {v14, v1, v15}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    .line 2004
    invoke-virtual {v6, v1, v2, v5}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    move-result-object v1

    .line 2005
    :goto_5
    invoke-virtual {v10, v1}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2025
    iget-object v1, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 2026
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3020
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v3, v2

    if-ne v2, v4, :cond_8

    .line 3021
    move-object v2, v7

    check-cast v2, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/RedactionAnnotation;->shouldRepeatOverlayText()Z

    move-result v2

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    .line 3022
    :goto_6
    new-instance v5, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda6;

    invoke-direct {v5, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 3023
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v14

    sget-object v15, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v14, v1, v15}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v14

    if-nez v14, :cond_9

    const/4 v1, 0x0

    goto :goto_7

    .line 3024
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v14

    const-class v15, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-interface {v14, v1, v15}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    .line 3025
    invoke-virtual {v6, v1, v2, v5}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;ZLcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;)Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;

    move-result-object v1

    .line 3026
    :goto_7
    invoke-virtual {v10, v1}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3051
    iget-object v1, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3052
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3747
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v14, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v2, v14, :cond_a

    .line 3748
    move-object v2, v7

    check-cast v2, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v2}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v2

    goto :goto_8

    .line 3750
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v2

    .line 3751
    :goto_8
    new-instance v5, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda7;

    invoke-direct {v5, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 3752
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v15

    sget-object v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v15, v1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v15

    if-nez v15, :cond_b

    move-object/from16 v18, v0

    const/4 v0, 0x0

    goto :goto_9

    .line 3753
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v15

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-interface {v15, v1, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    .line 3755
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v15

    move-object/from16 v18, v0

    sget-object v0, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v15, v1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v0

    .line 3756
    invoke-virtual {v6, v4, v2, v0, v5}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;IZLcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_c

    .line 3757
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v15, 0x1

    goto :goto_a

    :cond_c
    const/4 v15, 0x0

    .line 3764
    :goto_a
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3765
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4739
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_d

    .line 4740
    move-object v1, v7

    check-cast v1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/RedactionAnnotation;->getOutlineColor()I

    move-result v1

    goto :goto_b

    :cond_d
    const/4 v1, 0x0

    .line 4741
    :goto_b
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda8;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 4742
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OUTLINE_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_e

    const/4 v0, 0x0

    goto :goto_c

    .line 4743
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    .line 4744
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    .line 4745
    :goto_c
    invoke-virtual {v10, v0}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4755
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 4756
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v1

    .line 4757
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda9;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 4758
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_f

    const/4 v0, 0x0

    goto :goto_d

    .line 4759
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    .line 4760
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    .line 4761
    :goto_d
    invoke-virtual {v10, v0}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4788
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 4789
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5431
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v2, -0x1

    const/4 v3, 0x5

    if-eq v1, v3, :cond_12

    const/4 v3, 0x7

    if-eq v1, v3, :cond_11

    const/16 v3, 0x11

    if-eq v1, v3, :cond_12

    const/16 v3, 0x9

    if-eq v1, v3, :cond_12

    const/16 v3, 0xa

    if-eq v1, v3, :cond_10

    const/16 v3, 0x13

    if-eq v1, v3, :cond_10

    const/16 v3, 0x14

    if-eq v1, v3, :cond_10

    const/4 v1, -0x1

    goto :goto_f

    .line 5433
    :cond_10
    move-object v1, v7

    check-cast v1, Lcom/pspdfkit/annotations/BaseLineAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/BaseLineAnnotation;->getLineWidth()F

    move-result v1

    goto :goto_e

    .line 5434
    :cond_11
    move-object v1, v7

    check-cast v1, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLineWidth()F

    move-result v1

    goto :goto_e

    .line 5438
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v1

    :goto_e
    float-to-int v1, v1

    :goto_f
    int-to-float v1, v1

    .line 5439
    new-instance v3, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda10;

    invoke-direct {v3, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 5440
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_13

    const/4 v0, 0x0

    goto :goto_10

    .line 5441
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    invoke-interface {v4, v0, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    .line 5442
    invoke-virtual {v6, v0, v1, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v0

    .line 5443
    :goto_10
    invoke-virtual {v10, v0}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5477
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 5478
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6213
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v3, :cond_14

    .line 6214
    move-object v1, v7

    check-cast v1, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextSize()F

    move-result v1

    float-to-int v2, v1

    :cond_14
    int-to-float v1, v2

    .line 6215
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda11;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 6216
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_15

    const/4 v0, 0x0

    goto :goto_11

    .line 6217
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    .line 6218
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v0

    .line 6219
    :goto_11
    invoke-virtual {v10, v0}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6253
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 6254
    new-instance v1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 6255
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v2

    .line 6256
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v3

    .line 6257
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object v4

    .line 6258
    invoke-direct {v1, v2, v3, v4}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;Ljava/util/List;)V

    .line 6259
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda12;

    invoke-direct {v2, v7, v6}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;)V

    .line 6260
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->BORDER_STYLE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_16

    const/4 v0, 0x0

    goto :goto_12

    .line 6261
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    .line 6262
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;)Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;

    move-result-object v0

    .line 6263
    :goto_12
    invoke-virtual {v10, v0}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6345
    invoke-static/range {p1 .. p1}, Lcom/pspdfkit/internal/ao;->c(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object v5

    if-eqz v5, :cond_1c

    if-nez v13, :cond_18

    .line 6355
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 6356
    iget-object v1, v5, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    const-string v2, "lineEnds.first"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v1

    check-cast v2, Lcom/pspdfkit/annotations/LineEndType;

    .line 6357
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/pspdfkit/R$string;->pspdf__picker_line_start:I

    const/4 v4, 0x0

    .line 6358
    invoke-static {v1, v3, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "getString(context, R.str\u2026pspdf__picker_line_start)"

    .line 6359
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6360
    new-instance v4, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda13;

    invoke-direct {v4, v7, v6}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;)V

    const/16 v16, 0x1

    .line 6361
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    move-object/from16 v17, v4

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v1, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v1

    if-nez v1, :cond_17

    move-object/from16 v16, v14

    move-object/from16 v19, v18

    const/4 v4, 0x0

    move-object v14, v5

    goto :goto_13

    .line 6362
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    invoke-interface {v1, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    move-object/from16 v4, v18

    move-object/from16 v0, p0

    move-object/from16 v19, v4

    move/from16 v4, v16

    move-object/from16 v16, v14

    move-object v14, v5

    move-object/from16 v5, v17

    .line 6363
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;Lcom/pspdfkit/annotations/LineEndType;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v4

    .line 6364
    :goto_13
    invoke-virtual {v10, v4}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_14

    :cond_18
    move-object/from16 v16, v14

    move-object/from16 v19, v18

    move-object v14, v5

    .line 6410
    :goto_14
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eqz v13, :cond_19

    .line 6411
    iget-object v1, v14, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    goto :goto_15

    :cond_19
    iget-object v1, v14, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    :goto_15
    check-cast v1, Lcom/pspdfkit/annotations/LineEndType;

    move-object v2, v1

    const-string v1, "if (flipValuePosition) l\u2026irst else lineEnds.second"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6412
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/pspdfkit/R$string;->pspdf__picker_line_end:I

    const/4 v13, 0x0

    .line 6413
    invoke-static {v1, v3, v13}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "getString(context, R.str\u2026g.pspdf__picker_line_end)"

    .line 6414
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6415
    new-instance v5, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda1;

    invoke-direct {v5, v7, v6}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/q0;)V

    const/4 v4, 0x0

    .line 6416
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    sget-object v14, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v1, v0, v14}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v1

    if-nez v1, :cond_1a

    move-object v4, v13

    goto :goto_16

    .line 6417
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v1

    const-class v14, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    invoke-interface {v1, v0, v14}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    move-object/from16 v0, p0

    .line 6418
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;Lcom/pspdfkit/annotations/LineEndType;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v4

    .line 6419
    :goto_16
    invoke-virtual {v10, v4}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6463
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 6464
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v1

    .line 6465
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda2;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 6466
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS_FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_1b

    move-object v4, v13

    goto :goto_17

    .line 6467
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    .line 6468
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->b(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v4

    .line 6469
    :goto_17
    invoke-virtual {v10, v4}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18

    :cond_1c
    move-object/from16 v16, v14

    move-object/from16 v19, v18

    const/4 v13, 0x0

    :goto_18
    if-eqz v15, :cond_1f

    .line 6500
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f

    .line 6502
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 6503
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7198
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    move-object/from16 v2, v16

    if-ne v1, v2, :cond_1d

    .line 7199
    move-object v1, v7

    check-cast v1, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v1}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v1

    goto :goto_19

    .line 7201
    :cond_1d
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v1

    .line 7202
    :goto_19
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda3;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 7203
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    move-object/from16 v4, v19

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_1e

    move-object v4, v13

    goto :goto_1a

    .line 7204
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    .line 7205
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    move-result-object v4

    :goto_1a
    if-eqz v4, :cond_1f

    .line 7206
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 7207
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7212
    :cond_1f
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 7213
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v1

    .line 7214
    new-instance v2, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda4;

    invoke-direct {v2, v6, v7}, Lcom/pspdfkit/internal/q0$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 7215
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_ALPHA:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_20

    move-object v15, v13

    goto :goto_1b

    .line 7216
    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    invoke-interface {v3, v0, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    .line 7217
    invoke-virtual {v6, v0, v1, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v15

    .line 7218
    :goto_1b
    invoke-virtual {v10, v15}, Lcom/pspdfkit/internal/s0;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7240
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-nez v0, :cond_21

    .line 7241
    iget-object v0, v11, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-direct {v6, v7, v0, v9}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/util/ArrayList;)V

    :cond_21
    return-object v9
.end method

.method public final c(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 7

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->b(Lcom/pspdfkit/annotations/Annotation;)Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 3
    invoke-static {}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->values()[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 4
    sget v5, Lcom/pspdfkit/internal/ao;->a:I

    const-string v5, "property"

    .line 5
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1108
    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v6, 0x1

    if-eq v4, v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    .line 1109
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v5

    invoke-interface {v5, p1, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v6

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

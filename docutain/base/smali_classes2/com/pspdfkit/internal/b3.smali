.class public final Lcom/pspdfkit/internal/b3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/audio/AudioPlaybackController;
.implements Lcom/pspdfkit/internal/j3$a;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final b:Lcom/pspdfkit/internal/a3;

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/audio/AudioPlaybackController$AudioPlaybackListener;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/pspdfkit/annotations/SoundAnnotation;

.field private e:Lio/reactivex/rxjava3/disposables/Disposable;

.field private f:Lcom/pspdfkit/internal/j3;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/a3;)V
    .locals 1

    const-string v0, "audioManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    .line 6
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/b3;->c:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/annotations/SoundAnnotation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    return-object p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/b3;Lcom/pspdfkit/internal/j3;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/ls;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez v0, :cond_0

    return-void

    .line 55
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/b3$b;

    invoke-direct {v1, v0, p1}, Lcom/pspdfkit/internal/b3$b;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/internal/ls;)V

    invoke-static {v1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final a(Z)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->f()V

    .line 60
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/j3;->a(Lcom/pspdfkit/internal/b3;)V

    .line 61
    iput-object v1, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez v0, :cond_1

    return-void

    .line 63
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 64
    sget-object v0, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 65
    iput-object v1, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz p1, :cond_2

    .line 68
    iget-object p1, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/a3;->c(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    :cond_2
    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/internal/a3;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/internal/nh;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/b3;->c:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/v3;
    .locals 5

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    .line 30
    new-instance v1, Lcom/pspdfkit/internal/v3;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b3;->isResumed()Z

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b3;->getCurrentPosition()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/pspdfkit/internal/v3;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;ZZI)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public final a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;ZI)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/b3;->a(Z)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-nez v0, :cond_1

    .line 10
    iput-object p2, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/a3;->b(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    goto :goto_0

    .line 13
    :cond_1
    iput-object p2, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/a3;->a(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    .line 18
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/b3$a;

    invoke-direct {v0, p3, p0, p4}, Lcom/pspdfkit/internal/b3$a;-><init>(ZLcom/pspdfkit/internal/b3;I)V

    .line 19
    iget-object p3, p0, Lcom/pspdfkit/internal/b3;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {p3}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 20
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/j3$c;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 21
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p3

    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 22
    new-instance p3, Lcom/pspdfkit/internal/h3;

    invoke-direct {p3, p0, v0}, Lcom/pspdfkit/internal/h3;-><init>(Lcom/pspdfkit/internal/b3;Lkotlin/jvm/functions/Function0;)V

    new-instance p4, Lcom/pspdfkit/internal/i3;

    invoke-direct {p4, p0}, Lcom/pspdfkit/internal/i3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-virtual {p1, p3, p4}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 23
    iput-object p1, p0, Lcom/pspdfkit/internal/b3;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 24
    sget-object p1, Lcom/pspdfkit/internal/ls;->e:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 27
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/v3;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p2

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/b3$c;

    invoke-direct {v0, p0, p1, p3}, Lcom/pspdfkit/internal/b3$c;-><init>(Lcom/pspdfkit/internal/b3;Landroid/content/Context;Lcom/pspdfkit/internal/v3;)V

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/j3$b;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    sget-object p1, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    goto :goto_0

    .line 48
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/ls;->e:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 49
    new-instance p1, Lcom/pspdfkit/internal/g3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/g3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 50
    :cond_2
    sget-object p1, Lcom/pspdfkit/internal/ls;->e:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 51
    new-instance p1, Lcom/pspdfkit/internal/d3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/d3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 52
    :cond_3
    sget-object p1, Lcom/pspdfkit/internal/ls;->d:Lcom/pspdfkit/internal/ls;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V

    .line 53
    new-instance p1, Lcom/pspdfkit/internal/e3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/e3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-static {p1}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public final addAudioPlaybackListener(Lcom/pspdfkit/ui/audio/AudioPlaybackController$AudioPlaybackListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->d:Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final exitAudioPlaybackMode()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/b3;->a(Z)V

    return-void
.end method

.method public final getAudioModeManager()Lcom/pspdfkit/ui/audio/AudioModeManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->b:Lcom/pspdfkit/internal/a3;

    return-object v0
.end method

.method public final getCurrentPosition()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->b()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getDuration()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->c()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isReady()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isResumed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->d()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Z)V

    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->hasAudioData()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/b3;->a(Z)V

    :cond_0
    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldOrder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newOrder"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final pause()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->e()V

    :cond_0
    return-void
.end method

.method public final removeAudioPlaybackListener(Lcom/pspdfkit/ui/audio/AudioPlaybackController$AudioPlaybackListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final resume()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j3;->g()V

    :cond_0
    return-void
.end method

.method public final seekTo(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b3;->getDuration()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/b3;->f:Lcom/pspdfkit/internal/j3;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/j3;->a(I)V

    :cond_0
    return-void
.end method

.method public synthetic toggle()V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/ui/audio/AudioPlaybackController$-CC;->$default$toggle(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    return-void
.end method

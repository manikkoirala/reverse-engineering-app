.class public final Lcom/pspdfkit/internal/pr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;
.implements Lcom/pspdfkit/listeners/DocumentSigningListener;
.implements Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;


# instance fields
.field private final b:Lcom/pspdfkit/ui/PdfFragment;

.field private c:Lcom/pspdfkit/internal/zf;

.field private d:Lcom/pspdfkit/internal/nm;

.field private e:Lcom/pspdfkit/internal/nm;

.field private f:Lcom/pspdfkit/forms/SignatureFormElement;

.field private g:Lcom/pspdfkit/annotations/Annotation;

.field private h:Lcom/pspdfkit/listeners/DocumentSigningListener;

.field private final i:Lcom/pspdfkit/listeners/DocumentListener;

.field private final j:Lcom/pspdfkit/internal/fl;

.field private k:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$QrM2kg085SP3mWTN9oFzVIJLeww(Lcom/pspdfkit/forms/SignatureFormElement;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/pr;->b(Lcom/pspdfkit/forms/SignatureFormElement;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YClgFZD7na1wRPuKWS9qho9yV8I(Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/forms/FormElement;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pr;->a(Lcom/pspdfkit/forms/FormElement;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oXGu1fi5ho7YNeVoLuDfrMfplIk(Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pr;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pr;->b(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/vu;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/pr$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/pr$a;-><init>(Lcom/pspdfkit/internal/pr;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->i:Lcom/pspdfkit/listeners/DocumentListener;

    const-string v0, "pdfFragment"

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 21
    iput-object p2, p0, Lcom/pspdfkit/internal/pr;->j:Lcom/pspdfkit/internal/fl;

    return-void
.end method

.method private static a(Lcom/pspdfkit/forms/SignatureFormElement;)Ljava/lang/Runnable;
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 74
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/forms/SignatureFormElement;)V

    return-object v0
.end method

.method private a(Lcom/pspdfkit/forms/FormElement;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 16
    check-cast p1, Lcom/pspdfkit/forms/SignatureFormElement;

    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->f:Lcom/pspdfkit/forms/SignatureFormElement;

    if-eqz p1, :cond_2

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    const-string v0, "pdfFragment"

    .line 22
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSignaturePickedListener"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    .line 56
    monitor-enter v0

    .line 57
    :try_start_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->b()Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    if-eqz v1, :cond_1

    .line 58
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 60
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p1

    .line 61
    invoke-static {v0, p0, p1}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    goto :goto_1

    .line 68
    :cond_1
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p1

    .line 71
    invoke-static {v0, p0, p1}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/zf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->h:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-nez v0, :cond_0

    move-object v0, p0

    .line 9
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setListener(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method private static synthetic b(Lcom/pspdfkit/forms/SignatureFormElement;)V
    .locals 2

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/SignatureFormElement;->getFormField()Lcom/pspdfkit/forms/SignatureFormField;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/forms/SignatureFormField;->removeSignature()V
    :try_end_0
    .catch Lcom/pspdfkit/exceptions/PSPDFKitException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "PSPDFKit.Signatures"

    const-string v1, "Error while deleting a signature"

    .line 112
    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private b(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "document"

    const-string v1, "argumentName"

    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 67
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 69
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->e:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_2

    .line 73
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Maybe;->toSingle()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->d:Lcom/pspdfkit/internal/nm;

    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nm;->a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Maybe;->toSingle()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 87
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElementAsync()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_3
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 92
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 93
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 94
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Maybe;->onErrorComplete()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 95
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/pr;Lcom/pspdfkit/internal/zf;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/pr$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/pr;)V

    .line 109
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->i:Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "SignatureFormSigningHandler.FormElementBeingSigned"

    .line 2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/nm;

    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->d:Lcom/pspdfkit/internal/nm;

    const-string v0, "SignatureFormSigningHandler.AnnotationUsedForSigning"

    .line 6
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/nm;

    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->e:Lcom/pspdfkit/internal/nm;

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/listeners/DocumentSigningListener;)V
    .locals 1

    .line 75
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->h:Lcom/pspdfkit/listeners/DocumentSigningListener;

    .line 77
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->setListener(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->i:Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->removeDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->f:Lcom/pspdfkit/forms/SignatureFormElement;

    if-eqz v0, :cond_0

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/nm;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    const-string v0, "SignatureFormSigningHandler.FormElementBeingSigned"

    .line 9
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_1

    .line 14
    new-instance v1, Lcom/pspdfkit/internal/nm;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/nm;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    const-string v0, "SignatureFormSigningHandler.AnnotationUsedForSigning"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method public synthetic isFormElementClickable(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener$-CC;->$default$isFormElementClickable(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    return p1
.end method

.method public final onDismiss()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->k:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final onDocumentSigned(Landroid/net/Uri;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    instance-of v1, v0, Lcom/pspdfkit/ui/PdfActivity;

    if-eqz v1, :cond_1

    .line 5
    check-cast v0, Lcom/pspdfkit/ui/PdfActivity;

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v1

    .line 8
    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/ui/PdfUi;->setDocumentFromUri(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_1
    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method

.method public final onDocumentSigningError(Ljava/lang/Throwable;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    sget v1, Lcom/pspdfkit/R$string;->pspdf__digital_signature_could_not_save:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Signatures"

    const-string v2, "Error while signing a document."

    .line 6
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    if-eqz p1, :cond_1

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 p1, 0x0

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    :cond_1
    return-void
.end method

.method public final onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_5

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 4
    check-cast p1, Lcom/pspdfkit/forms/SignatureFormElement;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 11
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v1

    .line 13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v3

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/hb;->d()Z

    move-result v4

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/forms/SignatureFormElement;->isSigned()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/forms/SignatureFormElement;->getSignatureInfo()Lcom/pspdfkit/signatures/DigitalSignatureInfo;

    move-result-object v1

    .line 21
    invoke-static {p1}, Lcom/pspdfkit/internal/pr;->a(Lcom/pspdfkit/forms/SignatureFormElement;)Ljava/lang/Runnable;

    move-result-object p1

    .line 22
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/ui/signatures/SignatureInfoDialog;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/DigitalSignatureInfo;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/forms/SignatureFormElement;->getOverlappingSignature()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/SignatureFormElement;->getOverlappingSignature()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_4

    .line 30
    iput-object p1, p0, Lcom/pspdfkit/internal/pr;->f:Lcom/pspdfkit/forms/SignatureFormElement;

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/qr;->a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V

    goto :goto_0

    :cond_4
    new-array p1, v2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.Signatures"

    const-string v1, "Attempted to add or select a signature but license does not include Electronic Signatures, skipping..."

    .line 35
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_5
    return v2
.end method

.method public synthetic onSignatureCreated(Lcom/pspdfkit/signatures/Signature;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener$-CC;->$default$onSignatureCreated(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/Signature;Z)V

    return-void
.end method

.method public final onSignaturePicked(Lcom/pspdfkit/signatures/Signature;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->f:Lcom/pspdfkit/forms/SignatureFormElement;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    .line 9
    sget-object v3, Lcom/pspdfkit/internal/pr$b;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 14
    iget-object v3, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1, v3, v1, v2}, Lcom/pspdfkit/signatures/Signature;->toStampAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object v1

    goto :goto_0

    .line 17
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unhandled Signature type. Neither Ink, nor Stamp."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 18
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1, v3, v1, v2}, Lcom/pspdfkit/signatures/Signature;->toInkAnnotation(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/RectF;)Lcom/pspdfkit/annotations/InkAnnotation;

    move-result-object v1

    .line 26
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/annotations/Annotation;->setCreator(Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DIGITAL_SIGNATURES:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getSignerIdentifier()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    move-object v2, v3

    :goto_1
    if-eqz v2, :cond_4

    .line 32
    invoke-static {}, Lcom/pspdfkit/signatures/SignatureManager;->getSigners()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/signatures/signers/Signer;

    goto :goto_2

    :cond_4
    move-object v2, v3

    .line 34
    :goto_2
    iget-object v4, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v4}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 36
    invoke-interface {v4}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 37
    iget-object v4, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v4, v1}, Lcom/pspdfkit/ui/PdfFragment;->setSelectedAnnotation(Lcom/pspdfkit/annotations/Annotation;)V

    .line 38
    iget-object v4, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v4, v1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 39
    iget-object v4, p0, Lcom/pspdfkit/internal/pr;->j:Lcom/pspdfkit/internal/fl;

    invoke-static {v1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_5
    if-eqz v2, :cond_7

    .line 40
    iget-object v4, p0, Lcom/pspdfkit/internal/pr;->h:Lcom/pspdfkit/listeners/DocumentSigningListener;

    if-nez v4, :cond_6

    move-object v4, p0

    .line 41
    :cond_6
    iput-object v1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    .line 43
    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 44
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    new-instance v5, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;

    iget-object v6, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    .line 45
    invoke-virtual {v0}, Lcom/pspdfkit/forms/SignatureFormElement;->getFormField()Lcom/pspdfkit/forms/SignatureFormField;

    move-result-object v0

    invoke-direct {v5, v6, v0, v2}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/forms/SignatureFormField;Lcom/pspdfkit/signatures/signers/Signer;)V

    .line 46
    invoke-virtual {p1}, Lcom/pspdfkit/signatures/Signature;->getBiometricData()Lcom/pspdfkit/signatures/BiometricSignatureData;

    move-result-object p1

    invoke-virtual {v5, p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->biometricSignatureData(Lcom/pspdfkit/signatures/BiometricSignatureData;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 49
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAutosaveEnabled()Z

    move-result v0

    .line 50
    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->saveDocumentBeforeSigning(Z)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 52
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureAppearance()Lcom/pspdfkit/signatures/SignatureAppearance;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureAppearance(Lcom/pspdfkit/signatures/SignatureAppearance;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 53
    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureMetadata()Lcom/pspdfkit/signatures/SignatureMetadata;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->signatureMetadata(Lcom/pspdfkit/signatures/SignatureMetadata;)Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options$Builder;->build()Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;

    move-result-object p1

    .line 55
    invoke-static {v1, p1, v4}, Lcom/pspdfkit/ui/signatures/SignatureSignerDialog;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/signatures/SignatureSignerDialog$Options;Lcom/pspdfkit/listeners/DocumentSigningListener;)V

    .line 68
    iput-object v3, p0, Lcom/pspdfkit/internal/pr;->f:Lcom/pspdfkit/forms/SignatureFormElement;

    :cond_7
    return-void
.end method

.method public synthetic onSignatureUiDataCollected(Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener$-CC;->$default$onSignatureUiDataCollected(Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/signatures/Signature;Lcom/pspdfkit/ui/signatures/SignatureUiData;)V

    return-void
.end method

.method public final onSigningCancelled()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    invoke-interface {v0, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/pr;->b:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/pr;->g:Lcom/pspdfkit/annotations/Annotation;

    :cond_0
    return-void
.end method

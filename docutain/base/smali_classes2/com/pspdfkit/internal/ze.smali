.class public final Lcom/pspdfkit/internal/ze;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/vf;


# direct methods
.method public static synthetic $r8$lambda$uKY2lsXo9Sg0gJfwuMq9H3x8DS8(Lcom/pspdfkit/internal/ze;Ljava/lang/String;Lcom/pspdfkit/internal/kf;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ze;->a(Ljava/lang/String;Lcom/pspdfkit/internal/kf;Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/ze;)Lcom/pspdfkit/internal/vf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/vf;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/pspdfkit/internal/kf;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/pspdfkit/internal/ye;

    invoke-direct {v0, p0, p3, p1}, Lcom/pspdfkit/internal/ye;-><init>(Lcom/pspdfkit/internal/ze;Lio/reactivex/rxjava3/core/CompletableEmitter;Ljava/lang/String;)V

    .line 75
    iget-object p1, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ef;->a(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 76
    iget-object p1, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    .line 77
    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object p1

    .line 78
    invoke-virtual {p2}, Lcom/pspdfkit/internal/kf;->c()Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->updateAuthenticationToken(Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "JWT"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/vf;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/kf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/kf;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/pspdfkit/internal/ze;->a:Lcom/pspdfkit/internal/vf;

    .line 56
    invoke-virtual {v1}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getJWT()Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {v1}, Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;->rawValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 64
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/ze$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/ze$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ze;Ljava/lang/String;Lcom/pspdfkit/internal/kf;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1
    :try_end_0
    .catch Lcom/pspdfkit/instant/exceptions/InstantException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 73
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

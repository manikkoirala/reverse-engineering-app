.class public final Lcom/pspdfkit/internal/wb;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/wb$a;
    }
.end annotation


# direct methods
.method public static final a(Lcom/pspdfkit/forms/ComboBoxFormElement;Landroid/content/ContentResolver;)I
    .locals 1

    const-string v0, "formElement"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentResolver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/pspdfkit/forms/ComboBoxFormElement;->isSpellCheckEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x8000

    goto :goto_0

    :cond_0
    const/high16 v0, 0x80000

    :goto_0
    invoke-static {p0}, Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/FormElement;)Lcom/pspdfkit/forms/TextInputFormat;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/wb;->a(Lcom/pspdfkit/forms/TextInputFormat;Landroid/content/ContentResolver;)I

    move-result p0

    or-int/2addr p0, v0

    return p0
.end method

.method public static final a(Lcom/pspdfkit/forms/TextFormElement;Landroid/content/ContentResolver;)I
    .locals 3

    const-string v0, "formElement"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentResolver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->isSpellCheckEnabled()Z

    move-result v0

    const/high16 v1, 0x80000

    if-eqz v0, :cond_0

    const v0, 0x8000

    goto :goto_0

    :cond_0
    const/high16 v0, 0x80000

    .line 79
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->isMultiLine()Z

    move-result v2

    if-eqz v2, :cond_1

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->isPassword()Z

    move-result v2

    if-eqz v2, :cond_2

    or-int/2addr v0, v1

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/forms/TextFormElement;->getInputFormat()Lcom/pspdfkit/forms/TextInputFormat;

    move-result-object p0

    const-string v1, "formElement.inputFormat"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/wb;->a(Lcom/pspdfkit/forms/TextInputFormat;Landroid/content/ContentResolver;)I

    move-result p0

    or-int/2addr p0, v0

    return p0
.end method

.method private static final a(Lcom/pspdfkit/forms/TextInputFormat;Landroid/content/ContentResolver;)I
    .locals 2

    const-string v0, "inputFormat"

    .line 1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentResolver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/pspdfkit/forms/TextInputFormat;->NUMBER:Lcom/pspdfkit/forms/TextInputFormat;

    const/4 v1, 0x1

    if-ne p0, v0, :cond_0

    .line 75
    invoke-static {}, Lcom/pspdfkit/internal/v;->d()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    const-string v0, "default_input_method"

    .line 76
    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "com.sec.android.inputmethod/.SamsungKeypad"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return v1

    .line 77
    :cond_1
    sget-object p1, Lcom/pspdfkit/internal/wb$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, p1, p0

    if-eq p0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0x2002

    :goto_1
    return v1
.end method

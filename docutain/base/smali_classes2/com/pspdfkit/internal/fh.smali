.class public final Lcom/pspdfkit/internal/fh;
.super Lcom/pspdfkit/internal/i4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/i4<",
        "Lcom/pspdfkit/internal/gh;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/i4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->LINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method protected final f()Lcom/pspdfkit/internal/b2;
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/gh;

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v4

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v5

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/gh;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Landroidx/core/util/Pair;)V

    return-object v7
.end method

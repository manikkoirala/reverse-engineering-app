.class final Lcom/pspdfkit/internal/k7;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:Lcom/pspdfkit/utils/Size;

.field private final c:Z

.field private final d:Z

.field private e:I


# direct methods
.method public static synthetic $r8$lambda$kQHXnBg10-eCISq1kE5RVWU8-tg(Lio/reactivex/rxjava3/disposables/Disposable;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k7;->a(Lio/reactivex/rxjava3/disposables/Disposable;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    return-void
.end method

.method public static synthetic $r8$lambda$p-pq4JdeNkXnQV-TGJDsukSHTtg(Lcom/pspdfkit/internal/jni/NativePrintConfiguration;Landroid/os/ParcelFileDescriptor;Lio/reactivex/rxjava3/core/FlowableEmitter;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/k7;->a(Lcom/pspdfkit/internal/jni/NativePrintConfiguration;Landroid/os/ParcelFileDescriptor;Lio/reactivex/rxjava3/core/FlowableEmitter;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;IZZ)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/k7;->a:Lcom/pspdfkit/internal/zf;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/k7;->b:Lcom/pspdfkit/utils/Size;

    .line 7
    iput p3, p0, Lcom/pspdfkit/internal/k7;->e:I

    .line 8
    iput-boolean p4, p0, Lcom/pspdfkit/internal/k7;->c:Z

    .line 9
    iput-boolean p5, p0, Lcom/pspdfkit/internal/k7;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;Landroid/print/PrintAttributes;Z)V
    .locals 6

    .line 1
    invoke-static {p3, p4}, Lcom/pspdfkit/internal/k7;->a(Landroid/print/PrintAttributes;Z)I

    move-result v3

    .line 2
    invoke-virtual {p3}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result p3

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v4, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    .line 3
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/k7;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;IZZ)V

    return-void
.end method

.method private static a(Landroid/print/PrintAttributes;Z)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object v0

    const/16 v1, 0x96

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/16 v1, 0x48

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object p1

    invoke-virtual {p1}, Landroid/print/PrintAttributes$Resolution;->getHorizontalDpi()I

    move-result p1

    .line 7
    invoke-virtual {p0}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object p0

    invoke-virtual {p0}, Landroid/print/PrintAttributes$Resolution;->getVerticalDpi()I

    move-result p0

    .line 8
    invoke-static {p1, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    .line 9
    invoke-static {v1, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    return p0

    :cond_1
    return v1
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/jni/NativePrintConfiguration;Landroid/os/ParcelFileDescriptor;Lio/reactivex/rxjava3/core/FlowableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 53
    invoke-static {p2}, Lcom/pspdfkit/internal/xj;->a(Lio/reactivex/rxjava3/core/FlowableEmitter;)Lcom/pspdfkit/internal/jni/NativeProcessorDelegate;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/sl;

    new-instance v1, Ljava/io/FileOutputStream;

    .line 55
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 56
    invoke-static {p0, p2, v0}, Lcom/pspdfkit/internal/jni/NativePrintProcessor;->asyncGenerateToDataSink(Lcom/pspdfkit/internal/jni/NativePrintConfiguration;Lcom/pspdfkit/internal/jni/NativeProcessorDelegate;Lcom/pspdfkit/internal/jni/NativeDataSink;)V

    return-void
.end method

.method private static synthetic a(Lio/reactivex/rxjava3/disposables/Disposable;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 0

    .line 57
    invoke-interface {p0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 58
    invoke-virtual {p1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    return-void
.end method


# virtual methods
.method public final a([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 6

    .line 10
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 11
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    .line 12
    invoke-virtual {v3}, Landroid/print/PageRange;->getStart()I

    move-result v4

    :goto_1
    invoke-virtual {v3}, Landroid/print/PageRange;->getEnd()I

    move-result v5

    if-gt v4, v5, :cond_0

    .line 13
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 17
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/k7;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativePrintConfiguration;

    move-result-object v1

    .line 18
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setPagesToPrint(Ljava/util/HashSet;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/k7;->b:Lcom/pspdfkit/utils/Size;

    iget v2, v0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v2, v2

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setMediaSize(II)V

    .line 20
    iget v0, p0, Lcom/pspdfkit/internal/k7;->e:I

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setDensity(I)V

    .line 21
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k7;->c:Z

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setMonochrome(Z)V

    .line 22
    iget-boolean v0, p0, Lcom/pspdfkit/internal/k7;->d:Z

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setPreview(Z)V

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yl;->c()Lcom/pspdfkit/internal/jni/NativePageCache;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativePrintConfiguration;->setCache(Lcom/pspdfkit/internal/jni/NativePageCache;)V

    .line 27
    new-instance v0, Lcom/pspdfkit/internal/k7$$ExternalSyntheticLambda0;

    invoke-direct {v0, v1, p2}, Lcom/pspdfkit/internal/k7$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/jni/NativePrintConfiguration;Landroid/os/ParcelFileDescriptor;)V

    sget-object p2, Lio/reactivex/rxjava3/core/BackpressureStrategy;->LATEST:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    invoke-static {v0, p2}, Lio/reactivex/rxjava3/core/Flowable;->create(Lio/reactivex/rxjava3/core/FlowableOnSubscribe;Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 35
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Flowable;->ignoreElements()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    .line 36
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/k7$a;

    invoke-direct {v0, p4, p1}, Lcom/pspdfkit/internal/k7$a;-><init>(Landroid/print/PrintDocumentAdapter$WriteResultCallback;[Landroid/print/PageRange;)V

    .line 37
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeWith(Lio/reactivex/rxjava3/core/CompletableObserver;)Lio/reactivex/rxjava3/core/CompletableObserver;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    .line 52
    new-instance p2, Lcom/pspdfkit/internal/k7$$ExternalSyntheticLambda1;

    invoke-direct {p2, p1, p4}, Lcom/pspdfkit/internal/k7$$ExternalSyntheticLambda1;-><init>(Lio/reactivex/rxjava3/disposables/Disposable;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    invoke-virtual {p3, p2}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    return-void
.end method

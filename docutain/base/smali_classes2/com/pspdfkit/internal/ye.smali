.class final Lcom/pspdfkit/internal/ye;
.super Lcom/pspdfkit/instant/listeners/SimpleInstantDocumentListener;
.source "SourceFile"


# instance fields
.field final synthetic b:Lio/reactivex/rxjava3/core/CompletableEmitter;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/pspdfkit/internal/ze;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ze;Lio/reactivex/rxjava3/core/CompletableEmitter;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ye;->d:Lcom/pspdfkit/internal/ze;

    iput-object p2, p0, Lcom/pspdfkit/internal/ye;->b:Lio/reactivex/rxjava3/core/CompletableEmitter;

    iput-object p3, p0, Lcom/pspdfkit/internal/ye;->c:Ljava/lang/String;

    invoke-direct {p0}, Lcom/pspdfkit/instant/listeners/SimpleInstantDocumentListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->d:Lcom/pspdfkit/internal/ze;

    invoke-static {p1}, Lcom/pspdfkit/internal/ze;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ze;)Lcom/pspdfkit/internal/vf;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ef;->b(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->b:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->b:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1, p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public final onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->d:Lcom/pspdfkit/internal/ze;

    invoke-static {p1}, Lcom/pspdfkit/internal/ze;->-$$Nest$fgeta(Lcom/pspdfkit/internal/ze;)Lcom/pspdfkit/internal/vf;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ef;->b(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->b:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ye;->b:Lio/reactivex/rxjava3/core/CompletableEmitter;

    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    :cond_0
    return-void
.end method

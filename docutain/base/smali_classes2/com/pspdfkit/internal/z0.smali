.class public final Lcom/pspdfkit/internal/z0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/p1;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/p1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/p1;)Lcom/pspdfkit/internal/z0;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/z0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/z0;-><init>(Lcom/pspdfkit/internal/p1;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/mb;)I
    .locals 6

    const-string v0, ""

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    .line 5
    sget v1, Lcom/pspdfkit/internal/m1;->e:I

    const/16 v1, 0x4b

    .line 6
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/p1;->b()Lcom/pspdfkit/internal/kl;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 9
    iget-object v3, p0, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/16 v3, 0xbb8

    if-eq v2, v3, :cond_c

    const/16 v3, 0xbb9

    if-eq v2, v3, :cond_b

    const/16 v3, 0x1771

    if-eq v2, v3, :cond_a

    const/16 v3, 0x1772

    if-eq v2, v3, :cond_9

    const-wide/16 v3, 0x0

    const/16 v5, 0x8

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 10
    :pswitch_0
    invoke-virtual {p1, v5, v5}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 11
    invoke-virtual {p1, v3, v4}, Lcom/pspdfkit/internal/mb;->a(J)V

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v2

    const/16 v3, 0x32

    .line 13
    invoke-virtual {p1, v3, v2}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto :goto_0

    .line 14
    :pswitch_1
    invoke-virtual {p1, v5, v5}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 15
    invoke-virtual {p1, v3, v4}, Lcom/pspdfkit/internal/mb;->a(J)V

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v2

    const/16 v3, 0x31

    .line 17
    invoke-virtual {p1, v3, v2}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x34

    .line 18
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_0

    :pswitch_3
    const/16 v2, 0x2c

    .line 19
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_0

    :pswitch_4
    const/16 v2, 0x2d

    .line 20
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_0

    :pswitch_5
    const/16 v2, 0x2b

    .line 21
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_0

    :pswitch_6
    const/16 v2, 0x30

    .line 22
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_0

    :goto_1
    const/16 v3, 0xf

    if-eq v2, v3, :cond_8

    const/16 v3, 0x16

    if-eq v2, v3, :cond_7

    const/16 v3, 0x1a

    if-eq v2, v3, :cond_6

    const/16 v3, 0x3e9

    if-eq v2, v3, :cond_5

    const/16 v3, 0xfa0

    if-eq v2, v3, :cond_4

    const/16 v3, 0x1f42

    if-eq v2, v3, :cond_3

    const/16 v3, 0x2329

    if-eq v2, v3, :cond_2

    const/16 v3, 0x2afa

    if-eq v2, v3, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v2, 0x49

    .line 23
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x5

    .line 24
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_3
    const/16 v2, 0x47

    .line 25
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0xe

    .line 26
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_5
    const/16 v2, 0x1d

    .line 27
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_6
    const/16 v2, 0x28

    .line 28
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    .line 29
    :cond_7
    invoke-virtual {p1, v5, v0}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_0

    :cond_8
    const/16 v2, 0x17

    .line 30
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_9
    const/16 v2, 0x2e

    .line 31
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_a
    const/16 v2, 0x2f

    .line 32
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_b
    const/16 v2, 0xd

    .line 33
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    :cond_c
    const/16 v2, 0xc

    .line 34
    invoke-virtual {p1, v2, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_0

    .line 35
    :cond_d
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p1

    return p1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/pspdfkit/internal/mb;)I
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 1
    new-instance v3, Landroidx/collection/SparseArrayCompat;

    invoke-direct {v3}, Landroidx/collection/SparseArrayCompat;-><init>()V

    .line 2
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p1;->b()Lcom/pspdfkit/internal/kl;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/16 v6, 0xbb8

    const/16 v7, 0xfa0

    const/16 v8, 0x1a

    const/16 v9, 0xf

    const/16 v10, 0x9

    const/4 v11, 0x6

    const/4 v12, 0x3

    const/4 v15, 0x5

    const/4 v13, 0x2

    const/4 v14, 0x4

    const/4 v5, 0x0

    if-eqz v0, :cond_17

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    .line 3
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v13, :cond_14

    if-eq v0, v12, :cond_14

    if-eq v0, v14, :cond_14

    if-eq v0, v15, :cond_14

    if-eq v0, v11, :cond_14

    if-eq v0, v10, :cond_13

    if-eq v0, v9, :cond_f

    if-eq v0, v8, :cond_14

    const/16 v8, 0x64

    if-eq v0, v8, :cond_b

    const/16 v8, 0x3e9

    if-eq v0, v8, :cond_14

    if-eq v0, v7, :cond_14

    const/16 v7, 0x1389

    if-eq v0, v7, :cond_9

    const/16 v7, 0x1b5a

    if-eq v0, v7, :cond_14

    const/16 v7, 0x1f42

    if-eq v0, v7, :cond_14

    const/16 v7, 0x2329

    if-eq v0, v7, :cond_8

    const/16 v7, 0x66

    if-eq v0, v7, :cond_5

    const/16 v7, 0x67

    if-eq v0, v7, :cond_3

    if-eq v0, v6, :cond_2

    const/16 v6, 0xbb9

    if-eq v0, v6, :cond_1

    const/16 v6, 0x1771

    if-eq v0, v6, :cond_14

    const/16 v6, 0x1772

    if-eq v0, v6, :cond_14

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_a

    .line 113
    :cond_1
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Lcom/pspdfkit/internal/o;

    invoke-virtual {v0, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/o;

    if-eqz v0, :cond_15

    .line 116
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 117
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/o;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v0

    .line 118
    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 119
    :cond_2
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Lcom/pspdfkit/annotations/actions/Action;

    invoke-virtual {v0, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/actions/Action;

    if-eqz v0, :cond_15

    .line 122
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v0, v2}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v0

    .line 123
    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 159
    :cond_3
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Ljava/lang/Object;

    invoke-virtual {v0, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 161
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 162
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    sget v8, Lcom/pspdfkit/internal/m1;->e:I

    const/16 v8, 0x8

    .line 163
    invoke-virtual {v2, v8, v7, v14}, Lcom/pspdfkit/internal/mb;->a(III)V

    .line 164
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    const/4 v9, 0x1

    sub-int/2addr v7, v9

    :goto_1
    if-ltz v7, :cond_4

    .line 165
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    .line 166
    iget v10, v9, Landroid/graphics/PointF;->x:F

    iget v9, v9, Landroid/graphics/PointF;->y:F

    .line 167
    invoke-virtual {v2, v14, v8}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 168
    invoke-virtual {v2, v9}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 169
    invoke-virtual {v2, v10}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 170
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    add-int/lit8 v7, v7, -0x1

    const/16 v8, 0x8

    goto :goto_1

    .line 171
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 172
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 173
    :cond_5
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 174
    const-class v6, Ljava/util/ArrayList;

    invoke-virtual {v0, v7, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 176
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 177
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    new-array v8, v7, [S

    const/4 v9, 0x0

    .line 178
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_6

    .line 179
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v10}, Ljava/lang/Enum;->ordinal()I

    move-result v10

    int-to-short v10, v10

    aput-short v10, v8, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 181
    :cond_6
    sget v0, Lcom/pspdfkit/internal/m1;->e:I

    .line 182
    invoke-virtual {v2, v13, v7, v13}, Lcom/pspdfkit/internal/mb;->a(III)V

    add-int/lit8 v7, v7, -0x1

    :goto_3
    if-ltz v7, :cond_7

    aget-short v0, v8, v7

    .line 183
    invoke-virtual {v2, v13, v5}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->a(S)V

    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 184
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 185
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 75
    :cond_8
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Lorg/json/JSONObject;

    invoke-virtual {v0, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    if-eqz v0, :cond_15

    .line 78
    :try_start_0
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 81
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 82
    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    sget v7, Lcom/pspdfkit/internal/ng;->e:I

    const/4 v7, 0x1

    .line 83
    invoke-virtual {v2, v7}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 84
    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v0

    .line 86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 87
    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_a

    :catch_0
    move-exception v0

    new-array v6, v5, [Ljava/lang/Object;

    const-string v7, "PSPDFKit.Annotations"

    const-string v8, "Can\'t serialize annotation custom data to string"

    .line 92
    invoke-static {v7, v0, v8, v6}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_a

    .line 93
    :cond_9
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Ljava/lang/Object;

    invoke-virtual {v0, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 95
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 96
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    sget v8, Lcom/pspdfkit/internal/m1;->e:I

    const/16 v8, 0x20

    .line 97
    invoke-virtual {v2, v8, v7, v14}, Lcom/pspdfkit/internal/mb;->a(III)V

    .line 98
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    sub-int/2addr v7, v8

    :goto_4
    if-ltz v7, :cond_a

    .line 99
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/ho;

    .line 100
    iget v9, v8, Lcom/pspdfkit/internal/ho;->a:F

    iget v10, v8, Lcom/pspdfkit/internal/ho;->b:F

    iget v11, v8, Lcom/pspdfkit/internal/ho;->c:F

    iget v12, v8, Lcom/pspdfkit/internal/ho;->d:F

    iget v13, v8, Lcom/pspdfkit/internal/ho;->e:F

    iget v15, v8, Lcom/pspdfkit/internal/ho;->f:F

    iget v5, v8, Lcom/pspdfkit/internal/ho;->g:F

    iget v8, v8, Lcom/pspdfkit/internal/ho;->h:F

    move-object/from16 v16, v0

    const/16 v0, 0x20

    .line 101
    invoke-virtual {v2, v14, v0}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 102
    invoke-virtual {v2, v8}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 103
    invoke-virtual {v2, v5}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 104
    invoke-virtual {v2, v15}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 105
    invoke-virtual {v2, v13}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 106
    invoke-virtual {v2, v12}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 107
    invoke-virtual {v2, v11}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 108
    invoke-virtual {v2, v10}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 109
    invoke-virtual {v2, v9}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 110
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v16

    const/4 v5, 0x0

    goto :goto_4

    .line 111
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 112
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 186
    :cond_b
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const-class v6, Ljava/lang/Object;

    invoke-virtual {v0, v5, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 188
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 189
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, v6, [I

    .line 191
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v8, 0x0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 192
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    sget v11, Lcom/pspdfkit/internal/bh;->e:I

    const/16 v11, 0xc

    .line 193
    invoke-virtual {v2, v11, v10, v14}, Lcom/pspdfkit/internal/mb;->a(III)V

    .line 194
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    const/4 v12, 0x1

    sub-int/2addr v10, v12

    :goto_6
    if-ltz v10, :cond_c

    .line 195
    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/PointF;

    .line 196
    iget v13, v12, Landroid/graphics/PointF;->x:F

    iget v12, v12, Landroid/graphics/PointF;->y:F

    .line 197
    invoke-virtual {v2, v14, v11}, Lcom/pspdfkit/internal/mb;->d(II)V

    const/4 v11, 0x0

    .line 198
    invoke-virtual {v2, v11}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 199
    invoke-virtual {v2, v12}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 200
    invoke-virtual {v2, v13}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 201
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    add-int/lit8 v10, v10, -0x1

    const/16 v11, 0xc

    goto :goto_6

    .line 202
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v9

    const/4 v10, 0x1

    .line 203
    invoke-virtual {v2, v10}, Lcom/pspdfkit/internal/mb;->e(I)V

    const/4 v11, 0x0

    .line 204
    invoke-virtual {v2, v11, v9}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 205
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v9

    .line 206
    aput v9, v7, v8

    add-int/2addr v8, v10

    goto :goto_5

    .line 210
    :cond_d
    sget v0, Lcom/pspdfkit/internal/m1;->e:I

    .line 211
    invoke-virtual {v2, v14, v6, v14}, Lcom/pspdfkit/internal/mb;->a(III)V

    add-int/lit8 v6, v6, -0x1

    :goto_7
    if-ltz v6, :cond_e

    aget v0, v7, v6

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v6, v6, -0x1

    goto :goto_7

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 212
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_a

    .line 213
    :cond_f
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    const-class v7, Ljava/util/List;

    invoke-virtual {v0, v5, v7, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 214
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 215
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, v6, [I

    const/4 v8, 0x0

    .line 216
    :goto_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_11

    .line 217
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_10

    .line 218
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v7, v8

    :cond_10
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 222
    :cond_11
    sget v0, Lcom/pspdfkit/internal/m1;->e:I

    .line 223
    invoke-virtual {v2, v14, v6, v14}, Lcom/pspdfkit/internal/mb;->a(III)V

    add-int/lit8 v6, v6, -0x1

    :goto_9
    if-ltz v6, :cond_12

    aget v0, v7, v6

    const/4 v8, 0x0

    .line 224
    invoke-virtual {v2, v14, v8}, Lcom/pspdfkit/internal/mb;->d(II)V

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->c(I)V

    add-int/lit8 v6, v6, -0x1

    goto :goto_9

    .line 225
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 226
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_a

    .line 227
    :cond_13
    :pswitch_0
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 228
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-class v7, Landroid/graphics/RectF;

    invoke-virtual {v5, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    invoke-static {v5, v2}, Lcom/pspdfkit/internal/nb;->a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v5

    .line 229
    invoke-virtual {v3, v0, v5}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_a

    .line 230
    :cond_14
    :pswitch_1
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 232
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 305
    :cond_15
    :goto_a
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroidx/collection/SparseArrayCompat;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_16

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 306
    :cond_16
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 307
    :cond_17
    sget v0, Lcom/pspdfkit/internal/m1;->e:I

    const/16 v0, 0x4b

    .line 308
    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 309
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->c()V

    .line 316
    iget-object v0, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p1;->b()Lcom/pspdfkit/internal/kl;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_32

    const/4 v5, 0x1

    if-eq v4, v5, :cond_32

    if-eq v4, v13, :cond_31

    if-eq v4, v12, :cond_30

    if-eq v4, v14, :cond_2f

    if-eq v4, v15, :cond_2e

    if-eq v4, v11, :cond_2d

    const/4 v5, 0x7

    const/16 v17, 0x0

    if-eq v4, v5, :cond_25

    if-eq v4, v6, :cond_2b

    const/16 v5, 0xbb9

    if-eq v4, v5, :cond_2a

    if-eq v4, v7, :cond_29

    const/16 v5, 0xfa1

    if-eq v4, v5, :cond_28

    const/16 v5, 0x1771

    if-eq v4, v5, :cond_27

    const/16 v5, 0x1772

    if-eq v4, v5, :cond_26

    const-wide/16 v19, 0x0

    const/16 v5, 0x10

    sparse-switch v4, :sswitch_data_0

    packed-switch v4, :pswitch_data_1

    packed-switch v4, :pswitch_data_2

    packed-switch v4, :pswitch_data_3

    packed-switch v4, :pswitch_data_4

    packed-switch v4, :pswitch_data_5

    packed-switch v4, :pswitch_data_6

    .line 643
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 644
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "Field implementation missing (%d), implement field conversion!"

    .line 645
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :pswitch_2
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 667
    const-class v6, Lcom/pspdfkit/annotations/sound/AudioEncoding;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/sound/AudioEncoding;

    sget-object v5, Lcom/pspdfkit/annotations/sound/AudioEncoding;->SIGNED:Lcom/pspdfkit/annotations/sound/AudioEncoding;

    if-eqz v4, :cond_18

    .line 668
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    goto :goto_c

    :cond_18
    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    :goto_c
    int-to-short v4, v4

    const/16 v5, 0x3c

    .line 669
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_f

    .line 670
    :pswitch_3
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3a

    .line 671
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_f

    :pswitch_4
    const/4 v6, 0x0

    .line 672
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x39

    .line 673
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_f

    :pswitch_5
    const/4 v6, 0x0

    .line 674
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3b

    .line 675
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_f

    .line 679
    :pswitch_6
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 680
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->b(I)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/16 v5, 0x48

    const/4 v6, 0x0

    .line 681
    invoke-virtual {v2, v5, v4, v6}, Lcom/pspdfkit/internal/mb;->a(IZZ)V

    goto/16 :goto_f

    .line 682
    :pswitch_7
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 683
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x47

    .line 684
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_f

    .line 685
    :pswitch_8
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 686
    const-class v6, Ljava/lang/Integer;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 687
    check-cast v4, Ljava/lang/Integer;

    .line 688
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_1c

    .line 690
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x46

    .line 691
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_f

    .line 692
    :pswitch_9
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3e

    .line 693
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_f

    .line 694
    :pswitch_a
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3d

    .line 695
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_f

    .line 696
    :pswitch_b
    iget-object v4, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 697
    const-class v6, Ljava/util/EnumSet;

    invoke-virtual {v4, v5, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/EnumSet;

    if-nez v4, :cond_19

    goto :goto_e

    .line 698
    :cond_19
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1a

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    goto :goto_e

    .line 700
    :cond_1a
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Enum;

    .line 701
    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    const/4 v6, 0x1

    shl-int v5, v6, v5

    int-to-long v5, v5

    or-long v19, v19, v5

    goto :goto_d

    .line 703
    :cond_1b
    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    :goto_e
    if-eqz v17, :cond_1c

    .line 704
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->intValue()I

    move-result v4

    const/16 v5, 0x40

    .line 705
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto :goto_f

    .line 706
    :pswitch_c
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    sget-object v6, Lcom/pspdfkit/annotations/MediaWindowType;->USE_ANNOTATION_RECTANGLE:Lcom/pspdfkit/annotations/MediaWindowType;

    .line 708
    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3f

    .line 709
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    :cond_1c
    :goto_f
    const/16 v6, 0xbb8

    goto/16 :goto_b

    .line 731
    :pswitch_d
    iget-object v6, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const-class v7, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-virtual {v6, v4, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/utils/EdgeInsets;

    if-eqz v4, :cond_1d

    .line 732
    iget v6, v4, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget v7, v4, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget v11, v4, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    iget v4, v4, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    .line 733
    invoke-virtual {v2, v14, v5}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 734
    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 735
    invoke-virtual {v2, v11}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 736
    invoke-virtual {v2, v7}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 737
    invoke-virtual {v2, v6}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 738
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    const/16 v5, 0x23

    .line 739
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    :cond_1d
    const/16 v6, 0xbb8

    const/16 v7, 0xfa0

    goto/16 :goto_b

    .line 740
    :pswitch_e
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v6, 0x0

    .line 741
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    const-class v11, Ljava/lang/Byte;

    invoke-virtual {v5, v4, v11, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    const/16 v5, 0x1f

    .line 742
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IB)V

    goto/16 :goto_16

    :pswitch_f
    const/4 v6, 0x0

    .line 743
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 744
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    const-class v6, Ljava/lang/Byte;

    invoke-virtual {v5, v4, v6, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    const/16 v6, 0x20

    .line 745
    invoke-virtual {v2, v6, v4}, Lcom/pspdfkit/internal/mb;->a(IB)V

    goto/16 :goto_16

    :pswitch_10
    const/16 v6, 0x20

    .line 746
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 747
    const-class v7, Ljava/lang/Integer;

    invoke-virtual {v5, v4, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 748
    check-cast v4, Ljava/lang/Integer;

    .line 749
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 751
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x13

    .line 752
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    :pswitch_11
    const/16 v6, 0x20

    .line 753
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7}, Lcom/pspdfkit/internal/p1;->a(IF)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/16 v5, 0x1e

    .line 754
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IF)V

    goto/16 :goto_16

    :pswitch_12
    const/16 v6, 0x20

    .line 755
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 756
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x1d

    .line 757
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    :pswitch_13
    const/16 v6, 0x20

    .line 758
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v7, 0x0

    .line 759
    invoke-virtual {v5, v4, v7}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-byte v4, v4

    const/16 v5, 0x37

    .line 760
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IB)V

    goto/16 :goto_16

    :pswitch_14
    const/16 v6, 0x20

    const/4 v7, 0x0

    .line 761
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 762
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    const-class v6, Ljava/lang/Byte;

    invoke-virtual {v5, v4, v6, v11}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    const/16 v5, 0x27

    .line 763
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IB)V

    goto/16 :goto_16

    :pswitch_15
    const/4 v7, 0x0

    .line 764
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    const-class v7, Ljava/lang/Byte;

    invoke-virtual {v5, v4, v7, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    const/16 v5, 0x26

    .line 765
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IB)V

    goto/16 :goto_16

    .line 766
    :pswitch_16
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xa

    .line 767
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 768
    :pswitch_17
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x24

    .line 769
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 770
    :pswitch_18
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(IF)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/16 v5, 0x14

    .line 771
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IF)V

    goto/16 :goto_16

    :pswitch_19
    const/4 v6, 0x0

    .line 772
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xb

    .line 773
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    :sswitch_0
    const/4 v6, 0x0

    .line 646
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 647
    const-class v7, Lcom/pspdfkit/annotations/measurements/Scale;

    invoke-virtual {v5, v4, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/measurements/Scale;

    if-nez v4, :cond_1e

    goto :goto_10

    .line 648
    :cond_1e
    iget-object v5, v4, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 650
    invoke-static {v5}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Short;->shortValue()S

    move-result v5

    iget-object v7, v4, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 651
    invoke-static {v7}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Short;->shortValue()S

    move-result v7

    iget v11, v4, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    iget v4, v4, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    const/16 v6, 0xc

    .line 652
    invoke-virtual {v2, v14, v6}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 653
    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 654
    invoke-virtual {v2, v11}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 655
    invoke-virtual {v2, v7}, Lcom/pspdfkit/internal/mb;->a(S)V

    .line 656
    invoke-virtual {v2, v5}, Lcom/pspdfkit/internal/mb;->a(S)V

    .line 657
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    .line 658
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    :goto_10
    if-eqz v17, :cond_33

    .line 659
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x49

    .line 660
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 661
    :sswitch_1
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 662
    const-class v6, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-static {v4}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 664
    invoke-virtual {v4}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x4a

    .line 665
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IS)V

    goto/16 :goto_16

    .line 676
    :sswitch_2
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 677
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 678
    invoke-virtual {v2, v15, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 716
    :sswitch_3
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 717
    invoke-virtual {v2, v10, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 729
    :sswitch_4
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->b(I)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/16 v5, 0x36

    const/4 v6, 0x0

    .line 730
    invoke-virtual {v2, v5, v4, v6}, Lcom/pspdfkit/internal/mb;->a(IZZ)V

    goto/16 :goto_16

    .line 774
    :sswitch_5
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 775
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x28

    .line 776
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 777
    :sswitch_6
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const-class v6, Ljava/lang/Float;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    if-eqz v4, :cond_33

    .line 779
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 780
    invoke-virtual {v2, v8, v4}, Lcom/pspdfkit/internal/mb;->a(IF)V

    goto/16 :goto_16

    .line 781
    :sswitch_7
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 782
    const-class v6, Lcom/pspdfkit/annotations/BorderEffect;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/BorderEffect;

    invoke-static {v4}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 784
    invoke-virtual {v4}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x19

    .line 785
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IS)V

    goto/16 :goto_16

    .line 786
    :sswitch_8
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 787
    const-class v6, Lcom/pspdfkit/annotations/BlendMode;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/BlendMode;

    invoke-static {v4}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 789
    invoke-virtual {v4}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x16

    .line 790
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IS)V

    goto/16 :goto_16

    .line 791
    :sswitch_9
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 792
    const-class v6, Landroid/graphics/RectF;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 794
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x8

    .line 795
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 796
    :sswitch_a
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 797
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x42

    .line 798
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 799
    :sswitch_b
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 800
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 801
    invoke-virtual {v2, v14, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 802
    :sswitch_c
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 805
    const-class v6, Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/note/AuthorState;

    sget-object v5, Lcom/pspdfkit/annotations/note/AuthorState;->NONE:Lcom/pspdfkit/annotations/note/AuthorState;

    if-eqz v4, :cond_1f

    .line 806
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    goto :goto_11

    :cond_1f
    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    :goto_11
    int-to-short v4, v4

    const/16 v5, 0x44

    .line 807
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IS)V

    goto/16 :goto_16

    .line 808
    :sswitch_d
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 809
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 810
    invoke-virtual {v2, v13, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_16

    :sswitch_e
    const/4 v6, 0x0

    .line 811
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(II)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x43

    .line 812
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(II)V

    goto/16 :goto_16

    .line 813
    :sswitch_f
    iget-object v4, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 814
    const-class v6, Ljava/util/EnumSet;

    invoke-virtual {v4, v5, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/EnumSet;

    if-nez v4, :cond_20

    goto :goto_13

    .line 815
    :cond_20
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_21

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    goto :goto_13

    .line 817
    :cond_21
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_22

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Enum;

    .line 818
    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    const/4 v6, 0x1

    shl-int v5, v6, v5

    int-to-long v6, v5

    or-long v19, v19, v6

    goto :goto_12

    .line 820
    :cond_22
    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    :goto_13
    if-eqz v17, :cond_33

    .line 821
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v6, 0x33

    .line 822
    invoke-virtual {v2, v6, v4, v5}, Lcom/pspdfkit/internal/mb;->a(IJ)V

    goto/16 :goto_16

    .line 823
    :sswitch_10
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x17

    .line 824
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 825
    :sswitch_11
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 826
    const-class v6, Lcom/pspdfkit/annotations/BorderStyle;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/BorderStyle;

    invoke-static {v4}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Enum;)Ljava/lang/Short;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 828
    invoke-virtual {v4}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x18

    .line 829
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->a(IS)V

    goto/16 :goto_16

    .line 830
    :sswitch_12
    iget-object v6, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 831
    const-class v7, Ljava/lang/Integer;

    invoke-virtual {v6, v4, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 832
    check-cast v4, Ljava/lang/Integer;

    .line 833
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 835
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 836
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 837
    :sswitch_13
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 838
    const-class v6, Ljava/lang/Float;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 839
    check-cast v4, Ljava/lang/Float;

    if-eqz v4, :cond_23

    .line 840
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 841
    invoke-virtual {v2, v14, v14}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 842
    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/mb;->a(F)V

    .line 843
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    .line 844
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    :cond_23
    if-eqz v17, :cond_33

    .line 845
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x15

    .line 846
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 847
    :sswitch_14
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 848
    const-class v6, Ljava/lang/Integer;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 849
    check-cast v4, Ljava/lang/Integer;

    .line 850
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 852
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x11

    .line 853
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 854
    :sswitch_15
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 855
    const-class v6, Ljava/lang/Integer;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 856
    check-cast v4, Ljava/lang/Integer;

    .line 857
    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Ljava/lang/Integer;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 859
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 860
    invoke-virtual {v2, v9, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 861
    :sswitch_16
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 862
    const-class v6, Landroid/graphics/RectF;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    invoke-static {v4, v2}, Lcom/pspdfkit/internal/nb;->a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_33

    .line 864
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x7

    .line 865
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    .line 866
    :sswitch_17
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 867
    const-class v6, Ljava/util/Date;

    invoke-virtual {v5, v4, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 868
    check-cast v4, Ljava/util/Date;

    if-eqz v4, :cond_24

    .line 869
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    const/16 v6, 0x8

    .line 870
    invoke-virtual {v2, v6, v6}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 871
    invoke-virtual {v2, v4, v5}, Lcom/pspdfkit/internal/mb;->a(J)V

    .line 872
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    .line 873
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    :cond_24
    if-eqz v17, :cond_33

    .line 874
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x32

    .line 875
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    :cond_25
    :sswitch_18
    const/16 v5, 0xc

    const/4 v6, 0x0

    goto/16 :goto_14

    .line 710
    :cond_26
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 711
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x2e

    .line 712
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 713
    :cond_27
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 714
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x2f

    .line 715
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 718
    :cond_28
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->b(I)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/16 v5, 0x38

    const/4 v6, 0x0

    .line 719
    invoke-virtual {v2, v5, v4, v6}, Lcom/pspdfkit/internal/mb;->a(IZZ)V

    goto/16 :goto_16

    :cond_29
    const/4 v6, 0x0

    .line 720
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_33

    .line 721
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xe

    .line 722
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    :cond_2a
    const/4 v6, 0x0

    .line 723
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 724
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xd

    .line 725
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    :cond_2b
    const/4 v6, 0x0

    .line 726
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 727
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xc

    .line 728
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto/16 :goto_16

    .line 876
    :goto_14
    iget-object v7, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    .line 877
    const-class v11, Ljava/util/Date;

    invoke-virtual {v7, v4, v11}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 878
    check-cast v4, Ljava/util/Date;

    if-eqz v4, :cond_2c

    .line 879
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v17

    const-wide/16 v19, 0x3e8

    div-long v5, v17, v19

    const/16 v7, 0x8

    .line 880
    invoke-virtual {v2, v7, v7}, Lcom/pspdfkit/internal/mb;->d(II)V

    .line 881
    invoke-virtual {v2, v5, v6}, Lcom/pspdfkit/internal/mb;->a(J)V

    .line 882
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->d()I

    move-result v4

    .line 883
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    goto :goto_15

    :cond_2c
    const/16 v7, 0x8

    :goto_15
    if-eqz v17, :cond_33

    .line 884
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x31

    .line 885
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->c(II)V

    goto/16 :goto_16

    :cond_2d
    const/16 v7, 0x8

    .line 886
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 887
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x34

    .line 888
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_16

    :cond_2e
    const/16 v7, 0x8

    .line 889
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 890
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x2c

    .line 891
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_16

    :cond_2f
    const/16 v7, 0x8

    .line 892
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 893
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x2d

    .line 894
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_16

    :cond_30
    const/16 v7, 0x8

    .line 895
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 896
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x2b

    .line 897
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_16

    :cond_31
    const/16 v7, 0x8

    .line 898
    iget-object v5, v1, Lcom/pspdfkit/internal/z0;->a:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/p1;->a(I)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 899
    invoke-virtual {v3, v4}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x30

    .line 900
    invoke-virtual {v2, v5, v4}, Lcom/pspdfkit/internal/mb;->b(II)V

    goto :goto_16

    :cond_32
    const/16 v7, 0x8

    :cond_33
    :goto_16
    const/16 v6, 0xbb8

    const/16 v7, 0xfa0

    const/4 v11, 0x6

    goto/16 :goto_b

    .line 901
    :cond_34
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_18
        0x8 -> :sswitch_17
        0x9 -> :sswitch_16
        0xa -> :sswitch_15
        0xb -> :sswitch_14
        0xc -> :sswitch_13
        0xd -> :sswitch_12
        0xe -> :sswitch_11
        0xf -> :sswitch_10
        0x10 -> :sswitch_f
        0x11 -> :sswitch_e
        0x12 -> :sswitch_d
        0x13 -> :sswitch_c
        0x14 -> :sswitch_b
        0x15 -> :sswitch_a
        0x16 -> :sswitch_9
        0x17 -> :sswitch_8
        0x18 -> :sswitch_7
        0x19 -> :sswitch_6
        0x1a -> :sswitch_5
        0x7d0 -> :sswitch_4
        0x1389 -> :sswitch_3
        0x2329 -> :sswitch_2
        0x2af9 -> :sswitch_1
        0x2afa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x64
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3e8
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3ec
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1b58
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1f41
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x2711
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

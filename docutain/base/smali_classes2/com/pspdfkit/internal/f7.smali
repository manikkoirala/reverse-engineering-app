.class public final Lcom/pspdfkit/internal/f7;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 21
    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar:[I

    .line 22
    sget v3, Lcom/pspdfkit/R$attr;->pspdf__contextualToolbarStyle:I

    .line 23
    sget v4, Lcom/pspdfkit/R$style;->PSPDFKit_ContextualToolbar:I

    .line 24
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    .line 34
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__backgroundColor:I

    .line 35
    sget v2, Landroidx/appcompat/R$attr;->colorPrimaryDark:I

    .line 36
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_dark:I

    .line 37
    invoke-static {v1, p1, v0, v2, v3}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;III)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->a:I

    .line 46
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__borderColor:I

    .line 47
    sget v2, Landroidx/appcompat/R$attr;->colorPrimaryDark:I

    .line 48
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color_dark:I

    .line 49
    invoke-static {v1, p1, v0, v2, v3}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;III)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->b:I

    .line 58
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__submenuBackgroundColor:I

    .line 59
    sget v2, Landroidx/appcompat/R$attr;->colorPrimary:I

    .line 60
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 61
    invoke-static {v1, p1, v0, v2, v3}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;III)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->d:I

    .line 70
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__submenuBorderColor:I

    .line 71
    sget v2, Landroidx/appcompat/R$attr;->colorPrimary:I

    .line 72
    sget v3, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 73
    invoke-static {v1, p1, v0, v2, v3}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;III)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->e:I

    .line 82
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__iconsColor:I

    .line 83
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 84
    invoke-static {v1, p1, v0, v2}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->f:I

    .line 92
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__iconsColorActivated:I

    .line 93
    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 94
    invoke-static {v1, p1, v0, v2}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->g:I

    .line 102
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__ContextualToolbar_pspdf__alternateBackgroundColor:I

    .line 103
    sget v2, Lcom/pspdfkit/R$color;->pspdf__gray_10:I

    .line 104
    invoke-static {v1, p1, v0, v2}, Lcom/pspdfkit/internal/m;->a(Landroid/content/res/TypedArray;Landroid/content/Context;II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/f7;->c:I

    .line 110
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 113
    :cond_1
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_dark:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->a:I

    .line 114
    iput v0, p0, Lcom/pspdfkit/internal/f7;->b:I

    .line 115
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->d:I

    .line 116
    iput v0, p0, Lcom/pspdfkit/internal/f7;->e:I

    .line 117
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->f:I

    .line 118
    sget v0, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f7;->g:I

    .line 119
    sget v0, Lcom/pspdfkit/R$color;->pspdf__gray_10:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/f7;->c:I

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->c:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->a:I

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->b:I

    return v0
.end method

.method public final d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->f:I

    return v0
.end method

.method public final e()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->g:I

    return v0
.end method

.method public final f()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->d:I

    return v0
.end method

.method public final g()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/f7;->e:I

    return v0
.end method

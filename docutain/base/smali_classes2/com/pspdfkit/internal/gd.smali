.class final Lcom/pspdfkit/internal/gd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/hd;

.field final synthetic b:I


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/hd;I)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/gd;->a:Lcom/pspdfkit/internal/hd;

    iput p2, p0, Lcom/pspdfkit/internal/gd;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Landroid/net/Uri;

    const-string v0, "uri"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/gd;->a:Lcom/pspdfkit/internal/hd;

    invoke-static {v0}, Lcom/pspdfkit/internal/hd;->a(Lcom/pspdfkit/internal/hd;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/document/DocumentSource;

    new-instance v2, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;

    invoke-direct {v2, p1}, Lcom/pspdfkit/document/providers/ContentResolverDataProvider;-><init>(Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->setCustomPdfSource(Lcom/pspdfkit/document/DocumentSource;)V

    .line 46
    iget-object p1, p0, Lcom/pspdfkit/internal/gd;->a:Lcom/pspdfkit/internal/hd;

    invoke-static {p1}, Lcom/pspdfkit/internal/hd;->a(Lcom/pspdfkit/internal/hd;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget v0, p0, Lcom/pspdfkit/internal/gd;->b:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->setPageIndex(I)V

    return-void
.end method

.class public abstract Lcom/pspdfkit/internal/g7;
.super Lcom/pspdfkit/internal/gs;
.source "SourceFile"


# instance fields
.field protected A:F

.field protected B:F

.field protected C:F

.field protected D:I

.field protected E:I

.field protected F:I

.field protected G:I

.field protected H:F

.field protected I:F

.field protected J:Z

.field protected K:Z

.field protected w:Lcom/pspdfkit/internal/rv;

.field protected x:Lcom/pspdfkit/internal/qq;

.field protected y:F

.field protected z:F


# direct methods
.method public static synthetic $r8$lambda$63YaHPdRVGD0CoKiR9GO3bTmugo(Lcom/pspdfkit/internal/g7;Landroid/graphics/RectF;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/g7;->c(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method public static synthetic $r8$lambda$9qh5fBfmkD9Obh7fpMfb6JH8CEQ(Lcom/pspdfkit/internal/g7;IIIFJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/g7;->c(IIIFJ)V

    return-void
.end method

.method public static synthetic $r8$lambda$uR4xczI5W_kkTQTgHc3_g79A8Ws(Lcom/pspdfkit/internal/g7;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/g7;->E()V

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p8}, Lcom/pspdfkit/internal/gs;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    const/high16 p2, 0x3f800000    # 1.0f

    .line 2
    iput p2, p0, Lcom/pspdfkit/internal/g7;->y:F

    const/4 p3, 0x0

    .line 15
    iput p3, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 18
    iput p3, p0, Lcom/pspdfkit/internal/g7;->E:I

    .line 31
    iput-boolean p3, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 58
    new-instance p4, Lcom/pspdfkit/internal/rv;

    invoke-direct {p4, p1, p0}, Lcom/pspdfkit/internal/rv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/ug;)V

    iput-object p4, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    .line 59
    new-instance p4, Lcom/pspdfkit/internal/qq;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p4, p1}, Lcom/pspdfkit/internal/qq;-><init>(Landroid/content/Context;)V

    iput-object p4, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    .line 62
    iput p2, p0, Lcom/pspdfkit/internal/g7;->y:F

    .line 63
    iget p1, p0, Lcom/pspdfkit/internal/g7;->D:I

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    iput p3, p0, Lcom/pspdfkit/internal/g7;->D:I

    :cond_0
    return-void
.end method

.method private synthetic E()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->H:F

    float-to-int v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/g7;->I:F

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/g7;->c(II)Z

    return-void
.end method

.method private synthetic c(IIIFJ)V
    .locals 4

    .line 26
    new-instance v0, Landroid/graphics/PointF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 p1, 0x0

    .line 28
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    .line 29
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 31
    iget p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    div-float/2addr p4, p1

    .line 32
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float p1, p1

    div-float/2addr p1, p4

    float-to-int p1, p1

    .line 33
    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float p2, p2

    div-float/2addr p2, p4

    float-to-int p2, p2

    .line 34
    new-instance p4, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    int-to-float p1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr p1, v2

    sub-float v3, v1, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    int-to-float p2, p2

    div-float/2addr p2, v2

    sub-float v2, v0, p2

    add-float/2addr v1, p1

    add-float/2addr v0, p2

    invoke-direct {p4, v3, v2, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 39
    invoke-virtual {p0, p4, p3, p5, p6}, Lcom/pspdfkit/internal/g7;->b(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method private c(Landroid/graphics/RectF;IJ)V
    .locals 2

    .line 40
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    .line 42
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 43
    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 44
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 45
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/pspdfkit/internal/g7;->b(Landroid/graphics/RectF;IJ)V

    return-void
.end method


# virtual methods
.method protected abstract C()I
.end method

.method protected abstract D()I
.end method

.method public final a(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    return p1
.end method

.method public final a(F)V
    .locals 4

    .line 41
    iget p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v0, p0, Lcom/pspdfkit/internal/ug;->c:F

    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double v0, p1

    const-wide v2, 0x3fb999999999999aL    # 0.1

    cmpg-double p1, v0, v2

    if-gez p1, :cond_0

    .line 42
    iget p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    iput p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    .line 43
    iget p1, p0, Lcom/pspdfkit/internal/g7;->B:F

    iput p1, p0, Lcom/pspdfkit/internal/g7;->z:F

    .line 44
    iget p1, p0, Lcom/pspdfkit/internal/g7;->C:F

    iput p1, p0, Lcom/pspdfkit/internal/g7;->A:F

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    .line 50
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(I)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    .line 51
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/g7;->b(Lcom/pspdfkit/internal/dm;)V

    .line 52
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/g7;->a(Lcom/pspdfkit/internal/dm;)V

    .line 53
    invoke-static {v2}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_1
    iget p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpg-float p1, p1, v1

    if-gez p1, :cond_2

    .line 58
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v0, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/g7;)V

    const-wide/16 v1, 0x32

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 p1, 0x1

    .line 59
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->J:Z

    goto :goto_1

    .line 60
    :cond_2
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    if-nez p1, :cond_3

    .line 61
    iput-boolean v0, p0, Lcom/pspdfkit/internal/g7;->J:Z

    :cond_3
    :goto_1
    return-void
.end method

.method public final a(III)V
    .locals 6

    .line 80
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/g7;->F:I

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int p1, p1

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p1

    neg-int p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    div-int/lit8 p2, p2, 0x2

    add-int v4, p2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 81
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final a(IIIFJ)V
    .locals 10

    move-object v9, p0

    .line 62
    iget v0, v9, Lcom/pspdfkit/internal/g7;->y:F

    mul-float v4, v0, p4

    const-wide/16 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v5, p5

    .line 63
    invoke-virtual/range {v0 .. v8}, Lcom/pspdfkit/internal/g7;->a(IIIFJJ)V

    return-void
.end method

.method protected final a(IIIFJJ)V
    .locals 13

    move-object v8, p0

    move/from16 v4, p3

    .line 64
    iget v0, v8, Lcom/pspdfkit/internal/g7;->D:I

    if-eq v0, v4, :cond_0

    .line 65
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ug;->k(I)V

    move-wide/from16 v9, p7

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    move-wide v9, v0

    .line 71
    :goto_0
    iget-object v11, v8, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v12, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda2;

    move-object v0, v12

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/g7;IIIFJ)V

    invoke-virtual {v11, v12, v9, v10}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final a(Landroid/graphics/RectF;IJ)V
    .locals 10

    .line 72
    iget v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    if-eq v0, p2, :cond_0

    .line 73
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->k(I)V

    const-wide/16 v0, 0x1f4

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 79
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v9, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda1;

    move-object v3, v9

    move-object v4, p0

    move-object v5, p1

    move v6, p2

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/g7$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/g7;Landroid/graphics/RectF;IJ)V

    invoke-virtual {v2, v9, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final a(Landroid/graphics/RectF;IJZ)V
    .locals 10

    .line 82
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 85
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->q()Landroid/graphics/RectF;

    move-result-object v1

    .line 88
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v2

    iget v3, p0, Lcom/pspdfkit/internal/g7;->D:I

    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 89
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result v3

    iget v4, p0, Lcom/pspdfkit/internal/g7;->D:I

    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 90
    iget v4, v0, Landroid/graphics/RectF;->left:F

    int-to-float v2, v2

    add-float/2addr v4, v2

    iput v4, v0, Landroid/graphics/RectF;->left:F

    .line 91
    iget v4, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v2

    iput v4, v0, Landroid/graphics/RectF;->right:F

    .line 92
    iget v2, v0, Landroid/graphics/RectF;->top:F

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 93
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    if-nez p5, :cond_0

    .line 95
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result p5

    if-nez p5, :cond_1

    .line 97
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result p5

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr p5, v2

    .line 98
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v1, v0

    .line 99
    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p5, p5, v0

    mul-float v1, v1, v0

    .line 100
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 101
    invoke-static {v0, p5}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 104
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->k()F

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 105
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->j()F

    move-result v1

    .line 106
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    invoke-static {v0, p5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 107
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p5

    float-to-int v2, p5

    .line 108
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    float-to-int v3, p1

    const-wide/16 v8, 0x64

    move-object v1, p0

    move v4, p2

    move-wide v6, p3

    .line 109
    invoke-virtual/range {v1 .. v9}, Lcom/pspdfkit/internal/g7;->a(IIIFJJ)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm;)V
    .locals 4

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v1

    .line 4
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result v2

    .line 5
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result v3

    add-int/2addr v3, v1

    .line 6
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/g7;->a(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 8
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g7;->J:Z

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 4

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->o()V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 18
    iget-boolean v0, p0, Lcom/pspdfkit/internal/g7;->J:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    if-nez v0, :cond_1

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    goto :goto_0

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/g7;->C()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->h()I

    move-result v3

    .line 23
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 24
    iput v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/g7;->D()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->i()I

    move-result v3

    .line 26
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 27
    iput v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    .line 30
    :goto_0
    invoke-virtual {p0, v1, v1}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    .line 31
    iget v2, p0, Lcom/pspdfkit/internal/g7;->D:I

    if-eq v0, v2, :cond_2

    iget v2, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 32
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g(I)V

    .line 33
    iput v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    .line 36
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_1
    if-ge v1, v0, :cond_4

    .line 37
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(I)Lcom/pspdfkit/internal/dm;

    move-result-object v2

    .line 38
    invoke-virtual {v2}, Landroid/view/View;->isLayoutRequested()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 39
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/g7;->b(Lcom/pspdfkit/internal/dm;)V

    .line 40
    :cond_3
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/g7;->a(Lcom/pspdfkit/internal/dm;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    return v0

    :cond_5
    return v1
.end method

.method public final b(IIIFJ)V
    .locals 9

    const-wide/16 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    .line 10
    invoke-virtual/range {v0 .. v8}, Lcom/pspdfkit/internal/g7;->a(IIIFJJ)V

    return-void
.end method

.method protected final b(Landroid/graphics/RectF;)V
    .locals 9

    .line 23
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    return-void

    .line 25
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    .line 26
    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    .line 28
    new-instance v4, Landroid/graphics/RectF;

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v3, v3

    iget v5, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v5, v5

    invoke-direct {v4, v1, v1, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 29
    iget v1, p1, Landroid/graphics/RectF;->left:F

    int-to-float v0, v0

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->left:F

    .line 30
    iget v1, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->right:F

    .line 31
    iget v0, p1, Landroid/graphics/RectF;->top:F

    int-to-float v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 32
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 34
    iget-object v3, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    iget v6, p0, Lcom/pspdfkit/internal/g7;->y:F

    const-wide/16 v7, 0x0

    move-object v5, p1

    invoke-virtual/range {v3 .. v8}, Lcom/pspdfkit/internal/rv;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;FJ)V

    return-void
.end method

.method protected final b(Landroid/graphics/RectF;IJ)V
    .locals 8

    .line 11
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    return-void

    .line 13
    :cond_0
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v0

    .line 14
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result p2

    .line 16
    new-instance v3, Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v2, v2

    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v4, v4

    invoke-direct {v3, v1, v1, v2, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 17
    iget v1, p1, Landroid/graphics/RectF;->left:F

    int-to-float v0, v0

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->left:F

    .line 18
    iget v1, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v0

    iput v1, p1, Landroid/graphics/RectF;->right:F

    .line 19
    iget v0, p1, Landroid/graphics/RectF;->top:F

    int-to-float p2, p2

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 20
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 22
    iget-object v2, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    iget v5, p0, Lcom/pspdfkit/internal/g7;->y:F

    move-object v4, p1

    move-wide v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/internal/rv;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;FJ)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/dm;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/g7;->l(I)I

    move-result v1

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/g7;->a(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    .line 6
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 7
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 9
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    return v0
.end method

.method public c(II)Z
    .locals 5

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 3
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    .line 9
    iget v3, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v4, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 11
    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    move-result v3

    add-int/2addr v3, p1

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result p1

    sub-int/2addr v3, p1

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result p1

    add-int/2addr p1, p2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result p2

    sub-int/2addr p1, p2

    .line 12
    invoke-virtual {v0, v3, p1}, Lcom/pspdfkit/internal/dm;->b(II)Landroid/graphics/RectF;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 13
    iget p2, p0, Lcom/pspdfkit/internal/g7;->y:F

    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    mul-float p2, p2, v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr p2, v0

    .line 15
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ug;->b(I)I

    move-result v0

    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    move-result v3

    sub-int/2addr v0, v3

    .line 16
    iget v3, p1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    add-int/2addr v3, v0

    .line 17
    iget v4, p1, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    add-int/2addr v4, v0

    .line 18
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-static {v3, v4, v1, v0}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result v0

    .line 20
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ug;->c(I)I

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    .line 21
    iget v3, p1, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    add-int/2addr v3, v2

    .line 22
    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int p1, p1

    add-int/2addr p1, v2

    .line 23
    iget v2, p0, Lcom/pspdfkit/internal/ug;->j:I

    invoke-static {v3, p1, v1, v2}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/internal/g7;->w:Lcom/pspdfkit/internal/rv;

    int-to-float v0, v0

    int-to-float p1, p1

    iget v2, p0, Lcom/pspdfkit/internal/g7;->y:F

    invoke-virtual {v1, v0, p1, v2, p2}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return v1
.end method

.method public final e(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    return-object p1
.end method

.method public final f()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->F:I

    neg-int v0, v0

    return v0
.end method

.method public final g()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g7;->C()I

    move-result v0

    neg-int v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final i(I)F
    .locals 0

    .line 1
    iget p1, p0, Lcom/pspdfkit/internal/g7;->y:F

    return p1
.end method

.method public final j(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->D:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/ug;->a(IZ)V

    return-void
.end method

.method public final l(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/internal/g7;->y:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    return p1
.end method

.method public final n()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/g7;->G:I

    neg-int v0, v0

    return v0
.end method

.method public final o()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g7;->D()I

    move-result v0

    neg-int v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final s()Landroid/graphics/RectF;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    iget v2, p0, Lcom/pspdfkit/internal/g7;->F:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/pspdfkit/internal/g7;->G:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 4
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 5
    iget v2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method public final w()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/g7;->J:Z

    return v0
.end method

.method public final x()V
    .locals 2

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/g7;->K:Z

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/g7;->x:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    return-void
.end method

.method public final z()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->A()V

    return-void
.end method

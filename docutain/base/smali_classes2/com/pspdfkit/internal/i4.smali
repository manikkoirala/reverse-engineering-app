.class public abstract Lcom/pspdfkit/internal/i4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;
.implements Lcom/pspdfkit/internal/el;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/internal/b2;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/k1;",
        "Lcom/pspdfkit/internal/el;",
        "Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;"
    }
.end annotation


# instance fields
.field private final A:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/pspdfkit/internal/b2;",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field private final C:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private D:Lio/reactivex/rxjava3/disposables/Disposable;

.field protected final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field protected final c:Landroid/graphics/Matrix;

.field final d:Ljava/util/ArrayList;

.field private final e:Lcom/pspdfkit/internal/hr;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field protected k:Lcom/pspdfkit/internal/zf;

.field protected l:I

.field protected m:F

.field protected n:Lcom/pspdfkit/internal/dm;

.field protected o:Lcom/pspdfkit/internal/os;

.field p:Lcom/pspdfkit/internal/b2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private q:F

.field private r:F

.field private s:J

.field private t:F

.field private u:F

.field private v:Z

.field private w:F

.field private x:Lcom/pspdfkit/internal/fa;

.field private y:Z

.field private z:Z


# direct methods
.method public static synthetic $r8$lambda$6Ys75RQ4IXaaEqRTA-zQGTaH71U(Lcom/pspdfkit/internal/i4;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->j()V

    return-void
.end method

.method public static synthetic $r8$lambda$GpoO7-qrj3QkB180K64Jh8mLAZg(Lcom/pspdfkit/internal/i4;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->l()V

    return-void
.end method

.method public static synthetic $r8$lambda$_fn-kQUXJuBxNjssFIZ0ZUiHZRo(Lcom/pspdfkit/internal/i4;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i4;->a(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    .line 13
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    .line 16
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->g:Landroid/graphics/Rect;

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->j:Landroid/graphics/Paint;

    const/4 v0, 0x0

    .line 35
    iput v0, p0, Lcom/pspdfkit/internal/i4;->m:F

    const/4 v0, 0x0

    .line 51
    iput-boolean v0, p0, Lcom/pspdfkit/internal/i4;->v:Z

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    .line 75
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 76
    iput-object p2, p0, Lcom/pspdfkit/internal/i4;->C:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 78
    invoke-static {}, Lcom/pspdfkit/internal/h4;->i()Landroid/graphics/Paint;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->h:Landroid/graphics/Paint;

    .line 79
    invoke-static {}, Lcom/pspdfkit/internal/h4;->h()Landroid/graphics/Paint;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/i4;->i:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Lcom/pspdfkit/internal/hr;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/hr;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 4

    .line 186
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 187
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 188
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/b2;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/pspdfkit/internal/i4;->m:F

    const/4 v3, 0x0

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/pspdfkit/internal/b2;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    .line 189
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    :cond_1
    return-void
.end method

.method private j()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    const/4 v1, 0x0

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm;->a(Z)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->c()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->recycle()V

    return-void
.end method

.method private synthetic l()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->d()V

    return-void
.end method

.method private o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/pspdfkit/internal/i4;->m:F

    const-wide/16 v6, 0x64

    .line 4
    invoke-virtual/range {v1 .. v7}, Lcom/pspdfkit/internal/hr;->a(Landroid/graphics/Rect;Ljava/util/ArrayList;Landroid/graphics/Matrix;FJ)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/i4;)V

    .line 5
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method protected a(FF)V
    .locals 4

    .line 158
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-static {p2, v0}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return-void

    .line 159
    :cond_1
    iput-boolean v1, p0, Lcom/pspdfkit/internal/i4;->v:Z

    .line 161
    iput p1, p0, Lcom/pspdfkit/internal/i4;->r:F

    .line 162
    iput p2, p0, Lcom/pspdfkit/internal/i4;->q:F

    .line 163
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/pspdfkit/internal/i4;->s:J

    .line 164
    iput p1, p0, Lcom/pspdfkit/internal/i4;->t:F

    .line 165
    iput p2, p0, Lcom/pspdfkit/internal/i4;->u:F

    .line 167
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/i4;->w:F

    .line 169
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->f()Lcom/pspdfkit/internal/b2;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 170
    iget v1, p0, Lcom/pspdfkit/internal/i4;->m:F

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    .line 172
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 174
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-interface {v1}, Lcom/pspdfkit/internal/b2;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/am;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    .line 178
    :cond_2
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, p0, Lcom/pspdfkit/internal/i4;->m:F

    div-float/2addr v1, v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    div-float/2addr v3, v2

    invoke-virtual {v0, v1, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 179
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-interface {v1, v0, v2, v3}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V

    .line 180
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/i4;->b(FF)V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .line 107
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 108
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v0

    .line 109
    iput v0, p0, Lcom/pspdfkit/internal/i4;->m:F

    .line 110
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v1, :cond_0

    .line 111
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v1, v0, v2}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/b2;

    .line 114
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v2, v0, v3}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    .line 116
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    .line 117
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 119
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 120
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 122
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    .line 123
    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->g:Landroid/graphics/Rect;

    iget-boolean v2, p0, Lcom/pspdfkit/internal/i4;->y:Z

    const/4 v3, 0x0

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/pspdfkit/internal/i4;->z:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move-object v2, v3

    goto :goto_2

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->j:Landroid/graphics/Paint;

    .line 124
    :goto_2
    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 130
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/b2;

    .line 131
    invoke-interface {v1}, Lcom/pspdfkit/internal/br;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/br$a;->c:Lcom/pspdfkit/internal/br$a;

    if-eq v2, v3, :cond_4

    .line 132
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->h:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->i:Landroid/graphics/Paint;

    invoke-interface {v1, p1, v2, v3}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_3

    .line 137
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 138
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 139
    iget v0, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 140
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/b2;

    .line 141
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eq v1, v2, :cond_6

    .line 142
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->h:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->i:Landroid/graphics/Paint;

    invoke-interface {v1, p1, v2, v3}, Lcom/pspdfkit/internal/br;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_4

    .line 145
    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 148
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_8

    .line 149
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->h:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->i:Landroid/graphics/Paint;

    invoke-interface {v0, p1, v1, v2}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 154
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_9

    .line 156
    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/br$a;->a:Lcom/pspdfkit/internal/br$a;

    if-ne v0, v1, :cond_9

    .line 157
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fa;->a(Landroid/graphics/Canvas;)V

    :cond_9
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result p1

    .line 99
    iput p1, p0, Lcom/pspdfkit/internal/i4;->m:F

    .line 100
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_1

    .line 101
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/b2;

    .line 104
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v1, p1, v2}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    goto :goto_0

    .line 105
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/hr;->c()Landroid/graphics/Rect;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 106
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    :cond_3
    return-void
.end method

.method final a(Lcom/pspdfkit/internal/fa;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    return-void
.end method

.method public a(Lcom/pspdfkit/internal/os;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/i4;->l:I

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->k:Lcom/pspdfkit/internal/zf;

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->f:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result p1

    .line 9
    iput p1, p0, Lcom/pspdfkit/internal/i4;->m:F

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, p1, v1}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/b2;

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    invoke-interface {v1, p1, v2}, Lcom/pspdfkit/internal/br;->a(FLandroid/graphics/Matrix;)Z

    goto :goto_0

    .line 15
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPdfConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/i4;->z:Z

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPdfConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/i4;->y:Z

    .line 20
    iget-boolean v0, p0, Lcom/pspdfkit/internal/i4;->z:Z

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ga;->a(ZZ)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object p1

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->i:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    .line 24
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 27
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_c

    if-eq v0, v1, :cond_b

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    goto/16 :goto_3

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->m()V

    goto/16 :goto_3

    .line 42
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 43
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/di;->b(FF)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_4

    .line 44
    iget-boolean v2, p0, Lcom/pspdfkit/internal/i4;->v:Z

    if-eqz v2, :cond_3

    goto/16 :goto_2

    .line 47
    :cond_3
    iput-boolean v1, p0, Lcom/pspdfkit/internal/i4;->v:Z

    goto :goto_1

    .line 49
    :cond_4
    iget-boolean v2, p0, Lcom/pspdfkit/internal/i4;->v:Z

    if-eqz v2, :cond_5

    .line 50
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/i4;->a(FF)V

    goto/16 :goto_2

    .line 54
    :cond_5
    :goto_1
    iget v2, p0, Lcom/pspdfkit/internal/i4;->w:F

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/i4;->w:F

    sub-float/2addr v3, v4

    .line 55
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 56
    iget v2, p0, Lcom/pspdfkit/internal/i4;->w:F

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/i4;->w:F

    sub-float/2addr v3, v4

    .line 57
    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 58
    iget v2, p0, Lcom/pspdfkit/internal/i4;->t:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 59
    iget v3, p0, Lcom/pspdfkit/internal/i4;->u:F

    sub-float v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 61
    iget-boolean v4, p0, Lcom/pspdfkit/internal/i4;->v:Z

    if-nez v4, :cond_6

    const/high16 v4, 0x40800000    # 4.0f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_6

    cmpl-float v2, v3, v4

    if-lez v2, :cond_a

    .line 62
    :cond_6
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 63
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    if-eqz v3, :cond_7

    .line 64
    iget v4, p0, Lcom/pspdfkit/internal/i4;->r:F

    iget v5, p0, Lcom/pspdfkit/internal/i4;->q:F

    iget v6, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-virtual {v3, v4, v5, v2, v6}, Lcom/pspdfkit/internal/fa;->a(FFLandroid/graphics/PointF;F)Landroid/graphics/PointF;

    move-result-object v2

    .line 67
    :cond_7
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iput v3, p0, Lcom/pspdfkit/internal/i4;->t:F

    .line 68
    iget v3, v2, Landroid/graphics/PointF;->y:F

    iput v3, p0, Lcom/pspdfkit/internal/i4;->u:F

    .line 70
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v3, :cond_9

    .line 72
    invoke-interface {v3}, Lcom/pspdfkit/internal/b2;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 73
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/am;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 76
    :cond_8
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v4, p0, Lcom/pspdfkit/internal/i4;->m:F

    div-float/2addr v3, v4

    iget v5, v2, Landroid/graphics/PointF;->y:F

    div-float/2addr v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 77
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-interface {v3, v2, v4, v5}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V

    .line 80
    :cond_9
    iget-boolean v2, p0, Lcom/pspdfkit/internal/i4;->v:Z

    if-eqz v2, :cond_a

    .line 81
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->n()V

    .line 85
    :cond_a
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/i4;->b(FF)V

    .line 86
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    goto :goto_3

    .line 89
    :cond_b
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->n()V

    goto :goto_3

    .line 90
    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/i4;->a(FF)V

    .line 91
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    :goto_3
    return v1
.end method

.method protected final b(FF)V
    .locals 4

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    new-array v0, v0, [I

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/uh;->e()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 27
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/4 v2, 0x0

    aget v3, v1, v2

    int-to-float v3, v3

    add-float/2addr p1, v3

    aget v3, v0, v2

    int-to-float v3, v3

    sub-float/2addr p1, v3

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    .line 28
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    const/4 v3, 0x1

    aget v1, v1, v3

    int-to-float v1, v1

    add-float/2addr p2, v1

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float/2addr p2, v0

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    .line 29
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object p1

    const/high16 p2, 0x40000000    # 2.0f

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/uh;->a(F)V

    .line 31
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object p1

    iget-object p2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/uh;->a(FF)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-interface {p1}, Lcom/pspdfkit/internal/b2;->d()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 36
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-interface {p1, v2}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    :cond_1
    return-void
.end method

.method public b()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->a()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->r()Ljava/util/List;

    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/os;->setPageModeHandlerViewHolder(Lcom/pspdfkit/internal/em;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v1

    new-instance v3, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/i4;)V

    .line 11
    invoke-virtual {v1, v0, v2, v3}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V

    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->recycle()V

    .line 19
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    return v2
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->C:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->b()Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAnnotationManager()Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract f()Lcom/pspdfkit/internal/b2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->a()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->D:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->r()Ljava/util/List;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/os;->c()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->e:Lcom/pspdfkit/internal/hr;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hr;->recycle()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 11
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method final i()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    return-void
.end method

.method protected m()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fa;->a()V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 5
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_4

    .line 7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/pspdfkit/internal/i4;->s:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x12c

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    goto :goto_0

    .line 12
    :cond_2
    new-instance v0, Landroid/graphics/PointF;

    iget v2, p0, Lcom/pspdfkit/internal/i4;->r:F

    iget v3, p0, Lcom/pspdfkit/internal/i4;->t:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/pspdfkit/internal/i4;->q:F

    iget v4, p0, Lcom/pspdfkit/internal/i4;->u:F

    sub-float/2addr v3, v4

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 14
    invoke-virtual {v0}, Landroid/graphics/PointF;->length()F

    move-result v0

    const/high16 v2, 0x42960000    # 75.0f

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_3

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 17
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    return-void
.end method

.method protected n()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->x:Lcom/pspdfkit/internal/fa;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fa;->a()V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 5
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_2

    .line 7
    sget-object v1, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/br;->a(Lcom/pspdfkit/internal/br$a;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->hide()V

    .line 11
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    return-void
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_0
    return-void
.end method

.method public onAnnotationPropertyChange(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/i4;->B:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p4, :cond_2

    .line 3
    invoke-virtual {p4, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_2

    const/16 p3, 0x64

    if-eq p2, p3, :cond_1

    const/16 p3, 0x67

    if-ne p2, p3, :cond_2

    .line 6
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0, p1}, Lcom/pspdfkit/internal/i4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/i4;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {p2, p3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method public onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 7
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/b2;

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 9
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/i4;->o()V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    :cond_2
    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected p()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    .line 8
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v2

    .line 9
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/o1;->a(Ljava/util/List;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/pspdfkit/internal/o1;->a()V

    .line 12
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/b2;

    .line 14
    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 16
    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/annotations/Annotation;

    const/4 v7, 0x1

    .line 17
    iput-boolean v7, p0, Lcom/pspdfkit/internal/i4;->B:Z

    .line 18
    iget-object v7, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v8, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-interface {v4, v6, v7, v8}, Lcom/pspdfkit/internal/b2;->b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z

    .line 19
    iput-boolean v5, p0, Lcom/pspdfkit/internal/i4;->B:Z

    goto :goto_0

    .line 20
    :cond_1
    invoke-interface {v4}, Lcom/pspdfkit/internal/br;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 22
    iget v5, p0, Lcom/pspdfkit/internal/i4;->l:I

    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-interface {v4, v5, v6, v7}, Lcom/pspdfkit/internal/b2;->a(ILandroid/graphics/Matrix;F)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v5

    if-nez v5, :cond_2

    .line 24
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 28
    :cond_2
    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v6, v5}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 29
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    .line 34
    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v6, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    .line 38
    :cond_3
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/b2;

    .line 43
    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_5
    invoke-virtual {v1}, Lcom/pspdfkit/internal/o1;->b()V

    .line 46
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 47
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v3

    invoke-virtual {v3, v2, v5}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_2

    .line 48
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Created "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " annotations from the drawing session."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ShapeAnnotations"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected q()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/internal/b2;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected r()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/w1;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    .line 8
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->A:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    return-object v0
.end method

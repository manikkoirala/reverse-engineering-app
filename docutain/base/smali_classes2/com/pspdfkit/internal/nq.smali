.class public final Lcom/pspdfkit/internal/nq;
.super Ljava/util/concurrent/locks/ReentrantReadWriteLock;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/pspdfkit/internal/oq;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/pspdfkit/internal/oq;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lockStore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/nq;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/pspdfkit/internal/nq;->b:Lcom/pspdfkit/internal/oq;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nq;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected final finalize()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nq;->b:Lcom/pspdfkit/internal/oq;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/oq;->a(Lcom/pspdfkit/internal/nq;)V

    return-void
.end method

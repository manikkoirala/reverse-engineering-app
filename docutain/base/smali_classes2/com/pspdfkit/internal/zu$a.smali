.class final Lcom/pspdfkit/internal/zu$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result p3

    invoke-static {p2, p3}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputk(Lcom/pspdfkit/internal/zu;I)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result p1

    invoke-static {p2, p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputl(Lcom/pspdfkit/internal/zu;I)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result p2

    if-eqz p2, :cond_0

    .line 4
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p2}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I

    move-result p3

    invoke-static {p2}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I

    move-result p2

    invoke-interface {p1, p3, p2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$a;->a:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

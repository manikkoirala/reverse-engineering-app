.class public final Lcom/pspdfkit/internal/t4;
.super Lcom/pspdfkit/internal/ql;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableManager;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/ql<",
        "Lcom/pspdfkit/bookmarks/Bookmark;",
        ">;",
        "Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;",
        "Lcom/pspdfkit/ui/drawable/PdfDrawableManager;"
    }
.end annotation


# instance fields
.field private final d:Landroidx/recyclerview/widget/RecyclerView;

.field private final e:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/ImageButton;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Lcom/pspdfkit/internal/s4;

.field private final k:Lcom/pspdfkit/internal/w4;

.field private final l:Lcom/pspdfkit/internal/v4;

.field private m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

.field private n:Lcom/pspdfkit/internal/u4;

.field private o:Z

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Z

.field private s:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final t:Lcom/pspdfkit/internal/fm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/fm<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/HashSet;

.field private final w:Ljava/util/ArrayList;

.field private x:Z


# direct methods
.method public static synthetic $r8$lambda$1N__s_RuB1K8FYjCXbu1yL_mrcY(Lcom/pspdfkit/internal/t4;Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/t4;->a(Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$352pWeRSSAKJDxaJo60-D5UoCkw(Lcom/pspdfkit/internal/t4;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/t4;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$CGa_DWUys0wQPphwsQbpkdVqmek(Lcom/pspdfkit/internal/t4;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/t4;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$RAiEYD4i2qCLqGWgbOVNt9SIOQg(Lcom/pspdfkit/internal/t4;Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/t4;->a(Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$eatPx8LTZfe1GtGJ4KE48eS2OKQ(Lcom/pspdfkit/internal/t4;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/t4;->b(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fJnKTEPi16pxAkjEBjJ1ZJ4ewTo(Lcom/pspdfkit/internal/t4;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/t4;->e()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/t4;)Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeto(Lcom/pspdfkit/internal/t4;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/t4;->o:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetr(Lcom/pspdfkit/internal/t4;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/t4;->r:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/t4;Lcom/pspdfkit/bookmarks/Bookmark;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/t4;->a(Lcom/pspdfkit/bookmarks/Bookmark;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ql;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/t4;->o:Z

    const/4 v1, 0x1

    .line 10
    iput-boolean v1, p0, Lcom/pspdfkit/internal/t4;->r:Z

    .line 13
    new-instance v2, Lcom/pspdfkit/internal/fm;

    invoke-direct {v2}, Lcom/pspdfkit/internal/fm;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/t4;->t:Lcom/pspdfkit/internal/fm;

    .line 17
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/t4;->u:Ljava/util/List;

    .line 20
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/t4;->v:Ljava/util/HashSet;

    .line 23
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/t4;->w:Ljava/util/ArrayList;

    .line 385
    iput-boolean v1, p0, Lcom/pspdfkit/internal/t4;->x:Z

    .line 386
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$layout;->pspdf__outline_bookmarks_view:I

    invoke-virtual {v2, v3, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 387
    sget v3, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_view:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 388
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 393
    sget v2, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_recycler_view:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v2, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    .line 394
    sget v3, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_empty_text:I

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/pspdfkit/internal/t4;->f:Landroid/widget/TextView;

    .line 395
    sget v3, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_toolbar:I

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/internal/t4;->g:Landroid/view/View;

    .line 396
    sget v3, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_add:I

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    .line 397
    sget v4, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_edit:I

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    .line 401
    new-instance v5, Lcom/pspdfkit/internal/s4;

    new-instance v6, Lcom/pspdfkit/internal/t4$a;

    invoke-direct {v6, p0}, Lcom/pspdfkit/internal/t4$a;-><init>(Lcom/pspdfkit/internal/t4;)V

    invoke-direct {v5, p1, v6}, Lcom/pspdfkit/internal/s4;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/s4$a;)V

    iput-object v5, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    .line 420
    iget-boolean v6, p0, Lcom/pspdfkit/internal/t4;->x:Z

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/s4;->b(Z)V

    .line 422
    new-instance v6, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v6, p1, v1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v6, p0, Lcom/pspdfkit/internal/t4;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 423
    invoke-virtual {v2, v6}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 424
    invoke-virtual {v2, v5}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 426
    new-instance p1, Lcom/pspdfkit/internal/w4;

    invoke-direct {p1, v5}, Lcom/pspdfkit/internal/w4;-><init>(Lcom/pspdfkit/internal/s4;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    .line 427
    new-instance v0, Lcom/pspdfkit/internal/v4;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/v4;-><init>(Lcom/pspdfkit/internal/w4;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->l:Lcom/pspdfkit/internal/v4;

    .line 428
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 430
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-direct {v0, p1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 431
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 433
    new-instance p1, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/t4;)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    new-instance p1, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda4;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/t4;)V

    invoke-virtual {v4, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkAdd()V

    :cond_0
    return-void
.end method

.method private a(Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;I)V
    .locals 0

    .line 89
    iget-object p4, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-nez p4, :cond_0

    goto :goto_1

    .line 90
    :cond_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 91
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_1

    .line 92
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    const/4 p4, 0x0

    invoke-interface {p1, p2, p4}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkNameSet(Lcom/pspdfkit/bookmarks/Bookmark;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_1
    iget-object p4, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    invoke-interface {p4, p2, p1}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkNameSet(Lcom/pspdfkit/bookmarks/Bookmark;Ljava/lang/String;)V

    .line 97
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :goto_1
    return-void
.end method

.method private a(Lcom/pspdfkit/bookmarks/Bookmark;I)V
    .locals 5

    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__outline_bookmarks_name_dialog:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 48
    sget v1, Lcom/pspdfkit/R$id;->pspdf__outline_bookmarks_name_dialog_edit_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 49
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_0
    new-instance v3, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {v3, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$string;->pspdf__name:I

    .line 55
    invoke-static {v3, v4, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 56
    invoke-virtual {v0, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    .line 58
    invoke-static {v3, v4, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-virtual {v0, v3, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v3, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v1, p1, p2}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/t4;Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;I)V

    .line 60
    invoke-virtual {v0, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$string;->pspdf__ok:I

    .line 70
    invoke-static {v3, v4, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 71
    new-instance v3, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, v1, p1, p2}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/t4;Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;I)V

    .line 72
    invoke-virtual {v0, v2, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p2

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 78
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->n:Lcom/pspdfkit/internal/u4;

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/u4;->a(Ljava/util/List;)V

    .line 100
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private a(Landroid/widget/EditText;Lcom/pspdfkit/bookmarks/Bookmark;ILandroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/16 p6, 0x42

    if-ne p5, p6, :cond_2

    .line 79
    iget-object p5, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-nez p5, :cond_0

    goto :goto_1

    .line 80
    :cond_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-eqz p5, :cond_1

    .line 82
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    const/4 p5, 0x0

    invoke-interface {p1, p2, p5}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkNameSet(Lcom/pspdfkit/bookmarks/Bookmark;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object p5, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    invoke-interface {p5, p2, p1}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->onBookmarkNameSet(Lcom/pspdfkit/bookmarks/Bookmark;Ljava/lang/String;)V

    .line 87
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 88
    :goto_1
    invoke-interface {p4}, Landroid/content/DialogInterface;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/t4;->o:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/t4;->o:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w4;->a(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s4;->a(Z)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/t4;->o:Z

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w4;->a(Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s4;->a(Z)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object p1

    const-string v0, "edit_bookmarks"

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    :goto_0
    return-void
.end method

.method private synthetic e()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    .line 4
    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/s4$b;

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->v:Ljava/util/HashSet;

    iget v3, v1, Lcom/pspdfkit/internal/s4$b;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    .line 7
    iput v2, v1, Lcom/pspdfkit/internal/s4$b;->b:I

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->v:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 15
    invoke-virtual {p0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 17
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->t:Lcom/pspdfkit/internal/fm;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fm;->b()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 7
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->take(J)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/t4;)V

    .line 8
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->s:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private setData(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/t4;->u:Ljava/util/List;

    .line 2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/t4;->o:Z

    if-eqz v0, :cond_0

    .line 3
    iput-boolean v1, p0, Lcom/pspdfkit/internal/t4;->o:Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/w4;->a(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/s4;->a(Z)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->n:Lcom/pspdfkit/internal/u4;

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/s4;->a(Ljava/util/List;Lcom/pspdfkit/internal/u4;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    const/16 v2, 0xff

    const/16 v3, 0x80

    const/4 v4, 0x1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->isBookmarkAddButtonEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 12
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 14
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1

    .line 19
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 22
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->f:Landroid/widget/TextView;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const/4 v3, 0x4

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x4

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v1, 0x4

    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 42
    iget-boolean v0, p0, Lcom/pspdfkit/internal/t4;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p0, Lcom/pspdfkit/internal/t4;->o:Z

    .line 44
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/w4;->a(Z)V

    .line 45
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/s4;->a(Z)V

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 2

    .line 101
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->v:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 104
    new-instance p1, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda2;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/t4$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/t4;)V

    .line 126
    iget-object p2, p0, Lcom/pspdfkit/internal/t4;->w:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x64

    .line 127
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 8

    .line 8
    iget v0, p1, Lcom/pspdfkit/internal/rl;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->g:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->e:I

    .line 12
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->h:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->e:I

    .line 18
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->p:Landroid/graphics/drawable/Drawable;

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->i:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->e:I

    .line 24
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->q:Landroid/graphics/drawable/Drawable;

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->g:Landroid/view/View;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 28
    iget-object v2, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    iget v3, p1, Lcom/pspdfkit/internal/rl;->c:I

    iget v4, p1, Lcom/pspdfkit/internal/rl;->f:I

    iget v5, p1, Lcom/pspdfkit/internal/rl;->a:I

    iget v6, p1, Lcom/pspdfkit/internal/rl;->o:I

    iget v7, p1, Lcom/pspdfkit/internal/rl;->n:I

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/internal/s4;->a(IIIII)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->f:Landroid/widget/TextView;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-static {v1}, Lcom/pspdfkit/internal/s5;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->k:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->l:I

    .line 38
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->k:Lcom/pspdfkit/internal/w4;

    iget v2, p1, Lcom/pspdfkit/internal/rl;->m:I

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/w4;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->l:Lcom/pspdfkit/internal/v4;

    iget p1, p1, Lcom/pspdfkit/internal/rl;->m:I

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/v4;->a(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/u4;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, Lcom/pspdfkit/internal/u4;-><init>(Lcom/pspdfkit/internal/zf;Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/t4;->n:Lcom/pspdfkit/internal/u4;

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/t4;->f()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/t4;->n:Lcom/pspdfkit/internal/u4;

    .line 7
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

.method public final addDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->t:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/ui/PageObjectProvider;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/t4;->f()V

    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->getBookmarks()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/t4;->setData(Ljava/util/List;)V

    return-void
.end method

.method public getTabButtonId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_bookmarks:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__bookmarks:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    :cond_0
    return-void
.end method

.method public final onBookmarkAdded(Lcom/pspdfkit/bookmarks/Bookmark;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->e:Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/t4;->d:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->smoothScrollToPosition(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s4;->a(I)V

    :cond_0
    return-void
.end method

.method public final onBookmarksChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/t4;->setData(Ljava/util/List;)V

    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->removeBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    :cond_0
    return-void
.end method

.method public final removeDrawableProvider(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->t:Lcom/pspdfkit/internal/fm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/fm;->b(Lcom/pspdfkit/ui/PageObjectProvider;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/t4;->f()V

    return-void
.end method

.method public setBookmarkEditingEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->h:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setBookmarkRenamingEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/t4;->r:Z

    return-void
.end method

.method public setBookmarkViewAdapter(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/t4;->m:Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1, p0}, Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;->addBookmarkListener(Lcom/pspdfkit/bookmarks/BookmarkProvider$BookmarkListener;)V

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

.method public setCurrentPageIndex(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s4;->c(I)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setRedactionAnnotationPreviewEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->n:Lcom/pspdfkit/internal/u4;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/u4;->a(Z)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setShowPageLabels(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/t4;->x:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s4;->b(Z)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/t4;->j:Lcom/pspdfkit/internal/s4;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

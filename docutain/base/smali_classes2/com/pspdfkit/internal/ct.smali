.class public final Lcom/pspdfkit/internal/ct;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/document/PdfDocument;

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lcom/pspdfkit/internal/bt;

.field private g:I

.field private final h:Ljava/util/LinkedHashMap;

.field private final i:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 146
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    .line 148
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/pspdfkit/utils/Size;
    .locals 7

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    const/4 v1, 0x0

    const-string v2, "thumbnailBarThemeConfiguration"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    iget-boolean v0, v0, Lcom/pspdfkit/internal/bt;->g:Z

    if-eqz v0, :cond_3

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v0, p1}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    const-string v3, "document.getPageSize(pageIndex)"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget v3, v0, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v3, v0

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/pspdfkit/utils/Size;

    .line 16
    iget-object v6, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    if-nez v6, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v6, v1

    :cond_1
    iget v6, v6, Lcom/pspdfkit/internal/bt;->e:I

    int-to-float v6, v6

    mul-float v6, v6, v3

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    if-nez v3, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_0
    iget v1, v1, Lcom/pspdfkit/internal/bt;->e:I

    int-to-float v1, v1

    .line 18
    invoke-direct {v5, v6, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 24
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 25
    new-instance v4, Lcom/pspdfkit/utils/Size;

    .line 26
    iget-object v5, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    if-nez v5, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v5, v1

    :cond_4
    iget v5, v5, Lcom/pspdfkit/internal/bt;->d:I

    int-to-float v5, v5

    .line 27
    iget-object v6, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    if-nez v6, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v1, v6

    :goto_1
    iget v1, v1, Lcom/pspdfkit/internal/bt;->e:I

    int-to-float v1, v1

    .line 28
    invoke-direct {v4, v5, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 29
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    return-object p1
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(IZZZLcom/pspdfkit/internal/bt;)V
    .locals 1

    const-string v0, "thumbnailBarThemeConfiguration"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/ct;->b:I

    .line 3
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ct;->c:Z

    .line 4
    iput-boolean p3, p0, Lcom/pspdfkit/internal/ct;->d:Z

    .line 5
    iput-boolean p4, p0, Lcom/pspdfkit/internal/ct;->e:Z

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/ct;->f:Lcom/pspdfkit/internal/bt;

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->clear()V

    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    .line 172
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 173
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 174
    check-cast v2, Lcom/pspdfkit/internal/lu;

    .line 175
    invoke-virtual {v2}, Lcom/pspdfkit/internal/lu;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 348
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final b(I)V
    .locals 13

    .line 349
    iget v0, p0, Lcom/pspdfkit/internal/ct;->g:I

    if-ne p1, v0, :cond_0

    return-void

    .line 350
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/ct;->g:I

    .line 351
    iget-boolean p1, p0, Lcom/pspdfkit/internal/ct;->c:Z

    const/16 v0, 0x19

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_18

    .line 352
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 355
    div-int/lit8 v0, p1, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 358
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 360
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 363
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 366
    iget-object v5, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v5}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v5

    iget-boolean v6, p0, Lcom/pspdfkit/internal/ct;->e:Z

    if-eqz v0, :cond_10

    if-nez v5, :cond_2

    goto/16 :goto_6

    .line 367
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 373
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x2

    if-nez v6, :cond_3

    if-le v5, v2, :cond_3

    .line 378
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x2

    goto :goto_0

    :cond_3
    const/4 v9, 0x1

    :goto_0
    if-ge v0, v9, :cond_4

    .line 384
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    goto/16 :goto_7

    .line 388
    :cond_4
    rem-int/lit8 v10, v5, 0x2

    if-ne v10, v6, :cond_5

    const/4 v6, 0x1

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_6

    const/4 v10, 0x2

    goto :goto_2

    :cond_6
    const/4 v10, 0x1

    :goto_2
    sub-int v10, v5, v10

    .line 392
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    sub-int/2addr v11, v2

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    const-string v12, "pages[pages.size - 1]"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->intValue()I

    move-result v11

    if-le v10, v11, :cond_a

    if-eqz v6, :cond_7

    goto :goto_3

    :cond_7
    const/4 v8, 0x1

    :goto_3
    add-int/2addr v8, v9

    if-lt v0, v8, :cond_8

    const/4 v8, 0x1

    goto :goto_4

    :cond_8
    const/4 v8, 0x0

    :goto_4
    if-nez v8, :cond_9

    goto :goto_7

    .line 402
    :cond_9
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    if-eqz v6, :cond_a

    add-int/lit8 v10, v10, 0x1

    .line 406
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    :cond_a
    sub-int v6, v0, v9

    .line 414
    rem-int/lit8 v8, v6, 0x2

    if-ne v8, v2, :cond_b

    add-int/lit8 v6, v6, -0x1

    :cond_b
    if-nez v6, :cond_c

    goto :goto_7

    :cond_c
    mul-int/lit8 v8, v5, 0x2

    int-to-float v8, v8

    add-int/2addr v6, v9

    int-to-float v6, v6

    div-float/2addr v8, v6

    const/4 v6, 0x0

    :cond_d
    add-float/2addr v6, v8

    float-to-double v10, v6

    .line 426
    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    if-lt v10, v5, :cond_e

    goto :goto_5

    .line 429
    :cond_e
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_f

    add-int/lit8 v11, v10, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 430
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x2

    :cond_f
    if-lt v9, v0, :cond_d

    .line 436
    :goto_5
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->sort(Ljava/util/List;)V

    goto :goto_7

    .line 437
    :cond_10
    :goto_6
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    .line 438
    :goto_7
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ct;->d:Z

    if-eqz v5, :cond_11

    .line 439
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->asReversed(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 445
    :cond_11
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ct;->e:Z

    .line 446
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v7, 0x0

    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_13

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    .line 447
    invoke-virtual {p0, v8}, Lcom/pspdfkit/internal/ct;->a(I)Lcom/pspdfkit/utils/Size;

    move-result-object v9

    .line 448
    new-instance v10, Lcom/pspdfkit/internal/lu;

    invoke-direct {v10, v8, v7, v9}, Lcom/pspdfkit/internal/lu;-><init>(IILcom/pspdfkit/utils/Size;)V

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    iget v8, v9, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v8, v8

    add-int/2addr v7, v8

    if-eqz v5, :cond_12

    .line 451
    iget v8, p0, Lcom/pspdfkit/internal/ct;->b:I

    add-int/2addr v7, v8

    :cond_12
    xor-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_13
    if-nez v5, :cond_14

    .line 458
    iget v5, p0, Lcom/pspdfkit/internal/ct;->b:I

    sub-int/2addr v7, v5

    .line 461
    :cond_14
    iget v5, p0, Lcom/pspdfkit/internal/ct;->g:I

    if-le v7, v5, :cond_16

    .line 463
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v2

    if-eqz v5, :cond_15

    goto :goto_b

    :cond_15
    add-int/lit8 v0, v0, -0x1

    goto :goto_9

    .line 470
    :cond_16
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 471
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    :goto_9
    if-gt v2, v0, :cond_17

    if-gt v0, p1, :cond_17

    const/4 v5, 0x1

    goto :goto_a

    :cond_17
    const/4 v5, 0x0

    :goto_a
    if-nez v5, :cond_1

    .line 477
    :goto_b
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 478
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_14

    .line 479
    :cond_18
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 482
    div-int/lit8 v0, p1, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 485
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 487
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 490
    :cond_19
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 493
    iget-object v5, p0, Lcom/pspdfkit/internal/ct;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {v5}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v5

    if-eqz v0, :cond_1e

    if-nez v5, :cond_1a

    goto :goto_e

    .line 494
    :cond_1a
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    if-gt v5, v0, :cond_1b

    const/4 v7, 0x0

    :goto_c
    if-ge v7, v5, :cond_1f

    .line 499
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_c

    :cond_1b
    if-ne v0, v2, :cond_1c

    .line 506
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_1c
    add-int/lit8 v5, v5, -0x1

    int-to-float v7, v5

    add-int/lit8 v8, v0, -0x1

    int-to-float v9, v8

    div-float/2addr v7, v9

    const/4 v9, 0x0

    :goto_d
    if-ge v9, v8, :cond_1d

    int-to-float v10, v9

    mul-float v10, v10, v7

    float-to-double v10, v10

    .line 512
    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    .line 513
    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 514
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_d

    .line 518
    :cond_1d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1f

    .line 519
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 520
    :cond_1e
    :goto_e
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 521
    :cond_1f
    :goto_f
    iget-boolean v5, p0, Lcom/pspdfkit/internal/ct;->d:Z

    if-eqz v5, :cond_20

    .line 522
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->asReversed(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 528
    :cond_20
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v6, 0x0

    :goto_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_21

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    .line 529
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/ct;->a(I)Lcom/pspdfkit/utils/Size;

    move-result-object v8

    .line 530
    new-instance v9, Lcom/pspdfkit/internal/lu;

    invoke-direct {v9, v7, v6, v8}, Lcom/pspdfkit/internal/lu;-><init>(IILcom/pspdfkit/utils/Size;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531
    iget v7, v8, Lcom/pspdfkit/utils/Size;->width:F

    float-to-int v7, v7

    iget v8, p0, Lcom/pspdfkit/internal/ct;->b:I

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    goto :goto_10

    .line 535
    :cond_21
    iget v5, p0, Lcom/pspdfkit/internal/ct;->b:I

    sub-int/2addr v6, v5

    .line 537
    iget v5, p0, Lcom/pspdfkit/internal/ct;->g:I

    if-le v6, v5, :cond_23

    .line 539
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v2

    if-eqz v5, :cond_22

    goto :goto_13

    :cond_22
    add-int/lit8 v0, v0, -0x1

    goto :goto_11

    .line 546
    :cond_23
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 547
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    :goto_11
    if-gt v2, v0, :cond_24

    if-gt v0, p1, :cond_24

    const/4 v5, 0x1

    goto :goto_12

    :cond_24
    const/4 v5, 0x0

    :goto_12
    if-nez v5, :cond_19

    .line 553
    :goto_13
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 554
    iget-object p1, p0, Lcom/pspdfkit/internal/ct;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :goto_14
    return-void
.end method

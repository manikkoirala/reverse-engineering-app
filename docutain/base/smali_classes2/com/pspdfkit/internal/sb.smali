.class public final Lcom/pspdfkit/internal/sb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/uf;

.field private final b:Lcom/pspdfkit/internal/zf;

.field private final c:Lcom/pspdfkit/internal/jni/NativeFormManager;

.field private final d:I

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private final g:Ljava/util/ArrayList;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/uf;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/jni/NativeFormManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/sb;->a:Lcom/pspdfkit/internal/uf;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->b:Lcom/pspdfkit/internal/zf;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/sb;->c:Lcom/pspdfkit/internal/jni/NativeFormManager;

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/sb;->d:I

    .line 7
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    .line 8
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    .line 10
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->g:Ljava/util/ArrayList;

    .line 11
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->h:Ljava/util/ArrayList;

    .line 13
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 15
    :goto_0
    iget p2, p0, Lcom/pspdfkit/internal/sb;->d:I

    if-ge p1, p2, :cond_0

    .line 16
    iget-object p2, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/internal/sb;->g:Ljava/util/ArrayList;

    new-instance p3, Landroid/util/SparseArray;

    invoke-direct {p3}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    iget-object p2, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/sb;->f()V

    .line 22
    invoke-direct {p0}, Lcom/pspdfkit/internal/sb;->b()V

    .line 23
    invoke-direct {p0}, Lcom/pspdfkit/internal/sb;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    const/4 v0, 0x0

    .line 38
    :goto_0
    iget v1, p0, Lcom/pspdfkit/internal/sb;->d:I

    if-ge v0, v1, :cond_0

    .line 39
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sb;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(ILjava/util/List;)V
    .locals 6

    .line 1
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 5
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormField;

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/sb;->b:Lcom/pspdfkit/internal/zf;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/forms/FormField;)Ljava/util/ArrayList;

    move-result-object v2

    .line 8
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v4, :cond_1

    .line 11
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/forms/FormElement;

    if-nez v5, :cond_2

    .line 13
    iget-object v5, p0, Lcom/pspdfkit/internal/sb;->a:Lcom/pspdfkit/internal/uf;

    invoke-interface {v5, v1, v4}, Lcom/pspdfkit/internal/uf;->createFormElement(Lcom/pspdfkit/forms/FormField;Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;

    move-result-object v5

    .line 14
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v4

    invoke-virtual {v0, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 15
    iget-object v4, p0, Lcom/pspdfkit/internal/sb;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    :cond_2
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 21
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 22
    iget-object v2, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 23
    iget-object v2, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-virtual {v1}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 25
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/sb;->a:Lcom/pspdfkit/internal/uf;

    invoke-interface {v2, v1, v3}, Lcom/pspdfkit/internal/uf;->attachFormElement(Lcom/pspdfkit/forms/FormField;Ljava/util/List;)V

    goto :goto_0

    :cond_5
    return-void
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    .line 1
    :goto_0
    iget v1, p0, Lcom/pspdfkit/internal/sb;->d:I

    if-ge v0, v1, :cond_1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 3
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 4
    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/sb;->a(ILjava/util/List;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(II)V
    .locals 2

    if-ltz p1, :cond_5

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/sb;->d:I

    if-lt p2, v0, :cond_0

    goto :goto_2

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    if-lez p1, :cond_1

    .line 14
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 17
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    .line 21
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 22
    iget v1, p0, Lcom/pspdfkit/internal/sb;->d:I

    if-ge p2, v1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    goto :goto_1

    .line 25
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_4

    return-void

    .line 28
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/forms/FormElement;

    const/4 v0, 0x0

    .line 29
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/forms/FormElement;

    .line 31
    invoke-virtual {p2, p1}, Lcom/pspdfkit/forms/FormElement;->setNextElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 32
    invoke-virtual {p1, p2}, Lcom/pspdfkit/forms/FormElement;->setPreviousElement(Lcom/pspdfkit/forms/FormElement;)V

    :cond_5
    :goto_2
    return-void
.end method

.method private f()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->c:Lcom/pspdfkit/internal/jni/NativeFormManager;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeFormManager;->getFormFields()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 4
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 5
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 8
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/jni/NativeFormField;

    .line 9
    iget-object v5, p0, Lcom/pspdfkit/internal/sb;->a:Lcom/pspdfkit/internal/uf;

    invoke-interface {v5, v1, v4}, Lcom/pspdfkit/internal/uf;->createFormField(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    move-result-object v4

    .line 10
    invoke-virtual {v4}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    iget-object v5, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method final a(II)Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/forms/FormElement;

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/annotations/WidgetAnnotation;)Lcom/pspdfkit/forms/FormElement;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->g(I)I

    move-result v0

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/sb;->a(II)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getFQN()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/FormField;

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->a:Lcom/pspdfkit/internal/uf;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/uf;->createFormField(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object p2, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/sb;->a(ILjava/util/List;)V

    return-object v0
.end method

.method public final a(ILjava/lang/String;)Lcom/pspdfkit/forms/FormField;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    .line 27
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/forms/FormField;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method final a(I)V
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->c:Lcom/pspdfkit/internal/jni/NativeFormManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeFormManager;->getTabOrderForProvider(I)Lcom/pspdfkit/internal/jni/NativeTabOrder;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTabOrder;->getWidgetIDs()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 44
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTabOrder;->getWidgetIDs()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/sb;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 50
    new-instance v2, Ljava/util/ArrayList;

    .line 51
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTabOrder;->getWidgetIDs()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    .line 53
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTabOrder;->getWidgetIDs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 54
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/forms/FormElement;

    if-nez v4, :cond_1

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v4, v3}, Lcom/pspdfkit/forms/FormElement;->setPreviousElement(Lcom/pspdfkit/forms/FormElement;)V

    if-eqz v3, :cond_2

    .line 59
    invoke-virtual {v3, v4}, Lcom/pspdfkit/forms/FormElement;->setNextElement(Lcom/pspdfkit/forms/FormElement;)V

    .line 62
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    goto :goto_0

    .line 64
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, p1, -0x1

    .line 66
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/sb;->b(II)V

    add-int/lit8 v0, p1, 0x1

    .line 67
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/sb;->b(II)V

    :cond_4
    :goto_1
    return-void
.end method

.method public final b(ILcom/pspdfkit/internal/jni/NativeFormField;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeFormField;->getFQN()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/forms/FormField;

    if-eqz p2, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormField;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sb;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final e()Ljava/util/ArrayList;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/sb;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 3
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

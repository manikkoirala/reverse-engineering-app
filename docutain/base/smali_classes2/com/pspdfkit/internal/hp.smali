.class public final Lcom/pspdfkit/internal/hp;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/hp$a;,
        Lcom/pspdfkit/internal/hp$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/hp$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/iv;

.field private final b:Lcom/pspdfkit/internal/c7;

.field private final c:Lcom/pspdfkit/internal/iv;

.field private final d:Lcom/pspdfkit/internal/bd;

.field private final e:Lcom/pspdfkit/internal/m7;

.field private final f:Lcom/pspdfkit/internal/uq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/hp$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/hp$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/hp;->Companion:Lcom/pspdfkit/internal/hp$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/m7;Lcom/pspdfkit/internal/uq;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0xf

    const/16 v1, 0xf

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/hp$a;->a:Lcom/pspdfkit/internal/hp$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hp$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/hp;->a:Lcom/pspdfkit/internal/iv;

    iput-object p3, p0, Lcom/pspdfkit/internal/hp;->b:Lcom/pspdfkit/internal/c7;

    iput-object p4, p0, Lcom/pspdfkit/internal/hp;->c:Lcom/pspdfkit/internal/iv;

    iput-object p5, p0, Lcom/pspdfkit/internal/hp;->d:Lcom/pspdfkit/internal/bd;

    and-int/lit8 p2, p1, 0x10

    const/4 p3, 0x0

    if-nez p2, :cond_1

    iput-object p3, p0, Lcom/pspdfkit/internal/hp;->e:Lcom/pspdfkit/internal/m7;

    goto :goto_0

    :cond_1
    iput-object p6, p0, Lcom/pspdfkit/internal/hp;->e:Lcom/pspdfkit/internal/m7;

    :goto_0
    and-int/lit8 p1, p1, 0x20

    if-nez p1, :cond_2

    iput-object p3, p0, Lcom/pspdfkit/internal/hp;->f:Lcom/pspdfkit/internal/uq;

    goto :goto_1

    :cond_2
    iput-object p7, p0, Lcom/pspdfkit/internal/hp;->f:Lcom/pspdfkit/internal/uq;

    :goto_1
    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/m7;Lcom/pspdfkit/internal/uq;)V
    .locals 1

    const-string v0, "pixelPageSize"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pixelViewport"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pixelAnchor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "globalEffects"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/hp;->a:Lcom/pspdfkit/internal/iv;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/hp;->b:Lcom/pspdfkit/internal/c7;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/hp;->c:Lcom/pspdfkit/internal/iv;

    .line 7
    iput-object p4, p0, Lcom/pspdfkit/internal/hp;->d:Lcom/pspdfkit/internal/bd;

    .line 8
    iput-object p5, p0, Lcom/pspdfkit/internal/hp;->e:Lcom/pspdfkit/internal/m7;

    .line 9
    iput-object p6, p0, Lcom/pspdfkit/internal/hp;->f:Lcom/pspdfkit/internal/uq;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/hp;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/iv$a;->a:Lcom/pspdfkit/internal/iv$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/hp;->a:Lcom/pspdfkit/internal/iv;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v1, Lcom/pspdfkit/internal/c7$a;->a:Lcom/pspdfkit/internal/c7$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/hp;->b:Lcom/pspdfkit/internal/c7;

    const/4 v4, 0x1

    invoke-interface {p1, p2, v4, v1, v3}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/hp;->c:Lcom/pspdfkit/internal/iv;

    const/4 v3, 0x2

    invoke-interface {p1, p2, v3, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/bd$a;->a:Lcom/pspdfkit/internal/bd$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/hp;->d:Lcom/pspdfkit/internal/bd;

    const/4 v3, 0x3

    invoke-interface {p1, p2, v3, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    const/4 v0, 0x4

    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/hp;->e:Lcom/pspdfkit/internal/m7;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    sget-object v1, Lcom/pspdfkit/internal/m7$a;->a:Lcom/pspdfkit/internal/m7$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/hp;->e:Lcom/pspdfkit/internal/m7;

    invoke-interface {p1, p2, v0, v1, v3}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x5

    invoke-interface {p1, p2, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->shouldEncodeElementDefault(Lkotlinx/serialization/descriptors/SerialDescriptor;I)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/hp;->f:Lcom/pspdfkit/internal/uq;

    if-eqz v1, :cond_4

    :goto_2
    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    sget-object v1, Lcom/pspdfkit/internal/uq$a;->a:Lcom/pspdfkit/internal/uq$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/hp;->f:Lcom/pspdfkit/internal/uq;

    invoke-interface {p1, p2, v0, v1, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeNullableSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    :cond_5
    return-void
.end method

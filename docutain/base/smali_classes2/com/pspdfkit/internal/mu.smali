.class public final Lcom/pspdfkit/internal/mu;
.super Lcom/pspdfkit/internal/im$h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/mu$c;,
        Lcom/pspdfkit/internal/mu$a;,
        Lcom/pspdfkit/internal/mu$b;
    }
.end annotation


# instance fields
.field private final d:I

.field private final e:I

.field private final f:Landroid/graphics/Paint;

.field private final g:Ljava/util/ArrayList;

.field private final h:Landroid/graphics/Rect;

.field private i:F

.field private j:F

.field private final k:Ljava/util/ArrayList;

.field private final l:Ljava/util/ArrayList;

.field private final m:Lcom/pspdfkit/internal/dl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/dl<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/pspdfkit/internal/do;

.field private o:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$pDQVTQLndN8bFAGw9j6g3YG11Os(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/mu;->b(Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$xgbtGPGRVIUdO6bbTV9LCUAiIEw(Lcom/pspdfkit/internal/mu;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/mu;->a(Ljava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/mu;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/mu;)F
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/mu;->i:F

    return p0
.end method

.method static bridge synthetic -$$Nest$md(Lcom/pspdfkit/internal/mu;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->d()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/im;Landroid/util/DisplayMetrics;)V
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im$h;-><init>(Lcom/pspdfkit/internal/im;)V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/mu;->f:Landroid/graphics/Paint;

    .line 10
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    const/4 v1, 0x0

    .line 14
    iput v1, p0, Lcom/pspdfkit/internal/mu;->i:F

    .line 17
    iput v1, p0, Lcom/pspdfkit/internal/mu;->j:F

    .line 28
    new-instance v1, Lcom/pspdfkit/internal/dl;

    new-instance v2, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda2;

    invoke-direct {v2}, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda2;-><init>()V

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/dl;-><init>(Lcom/pspdfkit/internal/dl$a;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/mu;->m:Lcom/pspdfkit/internal/dl;

    .line 33
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x1

    const-string v3, "tile-coordinator"

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/u;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/mu;->n:Lcom/pspdfkit/internal/do;

    .line 41
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    .line 45
    invoke-virtual {p1}, Lcom/pspdfkit/internal/im;->getLocalVisibleRect()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 48
    iget p1, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/pspdfkit/internal/mu;->d:I

    .line 49
    iget p2, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/pspdfkit/internal/mu;->e:I

    .line 51
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/n4;->b(II)V

    return-void
.end method

.method static synthetic a(Lcom/pspdfkit/internal/mu;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method private a()V
    .locals 8

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/mu$c;

    .line 82
    invoke-static {v1}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v4

    .line 83
    invoke-static {v4}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 84
    invoke-static {v1, v3}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 85
    iput-boolean v2, v1, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 86
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v2

    iget-object v4, v1, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 87
    iput-object v3, v1, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 89
    iput v0, p0, Lcom/pspdfkit/internal/mu;->j:F

    .line 90
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 91
    iget-object v4, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/mu$c;

    .line 92
    invoke-static {v5}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v6

    .line 93
    invoke-static {v6}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 94
    invoke-static {v5, v3}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 95
    iput-boolean v2, v5, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 96
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v6

    iget-object v7, v5, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 97
    iput-object v3, v5, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 99
    iput v0, p0, Lcom/pspdfkit/internal/mu;->i:F

    if-eqz v1, :cond_2

    .line 102
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method private synthetic a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 104
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->b()V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    invoke-static {v2, v3}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7
    new-instance v3, Lcom/pspdfkit/internal/mu$c;

    invoke-direct {v3, p0, p1, v2}, Lcom/pspdfkit/internal/mu$c;-><init>(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/dm$e;Landroid/graphics/Rect;)V

    .line 8
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/mu$c;

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/internal/mu$c;->a()V

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private b()V
    .locals 12

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 14
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->m:Lcom/pspdfkit/internal/dl;

    iget-object v2, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/dl;->a(Ljava/util/ArrayList;)V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/pspdfkit/internal/mu;->d:I

    div-int/lit8 v4, v3, 0x2

    sub-int/2addr v2, v4

    .line 17
    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/pspdfkit/internal/mu;->e:I

    div-int/lit8 v5, v4, 0x2

    sub-int/2addr v1, v5

    if-lez v2, :cond_1

    .line 21
    div-int v5, v2, v3

    add-int/lit8 v5, v5, 0x1

    mul-int v5, v5, v3

    sub-int/2addr v2, v5

    :cond_1
    if-lez v1, :cond_2

    .line 27
    div-int v3, v1, v4

    add-int/lit8 v3, v3, 0x1

    mul-int v3, v3, v4

    sub-int/2addr v1, v3

    .line 32
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v3

    .line 33
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v4

    iget v4, v4, Lcom/pspdfkit/utils/Size;->width:F

    mul-float v4, v4, v3

    float-to-int v4, v4

    .line 34
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->g()Lcom/pspdfkit/utils/Size;

    move-result-object v0

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 36
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v3, v4

    iget v4, p0, Lcom/pspdfkit/internal/mu;->d:I

    div-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    .line 37
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    add-int/2addr v4, v0

    iget v0, p0, Lcom/pspdfkit/internal/mu;->e:I

    div-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_4

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_3

    .line 41
    iget v7, p0, Lcom/pspdfkit/internal/mu;->d:I

    mul-int v7, v7, v5

    add-int/2addr v7, v2

    .line 42
    iget v8, p0, Lcom/pspdfkit/internal/mu;->e:I

    mul-int v8, v8, v6

    add-int/2addr v8, v1

    .line 43
    iget-object v9, p0, Lcom/pspdfkit/internal/mu;->m:Lcom/pspdfkit/internal/dl;

    invoke-virtual {v9}, Lcom/pspdfkit/internal/dl;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Rect;

    .line 44
    iget v10, p0, Lcom/pspdfkit/internal/mu;->d:I

    add-int/2addr v10, v7

    iget v11, p0, Lcom/pspdfkit/internal/mu;->e:I

    add-int/2addr v11, v8

    invoke-virtual {v9, v7, v8, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 45
    iget-object v7, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method private c()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    goto :goto_0

    .line 3
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/mu$c;

    .line 4
    iget-boolean v3, v3, Lcom/pspdfkit/internal/mu$c;->f:Z

    if-nez v3, :cond_2

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    :goto_1
    const/4 v3, 0x0

    if-eqz v1, :cond_5

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/mu$c;

    .line 6
    invoke-static {v4}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v5

    .line 7
    invoke-static {v5}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 8
    invoke-static {v4, v3}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 9
    iput-boolean v2, v4, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v5

    iget-object v6, v4, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 11
    iput-object v3, v4, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 12
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    .line 13
    iput v1, p0, Lcom/pspdfkit/internal/mu;->j:F

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 15
    iget v1, p0, Lcom/pspdfkit/internal/mu;->i:F

    iput v1, p0, Lcom/pspdfkit/internal/mu;->j:F

    goto :goto_4

    .line 17
    :cond_5
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/mu$c;

    .line 18
    invoke-static {v4}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v5

    .line 19
    invoke-static {v5}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 20
    invoke-static {v4, v3}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 21
    iput-boolean v2, v4, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 22
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v5

    iget-object v6, v4, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 23
    iput-object v3, v4, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 24
    :cond_6
    :goto_4
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 25
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/mu;->i:F

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->o:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 29
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 30
    new-instance v1, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/dm$e;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->n:Lcom/pspdfkit/internal/do;

    const/4 v2, 0x5

    .line 46
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 47
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/mu$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/mu;)V

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/mu;->o:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private d()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mu$c;

    .line 3
    iget-boolean v2, v2, Lcom/pspdfkit/internal/mu$c;->f:Z

    if-nez v2, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    return-void

    .line 4
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mu$c;

    .line 5
    invoke-static {v2}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v3

    .line 6
    invoke-static {v3}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v3, 0x0

    .line 7
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 8
    iput-boolean v1, v2, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v4

    iget-object v5, v2, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 10
    iput-object v3, v2, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 11
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/pspdfkit/internal/mu;->j:F

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    sget-object v1, Lcom/pspdfkit/internal/im$g;->b:Lcom/pspdfkit/internal/im$g;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/im;->a(Lcom/pspdfkit/internal/im$g;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method private f()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->o:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->c()V

    :cond_2
    return-void

    .line 13
    :cond_3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/mu;->i:F

    .line 16
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    iget-object v2, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 18
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/mu$c;

    .line 19
    iget-object v6, v5, Lcom/pspdfkit/internal/mu$c;->b:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    invoke-static {v6, v7}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 20
    invoke-static {v5}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fgetg(Lcom/pspdfkit/internal/mu$c;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v4

    .line 21
    invoke-static {v4}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v4, 0x0

    .line 22
    invoke-static {v5, v4}, Lcom/pspdfkit/internal/mu$c;->-$$Nest$fputg(Lcom/pspdfkit/internal/mu$c;Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 23
    iput-boolean v3, v5, Lcom/pspdfkit/internal/mu$c;->f:Z

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v6

    iget-object v7, v5, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/pspdfkit/internal/n4;->e(Landroid/graphics/Bitmap;)V

    .line 25
    iput-object v4, v5, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    .line 26
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    const/4 v4, 0x1

    goto :goto_0

    .line 29
    :cond_4
    iget-object v5, v5, Lcom/pspdfkit/internal/mu$c;->b:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/mu;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 36
    iget-object v5, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    invoke-static {v3, v5}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 37
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 38
    new-instance v5, Lcom/pspdfkit/internal/mu$c;

    invoke-direct {v5, p0, v0, v3}, Lcom/pspdfkit/internal/mu$c;-><init>(Lcom/pspdfkit/internal/mu;Lcom/pspdfkit/internal/dm$e;Landroid/graphics/Rect;)V

    .line 39
    iget-object v3, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    invoke-virtual {v5}, Lcom/pspdfkit/internal/mu$c;->a()V

    goto :goto_1

    :cond_7
    if-eqz v4, :cond_8

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_8
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 8

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_8

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/im;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->e()Z

    move-result v0

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/im;->getLocalVisibleRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 41
    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v1

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    const/4 v4, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 42
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v2

    const v5, 0x3f8ccccd    # 1.1f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 49
    :goto_1
    iget-object v5, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/im;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/views/document/DocumentView;->l()Z

    move-result v5

    .line 50
    iget v6, p0, Lcom/pspdfkit/internal/mu;->i:F

    iget-object v7, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    .line 51
    invoke-virtual {v7}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v7

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_2

    iget v6, p0, Lcom/pspdfkit/internal/mu;->i:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-eqz v0, :cond_6

    if-nez v2, :cond_4

    if-eqz v1, :cond_3

    if-nez v5, :cond_3

    goto :goto_3

    :cond_3
    if-eqz v5, :cond_7

    .line 68
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->a()V

    goto :goto_4

    :cond_4
    :goto_3
    if-eqz v3, :cond_5

    if-eqz p1, :cond_7

    .line 69
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->c()V

    goto :goto_4

    .line 71
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->f()V

    goto :goto_4

    .line 79
    :cond_6
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->a()V

    :cond_7
    :goto_4
    return-void

    .line 80
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Trying to update the TileSubview while the State is not initialized, meaning that the view was never bound to the page, or already recycled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_6

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/im;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/pspdfkit/internal/mu;->j:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/mu;->j:F

    div-float/2addr v0, v2

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 12
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mu$c;

    .line 14
    iget-boolean v3, v2, Lcom/pspdfkit/internal/mu$c;->f:Z

    if-eqz v3, :cond_0

    .line 15
    iget-object v3, v2, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    iget-object v2, v2, Lcom/pspdfkit/internal/mu$c;->b:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v5, p0, Lcom/pspdfkit/internal/mu;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 19
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return v1

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lcom/pspdfkit/internal/mu;->i:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_5

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->h()F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/mu;->i:F

    div-float/2addr v0, v2

    .line 24
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 25
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mu$c;

    .line 27
    iget-boolean v3, v2, Lcom/pspdfkit/internal/mu$c;->f:Z

    if-eqz v3, :cond_3

    .line 28
    iget-object v3, v2, Lcom/pspdfkit/internal/mu$c;->e:Landroid/graphics/Bitmap;

    iget-object v2, v2, Lcom/pspdfkit/internal/mu$c;->b:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v5, p0, Lcom/pspdfkit/internal/mu;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 32
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return v1

    :cond_5
    const/4 p1, 0x0

    return p1

    .line 33
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Trying to draw the TileSubview while the State is not initialized, meaning that the view was never bound to the page, or already recycled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final e()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/im;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->h:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/im;->getLocalVisibleRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->c()V

    :cond_0
    return-void
.end method

.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->n:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/do;->c()V

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public final recycle()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/im$h;->recycle()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mu;->o:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/mu;->o:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/mu;->a()V

    return-void
.end method

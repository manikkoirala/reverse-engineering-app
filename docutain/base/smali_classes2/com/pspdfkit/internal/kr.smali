.class public Lcom/pspdfkit/internal/kr;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu$SharingMenuListener;
.implements Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/kr$b;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/ui/PdfFragment;

.field private c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

.field private d:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

.field private e:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

.field private f:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

.field private g:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

.field l:Lcom/pspdfkit/internal/y9;

.field private m:Lcom/pspdfkit/internal/j9;

.field private n:Lcom/pspdfkit/internal/r9;

.field private o:Lcom/pspdfkit/internal/kr$b;

.field private p:Lcom/pspdfkit/document/sharing/ShareTarget;

.field private q:Lcom/pspdfkit/document/sharing/ShareAction;

.field private r:Landroid/os/Bundle;

.field private final s:Lcom/pspdfkit/listeners/DocumentListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/kr;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/kr$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/kr$a;-><init>(Lcom/pspdfkit/internal/kr;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->s:Lcom/pspdfkit/listeners/DocumentListener;

    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 17
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    :cond_0
    return-void
.end method

.method public static a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/ui/PdfFragment;)Lcom/pspdfkit/internal/kr;
    .locals 3

    const-string v0, "com.pspdfkit.ui.SharingMenuFragment.FRAGMENT_TAG"

    .line 12
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/kr;

    if-nez v1, :cond_0

    .line 14
    new-instance v1, Lcom/pspdfkit/internal/kr;

    invoke-direct {v1}, Lcom/pspdfkit/internal/kr;-><init>()V

    .line 15
    :cond_0
    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object p2

    sget-object v2, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-virtual {p2, v2}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p2

    iput-boolean p2, v1, Lcom/pspdfkit/internal/kr;->h:Z

    .line 17
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->isPrintingAvailable(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Z

    move-result p2

    iput-boolean p2, v1, Lcom/pspdfkit/internal/kr;->i:Z

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v1, Lcom/pspdfkit/internal/kr;->j:Ljava/lang/String;

    .line 19
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-nez p1, :cond_1

    .line 20
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    .line 21
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentTransaction;->commitNow()V

    :cond_1
    return-object v1
.end method

.method public static a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;Lcom/pspdfkit/document/printing/PrintOptionsProvider;)Lcom/pspdfkit/internal/kr;
    .locals 1

    const-string v0, "com.pspdfkit.ui.SharingMenuFragment.FRAGMENT_TAG"

    .line 1
    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/kr;

    if-eqz p0, :cond_0

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    .line 4
    iput-object p4, p0, Lcom/pspdfkit/internal/kr;->f:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    .line 5
    iput-object p5, p0, Lcom/pspdfkit/internal/kr;->g:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    .line 6
    iput-object p6, p0, Lcom/pspdfkit/internal/kr;->d:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    .line 7
    iput-object p7, p0, Lcom/pspdfkit/internal/kr;->e:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p3

    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object p3

    sget-object p4, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-virtual {p3, p4}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/pspdfkit/internal/kr;->h:Z

    .line 9
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object p3

    invoke-virtual {p3, p1}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->isPrintingAvailable(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/pspdfkit/internal/kr;->i:Z

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getActivityTitle()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->j:Ljava/lang/String;

    .line 11
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/kr;->a(Lcom/pspdfkit/ui/PdfFragment;)V

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->dismiss()V

    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->l:Lcom/pspdfkit/internal/y9;

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {v0}, Lcom/pspdfkit/internal/y9;->a()V

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->m:Lcom/pspdfkit/internal/j9;

    if-eqz v0, :cond_2

    .line 43
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j9;->a()V

    :cond_2
    return-void
.end method

.method public final a(Lcom/pspdfkit/document/printing/PrintOptionsProvider;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->e:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    return-void
.end method

.method public final a(Lcom/pspdfkit/document/sharing/SharingOptionsProvider;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->d:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 1

    .line 23
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kr;->b()V

    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->s:Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->g:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->f:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    return-void
.end method

.method public final b()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    const-string v1, "STATE_SHARING_MENU_STATE"

    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/kr$b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    return-void

    .line 10
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kr;->performSaveAs()V

    goto :goto_0

    .line 41
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    const-string v2, "STATE_SHARE_TARGET_ACTION"

    .line 42
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/sharing/ShareAction;

    if-eqz v0, :cond_7

    .line 44
    iget-object v2, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    const-string v3, "STATE_SHARE_TARGET_PACKAGE_NAME"

    .line 45
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 48
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 49
    invoke-static {v3, v0, v2}, Lcom/pspdfkit/document/sharing/DocumentSharingIntentHelper;->getShareTarget(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;Ljava/lang/String;)Lcom/pspdfkit/document/sharing/ShareTarget;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 52
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/kr;->performShare(Lcom/pspdfkit/document/sharing/ShareTarget;)V

    goto :goto_0

    .line 53
    :cond_4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kr;->performPrint()V

    goto :goto_0

    .line 54
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    const-string v2, "STATE_SHARING_MENU_SHARE_ACTION"

    .line 55
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/sharing/ShareAction;

    if-eqz v0, :cond_7

    .line 57
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/kr;->showShareMenu(Lcom/pspdfkit/document/sharing/ShareAction;)V

    goto :goto_0

    .line 58
    :cond_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kr;->c()V

    .line 88
    :cond_7
    :goto_0
    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    nop

    :cond_8
    :goto_1
    return-void
.end method

.method public final c()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu$SharingMenuListener;)V

    .line 3
    iget-boolean v1, p0, Lcom/pspdfkit/internal/kr;->h:Z

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu;->setSharingEnabled(Z)V

    .line 4
    iget-boolean v1, p0, Lcom/pspdfkit/internal/kr;->i:Z

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu;->setPrintingEnabled(Z)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v1, :cond_1

    .line 7
    invoke-virtual {v0, p0}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->addActionMenuListener(Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;)V

    .line 9
    :cond_1
    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    .line 10
    sget-object v1, Lcom/pspdfkit/internal/kr$b;->a:Lcom/pspdfkit/internal/kr$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/DefaultSharingMenu;->show()Z

    :cond_2
    :goto_0
    return-void
.end method

.method public final onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;->onActionMenuItemClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;->onActionMenuItemLongClicked(Lcom/pspdfkit/ui/actionmenu/ActionMenu;Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->r:Landroid/os/Bundle;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kr;->b()V

    :cond_0
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method

.method public final onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;->onDisplayActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->onDetach()V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->l:Lcom/pspdfkit/internal/y9;

    if-eqz v0, :cond_1

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/y9;->c()V

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->m:Lcom/pspdfkit/internal/j9;

    if-eqz v0, :cond_2

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j9;->c()V

    .line 12
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->n:Lcom/pspdfkit/internal/r9;

    if-eqz v0, :cond_3

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/r9;->b()V

    :cond_3
    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    return-void
.end method

.method public final onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;->onPrepareActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->c:Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/actionmenu/ActionMenuListener;->onRemoveActionMenu(Lcom/pspdfkit/ui/actionmenu/ActionMenu;)V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->onAttach(Landroidx/fragment/app/FragmentActivity;)V

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->l:Lcom/pspdfkit/internal/y9;

    if-eqz v0, :cond_2

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/y9;->a(Landroidx/fragment/app/FragmentActivity;)V

    .line 11
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->m:Lcom/pspdfkit/internal/j9;

    if-eqz v0, :cond_3

    .line 12
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/j9;->a(Landroidx/fragment/app/FragmentActivity;)V

    .line 14
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->n:Lcom/pspdfkit/internal/r9;

    if-eqz v0, :cond_4

    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r9;->a(Landroidx/fragment/app/FragmentActivity;)V

    :cond_4
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const-string v1, "STATE_SHARING_MENU_STATE"

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto/16 :goto_0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->n:Lcom/pspdfkit/internal/r9;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r9;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 32
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->l:Lcom/pspdfkit/internal/y9;

    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/pspdfkit/internal/kr;->p:Lcom/pspdfkit/document/sharing/ShareTarget;

    if-eqz v2, :cond_6

    .line 34
    invoke-virtual {v0}, Lcom/pspdfkit/internal/y9;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->p:Lcom/pspdfkit/document/sharing/ShareTarget;

    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/ShareTarget;->getShareAction()Lcom/pspdfkit/document/sharing/ShareAction;

    move-result-object v0

    const-string v1, "STATE_SHARE_TARGET_ACTION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->p:Lcom/pspdfkit/document/sharing/ShareTarget;

    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/ShareTarget;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "STATE_SHARE_TARGET_PACKAGE_NAME"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->m:Lcom/pspdfkit/internal/j9;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/pspdfkit/internal/j9;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 40
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->q:Lcom/pspdfkit/document/sharing/ShareAction;

    const-string v1, "STATE_SHARING_MENU_SHARE_ACTION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 43
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/ActionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public final performPrint()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/kr;->i:Z

    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v6

    if-gez v6, :cond_1

    return-void

    .line 5
    :cond_1
    sget-object v0, Lcom/pspdfkit/internal/kr$b;->c:Lcom/pspdfkit/internal/kr$b;

    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/j9;

    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    iget-object v1, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 8
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/internal/kr;->g:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    iget-object v5, p0, Lcom/pspdfkit/internal/kr;->e:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    iget-object v7, p0, Lcom/pspdfkit/internal/kr;->j:Ljava/lang/String;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/j9;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;Lcom/pspdfkit/document/printing/PrintOptionsProvider;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->m:Lcom/pspdfkit/internal/j9;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/j9;->d()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final performSaveAs()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/kr;->h:Z

    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v6

    if-gez v6, :cond_1

    return-void

    .line 5
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/r9;

    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    iget-object v1, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/internal/kr;->f:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    sget-object v5, Lcom/pspdfkit/document/sharing/ShareAction;->VIEW:Lcom/pspdfkit/document/sharing/ShareAction;

    iget-object v7, p0, Lcom/pspdfkit/internal/kr;->j:Ljava/lang/String;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/r9;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/document/sharing/ShareAction;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->n:Lcom/pspdfkit/internal/r9;

    .line 12
    sget-object v1, Lcom/pspdfkit/internal/kr$b;->e:Lcom/pspdfkit/internal/kr$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/r9;->c()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final performShare(Lcom/pspdfkit/document/sharing/ShareTarget;)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/kr;->h:Z

    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getPageIndex()I

    move-result v7

    if-gez v7, :cond_1

    return-void

    .line 5
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/y9;

    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    iget-object v1, p0, Lcom/pspdfkit/internal/kr;->b:Lcom/pspdfkit/ui/PdfFragment;

    .line 7
    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/internal/kr;->f:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    iget-object v5, p0, Lcom/pspdfkit/internal/kr;->d:Lcom/pspdfkit/document/sharing/SharingOptionsProvider;

    iget-object v8, p0, Lcom/pspdfkit/internal/kr;->j:Ljava/lang/String;

    move-object v1, v0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/pspdfkit/internal/y9;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/document/sharing/SharingOptionsProvider;Lcom/pspdfkit/document/sharing/ShareTarget;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->l:Lcom/pspdfkit/internal/y9;

    .line 13
    sget-object v1, Lcom/pspdfkit/internal/kr$b;->d:Lcom/pspdfkit/internal/kr$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->p:Lcom/pspdfkit/document/sharing/ShareTarget;

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/y9;->d()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final showShareMenu(Lcom/pspdfkit/document/sharing/ShareAction;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/pspdfkit/ui/actionmenu/SharingMenu;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/ui/actionmenu/SharingMenu$SharingMenuListener;)V

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/actionmenu/SharingMenu;->setShareAction(Lcom/pspdfkit/document/sharing/ShareAction;)V

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/kr;->k:Lcom/pspdfkit/ui/actionmenu/SharingMenu;

    .line 5
    sget-object v1, Lcom/pspdfkit/internal/kr$b;->b:Lcom/pspdfkit/internal/kr$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/kr;->o:Lcom/pspdfkit/internal/kr$b;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/kr;->q:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/ui/actionmenu/SharingMenu;->show()Z

    return-void
.end method

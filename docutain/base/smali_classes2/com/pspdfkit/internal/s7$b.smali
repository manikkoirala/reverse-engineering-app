.class final Lcom/pspdfkit/internal/s7$b;
.super Lcom/pspdfkit/internal/jni/NativeDataSink;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/s7;->createDataSink(Lcom/pspdfkit/internal/jni/NativeDataSinkOption;)Lcom/pspdfkit/internal/jni/NativeDataSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/document/providers/WritableDataProvider;


# direct methods
.method constructor <init>(Lcom/pspdfkit/document/providers/WritableDataProvider;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/s7$b;->a:Lcom/pspdfkit/document/providers/WritableDataProvider;

    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeDataSink;-><init>()V

    return-void
.end method


# virtual methods
.method public final finish()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final writeData([B)Z
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/s7$b;->a:Lcom/pspdfkit/document/providers/WritableDataProvider;

    invoke-interface {v0, p1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->write([B)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "NativeDataProviderShim"

    const-string v2, "Exception on writeData: %s"

    .line 3
    invoke-static {p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1
.end method

.class public final Lcom/pspdfkit/internal/sq;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/sq$b;,
        Lcom/pspdfkit/internal/sq$a;
    }
.end annotation


# instance fields
.field public final b:Landroid/text/style/BackgroundColorSpan;

.field public final c:Landroid/text/style/ForegroundColorSpan;

.field public final d:Ljava/util/ArrayList;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:I

.field private final h:Lcom/pspdfkit/internal/sq$a;

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/pspdfkit/internal/sq$a;IZ)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    .line 23
    iput-object p1, p0, Lcom/pspdfkit/internal/sq;->e:Landroid/view/View;

    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/pspdfkit/internal/sq;->f:Landroid/view/LayoutInflater;

    .line 25
    new-instance p1, Landroid/text/style/BackgroundColorSpan;

    invoke-static {p2}, Lcom/pspdfkit/internal/sq$a;->-$$Nest$fgetd(Lcom/pspdfkit/internal/sq$a;)I

    move-result v0

    invoke-direct {p1, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/sq;->b:Landroid/text/style/BackgroundColorSpan;

    .line 26
    new-instance p1, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p2}, Lcom/pspdfkit/internal/sq$a;->-$$Nest$fgete(Lcom/pspdfkit/internal/sq$a;)I

    move-result v0

    invoke-direct {p1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/sq;->c:Landroid/text/style/ForegroundColorSpan;

    .line 27
    iput-object p2, p0, Lcom/pspdfkit/internal/sq;->h:Lcom/pspdfkit/internal/sq$a;

    .line 28
    iput p3, p0, Lcom/pspdfkit/internal/sq;->g:I

    .line 29
    iput-boolean p4, p0, Lcom/pspdfkit/internal/sq;->i:Z

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/search/SearchResult;

    return-object p1
.end method

.method public final getItemId(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/search/SearchResult;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/sq;->f:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/pspdfkit/internal/sq;->g:I

    invoke-virtual {p2, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2
    iget-object p3, p0, Lcom/pspdfkit/internal/sq;->h:Lcom/pspdfkit/internal/sq$a;

    invoke-static {p3}, Lcom/pspdfkit/internal/sq$a;->-$$Nest$fgeta(Lcom/pspdfkit/internal/sq$a;)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 3
    new-instance p3, Lcom/pspdfkit/internal/sq$b;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__search_item_page:I

    .line 4
    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/pspdfkit/R$id;->pspdf__search_item_snippet:I

    .line 5
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/pspdfkit/internal/sq;->h:Lcom/pspdfkit/internal/sq$a;

    invoke-direct {p3, v2, v3, v4, v0}, Lcom/pspdfkit/internal/sq$b;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/pspdfkit/internal/sq$a;Lcom/pspdfkit/internal/sq$b-IA;)V

    .line 7
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 10
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/sq$b;

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/sq;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/search/SearchResult;

    .line 13
    iget-object v2, p3, Lcom/pspdfkit/internal/sq$b;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 14
    iget-boolean v3, p0, Lcom/pspdfkit/internal/sq;->i:Z

    if-eqz v3, :cond_1

    .line 15
    iget-object v0, p1, Lcom/pspdfkit/document/search/SearchResult;->document:Lcom/pspdfkit/document/PdfDocument;

    iget v3, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    invoke-interface {v0, v3, v1}, Lcom/pspdfkit/document/PdfDocument;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/sq;->e:Landroid/view/View;

    .line 20
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    iget v6, p1, Lcom/pspdfkit/document/search/SearchResult;->pageIndex:I

    add-int/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v1

    .line 21
    invoke-static {v0, v3, v2, v5}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 25
    :cond_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    :cond_3
    iget-object v0, p3, Lcom/pspdfkit/internal/sq$b;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 27
    iget-object p1, p1, Lcom/pspdfkit/document/search/SearchResult;->snippet:Lcom/pspdfkit/document/search/SearchResult$TextSnippet;

    if-eqz p1, :cond_4

    .line 29
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p1, Lcom/pspdfkit/document/search/SearchResult$TextSnippet;->text:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 30
    iget-object v1, p1, Lcom/pspdfkit/document/search/SearchResult$TextSnippet;->rangeInSnippet:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v1}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v1

    .line 31
    iget-object p1, p1, Lcom/pspdfkit/document/search/SearchResult$TextSnippet;->rangeInSnippet:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {p1}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result p1

    .line 32
    iget-object v2, p0, Lcom/pspdfkit/internal/sq;->b:Landroid/text/style/BackgroundColorSpan;

    const/16 v3, 0x12

    invoke-virtual {v0, v2, v1, p1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/internal/sq;->c:Landroid/text/style/ForegroundColorSpan;

    const/16 v3, 0x21

    invoke-virtual {v0, v2, v1, p1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 34
    iget-object p1, p3, Lcom/pspdfkit/internal/sq$b;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    const-string p1, ""

    .line 36
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_0
    return-object p2
.end method

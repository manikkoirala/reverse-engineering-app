.class public final Lcom/pspdfkit/internal/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/views/annotations/d$a;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationSelectedListener;
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/am$a;,
        Lcom/pspdfkit/internal/am$b;
    }
.end annotation


# static fields
.field private static final v:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final w:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/pspdfkit/internal/dm;

.field private final c:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final d:Lcom/pspdfkit/internal/w0;

.field private final e:Lcom/pspdfkit/internal/u0;

.field private final f:Lcom/pspdfkit/internal/fl;

.field private final g:Lcom/pspdfkit/internal/f2;

.field private final h:Lcom/pspdfkit/internal/na;

.field private final i:Landroid/graphics/Matrix;

.field private final j:Lcom/pspdfkit/internal/xc;

.field private k:Z

.field private l:Lcom/pspdfkit/internal/y1;

.field private m:Lcom/pspdfkit/internal/ka;

.field private final n:Ljava/util/ArrayList;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private t:Lcom/pspdfkit/internal/ti;

.field private u:Lcom/pspdfkit/internal/z1;


# direct methods
.method public static synthetic $r8$lambda$5Oh86f4l_VGhaOsjlkmshOXg3tM(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V

    return-void
.end method

.method public static synthetic $r8$lambda$FJSF6xGOQu8swMyD-15okAGNtE0(Lcom/pspdfkit/internal/am;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/am;->m(Lcom/pspdfkit/internal/am;)V

    return-void
.end method

.method public static synthetic $r8$lambda$KByZVSLlRF0PuZ81v-IhmAw3gpI(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->d(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Q0X4ZE_ReltWz3_Uu0TSuPS3_14(Lcom/pspdfkit/internal/am;Landroid/graphics/RectF;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Landroid/graphics/RectF;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ibtps03IbbcVNE9asKtHWiJdQaU(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/z1;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/z1;)V

    return-void
.end method

.method public static synthetic $r8$lambda$imL_Dq_UX_IRzxVdmxEFvtfYja4(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qALrEPy2nM_y8YPdHRuqM9x3DcY(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 33

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->INK:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v1, 0xf

    new-array v2, v1, [Lcom/pspdfkit/annotations/AnnotationType;

    .line 2
    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 3
    sget-object v5, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v6, 0x1

    aput-object v5, v2, v6

    .line 4
    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v8, 0x2

    aput-object v7, v2, v8

    .line 5
    sget-object v9, Lcom/pspdfkit/annotations/AnnotationType;->SQUIGGLY:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v10, 0x3

    aput-object v9, v2, v10

    .line 6
    sget-object v11, Lcom/pspdfkit/annotations/AnnotationType;->STRIKEOUT:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v12, 0x4

    aput-object v11, v2, v12

    .line 7
    sget-object v13, Lcom/pspdfkit/annotations/AnnotationType;->UNDERLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/4 v14, 0x5

    aput-object v13, v2, v14

    .line 8
    sget-object v15, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v16, 0x6

    aput-object v15, v2, v16

    .line 9
    sget-object v17, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v18, 0x7

    aput-object v17, v2, v18

    .line 10
    sget-object v19, Lcom/pspdfkit/annotations/AnnotationType;->SQUARE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v20, 0x8

    aput-object v19, v2, v20

    .line 11
    sget-object v21, Lcom/pspdfkit/annotations/AnnotationType;->CIRCLE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v22, 0x9

    aput-object v21, v2, v22

    .line 12
    sget-object v23, Lcom/pspdfkit/annotations/AnnotationType;->POLYGON:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v24, 0xa

    aput-object v23, v2, v24

    .line 13
    sget-object v25, Lcom/pspdfkit/annotations/AnnotationType;->POLYLINE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v26, 0xb

    aput-object v25, v2, v26

    .line 14
    sget-object v27, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v28, 0xc

    aput-object v27, v2, v28

    .line 15
    sget-object v29, Lcom/pspdfkit/annotations/AnnotationType;->SOUND:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v30, 0xd

    aput-object v29, v2, v30

    .line 16
    sget-object v31, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    const/16 v32, 0xe

    aput-object v31, v2, v32

    .line 17
    invoke-static {v0, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    sput-object v2, Lcom/pspdfkit/internal/am;->v:Ljava/util/EnumSet;

    new-array v1, v1, [Lcom/pspdfkit/annotations/AnnotationType;

    aput-object v3, v1, v4

    aput-object v5, v1, v6

    aput-object v27, v1, v8

    aput-object v29, v1, v10

    aput-object v15, v1, v12

    aput-object v17, v1, v14

    aput-object v19, v1, v16

    aput-object v21, v1, v18

    aput-object v23, v1, v20

    aput-object v25, v1, v22

    aput-object v7, v1, v24

    aput-object v9, v1, v26

    aput-object v11, v1, v28

    aput-object v13, v1, v30

    aput-object v31, v1, v32

    .line 57
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/am;->w:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i1;Lcom/pspdfkit/internal/views/document/a;Lcom/pspdfkit/internal/fl;Lcom/pspdfkit/internal/a1;Lcom/pspdfkit/internal/f2;)V
    .locals 1

    const-string v0, "pageLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfDocument"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationEventDispatcher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationEditorController"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditRecordedListener"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationHitDetector"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themeConfiguration"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 7
    iput-object p3, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 9
    iput-object p4, p0, Lcom/pspdfkit/internal/am;->d:Lcom/pspdfkit/internal/w0;

    .line 11
    iput-object p5, p0, Lcom/pspdfkit/internal/am;->e:Lcom/pspdfkit/internal/u0;

    .line 12
    iput-object p6, p0, Lcom/pspdfkit/internal/am;->f:Lcom/pspdfkit/internal/fl;

    .line 14
    iput-object p8, p0, Lcom/pspdfkit/internal/am;->g:Lcom/pspdfkit/internal/f2;

    .line 95
    new-instance p4, Landroid/graphics/Matrix;

    invoke-direct {p4}, Landroid/graphics/Matrix;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    .line 111
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    .line 126
    new-instance p4, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p4}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/am;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 1173
    new-instance p4, Lcom/pspdfkit/internal/z1;

    invoke-direct {p4, p1, p3, p8}, Lcom/pspdfkit/internal/z1;-><init>(Lcom/pspdfkit/internal/en;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    iput-object p4, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    .line 1174
    new-instance p1, Lcom/pspdfkit/internal/y1;

    invoke-direct {p1, p4, p3, p8}, Lcom/pspdfkit/internal/y1;-><init>(Lcom/pspdfkit/internal/z1;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    .line 1175
    new-instance p1, Lcom/pspdfkit/internal/am$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/am$a;-><init>(Lcom/pspdfkit/internal/am;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/am;->j:Lcom/pspdfkit/internal/xc;

    .line 1176
    new-instance p1, Lcom/pspdfkit/internal/na;

    invoke-direct {p1, p7}, Lcom/pspdfkit/internal/na;-><init>(Lcom/pspdfkit/internal/a1;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    .line 1177
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/am;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/am;Landroid/view/MotionEvent;)Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1451
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    .line 1452
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p0}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object p0

    const-string p1, "annotationHitDetector.ge\u2026v, pdfToViewMatrix, true)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1453
    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 1459
    :cond_0
    new-instance p1, Lcom/pspdfkit/internal/bm;

    invoke-direct {p1}, Lcom/pspdfkit/internal/bm;-><init>()V

    .line 1460
    invoke-static {p0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 p1, 0x0

    .line 1495
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/Annotation;

    :goto_0
    return-object p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/u0;
    .locals 0

    .line 3
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->e:Lcom/pspdfkit/internal/u0;

    return-object p0
.end method

.method public static final synthetic a()Ljava/util/EnumSet;
    .locals 1

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/am;->w:Ljava/util/EnumSet;

    return-object v0
.end method

.method private static final a(Lcom/pspdfkit/internal/am;Landroid/graphics/RectF;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$pdfRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1437
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    if-eqz v0, :cond_0

    .line 1438
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/RectF;)V

    :cond_0
    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/ka;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/am;->m:Lcom/pspdfkit/internal/ka;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationRenderingCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1421
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/Annotation;

    .line 1422
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    .line 1426
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 1427
    new-instance v1, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, v0}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V

    invoke-virtual {p1, p0, v1}, Lcom/pspdfkit/internal/w1;->b(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/z1;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$selectionView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1405
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1406
    invoke-virtual {p1}, Lcom/pspdfkit/internal/z1;->f()[Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/internal/views/annotations/a;Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/w1;Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 1

    const-string v0, "$annotationRenderingCoordinator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1428
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    .line 1429
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;Z)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 1439
    const-class v0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 1440
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1441
    sget-object v1, Lcom/pspdfkit/document/DocumentPermissions;->ANNOTATIONS_AND_FORMS:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/zf;->hasPermission(Lcom/pspdfkit/document/DocumentPermissions;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1443
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object p1

    const-string v1, "configuration.editableAnnotationTypes"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1444
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 1445
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEditableAnnotationTypes()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1447
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/am;->v:Ljava/util/EnumSet;

    .line 1450
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/na;->a(Ljava/util/EnumSet;)V

    return-void
.end method

.method private final a([Lcom/pspdfkit/internal/views/annotations/a;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;Z)V"
        }
    .end annotation

    .line 1407
    array-length v0, p1

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    .line 1408
    :cond_1
    array-length v0, p1

    :goto_1
    if-ge v1, v0, :cond_5

    aget-object v2, p1, v1

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 1409
    instance-of v4, v2, Lcom/pspdfkit/internal/views/annotations/m;

    if-eqz v4, :cond_2

    .line 1410
    check-cast v2, Lcom/pspdfkit/internal/views/annotations/m;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/views/annotations/m;->c()Lcom/pspdfkit/internal/views/annotations/e;

    move-result-object v2

    goto :goto_2

    .line 1412
    :cond_2
    instance-of v4, v2, Lcom/pspdfkit/internal/views/annotations/e;

    if-eqz v4, :cond_3

    .line 1413
    check-cast v2, Lcom/pspdfkit/internal/views/annotations/e;

    goto :goto_2

    :cond_3
    move-object v2, v3

    :goto_2
    if-eqz v2, :cond_4

    .line 1414
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/views/annotations/d;->setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V

    .line 1415
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/views/annotations/e;->setOnEditRecordedListener(Lcom/pspdfkit/internal/fl;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1418
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 1419
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    .line 1420
    array-length v1, p1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    .line 1315
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    .line 1316
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/am;Z)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, v0, p1, v1}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    move-result p0

    return p0
.end method

.method private final a(ZZZ)Z
    .locals 8

    .line 1340
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    .line 1342
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    if-ne p1, p2, :cond_0

    .line 1343
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/z1;->f()[Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    invoke-direct {p0, p1, v2}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/internal/views/annotations/a;Z)V

    .line 1344
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    iget-object p2, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return v1

    .line 1348
    :cond_1
    iput-boolean v1, p0, Lcom/pspdfkit/internal/am;->k:Z

    .line 1349
    iput-boolean p2, p0, Lcom/pspdfkit/internal/am;->o:Z

    const/4 v0, 0x0

    .line 1350
    iput-object v0, p0, Lcom/pspdfkit/internal/am;->m:Lcom/pspdfkit/internal/ka;

    .line 1351
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v3, :cond_2

    .line 1352
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v3}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1354
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v3}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 1355
    iput-object v0, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 1360
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1361
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1366
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    .line 1367
    iget-object v4, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v4, v1}, Lcom/pspdfkit/internal/y1;->c(Z)V

    .line 1368
    iget-object v4, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/y1;->a()V

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1369
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 1370
    iget-object v4, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    .line 1371
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    if-ge v1, v4, :cond_4

    .line 1373
    iget-object v6, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1374
    instance-of v7, v6, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v7, :cond_3

    .line 1375
    check-cast v6, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1376
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/views/annotations/a;

    .line 1377
    invoke-interface {v4}, Lcom/pspdfkit/internal/views/annotations/a;->k()Z

    .line 1380
    invoke-interface {v4}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1381
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1382
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    goto :goto_1

    :cond_6
    if-eqz p1, :cond_7

    .line 1388
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 1389
    iget-object v4, p0, Lcom/pspdfkit/internal/am;->d:Lcom/pspdfkit/internal/w0;

    check-cast v4, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v4, v1, p2}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_2

    :cond_7
    if-eqz p3, :cond_8

    .line 1395
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1396
    invoke-virtual {v3}, Lcom/pspdfkit/internal/z1;->f()[Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    invoke-direct {p0, p1, v2}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/internal/views/annotations/a;Z)V

    return v2

    .line 1402
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 1403
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p1

    .line 1404
    new-instance p2, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda6;

    invoke-direct {p2, p0, v3}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/z1;)V

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/w1;->b(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    return v2
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/w0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->d:Lcom/pspdfkit/internal/w0;

    return-object p0
.end method

.method private static final b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationRenderingCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 32
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 33
    new-instance v2, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {p1, v1, v2}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;Lcom/pspdfkit/internal/w1$a;)V

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/am;Z)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/am;->q:Z

    return-void
.end method

.method public static final b(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    .line 35
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/na;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    return-object p0
.end method

.method public static final c(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 3

    .line 3
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/w1;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p0, :cond_0

    goto :goto_1

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne p0, v2, :cond_1

    goto :goto_2

    .line 14
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBlendMode()Lcom/pspdfkit/annotations/BlendMode;

    move-result-object p0

    sget-object v2, Lcom/pspdfkit/annotations/BlendMode;->NORMAL:Lcom/pspdfkit/annotations/BlendMode;

    if-eq p0, v2, :cond_2

    goto :goto_2

    .line 19
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result p0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float p0, p0, v2

    if-nez p0, :cond_3

    const/4 p0, 0x1

    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_4

    goto :goto_1

    .line 22
    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object p1, Lcom/pspdfkit/internal/am$b;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, p1, p0

    packed-switch p0, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    :goto_2
    :pswitch_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/configuration/PdfConfiguration;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-object p0
.end method

.method private static final d(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$editedAnnotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    if-eqz v0, :cond_3

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/w1;->d(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p1

    const-string v0, "pageLayout\n            .\u2026ationIntoView(annotation)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/m;

    if-eqz v0, :cond_0

    .line 8
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/m;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/annotations/m;->c()Lcom/pspdfkit/internal/views/annotations/e;

    move-result-object v0

    goto :goto_0

    .line 10
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/internal/views/annotations/e;

    if-eqz v0, :cond_1

    .line 11
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/e;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 12
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/annotations/d;->setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V

    .line 13
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->f:Lcom/pspdfkit/internal/fl;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/views/annotations/e;->setOnEditRecordedListener(Lcom/pspdfkit/internal/fl;)V

    .line 17
    :cond_2
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    .line 18
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    :cond_3
    return-void
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/am;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/pspdfkit/internal/am;->k:Z

    return p0
.end method

.method public static final synthetic f(Lcom/pspdfkit/internal/am;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/pspdfkit/internal/am;->p:Z

    return p0
.end method

.method public static final synthetic g(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/fl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->f:Lcom/pspdfkit/internal/fl;

    return-object p0
.end method

.method public static final synthetic h(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/dm;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method public static final synthetic i(Lcom/pspdfkit/internal/am;)Landroid/graphics/Matrix;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    return-object p0
.end method

.method public static final synthetic j(Lcom/pspdfkit/internal/am;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic k(Lcom/pspdfkit/internal/am;)Lcom/pspdfkit/internal/ka;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->m:Lcom/pspdfkit/internal/ka;

    return-object p0
.end method

.method public static final synthetic l(Lcom/pspdfkit/internal/am;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/am;->o:Z

    return-void
.end method

.method private static final m(Lcom/pspdfkit/internal/am;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-static {p0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 3
    invoke-virtual {p0, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p0

    const-wide/16 v0, 0x12c

    .line 4
    invoke-virtual {p0, v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 1

    const-string v0, "viewPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->t:Lcom/pspdfkit/internal/ti;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ti;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 2

    const-string v0, "pdfRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1435
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 1436
    new-instance v1, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/am;Landroid/graphics/RectF;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 6

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1317
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v2

    .line 1318
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1319
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    invoke-virtual {v3, p1}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1320
    invoke-direct {p0, v0, v1, v1}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    goto :goto_1

    .line 1321
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/am;->b(Lcom/pspdfkit/annotations/Annotation;)V

    .line 1326
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/Annotation;

    .line 1327
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v5

    or-int/2addr v3, v5

    .line 1328
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v4

    or-int/2addr v2, v4

    goto :goto_0

    .line 1330
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/y1;->d(Z)V

    .line 1331
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/y1;->e(Z)V

    if-eqz v2, :cond_2

    .line 1333
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/y1;->a()V

    .line 1337
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/y1;->b()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1338
    :catch_0
    invoke-direct {p0, v0, v1, v1}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    :cond_3
    :goto_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;I)V
    .locals 8

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    monitor-enter v0

    :try_start_0
    const-string v2, "configuration"

    .line 6
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->MEASUREMENT_TOOLS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMeasurementsEnabled()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    if-nez v1, :cond_1

    return-void

    .line 255
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/ti;

    .line 256
    iget-object v1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v1, "pageLayout.context"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    iget-object v6, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    .line 260
    invoke-static {v3}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v7

    const-string v1, "get(context)"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    move v4, p2

    move-object v5, p1

    .line 261
    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/ti;-><init>(Landroid/content/Context;ILcom/pspdfkit/internal/zf;Landroid/graphics/Matrix;Lcom/pspdfkit/preferences/PSPDFKitPreferences;)V

    .line 262
    iput-object v0, p0, Lcom/pspdfkit/internal/am;->t:Lcom/pspdfkit/internal/ti;

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1431
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object p1

    .line 1433
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 1434
    instance-of v1, v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final varargs a([Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/pspdfkit/annotations/Annotation;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/am;->a(Z[Lcom/pspdfkit/annotations/Annotation;)Z

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1430
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final a(ZZ)Z
    .locals 1

    const/4 v0, 0x0

    .line 1339
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    move-result p1

    return p1
.end method

.method public final varargs a(Z[Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "annotations"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/pspdfkit/annotations/Annotation;

    .line 266
    array-length v4, v3

    const-string v5, "annotation"

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-le v4, v6, :cond_5

    .line 269
    array-length v4, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_0
    if-ge v8, v4, :cond_1

    aget-object v11, v3, v8

    .line 270
    iget-object v12, v0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    invoke-virtual {v12, v11}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v12

    if-eqz v12, :cond_0

    add-int/lit8 v9, v9, 0x1

    .line 271
    invoke-static {v11, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    sget-object v12, Lcom/pspdfkit/internal/am;->w:Ljava/util/EnumSet;

    .line 273
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 274
    instance-of v11, v11, Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-nez v11, :cond_0

    add-int/lit8 v10, v10, 0x1

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    if-le v9, v6, :cond_3

    if-ne v10, v9, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_4

    goto :goto_3

    .line 280
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t select multiple annotations that does not support multi selection."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :cond_5
    :goto_3
    iget-boolean v4, v0, Lcom/pspdfkit/internal/am;->k:Z

    if-eqz v4, :cond_a

    .line 282
    array-length v4, v3

    iget-object v8, v0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eq v4, v8, :cond_6

    goto :goto_5

    .line 283
    :cond_6
    array-length v4, v3

    const/4 v8, 0x0

    :goto_4
    if-ge v8, v4, :cond_8

    aget-object v9, v3, v8

    .line 284
    iget-object v10, v0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    :goto_5
    const/4 v3, 0x0

    goto :goto_6

    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_8
    const/4 v3, 0x1

    :goto_6
    if-eqz v3, :cond_9

    return v7

    .line 285
    :cond_9
    invoke-direct {v0, v6, v6, v7}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    .line 286
    :cond_a
    iput-boolean v6, v0, Lcom/pspdfkit/internal/am;->k:Z

    .line 289
    new-instance v3, Lcom/pspdfkit/internal/views/annotations/c;

    invoke-direct {v3}, Lcom/pspdfkit/internal/views/annotations/c;-><init>()V

    .line 290
    new-instance v4, Ljava/util/ArrayList;

    array-length v8, v2

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 291
    iget-object v8, v0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 292
    new-instance v8, Lcom/pspdfkit/internal/z1;

    .line 293
    iget-object v9, v0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 294
    iget-object v10, v0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 295
    invoke-static {}, Lcom/pspdfkit/internal/x5;->a()Lcom/pspdfkit/internal/f2;

    move-result-object v11

    const-string v12, "getAnnotationThemeConfiguration()"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    invoke-direct {v8, v9, v10, v11}, Lcom/pspdfkit/internal/z1;-><init>(Lcom/pspdfkit/internal/en;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    iput-object v8, v0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    .line 301
    new-instance v9, Lcom/pspdfkit/internal/y1;

    iget-object v10, v0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v11, v0, Lcom/pspdfkit/internal/am;->g:Lcom/pspdfkit/internal/f2;

    invoke-direct {v9, v8, v10, v11}, Lcom/pspdfkit/internal/y1;-><init>(Lcom/pspdfkit/internal/z1;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    iput-object v9, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    goto :goto_7

    .line 303
    :cond_b
    iget-object v8, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    iget-object v9, v0, Lcom/pspdfkit/internal/am;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    iget-object v10, v0, Lcom/pspdfkit/internal/am;->g:Lcom/pspdfkit/internal/f2;

    invoke-virtual {v8, v9, v10}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    .line 305
    :goto_7
    iget-object v8, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    iget-object v9, v0, Lcom/pspdfkit/internal/am;->t:Lcom/pspdfkit/internal/ti;

    invoke-virtual {v8, v9}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/internal/ti;)V

    .line 314
    new-instance v8, Ljava/util/ArrayList;

    array-length v9, v2

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 315
    array-length v9, v2

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_8
    if-ge v10, v9, :cond_10

    aget-object v14, v2, v10

    .line 316
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v15

    const-string v6, "select_annotation"

    .line 317
    invoke-virtual {v15, v6}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v6

    .line 318
    invoke-virtual {v6, v14}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v6

    .line 319
    invoke-virtual {v6}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 320
    iget-object v6, v0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    invoke-virtual {v6, v14}, Lcom/pspdfkit/internal/na;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 321
    iget-object v6, v0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    invoke-virtual {v6, v14}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 322
    iget-object v6, v0, Lcom/pspdfkit/internal/am;->d:Lcom/pspdfkit/internal/w0;

    .line 323
    iget-object v15, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    .line 324
    check-cast v6, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v6, v15, v14, v1}, Lcom/pspdfkit/internal/i1;->a(Lcom/pspdfkit/internal/y1;Lcom/pspdfkit/annotations/Annotation;Z)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 330
    iget-object v6, v0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-virtual {v14}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v6

    or-int/2addr v12, v6

    .line 332
    invoke-virtual {v14}, Lcom/pspdfkit/annotations/Annotation;->hasLockedContents()Z

    move-result v6

    or-int/2addr v13, v6

    .line 333
    invoke-static {v14, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 334
    sget-object v6, Lcom/pspdfkit/internal/am;->w:Ljava/util/EnumSet;

    .line 335
    invoke-virtual {v14}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v15

    invoke-virtual {v6, v15}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 336
    iget-object v6, v0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    .line 337
    invoke-virtual {v6}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v6

    .line 338
    invoke-virtual {v6, v14}, Lcom/pspdfkit/internal/w1;->d(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v6

    const-string v15, "pageLayout\n            .\u2026ationIntoView(annotation)"

    invoke-static {v6, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    instance-of v15, v6, Lcom/pspdfkit/internal/views/annotations/m;

    if-eqz v15, :cond_c

    .line 340
    move-object v15, v6

    check-cast v15, Lcom/pspdfkit/internal/views/annotations/m;

    invoke-virtual {v15}, Lcom/pspdfkit/internal/views/annotations/m;->c()Lcom/pspdfkit/internal/views/annotations/e;

    move-result-object v15

    goto :goto_9

    .line 342
    :cond_c
    instance-of v15, v6, Lcom/pspdfkit/internal/views/annotations/e;

    if-eqz v15, :cond_d

    .line 343
    move-object v15, v6

    check-cast v15, Lcom/pspdfkit/internal/views/annotations/e;

    goto :goto_9

    :cond_d
    const/4 v15, 0x0

    :goto_9
    if-eqz v15, :cond_e

    .line 344
    invoke-virtual {v15, v0}, Lcom/pspdfkit/internal/views/annotations/d;->setEditTextViewListener(Lcom/pspdfkit/internal/views/annotations/d$a;)V

    .line 345
    iget-object v7, v0, Lcom/pspdfkit/internal/am;->f:Lcom/pspdfkit/internal/fl;

    invoke-virtual {v15, v7}, Lcom/pspdfkit/internal/views/annotations/e;->setOnEditRecordedListener(Lcom/pspdfkit/internal/fl;)V

    .line 349
    :cond_e
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    .line 350
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 351
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    invoke-virtual {v3, v6}, Lcom/pspdfkit/internal/views/annotations/c;->a(Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 353
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    invoke-interface {v6, v1}, Lcom/pspdfkit/internal/views/annotations/a;->b(Z)Z

    move-result v6

    or-int/2addr v11, v6

    :cond_f
    add-int/lit8 v10, v10, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    goto/16 :goto_8

    .line 360
    :cond_10
    iget-object v2, v0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x0

    .line 361
    iput-boolean v2, v0, Lcom/pspdfkit/internal/am;->k:Z

    return v2

    :cond_11
    const/4 v2, 0x0

    .line 367
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v5}, Landroid/view/View;->bringToFront()V

    .line 370
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    iget-boolean v6, v0, Lcom/pspdfkit/internal/am;->p:Z

    if-eqz v6, :cond_12

    const/4 v6, 0x0

    goto :goto_a

    :cond_12
    const/4 v6, 0x4

    :goto_a
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/y1;->c(Z)V

    .line 372
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v5, v12}, Lcom/pspdfkit/internal/y1;->d(Z)V

    .line 373
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v5, v13}, Lcom/pspdfkit/internal/y1;->e(Z)V

    .line 374
    iget-object v5, v0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    new-array v2, v2, [Lcom/pspdfkit/internal/views/annotations/a;

    .line 1298
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .line 1299
    check-cast v2, [Lcom/pspdfkit/internal/views/annotations/a;

    array-length v6, v2

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/pspdfkit/internal/views/annotations/a;

    .line 1300
    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/y1;->a([Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 1303
    iget-object v2, v0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    iget-object v5, v0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-nez v11, :cond_13

    .line 1307
    iget-object v2, v0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-static {v2}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 1313
    :cond_13
    new-instance v2, Lcom/pspdfkit/internal/am$d;

    invoke-direct {v2, v0, v4, v8, v1}, Lcom/pspdfkit/internal/am$d;-><init>(Lcom/pspdfkit/internal/am;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 1314
    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/views/annotations/c;->a(Lcom/pspdfkit/internal/views/annotations/c$a;)V

    const/4 v1, 0x1

    return v1
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 5

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 7
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_0

    .line 8
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 9
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 10
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 11
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-ne v2, p1, :cond_2

    .line 12
    :cond_3
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type com.pspdfkit.ui.overlay.OverlayLayoutParams"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 13
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-nez v3, :cond_4

    goto :goto_1

    .line 14
    :cond_4
    iget-object v2, v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 15
    iget-boolean v2, p0, Lcom/pspdfkit/internal/am;->q:Z

    if-nez v2, :cond_5

    .line 16
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 18
    :cond_5
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    goto :goto_1

    .line 24
    :cond_6
    iget-boolean v2, p0, Lcom/pspdfkit/internal/am;->q:Z

    if-nez v2, :cond_7

    .line 25
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 27
    :cond_7
    iget-object v2, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/y1;->b()V

    .line 28
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    goto :goto_1

    :cond_8
    return-void
.end method

.method public final b()Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, v0, v1, v1}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return v2

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/am;->h:Lcom/pspdfkit/internal/na;

    iget-object v3, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, p1, v3}, Lcom/pspdfkit/internal/na;->a(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v1, "annotationHitDetector.ge\u2026t, pdfToViewMatrix, true)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    .line 40
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 41
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_3
    return v2
.end method

.method public final c()Lcom/pspdfkit/internal/xc;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->j:Lcom/pspdfkit/internal/xc;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->n:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v1, "unmodifiableList(selectedAnnotations)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/internal/y1;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->o:Z

    return v0
.end method

.method public final g()Lcom/pspdfkit/internal/z1;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    return v0
.end method

.method public final i()V
    .locals 2

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/am;->p:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final j()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 6
    instance-of v4, v3, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v4, :cond_0

    .line 7
    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 8
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/views/annotations/a;

    .line 9
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type com.pspdfkit.ui.overlay.OverlayLayoutParams"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 10
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-nez v3, :cond_2

    goto :goto_1

    .line 11
    :cond_2
    iget-object v2, v2, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 12
    iget-boolean v2, p0, Lcom/pspdfkit/internal/am;->q:Z

    if-nez v2, :cond_3

    .line 13
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 15
    :cond_3
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    goto :goto_1

    .line 21
    :cond_4
    iget-boolean v2, p0, Lcom/pspdfkit/internal/am;->q:Z

    if-nez v2, :cond_5

    .line 22
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->p()V

    .line 24
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/am;->l:Lcom/pspdfkit/internal/y1;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/y1;->b()V

    .line 25
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    goto :goto_1

    :cond_6
    return-void
.end method

.method public final k()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/am;->k:Z

    if-nez v0, :cond_1

    return-void

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    const-string v1, "pageLayout.annotationRenderingCoordinator"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    new-instance v1, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/pspdfkit/internal/am$c;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/am$c;-><init>(Lcom/pspdfkit/internal/am;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->doOnSubscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 37
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x2bc

    .line 38
    invoke-virtual {v1, v3, v4, v2}, Lio/reactivex/rxjava3/core/Completable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 43
    new-instance v3, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda4;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/am;)V

    invoke-static {v3}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v3

    .line 44
    invoke-virtual {v1, v3}, Lio/reactivex/rxjava3/core/Completable;->andThen(Lio/reactivex/rxjava3/core/CompletableSource;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    const-wide/16 v3, 0x12c

    .line 52
    invoke-virtual {v1, v3, v4, v2}, Lio/reactivex/rxjava3/core/Completable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 53
    new-instance v2, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/am$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/am;Lcom/pspdfkit/internal/w1;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 66
    iput-object v0, p0, Lcom/pspdfkit/internal/am;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 117
    iget-object v1, p0, Lcom/pspdfkit/internal/am;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    const-string v2, "null cannot be cast to non-null type io.reactivex.rxjava3.disposables.Disposable"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method public final l()V
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->u:Lcom/pspdfkit/internal/z1;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    iget-object v2, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    const-string v2, "pageLayout.getPdfToViewT\u2026ormation(pdfToViewMatrix)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getZoomScale()F

    move-result v2

    .line 5
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "pdfToViewTransformation"

    .line 6
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 508
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    .line 512
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    :goto_0
    if-ge v3, v4, :cond_1

    .line 514
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/pspdfkit/internal/views/annotations/a;

    .line 515
    invoke-interface {v5, v2, v1}, Lcom/pspdfkit/internal/views/annotations/a;->a(FLandroid/graphics/Matrix;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 516
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->t:Lcom/pspdfkit/internal/ti;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/am;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ti;->a(Landroid/graphics/Matrix;)V

    :cond_2
    return-void
.end method

.method public final onAnnotationSelected(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 3

    const-string p2, "annotation"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    const/4 v0, 0x1

    .line 1
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/am;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/am;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 3
    :cond_0
    invoke-direct {p0, v0, v0, p2}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4
    :catch_0
    invoke-direct {p0, v0, p2, p2}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public final onPrepareAnnotationSelection(Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;Lcom/pspdfkit/annotations/Annotation;Z)Z
    .locals 0

    const-string p3, "controller"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "annotation"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final recycle()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, v0, v1, v0}, Lcom/pspdfkit/internal/am;->a(ZZZ)Z

    .line 2
    iput-boolean v1, p0, Lcom/pspdfkit/internal/am;->p:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/am;->s:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    return-void
.end method

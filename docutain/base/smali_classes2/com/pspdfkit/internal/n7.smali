.class public final Lcom/pspdfkit/internal/n7;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/n7$d;
    }
.end annotation


# instance fields
.field private final b:Landroid/content/ClipboardManager;

.field private final c:Landroid/view/View;

.field private final d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

.field private final e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

.field private final f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

.field private final g:Landroid/view/View;

.field private final h:Lcom/google/android/material/textfield/TextInputLayout;

.field private final i:Landroid/widget/EditText;

.field private final j:Landroid/widget/RadioGroup;

.field private k:I

.field private l:Lcom/pspdfkit/internal/n7$d;


# direct methods
.method public static synthetic $r8$lambda$7tF8pQrG4fKBO8efEk6mL7J9YtI(Lcom/pspdfkit/internal/n7;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/n7;->a(Lcom/pspdfkit/internal/n7;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$CWPDqLWarTA2CypWc7dZYgVakE4(Lcom/pspdfkit/internal/n7;Landroid/widget/RadioGroup;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/n7;->a(Lcom/pspdfkit/internal/n7;Landroid/widget/RadioGroup;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Y7PUo2qry1tI6o6SVsWDrXSQn78(Lcom/pspdfkit/internal/n7;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/n7;->a(Lcom/pspdfkit/internal/n7;Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/internal/n7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p2, "clipboard"

    .line 4
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    const-string p3, "null cannot be cast to non-null type android.content.ClipboardManager"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/content/ClipboardManager;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->b:Landroid/content/ClipboardManager;

    const/high16 p2, -0x10000

    .line 21
    iput p2, p0, Lcom/pspdfkit/internal/n7;->k:I

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 35
    sget p3, Lcom/pspdfkit/R$layout;->pspdf__custom_color_picker:I

    invoke-virtual {p2, p3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    .line 36
    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 38
    sget p2, Lcom/pspdfkit/R$id;->pspdf__slider_container:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__slider_container)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->c:Landroid/view/View;

    .line 39
    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_slider_1:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__custom_color_slider_1)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    .line 40
    new-instance p3, Lcom/pspdfkit/internal/n7$a;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/n7$a;-><init>(Lcom/pspdfkit/internal/n7;)V

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->setListener(Lkotlin/jvm/functions/Function1;)V

    .line 43
    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_slider_2:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__custom_color_slider_2)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    .line 44
    new-instance p3, Lcom/pspdfkit/internal/n7$b;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/n7$b;-><init>(Lcom/pspdfkit/internal/n7;)V

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->setListener(Lkotlin/jvm/functions/Function1;)V

    .line 47
    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_slider_3:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__custom_color_slider_3)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    .line 48
    new-instance p3, Lcom/pspdfkit/internal/n7$c;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/n7$c;-><init>(Lcom/pspdfkit/internal/n7;)V

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->setListener(Lkotlin/jvm/functions/Function1;)V

    .line 52
    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_switcher:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf_\u2026om_color_picker_switcher)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/RadioGroup;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    .line 53
    new-instance p3, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/n7;)V

    invoke-virtual {p2, p3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 67
    sget p2, Lcom/pspdfkit/R$id;->pspdf__hex_container:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__hex_container)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->g:Landroid/view/View;

    .line 68
    sget p2, Lcom/pspdfkit/R$id;->pspdf__hex_entry_container:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__hex_entry_container)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->h:Lcom/google/android/material/textfield/TextInputLayout;

    .line 69
    sget p2, Lcom/pspdfkit/R$id;->pspdf__hex_entry:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__hex_entry)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/pspdfkit/internal/n7;->i:Landroid/widget/EditText;

    .line 70
    new-instance p3, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda1;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/n7;)V

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 74
    sget p2, Lcom/pspdfkit/R$id;->pspdf__paste_hex_button:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__paste_hex_button)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/Button;

    .line 75
    new-instance p3, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda2;

    invoke-direct {p3, p0, p1}, Lcom/pspdfkit/internal/n7$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/n7;Landroid/content/Context;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->a()V

    return-void
.end method

.method private final a()V
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->g:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__color_picker_hue:I

    const/4 v3, 0x0

    .line 51
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(context, R.str\u2026.pspdf__color_picker_hue)"

    .line 52
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x168

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__color_picker_saturation:I

    .line 54
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(context, R.str\u2026_color_picker_saturation)"

    .line 55
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/pspdfkit/R$string;->pspdf__color_picker_lightness:I

    .line 57
    invoke-static {v1, v4, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "getString(context, R.str\u2026__color_picker_lightness)"

    .line 58
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hsl:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 60
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->c()V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/n7;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->b()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/n7;Landroid/content/Context;Landroid/view/View;)V
    .locals 4

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p2, p0, Lcom/pspdfkit/internal/n7;->b:Landroid/content/ClipboardManager;

    invoke-virtual {p2}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 28
    invoke-virtual {p2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 31
    invoke-virtual {p2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object p2

    .line 32
    invoke-virtual {p2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    :try_start_0
    const-string v1, "text"

    .line 35
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x23

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p2, v1, v0, v2, v3}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 38
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 41
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    .line 42
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/n7;->setCurrentColor(I)V

    .line 43
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->c()V

    .line 44
    iget-object p0, p0, Lcom/pspdfkit/internal/n7;->l:Lcom/pspdfkit/internal/n7$d;

    if-eqz p0, :cond_2

    invoke-interface {p0, p2}, Lcom/pspdfkit/internal/n7$d;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 47
    :catch_0
    sget p0, Lcom/pspdfkit/R$string;->pspdf__color_picker_invalid_color_value:I

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_1
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/n7;Landroid/widget/RadioGroup;I)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    sget p1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hsl:I

    if-ne p2, p1, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->a()V

    goto :goto_0

    .line 5
    :cond_0
    sget p1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_rgb:I

    const/4 v0, 0x4

    const/4 v1, 0x0

    if-ne p2, p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->c:Landroid/view/View;

    .line 7
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->g:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lcom/pspdfkit/R$string;->pspdf__color_red:I

    const/4 v1, 0x0

    .line 10
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "getString(context, R.string.pspdf__color_red)"

    .line 11
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0xff

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v2, Lcom/pspdfkit/R$string;->pspdf__color_green:I

    .line 13
    invoke-static {p2, v2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p2

    const-string v2, "getString(context, R.string.pspdf__color_green)"

    .line 14
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v2, Lcom/pspdfkit/R$string;->pspdf__color_blue:I

    .line 16
    invoke-static {p2, v2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "getString(context, R.string.pspdf__color_blue)"

    .line 17
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(Ljava/lang/String;I)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_rgb:I

    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->check(I)V

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->c()V

    goto :goto_0

    .line 20
    :cond_1
    sget p1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hex:I

    if-ne p2, p1, :cond_2

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->c:Landroid/view/View;

    .line 22
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->g:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hex:I

    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->check(I)V

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->c()V

    :cond_2
    :goto_0
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/n7;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->b()V

    const/4 p0, 0x0

    return p0
.end method

.method private final b()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hsl:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    const/4 v3, 0x1

    aput v1, v0, v3

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 7
    invoke-static {v0}, Landroidx/core/graphics/ColorUtils;->HSLToColor([F)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/n7;->k:I

    goto :goto_0

    .line 10
    :cond_0
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_rgb:I

    if-ne v0, v1, :cond_1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->getValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/n7;->k:I

    goto :goto_0

    .line 14
    :cond_1
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hex:I

    if-ne v0, v1, :cond_2

    .line 16
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/n7;->k:I

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->h:Lcom/google/android/material/textfield/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 19
    :catch_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->h:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__color_picker_invalid_color_value:I

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 24
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->l:Lcom/pspdfkit/internal/n7$d;

    if-eqz v0, :cond_3

    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/n7$d;->a(I)V

    :cond_3
    return-void
.end method

.method private final c()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 2
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hsl:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-static {v1, v0}, Landroidx/core/graphics/ColorUtils;->colorToHSL(I[F)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    aget v3, v0, v2

    float-to-int v3, v3

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    const/4 v3, 0x1

    aget v3, v0, v3

    const/16 v4, 0x64

    int-to-float v4, v4

    mul-float v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    const/4 v3, 0x2

    aget v0, v0, v3

    mul-float v0, v0, v4

    float-to-int v0, v0

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    goto :goto_0

    .line 10
    :cond_0
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_rgb:I

    if-ne v0, v1, :cond_1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->d:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->e:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->f:Lcom/pspdfkit/internal/ui/views/ValueSliderView;

    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/views/ValueSliderView;->a(IZ)V

    goto :goto_0

    .line 16
    :cond_1
    sget v1, Lcom/pspdfkit/R$id;->pspdf__custom_color_picker_hex:I

    if-ne v0, v1, :cond_2

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->i:Landroid/widget/EditText;

    iget v1, p0, Lcom/pspdfkit/internal/n7;->k:I

    invoke-static {v1, v2, v2}, Lcom/pspdfkit/internal/ft;->a(IZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public final getCurrentColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/n7;->k:I

    return v0
.end method

.method public final getCurrentMode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    return v0
.end method

.method public final getListener()Lcom/pspdfkit/internal/n7$d;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->l:Lcom/pspdfkit/internal/n7$d;

    return-object v0
.end method

.method public final setCurrentColor(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/n7;->k:I

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    :goto_0
    iput p1, p0, Lcom/pspdfkit/internal/n7;->k:I

    if-eqz v0, :cond_1

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/n7;->c()V

    :cond_1
    return-void
.end method

.method public final setCurrentMode(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n7;->j:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->check(I)V

    return-void
.end method

.method public final setListener(Lcom/pspdfkit/internal/n7$d;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/n7;->l:Lcom/pspdfkit/internal/n7$d;

    return-void
.end method

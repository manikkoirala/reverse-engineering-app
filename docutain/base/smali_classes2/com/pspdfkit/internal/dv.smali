.class public final Lcom/pspdfkit/internal/dv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/dv$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/UriAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private final b:Lcom/pspdfkit/configuration/PdfConfiguration;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    const-string v0, "documentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dv;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iput-object p2, p0, Lcom/pspdfkit/internal/dv;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method

.method private final a(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V
    .locals 8

    .line 1
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/pspdfkit/media/MediaUri;->parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;

    move-result-object p2

    const-string v0, "parse(action.uri!!)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p2}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/dv$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const-string v1, "Activity PdfMediaDialog doesn\'t exist (make sure it\'s declared in manifest)."

    const-string v2, "PSPDFKit.VideoPlaybackEnabled"

    const/4 v3, 0x1

    const-string v4, "PSPDFKit.MediaURI"

    const-string v5, "PSPDFKit.ActionResolver"

    const/4 v6, 0x0

    if-eq v0, v3, :cond_2

    const/4 v7, 0x2

    if-eq v0, v7, :cond_1

    const/4 v7, 0x3

    if-eq v0, v7, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto/16 :goto_1

    .line 30
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p2}, Lcom/pspdfkit/media/MediaUri;->getParsedUri()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 32
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    .line 34
    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    sget v2, Lcom/pspdfkit/R$string;->pspdf__file_not_found_title:I

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 36
    sget v2, Lcom/pspdfkit/R$string;->pspdf__file_not_found_message:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x0

    invoke-static {p1, v2, v4, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 37
    sget v2, Lcom/pspdfkit/R$string;->pspdf__ok:I

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1, v4}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->show()Landroidx/appcompat/app/AlertDialog;

    .line 39
    invoke-virtual {p2}, Lcom/pspdfkit/media/MediaUri;->getUri()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not find an activity to open "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, v6, [Ljava/lang/Object;

    invoke-static {v5, v0, p1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 40
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/pspdfkit/ui/PdfMediaDialog;

    invoke-direct {v0, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 42
    iget-object p2, p0, Lcom/pspdfkit/internal/dv;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result p2

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    :try_start_1
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    new-array p2, v6, [Ljava/lang/Object;

    .line 46
    invoke-static {v5, p1, v1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 47
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/dv;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    :try_start_2
    const-string v0, "com.google.android.youtube.player.YouTubeBaseActivity"

    .line 52
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 54
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/pspdfkit/ui/PdfYouTubeActivity;

    invoke-direct {v0, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    new-array v3, v6, [Ljava/lang/Object;

    const-string v7, "YouTube API dependency not found, opening video with PdfMediaDialog."

    .line 61
    invoke-static {v5, v0, v7, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v0

    new-array v3, v6, [Ljava/lang/Object;

    const-string v7, "PdfYouTubeActivity not found - make sure it\'s declared in manifest, opening video with PdfMediaDialog."

    .line 62
    invoke-static {v5, v0, v7, v3}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/pspdfkit/ui/PdfMediaDialog;

    invoke-direct {v0, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 65
    iget-object p2, p0, Lcom/pspdfkit/internal/dv;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result p2

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    :try_start_3
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    :catch_3
    move-exception p1

    new-array p2, v6, [Ljava/lang/Object;

    .line 69
    invoke-static {v5, p1, v1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/UriAction;

    const-string p2, "action"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 37
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/dv;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    if-nez p2, :cond_1

    :goto_0
    const/4 p1, 0x0

    goto :goto_1

    .line 38
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/internal/dv;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/actions/UriAction;)V

    const/4 p1, 0x1

    :goto_1
    return p1
.end method

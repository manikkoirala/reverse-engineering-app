.class public final Lcom/pspdfkit/internal/wc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/wc$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/HashSet;

.field private final b:Lcom/pspdfkit/internal/o7;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashMap;

.field private e:Lcom/pspdfkit/internal/xc;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/wc;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/wc;->a:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/wc;->c:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/wc;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/wc;->d:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/wc;)Lcom/pspdfkit/internal/xc;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/wc;->e:Lcom/pspdfkit/internal/xc;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fpute(Lcom/pspdfkit/internal/wc;Lcom/pspdfkit/internal/xc;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/wc;->e:Lcom/pspdfkit/internal/xc;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/wc;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/wc;->a:Ljava/util/HashSet;

    .line 23
    new-instance p2, Lcom/pspdfkit/internal/o7;

    new-instance v0, Lcom/pspdfkit/internal/wc$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/wc$a;-><init>(Lcom/pspdfkit/internal/wc;Lcom/pspdfkit/internal/wc$a-IA;)V

    const/4 v1, 0x0

    invoke-direct {p2, p1, v0, v1}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/wc;->b:Lcom/pspdfkit/internal/o7;

    const/4 p1, 0x1

    .line 24
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/o7;->a(Z)V

    .line 25
    invoke-virtual {p2}, Lcom/pspdfkit/internal/o7;->a()V

    .line 27
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wc;->c:Ljava/util/HashMap;

    .line 28
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wc;->d:Ljava/util/HashMap;

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/vc;->values()[Lcom/pspdfkit/internal/vc;

    move-result-object p1

    array-length p2, p1

    :goto_0
    if-ge v1, p2, :cond_0

    aget-object v0, p1, v1

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/internal/wc;->d:Ljava/util/HashMap;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final varargs a(Lcom/pspdfkit/internal/vc;[Lcom/pspdfkit/internal/xc;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/yc$a;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/yc$a;-><init>([Lcom/pspdfkit/internal/xc;)V

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/wc;->c:Ljava/util/HashMap;

    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/wc;->a:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/wc;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/yc;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/wc;->a:Ljava/util/HashSet;

    invoke-interface {p2}, Lcom/pspdfkit/internal/yc;->a()Ljava/util/List;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/wc;->b:Lcom/pspdfkit/internal/o7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

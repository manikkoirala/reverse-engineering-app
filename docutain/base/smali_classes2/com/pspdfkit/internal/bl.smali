.class final Lcom/pspdfkit/internal/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__annotationEditingToolbarIconsStyle:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 4
    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 10
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__undoIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_undo:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 12
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__redoIcon:I

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_redo:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 14
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationEditingToolbarIcons_pspdf__deleteIcon:I

    sget v4, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 16
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v0, -0x1

    .line 18
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/bl;->a:Landroid/graphics/drawable/Drawable;

    .line 19
    invoke-static {p1, v2, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/bl;->b:Landroid/graphics/drawable/Drawable;

    .line 20
    invoke-static {p1, v3, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/bl;->c:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bl;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method final b()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bl;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method final c()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bl;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/pc$b;
.super Lcom/pspdfkit/internal/o7$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/pc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/pc;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/pc;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    invoke-direct {p0}, Lcom/pspdfkit/internal/o7$c;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/internal/pc$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pc$b;-><init>(Lcom/pspdfkit/internal/pc;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/pc;->-$$Nest$fputj(Lcom/pspdfkit/internal/pc;Landroid/graphics/Point;)V

    return-void
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v1, v2, p1}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fputj(Lcom/pspdfkit/internal/pc;Landroid/graphics/Point;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    invoke-static {v0}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetj(Lcom/pspdfkit/internal/pc;)Landroid/graphics/Point;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-static {v0}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetb(Lcom/pspdfkit/internal/pc;)Landroid/content/Context;

    move-result-object v0

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v5, v5

    .line 4
    invoke-static {v0, v3, v1, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    if-nez v0, :cond_4

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    invoke-static {v0}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetd(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/k1;

    .line 9
    instance-of v4, v1, Lcom/pspdfkit/internal/pc;

    if-eqz v4, :cond_0

    .line 10
    move-object v4, v1

    check-cast v4, Lcom/pspdfkit/internal/pc;

    .line 12
    iget-object v5, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    if-ne v1, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 13
    :goto_1
    invoke-static {v4}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgeti(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 15
    :cond_2
    invoke-static {v4}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetf(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/am;->a(ZZ)Z

    const/4 v1, 0x0

    .line 16
    invoke-static {v4, v1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fputi(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    goto :goto_0

    .line 17
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/pc$b;->a:Lcom/pspdfkit/internal/pc;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/pc;->-$$Nest$ma(Lcom/pspdfkit/internal/pc;FF)V

    return v3

    :cond_4
    return v2
.end method

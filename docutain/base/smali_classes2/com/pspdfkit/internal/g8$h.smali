.class final Lcom/pspdfkit/internal/g8$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/g8;->a(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/g8;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/g8;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/g8$h;->a:Lcom/pspdfkit/internal/g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Landroid/net/Uri;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/pspdfkit/internal/g8$h;->a:Lcom/pspdfkit/internal/g8;

    invoke-static {v0}, Lcom/pspdfkit/internal/g8;->b(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "configuration"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v1, v2

    :cond_0
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->newInstance(Landroid/net/Uri;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    const-string v1, "newInstance(it, configuration.configuration)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/ui/PdfFragment;)V

    .line 320
    iget-object p1, p0, Lcom/pspdfkit/internal/g8$h;->a:Lcom/pspdfkit/internal/g8;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 321
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 322
    sget v0, Lcom/pspdfkit/R$id;->pspdf__comparison_fragment_frame:I

    iget-object v1, p0, Lcom/pspdfkit/internal/g8$h;->a:Lcom/pspdfkit/internal/g8;

    invoke-static {v1}, Lcom/pspdfkit/internal/g8;->c(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "pdfFragment"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    const-string v1, "com.pspdfkit.ui.PdfFragment"

    invoke-virtual {p1, v0, v2, v1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 323
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void
.end method

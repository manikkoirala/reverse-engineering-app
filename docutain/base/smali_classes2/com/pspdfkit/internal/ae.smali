.class public final Lcom/pspdfkit/internal/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/ImageDocument;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ae$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/pspdfkit/document/DocumentSource;

.field private final c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

.field private d:Lcom/pspdfkit/internal/ae$a;


# direct methods
.method public static synthetic $r8$lambda$pPQbfXVm1n18QLqDDYaBK6GCcUQ(Lcom/pspdfkit/internal/ae;Lcom/pspdfkit/document/DocumentSaveOptions;Z)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ae;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private constructor <init>(Lcom/pspdfkit/document/DocumentSource;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->IMAGE_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ae;->a:Z

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/internal/ae;->a()Lcom/pspdfkit/internal/jni/NativeImageDocument;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ae;->c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 14
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v4, "Image document open took "

    invoke-direct {p1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v2, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ImageDocument"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 15
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license doesn\'t allow opening image documents."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/internal/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ae;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ae;-><init>(Lcom/pspdfkit/document/DocumentSource;)V

    return-object v0
.end method

.method private a()Lcom/pspdfkit/internal/jni/NativeImageDocument;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->i()Lcom/pspdfkit/internal/oq;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/oq;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/nq;

    move-result-object v0

    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->toDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v1

    .line 5
    invoke-static {v1}, Lcom/pspdfkit/internal/jni/NativeImageDocument;->createImageDocument(Lcom/pspdfkit/internal/jni/NativeDataDescriptor;)Lcom/pspdfkit/internal/jni/NativeImageDocumentOpenResult;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeImageDocumentOpenResult;->getResult()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v2

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v3

    if-nez v3, :cond_1

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeImageDocumentOpenResult;->getImageDocument()Lcom/pspdfkit/internal/jni/NativeImageDocument;

    move-result-object v2

    const-string v3, "Could not load image document"

    const-string v4, "message"

    .line 11
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeImageDocumentOpenResult;->getImageDocument()Lcom/pspdfkit/internal/jni/NativeImageDocument;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return-object v1

    .line 14
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 15
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 20
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "A license for image documents and annotation editing is needed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 21
    new-instance v1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v2, "A license for image documents and annotation editing is needed. Your PSPDFKit license can only be used with Pdf documents."

    invoke-direct {v1, v2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 25
    :cond_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error while loading ImageDocument"

    invoke-direct {v2, v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 26
    :goto_0
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 27
    throw v1
.end method

.method private synthetic a(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/ae;->saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->d:Lcom/pspdfkit/internal/ae$a;

    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeImageDocument;->getDocument()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeImageDocument;->open()Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "PSPDFKit.ImageDocument"

    const-string v2, "Image document couldn\'t be opened: %s"

    invoke-static {v0, v2, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 11
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ae$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/ae;->c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

    .line 13
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeImageDocument;->getDocument()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v3

    iget-boolean v4, p0, Lcom/pspdfkit/internal/ae;->a:Z

    new-instance v5, Lcom/pspdfkit/internal/z7;

    invoke-direct {v5}, Lcom/pspdfkit/internal/z7;-><init>()V

    iget-object v6, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/ae$a;-><init>(Lcom/pspdfkit/internal/ae;Lcom/pspdfkit/internal/jni/NativeDocument;ZLcom/pspdfkit/internal/z7;Lcom/pspdfkit/document/DocumentSource;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ae;->d:Lcom/pspdfkit/internal/ae$a;

    .line 18
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->d:Lcom/pspdfkit/internal/ae$a;

    return-object v0
.end method

.method public final getImageDocumentSource()Lcom/pspdfkit/document/DocumentSource;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    return-object v0
.end method

.method public final isValidForEditing()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ae;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    instance-of v0, v0, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final saveIfModified()Z
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ae;->saveIfModified(Z)Z

    move-result v0

    return v0
.end method

.method public final saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Z
    .locals 7

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ae;->a:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v0, "saveOptions"

    const-string v2, "argumentName"

    .line 7
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 58
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 61
    check-cast v0, Lcom/pspdfkit/internal/ae$a;

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_2

    return v1

    .line 62
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->wasModified()Z

    move-result v3

    const-string v4, "PSPDFKit.ImageDocument"

    if-nez v3, :cond_3

    if-eqz p2, :cond_3

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Image document not modified, not saving."

    .line 63
    invoke-static {v4, p2, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 69
    :cond_3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->i()Lcom/pspdfkit/internal/oq;

    move-result-object v3

    iget-object v5, p0, Lcom/pspdfkit/internal/ae;->b:Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v5}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/pspdfkit/internal/oq;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/nq;

    move-result-object v3

    .line 70
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 71
    :try_start_0
    iget-object v5, p0, Lcom/pspdfkit/internal/ae;->c:Lcom/pspdfkit/internal/jni/NativeImageDocument;

    .line 72
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    move-result-object p1

    .line 73
    invoke-virtual {v5, p1, p2}, Lcom/pspdfkit/internal/jni/NativeImageDocument;->saveIfModified(Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;Z)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x1

    if-nez p2, :cond_5

    .line 76
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 77
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->g()Lcom/pspdfkit/internal/nh;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/zf$f;

    .line 78
    invoke-interface {p2, v0}, Lcom/pspdfkit/internal/zf$f;->onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V

    goto :goto_1

    :cond_4
    return v5

    .line 79
    :cond_5
    :try_start_1
    new-instance p2, Ljava/io/IOException;

    const-string v6, "Image document could not be saved: %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v5, v1

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_2
    new-array p2, v1, [Ljava/lang/Object;

    .line 82
    invoke-static {v4, p1, v2, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->g()Lcom/pspdfkit/internal/nh;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/zf$f;

    .line 84
    invoke-interface {v2, v0, p1}, Lcom/pspdfkit/internal/zf$f;->onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 85
    :cond_6
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    return v1

    :goto_3
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 86
    throw p1
.end method

.method public final saveIfModified(Z)Z
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 5
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/ae;->saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Z

    move-result p1

    return p1
.end method

.method public final saveIfModifiedAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 60
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ae;->saveIfModifiedAsync(Z)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final saveIfModifiedAsync(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            "Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "saveOptions"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 55
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ae$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/ae$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ae;Lcom/pspdfkit/document/DocumentSaveOptions;Z)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 56
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 58
    move-object v1, p2

    check-cast v1, Lcom/pspdfkit/internal/ae$a;

    :cond_1
    const/16 p2, 0xa

    .line 59
    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final saveIfModifiedAsync(Z)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 61
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ae;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    .line 63
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/ae;->saveIfModifiedAsync(Lcom/pspdfkit/document/DocumentSaveOptions;Z)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

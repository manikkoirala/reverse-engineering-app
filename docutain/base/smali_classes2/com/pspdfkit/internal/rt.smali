.class public final Lcom/pspdfkit/internal/rt;
.super Lcom/pspdfkit/internal/qt;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/rt$a;,
        Lcom/pspdfkit/internal/rt$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/rt$b;


# instance fields
.field private final a:Ljava/util/UUID;

.field private final b:Lcom/pspdfkit/internal/ut;

.field private final c:Lcom/pspdfkit/internal/cv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/rt$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/rt$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/rt;->Companion:Lcom/pspdfkit/internal/rt$b;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/UUID;Lcom/pspdfkit/internal/ut;Lcom/pspdfkit/internal/cv;)V
    .locals 2
    .param p3    # Lcom/pspdfkit/internal/ut;
        .annotation runtime Lkotlinx/serialization/SerialName;
            value = "textBlock"
        .end annotation
    .end param
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x7

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/rt$a;->a:Lcom/pspdfkit/internal/rt$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/rt$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/qt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/rt;->a:Ljava/util/UUID;

    iput-object p3, p0, Lcom/pspdfkit/internal/rt;->b:Lcom/pspdfkit/internal/ut;

    iput-object p4, p0, Lcom/pspdfkit/internal/rt;->c:Lcom/pspdfkit/internal/cv;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/rt;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/qu;->a:Lcom/pspdfkit/internal/qu;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/rt;->a:Ljava/util/UUID;

    const/4 v2, 0x0

    .line 3
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/ut$a;->a:Lcom/pspdfkit/internal/ut$a;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/rt;->b:Lcom/pspdfkit/internal/ut;

    const/4 v2, 0x1

    .line 5
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/cv$a;->a:Lcom/pspdfkit/internal/cv$a;

    .line 6
    iget-object p0, p0, Lcom/pspdfkit/internal/rt;->c:Lcom/pspdfkit/internal/cv;

    const/4 v1, 0x2

    .line 7
    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/UUID;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/rt;->a:Ljava/util/UUID;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/tt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/rt;->b:Lcom/pspdfkit/internal/ut;

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/internal/bv;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/rt;->c:Lcom/pspdfkit/internal/cv;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/ut;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/rt;->b:Lcom/pspdfkit/internal/ut;

    return-object v0
.end method

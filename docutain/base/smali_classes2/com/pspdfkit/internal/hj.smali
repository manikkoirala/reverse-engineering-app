.class public abstract Lcom/pspdfkit/internal/hj;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/hj$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/pspdfkit/internal/hj$a;",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/hj$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    return-void
.end method


# virtual methods
.method public final a(Z)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 51
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    const/4 v1, 0x0

    .line 52
    iput-boolean v1, p0, Lcom/pspdfkit/internal/hj;->a:Z

    .line 53
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    .line 54
    :goto_0
    iget-object v4, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 55
    iget-object v4, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/hj$a;

    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 56
    iget-object v4, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/hj$a;

    .line 58
    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    .line 61
    :cond_0
    iget-object v5, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 63
    invoke-interface {v4, v1}, Lcom/pspdfkit/internal/hj$a;->a(I)V

    add-int/lit8 v4, v3, 0x1

    if-eqz v5, :cond_1

    .line 64
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 65
    iget-object v6, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v6, v4, v5}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 66
    iget-boolean v6, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v6, :cond_1

    .line 67
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {p0, v4, v5}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    .line 68
    :cond_1
    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    .line 72
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_2
    if-ltz p1, :cond_4

    .line 73
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/hj;->a(I)V

    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    .line 76
    :cond_4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    return-object v2
.end method

.method public final a()V
    .locals 2

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 10
    iget-boolean v1, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 11
    invoke-virtual {p0, v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeRemoved(II)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 7

    .line 12
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/hj$a;

    .line 18
    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_3

    .line 23
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    .line 28
    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    :goto_0
    if-ltz v4, :cond_2

    .line 29
    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/hj$a;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 32
    :cond_2
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 33
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/hj$a;

    .line 34
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    .line 37
    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v5

    if-nez v5, :cond_3

    .line 38
    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    :goto_2
    if-ltz v5, :cond_3

    .line 39
    invoke-interface {v4}, Lcom/pspdfkit/internal/hj$a;->getChildren()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/internal/hj$a;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 42
    :cond_3
    iget-object v5, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-interface {v0, v3}, Lcom/pspdfkit/internal/hj$a;->a(I)V

    add-int/lit8 p1, p1, 0x1

    .line 48
    invoke-virtual {p0, p1, v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeRemoved(II)V

    :cond_5
    :goto_3
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Z)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 77
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    const/4 v1, 0x0

    .line 78
    iput-boolean v1, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz p2, :cond_3

    const/4 p2, 0x0

    .line 79
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p2, v2, :cond_3

    .line 80
    iget-object v2, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/hj$a;

    invoke-interface {v2}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    iget-object v2, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/hj$a;

    .line 83
    invoke-interface {v2}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    .line 86
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 88
    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/hj$a;->a(I)V

    add-int/lit8 v2, p2, 0x1

    if-eqz v3, :cond_2

    .line 89
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 90
    iget-object v4, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, v3}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 91
    iget-boolean v4, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v4, :cond_2

    .line 92
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    :cond_2
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 93
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_2
    if-ltz p2, :cond_4

    .line 94
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/hj;->a(I)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_2

    .line 96
    :cond_4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v0, :cond_5

    .line 98
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_5
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/pspdfkit/internal/hj$a;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz p1, :cond_0

    .line 2
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 4
    iget-boolean v1, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Collection;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/pspdfkit/internal/hj$a;",
            ">;",
            "Ljava/util/HashMap<",
            "Lcom/pspdfkit/internal/hj$a;",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/hj$a;",
            ">;>;)V"
        }
    .end annotation

    .line 49
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/Collection;)V

    .line 50
    iget-object p1, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public final b(I)Z
    .locals 1

    if-ltz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final c(I)Lcom/pspdfkit/internal/hj$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/hj$a;

    return-object p1
.end method

.method public final d(I)V
    .locals 3

    if-ltz p1, :cond_3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->c(I)Lcom/pspdfkit/internal/hj$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/hj$a;

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/hj$a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 10
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/hj;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    const/4 v2, 0x0

    .line 12
    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/hj$a;->a(I)V

    add-int/lit8 p1, p1, 0x1

    if-eqz v1, :cond_3

    .line 13
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 15
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hj;->a:Z

    if-eqz v0, :cond_3

    .line 16
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/hj;->a(I)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/hj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.class public final Lcom/pspdfkit/internal/pn;
.super Lcom/pspdfkit/internal/d4;
.source "SourceFile"


# instance fields
.field private final w:Landroid/graphics/Path;

.field private final x:Landroid/graphics/Path;

.field private final y:Landroid/graphics/Matrix;

.field private final z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 6

    .line 1
    sget-object v5, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->SOLID:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/pn;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.method public constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/d4;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 3
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    .line 7
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/pn;->x:Landroid/graphics/Path;

    .line 10
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/pn;->y:Landroid/graphics/Matrix;

    .line 13
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/pn;->z:Landroid/graphics/Rect;

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Canvas;F)V
    .locals 7

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 75
    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 76
    iget v4, v3, Landroid/graphics/PointF;->y:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    cmpg-float v6, v4, v5

    if-ltz v6, :cond_1

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->x:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    :cond_1
    move-object v0, v3

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v2

    const/high16 v3, 0x41600000    # 14.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/16 v5, 0x9

    new-array v5, v5, [F

    .line 79
    invoke-virtual {v3, v5}, Landroid/graphics/Matrix;->getValues([F)V

    aget v1, v5, v1

    div-float/2addr v1, v4

    mul-float v1, v1, v2

    .line 81
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 82
    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 84
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p3, v1

    if-eqz v1, :cond_3

    .line 85
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 86
    invoke-virtual {v1, p3, p3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 87
    invoke-virtual {p2, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 90
    :cond_3
    iget-object p3, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {p2, p1, v2, v0, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 91
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/y4;->r:F

    iget-object v3, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/d4;->x()Z

    move-result v4

    .line 5
    invoke-static {v0, v2, v3, v4}, Lcom/pspdfkit/internal/k5;->a(Ljava/util/ArrayList;FLandroid/graphics/Path;Z)Landroid/graphics/Path;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    goto :goto_1

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 13
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/d4;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 20
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 21
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->z:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 26
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->z:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v6, v4

    iget v4, v2, Landroid/graphics/Rect;->top:I

    int-to-float v7, v4

    iget v4, v2, Landroid/graphics/Rect;->right:I

    int-to-float v8, v4

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v2

    const/4 v10, 0x0

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    goto :goto_2

    .line 28
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v7, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v8, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;)I

    .line 29
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-nez v2, :cond_6

    goto :goto_3

    .line 34
    :cond_6
    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v1, :cond_8

    invoke-virtual {p0}, Lcom/pspdfkit/internal/d4;->x()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->c()Lcom/pspdfkit/internal/br$a;

    move-result-object v2

    sget-object v4, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    if-ne v2, v4, :cond_8

    .line 35
    :cond_7
    iget-object v2, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    invoke-direct {p0, v2, p1, p4}, Lcom/pspdfkit/internal/pn;->a(Ljava/lang/String;Landroid/graphics/Canvas;F)V

    goto :goto_3

    :cond_8
    const-string v2, ""

    .line 37
    invoke-direct {p0, v2, p1, p4}, Lcom/pspdfkit/internal/pn;->a(Ljava/lang/String;Landroid/graphics/Canvas;F)V

    :cond_9
    :goto_3
    if-eqz p3, :cond_b

    .line 38
    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v1, :cond_b

    cmpl-float v2, p4, v3

    if-eqz v2, :cond_a

    .line 40
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->y:Landroid/graphics/Matrix;

    invoke-virtual {v2, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 41
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/pspdfkit/internal/pn;->x:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/pspdfkit/internal/pn;->y:Landroid/graphics/Matrix;

    .line 42
    invoke-virtual {v4, v2}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 43
    invoke-virtual {v4, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 44
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->x:Landroid/graphics/Path;

    invoke-virtual {p1, v2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_4

    .line 46
    :cond_a
    iget-object v2, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    invoke-virtual {p1, v2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 47
    :cond_b
    :goto_4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y4;->t()Z

    move-result v2

    if-nez v2, :cond_c

    goto :goto_5

    :cond_c
    const/4 v2, 0x0

    cmpl-float v4, p4, v3

    if-eqz v4, :cond_e

    .line 51
    iget-object v4, p0, Lcom/pspdfkit/internal/pn;->y:Landroid/graphics/Matrix;

    invoke-virtual {v4, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 52
    iget-object p4, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/pspdfkit/internal/pn;->x:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/pspdfkit/internal/pn;->y:Landroid/graphics/Matrix;

    .line 53
    invoke-virtual {v4, p4}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 54
    invoke-virtual {v4, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 55
    iget-object p4, p0, Lcom/pspdfkit/internal/pn;->x:Landroid/graphics/Path;

    if-eqz p3, :cond_d

    .line 56
    iget-object p3, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-le p3, v1, :cond_d

    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p3

    cmpg-float p3, p3, v3

    if-gez p3, :cond_d

    .line 59
    new-instance p3, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p3, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 60
    invoke-virtual {p1, p4, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 61
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 63
    :cond_d
    invoke-virtual {p1, p4, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_5

    .line 64
    :cond_e
    iget-object p4, p0, Lcom/pspdfkit/internal/pn;->w:Landroid/graphics/Path;

    if-eqz p3, :cond_f

    .line 65
    iget-object p3, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-le p3, v1, :cond_f

    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result p3

    cmpg-float p3, p3, v3

    if-gez p3, :cond_f

    .line 68
    new-instance p3, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p3, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 69
    invoke-virtual {p1, p4, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 70
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 72
    :cond_f
    invoke-virtual {p1, p4, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 73
    :goto_5
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

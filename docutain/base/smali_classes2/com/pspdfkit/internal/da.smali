.class public final Lcom/pspdfkit/internal/da;
.super Lcom/pspdfkit/internal/im$h;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;


# instance fields
.field private final d:Landroid/graphics/Matrix;

.field private final e:Ljava/util/HashMap;

.field private f:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$-Jq8iFrYA08l-FPp6clE1WpcyO8(Lcom/pspdfkit/internal/da;Lcom/pspdfkit/ui/drawable/PdfDrawable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/da;->a(Lcom/pspdfkit/ui/drawable/PdfDrawable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JZbS22x4Ax0p52Rpq5S9eGNaJFo(Lcom/pspdfkit/internal/da;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/da;->a(Landroid/util/Pair;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_IkpqChTMeGY5C6d1xwInoOhCCQ(Lcom/pspdfkit/internal/da;Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/da;->a(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$aTlgzT2W6KHvpGac_48y7z9f0YE(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Ljava/util/List;)Landroid/util/Pair;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/da;->a(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Ljava/util/List;)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/da;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/im;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/im$h;-><init>(Lcom/pspdfkit/internal/im;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/da;->d:Landroid/graphics/Matrix;

    .line 5
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    .line 16
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Ljava/util/List;)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 37
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p0, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic a(Lcom/pspdfkit/internal/da;)Lcom/pspdfkit/internal/im;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    return-object p0
.end method

.method private synthetic a(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/dm$e;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 31
    invoke-virtual {p1, p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->registerDrawableProviderObserver(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;)V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    .line 33
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p2

    .line 34
    invoke-virtual {p1, v0, v1, p2}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a()V
    .locals 2

    .line 43
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Page drawables touched from non-main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->f:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 46
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/pspdfkit/internal/da;->f:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 50
    invoke-virtual {v1, p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->unregisterDrawableProviderObserver(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider$DrawableProviderObserver;)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method private synthetic a(Landroid/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 38
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 39
    iget-object v2, p0, Lcom/pspdfkit/internal/da;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/ui/drawable/PdfDrawable;->updatePdfToViewTransformation(Landroid/graphics/Matrix;)V

    .line 40
    iget-object v2, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/ui/drawable/PdfDrawable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->d:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/drawable/PdfDrawable;->updatePdfToViewTransformation(Landroid/graphics/Matrix;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const-string v2, "Page drawables touched from non-main thread."

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/da;->a()V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/da;->f:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 7
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x0

    .line 8
    iput-object v1, p0, Lcom/pspdfkit/internal/da;->f:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 12
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 14
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 15
    new-instance v3, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, v2, v0}, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/da;Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;Lcom/pspdfkit/internal/dm$e;)V

    .line 16
    invoke-static {v3}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda2;

    invoke-direct {v4, v2}, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    .line 23
    invoke-virtual {v3, v4}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v2

    .line 24
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 27
    :cond_1
    invoke-static {v1}, Lio/reactivex/rxjava3/core/Observable;->concat(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 28
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 29
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/da;)V

    .line 30
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/da;->f:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 3

    .line 55
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Page drawables touched from non-main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 58
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 59
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .line 60
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Page drawables touched from non-main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 63
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    if-ne v2, p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public final b()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Page drawables touched from non-main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    iget-object v1, p0, Lcom/pspdfkit/internal/da;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/im;->a(Landroid/graphics/Matrix;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 5
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 6
    iget-object v3, p0, Lcom/pspdfkit/internal/da;->d:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/drawable/PdfDrawable;->updatePdfToViewTransformation(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final onDrawablesChanged(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->b:Lcom/pspdfkit/internal/im;

    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPageAsync(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/da$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/da;)V

    .line 4
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doOnNext(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/da$a;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/da$a;-><init>(Lcom/pspdfkit/internal/da;Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    .line 10
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/core/SingleObserver;)V

    return-void
.end method

.method public final onDrawablesChanged(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;I)V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/im$h;->c:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    if-eq p2, v0, :cond_0

    return-void

    .line 12
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/da;->onDrawablesChanged(Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;)V

    return-void
.end method

.method public final recycle()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/im$h;->recycle()V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/da;->a()V

    return-void
.end method

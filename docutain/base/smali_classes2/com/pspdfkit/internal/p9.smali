.class public final Lcom/pspdfkit/internal/p9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zf$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/p9$a;
    }
.end annotation


# instance fields
.field protected final b:Lcom/pspdfkit/internal/p9$a;

.field private final c:Lcom/pspdfkit/internal/zf;

.field protected volatile d:Z

.field private e:Z


# direct methods
.method public static synthetic $r8$lambda$B0xaKInt595_fMFvShQmOOp6AsU(Lcom/pspdfkit/internal/p9;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/p9;->a(Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public static synthetic $r8$lambda$DMNreoOnhfnPRVtO65CAIsvMLjk(Lcom/pspdfkit/internal/p9;)Lcom/pspdfkit/document/DocumentSaveOptions;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/p9;->c()Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$GyREMqEc20dXD1ony5oc7FTEZUk(Lcom/pspdfkit/internal/p9;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/p9;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VdY85xQO0_5poEFZ5T9lwkX0InU(Lcom/pspdfkit/internal/p9;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/p9;->d()V

    return-void
.end method

.method public static synthetic $r8$lambda$__tv5iU2F8OeJSfWgU2E9Z4hUfQ(Lcom/pspdfkit/internal/p9;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/p9;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$eyIEXLQVesNHK1wrql9tl_23NgA(Lcom/pspdfkit/internal/p9;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/p9;->a(Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$vjmULLs7Cjo-cGcF1DT_COwFhCk(Lcom/pspdfkit/internal/p9;Ljava/lang/Throwable;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p9;->a(Ljava/lang/Throwable;Lcom/pspdfkit/internal/zf;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/p9$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    .line 10
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/internal/zf$f;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/zf;)V
    .locals 4

    .line 18
    monitor-enter p0

    .line 19
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "PSPDFKit.Document"

    const-string v1, "Document has been saved."

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    .line 20
    invoke-static {v0, v1, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iput-boolean v2, p0, Lcom/pspdfkit/internal/p9;->d:Z

    .line 23
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/p9$a;->onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V

    return-void

    :catchall_0
    move-exception p1

    .line 25
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private synthetic a(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    .line 8
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.Document"

    const-string v1, "Document has been saved."

    .line 9
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 14
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/p9$a;->onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V

    :goto_0
    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 15
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Document"

    const-string v2, "Document save has failed."

    .line 17
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 26
    monitor-enter p0

    .line 27
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    const-string v1, "PSPDFKit.Document"

    const-string v2, "Document save has failed."

    new-array v0, v0, [Ljava/lang/Object;

    .line 29
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    invoke-interface {v0, p2, p1}, Lcom/pspdfkit/internal/p9$a;->onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void

    :catchall_0
    move-exception p1

    .line 33
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private synthetic a(Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    invoke-interface {v0, v1, p1}, Lcom/pspdfkit/internal/p9$a;->onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->b:Lcom/pspdfkit/internal/p9$a;

    .line 4
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "PSPDFKit.Document"

    const-string v2, "Document save has been cancelled by %s"

    .line 5
    invoke-static {v0, v2, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_0
    return v0
.end method

.method private c()Lcom/pspdfkit/document/DocumentSaveOptions;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->wasModified()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    .line 2
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private synthetic d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->e:Z

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/internal/zf$f;)V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->e:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/internal/zf$f;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    .line 1
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p9;->d:Z

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/p9;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    const/16 v2, 0xf

    .line 8
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/p9;)V

    .line 10
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    .line 20
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda3;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Maybe;->flatMapSingle(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 21
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 22
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/p9;)V

    .line 23
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/p9;)V

    .line 29
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/p9;)V

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    if-eq v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/p9;Ljava/lang/Throwable;Lcom/pspdfkit/internal/zf;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p9;->c:Lcom/pspdfkit/internal/zf;

    if-eq v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/p9$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/p9;Lcom/pspdfkit/internal/zf;)V

    check-cast v0, Lcom/pspdfkit/internal/u;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onPageBindingChanged()V
    .locals 0

    return-void
.end method

.method public final onPageRotationOffsetChanged()V
    .locals 0

    return-void
.end method

.class public final Lcom/pspdfkit/internal/g8;
.super Landroidx/fragment/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/g8$a;,
        Lcom/pspdfkit/internal/g8$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0004B\u0007\u00a2\u0006\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/pspdfkit/internal/g8;",
        "Landroidx/fragment/app/DialogFragment;",
        "<init>",
        "()V",
        "a",
        "pspdfkit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x8,
        0x0
    }
.end annotation


# static fields
.field public static final synthetic p:I


# instance fields
.field private b:Ljava/util/ArrayList;

.field private c:Lcom/pspdfkit/ui/PdfFragment;

.field private d:I

.field private e:I

.field private f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

.field private g:Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;

.field private h:Lcom/pspdfkit/document/processor/ComparisonDialogListener;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/document/processor/ComparisonDocument;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private l:Landroidx/appcompat/widget/Toolbar;

.field private m:Lio/reactivex/rxjava3/disposables/Disposable;

.field private n:Landroid/widget/ProgressBar;

.field private o:Landroid/widget/ImageView;


# direct methods
.method public static synthetic $r8$lambda$0Uuc6i8w_eFSO9yN78b75kkB-AM(Landroidx/cardview/widget/CardView;Lcom/pspdfkit/preferences/PSPDFKitPreferences;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/g8;->a(Landroidx/cardview/widget/CardView;Lcom/pspdfkit/preferences/PSPDFKitPreferences;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$2OIj3AseSJU8PzNmcaSH3HbaI8U(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;I)Landroid/net/Uri;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;I)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$6pXXQzlNpRu_uV_S5OCMsbszZGw(Lcom/pspdfkit/internal/g8;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/g8;->g(Lcom/pspdfkit/internal/g8;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Kmbcj5c8_yWiRsrkhjOXSMfDqEI(Lcom/pspdfkit/internal/g8;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/g8;->f(Lcom/pspdfkit/internal/g8;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iNJxroQiPFVSEhukZ6K64tXOWdk(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Landroid/graphics/Matrix;)Lkotlin/Triple;
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Landroid/graphics/Matrix;)Lkotlin/Triple;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$mBApQw_Do-VM9TsiDNloyZr9X6I(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/g8;->b(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$peSR22E6z7LxjHPQL_iLj9fIMkc(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/g8;->a(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/DialogFragment;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/ArrayList;

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->arrayListOf([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/g8;)I
    .locals 0

    .line 2
    iget p0, p0, Lcom/pspdfkit/internal/g8;->e:I

    return p0
.end method

.method private static final a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;I)Landroid/net/Uri;
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$comparisonDocument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "requireContext()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "document_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 59
    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/f8;->a(Landroid/content/Context;Lcom/pspdfkit/document/processor/ComparisonDocument;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Landroid/graphics/Matrix;)Lkotlin/Triple;
    .locals 11

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$oldComparisonDocument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$newComparisonDocument"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$transformationMatrix"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "temp_old"

    .line 80
    invoke-static {v0, p1, v2}, Lcom/pspdfkit/internal/f8;->a(Landroid/content/Context;Lcom/pspdfkit/document/processor/ComparisonDocument;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "temp_new"

    .line 89
    invoke-static {v2, p2, v3}, Lcom/pspdfkit/internal/f8;->a(Landroid/content/Context;Lcom/pspdfkit/document/processor/ComparisonDocument;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 97
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getPageIndex()I

    move-result v5

    .line 101
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getPageIndex()I

    move-result v7

    .line 102
    sget p1, Lcom/pspdfkit/R$string;->pspdf__document_comparison:I

    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string p0, "getString(R.string.pspdf__document_comparison)"

    invoke-static {v8, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    sget-object v10, Lcom/pspdfkit/annotations/BlendMode;->DARKEN:Lcom/pspdfkit/annotations/BlendMode;

    move-object v4, v0

    move-object v6, v2

    move-object v9, p3

    .line 105
    invoke-static/range {v3 .. v10}, Lcom/pspdfkit/internal/f8;->a(Landroid/content/Context;Landroid/net/Uri;ILandroid/net/Uri;ILjava/lang/String;Landroid/graphics/Matrix;Lcom/pspdfkit/annotations/BlendMode;)Landroid/net/Uri;

    move-result-object p0

    .line 116
    new-instance p1, Lkotlin/Triple;

    invoke-direct {p1, v0, v2, p0}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private final a()V
    .locals 6

    .line 286
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    .line 375
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "requireContext()"

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v4, v1, 0x1

    if-gez v1, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/pspdfkit/document/processor/ComparisonDocument;

    .line 376
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "document_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/pspdfkit/internal/g8;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v1, v4

    goto :goto_0

    .line 379
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "temp_new"

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/g8;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "temp_old"

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/g8;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private final a(I)V
    .locals 4

    .line 212
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->n:Landroid/widget/ProgressBar;

    const-string v1, "<this>"

    if-nez v0, :cond_0

    goto :goto_0

    .line 213
    :cond_0
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 233
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->o:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    goto :goto_1

    .line 235
    :cond_1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    .line 255
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "comparisonDocuments[documentIndex]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/document/processor/ComparisonDocument;

    .line 258
    new-instance v1, Lcom/pspdfkit/document/processor/ComparisonDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    invoke-virtual {v0}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getPageIndex()I

    move-result v0

    const/high16 v3, -0x1000000

    invoke-direct {v1, v2, v0, v3}, Lcom/pspdfkit/document/processor/ComparisonDocument;-><init>(Lcom/pspdfkit/document/DocumentSource;II)V

    .line 260
    new-instance v0, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, v1, p1}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 267
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 268
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 269
    new-instance v1, Lcom/pspdfkit/internal/g8$g;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/g8$g;-><init>(Lcom/pspdfkit/internal/g8;I)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 275
    new-instance v0, Lcom/pspdfkit/internal/g8$h;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/g8$h;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 283
    new-instance v0, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 284
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 285
    iput-object p1, p0, Lcom/pspdfkit/internal/g8;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(ILandroid/graphics/PointF;)V
    .locals 4

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 60
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__point_selection_1:I

    goto :goto_0

    .line 61
    :cond_0
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__point_selection_3:I

    goto :goto_0

    .line 62
    :cond_1
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__point_selection_2:I

    goto :goto_0

    .line 63
    :cond_2
    sget p1, Lcom/pspdfkit/R$drawable;->pspdf__point_selection_1:I

    .line 69
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/document/image/BitmapUtils;->fromDrawable(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v0, "fromDrawable(requireContext(), drawableResourceId)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget v0, p2, Landroid/graphics/PointF;->x:F

    const/high16 v1, 0x41700000    # 15.0f

    sub-float v2, v0, v1

    add-float/2addr v0, v1

    .line 72
    iget p2, p2, Landroid/graphics/PointF;->y:F

    add-float v3, p2, v1

    sub-float/2addr p2, v1

    .line 74
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v2, v3, v0, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 75
    new-instance p2, Lcom/pspdfkit/annotations/StampAnnotation;

    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/processor/ComparisonDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getPageIndex()I

    move-result v0

    invoke-direct {p2, v0, v1, p1}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(ILandroid/graphics/RectF;Landroid/graphics/Bitmap;)V

    const/high16 p1, 0x3f000000    # 0.5f

    .line 76
    invoke-virtual {p2, p1}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 78
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->c:Lcom/pspdfkit/ui/PdfFragment;

    if-nez p1, :cond_3

    const-string p1, "pdfFragment"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 383
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".pdf"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 384
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 385
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method private final a(Landroid/graphics/Matrix;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;)V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->n:Landroid/widget/ProgressBar;

    const-string v1, "<this>"

    if-nez v0, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 138
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 139
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->o:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    goto :goto_1

    .line 140
    :cond_1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    .line 160
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    :goto_1
    new-instance v0, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;Landroid/graphics/Matrix;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const-string p2, "fromCallable {\n         \u2026gedDocumentUri)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 194
    new-instance p2, Lcom/pspdfkit/internal/g8$c;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/g8$c;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 200
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 201
    new-instance p2, Lcom/pspdfkit/internal/g8$d;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/g8$d;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->doOnError(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 204
    new-instance p2, Lcom/pspdfkit/internal/g8$e;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/g8$e;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 205
    new-instance p2, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda4;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 211
    iput-object p1, p0, Lcom/pspdfkit/internal/g8;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private final a(Landroid/view/View;)V
    .locals 4

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__align_progressbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->n:Landroid/widget/ProgressBar;

    .line 6
    sget v0, Lcom/pspdfkit/R$id;->pspdf__cross_hair_target:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->o:Landroid/widget/ImageView;

    .line 8
    sget v0, Lcom/pspdfkit/R$id;->pspdf__comparison_dialog_toolbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView.findViewById(R.\u2026omparison_dialog_toolbar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->l:Landroidx/appcompat/widget/Toolbar;

    const-string v1, "toolbar"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 9
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    new-instance v3, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {v0, v3}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->l:Landroidx/appcompat/widget/Toolbar;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    sget v1, Lcom/pspdfkit/R$string;->pspdf__align_documents:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__select_point_fab:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView.findViewById(R.\u2026.pspdf__select_point_fab)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/floatingactionbutton/ExtendedFloatingActionButton;

    if-nez v0, :cond_2

    const-string v0, "selectPointFab"

    .line 15
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    new-instance v1, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    sget v0, Lcom/pspdfkit/R$id;->pspdf__pointSelectionStepperView:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView.findViewById(R.\u2026ointSelectionStepperView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/internal/ui/stepper/StepperView;

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

    const-string v1, "stepperView"

    if-nez v0, :cond_3

    .line 20
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    iget-object v3, p0, Lcom/pspdfkit/internal/g8;->b:Ljava/util/ArrayList;

    if-nez v3, :cond_4

    const-string v3, "pointSelectionSteps"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v3, v2

    :cond_4
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->setSteps(Ljava/util/List;)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v2, v0

    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/g8;->d:I

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(I)V

    .line 23
    sget v0, Lcom/pspdfkit/R$id;->pspdf__comparison_breadcrumbs:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView.findViewById(R.\u2026__comparison_breadcrumbs)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;

    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->g:Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;

    .line 25
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v0

    const-string v1, "get(requireContext())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget v1, Lcom/pspdfkit/R$id;->pspdf__comparison_hint_text_card:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/cardview/widget/CardView;

    const-string v2, "comparisonHintCard"

    .line 29
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isComparisonFirstLaunch()Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "get(requireContext()).isComparisonFirstLaunch"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const-string v3, "<this>"

    .line 30
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/16 v2, 0x8

    .line 50
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    sget v2, Lcom/pspdfkit/R$id;->pspdf__comparison_hint_dismiss:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 52
    new-instance v2, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda2;

    invoke-direct {v2, v1, v0}, Lcom/pspdfkit/internal/g8$$ExternalSyntheticLambda2;-><init>(Landroidx/cardview/widget/CardView;Lcom/pspdfkit/preferences/PSPDFKitPreferences;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final a(Landroidx/cardview/widget/CardView;Lcom/pspdfkit/preferences/PSPDFKitPreferences;Landroid/view/View;)V
    .locals 0

    const-string p2, "$preferences"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x8

    .line 54
    invoke-virtual {p0, p2}, Landroid/view/View;->setVisibility(I)V

    const/4 p0, 0x0

    .line 55
    invoke-virtual {p1, p0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->setIsComparisonFirstLaunch(Z)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/g8;Landroid/graphics/PointF;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p2, p1}, Lcom/pspdfkit/internal/g8;->a(ILandroid/graphics/PointF;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/g8;Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/g8;->c:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/g8;->k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-object p0
.end method

.method private final b()V
    .locals 10

    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->flatten(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/m5;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    .line 25
    invoke-static {v1}, Lcom/pspdfkit/internal/jni/NativeComparisonUtilities;->calculateMatrixFromPoints(Ljava/util/ArrayList;)Landroid/graphics/Matrix;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "comparisonDocuments[0]"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/pspdfkit/document/processor/ComparisonDocument;

    iget-object v2, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "comparisonDocuments[1]"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/pspdfkit/document/processor/ComparisonDocument;

    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/g8;->a(Landroid/graphics/Matrix;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;)V

    goto :goto_0

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 29
    sget-object v7, Lcom/pspdfkit/internal/g8$f;->a:Lcom/pspdfkit/internal/g8$f;

    const/16 v8, 0x1f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create a matrix for aligning documents using points: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/g8;->h:Lcom/pspdfkit/document/processor/ComparisonDialogListener;

    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/pspdfkit/document/processor/ComparisonDialogListener;->onError(Ljava/lang/Throwable;)V

    .line 33
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    :goto_0
    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/g8;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 3
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->c:Lcom/pspdfkit/ui/PdfFragment;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "pdfFragment"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    iget v3, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/processor/ComparisonDocument;

    invoke-virtual {v2}, Lcom/pspdfkit/document/processor/ComparisonDocument;->getPageIndex()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/ui/PdfFragment;->getVisiblePdfRect(Landroid/graphics/RectF;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    invoke-direct {v0, v2, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/g8;->d:I

    invoke-virtual {p1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 11
    iget p1, p0, Lcom/pspdfkit/internal/g8;->d:I

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/g8;->a(ILandroid/graphics/PointF;)V

    .line 12
    iget p1, p0, Lcom/pspdfkit/internal/g8;->d:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/pspdfkit/internal/g8;->d:I

    const-string v2, "stepperView"

    const/4 v3, 0x2

    if-gt p1, v3, :cond_2

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

    if-nez p1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, p1

    :goto_0
    iget p0, p0, Lcom/pspdfkit/internal/g8;->d:I

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(I)V

    goto :goto_2

    :cond_2
    if-le p1, v3, :cond_6

    .line 16
    iget p1, p0, Lcom/pspdfkit/internal/g8;->e:I

    if-nez p1, :cond_6

    const/4 p1, 0x0

    .line 17
    iput p1, p0, Lcom/pspdfkit/internal/g8;->d:I

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

    if-nez p1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v1

    :cond_3
    iget v2, p0, Lcom/pspdfkit/internal/g8;->d:I

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/ui/stepper/StepperView;->a(I)V

    .line 19
    iget p1, p0, Lcom/pspdfkit/internal/g8;->e:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/pspdfkit/internal/g8;->e:I

    .line 20
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/g8;->a(I)V

    .line 21
    iget p1, p0, Lcom/pspdfkit/internal/g8;->e:I

    if-eqz p1, :cond_4

    if-ne p1, v0, :cond_7

    .line 22
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->g:Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;

    if-nez p1, :cond_5

    const-string p1, "comparisonDocumentTitlesView"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v1, p1

    :goto_1
    iget p0, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->setCurrentDocument(I)V

    goto :goto_2

    .line 23
    :cond_6
    invoke-direct {p0}, Lcom/pspdfkit/internal/g8;->b()V

    :cond_7
    :goto_2
    return-void
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/ui/PdfFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/g8;->c:Lcom/pspdfkit/ui/PdfFragment;

    return-object p0
.end method

.method public static final synthetic d(Lcom/pspdfkit/internal/g8;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic e(Lcom/pspdfkit/internal/g8;)Lcom/pspdfkit/internal/ui/stepper/StepperView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/g8;->f:Lcom/pspdfkit/internal/ui/stepper/StepperView;

    return-object p0
.end method

.method private static final f(Lcom/pspdfkit/internal/g8;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/g8;->a()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->n:Landroid/widget/ProgressBar;

    const-string v1, "<this>"

    if-nez v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x8

    .line 23
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 24
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->o:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    goto :goto_1

    .line 25
    :cond_1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 46
    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    return-void
.end method

.method private static final g(Lcom/pspdfkit/internal/g8;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->n:Landroid/widget/ProgressBar;

    const-string v1, "<this>"

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x8

    .line 22
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 23
    :goto_0
    iget-object p0, p0, Lcom/pspdfkit/internal/g8;->o:Landroid/widget/ImageView;

    if-nez p0, :cond_1

    goto :goto_1

    .line 24
    :cond_1
    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 44
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/document/processor/ComparisonDialogListener;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/g8;->h:Lcom/pspdfkit/document/processor/ComparisonDialogListener;

    return-void
.end method

.method public final c()Lcom/pspdfkit/document/processor/ComparisonDialogListener;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->h:Lcom/pspdfkit/document/processor/ComparisonDialogListener;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v1, "comparison_documents_list_argument"

    .line 4
    const-class v2, Lcom/pspdfkit/document/processor/ComparisonDocument;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelableArrayList(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 5
    iput-object v1, p0, Lcom/pspdfkit/internal/g8;->i:Ljava/util/ArrayList;

    const-string v1, "pdf_configuration_argument"

    .line 9
    const-class v2, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelable(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-eqz v1, :cond_5

    .line 10
    iput-object v1, p0, Lcom/pspdfkit/internal/g8;->k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    const-string v1, "output_file_argument"

    .line 14
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 15
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    const/4 v2, 0x1

    :goto_0
    const/4 v3, 0x4

    const/4 v4, 0x0

    if-ge v2, v3, :cond_0

    .line 18
    sget v3, Lcom/pspdfkit/R$string;->pspdf__point_selection_step:I

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {p0, v3, v5}, Landroidx/fragment/app/DialogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 19
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->b:Ljava/util/ArrayList;

    if-eqz p1, :cond_3

    const-string v0, "selected_point_index"

    .line 21
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/g8;->d:I

    const-string v0, "current_documentIndex"

    .line 22
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/g8;->e:I

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    const-class v2, Landroid/graphics/PointF;

    const-string v3, "old_selected_points"

    invoke-static {p1, v3, v2}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelableArrayList(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-virtual {v0, v4, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    const-class v2, Landroid/graphics/PointF;

    const-string v3, "new_selected_points"

    invoke-static {p1, v3, v2}, Lcom/pspdfkit/utils/BundleExtensions;->getSupportParcelableArrayList(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object p1

    if-nez p1, :cond_2

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void

    .line 25
    :cond_4
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No output file path was provided."

    .line 28
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 29
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No PdfActivityConfiguration was provided."

    .line 31
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 32
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No documents were provided."

    .line 34
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 35
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No arguments were supplied."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 47
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error while creating DocumentComparisonDialog. Make sure to show the dialog by calling DocumentComparisonDialog.show(...) rather than creating the dialog manually."

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    const/4 p3, 0x0

    const-string v0, "configuration"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, p3

    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getThemeMode()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/pspdfkit/internal/g8$b;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v1, p1

    :goto_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object p3, p1

    :goto_1
    invoke-virtual {p3}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getDarkTheme()I

    move-result p1

    goto :goto_3

    .line 3
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->k:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object p3, p1

    :goto_2
    invoke-virtual {p3}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTheme()I

    move-result p1

    .line 5
    :goto_3
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {p3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 6
    sget p3, Lcom/pspdfkit/R$layout;->pspdf__compare_documents_dialog:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string p2, "rootView"

    .line 7
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/g8;->a(Landroid/view/View;)V

    return-object p1
.end method

.method public final onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->m:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/g8;->h:Lcom/pspdfkit/document/processor/ComparisonDialogListener;

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string v1, "old_selected_points"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/g8;->j:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string v1, "new_selected_points"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/g8;->e:I

    const-string v1, "current_documentIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 5
    iget v0, p0, Lcom/pspdfkit/internal/g8;->d:I

    const-string v1, "selected_point_index"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    if-nez p2, :cond_0

    .line 3
    iget p1, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/g8;->a(I)V

    goto :goto_1

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/g8;->g:Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;

    const/4 p2, 0x0

    if-nez p1, :cond_1

    const-string p1, "comparisonDocumentTitlesView"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, p2

    :cond_1
    iget v0, p0, Lcom/pspdfkit/internal/g8;->e:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/comparison/ComparisonDocumentTitlesView;->setCurrentDocument(I)V

    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "com.pspdfkit.ui.PdfFragment"

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type com.pspdfkit.ui.PdfFragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/ui/PdfFragment;

    iput-object p1, p0, Lcom/pspdfkit/internal/g8;->c:Lcom/pspdfkit/ui/PdfFragment;

    if-nez p1, :cond_2

    const-string p1, "pdfFragment"

    .line 7
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object p2, p1

    :goto_0
    new-instance p1, Lcom/pspdfkit/internal/g8$i;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/g8$i;-><init>(Lcom/pspdfkit/internal/g8;)V

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    :goto_1
    return-void
.end method

.class public final Lcom/pspdfkit/internal/b6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/contentediting/ContentEditingFillColorConfiguration;


# instance fields
.field private final a:Lcom/pspdfkit/internal/d6;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/d6;

    invoke-direct {v0}, Lcom/pspdfkit/internal/d6;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/b6;->a()V

    return-void
.end method

.method private final a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d6;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/ao;->c()Ljava/util/List;

    move-result-object v0

    .line 4
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/d6;->a(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final customColorPickerEnabled()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    sget-object v1, Lcom/pspdfkit/internal/c6;->a:Lcom/pspdfkit/internal/c6;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/d6;->a(Lcom/pspdfkit/internal/c6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getAvailableFillColors()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/c6;->c:Lcom/pspdfkit/internal/c6;

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/ao;->c()Ljava/util/List;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/d6;->a(Lcom/pspdfkit/internal/c6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getDefaultFillColor()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b6;->a:Lcom/pspdfkit/internal/d6;

    sget-object v1, Lcom/pspdfkit/internal/c6;->b:Lcom/pspdfkit/internal/c6;

    const/high16 v2, -0x1000000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/d6;->a(Lcom/pspdfkit/internal/c6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

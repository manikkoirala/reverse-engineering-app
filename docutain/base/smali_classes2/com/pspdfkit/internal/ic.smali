.class public final Lcom/pspdfkit/internal/ic;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final h:[I

.field private static final i:I

.field private static final j:I


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection:[I

    sput-object v0, Lcom/pspdfkit/internal/ic;->h:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__formSelectionStyle:I

    sput v0, Lcom/pspdfkit/internal/ic;->i:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_FormSelection:I

    sput v0, Lcom/pspdfkit/internal/ic;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ic;->h:[I

    sget v2, Lcom/pspdfkit/internal/ic;->i:I

    sget v3, Lcom/pspdfkit/internal/ic;->j:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 3
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__highlightColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__form_highlight_color:I

    .line 5
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->a:I

    .line 9
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__itemHighlightColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__selected_choice_form_item_highlight_color:I

    .line 11
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 12
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->b:I

    .line 15
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__selectedTextElementBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__selected_text_form_element_background_color:I

    .line 17
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->d:I

    .line 21
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__touchedFormElementHighlightColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__touched_form_element_highlight_color:I

    .line 23
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->c:I

    .line 27
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__selectedTextElementBorderColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__selected_text_form_element_border_color:I

    .line 29
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->e:I

    .line 33
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__requiredTextElementBorderColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__required_text_form_element_border_color:I

    .line 35
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 36
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/ic;->f:I

    .line 39
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__FormSelection_pspdf__signHereOverlayBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__form_sign_here_overlay_color:I

    .line 41
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 42
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ic;->g:I

    .line 45
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

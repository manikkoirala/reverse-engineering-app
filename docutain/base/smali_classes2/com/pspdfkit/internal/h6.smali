.class public interface abstract Lcom/pspdfkit/internal/h6;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lcom/pspdfkit/internal/jt;)V
.end method

.method public abstract b(Lcom/pspdfkit/internal/jt;)Z
.end method

.method public abstract c(Lcom/pspdfkit/internal/jt;)V
.end method

.method public abstract d(Lcom/pspdfkit/internal/jt;)Z
.end method

.method public abstract getAvailableFontSizes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setBold(Z)V
.end method

.method public abstract setFaceName(Ljava/lang/String;)V
.end method

.method public abstract setFontColor(I)V
.end method

.method public abstract setFontSize(F)V
.end method

.method public abstract setItalic(Z)V
.end method

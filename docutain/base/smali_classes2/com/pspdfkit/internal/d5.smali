.class final Lcom/pspdfkit/internal/d5;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/yh;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yh;)V
    .locals 1

    const-string v0, "managedBitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/d5;->a:Lcom/pspdfkit/internal/yh;

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/yh;->b()V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/tc;->a(Lcom/pspdfkit/internal/rc;)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/d5;->b:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d5;->a:Lcom/pspdfkit/internal/yh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v1, "managedBitmap.bitmap"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/rc;)Z
    .locals 1

    const-string v0, "renderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/tc;->a(Lcom/pspdfkit/internal/rc;)I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/internal/d5;->b:I

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()Lcom/pspdfkit/internal/yh;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d5;->a:Lcom/pspdfkit/internal/yh;

    return-object v0
.end method

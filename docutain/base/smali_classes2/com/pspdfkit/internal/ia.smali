.class public final Lcom/pspdfkit/internal/ia;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;
    .locals 2

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/2addr p1, p2

    add-int/lit16 p1, p1, 0x168

    .line 1
    rem-int/lit16 p1, p1, 0x168

    const/16 p2, 0x5a

    if-eq p1, p2, :cond_2

    const/16 p2, 0xb4

    if-eq p1, p2, :cond_1

    const/16 p2, 0x10e

    if-eq p1, p2, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    new-instance p1, Lcom/pspdfkit/utils/EdgeInsets;

    iget p2, p0, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget v0, p0, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    iget v1, p0, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    iget p0, p0, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    invoke-direct {p1, p2, v0, v1, p0}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    goto :goto_0

    .line 5
    :cond_1
    new-instance p1, Lcom/pspdfkit/utils/EdgeInsets;

    iget p2, p0, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    iget v0, p0, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    iget v1, p0, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget p0, p0, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    invoke-direct {p1, p2, v0, v1, p0}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    goto :goto_0

    .line 6
    :cond_2
    new-instance p1, Lcom/pspdfkit/utils/EdgeInsets;

    iget p2, p0, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    iget v0, p0, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget v1, p0, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget p0, p0, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    invoke-direct {p1, p2, v0, v1, p0}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    :goto_0
    move-object p0, p1

    :goto_1
    return-object p0
.end method

.class public interface abstract Lcom/pspdfkit/internal/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract addUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V
.end method

.method public abstract getDocumentListeners()Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/listeners/DocumentListener;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPasteManager()Lcom/pspdfkit/internal/h7;
.end method

.method public abstract getViewCoordinator()Lcom/pspdfkit/internal/xm;
.end method

.method public abstract isLastViewedPageRestorationActiveAndIsConfigChange()Z
.end method

.method public abstract removeUserInterfaceListener(Lcom/pspdfkit/internal/ev;)V
.end method

.method public abstract setDocument(Lcom/pspdfkit/document/PdfDocument;)V
.end method

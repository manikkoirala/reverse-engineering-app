.class public final Lcom/pspdfkit/internal/z8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/r8;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/zf;

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$BvtW7YmZGueHbjNLpS3hZRCWSO0(Lcom/pspdfkit/internal/z8;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/z8;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$K0FKe91GhEjI0vEh68M1XqEafpo(Lcom/pspdfkit/internal/z8;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/z8;->b()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$kdQdSVE1SmVV5hTt_raSE3mFl1Q(Lcom/pspdfkit/internal/z8;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/z8;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/z8;->c:Lcom/pspdfkit/internal/nh;

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    .line 13
    iput-object p2, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method private synthetic a(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 44
    iget-object p1, p0, Lcom/pspdfkit/internal/z8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/z8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;

    .line 46
    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-interface {v0, v1}, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;->onDocumentInfoChangesSaved(Lcom/pspdfkit/document/PdfDocument;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string v0, "PSPDFKit.PdfActivity"

    const-string v1, "Couldn\'t save document."

    .line 47
    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v0, Lcom/pspdfkit/R$string;->pspdf__document_could_not_be_saved:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private b()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/w8;

    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__document_info_title:I

    .line 4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/g9;->getTitle()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    invoke-direct {v1, v5, v2, v3, v5}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    new-instance v1, Lcom/pspdfkit/internal/w8;

    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__document_info_author:I

    .line 13
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/g9;->getAuthor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    invoke-direct {v1, v6, v2, v3, v5}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 15
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    new-instance v1, Lcom/pspdfkit/internal/w8;

    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v3, Lcom/pspdfkit/R$string;->pspdf__document_info_subject:I

    .line 22
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 23
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/g9;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x3

    invoke-direct {v1, v7, v2, v3, v5}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 24
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    new-instance v1, Lcom/pspdfkit/internal/xl;

    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/internal/xl;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PageBinding;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/g9;->getKeywords()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const/4 v8, 0x0

    .line 34
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 35
    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    sub-int/2addr v9, v5

    if-ge v8, v9, :cond_0

    const-string v9, ", "

    .line 37
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 41
    :cond_1
    new-instance v2, Lcom/pspdfkit/internal/w8;

    iget-object v8, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v9, Lcom/pspdfkit/R$string;->pspdf__document_info_keywords:I

    .line 43
    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x4

    invoke-direct {v2, v9, v8, v1, v5}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 45
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v1, Lcom/pspdfkit/internal/u8;

    iget-object v2, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__document_info_content:I

    .line 52
    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline:I

    invoke-direct {v1, v5, v2, v8, v0}, Lcom/pspdfkit/internal/u8;-><init>(ILjava/lang/String;ILjava/util/ArrayList;)V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    new-instance v2, Lcom/pspdfkit/internal/w8;

    const/4 v8, 0x7

    iget-object v9, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v10, Lcom/pspdfkit/R$string;->pspdf__document_info_creation_date:I

    .line 59
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 60
    invoke-virtual {v10}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/g9;->getCreationDate()Ljava/util/Date;

    move-result-object v10

    if-nez v10, :cond_2

    move-object v10, v4

    goto :goto_1

    .line 63
    :cond_2
    iget-object v11, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    .line 66
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    invoke-static {v11}, Landroidx/core/os/ConfigurationCompat;->getLocales(Landroid/content/res/Configuration;)Landroidx/core/os/LocaleListCompat;

    move-result-object v11

    .line 67
    invoke-virtual {v11, v3}, Landroidx/core/os/LocaleListCompat;->get(I)Ljava/util/Locale;

    move-result-object v11

    .line 68
    invoke-static {v5, v7, v11}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v11

    .line 74
    invoke-virtual {v11, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 75
    :goto_1
    invoke-direct {v2, v8, v9, v10, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 76
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v2, Lcom/pspdfkit/internal/w8;

    const/16 v8, 0x8

    iget-object v9, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v10, Lcom/pspdfkit/R$string;->pspdf__document_info_mod_date:I

    .line 83
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 84
    invoke-virtual {v10}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/g9;->getModificationDate()Ljava/util/Date;

    move-result-object v10

    if-nez v10, :cond_3

    move-object v5, v4

    goto :goto_2

    .line 87
    :cond_3
    iget-object v11, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    .line 90
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    invoke-static {v11}, Landroidx/core/os/ConfigurationCompat;->getLocales(Landroid/content/res/Configuration;)Landroidx/core/os/LocaleListCompat;

    move-result-object v11

    .line 91
    invoke-virtual {v11, v3}, Landroidx/core/os/LocaleListCompat;->get(I)Ljava/util/Locale;

    move-result-object v11

    .line 92
    invoke-static {v5, v7, v11}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v5

    .line 98
    invoke-virtual {v5, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 99
    :goto_2
    invoke-direct {v2, v8, v9, v5, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 100
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v2, Lcom/pspdfkit/internal/w8;

    iget-object v5, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__document_info_content_creator:I

    .line 107
    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 108
    invoke-virtual {v8}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/g9;->getCreator()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x5

    invoke-direct {v2, v9, v5, v8, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 109
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v2, Lcom/pspdfkit/internal/w8;

    iget-object v5, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__document_info_producer:I

    .line 116
    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 117
    invoke-virtual {v8}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/g9;->getProducer()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x6

    invoke-direct {v2, v9, v5, v8, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 118
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v2, Lcom/pspdfkit/internal/u8;

    iget-object v5, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__document_info_changes:I

    .line 125
    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__ic_info:I

    invoke-direct {v2, v6, v5, v8, v0}, Lcom/pspdfkit/internal/u8;-><init>(ILjava/lang/String;ILjava/util/ArrayList;)V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    new-instance v5, Lcom/pspdfkit/internal/w8;

    iget-object v6, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v8, Lcom/pspdfkit/R$string;->pspdf__document_info_number_pf_pages:I

    .line 132
    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    .line 133
    invoke-virtual {v8}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Objects;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x9

    invoke-direct {v5, v9, v6, v8, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 134
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v5, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v5

    .line 144
    invoke-virtual {v5}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 145
    new-instance v6, Ljava/io/File;

    invoke-virtual {v5}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v5

    goto :goto_3

    .line 146
    :cond_4
    invoke-virtual {v5}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/document/providers/DataProvider;->getSize()J

    move-result-wide v5

    .line 147
    :goto_3
    new-instance v8, Lcom/pspdfkit/internal/w8;

    iget-object v9, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v10, Lcom/pspdfkit/R$string;->pspdf__document_info_file_size:I

    .line 149
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    .line 150
    invoke-static {v10, v5, v6}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Ljava/util/Objects;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    invoke-direct {v8, v5, v9, v4, v3}, Lcom/pspdfkit/internal/w8;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 151
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v3, Lcom/pspdfkit/internal/u8;

    iget-object v4, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__size:I

    .line 159
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/pspdfkit/R$drawable;->pspdf__ic_size:I

    invoke-direct {v3, v7, v4, v5, v0}, Lcom/pspdfkit/internal/u8;-><init>(ILjava/lang/String;ILjava/util/ArrayList;)V

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/w8;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;

    move-result-object v0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/w8;->b()I

    move-result v1

    invoke-static {v1}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v1

    const/16 v2, 0xe

    if-eq v1, v2, :cond_4

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 13
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    .line 14
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ",\\s"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 15
    check-cast v0, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/g9;->setKeywords(Ljava/util/List;)V

    goto :goto_0

    .line 16
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    check-cast v0, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/g9;->setSubject(Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    check-cast v0, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/g9;->setAuthor(Ljava/lang/String;)V

    goto :goto_0

    .line 18
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    check-cast v0, Lcom/pspdfkit/internal/g9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/g9;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 31
    :cond_4
    check-cast p1, Lcom/pspdfkit/internal/xl;

    .line 32
    invoke-virtual {p1}, Lcom/pspdfkit/internal/xl;->e()Lcom/pspdfkit/document/PageBinding;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/document/PageBinding;->UNKNOWN:Lcom/pspdfkit/document/PageBinding;

    if-eq v0, v1, :cond_5

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/xl;->e()Lcom/pspdfkit/document/PageBinding;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zf;->setPageBinding(Lcom/pspdfkit/document/PageBinding;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a()Z
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    .line 38
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v1

    instance-of v1, v1, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-nez v1, :cond_0

    return v2

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->a()Z

    move-result v0

    xor-int/2addr v0, v2

    return v0
.end method

.method public final b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final c()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u8;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/z8;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 100
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z8;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda0;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/z8;->b:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x5

    .line 3
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/z8;)V

    new-instance v2, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/z8$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/z8;)V

    .line 5
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    goto :goto_0

    :cond_0
    const-string v0, "PSPDFKit.PdfActivity"

    const-string v1, "Trying to save readonly document."

    .line 21
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/z8;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__document_could_not_be_saved:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void
.end method

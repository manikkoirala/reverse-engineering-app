.class public final Lcom/pspdfkit/internal/o1;
.super Lcom/pspdfkit/internal/ma;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/el;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/ma<",
        "Lcom/pspdfkit/internal/n1;",
        ">;",
        "Lcom/pspdfkit/internal/el;"
    }
.end annotation


# instance fields
.field private final e:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Ljava/util/List;Lcom/pspdfkit/internal/fl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/fl;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/ma;-><init>(Lcom/pspdfkit/internal/fl;)V

    .line 2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/o1;->e:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;
    .locals 3

    const-string v0, "annotation"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onEditRecordedListener"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/pspdfkit/internal/o1;

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/o1;-><init>(Ljava/util/List;Lcom/pspdfkit/internal/fl;)V

    return-object v0
.end method

.method public static a(Ljava/util/List;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/fl;",
            ")",
            "Lcom/pspdfkit/internal/o1;"
        }
    .end annotation

    const-string v0, "annotations"

    const-string v1, "argumentName"

    .line 109
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 160
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "onEditRecordedListener"

    .line 162
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 214
    new-instance v0, Lcom/pspdfkit/internal/o1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/o1;-><init>(Ljava/util/List;Lcom/pspdfkit/internal/fl;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 215
    invoke-super {p0}, Lcom/pspdfkit/internal/ma;->a()V

    .line 216
    iget-object v0, p0, Lcom/pspdfkit/internal/o1;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 217
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/ma;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/o1;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onAnnotationPropertyChange(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o1;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.UndoRedo"

    const-string v2, "Annotation reporting property changes to this recorder is not the in the collection of annotations whose property edits were set to be recorded by this object."

    .line 10
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    if-eqz p3, :cond_1

    .line 14
    invoke-virtual {p3, p4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 19
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/n1;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/n1;-><init>(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V

    .line 21
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ma;->a(Lcom/pspdfkit/internal/n1;)V

    return-void
.end method

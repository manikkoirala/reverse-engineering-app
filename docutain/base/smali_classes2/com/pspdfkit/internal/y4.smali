.class public abstract Lcom/pspdfkit/internal/y4;
.super Lcom/pspdfkit/internal/h4;
.source "SourceFile"


# instance fields
.field private m:Lcom/pspdfkit/annotations/BorderStyle;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/pspdfkit/annotations/BorderEffect;

.field private p:F

.field protected q:F

.field protected r:F


# direct methods
.method constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/h4;-><init>(IIFF)V

    .line 2
    invoke-virtual {p5}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    .line 3
    invoke-virtual {p5}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    .line 4
    invoke-virtual {p5}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/y4;->p:F

    .line 5
    invoke-virtual {p5}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    .line 4
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 5
    sget-object p2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    const/high16 p2, 0x41200000    # 10.0f

    .line 6
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    .line 7
    sget-object p2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const/4 p2, 0x0

    .line 8
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    div-float/2addr v0, p3

    .line 13
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    .line 14
    sget-object v1, Lcom/pspdfkit/annotations/BorderStyle;->DASHED:Lcom/pspdfkit/annotations/BorderStyle;

    if-ne p3, v1, :cond_2

    .line 15
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    if-eqz p3, :cond_2

    .line 16
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-lez p3, :cond_2

    .line 17
    iget-object p2, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 18
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x2

    const/4 v1, 0x0

    if-lt p2, p3, :cond_0

    .line 19
    iget-object p2, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 20
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    new-array p2, p2, [F

    .line 21
    :goto_0
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 22
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-ge v1, p3, :cond_1

    .line 23
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 24
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    int-to-float p3, p3

    mul-float p3, p3, v0

    aput p3, p2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-array p2, p3, [F

    .line 30
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 31
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    int-to-float p3, p3

    mul-float p3, p3, v0

    aput p3, p2, v1

    .line 32
    iget-object p3, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    .line 33
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    int-to-float p3, p3

    mul-float p3, p3, v0

    const/4 v0, 0x1

    aput p3, p2, v0

    .line 35
    :cond_1
    new-instance p3, Landroid/graphics/DashPathEffect;

    const/4 v0, 0x0

    invoke-direct {p3, p2, v0}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 36
    sget-object p2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_1

    .line 38
    :cond_2
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 39
    sget-object p2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    :goto_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/BorderEffect;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/BorderStyle;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    .line 43
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    .line 45
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    .line 47
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v0

    .line 48
    iput v0, p0, Lcom/pspdfkit/internal/y4;->p:F

    .line 49
    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object p1

    .line 50
    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 42
    iput-object p1, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    return-void
.end method

.method public final c(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/y4;->p:F

    return-void
.end method

.method protected e()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/y4;->q:F

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/y4;->p:F

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    .line 4
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/pspdfkit/internal/y4;->r:F

    return-void
.end method

.method public final p()Lcom/pspdfkit/annotations/BorderEffect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    return-object v0
.end method

.method public final q()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/y4;->p:F

    return v0
.end method

.method public final r()Lcom/pspdfkit/annotations/BorderStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    return-object v0
.end method

.method public final s()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->n:Ljava/util/List;

    return-object v0
.end method

.method protected final t()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->m:Lcom/pspdfkit/annotations/BorderStyle;

    .line 2
    sget-object v1, Lcom/pspdfkit/annotations/BorderStyle;->NONE:Lcom/pspdfkit/annotations/BorderStyle;

    if-ne v0, v1, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    .line 4
    sget-object v1, Lcom/pspdfkit/annotations/BorderEffect;->NO_EFFECT:Lcom/pspdfkit/annotations/BorderEffect;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected final u()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y4;->o:Lcom/pspdfkit/annotations/BorderEffect;

    .line 2
    sget-object v1, Lcom/pspdfkit/annotations/BorderEffect;->CLOUDY:Lcom/pspdfkit/annotations/BorderEffect;

    if-ne v0, v1, :cond_0

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/y4;->p:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.class final Lcom/pspdfkit/internal/k6$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/k6;->u()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/k6;

.field final synthetic b:J


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/k6;J)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/k6$j;->a:Lcom/pspdfkit/internal/k6;

    iput-wide p2, p0, Lcom/pspdfkit/internal/k6$j;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 6

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/o6;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$j;->a:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->h(Lcom/pspdfkit/internal/k6;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ip;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ip;->b()Ljava/util/UUID;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/k6$b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ip;

    iget-wide v4, p0, Lcom/pspdfkit/internal/k6$j;->b:J

    invoke-direct {v2, v3, v4, v5}, Lcom/pspdfkit/internal/k6$b;-><init>(Lcom/pspdfkit/internal/ip;J)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$j;->a:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->g(Lcom/pspdfkit/internal/k6;)Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/k6$j;->a:Lcom/pspdfkit/internal/k6;

    .line 529
    invoke-virtual {p1}, Lcom/pspdfkit/internal/o6;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ip;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ip;->b()Ljava/util/UUID;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object p1

    if-nez p1, :cond_0

    .line 530
    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->f(Lcom/pspdfkit/internal/k6;)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->e(Lcom/pspdfkit/internal/k6;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v0, p1, v2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 534
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/k6$j;->a:Lcom/pspdfkit/internal/k6;

    invoke-static {p1}, Lcom/pspdfkit/internal/k6;->i(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/os;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    :cond_1
    return-void
.end method

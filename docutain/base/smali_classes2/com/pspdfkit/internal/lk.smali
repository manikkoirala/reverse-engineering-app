.class public final enum Lcom/pspdfkit/internal/lk;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/lk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic c:[Lcom/pspdfkit/internal/lk;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/lk;

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_status_accepted:I

    sget v2, Lcom/pspdfkit/R$string;->pspdf__reply_status_accepted:I

    const-string v3, "ACCEPTED"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/pspdfkit/internal/lk;-><init>(Ljava/lang/String;III)V

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/lk;

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_status_rejected:I

    sget v3, Lcom/pspdfkit/R$string;->pspdf__reply_status_rejected:I

    const-string v5, "REJECTED"

    const/4 v6, 0x1

    invoke-direct {v1, v5, v6, v2, v3}, Lcom/pspdfkit/internal/lk;-><init>(Ljava/lang/String;III)V

    .line 3
    new-instance v2, Lcom/pspdfkit/internal/lk;

    sget v3, Lcom/pspdfkit/R$drawable;->pspdf__ic_status_cancelled:I

    sget v5, Lcom/pspdfkit/R$string;->pspdf__reply_status_cancelled:I

    const-string v7, "CANCELLED"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v3, v5}, Lcom/pspdfkit/internal/lk;-><init>(Ljava/lang/String;III)V

    .line 4
    new-instance v3, Lcom/pspdfkit/internal/lk;

    sget v5, Lcom/pspdfkit/R$drawable;->pspdf__ic_status_completed:I

    sget v7, Lcom/pspdfkit/R$string;->pspdf__reply_status_completed:I

    const-string v9, "COMPLETED"

    const/4 v10, 0x3

    invoke-direct {v3, v9, v10, v5, v7}, Lcom/pspdfkit/internal/lk;-><init>(Ljava/lang/String;III)V

    .line 5
    new-instance v5, Lcom/pspdfkit/internal/lk;

    sget v7, Lcom/pspdfkit/R$drawable;->pspdf__ic_status_clear:I

    sget v9, Lcom/pspdfkit/R$string;->pspdf__reply_status_none:I

    const-string v11, "NONE"

    const/4 v12, 0x4

    invoke-direct {v5, v11, v12, v7, v9}, Lcom/pspdfkit/internal/lk;-><init>(Ljava/lang/String;III)V

    const/4 v7, 0x5

    new-array v7, v7, [Lcom/pspdfkit/internal/lk;

    aput-object v0, v7, v4

    aput-object v1, v7, v6

    aput-object v2, v7, v8

    aput-object v3, v7, v10

    aput-object v5, v7, v12

    .line 6
    sput-object v7, Lcom/pspdfkit/internal/lk;->c:[Lcom/pspdfkit/internal/lk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput p3, p0, Lcom/pspdfkit/internal/lk;->a:I

    .line 3
    iput p4, p0, Lcom/pspdfkit/internal/lk;->b:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/lk;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/internal/lk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/lk;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/lk;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/lk;->c:[Lcom/pspdfkit/internal/lk;

    invoke-virtual {v0}, [Lcom/pspdfkit/internal/lk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/lk;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lk;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lk;->b:I

    return v0
.end method

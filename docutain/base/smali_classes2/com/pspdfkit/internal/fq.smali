.class public final Lcom/pspdfkit/internal/fq;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Landroid/graphics/RectF;FFLandroid/graphics/Path;)V
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "rect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 2
    iget v9, v0, Landroid/graphics/RectF;->top:F

    .line 3
    iget v10, v0, Landroid/graphics/RectF;->right:F

    .line 4
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v10, v1

    sub-float v3, v0, v9

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    move/from16 v5, p1

    .line 5
    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/4 v5, 0x0

    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v11

    div-float/2addr v3, v4

    move/from16 v2, p2

    .line 6
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v12

    if-nez p3, :cond_0

    .line 7
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object v13, v2

    goto :goto_0

    :cond_0
    move-object/from16 v13, p3

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    cmpg-float v4, v11, v5

    if-nez v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    cmpg-float v4, v12, v5

    if-nez v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    .line 12
    invoke-virtual {v13, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 13
    invoke-virtual {v13, v1, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 14
    invoke-virtual {v13, v10, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 15
    invoke-virtual {v13, v10, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 16
    invoke-virtual {v13}, Landroid/graphics/Path;->close()V

    return-void

    :cond_3
    int-to-float v2, v3

    const v3, 0x3f0d6288

    sub-float/2addr v2, v3

    mul-float v14, v11, v2

    mul-float v15, v12, v2

    add-float v8, v1, v11

    .line 26
    invoke-virtual {v13, v8, v0}, Landroid/graphics/Path;->moveTo(FF)V

    add-float v16, v1, v14

    sub-float v17, v0, v15

    add-float v7, v9, v12

    move-object v2, v13

    move/from16 v3, v16

    move v4, v0

    move v5, v1

    move/from16 v6, v17

    move/from16 p0, v7

    move v7, v1

    move/from16 p1, v8

    move/from16 v8, p0

    .line 29
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 39
    invoke-virtual {v13, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    add-float/2addr v15, v9

    move v3, v1

    move v4, v15

    move/from16 v5, v16

    move v6, v9

    move/from16 v7, p1

    move v1, v8

    move v8, v9

    .line 42
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    sub-float v11, v10, v11

    .line 52
    invoke-virtual {v13, v11, v9}, Landroid/graphics/Path;->lineTo(FF)V

    sub-float v14, v10, v14

    move v3, v14

    move v4, v9

    move v5, v10

    move v6, v15

    move v7, v10

    move v8, v1

    .line 55
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    sub-float v1, v0, v12

    .line 65
    invoke-virtual {v13, v10, v1}, Landroid/graphics/Path;->lineTo(FF)V

    move v3, v10

    move/from16 v4, v17

    move v5, v14

    move v6, v0

    move v7, v11

    move v8, v0

    .line 68
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move/from16 v1, p1

    .line 78
    invoke-virtual {v13, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 79
    invoke-virtual {v13}, Landroid/graphics/Path;->close()V

    return-void
.end method

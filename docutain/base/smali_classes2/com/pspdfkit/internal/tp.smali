.class public final Lcom/pspdfkit/internal/tp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/ResetFormAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    const-string v0, "documentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/tp;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-void
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/ResetFormAction;

    const-string p2, "action"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p2, p0, Lcom/pspdfkit/internal/tp;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 29
    :cond_0
    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->getTargetFormFieldsAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 30
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/pspdfkit/internal/rp;

    invoke-direct {v1, p2, p1}, Lcom/pspdfkit/internal/rp;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/actions/ResetFormAction;)V

    sget-object p1, Lcom/pspdfkit/internal/sp;->a:Lcom/pspdfkit/internal/sp;

    invoke-virtual {v0, v1, p1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 p1, 0x1

    :goto_0
    return p1
.end method

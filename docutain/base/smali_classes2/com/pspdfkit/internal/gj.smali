.class public final Lcom/pspdfkit/internal/gj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/pspdfkit/internal/r; = null

.field static b:Lcom/pspdfkit/internal/yl; = null

.field static c:Lcom/pspdfkit/internal/u; = null

.field static d:Lcom/pspdfkit/internal/hb; = null

.field static e:Lcom/pspdfkit/internal/dj; = null

.field private static f:Lcom/pspdfkit/internal/n4; = null

.field private static g:Lcom/pspdfkit/internal/q; = null

.field private static h:Lcom/pspdfkit/internal/fn; = null

.field private static i:Lcom/pspdfkit/listeners/LocalizationListener; = null

.field private static j:Lcom/pspdfkit/internal/k; = null

.field private static k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy; = null

.field static l:Lcom/pspdfkit/internal/lp; = null

.field private static m:Lcom/pspdfkit/internal/mt; = null

.field static n:Lcom/pspdfkit/internal/jo; = null

.field private static o:Lcom/pspdfkit/internal/mj; = null

.field private static p:Lcom/pspdfkit/internal/b0; = null

.field static q:Lcom/pspdfkit/internal/tr; = null

.field private static r:Lcom/pspdfkit/internal/oq; = null

.field private static s:Lcom/pspdfkit/internal/fr; = null

.field private static t:Lcom/pspdfkit/signatures/SignatureBitmapStorage; = null

.field static u:Landroid/content/Context; = null

.field static v:Ljava/lang/ref/WeakReference; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static w:Z = false


# direct methods
.method public static a()Landroid/app/Activity;
    .locals 1

    .line 112
    sget-object v0, Lcom/pspdfkit/internal/gj;->v:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    invoke-static {p0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/pspdfkit/internal/gj;->v:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    :try_start_0
    const-string v1, "applicationPolicy"

    const-string v2, "argumentName"

    .line 57
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 108
    invoke-static {p0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    sput-object p0, Lcom/pspdfkit/internal/gj;->k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static a(Lcom/pspdfkit/listeners/LocalizationListener;)V
    .locals 0

    .line 111
    sput-object p0, Lcom/pspdfkit/internal/gj;->i:Lcom/pspdfkit/listeners/LocalizationListener;

    return-void
.end method

.method public static declared-synchronized a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 110
    :try_start_0
    new-instance v1, Lcom/pspdfkit/internal/mt;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/mt;-><init>(Ljava/util/List;)V

    sput-object v1, Lcom/pspdfkit/internal/gj;->m:Lcom/pspdfkit/internal/mt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Z)V
    .locals 1

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 114
    :try_start_0
    sput-boolean p0, Lcom/pspdfkit/internal/gj;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized b()Lcom/pspdfkit/internal/k;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->j:Lcom/pspdfkit/internal/k;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/k;

    invoke-direct {v1}, Lcom/pspdfkit/internal/k;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->j:Lcom/pspdfkit/internal/k;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->j:Lcom/pspdfkit/internal/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 0

    .line 5
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/pspdfkit/internal/gj;->u:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized c()Lcom/pspdfkit/internal/q;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->g:Lcom/pspdfkit/internal/q;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/q;

    invoke-direct {v1}, Lcom/pspdfkit/internal/q;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->g:Lcom/pspdfkit/internal/q;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->g:Lcom/pspdfkit/internal/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized d()Lcom/pspdfkit/internal/b0;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->p:Lcom/pspdfkit/internal/b0;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/b0;

    invoke-direct {v1}, Lcom/pspdfkit/internal/b0;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->p:Lcom/pspdfkit/internal/b0;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->p:Lcom/pspdfkit/internal/b0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static e()Landroid/content/Context;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/gj;->u:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized f()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/configuration/policy/DefaultApplicationPolicy;

    invoke-direct {v1}, Lcom/pspdfkit/configuration/policy/DefaultApplicationPolicy;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized g()Lcom/pspdfkit/internal/yl;
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->b:Lcom/pspdfkit/internal/yl;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/yl;

    const/high16 v2, 0xf00000

    .line 3
    invoke-static {v2}, Lcom/pspdfkit/internal/jni/NativePageCache;->create(I)Lcom/pspdfkit/internal/jni/NativePageCache;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/yl;-><init>(Lcom/pspdfkit/internal/jni/NativePageCache;)V

    sput-object v1, Lcom/pspdfkit/internal/gj;->b:Lcom/pspdfkit/internal/yl;

    .line 6
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->b:Lcom/pspdfkit/internal/yl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized h()Lcom/pspdfkit/internal/n4;
    .locals 6

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->f:Lcom/pspdfkit/internal/n4;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/n4;

    .line 3
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    .line 4
    div-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/internal/n4;-><init>(J)V

    .line 5
    sput-object v1, Lcom/pspdfkit/internal/gj;->f:Lcom/pspdfkit/internal/n4;

    .line 7
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->f:Lcom/pspdfkit/internal/n4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized i()Lcom/pspdfkit/internal/oq;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->r:Lcom/pspdfkit/internal/oq;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/oq;

    invoke-direct {v1}, Lcom/pspdfkit/internal/oq;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->r:Lcom/pspdfkit/internal/oq;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->r:Lcom/pspdfkit/internal/oq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized j()Lcom/pspdfkit/internal/hb;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->d:Lcom/pspdfkit/internal/hb;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/hb;

    invoke-direct {v1}, Lcom/pspdfkit/internal/hb;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->d:Lcom/pspdfkit/internal/hb;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->d:Lcom/pspdfkit/internal/hb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized k()Lcom/pspdfkit/internal/of;
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->u:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 5
    sget-object v2, Lcom/pspdfkit/internal/gj;->a:Lcom/pspdfkit/internal/r;

    if-nez v2, :cond_0

    .line 6
    new-instance v2, Lcom/pspdfkit/internal/r;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/r;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/pspdfkit/internal/gj;->a:Lcom/pspdfkit/internal/r;

    .line 8
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->a:Lcom/pspdfkit/internal/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 9
    :cond_1
    :try_start_1
    new-instance v1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v2, "PSPDFKit must be initialized before trying to create intents!"

    invoke-direct {v1, v2}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized l()Lcom/pspdfkit/listeners/LocalizationListener;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->i:Lcom/pspdfkit/listeners/LocalizationListener;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/listeners/DefaultLocalizationListener;

    invoke-direct {v1}, Lcom/pspdfkit/listeners/DefaultLocalizationListener;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->i:Lcom/pspdfkit/listeners/LocalizationListener;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->i:Lcom/pspdfkit/listeners/LocalizationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized m()Lcom/pspdfkit/internal/dj;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->e:Lcom/pspdfkit/internal/dj;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/dj;

    invoke-direct {v1}, Lcom/pspdfkit/internal/dj;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->e:Lcom/pspdfkit/internal/dj;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->e:Lcom/pspdfkit/internal/dj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static n()Lcom/pspdfkit/internal/mj;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/gj;->o:Lcom/pspdfkit/internal/mj;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/mj$-CC;->a()Lcom/pspdfkit/internal/mj;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/gj;->o:Lcom/pspdfkit/internal/mj;

    .line 4
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/gj;->o:Lcom/pspdfkit/internal/mj;

    return-object v0
.end method

.method public static declared-synchronized o()Z
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-boolean v1, Lcom/pspdfkit/internal/gj;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized p()Lcom/pspdfkit/internal/fn;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->h:Lcom/pspdfkit/internal/fn;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/fn;

    invoke-direct {v1}, Lcom/pspdfkit/internal/fn;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->h:Lcom/pspdfkit/internal/fn;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->h:Lcom/pspdfkit/internal/fn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized q()Lcom/pspdfkit/internal/lp;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->l:Lcom/pspdfkit/internal/lp;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/lp;

    invoke-direct {v1}, Lcom/pspdfkit/internal/lp;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->l:Lcom/pspdfkit/internal/lp;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->l:Lcom/pspdfkit/internal/lp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized r()Lcom/pspdfkit/internal/fr;
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->u:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 5
    sget-object v2, Lcom/pspdfkit/internal/gj;->s:Lcom/pspdfkit/internal/fr;

    if-nez v2, :cond_0

    .line 6
    new-instance v2, Lcom/pspdfkit/internal/fr;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/fr;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/pspdfkit/internal/gj;->s:Lcom/pspdfkit/internal/fr;

    .line 8
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->s:Lcom/pspdfkit/internal/fr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 9
    :cond_1
    :try_start_1
    new-instance v1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v2, "PSPDFKit must be initialized before working with shape detector!"

    invoke-direct {v1, v2}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized s()Lcom/pspdfkit/signatures/SignatureBitmapStorage;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->t:Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    invoke-direct {v1}, Lcom/pspdfkit/signatures/SignatureBitmapStorage;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->t:Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->t:Lcom/pspdfkit/signatures/SignatureBitmapStorage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized t()Lcom/pspdfkit/internal/tr;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->q:Lcom/pspdfkit/internal/tr;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/tr;

    invoke-direct {v1}, Lcom/pspdfkit/internal/tr;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->q:Lcom/pspdfkit/internal/tr;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->q:Lcom/pspdfkit/internal/tr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized u()Lcom/pspdfkit/internal/mt;
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->m:Lcom/pspdfkit/internal/mt;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/mt;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/mt;-><init>(Ljava/util/List;)V

    sput-object v1, Lcom/pspdfkit/internal/gj;->m:Lcom/pspdfkit/internal/mt;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->m:Lcom/pspdfkit/internal/mt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized v()Lcom/pspdfkit/internal/du;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->c:Lcom/pspdfkit/internal/u;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/u;

    invoke-direct {v1}, Lcom/pspdfkit/internal/u;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->c:Lcom/pspdfkit/internal/u;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->c:Lcom/pspdfkit/internal/u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized w()Lcom/pspdfkit/internal/gv;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->n:Lcom/pspdfkit/internal/jo;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/jo;

    invoke-direct {v1}, Lcom/pspdfkit/internal/jo;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/gj;->n:Lcom/pspdfkit/internal/jo;

    .line 4
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->n:Lcom/pspdfkit/internal/jo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized x()V
    .locals 3

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->g:Lcom/pspdfkit/internal/q;

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/q;->a()V

    .line 4
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->y()V

    .line 5
    sget-object v1, Lcom/pspdfkit/internal/gj;->b:Lcom/pspdfkit/internal/yl;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yl;->a()V

    .line 7
    sput-object v2, Lcom/pspdfkit/internal/gj;->b:Lcom/pspdfkit/internal/yl;

    .line 9
    :cond_1
    sget-object v1, Lcom/pspdfkit/internal/gj;->f:Lcom/pspdfkit/internal/n4;

    if-eqz v1, :cond_2

    .line 10
    invoke-virtual {v1}, Lcom/pspdfkit/internal/n4;->a()V

    .line 11
    sput-object v2, Lcom/pspdfkit/internal/gj;->f:Lcom/pspdfkit/internal/n4;

    .line 13
    :cond_2
    sget-object v1, Lcom/pspdfkit/internal/gj;->o:Lcom/pspdfkit/internal/mj;

    if-eqz v1, :cond_3

    .line 14
    invoke-interface {v1}, Lcom/pspdfkit/internal/mj;->clear()V

    .line 15
    sput-object v2, Lcom/pspdfkit/internal/gj;->o:Lcom/pspdfkit/internal/mj;

    .line 17
    :cond_3
    sget-object v1, Lcom/pspdfkit/internal/gj;->t:Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    if-eqz v1, :cond_4

    .line 18
    invoke-virtual {v1}, Lcom/pspdfkit/signatures/SignatureBitmapStorage;->release()V

    .line 19
    sput-object v2, Lcom/pspdfkit/internal/gj;->t:Lcom/pspdfkit/signatures/SignatureBitmapStorage;

    .line 21
    :cond_4
    sput-object v2, Lcom/pspdfkit/internal/gj;->k:Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    .line 22
    sput-object v2, Lcom/pspdfkit/internal/gj;->l:Lcom/pspdfkit/internal/lp;

    .line 23
    sput-object v2, Lcom/pspdfkit/internal/gj;->u:Landroid/content/Context;

    .line 24
    sput-object v2, Lcom/pspdfkit/internal/gj;->v:Ljava/lang/ref/WeakReference;

    .line 25
    sget-object v1, Lcom/pspdfkit/internal/gj;->p:Lcom/pspdfkit/internal/b0;

    if-eqz v1, :cond_5

    .line 26
    invoke-virtual {v1}, Lcom/pspdfkit/internal/b0;->e()V

    .line 27
    sput-object v2, Lcom/pspdfkit/internal/gj;->p:Lcom/pspdfkit/internal/b0;

    .line 29
    :cond_5
    sput-object v2, Lcom/pspdfkit/internal/gj;->q:Lcom/pspdfkit/internal/tr;

    .line 30
    sput-object v2, Lcom/pspdfkit/internal/gj;->m:Lcom/pspdfkit/internal/mt;

    const/4 v1, 0x0

    .line 31
    sput-boolean v1, Lcom/pspdfkit/internal/gj;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized y()V
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/gj;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/gj;->d:Lcom/pspdfkit/internal/hb;

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hb;->e()V

    const/4 v1, 0x0

    .line 3
    sput-object v1, Lcom/pspdfkit/internal/gj;->d:Lcom/pspdfkit/internal/hb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

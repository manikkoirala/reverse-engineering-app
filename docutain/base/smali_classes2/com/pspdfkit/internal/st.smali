.class public final Lcom/pspdfkit/internal/st;
.super Lcom/pspdfkit/internal/tt;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/st$a;,
        Lcom/pspdfkit/internal/st$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/st$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/iv;

.field private final b:Lcom/pspdfkit/internal/p;

.field private final c:Lcom/pspdfkit/internal/bd;

.field private final d:F

.field private e:Lcom/pspdfkit/internal/fj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/st$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/st$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/st;->Companion:Lcom/pspdfkit/internal/st$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/p;Lcom/pspdfkit/internal/bd;FLcom/pspdfkit/internal/fj;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x1f

    const/16 v1, 0x1f

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/st$a;->a:Lcom/pspdfkit/internal/st$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/st$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/tt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/st;->a:Lcom/pspdfkit/internal/iv;

    iput-object p3, p0, Lcom/pspdfkit/internal/st;->b:Lcom/pspdfkit/internal/p;

    iput-object p4, p0, Lcom/pspdfkit/internal/st;->c:Lcom/pspdfkit/internal/bd;

    iput p5, p0, Lcom/pspdfkit/internal/st;->d:F

    iput-object p6, p0, Lcom/pspdfkit/internal/st;->e:Lcom/pspdfkit/internal/fj;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/st;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/iv$a;->a:Lcom/pspdfkit/internal/iv$a;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/st;->a:Lcom/pspdfkit/internal/iv;

    const/4 v2, 0x0

    .line 3
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/p;->Companion:Lcom/pspdfkit/internal/p$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/st;->b:Lcom/pspdfkit/internal/p;

    const/4 v2, 0x1

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/bd$a;->a:Lcom/pspdfkit/internal/bd$a;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/st;->c:Lcom/pspdfkit/internal/bd;

    const/4 v2, 0x2

    .line 5
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget v0, p0, Lcom/pspdfkit/internal/st;->d:F

    const/4 v1, 0x3

    invoke-interface {p1, p2, v1, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeFloatElement(Lkotlinx/serialization/descriptors/SerialDescriptor;IF)V

    sget-object v0, Lcom/pspdfkit/internal/fj$a;->a:Lcom/pspdfkit/internal/fj$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/st;->e:Lcom/pspdfkit/internal/fj;

    const/4 v1, 0x4

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/iv;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/st;->a:Lcom/pspdfkit/internal/iv;

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/p;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/st;->b:Lcom/pspdfkit/internal/p;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/bd;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/st;->c:Lcom/pspdfkit/internal/bd;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/fj;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/st;->e:Lcom/pspdfkit/internal/fj;

    return-object v0
.end method

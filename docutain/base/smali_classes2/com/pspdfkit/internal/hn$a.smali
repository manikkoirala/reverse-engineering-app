.class final Lcom/pspdfkit/internal/hn$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/activity/result/ActivityResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/hn;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroidx/activity/result/ActivityResultCallback<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/hn;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/hn;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityResult(Ljava/lang/Object;)V
    .locals 7

    .line 1
    check-cast p1, Ljava/util/Map;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    invoke-static {v0}, Lcom/pspdfkit/internal/hn;->a(Lcom/pspdfkit/internal/hn;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "permissions"

    const-string v3, "grantResults"

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_4

    .line 3
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v6, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    .line 4
    iget-object v6, v6, Lcom/pspdfkit/internal/hn;->d:[Ljava/lang/String;

    if-eqz v6, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v6, v1

    .line 6
    :goto_0
    array-length v6, v6

    if-ne v0, v6, :cond_7

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 88
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-eqz p1, :cond_7

    goto :goto_3

    .line 91
    :cond_4
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_4

    .line 174
    :cond_5
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 175
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_3
    const/4 p1, 0x1

    goto :goto_5

    :cond_7
    :goto_4
    const/4 p1, 0x0

    :goto_5
    if-eqz p1, :cond_8

    .line 179
    iget-object p1, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    invoke-static {p1, v4}, Lcom/pspdfkit/internal/hn;->a(Lcom/pspdfkit/internal/hn;Z)V

    goto :goto_7

    .line 186
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    .line 187
    iget-object v0, p1, Lcom/pspdfkit/internal/hn;->d:[Ljava/lang/String;

    if-eqz v0, :cond_9

    move-object v1, v0

    goto :goto_6

    .line 188
    :cond_9
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 189
    :goto_6
    invoke-static {v1}, Lkotlin/collections/ArraysKt;->first([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_a

    .line 190
    iget-object p1, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/hn;->a(Lcom/pspdfkit/internal/hn;Landroid/content/Context;)V

    .line 193
    :cond_a
    iget-object p1, p0, Lcom/pspdfkit/internal/hn$a;->a:Lcom/pspdfkit/internal/hn;

    invoke-static {p1, v5}, Lcom/pspdfkit/internal/hn;->a(Lcom/pspdfkit/internal/hn;Z)V

    :goto_7
    return-void
.end method

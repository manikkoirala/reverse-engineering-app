.class final Lcom/pspdfkit/internal/c1$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/c1;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/c1;

.field final synthetic b:Lcom/pspdfkit/document/PdfDocument;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/c1;Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/c1$b;->a:Lcom/pspdfkit/internal/c1;

    iput-object p2, p0, Lcom/pspdfkit/internal/c1$b;->b:Lcom/pspdfkit/document/PdfDocument;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 1
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c1$b;->a:Lcom/pspdfkit/internal/c1;

    iget-object v1, p0, Lcom/pspdfkit/internal/c1$b;->b:Lcom/pspdfkit/document/PdfDocument;

    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    invoke-interface {v1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 5
    sget-object v1, Lcom/pspdfkit/internal/d1;->a:Lcom/pspdfkit/internal/d1;

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Observable;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 14
    new-instance v1, Lcom/pspdfkit/internal/e1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/e1;-><init>(Lcom/pspdfkit/internal/c1;)V

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 18
    new-instance v1, Lcom/pspdfkit/internal/f1;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/f1;-><init>(Lcom/pspdfkit/internal/c1;)V

    invoke-virtual {p1, v1}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Observable;->toList()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const-string v0, "private fun getAnnotatio\u2026          .toList()\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.class final Lcom/pspdfkit/internal/zf$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zf$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:[Lcom/pspdfkit/utils/Size;

.field private final b:[B

.field private final c:[B

.field private final d:[Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/zf$c;)[Lcom/pspdfkit/utils/Size;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zf$c;->a:[Lcom/pspdfkit/utils/Size;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/zf$c;)[B
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zf$c;->c:[B

    return-object p0
.end method

.method private constructor <init>([Lcom/pspdfkit/utils/Size;[B[B[Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/zf$c;->a:[Lcom/pspdfkit/utils/Size;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/zf$c;->b:[B

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/zf$c;->c:[B

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/zf$c;->d:[Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>([Lcom/pspdfkit/utils/Size;[B[B[Ljava/lang/String;Lcom/pspdfkit/internal/zf$c-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/zf$c;-><init>([Lcom/pspdfkit/utils/Size;[B[B[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getPageLabel(IZ)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$c;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    add-int/lit8 p1, p1, 0x1

    .line 2
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    return-object v0
.end method

.method public final getPageRotation(I)B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$c;->b:[B

    aget-byte p1, v0, p1

    return p1
.end method

.method public final getPageSize(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$c;->a:[Lcom/pspdfkit/utils/Size;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public final getRotationOffset(I)B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$c;->c:[B

    aget-byte p1, v0, p1

    return p1
.end method

.class public final Lcom/pspdfkit/internal/qa$a;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/qa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/ui/dialog/signatures/f;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ui/dialog/signatures/f;)V
    .locals 1

    const-string v0, "electronicSignatureLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/qa$a;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    .line 8
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 14
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/pa;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/qa$a;->a:Lcom/pspdfkit/internal/ui/dialog/signatures/f;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/dialog/signatures/f;->setListener(Lcom/pspdfkit/internal/pa;)V

    return-void
.end method

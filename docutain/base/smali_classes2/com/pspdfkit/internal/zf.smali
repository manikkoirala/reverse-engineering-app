.class public Lcom/pspdfkit/internal/zf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/PdfDocument;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/zf$f;,
        Lcom/pspdfkit/internal/zf$c;,
        Lcom/pspdfkit/internal/zf$d;,
        Lcom/pspdfkit/internal/zf$e;
    }
.end annotation


# static fields
.field private static final J:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;


# instance fields
.field private A:[I

.field protected B:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;"
        }
    .end annotation
.end field

.field private volatile C:Z

.field private volatile D:Z

.field private E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

.field private final F:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/zf$f;",
            ">;"
        }
    .end annotation
.end field

.field private final G:Z

.field private H:Z

.field private final I:Lcom/pspdfkit/internal/zm;

.field public final a:Lcom/pspdfkit/internal/aa;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/pspdfkit/internal/ym;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Lcom/pspdfkit/internal/qf;

.field private final e:Lcom/pspdfkit/internal/rf;

.field private final f:Lcom/pspdfkit/internal/uf;

.field private final g:Lcom/pspdfkit/internal/ta;

.field private final h:Lcom/pspdfkit/internal/g9;

.field private final i:Lcom/pspdfkit/internal/ba;

.field private final j:Lio/reactivex/rxjava3/core/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/signatures/DocumentSignatureInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Lcom/pspdfkit/internal/ig;

.field private final l:Ljava/util/concurrent/locks/ReentrantLock;

.field private final m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private final n:Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

.field private final o:Lcom/pspdfkit/internal/jni/NativeResourceManager;

.field private final p:Lcom/pspdfkit/document/DocumentSource;

.field protected q:I

.field r:Lcom/pspdfkit/internal/jni/NativeDocument;

.field private s:Lcom/pspdfkit/internal/zf$e;

.field private final t:Lio/reactivex/rxjava3/core/Completable;

.field private final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Lcom/pspdfkit/document/PdfVersion;

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation
.end field

.field private z:Z


# direct methods
.method public static synthetic $r8$lambda$2QFM63PbuwJr5OgvYXMg71TNTzs(Lcom/pspdfkit/internal/zf;Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Void;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->a(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Void;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$XOISreqgEkALfyyJVdgGXpJqk2s(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$buqs_Rl-M7Ekis0U1533EGHiLzg(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->b(Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$jA480YwDJw_Yt04ziOf-FUeYcuU(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/signatures/DocumentSignatureInfo;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/zf;->p()Lcom/pspdfkit/signatures/DocumentSignatureInfo;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$p_cNg5L7PGkjdI-r_qgTJ8oCI4o(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/signatures/DocumentSignatureInfo;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/zf;->q()Lcom/pspdfkit/signatures/DocumentSignatureInfo;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$vtoO8CLxyArY24HxRBZ1V66c2Uk(Lcom/pspdfkit/internal/zf;Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->b(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputs(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/zf$e;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->build()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/zf;->J:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocument;ZLcom/pspdfkit/internal/z7;Lcom/pspdfkit/document/DocumentSource;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/zf$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/zf$a;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->b:Ljava/util/Map;

    .line 37
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    .line 42
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 70
    new-instance v0, Lcom/pspdfkit/internal/zf$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/zf$b;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 92
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->a()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->cache()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->t:Lio/reactivex/rxjava3/core/Completable;

    .line 120
    const-class v0, Lcom/pspdfkit/document/DocumentPermissions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->C:Z

    .line 124
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->D:Z

    .line 131
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    const/4 v0, 0x0

    .line 143
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->H:Z

    .line 146
    new-instance v1, Lcom/pspdfkit/internal/zm;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/zm;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->I:Lcom/pspdfkit/internal/zm;

    const/4 v1, 0x0

    .line 155
    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    .line 156
    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 157
    iput-boolean p2, p0, Lcom/pspdfkit/internal/zf;->c:Z

    .line 158
    iput-object p4, p0, Lcom/pspdfkit/internal/zf;->p:Lcom/pspdfkit/document/DocumentSource;

    .line 160
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->G:Z

    .line 161
    new-instance p2, Lcom/pspdfkit/internal/aa;

    invoke-direct {p2, v0}, Lcom/pspdfkit/internal/aa;-><init>(Z)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zf;->a:Lcom/pspdfkit/internal/aa;

    .line 164
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getDocumentProviders()Ljava/util/ArrayList;

    move-result-object p1

    .line 165
    new-instance p2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p4

    invoke-direct {p2, p4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    .line 166
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    .line 167
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->getDataProvider()Lcom/pspdfkit/internal/jni/NativeDataProvider;

    move-result-object p4

    if-nez p4, :cond_0

    .line 169
    new-instance v0, Ljava/io/File;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->getFilePath()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p2

    goto :goto_1

    :cond_0
    move-object p2, v1

    :goto_1
    if-eqz p4, :cond_1

    .line 171
    new-instance v0, Lcom/pspdfkit/internal/uj;

    invoke-direct {v0, p4}, Lcom/pspdfkit/internal/uj;-><init>(Lcom/pspdfkit/internal/jni/NativeDataProvider;)V

    goto :goto_2

    :cond_1
    move-object v0, v1

    .line 172
    :goto_2
    iget-object p4, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    new-instance v2, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v2, p2, v0, v1, v1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;Lcom/pspdfkit/document/providers/DataProvider;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_2
    new-instance p1, Lcom/pspdfkit/internal/zf$d;

    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-direct {p1, p2, v1}, Lcom/pspdfkit/internal/zf$d;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/zf$d-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    .line 178
    invoke-direct {p0}, Lcom/pspdfkit/internal/zf;->n()V

    .line 179
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;->create()Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->n:Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    .line 180
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->create()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->o:Lcom/pspdfkit/internal/jni/NativeResourceManager;

    .line 181
    new-instance p1, Lcom/pspdfkit/internal/ig;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/ig;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->k:Lcom/pspdfkit/internal/ig;

    .line 183
    invoke-virtual {p3, p0}, Lcom/pspdfkit/internal/z7;->a(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    .line 184
    invoke-static {p0}, Lcom/pspdfkit/internal/z7;->b(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/rf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    .line 185
    invoke-static {p0}, Lcom/pspdfkit/internal/z7;->f(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/uf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    .line 186
    invoke-static {p0}, Lcom/pspdfkit/internal/z7;->e(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/ta;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->g:Lcom/pspdfkit/internal/ta;

    .line 187
    invoke-virtual {p3, p0}, Lcom/pspdfkit/internal/z7;->c(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/g9;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    .line 188
    invoke-virtual {p3, p0}, Lcom/pspdfkit/internal/z7;->d(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/ba;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->i:Lcom/pspdfkit/internal/ba;

    .line 191
    new-instance p1, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda3;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const/4 p2, 0x5

    .line 192
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 193
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->cache()Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->j:Lio/reactivex/rxjava3/core/Single;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Lcom/pspdfkit/internal/h5;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;",
            "Lcom/pspdfkit/internal/h5;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    new-instance v0, Lcom/pspdfkit/internal/zf$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/zf$a;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->b:Ljava/util/Map;

    .line 230
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    .line 235
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 263
    new-instance v0, Lcom/pspdfkit/internal/zf$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/zf$b;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 285
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->a()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->cache()Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->t:Lio/reactivex/rxjava3/core/Completable;

    .line 313
    const-class v0, Lcom/pspdfkit/document/DocumentPermissions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    const/4 v0, 0x1

    .line 315
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->C:Z

    .line 317
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->D:Z

    .line 324
    new-instance v1, Lcom/pspdfkit/internal/nh;

    invoke-direct {v1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    const/4 v1, 0x0

    .line 336
    iput-boolean v1, p0, Lcom/pspdfkit/internal/zf;->H:Z

    .line 339
    new-instance v2, Lcom/pspdfkit/internal/zm;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/zm;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/zf;->I:Lcom/pspdfkit/internal/zm;

    .line 394
    iput-object p1, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    .line 395
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->c:Z

    const/4 v2, 0x0

    .line 396
    iput-object v2, p0, Lcom/pspdfkit/internal/zf;->p:Lcom/pspdfkit/document/DocumentSource;

    .line 398
    iput-boolean p3, p0, Lcom/pspdfkit/internal/zf;->G:Z

    .line 399
    new-instance v3, Lcom/pspdfkit/internal/aa;

    invoke-direct {v3, p3}, Lcom/pspdfkit/internal/aa;-><init>(Z)V

    iput-object v3, p0, Lcom/pspdfkit/internal/zf;->a:Lcom/pspdfkit/internal/aa;

    .line 401
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 402
    invoke-direct {p0}, Lcom/pspdfkit/internal/zf;->r()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 405
    new-instance v5, Lcom/pspdfkit/internal/zf$d;

    invoke-direct {v5, p3, v2}, Lcom/pspdfkit/internal/zf$d;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/zf$d-IA;)V

    iput-object v5, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    .line 407
    invoke-direct {p0}, Lcom/pspdfkit/internal/zf;->n()V

    .line 408
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 409
    new-instance p3, Ljava/lang/StringBuilder;

    const-string v2, "Document open took "

    invoke-direct {p3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v5, v3

    invoke-virtual {p3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " ms."

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.Document"

    invoke-static {v3, p3, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;->create()Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->n:Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    .line 412
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->create()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->o:Lcom/pspdfkit/internal/jni/NativeResourceManager;

    .line 413
    new-instance p3, Lcom/pspdfkit/internal/ig;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ig;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->k:Lcom/pspdfkit/internal/ig;

    .line 415
    new-instance p3, Lcom/pspdfkit/internal/r1;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/r1;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    .line 416
    invoke-static {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderFactory;->fromInternalDocument(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/rf;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    .line 417
    invoke-static {p0}, Lcom/pspdfkit/forms/FormProviderFactory;->createFromInternalDocument(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/uf;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    .line 418
    new-instance p3, Lcom/pspdfkit/internal/ta;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/ta;-><init>(Lcom/pspdfkit/internal/zf;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->g:Lcom/pspdfkit/internal/ta;

    .line 419
    new-instance p3, Lcom/pspdfkit/internal/g9;

    invoke-direct {p3, p0, v0}, Lcom/pspdfkit/internal/g9;-><init>(Lcom/pspdfkit/internal/zf;Z)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    .line 420
    new-instance p3, Lcom/pspdfkit/internal/ba;

    invoke-direct {p3, p0, v0}, Lcom/pspdfkit/internal/ba;-><init>(Lcom/pspdfkit/internal/zf;Z)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->i:Lcom/pspdfkit/internal/ba;

    .line 423
    new-instance p3, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {p3}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    const/4 v2, 0x5

    .line 424
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {p3, v2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    .line 425
    invoke-virtual {p3}, Lio/reactivex/rxjava3/core/Single;->cache()Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->j:Lio/reactivex/rxjava3/core/Single;

    if-eqz p2, :cond_1

    .line 427
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    if-ne p3, v0, :cond_1

    .line 428
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/document/DocumentSource;

    invoke-static {p3}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->isCheckpointSupported(Lcom/pspdfkit/document/DocumentSource;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 432
    new-instance p3, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getCheckpointFile()Ljava/io/File;

    move-result-object p1

    invoke-direct {p3, p0, p1, p2}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;-><init>(Lcom/pspdfkit/internal/zf;Ljava/io/File;Lcom/pspdfkit/internal/h5;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/zf;->E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    goto :goto_0

    .line 433
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Checkpoint is not available for documents that have multiple providers or protected."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Z)Lcom/pspdfkit/internal/zf;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/pspdfkit/internal/zf;-><init>(Ljava/util/List;Lcom/pspdfkit/internal/h5;Z)V

    return-object v0
.end method

.method public static a(Ljava/util/List;Lcom/pspdfkit/internal/h5;Z)Lcom/pspdfkit/internal/zf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;",
            "Lcom/pspdfkit/internal/h5;",
            "Z)",
            "Lcom/pspdfkit/internal/zf;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/zf;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/zf;-><init>(Ljava/util/List;Lcom/pspdfkit/internal/h5;Z)V

    return-object v0
.end method

.method private synthetic a(Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 58
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->c(Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->c(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private a(I)V
    .locals 5

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/zf;->q:I

    if-ltz p1, :cond_0

    if-ge p1, v0, :cond_0

    return-void

    .line 4
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    aput-object p1, v3, v0

    const-string p1, "Invalid page number passed: %d. Page number has to be in the interval [0, %d)"

    .line 9
    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private synthetic b(Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private synthetic b(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->d(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private d(I)Lcom/pspdfkit/internal/ym;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 56
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ym;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 66
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    .line 67
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 68
    new-instance v1, Lcom/pspdfkit/internal/ym;

    invoke-direct {v1, v0, p1, p1}, Lcom/pspdfkit/internal/ym;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;II)V

    .line 69
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 73
    throw p1

    .line 74
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Document has already been closed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private n()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 2
    iget-boolean v1, p0, Lcom/pspdfkit/internal/zf;->C:Z

    if-eqz v1, :cond_0

    .line 3
    sget-object v1, Lcom/pspdfkit/internal/jni/NativeTextParserOptions;->FILTER_WATERMARKS:Lcom/pspdfkit/internal/jni/NativeTextParserOptions;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    goto :goto_0

    .line 4
    :cond_0
    const-class v1, Lcom/pspdfkit/internal/jni/NativeTextParserOptions;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 5
    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->setTextParserOptions(Ljava/util/EnumSet;)V

    .line 9
    iget-boolean v1, p0, Lcom/pspdfkit/internal/zf;->D:Z

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->enableAutomaticLinkExtraction(Z)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageCount()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/zf;->q:I

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->A:[I

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 12
    :goto_1
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->A:[I

    iget-object v3, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getProviderPageOffset(I)I

    move-result v3

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 14
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getTitle()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_8

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    const-string v4, "<this>"

    .line 17
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 45
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    goto :goto_2

    .line 46
    :cond_2
    invoke-static {v1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    :goto_2
    move-object v1, v2

    :goto_3
    if-nez v1, :cond_4

    goto :goto_5

    .line 47
    :cond_4
    invoke-static {v1}, Lcom/pspdfkit/internal/r2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x2e

    .line 48
    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    if-ge v4, v3, :cond_5

    goto :goto_4

    .line 49
    :cond_5
    invoke-virtual {v1, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 50
    :goto_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_8

    goto :goto_5

    .line 51
    :cond_6
    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/document/providers/DataProvider;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_7
    :goto_5
    move-object v1, v2

    .line 52
    :cond_8
    :goto_6
    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->w:Ljava/lang/String;

    .line 56
    iget v1, p0, Lcom/pspdfkit/internal/zf;->q:I

    sub-int/2addr v1, v3

    :goto_7
    if-ltz v1, :cond_9

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    add-int/lit8 v1, v1, -0x1

    goto :goto_7

    .line 63
    :cond_9
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->hasOutline()Z

    move-result v1

    iput-boolean v1, p0, Lcom/pspdfkit/internal/zf;->z:Z

    if-nez v1, :cond_a

    .line 65
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->y:Ljava/util/List;

    .line 69
    :cond_a
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPermissions()Ljava/util/EnumSet;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    .line 70
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPdfVersion()Lcom/pspdfkit/internal/jni/NativePDFVersion;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativePDFVersion;)Lcom/pspdfkit/document/PdfVersion;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/zf;->x:Lcom/pspdfkit/document/PdfVersion;

    .line 72
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 73
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getUid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->v:Ljava/lang/String;

    goto :goto_8

    .line 76
    :cond_b
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/document/providers/DataProvider;->getUid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->v:Ljava/lang/String;

    :goto_8
    return-void
.end method

.method private synthetic p()Lcom/pspdfkit/signatures/DocumentSignatureInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/z9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/z9;-><init>(Lcom/pspdfkit/internal/zf;)V

    return-object v0
.end method

.method private synthetic q()Lcom/pspdfkit/signatures/DocumentSignatureInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/z9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/z9;-><init>(Lcom/pspdfkit/internal/zf;)V

    return-object v0
.end method

.method private r()Lcom/pspdfkit/internal/jni/NativeDocument;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v1

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/document/DocumentSource;

    .line 4
    invoke-virtual {v3}, Lcom/pspdfkit/document/DocumentSource;->toDataDescriptor()Lcom/pspdfkit/internal/jni/NativeDataDescriptor;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->i()Lcom/pspdfkit/internal/oq;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/oq;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/nq;

    move-result-object v1

    .line 9
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 10
    :try_start_0
    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->open(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;->getHasError()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;->getErrorCode()Lcom/pspdfkit/internal/jni/NativeDocumentOpenErrorCode;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeDocumentOpenErrorCode;->ERROR_PASSWORD:Lcom/pspdfkit/internal/jni/NativeDocumentOpenErrorCode;

    if-eq v2, v3, :cond_4

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;->getErrorString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Invalid content signature"

    .line 16
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "Content signatures feature is not available for this license."

    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "No content signature provided."

    .line 23
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidSignatureException;

    const-string v2, "Content signature was missing. Your PSPDFKit license can only be used with signed documents."

    invoke-direct {v0, v2}, Lcom/pspdfkit/exceptions/InvalidSignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while loading PdfDocument: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 30
    :cond_2
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidSignatureException;

    const-string v2, "Content signatures are not supported by your PSPDFKit license. Please open the document without providing a signature, or upgrade your PSPDFKit license."

    invoke-direct {v0, v2}, Lcom/pspdfkit/exceptions/InvalidSignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_3
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidSignatureException;

    const-string v2, "Invalid document signature."

    invoke-direct {v0, v2}, Lcom/pspdfkit/exceptions/InvalidSignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_4
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPasswordException;

    const-string v2, "Invalid password for document."

    invoke-direct {v0, v2}, Lcom/pspdfkit/exceptions/InvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentOpenResult;->getDocument()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    .line 52
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 53
    throw v0
.end method


# virtual methods
.method public final a(ILandroid/graphics/PointF;F)Landroid/graphics/RectF;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 40
    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/ym;->a(Landroid/graphics/PointF;F)Landroid/graphics/RectF;

    move-result-object p1

    return-object p1
.end method

.method public final a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    .line 60
    iget-boolean p1, p0, Lcom/pspdfkit/internal/zf;->c:Z

    if-nez p1, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p1

    .line 64
    iget-object v3, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_2

    iget-object v3, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    .line 65
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v3}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v3

    if-nez v3, :cond_1

    instance-of v3, p1, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-eqz v3, :cond_2

    check-cast p1, Lcom/pspdfkit/document/providers/WritableDataProvider;

    .line 67
    invoke-interface {p1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->supportsAppending()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v1, 0x1

    .line 68
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPdfVersion()Lcom/pspdfkit/internal/jni/NativePDFVersion;

    move-result-object p1

    .line 69
    new-instance v2, Lcom/pspdfkit/document/DocumentSaveOptions;

    iget-object v3, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    .line 71
    invoke-virtual {v3}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object v3

    .line 73
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativePDFVersion;)Lcom/pspdfkit/document/PdfVersion;

    move-result-object p1

    invoke-direct {v2, v0, v3, v1, p1}, Lcom/pspdfkit/document/DocumentSaveOptions;-><init>(Ljava/lang/String;Ljava/util/EnumSet;ZLcom/pspdfkit/document/PdfVersion;)V

    return-object v2
.end method

.method public final a(IFFF)Lcom/pspdfkit/internal/jni/NativeTextRange;
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 56
    invoke-virtual {p1, p2, p3, p4}, Lcom/pspdfkit/internal/ym;->a(FFF)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/TextBlock;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 43
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 45
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/datastructures/TextBlock;

    .line 46
    iget v0, v0, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    .line 47
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/datastructures/TextBlock;

    .line 48
    iget v2, v2, Lcom/pspdfkit/datastructures/TextBlock;->pageIndex:I

    if-ne v2, v0, :cond_1

    goto :goto_0

    .line 49
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "All text blocks must belong to the same page!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 53
    :cond_2
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ym;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILandroid/graphics/RectF;Z)Ljava/util/List;
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 38
    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/ym;->a(Landroid/graphics/RectF;Z)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILjava/util/List;)Ljava/util/List;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 42
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ym;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/util/SparseIntArray;)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 74
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_8

    .line 75
    invoke-virtual {v1, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    .line 76
    invoke-virtual {v1, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    .line 77
    invoke-direct {v0, v4}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 78
    iget-object v6, v0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v6, v4}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object v6

    .line 86
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotationOffset()B

    move-result v7

    add-int/2addr v7, v5

    const/4 v8, 0x2

    rem-int/2addr v7, v8

    const/4 v9, 0x1

    if-ne v7, v9, :cond_0

    .line 89
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getSize()Lcom/pspdfkit/utils/Size;

    move-result-object v7

    .line 90
    new-instance v10, Lcom/pspdfkit/utils/Size;

    iget v11, v7, Lcom/pspdfkit/utils/Size;->height:F

    iget v7, v7, Lcom/pspdfkit/utils/Size;->width:F

    invoke-direct {v10, v11, v7}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    goto :goto_1

    .line 93
    :cond_0
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getSize()Lcom/pspdfkit/utils/Size;

    move-result-object v10

    .line 96
    :goto_1
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getUntransformedBbox()Landroid/graphics/RectF;

    move-result-object v14

    .line 97
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotation()B

    move-result v7

    add-int/2addr v7, v5

    const/4 v11, 0x4

    rem-int/2addr v7, v11

    int-to-byte v7, v7

    .line 98
    new-instance v15, Landroid/graphics/Matrix;

    invoke-direct {v15}, Landroid/graphics/Matrix;-><init>()V

    const/16 v16, 0x6

    const/16 v17, 0x5

    const/16 v12, 0x9

    const/4 v13, 0x3

    const/high16 v20, 0x3f800000    # 1.0f

    const/16 v21, 0x0

    if-eqz v7, :cond_4

    const/high16 v22, -0x40800000    # -1.0f

    if-eq v7, v9, :cond_3

    if-eq v7, v8, :cond_2

    if-eq v7, v13, :cond_1

    goto/16 :goto_2

    :cond_1
    new-array v7, v12, [F

    aput v21, v7, v2

    aput v22, v7, v9

    .line 120
    iget v12, v14, Landroid/graphics/RectF;->top:F

    aput v12, v7, v8

    aput v20, v7, v13

    aput v21, v7, v11

    iget v8, v14, Landroid/graphics/RectF;->left:F

    neg-float v8, v8

    aput v8, v7, v17

    aput v21, v7, v16

    const/4 v8, 0x7

    aput v21, v7, v8

    const/16 v8, 0x8

    aput v20, v7, v8

    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->setValues([F)V

    goto :goto_2

    :cond_2
    new-array v7, v12, [F

    aput v22, v7, v2

    aput v21, v7, v9

    .line 121
    iget v12, v14, Landroid/graphics/RectF;->right:F

    aput v12, v7, v8

    aput v21, v7, v13

    aput v22, v7, v11

    iget v8, v14, Landroid/graphics/RectF;->top:F

    aput v8, v7, v17

    aput v21, v7, v16

    const/4 v8, 0x7

    aput v21, v7, v8

    const/16 v8, 0x8

    aput v20, v7, v8

    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->setValues([F)V

    goto :goto_2

    :cond_3
    new-array v7, v12, [F

    aput v21, v7, v2

    aput v20, v7, v9

    .line 122
    iget v12, v14, Landroid/graphics/RectF;->bottom:F

    neg-float v12, v12

    aput v12, v7, v8

    aput v22, v7, v13

    aput v21, v7, v11

    iget v8, v14, Landroid/graphics/RectF;->right:F

    aput v8, v7, v17

    aput v21, v7, v16

    const/4 v8, 0x7

    aput v21, v7, v8

    const/16 v8, 0x8

    aput v20, v7, v8

    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->setValues([F)V

    goto :goto_2

    :cond_4
    new-array v7, v12, [F

    aput v20, v7, v2

    aput v21, v7, v9

    .line 123
    iget v12, v14, Landroid/graphics/RectF;->left:F

    neg-float v12, v12

    aput v12, v7, v8

    aput v21, v7, v13

    aput v20, v7, v11

    iget v8, v14, Landroid/graphics/RectF;->bottom:F

    neg-float v8, v8

    aput v8, v7, v17

    aput v21, v7, v16

    const/4 v8, 0x7

    aput v21, v7, v8

    const/16 v8, 0x8

    aput v20, v7, v8

    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->setValues([F)V

    .line 145
    :goto_2
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 146
    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 148
    new-instance v8, Lcom/pspdfkit/internal/jni/NativePageInfo;

    .line 151
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getBbox()Landroid/graphics/RectF;

    move-result-object v13

    .line 153
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotation()B

    move-result v16

    int-to-byte v5, v5

    .line 157
    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getAllowAnnotationCreation()Z

    move-result v19

    move-object v11, v8

    move-object v12, v10

    move-object v6, v15

    move/from16 v15, v16

    move/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    invoke-direct/range {v11 .. v19}, Lcom/pspdfkit/internal/jni/NativePageInfo;-><init>(Lcom/pspdfkit/utils/Size;Landroid/graphics/RectF;Landroid/graphics/RectF;BBLandroid/graphics/Matrix;Landroid/graphics/Matrix;Z)V

    .line 158
    iget-object v6, v0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/jni/NativeDocument;->getDocumentProviders()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    .line 159
    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v7, v11, :cond_7

    .line 160
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    .line 161
    iget-object v12, v0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v12, v7}, Lcom/pspdfkit/internal/jni/NativeDocument;->getProviderPageOffset(I)I

    move-result v12

    .line 162
    invoke-virtual {v11}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->getPageCount()I

    move-result v13

    add-int/2addr v13, v12

    if-gt v12, v4, :cond_6

    if-ge v4, v13, :cond_6

    sub-int v6, v4, v12

    .line 165
    invoke-virtual {v11, v8, v6}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->setPageInfo(Lcom/pspdfkit/internal/jni/NativePageInfo;I)V

    .line 166
    iget-object v6, v0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    instance-of v7, v6, Lcom/pspdfkit/internal/zf$c;

    if-eqz v7, :cond_5

    .line 167
    check-cast v6, Lcom/pspdfkit/internal/zf$c;

    .line 168
    invoke-static {v6}, Lcom/pspdfkit/internal/zf$c;->-$$Nest$fgeta(Lcom/pspdfkit/internal/zf$c;)[Lcom/pspdfkit/utils/Size;

    move-result-object v7

    aput-object v10, v7, v4

    .line 169
    invoke-static {v6}, Lcom/pspdfkit/internal/zf$c;->-$$Nest$fgetc(Lcom/pspdfkit/internal/zf$c;)[B

    move-result-object v6

    aput-byte v5, v6, v4

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 170
    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 171
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    const-string v2, "Couldn\'t find document provider for given page index: %d"

    .line 172
    invoke-static {v3, v2, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173
    :cond_8
    iget-object v3, v0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeDocument;->clearPageCache()V

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/r1;->c()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->updateAnnotationTransforms()V

    .line 176
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 177
    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v4, v3}, Lcom/pspdfkit/internal/qf;->a(Ljava/util/Set;)Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 182
    :cond_9
    iget-object v1, v0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/zf$f;

    .line 183
    invoke-interface {v2}, Lcom/pspdfkit/internal/zf$f;->onPageRotationOffsetChanged()V

    goto :goto_5

    :cond_a
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf$f;)V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a(II)Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->cancelRenderProcess(II)Z

    move-result p1

    return p1
.end method

.method public final a(ILandroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z
    .locals 10

    move-object v1, p0

    .line 10
    iget-object v0, v1, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 12
    :try_start_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object v2

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    .line 13
    invoke-virtual/range {v2 .. v9}, Lcom/pspdfkit/internal/ym;->a(Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    iget-object v2, v1, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v2, v1, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 17
    throw v0
.end method

.method public final a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 20
    :try_start_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 21
    invoke-virtual {p1, p4, p2, p3}, Lcom/pspdfkit/internal/ym;->a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 24
    throw p1
.end method

.method public final a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/yl;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z
    .locals 7

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 27
    :try_start_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object v1

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->v:Ljava/lang/String;

    .line 29
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 30
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    aput-object p1, v3, v0

    const-string p1, "d[%s]p[%d]_"

    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    .line 31
    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/ym;->a(Landroid/graphics/Bitmap;Lcom/pspdfkit/internal/yl;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 35
    throw p1
.end method

.method public final b(I)I
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/zf$e;->getRotationOffset(I)B

    move-result p1

    return p1
.end method

.method public final b()Lcom/pspdfkit/internal/jni/NativeAnnotationManager;
    .locals 4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    new-instance v1, Lcom/pspdfkit/internal/s7;

    new-instance v2, Lcom/pspdfkit/document/providers/AssetDataProvider;

    const-string v3, "annotations.bfbs"

    .line 9
    invoke-static {v3}, Lcom/pspdfkit/internal/kb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/pspdfkit/document/providers/AssetDataProvider;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 10
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->create(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 14
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Could not initialize NativeAnnotationManager."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(II)Lcom/pspdfkit/internal/jni/NativeTextRange;
    .locals 1

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    const/4 v0, 0x1

    .line 4
    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/ym;->b(II)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lcom/pspdfkit/internal/zf$f;)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public c()Lcom/pspdfkit/internal/qf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    return-object v0
.end method

.method public final c(I)Lio/reactivex/rxjava3/core/Scheduler;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->a:Lcom/pspdfkit/internal/aa;

    iget-object v0, v0, Lcom/pspdfkit/internal/aa;->b:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "Saving document to "

    if-eqz p2, :cond_1

    .line 91
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    const-string v1, "PSPDFKit.Document"

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/rf;->prepareToSave()V

    .line 95
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/qf;->a()V

    const/4 v0, 0x1

    .line 97
    invoke-static {p2, p0, v0}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    move-result-object p2

    .line 98
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->mergeToFilePath(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    .line 102
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    .line 103
    :cond_0
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to save document."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    .line 106
    iget-object p2, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 107
    throw p1

    .line 108
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Save options must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c(Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->c:Z

    if-eqz v0, :cond_8

    if-eqz p1, :cond_7

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 9
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/rf;->prepareToSave()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/qf;->a()V

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->i()Lcom/pspdfkit/internal/oq;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/oq;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/nq;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 16
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    const/4 v2, 0x0

    invoke-static {p1, p0, v2}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->save(Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;)Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17
    :try_start_2
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 18
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;->ERROR:Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;

    if-eq p1, v0, :cond_5

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPermissions()Ljava/util/EnumSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPermissions()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPdfVersion()Lcom/pspdfkit/internal/jni/NativePDFVersion;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getCurrentPdfVersion()Lcom/pspdfkit/internal/jni/NativePDFVersion;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativePDFVersion;)Lcom/pspdfkit/document/PdfVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->x:Lcom/pspdfkit/document/PdfVersion;

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->e()V

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/rf;->markBookmarksAsSavedToDisk()V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    invoke-interface {v0}, Lcom/pspdfkit/internal/uf;->markFormAsSavedToDisk()V

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/f9;->d()V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->i:Lcom/pspdfkit/internal/ba;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/f9;->d()V

    .line 35
    iput-boolean v2, p0, Lcom/pspdfkit/internal/zf;->H:Z

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    if-eqz v0, :cond_2

    .line 37
    invoke-virtual {v0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->documentSavedSuccessfully()V

    .line 40
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/zf$f;

    .line 41
    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/zf$f;->onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V

    goto :goto_0

    .line 43
    :cond_3
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;->SAVED:Lcom/pspdfkit/internal/jni/NativeDocumentSaveResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-ne p1, v0, :cond_4

    const/4 v2, 0x1

    .line 50
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    .line 51
    :cond_5
    :try_start_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Failed to save document."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 52
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 53
    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 82
    :try_start_4
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/zf$f;

    .line 83
    invoke-interface {v1, p0, p1}, Lcom/pspdfkit/internal/zf$f;->onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 85
    :cond_6
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 87
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 88
    throw p1

    .line 89
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Save options must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 90
    :cond_8
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Document can\'t be saved."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public d(Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "saveOptions"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const/16 v0, 0xa

    .line 54
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final d()Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method public d(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p2, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->wasModified()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.Document"

    const-string v1, "Document not modified, not saving."

    .line 76
    invoke-static {v0, v1, p2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return p1

    .line 80
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->c(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    const/4 p1, 0x1

    return p1

    .line 81
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Save options must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final e()Lcom/pspdfkit/internal/uf;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    return-object v0
.end method

.method public final e(I)Ljava/util/ArrayList;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ym;->a(Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final f()Lcom/pspdfkit/document/DocumentSource;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->p:Lcom/pspdfkit/document/DocumentSource;

    return-object v0
.end method

.method public final f(I)Ljava/util/ArrayList;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ym;->a(Z)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final g(I)I
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 4
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->A:[I

    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget v2, v2, v1

    if-lt p1, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, -0x1

    :goto_1
    return v0
.end method

.method final g()Lcom/pspdfkit/internal/nh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/zf$f;",
            ">;"
        }
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    return-object v0
.end method

.method public bridge synthetic getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    return-object v0
.end method

.method public getBookmarkProvider()Lcom/pspdfkit/bookmarks/BookmarkProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    return-object v0
.end method

.method public final getCharIndexAt(IFF)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 2
    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/ym;->a(FF)I

    move-result p1

    return p1
.end method

.method public final getCheckpointer()Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    return-object v0
.end method

.method public final getDefaultDocumentSaveOptions()Lcom/pspdfkit/document/DocumentSaveOptions;
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    return-object v0
.end method

.method public final getDocumentSignatureInfo()Lcom/pspdfkit/signatures/DocumentSignatureInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->j:Lio/reactivex/rxjava3/core/Single;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/signatures/DocumentSignatureInfo;

    return-object v0
.end method

.method public final getDocumentSignatureInfoAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/signatures/DocumentSignatureInfo;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->j:Lio/reactivex/rxjava3/core/Single;

    return-object v0
.end method

.method public final getDocumentSource()Lcom/pspdfkit/document/DocumentSource;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    return-object v0
.end method

.method public final getDocumentSources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/DocumentSource;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEmbeddedFilesProvider()Lcom/pspdfkit/document/files/EmbeddedFilesProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->g:Lcom/pspdfkit/internal/ta;

    return-object v0
.end method

.method public final getFormProvider()Lcom/pspdfkit/forms/FormProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    return-object v0
.end method

.method public final getHashForDocumentRange(ILjava/util/List;Lcom/pspdfkit/signatures/HashAlgorithm;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/pspdfkit/signatures/HashAlgorithm;",
            ")[B"
        }
    .end annotation

    const/4 v0, 0x1

    if-ltz p1, :cond_3

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->A:[I

    array-length v1, v1

    if-ge p1, v1, :cond_3

    const-string v1, "range"

    const-string v2, "argumentName"

    .line 3
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 54
    invoke-static {p2, v1, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Document range must have even number of elements!"

    invoke-static {v1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    const-string v0, "hashAlgorithm"

    .line 57
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static {p3, v0, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 110
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getDocumentProviders()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 115
    invoke-static {p3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/signatures/HashAlgorithm;)Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;

    move-result-object p2

    .line 116
    invoke-static {p1, v0, p2}, Lcom/pspdfkit/internal/jni/NativePlatformDocumentDigester;->digestRangeOfDocument(Lcom/pspdfkit/internal/jni/NativeDocumentProvider;Ljava/util/ArrayList;Lcom/pspdfkit/internal/jni/NativeHashAlgorithm;)Lcom/pspdfkit/internal/jni/NativePlatformDocumentDigesterResult;

    move-result-object p1

    .line 121
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePlatformDocumentDigesterResult;->getError()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_2

    .line 125
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePlatformDocumentDigesterResult;->getDocumentDigest()[B

    move-result-object p1

    if-eqz p1, :cond_1

    return-object p1

    .line 127
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Document digest was null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 128
    :cond_2
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePlatformDocumentDigesterResult;->getError()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 129
    :cond_3
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v1, "Invalid document provider index "

    invoke-direct {p3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", valid range is [0, "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->A:[I

    array-length p1, p1

    sub-int/2addr p1, v0

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final getHashForDocumentRange(Ljava/util/List;Lcom/pspdfkit/signatures/HashAlgorithm;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/pspdfkit/signatures/HashAlgorithm;",
            ")[B"
        }
    .end annotation

    const/4 v0, 0x0

    .line 130
    invoke-virtual {p0, v0, p1, p2}, Lcom/pspdfkit/internal/zf;->getHashForDocumentRange(ILjava/util/List;Lcom/pspdfkit/signatures/HashAlgorithm;)[B

    move-result-object p1

    return-object p1
.end method

.method public final getJavaScriptProvider()Lcom/pspdfkit/javascript/JavaScriptProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->k:Lcom/pspdfkit/internal/ig;

    return-object v0
.end method

.method public final getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getMeasurementPrecision()Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    return-object v0
.end method

.method public final getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getMeasurementScale()Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object v0

    const-string v1, "nativeScale"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v1, Lcom/pspdfkit/annotations/measurements/Scale;

    .line 129
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getFrom()D

    move-result-wide v2

    double-to-float v2, v2

    .line 130
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getUnitFrom()Lcom/pspdfkit/internal/jni/NativeUnitFrom;

    move-result-object v3

    const-string v4, "nativeScale.unitFrom"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeUnitFrom;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    move-result-object v3

    .line 131
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getTo()D

    move-result-wide v4

    double-to-float v4, v4

    .line 132
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeMeasurementScale;->getUnitTo()Lcom/pspdfkit/internal/jni/NativeUnitTo;

    move-result-object v0

    const-string v5, "nativeScale.unitTo"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/internal/jni/NativeUnitTo;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object v0

    .line 133
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    return-object v1
.end method

.method public final getOutline()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getOutlineParser()Lcom/pspdfkit/internal/jni/NativeOutlineParser;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeOutlineParser;->getFlatbuffersOutline()[B

    move-result-object v0

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/nl;->a(Lcom/pspdfkit/internal/zf;[B)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/zf;->y:Ljava/util/List;

    return-object v0
.end method

.method public final getOutlineAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/zf;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    const/16 v1, 0xa

    .line 5
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public final getPageBinding()Lcom/pspdfkit/document/PageBinding;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageBinding()Lcom/pspdfkit/internal/jni/NativePageBinding;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativePageBinding;)Lcom/pspdfkit/document/PageBinding;

    move-result-object v0

    return-object v0
.end method

.method public final getPageBox(ILcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;
    .locals 2

    const-string v0, "box"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/ym;->a(Lcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;

    move-result-object v0

    if-nez v0, :cond_4

    .line 56
    sget-object v0, Lcom/pspdfkit/document/PdfBox;->CROP_BOX:Lcom/pspdfkit/document/PdfBox;

    if-ne p2, v0, :cond_0

    .line 59
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/document/PdfBox;->MEDIA_BOX:Lcom/pspdfkit/document/PdfBox;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/ym;->a(Lcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_3

    return-object p2

    .line 61
    :cond_0
    sget-object v1, Lcom/pspdfkit/document/PdfBox;->BLEED_BOX:Lcom/pspdfkit/document/PdfBox;

    if-eq p2, v1, :cond_1

    sget-object v1, Lcom/pspdfkit/document/PdfBox;->TRIM_BOX:Lcom/pspdfkit/document/PdfBox;

    if-ne p2, v1, :cond_3

    .line 64
    :cond_1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p2

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/ym;->a(Lcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_2

    return-object p2

    .line 69
    :cond_2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/document/PdfBox;->MEDIA_BOX:Lcom/pspdfkit/document/PdfBox;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/ym;->a(Lcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_3

    return-object p2

    .line 76
    :cond_3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 77
    new-instance p2, Landroid/graphics/RectF;

    iget v0, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    const/4 v1, 0x0

    invoke-direct {p2, v1, v0, p1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object p2

    :cond_4
    return-object v0
.end method

.method public final getPageCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/zf;->q:I

    return v0
.end method

.method public final getPageIndexForPageLabel(Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 2

    const-string v0, "pageLabel"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageIndexForPageLabel(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public final getPageLabel(IZ)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/zf$e;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getPageRotation(I)I
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/zf$e;->getPageRotation(I)B

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/zf$e;->getRotationOffset(I)B

    move-result p1

    add-int/2addr v0, p1

    rem-int/lit8 v0, v0, 0x4

    const/4 p1, 0x1

    if-eq v0, p1, :cond_2

    const/4 p1, 0x2

    if-eq v0, p1, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x10e

    goto :goto_0

    :cond_1
    const/16 p1, 0xb4

    goto :goto_0

    :cond_2
    const/16 p1, 0x5a

    :goto_0
    return p1
.end method

.method public final getPageSize(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->s:Lcom/pspdfkit/internal/zf$e;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/zf$e;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    return-object p1
.end method

.method public final getPageText(I)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ym;->c()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getPageText(III)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 4
    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/ym;->a(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getPageText(ILandroid/graphics/RectF;)Ljava/lang/String;
    .locals 2

    const-string v0, "rectF"

    const-string v1, "argumentName"

    .line 6
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 57
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 59
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ym;->a(Landroid/graphics/RectF;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getPageTextLength(I)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ym;->d()I

    move-result p1

    return p1
.end method

.method public final getPageTextRects(III)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/pspdfkit/internal/zf;->getPageTextRects(IIIZ)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final getPageTextRects(IIIZ)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ)",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf;->d(I)Lcom/pspdfkit/internal/ym;

    move-result-object p1

    .line 3
    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/ym;->b(II)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object p1

    if-nez p1, :cond_0

    .line 4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1

    :cond_0
    if-eqz p4, :cond_1

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getMarkupRects()Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    .line 11
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getRects()Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getPdfMetadata()Lcom/pspdfkit/document/metadata/DocumentPdfMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    return-object v0
.end method

.method public final getPdfProjection()Lcom/pspdfkit/projection/PdfProjection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->I:Lcom/pspdfkit/internal/zm;

    return-object v0
.end method

.method public final getPdfVersion()Lcom/pspdfkit/document/PdfVersion;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->x:Lcom/pspdfkit/document/PdfVersion;

    return-object v0
.end method

.method public final getPermissions()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/document/DocumentPermissions;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final getRotationOffset(I)I
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->b(I)I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x10e

    goto :goto_0

    :cond_1
    const/16 p1, 0xb4

    goto :goto_0

    :cond_2
    const/16 p1, 0x5a

    :goto_0
    return p1
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g9;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g9;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->w:Ljava/lang/String;

    .line 6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->w:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getUid()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final getXmpMetadata()Lcom/pspdfkit/document/metadata/DocumentXmpMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->i:Lcom/pspdfkit/internal/ba;

    return-object v0
.end method

.method public final h()Lcom/pspdfkit/internal/xf;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->k:Lcom/pspdfkit/internal/ig;

    return-object v0
.end method

.method public final h(I)Lio/reactivex/rxjava3/core/Scheduler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->a:Lcom/pspdfkit/internal/aa;

    iget-object v0, v0, Lcom/pspdfkit/internal/aa;->a:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    return-object p1
.end method

.method public final hasOutline()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->z:Z

    return v0
.end method

.method public final hasPermission(Lcom/pspdfkit/document/DocumentPermissions;)Z
    .locals 2

    const-string v0, "permission"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->B:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final i()Lcom/pspdfkit/internal/jni/NativeDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    return-object v0
.end method

.method public final initPageCache()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->t:Lio/reactivex/rxjava3/core/Completable;

    .line 2
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final initPageCacheAsync()Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->t:Lio/reactivex/rxjava3/core/Completable;

    return-object v0
.end method

.method public final invalidateCache()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    .line 2
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-void
.end method

.method public final invalidateCacheForPage(I)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-void
.end method

.method public final isAutomaticLinkGenerationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->D:Z

    return v0
.end method

.method public final isValidForEditing()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->c:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 5
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    .line 6
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    instance-of v0, v0, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public final isWatermarkFilteringEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->C:Z

    return v0
.end method

.method public final j()Lcom/pspdfkit/internal/jni/NativeResourceManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->o:Lcom/pspdfkit/internal/jni/NativeResourceManager;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->u:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/DocumentSource;

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->n:Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    return-object v0
.end method

.method public final m()Ljava/util/concurrent/locks/ReentrantReadWriteLock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->m:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->G:Z

    return v0
.end method

.method public final renderPageToBitmap(Landroid/content/Context;III)Landroid/graphics/Bitmap;
    .locals 6

    .line 1
    sget-object v5, Lcom/pspdfkit/internal/zf;->J:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/zf;->renderPageToBitmap(Landroid/content/Context;IIILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public final renderPageToBitmap(Landroid/content/Context;IIILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Landroid/graphics/Bitmap;
    .locals 0

    .line 2
    :try_start_0
    invoke-virtual/range {p0 .. p5}, Lcom/pspdfkit/internal/zf;->renderPageToBitmapAsync(Landroid/content/Context;IIILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 5
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Ljava/lang/IllegalArgumentException;

    if-eqz p2, :cond_0

    .line 6
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Ljava/lang/IllegalArgumentException;

    throw p1

    .line 8
    :cond_0
    throw p1
.end method

.method public final renderPageToBitmapAsync(Landroid/content/Context;III)Lio/reactivex/rxjava3/core/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "III)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v5, Lcom/pspdfkit/internal/zf;->J:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/zf;->renderPageToBitmapAsync(Landroid/content/Context;IIILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final renderPageToBitmapAsync(Landroid/content/Context;IIILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "III",
            "Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 3
    iget-object p1, p5, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->reuseBitmap:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    if-ne p1, p3, :cond_0

    iget-object p1, p5, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->reuseBitmap:Landroid/graphics/Bitmap;

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    if-ne p1, p4, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Reusable bitmap has to be the same size as passed width and height."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 9
    :cond_1
    :goto_0
    iget-boolean p1, p5, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->renderRegion:Z

    const/16 v0, 0xa

    if-eqz p1, :cond_2

    .line 10
    new-instance p1, Lcom/pspdfkit/internal/gm$a;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/gm$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    .line 11
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    .line 12
    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/gm$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/gm$a;

    move-result-object p1

    .line 13
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    .line 14
    invoke-virtual {p1, p4}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/gm$a;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/gm$a;->b()Lcom/pspdfkit/internal/gm;

    move-result-object p1

    .line 16
    invoke-static {p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/gm;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 18
    :cond_2
    new-instance p1, Lcom/pspdfkit/internal/rc$a;

    invoke-direct {p1, p0, p2}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    .line 19
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    .line 20
    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object p1

    .line 21
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    .line 22
    invoke-virtual {p1, p4}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object p1

    .line 24
    invoke-static {p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public save(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 108
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 109
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zf;->save(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    return-void
.end method

.method public save(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "path"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "saveOptions"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->c(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    return-void
.end method

.method public saveAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1

    const/4 v0, 0x1

    .line 112
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 113
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zf;->saveAsync(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public saveAsync(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    const-string v0, "path"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "saveOptions"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/zf;Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    const/16 p2, 0xa

    .line 111
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public saveIfModified()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 178
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 179
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result v0

    return v0
.end method

.method public saveIfModified(Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "saveOptions"

    const-string v1, "argumentName"

    .line 109
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 160
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 161
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->c:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 166
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->wasModified()Z

    move-result v0

    if-nez v0, :cond_1

    const-string p1, "PSPDFKit.Document"

    const-string v0, "Document not modified, not saving."

    new-array v2, v1, [Ljava/lang/Object;

    .line 167
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v1

    .line 174
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zf;->c(Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 177
    throw p1
.end method

.method public saveIfModified(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 180
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 181
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zf;->saveIfModified(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public saveIfModified(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "path"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "saveOptions"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/zf;->d(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Z

    move-result p1

    return p1
.end method

.method public saveIfModifiedAsync()Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 164
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 165
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->saveIfModifiedAsync(Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method public saveIfModifiedAsync(Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "saveOptions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const/16 v0, 0xa

    .line 55
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public saveIfModifiedAsync(Ljava/lang/String;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 166
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object v0

    .line 167
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zf;->saveIfModifiedAsync(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public saveIfModifiedAsync(Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "path"

    const-string v1, "argumentName"

    .line 57
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 108
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "saveOptions"

    .line 110
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 162
    new-instance v0, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/zf$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/zf;Ljava/lang/String;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const/16 p2, 0xa

    .line 163
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public setAutomaticLinkGenerationEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 3
    :try_start_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zf;->D:Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->enableAutomaticLinkExtraction(Z)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    invoke-interface {p1}, Lcom/pspdfkit/internal/qf;->invalidateCache()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 8
    throw p1
.end method

.method public final setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->setMeasurementPrecision(Lcom/pspdfkit/internal/jni/NativeMeasurementPrecision;)V

    return-void
.end method

.method public final setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/annotations/measurements/Scale;)Lcom/pspdfkit/internal/jni/NativeMeasurementScale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->setMeasurementScale(Lcom/pspdfkit/internal/jni/NativeMeasurementScale;)V

    return-void
.end method

.method public final setPageBinding(Lcom/pspdfkit/document/PageBinding;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/pspdfkit/document/PageBinding;->UNKNOWN:Lcom/pspdfkit/document/PageBinding;

    if-eq p1, v0, :cond_3

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v1

    if-ne v1, v0, :cond_0

    .line 9
    sget-object v1, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    :cond_0
    const/4 v0, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const-string v2, "pageBinding"

    const-string v3, "argumentName"

    .line 14
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 65
    invoke-static {p1, v2, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 66
    iget-object v2, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/document/PageBinding;)Lcom/pspdfkit/internal/jni/NativePageBinding;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->setPageBinding(Lcom/pspdfkit/internal/jni/NativePageBinding;)V

    .line 67
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zf;->H:Z

    if-eqz v1, :cond_2

    .line 70
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->F:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/zf$f;

    .line 71
    invoke-interface {v0}, Lcom/pspdfkit/internal/zf$f;->onPageBindingChanged()V

    goto :goto_1

    :cond_2
    return-void

    .line 72
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "You can\'t set the page binding to UNKNOWN."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setRotationOffset(II)V
    .locals 4

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/zf;->a(I)V

    .line 2
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    const/16 v2, 0x5a

    if-eqz p1, :cond_1

    if-eq p1, v2, :cond_1

    const/16 v3, 0xb4

    if-eq p1, v3, :cond_1

    const/16 v3, 0x10e

    if-ne p1, v3, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Invalid rotation passed: %d. Expected one of: 0, 90, 180, 270."

    .line 5
    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 8
    :cond_1
    :goto_0
    div-int/2addr p1, v2

    .line 9
    invoke-virtual {v0, p2, p1}, Landroid/util/SparseIntArray;->put(II)V

    .line 10
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Landroid/util/SparseIntArray;)V

    return-void
.end method

.method public final setRotationOffsets(Landroid/util/SparseIntArray;)V
    .locals 6

    const-string v0, "pageRotations"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 54
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 56
    invoke-virtual {p1, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    .line 57
    invoke-virtual {p1, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    .line 58
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/zf;->a(I)V

    if-eqz v4, :cond_1

    const/16 v5, 0x5a

    if-eq v4, v5, :cond_1

    const/16 v5, 0xb4

    if-eq v4, v5, :cond_1

    const/16 v5, 0x10e

    if-ne v4, v5, :cond_0

    goto :goto_1

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 60
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "Invalid rotation passed: %d. Expected one of: 0, 90, 180, 270."

    .line 61
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 64
    :cond_1
    :goto_1
    div-int/lit8 v4, v4, 0x5a

    .line 65
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/zf;->a(Landroid/util/SparseIntArray;)V

    return-void
.end method

.method public final setWatermarkTextFilteringEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 3
    :try_start_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zf;->C:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->C:Z

    if-eqz v0, :cond_0

    .line 6
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeTextParserOptions;->FILTER_WATERMARKS:Lcom/pspdfkit/internal/jni/NativeTextParserOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    .line 7
    :cond_0
    const-class v0, Lcom/pspdfkit/internal/jni/NativeTextParserOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 8
    :goto_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->setTextParserOptions(Ljava/util/EnumSet;)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ym;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ym;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 16
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 17
    throw p1
.end method

.method public wasModified()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 3
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->d:Lcom/pspdfkit/internal/qf;

    invoke-interface {v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->e:Lcom/pspdfkit/internal/rf;

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/bookmarks/BookmarkProvider;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->f:Lcom/pspdfkit/internal/uf;

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/forms/FormProvider;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->h:Lcom/pspdfkit/internal/g9;

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/f9;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->i:Lcom/pspdfkit/internal/ba;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/f9;->hasUnsavedChanges()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->E:Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/document/checkpoint/PdfDocumentCheckpointer;->checkpointExists()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zf;->H:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocument;->needsSave()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 12
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/pspdfkit/internal/zf;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 13
    throw v0
.end method

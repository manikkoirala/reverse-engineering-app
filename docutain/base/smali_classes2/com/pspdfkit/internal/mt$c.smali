.class final Lcom/pspdfkit/internal/mt$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/mt;-><init>(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/mt;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/mt;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/mt$c;->a:Lcom/pspdfkit/internal/mt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    check-cast p1, Ljava/util/List;

    const-string v0, "fonts"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/mt$c;->a:Lcom/pspdfkit/internal/mt;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/mt;->a(Lcom/pspdfkit/internal/mt;Ljava/util/List;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    if-nez p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/mt$c;->a:Lcom/pspdfkit/internal/mt;

    invoke-static {p1}, Lcom/pspdfkit/internal/mt;->a(Lcom/pspdfkit/internal/mt;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    :cond_0
    return-object p1
.end method

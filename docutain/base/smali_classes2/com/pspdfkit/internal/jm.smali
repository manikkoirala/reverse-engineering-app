.class public final Lcom/pspdfkit/internal/jm;
.super Landroidx/core/view/AccessibilityDelegateCompat;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private final b:Lcom/pspdfkit/internal/zf;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/zf;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/core/view/AccessibilityDelegateCompat;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/jm;->b:Lcom/pspdfkit/internal/zf;

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/jm;->c:I

    return-void
.end method


# virtual methods
.method public final onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/core/view/AccessibilityDelegateCompat;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2
    const-class p1, Lcom/pspdfkit/internal/im;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/core/view/AccessibilityDelegateCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 2
    const-class p1, Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 4
    :goto_0
    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setScrollable(Z)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    if-ltz p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-ge p1, v1, :cond_1

    const/16 p1, 0x1000

    .line 8
    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    if-lez p1, :cond_2

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v0

    if-ge p1, v0, :cond_2

    const/16 p1, 0x2000

    .line 12
    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    :cond_2
    return-void
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/core/view/AccessibilityDelegateCompat;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 2
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "PSPDFKit.Accessibility"

    const-string v1, "[POPULATE] %s"

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->d()Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 7
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/jm;->b:Lcom/pspdfkit/internal/zf;

    iget v1, p0, Lcom/pspdfkit/internal/jm;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageText(I)Ljava/lang/String;

    move-result-object v0

    .line 8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 9
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    :cond_1
    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 13
    throw p2
.end method

.method public final performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/core/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result p1

    const/4 p3, 0x1

    if-eqz p1, :cond_0

    return p3

    :cond_0
    const/16 p1, 0x1000

    if-eq p2, p1, :cond_2

    const/16 p1, 0x2000

    if-eq p2, p1, :cond_1

    goto :goto_0

    .line 2
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    if-lez p1, :cond_3

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result p2

    if-ge p1, p2, :cond_3

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(Z)Z

    return p3

    .line 6
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    if-ltz p1, :cond_3

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPage()I

    move-result p1

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result p2

    sub-int/2addr p2, p3

    if-ge p1, p2, :cond_3

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/jm;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(Z)Z

    return p3

    :cond_3
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

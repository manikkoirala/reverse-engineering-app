.class final Lcom/pspdfkit/internal/lf$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/activity/result/ActivityResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/lf;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroidx/activity/result/ActivityResultCallback<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/lf;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/lf;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityResult(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {v0}, Lcom/pspdfkit/internal/lf;->g(Lcom/pspdfkit/internal/lf;)V

    const-string v0, "permissionGranted"

    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->b(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->b(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf;Landroid/content/Intent;Landroid/content/Intent;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->e(Lcom/pspdfkit/internal/lf;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->f(Lcom/pspdfkit/internal/lf;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->c(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf;)Lcom/pspdfkit/internal/lf$c;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    const-string v2, "android.permission.CAMERA"

    invoke-virtual {v1, v2}, Landroidx/fragment/app/Fragment;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    .line 11
    invoke-interface {p1}, Lcom/pspdfkit/internal/lf$c;->a()V

    .line 14
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->c(Lcom/pspdfkit/internal/lf;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/lf;->a(Lcom/pspdfkit/internal/lf;Landroid/content/Intent;Landroid/content/Intent;)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->e(Lcom/pspdfkit/internal/lf;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->f(Lcom/pspdfkit/internal/lf;)V

    goto :goto_0

    .line 18
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->e(Lcom/pspdfkit/internal/lf;)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->f(Lcom/pspdfkit/internal/lf;)V

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/lf$e;->a:Lcom/pspdfkit/internal/lf;

    invoke-static {p1}, Lcom/pspdfkit/internal/lf;->d(Lcom/pspdfkit/internal/lf;)V

    :goto_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/zu$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 5

    const/4 v0, 0x1

    const-string v1, "PSPDFKit.VideoView"

    const/4 v2, 0x0

    const/16 v3, 0x2bd

    if-eq p2, v3, :cond_2

    const/16 v3, 0x2be

    if-eq p2, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_0
    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "onInfo MediaPlayer.MEDIA_INFO_BUFFERING_END"

    .line 1
    invoke-static {v1, v4, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetv(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/zu$h;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3
    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 5
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yu;->b()V

    goto :goto_0

    :cond_2
    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "onInfo MediaPlayer.MEDIA_INFO_BUFFERING_START"

    .line 7
    invoke-static {v1, v4, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetv(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/zu$h;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 11
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yu;->g()V

    :cond_4
    :goto_0
    const/4 v1, 0x1

    .line 27
    :goto_1
    iget-object v3, p0, Lcom/pspdfkit/internal/zu$d;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {v3}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetu(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnInfoListener;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 28
    invoke-interface {v3, p1, p2, p3}, Landroid/media/MediaPlayer$OnInfoListener;->onInfo(Landroid/media/MediaPlayer;II)Z

    move-result p1

    if-nez p1, :cond_6

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :cond_6
    :goto_2
    return v0

    :cond_7
    return v1
.end method

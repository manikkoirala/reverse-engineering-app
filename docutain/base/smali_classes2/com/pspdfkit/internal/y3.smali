.class public final Lcom/pspdfkit/internal/y3;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/y3$a;,
        Lcom/pspdfkit/internal/y3$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/y3$b;


# instance fields
.field private final a:Lcom/pspdfkit/internal/w3;

.field private final b:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/y3$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/y3$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/y3;->Companion:Lcom/pspdfkit/internal/y3$b;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/pspdfkit/internal/w3;F)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x3

    const/4 v1, 0x3

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/y3$a;->a:Lcom/pspdfkit/internal/y3$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y3$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/y3;->a:Lcom/pspdfkit/internal/w3;

    iput p3, p0, Lcom/pspdfkit/internal/y3;->b:F

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/w3;F)V
    .locals 1

    const-string v0, "faceRef"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/y3;->a:Lcom/pspdfkit/internal/w3;

    iput p2, p0, Lcom/pspdfkit/internal/y3;->b:F

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/y3;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/w3$a;->a:Lcom/pspdfkit/internal/w3$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/y3;->a:Lcom/pspdfkit/internal/w3;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    iget p0, p0, Lcom/pspdfkit/internal/y3;->b:F

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeFloatElement(Lkotlinx/serialization/descriptors/SerialDescriptor;IF)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/w3;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y3;->a:Lcom/pspdfkit/internal/w3;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/y3;->b:F

    return v0
.end method

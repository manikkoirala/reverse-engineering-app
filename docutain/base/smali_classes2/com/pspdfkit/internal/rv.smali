.class public final Lcom/pspdfkit/internal/rv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:Z

.field private final j:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private final k:Lcom/pspdfkit/internal/ug;

.field private l:Ljava/lang/Runnable;


# direct methods
.method public static synthetic $r8$lambda$Qge6QZYmSIPhXhgbpUyCzr4TUNw(Lcom/pspdfkit/internal/rv;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/rv;->b()V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/ug;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/rv;->j:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 18
    iput-object p2, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    return-void
.end method

.method public static a(IIII)I
    .locals 1

    add-int v0, p1, p2

    sub-int/2addr v0, p0

    sub-int/2addr v0, p3

    if-eqz v0, :cond_0

    mul-int p1, p1, p2

    mul-int p0, p0, p3

    sub-int/2addr p1, p0

    .line 1
    div-int/2addr p1, v0

    return p1

    :cond_0
    add-int/2addr p0, p1

    .line 6
    div-int/lit8 p0, p0, 0x2

    return p0
.end method

.method private b()V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-wide v0, p0, Lcom/pspdfkit/internal/rv;->a:J

    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    cmp-long v5, v0, v2

    if-lez v5, :cond_1

    iget-wide v2, p0, Lcom/pspdfkit/internal/rv;->c:J

    iget-wide v5, p0, Lcom/pspdfkit/internal/rv;->b:J

    sub-long/2addr v2, v5

    long-to-float v2, v2

    long-to-float v0, v0

    div-float/2addr v2, v0

    goto :goto_0

    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    cmpg-float v0, v2, v4

    if-gez v0, :cond_2

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/rv;->d:F

    iget v1, p0, Lcom/pspdfkit/internal/rv;->f:F

    sub-float/2addr v1, v0

    mul-float v1, v1, v2

    add-float/2addr v1, v0

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    iget v2, p0, Lcom/pspdfkit/internal/rv;->e:F

    div-float v2, v1, v2

    iget v3, p0, Lcom/pspdfkit/internal/rv;->g:F

    iget v4, p0, Lcom/pspdfkit/internal/rv;->h:F

    invoke-virtual {v0, v2, v3, v4}, Lcom/pspdfkit/internal/ug;->a(FFF)Z

    .line 11
    iput v1, p0, Lcom/pspdfkit/internal/rv;->e:F

    .line 12
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/pspdfkit/internal/rv;->c:J

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/rv;->j:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget-object v1, p0, Lcom/pspdfkit/internal/rv;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x8

    invoke-static {v0, v1, v2, v3}, Landroidx/core/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto :goto_1

    .line 17
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    iget v1, p0, Lcom/pspdfkit/internal/rv;->f:F

    iget v2, p0, Lcom/pspdfkit/internal/rv;->e:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/pspdfkit/internal/rv;->g:F

    iget v3, p0, Lcom/pspdfkit/internal/rv;->h:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/pspdfkit/internal/ug;->a(FFF)Z

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    iget v1, p0, Lcom/pspdfkit/internal/rv;->f:F

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ug;->a(F)V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    :goto_1
    return-void
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 2

    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    .line 9
    iput p1, p0, Lcom/pspdfkit/internal/rv;->g:F

    .line 10
    iput p2, p0, Lcom/pspdfkit/internal/rv;->h:F

    .line 11
    iput p3, p0, Lcom/pspdfkit/internal/rv;->e:F

    iput p3, p0, Lcom/pspdfkit/internal/rv;->d:F

    .line 12
    iput p4, p0, Lcom/pspdfkit/internal/rv;->f:F

    const-wide/16 v0, 0x190

    .line 13
    iput-wide v0, p0, Lcom/pspdfkit/internal/rv;->a:J

    .line 16
    iget-object p4, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    invoke-virtual {p4, p3, p1, p2}, Lcom/pspdfkit/internal/ug;->b(FFF)Z

    .line 19
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/rv;->c:J

    iput-wide p1, p0, Lcom/pspdfkit/internal/rv;->b:J

    const/4 p1, 0x1

    .line 21
    iput-boolean p1, p0, Lcom/pspdfkit/internal/rv;->i:Z

    .line 22
    iget-wide p1, p0, Lcom/pspdfkit/internal/rv;->a:J

    const-wide/16 p3, 0x0

    cmp-long v0, p1, p3

    if-lez v0, :cond_0

    .line 24
    new-instance p1, Lcom/pspdfkit/internal/rv$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/rv$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/rv;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/rv;->l:Ljava/lang/Runnable;

    .line 26
    iget-object p2, p0, Lcom/pspdfkit/internal/rv;->j:Lcom/pspdfkit/internal/views/document/DocumentView;

    const-wide/16 p3, 0x8

    invoke-static {p2, p1, p3, p4}, Landroidx/core/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/rv;->b()V

    :goto_0
    return-void
.end method

.method public final a(Landroid/graphics/RectF;Landroid/graphics/RectF;FJ)V
    .locals 6

    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    .line 31
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v2, v3

    .line 32
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    mul-float v1, v1, p3

    .line 34
    iget-object v2, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ug;->k()F

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/ug;->j()F

    move-result v3

    .line 35
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 36
    iput v1, p0, Lcom/pspdfkit/internal/rv;->f:F

    div-float/2addr v1, p3

    .line 41
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v2, v3

    .line 42
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v3, v1

    .line 44
    iget v1, p2, Landroid/graphics/RectF;->left:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    iput v1, p2, Landroid/graphics/RectF;->left:F

    .line 45
    iget v5, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v5, v2

    iput v5, p2, Landroid/graphics/RectF;->right:F

    .line 46
    iget v2, p2, Landroid/graphics/RectF;->top:F

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, p2, Landroid/graphics/RectF;->top:F

    .line 47
    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v3

    iput v2, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    float-to-int v2, v5

    .line 49
    iget v3, p1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v5, p1, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    invoke-static {v1, v2, v3, v5}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/pspdfkit/internal/rv;->g:F

    .line 52
    iget v1, p2, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int p1, p1

    invoke-static {v1, v2, v3, p1}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/rv;->h:F

    .line 56
    iput p3, p0, Lcom/pspdfkit/internal/rv;->e:F

    iput p3, p0, Lcom/pspdfkit/internal/rv;->d:F

    .line 57
    iput-wide p4, p0, Lcom/pspdfkit/internal/rv;->a:J

    .line 64
    iget p1, p0, Lcom/pspdfkit/internal/rv;->f:F

    sub-float p1, p3, p1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const v1, 0x3dcccccd    # 0.1f

    cmpg-float p1, p1, v1

    if-gez p1, :cond_0

    .line 65
    iget-object p1, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    iget p3, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr p3, v1

    div-float/2addr p3, v4

    float-to-int p3, p3

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, p2

    div-float/2addr v1, v4

    float-to-int p2, v1

    long-to-int p5, p4

    invoke-virtual {p1, p3, p2, p5}, Lcom/pspdfkit/internal/ug;->a(III)V

    .line 69
    iput-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    return-void

    .line 74
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/rv;->k:Lcom/pspdfkit/internal/ug;

    iget p2, p0, Lcom/pspdfkit/internal/rv;->g:F

    iget p4, p0, Lcom/pspdfkit/internal/rv;->h:F

    invoke-virtual {p1, p3, p2, p4}, Lcom/pspdfkit/internal/ug;->b(FFF)Z

    .line 77
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/rv;->c:J

    iput-wide p1, p0, Lcom/pspdfkit/internal/rv;->b:J

    const/4 p1, 0x1

    .line 79
    iput-boolean p1, p0, Lcom/pspdfkit/internal/rv;->i:Z

    .line 80
    iget-wide p1, p0, Lcom/pspdfkit/internal/rv;->a:J

    const-wide/16 p3, 0x0

    cmp-long p5, p1, p3

    if-lez p5, :cond_1

    .line 82
    new-instance p1, Lcom/pspdfkit/internal/rv$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/rv$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/rv;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/rv;->l:Ljava/lang/Runnable;

    .line 84
    iget-object p2, p0, Lcom/pspdfkit/internal/rv;->j:Lcom/pspdfkit/internal/views/document/DocumentView;

    const-wide/16 p3, 0x8

    invoke-static {p2, p1, p3, p4}, Landroidx/core/view/ViewCompat;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 87
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/rv;->b()V

    :goto_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/rv;->i:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

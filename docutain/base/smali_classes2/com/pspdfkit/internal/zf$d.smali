.class final Lcom/pspdfkit/internal/zf$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zf$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeDocument;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocument;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/zf$d;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/zf$d-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zf$d;-><init>(Lcom/pspdfkit/internal/jni/NativeDocument;)V

    return-void
.end method


# virtual methods
.method public final getPageLabel(IZ)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$d;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getPageRotation(I)B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$d;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotation()B

    move-result p1

    return p1
.end method

.method public final getPageSize(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$d;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getSize()Lcom/pspdfkit/utils/Size;

    move-result-object p1

    return-object p1
.end method

.method public final getRotationOffset(I)B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$d;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotationOffset()B

    move-result p1

    return p1
.end method

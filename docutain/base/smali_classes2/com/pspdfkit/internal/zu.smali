.class public final Lcom/pspdfkit/internal/zu;
.super Landroid/view/SurfaceView;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/yu$g;
.implements Lcom/pspdfkit/internal/ll$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/zu$h;
    }
.end annotation


# instance fields
.field A:Landroid/media/MediaPlayer$OnPreparedListener;

.field private B:Z

.field private C:Z

.field private final D:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final E:Landroid/media/MediaPlayer$OnInfoListener;

.field private final F:Landroid/media/MediaPlayer$OnErrorListener;

.field private final G:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private final H:Landroid/view/SurfaceHolder$Callback;

.field private b:Landroid/net/Uri;

.field private c:I

.field private d:I

.field private e:Landroid/view/SurfaceHolder;

.field private f:Landroid/media/MediaPlayer;

.field private g:I

.field private final h:Landroid/content/Context;

.field private i:Lcom/pspdfkit/internal/yu;

.field private j:Lcom/pspdfkit/internal/ll;

.field private k:I

.field private l:I

.field m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Landroid/media/MediaPlayer$OnCompletionListener;

.field private s:Landroid/media/MediaPlayer$OnPreparedListener;

.field private t:Landroid/media/MediaPlayer$OnErrorListener;

.field private u:Landroid/media/MediaPlayer$OnInfoListener;

.field private v:Lcom/pspdfkit/internal/zu$h;

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetC(Lcom/pspdfkit/internal/zu;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/zu;->C:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->c:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->d:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/zu;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->h:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/ll;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->j:Lcom/pspdfkit/internal/ll;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetk(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->k:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->l:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->n:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeto(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->o:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetr(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->r:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgets(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->s:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgett(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->t:Landroid/media/MediaPlayer$OnErrorListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetu(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnInfoListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->u:Landroid/media/MediaPlayer$OnInfoListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetv(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/zu$h;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zu;->v:Lcom/pspdfkit/internal/zu$h;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetx(Lcom/pspdfkit/internal/zu;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zu;->x:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputc(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->c:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->d:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fpute(Lcom/pspdfkit/internal/zu;Landroid/view/SurfaceHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->e:Landroid/view/SurfaceHolder;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/zu;Landroid/media/MediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/zu;Lcom/pspdfkit/internal/ll;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->j:Lcom/pspdfkit/internal/ll;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputk(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->k:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->l:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputn(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->n:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputo(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->o:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputw(Lcom/pspdfkit/internal/zu;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/zu;->w:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputy(Lcom/pspdfkit/internal/zu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/zu;->y:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputz(Lcom/pspdfkit/internal/zu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/zu;->z:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$me(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/zu;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    const/4 p2, 0x0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zu;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, p2, v0}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 5
    iput v0, p0, Lcom/pspdfkit/internal/zu;->d:I

    .line 8
    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->e:Landroid/view/SurfaceHolder;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    .line 17
    new-instance p2, Lcom/pspdfkit/internal/zu$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$a;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 31
    iput v0, p0, Lcom/pspdfkit/internal/zu;->p:I

    .line 32
    iput v0, p0, Lcom/pspdfkit/internal/zu;->q:I

    .line 55
    new-instance p2, Lcom/pspdfkit/internal/zu$b;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$b;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->A:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 112
    new-instance p2, Lcom/pspdfkit/internal/zu$c;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$c;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->D:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 129
    new-instance p2, Lcom/pspdfkit/internal/zu$d;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$d;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->E:Landroid/media/MediaPlayer$OnInfoListener;

    .line 162
    new-instance p2, Lcom/pspdfkit/internal/zu$e;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$e;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->F:Landroid/media/MediaPlayer$OnErrorListener;

    .line 184
    new-instance p2, Lcom/pspdfkit/internal/zu$f;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$f;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->G:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 193
    new-instance p2, Lcom/pspdfkit/internal/zu$g;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/zu$g;-><init>(Lcom/pspdfkit/internal/zu;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/zu;->H:Landroid/view/SurfaceHolder$Callback;

    .line 240
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->h:Landroid/content/Context;

    .line 241
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zu;->B:Z

    .line 242
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zu;->C:Z

    .line 243
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->b()V

    return-void
.end method

.method private a(II)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/zu;->k:I

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result p1

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/zu;->l:I

    invoke-static {v0, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result p2

    .line 3
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    .line 56
    iput v0, p0, Lcom/pspdfkit/internal/zu;->k:I

    .line 57
    iput v0, p0, Lcom/pspdfkit/internal/zu;->l:I

    .line 58
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/zu;->H:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    const/4 v1, 0x1

    .line 59
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 60
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 62
    iput v0, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 63
    iput v0, p0, Lcom/pspdfkit/internal/zu;->d:I

    return-void
.end method

.method private b(II)V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/zu;->k:I

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/zu;->l:I

    invoke-static {v1, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v1

    .line 3
    iget v2, p0, Lcom/pspdfkit/internal/zu;->k:I

    if-lez v2, :cond_8

    iget v2, p0, Lcom/pspdfkit/internal/zu;->l:I

    if-lez v2, :cond_8

    .line 5
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 6
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 7
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 8
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_1

    if-ne v1, v2, :cond_1

    .line 16
    iget v0, p0, Lcom/pspdfkit/internal/zu;->k:I

    mul-int v1, v0, p2

    iget v2, p0, Lcom/pspdfkit/internal/zu;->l:I

    mul-int v3, p1, v2

    if-ge v1, v3, :cond_0

    .line 17
    div-int v0, v1, v2

    goto :goto_2

    :cond_0
    if-le v1, v3, :cond_4

    .line 19
    div-int v1, v3, v0

    goto :goto_0

    :cond_1
    const/high16 v3, -0x80000000

    if-ne v0, v2, :cond_3

    .line 24
    iget v0, p0, Lcom/pspdfkit/internal/zu;->l:I

    mul-int v0, v0, p1

    iget v2, p0, Lcom/pspdfkit/internal/zu;->k:I

    div-int/2addr v0, v2

    if-ne v1, v3, :cond_2

    if-le v0, p2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v0

    :goto_0
    move v0, p1

    goto :goto_4

    :cond_3
    if-ne v1, v2, :cond_6

    .line 32
    iget v1, p0, Lcom/pspdfkit/internal/zu;->k:I

    mul-int v1, v1, p2

    iget v2, p0, Lcom/pspdfkit/internal/zu;->l:I

    div-int/2addr v1, v2

    if-ne v0, v3, :cond_5

    if-le v1, p1, :cond_5

    :cond_4
    :goto_1
    move v0, p1

    goto :goto_2

    :cond_5
    move v0, v1

    :goto_2
    move v1, p2

    goto :goto_4

    .line 39
    :cond_6
    iget v2, p0, Lcom/pspdfkit/internal/zu;->k:I

    .line 40
    iget v4, p0, Lcom/pspdfkit/internal/zu;->l:I

    if-ne v1, v3, :cond_7

    if-le v4, p2, :cond_7

    mul-int v1, p2, v2

    .line 44
    div-int/2addr v1, v4

    goto :goto_3

    :cond_7
    move v1, v2

    move p2, v4

    :goto_3
    if-ne v0, v3, :cond_5

    if-le v1, p1, :cond_5

    mul-int v4, v4, p1

    .line 49
    div-int v1, v4, v2

    goto :goto_0

    .line 55
    :cond_8
    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method private c()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/zu;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    if-eqz v0, :cond_0

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private e()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->b:Landroid/net/Uri;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->e:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->h:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_3

    .line 6
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x1a

    if-lt v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x3

    if-eqz v4, :cond_2

    .line 7
    new-instance v4, Landroid/media/AudioFocusRequest$Builder;

    invoke-direct {v4, v3}, Landroid/media/AudioFocusRequest$Builder;-><init>(I)V

    new-instance v6, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v6}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 9
    invoke-virtual {v6, v5}, Landroid/media/AudioAttributes$Builder;->setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v5

    .line 10
    invoke-virtual {v5}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v5

    .line 11
    invoke-virtual {v4, v5}, Landroid/media/AudioFocusRequest$Builder;->setAudioAttributes(Landroid/media/AudioAttributes;)Landroid/media/AudioFocusRequest$Builder;

    move-result-object v4

    .line 14
    invoke-virtual {v4}, Landroid/media/AudioFocusRequest$Builder;->build()Landroid/media/AudioFocusRequest;

    move-result-object v4

    .line 15
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioFocusRequest;)I

    goto :goto_1

    .line 21
    :cond_2
    invoke-virtual {v0, v1, v5, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 22
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_4

    .line 23
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 25
    iput-object v1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    .line 26
    iput v2, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 27
    :cond_4
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    .line 29
    iget v1, p0, Lcom/pspdfkit/internal/zu;->g:I

    if-eqz v1, :cond_5

    .line 30
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    goto :goto_2

    .line 32
    :cond_5
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/zu;->g:I

    .line 34
    :goto_2
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->A:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->m:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->D:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->F:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->E:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->G:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 40
    iput v2, p0, Lcom/pspdfkit/internal/zu;->w:I

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->h:Landroid/content/Context;

    iget-object v4, p0, Lcom/pspdfkit/internal/zu;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->e:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/zu;->setAudioStreamType(Landroid/media/MediaPlayer;)V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 49
    iput v3, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz v0, :cond_6

    .line 51
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/yu;->setMediaPlayer(Lcom/pspdfkit/internal/yu$g;)V

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/yu;->setEnabled(Z)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open content: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/pspdfkit/internal/zu;->b:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "PSPDFKit.VideoView"

    invoke-static {v4, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    .line 55
    iput v0, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 56
    iput v0, p0, Lcom/pspdfkit/internal/zu;->d:I

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->F:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    check-cast v0, Lcom/pspdfkit/internal/zu$e;

    invoke-virtual {v0, v1, v3, v2}, Lcom/pspdfkit/internal/zu$e;->onError(Landroid/media/MediaPlayer;II)Z

    :cond_6
    :goto_3
    return-void
.end method

.method private setAudioStreamType(Landroid/media/MediaPlayer;)V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-eqz v0, :cond_1

    .line 2
    new-instance v0, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v0}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 3
    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    .line 5
    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setAudioAttributes(Landroid/media/AudioAttributes;)V

    goto :goto_1

    .line 9
    :cond_1
    invoke-virtual {p1, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zu;->C:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 9
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/zu;->a(IZ)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    const/16 p1, 0x9

    .line 11
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zu;->a(IZ)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne p1, v2, :cond_3

    .line 13
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/zu;->a(IZ)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    const/16 p1, 0x8

    .line 15
    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/internal/zu;->a(IZ)V

    :cond_4
    :goto_0
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->h:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const/16 v1, 0x400

    if-eqz p2, :cond_1

    .line 20
    iget v2, p0, Lcom/pspdfkit/internal/zu;->p:I

    if-nez v2, :cond_0

    iget v2, p0, Lcom/pspdfkit/internal/zu;->q:I

    if-nez v2, :cond_0

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 22
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v3, p0, Lcom/pspdfkit/internal/zu;->p:I

    .line 23
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v2, p0, Lcom/pspdfkit/internal/zu;->q:I

    .line 25
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->addFlags(I)V

    .line 26
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 28
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 29
    iget v3, p0, Lcom/pspdfkit/internal/zu;->p:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 30
    iget v3, p0, Lcom/pspdfkit/internal/zu;->q:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 31
    invoke-virtual {p0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 33
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 34
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 36
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/yu;->a(Z)V

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->v:Lcom/pspdfkit/internal/zu$h;

    if-eqz p1, :cond_2

    .line 38
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zu;->y:Z

    return v0
.end method

.method public final b(I)V
    .locals 1

    .line 64
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const/4 p1, 0x0

    .line 66
    iput p1, p0, Lcom/pspdfkit/internal/zu;->x:I

    goto :goto_0

    .line 68
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/zu;->x:I

    :goto_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 4
    iput v1, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->v:Lcom/pspdfkit/internal/zu$h;

    if-eqz v0, :cond_0

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/aj;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/aj;->e()V

    .line 10
    :cond_0
    iput v1, p0, Lcom/pspdfkit/internal/zu;->d:I

    return-void
.end method

.method public final g()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->e()V

    return-void
.end method

.method public getBufferPercentage()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/zu;->w:I

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDuration()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public final h()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zu;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->g()V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 7
    iput v1, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->v:Lcom/pspdfkit/internal/zu$h;

    if-eqz v0, :cond_1

    .line 9
    check-cast v0, Lcom/pspdfkit/internal/aj;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/aj;->f()V

    .line 12
    :cond_1
    iput v1, p0, Lcom/pspdfkit/internal/zu;->d:I

    return-void
.end method

.method public final i()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/pspdfkit/internal/zu;->c:I

    .line 6
    iput v0, p0, Lcom/pspdfkit/internal/zu;->d:I

    :cond_0
    return-void
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2
    const-class v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/16 v1, 0x18

    if-eq p1, v1, :cond_0

    const/16 v1, 0x19

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa4

    if-eq p1, v1, :cond_0

    const/16 v1, 0x52

    if-eq p1, v1, :cond_0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    const/4 v1, 0x6

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1
    :goto_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz v1, :cond_a

    const/16 v2, 0x4f

    const/16 v3, 0xbb8

    if-eq p1, v2, :cond_8

    const/16 v2, 0x55

    if-ne p1, v2, :cond_1

    goto :goto_2

    :cond_1
    const/16 v2, 0x7e

    if-ne p1, v2, :cond_3

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result p1

    if-nez p1, :cond_2

    .line 13
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zu;->h()V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    :cond_2
    return v0

    :cond_3
    const/16 v2, 0x56

    if-eq p1, v2, :cond_6

    const/16 v2, 0x7f

    if-ne p1, v2, :cond_4

    goto :goto_1

    .line 15
    :cond_4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yu;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->a()V

    goto :goto_4

    .line 18
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    .line 19
    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/yu;->a(I)V

    goto :goto_4

    .line 20
    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zu;->f()V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    .line 23
    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/yu;->a(I)V

    :cond_7
    return v0

    .line 24
    :cond_8
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zu;->f()V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    .line 27
    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/yu;->a(I)V

    goto :goto_3

    .line 28
    :cond_9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zu;->h()V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    :goto_3
    return v0

    .line 49
    :cond_a
    :goto_4
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected final onMeasure(II)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onMeasure(II)V

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zu;->B:Z

    if-eqz v0, :cond_0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zu;->a(II)V

    goto :goto_0

    .line 5
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zu;->b(II)V

    :goto_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/zu;->performClick()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz p1, :cond_1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    const/16 v0, 0xbb8

    .line 6
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public final performClick()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->a()V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    const/16 v1, 0xbb8

    .line 6
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/yu;->a(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    .line 7
    :cond_1
    invoke-super {p0}, Landroid/view/SurfaceView;->performClick()Z

    move-result v0

    return v0
.end method

.method public setAutoRotation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zu;->C:Z

    return-void
.end method

.method public setFitXY(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zu;->B:Z

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 1

    xor-int/lit8 v0, p1, 0x1

    .line 1
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/zu;->a(IZ)V

    return-void
.end method

.method public setMediaController(Lcom/pspdfkit/internal/yu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/yu;->a()V

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zu;->f:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 6
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yu;->setMediaPlayer(Lcom/pspdfkit/internal/yu$g;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->setEnabled(Z)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/zu;->i:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    :cond_1
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->r:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->t:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->u:Landroid/media/MediaPlayer$OnInfoListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->s:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/zu;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->b:Landroid/net/Uri;

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/zu;->x:I

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/zu;->e()V

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setVideoViewListener(Lcom/pspdfkit/internal/zu$h;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu;->v:Lcom/pspdfkit/internal/zu$h;

    return-void
.end method

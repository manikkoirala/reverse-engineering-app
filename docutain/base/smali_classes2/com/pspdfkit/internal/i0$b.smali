.class public final Lcom/pspdfkit/internal/i0$b;
.super Lcom/pspdfkit/internal/yg;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/i0;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {}, Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$-CC;->builder()Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$Builder;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration$Builder;->build()Lcom/pspdfkit/annotations/configuration/EraserToolConfiguration;

    move-result-object p1

    const-string v0, "builder().build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

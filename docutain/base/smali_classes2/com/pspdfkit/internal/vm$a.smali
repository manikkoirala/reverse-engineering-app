.class final Lcom/pspdfkit/internal/vm$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/vm;->onImagePicked(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/forms/PushButtonFormElement;

.field final synthetic b:Lcom/pspdfkit/internal/wm;

.field final synthetic c:Lcom/pspdfkit/annotations/WidgetAnnotation;


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/PushButtonFormElement;Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/vm$a;->a:Lcom/pspdfkit/forms/PushButtonFormElement;

    iput-object p2, p0, Lcom/pspdfkit/internal/vm$a;->b:Lcom/pspdfkit/internal/wm;

    iput-object p3, p0, Lcom/pspdfkit/internal/vm$a;->c:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    const-string v0, "bitmap"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/pspdfkit/internal/vm$a;->a:Lcom/pspdfkit/forms/PushButtonFormElement;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/forms/PushButtonFormElement;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 169
    iget-object p1, p0, Lcom/pspdfkit/internal/vm$a;->b:Lcom/pspdfkit/internal/wm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wm;->b()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/vm$a;->c:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

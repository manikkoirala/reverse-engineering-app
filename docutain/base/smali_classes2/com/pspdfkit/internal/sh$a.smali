.class final Lcom/pspdfkit/internal/sh$a;
.super Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/sh;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/rxjava3/subscribers/DisposableSubscriber<",
        "Lcom/pspdfkit/internal/sh$b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/sh;


# direct methods
.method public static synthetic $r8$lambda$xArKk6sldjaqWGPQvzawJ7p49r8(Lcom/pspdfkit/internal/sh$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/sh$a;->a()V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/sh;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    invoke-direct {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;-><init>()V

    return-void
.end method

.method private synthetic a()V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    invoke-static {v0}, Lcom/pspdfkit/internal/sh;->a(Lcom/pspdfkit/internal/sh;)Lcom/pspdfkit/internal/im;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/im$g;->a:Lcom/pspdfkit/internal/im$g;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/im;->a(Lcom/pspdfkit/internal/im$g;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/sh$b;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    iget-object v2, p1, Lcom/pspdfkit/internal/sh$b;->a:Lcom/pspdfkit/internal/yh;

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/sh;->-$$Nest$fputf(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/yh;)V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v1

    .line 4
    invoke-virtual {v1}, Lcom/pspdfkit/internal/yl;->b()Lcom/pspdfkit/internal/sc;

    move-result-object v1

    iget-object v2, p1, Lcom/pspdfkit/internal/sh$b;->b:Lcom/pspdfkit/internal/rc;

    iget-object p1, p1, Lcom/pspdfkit/internal/sh$b;->a:Lcom/pspdfkit/internal/yh;

    .line 5
    invoke-virtual {v1, v2, p1}, Lcom/pspdfkit/internal/sc;->a(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yh;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    invoke-static {p1}, Lcom/pspdfkit/internal/sh;->-$$Nest$fgetg(Lcom/pspdfkit/internal/sh;)Lcom/pspdfkit/internal/yh;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->c()V

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/sh;->-$$Nest$fputg(Lcom/pspdfkit/internal/sh;Lcom/pspdfkit/internal/yh;)V

    .line 12
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/sh$a;->a:Lcom/pspdfkit/internal/sh;

    invoke-static {p1}, Lcom/pspdfkit/internal/sh;->-$$Nest$fgeti(Lcom/pspdfkit/internal/sh;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    .line 13
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/sh;->-$$Nest$fputi(Lcom/pspdfkit/internal/sh;Z)V

    .line 14
    new-instance v1, Lcom/pspdfkit/internal/sh$a$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/sh$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/sh$a;)V

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/sh;->-$$Nest$mb(Lcom/pspdfkit/internal/sh;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 16
    :cond_1
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/sh;->-$$Nest$mb(Lcom/pspdfkit/internal/sh;Ljava/lang/Runnable;)V

    .line 18
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final onComplete()V
    .locals 0

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to render low-res page image: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/sh$b;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/sh$a;->a(Lcom/pspdfkit/internal/sh$b;)V

    return-void
.end method

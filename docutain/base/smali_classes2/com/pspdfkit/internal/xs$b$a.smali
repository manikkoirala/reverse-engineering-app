.class public final Lcom/pspdfkit/internal/xs$b$a;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/xs$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

.field final synthetic c:Lcom/pspdfkit/internal/xs$b;


# direct methods
.method public static synthetic $r8$lambda$44Cod9fVbnGqi3_LCeuU5Zu8PSg(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/internal/xs;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/internal/xs;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/xs$b;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "root"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/xs$b$a;->c:Lcom/pspdfkit/internal/xs$b;

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iget-object p1, p1, Lcom/pspdfkit/internal/xs$b;->b:Lcom/pspdfkit/internal/xs;

    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    .line 3
    new-instance v1, Lcom/pspdfkit/internal/xs$b$a$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/xs$b$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/internal/xs;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p1, "root.findViewById<ImageV\u2026          }\n            }"

    .line 4
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/xs$b$a;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/internal/xs;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$1"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/xs$b$a;->b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/pspdfkit/internal/xs;->a(Lcom/pspdfkit/internal/xs;)Lcom/pspdfkit/internal/xs$a;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/xs$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/annotations/stamps/StampPickerItem;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 5

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/xs$b$a;->b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 36
    iget-object v2, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfWidth()F

    move-result v3

    float-to-int v3, v3

    .line 40
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfHeight()F

    move-result v4

    float-to-int v4, v4

    .line 41
    invoke-static {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 42
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 52
    :cond_2
    :goto_0
    invoke-virtual {p1, v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->createStampAnnotation(I)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object v0

    const-string v1, "item.createStampAnnotation(0)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v1, Lcom/pspdfkit/internal/vs;

    iget-object v2, p0, Lcom/pspdfkit/internal/xs$b$a;->c:Lcom/pspdfkit/internal/xs$b;

    invoke-static {v2}, Lcom/pspdfkit/internal/xs$b;->a(Lcom/pspdfkit/internal/xs$b;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/pspdfkit/internal/vs;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/StampAnnotation;)V

    .line 57
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    const-string v2, "annotation.boundingBox"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/internal/xs$b$a;->c:Lcom/pspdfkit/internal/xs$b;

    invoke-static {v2}, Lcom/pspdfkit/internal/xs$b;->a(Lcom/pspdfkit/internal/xs$b;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    .line 60
    iget-object v3, p0, Lcom/pspdfkit/internal/xs$b$a;->c:Lcom/pspdfkit/internal/xs$b;

    invoke-static {v3}, Lcom/pspdfkit/internal/xs$b;->a(Lcom/pspdfkit/internal/xs$b;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {v3, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    .line 61
    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/vs;->a(II)V

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getAppearanceStreamGenerator()Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 69
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->c:Lcom/pspdfkit/internal/xs$b;

    invoke-static {v0}, Lcom/pspdfkit/internal/xs$b;->a(Lcom/pspdfkit/internal/xs$b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->renderAppearanceStreamToBitmapAsync(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 70
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/pspdfkit/internal/xs$b$a$a;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/xs$b$a$a;-><init>(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 94
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

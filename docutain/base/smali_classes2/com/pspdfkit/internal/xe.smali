.class public final Lcom/pspdfkit/internal/xe;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/xe$c;,
        Lcom/pspdfkit/internal/xe$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/vf;

.field private final b:Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;

.field private final c:Lcom/pspdfkit/internal/xe$b;

.field private final d:Ljava/util/HashSet;


# direct methods
.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/xe;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/xe;->d:Ljava/util/HashSet;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/vf;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/xe;->d:Ljava/util/HashSet;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/xe;->a:Lcom/pspdfkit/internal/vf;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->getAssetManager()Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 15
    iput-object v0, p0, Lcom/pspdfkit/internal/xe;->b:Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/xe$b;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/xe$b;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/xe$b-IA;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/xe;->c:Lcom/pspdfkit/internal/xe$b;

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ef;->a(Lcom/pspdfkit/internal/s2;)V

    return-void

    .line 19
    :cond_0
    new-instance p1, Lcom/pspdfkit/instant/exceptions/InstantException;

    const-string v0, "Asset provider for Instant document was null! Contact PSPDFKit support to report this issue."

    invoke-direct {p1, v0, v1}, Lcom/pspdfkit/instant/exceptions/InstantException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method private static a(Lcom/pspdfkit/instant/internal/jni/NativeAsset;)Lcom/pspdfkit/internal/te;
    .locals 9

    .line 255
    new-instance v0, Lcom/pspdfkit/internal/te;

    .line 256
    invoke-virtual {p0}, Lcom/pspdfkit/instant/internal/jni/NativeAsset;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-virtual {p0}, Lcom/pspdfkit/instant/internal/jni/NativeAsset;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 258
    invoke-virtual {p0}, Lcom/pspdfkit/instant/internal/jni/NativeAsset;->getMimeType()Ljava/lang/String;

    .line 259
    invoke-virtual {p0}, Lcom/pspdfkit/instant/internal/jni/NativeAsset;->getLoadState()Lcom/pspdfkit/instant/internal/jni/NativeAssetLoadState;

    move-result-object p0

    .line 260
    sget-object v3, Lcom/pspdfkit/internal/xe$a;->b:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-eq v3, v8, :cond_4

    if-eq v3, v7, :cond_3

    if-eq v3, v6, :cond_2

    if-eq v3, v5, :cond_1

    if-ne v3, v4, :cond_0

    goto :goto_0

    .line 272
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v8, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "Conversion for NativeAssetLoadState (%s) is not implemented"

    .line 273
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v4, 0x4

    goto :goto_0

    :cond_2
    const/4 v4, 0x3

    goto :goto_0

    :cond_3
    const/4 v4, 0x2

    goto :goto_0

    :cond_4
    const/4 v4, 0x1

    .line 274
    :goto_0
    invoke-direct {v0, v1, v2, v4}, Lcom/pspdfkit/internal/te;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(Lcom/pspdfkit/internal/te;)[B
    .locals 6

    const-string v0, "asset"

    const-string v1, "argumentName"

    .line 59
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 110
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 111
    invoke-virtual {p0}, Lcom/pspdfkit/internal/te;->c()I

    move-result v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v2, :cond_1

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    goto :goto_0

    .line 127
    :cond_0
    new-instance v0, Lcom/pspdfkit/instant/exceptions/InstantException;

    sget-object v3, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    const-string p0, "The Instant asset has not been downloaded yet: %s"

    invoke-direct {v0, v3, p0, v2}, Lcom/pspdfkit/instant/exceptions/InstantException;-><init>(Lcom/pspdfkit/instant/exceptions/InstantErrorCode;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 128
    :cond_1
    :goto_0
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/te;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :try_start_1
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const v4, 0x19000

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const v4, 0xffff

    new-array v4, v4, [B

    .line 133
    :goto_1
    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-ltz v5, :cond_2

    .line 134
    invoke-virtual {v3, v4, v1, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 137
    :cond_2
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    return-object v3

    :catchall_0
    move-exception v3

    .line 139
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_4
    invoke-virtual {v3, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_2
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    .line 142
    new-instance v3, Lcom/pspdfkit/instant/exceptions/InstantException;

    sget-object v4, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_NOT_LOADED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    const-string p0, "Could not read backing data for Instant asset: %s"

    invoke-direct {v3, v4, v0, p0, v2}, Lcom/pspdfkit/instant/exceptions/InstantException;-><init>(Lcom/pspdfkit/instant/exceptions/InstantErrorCode;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v3
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/pspdfkit/internal/te;
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "identifier"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "identifier"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->b:Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;->assetForIdentifier(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->isError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeAsset;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/xe;->a(Lcom/pspdfkit/instant/internal/jni/NativeAsset;)Lcom/pspdfkit/internal/te;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 58
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a([B)Lcom/pspdfkit/internal/te;
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "data"

    const-string v1, "argumentName"

    .line 143
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    const/4 v1, 0x0

    .line 194
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "mimeType"

    const-string v2, "argumentName"

    .line 195
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "image/jpeg"

    const-string v2, "mimeType"

    .line 246
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->b:Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;

    const-string v1, "image/jpeg"

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetManager;->importData([BLjava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;

    move-result-object p1

    .line 248
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->isError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->value()Lcom/pspdfkit/instant/internal/jni/NativeAsset;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/xe;->a(Lcom/pspdfkit/instant/internal/jni/NativeAsset;)Lcom/pspdfkit/internal/te;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 252
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAssetResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    .line 253
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 254
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/xe;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/pspdfkit/internal/xe$c;)V
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->c:Lcom/pspdfkit/internal/xe$b;

    invoke-static {v0}, Lcom/pspdfkit/internal/xe$b;->-$$Nest$fgeta(Lcom/pspdfkit/internal/xe$b;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/xe$c;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->c:Lcom/pspdfkit/internal/xe$b;

    invoke-static {v0}, Lcom/pspdfkit/internal/xe$b;->-$$Nest$fgeta(Lcom/pspdfkit/internal/xe$b;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    const-string v0, "assetIdentifier"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assetIdentifier"

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->a:Lcom/pspdfkit/internal/vf;

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->scheduleDownloadOfAsset(Ljava/lang/String;)Lcom/pspdfkit/instant/internal/jni/NativeProgressReporterResult;

    move-result-object v0

    .line 56
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 57
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporterResult;->isError()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    invoke-virtual {v0}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporterResult;->error()Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_ALREADY_TRANSFERRED:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    if-ne v1, v2, :cond_0

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/xe;->c:Lcom/pspdfkit/internal/xe$b;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xe$b;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/instant/exceptions/InstantErrorCode;->ATTACHMENT_TRANSFER_IN_PROGRESS:Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    if-eq v1, v2, :cond_1

    .line 64
    monitor-enter p0

    .line 65
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/internal/xe;->d:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 66
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    iget-object v1, p0, Lcom/pspdfkit/internal/xe;->c:Lcom/pspdfkit/internal/xe$b;

    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/internal/xe$b;->a(Ljava/lang/String;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 68
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-void

    :catchall_1
    move-exception p1

    .line 69
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.class public final Lcom/pspdfkit/internal/gg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/JavaScriptAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    const-string v0, "documentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/gg;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-void
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 10

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    const-string v0, "action"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/gg;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->getScript()Ljava/lang/String;

    move-result-object v1

    const-string v2, "action.script"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_9

    .line 36
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ig;->isJavaScriptEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    :cond_0
    const-string v1, "<this>"

    if-eqz p2, :cond_8

    .line 40
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/ActionSender;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v5

    .line 41
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/actions/ActionSender;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object p2

    const-string v6, "document.asInternalDocument().javaScriptProvider"

    const-string v7, "PSPDFKit.JavaScript"

    if-eqz v5, :cond_2

    .line 42
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v8

    sget-object v9, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v8, v9, :cond_2

    .line 43
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p2

    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/pspdfkit/annotations/LinkAnnotation;

    .line 45
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v6

    if-nez v6, :cond_1

    new-array p2, v3, [Ljava/lang/Object;

    aput-object v5, p2, v4

    const-string v5, "Error executing javascript action for annotation %s. Annotation was not attached to document."

    .line 46
    invoke-static {v7, v5, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 49
    :cond_1
    check-cast p2, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p2, v5}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/annotations/LinkAnnotation;)Z

    :goto_0
    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    if-eqz p2, :cond_7

    .line 50
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v4

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v5

    const-string v6, "formElement.annotation"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v6

    invoke-interface {v6}, Lcom/pspdfkit/internal/pf;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v6

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v5, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_UP:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    goto :goto_1

    .line 54
    :cond_3
    invoke-static {}, Lcom/pspdfkit/internal/hg;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    .line 55
    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v8

    invoke-interface {v8, v7}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v8

    invoke-static {v8, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v5, v7

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_6

    .line 56
    check-cast v4, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v4, p2, v5}, Lcom/pspdfkit/internal/ig;->a(Lcom/pspdfkit/forms/FormElement;Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Z

    goto :goto_0

    .line 58
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->getScript()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v6, p2}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    check-cast v4, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v4, v5, v6}, Lcom/pspdfkit/internal/ig;->a(Ljava/lang/String;Lcom/pspdfkit/annotations/actions/ActionSender;)Z

    goto :goto_0

    :cond_7
    new-array p2, v4, [Ljava/lang/Object;

    const-string v5, "Trying to execute a JavaScript action on something that is not a form element is not supported yet."

    .line 59
    invoke-static {v7, v5, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    :goto_2
    if-nez v4, :cond_a

    .line 60
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->getScript()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/pspdfkit/internal/ig;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/ig;->a(Ljava/lang/String;)Z

    goto :goto_4

    :cond_9
    :goto_3
    const/4 v3, 0x0

    :cond_a
    :goto_4
    return v3
.end method

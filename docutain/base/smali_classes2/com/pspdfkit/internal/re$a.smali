.class final Lcom/pspdfkit/internal/re$a;
.super Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/re;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lio/reactivex/rxjava3/core/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/core/Observer<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/rxjava3/subjects/PublishSubject;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/re$a;->a:Lio/reactivex/rxjava3/core/Observer;

    return-void
.end method

.method synthetic constructor <init>(Lio/reactivex/rxjava3/subjects/PublishSubject;Lcom/pspdfkit/internal/re$a-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/re$a;-><init>(Lio/reactivex/rxjava3/subjects/PublishSubject;)V

    return-void
.end method


# virtual methods
.method public final onCancellation(Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 0

    return-void
.end method

.method public final onError(Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 0

    return-void
.end method

.method public final onProgress(Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/re$a;->a:Lio/reactivex/rxjava3/core/Observer;

    new-instance v1, Lcom/pspdfkit/instant/client/InstantProgress;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;->getCurrentProgress()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;->isInFinalState()Z

    move-result p1

    invoke-direct {v1, v2, p1}, Lcom/pspdfkit/instant/client/InstantProgress;-><init>(IZ)V

    .line 3
    invoke-interface {v0, v1}, Lio/reactivex/rxjava3/core/Observer;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final onSuccess(Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 0

    return-void
.end method

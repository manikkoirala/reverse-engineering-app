.class public final Lcom/pspdfkit/internal/zi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/aj$a;


# instance fields
.field private final a:Ljava/util/ArrayDeque;

.field private final b:Ljava/util/HashSet;

.field private final c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/zi;->d:Ljava/util/List;

    .line 15
    invoke-static {}, Lcom/pspdfkit/internal/v;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isPlayingMultipleMediaInstancesEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7fffffff

    .line 18
    new-instance v0, Landroid/media/MediaCodecList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/media/MediaCodecList;-><init>(I)V

    .line 19
    invoke-virtual {v0}, Landroid/media/MediaCodecList;->getCodecInfos()[Landroid/media/MediaCodecInfo;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 20
    invoke-virtual {v4}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_0

    aget-object v8, v5, v7

    .line 23
    invoke-virtual {v4, v8}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v8

    invoke-virtual {v8}, Landroid/media/MediaCodecInfo$CodecCapabilities;->getMaxSupportedInstances()I

    move-result v8

    .line 24
    invoke-static {p1, v8}, Ljava/lang/Math;->min(II)I

    move-result p1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 29
    :cond_1
    iput p1, p0, Lcom/pspdfkit/internal/zi;->c:I

    goto :goto_2

    :cond_2
    const/4 p1, 0x1

    .line 31
    iput p1, p0, Lcom/pspdfkit/internal/zi;->c:I

    .line 33
    :goto_2
    new-instance p1, Ljava/util/ArrayDeque;

    iget v0, p0, Lcom/pspdfkit/internal/zi;->c:I

    invoke-direct {p1, v0}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 8

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/wi;

    .line 4
    invoke-virtual {v2}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v3

    .line 5
    iget-object v4, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/yi;

    if-eqz v5, :cond_1

    .line 6
    invoke-virtual {v5}, Lcom/pspdfkit/internal/yi;->c()I

    move-result v6

    if-ne v6, v3, :cond_1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    .line 7
    new-instance v3, Lcom/pspdfkit/internal/xi;

    .line 8
    invoke-virtual {v2}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v4

    .line 9
    invoke-virtual {v2}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v6

    .line 10
    invoke-virtual {v2}, Lcom/pspdfkit/internal/wi;->g()Z

    move-result v7

    .line 11
    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/wi;)I

    move-result v2

    invoke-direct {v3, v4, v6, v2, v7}, Lcom/pspdfkit/internal/xi;-><init>(IIIZ)V

    .line 12
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/wi;)V
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/zi;->c:I

    if-le v0, v1, :cond_3

    .line 48
    iget-object p1, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/wi;

    .line 50
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    .line 51
    iget-object v1, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/yi;

    if-eqz v2, :cond_1

    .line 52
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yi;->c()I

    move-result v3

    if-ne v3, v0, :cond_1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_4

    .line 53
    invoke-virtual {v2, p1}, Lcom/pspdfkit/internal/yi;->d(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    .line 56
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/wi;->a(Z)V

    :cond_4
    :goto_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/yi;)V
    .locals 7

    .line 26
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/aj$a;)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 31
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object p1, p0, Lcom/pspdfkit/internal/zi;->d:Ljava/util/List;

    if-nez p1, :cond_0

    .line 33
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/yi;

    .line 36
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/xi;

    .line 38
    invoke-virtual {v4}, Lcom/pspdfkit/internal/xi;->b()I

    move-result v5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/yi;->c()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 39
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 42
    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 43
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/yi;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;)V"
        }
    .end annotation

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/zi;->d:Ljava/util/List;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    if-nez p1, :cond_0

    .line 15
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/yi;

    .line 18
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 19
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/xi;

    .line 20
    invoke-virtual {v4}, Lcom/pspdfkit/internal/xi;->b()I

    move-result v5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/yi;->c()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 21
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 24
    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 25
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/yi;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/wi;)V
    .locals 1

    const/4 v0, 0x0

    .line 3
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/wi;->a(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/yi;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/aj$a;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zi;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

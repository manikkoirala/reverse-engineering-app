.class public final Lcom/pspdfkit/internal/yj;
.super Lcom/pspdfkit/internal/jni/NativeSignatureContents;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/signatures/contents/SignatureContents;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/signatures/contents/SignatureContents;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativeSignatureContents;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/yj;->a:Lcom/pspdfkit/signatures/contents/SignatureContents;

    return-void
.end method


# virtual methods
.method public final signData([B)[B
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yj;->a:Lcom/pspdfkit/signatures/contents/SignatureContents;

    invoke-interface {v0, p1}, Lcom/pspdfkit/signatures/contents/SignatureContents;->signData([B)[B

    move-result-object p1

    const-string v0, "SignatureContents returned null array when signing."

    const-string v1, "message"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    return-object p1

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

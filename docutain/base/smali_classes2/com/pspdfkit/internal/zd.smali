.class public final Lcom/pspdfkit/internal/zd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/HashMap;

.field public static final synthetic b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/Pair;

    const-string v1, "com.facebook.react.ReactApplication"

    const-string v2, "com.facebook.react.bridge.ReactBridge"

    const-string v3, "com.pspdfkit.react.PSPDFKitPackage"

    const-string v4, "com.pspdfkit.react.PSPDFKitModule"

    .line 5
    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v1

    .line 6
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "ReactNative"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "io.flutter.view.FlutterView"

    const-string v2, "io.flutter.BuildConfig"

    const-string v3, "io.flutter.app.FlutterActivity"

    const-string v4, "com.pspdfkit.flutter.pspdfkit.PspdfkitPlugin"

    const-string v5, "com.pspdfkit.flutter.pspdfkit.FlutterPdfActivity"

    .line 17
    filled-new-array {v1, v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v1

    .line 18
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Flutter"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "org.apache.cordova.CordovaPlugin"

    const-string v2, "org.apache.cordova.BuildConfig"

    const-string v3, "org.apache.cordova.CordovaActivity"

    .line 28
    filled-new-array {v1, v2, v3}, [Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Cordova"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "mono.android.Runtime"

    const-string v2, "mono.MonoRuntimeProvider"

    const-string v3, "com.xamarin.forms.platform.android.FormsViewGroup"

    const-string v4, "com.xamarin.java_interop.internal.JavaProxyObject"

    .line 38
    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Xamarin"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 40
    invoke-static {v0}, Lkotlin/collections/MapsKt;->hashMapOf([Lkotlin/Pair;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/zd;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static final synthetic a()Ljava/util/HashMap;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/zd;->a:Ljava/util/HashMap;

    return-object v0
.end method

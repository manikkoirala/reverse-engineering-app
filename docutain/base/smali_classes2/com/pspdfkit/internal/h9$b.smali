.class final Lcom/pspdfkit/internal/h9$b;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/h9;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/h9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/h9;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/h9$b;->a:Lcom/pspdfkit/internal/h9;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h9$b;->a:Lcom/pspdfkit/internal/h9;

    invoke-static {v0}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/h9;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isTextSelectionPopupToolbarEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 4
    :cond_0
    new-instance v0, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/h9$b;->a:Lcom/pspdfkit/internal/h9;

    invoke-static {v1}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/h9;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/toolbar/popup/PdfTextSelectionPopupToolbar;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    :goto_0
    return-object v0
.end method

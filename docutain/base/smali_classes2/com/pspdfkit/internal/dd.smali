.class public final Lcom/pspdfkit/internal/dd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/GoToAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/ui/navigation/PageNavigator;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/ui/navigation/PageNavigator;)V
    .locals 1

    const-string v0, "navigator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/dd;->a:Lcom/pspdfkit/ui/navigation/PageNavigator;

    return-void
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/GoToAction;

    const-string p2, "action"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/GoToAction;->getPageIndex()I

    move-result p1

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-ltz p1, :cond_1

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/internal/dd;->a:Lcom/pspdfkit/ui/navigation/PageNavigator;

    invoke-interface {v1}, Lcom/pspdfkit/ui/navigation/PageNavigator;->getPageCount()I

    move-result v1

    sub-int/2addr v1, p2

    if-le p1, v1, :cond_0

    goto :goto_0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/dd;->a:Lcom/pspdfkit/ui/navigation/PageNavigator;

    invoke-interface {v0}, Lcom/pspdfkit/ui/navigation/PageNavigator;->beginNavigation()V

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/dd;->a:Lcom/pspdfkit/ui/navigation/PageNavigator;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/navigation/PageNavigator;->setPageIndex(I)V

    .line 33
    iget-object p1, p0, Lcom/pspdfkit/internal/dd;->a:Lcom/pspdfkit/ui/navigation/PageNavigator;

    invoke-interface {p1}, Lcom/pspdfkit/ui/navigation/PageNavigator;->endNavigation()V

    goto :goto_1

    :cond_1
    :goto_0
    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "PSPDFKit.ActionResolver"

    const-string v1, "Go to page action executed, but the target page doesn\'t exist in the current document."

    .line 34
    invoke-static {p2, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p2, 0x0

    :goto_1
    return p2
.end method

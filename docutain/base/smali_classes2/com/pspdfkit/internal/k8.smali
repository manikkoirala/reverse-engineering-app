.class public final Lcom/pspdfkit/internal/k8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/editor/PdfDocumentEditor;


# static fields
.field static final synthetic e:Z = true


# instance fields
.field private a:Ljava/util/HashSet;

.field private final b:Lcom/pspdfkit/internal/zf;

.field private c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

.field private d:Ljava/lang/Integer;


# direct methods
.method public static synthetic $r8$lambda$4GyUKfEZPu-Jnz8KbbR8iRGkKEw(Lcom/pspdfkit/internal/k8;Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$8V0pDN1KmEewzavsscq8aSwoeIU(Lcom/pspdfkit/internal/k8;ILjava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(ILjava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$DfAj4YuY0gf5CmSizgcnCqIhyh0(Lcom/pspdfkit/internal/k8;Ljava/util/Set;I)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->b(Ljava/util/Set;I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$E7UoN8W7glqcGjVtQDLX01JwPw8(Lcom/pspdfkit/internal/k8;Ljava/util/Set;I)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/Set;I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$EKvFGp3d_J35w7LwtjUH7p67r8E(Lcom/pspdfkit/internal/k8;Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/util/HashSet;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/k8;->a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$Kh04Bp93e6m13WwX4DhK5NcGDKQ(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static synthetic $r8$lambda$UsE7vEWZG_8nM0WrQsO1T_VisXc(Lcom/pspdfkit/internal/k8;Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->b(Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$bDycienINbe-QG-HPgUyzBSVBA0(Lcom/pspdfkit/internal/k8;Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$c99tKWDIgqJ3-00WkE3fQ67XpFI(Lcom/pspdfkit/internal/k8;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/k8;->b(Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fXOvVjLvjq4VEAJuCVsXEfAnCtg(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/document/PdfDocument;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/k8;->a(ILcom/pspdfkit/document/PdfDocument;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$fpzrl4LcTn-jVmcncSer5uPWXWI(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/document/processor/NewPage;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(ILcom/pspdfkit/document/processor/NewPage;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$kSX-uBFwRhPptOpNIfYcNVDrbeY(Lcom/pspdfkit/internal/k8;Ljava/util/Set;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->b(Ljava/util/Set;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$n3LScXhss2l_hCP961Qs1wbBsyI(Lcom/pspdfkit/internal/k8;Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$raw-Q1HjQE2tqqIBrw0ABWk_jdE(Lcom/pspdfkit/internal/k8;Ljava/util/Set;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/Set;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$xAfW2hNICHSJGGLtJ98Q-BwOBjQ(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/k8;->a(ILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$zoQqGn_AuABlnvJ0K4UMqqnjFS0(Lcom/pspdfkit/internal/k8;Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iput-object p1, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    return-void

    .line 10
    :cond_0
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your current license does not allow editing of PDF documents."

    invoke-direct {p1, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 289
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v2, v1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/util/List;Z)V

    .line 290
    sget-boolean v1, Lcom/pspdfkit/internal/k8;->e:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 291
    :cond_1
    :goto_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    .line 292
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 293
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/k8;->b(Ljava/lang/String;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 294
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    .line 295
    new-instance p1, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {p1, v0}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    return-object p1
.end method

.method private synthetic a(Ljava/lang/String;)Lcom/pspdfkit/document/DocumentSource;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string v0, "PSPDFKit.DocumentEditor"

    .line 296
    iget-object v1, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    .line 297
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/providers/WritableDataProvider;

    .line 298
    sget-object v2, Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;->REWRITE_FILE:Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;

    invoke-interface {v1, v2}, Lcom/pspdfkit/document/providers/WritableDataProvider;->startWrite(Lcom/pspdfkit/document/providers/WritableDataProvider$WriteMode;)Z

    const/4 v2, 0x0

    .line 300
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 301
    invoke-static {v3, v1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/FileInputStream;Lcom/pspdfkit/document/providers/WritableDataProvider;)V

    .line 302
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    const-string v3, "Error while writing."

    new-array v2, v2, [Ljava/lang/Object;

    .line 306
    invoke-static {v0, p1, v3, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p1

    const-string v3, "Error while opening cached file."

    new-array v2, v2, [Ljava/lang/Object;

    .line 307
    invoke-static {v0, p1, v3, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    :goto_0
    invoke-interface {v1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->finishWrite()Z

    .line 313
    new-instance p1, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {p1, v1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    return-object p1

    .line 314
    :goto_1
    invoke-interface {v1}, Lcom/pspdfkit/document/providers/WritableDataProvider;->finishWrite()Z

    .line 315
    throw p1
.end method

.method private static a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocument;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 204
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 207
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    const/4 v4, 0x1

    if-lt v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-array v0, v4, [Landroid/net/Uri;

    .line 208
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v0, v2

    .line 209
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Ljava/util/List;Z)V

    .line 210
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_1

    .line 213
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 217
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 218
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    goto :goto_1

    .line 221
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    .line 229
    :try_start_0
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 231
    :try_start_1
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :try_start_2
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 233
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    goto :goto_5

    :catchall_0
    move-exception p0

    .line 234
    :try_start_3
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    :try_start_4
    invoke-virtual {p0, p1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_2
    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception p0

    .line 238
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 239
    throw p0

    .line 240
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to open document source with Uri: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 250
    :cond_4
    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object p1

    .line 251
    sget-boolean v0, Lcom/pspdfkit/internal/k8;->e:Z

    if-nez v0, :cond_6

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0

    .line 252
    :cond_6
    :goto_3
    :try_start_5
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 253
    :try_start_6
    invoke-interface {p1}, Lcom/pspdfkit/document/providers/DataProvider;->getSize()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    :goto_4
    cmp-long v7, v5, v3

    if-gez v7, :cond_7

    sub-long v7, v3, v5

    const-wide/32 v9, 0xffff

    .line 255
    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    long-to-int v8, v7

    int-to-long v11, v8

    .line 256
    invoke-interface {p1, v11, v12, v5, v6}, Lcom/pspdfkit/document/providers/DataProvider;->read(JJ)[B

    move-result-object v7

    .line 257
    invoke-virtual {v0, v7, v2, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    add-long/2addr v5, v9

    goto :goto_4

    .line 258
    :cond_7
    :try_start_7
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 259
    invoke-interface {p1}, Lcom/pspdfkit/document/providers/DataProvider;->release()V

    .line 264
    :goto_5
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object p0

    return-object p0

    :catchall_3
    move-exception p0

    .line 265
    :try_start_8
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    goto :goto_6

    :catchall_4
    move-exception v0

    :try_start_9
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_6
    throw p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :catchall_5
    move-exception p0

    .line 269
    invoke-interface {p1}, Lcom/pspdfkit/document/providers/DataProvider;->release()V

    .line 270
    throw p0

    .line 271
    :cond_8
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Failed to create temporary destination path."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private synthetic a(Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 419
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/k8;->b(Ljava/lang/String;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 316
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->saveIfModified()Z

    const-string v0, "pdf"

    .line 317
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-nez p2, :cond_0

    .line 318
    iget-object p2, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    .line 319
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object p2

    :cond_0
    const/4 v1, 0x0

    .line 320
    invoke-virtual {p2, v1}, Lcom/pspdfkit/document/DocumentSaveOptions;->setIncremental(Z)V

    .line 321
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    .line 322
    invoke-static {p2, v2, v0}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    move-result-object p2

    .line 323
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 324
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->writeToFilePath(Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "Saved edited file to "

    .line 325
    invoke-virtual {p2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.DocumentEditor"

    invoke-static {v1, p2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object p2

    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-object p1

    .line 328
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to save file to new destination."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 329
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to create temporary file."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/util/HashSet;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 387
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->saveIfModified()Z

    const-string v0, "pdf"

    .line 388
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/kb;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    .line 389
    iget-object p2, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x1

    .line 390
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->a(Z)Lcom/pspdfkit/document/DocumentSaveOptions;

    move-result-object p2

    :cond_0
    const/4 v0, 0x0

    .line 391
    invoke-virtual {p2, v0}, Lcom/pspdfkit/document/DocumentSaveOptions;->setIncremental(Z)V

    .line 392
    iget-object v1, p0, Lcom/pspdfkit/internal/k8;->a:Ljava/util/HashSet;

    if-nez v1, :cond_1

    .line 393
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/k8;->a:Ljava/util/HashSet;

    goto :goto_0

    .line 395
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 398
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/k8;->a:Ljava/util/HashSet;

    invoke-interface {v1, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 399
    invoke-virtual {p0, p2, p4, p1}, Lcom/pspdfkit/internal/k8;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    const-string p2, "Exported file to "

    .line 401
    invoke-virtual {p2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-array p3, v0, [Ljava/lang/Object;

    const-string p4, "PSPDFKit.DocumentEditor"

    invoke-static {p4, p2, p3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 403
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object p2

    iget-object p3, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p2

    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Completable;->blockingAwait()V

    return-object p1

    .line 404
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to create temporary file."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private synthetic a(ILcom/pspdfkit/document/PdfDocument;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 187
    :goto_0
    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 189
    invoke-static {p2, v0}, Lcom/pspdfkit/document/processor/NewPage;->fromPage(Lcom/pspdfkit/document/PdfDocument;I)Lcom/pspdfkit/document/processor/NewPage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/document/processor/NewPage$Builder;->build()Lcom/pspdfkit/document/processor/NewPage;

    move-result-object v1

    .line 190
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/pspdfkit/internal/k8;->addPages(ILjava/util/List;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p4, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 196
    invoke-interface {p2}, Lcom/pspdfkit/document/PdfDocument;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/document/DocumentSource;->getFileUri()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 198
    new-instance p2, Ljava/io/File;

    invoke-static {p5, p1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 201
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_1
    return-object p4
.end method

.method private a(ILcom/pspdfkit/document/processor/NewPage;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 164
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 165
    invoke-virtual {p2}, Lcom/pspdfkit/document/processor/NewPage;->getNativeNewPageConfiguration()Lcom/pspdfkit/internal/jni/NativeNewPageConfiguration;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->addPage(ILcom/pspdfkit/internal/jni/NativeNewPageConfiguration;)Ljava/util/ArrayList;

    move-result-object p1

    .line 166
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 167
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private a(ILjava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 168
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 169
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 170
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/document/processor/NewPage;

    .line 171
    invoke-virtual {v2}, Lcom/pspdfkit/document/processor/NewPage;->getNativeNewPageConfiguration()Lcom/pspdfkit/internal/jni/NativeNewPageConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_0
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->addPages(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    .line 173
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 174
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/Set;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 175
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 176
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->duplicatePages(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object p1

    .line 177
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 178
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/Set;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 179
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 180
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->movePages(Ljava/util/HashSet;I)Ljava/util/ArrayList;

    move-result-object p1

    .line 181
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 182
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private a(ILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;Landroid/graphics/Bitmap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 272
    new-instance v0, Lcom/pspdfkit/internal/l8$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    const/4 v2, 0x1

    .line 273
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v3

    .line 274
    invoke-direct {v0, v1, p1, v3}, Lcom/pspdfkit/internal/l8$a;-><init>(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    const/16 v1, 0xa

    .line 275
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/l8$a;

    .line 276
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/g4$a;->a(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    .line 277
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    .line 278
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    .line 279
    invoke-virtual {p2}, Lcom/pspdfkit/internal/l8$a;->b()Lcom/pspdfkit/internal/l8;

    move-result-object p2

    .line 280
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 281
    sget-object v1, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 285
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    move-result-object p2

    .line 286
    invoke-virtual {v0, p1, p3, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->render(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;)V

    return-void
.end method

.method private static synthetic a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Source document is an URI, copy "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.DocumentEditor"

    invoke-static {v3, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 408
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 409
    :try_start_1
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/kb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 411
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    new-array p0, v1, [Ljava/lang/Object;

    const-string p1, "Export OK."

    .line 413
    invoke-static {v3, p1, p0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p0

    .line 414
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_4
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception p0

    .line 417
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 418
    throw p0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 7

    .line 421
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/jni/NativeEditingChange;

    .line 422
    iget-object v3, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    if-nez v3, :cond_1

    return-void

    .line 423
    :cond_1
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getAffectedPageIndex()I

    move-result v3

    if-eqz v1, :cond_2

    .line 427
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getOperation()Lcom/pspdfkit/internal/jni/NativeEditingOperation;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeEditingOperation;->MOVE:Lcom/pspdfkit/internal/jni/NativeEditingOperation;

    if-eq v4, v5, :cond_2

    .line 428
    iput-object v1, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    move-object v1, v0

    .line 432
    :cond_2
    sget-object v4, Lcom/pspdfkit/internal/k8$a;->a:[I

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getOperation()Lcom/pspdfkit/internal/jni/NativeEditingOperation;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_3

    goto :goto_0

    .line 449
    :cond_3
    iget-object v4, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 450
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeEditingChange;->getPageIndexDestination()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 451
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt v3, v2, :cond_0

    .line 452
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 453
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 454
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 455
    :cond_6
    iget-object v2, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v3, v2, :cond_0

    .line 456
    iput-object v0, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    if-eqz v1, :cond_8

    .line 479
    iput-object v1, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    :cond_8
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    const-string v0, "cachedDocumentPath"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "destinationUri"

    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 105
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda9;-><init>(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    return-object p0
.end method

.method private synthetic b(Ljava/io/OutputStream;Ljava/lang/String;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 121
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/k8;->b(Ljava/lang/String;Ljava/io/OutputStream;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/util/Set;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 106
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 107
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->removePages(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object p1

    .line 108
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 109
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/util/Set;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 110
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->rotatePagesBy(Ljava/util/HashSet;I)Ljava/util/ArrayList;

    move-result-object p1

    .line 112
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private b(Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x1

    .line 115
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/tj;->a(Lcom/pspdfkit/document/DocumentSaveOptions;Lcom/pspdfkit/internal/zf;Z)Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;

    move-result-object p1

    .line 116
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 117
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 118
    invoke-virtual {v0, v2, p3, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->exportPagesToFilePath(Ljava/util/HashSet;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDocumentSaveOptions;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 120
    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-array p2, v1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p3, p2, v0

    const-string p3, "Failed to export file to new destination: %s."

    invoke-static {p3, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/jni/NativeDocumentEditor;
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    const/4 v1, 0x0

    .line 163
    iput-object v1, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    return-object v0
.end method

.method public final declared-synchronized a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;
    .locals 1

    monitor-enter p0

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 184
    iget-object p1, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->EditDocument(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 186
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method final a(Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    const-string v0, "pageIndexes"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 52
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 53
    invoke-static {v0, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    const-string v0, "filePath"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "options"

    .line 106
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 158
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/k8;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/content/Context;Ljava/util/Set;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "pageIndexes"

    const-string v1, "argumentName"

    .line 331
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 382
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 383
    invoke-static {v0, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 385
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 386
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/k8;Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;Ljava/util/Set;Ljava/util/HashSet;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;)V
    .locals 0

    if-nez p1, :cond_0

    .line 159
    iget-object p1, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->EditDocument(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    goto :goto_0

    .line 161
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/k8;->c:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    :goto_0
    return-void
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    return-void
.end method

.method public final addPage(ILcom/pspdfkit/document/processor/NewPage;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/pspdfkit/document/processor/NewPage;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    if-gt p1, v0, :cond_0

    const-string v0, "newPageConfiguration"

    const-string v1, "argumentName"

    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 60
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 61
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda14;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/document/processor/NewPage;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 62
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid page destination index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page destination indexes are [0, "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final addPages(ILjava/util/List;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/processor/NewPage;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    if-ltz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    if-gt p1, v0, :cond_0

    const-string v0, "newPageConfigurations"

    const-string v1, "argumentName"

    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 60
    invoke-static {p2, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "newPageConfigurations may not be empty."

    .line 61
    invoke-static {v0, p2}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 62
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/k8;ILjava/util/List;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 63
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid page destination index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page destination indexes are [0, "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method final b(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/k8;Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/util/HashSet;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method public final beginTransaction()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->beginUpdates()V

    return-void
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public final canRedo()Z
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->canRedo()Z

    move-result v0

    return v0
.end method

.method public final canUndo()Z
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->canUndo()Z

    move-result v0

    return v0
.end method

.method public final commitTransaction()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->commitUpdates()Ljava/util/ArrayList;

    move-result-object v0

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final discardTransaction()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->discardUpdates()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final duplicatePages(Ljava/util/Set;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    const-string v0, "pageIndexes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 55
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda15;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/internal/k8;Ljava/util/Set;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final exportPages(Landroid/content/Context;Ljava/io/OutputStream;Ljava/util/Set;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/io/OutputStream;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/pspdfkit/document/DocumentSaveOptions;",
            ")",
            "Lio/reactivex/rxjava3/core/Completable;"
        }
    .end annotation

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "outputStream"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes"

    .line 108
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 160
    invoke-static {v0, p3}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 161
    invoke-virtual {p0, p1, p3, p4}, Lcom/pspdfkit/internal/k8;->a(Landroid/content/Context;Ljava/util/Set;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda8;

    invoke-direct {p3, p0, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/k8;Ljava/io/OutputStream;)V

    .line 162
    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final getDocument()Lcom/pspdfkit/document/PdfDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public final getPageCount()I
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->getPageCount()I

    move-result v0

    return v0
.end method

.method public final getRotatedPageSize(I)Lcom/pspdfkit/utils/Size;
    .locals 4

    const/4 v0, 0x1

    if-ltz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-gt p1, v1, :cond_0

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->getRotatedPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid page destination index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page destination indexes are [0, "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v0

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final importDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;I)Lio/reactivex/rxjava3/core/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/DocumentSource;",
            "I)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    if-ltz p3, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    if-gt p3, v0, :cond_0

    const-string v0, "documentSource"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "context"

    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 107
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 112
    :try_start_0
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/k8;->a(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSource;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    new-instance p2, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda10;

    move-object v3, p2

    move-object v4, p0

    move v5, p3

    move-object v9, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/document/PdfDocument;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/Context;)V

    invoke-static {p2}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string p3, "PSPDFKit.DocumentEditor"

    const-string v0, "Can\'t extract document to import."

    .line 119
    invoke-static {p3, p1, v0, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    invoke-static {v8}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 121
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Invalid page destination index "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " - valid page destination indexes are [0, "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "]"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final isTransactionActive()Z
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->isInsideUpdateGroup()Z

    move-result v0

    return v0
.end method

.method public final movePages(Ljava/util/Set;I)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    const-string v0, "fromPositions"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "fromPositions may not be empty."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    if-ltz p2, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    if-gt p2, v0, :cond_0

    .line 62
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/k8;Ljava/util/Set;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 63
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid page destination index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " - valid page destination indexes are [0, "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "]"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final redo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->redo()Ljava/util/ArrayList;

    move-result-object v0

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final removePages(Ljava/util/Set;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    const-string v0, "pageIndexes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    .line 55
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda13;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/internal/k8;Ljava/util/Set;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final renderPageToBitmap(ILandroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lio/reactivex/rxjava3/core/Completable;
    .locals 4

    if-ltz p1, :cond_2

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_2

    const-string v0, "buffer"

    const-string v1, "argumentName"

    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 60
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "configuration"

    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p3, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 114
    iget-object v0, p3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->reuseBitmap:Landroid/graphics/Bitmap;

    const-string v1, "PSPDFKit.DocumentEditor"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Object;

    const-string v3, "configuration reuseBitmap is not supported and will be ignored."

    .line 115
    invoke-static {v1, v3, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    :cond_0
    iget-boolean v0, p3, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->renderRegion:Z

    if-eqz v0, :cond_1

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "configuration renderRegion is not supported and will be ignored."

    .line 118
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/k8;ILcom/pspdfkit/configuration/rendering/PageRenderConfiguration;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 121
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "Invalid page destination index "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page destination indexes are [0, "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final rotatePages(Ljava/util/Set;I)Lio/reactivex/rxjava3/core/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;>;"
        }
    .end annotation

    const-string v0, "pageIndexes"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "pageIndexes may not be empty."

    .line 54
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/String;Ljava/util/Collection;)V

    if-eqz p2, :cond_1

    const/16 v0, 0x5a

    if-eq p2, v0, :cond_1

    const/16 v1, 0xb4

    if-eq p2, v1, :cond_1

    const/16 v2, 0x10e

    if-ne p2, v2, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    .line 57
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x2

    aput-object p2, v3, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x3

    aput-object p2, v3, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x4

    aput-object p2, v3, v0

    const-string p2, "Illegal page rotation: %d. Page rotation may be one the following: %d, %d, %d, %d"

    .line 58
    invoke-static {p2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 59
    :cond_1
    :goto_0
    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/k8;Ljava/util/Set;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final saveDocument(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->isFileSource()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->b(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda11;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/k8;Landroid/content/Context;)V

    .line 56
    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->ignoreElement()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    instance-of v0, v0, Lcom/pspdfkit/document/providers/WritableDataProvider;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/k8;->b:Lcom/pspdfkit/internal/zf;

    .line 68
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSource()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/document/DocumentSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/document/providers/WritableDataProvider;

    invoke-interface {v0}, Lcom/pspdfkit/document/providers/WritableDataProvider;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/k8;->b(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda12;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/k8;)V

    .line 70
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Single;->ignoreElement()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 89
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Saving document in place can be applied only when the source is a file Uri or a data provider that supports saving."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final saveDocument(Landroid/content/Context;Ljava/io/OutputStream;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Completable;
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 91
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 142
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "destinationUri"

    .line 144
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 196
    invoke-virtual {p0, p1, p3}, Lcom/pspdfkit/internal/k8;->b(Landroid/content/Context;Lcom/pspdfkit/document/DocumentSaveOptions;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p3, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda5;

    invoke-direct {p3, p0, p2}, Lcom/pspdfkit/internal/k8$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/k8;Ljava/io/OutputStream;)V

    .line 197
    invoke-virtual {p1, p3}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final setPageLabel(ILjava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    if-ltz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-gt p1, v1, :cond_0

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->setPageLabel(ILjava/lang/String;)V

    return-void

    .line 4
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page destination index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - valid page destination indexes are [0, "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/k8;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v0

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final undo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/undo/EditingChange;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Z)Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->undo()Ljava/util/ArrayList;

    move-result-object v0

    .line 3
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/k8;->a(Ljava/util/ArrayList;)V

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/sj;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/zc;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/util/List<",
        "+",
        "Lcom/pspdfkit/internal/qt;",
        ">;",
        "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/utils/Size;


# direct methods
.method constructor <init>(Lcom/pspdfkit/utils/Size;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/zc;->a:Lcom/pspdfkit/utils/Size;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 1
    check-cast p1, Ljava/util/List;

    check-cast p2, Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    const-string v0, "list"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 1>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object p2, p0, Lcom/pspdfkit/internal/zc;->a:Lcom/pspdfkit/utils/Size;

    const-string v0, "blocks"

    .line 51
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/qt;

    .line 112
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 113
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object v2

    invoke-virtual {v1}, Lcom/pspdfkit/internal/qt;->c()Lcom/pspdfkit/internal/tt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/tt;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v1

    invoke-virtual {v2, p2, v1}, Lcom/pspdfkit/internal/bv;->a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/iv;)V

    goto :goto_0

    .line 145
    :cond_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

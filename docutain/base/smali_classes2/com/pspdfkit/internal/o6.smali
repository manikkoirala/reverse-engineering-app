.class public final Lcom/pspdfkit/internal/o6;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/jni/NativeContentEditingResult;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/pspdfkit/internal/jni/NativeContentEditingResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            ")V"
        }
    .end annotation

    const-string v0, "nativeResult"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/o6;->a:Ljava/lang/Object;

    iput-object p2, p0, Lcom/pspdfkit/internal/o6;->b:Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o6;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/jni/NativeContentEditingResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o6;->b:Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o6;->b:Lcom/pspdfkit/internal/jni/NativeContentEditingResult;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeContentEditingResult;->getError()Lcom/pspdfkit/internal/jni/NativeContentEditingError;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    return v0
.end method

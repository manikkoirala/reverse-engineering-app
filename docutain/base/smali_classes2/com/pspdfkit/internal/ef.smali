.class public final Lcom/pspdfkit/internal/ef;
.super Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerDelegate;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/vf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/instant/listeners/InstantDocumentListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/e2;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/s2;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/instant/document/InstantDocumentState;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/vf;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerDelegate;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    .line 11
    sget-object v0, Lcom/pspdfkit/instant/document/InstantDocumentState;->UNKNOWN:Lcom/pspdfkit/instant/document/InstantDocumentState;

    iput-object v0, p0, Lcom/pspdfkit/internal/ef;->e:Lcom/pspdfkit/instant/document/InstantDocumentState;

    .line 15
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ef;->a:Ljava/lang/ref/WeakReference;

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->setDelegate(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayerDelegate;)V

    return-void
.end method

.method private a()Lcom/pspdfkit/internal/e2;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/e2;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private b()Lcom/pspdfkit/internal/wf;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/vf;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->f()Lcom/pspdfkit/internal/wf;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method final a(Lcom/pspdfkit/instant/exceptions/InstantSyncException;)V
    .locals 3

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 10
    invoke-interface {v2, v0, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncError(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method final a(Lcom/pspdfkit/internal/e2;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/ef;->c:Ljava/lang/ref/WeakReference;

    goto :goto_0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ef;->c:Ljava/lang/ref/WeakReference;

    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/s2;)V
    .locals 1

    .line 6
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ef;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public final b(Lcom/pspdfkit/instant/listeners/InstantDocumentListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method final c()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/wf;->getDocumentState()Lcom/pspdfkit/instant/document/InstantDocumentState;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/ef;->e:Lcom/pspdfkit/instant/document/InstantDocumentState;

    if-ne v2, v1, :cond_1

    return-void

    .line 6
    :cond_1
    iput-object v1, p0, Lcom/pspdfkit/internal/ef;->e:Lcom/pspdfkit/instant/document/InstantDocumentState;

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 10
    invoke-interface {v3, v0, v1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentStateChanged(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/document/InstantDocumentState;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method final d()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 6
    invoke-interface {v2, v0}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final didBeginLoadingAsset(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ef;->d:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/s2;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 2
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/s2;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final didBeginReceivingData(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/internal/e2;->a()V

    :cond_0
    return-void
.end method

.method public final didBeginSendingAssetData(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 0

    return-void
.end method

.method public final didBeginSyncCycle(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/internal/e2;->b()V

    :cond_0
    return-void
.end method

.method public final didBeginTransfer(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1, p2, p4, p3}, Lcom/pspdfkit/internal/e2;->a(Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V

    :cond_0
    return-void
.end method

.method public final didDetectCorruption(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentCorrupted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final didFailLoadingAsset(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ef;->d:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/s2;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 2
    invoke-interface {p1, p2, p3}, Lcom/pspdfkit/internal/s2;->a(Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V

    :cond_1
    return-void
.end method

.method public final didFailSendingAssetData(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 0

    return-void
.end method

.method public final didFailSyncing(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/e2;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V

    :cond_0
    return-void
.end method

.method public final didFailUpdatingAuthenticationToken(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 2

    .line 1
    new-instance p1, Lcom/pspdfkit/instant/exceptions/InstantException;

    .line 2
    invoke-virtual {p2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getCode()Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;)Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v0

    .line 3
    invoke-virtual {p2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getUnderlyingError()Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {p1, v0, v1, p2}, Lcom/pspdfkit/instant/exceptions/InstantException;-><init>(Lcom/pspdfkit/instant/exceptions/InstantErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 8
    invoke-interface {v1, p2, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFailed(Lcom/pspdfkit/instant/document/InstantPdfDocument;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final didFinishLoadingAsset(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeAsset;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ef;->d:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/s2;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 2
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/s2;->a(Lcom/pspdfkit/instant/internal/jni/NativeAsset;)V

    :cond_1
    return-void
.end method

.method public final didFinishSendingAssetData(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final didFinishSyncing(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/internal/e2;->c()V

    :cond_0
    return-void
.end method

.method public final didUpdateAuthenticationToken(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;",
            "Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/instant/internal/jni/NativeLayerCapabilities;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/wf;->b(Ljava/util/EnumSet;)V

    .line 4
    invoke-virtual {p2}, Lcom/pspdfkit/instant/internal/jni/NativeInstantJWT;->rawValue()Ljava/lang/String;

    move-result-object p2

    .line 5
    iget-object p3, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 6
    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onAuthenticationFinished(Lcom/pspdfkit/instant/document/InstantPdfDocument;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method final e()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 4
    invoke-interface {v2, v0}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onSyncStarted(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final isBecomingInvalid(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->b()Lcom/pspdfkit/internal/wf;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/wf;->setListenToServerChanges(Z)V

    const-wide v0, 0x7fffffffffffffffL

    .line 5
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/wf;->setDelayForSyncingLocalChanges(J)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ef;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/instant/listeners/InstantDocumentListener;->onDocumentInvalidated(Lcom/pspdfkit/instant/document/InstantPdfDocument;)V

    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ef;->c()V

    :cond_1
    return-void
.end method

.method public final wantsToApplyChanges(Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicator;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ef;->a()Lcom/pspdfkit/internal/e2;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/e2;->a(Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicator;)V

    :cond_0
    return-void
.end method

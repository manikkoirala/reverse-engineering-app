.class public final Lcom/pspdfkit/internal/uh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field final b:Landroid/widget/Magnifier;

.field final c:Lcom/pspdfkit/internal/lt;

.field final d:Z

.field private final e:Z

.field private final f:F

.field private final g:F

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "View to magnify may not be null."

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "PdfConfiguration may not be null."

    .line 3
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/uh;->a:Landroid/view/View;

    .line 5
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isMagnifierEnabled()Z

    move-result p2

    iput-boolean p2, p0, Lcom/pspdfkit/internal/uh;->e:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 12
    new-instance p2, Landroid/widget/Magnifier;

    invoke-direct {p2, p1}, Landroid/widget/Magnifier;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    .line 13
    iput-object v1, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    goto :goto_0

    .line 15
    :cond_0
    new-instance p2, Lcom/pspdfkit/internal/lt;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/lt;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    .line 16
    iput-object v1, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    .line 19
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/internal/uh;->b()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/uh;->f:F

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/uh;->c()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/uh;->g:F

    .line 23
    invoke-direct {p0}, Lcom/pspdfkit/internal/uh;->k()V

    goto :goto_1

    .line 25
    :cond_2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    .line 26
    iput-object v1, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    .line 27
    iput-object v1, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    const/4 p1, 0x0

    .line 28
    iput p1, p0, Lcom/pspdfkit/internal/uh;->f:F

    .line 29
    iput p1, p0, Lcom/pspdfkit/internal/uh;->g:F

    :goto_1
    return-void
.end method

.method private k()V
    .locals 1

    const/high16 v0, 0x3fa00000    # 1.25f

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/uh;->a(F)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uh;->h:Z

    .line 21
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0}, Landroid/widget/Magnifier;->dismiss()V

    goto :goto_0

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->a()V

    :goto_0
    const/high16 v0, 0x3fa00000    # 1.25f

    .line 25
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/uh;->a(F)V

    return-void
.end method

.method public final a(F)V
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    return-void

    .line 31
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0, p1}, Landroid/widget/Magnifier;->setZoom(F)V

    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/lt;->a(F)V

    :goto_0
    return-void
.end method

.method public final a(FF)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uh;->h:Z

    .line 7
    iget v0, p0, Lcom/pspdfkit/internal/uh;->f:F

    .line 8
    iget v1, p0, Lcom/pspdfkit/internal/uh;->g:F

    .line 9
    iget-boolean v2, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v2, :cond_2

    .line 10
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    if-lt v2, v3, :cond_1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    add-float/2addr v0, p1

    add-float/2addr v1, p2

    invoke-virtual {v2, p1, p2, v0, v1}, Landroid/widget/Magnifier;->show(FFFF)V

    goto :goto_0

    .line 12
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0, p1, p2}, Landroid/widget/Magnifier;->show(FF)V

    goto :goto_0

    .line 16
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/lt;->a(FF)V

    :goto_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    return-void

    .line 28
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-nez v0, :cond_1

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/lt;->a(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method public final b()F
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_2

    .line 3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1d

    if-lt v0, v2, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0}, Landroid/widget/Magnifier;->getDefaultHorizontalSourceToMagnifierOffset()I

    move-result v0

    int-to-float v0, v0

    return v0

    :cond_1
    return v1

    .line 7
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->b()F

    move-result v0

    return v0
.end method

.method public final c()F
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_2

    .line 3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0}, Landroid/widget/Magnifier;->getDefaultVerticalSourceToMagnifierOffset()I

    move-result v0

    int-to-float v0, v0

    return v0

    :cond_1
    const/high16 v0, -0x3d120000    # -119.0f

    return v0

    .line 7
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->c()F

    move-result v0

    return v0
.end method

.method public final d()Landroid/graphics/Point;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0}, Landroid/widget/Magnifier;->getPosition()Landroid/graphics/Point;

    move-result-object v0

    return-object v0

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->d()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->a:Landroid/view/View;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->b:Landroid/widget/Magnifier;

    invoke-virtual {v0}, Landroid/widget/Magnifier;->getWidth()I

    move-result v0

    return v0

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->e()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->h:Z

    return v0
.end method

.method public final i()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-nez v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->f()V

    :cond_1
    return-void
.end method

.method public final j()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->e:Z

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/uh;->d:Z

    if-nez v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uh;->c:Lcom/pspdfkit/internal/lt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/lt;->g()V

    :cond_1
    return-void
.end method

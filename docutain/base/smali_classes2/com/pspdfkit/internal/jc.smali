.class final Lcom/pspdfkit/internal/jc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/jc$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/text/DecimalFormat;

.field private static final b:Ljava/text/DecimalFormat;

.field private static final c:Ljava/text/DecimalFormat;

.field private static final d:Ljava/text/DecimalFormat;

.field private static final e:Ljava/text/DecimalFormat;

.field public static final synthetic f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "# "

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/jc;->a:Ljava/text/DecimalFormat;

    .line 2
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.# "

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/pspdfkit/internal/jc;->b:Ljava/text/DecimalFormat;

    .line 3
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "#.## "

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/pspdfkit/internal/jc;->c:Ljava/text/DecimalFormat;

    .line 4
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "#.### "

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/pspdfkit/internal/jc;->d:Ljava/text/DecimalFormat;

    .line 5
    new-instance v4, Ljava/text/DecimalFormat;

    const-string v5, "#.#### "

    invoke-direct {v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/pspdfkit/internal/jc;->e:Ljava/text/DecimalFormat;

    .line 8
    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v5}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 9
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 10
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 11
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 12
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    return-void
.end method

.method public static final synthetic a()Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jc;->e:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method public static final synthetic b()Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jc;->b:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method public static final synthetic c()Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jc;->d:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method public static final synthetic d()Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jc;->c:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method public static final synthetic e()Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jc;->a:Ljava/text/DecimalFormat;

    return-object v0
.end method

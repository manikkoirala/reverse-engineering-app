.class public final Lcom/pspdfkit/internal/vk;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/wk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/zk;

.field private final c:Ljava/util/ArrayList;

.field private final d:Lcom/pspdfkit/internal/gk;

.field private e:Z

.field private f:Z

.field private g:Lcom/pspdfkit/internal/ik;

.field private h:Lcom/pspdfkit/internal/jk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/internal/vk;->e:Z

    const/4 v1, 0x0

    .line 13
    iput-boolean v1, p0, Lcom/pspdfkit/internal/vk;->f:Z

    const/4 v1, 0x0

    .line 16
    iput-object v1, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    .line 24
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    .line 25
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->a:Landroid/content/Context;

    .line 26
    new-instance p1, Lcom/pspdfkit/internal/zk;

    invoke-direct {p1}, Lcom/pspdfkit/internal/zk;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    .line 27
    new-instance p1, Lcom/pspdfkit/internal/gk;

    invoke-direct {p1}, Lcom/pspdfkit/internal/gk;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->d:Lcom/pspdfkit/internal/gk;

    return-void
.end method

.method private c()V
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 9
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->a(Ljava/lang/Integer;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->e:Z

    add-int/2addr p1, v0

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jk;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->h:Lcom/pspdfkit/internal/jk;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->a(Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->a(Ljava/util/List;)V

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;Z)V"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    if-eqz p2, :cond_0

    .line 11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ik;

    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    .line 14
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/vk;->getItemCount()I

    move-result v0

    .line 22
    iget-boolean v1, p0, Lcom/pspdfkit/internal/vk;->f:Z

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 25
    iput-boolean v1, p0, Lcom/pspdfkit/internal/vk;->f:Z

    .line 26
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 28
    iput-boolean p1, p0, Lcom/pspdfkit/internal/vk;->f:Z

    sub-int/2addr v0, v1

    .line 29
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    :goto_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->a:Landroid/content/Context;

    const/4 v1, 0x0

    .line 6
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    .line 7
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vk;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/ik;)V
    .locals 2

    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 13
    iget-boolean p1, p0, Lcom/pspdfkit/internal/vk;->e:Z

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->b(Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->b(Ljava/util/List;)V

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/vk;->e:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zk;->f()Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    .line 7
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->e:Z

    add-int/2addr p1, v0

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    return-void
.end method

.method public final c(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zk;->a(Z)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/vk;->c()V

    return-void
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zk;->f()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/vk;->c(Z)V

    return-void
.end method

.method public final getItemCount()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->e:Z

    iget-object v1, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, v0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->f:Z

    add-int/2addr v1, v0

    return v1
.end method

.method public final getItemId(I)J
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/vk;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    int-to-long v0, p1

    return-wide v0

    .line 5
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->f:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/vk;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const-wide/16 v0, -0x3

    return-wide v0

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    .line 9
    iget-boolean v1, p0, Lcom/pspdfkit/internal/vk;->e:Z

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ik;

    .line 10
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->getId()J

    move-result-wide v0

    return-wide v0

    :cond_2
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 0

    if-nez p1, :cond_0

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/vk;->e:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/wk;

    .line 2
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/vk;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_2

    .line 9
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/y5;

    .line 12
    iget-boolean v0, p0, Lcom/pspdfkit/internal/vk;->f:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/vk;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p2, v0, :cond_1

    .line 13
    iget-object p2, p0, Lcom/pspdfkit/internal/vk;->d:Lcom/pspdfkit/internal/gk;

    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->c:Ljava/util/ArrayList;

    iget-boolean v2, p0, Lcom/pspdfkit/internal/vk;->e:Z

    sub-int/2addr p2, v2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ik;

    .line 17
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->h:Lcom/pspdfkit/internal/jk;

    iget-object v2, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    if-ne v2, p2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1, p2, v0, v1}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;Z)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    if-ne p1, p2, :cond_4

    const/4 p1, 0x0

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/vk;->g:Lcom/pspdfkit/internal/ik;

    goto :goto_2

    .line 21
    :cond_3
    check-cast p1, Lcom/pspdfkit/internal/ht;

    .line 23
    iget-object p2, p0, Lcom/pspdfkit/internal/vk;->b:Lcom/pspdfkit/internal/zk;

    iget-object v0, p0, Lcom/pspdfkit/internal/vk;->h:Lcom/pspdfkit/internal/jk;

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/ht;->a(Lcom/pspdfkit/internal/zk;Lcom/pspdfkit/internal/jk;)V

    :cond_4
    :goto_2
    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/y5;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__note_editor_item_card_layout:I

    .line 2
    invoke-virtual {v1, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/y5;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Lcom/pspdfkit/internal/ht;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__note_editor_style_box_card_layout:I

    .line 4
    invoke-virtual {v1, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/ht;-><init>(Landroid/view/View;)V

    :goto_0
    return-object p2
.end method

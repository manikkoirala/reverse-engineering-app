.class public final Lcom/pspdfkit/internal/u3;
.super Lcom/pspdfkit/internal/g2;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g2<",
        "Lcom/pspdfkit/internal/t3;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/qf;",
            "Landroid/util/SparseIntArray;",
            "Lcom/pspdfkit/internal/k4$a<",
            "-",
            "Lcom/pspdfkit/internal/t3;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/t3;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/pspdfkit/internal/g2;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    return-void
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/t3;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 3
    instance-of p1, p1, Lcom/pspdfkit/annotations/SoundAnnotation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/t3;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 3
    instance-of p1, p1, Lcom/pspdfkit/annotations/SoundAnnotation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final f(Lcom/pspdfkit/internal/ja;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/t3;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 3
    instance-of v1, v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v1, :cond_0

    .line 4
    check-cast v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    iget-object p1, p1, Lcom/pspdfkit/internal/t3;->c:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    .line 5
    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/SoundAnnotation;->setAudioSource(Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    .line 10
    new-instance v0, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;

    const-string v1, "Could not perform redo operation."

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final g(Lcom/pspdfkit/internal/ja;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/t3;

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 3
    instance-of v0, p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    if-eqz v0, :cond_0

    .line 4
    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/SoundAnnotation;->setAudioSource(Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    .line 10
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    const-string v1, "Could not perform undo operation."

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.class public final Lcom/pspdfkit/internal/lt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/dq;

.field private final b:Landroid/view/View;

.field private c:I

.field private d:F

.field private final e:Landroid/graphics/Path;

.field private final f:Landroid/graphics/RectF;

.field private final g:Landroid/graphics/Matrix;

.field private final h:[I

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private final m:F

.field private final n:F

.field private final o:F

.field private p:Z

.field private final q:Landroid/graphics/Paint;

.field private r:Landroid/graphics/Bitmap;


# direct methods
.method public static synthetic $r8$lambda$J6_9uwhssa_BPp5FjeU64VGHTCY(Lcom/pspdfkit/internal/lt;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/lt;->a(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p0

    return-object p0
.end method

.method constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/lt;->c:I

    const/high16 v1, 0x3fa00000    # 1.25f

    .line 4
    iput v1, p0, Lcom/pspdfkit/internal/lt;->d:F

    .line 7
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/lt;->e:Landroid/graphics/Path;

    .line 11
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    .line 12
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/lt;->g:Landroid/graphics/Matrix;

    const/4 v1, 0x2

    new-array v2, v1, [I

    .line 14
    iput-object v2, p0, Lcom/pspdfkit/internal/lt;->h:[I

    .line 33
    iput-boolean v0, p0, Lcom/pspdfkit/internal/lt;->p:Z

    .line 37
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/pspdfkit/internal/lt;->q:Landroid/graphics/Paint;

    const-string v1, "View to magnify may not be null."

    .line 43
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    .line 47
    new-instance v1, Lcom/pspdfkit/internal/lt$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/lt$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/lt;)V

    invoke-static {p1, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 56
    new-instance v2, Lcom/pspdfkit/internal/dq;

    invoke-direct {v2, p1}, Lcom/pspdfkit/internal/dq;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/lt;->a:Lcom/pspdfkit/internal/dq;

    const/16 p1, 0x64

    .line 57
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr p1, v2

    iput p1, p0, Lcom/pspdfkit/internal/lt;->m:F

    const/16 p1, 0x30

    .line 58
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v2

    iput p1, p0, Lcom/pspdfkit/internal/lt;->n:F

    .line 60
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/lt;->k:F

    const/16 p1, -0x2a

    .line 61
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/lt;->l:F

    const/16 p1, 0x26

    .line 62
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/lt;->o:F

    return-void
.end method

.method private synthetic a(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->getDisplayCutout()Landroidx/core/view/DisplayCutoutCompat;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Landroidx/core/view/DisplayCutoutCompat;->getSafeInsetTop()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/lt;->c:I

    .line 5
    :cond_0
    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->consumeStableInsets()Landroidx/core/view/WindowInsetsCompat;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/pspdfkit/internal/lt;->p:Z

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final a(F)V
    .locals 0

    .line 6
    iput p1, p0, Lcom/pspdfkit/internal/lt;->d:F

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final a(FF)V
    .locals 5

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 9
    iget-boolean v1, p0, Lcom/pspdfkit/internal/lt;->p:Z

    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    const/4 v3, -0x1

    .line 14
    invoke-virtual {v0, v3}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 16
    invoke-virtual {v0, v2}, Landroid/view/View;->buildDrawingCache(Z)V

    const/4 v3, 0x0

    .line 17
    iput-boolean v3, p0, Lcom/pspdfkit/internal/lt;->p:Z

    .line 18
    invoke-virtual {v0, v3}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 19
    iput-boolean v1, p0, Lcom/pspdfkit/internal/lt;->p:Z

    if-eqz v4, :cond_0

    .line 23
    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 25
    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 26
    iput-object v1, p0, Lcom/pspdfkit/internal/lt;->r:Landroid/graphics/Bitmap;

    .line 27
    iput-boolean v2, p0, Lcom/pspdfkit/internal/lt;->p:Z

    float-to-int p1, p1

    int-to-float p1, p1

    .line 28
    iput p1, p0, Lcom/pspdfkit/internal/lt;->i:F

    float-to-int p1, p2

    int-to-float p1, p1

    .line 29
    iput p1, p0, Lcom/pspdfkit/internal/lt;->j:F

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 7

    .line 33
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lt;->p:Z

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->r:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 45
    iget v0, p0, Lcom/pspdfkit/internal/lt;->i:F

    .line 46
    iget v1, p0, Lcom/pspdfkit/internal/lt;->m:F

    iget v2, p0, Lcom/pspdfkit/internal/lt;->k:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/pspdfkit/internal/lt;->m:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/pspdfkit/internal/lt;->k:F

    sub-float/2addr v2, v3

    .line 47
    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 48
    iget v1, p0, Lcom/pspdfkit/internal/lt;->j:F

    .line 49
    iget v2, p0, Lcom/pspdfkit/internal/lt;->n:F

    iget v3, p0, Lcom/pspdfkit/internal/lt;->l:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/pspdfkit/internal/lt;->c:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/pspdfkit/internal/lt;->n:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/pspdfkit/internal/lt;->l:F

    sub-float/2addr v3, v4

    .line 50
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 51
    iget-object v2, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    iget v3, p0, Lcom/pspdfkit/internal/lt;->m:F

    sub-float v4, v0, v3

    iget v5, p0, Lcom/pspdfkit/internal/lt;->n:F

    sub-float v6, v1, v5

    add-float/2addr v0, v3

    add-float/2addr v1, v5

    invoke-virtual {v2, v4, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/pspdfkit/internal/lt;->k:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/pspdfkit/internal/lt;->l:F

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->a:Lcom/pspdfkit/internal/dq;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, p1, v2, v1}, Lcom/pspdfkit/internal/dq;->a(Landroid/graphics/Canvas;FF)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 69
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->e:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/lt;->o:F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->e:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 80
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->h:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    .line 82
    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->h:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/lt;->k:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    iget-object v3, p0, Lcom/pspdfkit/internal/lt;->h:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/lt;->l:F

    add-float/2addr v1, v3

    .line 83
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 89
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 90
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->g:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/pspdfkit/internal/lt;->d:F

    iget v3, p0, Lcom/pspdfkit/internal/lt;->i:F

    iget-object v5, p0, Lcom/pspdfkit/internal/lt;->h:[I

    aget v2, v5, v2

    int-to-float v2, v2

    add-float/2addr v3, v2

    iget v2, p0, Lcom/pspdfkit/internal/lt;->j:F

    aget v4, v5, v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    invoke-virtual {v0, v1, v1, v3, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 95
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->r:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->g:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/pspdfkit/internal/lt;->q:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final b()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lt;->k:F

    return v0
.end method

.method public final c()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lt;->l:F

    return v0
.end method

.method public final d()Landroid/graphics/Point;
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public final e()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->f:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->a:Lcom/pspdfkit/internal/dq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dq;->b()V

    return-void
.end method

.method public final g()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lt;->a:Lcom/pspdfkit/internal/dq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dq;->c()V

    return-void
.end method

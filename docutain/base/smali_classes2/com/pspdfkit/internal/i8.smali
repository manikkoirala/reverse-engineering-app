.class public final Lcom/pspdfkit/internal/i8;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lcom/pspdfkit/internal/i8;

.field private static c:Ljava/lang/String;

.field private static d:Lcom/pspdfkit/internal/do;


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;


# direct methods
.method private constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 7
    sget-object v1, Lcom/pspdfkit/internal/i8;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 8
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "pspdfkit_data.db"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 9
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/i8;->c:Ljava/lang/String;

    .line 12
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 14
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unable to create a default file for document data store."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 18
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Document data store located at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/pspdfkit/internal/i8;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Document"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    sget-object v0, Lcom/pspdfkit/internal/i8;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;->create(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->getHasError()Z

    move-result v1

    if-nez v1, :cond_2

    .line 24
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->getDocumentDataStore()Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/internal/i8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    return-void

    .line 29
    :cond_1
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Could not initialize data store."

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    new-instance v1, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->getErrorString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStoreCreateResult;->getErrorCode()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 31
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must initialize PSPDFKit before accessing data store."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized a()Lcom/pspdfkit/internal/i8;
    .locals 2

    const-class v0, Lcom/pspdfkit/internal/i8;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/i8;->b:Lcom/pspdfkit/internal/i8;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/i8;

    invoke-direct {v1}, Lcom/pspdfkit/internal/i8;-><init>()V

    sput-object v1, Lcom/pspdfkit/internal/i8;->b:Lcom/pspdfkit/internal/i8;

    .line 4
    :goto_0
    sget-object v1, Lcom/pspdfkit/internal/i8;->b:Lcom/pspdfkit/internal/i8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized b()Lio/reactivex/rxjava3/core/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/internal/i8;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/internal/i8;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/internal/i8;->d:Lcom/pspdfkit/internal/do;

    if-nez v1, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    const-string v2, "pspdfkit-data-store"

    const/4 v3, 0x1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "threadName"

    .line 3
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v1, Lcom/pspdfkit/internal/do;

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/internal/do;-><init>(Ljava/lang/String;I)V

    .line 91
    sput-object v1, Lcom/pspdfkit/internal/i8;->d:Lcom/pspdfkit/internal/do;

    .line 93
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/i8$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/pspdfkit/internal/i8$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/i8;->d:Lcom/pspdfkit/internal/do;

    const/4 v3, 0x5

    .line 94
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/h8;
    .locals 2

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getUid()Ljava/lang/String;

    move-result-object p1

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/h8;

    iget-object v1, p0, Lcom/pspdfkit/internal/i8;->a:Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;->get(Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeDocumentData;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/h8;-><init>(Lcom/pspdfkit/internal/jni/NativeDocumentDataStore;Lcom/pspdfkit/internal/jni/NativeDocumentData;)V

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/i7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/h7;


# instance fields
.field private final a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

.field private final b:Lcom/pspdfkit/internal/fl;

.field private final c:Lcom/pspdfkit/internal/b0;

.field private final d:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private f:Lcom/pspdfkit/internal/zf;

.field private g:Landroid/graphics/PointF;

.field private h:I


# direct methods
.method public static synthetic $r8$lambda$3QHwQXiiankPEbAr33SFfIRxnrc(Lcom/pspdfkit/internal/i7;Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i7;->c(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$3hOCNNfZMGNzo2tHrwm1-4t6gOI(Lcom/pspdfkit/internal/i7;I)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i7;->b(I)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$3odB-dQjtqoQIEL2la5iMeLpSJQ(Lcom/pspdfkit/internal/i7;Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/i7;->d(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$pXGs_KD04eueUwuQY0ukX24MEXA(Lcom/pspdfkit/internal/i7;ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/i7;->b(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/vu;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->d()Lcom/pspdfkit/internal/b0;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/pspdfkit/internal/i7;->h:I

    .line 25
    invoke-static {p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/i7;->a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    .line 26
    iput-object p2, p0, Lcom/pspdfkit/internal/i7;->e:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 27
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledCopyPasteFeatures()Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/i7;->d:Ljava/util/EnumSet;

    .line 28
    iput-object p3, p0, Lcom/pspdfkit/internal/i7;->b:Lcom/pspdfkit/internal/fl;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;ILandroid/graphics/PointF;)V
    .locals 7

    .line 145
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 147
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/pspdfkit/internal/pf;->setPageIndex(I)V

    .line 148
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 152
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 155
    iget v1, p3, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget p3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    sub-float/2addr p3, v2

    invoke-virtual {v0, v1, p3}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 158
    iget-object p3, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p3

    .line 159
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p3, Lcom/pspdfkit/utils/Size;->width:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 160
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v2, v1

    .line 163
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    neg-float v1, v1

    mul-float v1, v1, v2

    .line 166
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v4, p3, Lcom/pspdfkit/utils/Size;->width:F

    sub-float/2addr v2, v4

    div-float/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v1

    div-float/2addr v4, v3

    invoke-virtual {v0, v2, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 169
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v2, p3, Lcom/pspdfkit/utils/Size;->height:F

    neg-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 170
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v2, v1

    .line 171
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float v1, v1, v2

    .line 172
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v2, v1

    div-float/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v4, p3, Lcom/pspdfkit/utils/Size;->height:F

    add-float/2addr v1, v4

    div-float/2addr v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 175
    :cond_2
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    .line 176
    iput p2, p0, Lcom/pspdfkit/internal/i7;->h:I

    .line 178
    iget p2, v0, Landroid/graphics/RectF;->left:F

    const/4 v1, 0x0

    cmpg-float v2, p2, v1

    if-gez v2, :cond_3

    neg-float p2, p2

    .line 179
    invoke-virtual {v0, p2, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 181
    :cond_3
    iget p2, v0, Landroid/graphics/RectF;->bottom:F

    const v2, 0x3e4ccccd    # 0.2f

    cmpg-float v4, p2, v1

    if-gez v4, :cond_4

    neg-float p2, p2

    .line 182
    invoke-virtual {v0, v1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 185
    iget-object p2, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    iget v4, p3, Lcom/pspdfkit/utils/Size;->height:F

    .line 186
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float v6, v6, v2

    sub-float/2addr v5, v6

    add-float/2addr v5, v4

    iput v5, p2, Landroid/graphics/PointF;->y:F

    .line 188
    :cond_4
    iget p2, v0, Landroid/graphics/RectF;->right:F

    iget v4, p3, Lcom/pspdfkit/utils/Size;->width:F

    cmpl-float v5, p2, v4

    if-lez v5, :cond_5

    sub-float/2addr p2, v4

    neg-float p2, p2

    .line 189
    invoke-virtual {v0, p2, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 192
    iget-object p2, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float v3, v3, v2

    sub-float/2addr v4, v3

    iput v4, p2, Landroid/graphics/PointF;->x:F

    .line 194
    :cond_5
    iget p2, v0, Landroid/graphics/RectF;->top:F

    iget p3, p3, Lcom/pspdfkit/utils/Size;->height:F

    cmpl-float v2, p2, p3

    if-lez v2, :cond_6

    sub-float/2addr p2, p3

    neg-float p2, p2

    .line 195
    invoke-virtual {v0, v1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 198
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/annotations/Annotation;->updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 199
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 202
    iget-object p2, p0, Lcom/pspdfkit/internal/i7;->b:Lcom/pspdfkit/internal/fl;

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method private b(I)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "pasteAnnotation() may not be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->d:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;->CROSS_DOCUMENT_COPY_PASTE:Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getAnnotationCreator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/b0;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    goto :goto_1

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b0;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 15
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getAnnotationCreator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/b0;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    .line 17
    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/pspdfkit/internal/i7;->h:I

    if-eq v2, p1, :cond_3

    goto :goto_2

    .line 22
    :cond_3
    new-instance v2, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    .line 23
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    const v5, 0x3e4ccccd    # 0.2f

    mul-float v4, v4, v5

    add-float/2addr v4, v3

    iget-object v3, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 24
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float v1, v1, v5

    add-float/2addr v1, v3

    invoke-direct {v2, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_3

    .line 25
    :cond_4
    :goto_2
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 32
    :goto_3
    invoke-direct {p0, v0, p1, v2}, Lcom/pspdfkit/internal/i7;->a(Lcom/pspdfkit/annotations/Annotation;ILandroid/graphics/PointF;)V

    :cond_5
    if-eqz v0, :cond_6

    .line 33
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_4

    :cond_6
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method private b(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 34
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "pasteAnnotation() may not be called from the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->d:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;->CROSS_DOCUMENT_COPY_PASTE:Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getAnnotationCreator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/b0;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    goto :goto_1

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b0;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 40
    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 43
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    iget-object v2, p0, Lcom/pspdfkit/internal/i7;->a:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getAnnotationCreator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/b0;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    .line 44
    invoke-direct {p0, v0, p1, p2}, Lcom/pspdfkit/internal/i7;->a(Lcom/pspdfkit/annotations/Annotation;ILandroid/graphics/PointF;)V

    :cond_3
    if-eqz v0, :cond_4

    .line 45
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_2

    :cond_4
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private c(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/internal/b0;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/i7;->h:I

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    .line 5
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    goto :goto_2

    .line 7
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation could not be copied."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private d(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/CompletableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/internal/b0;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->b:Lcom/pspdfkit/internal/fl;

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    const/4 p1, -0x1

    .line 5
    iput p1, p0, Lcom/pspdfkit/internal/i7;->h:I

    const/4 p1, 0x0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/i7;->g:Landroid/graphics/PointF;

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 7
    invoke-static {}, Lio/reactivex/rxjava3/core/Completable;->complete()Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    goto :goto_2

    .line 8
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation could not be cut."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    :goto_2
    return-object p1
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation could not be copied."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 3
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/i7;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 6
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a(I)Lio/reactivex/rxjava3/core/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 133
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 134
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/i7;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 138
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILandroid/graphics/PointF;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/PointF;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 140
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/i7;ILandroid/graphics/PointF;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    const/4 v0, 0x5

    .line 144
    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method public final a()Z
    .locals 4

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/i7;->e:Lcom/pspdfkit/configuration/PdfConfiguration;

    monitor-enter v0

    :try_start_0
    const-string v2, "configuration"

    .line 8
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    sget-object v2, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationEditingEnabled()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    if-nez v1, :cond_1

    return v3

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->d:Ljava/util/EnumSet;

    sget-object v1, Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;->CROSS_DOCUMENT_COPY_PASTE:Lcom/pspdfkit/configuration/annotations/CopyPasteFeatures;

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b0;->c()Z

    move-result v0

    return v0

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b0;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 131
    iget-object v1, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    return v3

    .line 132
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->c:Lcom/pspdfkit/internal/b0;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/b0;->c()Z

    move-result v0

    return v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Annotation could not be cut."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->error(Ljava/lang/Throwable;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/i7$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/i7;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/i7;->f:Lcom/pspdfkit/internal/zf;

    const/4 v1, 0x5

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

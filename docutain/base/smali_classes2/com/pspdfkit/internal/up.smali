.class public final Lcom/pspdfkit/internal/up;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/z1;

.field private final b:F

.field private final c:F

.field private final d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Path;

.field private f:Lcom/pspdfkit/internal/z1$b;

.field private final g:Lcom/pspdfkit/internal/lo;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/z1;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/lo;

    invoke-direct {v0}, Lcom/pspdfkit/internal/lo;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/x5;->a()Lcom/pspdfkit/internal/f2;

    move-result-object v0

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    .line 12
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getResizeGuideSnapAllowance()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/up;->b:F

    .line 13
    iget p1, v0, Lcom/pspdfkit/internal/f2;->h:I

    int-to-float p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/up;->c:F

    .line 15
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/up;->d:Landroid/graphics/Paint;

    .line 16
    iget v1, v0, Lcom/pspdfkit/internal/f2;->g:I

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 17
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 18
    iget v0, v0, Lcom/pspdfkit/internal/f2;->f:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 21
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getGuideLineIntervals()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    new-array v0, p1, [F

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 23
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->getGuideLineIntervals()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/up;->d:Landroid/graphics/Paint;

    new-instance p2, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x0

    invoke-direct {p2, v0, v1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    return-void
.end method

.method private static a(Lcom/pspdfkit/internal/z1$b;Lcom/pspdfkit/internal/lo;Landroid/graphics/RectF;)Z
    .locals 12

    .line 168
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->c()Landroid/graphics/RectF;

    move-result-object v0

    .line 169
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->b()Landroid/graphics/RectF;

    move-result-object v1

    .line 171
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch p0, :pswitch_data_0

    goto/16 :goto_0

    .line 224
    :pswitch_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float p0, p0, v4

    if-gez p0, :cond_4

    .line 226
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v4

    .line 227
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v5

    .line 228
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v6

    .line 229
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v7

    iget v8, p2, Landroid/graphics/RectF;->left:F

    iget v11, p2, Landroid/graphics/RectF;->bottom:F

    iget v10, p2, Landroid/graphics/RectF;->right:F

    move v9, v11

    .line 230
    invoke-static/range {v4 .. v11}, Lcom/pspdfkit/internal/di;->a(FFFFFFFF)Landroid/graphics/PointF;

    move-result-object p0

    .line 239
    iget p1, p0, Landroid/graphics/PointF;->y:F

    iget p2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr p1, p2

    iput p1, v1, Landroid/graphics/RectF;->bottom:F

    .line 240
    iget p0, p0, Landroid/graphics/PointF;->x:F

    iget p1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr p0, p1

    iput p0, v1, Landroid/graphics/RectF;->right:F

    goto/16 :goto_1

    .line 241
    :pswitch_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float p0, p0, v4

    if-gez p0, :cond_4

    .line 243
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v4

    .line 244
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v5

    .line 245
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v6

    .line 246
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v7

    iget v8, p2, Landroid/graphics/RectF;->left:F

    iget v11, p2, Landroid/graphics/RectF;->bottom:F

    iget v10, p2, Landroid/graphics/RectF;->right:F

    move v9, v11

    .line 247
    invoke-static/range {v4 .. v11}, Lcom/pspdfkit/internal/di;->a(FFFFFFFF)Landroid/graphics/PointF;

    move-result-object p0

    .line 256
    iget p1, p0, Landroid/graphics/PointF;->y:F

    iget p2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr p1, p2

    iput p1, v1, Landroid/graphics/RectF;->bottom:F

    .line 257
    iget p0, p0, Landroid/graphics/PointF;->x:F

    iget p1, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p0, p1

    iput p0, v1, Landroid/graphics/RectF;->left:F

    goto/16 :goto_1

    .line 258
    :pswitch_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->top:F

    cmpl-float p0, p0, v4

    if-lez p0, :cond_4

    .line 260
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v4

    .line 261
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v5

    .line 262
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v6

    .line 263
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v7

    iget v8, p2, Landroid/graphics/RectF;->left:F

    iget v11, p2, Landroid/graphics/RectF;->top:F

    iget v10, p2, Landroid/graphics/RectF;->right:F

    move v9, v11

    .line 264
    invoke-static/range {v4 .. v11}, Lcom/pspdfkit/internal/di;->a(FFFFFFFF)Landroid/graphics/PointF;

    move-result-object p0

    .line 273
    iget p1, p0, Landroid/graphics/PointF;->y:F

    iget p2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr p1, p2

    iput p1, v1, Landroid/graphics/RectF;->top:F

    .line 274
    iget p0, p0, Landroid/graphics/PointF;->x:F

    iget p1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr p0, p1

    iput p0, v1, Landroid/graphics/RectF;->right:F

    goto/16 :goto_1

    .line 334
    :pswitch_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->top:F

    cmpl-float p0, p0, v4

    if-lez p0, :cond_0

    .line 335
    iget p0, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, p0

    iput v4, v1, Landroid/graphics/RectF;->top:F

    const/4 v3, 0x1

    .line 339
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float p0, p0, v4

    if-gez p0, :cond_1

    .line 340
    iget p0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, p0

    iput v4, v1, Landroid/graphics/RectF;->bottom:F

    const/4 v3, 0x1

    .line 344
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->e()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->left:F

    cmpg-float p0, p0, v4

    if-gez p0, :cond_2

    .line 345
    iget p0, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, p0

    iput v4, v1, Landroid/graphics/RectF;->left:F

    const/4 v3, 0x1

    .line 349
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->f()F

    move-result p0

    iget p1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float p0, p0, p1

    if-lez p0, :cond_3

    .line 350
    iget p0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr p1, p0

    iput p1, v1, Landroid/graphics/RectF;->right:F

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_1

    .line 351
    :pswitch_4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result p0

    iget v4, p2, Landroid/graphics/RectF;->top:F

    cmpl-float p0, p0, v4

    if-lez p0, :cond_4

    .line 353
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v4

    .line 354
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v5

    .line 355
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v6

    .line 356
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v7

    iget v8, p2, Landroid/graphics/RectF;->left:F

    iget v11, p2, Landroid/graphics/RectF;->top:F

    iget v10, p2, Landroid/graphics/RectF;->right:F

    move v9, v11

    .line 357
    invoke-static/range {v4 .. v11}, Lcom/pspdfkit/internal/di;->a(FFFFFFFF)Landroid/graphics/PointF;

    move-result-object p0

    .line 366
    iget p1, p0, Landroid/graphics/PointF;->y:F

    iget p2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr p1, p2

    iput p1, v1, Landroid/graphics/RectF;->top:F

    .line 367
    iget p0, p0, Landroid/graphics/PointF;->x:F

    iget p1, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p0, p1

    iput p0, v1, Landroid/graphics/RectF;->left:F

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/up;->f:Lcom/pspdfkit/internal/z1$b;

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/up;->e:Landroid/graphics/Path;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->e:Landroid/graphics/Path;

    if-nez v0, :cond_0

    return-void

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/up;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public final a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/z1$b;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;ZFF)Z
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    .line 8
    iget-object v5, v0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    move-object/from16 v6, p3

    invoke-virtual {v5, v6, v1}, Lcom/pspdfkit/internal/lo;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 11
    iget-object v5, v0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->h()F

    move-result v5

    cmpl-float v5, v5, p7

    if-nez v5, :cond_0

    iget-object v5, v0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->d()F

    move-result v5

    cmpl-float v5, v5, p8

    if-eqz v5, :cond_16

    .line 12
    :cond_0
    iget-object v5, v0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v10, 0x2

    const/4 v11, 0x5

    if-nez v2, :cond_1

    goto :goto_0

    .line 13
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v12, 0x2

    goto :goto_1

    :pswitch_1
    const/4 v12, 0x5

    goto :goto_1

    :pswitch_2
    const/4 v12, 0x3

    goto :goto_1

    :pswitch_3
    const/4 v12, 0x4

    goto :goto_1

    :goto_0
    const/4 v12, 0x1

    :goto_1
    const/4 v13, 0x7

    if-eq v12, v8, :cond_2

    if-eq v12, v11, :cond_2

    goto/16 :goto_7

    .line 14
    :cond_2
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->b()Landroid/graphics/RectF;

    move-result-object v12

    .line 16
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/RectF;->width()F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    .line 17
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/RectF;->height()F

    move-result v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    if-nez p6, :cond_4

    cmpl-float v16, v14, v15

    if-lez v16, :cond_3

    div-float/2addr v14, v15

    goto :goto_2

    :cond_3
    div-float v14, v15, v14

    :goto_2
    const/high16 v15, 0x40400000    # 3.0f

    cmpl-float v14, v14, v15

    if-ltz v14, :cond_4

    goto/16 :goto_7

    .line 32
    :cond_4
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v14

    if-eqz v14, :cond_8

    if-eq v14, v10, :cond_7

    if-eq v14, v11, :cond_6

    if-eq v14, v13, :cond_5

    goto/16 :goto_7

    .line 43
    :cond_5
    iget v14, v3, Landroid/graphics/RectF;->right:F

    .line 44
    iget v15, v3, Landroid/graphics/RectF;->bottom:F

    .line 45
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v16

    .line 46
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v17

    .line 47
    iget v6, v3, Landroid/graphics/RectF;->left:F

    .line 48
    iget v3, v3, Landroid/graphics/RectF;->top:F

    goto :goto_3

    .line 61
    :cond_6
    iget v14, v3, Landroid/graphics/RectF;->left:F

    .line 62
    iget v15, v3, Landroid/graphics/RectF;->bottom:F

    .line 63
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v16

    .line 64
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->a()F

    move-result v17

    .line 65
    iget v6, v3, Landroid/graphics/RectF;->right:F

    .line 66
    iget v3, v3, Landroid/graphics/RectF;->top:F

    goto :goto_3

    .line 67
    :cond_7
    iget v14, v3, Landroid/graphics/RectF;->right:F

    .line 68
    iget v15, v3, Landroid/graphics/RectF;->top:F

    .line 69
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->f()F

    move-result v16

    .line 70
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v17

    .line 71
    iget v6, v3, Landroid/graphics/RectF;->left:F

    .line 72
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    goto :goto_3

    .line 73
    :cond_8
    iget v14, v3, Landroid/graphics/RectF;->left:F

    .line 74
    iget v15, v3, Landroid/graphics/RectF;->top:F

    .line 75
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->e()F

    move-result v16

    .line 76
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->g()F

    move-result v17

    .line 77
    iget v6, v3, Landroid/graphics/RectF;->right:F

    .line 78
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    :goto_3
    sub-float v18, v3, v15

    sub-float v19, v6, v14

    div-float v18, v18, v19

    mul-float v19, v18, v6

    sub-float v19, v3, v19

    sub-float v9, v3, v17

    mul-float v14, v14, v9

    sub-float v8, v6, v16

    mul-float v15, v15, v8

    sub-float/2addr v14, v15

    mul-float v6, v6, v17

    add-float/2addr v6, v14

    mul-float v3, v3, v16

    sub-float/2addr v6, v3

    .line 79
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v14, v3

    move-object v6, v12

    float-to-double v11, v9

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    .line 80
    invoke-static {v11, v12, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    float-to-double v8, v8

    invoke-static {v8, v9, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v3, v11

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    div-double/2addr v14, v3

    double-to-float v3, v14

    if-eqz p6, :cond_9

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_4

    .line 81
    :cond_9
    iget v4, v0, Lcom/pspdfkit/internal/up;->b:F

    :goto_4
    iget-object v8, v0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/z1;->getZoomScale()F

    move-result v8

    div-float/2addr v4, v8

    cmpg-float v3, v3, v4

    if-gez v3, :cond_c

    .line 82
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    if-eqz v4, :cond_b

    if-eq v4, v10, :cond_b

    const/4 v3, 0x5

    if-eq v4, v3, :cond_a

    if-eq v4, v13, :cond_a

    goto :goto_6

    .line 90
    :cond_a
    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    mul-float v18, v18, v16

    add-float v18, v18, v19

    sub-float v18, v18, v17

    add-float v4, v18, v4

    iput v4, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    .line 91
    :cond_b
    iget v4, v6, Landroid/graphics/RectF;->top:F

    mul-float v18, v18, v16

    add-float v18, v18, v19

    sub-float v18, v18, v17

    add-float v4, v18, v4

    iput v4, v6, Landroid/graphics/RectF;->top:F

    :goto_5
    move-object/from16 v4, p5

    .line 104
    invoke-static {v2, v5, v4}, Lcom/pspdfkit/internal/up;->a(Lcom/pspdfkit/internal/z1$b;Lcom/pspdfkit/internal/lo;Landroid/graphics/RectF;)Z

    const/4 v5, 0x1

    goto :goto_8

    :cond_c
    :goto_6
    move-object/from16 v4, p5

    :goto_7
    const/4 v5, 0x0

    :goto_8
    if-nez v5, :cond_15

    if-nez p6, :cond_15

    .line 105
    iget-object v5, v0, Lcom/pspdfkit/internal/up;->g:Lcom/pspdfkit/internal/lo;

    if-nez v2, :cond_d

    goto :goto_9

    .line 106
    :cond_d
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    goto :goto_9

    :pswitch_4
    const/4 v6, 0x2

    goto :goto_a

    :pswitch_5
    const/4 v6, 0x5

    goto :goto_a

    :pswitch_6
    const/4 v6, 0x3

    goto :goto_a

    :pswitch_7
    const/4 v6, 0x4

    goto :goto_a

    :goto_9
    const/4 v6, 0x1

    :goto_a
    if-eq v6, v7, :cond_e

    if-eq v6, v10, :cond_e

    goto/16 :goto_c

    .line 107
    :cond_e
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->b()Landroid/graphics/RectF;

    move-result-object v6

    .line 108
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->h()F

    move-result v8

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->d()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 109
    iget v9, v0, Lcom/pspdfkit/internal/up;->b:F

    iget-object v11, v0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v11}, Lcom/pspdfkit/internal/z1;->getZoomScale()F

    move-result v11

    div-float/2addr v9, v11

    cmpg-float v9, v8, v9

    if-gez v9, :cond_14

    .line 110
    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->h()F

    move-result v9

    invoke-virtual {v5}, Lcom/pspdfkit/internal/lo;->d()F

    move-result v11

    cmpg-float v9, v9, v11

    if-gez v9, :cond_11

    .line 111
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v9

    if-eqz v9, :cond_10

    if-eq v9, v13, :cond_f

    if-eq v9, v10, :cond_f

    if-eq v9, v7, :cond_10

    const/4 v7, 0x4

    if-eq v9, v7, :cond_f

    const/4 v3, 0x5

    if-eq v9, v3, :cond_10

    goto :goto_c

    .line 121
    :cond_f
    iget v3, v6, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v8

    iput v3, v6, Landroid/graphics/RectF;->right:F

    goto :goto_b

    .line 122
    :cond_10
    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v8

    iput v3, v6, Landroid/graphics/RectF;->left:F

    goto :goto_b

    .line 135
    :cond_11
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-eqz v7, :cond_13

    const/4 v9, 0x1

    if-eq v7, v9, :cond_13

    if-eq v7, v10, :cond_13

    const/4 v3, 0x5

    if-eq v7, v3, :cond_12

    const/4 v3, 0x6

    if-eq v7, v3, :cond_12

    if-eq v7, v13, :cond_12

    goto :goto_c

    .line 145
    :cond_12
    iget v3, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v8

    iput v3, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_b

    .line 146
    :cond_13
    iget v3, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v8

    iput v3, v6, Landroid/graphics/RectF;->top:F

    .line 160
    :goto_b
    invoke-static {v2, v5, v4}, Lcom/pspdfkit/internal/up;->a(Lcom/pspdfkit/internal/z1$b;Lcom/pspdfkit/internal/lo;Landroid/graphics/RectF;)Z

    move-result v3

    const/4 v4, 0x1

    xor-int/lit8 v5, v3, 0x1

    goto :goto_d

    :cond_14
    :goto_c
    const/4 v4, 0x1

    const/4 v5, 0x0

    goto :goto_d

    :cond_15
    const/4 v4, 0x1

    :goto_d
    if-eqz v5, :cond_16

    .line 161
    iput-object v2, v0, Lcom/pspdfkit/internal/up;->f:Lcom/pspdfkit/internal/z1$b;

    return v4

    :cond_16
    if-eqz p6, :cond_17

    const/4 v2, 0x0

    .line 167
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_17
    const/4 v1, 0x0

    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->f:Lcom/pspdfkit/internal/z1$b;

    if-nez v0, :cond_0

    goto/16 :goto_6

    .line 2
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    move-object v2, v0

    goto :goto_0

    .line 19
    :pswitch_0
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 20
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_0

    .line 21
    :pswitch_1
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 22
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_0

    .line 32
    :pswitch_2
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 33
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_0

    .line 34
    :pswitch_3
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 35
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_0

    .line 56
    :pswitch_4
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 57
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_0

    .line 58
    :pswitch_5
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v0

    .line 59
    iget-object v2, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;

    move-result-object v2

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 93
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    if-eqz v1, :cond_7

    .line 94
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_7

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_7

    .line 95
    check-cast v0, Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/Point;->x:I

    .line 96
    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 97
    check-cast v1, Landroid/graphics/Point;

    iget v3, v1, Landroid/graphics/Point;->x:I

    .line 98
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 102
    iget-object v4, p0, Lcom/pspdfkit/internal/up;->f:Lcom/pspdfkit/internal/z1$b;

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-nez v4, :cond_2

    goto :goto_1

    .line 103
    :cond_2
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_1

    :pswitch_6
    const/4 v4, 0x2

    goto :goto_2

    :pswitch_7
    const/4 v4, 0x5

    goto :goto_2

    :pswitch_8
    const/4 v4, 0x3

    goto :goto_2

    :pswitch_9
    const/4 v4, 0x4

    goto :goto_2

    :goto_1
    const/4 v4, 0x1

    .line 104
    :goto_2
    invoke-static {v4}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v4

    if-eq v4, v8, :cond_6

    if-eq v4, v7, :cond_5

    if-eq v4, v6, :cond_3

    if-eq v4, v5, :cond_3

    goto :goto_4

    :cond_3
    sub-int/2addr v1, v0

    int-to-float v1, v1

    sub-int v4, v3, v2

    int-to-float v4, v4

    div-float/2addr v1, v4

    if-ge v2, v3, :cond_4

    goto :goto_3

    :cond_4
    const/4 v8, -0x1

    :goto_3
    int-to-float v4, v8

    int-to-float v5, v2

    .line 125
    iget v6, p0, Lcom/pspdfkit/internal/up;->c:F

    mul-float v6, v6, v4

    sub-float/2addr v5, v6

    float-to-int v4, v5

    int-to-float v3, v3

    add-float/2addr v6, v3

    float-to-int v3, v6

    sub-int v5, v4, v2

    int-to-float v5, v5

    mul-float v5, v5, v1

    int-to-float v0, v0

    add-float/2addr v5, v0

    float-to-int v5, v5

    sub-int v2, v3, v2

    int-to-float v2, v2

    mul-float v2, v2, v1

    add-float/2addr v2, v0

    float-to-int v1, v2

    move v2, v4

    move v0, v5

    goto :goto_4

    :cond_5
    int-to-float v2, v2

    .line 126
    iget v4, p0, Lcom/pspdfkit/internal/up;->c:F

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-float v3, v3

    add-float/2addr v3, v4

    float-to-int v3, v3

    goto :goto_4

    :cond_6
    int-to-float v0, v0

    .line 127
    iget v4, p0, Lcom/pspdfkit/internal/up;->c:F

    sub-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v1, v1

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 155
    :goto_4
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    iput-object v4, p0, Lcom/pspdfkit/internal/up;->e:Landroid/graphics/Path;

    int-to-float v2, v2

    int-to-float v0, v0

    .line 156
    invoke-virtual {v4, v2, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->e:Landroid/graphics/Path;

    int-to-float v2, v3

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_5

    .line 159
    :cond_7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/up;->a()V

    .line 161
    :goto_5
    iget-object v0, p0, Lcom/pspdfkit/internal/up;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

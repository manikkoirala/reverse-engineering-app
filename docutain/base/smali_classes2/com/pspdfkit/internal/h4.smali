.class public abstract Lcom/pspdfkit/internal/h4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/br;


# instance fields
.field private a:Lcom/pspdfkit/internal/br$a;

.field protected b:F

.field protected c:Landroid/graphics/Matrix;

.field private d:Z

.field private e:I

.field private f:I

.field private g:F

.field private h:F

.field protected i:Landroid/graphics/Paint;

.field protected j:Lcom/pspdfkit/internal/ri;

.field private k:Z

.field protected l:Ljava/lang/String;


# direct methods
.method constructor <init>(IIFF)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/br$a;->a:Lcom/pspdfkit/internal/br$a;

    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->a:Lcom/pspdfkit/internal/br$a;

    const/high16 v0, -0x40800000    # -1.0f

    .line 6
    iput v0, p0, Lcom/pspdfkit/internal/h4;->b:F

    .line 9
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/pspdfkit/internal/h4;->d:Z

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    .line 30
    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    const/4 v1, 0x1

    .line 33
    iput-boolean v1, p0, Lcom/pspdfkit/internal/h4;->k:Z

    .line 36
    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    .line 52
    iput p1, p0, Lcom/pspdfkit/internal/h4;->e:I

    .line 53
    iput p2, p0, Lcom/pspdfkit/internal/h4;->f:I

    .line 54
    iput p4, p0, Lcom/pspdfkit/internal/h4;->h:F

    .line 55
    iput p3, p0, Lcom/pspdfkit/internal/h4;->g:F

    return-void
.end method

.method public static h()Landroid/graphics/Paint;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 2
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 4
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-object v0
.end method

.method public static i()Landroid/graphics/Paint;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 2
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 4
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-object v0
.end method


# virtual methods
.method public final a(F)V
    .locals 0

    .line 47
    iput p1, p0, Lcom/pspdfkit/internal/h4;->h:F

    return-void
.end method

.method public final a(I)V
    .locals 0

    .line 46
    iput p1, p0, Lcom/pspdfkit/internal/h4;->e:I

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->n()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 5
    invoke-virtual {p0, p2, p3, v0}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    .line 8
    iget v0, p0, Lcom/pspdfkit/internal/h4;->b:F

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    return-void
.end method

.method protected abstract a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
.end method

.method protected a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 2

    .line 27
    iget v0, p0, Lcom/pspdfkit/internal/h4;->e:I

    .line 28
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 29
    iget v0, p0, Lcom/pspdfkit/internal/h4;->h:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 30
    iget v0, p0, Lcom/pspdfkit/internal/h4;->g:F

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/nu;->a(FLandroid/graphics/Matrix;)F

    move-result v0

    div-float/2addr v0, p3

    .line 32
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    if-eqz p2, :cond_0

    .line 33
    iget p3, p0, Lcom/pspdfkit/internal/h4;->f:I

    .line 34
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    iget p3, p0, Lcom/pspdfkit/internal/h4;->f:I

    .line 36
    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    if-eqz p3, :cond_0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 42
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-eqz p2, :cond_2

    .line 43
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    iget-boolean p2, p0, Lcom/pspdfkit/internal/h4;->k:Z

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 45
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/br$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/h4;->a:Lcom/pspdfkit/internal/br$a;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ri;)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/fonts/Font;

    .line 57
    invoke-virtual {v1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eq v0, p1, :cond_1

    .line 65
    iput-object p1, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    .line 66
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->o()V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    return-void
.end method

.method public final a(FLandroid/graphics/Matrix;)Z
    .locals 4

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/4 v1, 0x0

    const/4 v2, 0x1

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 10
    iput p1, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/h4;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move v2, p1

    goto :goto_2

    .line 15
    :cond_2
    :goto_1
    iput-boolean v2, p0, Lcom/pspdfkit/internal/h4;->d:Z

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->e()V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    if-nez p1, :cond_3

    goto :goto_2

    .line 22
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    iget p2, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/high16 v0, 0x41900000    # 18.0f

    const/16 v3, 0x9

    new-array v3, v3, [F

    .line 24
    invoke-virtual {p1, v3}, Landroid/graphics/Matrix;->getValues([F)V

    aget p1, v3, v1

    div-float/2addr p1, p2

    mul-float p1, p1, v0

    .line 26
    iget-object p2, p0, Lcom/pspdfkit/internal/h4;->i:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    :goto_2
    return v2
.end method

.method public final a(Z)Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/pspdfkit/internal/h4;->k:Z

    if-eq p1, v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->o()V

    .line 71
    iput-boolean p1, p0, Lcom/pspdfkit/internal/h4;->k:Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final b(F)V
    .locals 1

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/h4;->g:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 10
    iput p1, p0, Lcom/pspdfkit/internal/h4;->g:F

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->e()V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 0

    .line 8
    iput p1, p0, Lcom/pspdfkit/internal/h4;->f:I

    return-void
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->n()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/h4;->b:F

    invoke-virtual {p0, p2, p3, v0}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 7
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    return-void
.end method

.method public final c()Lcom/pspdfkit/internal/br$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->a:Lcom/pspdfkit/internal/br$a;

    return-object v0
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method public final f()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/h4;->h:F

    return v0
.end method

.method public final g()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/h4;->e:I

    return v0
.end method

.method public final hide()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/h4;->h:F

    return-void
.end method

.method public final j()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/h4;->f:I

    return v0
.end method

.method public final k()Lcom/pspdfkit/internal/ri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final m()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/h4;->g:F

    return v0
.end method

.method protected n()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public o()V
    .locals 0

    return-void
.end method

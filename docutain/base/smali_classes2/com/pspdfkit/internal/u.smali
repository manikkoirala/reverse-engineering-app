.class public final Lcom/pspdfkit/internal/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/du;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/u$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/do;

.field private final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    const-string v1, "pspdfkit-computation"

    .line 5
    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/u;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/u;->a:Lcom/pspdfkit/internal/do;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/u$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/u$a;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/u;->b:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;
    .locals 1

    const-string v0, "threadName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/pspdfkit/internal/do;

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/internal/do;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public final a()Lio/reactivex/rxjava3/core/Scheduler;
    .locals 1

    const/4 v0, 0x5

    .line 44
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lio/reactivex/rxjava3/core/Scheduler;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/u;->a:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/do;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    const-string v0, "computationScheduler.priority(priority)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    const-string v0, "runnable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/u;->b:Ljava/util/concurrent/Executor;

    .line 42
    check-cast v0, Lcom/pspdfkit/internal/u$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/u$a;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "exceptionMessage"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/internal/u;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Lio/reactivex/rxjava3/core/Scheduler;
    .locals 2

    .line 36
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    const-string v1, "io()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 1

    const-string v0, "runnable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/internal/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/u;->b:Ljava/util/concurrent/Executor;

    .line 35
    check-cast v0, Lcom/pspdfkit/internal/u$a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/u$a;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    const-string v0, "exceptionMessage"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/internal/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/u;->b:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

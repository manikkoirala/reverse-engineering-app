.class public abstract Lcom/pspdfkit/internal/d4;
.super Lcom/pspdfkit/internal/y4;
.source "SourceFile"


# instance fields
.field protected final s:Ljava/util/ArrayList;

.field private t:Z

.field private u:Z

.field private v:F


# direct methods
.method constructor <init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/y4;-><init>(IIFFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/pspdfkit/internal/d4;->t:Z

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/d4;->u:Z

    const/high16 p1, 0x3f800000    # 1.0f

    .line 11
    iput p1, p0, Lcom/pspdfkit/internal/d4;->v:F

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 0

    .line 1
    iget-boolean p2, p0, Lcom/pspdfkit/internal/d4;->t:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    const/4 p3, 0x2

    if-lt p2, p3, :cond_0

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    add-int/lit8 p3, p3, -0x1

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/graphics/PointF;

    .line 4
    invoke-virtual {p2, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/d4;->o()V

    return-void
.end method

.method public final a()Z
    .locals 4

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    return v1

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/d4;->v:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    .line 12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/pspdfkit/internal/d4;->v:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    return v1

    :cond_1
    return v3
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/d4;->o()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/d4;->t:Z

    return-void
.end method

.method protected final n()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h4;->j:Lcom/pspdfkit/internal/ri;

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/pspdfkit/internal/h4;->b:F

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_2

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/pspdfkit/internal/h4;->c:Landroid/graphics/Matrix;

    const-string v4, "measurementProperties"

    .line 3
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "viewPoints"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "viewPointsToPdfPointsTransformationMatrix"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    .line 164
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 167
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    .line 169
    iget v7, v5, Landroid/graphics/PointF;->x:F

    mul-float v7, v7, v1

    iget v5, v5, Landroid/graphics/PointF;->y:F

    mul-float v5, v5, v1

    invoke-virtual {v6, v7, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 171
    invoke-static {v6, v3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    const-string v5, "getUnscaledViewPointAsPd\u2026      pageScale\n        )"

    .line 172
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_1
    invoke-static {v0, v4}, Lcom/pspdfkit/internal/li;->a(Lcom/pspdfkit/internal/ri;Ljava/util/List;)Lcom/pspdfkit/internal/mi;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    .line 181
    invoke-virtual {v0}, Lcom/pspdfkit/internal/mi;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/h4;->l:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public final v()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/d4;->t:Z

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/d4;->u:Z

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/br$a;)V

    return-void
.end method

.method public final w()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/d4;->u:Z

    return v0
.end method

.method public final y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/d4;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/zt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/em;
.implements Lcom/pspdfkit/internal/ne;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/zt$c;,
        Lcom/pspdfkit/internal/zt$d;,
        Lcom/pspdfkit/internal/zt$b;
    }
.end annotation


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/PointF;

.field private final d:Landroid/graphics/RectF;

.field private final e:Ljava/util/ArrayList;

.field private final f:Lcom/pspdfkit/internal/specialMode/handler/e;

.field private final g:Lcom/pspdfkit/internal/au;

.field private final h:Lcom/pspdfkit/internal/o7;

.field private final i:Lcom/pspdfkit/internal/lq;

.field private final j:Landroid/graphics/RectF;

.field private final k:Landroid/graphics/RectF;

.field private final l:I

.field m:Lcom/pspdfkit/internal/dm;

.field n:Landroid/graphics/drawable/Drawable;

.field o:Landroid/graphics/drawable/Drawable;

.field p:Landroid/graphics/PointF;

.field q:Landroid/graphics/PointF;

.field r:Lcom/pspdfkit/internal/zt$d;

.field private s:I

.field private t:Lcom/pspdfkit/datastructures/TextSelection;

.field private u:Lcom/pspdfkit/internal/zt$b;

.field private v:Landroid/graphics/Matrix;

.field private w:Z

.field private x:Lcom/pspdfkit/internal/uh;


# direct methods
.method public static synthetic $r8$lambda$moK_TAjbLShPncdP19JQXZXv4ig(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/dm;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/dm;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/zt;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/zt;->l:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetu(Lcom/pspdfkit/internal/zt;)Lcom/pspdfkit/internal/zt$b;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetv(Lcom/pspdfkit/internal/zt;)Landroid/graphics/Matrix;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/zt;->v:Landroid/graphics/Matrix;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const v1, -0xff0100

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 5
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v1, 0x41200000    # 10.0f

    .line 6
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zt;->b:Landroid/graphics/Paint;

    .line 4
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    .line 5
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->d:Landroid/graphics/RectF;

    .line 6
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 7
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    .line 27
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    .line 40
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->p:Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->q:Landroid/graphics/PointF;

    .line 54
    sget-object v1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    .line 67
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    .line 68
    iput-object p2, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 69
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__min_selectable_text_size:I

    .line 72
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/zt;->l:I

    .line 74
    new-instance p1, Lcom/pspdfkit/internal/o7;

    .line 75
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/zt$c;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/pspdfkit/internal/zt$c;-><init>(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/internal/zt$c-IA;)V

    invoke-direct {p1, v1, v2}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->h:Lcom/pspdfkit/internal/o7;

    .line 77
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    .line 78
    new-instance v1, Lcom/pspdfkit/internal/au;

    .line 79
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/au;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/zt;->g:Lcom/pspdfkit/internal/au;

    .line 80
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 81
    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    .line 82
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    :cond_0
    sget-object p1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    invoke-direct {v2, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 83
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 85
    iget p1, v1, Lcom/pspdfkit/internal/au;->a:I

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    new-instance p1, Lcom/pspdfkit/internal/lq;

    .line 88
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    const-string v0, "com.pspdfkit.internal.TextSelectionModeHandler.SAVED_STATE_FRAGMENT_TAG"

    invoke-direct {p1, p2, v0, p0}, Lcom/pspdfkit/internal/lq;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/internal/ne;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->i:Lcom/pspdfkit/internal/lq;

    .line 89
    invoke-virtual {p1}, Lcom/pspdfkit/internal/lq;->c()Lcom/pspdfkit/internal/me;

    return-void
.end method

.method public static a(FFFLcom/pspdfkit/internal/zf;ILandroid/graphics/Matrix;)Lcom/pspdfkit/datastructures/TextSelectionRectangles;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2
    invoke-static {v0, p5}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 4
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 8
    invoke-static {p2, p5}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result p2

    .line 10
    invoke-virtual {p3, p4, v0, p2}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/PointF;F)Landroid/graphics/RectF;

    move-result-object p5

    .line 12
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p3, p4, v1, v0, p2}, Lcom/pspdfkit/internal/zf;->a(IFFF)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 15
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getMarkupRects()Ljava/util/ArrayList;

    move-result-object p3

    invoke-static {p3}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p3

    .line 16
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getRects()Ljava/util/ArrayList;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p2

    const/4 p4, 0x0

    const/4 v0, 0x0

    .line 17
    :goto_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 18
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    if-eqz p5, :cond_1

    .line 19
    iget v2, p5, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->right:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, p5, Landroid/graphics/RectF;->right:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget v2, p5, Landroid/graphics/RectF;->top:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p5, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 20
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 22
    invoke-virtual {p0, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    invoke-virtual {p1, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 26
    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_3

    .line 33
    new-instance p2, Lcom/pspdfkit/datastructures/TextSelectionRectangles;

    invoke-direct {p2, p0, p1}, Lcom/pspdfkit/datastructures/TextSelectionRectangles;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto :goto_3

    :cond_3
    const/4 p2, 0x0

    :goto_3
    return-object p2
.end method

.method private a(F)V
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    sget-object v1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->v:Landroid/graphics/Matrix;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->x:Lcom/pspdfkit/internal/uh;

    if-nez v2, :cond_0

    goto :goto_1

    .line 40
    :cond_0
    sget-object v2, Lcom/pspdfkit/internal/zt$b;->b:Lcom/pspdfkit/internal/zt$b;

    if-ne v0, v2, :cond_1

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    .line 46
    :goto_0
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->d:Landroid/graphics/RectF;

    .line 47
    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 48
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 50
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    new-array v0, v0, [I

    .line 53
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->x:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/uh;->e()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x0

    aget v3, v1, v2

    int-to-float v3, v3

    add-float/2addr p1, v3

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float/2addr p1, v2

    .line 62
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->d:Landroid/graphics/RectF;

    .line 63
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/4 v3, 0x1

    aget v1, v1, v3

    int-to-float v1, v1

    add-float/2addr v2, v1

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float/2addr v2, v0

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->x:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/uh;->a(FF)V

    :cond_2
    :goto_1
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/dm;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1760
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 1762
    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object p2

    .line 1763
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Lcom/pspdfkit/internal/w1;->a(Ljava/util/List;ZLcom/pspdfkit/internal/w1$a;)V

    .line 1764
    iget-object p2, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object p2

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    .line 339
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    .line 340
    iput-object p2, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    if-eqz p1, :cond_0

    .line 342
    invoke-direct {p0}, Lcom/pspdfkit/internal/zt;->i()V

    .line 345
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    iget-object p2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)V

    .line 348
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    if-eqz p1, :cond_1

    .line 349
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/d;->exitActiveMode()V

    :cond_1
    return-void
.end method

.method private i()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.TextSelection"

    const-string v3, "Can\'t update selection UI without a selection."

    invoke-static {v2, v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 10
    :cond_0
    iget-object v0, v0, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 12
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v3, v2, :cond_1

    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/pspdfkit/utils/PageRect;

    invoke-direct {v4}, Lcom/pspdfkit/utils/PageRect;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v2, :cond_2

    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_3

    .line 18
    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/utils/PageRect;

    .line 19
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    .line 20
    iget v6, v5, Landroid/graphics/RectF;->left:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v6, v7

    iget v8, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v8, v7

    iget v9, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v9, v7

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v7

    invoke-virtual {v4, v6, v8, v9, v5}, Lcom/pspdfkit/utils/PageRect;->set(FFFF)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    const/4 v3, 0x1

    if-lez v2, :cond_4

    .line 32
    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    .line 33
    invoke-virtual {v4}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v4

    if-lez v4, :cond_4

    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->text:Ljava/lang/String;

    if-eqz v4, :cond_4

    const-string v5, "\r|\n"

    const-string v6, ""

    .line 35
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    sub-int/2addr v2, v3

    .line 37
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 38
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 39
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_4

    .line 41
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 48
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v2

    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/zf;->getPageTextLength(I)I

    move-result v4

    if-eq v2, v4, :cond_6

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v4, v2, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    .line 51
    invoke-virtual {v2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v2

    .line 52
    invoke-virtual {v0, v4, v2, v3}, Lcom/pspdfkit/internal/zf;->getPageText(III)Ljava/lang/String;

    move-result-object v2

    const-string v4, "\n\r"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_3

    .line 62
    :cond_5
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v4, v2, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    .line 63
    invoke-virtual {v2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v2

    .line 64
    invoke-virtual {v0, v4, v2, v3, v1}, Lcom/pspdfkit/internal/zf;->getPageTextRects(IIIZ)Ljava/util/List;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 66
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 67
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 68
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_4

    .line 69
    :cond_6
    :goto_3
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v4, v2, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    .line 70
    invoke-virtual {v2}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v2

    sub-int/2addr v2, v3

    .line 71
    invoke-virtual {v0, v4, v2, v3, v1}, Lcom/pspdfkit/internal/zf;->getPageTextRects(IIIZ)Ljava/util/List;

    move-result-object v0

    .line 72
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 73
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 74
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 75
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 88
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->p:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->j:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v3, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 89
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->q:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->k:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v3, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 91
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    .line 92
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/dm;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public final a(Lcom/pspdfkit/annotations/AnnotationType;Z)Lio/reactivex/rxjava3/core/Single;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            "Z)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Lcom/pspdfkit/annotations/BaseRectsAnnotation;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    .line 350
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const/4 v2, 0x0

    if-eqz v1, :cond_15

    iget-object v1, v1, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v1}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_6

    .line 351
    :cond_0
    iget-object v8, v0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    .line 352
    invoke-virtual {v8}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v9

    const-string v10, " is not supported."

    const-string v11, "Passed annotation type "

    const/4 v12, 0x4

    const/4 v13, 0x3

    const/4 v14, 0x2

    const/4 v15, 0x1

    if-eqz p2, :cond_1

    .line 359
    new-instance v1, Lcom/pspdfkit/internal/zh;

    iget-object v3, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 360
    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v3

    invoke-virtual {v3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INSTANT_HIGHLIGHT_COMMENT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/zh;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)V

    .line 361
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zh;->build()Lcom/pspdfkit/annotations/configuration/MarkupAnnotationConfiguration;

    move-result-object v1

    .line 362
    check-cast v1, Lcom/pspdfkit/internal/d0;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/d0;->getDefaultColor()I

    move-result v3

    .line 363
    invoke-virtual {v1}, Lcom/pspdfkit/internal/d0;->getDefaultAlpha()F

    move-result v1

    move v5, v1

    move v6, v3

    goto/16 :goto_2

    .line 365
    :cond_1
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    .line 366
    sget-object v3, Lcom/pspdfkit/internal/zt$a;->a:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v4, v3, v4

    if-eq v4, v15, :cond_5

    if-eq v4, v14, :cond_4

    if-eq v4, v13, :cond_3

    if-ne v4, v12, :cond_2

    .line 377
    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_0

    .line 376
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 378
    :cond_3
    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_0

    .line 379
    :cond_4
    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_0

    .line 380
    :cond_5
    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 381
    :goto_0
    invoke-interface {v1, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I

    move-result v1

    .line 382
    iget-object v4, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/specialMode/handler/e;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v4

    .line 383
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v3, v3, v5

    if-eq v3, v15, :cond_9

    if-eq v3, v14, :cond_8

    if-eq v3, v13, :cond_7

    if-ne v3, v12, :cond_6

    .line 394
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->UNDERLINE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 393
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 395
    :cond_7
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STRIKEOUT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 396
    :cond_8
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->HIGHLIGHT:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    goto :goto_1

    .line 397
    :cond_9
    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 398
    :goto_1
    invoke-interface {v4, v3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)F

    move-result v3

    move v6, v1

    move v5, v3

    :goto_2
    if-eqz p2, :cond_a

    move/from16 v17, v6

    goto :goto_3

    .line 405
    :cond_a
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v2, v1, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v4, v1, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    move-object v1, v9

    move-object/from16 v3, p1

    move-object/from16 v16, v4

    move v4, v6

    move/from16 v17, v6

    move-object/from16 v6, v16

    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    move-result-object v2

    :goto_3
    const-string v1, "com.pspdfkit.internal.annotations.markup.default-rect-name"

    if-eqz v2, :cond_c

    .line 406
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    goto :goto_4

    :cond_b
    move-object v3, v2

    goto :goto_5

    .line 407
    :cond_c
    :goto_4
    sget-object v3, Lcom/pspdfkit/internal/zt$a;->a:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v15, :cond_10

    if-eq v3, v14, :cond_f

    if-eq v3, v13, :cond_e

    if-ne v3, v12, :cond_d

    .line 418
    new-instance v3, Lcom/pspdfkit/annotations/UnderlineAnnotation;

    iget-object v4, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v5, v4, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/annotations/UnderlineAnnotation;-><init>(ILjava/util/List;)V

    goto :goto_5

    .line 417
    :cond_d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 419
    :cond_e
    new-instance v3, Lcom/pspdfkit/annotations/StrikeOutAnnotation;

    iget-object v4, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v5, v4, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/annotations/StrikeOutAnnotation;-><init>(ILjava/util/List;)V

    goto :goto_5

    .line 420
    :cond_f
    new-instance v3, Lcom/pspdfkit/annotations/HighlightAnnotation;

    iget-object v4, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v5, v4, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/annotations/HighlightAnnotation;-><init>(ILjava/util/List;)V

    goto :goto_5

    .line 421
    :cond_10
    new-instance v3, Lcom/pspdfkit/annotations/RedactionAnnotation;

    iget-object v4, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v5, v4, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v4, v4, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-direct {v3, v5, v4}, Lcom/pspdfkit/annotations/RedactionAnnotation;-><init>(ILjava/util/List;)V

    .line 422
    :goto_5
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    if-eqz p2, :cond_11

    .line 424
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->markAsInstantCommentRoot()V

    .line 426
    :cond_11
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    sget v2, Lcom/pspdfkit/internal/ao;->a:I

    const-string v2, "annotationPreferences"

    .line 427
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "annotation"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1717
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_12

    .line 1718
    invoke-interface {v1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 1720
    invoke-virtual {v3, v1}, Lcom/pspdfkit/annotations/Annotation;->setCreator(Ljava/lang/String;)V

    :cond_12
    move/from16 v1, v17

    .line 1721
    invoke-virtual {v3, v1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 1722
    instance-of v1, v3, Lcom/pspdfkit/annotations/RedactionAnnotation;

    if-eqz v1, :cond_13

    .line 1723
    move-object v1, v3

    check-cast v1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    .line 1724
    iget-object v2, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/e;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v2

    .line 1725
    sget-object v4, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->REDACTION:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-interface {v2, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOverlayText(Ljava/lang/String;)V

    .line 1726
    invoke-interface {v2, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setRepeatOverlayText(Z)V

    .line 1727
    invoke-interface {v2, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/pspdfkit/annotations/RedactionAnnotation;->setOutlineColor(I)V

    .line 1728
    invoke-interface {v2, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 1729
    :cond_13
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "create_annotation"

    .line 1730
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 1731
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    .line 1732
    invoke-virtual {v1}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 1734
    invoke-virtual {v9}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v1

    .line 1735
    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/r1;->addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 1736
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/zt$$ExternalSyntheticLambda0;

    invoke-direct {v2, v0, v3, v8}, Lcom/pspdfkit/internal/zt$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/dm;)V

    .line 1737
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 1745
    invoke-virtual {v1, v3}, Lio/reactivex/rxjava3/core/Completable;->toSingleDefault(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    return-object v1

    .line 1748
    :cond_14
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    .line 1749
    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v1

    .line 1750
    invoke-virtual {v1}, Lcom/pspdfkit/internal/o1;->a()V

    .line 1755
    iget-object v2, v0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-static {v3, v2}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/List;)V

    .line 1756
    invoke-virtual {v1}, Lcom/pspdfkit/internal/o1;->b()V

    .line 1757
    iget-object v1, v0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 1758
    invoke-static {v3}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    return-object v1

    .line 1759
    :cond_15
    :goto_6
    invoke-static {v2}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    return-object v1
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .line 321
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 323
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 324
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    .line 325
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/internal/zt;->s:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->b:Landroid/graphics/Paint;

    .line 326
    invoke-virtual {p1, v1, v2, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 335
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 6

    .line 298
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->v:Landroid/graphics/Matrix;

    .line 299
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 303
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v1, v1, Lcom/pspdfkit/datastructures/TextSelection;->textBlocks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 304
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->p:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 309
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 310
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    iget v3, v2, Landroid/graphics/PointF;->y:F

    float-to-int v4, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget-object v5, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    .line 311
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v3, v5

    float-to-int v3, v3

    .line 312
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 316
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->q:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 317
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    .line 318
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v1

    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->c:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    .line 319
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 320
    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/datastructures/TextSelection;)V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 4

    .line 67
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    .line 72
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const-string v0, "PSPDFKit.TextSelection"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v1, "Text selection mode was launched without selection. Leaving now."

    .line 73
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/d;->exitActiveMode()V

    return-void

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 81
    iput-object v3, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    new-array p1, v1, [Ljava/lang/Object;

    const-string v1, "Canceling attempted selection from listener."

    .line 82
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/d;->exitActiveMode()V

    return-void

    .line 87
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x1

    .line 88
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/zt;->s:I

    .line 90
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__text_select_handle_left:I

    .line 91
    invoke-static {p1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->g:Lcom/pspdfkit/internal/au;

    iget v1, v1, Lcom/pspdfkit/internal/au;->b:I

    .line 92
    invoke-static {v0}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 93
    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 94
    iput-object v0, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    .line 98
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__text_select_handle_right:I

    .line 99
    invoke-static {p1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->g:Lcom/pspdfkit/internal/au;

    iget v0, v0, Lcom/pspdfkit/internal/au;->c:I

    .line 100
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 101
    invoke-static {p1, v0}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 102
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    .line 105
    invoke-direct {p0}, Lcom/pspdfkit/internal/zt;->i()V

    .line 107
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Lcom/pspdfkit/internal/zt;)V

    .line 109
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->i:Lcom/pspdfkit/internal/lq;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/lq;->b()V

    .line 110
    iget-boolean p1, p0, Lcom/pspdfkit/internal/zt;->w:Z

    if-eqz p1, :cond_2

    .line 111
    iget-object p1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->createLinkAboveSelectedText()V

    :cond_2
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/uh;)V
    .locals 0

    .line 1765
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->x:Lcom/pspdfkit/internal/uh;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zt$d;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/pspdfkit/internal/zt;->r:Lcom/pspdfkit/internal/zt$d;

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 14

    .line 112
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_13

    if-eq v0, v2, :cond_10

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_10

    const/4 v0, 0x0

    goto/16 :goto_a

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    sget-object v4, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    if-eq v0, v4, :cond_f

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 144
    iget-object v7, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const-string v8, "PSPDFKit.TextSelection"

    if-eqz v7, :cond_d

    if-ne v0, v4, :cond_1

    goto/16 :goto_5

    .line 152
    :cond_1
    iget-object v4, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v4

    .line 153
    iget-object v7, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v7}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v7

    .line 154
    iget-object v9, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v9

    .line 158
    sget-object v10, Lcom/pspdfkit/internal/zt$b;->b:Lcom/pspdfkit/internal/zt$b;

    if-ne v0, v10, :cond_2

    .line 159
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    goto :goto_0

    .line 160
    :cond_2
    iget-object v11, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    div-int/2addr v11, v3

    neg-int v3, v11

    :goto_0
    if-ne v0, v10, :cond_3

    .line 162
    iget-object v11, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    goto :goto_1

    .line 163
    :cond_3
    iget-object v11, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    :goto_1
    neg-int v11, v11

    .line 164
    new-instance v12, Landroid/graphics/PointF;

    int-to-float v3, v3

    add-float/2addr v5, v3

    int-to-float v3, v11

    add-float/2addr v6, v3

    invoke-direct {v12, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 169
    invoke-static {v12, v9}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 172
    iget v3, v12, Landroid/graphics/PointF;->x:F

    iget v5, v12, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v7, v3, v5}, Lcom/pspdfkit/internal/zf;->getCharIndexAt(IFF)I

    move-result v3

    if-gez v3, :cond_4

    goto/16 :goto_6

    .line 178
    :cond_4
    invoke-virtual {v4, v7, v3, v2}, Lcom/pspdfkit/internal/zf;->getPageText(III)Ljava/lang/String;

    move-result-object v5

    const-string v6, "\r\n"

    .line 179
    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto/16 :goto_6

    .line 185
    :cond_5
    invoke-virtual {v4, v7, v3}, Lcom/pspdfkit/internal/zf;->b(II)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 186
    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getRects()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_6

    goto :goto_4

    .line 195
    :cond_6
    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getRects()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRect()Landroid/graphics/RectF;

    move-result-object v5

    .line 196
    iget v6, v12, Landroid/graphics/PointF;->x:F

    iget v8, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v5, v9

    add-float/2addr v5, v8

    cmpl-float v5, v6, v5

    if-lez v5, :cond_7

    add-int/lit8 v3, v3, 0x1

    .line 206
    :cond_7
    iget-object v5, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v5, v5, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v5}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v5

    .line 207
    iget-object v6, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v6, v6, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v6}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result v6

    if-ne v0, v10, :cond_8

    goto :goto_2

    :cond_8
    move v6, v3

    move v3, v5

    :goto_2
    if-le v3, v6, :cond_a

    if-ne v0, v10, :cond_9

    .line 221
    sget-object v0, Lcom/pspdfkit/internal/zt$b;->c:Lcom/pspdfkit/internal/zt$b;

    goto :goto_3

    :cond_9
    move-object v0, v10

    :goto_3
    move v13, v6

    move v6, v3

    move v3, v13

    .line 225
    :cond_a
    iget-object v5, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v5, v5, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v5}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v5

    if-ne v3, v5, :cond_b

    iget-object v5, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget-object v5, v5, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    .line 226
    invoke-virtual {v5}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result v5

    if-eq v6, v5, :cond_e

    .line 227
    :cond_b
    new-instance v5, Lcom/pspdfkit/datastructures/Range;

    sub-int/2addr v6, v3

    invoke-direct {v5, v3, v6}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    invoke-static {v4, v7, v5}, Lcom/pspdfkit/datastructures/TextSelection;->fromTextRange(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/Range;)Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object v3

    .line 229
    invoke-direct {p0, v3, v0}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V

    goto :goto_6

    :cond_c
    :goto_4
    new-array v0, v1, [Ljava/lang/Object;

    const-string v3, "Could not extract character rect for previously fetched touched index."

    .line 230
    invoke-static {v8, v3, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 231
    :cond_d
    :goto_5
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    new-array v3, v1, [Ljava/lang/Object;

    const-string v4, "Invalid state while trying to drag selection."

    invoke-static {v8, v0, v4, v3}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_e
    :goto_6
    const/4 v0, 0x1

    goto :goto_7

    :cond_f
    const/4 v0, 0x0

    :goto_7
    if-eqz v0, :cond_17

    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/zt;->a(F)V

    goto/16 :goto_a

    .line 239
    :cond_10
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    sget-object v3, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    if-eq v0, v3, :cond_11

    .line 240
    iput-object v3, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    .line 241
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->r:Lcom/pspdfkit/internal/zt$d;

    if-eqz v0, :cond_11

    .line 242
    check-cast v0, Lcom/pspdfkit/internal/h9;

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/zt$b;)V

    .line 243
    :cond_11
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->x:Lcom/pspdfkit/internal/uh;

    if-eqz v0, :cond_12

    .line 244
    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    :cond_12
    const/4 v0, 0x1

    goto :goto_a

    .line 245
    :cond_13
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    .line 246
    new-instance v3, Landroid/graphics/Rect;

    .line 247
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v4, v0

    .line 248
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v5, v0

    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    add-int/2addr v6, v0

    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    add-int/2addr v7, v0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 252
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 253
    sget-object v0, Lcom/pspdfkit/internal/zt$b;->b:Lcom/pspdfkit/internal/zt$b;

    iput-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    .line 254
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->r:Lcom/pspdfkit/internal/zt$d;

    if-eqz v3, :cond_15

    .line 255
    check-cast v3, Lcom/pspdfkit/internal/h9;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/zt$b;)V

    goto :goto_8

    .line 258
    :cond_14
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 259
    sget-object v0, Lcom/pspdfkit/internal/zt$b;->c:Lcom/pspdfkit/internal/zt$b;

    iput-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    .line 260
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->r:Lcom/pspdfkit/internal/zt$d;

    if-eqz v3, :cond_15

    .line 261
    check-cast v3, Lcom/pspdfkit/internal/h9;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/zt$b;)V

    :cond_15
    :goto_8
    const/4 v0, 0x1

    goto :goto_9

    :cond_16
    const/4 v0, 0x0

    :goto_9
    if-eqz v0, :cond_17

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/zt;->a(F)V

    .line 297
    :cond_17
    :goto_a
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->h:Lcom/pspdfkit/internal/o7;

    invoke-virtual {v3, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_18

    if-eqz v0, :cond_19

    :cond_18
    const/4 v1, 0x1

    :cond_19
    return v1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 5

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.TextSelection"

    const-string v3, "Leaving text selection mode."

    .line 1
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    iget v3, v2, Lcom/pspdfkit/datastructures/TextSelection;->pageIndex:I

    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextSelection;->textRange:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/ui/PdfFragment;->enterTextSelectionMode(ILcom/pspdfkit/datastructures/Range;)V

    return v0

    .line 9
    :cond_0
    iput-object v4, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    .line 10
    iput-object v4, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->j()V

    return v2
.end method

.method public final f()Lcom/pspdfkit/datastructures/TextSelection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->u:Lcom/pspdfkit/internal/zt$b;

    sget-object v1, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.TextSelection"

    const-string v2, "Leaving text selection mode."

    .line 1
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    iget-object v1, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/specialMode/handler/e;->b(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/datastructures/TextSelection;)Z

    .line 3
    iput-object v2, p0, Lcom/pspdfkit/internal/zt;->t:Lcom/pspdfkit/datastructures/TextSelection;

    .line 4
    iput-object v2, p0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->k()V

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)Z
    .locals 2

    const-string v0, "isLinkCreationDialogShown"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/zt;->w:Z

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    const-string v1, "linkCreationDialogText"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/e;->a(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->d()Z

    move-result v0

    const-string v1, "isLinkCreationDialogShown"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zt;->f:Lcom/pspdfkit/internal/specialMode/handler/e;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/e;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "linkCreationDialogText"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

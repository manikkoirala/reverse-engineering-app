.class public final Lcom/pspdfkit/internal/ym;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativeDocument;

.field private final b:I

.field private final c:Lcom/pspdfkit/internal/jni/NativePage;

.field private d:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/jni/NativeDocument;II)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/ym;->d:Ljava/lang/String;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/ym;->a:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 7
    iput p3, p0, Lcom/pspdfkit/internal/ym;->b:I

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    .line 8
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p3, v1

    const-string v0, "PSPDFKit.Document"

    const-string v1, "Loading page %d."

    invoke-static {v0, v1, p3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPage(I)Lcom/pspdfkit/internal/jni/NativePage;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    return-void
.end method

.method private b()Lcom/pspdfkit/internal/jni/NativeTextParser;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 28
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativePage;->getTextParser()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v1
.end method


# virtual methods
.method public final a(FF)I
    .locals 2

    .line 44
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 46
    :cond_0
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    const/high16 p1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/jni/NativeTextParser;->charIndexAt(Landroid/graphics/PointF;F)I

    move-result p1

    return p1
.end method

.method public final a(Landroid/graphics/PointF;F)Landroid/graphics/RectF;
    .locals 2

    .line 47
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 50
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textRectAt(Landroid/graphics/PointF;F)Lcom/pspdfkit/internal/jni/NativeRectDescriptor;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v1

    .line 53
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRect()Landroid/graphics/RectF;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/document/PdfBox;)Landroid/graphics/RectF;
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 10
    :cond_0
    const-class v1, Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/sj;->b(Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jni/NativePDFBoxType;

    .line 11
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativePage;->getBox(Lcom/pspdfkit/internal/jni/NativePDFBoxType;)Landroid/graphics/RectF;

    move-result-object p1

    return-object p1
.end method

.method public final a(FFF)Lcom/pspdfkit/internal/jni/NativeTextRange;
    .locals 3

    .line 38
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 41
    :cond_0
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v2, p3}, Lcom/pspdfkit/internal/jni/NativeTextParser;->wordsAt(Landroid/graphics/PointF;F)Ljava/util/ArrayList;

    move-result-object p1

    .line 42
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x0

    .line 43
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/jni/NativeTextRange;

    return-object p1

    :cond_1
    return-object v1
.end method

.method public final a(II)Ljava/lang/String;
    .locals 1

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 15
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textForRange(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/graphics/RectF;)Ljava/lang/String;
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 19
    :cond_0
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativeTextParser;->getTextForRect(Landroid/graphics/RectF;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/TextBlock;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 23
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 24
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/datastructures/TextBlock;

    .line 25
    iget-object v2, v2, Lcom/pspdfkit/datastructures/TextBlock;->range:Lcom/pspdfkit/datastructures/Range;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeTextParser;->getTextForRanges(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Z)Ljava/util/ArrayList;
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textRects()Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getMarkupRects()Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTextRange;->getRects()Ljava/util/ArrayList;

    move-result-object p1

    .line 31
    :goto_0
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/graphics/RectF;Z)Ljava/util/List;
    .locals 2

    .line 32
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    .line 33
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x1

    .line 36
    invoke-virtual {v0, p1, v1, p2, v1}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textRectsBoundedByRect(Landroid/graphics/RectF;ZZZ)Ljava/util/ArrayList;

    move-result-object p1

    .line 37
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->c(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/pspdfkit/internal/ym;->d:Ljava/lang/String;

    return-void
.end method

.method public final a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/pspdfkit/internal/jni/NativePage;->renderPage(Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;Ljava/lang/Integer;)Z

    move-result p1

    return p1
.end method

.method public final a(Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z
    .locals 8

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 4
    :cond_0
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/pspdfkit/internal/jni/NativePage;->renderPage(Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;Ljava/lang/Integer;)Z

    move-result p1

    return p1
.end method

.method public final a(Landroid/graphics/Bitmap;Lcom/pspdfkit/internal/yl;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z
    .locals 6

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->c:Lcom/pspdfkit/internal/jni/NativePage;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 7
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/internal/yl;->c()Lcom/pspdfkit/internal/jni/NativePageCache;

    move-result-object v2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    .line 8
    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/jni/NativePage;->renderPageWithCache(Landroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageCache;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;Ljava/lang/Integer;)Z

    move-result p1

    return p1
.end method

.method public final b(II)Lcom/pspdfkit/internal/jni/NativeTextRange;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 4
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textRectsForRange(II)Lcom/pspdfkit/internal/jni/NativeTextRange;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 7

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    .line 6
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v0, :cond_0

    goto :goto_1

    .line 8
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 11
    invoke-virtual {v0, v2, v3, v4, v4}, Lcom/pspdfkit/internal/jni/NativeTextParser;->textRectsBoundedByRect(Landroid/graphics/RectF;ZZZ)Ljava/util/ArrayList;

    move-result-object v2

    .line 12
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;

    .line 14
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRange()Lcom/pspdfkit/datastructures/Range;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v4

    .line 15
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRange()Lcom/pspdfkit/datastructures/Range;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v5

    .line 16
    invoke-virtual {p0, v4, v5}, Lcom/pspdfkit/internal/ym;->a(II)Ljava/lang/String;

    move-result-object v4

    .line 19
    iget v5, p0, Lcom/pspdfkit/internal/ym;->b:I

    .line 21
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRange()Lcom/pspdfkit/datastructures/Range;

    move-result-object v6

    .line 22
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeRectDescriptor;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 23
    invoke-static {v5, v6, v3, v4}, Lcom/pspdfkit/datastructures/TextBlock;->create(ILcom/pspdfkit/datastructures/Range;Ljava/util/List;Ljava/lang/String;)Lcom/pspdfkit/datastructures/TextBlock;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1

    .line 24
    :cond_3
    :goto_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTextParser;->text()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/pspdfkit/internal/ym;->d:Ljava/lang/String;

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ym;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ym;->b()Lcom/pspdfkit/internal/jni/NativeTextParser;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeTextParser;->count()I

    move-result v0

    return v0
.end method

.class final Lcom/pspdfkit/internal/co$a;
.super Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/co;->a(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Lcom/pspdfkit/internal/co$b;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/rxjava3/subscribers/DisposableSubscriber<",
        "Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/co$b;

.field final synthetic b:Ljava/io/File;

.field final synthetic c:Z

.field final synthetic d:Lcom/pspdfkit/internal/co;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/co;Lcom/pspdfkit/internal/co$b;Ljava/io/File;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    iput-object p2, p0, Lcom/pspdfkit/internal/co$a;->a:Lcom/pspdfkit/internal/co$b;

    iput-object p3, p0, Lcom/pspdfkit/internal/co$a;->b:Ljava/io/File;

    iput-boolean p4, p0, Lcom/pspdfkit/internal/co$a;->c:Z

    invoke-direct {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    invoke-static {v0}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetc(Lcom/pspdfkit/internal/co;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/co$a;->b:Ljava/io/File;

    .line 4
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    invoke-static {v3}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->k()Ljava/lang/String;

    move-result-object v3

    .line 5
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocument(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/zf;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fputd(Lcom/pspdfkit/internal/co;Lcom/pspdfkit/internal/zf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fputh(Lcom/pspdfkit/internal/co;Z)V

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const/4 v2, 0x5

    .line 17
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/co$a$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/co$a$a;-><init>(Lcom/pspdfkit/internal/co$a;)V

    .line 19
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    return-void

    .line 20
    :catch_0
    iget-object v0, p0, Lcom/pspdfkit/internal/co$a;->a:Lcom/pspdfkit/internal/co$b;

    check-cast v0, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/bo$a;->b()V

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lio/reactivex/rxjava3/subscribers/DisposableSubscriber;->isDisposed()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/co$a;->a:Lcom/pspdfkit/internal/co$b;

    check-cast p1, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/bo$a;->b()V

    return-void
.end method

.method public final bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/document/processor/PdfProcessor$ProcessorProgress;

    return-void
.end method

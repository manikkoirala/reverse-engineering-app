.class final Lcom/pspdfkit/internal/t0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/q0;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/q0;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/t0;->a:Lcom/pspdfkit/internal/q0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/t0;->a:Lcom/pspdfkit/internal/q0;

    invoke-static {p1}, Lcom/pspdfkit/internal/q0;->c(Lcom/pspdfkit/internal/q0;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/t0;->a:Lcom/pspdfkit/internal/q0;

    invoke-static {p1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->stopRecording()V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/t0;->a:Lcom/pspdfkit/internal/q0;

    invoke-static {p1}, Lcom/pspdfkit/internal/q0;->b(Lcom/pspdfkit/internal/q0;)V

    return-void
.end method

.class public Lcom/pspdfkit/internal/a0;
.super Lcom/pspdfkit/internal/x1;
.source "SourceFile"


# instance fields
.field private final c:Lcom/pspdfkit/annotations/Annotation;

.field private final d:Z

.field private e:Ljava/lang/String;

.field private f:Landroid/graphics/Bitmap;

.field private g:[B


# direct methods
.method public synthetic constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;I)V
    .locals 0

    const/4 p3, 0x0

    .line 8
    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Z)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0, p1, p3}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;Z)V

    .line 11
    iput-object p2, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    const/4 p1, 0x1

    .line 12
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageResourceId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 15
    iput-object p2, p0, Lcom/pspdfkit/internal/a0;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/x1;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    iput-boolean p2, p0, Lcom/pspdfkit/internal/a0;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;[B)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compressedBitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/a0;->g:[B

    const/4 p1, 0x1

    .line 6
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    .line 7
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    return-void
.end method

.method public final a([B)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/a0;->g:[B

    return-void
.end method

.method public g()Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v1

    const/4 v7, 0x0

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 4
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->k()[B

    move-result-object v1

    if-nez v1, :cond_1

    return v7

    .line 7
    :cond_1
    new-instance v6, Lcom/pspdfkit/internal/s7;

    new-instance v2, Lcom/pspdfkit/internal/bj;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/bj;-><init>([B)V

    invoke-direct {v6, v2}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 10
    iget-boolean v2, p0, Lcom/pspdfkit/internal/a0;->d:Z

    if-eqz v2, :cond_2

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->sort()V

    const-string v3, "annotation.boundingBox.apply { sort() }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 17
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    const/4 v9, 0x0

    invoke-direct {v3, v9, v9, v5, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 18
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-direct {v5, v9, v9, v8, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 19
    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    .line 20
    invoke-virtual {v4, v3, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 27
    iget-object v2, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v2

    .line 29
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v3, v9, v9, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v5, 0x0

    move-object v1, v2

    move-object v2, v0

    .line 30
    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->setImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Landroid/graphics/RectF;Landroid/graphics/Matrix;Lcom/pspdfkit/internal/jni/NativeImageScaleMode;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v1

    .line 42
    sget-object v5, Lcom/pspdfkit/internal/jni/NativeImageScaleMode;->SCALE_TO_FILL:Lcom/pspdfkit/internal/jni/NativeImageScaleMode;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, v0

    .line 43
    invoke-virtual/range {v1 .. v6}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->setImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Landroid/graphics/RectF;Landroid/graphics/Matrix;Lcom/pspdfkit/internal/jni/NativeImageScaleMode;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/lang/String;

    .line 53
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/a0;->e:Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    .line 54
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_5

    new-array v0, v7, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Annotations"

    const-string v2, "Couldn\'t set annotation bitmap"

    .line 55
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v7

    .line 59
    :cond_5
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/x1;->b(Z)V

    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    .line 63
    iput-object v0, p0, Lcom/pspdfkit/internal/a0;->g:[B

    return v1

    :cond_6
    :goto_3
    return v7
.end method

.method public final i()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final j()Landroid/graphics/Bitmap;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/a0;->e:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v0, :cond_1

    goto/16 :goto_0

    .line 7
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v3

    .line 8
    invoke-virtual {v3, v0, v1}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getImageInformation(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;

    move-result-object v3

    if-nez v3, :cond_2

    return-object v2

    .line 10
    :cond_2
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;->getOriginalSize()Lcom/pspdfkit/utils/Size;

    move-result-object v4

    if-nez v4, :cond_3

    .line 11
    new-instance v4, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;->getRect()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;->getRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-direct {v4, v5, v6}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 13
    :cond_3
    iget v5, v4, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-float v5, v5

    float-to-int v5, v5

    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-float v4, v6

    float-to-int v4, v4

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 14
    iget-object v5, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v5}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v5

    invoke-interface {v5}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    invoke-virtual {v5, v0, v1, v4}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getImageResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v0

    const-string v1, "annotation.internal.nati\u2026 imageResourceId, bitmap)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v1, v3

    const-string v0, "PSPDFKit.Annotations"

    const-string v3, "Couldn\'t retrieve annotation bitmap: %s"

    invoke-static {v0, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v2

    .line 21
    :cond_4
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;->getHasAlpha()Z

    move-result v0

    invoke-virtual {v4, v0}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    return-object v4

    :cond_5
    :goto_0
    return-object v2
.end method

.method protected final k()[B
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/internal/a0;->g:[B

    if-nez v2, :cond_0

    return-object v1

    .line 3
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/a0;->g:[B

    if-nez v2, :cond_3

    if-nez v0, :cond_1

    return-object v1

    .line 5
    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 6
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 7
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    goto :goto_0

    .line 9
    :cond_2
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x63

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 11
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/a0;->g:[B

    .line 13
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->g:[B

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->g:[B

    return-object v0
.end method

.method public final n()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public o()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->f:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->g:[B

    if-eqz v0, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/a0;->e:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 8
    iget-object v4, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 9
    iget-object v4, p0, Lcom/pspdfkit/internal/a0;->c:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->getNativeResourceManager()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getImageInformation(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;)Lcom/pspdfkit/internal/jni/NativeImageResourceInformation;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_2
    return v2

    :cond_3
    :goto_1
    return v1
.end method

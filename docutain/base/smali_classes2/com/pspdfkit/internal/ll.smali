.class public final Lcom/pspdfkit/internal/ll;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ll$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/view/OrientationEventListener;

.field private c:J

.field private d:J

.field private e:I

.field private f:I

.field private g:Lcom/pspdfkit/internal/ll$b;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ll;)J
    .locals 2

    iget-wide v0, p0, Lcom/pspdfkit/internal/ll;->c:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ll;)J
    .locals 2

    iget-wide v0, p0, Lcom/pspdfkit/internal/ll;->d:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ll;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ll;->e:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/ll;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/ll;->f:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/ll;)Lcom/pspdfkit/internal/ll$b;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ll;->g:Lcom/pspdfkit/internal/ll$b;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputc(Lcom/pspdfkit/internal/ll;J)V
    .locals 0

    iput-wide p1, p0, Lcom/pspdfkit/internal/ll;->c:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/ll;J)V
    .locals 0

    iput-wide p1, p0, Lcom/pspdfkit/internal/ll;->d:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fpute(Lcom/pspdfkit/internal/ll;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/ll;->e:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/ll;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/ll;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 2
    iput-wide v0, p0, Lcom/pspdfkit/internal/ll;->c:J

    .line 3
    iput-wide v0, p0, Lcom/pspdfkit/internal/ll;->d:J

    const/4 v0, 0x1

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/ll;->e:I

    .line 5
    iput v0, p0, Lcom/pspdfkit/internal/ll;->f:I

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/ll;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ll;->b:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ll;->g:Lcom/pspdfkit/internal/ll$b;

    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ll;->b:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ll$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/ll;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/internal/ll$a;-><init>(Lcom/pspdfkit/internal/ll;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ll;->b:Landroid/view/OrientationEventListener;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ll;->b:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    return-void
.end method

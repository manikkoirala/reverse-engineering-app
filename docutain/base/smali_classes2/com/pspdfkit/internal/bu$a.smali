.class final Lcom/pspdfkit/internal/bu$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/bu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/speech/tts/TextToSpeech;

.field private final c:Ljava/lang/String;

.field private d:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$qyl_Bq1jpL36fR0711e0iE_NFQA(Lcom/pspdfkit/internal/bu$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/bu$a;->a()V

    return-void
.end method

.method public static synthetic $r8$lambda$s7MZcledvaEznQN9IhVkDlnK4s8(Lcom/pspdfkit/internal/bu$a;Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/bu$a;->a(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$ma(Lcom/pspdfkit/internal/bu$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/bu$a;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bu$a;->a:Landroid/content/Context;

    .line 3
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/bu$a;->b:Landroid/speech/tts/TextToSpeech;

    .line 9
    invoke-static {p2}, Lcom/pspdfkit/internal/ft;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/bu$a;->c:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/bu$a;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 20
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/pspdfkit/internal/bu$a;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/bu$a;->b:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/bu$a;->b:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    return-void
.end method

.method private synthetic a(Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bu$a;->b:Landroid/speech/tts/TextToSpeech;

    new-instance v1, Lcom/pspdfkit/internal/bu$a$a;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/bu$a$a;-><init>(Lio/reactivex/rxjava3/core/CompletableEmitter;)V

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/bu$a;->b:Landroid/speech/tts/TextToSpeech;

    iget-object v0, p0, Lcom/pspdfkit/internal/bu$a;->c:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/CharSequence;ILandroid/os/Bundle;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public final onInit(I)V
    .locals 2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bu$a;->a:Landroid/content/Context;

    sget v0, Lcom/pspdfkit/R$string;->pspdf__tts_not_available:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    .line 2
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4
    :cond_0
    new-instance p1, Lcom/pspdfkit/internal/bu$a$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/bu$a$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/bu$a;)V

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Completable;->create(Lio/reactivex/rxjava3/core/CompletableOnSubscribe;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 24
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const/4 v1, 0x5

    .line 25
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    .line 26
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/bu$a$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/bu$a$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/bu$a;)V

    .line 27
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/bu$a;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_0
    return-void
.end method

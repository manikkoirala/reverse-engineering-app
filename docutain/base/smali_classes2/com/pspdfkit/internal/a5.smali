.class public final Lcom/pspdfkit/internal/a5;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$6FrxuOBjxuHr8w3-0U3I9wzmnyk(Lcom/pspdfkit/internal/a5;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/a5;->b(Lcom/pspdfkit/internal/a5;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SEin6mS5sJlek1ztSVAHfUC5aHY(Lcom/pspdfkit/internal/a5;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/a5;->a(Lcom/pspdfkit/internal/a5;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$XKkymAqQqfajHezMW9vJrPokZko(Lcom/pspdfkit/internal/a5;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/a5;->a(Lcom/pspdfkit/internal/a5;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/inspector/bottomsheet/c<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/a5;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object p0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->b()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/a5;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->setMeasuredHeight$pspdfkit_release(I)V

    .line 23
    iget-object p0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/a5;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object p0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;->c()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a5;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v1}, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/a5;)V

    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method public final a(II)V
    .locals 2

    const/4 v0, 0x0

    if-le p2, p1, :cond_0

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a5;->c()V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    sub-int/2addr p2, p1

    int-to-float p1, p2

    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 12
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 13
    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    goto :goto_0

    :cond_0
    if-le p1, p2, :cond_1

    .line 15
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a5;->c()V

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 19
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    sub-int/2addr p1, p2

    int-to-float p1, p1

    .line 20
    invoke-virtual {v0, p1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationY(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    .line 21
    new-instance v0, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/a5;I)V

    invoke-virtual {p1, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    :cond_1
    :goto_0
    return-void
.end method

.method public final b()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a5;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroidx/interpolator/view/animation/FastOutLinearInInterpolator;

    invoke-direct {v1}, Landroidx/interpolator/view/animation/FastOutLinearInInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/a5$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/a5;)V

    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a5;->a:Lcom/pspdfkit/internal/views/inspector/bottomsheet/c;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    return-void
.end method

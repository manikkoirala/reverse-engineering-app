.class public final Lcom/pspdfkit/internal/a1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/internal/jni/NativeAnnotationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:I

.field private final c:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/graphics/PointF;

.field private final g:Ljava/util/ArrayList;

.field private final h:Landroid/graphics/PointF;

.field private final i:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/a1;->j:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;ILjava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            "I",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->d:Ljava/util/List;

    .line 4
    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->e:Landroidx/collection/LongSparseArray;

    .line 8
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->f:Landroid/graphics/PointF;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a1;->i:Landroid/graphics/RectF;

    .line 37
    iput-object p1, p0, Lcom/pspdfkit/internal/a1;->a:Lcom/pspdfkit/internal/zf;

    .line 38
    iput p2, p0, Lcom/pspdfkit/internal/a1;->b:I

    if-nez p3, :cond_0

    .line 40
    const-class p1, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p1

    goto :goto_0

    .line 41
    :cond_0
    invoke-static {p3}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/pspdfkit/internal/a1;->c:Ljava/util/EnumSet;

    return-void
.end method


# virtual methods
.method public final a(FFLandroid/graphics/Matrix;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Landroid/graphics/Matrix;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->a:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    goto/16 :goto_7

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    invoke-static {p1, p3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->i:Landroid/graphics/RectF;

    iget-object p2, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    iget v0, p2, Landroid/graphics/PointF;->x:F

    iget p2, p2, Landroid/graphics/PointF;->y:F

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v2, v0, v1

    add-float/2addr v1, p2

    invoke-virtual {p1, v0, p2, v2, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 26
    monitor-enter p0

    .line 27
    :try_start_0
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->f:Landroid/graphics/PointF;

    iget-object p2, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    invoke-virtual {p1, p2}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    if-eqz p4, :cond_1

    .line 29
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    .line 30
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-eqz p3, :cond_2

    .line 31
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 33
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    move-object p2, p1

    .line 34
    :goto_1
    monitor-exit p0

    return-object p2

    .line 36
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 39
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 41
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/a1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_2

    .line 44
    :cond_5
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 46
    :cond_6
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 49
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->l()Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;

    move-result-object v0

    .line 51
    invoke-static {p1}, Lcom/pspdfkit/internal/sj;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/jni/NativeAnnotationPager;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/a1;->i:Landroid/graphics/RectF;

    new-instance v2, Lcom/pspdfkit/internal/jni/NativeAnnotationHitDetectionOptions;

    sget-object v3, Lcom/pspdfkit/internal/a1;->j:Ljava/util/ArrayList;

    iget v4, p0, Lcom/pspdfkit/internal/a1;->b:I

    int-to-float v4, v4

    .line 55
    invoke-static {v4, p3}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result p3

    const/4 v4, 0x1

    invoke-direct {v2, v3, p3, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationHitDetectionOptions;-><init>(Ljava/util/ArrayList;FZ)V

    .line 56
    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/jni/NativePdfObjectsHitDetector;->filterAndSortAnnotationsAtPdfRect(Lcom/pspdfkit/internal/jni/NativeAnnotationPager;Landroid/graphics/RectF;Lcom/pspdfkit/internal/jni/NativeAnnotationHitDetectionOptions;)Lcom/pspdfkit/internal/jni/NativeAnnotationPager;

    move-result-object p1

    .line 65
    iget-object p3, p0, Lcom/pspdfkit/internal/a1;->e:Landroidx/collection/LongSparseArray;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    .line 67
    :goto_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->size()I

    move-result v2

    if-ge v1, v2, :cond_a

    const/16 v2, 0x64

    .line 68
    invoke-virtual {p1, v1, v2}, Lcom/pspdfkit/internal/jni/NativeAnnotationPager;->get(II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    .line 69
    invoke-virtual {v3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v3

    invoke-virtual {p3, v3, v4}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/Annotation;

    if-eqz v3, :cond_8

    .line 71
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    add-int/lit8 v1, v1, 0x64

    goto :goto_3

    .line 72
    :cond_a
    monitor-enter p0

    .line 73
    :try_start_1
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->f:Landroid/graphics/PointF;

    iget-object p3, p0, Lcom/pspdfkit/internal/a1;->h:Landroid/graphics/PointF;

    invoke-virtual {p1, p3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 74
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 75
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 76
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    if-eqz p4, :cond_b

    .line 77
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_6

    .line 78
    :cond_b
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-eqz p3, :cond_c

    .line 79
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_5

    .line 81
    :cond_c
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_5
    move-object p2, p1

    .line 82
    :goto_6
    monitor-exit p0

    return-object p2

    :catchall_0
    move-exception p1

    .line 83
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 84
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1

    .line 85
    :cond_d
    :goto_7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/a1;->d:Ljava/util/List;

    .line 2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/a1;->e:Landroidx/collection/LongSparseArray;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide v2

    .line 6
    invoke-virtual {v1, v2, v3, v0}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    .line 12
    :cond_1
    monitor-enter p0

    .line 13
    :try_start_0
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->f:Landroid/graphics/PointF;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Landroid/graphics/PointF;->set(FF)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/a1;->g:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 15
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/a1;->c:Ljava/util/EnumSet;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->f(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

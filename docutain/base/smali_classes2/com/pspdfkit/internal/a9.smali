.class public final Lcom/pspdfkit/internal/a9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/s8;


# instance fields
.field private final a:Lcom/pspdfkit/internal/r8;

.field private b:Lcom/pspdfkit/internal/t8;

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/z8;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/a9;->a:Lcom/pspdfkit/internal/r8;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/b9;
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/pspdfkit/internal/b9;

    invoke-interface {v0}, Lcom/pspdfkit/internal/q8;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/b9;-><init>(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public final a(Lcom/pspdfkit/internal/t8;Lcom/pspdfkit/internal/b9;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/a9;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    if-eqz p2, :cond_1

    .line 7
    invoke-virtual {p2}, Lcom/pspdfkit/internal/b9;->a()Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/q8;->setItems(Ljava/util/List;)V

    goto :goto_0

    .line 9
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/a9;->a:Lcom/pspdfkit/internal/r8;

    check-cast p2, Lcom/pspdfkit/internal/z8;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/z8;->c()Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 10
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 11
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/pspdfkit/internal/a9$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/a9$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/t8;)V

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/a9;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 14
    :goto_0
    iget-object p2, p0, Lcom/pspdfkit/internal/a9;->a:Lcom/pspdfkit/internal/r8;

    check-cast p2, Lcom/pspdfkit/internal/z8;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/z8;->a()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/t8;->setEditingEnabled(Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/internal/q8;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;

    .line 5
    invoke-interface {v1}, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;->onDocumentInfoViewEditingModeExit()Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 10
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    invoke-interface {v0}, Lcom/pspdfkit/internal/q8;->getItems()Ljava/util/List;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u8;

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/internal/u8;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/w8;

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/a9;->a:Lcom/pspdfkit/internal/r8;

    check-cast v3, Lcom/pspdfkit/internal/z8;

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/z8;->a(Lcom/pspdfkit/internal/w8;)V

    goto :goto_0

    .line 16
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->a:Lcom/pspdfkit/internal/r8;

    check-cast v0, Lcom/pspdfkit/internal/z8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z8;->d()V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    invoke-interface {v0}, Lcom/pspdfkit/internal/q8;->a()V

    goto :goto_1

    .line 19
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;

    .line 21
    invoke-interface {v1}, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;->onDocumentInfoViewEditingModeEnter()Z

    move-result v1

    if-eqz v1, :cond_6

    return-void

    .line 24
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    invoke-interface {v0}, Lcom/pspdfkit/internal/q8;->b()V

    :goto_1
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/a9;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/a9;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/a9;->b:Lcom/pspdfkit/internal/t8;

    return-void
.end method

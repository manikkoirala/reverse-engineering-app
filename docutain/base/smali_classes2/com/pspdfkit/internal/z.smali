.class public final Lcom/pspdfkit/internal/z;
.super Lcom/pspdfkit/internal/x1;
.source "SourceFile"


# instance fields
.field private final c:Lcom/pspdfkit/annotations/SoundAnnotation;

.field private d:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/x1;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioSource"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/z;->d:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    const/4 p1, 0x1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/SoundAnnotation;Ljava/lang/String;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/z;-><init>(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/internal/z;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final g()Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/z;->d:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    if-nez v0, :cond_1

    return v1

    .line 4
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-nez v2, :cond_2

    return v1

    .line 5
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v3

    if-nez v3, :cond_3

    return v1

    .line 7
    :cond_3
    new-instance v4, Lcom/pspdfkit/internal/s7;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 9
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    const-string v3, "document.annotationProvider.nativeResourceManager"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "PSPDFKit.Annotations"

    const/4 v6, 0x1

    if-eqz v3, :cond_5

    .line 13
    invoke-virtual {v0, v2, v3, v4}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->setResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v0

    const-string v2, "resourceManager.setResou\u2026istingResource, provider)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v2

    if-eqz v2, :cond_4

    new-array v2, v6, [Ljava/lang/Object;

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    const-string v0, "Couldn\'t attach audio data to sound annotation: %s"

    invoke-static {v5, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 18
    :cond_4
    iput-object v3, p0, Lcom/pspdfkit/internal/z;->e:Ljava/lang/String;

    goto :goto_0

    .line 21
    :cond_5
    invoke-virtual {v0, v2, v4}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->createSoundResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/z;->e:Ljava/lang/String;

    .line 22
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Couldn\'t attach audio data to sound annotation."

    .line 23
    invoke-static {v5, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_6
    :goto_0
    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/internal/z;->d:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    .line 29
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    return v6

    :cond_7
    :goto_1
    return v1
.end method

.method public final h()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z;->d:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 4
    iget-object v3, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->requireNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v3

    const-string v4, "annotation.internal.requireNativeAnnotation()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 9
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v2

    const-string v4, "internalDocument.annotat\u2026der.nativeResourceManager"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->findResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 12
    new-instance v4, Lcom/pspdfkit/internal/s7;

    new-instance v5, Lcom/pspdfkit/internal/bj;

    new-array v1, v1, [B

    invoke-direct {v5, v1}, Lcom/pspdfkit/internal/bj;-><init>([B)V

    invoke-direct {v4, v5}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->createSoundResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeDataProvider;)Ljava/lang/String;

    .line 18
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v1

    const-string v2, "annotation.internal.properties"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getSampleSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x2711

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getSampleRate()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x2712

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 21
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getChannels()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x2713

    invoke-virtual {v1, v3, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 22
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;->getAudioEncoding()Lcom/pspdfkit/annotations/sound/AudioEncoding;

    move-result-object v0

    const/16 v2, 0x2714

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1

    .line 23
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Calling this method for a detached annotation is not supported."

    .line 24
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i()[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const-string v1, "outputStream"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "argumentName"

    .line 3
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 54
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    iget-object v1, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 57
    iget-object v2, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 60
    iget-object v3, p0, Lcom/pspdfkit/internal/z;->e:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 63
    new-instance v4, Lcom/pspdfkit/internal/sl;

    invoke-direct {v4, v0}, Lcom/pspdfkit/internal/sl;-><init>(Ljava/io/OutputStream;)V

    .line 64
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v5

    const-string v6, "document.annotationProvider.nativeResourceManager"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object v2

    invoke-virtual {v5, v2, v1, v3, v4}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->getResource(Lcom/pspdfkit/internal/jni/NativeDocument;Lcom/pspdfkit/internal/jni/NativeAnnotation;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeDataSink;)Lcom/pspdfkit/internal/jni/NativeResult;

    move-result-object v1

    const-string v2, "nativeResourceManager.ge\u2026on, resourceId, dataSink)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeResult;->getHasError()Z

    move-result v2

    if-nez v2, :cond_0

    .line 67
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v1, "byteArrayOutputStream.toByteArray()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 68
    :cond_0
    new-instance v0, Ljava/io/IOException;

    sget-object v2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/jni/NativeResult;->getErrorString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Couldn\'t retrieve embedded audio data: %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "format(format, *args)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Audio resource must be attached to the document."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Document must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Annotation must be attached to document."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z;->c:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/z;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

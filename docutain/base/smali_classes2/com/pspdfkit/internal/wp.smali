.class public final Lcom/pspdfkit/internal/wp;
.super Lcom/pspdfkit/internal/a6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/wp$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/a6<",
        "Lcom/pspdfkit/internal/wp$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final e:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/wp$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/wp$a;


# direct methods
.method private constructor <init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/a6;-><init>()V

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->RESTORE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/wp;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 12
    sget-object v0, Lcom/pspdfkit/internal/wp$a;->Companion:Lcom/pspdfkit/internal/wp$a$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wp$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/wp;->e:Lkotlinx/serialization/KSerializer;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/wp$a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/pspdfkit/internal/wp$a;-><init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/wp;->f:Lcom/pspdfkit/internal/wp$a;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/wp;-><init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wp;->f:Lcom/pspdfkit/internal/wp$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wp;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wp;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

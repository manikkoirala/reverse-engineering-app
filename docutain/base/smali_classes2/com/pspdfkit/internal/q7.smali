.class public final Lcom/pspdfkit/internal/q7;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/q7$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/q7$a;

.field private final c:Landroid/os/Handler;

.field private d:F

.field private e:F

.field private f:Z

.field private g:Z

.field private h:F

.field private i:F

.field private j:F

.field private k:Z

.field private final l:I

.field private final m:I

.field private n:F

.field private o:F

.field private p:F

.field private q:I

.field private r:J

.field private final s:I

.field private t:F

.field private u:F

.field private v:I

.field private w:Landroid/view/GestureDetector;

.field private x:Z


# direct methods
.method static bridge synthetic -$$Nest$fputt(Lcom/pspdfkit/internal/q7;F)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/q7;->t:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputu(Lcom/pspdfkit/internal/q7;F)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/q7;->u:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputv(Lcom/pspdfkit/internal/q7;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/q7;->v:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/q7$a;Landroid/os/Handler;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/q7;->v:I

    .line 31
    iput-object p1, p0, Lcom/pspdfkit/internal/q7;->a:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/pspdfkit/internal/q7;->b:Lcom/pspdfkit/internal/q7$a;

    .line 33
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p2

    mul-int/lit8 p2, p2, 0x2

    iput p2, p0, Lcom/pspdfkit/internal/q7;->l:I

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const-string v0, "config_minScalingTouchMajor"

    const-string v1, "dimen"

    const-string v2, "android"

    .line 38
    invoke-virtual {p2, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/q7;->s:I

    goto :goto_0

    :cond_0
    const/16 v0, 0x30

    .line 43
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/q7;->s:I

    :goto_0
    const-string v0, "config_minScalingSpan"

    .line 46
    invoke-virtual {p2, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/pspdfkit/internal/q7;->m:I

    goto :goto_2

    .line 51
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/e8;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x20

    goto :goto_1

    :cond_2
    const/16 v0, 0x1b

    :goto_1
    int-to-float v0, v0

    .line 53
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    const/4 v1, 0x5

    invoke-static {v1, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lcom/pspdfkit/internal/q7;->m:I

    .line 56
    :goto_2
    iput-object p3, p0, Lcom/pspdfkit/internal/q7;->c:Landroid/os/Handler;

    .line 57
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q7;->e()V

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 p2, 0x16

    if-le p1, p2, :cond_3

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/internal/q7;->f()V

    :cond_3
    return-void
.end method

.method private d()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/q7;->v:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final a()F
    .locals 1

    .line 176
    iget v0, p0, Lcom/pspdfkit/internal/q7;->d:F

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 6
    iget-boolean v3, v0, Lcom/pspdfkit/internal/q7;->f:Z

    if-eqz v3, :cond_0

    .line 7
    iget-object v3, v0, Lcom/pspdfkit/internal/q7;->w:Landroid/view/GestureDetector;

    invoke-virtual {v3, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 10
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    .line 11
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x17

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-lt v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v4

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 14
    :goto_1
    iget v5, v0, Lcom/pspdfkit/internal/q7;->v:I

    const/4 v8, 0x2

    if-ne v5, v8, :cond_3

    if-nez v4, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eq v2, v6, :cond_5

    const/4 v9, 0x3

    if-eq v2, v9, :cond_5

    if-eqz v5, :cond_4

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v5, 0x1

    :goto_4
    const/4 v9, 0x0

    if-eqz v2, :cond_6

    if-eqz v5, :cond_9

    .line 22
    :cond_6
    iget-boolean v10, v0, Lcom/pspdfkit/internal/q7;->k:Z

    if-eqz v10, :cond_7

    .line 23
    iget-object v10, v0, Lcom/pspdfkit/internal/q7;->b:Lcom/pspdfkit/internal/q7$a;

    invoke-interface {v10, v0}, Lcom/pspdfkit/internal/q7$a;->a(Lcom/pspdfkit/internal/q7;)V

    .line 24
    iput-boolean v7, v0, Lcom/pspdfkit/internal/q7;->k:Z

    .line 25
    iput v9, v0, Lcom/pspdfkit/internal/q7;->j:F

    .line 26
    iput v7, v0, Lcom/pspdfkit/internal/q7;->v:I

    goto :goto_5

    .line 27
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v10

    if-eqz v10, :cond_8

    if-eqz v5, :cond_8

    .line 28
    iput-boolean v7, v0, Lcom/pspdfkit/internal/q7;->k:Z

    .line 29
    iput v9, v0, Lcom/pspdfkit/internal/q7;->j:F

    .line 30
    iput v7, v0, Lcom/pspdfkit/internal/q7;->v:I

    :cond_8
    :goto_5
    if-eqz v5, :cond_9

    const/high16 v1, 0x7fc00000    # Float.NaN

    .line 31
    iput v1, v0, Lcom/pspdfkit/internal/q7;->n:F

    .line 32
    iput v1, v0, Lcom/pspdfkit/internal/q7;->o:F

    .line 33
    iput v1, v0, Lcom/pspdfkit/internal/q7;->p:F

    .line 34
    iput v7, v0, Lcom/pspdfkit/internal/q7;->q:I

    const-wide/16 v1, 0x0

    .line 35
    iput-wide v1, v0, Lcom/pspdfkit/internal/q7;->r:J

    return-void

    .line 36
    :cond_9
    iget-boolean v5, v0, Lcom/pspdfkit/internal/q7;->k:Z

    if-nez v5, :cond_a

    iget-boolean v5, v0, Lcom/pspdfkit/internal/q7;->g:Z

    if-eqz v5, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v5

    if-nez v5, :cond_a

    if-eqz v4, :cond_a

    .line 38
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, v0, Lcom/pspdfkit/internal/q7;->t:F

    .line 39
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, v0, Lcom/pspdfkit/internal/q7;->u:F

    .line 40
    iput v8, v0, Lcom/pspdfkit/internal/q7;->v:I

    .line 41
    iput v9, v0, Lcom/pspdfkit/internal/q7;->j:F

    :cond_a
    const/4 v4, 0x6

    if-eqz v2, :cond_c

    if-eq v2, v4, :cond_c

    const/4 v5, 0x5

    if-ne v2, v5, :cond_b

    goto :goto_6

    :cond_b
    const/4 v5, 0x0

    goto :goto_7

    :cond_c
    :goto_6
    const/4 v5, 0x1

    :goto_7
    if-ne v2, v4, :cond_d

    const/4 v4, 0x1

    goto :goto_8

    :cond_d
    const/4 v4, 0x0

    :goto_8
    if-eqz v4, :cond_e

    .line 49
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v10

    goto :goto_9

    :cond_e
    const/4 v10, -0x1

    :goto_9
    if-eqz v4, :cond_f

    add-int/lit8 v4, v3, -0x1

    goto :goto_a

    :cond_f
    move v4, v3

    .line 56
    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v11

    if-eqz v11, :cond_11

    .line 59
    iget v11, v0, Lcom/pspdfkit/internal/q7;->t:F

    .line 60
    iget v12, v0, Lcom/pspdfkit/internal/q7;->u:F

    .line 61
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v13

    cmpg-float v13, v13, v12

    if-gez v13, :cond_10

    const/4 v13, 0x1

    goto :goto_b

    :cond_10
    const/4 v13, 0x0

    :goto_b
    iput-boolean v13, v0, Lcom/pspdfkit/internal/q7;->x:Z

    goto :goto_e

    :cond_11
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_c
    if-ge v11, v3, :cond_13

    if-ne v10, v11, :cond_12

    goto :goto_d

    .line 65
    :cond_12
    invoke-virtual {v1, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v14

    add-float/2addr v13, v14

    .line 66
    invoke-virtual {v1, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v14

    add-float/2addr v12, v14

    :goto_d
    add-int/lit8 v11, v11, 0x1

    goto :goto_c

    :cond_13
    int-to-float v11, v4

    div-float/2addr v13, v11

    div-float/2addr v12, v11

    move v11, v13

    .line 67
    :goto_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    .line 68
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v15

    .line 69
    iget-wide v8, v0, Lcom/pspdfkit/internal/q7;->r:J

    sub-long/2addr v13, v8

    const-wide/16 v8, 0x80

    cmp-long v17, v13, v8

    if-ltz v17, :cond_14

    const/4 v8, 0x1

    goto :goto_f

    :cond_14
    const/4 v8, 0x0

    :goto_f
    const/4 v9, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_10
    if-ge v9, v15, :cond_1f

    .line 73
    iget v7, v0, Lcom/pspdfkit/internal/q7;->p:F

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    xor-int/2addr v7, v6

    .line 74
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    move/from16 v18, v15

    add-int/lit8 v15, v6, 0x1

    move/from16 v19, v8

    const/4 v8, 0x0

    :goto_11
    if-ge v8, v15, :cond_1e

    if-ge v8, v6, :cond_15

    .line 79
    invoke-virtual {v1, v9, v8}, Landroid/view/MotionEvent;->getHistoricalTouchMajor(II)F

    move-result v20

    goto :goto_12

    .line 81
    :cond_15
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v20

    :goto_12
    move/from16 v21, v2

    .line 83
    iget v2, v0, Lcom/pspdfkit/internal/q7;->s:I

    int-to-float v2, v2

    cmpg-float v22, v20, v2

    if-gez v22, :cond_16

    goto :goto_13

    :cond_16
    move/from16 v2, v20

    :goto_13
    add-float/2addr v13, v2

    move/from16 v20, v13

    .line 86
    iget v13, v0, Lcom/pspdfkit/internal/q7;->n:F

    invoke-static {v13}, Ljava/lang/Float;->isNaN(F)Z

    move-result v13

    if-nez v13, :cond_17

    iget v13, v0, Lcom/pspdfkit/internal/q7;->n:F

    cmpl-float v13, v2, v13

    if-lez v13, :cond_18

    .line 87
    :cond_17
    iput v2, v0, Lcom/pspdfkit/internal/q7;->n:F

    .line 89
    :cond_18
    iget v13, v0, Lcom/pspdfkit/internal/q7;->o:F

    invoke-static {v13}, Ljava/lang/Float;->isNaN(F)Z

    move-result v13

    if-nez v13, :cond_19

    iget v13, v0, Lcom/pspdfkit/internal/q7;->o:F

    cmpg-float v13, v2, v13

    if-gez v13, :cond_1a

    .line 90
    :cond_19
    iput v2, v0, Lcom/pspdfkit/internal/q7;->o:F

    :cond_1a
    if-eqz v7, :cond_1d

    .line 94
    iget v13, v0, Lcom/pspdfkit/internal/q7;->p:F

    sub-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-int v2, v2

    .line 95
    iget v13, v0, Lcom/pspdfkit/internal/q7;->q:I

    if-ne v2, v13, :cond_1b

    if-nez v2, :cond_1d

    if-nez v13, :cond_1d

    .line 96
    :cond_1b
    iput v2, v0, Lcom/pspdfkit/internal/q7;->q:I

    if-ge v8, v6, :cond_1c

    .line 98
    invoke-virtual {v1, v8}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v22

    goto :goto_14

    :cond_1c
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v22

    :goto_14
    move/from16 v24, v6

    move v2, v7

    move-wide/from16 v6, v22

    iput-wide v6, v0, Lcom/pspdfkit/internal/q7;->r:J

    const/16 v19, 0x0

    goto :goto_15

    :cond_1d
    move/from16 v24, v6

    move v2, v7

    :goto_15
    add-int/lit8 v8, v8, 0x1

    move v7, v2

    move/from16 v13, v20

    move/from16 v2, v21

    move/from16 v6, v24

    goto :goto_11

    :cond_1e
    move/from16 v21, v2

    add-int/2addr v14, v15

    add-int/lit8 v9, v9, 0x1

    move/from16 v15, v18

    move/from16 v8, v19

    const/4 v6, 0x1

    const/4 v7, 0x0

    goto/16 :goto_10

    :cond_1f
    move/from16 v21, v2

    int-to-float v2, v14

    div-float/2addr v13, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz v8, :cond_20

    .line 109
    iget v6, v0, Lcom/pspdfkit/internal/q7;->n:F

    iget v7, v0, Lcom/pspdfkit/internal/q7;->o:F

    add-float v8, v6, v7

    add-float/2addr v8, v13

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    add-float/2addr v6, v8

    div-float/2addr v6, v2

    .line 110
    iput v6, v0, Lcom/pspdfkit/internal/q7;->n:F

    add-float/2addr v7, v8

    div-float/2addr v7, v2

    .line 111
    iput v7, v0, Lcom/pspdfkit/internal/q7;->o:F

    .line 112
    iput v8, v0, Lcom/pspdfkit/internal/q7;->p:F

    const/4 v6, 0x0

    .line 113
    iput v6, v0, Lcom/pspdfkit/internal/q7;->q:I

    .line 114
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/pspdfkit/internal/q7;->r:J

    :cond_20
    const/4 v6, 0x0

    const/4 v9, 0x0

    const/16 v16, 0x0

    :goto_16
    if-ge v6, v3, :cond_22

    if-ne v10, v6, :cond_21

    goto :goto_17

    .line 115
    :cond_21
    iget v7, v0, Lcom/pspdfkit/internal/q7;->p:F

    div-float/2addr v7, v2

    .line 116
    invoke-virtual {v1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    sub-float/2addr v8, v11

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    add-float/2addr v8, v7

    add-float v8, v8, v16

    .line 117
    invoke-virtual {v1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v13

    sub-float/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    add-float/2addr v13, v7

    add-float/2addr v13, v9

    move/from16 v16, v8

    move v9, v13

    :goto_17
    add-int/lit8 v6, v6, 0x1

    goto :goto_16

    :cond_22
    int-to-float v1, v4

    div-float v16, v16, v1

    div-float/2addr v9, v1

    mul-float v1, v16, v2

    mul-float v9, v9, v2

    .line 128
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v2

    if-eqz v2, :cond_23

    goto :goto_18

    :cond_23
    float-to-double v1, v1

    float-to-double v3, v9

    .line 131
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v1

    double-to-float v9, v1

    .line 137
    :goto_18
    iget-boolean v1, v0, Lcom/pspdfkit/internal/q7;->k:Z

    .line 138
    iput v11, v0, Lcom/pspdfkit/internal/q7;->d:F

    .line 139
    iput v12, v0, Lcom/pspdfkit/internal/q7;->e:F

    .line 140
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v2

    if-nez v2, :cond_25

    iget-boolean v2, v0, Lcom/pspdfkit/internal/q7;->k:Z

    if-eqz v2, :cond_25

    iget v2, v0, Lcom/pspdfkit/internal/q7;->m:I

    int-to-float v2, v2

    cmpg-float v2, v9, v2

    if-ltz v2, :cond_24

    if-eqz v5, :cond_25

    .line 141
    :cond_24
    iget-object v2, v0, Lcom/pspdfkit/internal/q7;->b:Lcom/pspdfkit/internal/q7$a;

    invoke-interface {v2, v0}, Lcom/pspdfkit/internal/q7$a;->a(Lcom/pspdfkit/internal/q7;)V

    const/4 v2, 0x0

    .line 142
    iput-boolean v2, v0, Lcom/pspdfkit/internal/q7;->k:Z

    .line 143
    iput v9, v0, Lcom/pspdfkit/internal/q7;->j:F

    :cond_25
    if-eqz v5, :cond_26

    .line 148
    iput v9, v0, Lcom/pspdfkit/internal/q7;->h:F

    iput v9, v0, Lcom/pspdfkit/internal/q7;->i:F

    iput v9, v0, Lcom/pspdfkit/internal/q7;->j:F

    .line 151
    :cond_26
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v2

    if-eqz v2, :cond_27

    iget v2, v0, Lcom/pspdfkit/internal/q7;->l:I

    goto :goto_19

    :cond_27
    iget v2, v0, Lcom/pspdfkit/internal/q7;->m:I

    .line 152
    :goto_19
    iget-boolean v3, v0, Lcom/pspdfkit/internal/q7;->k:Z

    if-nez v3, :cond_29

    int-to-float v2, v2

    cmpl-float v2, v9, v2

    if-ltz v2, :cond_29

    if-nez v1, :cond_28

    iget v1, v0, Lcom/pspdfkit/internal/q7;->j:F

    sub-float v1, v9, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, v0, Lcom/pspdfkit/internal/q7;->l:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_29

    .line 155
    :cond_28
    iput v9, v0, Lcom/pspdfkit/internal/q7;->h:F

    iput v9, v0, Lcom/pspdfkit/internal/q7;->i:F

    .line 157
    iget-object v1, v0, Lcom/pspdfkit/internal/q7;->b:Lcom/pspdfkit/internal/q7$a;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/q7$a;->c(Lcom/pspdfkit/internal/q7;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/pspdfkit/internal/q7;->k:Z

    :cond_29
    move/from16 v1, v21

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2b

    .line 164
    iput v9, v0, Lcom/pspdfkit/internal/q7;->h:F

    .line 168
    iget-boolean v1, v0, Lcom/pspdfkit/internal/q7;->k:Z

    if-eqz v1, :cond_2a

    .line 169
    iget-object v1, v0, Lcom/pspdfkit/internal/q7;->b:Lcom/pspdfkit/internal/q7$a;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/q7$a;->b(Lcom/pspdfkit/internal/q7;)Z

    move-result v6

    goto :goto_1a

    :cond_2a
    const/4 v6, 0x1

    :goto_1a
    if-eqz v6, :cond_2b

    .line 175
    iget v1, v0, Lcom/pspdfkit/internal/q7;->h:F

    iput v1, v0, Lcom/pspdfkit/internal/q7;->i:F

    :cond_2b
    return-void
.end method

.method public final b()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/q7;->e:F

    return v0
.end method

.method public final c()F
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/q7;->d()Z

    move-result v0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz v0, :cond_5

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/q7;->x:Z

    if-eqz v0, :cond_0

    iget v3, p0, Lcom/pspdfkit/internal/q7;->h:F

    iget v4, p0, Lcom/pspdfkit/internal/q7;->i:F

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    :cond_0
    if-nez v0, :cond_2

    iget v0, p0, Lcom/pspdfkit/internal/q7;->h:F

    iget v3, p0, Lcom/pspdfkit/internal/q7;->i:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 7
    :goto_0
    iget v3, p0, Lcom/pspdfkit/internal/q7;->h:F

    iget v4, p0, Lcom/pspdfkit/internal/q7;->i:F

    div-float/2addr v3, v4

    sub-float v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v3, v3, v4

    .line 8
    iget v4, p0, Lcom/pspdfkit/internal/q7;->i:F

    cmpg-float v1, v4, v1

    if-gtz v1, :cond_3

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    add-float/2addr v2, v3

    goto :goto_1

    :cond_4
    sub-float/2addr v2, v3

    :goto_1
    return v2

    .line 10
    :cond_5
    iget v0, p0, Lcom/pspdfkit/internal/q7;->i:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_6

    iget v1, p0, Lcom/pspdfkit/internal/q7;->h:F

    div-float v2, v1, v0

    :cond_6
    return v2
.end method

.method public final e()V
    .locals 4

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/q7;->f:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/q7;->w:Landroid/view/GestureDetector;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/p7;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/p7;-><init>(Lcom/pspdfkit/internal/q7;)V

    .line 14
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/pspdfkit/internal/q7;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/pspdfkit/internal/q7;->c:Landroid/os/Handler;

    invoke-direct {v1, v2, v0, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/q7;->w:Landroid/view/GestureDetector;

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/q7;->g:Z

    return-void
.end method

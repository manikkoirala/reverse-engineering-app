.class public final Lcom/pspdfkit/internal/bo;
.super Landroid/print/PrintDocumentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/bo$a;,
        Lcom/pspdfkit/internal/bo$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/co;

.field private final b:Lcom/pspdfkit/internal/bo$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/printing/PrintOptions;Lcom/pspdfkit/document/processor/PdfProcessorTask;Lcom/pspdfkit/internal/bo$b;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 2
    iput-object p5, p0, Lcom/pspdfkit/internal/bo;->b:Lcom/pspdfkit/internal/bo$b;

    .line 3
    new-instance p5, Lcom/pspdfkit/internal/co;

    invoke-direct {p5, p1, p2, p3, p4}, Lcom/pspdfkit/internal/co;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/printing/PrintOptions;Lcom/pspdfkit/document/processor/PdfProcessorTask;)V

    iput-object p5, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bo;->b:Lcom/pspdfkit/internal/bo$b;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/internal/bo$b;->a()V

    :cond_0
    return-void
.end method

.method public final onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    new-instance v4, Lcom/pspdfkit/internal/bo$a;

    invoke-direct {v4, p4}, Lcom/pspdfkit/internal/bo$a;-><init>(Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/co;->a(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Lcom/pspdfkit/internal/co$b;Landroid/os/Bundle;)V

    return-void
.end method

.method public final onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/co;->a()Landroid/print/PrintAttributes;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 3
    invoke-virtual {p4, p1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V

    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/k7;

    iget-object v1, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    .line 9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/co;->b()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    .line 10
    invoke-virtual {v2}, Lcom/pspdfkit/internal/co;->c()Lcom/pspdfkit/utils/Size;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    .line 11
    invoke-virtual {v3}, Lcom/pspdfkit/internal/co;->a()Landroid/print/PrintAttributes;

    move-result-object v3

    iget-object v4, p0, Lcom/pspdfkit/internal/bo;->a:Lcom/pspdfkit/internal/co;

    .line 12
    invoke-virtual {v4}, Lcom/pspdfkit/internal/co;->d()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/internal/k7;-><init>(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/utils/Size;Landroid/print/PrintAttributes;Z)V

    .line 13
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/k7;->a([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    return-void
.end method

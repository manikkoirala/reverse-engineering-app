.class public final Lcom/pspdfkit/internal/j3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/j3$a;,
        Lcom/pspdfkit/internal/j3$b;,
        Lcom/pspdfkit/internal/j3$c;
    }
.end annotation


# instance fields
.field private final a:Landroid/media/MediaPlayer;

.field private final b:Landroid/media/AudioManager;

.field private c:Lcom/pspdfkit/internal/j3$b;

.field private d:Lcom/pspdfkit/internal/j3$a;

.field private final e:Landroid/media/AudioAttributes;

.field private f:Landroid/media/AudioFocusRequest;


# direct methods
.method public static synthetic $r8$lambda$j9urWbd1487ydYEkwEeU5JxjOG0(Lcom/pspdfkit/internal/j3;Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/j3;->a(Lcom/pspdfkit/internal/j3;Landroid/media/MediaPlayer;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    const-string v1, "audio"

    .line 59
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.media.AudioManager"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/pspdfkit/internal/j3;->b:Landroid/media/AudioManager;

    .line 61
    sget-object v1, Lcom/pspdfkit/internal/j3$b;->c:Lcom/pspdfkit/internal/j3$b;

    iput-object v1, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    .line 69
    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 72
    new-instance p1, Landroid/media/AudioAttributes$Builder;

    invoke-direct {p1}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 p2, 0x3

    .line 73
    invoke-virtual {p1, p2}, Landroid/media/AudioAttributes$Builder;->setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 74
    invoke-virtual {p1, p2}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object p1

    .line 76
    iput-object p1, p0, Lcom/pspdfkit/internal/j3;->e:Landroid/media/AudioAttributes;

    .line 80
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioAttributes(Landroid/media/AudioAttributes;)V

    .line 83
    new-instance p1, Lcom/pspdfkit/internal/j3$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/j3$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/j3;)V

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 89
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/j3;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method private final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->f:Landroid/media/AudioFocusRequest;

    if-eqz v0, :cond_2

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->abandonAudioFocusRequest(Landroid/media/AudioFocusRequest;)I

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->f:Landroid/media/AudioFocusRequest;

    goto :goto_1

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static final a(Lcom/pspdfkit/internal/j3;Landroid/media/MediaPlayer;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/pspdfkit/internal/j3;->a()V

    .line 12
    sget-object p1, Lcom/pspdfkit/internal/j3$b;->c:Lcom/pspdfkit/internal/j3$b;

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 14
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    .line 15
    iget-object p0, p0, Lcom/pspdfkit/internal/j3;->d:Lcom/pspdfkit/internal/j3$a;

    if-eqz p0, :cond_1

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/j3$a;->a(Lcom/pspdfkit/internal/j3$b;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/b3;)V
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/j3;->d:Lcom/pspdfkit/internal/j3$a;

    return-void
.end method

.method public final b()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/j3;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 4
    sget-object v0, Lcom/pspdfkit/internal/j3$b;->b:Lcom/pspdfkit/internal/j3$b;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    if-ne v1, v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->d:Lcom/pspdfkit/internal/j3$a;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/j3$a;->a(Lcom/pspdfkit/internal/j3$b;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final f()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/j3;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 4
    sget-object v0, Lcom/pspdfkit/internal/j3$b;->d:Lcom/pspdfkit/internal/j3$b;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    if-ne v1, v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->d:Lcom/pspdfkit/internal/j3$a;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/j3$a;->a(Lcom/pspdfkit/internal/j3$b;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final g()V
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->e:Landroid/media/AudioAttributes;

    if-eqz v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->f:Landroid/media/AudioFocusRequest;

    if-eqz v0, :cond_1

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/j3;->a()V

    .line 7
    :cond_1
    new-instance v0, Landroid/media/AudioFocusRequest$Builder;

    invoke-direct {v0, v3}, Landroid/media/AudioFocusRequest$Builder;-><init>(I)V

    .line 8
    invoke-virtual {v0, p0}, Landroid/media/AudioFocusRequest$Builder;->setOnAudioFocusChangeListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Landroid/media/AudioFocusRequest$Builder;

    move-result-object v0

    .line 9
    invoke-virtual {v0, v3}, Landroid/media/AudioFocusRequest$Builder;->setWillPauseWhenDucked(Z)Landroid/media/AudioFocusRequest$Builder;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->e:Landroid/media/AudioAttributes;

    invoke-virtual {v0, v1}, Landroid/media/AudioFocusRequest$Builder;->setAudioAttributes(Landroid/media/AudioAttributes;)Landroid/media/AudioFocusRequest$Builder;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroid/media/AudioFocusRequest$Builder;->build()Landroid/media/AudioFocusRequest;

    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->f:Landroid/media/AudioFocusRequest;

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioFocusRequest;)I

    move-result v0

    goto :goto_1

    .line 17
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->b:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, p0, v1, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-ne v0, v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    monitor-exit p0

    if-eqz v2, :cond_5

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/j3;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 19
    sget-object v0, Lcom/pspdfkit/internal/j3$b;->a:Lcom/pspdfkit/internal/j3$b;

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    if-ne v1, v0, :cond_4

    goto :goto_2

    .line 21
    :cond_4
    iput-object v0, p0, Lcom/pspdfkit/internal/j3;->c:Lcom/pspdfkit/internal/j3$b;

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/j3;->d:Lcom/pspdfkit/internal/j3$a;

    if-eqz v1, :cond_5

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/j3$a;->a(Lcom/pspdfkit/internal/j3$b;)V

    :cond_5
    :goto_2
    return-void

    :catchall_0
    move-exception v0

    .line 23
    monitor-exit p0

    throw v0
.end method

.method public final onAudioFocusChange(I)V
    .locals 1

    const/4 v0, -0x3

    if-eq p1, v0, :cond_0

    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/j3;->e()V

    :goto_0
    return-void
.end method

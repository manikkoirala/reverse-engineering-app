.class public final Lcom/pspdfkit/internal/rh;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static synthetic $r8$lambda$d5xrTla8-4973fj-aZUn0_fHbXo(Landroid/content/Context;IILjava/lang/String;Landroid/view/View;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;IILjava/lang/String;Landroid/view/View;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-static {p0, p1, v0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Landroid/content/Context;IILjava/lang/String;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 14
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v0, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x1

    aput-object p0, v0, p1

    const/4 p0, 0x2

    aput-object p3, v0, p0

    const/4 p0, 0x3

    aput-object p4, v0, p0

    .line 15
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    const-string p1, "Localize qty [%s][%d] to [%s] / [%s]."

    invoke-static {p0, p1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;
    .locals 2

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->l()Lcom/pspdfkit/listeners/LocalizationListener;

    move-result-object v0

    invoke-static {p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    invoke-interface {v0, p0, p1, v1, p2}, Lcom/pspdfkit/listeners/LocalizationListener;->getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static varargs a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 6

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->l()Lcom/pspdfkit/listeners/LocalizationListener;

    move-result-object v0

    .line 8
    invoke-static {p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v3

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/pspdfkit/listeners/LocalizationListener;->getLocalizedString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static varargs a(Landroid/content/Context;ILandroid/widget/TextView;I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 10

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->l()Lcom/pspdfkit/listeners/LocalizationListener;

    move-result-object v0

    .line 10
    invoke-static {p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v3

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/pspdfkit/listeners/LocalizationListener;->getLocalizedQuantityString(Landroid/content/Context;ILjava/util/Locale;Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/rh$$ExternalSyntheticLambda0;

    move-object v4, v0

    move-object v5, p0

    move v6, p1

    move v7, p3

    move-object v8, p4

    move-object v9, p2

    invoke-direct/range {v4 .. v9}, Lcom/pspdfkit/internal/rh$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;IILjava/lang/String;Landroid/view/View;)V

    const-string p0, "PSPDFKit.Localization"

    invoke-static {p0, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    return-object p4
.end method

.method public static a(Landroid/content/Context;)Ljava/util/Locale;
    .locals 3

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x18

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object p0

    return-object p0

    .line 4
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    iget-object p0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    .line 2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 3
    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 2
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    .line 3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 4
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x1

    aput-object p0, v2, v0

    const-string p0, "%s, %s"

    invoke-static {p0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    .line 2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 3
    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

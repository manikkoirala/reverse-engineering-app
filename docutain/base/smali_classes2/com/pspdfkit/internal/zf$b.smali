.class final Lcom/pspdfkit/internal/zf$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zf;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zf$b;->a:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$b;->a:Lcom/pspdfkit/internal/zf;

    iget v1, v0, Lcom/pspdfkit/internal/zf;->q:I

    .line 2
    iget-object v0, v0, Lcom/pspdfkit/internal/zf;->r:Lcom/pspdfkit/internal/jni/NativeDocument;

    .line 4
    new-array v3, v1, [Lcom/pspdfkit/utils/Size;

    .line 5
    new-array v4, v1, [B

    .line 6
    new-array v5, v1, [B

    .line 7
    new-array v6, v1, [Ljava/lang/String;

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 9
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageInfo(I)Lcom/pspdfkit/internal/jni/NativePageInfo;

    move-result-object v2

    .line 10
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getSize()Lcom/pspdfkit/utils/Size;

    move-result-object v7

    aput-object v7, v3, v1

    .line 11
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotation()B

    move-result v7

    aput-byte v7, v4, v1

    .line 12
    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativePageInfo;->getRotationOffset()B

    move-result v2

    aput-byte v2, v5, v1

    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zf$b;->a:Lcom/pspdfkit/internal/zf;

    new-instance v1, Lcom/pspdfkit/internal/zf$c;

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/zf$c;-><init>([Lcom/pspdfkit/utils/Size;[B[B[Ljava/lang/String;Lcom/pspdfkit/internal/zf$c-IA;)V

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/zf;->-$$Nest$fputs(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/zf$e;)V

    return-void
.end method

.class public abstract Lcom/pspdfkit/internal/ma;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/internal/ja;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private b:Z

.field protected c:Ljava/util/ArrayList;

.field private final d:Lcom/pspdfkit/internal/fl;


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/fl;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ma;->b:Z

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/ma;->d:Lcom/pspdfkit/internal/fl;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ma;->b:Z

    return-void
.end method

.method protected final a(Lcom/pspdfkit/internal/n1;)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ma;->b:Z

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ma;->b:Z

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ma;->d:Lcom/pspdfkit/internal/fl;

    iget-object v2, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ja;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/v5;

    iget-object v2, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Lcom/pspdfkit/internal/v5;-><init>(Ljava/util/List;)V

    :goto_0
    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ma;->c:Ljava/util/ArrayList;

    return-void
.end method

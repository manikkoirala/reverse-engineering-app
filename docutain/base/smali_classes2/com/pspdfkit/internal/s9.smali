.class final Lcom/pspdfkit/internal/s9;
.super Lcom/pspdfkit/internal/fs;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/w9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/w9;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/s9;->b:Lcom/pspdfkit/internal/w9;

    invoke-direct {p0}, Lcom/pspdfkit/internal/fs;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/s9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p2

    .line 2
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string p4, ""

    const-string v0, "[:\\\\/*\"?|<>\']"

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p3, :cond_1

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 5
    invoke-virtual {p1, v0, p4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 6
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/s9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetp(Lcom/pspdfkit/internal/w9;)I

    move-result p1

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/s9;->b:Lcom/pspdfkit/internal/w9;

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetq(Lcom/pspdfkit/internal/w9;)I

    move-result p1

    :goto_2
    invoke-static {p2, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/widget/EditText;I)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/s9;->b:Lcom/pspdfkit/internal/w9;

    .line 9
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeto(Lcom/pspdfkit/internal/w9;)Landroid/widget/TextView;

    move-result-object p2

    .line 10
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetj(Lcom/pspdfkit/internal/w9;)Landroid/widget/ArrayAdapter;

    move-result-object p3

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeti(Lcom/pspdfkit/internal/w9;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {p3, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/internal/w9$c;

    iget p3, p3, Lcom/pspdfkit/internal/w9$c;->a:I

    const/4 v3, 0x2

    if-ne p3, v3, :cond_3

    const/4 p3, 0x1

    goto :goto_3

    :cond_3
    const/4 p3, 0x0

    :goto_3
    if-eqz p3, :cond_4

    .line 11
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgetl(Lcom/pspdfkit/internal/w9;)Lcom/pspdfkit/internal/w9$c;

    move-result-object p3

    .line 12
    iget-object p3, p3, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 13
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p3

    xor-int/2addr p3, v2

    if-eqz p3, :cond_7

    .line 14
    :cond_4
    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p3

    invoke-virtual {p3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_6

    invoke-static {p1}, Lcom/pspdfkit/internal/w9;->-$$Nest$fgeth(Lcom/pspdfkit/internal/w9;)Landroid/widget/EditText;

    move-result-object p1

    .line 15
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 16
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_5

    .line 17
    invoke-virtual {p1, v0, p4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 18
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x1

    goto :goto_4

    :cond_5
    const/4 p1, 0x0

    :goto_4
    if-eqz p1, :cond_6

    const/4 p1, 0x1

    goto :goto_5

    :cond_6
    const/4 p1, 0x0

    :goto_5
    if-eqz p1, :cond_7

    const/4 v1, 0x1

    .line 19
    :cond_7
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

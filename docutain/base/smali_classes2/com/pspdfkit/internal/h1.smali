.class public final Lcom/pspdfkit/internal/h1;
.super Lcom/pspdfkit/internal/ql;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/s1$a;
.implements Lcom/pspdfkit/internal/c1$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/ql<",
        "Lcom/pspdfkit/annotations/Annotation;",
        ">;",
        "Lcom/pspdfkit/internal/s1$a;",
        "Lcom/pspdfkit/internal/c1$a;"
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/internal/ql$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/ql$b<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/s1;

.field private final f:Lcom/pspdfkit/internal/c1;

.field private final g:Lcom/pspdfkit/internal/m2;

.field private h:Lcom/pspdfkit/internal/zf;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/ProgressBar;

.field private k:Z

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/Button;

.field private final n:Landroid/widget/ImageButton;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Z

.field private r:Z

.field private s:Z


# direct methods
.method public static synthetic $r8$lambda$H1yhxzI5Ah9nQQrsLjMA2xyRZ7g(Lcom/pspdfkit/internal/h1;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/h1;->a(Lcom/pspdfkit/internal/h1;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Sd8L7ppKWTfg2Q1i0Zka1AuhEfU(Landroid/content/Context;Lcom/pspdfkit/internal/h1;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/h1;->a(Landroid/content/Context;Lcom/pspdfkit/internal/h1;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$TEtI8OZzW6LxWC6pAsC9OLRPXoo(Lcom/pspdfkit/internal/h1;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/h1;->a(Lcom/pspdfkit/internal/h1;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/ql$b;Lcom/pspdfkit/internal/fl;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/internal/ql$b<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Lcom/pspdfkit/internal/fl;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onItemTappedListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ql;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/h1;->d:Lcom/pspdfkit/internal/ql$b;

    .line 8
    new-instance p2, Lcom/pspdfkit/internal/s1;

    invoke-direct {p2, p1, p0}, Lcom/pspdfkit/internal/s1;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/s1$a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    const/4 v0, 0x1

    .line 50
    iput-boolean v0, p0, Lcom/pspdfkit/internal/h1;->r:Z

    .line 55
    new-instance v1, Lcom/pspdfkit/internal/c1;

    .line 56
    sget-object v2, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->DEFAULT_LISTED_ANNOTATION_TYPES:Ljava/util/EnumSet;

    const-string v3, "DEFAULT_LISTED_ANNOTATION_TYPES"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {v1, v2, p2, p0, p3}, Lcom/pspdfkit/internal/c1;-><init>(Ljava/util/EnumSet;Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/c1$a;Lcom/pspdfkit/internal/fl;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__outline_annotation_view:I

    const/4 v2, 0x0

    invoke-virtual {p3, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 64
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_list_empty_text:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "annotationListLayout.fin\u2026notation_list_empty_text)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/internal/h1;->i:Landroid/widget/TextView;

    .line 65
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_list_progress_bar:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "annotationListLayout.fin\u2026tation_list_progress_bar)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/pspdfkit/internal/h1;->j:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    .line 67
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 68
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 69
    sget v1, Lcom/pspdfkit/R$id;->pspdf__annotation_list_view:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v1, "annotationListLayout.fin\u2026df__annotation_list_view)"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView;

    .line 70
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v1, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 71
    new-instance v1, Landroidx/recyclerview/widget/DividerItemDecoration;

    invoke-direct {v1, p1, v0}, Landroidx/recyclerview/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p3, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 72
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 73
    new-instance v0, Lcom/pspdfkit/internal/m2;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/m2;-><init>(Lcom/pspdfkit/internal/s1;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/h1;->g:Lcom/pspdfkit/internal/m2;

    .line 74
    new-instance p2, Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-direct {p2, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 75
    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 76
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_list_toolbar:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf__annotation_list_toolbar)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    .line 77
    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_list_clear_all:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.pspdf_\u2026nnotation_list_clear_all)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/pspdfkit/internal/h1;->m:Landroid/widget/Button;

    .line 78
    new-instance p3, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda1;

    invoke-direct {p3, p1, p0}, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/h1;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    sget p1, Lcom/pspdfkit/R$id;->pspdf__annotation_list_edit:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.pspdf__annotation_list_edit)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    .line 87
    new-instance p2, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda2;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/h1;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final a(Landroid/content/Context;Lcom/pspdfkit/internal/h1;Landroid/view/View;)V
    .locals 3

    const-string p2, "$context"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance p2, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {p2, p0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2
    sget v0, Lcom/pspdfkit/R$string;->pspdf__clear_annotations_confirm:I

    const/4 v1, 0x0

    .line 3
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 4
    invoke-virtual {p2, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    .line 5
    sget v0, Lcom/pspdfkit/R$string;->pspdf__clear_annotations:I

    .line 6
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 7
    new-instance v2, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda0;

    invoke-direct {v2, p1}, Lcom/pspdfkit/internal/h1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/h1;)V

    invoke-virtual {p2, v0, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 8
    sget p2, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    .line 9
    invoke-static {p0, p2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p0

    .line 10
    invoke-virtual {p1, p0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    .line 11
    invoke-virtual {p0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p0

    .line 12
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/h1;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object p0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/c1;->d()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/h1;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-boolean p1, p0, Lcom/pspdfkit/internal/h1;->q:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 15
    iput-boolean p1, p0, Lcom/pspdfkit/internal/h1;->q:Z

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->g:Lcom/pspdfkit/internal/m2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m2;->a(Z)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s1;->a(Z)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    iget-object p0, p0, Lcom/pspdfkit/internal/h1;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 19
    iput-boolean p1, p0, Lcom/pspdfkit/internal/h1;->q:Z

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->g:Lcom/pspdfkit/internal/m2;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/m2;->a(Z)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s1;->a(Z)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    iget-object p0, p0, Lcom/pspdfkit/internal/h1;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c1;->e()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/mh;)V
    .locals 1

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/c1;->b(Lcom/pspdfkit/internal/mh;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/mh;Lcom/pspdfkit/internal/mh;I)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationAnnotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p1, p2, p3}, Lcom/pspdfkit/internal/c1;->a(Lcom/pspdfkit/internal/mh;Lcom/pspdfkit/internal/mh;I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 3

    const-string v0, "themeConfiguration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget v0, p1, Lcom/pspdfkit/internal/rl;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->i:Landroid/widget/TextView;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-static {v1}, Lcom/pspdfkit/internal/s5;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/s1;->a(Lcom/pspdfkit/internal/rl;)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->m:Landroid/widget/Button;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->t:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->q:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/h1;->o:Landroid/graphics/drawable/Drawable;

    .line 44
    iget-object v1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/pspdfkit/internal/rl;->u:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->q:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/h1;->p:Landroid/graphics/drawable/Drawable;

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    iget p1, p1, Lcom/pspdfkit/internal/rl;->p:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 2

    .line 23
    iput-object p1, p0, Lcom/pspdfkit/internal/h1;->h:Lcom/pspdfkit/internal/zf;

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/s1;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/c1;->a(Lcom/pspdfkit/document/PdfDocument;)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/c1;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 29
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 30
    :goto_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/h1;->s:Z

    .line 31
    iget-boolean p2, p0, Lcom/pspdfkit/internal/h1;->r:Z

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 34
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 36
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 37
    :goto_1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/h1;->k:Z

    if-eqz p1, :cond_2

    .line 38
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    :cond_2
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/mh;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->j:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->i:Landroid/widget/TextView;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->j:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->i:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->m:Landroid/widget/Button;

    .line 57
    iget-object p2, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/s1;->b()I

    move-result p2

    if-lez p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    .line 58
    :goto_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    .line 60
    iget-object p2, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/s1;->b()I

    move-result p2

    if-lez p2, :cond_3

    const/4 p2, 0x1

    goto :goto_2

    :cond_3
    const/4 p2, 0x0

    .line 61
    :goto_2
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 62
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1;->b()I

    move-result p1

    if-lez p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_5

    .line 63
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->m:Landroid/widget/Button;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setAlpha(F)V

    .line 64
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/16 p2, 0xff

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_4

    .line 65
    :cond_5
    iput-boolean v2, p0, Lcom/pspdfkit/internal/h1;->q:Z

    .line 66
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->g:Lcom/pspdfkit/internal/m2;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/m2;->a(Z)V

    .line 67
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/s1;->a(Z)V

    .line 68
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    iget-object p2, p0, Lcom/pspdfkit/internal/h1;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->m:Landroid/widget/Button;

    const/high16 p2, 0x3f000000    # 0.5f

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setAlpha(F)V

    .line 70
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->n:Landroid/widget/ImageButton;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/16 p2, 0x80

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_4
    return-void
.end method

.method public final b()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/mh;)V
    .locals 3

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/h1;->h:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_0

    .line 5
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    sget-object v2, Lcom/pspdfkit/document/DocumentPermissions;->EXTRACT:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/zf;->hasPermission(Lcom/pspdfkit/document/DocumentPermissions;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/h1;->q:Z

    if-nez v0, :cond_0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__text_copied_to_clipboard:I

    const/16 v2, 0x30

    invoke-static {p1, p1, v0, v1, v2}, Lcom/pspdfkit/internal/j5;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;II)Z

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->h:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/internal/h1;->k:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/internal/h1;->k:Z

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/h1;->e:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/s1;->b()I

    move-result v1

    if-gtz v1, :cond_1

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/h1;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c1;->c()V

    return-void
.end method

.method public final c(Lcom/pspdfkit/internal/mh;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ql;->b:Lcom/pspdfkit/internal/ql$a;

    invoke-interface {v0}, Lcom/pspdfkit/internal/ql$a;->hide()V

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "tap_annotation_in_outline_list"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->d:Lcom/pspdfkit/internal/ql$b;

    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/internal/ql$b;->a(Lcom/pspdfkit/internal/ql;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getTabButtonId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_annotations:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotations:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(context, R.string.pspdf__annotations)"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c1;->e()V

    return-void
.end method

.method public final setAnnotationEditingEnabled(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/h1;->r:Z

    if-eqz p1, :cond_0

    .line 2
    iget-boolean p1, p0, Lcom/pspdfkit/internal/h1;->s:Z

    if-eqz p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/h1;->l:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final setAnnotationListReorderingEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/c1;->a(Z)V

    return-void
.end method

.method public final setListedAnnotationTypes(Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listedAnnotationTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/h1;->f:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/c1;->a(Ljava/util/EnumSet;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

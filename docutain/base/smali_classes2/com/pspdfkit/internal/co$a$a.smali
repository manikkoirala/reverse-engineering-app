.class final Lcom/pspdfkit/internal/co$a$a;
.super Lcom/pspdfkit/internal/yr;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/co$a;->onComplete()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/co$a;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/co$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/co$a$a;->a:Lcom/pspdfkit/internal/co$a;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yr;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/co$a$a;->a:Lcom/pspdfkit/internal/co$a;

    iget-object v1, v0, Lcom/pspdfkit/internal/co$a;->d:Lcom/pspdfkit/internal/co;

    iget-object v2, v0, Lcom/pspdfkit/internal/co$a;->a:Lcom/pspdfkit/internal/co$b;

    iget-boolean v0, v0, Lcom/pspdfkit/internal/co$a;->c:Z

    .line 2
    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;

    move-result-object v3

    .line 3
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgeta(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/document/printing/PrintOptions;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5
    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgeta(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/document/printing/PrintOptions;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 6
    :cond_0
    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetc(Lcom/pspdfkit/internal/co;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/internal/co;->-$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ".pdf"

    goto :goto_1

    :cond_1
    const-string v1, ""

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 10
    check-cast v2, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {v2, v1, v3, v0}, Lcom/pspdfkit/internal/bo$a;->a(Ljava/lang/String;IZ)V

    goto :goto_2

    .line 12
    :cond_2
    check-cast v2, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/bo$a;->b()V

    :goto_2
    return-void
.end method

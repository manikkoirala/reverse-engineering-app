.class final Lcom/pspdfkit/internal/sm$a;
.super Lcom/pspdfkit/ui/search/PdfSearchViewLazy;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/sm;-><init>(Landroid/view/View;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/pspdfkit/internal/sm$a;->b:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final createSearchView()Lcom/pspdfkit/ui/search/PdfSearchView;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm$a;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/search/PdfSearchViewInline;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__search_view_inline:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

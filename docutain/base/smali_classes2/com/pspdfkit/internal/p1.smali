.class public final Lcom/pspdfkit/internal/p1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/p1$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/collection/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/SparseArrayCompat<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/kl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/kl<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Lcom/pspdfkit/internal/p1$a;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroidx/collection/SparseArrayCompat;

    invoke-direct {v0}, Landroidx/collection/SparseArrayCompat;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/kl;

    invoke-direct {v0}, Lcom/pspdfkit/internal/kl;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p1;->c:Z

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1$a;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/p1;-><init>()V

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/p1;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iget-object p1, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {p1}, Landroidx/collection/SparseArrayCompat;->clone()Landroidx/collection/SparseArrayCompat;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    .line 9
    new-instance p1, Lcom/pspdfkit/internal/kl;

    invoke-direct {p1}, Lcom/pspdfkit/internal/kl;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/pspdfkit/internal/p1;->c:Z

    return-void
.end method

.method private declared-synchronized b(ILjava/lang/Object;)V
    .locals 3

    monitor-enter p0

    .line 16
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    if-nez p2, :cond_1

    .line 20
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1, p1}, Landroidx/collection/SparseArrayCompat;->remove(I)V

    goto :goto_0

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1, p1, p2}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 25
    :goto_0
    iget-boolean v1, p0, Lcom/pspdfkit/internal/p1;->e:Z

    if-nez v1, :cond_2

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/kl;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    .line 27
    iput-boolean v1, p0, Lcom/pspdfkit/internal/p1;->c:Z

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    if-eqz v1, :cond_2

    if-eq v0, p2, :cond_2

    .line 30
    invoke-interface {v1, p1, v0, p2}, Lcom/pspdfkit/internal/p1$a;->a(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final a(IF)Ljava/lang/Float;
    .locals 1

    .line 35
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    const-class v0, Ljava/lang/Float;

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    return-object p1
.end method

.method public final a(II)Ljava/lang/Integer;
    .locals 1

    .line 34
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    return-object p1
.end method

.method public final declared-synchronized a(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    monitor-enter p0

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    .line 22
    :cond_0
    :try_start_1
    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    .line 26
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Property with key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not a "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    monitor-enter p0

    .line 27
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object p3

    .line 29
    :cond_0
    :try_start_1
    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 32
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    .line 33
    :cond_1
    :try_start_2
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Property with key "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not a "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p1;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(ILandroid/graphics/RectF;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(ILjava/lang/Boolean;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(ILjava/lang/Integer;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(ILjava/util/Date;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 17
    :cond_0
    new-instance v0, Lcom/pspdfkit/datastructures/ImmutableDate;

    invoke-direct {v0, p2}, Lcom/pspdfkit/datastructures/ImmutableDate;-><init>(Ljava/util/Date;)V

    move-object p2, v0

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/p1;->b(ILjava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    .line 111
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p1;->e:Z

    .line 112
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getProperties(Lcom/pspdfkit/internal/jni/NativeAnnotation;)[B

    move-result-object p2

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    .line 119
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/m1;->a(Ljava/nio/ByteBuffer;)Lcom/pspdfkit/internal/m1;

    move-result-object p1

    .line 120
    invoke-static {p1}, Lcom/pspdfkit/internal/y0;->a(Lcom/pspdfkit/internal/m1;)Lcom/pspdfkit/internal/y0;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/y0;->a(Lcom/pspdfkit/internal/p1;)V

    .line 121
    iput-boolean v1, p0, Lcom/pspdfkit/internal/p1;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 122
    :cond_0
    :try_start_1
    new-instance p2, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    .line 124
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v2, v0

    const-string p1, "Couldn\'t fetch properties for annotation %s: %s"

    .line 125
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/internal/p1;)V
    .locals 7

    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/internal/p1;->e:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    iget-object v0, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 5
    iget-object v3, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v2}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v3

    .line 6
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 7
    iget-object v5, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v5, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 9
    iget-object v6, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    invoke-interface {v6, v3, v4, v5}, Lcom/pspdfkit/internal/p1$a;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 10
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->clear()V

    .line 11
    iget-object v0, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v0

    :goto_2
    if-ge v1, v0, :cond_3

    .line 12
    iget-object v2, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v2, v1}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v2

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    iget-object v4, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v2}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(I)Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Lcom/pspdfkit/internal/qf;Lcom/pspdfkit/internal/jni/NativeAnnotation;)Z
    .locals 3

    .line 38
    monitor-enter p1

    .line 39
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 40
    :try_start_1
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .line 41
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return p2

    .line 44
    :cond_0
    :try_start_3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/hb;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    new-instance v0, Lcom/pspdfkit/internal/mb;

    invoke-direct {v0}, Lcom/pspdfkit/internal/mb;-><init>()V

    .line 51
    invoke-static {p0}, Lcom/pspdfkit/internal/z0;->a(Lcom/pspdfkit/internal/p1;)Lcom/pspdfkit/internal/z0;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/z0;->a(Lcom/pspdfkit/internal/mb;)I

    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/mb;->b(I)V

    .line 55
    new-instance v1, Lcom/pspdfkit/internal/mb;

    invoke-direct {v1}, Lcom/pspdfkit/internal/mb;-><init>()V

    .line 57
    invoke-static {p0}, Lcom/pspdfkit/internal/z0;->a(Lcom/pspdfkit/internal/p1;)Lcom/pspdfkit/internal/z0;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/z0;->b(Lcom/pspdfkit/internal/mb;)I

    move-result v2

    .line 58
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/mb;->b(I)V

    .line 62
    invoke-virtual {v1}, Lcom/pspdfkit/internal/mb;->e()[B

    move-result-object v1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/mb;->e()[B

    move-result-object v0

    .line 63
    move-object v2, p1

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2, p2, v1, v0}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/internal/jni/NativeAnnotation;[B[B)Landroid/graphics/RectF;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p2}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 73
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    const/16 v0, 0x8

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, v0, v1}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 75
    iget-object p2, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-virtual {p2}, Ljava/util/AbstractCollection;->clear()V

    const/4 p2, 0x1

    .line 77
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return p2

    .line 78
    :cond_2
    :try_start_5
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v0, "Your license does not allow annotation editing."

    invoke-direct {p2, v0}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p2

    :catchall_0
    move-exception p2

    .line 109
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p2

    :catchall_1
    move-exception p2

    .line 110
    monitor-exit p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw p2
.end method

.method public final a(Ljava/lang/Object;Ljava/util/HashSet;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 126
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/p1;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 127
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/p1;

    .line 128
    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v1

    iget-object v3, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v3

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    const/4 v1, 0x0

    .line 129
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 130
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v1}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v3

    if-eqz p2, :cond_3

    .line 131
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_1

    .line 132
    :cond_3
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v5, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-ne v4, v5, :cond_4

    goto :goto_1

    .line 133
    :cond_4
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_5

    iget-object v4, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5

    return v2

    .line 134
    :cond_5
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v5, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    return v2

    :cond_6
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_7
    return v0
.end method

.method final b()Lcom/pspdfkit/internal/kl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/kl<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Boolean;
    .locals 2

    .line 31
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, v1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    return-object p1
.end method

.method public final declared-synchronized b(Lcom/pspdfkit/internal/p1;)V
    .locals 7

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/internal/p1;->e:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 3
    :cond_0
    iget-object v0, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 4
    iget-object v3, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v2}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v3

    .line 5
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 6
    iget-object v5, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v5, v3}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 8
    iget-object v6, p0, Lcom/pspdfkit/internal/p1;->d:Lcom/pspdfkit/internal/p1$a;

    invoke-interface {v6, v3, v4, v5}, Lcom/pspdfkit/internal/p1$a;->a(ILjava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 9
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->clear()V

    .line 10
    iget-object v0, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v0

    :goto_2
    if-ge v1, v0, :cond_3

    .line 11
    iget-object v2, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v2, v1}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v2

    .line 12
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    iget-object v4, p1, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v2}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/kl;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    .line 14
    iput-boolean v2, p0, Lcom/pspdfkit/internal/p1;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .line 1
    const-class v0, Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public final declared-synchronized c()Z
    .locals 1

    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    xor-int/lit8 v0, v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    monitor-enter p0

    .line 3
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    const/16 v1, 0xbb9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/kl;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p1;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v0, p1}, Landroidx/collection/SparseArrayCompat;->remove(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p1;->b:Lcom/pspdfkit/internal/kl;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p1;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/p1;->a(Ljava/lang/Object;Ljava/util/HashSet;)Z

    move-result p1

    return p1
.end method

.method public final hashCode()I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 2
    iget-object v3, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v3, v1}, Landroidx/collection/SparseArrayCompat;->keyAt(I)I

    move-result v3

    .line 3
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v1}, Landroidx/collection/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    goto :goto_1

    .line 5
    :cond_0
    iget-object v4, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4, v1}, Landroidx/collection/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_1
    mul-int/lit8 v2, v2, 0x25

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x25

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AnnotationPropertyMap{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/pspdfkit/internal/p1;->a:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v1}, Landroidx/collection/SparseArrayCompat;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

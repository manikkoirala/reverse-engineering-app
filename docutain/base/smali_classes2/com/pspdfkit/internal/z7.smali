.class public Lcom/pspdfkit/internal/z7;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/rf;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/bookmarks/BookmarkProviderFactory;->fromInternalDocument(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/rf;

    move-result-object p0

    return-object p0
.end method

.method public static e(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/ta;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ta;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ta;-><init>(Lcom/pspdfkit/internal/zf;)V

    return-object v0
.end method

.method public static f(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/uf;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/forms/FormProviderFactory;->createFromInternalDocument(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/uf;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/qf;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r1;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/r1;-><init>(Lcom/pspdfkit/internal/zf;)V

    return-object v0
.end method

.method public c(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/g9;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/g9;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/internal/g9;-><init>(Lcom/pspdfkit/internal/zf;Z)V

    return-object v0
.end method

.method public d(Lcom/pspdfkit/internal/zf;)Lcom/pspdfkit/internal/ba;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ba;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/internal/ba;-><init>(Lcom/pspdfkit/internal/zf;Z)V

    return-object v0
.end method

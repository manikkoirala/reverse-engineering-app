.class public final Lcom/pspdfkit/internal/eo;
.super Landroidx/appcompat/app/AlertDialog;
.source "SourceFile"


# instance fields
.field private b:Landroid/widget/ProgressBar;

.field private c:Landroid/widget/TextView;

.field private d:I

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/String;

.field private g:Landroid/widget/TextView;

.field private h:Ljava/text/NumberFormat;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Ljava/lang/CharSequence;

.field private q:Z

.field private r:Z

.field private s:Landroid/os/Handler;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/eo;)Landroid/widget/ProgressBar;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/eo;->e:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/eo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/eo;->f:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/eo;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/eo;->g:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/eo;)Ljava/text/NumberFormat;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/eo;->h:Ljava/text/NumberFormat;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/appcompat/app/AlertDialog;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/eo;->d:I

    .line 28
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    const-string v0, "%1d/%2d"

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/eo;->f:Ljava/lang/String;

    .line 2
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/eo;->h:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    return-void
.end method

.method private b()V
    .locals 2

    .line 6
    iget v0, p0, Lcom/pspdfkit/internal/eo;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->s:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->s:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    goto :goto_0

    .line 8
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/eo;->i:I

    :goto_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 10
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 12
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/eo;->o:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/eo;->f:Ljava/lang/String;

    .line 18
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    return-void
.end method

.method public final a(Ljava/text/NumberFormat;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/eo;->h:Ljava/text/NumberFormat;

    .line 20
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 16
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/eo;->q:Z

    :goto_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/eo;->r:Z

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    goto :goto_0

    .line 5
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/eo;->j:I

    :goto_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/eo;->d:I

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/eo;->d:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 7
    new-instance v1, Lcom/pspdfkit/internal/eo$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/eo$a;-><init>(Lcom/pspdfkit/internal/eo;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->s:Landroid/os/Handler;

    .line 35
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__alert_dialog_progress:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 36
    sget v1, Lcom/pspdfkit/R$id;->pspdf__progress:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    .line 37
    sget v1, Lcom/pspdfkit/R$id;->pspdf__progress_number:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->e:Landroid/widget/TextView;

    .line 38
    sget v1, Lcom/pspdfkit/R$id;->pspdf__progress_percent:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->g:Landroid/widget/TextView;

    .line 39
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_0

    .line 41
    :cond_0
    sget v1, Lcom/pspdfkit/R$layout;->pspdf__progress_dialog:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 42
    sget v1, Lcom/pspdfkit/R$id;->pspdf__progress:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    .line 43
    sget v1, Lcom/pspdfkit/R$id;->pspdf__message:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/pspdfkit/internal/eo;->c:Landroid/widget/TextView;

    .line 44
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 46
    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/eo;->i:I

    if-lez v0, :cond_1

    .line 47
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/eo;->a(I)V

    .line 49
    :cond_1
    iget v0, p0, Lcom/pspdfkit/internal/eo;->j:I

    if-lez v0, :cond_2

    .line 50
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/eo;->b(I)V

    .line 52
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/eo;->k:I

    if-lez v0, :cond_4

    .line 53
    iget-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_3

    .line 54
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 55
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    goto :goto_1

    .line 57
    :cond_3
    iput v0, p0, Lcom/pspdfkit/internal/eo;->k:I

    .line 58
    :cond_4
    :goto_1
    iget v0, p0, Lcom/pspdfkit/internal/eo;->l:I

    if-lez v0, :cond_6

    .line 59
    iget-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_5

    .line 60
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->incrementProgressBy(I)V

    .line 61
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    goto :goto_2

    :cond_5
    add-int/2addr v0, v0

    .line 63
    iput v0, p0, Lcom/pspdfkit/internal/eo;->l:I

    .line 64
    :cond_6
    :goto_2
    iget v0, p0, Lcom/pspdfkit/internal/eo;->m:I

    if-lez v0, :cond_8

    .line 65
    iget-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_7

    .line 66
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->incrementSecondaryProgressBy(I)V

    .line 67
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    goto :goto_3

    :cond_7
    add-int/2addr v0, v0

    .line 69
    iput v0, p0, Lcom/pspdfkit/internal/eo;->m:I

    .line 70
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    .line 71
    iget-object v1, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_9

    .line 72
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 74
    :cond_9
    iput-object v0, p0, Lcom/pspdfkit/internal/eo;->n:Landroid/graphics/drawable/Drawable;

    .line 75
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    .line 76
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/eo;->a(Landroid/graphics/drawable/Drawable;)V

    .line 78
    :cond_b
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->p:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c

    .line 79
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/eo;->setMessage(Ljava/lang/CharSequence;)V

    .line 81
    :cond_c
    iget-boolean v0, p0, Lcom/pspdfkit/internal/eo;->q:Z

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/eo;->a(Z)V

    .line 82
    invoke-direct {p0}, Lcom/pspdfkit/internal/eo;->b()V

    .line 83
    invoke-super {p0, p1}, Landroidx/appcompat/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/activity/ComponentDialog;->onStart()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/eo;->r:Z

    return-void
.end method

.method protected final onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatDialog;->onStop()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/eo;->r:Z

    return-void
.end method

.method public final setMessage(Ljava/lang/CharSequence;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 2
    iget v0, p0, Lcom/pspdfkit/internal/eo;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3
    invoke-super {p0, p1}, Landroidx/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/eo;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 8
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/eo;->p:Ljava/lang/CharSequence;

    :goto_0
    return-void
.end method

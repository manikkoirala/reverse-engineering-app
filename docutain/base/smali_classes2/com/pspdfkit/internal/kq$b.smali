.class public final Lcom/pspdfkit/internal/kq$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/kq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/kq$b$a;,
        Lcom/pspdfkit/internal/kq$b$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/kq$b$b;


# instance fields
.field private final a:Ljava/util/UUID;

.field private final b:Lcom/pspdfkit/internal/iv;

.field private final c:Lcom/pspdfkit/internal/bd;

.field private final d:Lcom/pspdfkit/internal/cb;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/kq$b$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/kq$b$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/kq$b;->Companion:Lcom/pspdfkit/internal/kq$b$b;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/UUID;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/cb;)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0xf

    const/16 v1, 0xf

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/kq$b$a;->a:Lcom/pspdfkit/internal/kq$b$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kq$b$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/kq$b;->a:Ljava/util/UUID;

    iput-object p3, p0, Lcom/pspdfkit/internal/kq$b;->b:Lcom/pspdfkit/internal/iv;

    iput-object p4, p0, Lcom/pspdfkit/internal/kq$b;->c:Lcom/pspdfkit/internal/bd;

    iput-object p5, p0, Lcom/pspdfkit/internal/kq$b;->d:Lcom/pspdfkit/internal/cb;

    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/cb;)V
    .locals 1

    const-string v0, "textBlockId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "anchor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "globalEffects"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalControlState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/kq$b;->a:Ljava/util/UUID;

    .line 5
    iput-object p2, p0, Lcom/pspdfkit/internal/kq$b;->b:Lcom/pspdfkit/internal/iv;

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/kq$b;->c:Lcom/pspdfkit/internal/bd;

    .line 7
    iput-object p4, p0, Lcom/pspdfkit/internal/kq$b;->d:Lcom/pspdfkit/internal/cb;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/kq$b;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/qu;->a:Lcom/pspdfkit/internal/qu;

    iget-object v1, p0, Lcom/pspdfkit/internal/kq$b;->a:Ljava/util/UUID;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/iv$a;->a:Lcom/pspdfkit/internal/iv$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/kq$b;->b:Lcom/pspdfkit/internal/iv;

    const/4 v2, 0x1

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/bd$a;->a:Lcom/pspdfkit/internal/bd$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/kq$b;->c:Lcom/pspdfkit/internal/bd;

    const/4 v2, 0x2

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/cb$a;->a:Lcom/pspdfkit/internal/cb$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/kq$b;->d:Lcom/pspdfkit/internal/cb;

    const/4 v1, 0x3

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method

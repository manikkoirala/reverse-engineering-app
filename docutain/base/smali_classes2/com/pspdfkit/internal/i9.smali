.class final Lcom/pspdfkit/internal/i9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/dialog/DocumentPrintDialog$PrintDialogListener;


# instance fields
.field final synthetic a:Landroidx/fragment/app/FragmentActivity;

.field final synthetic b:Lcom/pspdfkit/internal/j9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/j9;Landroidx/fragment/app/FragmentActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/i9;->b:Lcom/pspdfkit/internal/j9;

    iput-object p2, p0, Lcom/pspdfkit/internal/i9;->a:Landroidx/fragment/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAccept(Lcom/pspdfkit/document/printing/PrintOptions;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i9;->b:Lcom/pspdfkit/internal/j9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/j9;->-$$Nest$fpute(Lcom/pspdfkit/internal/j9;Z)V

    .line 2
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/i9;->a:Landroidx/fragment/app/FragmentActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/i9;->b:Lcom/pspdfkit/internal/j9;

    invoke-static {v2}, Lcom/pspdfkit/internal/j9;->-$$Nest$fgeta(Lcom/pspdfkit/internal/j9;)Lcom/pspdfkit/document/PdfDocument;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->print(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/printing/PrintOptions;)V

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "print"

    .line 5
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/document/sharing/SharingOptions;->getAnnotationProcessingMode()Lcom/pspdfkit/document/processor/PdfProcessorTask$AnnotationProcessingMode;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    const-string v1, "processing_mode"

    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final onDismiss()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i9;->b:Lcom/pspdfkit/internal/j9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/j9;->-$$Nest$fpute(Lcom/pspdfkit/internal/j9;Z)V

    return-void
.end method

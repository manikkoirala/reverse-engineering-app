.class public final Lcom/pspdfkit/internal/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/document/providers/DataProvider;
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/internal/bj;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Landroidx/collection/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/ArrayMap<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:[B

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/bj$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/bj$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/bj;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 17
    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/bj;->d:Landroidx/collection/ArrayMap;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->a:Ljava/lang/String;

    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 28
    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->c:[B

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    .line 45
    sget-object v0, Lcom/pspdfkit/internal/bj;->d:Landroidx/collection/ArrayMap;

    invoke-virtual {v0, p1}, Landroidx/collection/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "PSPDFKit.MemoryDataProvider"

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v0, p1}, Landroidx/collection/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lcom/pspdfkit/internal/bj;->b:[B

    .line 47
    invoke-virtual {v0, p1}, Landroidx/collection/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Restored memory provider with UID "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v2, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array p1, v3, [Ljava/lang/Object;

    const-string v0, "Could not restore PDF activity - memory PDF data is not valid after process death."

    .line 50
    invoke-static {v2, v0, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-array p1, v3, [B

    .line 51
    iput-object p1, p0, Lcom/pspdfkit/internal/bj;->b:[B

    :goto_0
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->a:Ljava/lang/String;

    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->c:[B

    const-string v0, "pdfData"

    .line 19
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/pspdfkit/internal/bj;->b:[B

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getSize()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->b:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getUid()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->b:[B

    const/high16 v1, 0x500000

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ft;->a([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->a:Ljava/lang/String;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final read(JJ)[B
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->c:[B

    array-length v0, v0

    int-to-long v0, v0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    long-to-int v0, p1

    .line 2
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->c:[B

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/bj;->b:[B

    array-length v0, v0

    int-to-long v0, v0

    sub-long/2addr v0, p3

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    const/4 v2, 0x0

    cmp-long v3, v0, p1

    if-eqz v3, :cond_1

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/bj;->c:[B

    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/bj;->b:[B

    long-to-int p2, p3

    iget-object p3, p0, Lcom/pspdfkit/internal/bj;->c:[B

    long-to-int p4, v0

    invoke-static {p1, p2, p3, v2, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/bj;->c:[B

    return-object p1
.end method

.method public final release()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/bj;->b:[B

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/bj;->d:Landroidx/collection/ArrayMap;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/bj;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bj;->getUid()Ljava/lang/String;

    move-result-object p2

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Parceling memory provider with UID "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.MemoryDataProvider"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/bj;->d:Landroidx/collection/ArrayMap;

    iget-object v1, p0, Lcom/pspdfkit/internal/bj;->b:[B

    invoke-virtual {v0, p2, v1}, Landroidx/collection/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/j0;
.super Lcom/pspdfkit/internal/yg;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p1}, Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration$-CC;->builder(Landroid/content/Context;)Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration$Builder;

    move-result-object p1

    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder;->setDefaultThickness(F)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration$Builder;

    .line 3
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    invoke-interface {p1, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration$Builder;->setDefaultColor(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration$Builder;

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration$Builder;->build()Lcom/pspdfkit/annotations/configuration/MeasurementAreaAnnotationConfiguration;

    move-result-object p1

    const-string v0, "builder(context)\n       \u2026                 .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.class public final Lcom/pspdfkit/internal/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/pspdfkit/internal/do;

.field final b:Lcom/pspdfkit/internal/do;


# direct methods
.method public constructor <init>(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeDocumentProvider;->getDefaultMaximumAlternateDocuments()B

    move-result p1

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 5
    :goto_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const-string v2, "pspdfkit-render"

    invoke-virtual {v1, p1, v2}, Lcom/pspdfkit/internal/u;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/aa;->a:Lcom/pspdfkit/internal/do;

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u;

    const-string v1, "pspdfkit-metadata"

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/u;->a(ILjava/lang/String;)Lcom/pspdfkit/internal/do;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/aa;->b:Lcom/pspdfkit/internal/do;

    return-void
.end method


# virtual methods
.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aa;->a:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/do;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/aa;->b:Lcom/pspdfkit/internal/do;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/do;->c()V

    .line 3
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

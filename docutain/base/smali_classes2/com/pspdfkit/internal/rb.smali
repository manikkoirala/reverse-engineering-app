.class public final Lcom/pspdfkit/internal/rb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xc

    new-array v0, v0, [Lkotlin/Pair;

    .line 1
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_EXCLUDE:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 2
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_NO_VALUE_FIELDS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 3
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EXPORT_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 4
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->GET_METHOD:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 5
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->SUBMIT_COORDINATES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 6
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->XFDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 7
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_APPEND_SAVES:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x40

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 8
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->INCLUDE_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 9
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->SUBMIT_PDF:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x100

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 10
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->CANONICAL_FORMAT:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x200

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 11
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EXCLUDE_NON_USER_ANNOTATIONS:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 12
    sget-object v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;->EMBED_FORM:Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    const-wide/16 v2, 0x2000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 13
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/rb;->a:Ljava/util/Map;

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/ResetFormAction;)I
    .locals 11

    const-string v0, "builder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->getFieldNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->getFieldNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x4

    const/4 v8, 0x1

    if-eqz v5, :cond_0

    add-int/lit8 v5, v4, 0x1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 5
    invoke-virtual {p0, v9}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v9

    .line 6
    sget v10, Lcom/pspdfkit/internal/t1;->e:I

    .line 7
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/mb;->e(I)V

    const/4 v7, 0x3

    .line 8
    invoke-virtual {p0, v7, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 9
    invoke-virtual {p0, v6, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 10
    invoke-virtual {p0, v8, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 11
    invoke-virtual {p0, v3, v9}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v6

    .line 13
    aput v6, v1, v4

    move v4, v5

    goto :goto_0

    .line 23
    :cond_0
    sget v2, Lcom/pspdfkit/internal/qp;->e:I

    .line 24
    invoke-virtual {p0, v7, v0, v7}, Lcom/pspdfkit/internal/mb;->a(III)V

    sub-int/2addr v0, v8

    :goto_1
    if-ltz v0, :cond_1

    aget v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/ResetFormAction;->shouldExcludeFormFields()Z

    move-result p1

    if-eqz p1, :cond_2

    const-wide/16 v1, 0x1

    goto :goto_2

    :cond_2
    const-wide/16 v1, 0x0

    .line 26
    :goto_2
    invoke-virtual {p0, v6}, Lcom/pspdfkit/internal/mb;->e(I)V

    long-to-int p1, v1

    .line 27
    invoke-virtual {p0, v8, p1}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 28
    invoke-virtual {p0, v3, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method

.method public static final a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/SubmitFormAction;)I
    .locals 14

    const-string v0, "builder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->getFieldNames()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 39
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/AbstractFormAction;->getFieldNames()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x1

    if-eqz v5, :cond_0

    add-int/lit8 v5, v4, 0x1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 42
    invoke-virtual {p0, v10}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v10

    .line 43
    sget v11, Lcom/pspdfkit/internal/t1;->e:I

    .line 44
    invoke-virtual {p0, v8}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 45
    invoke-virtual {p0, v7, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 46
    invoke-virtual {p0, v6, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 47
    invoke-virtual {p0, v9, v3}, Lcom/pspdfkit/internal/mb;->a(II)V

    .line 48
    invoke-virtual {p0, v3, v10}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 49
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result v6

    .line 50
    aput v6, v1, v4

    move v4, v5

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/SubmitFormAction;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v2

    .line 61
    sget v4, Lcom/pspdfkit/internal/kt;->e:I

    .line 62
    invoke-virtual {p0, v8, v0, v8}, Lcom/pspdfkit/internal/mb;->a(III)V

    sub-int/2addr v0, v9

    :goto_1
    if-ltz v0, :cond_1

    aget v4, v1, v0

    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result v0

    .line 63
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/SubmitFormAction;->getFlags()Ljava/util/EnumSet;

    move-result-object p1

    const-string v1, "action.flags"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v1

    const-wide/16 v4, 0x0

    if-eqz v1, :cond_2

    goto :goto_4

    .line 66
    :cond_2
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-wide v10, v4

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    .line 67
    sget-object v8, Lcom/pspdfkit/internal/rb;->a:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    goto :goto_3

    :cond_3
    move-wide v12, v4

    :goto_3
    or-long/2addr v10, v12

    goto :goto_2

    :cond_4
    move-wide v4, v10

    .line 68
    :goto_4
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/mb;->e(I)V

    .line 69
    invoke-virtual {p0, v6, v4, v5}, Lcom/pspdfkit/internal/mb;->a(IJ)V

    .line 70
    invoke-virtual {p0, v9, v0}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 71
    invoke-virtual {p0, v3, v2}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 72
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method

.method public static final a(Lcom/pspdfkit/internal/qp;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/ResetFormAction;
    .locals 7

    const-string v0, "resetFormAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/qp;->a()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 31
    invoke-virtual {p0}, Lcom/pspdfkit/internal/qp;->a()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 32
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/qp;->f(I)Lcom/pspdfkit/internal/t1;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/t1;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 33
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/qp;->f(I)Lcom/pspdfkit/internal/t1;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/t1;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/qp;->b()J

    move-result-wide v3

    const-wide/16 v5, 0x1

    and-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long p0, v3, v5

    if-eqz p0, :cond_2

    const/4 v2, 0x1

    .line 37
    :cond_2
    new-instance p0, Lcom/pspdfkit/annotations/actions/ResetFormAction;

    invoke-direct {p0, v0, v2, p1}, Lcom/pspdfkit/annotations/actions/ResetFormAction;-><init>(Ljava/util/List;ZLjava/util/List;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/kt;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/SubmitFormAction;
    .locals 12

    const-string v0, "submitFormAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/kt;->b()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kt;->b()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 75
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/kt;->f(I)Lcom/pspdfkit/internal/t1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 76
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/kt;->f(I)Lcom/pspdfkit/internal/t1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 79
    :cond_1
    new-instance v1, Lcom/pspdfkit/annotations/actions/SubmitFormAction;

    .line 80
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kt;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {p0}, Lcom/pspdfkit/internal/kt;->c()J

    move-result-wide v3

    .line 83
    const-class p0, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    invoke-static {p0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p0

    .line 84
    sget-object v5, Lcom/pspdfkit/internal/rb;->a:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/annotations/actions/SubmitFormAction$SubmitFormActionFlag;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    and-long/2addr v8, v3

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_2

    .line 86
    invoke-virtual {p0, v7}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v3, "flagSet"

    .line 89
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {v1, v2, v0, p0, p1}, Lcom/pspdfkit/annotations/actions/SubmitFormAction;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/EnumSet;Ljava/util/List;)V

    return-object v1
.end method

.class final Lcom/pspdfkit/internal/tb$b;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/tb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/pspdfkit/forms/FormElement;

.field final synthetic b:Lcom/pspdfkit/internal/tb;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/tb;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/tb;Lcom/pspdfkit/internal/tb$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/tb$b;-><init>(Lcom/pspdfkit/internal/tb;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final c(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {v0}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetj(Lcom/pspdfkit/internal/tb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xb;

    .line 2
    invoke-interface {v1}, Lcom/pspdfkit/internal/xb;->a()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/tb;->c(Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final onDown(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {v0}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetb(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/dm;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetm(Lcom/pspdfkit/internal/tb;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {v1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetl(Lcom/pspdfkit/internal/tb;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/tb;->b(Landroid/view/MotionEvent;)Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {v1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetc(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/yb;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ac;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ac;->d(Lcom/pspdfkit/forms/FormElement;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    if-eqz p1, :cond_4

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/forms/FormType;->SIGNATURE:Lcom/pspdfkit/forms/FormType;

    if-ne p1, v0, :cond_4

    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/forms/FormType;->PUSHBUTTON:Lcom/pspdfkit/forms/FormType;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 15
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    .line 16
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v0

    .line 17
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zb;->setHighlightRect(Landroid/graphics/RectF;)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-nez p1, :cond_3

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetb(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 22
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgeti(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/internal/zb;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 26
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/tb$b;->b:Lcom/pspdfkit/internal/tb;

    iget-object v0, p0, Lcom/pspdfkit/internal/tb$b;->a:Lcom/pspdfkit/forms/FormElement;

    sget-object v1, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_DOWN:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez v0, :cond_5

    goto :goto_0

    .line 27
    :cond_5
    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/pf;->getAdditionalAction(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 29
    invoke-static {p1}, Lcom/pspdfkit/internal/tb;->-$$Nest$fgetp(Lcom/pspdfkit/internal/tb;)Lcom/pspdfkit/annotations/actions/ActionResolver;

    move-result-object p1

    new-instance v2, Lcom/pspdfkit/annotations/actions/ActionSender;

    invoke-direct {v2, v0}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/forms/FormElement;)V

    invoke-interface {p1, v1, v2}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

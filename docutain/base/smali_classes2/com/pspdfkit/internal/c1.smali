.class public final Lcom/pspdfkit/internal/c1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/c1$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/s1;

.field private final c:Lcom/pspdfkit/internal/c1$a;

.field private final d:Lcom/pspdfkit/internal/fl;

.field private e:Z

.field private final f:Ljava/util/ArrayList;

.field private g:Lio/reactivex/rxjava3/disposables/Disposable;

.field private h:Lcom/pspdfkit/document/PdfDocument;

.field private i:Lcom/pspdfkit/configuration/PdfConfiguration;


# direct methods
.method public static synthetic $r8$lambda$pEBoyfBjRWyCm06Lt-fBFUadlAM(Lcom/pspdfkit/internal/c1;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/c1;->d(Lcom/pspdfkit/internal/c1;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/EnumSet;Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/c1$a;Lcom/pspdfkit/internal/fl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;",
            "Lcom/pspdfkit/internal/s1;",
            "Lcom/pspdfkit/internal/c1$a;",
            "Lcom/pspdfkit/internal/fl;",
            ")V"
        }
    .end annotation

    const-string v0, "listedAnnotationTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/c1;->a:Ljava/util/EnumSet;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    .line 5
    iput-object p3, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    .line 6
    iput-object p4, p0, Lcom/pspdfkit/internal/c1;->d:Lcom/pspdfkit/internal/fl;

    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/pspdfkit/internal/c1;->e:Z

    .line 12
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/c1;)Lcom/pspdfkit/internal/s1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    return-object p0
.end method

.method private final a(Lcom/pspdfkit/internal/mh;)Lcom/pspdfkit/internal/x;
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 41
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mh;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    instance-of v0, p1, Lcom/pspdfkit/internal/mh$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/pspdfkit/internal/mh$a;

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh$a;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    .line 49
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh$a;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-object v1

    .line 53
    :cond_1
    instance-of v0, p1, Lcom/pspdfkit/internal/mh$c;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/pspdfkit/internal/mh$c;

    .line 54
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh$c;->f()Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getFormField()Lcom/pspdfkit/forms/FormField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormField;->reset()Z

    :cond_2
    return-object v1
.end method

.method public static final synthetic b(Lcom/pspdfkit/internal/c1;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic c(Lcom/pspdfkit/internal/c1;)Lcom/pspdfkit/internal/c1$a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    return-object p0
.end method

.method private static final d(Lcom/pspdfkit/internal/c1;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/s1;->b(Z)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    iget-object v2, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-interface {v0, v2, v1}, Lcom/pspdfkit/internal/c1$a;->a(Ljava/util/List;Z)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/c1;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method

.method public final a(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/c1;->h:Lcom/pspdfkit/document/PdfDocument;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/mh;Lcom/pspdfkit/internal/mh;I)V
    .locals 5

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationAnnotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->i:Lcom/pspdfkit/configuration/PdfConfiguration;

    if-eqz v0, :cond_5

    .line 8
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mh;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10
    instance-of v0, p1, Lcom/pspdfkit/internal/mh$a;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/internal/mh$c;

    :goto_0
    if-eqz v0, :cond_3

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_5

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p2

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    .line 15
    iget-object v3, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    add-int v4, p2, p3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/mh;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/mh;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-eqz p1, :cond_1

    if-eqz v3, :cond_1

    .line 18
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v4

    .line 19
    invoke-interface {v4, v3}, Lcom/pspdfkit/annotations/AnnotationProvider;->getZIndexAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v3

    .line 20
    new-instance v4, Lcom/pspdfkit/internal/g1;

    invoke-direct {v4, v0, p1, p3}, Lcom/pspdfkit/internal/g1;-><init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/Annotation;I)V

    invoke-virtual {v3, v4}, Lio/reactivex/rxjava3/core/Single;->flatMapCompletable(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    if-ge v2, p2, :cond_2

    :goto_1
    if-ge v2, p2, :cond_5

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    add-int/lit8 p3, v2, 0x1

    invoke-static {p1, v2, p3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    move v2, p3

    goto :goto_1

    :cond_2
    add-int/2addr p2, v1

    if-gt p2, v2, :cond_5

    .line 35
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    add-int/lit8 p3, v2, -0x1

    invoke-static {p1, v2, p3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    if-eq v2, p2, :cond_5

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 36
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Only list items that are annotations can be swapped."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 39
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Annotations can\'t be reordered."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    return-void
.end method

.method public final a(Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/c1;->a:Ljava/util/EnumSet;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/c1;->e:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/c1;->e:Z

    return v0
.end method

.method public final b()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->a:Ljava/util/EnumSet;

    return-object v0
.end method

.method public final b(Lcom/pspdfkit/internal/mh;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/c1;->a(Lcom/pspdfkit/internal/mh;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->d:Lcom/pspdfkit/internal/fl;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "listItems"

    .line 8
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1;->a()V

    .line 117
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/s1;->a(Ljava/util/List;)V

    .line 118
    iget-object p1, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1, v0, v1}, Lcom/pspdfkit/internal/c1$a;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public final c()V
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->h:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_1

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/s1;->a()V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/s1;->b(Z)V

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Lcom/pspdfkit/internal/c1$a;->a(Ljava/util/List;Z)V

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x7d0

    if-le v1, v3, :cond_0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v4, "PSPDFKit"

    const-string v5, "Only loading annotations from first 2000 pages into annotation list."

    .line 10
    invoke-static {v4, v5, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Lio/reactivex/rxjava3/core/Observable;->range(II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v1

    .line 13
    new-instance v2, Lcom/pspdfkit/internal/c1$b;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/c1$b;-><init>(Lcom/pspdfkit/internal/c1;Lcom/pspdfkit/document/PdfDocument;)V

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Observable;->flatMapSingle(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 15
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/pspdfkit/internal/c1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/c1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/c1;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/pspdfkit/internal/c1$c;->a:Lcom/pspdfkit/internal/c1$c;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/pspdfkit/internal/c1$d;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/c1$d;-><init>(Lcom/pspdfkit/internal/c1;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    return-void
.end method

.method public final d()V
    .locals 5

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    const/4 v3, -0x1

    if-ge v3, v1, :cond_1

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/mh;

    .line 8
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/c1;->a(Lcom/pspdfkit/internal/mh;)Lcom/pspdfkit/internal/x;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 10
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 14
    :cond_1
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->reverse(Ljava/util/List;)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->b:Lcom/pspdfkit/internal/s1;

    iget-object v3, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "listItems"

    .line 17
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v1}, Lcom/pspdfkit/internal/s1;->a()V

    .line 126
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/s1;->a(Ljava/util/List;)V

    .line 127
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1, v3, v2}, Lcom/pspdfkit/internal/c1$a;->a(Ljava/util/List;Z)V

    .line 130
    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->d:Lcom/pspdfkit/internal/fl;

    if-eqz v1, :cond_3

    new-instance v2, Lcom/pspdfkit/internal/v5;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/v5;-><init>(Ljava/util/List;)V

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_3
    return-void
.end method

.method public final e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/pspdfkit/internal/c1;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c1;->c:Lcom/pspdfkit/internal/c1$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/c1;->f:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/internal/c1$a;->a(Ljava/util/List;Z)V

    return-void
.end method

.class final Lcom/pspdfkit/internal/f1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/c1;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/c1;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/f1;->a:Lcom/pspdfkit/internal/c1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/Annotation;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    instance-of v0, p1, Lcom/pspdfkit/annotations/WidgetAnnotation;

    if-eqz v0, :cond_1

    .line 107
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/WidgetAnnotation;->getFormElement()Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    new-instance v1, Lcom/pspdfkit/internal/mh$c;

    iget-object v2, p0, Lcom/pspdfkit/internal/f1;->a:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/c1;->a()Z

    move-result v2

    invoke-direct {v1, p1, v0, v2}, Lcom/pspdfkit/internal/mh$c;-><init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/forms/FormElement;Z)V

    goto :goto_0

    .line 111
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/mh$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/f1;->a:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c1;->a()Z

    move-result v0

    invoke-direct {v1, p1, v0}, Lcom/pspdfkit/internal/mh$a;-><init>(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_0

    .line 114
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/mh$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/f1;->a:Lcom/pspdfkit/internal/c1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c1;->a()Z

    move-result v0

    invoke-direct {v1, p1, v0}, Lcom/pspdfkit/internal/mh$a;-><init>(Lcom/pspdfkit/annotations/Annotation;Z)V

    :goto_0
    return-object v1
.end method

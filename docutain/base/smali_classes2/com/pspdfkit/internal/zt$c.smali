.class final Lcom/pspdfkit/internal/zt$c;
.super Lcom/pspdfkit/internal/o7$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field private a:F

.field private b:F

.field private c:Z

.field final synthetic d:Lcom/pspdfkit/internal/zt;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/zt;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    invoke-direct {p0}, Lcom/pspdfkit/internal/o7$c;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/internal/zt$c-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zt$c;-><init>(Lcom/pspdfkit/internal/zt;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/MotionEvent;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    return-void
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zt;->g()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/zt$c;->a:F

    .line 3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/zt$c;->b:F

    .line 4
    iget-boolean p1, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    return p1
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 6

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zt;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_2

    .line 2
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/zt$c;->a:F

    iget v1, p0, Lcom/pspdfkit/internal/zt$c;->b:F

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    invoke-static {p1}, Lcom/pspdfkit/internal/zt;->-$$Nest$fgetv(Lcom/pspdfkit/internal/zt;)Landroid/graphics/Matrix;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object p1, p1, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    if-nez p1, :cond_1

    goto :goto_0

    .line 5
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v3

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    iget-object p1, p1, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    invoke-static {p1}, Lcom/pspdfkit/internal/zt;->-$$Nest$fgetl(Lcom/pspdfkit/internal/zt;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1}, Lcom/pspdfkit/internal/zt;->-$$Nest$fgetv(Lcom/pspdfkit/internal/zt;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/pspdfkit/internal/zt;->a(FFFLcom/pspdfkit/internal/zf;ILandroid/graphics/Matrix;)Lcom/pspdfkit/datastructures/TextSelectionRectangles;

    move-result-object p1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    iget-object v0, v0, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    if-eqz v0, :cond_3

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    iget-object v1, v1, Lcom/pspdfkit/internal/zt;->m:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    .line 12
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/datastructures/TextSelection;->fromTextRects(Lcom/pspdfkit/document/PdfDocument;ILcom/pspdfkit/datastructures/TextSelectionRectangles;)Lcom/pspdfkit/datastructures/TextSelection;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/internal/zt$b;->a:Lcom/pspdfkit/internal/zt$b;

    .line 13
    invoke-static {v2, p1, v0}, Lcom/pspdfkit/internal/zt;->-$$Nest$ma(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/zt$c;->d:Lcom/pspdfkit/internal/zt;

    const/4 v1, 0x0

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/zt;->-$$Nest$fgetu(Lcom/pspdfkit/internal/zt;)Lcom/pspdfkit/internal/zt$b;

    move-result-object v2

    .line 4
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/zt;->-$$Nest$ma(Lcom/pspdfkit/internal/zt;Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/zt$b;)V

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/zt$c;->c:Z

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method

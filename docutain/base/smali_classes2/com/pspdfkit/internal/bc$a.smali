.class final Lcom/pspdfkit/internal/bc$a;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/bc;->formDidAddFormField(Lcom/pspdfkit/internal/jni/NativeDocument;ILcom/pspdfkit/internal/jni/NativeFormField;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/pspdfkit/forms/FormField;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/uf;

.field final synthetic b:I

.field final synthetic c:Lcom/pspdfkit/internal/jni/NativeFormField;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/uf;ILcom/pspdfkit/internal/jni/NativeFormField;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/bc$a;->a:Lcom/pspdfkit/internal/uf;

    iput p2, p0, Lcom/pspdfkit/internal/bc$a;->b:I

    iput-object p3, p0, Lcom/pspdfkit/internal/bc$a;->c:Lcom/pspdfkit/internal/jni/NativeFormField;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bc$a;->a:Lcom/pspdfkit/internal/uf;

    iget v1, p0, Lcom/pspdfkit/internal/bc$a;->b:I

    iget-object v2, p0, Lcom/pspdfkit/internal/bc$a;->c:Lcom/pspdfkit/internal/jni/NativeFormField;

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/internal/uf;->onFormFieldAdded(ILcom/pspdfkit/internal/jni/NativeFormField;)Lcom/pspdfkit/forms/FormField;

    move-result-object v0

    return-object v0
.end method

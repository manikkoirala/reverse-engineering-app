.class public final Lcom/pspdfkit/internal/rl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final A:I

.field public final B:I

.field public final C:I

.field public final D:I

.field public final E:I

.field public final F:I

.field public final G:I

.field public final H:I

.field public final I:I

.field public final J:I

.field public final K:I

.field public final L:I

.field public final M:I

.field public final N:I

.field public final O:I

.field public final P:I

.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:I

.field public final y:I

.field public final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__outlineViewStyle:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_OutlineView:I

    const/4 v4, 0x0

    .line 3
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 8
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__backgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 10
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 11
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->a:I

    .line 14
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__listItemSelector:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->b:I

    .line 15
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__defaultTextColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 17
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->c:I

    .line 21
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksBarBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 23
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->d:I

    .line 27
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksBarIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 29
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->e:I

    .line 34
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksCurrentPageColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 36
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 37
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->f:I

    .line 41
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksAddIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_add:I

    .line 42
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->g:I

    .line 43
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksEditIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit:I

    .line 44
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->h:I

    .line 45
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDoneIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    .line 46
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->i:I

    .line 47
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksGroupIndicatorIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 49
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 50
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->j:I

    .line 53
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDeleteIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->k:I

    .line 55
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDeleteIconColor:I

    const/4 v2, -0x1

    .line 56
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->l:I

    .line 57
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDeleteBackgroundColor:I

    const/high16 v2, -0x10000

    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->m:I

    .line 59
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDragHandleIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_drag_handle:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->n:I

    .line 61
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__bookmarksDragHandleIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 63
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 64
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->o:I

    .line 67
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsBarBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 69
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 70
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->p:I

    .line 73
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsBarIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 75
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->q:I

    .line 79
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsEditIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit:I

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->t:I

    .line 81
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsDoneIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    .line 82
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->u:I

    .line 83
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsDeleteIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_delete:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->r:I

    .line 85
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsDeleteIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 87
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 88
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->s:I

    .line 91
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsDragHandleIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_drag_handle:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->v:I

    .line 93
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__annotationsDragHandleIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_black:I

    .line 95
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 96
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->w:I

    .line 99
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabOutlineIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline_view_outline:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->x:I

    .line 102
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabBookmarksIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline_view_bookmarks:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->y:I

    .line 105
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabAnnotationsIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline_view_annotations:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->z:I

    .line 108
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabDocumentInfoIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline_view_information:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->A:I

    .line 111
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabIconsColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    .line 113
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 114
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->B:I

    .line 117
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabIconsColorSelected:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 119
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 120
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->C:I

    .line 123
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__navigationTabBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 125
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 126
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->D:I

    .line 129
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoGroupTitleTextColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__document_info_group_title_text_color:I

    .line 131
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 132
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->E:I

    .line 135
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoItemTitleTextColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__document_info_item_title_text_color:I

    .line 137
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 138
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->F:I

    .line 141
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoItemValueTextColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__document_info_item_value_text_color:I

    .line 143
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 144
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->G:I

    .line 147
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoItemValueHintTextColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__document_info_item_value_hint_text_color:I

    .line 149
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 150
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->H:I

    .line 153
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoGroupIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_gray:I

    .line 155
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 156
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->I:I

    .line 159
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoContentIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->J:I

    .line 161
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoChangesIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_info:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->K:I

    .line 163
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoSizeIcon:I

    sget v2, Lcom/pspdfkit/R$drawable;->pspdf__ic_size:I

    .line 164
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->L:I

    .line 165
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoFabBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color:I

    .line 167
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 168
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/rl;->M:I

    .line 171
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoFabIconColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 173
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 174
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/rl;->N:I

    .line 177
    sget p1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoFabEditIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit:I

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/rl;->O:I

    .line 179
    sget p1, Lcom/pspdfkit/R$styleable;->pspdf__OutlineView_pspdf__documentInfoFabDoneIcon:I

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_done:I

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/rl;->P:I

    .line 182
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

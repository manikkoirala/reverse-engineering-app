.class public final Lcom/pspdfkit/internal/dp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/pspdfkit/internal/jni/NativeReflowProcessor;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/internal/fp;)V
    .locals 2

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->READER_VIEW:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/tm;->a(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/internal/zf;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativeReflowConfiguration;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeReflowConfiguration;

    move-result-object p1

    const-string v0, "create(document.asIntern\u2026ocument().nativeDocument)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/ep;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/ep;-><init>(Lcom/pspdfkit/internal/fp;)V

    .line 10
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/jni/NativeReflowProcessor;->create(Lcom/pspdfkit/internal/jni/NativeReflowConfiguration;Lcom/pspdfkit/internal/jni/NativeReflowProcessorDelegate;)Lcom/pspdfkit/internal/jni/NativeReflowProcessorCreationResult;

    move-result-object p1

    const-string p2, "create(nativeReflowConfi\u2026 reflowProcessorDelegate)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeReflowProcessorCreationResult;->getSuccess()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeReflowProcessorCreationResult;->getReflowProcessor()Lcom/pspdfkit/internal/jni/NativeReflowProcessor;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/dp;->a:Lcom/pspdfkit/internal/jni/NativeReflowProcessor;

    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeReflowProcessorCreationResult;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.ReaderView"

    invoke-static {v0, p1, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 15
    :cond_1
    new-instance p1, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p2, "Your current license doesn\'t allow using the reflow processor."

    invoke-direct {p1, p2}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dp;->a:Lcom/pspdfkit/internal/jni/NativeReflowProcessor;

    const-string v1, "format(format, *args)"

    const-string v2, "<!doctype html><html><head><meta charset=\"utf-8\"></meta></head><body>%s</body></html>"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v0, :cond_0

    .line 2
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array v0, v4, [Ljava/lang/Object;

    const-string v5, "The reflow processor could not be initialized."

    aput-object v5, v0, v3

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeReflowProcessor;->reflowAllPages()Lcom/pspdfkit/internal/jni/NativeReflowResult;

    move-result-object v5

    const-string v6, "nativeReflowProcessor.reflowAllPages()"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeReflowResult;->getHasError()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 5
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/jni/NativeReflowResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v3

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 7
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeReflowProcessor;->getReflowedDocument()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nativeReflowProcessor.reflowedDocument"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

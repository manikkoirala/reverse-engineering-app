.class public final Lcom/pspdfkit/internal/yh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(IILandroid/graphics/Bitmap;)V
    .locals 2

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yh;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez p3, :cond_0

    .line 35
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 36
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 38
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    .line 39
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yh;->a:Z

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lcom/pspdfkit/internal/yh;->a:Z

    .line 47
    iput-object p3, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    :goto_0
    return-void

    .line 48
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Reusable bitmap size doesn\'t conform to width and height parameters!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yh;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    .line 14
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yh;->a:Z

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempted to use recycled bitmap."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yh;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    return-void
.end method

.method public final c()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yh;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/yh;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/yh;->c:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

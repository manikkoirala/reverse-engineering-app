.class public final enum Lcom/pspdfkit/internal/ls;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/ls;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/ls;

.field public static final enum b:Lcom/pspdfkit/internal/ls;

.field public static final enum c:Lcom/pspdfkit/internal/ls;

.field public static final enum d:Lcom/pspdfkit/internal/ls;

.field public static final enum e:Lcom/pspdfkit/internal/ls;

.field private static final synthetic f:[Lcom/pspdfkit/internal/ls;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ls;

    const-string v1, "STOPPED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/ls;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/ls;->a:Lcom/pspdfkit/internal/ls;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/ls;

    const-string v3, "RECORDING"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/ls;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/ls;->b:Lcom/pspdfkit/internal/ls;

    .line 3
    new-instance v3, Lcom/pspdfkit/internal/ls;

    const-string v5, "RECORDING_PAUSED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/ls;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/ls;->c:Lcom/pspdfkit/internal/ls;

    .line 4
    new-instance v5, Lcom/pspdfkit/internal/ls;

    const-string v7, "PLAYING"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/ls;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/ls;->d:Lcom/pspdfkit/internal/ls;

    .line 5
    new-instance v7, Lcom/pspdfkit/internal/ls;

    const-string v9, "PLAYING_PAUSED"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/ls;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/ls;->e:Lcom/pspdfkit/internal/ls;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/pspdfkit/internal/ls;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    .line 7
    sput-object v9, Lcom/pspdfkit/internal/ls;->f:[Lcom/pspdfkit/internal/ls;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/ls;
    .locals 1

    const-class v0, Lcom/pspdfkit/internal/ls;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ls;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/ls;
    .locals 1

    sget-object v0, Lcom/pspdfkit/internal/ls;->f:[Lcom/pspdfkit/internal/ls;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/ls;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/mn;
.super Lcom/pspdfkit/internal/jni/NativePointsPager;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/wj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/wj<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativePointsPager;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/wj;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/wj;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/mn;->a:Lcom/pspdfkit/internal/wj;

    return-void
.end method


# virtual methods
.method public final get(II)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mn;->a:Lcom/pspdfkit/internal/wj;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/wj;->a(II)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public final size()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mn;->a:Lcom/pspdfkit/internal/wj;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wj;->a()I

    move-result v0

    return v0
.end method

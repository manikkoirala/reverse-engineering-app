.class public final Lcom/pspdfkit/internal/xm$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/xm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/widget/FrameLayout;

.field public final b:Lcom/pspdfkit/internal/views/document/DocumentView;

.field private c:Landroid/view/View;

.field private d:Lcom/pspdfkit/ui/PdfPasswordView;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/xm$b;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/xm$b;->c:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/xm$b;)Lcom/pspdfkit/ui/PdfPasswordView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/xm$b;->d:Lcom/pspdfkit/ui/PdfPasswordView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputc(Lcom/pspdfkit/internal/xm$b;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/xm$b;->c:Landroid/view/View;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/xm$b;Lcom/pspdfkit/ui/PdfPasswordView;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/xm$b;->d:Lcom/pspdfkit/ui/PdfPasswordView;

    return-void
.end method

.method constructor <init>(Landroid/widget/FrameLayout;Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/xm$b;->a:Landroid/widget/FrameLayout;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/xm$b;->b:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-void
.end method

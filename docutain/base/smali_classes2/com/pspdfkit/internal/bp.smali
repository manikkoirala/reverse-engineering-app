.class final Lcom/pspdfkit/internal/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ap;


# instance fields
.field private final b:Lcom/pspdfkit/internal/ui/c;

.field private c:Lcom/pspdfkit/document/PdfDocument;

.field private final d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/pspdfkit/ui/DocumentCoordinator;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ui/c;)V
    .locals 1

    const-string v0, "pdfActivityUserInterfaceCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    .line 4
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bp;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/bp;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final b(Lcom/pspdfkit/internal/bp;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 3
    iget-object p0, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p0, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/ui/e;)V
    .locals 1

    const-string v0, "documentCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->f:Lcom/pspdfkit/ui/DocumentCoordinator;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/DocumentCoordinator;->removeOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V

    .line 3
    :cond_0
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ui/e;->addOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/bp;->f:Lcom/pspdfkit/ui/DocumentCoordinator;

    return-void
.end method

.method public final g()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->f:Lcom/pspdfkit/ui/DocumentCoordinator;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/ui/DocumentCoordinator;->removeOnDocumentVisibleListener(Lcom/pspdfkit/ui/DocumentCoordinator$OnDocumentVisibleListener;)V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->c:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    :cond_1
    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/bp;->c:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final k()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/annotations/RedactionAnnotation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    :goto_0
    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Lkotlin/jvm/internal/TypeIntrinsics;->asMutableCollection(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    :goto_0
    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldOrder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newOrder"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 5

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->c:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 5
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x7d0

    if-le v0, v2, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v3, "PSPDFKit"

    const-string v4, "Only checking first 2000 pages for redactions."

    .line 6
    invoke-static {v3, v4, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v4

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v0, v3, v1, v2}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 10
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 11
    new-instance v1, Lcom/pspdfkit/internal/bp$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/bp$a;-><init>(Lcom/pspdfkit/internal/bp;)V

    sget-object v2, Lcom/pspdfkit/internal/bp$b;->a:Lcom/pspdfkit/internal/bp$b;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/bp;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    .line 23
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/annotations/AnnotationProvider;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/internal/bp;->c:Lcom/pspdfkit/document/PdfDocument;

    return-void
.end method

.method public final onDocumentVisible(Lcom/pspdfkit/ui/DocumentDescriptor;)V
    .locals 1

    const-string v0, "documentDescriptor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->u(Z)V

    goto :goto_0

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/bp;->b:Lcom/pspdfkit/internal/ui/c;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ui/c;->e(Z)V

    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/nl;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/ol;)Lcom/pspdfkit/document/OutlineElement;
    .locals 6

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 12
    :cond_0
    new-instance v2, Lcom/pspdfkit/document/OutlineElement$Builder;

    invoke-direct {v2, v0}, Lcom/pspdfkit/document/OutlineElement$Builder;-><init>(Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->e()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/document/OutlineElement$Builder;->setExpanded(Z)Lcom/pspdfkit/document/OutlineElement$Builder;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->c()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/n5;->a()J

    move-result-wide v3

    long-to-int v0, v3

    goto :goto_0

    :cond_1
    const/high16 v0, -0x1000000

    :goto_0
    invoke-virtual {v2, v0}, Lcom/pspdfkit/document/OutlineElement$Builder;->setColor(I)Lcom/pspdfkit/document/OutlineElement$Builder;

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->d()Z

    move-result v0

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    .line 19
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    .line 21
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 26
    :goto_1
    invoke-virtual {v2, v0}, Lcom/pspdfkit/document/OutlineElement$Builder;->setStyle(I)Lcom/pspdfkit/document/OutlineElement$Builder;

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->a()Lcom/pspdfkit/internal/b;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 30
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->a()Lcom/pspdfkit/internal/b;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v1

    .line 31
    invoke-virtual {v2, v1}, Lcom/pspdfkit/document/OutlineElement$Builder;->setAction(Lcom/pspdfkit/annotations/actions/Action;)Lcom/pspdfkit/document/OutlineElement$Builder;

    :cond_5
    if-eqz v1, :cond_6

    .line 35
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v0

    sget-object v4, Lcom/pspdfkit/annotations/actions/ActionType;->GOTO:Lcom/pspdfkit/annotations/actions/ActionType;

    if-ne v0, v4, :cond_6

    instance-of v0, v1, Lcom/pspdfkit/annotations/actions/GoToAction;

    if-eqz v0, :cond_6

    .line 36
    check-cast v1, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/actions/GoToAction;->getPageIndex()I

    move-result v0

    invoke-virtual {p0, v0, v3}, Lcom/pspdfkit/internal/zf;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/document/OutlineElement$Builder;->setPageLabel(Ljava/lang/String;)Lcom/pspdfkit/document/OutlineElement$Builder;

    .line 40
    :cond_6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->b()I

    move-result v0

    if-nez v0, :cond_7

    .line 41
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_3

    .line 43
    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->b()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 44
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->b()I

    move-result v1

    :goto_2
    if-ge v3, v1, :cond_9

    .line 45
    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/ol;->f(I)Lcom/pspdfkit/internal/ol;

    move-result-object v4

    const-string v5, "element.children(i)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v4}, Lcom/pspdfkit/internal/nl;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/ol;)Lcom/pspdfkit/document/OutlineElement;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 47
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    move-object p0, v0

    .line 51
    :goto_3
    invoke-virtual {v2, p0}, Lcom/pspdfkit/document/OutlineElement$Builder;->setChildren(Ljava/util/List;)Lcom/pspdfkit/document/OutlineElement$Builder;

    .line 53
    invoke-virtual {v2}, Lcom/pspdfkit/document/OutlineElement$Builder;->build()Lcom/pspdfkit/document/OutlineElement;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/zf;[B)Ljava/util/ArrayList;
    .locals 5

    const-string v0, "document"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_3

    .line 2
    array-length v1, p1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_2

    .line 4
    :cond_1
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/ol;->a(Ljava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ol;

    move-result-object p1

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->b()I

    move-result v1

    if-lez v1, :cond_3

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ol;->b()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_3

    .line 8
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/ol;->f(I)Lcom/pspdfkit/internal/ol;

    move-result-object v3

    const-string v4, "coreOutlineElement.children(i)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v3}, Lcom/pspdfkit/internal/nl;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/ol;)Lcom/pspdfkit/document/OutlineElement;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 10
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/x6;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 23
    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditing:[I

    .line 24
    sget v2, Lcom/pspdfkit/R$attr;->pspdf__contentEditingStyle:I

    .line 25
    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_ContentEditing:I

    const/4 v4, 0x0

    .line 26
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.theme\n          \u2026tentEditing\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditing_pspdf__textBlockFrameColor:I

    .line 34
    sget v2, Lcom/pspdfkit/R$attr;->colorPrimary:I

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result v2

    .line 35
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/x6;->a:I

    .line 40
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditing_pspdf__editedTextBlockFrameColor:I

    .line 41
    sget v3, Lcom/pspdfkit/R$attr;->colorPrimary:I

    invoke-static {p1, v3}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result p1

    .line 42
    invoke-virtual {v0, v2, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/x6;->c:I

    .line 47
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditing_pspdf__textBlockFrameColorInvertedMode:I

    .line 48
    invoke-static {v1}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v1

    .line 49
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/x6;->b:I

    .line 54
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__ContentEditing_pspdf__editedTextBlockFrameColorInvertedMode:I

    .line 55
    invoke-static {p1}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result p1

    .line 56
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/x6;->d:I

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/x6;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/x6;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/x6;->c:I

    return v0
.end method

.method public final d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/x6;->d:I

    return v0
.end method

.class public final Lcom/pspdfkit/internal/os;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;


# instance fields
.field private final b:Lcom/pspdfkit/internal/ui/a;

.field private final c:Lcom/pspdfkit/internal/a1;

.field private d:Lcom/pspdfkit/internal/em;

.field private e:Lcom/pspdfkit/internal/em;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/a1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/ce;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/ui/a;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/os;->b:Lcom/pspdfkit/internal/ui/a;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/os;->c:Lcom/pspdfkit/internal/a1;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 319
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Exiting special mode."

    .line 320
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    .line 323
    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->d()V

    :cond_0
    const/4 v0, 0x0

    .line 326
    iput-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_1

    .line 302
    instance-of v1, v0, Lcom/pspdfkit/internal/zt;

    if-eqz v1, :cond_0

    .line 303
    check-cast v0, Lcom/pspdfkit/internal/zt;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zt;->a(Lcom/pspdfkit/datastructures/TextSelection;)V

    return-void

    .line 306
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->d()Z

    .line 310
    :cond_1
    new-instance v0, Lcom/pspdfkit/internal/zt;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/zt;-><init>(Lcom/pspdfkit/datastructures/TextSelection;Lcom/pspdfkit/internal/specialMode/handler/e;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    .line 311
    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/em;->a(Lcom/pspdfkit/internal/os;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;)V
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_0

    .line 313
    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->d()Z

    .line 316
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/k6;

    .line 317
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/pspdfkit/internal/k6;-><init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    .line 318
    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/os;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Entering annotation creation mode for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.PdfView"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_2

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->a()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->a()V

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    check-cast v0, Lcom/pspdfkit/internal/k1;

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    .line 8
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    check-cast v0, Lcom/pspdfkit/internal/k1;

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/internal/k1;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    .line 11
    invoke-virtual {v0, p3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->d()V

    .line 18
    :cond_2
    :goto_0
    sget-object v0, Lcom/pspdfkit/internal/os$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 107
    :pswitch_0
    new-instance p2, Lcom/pspdfkit/internal/ak;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/ak;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 108
    :pswitch_1
    new-instance p2, Lcom/pspdfkit/internal/hi;

    sget-object v0, Lcom/pspdfkit/internal/ds$a;->a:Lcom/pspdfkit/internal/ds$a;

    invoke-direct {p2, p1, p3, v0}, Lcom/pspdfkit/internal/hi;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/ds$a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 109
    :pswitch_2
    new-instance p2, Lcom/pspdfkit/internal/hi;

    sget-object v0, Lcom/pspdfkit/internal/ds$a;->b:Lcom/pspdfkit/internal/ds$a;

    invoke-direct {p2, p1, p3, v0}, Lcom/pspdfkit/internal/hi;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/ds$a;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 110
    :pswitch_3
    new-instance p2, Lcom/pspdfkit/internal/gi;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/gi;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 111
    :pswitch_4
    new-instance p2, Lcom/pspdfkit/internal/qi;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/qi;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 112
    :pswitch_5
    new-instance p2, Lcom/pspdfkit/internal/ki;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/ki;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 113
    :pswitch_6
    new-instance p2, Lcom/pspdfkit/internal/jf;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/jf;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 114
    :pswitch_7
    new-instance p2, Lcom/pspdfkit/internal/df;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/df;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 115
    :pswitch_8
    new-instance p2, Lcom/pspdfkit/internal/js;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/js;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 116
    :pswitch_9
    new-instance p2, Lcom/pspdfkit/internal/to;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/to;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 117
    :pswitch_a
    new-instance p2, Lcom/pspdfkit/internal/nr;

    iget-object v0, p0, Lcom/pspdfkit/internal/os;->c:Lcom/pspdfkit/internal/a1;

    invoke-direct {p2, p1, p3, v0}, Lcom/pspdfkit/internal/nr;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/internal/a1;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 118
    :pswitch_b
    new-instance p2, Lcom/pspdfkit/internal/qn;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/qn;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 119
    :pswitch_c
    new-instance p2, Lcom/pspdfkit/internal/nn;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/nn;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 120
    :pswitch_d
    new-instance p2, Lcom/pspdfkit/internal/i5;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/i5;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 121
    :pswitch_e
    new-instance p2, Lcom/pspdfkit/internal/ps;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/ps;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 122
    :pswitch_f
    new-instance p2, Lcom/pspdfkit/internal/fh;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/fh;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 123
    :pswitch_10
    new-instance p2, Lcom/pspdfkit/internal/e5;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/e5;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 124
    :pswitch_11
    new-instance p2, Lcom/pspdfkit/internal/uc;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/uc;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 125
    :pswitch_12
    new-instance p2, Lcom/pspdfkit/internal/us;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/us;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 126
    :pswitch_13
    new-instance v0, Lcom/pspdfkit/internal/pc;

    invoke-direct {v0, p1, p2, p3}, Lcom/pspdfkit/internal/pc;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 127
    :pswitch_14
    new-instance p2, Lcom/pspdfkit/internal/et;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/et;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 128
    :pswitch_15
    new-instance p2, Lcom/pspdfkit/internal/ru;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/ru;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 129
    :pswitch_16
    new-instance p2, Lcom/pspdfkit/internal/qs;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/qs;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto/16 :goto_1

    .line 130
    :pswitch_17
    new-instance p2, Lcom/pspdfkit/internal/qd;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/qd;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto :goto_1

    .line 131
    :pswitch_18
    new-instance p2, Lcom/pspdfkit/internal/ek;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/ek;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto :goto_1

    .line 132
    :pswitch_19
    new-instance p2, Lcom/pspdfkit/internal/ya;

    const-string p3, "handler"

    .line 133
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 157
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 158
    new-instance v1, Lcom/pspdfkit/internal/ab;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/ab;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ab;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x1

    .line 159
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 160
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 162
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v1

    .line 163
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v2

    .line 164
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ga;->a(ZZ)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 165
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p3

    .line 204
    new-instance v1, Lcom/pspdfkit/internal/views/annotations/n;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object v2

    .line 205
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p3, v4}, Lcom/pspdfkit/internal/views/annotations/n;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/views/document/DocumentView;)V

    .line 206
    invoke-direct {p2, p1, v0, v1}, Lcom/pspdfkit/internal/ya;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Landroid/graphics/Paint;Lcom/pspdfkit/internal/views/annotations/n;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto :goto_1

    .line 207
    :pswitch_1a
    new-instance p2, Lcom/pspdfkit/internal/th;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/th;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    goto :goto_1

    .line 208
    :pswitch_1b
    new-instance p2, Lcom/pspdfkit/internal/fe;

    invoke-direct {p2, p1, p3}, Lcom/pspdfkit/internal/fe;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    .line 299
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/em;->a(Lcom/pspdfkit/internal/os;)V

    .line 300
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->d()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/os;->e:Lcom/pspdfkit/internal/em;

    return-void
.end method

.method public final d()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->d()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/em;->a(Landroid/graphics/Matrix;)V

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->e:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/em;->a(Landroid/graphics/Matrix;)V

    .line 11
    :cond_2
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public getCurrentMode()Lcom/pspdfkit/internal/em;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    return-object v0
.end method

.method public getParentView()Lcom/pspdfkit/internal/dm;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/dm;

    return-object v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/em;->a(Landroid/graphics/Canvas;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->e:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_1

    .line 8
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/em;->a(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/os;->b:Lcom/pspdfkit/internal/ui/a;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/a;->f()V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/em;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final recycle()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.PdfView"

    const-string v2, "Exiting special mode."

    .line 2
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/internal/em;->h()V

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/pspdfkit/internal/os;->d:Lcom/pspdfkit/internal/em;

    :cond_0
    return-void
.end method

.method public setPageModeHandlerViewHolder(Lcom/pspdfkit/internal/em;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/os;->e:Lcom/pspdfkit/internal/em;

    return-void
.end method

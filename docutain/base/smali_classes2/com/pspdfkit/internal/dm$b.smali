.class final Lcom/pspdfkit/internal/dm$b;
.super Lcom/pspdfkit/internal/as;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/dm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/dm;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/dm;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/dm$b;->a:Lcom/pspdfkit/internal/dm;

    invoke-direct {p0}, Lcom/pspdfkit/internal/as;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/dm$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm$b;-><init>(Lcom/pspdfkit/internal/dm;)V

    return-void
.end method


# virtual methods
.method public final d(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/dm$b;->a:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->b()Z

    move-result p1

    return p1
.end method

.method public final h(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/dm$b;->a:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getFormEditor()Lcom/pspdfkit/internal/tb;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/tb;->e()Lcom/pspdfkit/forms/FormElement;

    move-result-object p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/dm$b;->a:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->h()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

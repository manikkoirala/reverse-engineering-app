.class public final Lcom/pspdfkit/internal/nm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/pspdfkit/internal/nm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private d:Lcom/pspdfkit/annotations/Annotation;


# direct methods
.method public static synthetic $r8$lambda$kqlssBmzorRW1V-mtWSq9Gfu2ow(Lcom/pspdfkit/internal/nm;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/nm;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/nm$a;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nm$a;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/nm;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/nm;->a:I

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/nm;->c:I

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/nm;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/nm;->d:Lcom/pspdfkit/annotations/Annotation;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/nm;->a:I

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/nm;->b:Ljava/lang/String;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/nm;->c:I

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/nm;->d:Lcom/pspdfkit/annotations/Annotation;

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/zf;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/nm;->d:Lcom/pspdfkit/annotations/Annotation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/nm;->d:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {p1}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/nm;->a:I

    iget-object v2, p0, Lcom/pspdfkit/internal/nm;->b:Ljava/lang/String;

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/r1;->a(ILjava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    iget v1, p0, Lcom/pspdfkit/internal/nm;->a:I

    iget v2, p0, Lcom/pspdfkit/internal/nm;->c:I

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, v1, v2}, Lcom/pspdfkit/internal/r1;->getAnnotationAsync(II)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Maybe;->switchIfEmpty(Lio/reactivex/rxjava3/core/MaybeSource;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/nm$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/nm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/nm;)V

    .line 12
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Maybe;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/nm;->d:Lcom/pspdfkit/annotations/Annotation;

    if-eq p1, v0, :cond_1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/nm;->a:I

    if-ne v0, v1, :cond_0

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getUuid()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/nm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    iget p2, p0, Lcom/pspdfkit/internal/nm;->a:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2
    iget p2, p0, Lcom/pspdfkit/internal/nm;->c:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/nm;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/di;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/graphics/PointF;Landroid/graphics/RectF;)F
    .locals 4

    .line 28
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 29
    iget v1, p0, Landroid/graphics/PointF;->y:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 31
    iget v2, p0, Landroid/graphics/PointF;->x:F

    iget v3, p0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result p0

    return p0

    .line 33
    :cond_0
    iget v2, p0, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_2

    iget v3, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    goto :goto_0

    :cond_1
    return v1

    .line 34
    :cond_2
    :goto_0
    iget p0, p0, Landroid/graphics/PointF;->y:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, p0, v2

    if-lez v2, :cond_3

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float p0, p0, p1

    if-gez p0, :cond_3

    return v0

    :cond_3
    mul-float v0, v0, v0

    mul-float v1, v1, v1

    add-float/2addr v1, v0

    float-to-double p0, v1

    .line 37
    invoke-static {p0, p1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p0

    double-to-float p0, p0

    return p0
.end method

.method public static varargs a([F)F
    .locals 5

    .line 42
    array-length v0, p0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget v3, p0, v2

    cmpl-float v4, v3, v1

    if-lez v4, :cond_0

    move v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static a(I)I
    .locals 1

    .line 44
    invoke-static {p0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    if-eq v0, p0, :cond_0

    mul-int/lit8 p0, v0, 0x2

    :cond_0
    return p0
.end method

.method public static varargs a([I)I
    .locals 4

    .line 43
    array-length v0, p0

    const/high16 v1, -0x80000000

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget v3, p0, v2

    if-le v3, v1, :cond_0

    move v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static a(FFFFFFFF)Landroid/graphics/PointF;
    .locals 7

    .line 38
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sub-float v1, p0, p2

    sub-float v2, p5, p7

    mul-float v3, v1, v2

    sub-float v4, p1, p3

    sub-float v5, p4, p6

    mul-float v6, v4, v5

    sub-float/2addr v3, v6

    mul-float p0, p0, p3

    mul-float p1, p1, p2

    sub-float/2addr p0, p1

    mul-float v5, v5, p0

    mul-float p4, p4, p7

    mul-float p5, p5, p6

    sub-float/2addr p4, p5

    mul-float v1, v1, p4

    sub-float/2addr v5, v1

    div-float/2addr v5, v3

    .line 40
    iput v5, v0, Landroid/graphics/PointF;->x:F

    mul-float p0, p0, v2

    mul-float v4, v4, p4

    sub-float/2addr p0, v4

    div-float/2addr p0, v3

    .line 41
    iput p0, v0, Landroid/graphics/PointF;->y:F

    return-object v0
.end method

.method public static a(Ljava/util/List;)Landroid/graphics/RectF;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;)",
            "Landroid/graphics/RectF;"
        }
    .end annotation

    .line 2
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 4
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v1, 0x1

    .line 5
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 6
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 8
    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->right:F

    cmpg-float v5, v3, v4

    if-gez v5, :cond_4

    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v6, v5, v2

    if-gez v6, :cond_4

    .line 9
    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    cmpg-float v8, v6, v7

    if-gez v8, :cond_3

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    iget v9, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v10, v8, v9

    if-gez v10, :cond_3

    cmpl-float v6, v6, v3

    if-lez v6, :cond_0

    .line 11
    iput v3, v0, Landroid/graphics/RectF;->left:F

    :cond_0
    cmpg-float v3, v9, v2

    if-gez v3, :cond_1

    .line 14
    iput v2, v0, Landroid/graphics/RectF;->top:F

    :cond_1
    cmpg-float v2, v7, v4

    if-gez v2, :cond_2

    .line 17
    iput v4, v0, Landroid/graphics/RectF;->right:F

    :cond_2
    cmpl-float v2, v8, v5

    if-lez v2, :cond_4

    .line 20
    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_1

    .line 23
    :cond_3
    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 24
    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 25
    iput v4, v0, Landroid/graphics/RectF;->right:F

    .line 26
    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    :cond_4
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    return-object v0

    .line 27
    :cond_6
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "List of rects may not be empty."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/pspdfkit/utils/Size;F)Ljava/util/ArrayList;
    .locals 8

    float-to-double v0, p1

    .line 67
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 70
    iget p1, p0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v2, p1

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    .line 71
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget p1, p0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v6, p1

    div-double/2addr v6, v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 74
    iget p1, p0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v6, p1

    div-double/2addr v6, v4

    iget p0, p0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double p0, p0

    div-double/2addr p0, v4

    invoke-static {v6, v7, p0, p1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    sub-double v4, v0, v4

    add-double/2addr v4, p0

    .line 78
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double v6, v6, v2

    double-to-float v6, v6

    .line 80
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double v4, v4, v2

    double-to-float v4, v4

    sub-double/2addr v0, p0

    .line 82
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide p0

    mul-double p0, p0, v2

    double-to-float p0, p0

    .line 83
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double v0, v0, v2

    double-to-float p1, v0

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 86
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v6, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 87
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 90
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v1, Landroid/graphics/PointF;

    neg-float v2, v6

    neg-float v3, v4

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 93
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v1, Landroid/graphics/PointF;

    neg-float p0, p0

    neg-float p1, p1

    invoke-direct {v1, p0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 96
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static a(FF)Z
    .locals 0

    sub-float/2addr p0, p1

    .line 1
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result p0

    const p1, 0x322bcc77    # 1.0E-8f

    cmpg-float p0, p0, p1

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static a(Landroid/graphics/PointF;Ljava/util/ArrayList;)Z
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    .line 45
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    float-to-double v3, v3

    .line 46
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    .line 47
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    float-to-double v7, v7

    .line 48
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    float-to-double v9, v9

    const/4 v11, 0x1

    const/4 v12, 0x1

    .line 49
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v12, v13, :cond_0

    .line 50
    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/PointF;

    .line 51
    iget v14, v13, Landroid/graphics/PointF;->x:F

    float-to-double v14, v14

    invoke-static {v14, v15, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v3

    .line 52
    iget v14, v13, Landroid/graphics/PointF;->x:F

    float-to-double v14, v14

    invoke-static {v14, v15, v5, v6}, Ljava/lang/Math;->max(DD)D

    move-result-wide v5

    .line 53
    iget v14, v13, Landroid/graphics/PointF;->y:F

    float-to-double v14, v14

    invoke-static {v14, v15, v7, v8}, Ljava/lang/Math;->min(DD)D

    move-result-wide v7

    .line 54
    iget v13, v13, Landroid/graphics/PointF;->y:F

    float-to-double v13, v13

    invoke-static {v13, v14, v9, v10}, Ljava/lang/Math;->max(DD)D

    move-result-wide v9

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 57
    :cond_0
    iget v12, v0, Landroid/graphics/PointF;->x:F

    float-to-double v12, v12

    cmpg-double v14, v12, v3

    if-ltz v14, :cond_6

    cmpl-double v3, v12, v5

    if-gtz v3, :cond_6

    iget v3, v0, Landroid/graphics/PointF;->y:F

    float-to-double v3, v3

    cmpg-double v5, v3, v7

    if-ltz v5, :cond_6

    cmpl-double v5, v3, v9

    if-lez v5, :cond_1

    goto :goto_4

    .line 63
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v11

    move v4, v3

    const/4 v3, 0x0

    const/4 v5, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_5

    .line 64
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 65
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 66
    iget v7, v6, Landroid/graphics/PointF;->y:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v9, v7, v8

    if-lez v9, :cond_2

    const/4 v9, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    :goto_2
    iget v10, v4, Landroid/graphics/PointF;->y:F

    cmpl-float v12, v10, v8

    if-lez v12, :cond_3

    const/4 v12, 0x1

    goto :goto_3

    :cond_3
    const/4 v12, 0x0

    :goto_3
    if-eq v9, v12, :cond_4

    iget v9, v0, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v6

    sub-float/2addr v8, v7

    mul-float v8, v8, v4

    sub-float/2addr v10, v7

    div-float/2addr v8, v10

    add-float/2addr v8, v6

    cmpg-float v4, v9, v8

    if-gez v4, :cond_4

    xor-int/lit8 v5, v5, 0x1

    :cond_4
    add-int/lit8 v4, v3, 0x1

    move/from16 v16, v4

    move v4, v3

    move/from16 v3, v16

    goto :goto_1

    :cond_5
    return v5

    :cond_6
    :goto_4
    return v2
.end method

.method public static varargs b([F)F
    .locals 5

    .line 1
    array-length v0, p0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget v3, p0, v2

    cmpg-float v4, v3, v1

    if-gez v4, :cond_0

    move v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static b(Lcom/pspdfkit/utils/Size;F)Lcom/pspdfkit/utils/Size;
    .locals 8

    float-to-double v0, p1

    .line 2
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 3
    iget p1, p0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v2, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double v4, v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    iget p1, p0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v4, p1

    .line 4
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double v6, v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    add-double/2addr v4, v2

    double-to-float p1, v4

    .line 5
    iget v2, p0, Lcom/pspdfkit/utils/Size;->width:F

    float-to-double v2, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double v4, v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    iget p0, p0, Lcom/pspdfkit/utils/Size;->height:F

    float-to-double v4, p0

    .line 6
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double v0, v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    double-to-float p0, v0

    .line 8
    new-instance v0, Lcom/pspdfkit/utils/Size;

    invoke-direct {v0, p1, p0}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object v0
.end method

.method public static b(FF)Z
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    cmpg-float p0, p0, p1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

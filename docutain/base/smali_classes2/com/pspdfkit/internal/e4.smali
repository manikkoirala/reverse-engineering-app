.class public abstract Lcom/pspdfkit/internal/e4;
.super Lcom/pspdfkit/internal/hv;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/internal/c4;",
        ">",
        "Lcom/pspdfkit/internal/hv<",
        "TT;>;",
        "Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/hv;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method


# virtual methods
.method protected final a(FF)V
    .locals 0

    .line 3
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/i4;->a(FF)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez p1, :cond_0

    return-void

    .line 5
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/c4;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/c4;->b(Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/i4;->a(Lcom/pspdfkit/internal/os;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/internal/i4;->b()Z

    const/4 v0, 0x0

    return v0
.end method

.method protected final f()Lcom/pspdfkit/internal/b2;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e4;->t()Lcom/pspdfkit/internal/c4;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    check-cast v0, Lcom/pspdfkit/internal/c4;

    sget-object v1, Lcom/pspdfkit/internal/br$a;->a:Lcom/pspdfkit/internal/br$a;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/c4;->a(Lcom/pspdfkit/internal/br$a;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    check-cast v0, Lcom/pspdfkit/internal/c4;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    invoke-super {p0}, Lcom/pspdfkit/internal/i4;->h()V

    return-void
.end method

.method protected final m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    check-cast v0, Lcom/pspdfkit/internal/c4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c4;->f()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez v0, :cond_1

    goto :goto_0

    .line 5
    :cond_1
    check-cast v0, Lcom/pspdfkit/internal/c4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c4;->e()V

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->i()V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g()V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    .line 12
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    :cond_2
    return-void
.end method

.method protected final n()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    check-cast v0, Lcom/pspdfkit/internal/c4;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/c4;->b(Z)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->i()V

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->p()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->f()Lcom/pspdfkit/internal/uh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->a()V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getParentView()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 8
    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/b2;->a(Z)Z

    :cond_1
    return-void
.end method

.method public final onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 10

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez p1, :cond_0

    return-void

    .line 2
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/internal/c4;

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v4

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v5

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v6

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object v7

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v8

    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v9

    .line 13
    invoke-virtual/range {v0 .. v9}, Lcom/pspdfkit/internal/c4;->a(IIFLcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;FLandroidx/core/util/Pair;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_2

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    if-nez p1, :cond_1

    goto :goto_0

    .line 15
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/c4;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/c4;->e()V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->i()V

    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    :cond_2
    :goto_0
    return-void
.end method

.method protected abstract t()Lcom/pspdfkit/internal/c4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

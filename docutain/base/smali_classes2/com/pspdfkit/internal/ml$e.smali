.class final Lcom/pspdfkit/internal/ml$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/hj$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/ml;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation


# instance fields
.field final a:Lcom/pspdfkit/document/OutlineElement;

.field b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ml$e;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private final e:Lcom/pspdfkit/internal/ml$e;


# direct methods
.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/ml$e;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ml$e;->c:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/ml$e;)Lcom/pspdfkit/internal/ml$e;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/ml$e;I)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/ml$e;->d:I

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/document/OutlineElement;ILcom/pspdfkit/internal/ml$e;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    .line 9
    iput p2, p0, Lcom/pspdfkit/internal/ml$e;->b:I

    .line 10
    new-instance p2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/pspdfkit/document/OutlineElement;->getChildren()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ml$e;->c:Ljava/util/List;

    .line 11
    iput-object p3, p0, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/ml$e;->b()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/document/OutlineElement;ILcom/pspdfkit/internal/ml$e;Lcom/pspdfkit/internal/ml$e-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/document/OutlineElement;ILcom/pspdfkit/internal/ml$e;)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/ml$e;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    iput-object v0, p0, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    .line 3
    iget v0, p1, Lcom/pspdfkit/internal/ml$e;->b:I

    iput v0, p0, Lcom/pspdfkit/internal/ml$e;->b:I

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ml$e;->getChildren()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ml$e;->c:Ljava/util/List;

    .line 5
    iget-object p1, p1, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    iput-object p1, p0, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    const/4 p1, 0x0

    .line 6
    iput p1, p0, Lcom/pspdfkit/internal/ml$e;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/ml$e;Lcom/pspdfkit/internal/ml$e-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/internal/ml$e;)V

    return-void
.end method

.method private b()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v0}, Lcom/pspdfkit/document/OutlineElement;->getChildren()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/document/OutlineElement;

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/ml$e;->c:Ljava/util/List;

    new-instance v3, Lcom/pspdfkit/internal/ml$e;

    iget v4, p0, Lcom/pspdfkit/internal/ml$e;->b:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v3, v1, v4, p0}, Lcom/pspdfkit/internal/ml$e;-><init>(Lcom/pspdfkit/document/OutlineElement;ILcom/pspdfkit/internal/ml$e;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/ml$e;->d:I

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ml$e;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/pspdfkit/internal/ml$e;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/ml$e;

    .line 5
    iget v1, p0, Lcom/pspdfkit/internal/ml$e;->b:I

    iget v3, p1, Lcom/pspdfkit/internal/ml$e;->b:I

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    iget-object v3, p1, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    .line 6
    invoke-virtual {v1, v3}, Lcom/pspdfkit/document/OutlineElement;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    if-eqz v1, :cond_2

    iget-object p1, p1, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    .line 7
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ml$e;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_2
    iget-object p1, p1, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ml$e;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ml$e;->c:Ljava/util/List;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ml$e;->b:I

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ml$e;->a:Lcom/pspdfkit/document/OutlineElement;

    invoke-virtual {v1}, Lcom/pspdfkit/document/OutlineElement;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ml$e;->e:Lcom/pspdfkit/internal/ml$e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ml$e;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    return v1
.end method

.class public final Lcom/pspdfkit/internal/eh;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field private static final n:Landroid/graphics/Matrix;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

.field private final f:Lcom/pspdfkit/annotations/LineEndType;

.field private final g:Lcom/pspdfkit/annotations/LineEndType;

.field private final h:Lcom/pspdfkit/internal/ih;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Path;

.field private final k:I

.field private final l:Landroid/graphics/Paint;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/eh;->n:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/eh;->a:Landroid/content/Context;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/eh;->c:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/eh;->d:F

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/eh;->e:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/eh;->f:Lcom/pspdfkit/annotations/LineEndType;

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/internal/eh;->g:Lcom/pspdfkit/annotations/LineEndType;

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/h4;->i()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    .line 10
    new-instance v0, Lcom/pspdfkit/internal/ih;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ih;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    .line 11
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/h4;->a(I)V

    .line 12
    invoke-virtual {v0, p3}, Lcom/pspdfkit/internal/h4;->b(F)V

    .line 13
    new-instance p3, Landroidx/core/util/Pair;

    invoke-direct {p3, p5, p6}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p3}, Lcom/pspdfkit/internal/sn;->a(Landroidx/core/util/Pair;)V

    .line 14
    invoke-virtual {v0, p4}, Lcom/pspdfkit/internal/y4;->a(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    const/high16 p3, 0x3f800000    # 1.0f

    .line 17
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;F)F

    move-result p3

    .line 18
    invoke-virtual {v0, p3}, Lcom/pspdfkit/internal/y4;->c(F)V

    .line 20
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/pspdfkit/internal/eh;->j:Landroid/graphics/Path;

    const/16 p3, 0x8

    .line 22
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p3

    iput p3, p0, Lcom/pspdfkit/internal/eh;->b:I

    const/4 p3, 0x2

    .line 23
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/eh;->k:I

    .line 25
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/eh;->i:Landroid/graphics/Paint;

    const/4 p3, 0x1

    .line 26
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 27
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/eh;->m:Z

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->a:Landroid/content/Context;

    iget v1, p0, Lcom/pspdfkit/internal/eh;->c:I

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/s5;->a(Landroid/content/Context;I)I

    move-result v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/h4;->a(I)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->j:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/pspdfkit/internal/eh;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    iget v1, p0, Lcom/pspdfkit/internal/eh;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h4;->a(I)V

    .line 10
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->e:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/BorderStyle;->NONE:Lcom/pspdfkit/annotations/BorderStyle;

    if-ne v0, v1, :cond_1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/pspdfkit/internal/eh;->d:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 13
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 14
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 15
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/pspdfkit/internal/eh;->k:I

    mul-int/lit8 v3, v3, 0x8

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    const-wide v3, 0x3fe921fb54442d18L    # 0.7853981633974483

    .line 17
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    float-to-double v5, v2

    mul-double v3, v3, v5

    double-to-float v2, v3

    sub-float v4, v0, v2

    sub-float v5, v1, v2

    add-float v6, v0, v2

    add-float v7, v1, v2

    .line 18
    iget-object v8, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 25
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    sget-object v1, Lcom/pspdfkit/internal/eh;->n:Landroid/graphics/Matrix;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->l:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/h4;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    :goto_1
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public final isStateful()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 8

    .line 1
    new-instance v7, Lcom/pspdfkit/internal/eh;

    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->a:Landroid/content/Context;

    iget v2, p0, Lcom/pspdfkit/internal/eh;->c:I

    iget v3, p0, Lcom/pspdfkit/internal/eh;->d:F

    iget-object v4, p0, Lcom/pspdfkit/internal/eh;->e:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    iget-object v5, p0, Lcom/pspdfkit/internal/eh;->f:Lcom/pspdfkit/annotations/LineEndType;

    iget-object v6, p0, Lcom/pspdfkit/internal/eh;->g:Lcom/pspdfkit/annotations/LineEndType;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/pspdfkit/internal/eh;-><init>(Landroid/content/Context;IFLcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    return-object v7
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->e:Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/BorderEffect;->CLOUDY:Lcom/pspdfkit/annotations/BorderEffect;

    if-ne v1, v2, :cond_1

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y4;->q()F

    move-result v1

    const/high16 v2, 0x40880000    # 4.25f

    mul-float v1, v1, v2

    add-float/2addr v0, v1

    .line 10
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/eh;->h:Lcom/pspdfkit/internal/ih;

    iget v2, p0, Lcom/pspdfkit/internal/eh;->b:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/pspdfkit/internal/eh;->b:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/PointF;

    .line 11
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x0

    aput-object v5, v4, v2

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v0, 0x1

    aput-object v2, v4, v0

    .line 12
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/d4;->b(Ljava/util/List;)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/eh;->j:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/pspdfkit/internal/eh;->k:I

    int-to-float v2, v2

    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/pspdfkit/internal/eh;->k:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    iget v4, p0, Lcom/pspdfkit/internal/eh;->k:I

    sub-int/2addr p1, v4

    int-to-float p1, p1

    invoke-direct {v1, v2, v2, v3, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object p1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    const/high16 v2, 0x40800000    # 4.0f

    .line 20
    invoke-virtual {v0, v1, v2, v2, p1}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/eh;->j:Landroid/graphics/Path;

    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 32
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_2
    :goto_0
    return-void
.end method

.method protected final onStateChange([I)Z
    .locals 6

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_1

    aget v4, p1, v2

    const v5, 0x10100a1

    if-ne v4, v5, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 7
    :goto_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/eh;->m:Z

    if-eq p1, v0, :cond_2

    const/4 v1, 0x1

    .line 8
    :cond_2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/eh;->m:Z

    return v1
.end method

.method public final setAlpha(I)V
    .locals 0

    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

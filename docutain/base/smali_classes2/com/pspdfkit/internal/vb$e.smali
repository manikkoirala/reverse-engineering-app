.class final Lcom/pspdfkit/internal/vb$e;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/vb;->a(Lcom/pspdfkit/forms/CheckBoxFormElement;)Lio/reactivex/rxjava3/core/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/pspdfkit/forms/FormElement;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/forms/CheckBoxFormElement;


# direct methods
.method constructor <init>(Lcom/pspdfkit/forms/CheckBoxFormElement;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/vb$e;->a:Lcom/pspdfkit/forms/CheckBoxFormElement;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/FormElement;

    const-string v0, "$this$executeAsync"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    iget-object p1, p0, Lcom/pspdfkit/internal/vb$e;->a:Lcom/pspdfkit/forms/CheckBoxFormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/EditableButtonFormElement;->toggleSelection()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.class final Lcom/pspdfkit/internal/rm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:I

.field final c:Landroid/graphics/drawable/Drawable;

.field final d:Landroid/graphics/drawable/Drawable;

.field final e:Landroid/graphics/drawable/Drawable;

.field final f:Landroid/graphics/drawable/Drawable;

.field final g:Landroid/graphics/drawable/Drawable;

.field final h:Landroid/graphics/drawable/Drawable;

.field final i:Landroid/graphics/drawable/Drawable;

.field final j:Landroid/graphics/drawable/Drawable;

.field final k:Landroid/graphics/drawable/Drawable;

.field final l:Landroid/graphics/drawable/Drawable;

.field final m:Landroid/graphics/drawable/Drawable;

.field final n:Landroid/graphics/drawable/Drawable;

.field final o:Landroid/graphics/drawable/Drawable;

.field final p:Landroid/graphics/drawable/Drawable;

.field final q:Landroid/graphics/drawable/Drawable;

.field final r:Landroid/graphics/drawable/Drawable;

.field final s:Landroid/graphics/drawable/Drawable;

.field final t:Landroid/graphics/drawable/Drawable;

.field final u:Landroid/graphics/drawable/Drawable;

.field final v:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons:[I

    sget v4, Lcom/pspdfkit/R$attr;->pspdf__actionBarIconsStyle:I

    sget v5, Lcom/pspdfkit/R$style;->PSPDFKit_ActionBarIcons:I

    const/4 v6, 0x0

    .line 3
    invoke-virtual {v2, v6, v3, v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 9
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__iconsColor:I

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 11
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 12
    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, v0, Lcom/pspdfkit/internal/rm;->a:I

    .line 15
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__iconsColorActivated:I

    sget v4, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    .line 17
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    .line 18
    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, v0, Lcom/pspdfkit/internal/rm;->b:I

    .line 22
    sget v3, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__outlineIcon:I

    sget v4, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline:I

    .line 23
    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 24
    sget v4, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__searchIcon:I

    sget v5, Lcom/pspdfkit/R$drawable;->pspdf__ic_search:I

    .line 25
    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 26
    sget v5, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__gridIcon:I

    sget v6, Lcom/pspdfkit/R$drawable;->pspdf__ic_thumbnails:I

    .line 27
    invoke-virtual {v2, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 28
    sget v6, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__editAnnotationsIcon:I

    sget v7, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit_annotations:I

    invoke-virtual {v2, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 30
    sget v7, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__editAnnotationsIconActivated:I

    sget v8, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit_annotations:I

    invoke-virtual {v2, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 33
    sget v8, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__editContentIcon:I

    sget v9, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit_content:I

    invoke-virtual {v2, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 35
    sget v9, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__editContentIconActivated:I

    sget v10, Lcom/pspdfkit/R$drawable;->pspdf__ic_edit_content:I

    invoke-virtual {v2, v9, v10}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 37
    sget v10, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__signatureIcon:I

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_signature:I

    .line 38
    invoke-virtual {v2, v10, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v10

    .line 39
    sget v11, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__signatureIconActivated:I

    sget v12, Lcom/pspdfkit/R$drawable;->pspdf__ic_signature:I

    invoke-virtual {v2, v11, v12}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 41
    sget v12, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__shareIcon:I

    sget v13, Lcom/pspdfkit/R$drawable;->pspdf__ic_share:I

    .line 42
    invoke-virtual {v2, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    .line 43
    sget v13, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__printIcon:I

    sget v14, Lcom/pspdfkit/R$drawable;->pspdf__ic_print:I

    .line 44
    invoke-virtual {v2, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v13

    .line 45
    sget v14, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__settingsIcon:I

    sget v15, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings:I

    .line 46
    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 47
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__outlineIconActivated:I

    move/from16 v16, v14

    sget v14, Lcom/pspdfkit/R$drawable;->pspdf__ic_outline:I

    invoke-virtual {v2, v15, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 49
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__searchIconActivated:I

    move/from16 v17, v13

    sget v13, Lcom/pspdfkit/R$drawable;->pspdf__ic_search:I

    invoke-virtual {v2, v15, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v13

    .line 51
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__gridIconActivated:I

    move/from16 v18, v12

    sget v12, Lcom/pspdfkit/R$drawable;->pspdf__ic_thumbnails_active:I

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    .line 53
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__settingsIconActivated:I

    move/from16 v19, v11

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings:I

    invoke-virtual {v2, v15, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 55
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__readerViewIcon:I

    move/from16 v20, v11

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_reader_view:I

    invoke-virtual {v2, v15, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 57
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__readerViewIconActivated:I

    move/from16 v21, v11

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_reader_view:I

    invoke-virtual {v2, v15, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 59
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__infoViewIcon:I

    move/from16 v22, v11

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_info:I

    .line 60
    invoke-virtual {v2, v15, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 61
    sget v15, Lcom/pspdfkit/R$styleable;->pspdf__ActionBarIcons_pspdf__infoViewIconActivated:I

    move/from16 v23, v11

    sget v11, Lcom/pspdfkit/R$drawable;->pspdf__ic_info:I

    invoke-virtual {v2, v15, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 64
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    invoke-static {v1, v5}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->c:Landroid/graphics/drawable/Drawable;

    .line 67
    invoke-static {v1, v12}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->d:Landroid/graphics/drawable/Drawable;

    .line 68
    invoke-static {v1, v3}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->e:Landroid/graphics/drawable/Drawable;

    .line 69
    invoke-static {v1, v14}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->f:Landroid/graphics/drawable/Drawable;

    .line 70
    invoke-static {v1, v4}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->g:Landroid/graphics/drawable/Drawable;

    .line 71
    invoke-static {v1, v13}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->h:Landroid/graphics/drawable/Drawable;

    .line 72
    invoke-static {v1, v6}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->i:Landroid/graphics/drawable/Drawable;

    .line 73
    invoke-static {v1, v7}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->j:Landroid/graphics/drawable/Drawable;

    .line 75
    invoke-static {v1, v8}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->k:Landroid/graphics/drawable/Drawable;

    .line 76
    invoke-static {v1, v9}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->l:Landroid/graphics/drawable/Drawable;

    .line 78
    invoke-static {v1, v10}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->m:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v19

    .line 79
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->n:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v18

    .line 81
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->o:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v17

    .line 82
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->p:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v16

    .line 83
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->q:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v20

    .line 84
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->r:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v23

    .line 85
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->s:Landroid/graphics/drawable/Drawable;

    .line 86
    invoke-static {v1, v11}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->t:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v21

    .line 87
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/pspdfkit/internal/rm;->u:Landroid/graphics/drawable/Drawable;

    move/from16 v2, v22

    .line 88
    invoke-static {v1, v2}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lcom/pspdfkit/internal/rm;->v:Landroid/graphics/drawable/Drawable;

    return-void
.end method

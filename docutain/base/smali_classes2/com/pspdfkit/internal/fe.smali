.class public Lcom/pspdfkit/internal/fe;
.super Lcom/pspdfkit/internal/i4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/i4<",
        "Lcom/pspdfkit/internal/ge;",
        ">;"
    }
.end annotation


# instance fields
.field protected E:Lcom/pspdfkit/annotations/InkAnnotation;

.field private F:Ljava/util/ArrayList;

.field private final G:Ljava/util/ArrayList;

.field private final H:Ljava/util/ArrayList;

.field private final I:Ljava/util/ArrayList;

.field private J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;


# direct methods
.method public static synthetic $r8$lambda$dMdFUrlF6O6mhHHkGhDLA_WbUrE(Lcom/pspdfkit/internal/fe;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/fe;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/i4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    .line 9
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    .line 13
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    .line 19
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    .line 23
    sget-object p1, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->AUTOMATIC:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/ge;)Z
    .locals 10

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    const/4 v1, 0x0

    if-eqz v0, :cond_b

    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 13
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    sget-object v3, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->SEPARATE:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    if-ne v2, v3, :cond_1

    return v1

    .line 16
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->g()I

    move-result v2

    if-ne v0, v2, :cond_b

    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 17
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->i()I

    move-result v2

    if-ne v0, v2, :cond_b

    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/InkAnnotation;->getLineWidth()F

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->f()F

    move-result v2

    cmpl-float v0, v0, v2

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->e()F

    move-result v2

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    goto/16 :goto_2

    .line 21
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    sget-object v2, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->AUTOMATIC:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    const/4 v3, 0x1

    if-ne v0, v2, :cond_a

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->j()Landroid/graphics/PointF;

    move-result-object v2

    if-nez v2, :cond_3

    goto/16 :goto_1

    .line 28
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    goto/16 :goto_1

    .line 29
    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/ge;

    .line 33
    iget-object v5, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    int-to-float v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    .line 40
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ge;->l()J

    move-result-wide v6

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ge;->h()J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-float p1, v6

    const/high16 v4, 0x43160000    # 150.0f

    cmpg-float v6, p1, v4

    if-gez v6, :cond_5

    goto :goto_0

    :cond_5
    mul-float v5, v5, v4

    div-float/2addr v5, p1

    mul-float v5, v5, v5

    .line 54
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ge;

    .line 55
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ge;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 56
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 57
    iget v7, v6, Landroid/graphics/PointF;->x:F

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget v8, v2, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v7, v8

    mul-float v7, v7, v7

    sub-float/2addr v6, v9

    mul-float v6, v6, v6

    add-float/2addr v6, v7

    cmpg-float v6, v6, v5

    if-gez v6, :cond_8

    :goto_0
    const/4 v1, 0x1

    :cond_9
    :goto_1
    return v1

    .line 58
    :cond_a
    sget-object p1, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->MERGE_IF_POSSIBLE:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    if-ne v0, p1, :cond_b

    const/4 v1, 0x1

    :cond_b
    :goto_2
    return v1
.end method

.method private b(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 6

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/annotations/InkAnnotation;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_0

    .line 4
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_3

    return-void

    .line 5
    :cond_3
    new-instance v1, Lcom/pspdfkit/internal/ge;

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v2

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v3

    move-object v4, p1

    check-cast v4, Lcom/pspdfkit/annotations/InkAnnotation;

    .line 8
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/InkAnnotation;->getLineWidth()F

    move-result v4

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/pspdfkit/internal/ge;-><init>(IIFF)V

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/pspdfkit/internal/i4;->m:F

    const/4 v4, 0x1

    .line 11
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/pspdfkit/internal/ge;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_5

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_4

    goto :goto_2

    .line 22
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 23
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-ne v1, p1, :cond_6

    .line 24
    iput-object v0, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    .line 26
    :cond_6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->i()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/pspdfkit/annotations/InkAnnotation;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/i4;->a(Lcom/pspdfkit/internal/os;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v1

    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationAggregationStrategyConfiguration;

    invoke-interface {p1, v0, v1, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/AnnotationAggregationStrategyConfiguration;

    if-eqz p1, :cond_0

    .line 7
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationAggregationStrategyConfiguration;->getAnnotationAggregationStrategy()Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    goto :goto_0

    .line 9
    :cond_0
    sget-object p1, Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;->AUTOMATIC:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->J:Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;

    :goto_0
    return-void
.end method

.method public e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method protected final f()Lcom/pspdfkit/internal/b2;
    .locals 5

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ge;

    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 2
    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFillColor()I

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/specialMode/handler/a;->getThickness()F

    move-result v3

    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/internal/ge;-><init>(IIFF)V

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/i4;->h()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    :cond_0
    return-void
.end method

.method public onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-ne p1, v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    :cond_0
    return-void
.end method

.method public final onAnnotationPropertyChange(Lcom/pspdfkit/annotations/Annotation;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    if-eqz p4, :cond_1

    .line 1
    invoke-virtual {p4, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    const/16 p3, 0x64

    if-eq p2, p3, :cond_0

    const/16 p3, 0x67

    if-ne p2, p3, :cond_1

    .line 5
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    new-instance p3, Lcom/pspdfkit/internal/fe$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0, p1}, Lcom/pspdfkit/internal/fe$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/fe;Lcom/pspdfkit/annotations/Annotation;)V

    check-cast p2, Lcom/pspdfkit/internal/u;

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    iget v1, p0, Lcom/pspdfkit/internal/i4;->l:I

    if-eq v0, v1, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_3

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    goto :goto_0

    .line 4
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_2

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 9
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 11
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-ne p1, v0, :cond_4

    const/4 p1, 0x0

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    .line 14
    iput-object p1, p0, Lcom/pspdfkit/internal/i4;->p:Lcom/pspdfkit/internal/b2;

    .line 18
    :cond_4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/i4;->i()V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->o:Lcom/pspdfkit/internal/os;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->d()V

    return-void
.end method

.method protected p()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    .line 7
    iget-object v3, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v3, :cond_1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 10
    invoke-virtual {v2}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v2

    .line 11
    invoke-static {v3, v2}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v2

    .line 13
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o1;->a()V

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 21
    :cond_1
    iget-object v3, p0, Lcom/pspdfkit/internal/i4;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/ge;

    .line 22
    iget-object v5, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    .line 23
    :cond_2
    iget-object v5, p0, Lcom/pspdfkit/internal/fe;->G:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    invoke-direct {p0, v4}, Lcom/pspdfkit/internal/fe;->a(Lcom/pspdfkit/internal/ge;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 27
    iget-object v5, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v5, :cond_4

    .line 29
    invoke-virtual {v5, v0}, Lcom/pspdfkit/annotations/InkAnnotation;->setLines(Ljava/util/List;)V

    if-eqz v2, :cond_3

    .line 32
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o1;->b()V

    .line 36
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/fe;->a(Lcom/pspdfkit/annotations/InkAnnotation;)V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    :cond_4
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-virtual {v4, v5, v2}, Lcom/pspdfkit/internal/ge;->b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    new-instance v2, Lcom/pspdfkit/annotations/InkAnnotation;

    iget v5, p0, Lcom/pspdfkit/internal/i4;->l:I

    invoke-direct {v2, v5}, Lcom/pspdfkit/annotations/InkAnnotation;-><init>(I)V

    iput-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    .line 49
    invoke-virtual {v2, v0}, Lcom/pspdfkit/annotations/InkAnnotation;->setLines(Ljava/util/List;)V

    .line 50
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    iget-object v5, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v2, v5}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 51
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ge;->g()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 52
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ge;->i()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 53
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ge;->f()F

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/annotations/InkAnnotation;->setLineWidth(F)V

    .line 54
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/ge;->e()F

    move-result v5

    invoke-virtual {v2, v5}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 57
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    .line 58
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v2

    iget-object v4, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/w1;->b(Lcom/pspdfkit/annotations/Annotation;)V

    .line 66
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v2, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 70
    invoke-virtual {v4}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v4

    .line 71
    invoke-static {v2, v4}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v2

    .line 73
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o1;->a()V

    goto/16 :goto_0

    .line 76
    :cond_5
    iget-object v5, p0, Lcom/pspdfkit/internal/i4;->c:Landroid/graphics/Matrix;

    iget v6, p0, Lcom/pspdfkit/internal/i4;->m:F

    invoke-virtual {v4, v6, v5}, Lcom/pspdfkit/internal/ge;->b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 78
    iget-object v5, p0, Lcom/pspdfkit/internal/fe;->F:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 82
    :cond_6
    iget-object v3, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v3, :cond_8

    .line 85
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 86
    iget-object v3, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/annotations/InkAnnotation;->setLines(Ljava/util/List;)V

    .line 87
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 89
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    :cond_8
    if-eqz v2, :cond_9

    .line 93
    invoke-virtual {v2}, Lcom/pspdfkit/internal/o1;->b()V

    .line 94
    :cond_9
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/Annotation;

    .line 95
    iget-object v4, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;Z)V

    goto :goto_1

    .line 96
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Created "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " ink annotations from the drawing session."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.ShapeAnnotations"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected final r()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/i4;->n:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/w1;->c(Lcom/pspdfkit/annotations/Annotation;)V

    .line 6
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationPropertyChangeListener(Lcom/pspdfkit/internal/el;)V

    goto :goto_0

    .line 8
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->H:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/fe;->I:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    return-object v0
.end method

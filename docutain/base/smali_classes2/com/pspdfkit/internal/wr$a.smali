.class final Lcom/pspdfkit/internal/wr$a;
.super Lcom/pspdfkit/internal/fs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/wr;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/wr;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/wr;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/wr$a;->b:Lcom/pspdfkit/internal/wr;

    invoke-direct {p0}, Lcom/pspdfkit/internal/fs;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 p2, 0x1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 2
    :goto_0
    iget-object p3, p0, Lcom/pspdfkit/internal/wr$a;->b:Lcom/pspdfkit/internal/wr;

    invoke-static {p3}, Lcom/pspdfkit/internal/wr;->-$$Nest$fgetf(Lcom/pspdfkit/internal/wr;)Landroid/widget/TextView;

    move-result-object p3

    xor-int/2addr p1, p2

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

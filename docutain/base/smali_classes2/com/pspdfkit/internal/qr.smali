.class public final Lcom/pspdfkit/internal/qr;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;)V
    .locals 4

    const-string v0, "pdfFragment"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSignaturePickedListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    const-string v1, "pdfFragment.configuration"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    monitor-enter v1

    .line 3
    :try_start_0
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hb;->b()Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;->ELECTRONICSIGNATURES:Lcom/pspdfkit/internal/jni/NativeSignatureFeatureAvailability;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    monitor-exit v1

    if-eqz v2, :cond_1

    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 6
    new-instance v2, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    invoke-direct {v2}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;-><init>()V

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    move-result-object v2

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureColorOptions()Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureColorOptions(Lcom/pspdfkit/configuration/signatures/SignatureColorOptions;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    move-result-object v2

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCreationModes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->signatureCreationModes(Ljava/util/List;)Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions$Builder;->build()Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;

    move-result-object v0

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p0

    .line 12
    invoke-static {v1, p1, v0, p0}, Lcom/pspdfkit/ui/signatures/ElectronicSignatureFragment;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/ElectronicSignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    goto :goto_1

    .line 24
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 26
    new-instance v2, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    invoke-direct {v2}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;-><init>()V

    .line 27
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureCertificateSelectionMode()Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureCertificateSelectionMode(Lcom/pspdfkit/configuration/signatures/SignatureCertificateSelectionMode;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    move-result-object v2

    .line 28
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignaturePickerOrientation()Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signaturePickerOrientation(Lcom/pspdfkit/configuration/forms/SignaturePickerOrientation;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    move-result-object v2

    .line 29
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSignatureSavingStrategy()Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->signatureSavingStrategy(Lcom/pspdfkit/configuration/signatures/SignatureSavingStrategy;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    move-result-object v2

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getDefaultSigner()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->defaultSigner(Ljava/lang/String;)Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/pspdfkit/ui/signatures/SignatureOptions$Builder;->build()Lcom/pspdfkit/ui/signatures/SignatureOptions;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/ui/PdfFragment;->getSignatureStorage()Lcom/pspdfkit/signatures/storage/SignatureStorage;

    move-result-object p0

    .line 33
    invoke-static {v1, p1, v0, p0}, Lcom/pspdfkit/ui/signatures/SignaturePickerFragment;->show(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/signatures/listeners/OnSignaturePickedListener;Lcom/pspdfkit/ui/signatures/SignatureOptions;Lcom/pspdfkit/signatures/storage/SignatureStorage;)V

    :goto_1
    return-void

    :catchall_0
    move-exception p0

    monitor-exit v1

    throw p0
.end method

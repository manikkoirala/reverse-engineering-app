.class public final Lcom/pspdfkit/internal/hq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/cm;


# instance fields
.field private final a:I

.field private final b:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/hq;->a:I

    .line 3
    iput-boolean p2, p0, Lcom/pspdfkit/internal/hq;->b:Z

    return-void
.end method

.method private final c(I)I
    .locals 3

    if-nez p1, :cond_0

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/hq;->b:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 5
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/hq;->a:I

    rem-int/lit8 v1, v0, 0x2

    const/4 v2, 0x1

    if-nez v1, :cond_5

    .line 7
    iget-boolean v1, p0, Lcom/pspdfkit/internal/hq;->b:Z

    if-eqz v1, :cond_3

    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_1

    return p1

    .line 16
    :cond_1
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    sub-int/2addr p1, v2

    goto :goto_0

    :cond_2
    add-int/2addr p1, v2

    :goto_0
    return p1

    .line 25
    :cond_3
    rem-int/lit8 v0, p1, 0x2

    if-ne v0, v2, :cond_4

    sub-int/2addr p1, v2

    goto :goto_1

    :cond_4
    add-int/2addr p1, v2

    :goto_1
    return p1

    .line 33
    :cond_5
    iget-boolean v1, p0, Lcom/pspdfkit/internal/hq;->b:Z

    if-eqz v1, :cond_7

    .line 37
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_6

    sub-int/2addr p1, v2

    goto :goto_2

    :cond_6
    add-int/2addr p1, v2

    :goto_2
    return p1

    :cond_7
    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_8

    return p1

    .line 51
    :cond_8
    rem-int/lit8 v0, p1, 0x2

    if-ne v0, v2, :cond_9

    sub-int/2addr p1, v2

    goto :goto_3

    :cond_9
    add-int/2addr p1, v2

    :goto_3
    return p1
.end method


# virtual methods
.method public final a(I)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hq;->c(I)I

    move-result p1

    return p1
.end method

.method public final b(I)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/hq;->c(I)I

    move-result p1

    return p1
.end method

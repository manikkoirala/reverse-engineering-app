.class public final Lcom/pspdfkit/internal/ni;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/uh;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private d:Lcom/pspdfkit/internal/views/annotations/n;


# direct methods
.method public static synthetic $r8$lambda$va7rjWE18KEySE_PFFQSrSKS_6U(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/ni;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ni;->a(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/ni;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/uh;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "magnifierManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/ni;->a:Lcom/pspdfkit/internal/uh;

    .line 25
    new-instance p3, Lcom/pspdfkit/internal/vi;

    invoke-direct {p3, p1}, Lcom/pspdfkit/internal/vi;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p2

    const v0, 0x1020002

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    if-eqz p2, :cond_2

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 31
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__measurement_value_popup:I

    const/4 v1, 0x0

    .line 32
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    iput-object p1, p0, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    .line 38
    sget v0, Lcom/pspdfkit/R$id;->pspdf__measurement_value_popup_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/pspdfkit/internal/ni;->b:Landroid/widget/TextView;

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 41
    invoke-virtual {p3}, Lcom/pspdfkit/internal/vi;->a()I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 43
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p1, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p3, Lcom/pspdfkit/internal/ni$$ExternalSyntheticLambda0;

    invoke-direct {p3, p2, p0}, Lcom/pspdfkit/internal/ni$$ExternalSyntheticLambda0;-><init>(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/ni;)V

    invoke-virtual {p1, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    .line 44
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t initialise measurement popup. Can\'t find popup text view."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 45
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t initialise measurement popup."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 46
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t initialise measurement popup without application root view."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static final a(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/ni;)V
    .locals 1

    const-string v0, "$applicationRoot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p1, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->d:Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/n;->a(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/n;)V
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->d:Lcom/pspdfkit/internal/views/annotations/n;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->d:Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/views/annotations/n;->a(Z)V

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/ni;->d:Lcom/pspdfkit/internal/views/annotations/n;

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->a:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->g()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ni;->a:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uh;->d()Landroid/graphics/Point;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4
    :cond_1
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/pspdfkit/internal/ni;->a:Lcom/pspdfkit/internal/uh;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/uh;->f()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/pspdfkit/internal/ni;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Point;->x:I

    .line 5
    iget v2, v0, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/pspdfkit/internal/ni;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0xa

    .line 8
    iput v2, v0, Landroid/graphics/Point;->y:I

    :goto_0
    if-nez v0, :cond_2

    return v1

    .line 9
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/ni;->b:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setX(F)V

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/ni;->c:Landroid/view/View;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setY(F)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/ni;->d:Lcom/pspdfkit/internal/views/annotations/n;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/views/annotations/n;->a(Z)V

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

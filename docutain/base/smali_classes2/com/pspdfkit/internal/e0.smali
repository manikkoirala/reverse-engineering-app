.class public abstract Lcom/pspdfkit/internal/e0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationAggregationStrategyConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration$Builder;
.implements Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationAggregationStrategyConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration$Builder<",
        "TT;>;",
        "Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration$Builder<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/h0;

.field private b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V
    .locals 1

    const-string v0, "supportedProperties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/h0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/h0;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    .line 21
    array-length v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 22
    const-class p1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p1

    const-string v0, "noneOf(AnnotationProperty::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/e0;->a(Ljava/util/EnumSet;)Lcom/pspdfkit/internal/e0;

    goto :goto_1

    .line 24
    :cond_1
    invoke-static {p1}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object p1

    const-string v0, "copyOf(supportedProperties.toList())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/e0;->a(Ljava/util/EnumSet;)Lcom/pspdfkit/internal/e0;

    :goto_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/EnumSet;)Lcom/pspdfkit/internal/e0;
    .locals 3

    const-string v0, "supportedProperties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    const-string v1, "copyOf(supportedProperties)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/e0;->b:Ljava/util/EnumSet;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v2, Lcom/pspdfkit/internal/g0;->a:Lcom/pspdfkit/internal/g0;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {v1, v2, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method protected final a()Lcom/pspdfkit/internal/h0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    return-object v0
.end method

.method protected final b()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->b:Ljava/util/EnumSet;

    if-nez v0, :cond_0

    const-string v0, "supportedProperties"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public final disableProperty(Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Ljava/lang/Object;
    .locals 4

    const-string v0, "disabledProperty"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->b:Ljava/util/EnumSet;

    const/4 v1, 0x0

    const-string v2, "supportedProperties"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 82
    iget-object p1, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v0, Lcom/pspdfkit/internal/g0;->a:Lcom/pspdfkit/internal/g0;

    iget-object v3, p0, Lcom/pspdfkit/internal/e0;->b:Ljava/util/EnumSet;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    :cond_2
    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 83
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAnnotationAggregationStrategy(Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;)Ljava/lang/Object;
    .locals 2

    const-string v0, "aggregationStrategy"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->u:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 134
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAvailableFonts(Ljava/util/List;)Ljava/lang/Object;
    .locals 3

    const-string v0, "availableFonts"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableFonts may not contain null item"

    .line 169
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 170
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->A:Lcom/pspdfkit/internal/g0;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 171
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAvailableIconNames(Ljava/util/List;)Ljava/lang/Object;
    .locals 2

    const-string v0, "availableIconNames"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->F:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 191
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setAvailableLineEnds(Ljava/util/List;)Ljava/lang/Object;
    .locals 2

    const-string v0, "availableLineEnds"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableLineEnds may not contain null item"

    .line 158
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 159
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->y:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 160
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultAlpha(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->q:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultFont(Lcom/pspdfkit/ui/fonts/Font;)Ljava/lang/Object;
    .locals 2

    const-string v0, "defaultFont"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->z:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 165
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultIconName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    const-string v0, "iconName"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->E:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 186
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultLineEnds(Landroidx/core/util/Pair;)Ljava/lang/Object;
    .locals 2

    const-string v0, "defaultLineEnds"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->x:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 154
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultOverlayText(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    const-string v0, "defaultOverlayText"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->D:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 181
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)Ljava/lang/Object;
    .locals 2

    const-string v0, "defaultPrecision"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->L:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 211
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultRepeatOverlayTextSetting(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->C:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultScale(Lcom/pspdfkit/annotations/measurements/Scale;)Ljava/lang/Object;
    .locals 2

    const-string v0, "defaultScale"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->K:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 206
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultTextSize(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->n:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setDefaultThickness(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->k:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setForceDefaults(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->b:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setHorizontalResizingEnabled(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->J:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMaxAlpha(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->s:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMaxTextSize(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->p:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMaxThickness(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->m:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMinAlpha(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->r:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMinTextSize(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->o:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setMinThickness(F)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->l:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setPreviewEnabled(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->t:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final bridge synthetic setSupportedProperties(Ljava/util/EnumSet;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/e0;->a(Ljava/util/EnumSet;)Lcom/pspdfkit/internal/e0;

    move-result-object p1

    return-object p1
.end method

.method public final setVerticalResizingEnabled(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->I:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final setZIndexEditingEnabled(Z)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e0;->a:Lcom/pspdfkit/internal/h0;

    sget-object v1, Lcom/pspdfkit/internal/g0;->c:Lcom/pspdfkit/internal/g0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

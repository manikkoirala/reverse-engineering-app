.class public final Lcom/pspdfkit/internal/ga;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 1

    const/16 v0, 0x800

    .line 13
    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    return p0
.end method

.method public static a(IZZ)I
    .locals 0

    if-eqz p2, :cond_0

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result p0

    :cond_0
    if-eqz p1, :cond_1

    .line 2
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result p1

    mul-int/lit8 p1, p1, 0x1e

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result p2

    mul-int/lit8 p2, p2, 0x3b

    add-int/2addr p2, p1

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    mul-int/lit8 p1, p1, 0xb

    add-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x64

    .line 3
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result p0

    invoke-static {p0, p1, p1, p1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    :cond_1
    return p0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5

    .line 52
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 53
    move-object v0, p0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 54
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    .line 59
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 64
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 65
    :cond_2
    :goto_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 72
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 73
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x0

    .line 74
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p0

    .line 75
    array-length v1, p0

    invoke-static {p0, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static a()Landroid/graphics/ColorMatrix;
    .locals 2

    .line 12
    new-instance v0, Landroid/graphics/ColorMatrix;

    const/16 v1, 0x14

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrix;-><init>([F)V

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3e99999a    # 0.3f
        0x3f170a3d    # 0.59f
        0x3de147ae    # 0.11f
        0x0
        0x0
        0x3e99999a    # 0.3f
        0x3f170a3d    # 0.59f
        0x3de147ae    # 0.11f
        0x0
        0x0
        0x3e99999a    # 0.3f
        0x3f170a3d    # 0.59f
        0x3de147ae    # 0.11f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static a(ZZ)Landroid/graphics/ColorMatrixColorFilter;
    .locals 2

    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_2

    .line 4
    :cond_1
    :goto_0
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    const/16 v1, 0x14

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 5
    new-instance p0, Landroid/graphics/ColorMatrix;

    new-array p1, v1, [F

    fill-array-data p1, :array_0

    invoke-direct {p0, p1}, Landroid/graphics/ColorMatrix;-><init>([F)V

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    .line 6
    new-instance p0, Landroid/graphics/ColorMatrix;

    new-array p1, v1, [F

    fill-array-data p1, :array_1

    invoke-direct {p0, p1}, Landroid/graphics/ColorMatrix;-><init>([F)V

    goto :goto_1

    :cond_3
    if-eqz p0, :cond_4

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/ga;->a()Landroid/graphics/ColorMatrix;

    move-result-object p0

    goto :goto_1

    .line 10
    :cond_4
    new-instance p0, Landroid/graphics/ColorMatrix;

    invoke-direct {p0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 11
    :goto_1
    invoke-direct {v0, p0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    move-object p0, v0

    :goto_2
    return-object p0

    :array_0
    .array-data 4
        -0x41666666    # -0.3f
        -0x40e8f5c3    # -0.59f
        -0x421eb852    # -0.11f
        0x0
        0x437f0000    # 255.0f
        -0x41666666    # -0.3f
        -0x40e8f5c3    # -0.59f
        -0x421eb852    # -0.11f
        0x0
        0x437f0000    # 255.0f
        -0x41666666    # -0.3f
        -0x40e8f5c3    # -0.59f
        -0x421eb852    # -0.11f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 7

    .line 14
    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 15
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v3, v0, v1

    if-gtz v3, :cond_1

    .line 17
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v3, v0, v1

    if-gez v3, :cond_0

    sub-float v0, v1, v0

    .line 19
    iput v1, p0, Landroid/graphics/RectF;->left:F

    .line 20
    iget v1, p0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v0

    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 23
    :cond_0
    iget v0, p0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v3, v0, v1

    if-lez v3, :cond_2

    sub-float/2addr v0, v1

    .line 25
    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 26
    iget v1, p0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    iput v1, p0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    :cond_1
    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    neg-float v3, v0

    .line 30
    iput v3, p0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v0

    .line 31
    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 34
    :cond_2
    :goto_0
    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    sub-float v3, v0, v1

    .line 35
    iget v4, p1, Landroid/graphics/RectF;->top:F

    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float v5, v4, v5

    cmpg-float v6, v3, v5

    if-gtz v6, :cond_4

    cmpl-float v2, v0, v4

    if-lez v2, :cond_3

    sub-float/2addr v0, v4

    .line 39
    iput v4, p0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v0

    .line 40
    iput v1, p0, Landroid/graphics/RectF;->bottom:F

    .line 43
    :cond_3
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v0, p1

    if-gez v1, :cond_5

    sub-float v0, p1, v0

    .line 45
    iput p1, p0, Landroid/graphics/RectF;->bottom:F

    .line 46
    iget p1, p0, Landroid/graphics/RectF;->top:F

    add-float/2addr p1, v0

    iput p1, p0, Landroid/graphics/RectF;->top:F

    goto :goto_1

    :cond_4
    sub-float/2addr v3, v5

    div-float/2addr v3, v2

    add-float/2addr v5, v3

    .line 50
    iput v5, p0, Landroid/graphics/RectF;->top:F

    neg-float p1, v3

    .line 51
    iput p1, p0, Landroid/graphics/RectF;->bottom:F

    :cond_5
    :goto_1
    return-void
.end method

.method public static b(I)I
    .locals 1

    const/16 v0, 0x800

    .line 1
    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    return p0
.end method

.method public static b(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 4

    .line 2
    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 3
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v3, v0, v1

    if-gtz v3, :cond_1

    .line 5
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v3, v0, v1

    if-gez v3, :cond_0

    sub-float v0, v1, v0

    .line 7
    iput v1, p0, Landroid/graphics/RectF;->left:F

    .line 8
    iget v1, p0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v0

    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 11
    :cond_0
    iget v0, p0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v3, v0, v1

    if-lez v3, :cond_2

    sub-float/2addr v0, v1

    .line 13
    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 14
    iget v1, p0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    iput v1, p0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    :cond_1
    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    neg-float v3, v0

    .line 18
    iput v3, p0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v0

    .line 19
    iput v1, p0, Landroid/graphics/RectF;->right:F

    .line 22
    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 23
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v3, v0, v1

    if-gtz v3, :cond_4

    .line 25
    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v0, v1

    if-gez v2, :cond_3

    sub-float v0, v1, v0

    .line 27
    iput v1, p0, Landroid/graphics/RectF;->top:F

    .line 28
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v0

    iput v1, p0, Landroid/graphics/RectF;->bottom:F

    .line 31
    :cond_3
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v0, p1

    if-lez v1, :cond_5

    sub-float/2addr v0, p1

    .line 33
    iput p1, p0, Landroid/graphics/RectF;->bottom:F

    .line 34
    iget p1, p0, Landroid/graphics/RectF;->top:F

    sub-float/2addr p1, v0

    iput p1, p0, Landroid/graphics/RectF;->top:F

    goto :goto_1

    :cond_4
    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    neg-float p1, v0

    .line 38
    iput p1, p0, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v0

    .line 39
    iput v1, p0, Landroid/graphics/RectF;->bottom:F

    :cond_5
    :goto_1
    return-void
.end method

.method public static c(I)I
    .locals 3

    .line 1
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const v1, 0xffffff

    xor-int/2addr p0, v1

    .line 3
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    invoke-static {v0, v1, v2, p0}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    return p0
.end method

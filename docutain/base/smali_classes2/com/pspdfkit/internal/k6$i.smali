.class final Lcom/pspdfkit/internal/k6$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/k6;->u()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/k6;

.field final synthetic b:Landroid/graphics/Matrix;

.field final synthetic c:Lcom/pspdfkit/utils/Size;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/k6;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/k6$i;->a:Lcom/pspdfkit/internal/k6;

    iput-object p2, p0, Lcom/pspdfkit/internal/k6$i;->b:Landroid/graphics/Matrix;

    iput-object p3, p0, Lcom/pspdfkit/internal/k6$i;->c:Lcom/pspdfkit/utils/Size;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/pt;

    const-string v0, "it"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$i;->a:Lcom/pspdfkit/internal/k6;

    iget-object v1, p0, Lcom/pspdfkit/internal/k6$i;->b:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/pspdfkit/internal/k6$i;->c:Lcom/pspdfkit/utils/Size;

    invoke-static {v0, p1, v1, v2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;)Lcom/pspdfkit/internal/o6;

    move-result-object p1

    return-object p1
.end method

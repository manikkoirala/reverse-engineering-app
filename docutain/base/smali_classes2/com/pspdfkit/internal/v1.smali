.class public final Lcom/pspdfkit/internal/v1;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static synthetic $r8$lambda$WyzvffgA70HTwJYFQkEZhH_qQZo(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/v1;->a(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$euSZtxVSE2XSAlJj550Qy_cFdKM(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/v1;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v7, p3

    const/4 v1, 0x1

    .line 38
    invoke-virtual {v7, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    const/4 v1, 0x0

    .line 39
    invoke-virtual {v7, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 42
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 57
    invoke-static {}, Lcom/pspdfkit/internal/gj;->p()Lcom/pspdfkit/internal/fn;

    move-result-object v2

    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    invoke-static {v3}, Lcom/pspdfkit/internal/gn;->a(Lcom/pspdfkit/annotations/AnnotationType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/fn;->a(Ljava/lang/String;)V

    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 67
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 68
    new-instance v2, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;

    iget-object v9, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formHighlightColor:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formRequiredFieldBorderColor:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->signHereOverlayBackgroundColor:Ljava/lang/Integer;

    iget-object v12, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formItemHighlightColor:Ljava/lang/Integer;

    iget-boolean v13, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->showSignHereOverlay:Z

    move-object v8, v2

    invoke-direct/range {v8 .. v13}, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    .line 75
    new-instance v6, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;

    iget-boolean v10, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->toGrayscale:Z

    iget-boolean v11, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->invertColors:Z

    iget-boolean v15, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->drawRedactAsRedacted:Z

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object v8, v6

    move-object v9, v2

    invoke-direct/range {v8 .. v15}, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;-><init>(Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;ZZZZZZ)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, v1

    move-object/from16 v1, p3

    .line 76
    invoke-static/range {v0 .. v6}, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderer;->drawAnnotation(Lcom/pspdfkit/internal/jni/NativeAnnotation;Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;)Z

    return-object v7

    .line 77
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t render annotations that aren\'t attached to a document page!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v9, p3

    const/4 v1, 0x1

    .line 79
    invoke-virtual {v9, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    const/4 v1, 0x0

    .line 80
    invoke-virtual {v9, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 83
    invoke-interface/range {p0 .. p1}, Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;->shouldUseGeneratorForAnnotation(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v9

    .line 89
    :cond_0
    const-class v1, Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator$AppearanceStreamGenerationOptions;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 90
    invoke-interface {v2, v3, v1}, Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;->getDataProviderForAnnotation(Lcom/pspdfkit/annotations/Annotation;Ljava/util/EnumSet;)Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 97
    new-instance v2, Lcom/pspdfkit/internal/s7;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 100
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v3

    .line 104
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 105
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 106
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;

    iget-object v11, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formHighlightColor:Ljava/lang/Integer;

    iget-object v12, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formRequiredFieldBorderColor:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->signHereOverlayBackgroundColor:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->formItemHighlightColor:Ljava/lang/Integer;

    iget-boolean v15, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->showSignHereOverlay:Z

    move-object v10, v1

    invoke-direct/range {v10 .. v15}, Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    .line 113
    new-instance v8, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;

    iget-boolean v12, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->toGrayscale:Z

    iget-boolean v13, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->invertColors:Z

    iget-boolean v0, v0, Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;->drawRedactAsRedacted:Z

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object v10, v8

    move-object v11, v1

    move/from16 v17, v0

    invoke-direct/range {v10 .. v17}, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;-><init>(Lcom/pspdfkit/internal/jni/NativeFormRenderingConfig;ZZZZZZ)V

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object/from16 v3, p3

    .line 114
    invoke-static/range {v0 .. v8}, Lcom/pspdfkit/internal/jni/NativeAnnotationRenderer;->drawRawAPStream(Lcom/pspdfkit/internal/jni/NativeDataProvider;ILandroid/graphics/RectF;Landroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativeAnnotationRenderingConfig;)Z

    return-object v9

    .line 115
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t generate data provider for AP stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Lcom/pspdfkit/annotations/StampAnnotation;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/Single;
    .locals 1

    .line 78
    invoke-static {p3}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p3

    new-instance v0, Lcom/pspdfkit/internal/v1$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/v1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)V

    invoke-virtual {p3, v0}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zf;",
            "Lcom/pspdfkit/annotations/Annotation;",
            "Landroid/graphics/Bitmap;",
            "Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/v1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2, p3}, Lcom/pspdfkit/internal/v1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;Lcom/pspdfkit/configuration/rendering/AnnotationRenderConfiguration;)V

    .line 2
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const/4 p2, 0x5

    .line 37
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/zf;->c(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p0

    invoke-virtual {p1, p0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

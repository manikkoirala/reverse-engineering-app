.class public final Lcom/pspdfkit/internal/e;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/e$a;,
        Lcom/pspdfkit/internal/e$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/e$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/e$a;

.field private final b:Ljava/util/ArrayList;

.field private final c:I

.field private final d:I


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/e;)Lcom/pspdfkit/internal/e$a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/e;->a:Lcom/pspdfkit/internal/e$a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/e;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/e;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/e;->c:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/e;)I
    .locals 0

    iget p0, p0, Lcom/pspdfkit/internal/e;->d:I

    return p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/e$a;II)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    .line 15
    iput-object p1, p0, Lcom/pspdfkit/internal/e;->a:Lcom/pspdfkit/internal/e$a;

    .line 16
    iput p2, p0, Lcom/pspdfkit/internal/e;->c:I

    .line 17
    iput p3, p0, Lcom/pspdfkit/internal/e;->d:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    .line 4
    invoke-virtual {p0, v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeRemoved(II)V

    .line 5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {p0, v1, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/e$b;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/e$b;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e$b;)Lcom/pspdfkit/internal/jd;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jd;->setLabel(Ljava/lang/String;)V

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/e$b;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e$b;)Lcom/pspdfkit/internal/jd;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/jd;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/e$b;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e$b;)Lcom/pspdfkit/internal/jd;

    move-result-object p1

    invoke-virtual {p2}, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;->isEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jd;->setEnabled(Z)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 1

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/e$b;

    new-instance v0, Lcom/pspdfkit/internal/jd;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/jd;-><init>(Landroid/content/Context;)V

    invoke-direct {p2, p0, v0}, Lcom/pspdfkit/internal/e$b;-><init>(Lcom/pspdfkit/internal/e;Lcom/pspdfkit/internal/jd;)V

    return-object p2
.end method

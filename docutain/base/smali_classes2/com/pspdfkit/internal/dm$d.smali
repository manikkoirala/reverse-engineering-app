.class final Lcom/pspdfkit/internal/dm$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/im$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/dm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/dm;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/dm;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/dm$d;->a:Lcom/pspdfkit/internal/dm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/dm$d-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/dm$d;-><init>(Lcom/pspdfkit/internal/dm;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/im;Lcom/pspdfkit/internal/im$g;)V
    .locals 0

    .line 3
    sget-object p1, Lcom/pspdfkit/internal/im$g;->a:Lcom/pspdfkit/internal/im$g;

    if-ne p2, p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/dm$d;->a:Lcom/pspdfkit/internal/dm;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/dm;->-$$Nest$fputB(Lcom/pspdfkit/internal/dm;Z)V

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgetC(Lcom/pspdfkit/internal/dm;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgetw(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/oh;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/oh;->b()V

    .line 7
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgetl(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/am;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/am;->i()V

    .line 8
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgetp(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/w1;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/w1;->f()V

    .line 9
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgeti(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/dm$c;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 10
    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/dm$c;->a(Lcom/pspdfkit/internal/dm;)V

    .line 15
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgetm(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/tb;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/tb;->i()V

    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$d;->a:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgeti(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/dm$c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2
    invoke-interface {v1, v0, p1, p2, p3}, Lcom/pspdfkit/internal/dm$c;->a(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b(Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dm$d;->a:Lcom/pspdfkit/internal/dm;

    invoke-static {v0}, Lcom/pspdfkit/internal/dm;->-$$Nest$fgeti(Lcom/pspdfkit/internal/dm;)Lcom/pspdfkit/internal/dm$c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {v1, v0, p1, p2, p3}, Lcom/pspdfkit/internal/dm$c;->b(Lcom/pspdfkit/internal/dm;Landroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

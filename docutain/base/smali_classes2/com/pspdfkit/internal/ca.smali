.class public abstract Lcom/pspdfkit/internal/ca;
.super Lcom/pspdfkit/internal/ug;
.source "SourceFile"


# instance fields
.field protected final u:Z

.field protected final v:Z

.field protected final w:I

.field protected x:Ljava/util/ArrayList;


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZZZLcom/pspdfkit/internal/cm;)V
    .locals 9

    move-object v8, p0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p10

    .line 1
    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/ug;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFILcom/pspdfkit/internal/cm;)V

    move/from16 v0, p7

    .line 10
    iput-boolean v0, v8, Lcom/pspdfkit/internal/ca;->u:Z

    move/from16 v0, p8

    .line 11
    iput-boolean v0, v8, Lcom/pspdfkit/internal/ca;->v:Z

    if-eqz p9, :cond_0

    .line 12
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, v8, Lcom/pspdfkit/internal/ca;->w:I

    .line 13
    invoke-direct {p0}, Lcom/pspdfkit/internal/ca;->B()V

    return-void
.end method

.method private B()V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x2

    if-ge v2, v0, :cond_2

    .line 4
    iget-object v4, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v4, v2}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v4

    .line 5
    iget-object v5, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v5, v2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v5

    .line 6
    iget v6, v5, Lcom/pspdfkit/utils/Size;->width:F

    .line 7
    iget v5, v5, Lcom/pspdfkit/utils/Size;->height:F

    .line 10
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/ca;->l(I)I

    move-result v4

    const/4 v7, 0x3

    if-ne v4, v7, :cond_0

    .line 11
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    goto :goto_1

    .line 12
    :cond_0
    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    iget v7, p0, Lcom/pspdfkit/internal/ca;->w:I

    sub-int/2addr v4, v7

    div-int/lit8 v3, v4, 0x2

    .line 15
    :goto_1
    iget-boolean v4, p0, Lcom/pspdfkit/internal/ca;->u:Z

    if-eqz v4, :cond_1

    int-to-float v3, v3

    div-float/2addr v3, v6

    .line 16
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_2

    :cond_1
    int-to-float v3, v3

    div-float/2addr v3, v6

    :goto_2
    mul-float v6, v6, v3

    mul-float v5, v5, v3

    .line 21
    iget-object v3, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    new-instance v4, Lcom/pspdfkit/utils/Size;

    invoke-direct {v4, v6, v5}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 27
    :cond_2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ca;->u:Z

    if-eqz v0, :cond_7

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v0, :cond_7

    .line 29
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/ca;->q(I)I

    move-result v4

    if-ne v4, v3, :cond_6

    .line 30
    iget-object v4, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/utils/Size;

    .line 31
    iget-object v6, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/utils/Size;

    .line 33
    iget v7, v4, Lcom/pspdfkit/utils/Size;->width:F

    iget v8, v6, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/pspdfkit/internal/ca;->w:I

    int-to-float v9, v8

    add-float/2addr v7, v9

    iget v9, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v10, v9

    const/4 v11, 0x1

    cmpl-float v7, v7, v10

    if-ltz v7, :cond_3

    const/4 v7, 0x1

    goto :goto_4

    :cond_3
    const/4 v7, 0x0

    .line 35
    :goto_4
    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    iget v6, v6, Lcom/pspdfkit/utils/Size;->height:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_4

    goto :goto_5

    :cond_4
    const/4 v11, 0x0

    :goto_5
    if-nez v7, :cond_6

    if-nez v11, :cond_6

    if-lez v4, :cond_5

    move v4, v2

    goto :goto_6

    :cond_5
    move v4, v5

    move v5, v2

    :goto_6
    sub-int/2addr v9, v8

    .line 47
    div-int/2addr v9, v3

    int-to-float v6, v9

    iget-object v7, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    .line 48
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/utils/Size;

    iget v5, v5, Lcom/pspdfkit/utils/Size;->width:F

    sub-float/2addr v6, v5

    float-to-int v5, v6

    .line 50
    iget-object v6, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/utils/Size;

    .line 51
    iget v7, v6, Lcom/pspdfkit/utils/Size;->height:F

    iget v6, v6, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v7, v6

    int-to-float v5, v5

    add-float/2addr v6, v5

    .line 53
    iget-object v5, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    new-instance v8, Lcom/pspdfkit/utils/Size;

    mul-float v7, v7, v6

    invoke-direct {v8, v6, v7}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    invoke-virtual {v5, v4, v8}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    return-void
.end method


# virtual methods
.method public final d(I)I
    .locals 4

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->q(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    add-int/2addr p1, v2

    return p1

    .line 3
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->q(I)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    sub-int/2addr p1, v2

    return p1

    :cond_1
    return v0
.end method

.method protected final l(I)I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result v0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageBinding()Lcom/pspdfkit/document/PageBinding;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    if-ne v1, v2, :cond_c

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v1

    const/4 v2, 0x2

    rem-int/2addr v1, v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v1, :cond_5

    .line 14
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ca;->v:Z

    if-eqz v1, :cond_1

    .line 17
    rem-int/2addr p1, v2

    if-nez p1, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const/4 v4, 0x0

    goto :goto_3

    .line 23
    :cond_1
    rem-int/2addr p1, v2

    if-ne p1, v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-nez v0, :cond_3

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    .line 25
    :goto_1
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v1

    sub-int/2addr v1, v4

    if-ne v0, v1, :cond_4

    const/4 v3, 0x1

    :cond_4
    move v4, v3

    move v3, p1

    goto :goto_3

    .line 29
    :cond_5
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ca;->v:Z

    if-eqz v1, :cond_7

    .line 32
    rem-int/2addr p1, v2

    if-ne p1, v4, :cond_6

    const/4 v2, 0x1

    .line 34
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result p1

    sub-int/2addr p1, v4

    if-ne v0, p1, :cond_0

    goto :goto_3

    .line 38
    :cond_7
    rem-int/2addr p1, v2

    if-nez p1, :cond_8

    const/4 v2, 0x1

    :cond_8
    if-nez v0, :cond_9

    goto :goto_2

    :cond_9
    const/4 v4, 0x0

    :goto_2
    move v3, v4

    goto :goto_0

    :goto_3
    if-nez v3, :cond_a

    if-eqz v4, :cond_b

    :cond_a
    const/4 v2, 0x3

    :cond_b
    return v2

    .line 45
    :cond_c
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ca;->q(I)I

    move-result p1

    return p1
.end method

.method protected final m(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->p(I)I

    move-result p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget-object v1, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    :goto_0
    float-to-int p1, p1

    return p1
.end method

.method protected final n(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->p(I)I

    move-result p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget-object v1, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    add-float/2addr p1, v0

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ca;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    :goto_0
    float-to-int p1, p1

    return p1
.end method

.method protected final o(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->l(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method

.method protected final p(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->q(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method

.method protected final q(I)I
    .locals 5

    .line 1
    rem-int/lit8 v0, p1, 0x2

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ca;->v:Z

    xor-int/lit8 v2, v1, 0x1

    const/4 v3, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x0

    if-nez p1, :cond_1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 4
    :goto_1
    iget-object v4, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/views/document/DocumentView;->getPageCount()I

    move-result v4

    sub-int/2addr v4, v3

    if-ne p1, v4, :cond_2

    if-ne v0, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v0, 0x3

    :cond_4
    return v0
.end method

.method protected final r(I)I
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->p(I)I

    move-result p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->a(I)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->a(I)I

    move-result v2

    if-lt v1, v2, :cond_1

    goto :goto_0

    :cond_1
    move p1, v0

    :goto_0
    return p1
.end method

.method protected final s(I)Z
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ca;->d(I)I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.class public final Lcom/pspdfkit/internal/n0;
.super Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/EnumSet;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/internal/n;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupingRules"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/ui/toolbar/grouping/presets/PresetMenuItemGroupingRule;-><init>(Landroid/content/Context;)V

    .line 4
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/pspdfkit/internal/n0;->a:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/n0;->b:Ljava/util/ArrayList;

    .line 10
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/pspdfkit/internal/n0;->c:Ljava/util/ArrayList;

    .line 13
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/pspdfkit/internal/n0;->d:Ljava/util/ArrayList;

    .line 16
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x9

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/pspdfkit/internal/n0;->e:Ljava/util/ArrayList;

    .line 19
    invoke-virtual {p2}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 26
    sget-object v4, Lcom/pspdfkit/internal/n;->a:Lcom/pspdfkit/internal/n;

    invoke-virtual {p2, v4}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 28
    sget-object p2, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP_EXTRA:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP_PLUS_MEASUREMENTS:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    new-instance v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v4, v5}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v4, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->UNDO_REDO_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object p2, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MEASUREMENTS_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    new-instance v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v6, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v5, v6}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MARKUP_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_AND_MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v5, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {p1, v5}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->WRITING_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v1, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->DRAWING_GROUP_NO_ERASER:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v5, Lcom/pspdfkit/ui/toolbar/grouping/presets/AnnotationCreationToolbarItemGroups;->MULTIMEDIA_GROUP:Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    invoke-direct {v6, v7}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    new-instance v6, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget v7, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {v6, v7}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_eraser:I

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_picker:I

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_undo:I

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance p1, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;

    sget p2, Lcom/pspdfkit/R$id;->pspdf__annotation_creation_toolbar_item_redo:I

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    .line 68
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Grouping rules may not be empty."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final areGeneratedGroupItemsSelectable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final getGroupPreset(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/toolbar/grouping/presets/MenuItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->a:Ljava/util/ArrayList;

    goto :goto_2

    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->b:Ljava/util/ArrayList;

    goto :goto_2

    :cond_1
    const/4 v0, 0x6

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-gt v0, p1, :cond_2

    if-ge p1, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->c:Ljava/util/ArrayList;

    goto :goto_2

    :cond_3
    if-ne p1, v2, :cond_4

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->d:Ljava/util/ArrayList;

    goto :goto_2

    :cond_4
    const/16 v0, 0x9

    if-gt v0, p1, :cond_5

    if-ge p1, p2, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_6

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->e:Ljava/util/ArrayList;

    goto :goto_2

    .line 16
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/n0;->e:Ljava/util/ArrayList;

    :goto_2
    return-object p1
.end method

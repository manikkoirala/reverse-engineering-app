.class public final Lcom/pspdfkit/internal/vm;
.super Lcom/pspdfkit/document/image/SimpleOnImagePickedListener;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/wm;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Lcom/pspdfkit/forms/PushButtonFormElement;

.field final synthetic e:Lcom/pspdfkit/annotations/WidgetAnnotation;


# direct methods
.method public static synthetic $r8$lambda$UbWL170Feq_QduyBI7cncPc-Rh4(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/vm;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/wm;Landroid/content/Context;Lcom/pspdfkit/forms/PushButtonFormElement;Lcom/pspdfkit/annotations/WidgetAnnotation;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    iput-object p2, p0, Lcom/pspdfkit/internal/vm;->c:Landroid/content/Context;

    iput-object p3, p0, Lcom/pspdfkit/internal/vm;->d:Lcom/pspdfkit/forms/PushButtonFormElement;

    iput-object p4, p0, Lcom/pspdfkit/internal/vm;->e:Lcom/pspdfkit/annotations/WidgetAnnotation;

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/document/image/SimpleOnImagePickedListener;-><init>()V

    return-void
.end method

.method private static final a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "$context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/document/image/ImagePicker;->deleteTemporaryFile(Landroid/content/Context;Landroid/net/Uri;)Z

    return-void
.end method


# virtual methods
.method public final onImagePicked(Landroid/net/Uri;)V
    .locals 4

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    invoke-static {v0}, Lcom/pspdfkit/internal/wm;->a(Lcom/pspdfkit/internal/wm;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/vm;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/pspdfkit/document/image/BitmapUtils;->decodeBitmapAsync(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/vm;->c:Landroid/content/Context;

    new-instance v2, Lcom/pspdfkit/internal/vm$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1, p1}, Lcom/pspdfkit/internal/vm$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/vm$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/vm;->d:Lcom/pspdfkit/forms/PushButtonFormElement;

    iget-object v2, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    iget-object v3, p0, Lcom/pspdfkit/internal/vm;->e:Lcom/pspdfkit/annotations/WidgetAnnotation;

    invoke-direct {v0, v1, v2, v3}, Lcom/pspdfkit/internal/vm$a;-><init>(Lcom/pspdfkit/forms/PushButtonFormElement;Lcom/pspdfkit/internal/wm;Lcom/pspdfkit/annotations/WidgetAnnotation;)V

    sget-object v1, Lcom/pspdfkit/internal/vm$b;->a:Lcom/pspdfkit/internal/vm$b;

    invoke-virtual {p1, v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    invoke-static {v0}, Lcom/pspdfkit/internal/wm;->b(Lcom/pspdfkit/internal/wm;)Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    return-void
.end method

.method public final onImagePickerCancelled()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    invoke-static {v0}, Lcom/pspdfkit/internal/wm;->a(Lcom/pspdfkit/internal/wm;)V

    return-void
.end method

.method public final onImagePickerUnknownError()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vm;->b:Lcom/pspdfkit/internal/wm;

    invoke-static {v0}, Lcom/pspdfkit/internal/wm;->a(Lcom/pspdfkit/internal/wm;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/oe;
.super Lcom/pspdfkit/internal/a0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ve;
.implements Lcom/pspdfkit/internal/xe$c;


# instance fields
.field private final h:Lcom/pspdfkit/internal/xe;

.field private i:Ljava/lang/String;

.field private final j:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/ve$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    const-string v0, "assetProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/a0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    .line 28
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    const/4 p1, 0x1

    .line 33
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    .line 34
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    .line 35
    iput p1, p0, Lcom/pspdfkit/internal/oe;->k:I

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Bitmap;)V
    .locals 1

    const-string v0, "assetProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampBitmap"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;)V

    .line 40
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/a0;->a(Landroid/graphics/Bitmap;)V

    const/4 p1, 0x4

    .line 41
    iput p1, p0, Lcom/pspdfkit/internal/oe;->k:I

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;)V
    .locals 1

    const-string v0, "assetProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageAttachmentId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;)V

    .line 37
    iput-object p3, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    const/4 p1, 0x0

    .line 38
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;[B)V
    .locals 1

    const-string v0, "assetProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compressedStampBitmap"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/oe;-><init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/annotations/Annotation;)V

    .line 43
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/a0;->a([B)V

    const/4 p1, 0x4

    .line 44
    iput p1, p0, Lcom/pspdfkit/internal/oe;->k:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/pspdfkit/internal/oe;->k:I

    return v0
.end method

.method public final a(Lcom/pspdfkit/internal/ve$a;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "assetIdentifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x2

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/oe;->k:I

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ve$a;

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/internal/ve$a;->l()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    const-string v0, "assetIdentifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantException"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x3

    .line 10
    iput p1, p0, Lcom/pspdfkit/internal/oe;->k:I

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "Instant"

    const-string v1, "Could not download asset for %s"

    invoke-static {v0, p2, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/ve$a;

    .line 15
    invoke-interface {p2}, Lcom/pspdfkit/internal/ve$a;->c()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/ve$a;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    const-string v0, "assetIdentifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/xe;->b(Lcom/pspdfkit/internal/xe$c;)V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/oe;->j:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ve$a;

    .line 11
    invoke-interface {v0}, Lcom/pspdfkit/internal/ve$a;->m()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 2

    .line 13
    iget v0, p0, Lcom/pspdfkit/internal/oe;->k:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/xe;->b(Lcom/pspdfkit/internal/xe$c;)V

    return-void
.end method

.method public final g()Z
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x3

    .line 10
    :try_start_0
    iget-object v4, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v4, v0}, Lcom/pspdfkit/internal/xe;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/te;

    move-result-object v0

    const-string v4, "assetProvider.getAsset(imageAttachmentId)"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->c()I

    move-result v4

    const/4 v5, 0x5

    const/4 v6, 0x4

    if-eq v4, v5, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->c()I

    move-result v4

    if-ne v4, v2, :cond_2

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->c()I

    move-result v4

    if-eq v4, v3, :cond_3

    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->c()I

    move-result v4

    if-ne v4, v6, :cond_5

    .line 19
    :cond_3
    iget-object v4, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v4, p0}, Lcom/pspdfkit/internal/xe;->a(Lcom/pspdfkit/internal/xe$c;)V

    .line 20
    iget-object v4, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/pspdfkit/internal/xe;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 21
    :cond_4
    :goto_0
    iput v6, p0, Lcom/pspdfkit/internal/oe;->k:I

    .line 24
    iget-object v4, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/pspdfkit/internal/xe;->a(Lcom/pspdfkit/internal/te;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/a0;->a([B)V

    .line 25
    invoke-super {p0}, Lcom/pspdfkit/internal/a0;->g()Z

    move-result v0
    :try_end_0
    .catch Lcom/pspdfkit/instant/exceptions/InstantException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 32
    iput v3, p0, Lcom/pspdfkit/internal/oe;->k:I

    new-array v2, v2, [Ljava/lang/Object;

    .line 33
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "Instant"

    const-string v4, "Could not load asset for %s"

    invoke-static {v3, v0, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    :goto_1
    return v1

    .line 37
    :cond_6
    invoke-super {p0}, Lcom/pspdfkit/internal/a0;->g()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->k()[B

    move-result-object v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x1

    .line 6
    :try_start_0
    iget-object v3, p0, Lcom/pspdfkit/internal/oe;->h:Lcom/pspdfkit/internal/xe;

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/xe;->a([B)Lcom/pspdfkit/internal/te;

    move-result-object v0

    const-string v3, "assetProvider.importAsse\u2026tmapData, MIME_TYPE_JPEG)"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    .line 10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    const-string v4, "imageAttachmentId"

    invoke-virtual {v0}, Lcom/pspdfkit/internal/te;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0, v1}, Lcom/pspdfkit/internal/pf;->setAdditionalData(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const-string v3, "contentType"

    const-string v4, "image/jpeg"

    invoke-interface {v0, v3, v4, v2}, Lcom/pspdfkit/internal/pf;->setAdditionalData(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/pspdfkit/instant/exceptions/InstantException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception v0

    new-array v2, v2, [Ljava/lang/Object;

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/a0;->i()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "Instant"

    const-string v4, "Could not import asset for %s"

    invoke-static {v3, v0, v4, v2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return v1
.end method

.method public final o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/oe;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-super {p0}, Lcom/pspdfkit/internal/a0;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

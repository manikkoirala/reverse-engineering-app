.class public final Lcom/pspdfkit/internal/yi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/yi$b;
    }
.end annotation


# static fields
.field private static final p:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/pspdfkit/internal/dm;

.field private final c:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final d:Lcom/pspdfkit/internal/na;

.field private final e:Lcom/pspdfkit/internal/zf;

.field private final f:Lcom/pspdfkit/internal/xc;

.field private final g:Lcom/pspdfkit/annotations/actions/ActionResolver;

.field private final h:Ljava/util/HashMap;

.field private i:Lio/reactivex/rxjava3/disposables/Disposable;

.field private j:Z

.field private k:Lcom/pspdfkit/internal/aj$a;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/xi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$0JGbFEwwvcDL117uHvWEIA8jVsY(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/yi;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$8i_7vxHLwMSB8YPIbIMLJRgJQ30(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/actions/RenditionAction;Lcom/pspdfkit/annotations/ScreenAnnotation;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/annotations/actions/RenditionAction;Lcom/pspdfkit/annotations/ScreenAnnotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$9W4wihsUnN4hrXHKzV8FYqIQCug(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$9dAPSs8-7s2c8KCASaM3kJd7cZw(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$Mw28K9oKaYX0rgm8cofvwBKjx3E(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->d(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$coSDT36HMXL6KZ_z4Oxusi_tDH4(Lcom/pspdfkit/internal/yi;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->e()V

    return-void
.end method

.method public static synthetic $r8$lambda$mS1MRc6WV4z7ZPMNU1GDP4sE-dw(Lcom/pspdfkit/internal/yi;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->d()V

    return-void
.end method

.method public static synthetic $r8$lambda$oOdmQZcKJXYZV77NPhLEmyMWF40(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$oSNDgzoieCErfvdFDrssh1R3H-A(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;Lcom/pspdfkit/annotations/RichMediaAnnotation;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;Lcom/pspdfkit/annotations/RichMediaAnnotation;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/internal/dm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/internal/na;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yi;->d:Lcom/pspdfkit/internal/na;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/yi;)Lcom/pspdfkit/annotations/actions/ActionResolver;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yi;->g:Lcom/pspdfkit/annotations/actions/ActionResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/yi;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->SCREEN:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->RICHMEDIA:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->LINK:Lcom/pspdfkit/annotations/AnnotationType;

    .line 2
    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/yi;->p:Ljava/util/EnumSet;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/dm;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/i;Lcom/pspdfkit/internal/a1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->j:Z

    .line 17
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->l:Z

    .line 23
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->m:Z

    const/4 v0, 0x1

    .line 29
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->n:Z

    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    .line 48
    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    .line 49
    iput-object p3, p0, Lcom/pspdfkit/internal/yi;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 50
    iput-object p4, p0, Lcom/pspdfkit/internal/yi;->g:Lcom/pspdfkit/annotations/actions/ActionResolver;

    .line 52
    new-instance p1, Lcom/pspdfkit/internal/yi$b;

    invoke-direct {p1, p0, v0}, Lcom/pspdfkit/internal/yi$b;-><init>(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/internal/yi$b-IA;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->f:Lcom/pspdfkit/internal/xc;

    .line 54
    new-instance p1, Lcom/pspdfkit/internal/na;

    invoke-direct {p1, p5}, Lcom/pspdfkit/internal/na;-><init>(Lcom/pspdfkit/internal/a1;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->d:Lcom/pspdfkit/internal/na;

    .line 55
    new-instance p3, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda5;

    invoke-direct {p3}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda5;-><init>()V

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/na;->a(Lcom/pspdfkit/internal/na$a;)V

    .line 57
    iput-object p2, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 112
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/wi;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/wi;->d()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/wi;

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_2

    return-object v0

    .line 114
    :cond_2
    invoke-static {p1}, Lcom/pspdfkit/internal/wi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 116
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-object p1
.end method

.method private a()V
    .locals 3

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    new-instance v1, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/yi;)V

    if-eqz v0, :cond_0

    .line 26
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 27
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 28
    invoke-interface {v1}, Lio/reactivex/rxjava3/functions/Action;->run()V

    :cond_0
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/pspdfkit/internal/yi;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/actions/RenditionAction;Lcom/pspdfkit/annotations/ScreenAnnotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-ne v2, p2, :cond_0

    goto :goto_0

    .line 52
    :cond_1
    invoke-static {p2}, Lcom/pspdfkit/internal/wi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_2

    return-void

    .line 53
    :cond_2
    sget-object p2, Lcom/pspdfkit/internal/yi$a;->b:[I

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/RenditionAction;->getRenditionActionType()Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_8

    const/4 p2, 0x2

    if-eq p1, p2, :cond_6

    const/4 p2, 0x3

    if-eq p1, p2, :cond_5

    const/4 p2, 0x4

    if-eq p1, p2, :cond_4

    const/4 p2, 0x5

    if-eq p1, p2, :cond_3

    goto :goto_1

    .line 67
    :cond_3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->d(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 68
    :cond_4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 69
    :cond_5
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 70
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->b()Z

    move-result p2

    if-eqz p2, :cond_9

    .line 71
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->g()V

    goto :goto_1

    .line 72
    :cond_6
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 74
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->b()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 75
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->d(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 77
    :cond_7
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 78
    :cond_8
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    :cond_9
    :goto_1
    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;Lcom/pspdfkit/annotations/RichMediaAnnotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-ne v2, p2, :cond_0

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {p2}, Lcom/pspdfkit/internal/wi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_2

    return-void

    .line 86
    :cond_2
    sget-object p2, Lcom/pspdfkit/internal/yi$a;->a:[I

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;->getRichMediaExecuteActionType()Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_5

    const/4 p2, 0x2

    if-eq p1, p2, :cond_4

    const/4 p2, 0x3

    if-eq p1, p2, :cond_3

    .line 99
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 100
    :cond_3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/wi;)I

    move-result p1

    add-int/lit16 p1, p1, -0x1388

    .line 101
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 103
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/aj;->a(I)V

    goto :goto_1

    .line 104
    :cond_4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/wi;)I

    move-result p1

    add-int/lit16 p1, p1, 0x1388

    .line 105
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 107
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/aj;->a(I)V

    goto :goto_1

    .line 108
    :cond_5
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 109
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->b()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 110
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->g()V

    :cond_6
    :goto_1
    return-void
.end method

.method private a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/dm$e;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->h()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 11
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/internal/yi;->p:Ljava/util/EnumSet;

    .line 12
    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p2

    const/4 v1, 0x1

    invoke-interface {p1, v0, p2, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAllAnnotationsOfTypeAsync(Ljava/util/EnumSet;II)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda2;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/yi;)V

    .line 14
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Observable;->doOnComplete(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda3;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/yi;)V

    new-instance v0, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda4;

    invoke-direct {v0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda4;-><init>()V

    .line 20
    invoke-virtual {p1, p2, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->i:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.MediaAnnotation"

    const-string v2, "Error while retrieving video annotations."

    .line 21
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    if-ne v1, p1, :cond_0

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/aj;

    if-eqz v1, :cond_0

    return-object v1

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->c:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->isVideoPlaybackEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 7
    :cond_2
    new-instance v0, Lcom/pspdfkit/internal/aj;

    iget-object v1, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/aj;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;)V

    .line 8
    new-instance v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;->LAYOUT:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>(Landroid/graphics/RectF;Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$SizingMode;)V

    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/yi;->k:Lcom/pspdfkit/internal/aj$a;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/aj;->setOnMediaPlaybackChangeListener(Lcom/pspdfkit/internal/aj$a;)V

    .line 13
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/aj;->setMediaContent(Lcom/pspdfkit/internal/wi;)V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method private static synthetic b(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    .line 1
    instance-of p0, p0, Lcom/pspdfkit/annotations/LinkAnnotation;

    return p0
.end method

.method private static synthetic c(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/yi;->p:Ljava/util/EnumSet;

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private synthetic d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->l:Z

    return-void
.end method

.method private d(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->n:Z

    if-nez v0, :cond_3

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/wi;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/wi;->d()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/pspdfkit/annotations/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/wi;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 12
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    .line 17
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    :cond_3
    :goto_1
    return-void
.end method

.method private synthetic e()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->l:Z

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->j:Z

    if-eqz v0, :cond_0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->i()V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->a()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/aj;

    if-eqz v2, :cond_0

    .line 6
    invoke-virtual {v2}, Lcom/pspdfkit/internal/aj;->i()V

    const/4 v3, 0x0

    .line 7
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/aj;->setMediaContent(Lcom/pspdfkit/internal/wi;)V

    .line 8
    iget-object v4, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private i()V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->m:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->n:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->l:Z

    if-eqz v0, :cond_a

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xi;

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/wi;

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/internal/wi;->e()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    .line 7
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v5

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xi;->b()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 8
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v4

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xi;->a()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/xi;->d()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 10
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_1

    .line 11
    :cond_2
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 12
    invoke-virtual {v4}, Lcom/pspdfkit/internal/aj;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 13
    invoke-virtual {v4}, Lcom/pspdfkit/internal/aj;->g()V

    .line 14
    :cond_3
    :goto_1
    invoke-virtual {v1}, Lcom/pspdfkit/internal/xi;->c()I

    move-result v4

    .line 15
    invoke-direct {p0, v3}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 17
    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/aj;->a(I)V

    :cond_4
    const/4 v3, 0x0

    .line 18
    iput-object v3, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    goto :goto_0

    .line 19
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    .line 20
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 21
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yi;->c(Lcom/pspdfkit/internal/wi;)V

    goto :goto_2

    .line 22
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    .line 23
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_8

    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->g()Z

    move-result v2

    if-nez v2, :cond_8

    .line 24
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v2

    if-eq v2, v3, :cond_8

    .line 29
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->n:Z

    :cond_a
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/wi;)I
    .locals 0

    .line 117
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->getPosition()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final a(Lcom/pspdfkit/annotations/actions/RenditionAction;)V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 37
    :cond_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/actions/RenditionAction;->getScreenAnnotationAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 38
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/actions/RenditionAction;)V

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;)V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    if-nez v0, :cond_0

    return-void

    .line 43
    :cond_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;->getRichMediaAnnotationAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 44
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/yi;Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;)V

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/aj$a;)V
    .locals 2

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->k:Lcom/pspdfkit/internal/aj$a;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/aj;

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/aj;->setOnMediaPlaybackChangeListener(Lcom/pspdfkit/internal/aj$a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm$e;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->e:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/yi;->a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/dm$e;)V

    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->m:Z

    .line 23
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/internal/yi;->o:Ljava/util/List;

    :cond_0
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda0;-><init>()V

    .line 31
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    .line 32
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yi$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/yi;)V

    .line 33
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method final a(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/aj;

    if-eqz v3, :cond_1

    if-nez v2, :cond_2

    .line 123
    invoke-static {v3, p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v3, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    return v2
.end method

.method final b()Lcom/pspdfkit/internal/xc;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->f:Lcom/pspdfkit/internal/xc;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    return v0
.end method

.method public final c(Lcom/pspdfkit/internal/wi;)V
    .locals 1

    .line 3
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->h()V

    :cond_0
    return-void
.end method

.method public final d(Lcom/pspdfkit/internal/wi;)V
    .locals 3

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/wi;

    if-ne v1, p1, :cond_0

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/aj;

    if-eqz v1, :cond_0

    .line 23
    invoke-virtual {v1}, Lcom/pspdfkit/internal/aj;->i()V

    const/4 v0, 0x0

    .line 24
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/aj;->setMediaContent(Lcom/pspdfkit/internal/wi;)V

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/yi;->b:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 27
    :cond_1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yi;->b(Lcom/pspdfkit/internal/wi;)Lcom/pspdfkit/internal/aj;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/aj;->i()V

    :cond_2
    :goto_0
    return-void
.end method

.method final f()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yi;->j:Z

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->h()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->j:Z

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->n:Z

    :cond_0
    return-void
.end method

.method final g()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yi;->j:Z

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->i()V

    return-void
.end method

.method public final recycle()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yi;->h()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yi;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/y1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/special_mode/controller/AnnotationSelectionController;
.implements Lcom/pspdfkit/internal/a2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/y1$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/z1;

.field private final b:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private final c:Landroid/graphics/RectF;

.field private final d:Landroid/graphics/RectF;

.field private final e:Lcom/pspdfkit/internal/an;

.field private final f:Landroid/graphics/RectF;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/Boolean;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/pspdfkit/utils/Size;

.field private v:Z

.field private w:I

.field private x:Z

.field private y:Lcom/pspdfkit/internal/la;

.field private z:Lcom/pspdfkit/internal/ti;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/z1;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V
    .locals 4

    const-string v0, "selectionLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfConfiguration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themeConfiguration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    .line 6
    iput-object p2, p0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 14
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    .line 17
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/an;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/an;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    .line 23
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->f:Landroid/graphics/RectF;

    const/4 v0, 0x1

    .line 26
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    .line 29
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->h:Z

    .line 32
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->i:Z

    .line 35
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->j:Z

    .line 38
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->k:Z

    .line 81
    new-instance v0, Lcom/pspdfkit/utils/Size;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->u:Lcom/pspdfkit/utils/Size;

    .line 100
    new-instance v0, Lcom/pspdfkit/internal/la;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3, v1}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/y1;->y:Lcom/pspdfkit/internal/la;

    .line 106
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/z1;->setPresenter(Lcom/pspdfkit/internal/a2;)V

    .line 107
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationRotationEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->h:Z

    .line 108
    invoke-virtual {p0, p2, p3}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/ka;Lcom/pspdfkit/annotations/Annotation;FFFFLandroid/graphics/RectF;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    .line 5734
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    .line 5742
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v2

    iget v3, v0, Lcom/pspdfkit/internal/y1;->w:I

    .line 5743
    invoke-static {}, Lcom/pspdfkit/internal/cq;->a()Ljava/util/List;

    move-result-object v4

    .line 5744
    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x2

    if-eqz v4, :cond_1

    .line 5745
    invoke-static {v3}, Lcom/pspdfkit/internal/cq$a;->a(I)I

    move-result v3

    .line 5746
    invoke-static {}, Lcom/pspdfkit/internal/cq;->a()Ljava/util/List;

    move-result-object v4

    .line 5747
    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->indexOf(Ljava/util/List;Ljava/lang/Object;)I

    move-result v4

    if-ltz v4, :cond_3

    .line 5748
    invoke-static {}, Lcom/pspdfkit/internal/cq;->a()Ljava/util/List;

    move-result-object v2

    add-int/2addr v4, v3

    invoke-static {}, Lcom/pspdfkit/internal/cq;->a()Ljava/util/List;

    move-result-object v3

    .line 5749
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    rem-int/2addr v4, v3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/z1$b;

    goto :goto_0

    .line 5750
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/cq;->b()Ljava/util/List;

    move-result-object v4

    .line 5751
    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0xb4

    if-eq v3, v4, :cond_2

    const/16 v4, 0x10e

    if-ne v3, v4, :cond_3

    .line 5752
    :cond_2
    invoke-static {}, Lcom/pspdfkit/internal/cq;->b()Ljava/util/List;

    move-result-object v3

    .line 5753
    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->indexOf(Ljava/util/List;Ljava/lang/Object;)I

    move-result v2

    .line 5754
    invoke-static {}, Lcom/pspdfkit/internal/cq;->b()Ljava/util/List;

    move-result-object v3

    add-int/2addr v2, v5

    invoke-static {}, Lcom/pspdfkit/internal/cq;->b()Ljava/util/List;

    move-result-object v4

    .line 5755
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    rem-int/2addr v2, v4

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/z1$b;

    :cond_3
    :goto_0
    if-nez v2, :cond_4

    const/4 v2, -0x1

    goto :goto_1

    .line 5756
    :cond_4
    sget-object v3, Lcom/pspdfkit/internal/y1$a;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v3, v2

    :goto_1
    const/4 v3, 0x3

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    packed-switch v2, :pswitch_data_0

    move-object v10, v0

    .line 5797
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 5798
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Touched unhandled handle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5799
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/high16 v6, -0x40800000    # -1.0f

    goto :goto_3

    :pswitch_1
    const/high16 v6, -0x40800000    # -1.0f

    goto :goto_2

    :pswitch_2
    const/4 v3, 0x2

    const/4 v6, 0x0

    goto :goto_2

    :pswitch_3
    const/4 v3, 0x2

    :goto_2
    const/high16 v8, 0x3f800000    # 1.0f

    goto :goto_4

    :pswitch_4
    const/4 v3, 0x1

    :goto_3
    const/4 v8, 0x0

    goto :goto_4

    :pswitch_5
    const/4 v3, 0x1

    goto :goto_4

    :pswitch_6
    const/4 v3, 0x0

    const/4 v6, 0x0

    goto :goto_4

    :pswitch_7
    const/4 v3, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    .line 5808
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v2

    .line 5810
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v10

    invoke-virtual {v10}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v10

    sget-object v11, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    if-eq v10, v11, :cond_5

    .line 5811
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v10

    invoke-virtual {v10}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v10

    sget-object v11, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    if-eq v10, v11, :cond_5

    .line 5812
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v10

    invoke-virtual {v10}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v10

    sget-object v11, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    if-eq v10, v11, :cond_5

    .line 5813
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v10

    invoke-virtual {v10}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v10

    sget-object v11, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    if-ne v10, v11, :cond_6

    .line 5816
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v10

    invoke-interface {v10}, Lcom/pspdfkit/internal/pf;->needsFlippedContentSize()Z

    move-result v10

    if-eqz v10, :cond_6

    neg-float v6, v6

    move/from16 v20, v8

    move v8, v6

    move/from16 v6, v20

    :cond_6
    add-int/lit8 v10, v3, 0x2

    .line 5822
    rem-int/lit8 v10, v10, 0x4

    .line 5823
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v11

    invoke-interface {v11}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v11

    add-int/2addr v11, v2

    int-to-double v11, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v11

    move/from16 v13, p3

    float-to-double v13, v13

    .line 5826
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v15

    mul-double v15, v15, v13

    move/from16 v9, p4

    float-to-double v4, v9

    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v18, v18, v4

    sub-double v15, v15, v18

    move/from16 v18, v8

    float-to-double v7, v6

    mul-double v15, v15, v7

    .line 5827
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double v6, v6, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    mul-double v11, v11, v4

    add-double/2addr v11, v6

    move/from16 v8, v18

    float-to-double v4, v8

    mul-double v11, v11, v4

    .line 5828
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    iget-object v5, v0, Lcom/pspdfkit/internal/y1;->f:Landroid/graphics/RectF;

    invoke-interface {v4, v5}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v4

    .line 5829
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v4}, Landroid/graphics/RectF;->sort()V

    .line 5831
    new-instance v5, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-direct {v5, v6, v7}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 5833
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v6

    invoke-interface {v6}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v6

    add-int/2addr v6, v2

    int-to-float v6, v6

    .line 5834
    invoke-static {v5, v6}, Lcom/pspdfkit/internal/di;->b(Lcom/pspdfkit/utils/Size;F)Lcom/pspdfkit/utils/Size;

    move-result-object v5

    .line 5843
    new-instance v6, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 5845
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v7

    invoke-interface {v7}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v7

    add-int/2addr v7, v2

    int-to-float v7, v7

    .line 5846
    invoke-static {v6, v7}, Lcom/pspdfkit/internal/di;->a(Lcom/pspdfkit/utils/Size;F)Ljava/util/ArrayList;

    move-result-object v6

    .line 5852
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    .line 5853
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    add-int/lit8 v13, v10, 0x1

    .line 5854
    rem-int/lit8 v13, v13, 0x4

    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/PointF;

    add-int/lit8 v18, v10, 0x5

    .line 5855
    rem-int/lit8 v9, v18, 0x4

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    .line 5859
    iget v1, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 p3, v6

    iget v6, v5, Lcom/pspdfkit/utils/Size;->width:F

    neg-float v6, v6

    move-object/from16 p4, v7

    const/4 v7, 0x2

    int-to-float v0, v7

    div-float/2addr v6, v0

    invoke-static {v1, v6}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v1

    .line 5860
    iget v6, v8, Landroid/graphics/PointF;->y:F

    iget v7, v5, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v7, v0

    invoke-static {v6, v7}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v6

    .line 5861
    iget v7, v8, Landroid/graphics/PointF;->x:F

    move/from16 v18, v6

    iget v6, v5, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v6, v0

    invoke-static {v7, v6}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v6

    .line 5862
    iget v7, v8, Landroid/graphics/PointF;->y:F

    iget v5, v5, Lcom/pspdfkit/utils/Size;->height:F

    neg-float v5, v5

    div-float/2addr v5, v0

    invoke-static {v7, v5}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v0

    const/4 v5, 0x2

    new-array v7, v5, [F

    const/16 v17, 0x0

    aput p5, v7, v17

    .line 5865
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v5

    move/from16 v19, v6

    float-to-double v5, v5

    add-double/2addr v5, v15

    double-to-float v5, v5

    const/4 v6, 0x1

    aput v5, v7, v6

    invoke-static {v7}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v5

    const/4 v7, 0x2

    new-array v7, v7, [F

    aput p6, v7, v17

    .line 5866
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v15

    move-object/from16 v16, v7

    float-to-double v6, v15

    sub-double/2addr v6, v11

    double-to-float v6, v6

    const/4 v7, 0x1

    aput v6, v16, v7

    invoke-static/range {v16 .. v16}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v7

    const/4 v11, 0x0

    .line 5867
    invoke-virtual {v4, v11, v11, v5, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5873
    new-instance v5, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-direct {v5, v7, v11}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 5875
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v7

    invoke-interface {v7}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v7

    add-int/2addr v7, v2

    int-to-float v7, v7

    .line 5876
    invoke-static {v5, v7}, Lcom/pspdfkit/internal/di;->a(Lcom/pspdfkit/utils/Size;F)Ljava/util/ArrayList;

    move-result-object v5

    .line 5882
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 5883
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    .line 5884
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    .line 5885
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 5887
    new-instance v9, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v11

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v12

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 5889
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v11

    invoke-interface {v11}, Lcom/pspdfkit/internal/pf;->getRotation()I

    move-result v11

    add-int/2addr v11, v2

    int-to-float v2, v11

    .line 5890
    invoke-static {v9, v2}, Lcom/pspdfkit/internal/di;->b(Lcom/pspdfkit/utils/Size;F)Lcom/pspdfkit/utils/Size;

    move-result-object v2

    .line 5898
    iget v9, v2, Lcom/pspdfkit/utils/Size;->width:F

    invoke-virtual/range {p7 .. p7}, Landroid/graphics/RectF;->width()F

    move-result v11

    sub-float/2addr v9, v11

    .line 5899
    iget v2, v2, Lcom/pspdfkit/utils/Size;->height:F

    invoke-virtual/range {p7 .. p7}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    sub-float/2addr v2, v11

    if-eqz v1, :cond_7

    .line 5907
    iget v0, v8, Landroid/graphics/PointF;->y:F

    iget v1, v14, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 5908
    iget v1, v7, Landroid/graphics/PointF;->y:F

    iget v3, v10, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v3

    sub-float/2addr v1, v0

    move-object/from16 v10, p0

    .line 5910
    iget-object v0, v10, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    neg-float v2, v2

    add-float/2addr v2, v1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v9, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_5

    :cond_7
    move-object/from16 v10, p0

    if-eqz v0, :cond_8

    move-object/from16 v0, p4

    .line 5913
    iget v0, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v1, p3

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    .line 5914
    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v3, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    sub-float/2addr v1, v0

    .line 5916
    iget-object v0, v10, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    add-float/2addr v9, v1

    neg-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v9, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_5

    :cond_8
    move-object/from16 v1, p3

    move-object/from16 v0, p4

    if-eqz v19, :cond_9

    .line 5919
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 5920
    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v3, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v3

    sub-float/2addr v1, v0

    .line 5922
    iget-object v0, v10, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    neg-float v3, v9

    neg-float v5, v1

    neg-float v2, v2

    sub-float/2addr v2, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v5, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_5

    :cond_9
    if-eqz v18, :cond_a

    .line 5925
    iget v0, v8, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    .line 5926
    iget v1, v7, Landroid/graphics/PointF;->x:F

    iget v3, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    sub-float/2addr v1, v0

    .line 5928
    iget-object v0, v10, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    neg-float v3, v1

    sub-float/2addr v9, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v2, v9, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5934
    :cond_a
    :goto_5
    iget-object v0, v10, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v2, v10, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    move-object/from16 v5, p7

    iget v7, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v7

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_b

    .line 5935
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v7, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v7

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_b

    .line 5936
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v3, v2, Landroid/graphics/RectF;->top:F

    iget v7, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v7

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_b

    .line 5937
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_b

    .line 5939
    invoke-virtual/range {p2 .. p2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v4, v1}, Lcom/pspdfkit/internal/pf;->setContentSize(Landroid/graphics/RectF;Z)V

    goto :goto_6

    :cond_b
    const/4 v1, 0x0

    .line 5943
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 1

    .line 1181
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const-string v0, "annotation"

    .line 1182
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1983
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0x15

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private final b(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V
    .locals 20

    move-object/from16 v8, p0

    move-object/from16 v9, p4

    .line 1229
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-boolean v0, v8, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz v0, :cond_13

    iget-boolean v0, v8, Lcom/pspdfkit/internal/y1;->s:Z

    if-eqz v0, :cond_0

    goto/16 :goto_7

    .line 1230
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->g()F

    move-result v0

    add-float v3, v0, p1

    .line 1231
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->h()F

    move-result v0

    add-float v4, v0, p2

    .line 1232
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v15

    const-string v0, "selectionLayout.layoutParams!!.pageRect.pageRect"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1233
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget-object v1, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/z1;->getPdfViewGroup()Lcom/pspdfkit/internal/en;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/en;->getPdfRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    const/4 v0, 0x2

    new-array v1, v0, [F

    .line 1236
    iget-object v2, v8, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/an;->b()F

    move-result v2

    const/4 v14, 0x0

    aput v2, v1, v14

    iget-object v2, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    const/4 v13, 0x1

    aput v2, v1, v13

    invoke-static {v1}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v1

    new-array v2, v0, [F

    .line 1237
    iget-object v5, v8, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/an;->c()F

    move-result v5

    aput v5, v2, v14

    iget-object v5, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    aput v5, v2, v13

    invoke-static {v2}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v2

    new-array v5, v0, [F

    .line 1238
    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/an;->a()F

    move-result v6

    aput v6, v5, v14

    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    aput v6, v5, v13

    invoke-static {v5}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v5

    new-array v6, v0, [F

    .line 1239
    iget-object v7, v8, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    invoke-virtual {v7}, Lcom/pspdfkit/internal/an;->d()F

    move-result v7

    aput v7, v6, v14

    iget-object v7, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    aput v7, v6, v13

    invoke-static {v6}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v6

    new-array v7, v0, [F

    .line 1244
    iget-object v10, v8, Lcom/pspdfkit/internal/y1;->u:Lcom/pspdfkit/utils/Size;

    iget v10, v10, Lcom/pspdfkit/utils/Size;->width:F

    aput v10, v7, v14

    iget v10, v15, Landroid/graphics/RectF;->right:F

    iget v11, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    aput v10, v7, v13

    invoke-static {v7}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v17

    new-array v0, v0, [F

    .line 1245
    iget-object v7, v8, Lcom/pspdfkit/internal/y1;->u:Lcom/pspdfkit/utils/Size;

    iget v7, v7, Lcom/pspdfkit/utils/Size;->height:F

    aput v7, v0, v14

    iget v7, v15, Landroid/graphics/RectF;->top:F

    iget v10, v15, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v10

    aput v7, v0, v13

    invoke-static {v0}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v18

    .line 1251
    iget v0, v15, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    .line 1252
    iget v7, v15, Landroid/graphics/RectF;->right:F

    sub-float v7, v7, v17

    sub-float/2addr v7, v0

    .line 1253
    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1254
    iget v1, v15, Landroid/graphics/RectF;->left:F

    add-float v1, v1, v17

    iget v7, v15, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v7

    sub-float/2addr v2, v7

    .line 1255
    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1256
    iget v2, v15, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    .line 1257
    iget v7, v15, Landroid/graphics/RectF;->top:F

    sub-float v7, v7, v18

    sub-float/2addr v7, v2

    .line 1258
    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1259
    iget v5, v15, Landroid/graphics/RectF;->bottom:F

    add-float v5, v5, v18

    iget v7, v15, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v7

    sub-float/2addr v6, v7

    .line 1260
    invoke-static {v4, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 1261
    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1262
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->c()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->d()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->e()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->b()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1263
    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    invoke-virtual {v6}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationLimitedToPageBounds()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1264
    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    cmpl-float v10, v3, v7

    if-ltz v10, :cond_1

    move v0, v1

    :cond_1
    iput v0, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v4, v7

    if-ltz v0, :cond_2

    move v2, v5

    .line 1265
    :cond_2
    iput v2, v6, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 1270
    :cond_3
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 1271
    iput v4, v0, Landroid/graphics/RectF;->top:F

    .line 1273
    :goto_0
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1274
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_1

    .line 1276
    :cond_4
    iget-object v6, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->c()Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v0, 0x0

    :cond_5
    iput v0, v6, Landroid/graphics/RectF;->left:F

    .line 1277
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->d()Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v1, 0x0

    :cond_6
    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1278
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v2, 0x0

    :cond_7
    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 1279
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v5, 0x0

    :cond_8
    iput v5, v0, Landroid/graphics/RectF;->top:F

    .line 1282
    :goto_1
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    const/16 v19, 0x0

    if-eqz v0, :cond_a

    .line 1283
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    if-eq v0, v1, :cond_a

    .line 1284
    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/y1;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1286
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, v14}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1287
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    :cond_9
    move-object/from16 v2, v19

    :goto_2
    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v5, v17

    move/from16 v6, v18

    move-object v7, v15

    .line 1288
    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/internal/ka;Lcom/pspdfkit/annotations/Annotation;FFFFLandroid/graphics/RectF;)V

    .line 1292
    :cond_a
    iget-boolean v0, v8, Lcom/pspdfkit/internal/y1;->l:Z

    if-eqz v0, :cond_c

    .line 1293
    iget-boolean v0, v8, Lcom/pspdfkit/internal/y1;->o:Z

    if-eqz v0, :cond_c

    .line 1294
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->f()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/pspdfkit/internal/y1;->p()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1297
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    iget-object v10, v0, Lcom/pspdfkit/internal/z1;->j:Lcom/pspdfkit/internal/up;

    .line 1298
    iget-object v11, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    .line 1299
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v12

    .line 1301
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->f()Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 1302
    iget-object v1, v8, Lcom/pspdfkit/internal/y1;->d:Landroid/graphics/RectF;

    .line 1303
    iget-boolean v2, v8, Lcom/pspdfkit/internal/y1;->n:Z

    const/4 v3, 0x1

    move-object v13, v15

    const/4 v4, 0x0

    move-object v14, v0

    move-object v0, v15

    move-object v15, v1

    move/from16 v16, v2

    .line 1304
    invoke-virtual/range {v10 .. v18}, Lcom/pspdfkit/internal/up;->a(Landroid/graphics/RectF;Lcom/pspdfkit/internal/z1$b;Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/RectF;ZFF)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1315
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/internal/y1;->m()V

    .line 1317
    :cond_b
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->g()F

    move-result v1

    iget-object v2, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/RectF;->left:F

    sub-float v5, p1, v5

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v5, v2

    add-float/2addr v5, v1

    invoke-virtual {v9, v5}, Lcom/pspdfkit/internal/ka;->a(F)V

    .line 1318
    invoke-virtual/range {p4 .. p4}, Lcom/pspdfkit/internal/ka;->h()F

    move-result v1

    iget-object v2, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/RectF;->top:F

    sub-float v5, p2, v5

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v2

    add-float/2addr v5, v1

    invoke-virtual {v9, v5}, Lcom/pspdfkit/internal/ka;->b(F)V

    goto :goto_3

    :cond_c
    move-object v0, v15

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1322
    :goto_3
    iget-object v1, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    .line 1323
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v5

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1324
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v5

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1325
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v5

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1326
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 1327
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 1332
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v14, 0x0

    :goto_4
    if-ge v14, v0, :cond_11

    .line 1334
    iget-object v1, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1, v14}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 1336
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getPageRect()Lcom/pspdfkit/utils/PageRect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v2

    iget-object v5, v8, Lcom/pspdfkit/internal/y1;->c:Landroid/graphics/RectF;

    .line 1337
    iget v6, v2, Landroid/graphics/RectF;->left:F

    iget v7, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v7

    iput v6, v2, Landroid/graphics/RectF;->left:F

    .line 1338
    iget v6, v2, Landroid/graphics/RectF;->top:F

    iget v7, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v7

    iput v6, v2, Landroid/graphics/RectF;->top:F

    .line 1339
    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget v7, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v6, v7

    iput v6, v2, Landroid/graphics/RectF;->right:F

    .line 1340
    iget v6, v2, Landroid/graphics/RectF;->bottom:F

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v6, v5

    iput v6, v2, Landroid/graphics/RectF;->bottom:F

    .line 1341
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->g()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1344
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getContentScaler()Lcom/pspdfkit/internal/k0;

    move-result-object v2

    if-eqz v2, :cond_d

    iget-object v5, v8, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    move-object v6, v2

    check-cast v6, Lcom/pspdfkit/internal/views/annotations/m;

    move-object/from16 v7, p3

    invoke-virtual {v6, v9, v5, v7}, Lcom/pspdfkit/internal/views/annotations/m;->a(Lcom/pspdfkit/internal/ka;Lcom/pspdfkit/configuration/PdfConfiguration;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_e

    goto :goto_5

    :cond_d
    move-object/from16 v7, p3

    :cond_e
    move-object/from16 v2, v19

    .line 1345
    :goto_5
    invoke-virtual {v8, v1, v2}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/k0;)Z

    goto :goto_6

    :cond_f
    move-object/from16 v7, p3

    const/4 v4, 0x1

    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    :cond_10
    move-object/from16 v7, p3

    goto :goto_4

    :cond_11
    if-eqz v4, :cond_12

    .line 1358
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getSelectionLayoutHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1359
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getSelectionLayoutHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1364
    :cond_12
    iget-object v0, v8, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wq;->c()V

    :cond_13
    :goto_7
    return-void
.end method

.method private final p()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 8
    :goto_0
    instance-of v3, v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v3, :cond_2

    .line 9
    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->f:Landroid/graphics/RectF;

    invoke-interface {v0, v3}, Lcom/pspdfkit/internal/pf;->getContentSize(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/ka;
    .locals 10

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    if-nez v0, :cond_3

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->e()Z

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;Z)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 17
    new-instance v1, Lcom/pspdfkit/internal/ka;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/ka;-><init>(I)V

    goto/16 :goto_2

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->g()Z

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/pspdfkit/internal/z1;->b(Landroid/view/MotionEvent;Z)Lcom/pspdfkit/internal/z1$b;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string p1, "touchedScaleHandle"

    .line 19
    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    .line 172
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto/16 :goto_0

    .line 173
    :pswitch_1
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 174
    :pswitch_2
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 175
    :pswitch_3
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 176
    :pswitch_4
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 177
    :pswitch_5
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 178
    :pswitch_6
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 179
    :pswitch_7
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    goto :goto_0

    .line 180
    :pswitch_8
    new-instance p1, Lcom/pspdfkit/internal/ka;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V

    :goto_0
    move-object v1, p1

    .line 181
    new-instance p1, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/ka;->a(Landroid/graphics/RectF;)V

    goto :goto_2

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 183
    iget-boolean p1, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/pspdfkit/internal/y1;->s:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/pspdfkit/internal/y1;->k:Z

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/pspdfkit/internal/y1;->q:Z

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    .line 184
    new-instance p1, Lcom/pspdfkit/internal/ka;

    invoke-direct {p1, v1}, Lcom/pspdfkit/internal/ka;-><init>(Ljava/lang/Object;)V

    move-object v1, p1

    :cond_3
    :goto_2
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 3

    .line 6052
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    if-eqz v0, :cond_1

    .line 6053
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 6054
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6055
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->f()V

    .line 6057
    :cond_0
    iput-boolean v2, p0, Lcom/pspdfkit/internal/y1;->r:Z

    .line 6058
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_1
    return-void
.end method

.method public final a(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V
    .locals 7

    const-string v0, "mode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1984
    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->a()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, -0x1

    if-eq v0, v3, :cond_c

    if-eqz p3, :cond_15

    .line 1985
    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/la;->a()I

    move-result p1

    iget-object p2, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/z1;->getEditHandleCenters()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-ge p1, p2, :cond_15

    .line 1986
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-eq p1, v2, :cond_0

    goto/16 :goto_5

    .line 1991
    :cond_0
    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/la;->a()I

    move-result p1

    .line 1992
    iget-object p2, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object p2

    if-eqz p2, :cond_15

    .line 1993
    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p4

    if-nez p4, :cond_1

    goto/16 :goto_5

    .line 1996
    :cond_1
    invoke-static {p4}, Lcom/pspdfkit/internal/ao;->d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object v0

    .line 2846
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2847
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2848
    check-cast v3, Landroid/graphics/PointF;

    .line 2849
    new-instance v4, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3701
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3702
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_3

    goto/16 :goto_5

    .line 3704
    :cond_3
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result p3

    invoke-direct {v0, v3, p3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3706
    iget-object p3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/z1;->getPdfToViewTransformation()Landroid/graphics/Matrix;

    move-result-object p3

    .line 3707
    invoke-static {v0, p3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 3708
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/z1;->getPdfViewGroup()Lcom/pspdfkit/internal/en;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/en;->getPdfRect()Landroid/graphics/RectF;

    move-result-object v3

    .line 3711
    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/RectF;->right:F

    .line 3712
    invoke-static {v4, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 3713
    iput v4, v0, Landroid/graphics/PointF;->x:F

    .line 3714
    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget v5, v3, Landroid/graphics/RectF;->bottom:F

    iget v3, v3, Landroid/graphics/RectF;->top:F

    .line 3715
    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 3716
    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 3719
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3720
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->z:Lcom/pspdfkit/internal/ti;

    if-eqz v3, :cond_4

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/ti;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    .line 3723
    :cond_4
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    invoke-virtual {v3, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 3724
    instance-of v3, p4, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v3, :cond_5

    .line 3725
    move-object v2, p4

    check-cast v2, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v3, v4, :cond_b

    .line 3726
    invoke-virtual {v2, v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setCallOutPoints(Ljava/util/List;)V

    .line 3727
    invoke-static {v2}, Lcom/pspdfkit/utils/FreeTextAnnotationUtils;->placeCallOutPoints(Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    goto :goto_1

    .line 3731
    :cond_5
    sget v3, Lcom/pspdfkit/internal/ao;->a:I

    const-string v3, "annotation"

    .line 3732
    invoke-static {p4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "points"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4610
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_a

    if-eq v3, v2, :cond_8

    const/16 v2, 0x13

    if-eq v3, v2, :cond_7

    const/16 v2, 0x14

    if-eq v3, v2, :cond_6

    goto :goto_1

    .line 4632
    :cond_6
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/pf;->setPointsWithoutCoreSync(Ljava/util/List;)V

    goto :goto_1

    .line 4633
    :cond_7
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/pf;->setPointsWithoutCoreSync(Ljava/util/List;)V

    goto :goto_1

    .line 4634
    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_9

    goto :goto_1

    .line 4638
    :cond_9
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/pf;->setPointsWithoutCoreSync(Ljava/util/List;)V

    goto :goto_1

    .line 4659
    :cond_a
    move-object v2, p4

    check-cast v2, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setCallOutPoints(Ljava/util/List;)V

    .line 4660
    :cond_b
    :goto_1
    invoke-static {v0, p3}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 4662
    iget-object p3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/z1;->getEditHandleCenters()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/PointF;

    invoke-virtual {p1, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 4663
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 4666
    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/a;->getPageRect()Lcom/pspdfkit/utils/PageRect;

    move-result-object p1

    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/pspdfkit/utils/PageRect;->set(Landroid/graphics/RectF;)V

    .line 4667
    invoke-interface {p2}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    .line 4670
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->b()V

    .line 4671
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->k()V

    goto/16 :goto_5

    .line 4672
    :cond_c
    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    if-ne v0, v3, :cond_d

    .line 4673
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/wq;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_5

    :cond_d
    const/4 v0, 0x0

    if-eqz p3, :cond_13

    .line 4674
    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v3

    if-eqz v3, :cond_13

    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-eq v3, v2, :cond_e

    goto/16 :goto_3

    .line 4677
    :cond_e
    iget-boolean v2, p0, Lcom/pspdfkit/internal/y1;->o:Z

    if-eqz v2, :cond_f

    goto/16 :goto_3

    .line 4679
    :cond_f
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4680
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/z1;->getPdfToViewTransformation()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 4682
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3, v1}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 4683
    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    if-nez v1, :cond_10

    goto/16 :goto_3

    .line 4684
    :cond_10
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 4686
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->z:Lcom/pspdfkit/internal/ti;

    if-eqz v3, :cond_11

    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/ti;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 4689
    :cond_11
    sget v3, Lcom/pspdfkit/internal/z1;->z:I

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    const-string v3, "annotation.boundingBox"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/pspdfkit/internal/ka;->a()Lcom/pspdfkit/internal/la;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v3

    const-string v4, "rectF"

    .line 4690
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "scaleHandle"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5710
    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 5719
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    move-object v3, v0

    goto/16 :goto_2

    .line 5720
    :pswitch_1
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v5, v4

    iget v4, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    add-float/2addr v1, v4

    invoke-direct {v3, v5, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5721
    :pswitch_2
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    add-float/2addr v1, v5

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5722
    :pswitch_3
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    add-float/2addr v1, v5

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5723
    :pswitch_4
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v5, v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-direct {v3, v5, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5724
    :pswitch_5
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5725
    :pswitch_6
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v5, v4

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v5, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5726
    :pswitch_7
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 5727
    :pswitch_8
    new-instance v3, Landroid/graphics/PointF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    :goto_2
    if-nez v3, :cond_12

    goto :goto_3

    .line 5728
    :cond_12
    new-instance v1, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-direct {v1, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_4

    :cond_13
    :goto_3
    move-object v1, v0

    :goto_4
    if-eqz v1, :cond_14

    .line 5729
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/pspdfkit/internal/y1;->b(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V

    .line 5730
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :cond_14
    if-nez v0, :cond_15

    .line 5733
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/y1;->b(FFLandroid/view/MotionEvent;Lcom/pspdfkit/internal/ka;)V

    :cond_15
    :goto_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(I)V
    .locals 0

    .line 6062
    iput p1, p0, Lcom/pspdfkit/internal/y1;->w:I

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V
    .locals 2

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themeConfiguration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/f2;)V

    .line 3
    iget p2, p2, Lcom/pspdfkit/internal/f2;->a:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-lt p2, v1, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/pspdfkit/internal/y1;->i:Z

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeEnabled()Z

    move-result p2

    iput-boolean p2, p0, Lcom/pspdfkit/internal/y1;->j:Z

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getSelectedAnnotationResizeGuidesEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->l:Z

    .line 8
    iput-boolean v1, p0, Lcom/pspdfkit/internal/y1;->g:Z

    .line 9
    iput-boolean v1, p0, Lcom/pspdfkit/internal/y1;->k:Z

    .line 10
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->s:Z

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->t:Z

    const/4 p1, 0x0

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/y1;->m:Ljava/lang/Boolean;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/la;)V
    .locals 1

    const-string v0, "editModeHandle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6063
    iput-object p1, p0, Lcom/pspdfkit/internal/y1;->y:Lcom/pspdfkit/internal/la;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ti;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/y1;->z:Lcom/pspdfkit/internal/ti;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 6059
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->i:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 6060
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->i:Z

    .line 6061
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final varargs a([Lcom/pspdfkit/internal/views/annotations/a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "selectedViews"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x0

    .line 186
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    .line 187
    iget-object v1, p0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/an;->f()V

    .line 189
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p1, v2

    .line 190
    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->a()Landroid/view/View;

    move-result-object v4

    const-string v5, "annotationView.asView()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 192
    instance-of v6, v5, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v6, :cond_1

    .line 193
    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 194
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 195
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Selected views have to be bound to an Annotation."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 196
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Selected views have to use PageViewGroup.LayoutParams"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 201
    :cond_2
    array-length v1, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 202
    aget-object v1, p1, v0

    invoke-interface {v1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 205
    iget-object v3, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v1, v4, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Lcom/pspdfkit/internal/z1;->setScaleHandleDrawablesSupportRotation(Z)V

    goto :goto_2

    .line 206
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "annotationView isn\'t bound to an annotation."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1163
    :cond_5
    :goto_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1172
    array-length v2, p1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_7

    aget-object v4, p1, v3

    instance-of v5, v4, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v5, :cond_6

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    new-array p1, v0, [Lcom/pspdfkit/internal/views/annotations/a;

    .line 1175
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    .line 1176
    check-cast p1, [Lcom/pspdfkit/internal/views/annotations/a;

    .line 1177
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wq;->a([Lcom/pspdfkit/internal/views/annotations/a;)V

    .line 1180
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->b()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/k0;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;",
            "Lcom/pspdfkit/internal/k0;",
            ")Z"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5944
    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 5945
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    const-string v3, "annotation.boundingBox"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5946
    new-instance v3, Landroid/graphics/RectF;

    invoke-interface {p1}, Lcom/pspdfkit/internal/views/annotations/a;->getPageRect()Lcom/pspdfkit/utils/PageRect;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object p1

    invoke-direct {v3, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 5947
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 5949
    invoke-virtual {v0, v3, v2}, Lcom/pspdfkit/annotations/Annotation;->updateTransformationProperties(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 5950
    invoke-virtual {v0, v3}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    const/4 p1, 0x1

    if-eqz p2, :cond_6

    const-string v0, "<this>"

    .line 5951
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5970
    new-instance v4, Lcom/pspdfkit/internal/an;

    iget v5, v2, Landroid/graphics/RectF;->left:F

    iget v6, v2, Landroid/graphics/RectF;->top:F

    iget v7, v2, Landroid/graphics/RectF;->right:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v5, v6, v7, v2}, Lcom/pspdfkit/internal/an;-><init>(FFFF)V

    .line 5971
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5990
    new-instance v0, Lcom/pspdfkit/internal/an;

    iget v2, v3, Landroid/graphics/RectF;->left:F

    iget v5, v3, Landroid/graphics/RectF;->top:F

    iget v6, v3, Landroid/graphics/RectF;->right:F

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v2, v5, v6, v3}, Lcom/pspdfkit/internal/an;-><init>(FFFF)V

    .line 5991
    check-cast p2, Lcom/pspdfkit/internal/views/annotations/m;

    const-string v2, "oldBoundingBox"

    .line 5992
    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "newBoundingBox"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6035
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/annotations/m;->getTextForScaling()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    goto :goto_1

    .line 6037
    :cond_1
    invoke-virtual {v4}, Lcom/pspdfkit/internal/an;->g()F

    move-result v2

    invoke-virtual {v0}, Lcom/pspdfkit/internal/an;->g()F

    move-result v3

    cmpg-float v2, v2, v3

    if-nez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/an;->e()F

    move-result v2

    invoke-virtual {v0}, Lcom/pspdfkit/internal/an;->e()F

    move-result v3

    cmpg-float v2, v2, v3

    if-nez v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_4

    goto :goto_1

    .line 6041
    :cond_4
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/annotations/m;->getAnnotation()Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_1

    .line 6043
    :cond_5
    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/annotations/m;->getPaintForFontScalingCalculation()Landroid/graphics/Paint;

    move-result-object v6

    .line 6044
    invoke-virtual {v0}, Lcom/pspdfkit/internal/an;->g()F

    move-result v7

    .line 6045
    invoke-virtual {v0}, Lcom/pspdfkit/internal/an;->e()F

    move-result v8

    .line 6049
    sget-object p2, Lcom/pspdfkit/internal/qc;->a:Lcom/pspdfkit/internal/qc$a;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {}, Lcom/pspdfkit/internal/qc$a;->a()[F

    move-result-object v12

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 6050
    invoke-static/range {v5 .. v12}, Lcom/pspdfkit/internal/vt;->a(Ljava/lang/String;Landroid/graphics/Paint;FFZZZ[F)F

    move-result p2

    invoke-virtual {v1, p2}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setTextSize(F)V

    .line 6051
    :cond_6
    :goto_1
    iget-object p2, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/wq;->c()V

    return p1

    :cond_7
    return v1
.end method

.method public final a(Lcom/pspdfkit/internal/z1$b;)Z
    .locals 1

    const-string v0, "scaleHandle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6064
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->x:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->y:Lcom/pspdfkit/internal/la;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final b()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->p:Z

    .line 2
    iput-boolean v1, p0, Lcom/pspdfkit/internal/y1;->q:Z

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_1
    const/4 v4, 0x0

    const-string v5, "annotation"

    if-ge v3, v0, :cond_a

    .line 5
    iget-object v6, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v6, v2}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 6
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    .line 7
    :cond_1
    instance-of v7, v6, Lcom/pspdfkit/internal/h2;

    if-eqz v7, :cond_5

    .line 8
    check-cast v6, Lcom/pspdfkit/internal/h2;

    invoke-interface {v6}, Lcom/pspdfkit/internal/h2;->getAnnotations()Ljava/util/List;

    move-result-object v4

    const-string v6, "selectedView.annotations"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/annotations/Annotation;

    .line 10
    iget-boolean v7, p0, Lcom/pspdfkit/internal/y1;->p:Z

    if-eqz v7, :cond_3

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->isResizable()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationFlags;->NOZOOM:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v6, v7}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x1

    goto :goto_3

    :cond_2
    const/4 v7, 0x0

    :goto_3
    if-eqz v7, :cond_3

    const/4 v7, 0x1

    goto :goto_4

    :cond_3
    const/4 v7, 0x0

    .line 12
    :goto_4
    iput-boolean v7, p0, Lcom/pspdfkit/internal/y1;->p:Z

    .line 13
    iget-boolean v7, p0, Lcom/pspdfkit/internal/y1;->q:Z

    if-eqz v7, :cond_4

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    goto :goto_5

    :cond_4
    const/4 v6, 0x0

    :goto_5
    iput-boolean v6, p0, Lcom/pspdfkit/internal/y1;->q:Z

    goto :goto_2

    .line 17
    :cond_5
    iget-boolean v5, p0, Lcom/pspdfkit/internal/y1;->p:Z

    if-eqz v5, :cond_7

    if-eqz v4, :cond_7

    .line 18
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isResizable()Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v5, Lcom/pspdfkit/annotations/AnnotationFlags;->NOZOOM:Lcom/pspdfkit/annotations/AnnotationFlags;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/annotations/Annotation;->hasFlag(Lcom/pspdfkit/annotations/AnnotationFlags;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_6

    :cond_6
    const/4 v5, 0x0

    :goto_6
    if-eqz v5, :cond_7

    const/4 v5, 0x1

    goto :goto_7

    :cond_7
    const/4 v5, 0x0

    .line 19
    :goto_7
    iput-boolean v5, p0, Lcom/pspdfkit/internal/y1;->p:Z

    .line 24
    iget-boolean v5, p0, Lcom/pspdfkit/internal/y1;->q:Z

    if-eqz v5, :cond_8

    if-eqz v4, :cond_8

    .line 25
    invoke-static {v4}, Lcom/pspdfkit/internal/y1;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    goto :goto_8

    :cond_8
    const/4 v4, 0x0

    .line 26
    :goto_8
    iput-boolean v4, p0, Lcom/pspdfkit/internal/y1;->q:Z

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 27
    :cond_a
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->o()V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_11

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 32
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    :cond_b
    if-eqz v4, :cond_e

    .line 35
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    .line 36
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v0, v3, :cond_c

    .line 376
    move-object v0, v4

    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v0

    .line 377
    sget-object v3, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v0, v3, :cond_c

    const/4 v0, 0x1

    goto :goto_9

    :cond_c
    const/4 v0, 0x0

    .line 378
    :goto_9
    iget-boolean v3, p0, Lcom/pspdfkit/internal/y1;->v:Z

    if-ne v3, v0, :cond_d

    goto :goto_a

    .line 379
    :cond_d
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->v:Z

    .line 380
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_e
    :goto_a
    if-eqz v4, :cond_10

    .line 381
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    .line 382
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1217
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    sget-object v3, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v3, v0

    const/16 v3, 0xa

    if-eq v0, v3, :cond_f

    packed-switch v0, :pswitch_data_0

    goto :goto_b

    .line 1224
    :pswitch_0
    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v0

    if-nez v0, :cond_f

    goto :goto_b

    :cond_f
    :pswitch_1
    const/4 v1, 0x0

    :goto_b
    if-nez v1, :cond_10

    .line 1225
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/y1;->a(Z)V

    .line 1226
    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/y1;->setResizeEnabled(Z)V

    goto :goto_c

    .line 1228
    :cond_10
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_11
    :goto_c
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 0

    .line 1365
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->x:Z

    return-void
.end method

.method public final b(I)Z
    .locals 1

    .line 1366
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->x:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->y:Lcom/pspdfkit/internal/la;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final c(Z)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->g:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->v:Z

    return v0
.end method

.method public final d(Z)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->s:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 3
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->s:Z

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    return v0
.end method

.method public final e(Z)V
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->t:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 7
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->t:Z

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final e()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->s:Z

    if-nez v0, :cond_0

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->t:Z

    if-nez v0, :cond_0

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final f()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->i:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz v0, :cond_0

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->s:Z

    if-nez v0, :cond_0

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    if-nez v0, :cond_0

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->p:Z

    if-eqz v0, :cond_0

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getAnnotationSelectionViewThemeConfiguration()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getAnnotationSelectionViewThemeConfiguration()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getPageRotation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/y1;->w:I

    return v0
.end method

.method public final h()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->n:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    return v0
.end method

.method public final isDraggingEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isKeepAspectRatioEnabled()Ljava/lang/Boolean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->m:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final isResizeEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isResizeGuidesEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->l:Z

    return v0
.end method

.method public final isRotationEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->h:Z

    return v0
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    return-void
.end method

.method public final k()V
    .locals 16

    move-object/from16 v0, p0

    .line 1
    iget-object v1, v0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/z1;->h()V

    .line 4
    iget-object v1, v0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v1

    const-string v2, "selectionLayout.layoutParams!!.pageRect.pageRect"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 6
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 15
    iget-object v4, v0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x2

    const/4 v10, 0x1

    if-ge v8, v4, :cond_e

    .line 17
    iget-object v11, v0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v11, v8}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v11

    const/4 v12, 0x0

    if-eqz v11, :cond_0

    invoke-interface {v11}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v11

    goto :goto_1

    :cond_0
    move-object v11, v12

    :goto_1
    if-eqz v11, :cond_d

    .line 19
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v13

    const-string v14, "annotation.boundingBox"

    invoke-static {v13, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "annotation"

    .line 20
    invoke-static {v11, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1200
    instance-of v14, v11, Lcom/pspdfkit/annotations/ResizableAnnotation;

    if-eqz v14, :cond_9

    .line 1202
    instance-of v14, v11, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v14, :cond_1

    .line 1203
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto/16 :goto_2

    .line 1205
    :cond_1
    instance-of v14, v11, Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v14, :cond_2

    .line 1206
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1208
    :cond_2
    instance-of v14, v11, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    if-eqz v14, :cond_3

    .line 1209
    move-object v14, v11

    check-cast v14, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v14}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v14

    .line 1210
    sget-object v15, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-eq v14, v15, :cond_3

    .line 1211
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1213
    :cond_3
    instance-of v14, v11, Lcom/pspdfkit/annotations/SquareAnnotation;

    if-eqz v14, :cond_4

    .line 1214
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/SquareAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1216
    :cond_4
    instance-of v14, v11, Lcom/pspdfkit/annotations/LineAnnotation;

    if-eqz v14, :cond_5

    .line 1217
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1219
    :cond_5
    instance-of v14, v11, Lcom/pspdfkit/annotations/CircleAnnotation;

    if-eqz v14, :cond_6

    .line 1220
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/CircleAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1222
    :cond_6
    instance-of v14, v11, Lcom/pspdfkit/annotations/PolygonAnnotation;

    if-eqz v14, :cond_7

    .line 1223
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/PolygonAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1225
    :cond_7
    instance-of v14, v11, Lcom/pspdfkit/annotations/PolylineAnnotation;

    if-eqz v14, :cond_8

    .line 1226
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/PolylineAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    goto :goto_2

    .line 1228
    :cond_8
    instance-of v14, v11, Lcom/pspdfkit/annotations/ShapeAnnotation;

    if-eqz v14, :cond_9

    .line 1229
    iget-object v12, v0, Lcom/pspdfkit/internal/y1;->b:Lcom/pspdfkit/configuration/PdfConfiguration;

    const-class v14, Lcom/pspdfkit/annotations/ShapeAnnotation;

    invoke-virtual {v12, v14}, Lcom/pspdfkit/configuration/PdfConfiguration;->getMinimumAnnotationSize(Ljava/lang/Class;)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    :cond_9
    :goto_2
    if-nez v12, :cond_a

    .line 1233
    invoke-virtual {v11}, Lcom/pspdfkit/annotations/Annotation;->getMinimumSize()Lcom/pspdfkit/utils/Size;

    move-result-object v12

    const-string v11, "annotation.minimumSize"

    invoke-static {v12, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1234
    :cond_a
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v11

    .line 1235
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const/high16 v14, 0x3f800000    # 1.0f

    if-le v4, v10, :cond_b

    div-float v15, v11, v2

    goto :goto_3

    :cond_b
    const/high16 v15, 0x3f800000    # 1.0f

    :goto_3
    if-le v4, v10, :cond_c

    div-float v14, v13, v3

    :cond_c
    new-array v10, v9, [F

    aput v5, v10, v6

    new-array v5, v9, [F

    aput v11, v5, v6

    .line 1238
    iget v11, v12, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v11, v15

    const/4 v15, 0x1

    aput v11, v5, v15

    invoke-static {v5}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v5

    aput v5, v10, v15

    invoke-static {v10}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v5

    new-array v10, v9, [F

    aput v7, v10, v6

    new-array v7, v9, [F

    aput v13, v7, v6

    .line 1239
    iget v9, v12, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr v9, v14

    aput v9, v7, v15

    invoke-static {v7}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v7

    aput v7, v10, v15

    invoke-static {v10}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v7

    :cond_d
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 1243
    :cond_e
    new-instance v2, Lcom/pspdfkit/utils/Size;

    invoke-direct {v2, v5, v7}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v2, v0, Lcom/pspdfkit/internal/y1;->u:Lcom/pspdfkit/utils/Size;

    .line 1245
    iget-object v2, v0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    new-array v3, v9, [F

    invoke-virtual {v2}, Lcom/pspdfkit/internal/an;->b()F

    move-result v4

    aput v4, v3, v6

    iget v4, v1, Landroid/graphics/RectF;->left:F

    const/4 v5, 0x1

    aput v4, v3, v5

    invoke-static {v3}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/an;->b(F)V

    .line 1246
    iget-object v2, v0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    new-array v3, v9, [F

    invoke-virtual {v2}, Lcom/pspdfkit/internal/an;->c()F

    move-result v4

    aput v4, v3, v6

    iget v4, v1, Landroid/graphics/RectF;->right:F

    aput v4, v3, v5

    invoke-static {v3}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/an;->c(F)V

    .line 1247
    iget-object v2, v0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    new-array v3, v9, [F

    invoke-virtual {v2}, Lcom/pspdfkit/internal/an;->a()F

    move-result v4

    aput v4, v3, v6

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    aput v4, v3, v5

    invoke-static {v3}, Lcom/pspdfkit/internal/di;->b([F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/an;->a(F)V

    .line 1248
    iget-object v2, v0, Lcom/pspdfkit/internal/y1;->e:Lcom/pspdfkit/internal/an;

    new-array v3, v9, [F

    invoke-virtual {v2}, Lcom/pspdfkit/internal/an;->d()F

    move-result v4

    aput v4, v3, v6

    iget v1, v1, Landroid/graphics/RectF;->top:F

    aput v1, v3, v5

    invoke-static {v3}, Lcom/pspdfkit/internal/di;->a([F)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/an;->d(F)V

    return-void
.end method

.method public final l()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz v0, :cond_0

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->h:Z

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getRotationHandler()Lcom/pspdfkit/internal/wq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/z1;->getScaleHandleDrawables()Ljava/util/Map;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    iget-object v0, v0, Lcom/pspdfkit/internal/z1;->j:Lcom/pspdfkit/internal/up;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/up;->a()V

    return-void
.end method

.method public final n()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->s:Z

    return v0
.end method

.method public final o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->m:Ljava/lang/Boolean;

    const-string v1, "annotation"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v0, :cond_3

    .line 2
    iput-boolean v4, p0, Lcom/pspdfkit/internal/y1;->n:Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_5

    .line 4
    iget-object v6, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v6, v5}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 5
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v7

    goto :goto_1

    :cond_0
    move-object v7, v2

    :goto_1
    if-eqz v7, :cond_2

    .line 6
    sget v7, Lcom/pspdfkit/internal/ao;->a:I

    .line 7
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v6

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 8
    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 825
    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v6

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v6, v7, :cond_1

    const/4 v6, 0x1

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_2

    .line 826
    iput-boolean v3, p0, Lcom/pspdfkit/internal/y1;->n:Z

    goto :goto_4

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    .line 831
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y1;->n:Z

    .line 833
    :cond_5
    :goto_4
    iput-boolean v4, p0, Lcom/pspdfkit/internal/y1;->o:Z

    .line 834
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v5, 0x0

    :goto_5
    if-ge v5, v0, :cond_9

    .line 835
    iget-object v6, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v6, v5}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 836
    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, v2

    :goto_6
    if-eqz v7, :cond_8

    .line 837
    sget v7, Lcom/pspdfkit/internal/ao;->a:I

    invoke-interface {v6}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v6

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 838
    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1664
    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->isResizable()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->isMeasurement()Z

    move-result v7

    if-nez v7, :cond_7

    invoke-virtual {v6}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v6

    sget-object v7, Lcom/pspdfkit/annotations/AnnotationType;->LINE:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq v6, v7, :cond_7

    const/4 v6, 0x1

    goto :goto_7

    :cond_7
    const/4 v6, 0x0

    :goto_7
    if-eqz v6, :cond_8

    .line 1665
    iput-boolean v3, p0, Lcom/pspdfkit/internal/y1;->o:Z

    goto :goto_8

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_9
    :goto_8
    return-void
.end method

.method public final q()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->g:Z

    if-eqz v0, :cond_1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    if-nez v0, :cond_1

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->t:Z

    if-nez v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/z1;->b(I)Lcom/pspdfkit/internal/views/annotations/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->e()Z

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    .line 7
    iput-boolean v1, p0, Lcom/pspdfkit/internal/y1;->r:Z

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 11
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->r:Z

    return v0
.end method

.method public final setAnnotationSelectionViewThemeConfiguration(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V
    .locals 2

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z1;->setAnnotationSelectionViewThemeConfiguration(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V

    return-void
.end method

.method public final setDraggingEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->k:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->k:Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setKeepAspectRatioEnabled(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y1;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y1;->m:Ljava/lang/Boolean;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/y1;->o()V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final setResizeEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->j:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->j:Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setResizeGuidesEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->l:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->l:Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setRotationEnabled(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/y1;->h:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y1;->h:Z

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/y1;->a:Lcom/pspdfkit/internal/z1;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

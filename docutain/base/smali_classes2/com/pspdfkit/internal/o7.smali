.class public final Lcom/pspdfkit/internal/o7;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/o7$b;,
        Lcom/pspdfkit/internal/o7$a;,
        Lcom/pspdfkit/internal/o7$c;
    }
.end annotation


# static fields
.field private static final x:I

.field private static final y:I

.field private static final z:I


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/pspdfkit/internal/o7$a;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Landroid/view/MotionEvent;

.field private o:Landroid/view/MotionEvent;

.field private p:Z

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:Z

.field private v:Z

.field private w:Landroid/view/VelocityTracker;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/o7;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/o7;)Lcom/pspdfkit/internal/o7$a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeth(Lcom/pspdfkit/internal/o7;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgeti(Lcom/pspdfkit/internal/o7;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/o7;->i:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/internal/o7;)Landroid/view/MotionEvent;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputj(Lcom/pspdfkit/internal/o7;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/o7;->j:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputk(Lcom/pspdfkit/internal/o7;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/o7;->k:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/pspdfkit/internal/o7;->x:I

    .line 2
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/pspdfkit/internal/o7;->y:I

    .line 3
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/pspdfkit/internal/o7;->z:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/pspdfkit/internal/o7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/o7$c;I)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance p3, Lcom/pspdfkit/internal/o7$b;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p3, p0, v0}, Lcom/pspdfkit/internal/o7$b;-><init>(Lcom/pspdfkit/internal/o7;Landroid/os/Handler;)V

    iput-object p3, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    .line 8
    iput-object p2, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    .line 10
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/o7;->a(Lcom/pspdfkit/internal/o7$c;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/o7;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/pspdfkit/internal/o7;->u:Z

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/o7;->v:Z

    .line 12
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 14
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 15
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v2

    .line 16
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/pspdfkit/internal/o7;->f:I

    .line 19
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/o7;->g:I

    mul-int v0, v0, v0

    .line 21
    iput v0, p0, Lcom/pspdfkit/internal/o7;->c:I

    mul-int v1, v1, v1

    .line 22
    iput v1, p0, Lcom/pspdfkit/internal/o7;->d:I

    mul-int v2, v2, v2

    .line 23
    iput v2, p0, Lcom/pspdfkit/internal/o7;->e:I

    return-void

    .line 24
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "OnGestureListener must not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    .line 27
    iput-boolean v0, p0, Lcom/pspdfkit/internal/o7;->v:Z

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/o7$c;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/pspdfkit/internal/o7;->u:Z

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .line 28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 30
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    if-nez v1, :cond_0

    .line 31
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    .line 33
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    .line 36
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v5

    goto :goto_1

    :cond_2
    const/4 v5, -0x1

    .line 40
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_2
    if-ge v8, v6, :cond_4

    if-ne v5, v8, :cond_3

    goto :goto_3

    .line 43
    :cond_3
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    add-float/2addr v9, v11

    .line 44
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v11

    add-float/2addr v10, v11

    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    if-eqz v4, :cond_5

    add-int/lit8 v4, v6, -0x1

    goto :goto_4

    :cond_5
    move v4, v6

    :goto_4
    int-to-float v4, v4

    div-float/2addr v9, v4

    div-float/2addr v10, v4

    const/4 v4, 0x2

    const/4 v5, 0x3

    if-eqz v0, :cond_1c

    const/4 v8, 0x0

    const/16 v11, 0x3e8

    if-eq v0, v2, :cond_12

    if-eq v0, v4, :cond_c

    if-eq v0, v5, :cond_a

    const/4 v8, 0x5

    if-eq v0, v8, :cond_9

    if-eq v0, v1, :cond_6

    goto/16 :goto_f

    .line 61
    :cond_6
    iput v9, p0, Lcom/pspdfkit/internal/o7;->q:F

    iput v9, p0, Lcom/pspdfkit/internal/o7;->s:F

    .line 62
    iput v10, p0, Lcom/pspdfkit/internal/o7;->r:F

    iput v10, p0, Lcom/pspdfkit/internal/o7;->t:F

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    iget v1, p0, Lcom/pspdfkit/internal/o7;->g:I

    int-to-float v1, v1

    invoke-virtual {v0, v11, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 68
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 69
    iget-object v2, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v2

    .line 70
    iget-object v4, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v4, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v1

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v6, :cond_25

    if-ne v4, v0, :cond_7

    goto :goto_6

    .line 74
    :cond_7
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 75
    iget-object v8, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v8, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v8

    mul-float v8, v8, v2

    .line 76
    iget-object v9, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v9, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    mul-float v5, v5, v1

    add-float/2addr v5, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_8

    .line 80
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_f

    :cond_8
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 81
    :cond_9
    iput v9, p0, Lcom/pspdfkit/internal/o7;->q:F

    iput v9, p0, Lcom/pspdfkit/internal/o7;->s:F

    .line 82
    iput v10, p0, Lcom/pspdfkit/internal/o7;->r:F

    iput v10, p0, Lcom/pspdfkit/internal/o7;->t:F

    .line 83
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 84
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 85
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 86
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->p:Z

    .line 87
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->l:Z

    .line 88
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->m:Z

    .line 89
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->j:Z

    .line 90
    iget-boolean p1, p0, Lcom/pspdfkit/internal/o7;->k:Z

    if-eqz p1, :cond_25

    .line 91
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->k:Z

    goto/16 :goto_f

    .line 92
    :cond_a
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/o7$a;->b(Landroid/view/MotionEvent;)V

    .line 93
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 94
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 95
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 96
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_b

    .line 97
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    .line 98
    iput-object v8, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    .line 100
    :cond_b
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->p:Z

    .line 101
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->i:Z

    .line 102
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->l:Z

    .line 103
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->m:Z

    .line 104
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->j:Z

    .line 105
    iget-boolean p1, p0, Lcom/pspdfkit/internal/o7;->k:Z

    if-eqz p1, :cond_25

    .line 106
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->k:Z

    goto/16 :goto_f

    .line 107
    :cond_c
    iget-boolean v0, p0, Lcom/pspdfkit/internal/o7;->k:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/pspdfkit/internal/o7;->v:Z

    if-nez v0, :cond_d

    goto/16 :goto_f

    .line 110
    :cond_d
    iget v0, p0, Lcom/pspdfkit/internal/o7;->q:F

    sub-float/2addr v0, v9

    .line 111
    iget v1, p0, Lcom/pspdfkit/internal/o7;->r:F

    sub-float/2addr v1, v10

    .line 112
    iget-boolean v6, p0, Lcom/pspdfkit/internal/o7;->p:Z

    if-eqz v6, :cond_e

    .line 114
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    or-int/2addr v3, p1

    goto/16 :goto_f

    .line 115
    :cond_e
    iget-boolean v6, p0, Lcom/pspdfkit/internal/o7;->l:Z

    if-eqz v6, :cond_10

    .line 116
    iget v6, p0, Lcom/pspdfkit/internal/o7;->s:F

    sub-float v6, v9, v6

    float-to-int v6, v6

    .line 117
    iget v7, p0, Lcom/pspdfkit/internal/o7;->t:F

    sub-float v7, v10, v7

    float-to-int v7, v7

    mul-int v6, v6, v6

    mul-int v7, v7, v7

    add-int/2addr v7, v6

    .line 119
    iget v6, p0, Lcom/pspdfkit/internal/o7;->c:I

    if-le v7, v6, :cond_f

    .line 120
    iget-object v6, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    iget-object v8, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    invoke-interface {v6, v8, p1, v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result p1

    .line 121
    iput v9, p0, Lcom/pspdfkit/internal/o7;->q:F

    .line 122
    iput v10, p0, Lcom/pspdfkit/internal/o7;->r:F

    .line 123
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->l:Z

    .line 124
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 125
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 126
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_7

    :cond_f
    const/4 p1, 0x0

    .line 128
    :goto_7
    iget v0, p0, Lcom/pspdfkit/internal/o7;->d:I

    if-le v7, v0, :cond_1b

    .line 129
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->m:Z

    goto/16 :goto_b

    .line 131
    :cond_10
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-gez v2, :cond_11

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_25

    .line 132
    :cond_11
    iget-object v2, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    iget-object v3, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    invoke-interface {v2, v3, p1, v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 133
    iput v9, p0, Lcom/pspdfkit/internal/o7;->q:F

    .line 134
    iput v10, p0, Lcom/pspdfkit/internal/o7;->r:F

    goto/16 :goto_f

    .line 139
    :cond_12
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->i:Z

    .line 140
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/o7$a;->a(Landroid/view/MotionEvent;)V

    .line 142
    iget-boolean v1, p0, Lcom/pspdfkit/internal/o7;->p:Z

    if-eqz v1, :cond_13

    .line 144
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    or-int/2addr p1, v3

    goto :goto_a

    .line 145
    :cond_13
    iget-boolean v1, p0, Lcom/pspdfkit/internal/o7;->k:Z

    if-eqz v1, :cond_14

    .line 146
    iget-object p1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 147
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->k:Z

    goto :goto_8

    .line 148
    :cond_14
    iget-boolean v1, p0, Lcom/pspdfkit/internal/o7;->l:Z

    if-eqz v1, :cond_16

    .line 149
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 150
    iget-boolean v5, p0, Lcom/pspdfkit/internal/o7;->j:Z

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v5, :cond_15

    .line 151
    invoke-interface {v5, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    :cond_15
    move p1, v1

    goto :goto_a

    .line 156
    :cond_16
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    .line 157
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 158
    iget v6, p0, Lcom/pspdfkit/internal/o7;->g:I

    int-to-float v6, v6

    invoke-virtual {v1, v11, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 159
    invoke-virtual {v1, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v6

    .line 160
    invoke-virtual {v1, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v1

    .line 162
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/pspdfkit/internal/o7;->f:I

    int-to-float v7, v7

    cmpl-float v5, v5, v7

    if-gtz v5, :cond_18

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/pspdfkit/internal/o7;->f:I

    int-to-float v7, v7

    cmpl-float v5, v5, v7

    if-lez v5, :cond_17

    goto :goto_9

    :cond_17
    :goto_8
    const/4 p1, 0x0

    goto :goto_a

    .line 163
    :cond_18
    :goto_9
    iget-object v5, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    iget-object v7, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    invoke-interface {v5, v7, p1, v1, v6}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result p1

    .line 166
    :goto_a
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->o:Landroid/view/MotionEvent;

    if-eqz v1, :cond_19

    .line 167
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 170
    :cond_19
    iput-object v0, p0, Lcom/pspdfkit/internal/o7;->o:Landroid/view/MotionEvent;

    .line 171
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1a

    .line 174
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 175
    iput-object v8, p0, Lcom/pspdfkit/internal/o7;->w:Landroid/view/VelocityTracker;

    .line 177
    :cond_1a
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->p:Z

    .line 178
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->j:Z

    .line 179
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 180
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1b
    :goto_b
    move v3, p1

    goto/16 :goto_f

    .line 181
    :cond_1c
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_22

    .line 182
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 183
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 184
    :cond_1d
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    if-eqz v1, :cond_21

    iget-object v6, p0, Lcom/pspdfkit/internal/o7;->o:Landroid/view/MotionEvent;

    if-eqz v6, :cond_21

    if-eqz v0, :cond_21

    .line 185
    iget-boolean v0, p0, Lcom/pspdfkit/internal/o7;->m:Z

    if-nez v0, :cond_1e

    goto :goto_c

    .line 189
    :cond_1e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v7

    invoke-virtual {v6}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v11

    sub-long/2addr v7, v11

    .line 190
    sget v0, Lcom/pspdfkit/internal/o7;->z:I

    int-to-long v11, v0

    cmp-long v0, v7, v11

    if-gtz v0, :cond_20

    const-wide/16 v11, 0x1e

    cmp-long v0, v7, v11

    if-gez v0, :cond_1f

    goto :goto_c

    .line 194
    :cond_1f
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v0, v6

    .line 195
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v1, v6

    mul-int v0, v0, v0

    mul-int v1, v1, v1

    add-int/2addr v1, v0

    .line 196
    iget v0, p0, Lcom/pspdfkit/internal/o7;->e:I

    if-ge v1, v0, :cond_20

    const/4 v0, 0x1

    goto :goto_d

    :cond_20
    :goto_c
    const/4 v0, 0x0

    :goto_d
    if-eqz v0, :cond_21

    .line 197
    iput-boolean v2, p0, Lcom/pspdfkit/internal/o7;->p:Z

    .line 199
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 201
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->h:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_e

    .line 204
    :cond_21
    iget-object v0, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    sget v1, Lcom/pspdfkit/internal/o7;->z:I

    int-to-long v6, v1

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_22
    const/4 v0, 0x0

    .line 208
    :goto_e
    iput v9, p0, Lcom/pspdfkit/internal/o7;->q:F

    iput v9, p0, Lcom/pspdfkit/internal/o7;->s:F

    .line 209
    iput v10, p0, Lcom/pspdfkit/internal/o7;->r:F

    iput v10, p0, Lcom/pspdfkit/internal/o7;->t:F

    .line 210
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    if-eqz v1, :cond_23

    .line 211
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 213
    :cond_23
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    .line 214
    iput-boolean v2, p0, Lcom/pspdfkit/internal/o7;->l:Z

    .line 215
    iput-boolean v2, p0, Lcom/pspdfkit/internal/o7;->m:Z

    .line 216
    iput-boolean v2, p0, Lcom/pspdfkit/internal/o7;->i:Z

    .line 217
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->k:Z

    .line 218
    iput-boolean v3, p0, Lcom/pspdfkit/internal/o7;->j:Z

    .line 220
    iget-boolean v1, p0, Lcom/pspdfkit/internal/o7;->u:Z

    if-eqz v1, :cond_24

    .line 221
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 222
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    .line 223
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v5

    sget v3, Lcom/pspdfkit/internal/o7;->y:I

    int-to-long v7, v3

    add-long/2addr v5, v7

    sget v3, Lcom/pspdfkit/internal/o7;->x:I

    int-to-long v7, v3

    add-long/2addr v5, v7

    .line 224
    invoke-virtual {v1, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 227
    :cond_24
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/pspdfkit/internal/o7;->n:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v3

    sget v5, Lcom/pspdfkit/internal/o7;->y:I

    int-to-long v5, v5

    add-long/2addr v3, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 228
    iget-object v1, p0, Lcom/pspdfkit/internal/o7;->b:Lcom/pspdfkit/internal/o7$a;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result p1

    or-int v3, v0, p1

    :cond_25
    :goto_f
    return v3
.end method

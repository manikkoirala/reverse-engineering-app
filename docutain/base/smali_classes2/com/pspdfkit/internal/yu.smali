.class public final Lcom/pspdfkit/internal/yu;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/yu$g;
    }
.end annotation


# instance fields
.field private final A:Landroid/view/View$OnClickListener;

.field private final B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field b:Ljava/lang/StringBuilder;

.field c:Ljava/util/Formatter;

.field private d:Ljava/lang/String;

.field e:Z

.field private f:Lcom/pspdfkit/internal/yu$g;

.field private g:Landroid/content/Context;

.field private h:Landroid/widget/ProgressBar;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroidx/appcompat/widget/AppCompatImageButton;

.field private p:Landroidx/appcompat/widget/AppCompatImageButton;

.field private q:Landroid/view/View;

.field private r:Landroid/view/ViewGroup;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private final w:Landroid/os/Handler;

.field private final x:Landroid/view/View$OnTouchListener;

.field private final y:Landroid/view/View$OnClickListener;

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method public static synthetic $r8$lambda$XLSwz6IfuZ6jvLSLkFsQcxi8O0M(Lcom/pspdfkit/internal/yu;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/yu;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetj(Lcom/pspdfkit/internal/yu;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->j:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetl(Lcom/pspdfkit/internal/yu;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/yu;->l:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetm(Lcom/pspdfkit/internal/yu;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/yu;->m:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetn(Lcom/pspdfkit/internal/yu;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/pspdfkit/internal/yu;->n:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetp(Lcom/pspdfkit/internal/yu;)Landroidx/appcompat/widget/AppCompatImageButton;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->p:Landroidx/appcompat/widget/AppCompatImageButton;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetq(Lcom/pspdfkit/internal/yu;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetr(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgets(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetv(Lcom/pspdfkit/internal/yu;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetw(Lcom/pspdfkit/internal/yu;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputl(Lcom/pspdfkit/internal/yu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu;->l:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputm(Lcom/pspdfkit/internal/yu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu;->m:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputn(Lcom/pspdfkit/internal/yu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu;->n:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/yu;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yu;->b(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mc(Lcom/pspdfkit/internal/yu;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yu;->c(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$md(Lcom/pspdfkit/internal/yu;)I
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->d()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mh(Lcom/pspdfkit/internal/yu;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yu;->e:Z

    const/4 v1, 0x1

    .line 8
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yu;->l:Z

    .line 11
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yu;->n:Z

    .line 22
    new-instance v0, Lcom/pspdfkit/internal/yu$a;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$a;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    .line 59
    new-instance v0, Lcom/pspdfkit/internal/yu$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->x:Landroid/view/View$OnTouchListener;

    .line 69
    new-instance v0, Lcom/pspdfkit/internal/yu$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$b;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->y:Landroid/view/View$OnClickListener;

    .line 79
    new-instance v0, Lcom/pspdfkit/internal/yu$c;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$c;-><init>(Lcom/pspdfkit/internal/yu;)V

    .line 89
    new-instance v0, Lcom/pspdfkit/internal/yu$d;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$d;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->z:Landroid/view/View$OnClickListener;

    .line 101
    new-instance v0, Lcom/pspdfkit/internal/yu$e;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$e;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->A:Landroid/view/View$OnClickListener;

    .line 109
    new-instance v0, Lcom/pspdfkit/internal/yu$f;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/yu$f;-><init>(Lcom/pspdfkit/internal/yu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 172
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yu;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/yu;->g:Landroid/content/Context;

    const-string v0, "layout_inflater"

    .line 6
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    .line 7
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__uvv_player_controller:I

    invoke-virtual {p1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->x:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 9
    sget v0, Lcom/pspdfkit/R$id;->pspdf__title_part:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    .line 10
    sget v0, Lcom/pspdfkit/R$id;->pspdf__control_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    .line 11
    sget v0, Lcom/pspdfkit/R$id;->pspdf__loading_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__error_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    .line 13
    sget v0, Lcom/pspdfkit/R$id;->pspdf__turn_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scale_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageButton;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->p:Landroidx/appcompat/widget/AppCompatImageButton;

    .line 15
    sget v0, Lcom/pspdfkit/R$id;->pspdf__center_play_btn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    .line 16
    sget v0, Lcom/pspdfkit/R$id;->pspdf__back_btn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->p:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 35
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 39
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    :cond_3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__seekbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 43
    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->h:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_5

    .line 45
    instance-of v1, v0, Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    .line 46
    check-cast v0, Landroid/widget/SeekBar;

    .line 47
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->B:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 49
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 52
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->b:Ljava/lang/StringBuilder;

    .line 53
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->b:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->c:Ljava/util/Formatter;

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/yu;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->d:Ljava/lang/String;

    .line 56
    sget v0, Lcom/pspdfkit/R$id;->pspdf__duration:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->i:Landroid/widget/TextView;

    .line 57
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    sget v0, Lcom/pspdfkit/R$id;->pspdf__has_played:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/yu;->j:Landroid/widget/TextView;

    .line 59
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    sget v0, Lcom/pspdfkit/R$id;->pspdf__title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/yu;->k:Landroid/widget/TextView;

    return-void
.end method

.method private synthetic a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_0

    .line 2
    iget-boolean p1, p0, Lcom/pspdfkit/internal/yu;->l:Z

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yu;->a()V

    const/4 p1, 0x1

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu;->e:Z

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private b(I)V
    .locals 3

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__loading_layout:I

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-ne p1, v0, :cond_2

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_1

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_8

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 11
    :cond_2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__center_play_btn:I

    if-ne p1, v0, :cond_5

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_3

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_4

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 18
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_8

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 22
    :cond_5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__error_layout:I

    if-ne p1, v0, :cond_8

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_6

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_7

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->v:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    :cond_7
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_8

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    :goto_0
    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 7

    .line 2
    div-int/lit16 p1, p1, 0x3e8

    .line 4
    rem-int/lit8 v0, p1, 0x3c

    .line 5
    div-int/lit8 v1, p1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    .line 6
    div-int/lit16 p1, p1, 0xe10

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/yu;->b:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    const/4 v2, 0x1

    const/4 v4, 0x2

    if-lez p1, :cond_0

    .line 10
    iget-object v5, p0, Lcom/pspdfkit/internal/yu;->c:Ljava/util/Formatter;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v4

    const-string p1, "%d:%02d:%02d"

    invoke-virtual {v5, p1, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 12
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->c:Ljava/util/Formatter;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    const-string v0, "%02d:%02d"

    invoke-virtual {p1, v0, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private d()I
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    if-eqz v0, :cond_5

    iget-boolean v1, p0, Lcom/pspdfkit/internal/yu;->m:Z

    if-eqz v1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    move-result v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast v1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zu;->getDuration()I

    move-result v1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/yu;->h:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_2

    if-lez v1, :cond_1

    int-to-long v3, v0

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    int-to-long v5, v1

    .line 9
    div-long/2addr v3, v5

    long-to-int v4, v3

    .line 10
    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 12
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast v2, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zu;->getBufferPercentage()I

    move-result v2

    .line 13
    iget-object v3, p0, Lcom/pspdfkit/internal/yu;->h:Landroid/widget/ProgressBar;

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 16
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/yu;->i:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 17
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/yu;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 19
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->j:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 20
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/yu;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return v0

    :cond_5
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__uvv_stop_btn:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    goto :goto_0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__uvv_player_player_btn:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 90
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yu;->l:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 92
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    .line 94
    iput-boolean v0, p0, Lcom/pspdfkit/internal/yu;->l:Z

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 4

    .line 61
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yu;->l:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    .line 62
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->d()I

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 65
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    if-eqz v0, :cond_1

    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :catch_0
    :cond_1
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yu;->l:Z

    .line 69
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    .line 70
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    iget-boolean v3, p0, Lcom/pspdfkit/internal/yu;->n:Z

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x4

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    .line 72
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 74
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    .line 75
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_6

    .line 78
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 86
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    if-eqz p1, :cond_7

    .line 88
    iget-object v2, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 89
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_7
    return-void
.end method

.method final a(Z)V
    .locals 1

    .line 95
    iput-boolean p1, p0, Lcom/pspdfkit/internal/yu;->n:Z

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->p:Landroidx/appcompat/widget/AppCompatImageButton;

    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__uvv_player_scale_out_btn:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->p:Landroidx/appcompat/widget/AppCompatImageButton;

    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__uvv_player_scale_btn:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    .line 99
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    iget-boolean v0, p0, Lcom/pspdfkit/internal/yu;->n:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x4

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/yu;->l:Z

    return v0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/16 v3, 0x4f

    const/16 v4, 0xbb8

    if-eq v0, v3, :cond_c

    const/16 v3, 0x55

    if-eq v0, v3, :cond_c

    const/16 v3, 0x3e

    if-ne v0, v3, :cond_1

    goto/16 :goto_4

    :cond_1
    const/16 v3, 0x7e

    if-ne v0, v3, :cond_3

    if-eqz v1, :cond_2

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result p1

    if-nez p1, :cond_2

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->h()V

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    .line 18
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/yu;->a(I)V

    :cond_2
    return v2

    :cond_3
    const/16 v3, 0x56

    if-eq v0, v3, :cond_a

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_4

    goto :goto_3

    :cond_4
    const/16 v3, 0x19

    if-eq v0, v3, :cond_9

    const/16 v3, 0x18

    if-eq v0, v3, :cond_9

    const/16 v3, 0xa4

    if-eq v0, v3, :cond_9

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_5

    goto :goto_2

    :cond_5
    const/4 v3, 0x4

    if-eq v0, v3, :cond_7

    const/16 v3, 0x52

    if-ne v0, v3, :cond_6

    goto :goto_1

    .line 41
    :cond_6
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 42
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_7
    :goto_1
    if-eqz v1, :cond_8

    .line 43
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yu;->a()V

    :cond_8
    return v2

    .line 44
    :cond_9
    :goto_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_a
    :goto_3
    if-eqz v1, :cond_b

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 46
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->f()V

    .line 47
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    .line 48
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/yu;->a(I)V

    :cond_b
    return v2

    :cond_c
    :goto_4
    if-eqz v1, :cond_e

    .line 49
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 50
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->f()V

    goto :goto_5

    .line 52
    :cond_d
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    check-cast p1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->h()V

    .line 54
    :goto_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    .line 55
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz p1, :cond_e

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    :cond_e
    return v2
.end method

.method public final e()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final f()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final g()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->w:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-lez p1, :cond_0

    add-int/2addr p2, v0

    if-ge p1, p2, :cond_0

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/internal/yu;->t:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    div-int/lit8 p1, p1, 0x2

    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/internal/yu;->u:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    if-eq p1, v0, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yu;->a()V

    goto :goto_0

    .line 14
    :cond_1
    iget-boolean p1, p0, Lcom/pspdfkit/internal/yu;->e:Z

    if-nez p1, :cond_3

    .line 15
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yu;->e:Z

    const/16 p1, 0xbb8

    .line 16
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/yu;->a(I)V

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 18
    iput-boolean v1, p0, Lcom/pspdfkit/internal/yu;->e:Z

    :cond_3
    :goto_0
    return v0
.end method

.method public final onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    const/16 p1, 0xbb8

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/yu;->a(I)V

    const/4 p1, 0x0

    return p1
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->o:Landroidx/appcompat/widget/AppCompatImageButton;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->h:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu;->q:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public setMediaPlayer(Lcom/pspdfkit/internal/yu$g;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/yu;->f:Lcom/pspdfkit/internal/yu$g;

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yu;->h()V

    return-void
.end method

.method public setOnErrorView(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method public setOnErrorView(Landroid/view/View;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setOnErrorViewClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnLoadingView(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method public setOnLoadingView(Landroid/view/View;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yu;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

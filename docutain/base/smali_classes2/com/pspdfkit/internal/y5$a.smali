.class final Lcom/pspdfkit/internal/y5$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/y5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/y5$a$a;
    }
.end annotation


# instance fields
.field b:Lcom/pspdfkit/internal/ik;

.field c:Lcom/pspdfkit/internal/jk;

.field private final d:Lcom/pspdfkit/internal/y5$a$a;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/y5$a$a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/y5$a;->b:Lcom/pspdfkit/internal/ik;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/y5$a;->c:Lcom/pspdfkit/internal/jk;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/y5$a;->d:Lcom/pspdfkit/internal/y5$a$a;

    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/y5$a;->e:Z

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/pspdfkit/internal/y5$a;->e:Z

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/y5$a;->c:Lcom/pspdfkit/internal/jk;

    iget-object p3, p0, Lcom/pspdfkit/internal/y5$a;->b:Lcom/pspdfkit/internal/ik;

    check-cast p2, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p2, p3, p1}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V

    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    .line 4
    iget-boolean p2, p0, Lcom/pspdfkit/internal/y5$a;->e:Z

    if-eq p1, p2, :cond_0

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/internal/y5$a;->d:Lcom/pspdfkit/internal/y5$a$a;

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/y5$a$a;->a(Z)V

    :cond_0
    return-void
.end method

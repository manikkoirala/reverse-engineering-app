.class public final Lcom/pspdfkit/internal/cl;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/qk;
.implements Lcom/pspdfkit/internal/al$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/cl$d;,
        Lcom/pspdfkit/internal/cl$c;,
        Lcom/pspdfkit/internal/cl$b;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/al;

.field private c:Landroidx/appcompat/widget/Toolbar;

.field private d:Landroidx/recyclerview/widget/RecyclerView;

.field private e:Lcom/pspdfkit/internal/vk;

.field private f:Landroidx/fragment/app/FragmentManager;

.field private g:Lcom/pspdfkit/internal/ok;

.field private h:Lcom/pspdfkit/internal/cl$c;

.field private i:Lcom/pspdfkit/internal/cl$b;

.field private j:Landroid/os/Parcelable;

.field private k:Landroidx/recyclerview/widget/LinearLayoutManager;


# direct methods
.method public static synthetic $r8$lambda$0Qz4uPYVcJMf9u2JdslkDskQaUc(Lcom/pspdfkit/internal/cl;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/cl;->a(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$VJEjz42Ilflg2fFITcWRNIuyL4c(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/cl;->b(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$clFs4wOuVkYSr33bUJgsxZOU968(Lcom/pspdfkit/internal/cl;Lcom/pspdfkit/internal/ik;Landroid/app/Dialog;Lcom/pspdfkit/internal/lk;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/cl;->a(Lcom/pspdfkit/internal/ik;Landroid/app/Dialog;Lcom/pspdfkit/internal/lk;)V

    return-void
.end method

.method public static synthetic $r8$lambda$gSLcIOUlqEG4HtKkqJg8LTtfH24(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/cl;->c(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/cl;->d()V

    return-void
.end method

.method private synthetic a(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 37
    iget-object p1, p0, Lcom/pspdfkit/internal/cl;->g:Lcom/pspdfkit/internal/ok;

    if-eqz p1, :cond_0

    .line 38
    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/uk;->e()V

    :cond_0
    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/ik;Landroid/app/Dialog;Lcom/pspdfkit/internal/lk;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->g:Lcom/pspdfkit/internal/ok;

    if-eqz v0, :cond_0

    .line 34
    check-cast v0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0, p1, p3}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/lk;)V

    .line 36
    :cond_0
    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private static synthetic b(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 6
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private static synthetic c(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 21
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private d()V
    .locals 3

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__note_editor_layout:I

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 5
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/vk;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/vk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    .line 9
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_toolbar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/pspdfkit/internal/cl;->c:Landroidx/appcompat/widget/Toolbar;

    .line 10
    new-instance v1, Lcom/pspdfkit/internal/al;

    invoke-direct {v1, v0, p0}, Lcom/pspdfkit/internal/al;-><init>(Landroidx/appcompat/widget/Toolbar;Lcom/pspdfkit/internal/al$a;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_recycler_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/cl;->k:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/pspdfkit/internal/xk;

    invoke-direct {v1}, Lcom/pspdfkit/internal/xk;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 42
    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->g:Lcom/pspdfkit/internal/ok;

    if-eqz v0, :cond_0

    .line 32
    check-cast v0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uk;->b(I)V

    :cond_0
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/al;->a(IZ)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;)V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(Lcom/pspdfkit/internal/ik;)V

    const/4 p1, 0x0

    .line 9
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .line 12
    instance-of v1, v0, Lcom/pspdfkit/internal/y5;

    if-eqz v1, :cond_0

    .line 13
    check-cast v0, Lcom/pspdfkit/internal/y5;

    .line 14
    invoke-virtual {v0}, Lcom/pspdfkit/internal/y5;->a()V

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 23
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/vk;->getItemCount()I

    move-result p1

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    .line 24
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->k:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v1

    if-ge v1, p1, :cond_2

    .line 25
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->k:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setStackFromEnd(Z)V

    .line 30
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->f:Landroidx/fragment/app/FragmentManager;

    if-eqz v0, :cond_0

    .line 40
    new-instance v1, Lcom/pspdfkit/internal/cl$a;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/cl$a;-><init>(Ljava/lang/Runnable;)V

    const/4 p1, 0x0

    invoke-static {v0, p1, v1}, Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/ui/AnnotationCreatorInputDialogFragment$OnAnnotationCreatorSetListener;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;Z)V"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/vk;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/al;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    .line 3
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->i:Lcom/pspdfkit/internal/cl$b;

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/internal/cl$b;->dismiss()V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/al;->c(I)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->b(Lcom/pspdfkit/internal/ik;)V

    return-void
.end method

.method public final c()V
    .locals 3

    .line 22
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->k:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 28
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->k:Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->setStackFromEnd(Z)V

    .line 29
    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method public final c(Lcom/pspdfkit/internal/ik;)V
    .locals 5

    .line 1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__set_reply_status:I

    const/4 v3, 0x0

    .line 3
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    .line 7
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 8
    new-instance v2, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda0;-><init>()V

    .line 9
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__note_editor_set_status_dialog_layout:I

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 15
    sget v2, Lcom/pspdfkit/R$id;->pspdf__note_reply_status_dialog_list_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;

    .line 16
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {}, Lcom/pspdfkit/internal/lk;->values()[Lcom/pspdfkit/internal/lk;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->setItems(Ljava/util/List;)V

    .line 17
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 19
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, v0}, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/cl;Lcom/pspdfkit/internal/ik;Landroid/app/Dialog;)V

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView;->setOnReviewStateSelectedListener(Lcom/pspdfkit/internal/annotations/note/ui/NoteReplyStatusDialogView$b;)V

    return-void
.end method

.method public final d(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->c(Lcom/pspdfkit/internal/ik;)V

    return-void
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vk;->b()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->g:Lcom/pspdfkit/internal/ok;

    if-eqz v0, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/uk;->h()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->j:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/cl;->j:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/cl;->j:Landroid/os/Parcelable;

    :cond_0
    return-void
.end method

.method public getNoteEditorContentCards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vk;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__delete:I

    const/4 v3, 0x0

    .line 3
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__prompt_delete_annotation:I

    .line 6
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__ok:I

    .line 9
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 10
    new-instance v2, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/cl;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__cancel:I

    .line 17
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 18
    new-instance v2, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda3;

    invoke-direct {v2}, Lcom/pspdfkit/internal/cl$$ExternalSyntheticLambda3;-><init>()V

    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 21
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public final i()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/vk;->d()V

    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/cl$d;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/cl$d;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 9
    iget-object p1, p1, Lcom/pspdfkit/internal/cl$d;->a:Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/pspdfkit/internal/cl;->j:Landroid/os/Parcelable;

    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/cl$d;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/cl$d;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Lcom/pspdfkit/internal/cl$d;->a:Landroid/os/Parcelable;

    return-object v1
.end method

.method public setAddNewReplyBoxDisplayed(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(Z)V

    return-void
.end method

.method public setFragmentManager(Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cl;->f:Landroidx/fragment/app/FragmentManager;

    return-void
.end method

.method public setOnDismissViewListener(Lcom/pspdfkit/internal/cl$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cl;->i:Lcom/pspdfkit/internal/cl$b;

    return-void
.end method

.method public setPresenter(Lcom/pspdfkit/internal/ok;)V
    .locals 1

    if-nez p1, :cond_0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/cl;->g:Lcom/pspdfkit/internal/ok;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(Lcom/pspdfkit/internal/jk;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/cl;->c:Landroidx/appcompat/widget/Toolbar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/cl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setStatusBarColor(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->h:Lcom/pspdfkit/internal/cl$c;

    if-eqz v0, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/rk;

    .line 3
    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/s5;->a(Landroid/view/Window;I)V

    :cond_0
    return-void
.end method

.method public setStatusBarColorCallback(Lcom/pspdfkit/internal/cl$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/cl;->h:Lcom/pspdfkit/internal/cl$c;

    return-void
.end method

.method public setStyleBoxDisplayed(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->b(Z)V

    return-void
.end method

.method public setStyleBoxExpanded(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->c(Z)V

    return-void
.end method

.method public setStyleBoxPickerColors(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(Ljava/util/List;)V

    return-void
.end method

.method public setStyleBoxPickerIcons(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->b(Ljava/util/List;)V

    return-void
.end method

.method public setStyleBoxSelectedColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(I)V

    return-void
.end method

.method public setStyleBoxSelectedIcon(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->a(Ljava/lang/String;)V

    return-void
.end method

.method public setStyleBoxText(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->b(I)V

    return-void
.end method

.method public setStyleBoxText(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->e:Lcom/pspdfkit/internal/vk;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/vk;->b(Ljava/lang/String;)V

    return-void
.end method

.method public setToolbarForegroundColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/al;->b(I)V

    return-void
.end method

.method public setToolbarTitle(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/al;->d(I)V

    return-void
.end method

.method public setToolbarTitle(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cl;->b:Lcom/pspdfkit/internal/al;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/al;->a(Ljava/lang/String;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/jq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lio/reactivex/rxjava3/processors/FlowableProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/FlowableProcessor<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {}, Lio/reactivex/rxjava3/processors/PublishProcessor;->create()Lio/reactivex/rxjava3/processors/PublishProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/PublishProcessor;->toSerialized()Lio/reactivex/rxjava3/processors/FlowableProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/rxjava3/core/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "TT;>;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/FlowableProcessor;->hasComplete()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/FlowableProcessor;->hasThrowable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/processors/PublishProcessor;->create()Lio/reactivex/rxjava3/processors/PublishProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/PublishProcessor;->toSerialized()Lio/reactivex/rxjava3/processors/FlowableProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/FlowableProcessor;->onBackpressureBuffer()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/jq;->a:Lio/reactivex/rxjava3/processors/FlowableProcessor;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/processors/FlowableProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method

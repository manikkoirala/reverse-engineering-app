.class public final Lcom/pspdfkit/internal/fm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/pspdfkit/ui/PageObjectProvider;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/rxjava3/processors/BehaviorProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/processors/BehaviorProcessor<",
            "Lcom/pspdfkit/internal/lr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/rxjava3/core/Scheduler;


# direct methods
.method public static synthetic $r8$lambda$7qz0ch2HeHnwrBS4R289GVTjYOY(Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/lr;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/internal/lr;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$PAzhmaXnDu-CFqzoNmvtT8JiK_c(Lcom/pspdfkit/internal/fm;Lcom/pspdfkit/internal/nh;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/fm;->a(Lcom/pspdfkit/internal/nh;)V

    return-void
.end method

.method public static synthetic $r8$lambda$z926w3pO4RGuvR0-Jabu-0S-TNo(Lcom/pspdfkit/internal/fm;ILcom/pspdfkit/internal/lr;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/fm;->a(ILcom/pspdfkit/internal/lr;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/fm;-><init>(Lio/reactivex/rxjava3/core/Scheduler;)V

    return-void
.end method

.method constructor <init>(Lio/reactivex/rxjava3/core/Scheduler;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/lr;->a()Lcom/pspdfkit/internal/lr;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->createDefault(Ljava/lang/Object;)Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/fm;->a:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/nh;

    new-instance v1, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/fm;)V

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/nh;-><init>(Lcom/pspdfkit/internal/nh$a;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/fm;->b:Lcom/pspdfkit/internal/nh;

    .line 18
    iput-object p1, p0, Lcom/pspdfkit/internal/fm;->c:Lio/reactivex/rxjava3/core/Scheduler;

    return-void
.end method

.method private a()Lio/reactivex/rxjava3/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/functions/Function<",
            "Lcom/pspdfkit/internal/lr;",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/fm;)V

    return-object v0
.end method

.method private a(I)Lio/reactivex/rxjava3/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/functions/Function<",
            "Lcom/pspdfkit/internal/lr;",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/fm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/fm;I)V

    return-object v0
.end method

.method private synthetic a(ILcom/pspdfkit/internal/lr;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 61
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PageObjectProvider;

    .line 63
    invoke-interface {v1}, Lcom/pspdfkit/ui/PageObjectProvider;->getFilteredPages()Ljava/util/Set;

    move-result-object v2

    .line 64
    sget-object v3, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->NO_FILTERED_PAGES:Ljava/util/Set;

    if-eq v2, v3, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    :cond_1
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object p2
.end method

.method private synthetic a(Lcom/pspdfkit/internal/lr;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 59
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object p1
.end method

.method private a(Lcom/pspdfkit/internal/nh;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/fm;->a:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/lr;

    invoke-direct {v0}, Lcom/pspdfkit/internal/lr;-><init>()V

    .line 3
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/ui/PageObjectProvider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "provider"

    const-string v1, "argumentName"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 56
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Lio/reactivex/rxjava3/core/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->a:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->toObservable()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    invoke-direct {p0}, Lcom/pspdfkit/internal/fm;->a()Lio/reactivex/rxjava3/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/fm;->c:Lio/reactivex/rxjava3/core/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lio/reactivex/rxjava3/core/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->a:Lio/reactivex/rxjava3/processors/BehaviorProcessor;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/processors/BehaviorProcessor;->toObservable()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/fm;->a(I)Lio/reactivex/rxjava3/functions/Function;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/core/Observable;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->c:Lio/reactivex/rxjava3/core/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lcom/pspdfkit/ui/PageObjectProvider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "provider"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/fm;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

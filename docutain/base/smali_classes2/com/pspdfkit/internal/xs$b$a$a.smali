.class final Lcom/pspdfkit/internal/xs$b$a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/xs$b$a;

.field final synthetic b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/xs$b$a;Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/xs$b$a$a;->a:Lcom/pspdfkit/internal/xs$b$a;

    iput-object p2, p0, Lcom/pspdfkit/internal/xs$b$a$a;->b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a$a;->a:Lcom/pspdfkit/internal/xs$b$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xs$b$a;->a()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/xs$b$a$a;->b:Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    if-ne v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/xs$b$a$a;->a:Lcom/pspdfkit/internal/xs$b$a;

    invoke-static {v0}, Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/internal/xs$b$a;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/xs$b$a$a;->a:Lcom/pspdfkit/internal/xs$b$a;

    invoke-static {p1}, Lcom/pspdfkit/internal/xs$b$a;->a(Lcom/pspdfkit/internal/xs$b$a;)Landroid/widget/ImageView;

    move-result-object p1

    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_0
    return-void
.end method

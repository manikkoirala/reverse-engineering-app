.class final Lcom/pspdfkit/internal/h9$a;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/h9;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/pspdfkit/ui/PopupToolbar;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/h9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/h9;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/h9$a;->a:Lcom/pspdfkit/internal/h9;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/PopupToolbar;

    iget-object v1, p0, Lcom/pspdfkit/internal/h9$a;->a:Lcom/pspdfkit/internal/h9;

    invoke-static {v1}, Lcom/pspdfkit/internal/h9;->a(Lcom/pspdfkit/internal/h9;)Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/ui/PopupToolbar;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 3
    new-instance v1, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;

    sget v2, Lcom/pspdfkit/R$id;->pspdf__text_selection_toolbar_item_paste_annotation:I

    sget v3, Lcom/pspdfkit/R$string;->pspdf__paste:I

    invoke-direct {v1, v2, v3}, Lcom/pspdfkit/ui/toolbar/popup/PopupToolbarMenuItem;-><init>(II)V

    .line 4
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PopupToolbar;->setMenuItems(Ljava/util/List;)V

    return-object v0
.end method

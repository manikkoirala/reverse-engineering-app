.class public final Lcom/pspdfkit/internal/yl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/jni/NativePageCache;

.field private final b:Lcom/pspdfkit/internal/sc;


# direct methods
.method public static synthetic $r8$lambda$Nb5K5zhdc_472u5MR0QknrGQztc(Lcom/pspdfkit/internal/yl;Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/yl;->b(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/jni/NativePageCache;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/sc;

    invoke-direct {v0}, Lcom/pspdfkit/internal/sc;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/yl;->b:Lcom/pspdfkit/internal/sc;

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    return-void
.end method

.method private b(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getUid()Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 3
    iget-object v2, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x1

    aput-object v3, v5, v6

    const-string v3, "d[%s]p[%d]_"

    invoke-static {v4, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 5
    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/jni/NativePageCache;->remove(Ljava/lang/String;)V

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/yl;->b:Lcom/pspdfkit/internal/sc;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, p1, v1}, Lcom/pspdfkit/internal/sc;->b(Lcom/pspdfkit/document/PdfDocument;I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Completable;
    .locals 4

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v0

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/pspdfkit/internal/yl;->a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)Lio/reactivex/rxjava3/core/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/document/PdfDocument;",
            "Ljava/util/Collection<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/rxjava3/core/Completable;"
        }
    .end annotation

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/yl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/yl$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/yl;Lcom/pspdfkit/document/PdfDocument;Ljava/util/Collection;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    return-object p1
.end method

.method public final a()V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativePageCache;->clear()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->b:Lcom/pspdfkit/internal/sc;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/sc;->a()V

    return-void
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativePageCache;->setSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Landroid/graphics/Bitmap;Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativePageRenderingConfig;)Z
    .locals 4

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getUid()Ljava/lang/String;

    move-result-object p2

    .line 10
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, v2, p3

    const-string p2, "d[%s]p[%d]_"

    invoke-static {v1, p2, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 11
    invoke-virtual {v0, p1, p2, p4}, Lcom/pspdfkit/internal/jni/NativePageCache;->get(Landroid/graphics/Bitmap;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;)Z

    move-result p1

    return p1
.end method

.method public final b()Lcom/pspdfkit/internal/sc;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->b:Lcom/pspdfkit/internal/sc;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/jni/NativePageCache;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yl;->a:Lcom/pspdfkit/internal/jni/NativePageCache;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/ie;
.super Lcom/pspdfkit/internal/h4;
.source "SourceFile"


# instance fields
.field private final m:Ljava/util/ArrayList;

.field private final n:Landroid/graphics/Path;

.field private final o:Landroid/graphics/Matrix;

.field private p:Ljava/util/ArrayList;

.field private final q:Landroid/graphics/Path;

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1
    invoke-direct {p0, v0, v0, v1, v1}, Lcom/pspdfkit/internal/ie;-><init>(IIFF)V

    return-void
.end method

.method public constructor <init>(IIFF)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/h4;-><init>(IIFF)V

    .line 3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    .line 6
    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ie;->n:Landroid/graphics/Path;

    .line 7
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ie;->o:Landroid/graphics/Matrix;

    .line 9
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0x1f4

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    .line 12
    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    .line 34
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    if-eqz p3, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/pspdfkit/internal/h4;->j()I

    move-result v1

    if-eqz v1, :cond_1

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    invoke-static {v1, v2}, Lcom/pspdfkit/utils/PdfUtils;->boundingBoxFromLines(Ljava/util/List;F)Landroid/graphics/RectF;

    move-result-object v1

    cmpl-float v2, p4, v0

    if-eqz v2, :cond_0

    .line 28
    iget-object v2, p0, Lcom/pspdfkit/internal/ie;->o:Landroid/graphics/Matrix;

    invoke-virtual {v2, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 29
    iget-object v2, p0, Lcom/pspdfkit/internal/ie;->o:Landroid/graphics/Matrix;

    .line 30
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 33
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v4, v2, v3

    if-lez v4, :cond_0

    .line 35
    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 36
    iput v3, v1, Landroid/graphics/RectF;->bottom:F

    .line 37
    :cond_0
    invoke-virtual {p1, v1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 41
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_2
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 44
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x0

    .line 45
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 46
    iget v2, v1, Landroid/graphics/PointF;->x:F

    mul-float v2, v2, p4

    iget v1, v1, Landroid/graphics/PointF;->y:F

    mul-float v1, v1, p4

    invoke-virtual {p1, v2, v1, p2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 51
    :cond_3
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_5

    cmpl-float p3, p4, v0

    if-nez p3, :cond_4

    .line 53
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 55
    :cond_4
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->o:Landroid/graphics/Matrix;

    invoke-virtual {p3, p4, p4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 56
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    iget-object p4, p0, Lcom/pspdfkit/internal/ie;->n:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/pspdfkit/internal/ie;->o:Landroid/graphics/Matrix;

    .line 57
    invoke-virtual {p4, p3}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 58
    invoke-virtual {p4, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 59
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->n:Landroid/graphics/Path;

    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_5
    :goto_1
    return-void
.end method

.method protected final a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 0

    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/pspdfkit/internal/h4;->a(Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    .line 21
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 22
    sget-object p2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 23
    sget-object p2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const/4 p2, 0x0

    .line 24
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    return-void
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 4

    const/4 p2, 0x1

    .line 9
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ie;->r:Z

    .line 10
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 11
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    iget p3, p1, Landroid/graphics/PointF;->x:F

    iget v0, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2, p3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 13
    :cond_0
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, p2

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/graphics/PointF;

    .line 14
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    iget v0, p2, Landroid/graphics/PointF;->x:F

    iget p2, p2, Landroid/graphics/PointF;->y:F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p1, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, p2

    div-float/2addr v3, v2

    invoke-virtual {p3, v0, p2, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 16
    :goto_0
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/util/ArrayList;Landroid/graphics/Matrix;F)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    const/4 p2, 0x1

    .line 2
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ie;->r:Z

    .line 3
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 5
    iget-object p2, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 6
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 7
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p3

    const/4 v0, 0x2

    if-ge p3, v0, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    iget-object p3, p0, Lcom/pspdfkit/internal/ie;->q:Landroid/graphics/Path;

    invoke-static {p3, p2}, Lcom/pspdfkit/internal/he;->a(Landroid/graphics/Path;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ie;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final p()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ie;->r:Z

    return-void
.end method

.method public final q()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ie;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ie;->r:Z

    return v0
.end method

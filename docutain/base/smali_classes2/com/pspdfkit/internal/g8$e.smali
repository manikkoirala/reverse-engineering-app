.class final Lcom/pspdfkit/internal/g8$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/g8;->a(Landroid/graphics/Matrix;Lcom/pspdfkit/document/processor/ComparisonDocument;Lcom/pspdfkit/document/processor/ComparisonDocument;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/g8;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/g8;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/g8$e;->a:Lcom/pspdfkit/internal/g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Lkotlin/Triple;

    const-string v0, "<name for destructuring parameter 0>"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 406
    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/pspdfkit/internal/g8$e;->a:Lcom/pspdfkit/internal/g8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/g8;->c()Lcom/pspdfkit/document/processor/ComparisonDialogListener;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/pspdfkit/document/DocumentSource;

    invoke-direct {v1, p1}, Lcom/pspdfkit/document/DocumentSource;-><init>(Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Lcom/pspdfkit/document/processor/ComparisonDialogListener;->onComparisonSuccessful(Lcom/pspdfkit/document/DocumentSource;)V

    :cond_0
    return-void
.end method

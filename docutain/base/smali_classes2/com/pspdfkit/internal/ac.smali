.class public final Lcom/pspdfkit/internal/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/yb;


# instance fields
.field private final b:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->c:Lcom/pspdfkit/internal/nh;

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->d:Lcom/pspdfkit/internal/nh;

    .line 15
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    .line 19
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    .line 23
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method private static b()V
    .locals 2

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Form listeners touched on non ui thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->clear()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormElement;Z)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;

    .line 3
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;->onFormElementDeselected(Lcom/pspdfkit/forms/FormElement;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/TextFormElement;)V
    .locals 2

    .line 19
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;

    .line 21
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;->onFormElementValidationSuccess(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/TextFormElement;Ljava/lang/String;)V
    .locals 2

    .line 16
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;

    .line 18
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;->onFormElementValidationFailed(Lcom/pspdfkit/forms/FormElement;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;

    .line 9
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onChangeFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 2

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;->onFormElementClicked(Lcom/pspdfkit/forms/FormElement;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final addOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final addOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;->onFormElementSelected(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/forms/TextFormElement;)V
    .locals 2

    .line 8
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;

    .line 10
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;->onFormElementViewUpdated(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onEnterFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/pspdfkit/forms/FormElement;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;->onFormElementUpdated(Lcom/pspdfkit/forms/FormElement;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V
    .locals 2

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;

    .line 6
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;->onExitFormElementEditingMode(Lcom/pspdfkit/ui/special_mode/controller/FormEditingController;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;->isFormElementClickable(Lcom/pspdfkit/forms/FormElement;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final e(Lcom/pspdfkit/forms/FormElement;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/ac;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;

    .line 3
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;->onPrepareFormElementSelection(Lcom/pspdfkit/forms/FormElement;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final removeOnFormElementClickedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementClickedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->f:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnFormElementDeselectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementDeselectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->c:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnFormElementEditingModeChangeListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementEditingModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->e:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnFormElementSelectedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementSelectedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->b:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnFormElementUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->d:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final removeOnFormElementViewUpdatedListener(Lcom/pspdfkit/ui/special_mode/manager/FormManager$OnFormElementViewUpdatedListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ac;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/ou;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/core/CompletableOnSubscribe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:I

.field private final c:J

.field private final d:F

.field private final e:Landroid/view/animation/DecelerateInterpolator;


# direct methods
.method public static synthetic $r8$lambda$7oXaSjnpwJiezRFMWpsHWS8WgB8(Lcom/pspdfkit/internal/ou;ZLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/ou;->a(ZLio/reactivex/rxjava3/core/CompletableEmitter;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;IF)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ou;->e:Landroid/view/animation/DecelerateInterpolator;

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    .line 11
    iput p2, p0, Lcom/pspdfkit/internal/ou;->b:I

    const-wide/16 p1, 0xc8

    .line 12
    iput-wide p1, p0, Lcom/pspdfkit/internal/ou;->c:J

    .line 13
    iput p3, p0, Lcom/pspdfkit/internal/ou;->d:F

    return-void
.end method

.method private synthetic a(ZLio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 1

    if-nez p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3
    :cond_0
    invoke-interface {p2}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/rxjava3/core/CompletableEmitter;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ou;->b:I

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-ne v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 5
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    return-void

    :cond_0
    const/4 v5, 0x2

    if-ne v0, v5, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 9
    invoke-interface {p1}, Lio/reactivex/rxjava3/core/CompletableEmitter;->onComplete()V

    return-void

    .line 15
    :cond_1
    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    const/4 v6, 0x3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_2

    const/4 v0, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_1

    .line 17
    :cond_2
    iget v0, p0, Lcom/pspdfkit/internal/ou;->d:F

    neg-float v0, v0

    goto :goto_0

    .line 21
    :cond_3
    iget v0, p0, Lcom/pspdfkit/internal/ou;->d:F

    :goto_0
    const/4 v5, 0x0

    .line 30
    :goto_1
    iget-object v7, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 35
    iget v0, p0, Lcom/pspdfkit/internal/ou;->b:I

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    const/4 v5, 0x4

    if-eq v0, v5, :cond_5

    const/4 v7, 0x5

    if-eq v0, v7, :cond_4

    goto :goto_3

    .line 37
    :cond_4
    iget v0, p0, Lcom/pspdfkit/internal/ou;->d:F

    goto :goto_2

    .line 41
    :cond_5
    iget v0, p0, Lcom/pspdfkit/internal/ou;->d:F

    neg-float v0, v0

    :goto_2
    move v4, v0

    const/4 v2, 0x0

    .line 51
    :goto_3
    iget v0, p0, Lcom/pspdfkit/internal/ou;->b:I

    if-eq v0, v5, :cond_7

    if-ne v0, v6, :cond_6

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    :cond_7
    :goto_4
    if-eqz v1, :cond_8

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/ou;->a:Landroid/view/View;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v4}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/ou;->e:Landroid/view/animation/DecelerateInterpolator;

    .line 59
    invoke-virtual {v0, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    iget-wide v2, p0, Lcom/pspdfkit/internal/ou;->c:J

    .line 60
    invoke-virtual {v0, v2, v3}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v2, Lcom/pspdfkit/internal/ou$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v1, p1}, Lcom/pspdfkit/internal/ou$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/ou;ZLio/reactivex/rxjava3/core/CompletableEmitter;)V

    .line 61
    invoke-virtual {v0, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.class final Lcom/pspdfkit/internal/w9$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/w9;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/datastructures/Range;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic e:Lcom/pspdfkit/internal/w9;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/w9;IIILcom/pspdfkit/datastructures/Range;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/w9$c;->e:Lcom/pspdfkit/internal/w9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p2, p0, Lcom/pspdfkit/internal/w9$c;->a:I

    .line 3
    iput p4, p0, Lcom/pspdfkit/internal/w9$c;->c:I

    .line 4
    iput p3, p0, Lcom/pspdfkit/internal/w9$c;->b:I

    .line 5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/w9$c;->d:Ljava/util/List;

    .line 7
    invoke-virtual {p1, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/w9$c;->e:Lcom/pspdfkit/internal/w9;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/w9$c;->a:I

    invoke-static {v1}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v4, :cond_1

    const/4 v5, 0x2

    if-eq v1, v5, :cond_0

    .line 13
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 14
    :cond_0
    iget v1, p0, Lcom/pspdfkit/internal/w9$c;->c:I

    new-array v5, v5, [Ljava/lang/Object;

    .line 15
    sget v6, Lcom/pspdfkit/R$string;->pspdf__all:I

    .line 16
    invoke-static {v0, v6, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    .line 17
    sget v6, Lcom/pspdfkit/R$plurals;->pspdf__pages_number:I

    new-array v7, v4, [Ljava/lang/Object;

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v0, v6, v3, v1, v7}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/widget/TextView;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v4

    const-string v0, "%s (%s)"

    .line 19
    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 20
    :cond_1
    sget v1, Lcom/pspdfkit/R$string;->pspdf__page_range:I

    .line 21
    invoke-static {v0, v1, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 22
    :cond_2
    sget v1, Lcom/pspdfkit/R$string;->pspdf__current_page:I

    new-array v5, v4, [Ljava/lang/Object;

    iget v6, p0, Lcom/pspdfkit/internal/w9$c;->b:I

    add-int/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {v0, v1, v3, v5}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

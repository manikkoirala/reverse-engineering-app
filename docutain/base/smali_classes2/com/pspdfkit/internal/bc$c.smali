.class final Lcom/pspdfkit/internal/bc$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/bc;->formDidChangeButtonSelection(Lcom/pspdfkit/internal/jni/NativeDocument;ILjava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/pspdfkit/internal/bc;

.field final synthetic c:Z


# direct methods
.method constructor <init>(ILcom/pspdfkit/internal/bc;Z)V
    .locals 0

    iput p1, p0, Lcom/pspdfkit/internal/bc$c;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/bc$c;->b:Lcom/pspdfkit/internal/bc;

    iput-boolean p3, p0, Lcom/pspdfkit/internal/bc$c;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 5

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/FormField;

    const-string v0, "formField"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    instance-of v0, p1, Lcom/pspdfkit/forms/EditableButtonFormField;

    if-nez v0, :cond_0

    goto :goto_1

    .line 199
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/bc$c;->a:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/hc;->a(Lcom/pspdfkit/forms/FormField;I)Lcom/pspdfkit/forms/FormElement;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/forms/EditableButtonFormElement;

    if-nez v0, :cond_1

    goto :goto_1

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/bc$c;->b:Lcom/pspdfkit/internal/bc;

    invoke-static {v1}, Lcom/pspdfkit/internal/bc;->a(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;

    move-result-object v1

    iget-boolean v2, p0, Lcom/pspdfkit/internal/bc$c;->c:Z

    .line 420
    invoke-virtual {v1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;

    .line 421
    move-object v4, p1

    check-cast v4, Lcom/pspdfkit/forms/EditableButtonFormField;

    invoke-interface {v3, v4, v0, v2}, Lcom/pspdfkit/forms/FormListeners$OnButtonFormFieldUpdatedListener;->onButtonSelected(Lcom/pspdfkit/forms/EditableButtonFormField;Lcom/pspdfkit/forms/EditableButtonFormElement;Z)V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

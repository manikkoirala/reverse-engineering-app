.class public final enum Lcom/pspdfkit/internal/z1$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/z1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/z1$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/pspdfkit/internal/z1$b;

.field public static final enum b:Lcom/pspdfkit/internal/z1$b;

.field public static final enum c:Lcom/pspdfkit/internal/z1$b;

.field public static final enum d:Lcom/pspdfkit/internal/z1$b;

.field public static final enum e:Lcom/pspdfkit/internal/z1$b;

.field public static final enum f:Lcom/pspdfkit/internal/z1$b;

.field public static final enum g:Lcom/pspdfkit/internal/z1$b;

.field public static final enum h:Lcom/pspdfkit/internal/z1$b;

.field public static final enum i:Lcom/pspdfkit/internal/z1$b;

.field private static final synthetic j:[Lcom/pspdfkit/internal/z1$b;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/z1$b;

    const-string v1, "TOP_LEFT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    new-instance v1, Lcom/pspdfkit/internal/z1$b;

    const-string v3, "TOP_CENTER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    new-instance v3, Lcom/pspdfkit/internal/z1$b;

    const-string v5, "TOP_RIGHT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    new-instance v5, Lcom/pspdfkit/internal/z1$b;

    const-string v7, "CENTER_LEFT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    new-instance v7, Lcom/pspdfkit/internal/z1$b;

    const-string v9, "CENTER_RIGHT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    new-instance v9, Lcom/pspdfkit/internal/z1$b;

    const-string v11, "BOTTOM_LEFT"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    new-instance v11, Lcom/pspdfkit/internal/z1$b;

    const-string v13, "BOTTOM_CENTER"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    new-instance v13, Lcom/pspdfkit/internal/z1$b;

    const-string v15, "BOTTOM_RIGHT"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    new-instance v15, Lcom/pspdfkit/internal/z1$b;

    const-string v14, "ROTATION"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/internal/z1$b;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    const/16 v14, 0x9

    new-array v14, v14, [Lcom/pspdfkit/internal/z1$b;

    aput-object v0, v14, v2

    aput-object v1, v14, v4

    aput-object v3, v14, v6

    aput-object v5, v14, v8

    aput-object v7, v14, v10

    const/4 v0, 0x5

    aput-object v9, v14, v0

    const/4 v0, 0x6

    aput-object v11, v14, v0

    const/4 v0, 0x7

    aput-object v13, v14, v0

    aput-object v15, v14, v12

    .line 3
    sput-object v14, Lcom/pspdfkit/internal/z1$b;->j:[Lcom/pspdfkit/internal/z1$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/z1$b;
    .locals 1

    const-class v0, Lcom/pspdfkit/internal/z1$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/z1$b;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/z1$b;
    .locals 1

    sget-object v0, Lcom/pspdfkit/internal/z1$b;->j:[Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/z1$b;

    return-object v0
.end method

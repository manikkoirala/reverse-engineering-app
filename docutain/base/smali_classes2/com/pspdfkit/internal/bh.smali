.class public final Lcom/pspdfkit/internal/bh;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# static fields
.field public static final synthetic e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x4

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/bh;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ot;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method public final f(I)Lcom/pspdfkit/internal/ea;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ea;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ea;-><init>()V

    const/4 v1, 0x4

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0xc

    add-int/2addr p1, v1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ea;->a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/ea;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

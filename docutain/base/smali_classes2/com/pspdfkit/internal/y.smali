.class public final Lcom/pspdfkit/internal/y;
.super Lcom/pspdfkit/internal/g2;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g2<",
        "Lcom/pspdfkit/internal/x;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lcom/pspdfkit/ui/PdfFragment;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Lcom/pspdfkit/ui/PdfFragment;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/qf;",
            "Landroid/util/SparseIntArray;",
            "Lcom/pspdfkit/ui/PdfFragment;",
            "Lcom/pspdfkit/internal/k4$a<",
            "-",
            "Lcom/pspdfkit/internal/x;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/pspdfkit/internal/x;

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/pspdfkit/internal/g2;-><init>(Lcom/pspdfkit/internal/qf;Landroid/util/SparseIntArray;Ljava/lang/Class;Lcom/pspdfkit/internal/k4$a;)V

    .line 2
    iput-object p3, p0, Lcom/pspdfkit/internal/y;->e:Lcom/pspdfkit/ui/PdfFragment;

    return-void
.end method

.method private static a(Lcom/pspdfkit/internal/x;)Lcom/pspdfkit/annotations/Annotation;
    .locals 4

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/y$a;->a:[I

    iget-object v1, p0, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t create annotation of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/pspdfkit/internal/x;->d:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :pswitch_0
    new-instance v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    iget-object v3, p0, Lcom/pspdfkit/internal/x;->g:Lcom/pspdfkit/annotations/sound/EmbeddedAudioSource;

    invoke-direct {v0, v2, v1, v3}, Lcom/pspdfkit/annotations/SoundAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLcom/pspdfkit/annotations/sound/EmbeddedAudioSource;)V

    goto/16 :goto_0

    .line 63
    :pswitch_1
    new-instance v0, Lcom/pspdfkit/annotations/RedactionAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/RedactionAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_0

    .line 64
    :pswitch_2
    new-instance v0, Lcom/pspdfkit/annotations/PolylineAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/PolylineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto/16 :goto_0

    .line 65
    :pswitch_3
    new-instance v0, Lcom/pspdfkit/annotations/PolygonAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/PolygonAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 66
    :pswitch_4
    new-instance v0, Lcom/pspdfkit/annotations/StampAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    iget-object v3, p0, Lcom/pspdfkit/internal/x;->f:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2, v1, v3}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(Lcom/pspdfkit/internal/p1;ZLandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 67
    :pswitch_5
    new-instance v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/NoteAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 68
    :pswitch_6
    new-instance v0, Lcom/pspdfkit/annotations/LineAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/LineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 69
    :pswitch_7
    new-instance v0, Lcom/pspdfkit/annotations/CircleAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 70
    :pswitch_8
    new-instance v0, Lcom/pspdfkit/annotations/SquareAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/SquareAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 71
    :pswitch_9
    new-instance v0, Lcom/pspdfkit/annotations/InkAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/InkAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 72
    :pswitch_a
    new-instance v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 73
    :pswitch_b
    new-instance v0, Lcom/pspdfkit/annotations/SquigglyAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/SquigglyAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 74
    :pswitch_c
    new-instance v0, Lcom/pspdfkit/annotations/UnderlineAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/UnderlineAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 75
    :pswitch_d
    new-instance v0, Lcom/pspdfkit/annotations/StrikeOutAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/StrikeOutAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 76
    :pswitch_e
    new-instance v0, Lcom/pspdfkit/annotations/HighlightAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/HighlightAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    goto :goto_0

    .line 77
    :pswitch_f
    new-instance v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/LinkAnnotation;-><init>(Lcom/pspdfkit/internal/p1;Z)V

    .line 137
    :goto_0
    iget-object p0, p0, Lcom/pspdfkit/internal/x;->h:Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;

    if-eqz p0, :cond_0

    .line 138
    invoke-virtual {v0, p0}, Lcom/pspdfkit/annotations/Annotation;->setAppearanceStreamGenerator(Lcom/pspdfkit/annotations/appearance/AppearanceStreamGenerator;)V

    :cond_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 5

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/x;

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 3
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v4, p1, Lcom/pspdfkit/internal/zl;->a:I

    iget p1, p1, Lcom/pspdfkit/internal/p0;->b:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(I)I

    move-result p1

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v4, p1}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-ne v0, p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 5

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/x;

    .line 2
    iget-object v0, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 3
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    iget v4, p1, Lcom/pspdfkit/internal/zl;->a:I

    iget p1, p1, Lcom/pspdfkit/internal/p0;->b:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(I)I

    move-result p1

    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v4, p1}, Lcom/pspdfkit/internal/r1;->getAnnotation(II)Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eq v0, p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public final f(Lcom/pspdfkit/internal/ja;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/x;

    .line 2
    :try_start_0
    iget-object v0, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-ne v0, v1, :cond_0

    .line 3
    :try_start_1
    invoke-static {p1}, Lcom/pspdfkit/internal/y;->a(Lcom/pspdfkit/internal/x;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 5
    iget v1, p1, Lcom/pspdfkit/internal/p0;->b:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/g2;->a(I)I

    move-result v1

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2, v0, v3, v4}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/y;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 10
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/g2;->a(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 13
    :try_start_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not add annotation to the document."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 14
    :cond_0
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    invoke-interface {v1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/y;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_1
    move-exception v0

    .line 18
    :try_start_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not remove annotation from the document."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    nop

    .line 19
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not redo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 20
    iget-object p1, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v2, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    if-ne p1, v2, :cond_2

    const-string p1, "adding"

    goto :goto_1

    :cond_2
    const-string p1, "removing"

    :goto_1
    const-string v2, " of the annotation."

    .line 21
    invoke-static {v1, p1, v2}, Lcom/pspdfkit/internal/rg;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 22
    invoke-direct {v0, p1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g(Lcom/pspdfkit/internal/ja;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/x;

    .line 2
    :try_start_0
    iget-object v0, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v1, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-ne v0, v1, :cond_0

    .line 3
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g2;->a(Lcom/pspdfkit/internal/p0;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    invoke-interface {v1, v0}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/y;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 7
    :try_start_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not remove annotation from the document."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 8
    :cond_0
    :try_start_3
    invoke-static {p1}, Lcom/pspdfkit/internal/y;->a(Lcom/pspdfkit/internal/x;)Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 10
    iget v1, p1, Lcom/pspdfkit/internal/p0;->b:I

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/g2;->a(I)I

    move-result v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/g2;->c:Lcom/pspdfkit/internal/qf;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    check-cast v2, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v2, v0, v3, v4}, Lcom/pspdfkit/internal/r1;->a(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/y;->e:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getObjectNumber()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 15
    invoke-virtual {p0, v1, v0}, Lcom/pspdfkit/internal/g2;->a(II)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_1
    move-exception v0

    .line 18
    :try_start_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not add annotation to the document."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    nop

    .line 19
    new-instance v0, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not undo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 20
    iget-object v2, p1, Lcom/pspdfkit/internal/x;->e:Lcom/pspdfkit/internal/x$a;

    sget-object v3, Lcom/pspdfkit/internal/x$a;->a:Lcom/pspdfkit/internal/x$a;

    if-ne v2, v3, :cond_2

    const-string v2, "adding"

    goto :goto_1

    :cond_2
    const-string v2, "removing"

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " of the annotation. Annotation properties: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/pspdfkit/internal/x;->c:Lcom/pspdfkit/internal/p1;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

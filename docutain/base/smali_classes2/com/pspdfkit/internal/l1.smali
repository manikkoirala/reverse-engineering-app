.class public final Lcom/pspdfkit/internal/l1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/xn;

.field private final c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

.field private final d:Lcom/pspdfkit/internal/i0;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/l1;->a:Landroid/content/Context;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/xn;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/xn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 4
    new-instance v0, Lcom/pspdfkit/internal/i0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/i0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->get(Landroid/content/Context;)Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/l1;->c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    return-void
.end method

.method private static a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;
    .locals 1

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "_"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/i0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    return-object v0
.end method

.method public final getAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)F
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result p1

    return p1
.end method

.method public final getAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getDefaultAlpha()F

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_alpha_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;->getDefaultAlpha()F

    move-result p2

    goto :goto_0

    :cond_1
    const/high16 p2, 0x3f800000    # 1.0f

    .line 11
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result p1

    return p1
.end method

.method public final getAnnotationCreator()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->getAnnotationCreator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    return-object p1
.end method

.method public final getBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;
    .locals 10

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;->getDefaultBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_border_style_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 9
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 11
    iget-object v2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "annotation_preferences_border_effect_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 12
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 13
    invoke-virtual {v2, v4, v3}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 15
    iget-object v4, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "annotation_preferences_border_effect_intensity_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 16
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 17
    invoke-virtual {v4, v5, v6}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result v4

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 20
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    .line 21
    invoke-static {v1}, Lcom/pspdfkit/annotations/BorderStyle;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v1

    .line 22
    invoke-static {v2}, Lcom/pspdfkit/annotations/BorderEffect;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "annotation_preferences_dash_array_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 24
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 25
    iget-object p2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 26
    iget-object p2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    const-string v3, ""

    invoke-virtual {p2, p1, v3}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, ";"

    invoke-static {p1, p2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 27
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 28
    array-length p2, p1

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, p2, :cond_1

    aget-object v7, p1, v6

    .line 30
    :try_start_0
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v7, v8, v5

    const-string v7, "PSPDFKit.Internal.AnnotationPreferencesManagerImpl"

    const-string v9, "Parsing string %s to Integer failed and the exception was ignored."

    .line 32
    invoke-static {v7, v9, v8}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 33
    :cond_1
    invoke-direct {v0, v1, v2, v4, v3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;Lcom/pspdfkit/annotations/BorderEffect;FLjava/util/List;)V

    return-object v0

    :cond_2
    if-eqz v0, :cond_3

    .line 35
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;->getDefaultBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object p1

    return-object p1

    .line 37
    :cond_3
    new-instance p1, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    sget-object p2, Lcom/pspdfkit/annotations/BorderStyle;->SOLID:Lcom/pspdfkit/annotations/BorderStyle;

    invoke-direct {p1, p2}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;-><init>(Lcom/pspdfkit/annotations/BorderStyle;)V

    return-object p1
.end method

.method public final getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    return p1
.end method

.method public final getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getDefaultColor()I

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_color_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;->getDefaultColor()I

    move-result p1

    goto :goto_0

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    .line 12
    :goto_0
    invoke-virtual {v1, p1, v2}, Lcom/pspdfkit/internal/xn;->a(ILjava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final getFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    return p1
.end method

.method public final getFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getDefaultFillColor()I

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_fill_color_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;->getDefaultFillColor()I

    move-result p1

    goto :goto_0

    .line 11
    :cond_1
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const-string v0, "annotationTool"

    .line 12
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 513
    sget-object v0, Lcom/pspdfkit/internal/ao$a;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0x15

    if-ne p1, v0, :cond_2

    const/high16 p1, -0x1000000

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 514
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(ILjava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final getFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p1

    return-object p1
.end method

.method public final getFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;->getDefaultPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    sget-object v1, Lcom/pspdfkit/internal/ao;->b:Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "annotation_preferences_float_precision_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 11
    invoke-virtual {v2, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/annotations/measurements/FloatPrecision;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 16
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;->getDefaultPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v1

    :cond_2
    :goto_0
    return-object v1
.end method

.method public final getFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Lcom/pspdfkit/ui/fonts/Font;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    return-object p1
.end method

.method public final getFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/ui/fonts/Font;
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;->getDefaultFont()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_font_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object p2

    .line 10
    invoke-virtual {p2}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/fonts/Font;

    if-nez p1, :cond_1

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/mt;->getFontByName(Ljava/lang/String;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object p1

    if-eqz p1, :cond_2

    move-object v0, p1

    :cond_2
    :goto_0
    return-object v0
.end method

.method public final getLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            ")",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Landroidx/core/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final getLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Landroidx/core/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ")",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;->getDefaultLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    sget-object v1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "annotation_preferences_line_start_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 11
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 12
    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 15
    invoke-static {v2}, Lcom/pspdfkit/annotations/LineEndType;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/LineEndType;

    move-result-object v2

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 17
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;->getDefaultLineEnds()Landroidx/core/util/Pair;

    move-result-object v2

    iget-object v2, v2, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/pspdfkit/annotations/LineEndType;

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 20
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "annotation_preferences_line_end_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 21
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 22
    invoke-virtual {v3, p1, v4}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 25
    invoke-static {p1}, Lcom/pspdfkit/annotations/LineEndType;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/LineEndType;

    move-result-object v1

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    .line 27
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;->getDefaultLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/annotations/LineEndType;

    .line 29
    :cond_4
    :goto_1
    new-instance p1, Landroidx/core/util/Pair;

    invoke-direct {p1, v2, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method public final getMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    return-object p1
.end method

.method public final getMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Lcom/pspdfkit/annotations/measurements/Scale;
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;->getDefaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object p1

    return-object p1

    :cond_0
    if-eqz v0, :cond_1

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;->getDefaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/pspdfkit/annotations/measurements/Scale;->defaultScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    .line 9
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_scale_value_from_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    .line 11
    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result v1

    .line 13
    iget-object v2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "annotation_preferences_scale_value_to_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 14
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, v0, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    .line 15
    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result v2

    .line 17
    iget-object v3, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "annotation_preferences_scale_unit_from_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 18
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 19
    invoke-virtual {v5}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v5

    .line 20
    invoke-virtual {v3, v4, v5}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    move-result-object v3

    .line 23
    iget-object v4, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "annotation_preferences_scale_unit_to_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 24
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, v0, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    .line 25
    invoke-virtual {v4, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;->valueOf(Ljava/lang/String;)Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    move-result-object p1

    .line 28
    new-instance p2, Lcom/pspdfkit/annotations/measurements/Scale;

    invoke-direct {p2, v1, v3, v2, p1}, Lcom/pspdfkit/annotations/measurements/Scale;-><init>(FLcom/pspdfkit/annotations/measurements/Scale$UnitFrom;FLcom/pspdfkit/annotations/measurements/Scale$UnitTo;)V

    return-object p2
.end method

.method public final getNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;->getDefaultIconName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_note_icon_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Note"

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationNoteIconConfiguration;->getDefaultIconName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p2

    .line 12
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    move-object p2, p1

    :cond_2
    return-object p2
.end method

.method public final getOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)I
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result p1

    return p1
.end method

.method public final getOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getDefaultOutlineColor()I

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_outline_color_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;->getDefaultOutlineColor()I

    move-result p1

    goto :goto_0

    .line 11
    :cond_1
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const-string v0, "annotationTool"

    .line 12
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 513
    sget-object v0, Lcom/pspdfkit/internal/ao$a;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0x15

    if-ne p1, v0, :cond_2

    const/high16 p1, -0x1000000

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 514
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(ILjava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final getOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;->getDefaultOverlayText()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_overlay_text_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 9
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    return-object p1

    :cond_1
    if-eqz v0, :cond_2

    .line 14
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;->getDefaultOverlayText()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method

.method public final getRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Z

    move-result p1

    return p1
.end method

.method public final getRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Z
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;->getDefaultRepeatOverlayTextSetting()Z

    move-result p1

    return p1

    :cond_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;->getDefaultRepeatOverlayTextSetting()Z

    move-result v1

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_repeat_overlay_text_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 12
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 13
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public final getTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)F
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result p1

    return p1
.end method

.method public final getTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    .line 3
    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getDefaultTextSize()F

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_text_size_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;->getDefaultTextSize()F

    move-result p2

    goto :goto_0

    :cond_1
    const/high16 p2, 0x41900000    # 18.0f

    .line 11
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result p1

    return p1
.end method

.method public final getThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)F
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/l1;->getThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F

    move-result p1

    return p1
.end method

.method public final getThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)F
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->d:Lcom/pspdfkit/internal/i0;

    const-class v1, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    invoke-virtual {v0, p1, p2, v1}, Lcom/pspdfkit/internal/i0;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;->getForceDefaults()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getDefaultThickness()F

    move-result p1

    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "annotation_preferences_thickness_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;->getDefaultThickness()F

    move-result p2

    goto :goto_0

    :cond_1
    const/high16 p2, 0x40a00000    # 5.0f

    .line 11
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/pspdfkit/internal/xn;->a(Ljava/lang/String;F)F

    move-result p1

    return p1
.end method

.method public final isAnnotationCreatorSet()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/l1;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isMeasurementSnappingEnabled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final setAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;F)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    return-void
.end method

.method public final setAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_alpha_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.method public final setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_border_style_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    .line 6
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_border_effect_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    .line 12
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_border_effect_intensity_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 16
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getBorderEffectIntensity()F

    move-result v2

    .line 18
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 21
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_dash_array_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 23
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 24
    invoke-virtual {p3}, Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;->getDashArray()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Integer;

    .line 25
    invoke-interface {p2, p3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    const-string p3, ";"

    invoke-static {p3, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 26
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 28
    :cond_0
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 29
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;I)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    return-void
.end method

.method public final setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_color_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;I)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    return-void
.end method

.method public final setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_fill_color_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method public final setFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_float_precision_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    .line 6
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 8
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method public final setFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_font_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/pspdfkit/internal/l1;->setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public final setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_line_start_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p3

    .line 6
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p3

    .line 8
    invoke-interface {p3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 9
    iget-object p3, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 10
    invoke-virtual {p3}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object p3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "annotation_preferences_line_end_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 11
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 12
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void
.end method

.method public final setMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_scale_value_from_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p3, Lcom/pspdfkit/annotations/measurements/Scale;->valueFrom:F

    .line 6
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 9
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_scale_value_to_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 13
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p3, Lcom/pspdfkit/annotations/measurements/Scale;->valueTo:F

    .line 14
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 17
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_scale_unit_from_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 21
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p3, Lcom/pspdfkit/annotations/measurements/Scale;->unitFrom:Lcom/pspdfkit/annotations/measurements/Scale$UnitFrom;

    .line 22
    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    .line 23
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 26
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 28
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_scale_unit_to_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p3, Lcom/pspdfkit/annotations/measurements/Scale;->unitTo:Lcom/pspdfkit/annotations/measurements/Scale$UnitTo;

    .line 31
    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    .line 32
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 35
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setMeasurementSnappingEnabled(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->c:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->setMeasurementSnappingEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final setNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_note_icon_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V

    return-void
.end method

.method public final setOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;I)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    return-void
.end method

.method public final setOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_outline_color_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V
    .locals 2

    .line 2
    iget-object p2, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {p2}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "annotation_preferences_overlay_text_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V

    return-void
.end method

.method public final setRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_repeat_overlay_text_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 6
    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 9
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Z)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    return-void
.end method

.method public final setTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;F)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    return-void
.end method

.method public final setTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_text_size_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final setThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;F)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;->defaultVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/l1;->setThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    return-void
.end method

.method public final setThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/l1;->b:Lcom/pspdfkit/internal/xn;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/xn;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "annotation_preferences_thickness_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/l1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 5
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

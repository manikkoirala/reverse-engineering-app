.class public final Lcom/pspdfkit/internal/id;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;IJ)I
    .locals 1

    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/mb;->e(I)V

    const/4 v0, 0x1

    .line 3
    invoke-virtual {p0, v0, p2, p3}, Lcom/pspdfkit/internal/mb;->a(IJ)V

    const/4 p2, 0x0

    .line 4
    invoke-virtual {p0, p2, p1}, Lcom/pspdfkit/internal/mb;->b(II)V

    .line 5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method


# virtual methods
.method public final a()J
    .locals 3

    const/4 v0, 0x6

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x4

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

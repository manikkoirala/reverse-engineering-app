.class final Lcom/pspdfkit/internal/b3$b;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/ls;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/annotations/SoundAnnotation;

.field final synthetic b:Lcom/pspdfkit/internal/ls;


# direct methods
.method constructor <init>(Lcom/pspdfkit/annotations/SoundAnnotation;Lcom/pspdfkit/internal/ls;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/b3$b;->a:Lcom/pspdfkit/annotations/SoundAnnotation;

    iput-object p2, p0, Lcom/pspdfkit/internal/b3$b;->b:Lcom/pspdfkit/internal/ls;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/b3$b;->a:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getSoundAnnotationState()Lcom/pspdfkit/internal/ls;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/b3$b;->b:Lcom/pspdfkit/internal/ls;

    if-eq v0, v1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/b3$b;->a:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/b3$b;->b:Lcom/pspdfkit/internal/ls;

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setSoundAnnotationState(Lcom/pspdfkit/internal/ls;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/b3$b;->a:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/b3$b;->a:Lcom/pspdfkit/annotations/SoundAnnotation;

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/r1;->j(Lcom/pspdfkit/annotations/Annotation;)V

    .line 4
    :cond_0
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

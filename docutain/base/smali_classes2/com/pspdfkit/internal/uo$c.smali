.class final Lcom/pspdfkit/internal/uo$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/zf;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/uo;

.field final synthetic b:Lcom/pspdfkit/internal/zf;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/uo$c;->a:Lcom/pspdfkit/internal/uo;

    iput-object p2, p0, Lcom/pspdfkit/internal/uo$c;->b:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uo$c;->a:Lcom/pspdfkit/internal/uo;

    invoke-static {v0}, Lcom/pspdfkit/internal/uo;->a(Lcom/pspdfkit/internal/uo;)Lcom/pspdfkit/ui/PdfUi;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getPageIndex()I

    move-result p1

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfUi;->getDocumentCoordinator()Lcom/pspdfkit/ui/DocumentCoordinator;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/uo$c;->b:Lcom/pspdfkit/internal/zf;

    invoke-static {v2}, Lcom/pspdfkit/ui/DocumentDescriptor;->fromDocument(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/ui/DocumentDescriptor;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/pspdfkit/ui/DocumentCoordinator;->setDocument(Lcom/pspdfkit/ui/DocumentDescriptor;)Z

    .line 7
    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/PdfUi;->setPageIndex(I)V

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/uo$c;->a:Lcom/pspdfkit/internal/uo;

    invoke-static {p1}, Lcom/pspdfkit/internal/uo;->d(Lcom/pspdfkit/internal/uo;)V

    return-void
.end method

.class public final enum Lcom/pspdfkit/internal/yq$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/internal/yq$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lcom/pspdfkit/internal/yq$b;

.field public static final enum e:Lcom/pspdfkit/internal/yq$b;

.field private static final synthetic f:[Lcom/pspdfkit/internal/yq$b;


# instance fields
.field private final a:Lcom/pspdfkit/configuration/page/PageScrollMode;

.field private final b:Lcom/pspdfkit/configuration/page/PageLayoutMode;

.field private final c:Lcom/pspdfkit/configuration/page/PageScrollDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v6, Lcom/pspdfkit/internal/yq$b;

    sget-object v3, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    sget-object v11, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    sget-object v5, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    const-string v1, "HORIZONTAL"

    const/4 v2, 0x0

    move-object v0, v6

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/yq$b;-><init>(Ljava/lang/String;ILcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    sput-object v6, Lcom/pspdfkit/internal/yq$b;->d:Lcom/pspdfkit/internal/yq$b;

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/yq$b;

    sget-object v10, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    sget-object v12, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    const-string v8, "VERTICAL"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/pspdfkit/internal/yq$b;-><init>(Ljava/lang/String;ILcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    sput-object v0, Lcom/pspdfkit/internal/yq$b;->e:Lcom/pspdfkit/internal/yq$b;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/pspdfkit/internal/yq$b;

    aput-object v6, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 4
    sput-object v1, Lcom/pspdfkit/internal/yq$b;->f:[Lcom/pspdfkit/internal/yq$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/pspdfkit/configuration/page/PageScrollMode;Lcom/pspdfkit/configuration/page/PageLayoutMode;Lcom/pspdfkit/configuration/page/PageScrollDirection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/configuration/page/PageScrollMode;",
            "Lcom/pspdfkit/configuration/page/PageLayoutMode;",
            "Lcom/pspdfkit/configuration/page/PageScrollDirection;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/pspdfkit/internal/yq$b;->a:Lcom/pspdfkit/configuration/page/PageScrollMode;

    iput-object p4, p0, Lcom/pspdfkit/internal/yq$b;->b:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    iput-object p5, p0, Lcom/pspdfkit/internal/yq$b;->c:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/internal/yq$b;
    .locals 1

    const-class v0, Lcom/pspdfkit/internal/yq$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/yq$b;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/internal/yq$b;
    .locals 1

    sget-object v0, Lcom/pspdfkit/internal/yq$b;->f:[Lcom/pspdfkit/internal/yq$b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/internal/yq$b;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/configuration/page/PageLayoutMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->b:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/ar;)Z
    .locals 2

    const-string v0, "options"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->a:Lcom/pspdfkit/configuration/page/PageScrollMode;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->b:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->c:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()Lcom/pspdfkit/configuration/page/PageScrollDirection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->c:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/configuration/page/PageScrollMode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq$b;->a:Lcom/pspdfkit/configuration/page/PageScrollMode;

    return-object v0
.end method

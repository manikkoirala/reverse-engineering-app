.class final Lcom/pspdfkit/internal/he;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/graphics/Path;Ljava/util/List;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Path;",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_5

    .line 6
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    .line 9
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 10
    iget v5, v4, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v13, p0

    invoke-virtual {v13, v5, v4}, Landroid/graphics/Path;->moveTo(FF)V

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_0

    .line 15
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float v1, v1, v4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v5

    const/high16 v5, 0x40400000    # 3.0f

    div-float v7, v1, v5

    .line 16
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    mul-float v1, v1, v4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v6

    div-float v8, v1, v5

    mul-float v1, v7, v4

    .line 19
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float v9, v1, v5

    mul-float v4, v4, v8

    .line 20
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float v10, v4, v1

    .line 22
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 23
    iget v11, v0, Landroid/graphics/PointF;->x:F

    iget v12, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_4

    .line 27
    :cond_0
    new-array v5, v1, [D

    .line 29
    new-array v6, v1, [D

    const/4 v7, 0x1

    :goto_0
    add-int/lit8 v14, v1, -0x1

    const/high16 v8, 0x40800000    # 4.0f

    if-ge v7, v14, :cond_1

    .line 33
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    mul-float v9, v9, v8

    add-int/lit8 v8, v7, 0x1

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    mul-float v10, v10, v4

    add-float/2addr v10, v9

    float-to-double v9, v10

    aput-wide v9, v5, v7

    move v7, v8

    goto :goto_0

    .line 35
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    mul-float v9, v9, v4

    add-float/2addr v9, v7

    float-to-double v9, v9

    aput-wide v9, v5, v3

    .line 36
    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    const/high16 v9, 0x41000000    # 8.0f

    mul-float v7, v7, v9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v10

    float-to-double v10, v7

    const-wide/high16 v15, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v15

    aput-wide v10, v5, v14

    .line 38
    invoke-static {v5, v6}, Lcom/pspdfkit/internal/he;->a([D[D)[D

    move-result-object v17

    const/4 v7, 0x1

    :goto_1
    if-ge v7, v14, :cond_2

    .line 42
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    mul-float v10, v10, v8

    add-int/lit8 v11, v7, 0x1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/PointF;

    iget v12, v12, Landroid/graphics/PointF;->y:F

    mul-float v12, v12, v4

    add-float/2addr v12, v10

    float-to-double v8, v12

    aput-wide v8, v5, v7

    move v7, v11

    const/high16 v8, 0x40800000    # 4.0f

    const/high16 v9, 0x41000000    # 8.0f

    goto :goto_1

    .line 44
    :cond_2
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float v2, v2, v4

    add-float/2addr v2, v7

    float-to-double v7, v2

    aput-wide v7, v5, v3

    .line 45
    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    const/high16 v7, 0x41000000    # 8.0f

    mul-float v2, v2, v7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v7

    float-to-double v7, v2

    div-double/2addr v7, v15

    aput-wide v7, v5, v14

    .line 47
    invoke-static {v5, v6}, Lcom/pspdfkit/internal/he;->a([D[D)[D

    move-result-object v2

    :goto_2
    if-ge v3, v1, :cond_4

    .line 53
    aget-wide v5, v17, v3

    double-to-float v7, v5

    .line 54
    aget-wide v5, v2, v3

    double-to-float v8, v5

    if-ge v3, v14, :cond_3

    add-int/lit8 v5, v3, 0x1

    .line 60
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    mul-float v6, v6, v4

    float-to-double v9, v6

    aget-wide v11, v17, v5

    sub-double/2addr v9, v11

    double-to-float v6, v9

    .line 61
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    mul-float v9, v9, v4

    float-to-double v9, v9

    aget-wide v11, v2, v5

    sub-double/2addr v9, v11

    double-to-float v5, v9

    move v10, v5

    move v9, v6

    goto :goto_3

    .line 63
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    aget-wide v9, v17, v14

    add-double/2addr v5, v9

    double-to-float v5, v5

    div-float/2addr v5, v4

    .line 64
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-double v9, v6

    aget-wide v11, v2, v14

    add-double/2addr v9, v11

    double-to-float v6, v9

    div-float/2addr v6, v4

    move v9, v5

    move v10, v6

    :goto_3
    add-int/lit8 v3, v3, 0x1

    .line 68
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 71
    iget v11, v5, Landroid/graphics/PointF;->x:F

    iget v12, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto :goto_2

    :cond_4
    :goto_4
    return-void

    .line 72
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error building spline for ink annotation. At least two knot points required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a([D[D)[D
    .locals 11

    .line 73
    array-length v0, p0

    .line 74
    new-array v1, v0, [D

    const/4 v2, 0x0

    .line 78
    aget-wide v3, p0, v2

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v5

    aput-wide v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    :goto_0
    if-ge v3, v0, :cond_1

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    div-double/2addr v7, v5

    .line 81
    aput-wide v7, p1, v3

    add-int/lit8 v4, v0, -0x1

    if-ge v3, v4, :cond_0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    goto :goto_1

    :cond_0
    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    :goto_1
    sub-double v5, v4, v7

    .line 83
    aget-wide v7, p0, v3

    add-int/lit8 v4, v3, -0x1

    aget-wide v9, v1, v4

    sub-double/2addr v7, v9

    div-double/2addr v7, v5

    aput-wide v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_2
    if-ge v2, v0, :cond_2

    sub-int p0, v0, v2

    add-int/lit8 v3, p0, -0x1

    .line 88
    aget-wide v4, v1, v3

    aget-wide v6, p1, p0

    aget-wide v8, v1, p0

    mul-double v6, v6, v8

    sub-double/2addr v4, v6

    aput-wide v4, v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    return-object v1
.end method

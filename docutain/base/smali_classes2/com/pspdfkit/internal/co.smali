.class final Lcom/pspdfkit/internal/co;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/co$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/document/printing/PrintOptions;

.field private final b:Lcom/pspdfkit/document/processor/PdfProcessorTask;

.field private final c:Landroid/content/Context;

.field private d:Lcom/pspdfkit/internal/zf;

.field private e:Landroid/print/PrintAttributes;

.field private f:Lcom/pspdfkit/utils/Size;

.field private g:Z

.field private h:Z


# direct methods
.method public static synthetic $r8$lambda$86I3D3uIsQybFR-2tBE5QDdmVnA(Lio/reactivex/rxjava3/disposables/Disposable;Lcom/pspdfkit/internal/co$b;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/co;->a(Lio/reactivex/rxjava3/disposables/Disposable;Lcom/pspdfkit/internal/co$b;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/document/printing/PrintOptions;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetc(Lcom/pspdfkit/internal/co;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/co;->c:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/co;)Lcom/pspdfkit/internal/zf;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputd(Lcom/pspdfkit/internal/co;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputh(Lcom/pspdfkit/internal/co;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/co;->h:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/document/printing/PrintOptions;Lcom/pspdfkit/document/processor/PdfProcessorTask;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/co;->g:Z

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/co;->h:Z

    .line 10
    iput-object p1, p0, Lcom/pspdfkit/internal/co;->c:Landroid/content/Context;

    .line 11
    iput-object p2, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    .line 12
    iput-object p4, p0, Lcom/pspdfkit/internal/co;->b:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    .line 13
    iput-object p3, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    return-void
.end method

.method private static synthetic a(Lio/reactivex/rxjava3/disposables/Disposable;Lcom/pspdfkit/internal/co$b;)V
    .locals 0

    .line 102
    invoke-interface {p0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 103
    check-cast p1, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/bo$a;->a()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/print/PrintAttributes;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->e:Landroid/print/PrintAttributes;

    return-object v0
.end method

.method public final a(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Lcom/pspdfkit/internal/co$b;Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    check-cast p4, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p4}, Lcom/pspdfkit/internal/bo$a;->a()V

    return-void

    :cond_0
    const-string v0, "EXTRA_PRINT_PREVIEW"

    const/4 v1, 0x0

    .line 6
    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p5

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 7
    invoke-virtual {p1, p2}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_4

    .line 8
    iget-boolean p1, p0, Lcom/pspdfkit/internal/co;->g:Z

    if-eq p5, p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 p1, 0x1

    .line 9
    :goto_3
    iput-boolean p5, p0, Lcom/pspdfkit/internal/co;->g:Z

    .line 10
    iput-object p2, p0, Lcom/pspdfkit/internal/co;->e:Landroid/print/PrintAttributes;

    .line 12
    invoke-virtual {p2}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object p5

    if-eqz p5, :cond_5

    .line 13
    invoke-virtual {p2}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object p5

    invoke-virtual {p5}, Landroid/print/PrintAttributes$MediaSize;->getWidthMils()I

    move-result p5

    int-to-float p5, p5

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr p5, v2

    const/high16 v3, 0x42900000    # 72.0f

    mul-float p5, p5, v3

    float-to-int p5, p5

    .line 14
    invoke-virtual {p2}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object p2

    invoke-virtual {p2}, Landroid/print/PrintAttributes$MediaSize;->getHeightMils()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p2, v2

    mul-float p2, p2, v3

    float-to-int p2, p2

    .line 15
    new-instance v2, Lcom/pspdfkit/utils/Size;

    int-to-float p5, p5

    int-to-float p2, p2

    invoke-direct {v2, p5, p2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    iput-object v2, p0, Lcom/pspdfkit/internal/co;->f:Lcom/pspdfkit/utils/Size;

    goto :goto_4

    .line 17
    :cond_5
    sget-object p2, Lcom/pspdfkit/document/processor/NewPage;->PAGE_SIZE_A4:Lcom/pspdfkit/utils/Size;

    iput-object p2, p0, Lcom/pspdfkit/internal/co;->f:Lcom/pspdfkit/utils/Size;

    :goto_4
    const/4 p2, 0x0

    .line 21
    iget-boolean p5, p0, Lcom/pspdfkit/internal/co;->h:Z

    if-nez p5, :cond_7

    .line 22
    iget-object p5, p0, Lcom/pspdfkit/internal/co;->b:Lcom/pspdfkit/document/processor/PdfProcessorTask;

    if-eqz p5, :cond_6

    move-object p2, p5

    goto :goto_5

    .line 24
    :cond_6
    iget-object p5, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    if-eqz p5, :cond_7

    .line 26
    :try_start_0
    iget-object v2, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p5, v2}, Lcom/pspdfkit/document/sharing/SharingOptions;->getProcessorTask(Lcom/pspdfkit/document/PdfDocument;)Lcom/pspdfkit/document/processor/PdfProcessorTask;

    move-result-object p2
    :try_end_0
    .catch Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception p5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.PrintLayoutHandler"

    const-string v3, "Failed to create PdfProcessor instance for printing."

    .line 31
    invoke-static {v2, p5, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    move-object p5, p4

    check-cast p5, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p5}, Lcom/pspdfkit/internal/bo$a;->b()V

    :cond_7
    :goto_5
    const-string p5, ".pdf"

    const-string v1, ""

    if-eqz p2, :cond_a

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 34
    iget-object v2, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    invoke-virtual {v2}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 35
    :cond_8
    iget-object v2, p0, Lcom/pspdfkit/internal/co;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object v2

    :goto_6
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    iget-object v2, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v2

    if-nez v2, :cond_9

    goto :goto_7

    :cond_9
    move-object p5, v1

    :goto_7
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 39
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/pspdfkit/internal/co;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "print"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 43
    new-instance v2, Ljava/io/File;

    const-string v3, "[:\\\\/*\"?|<>\']"

    .line 44
    invoke-virtual {p5, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 45
    invoke-direct {v2, v0, p5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 47
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 48
    invoke-static {p2, v2}, Lcom/pspdfkit/document/processor/PdfProcessor;->processDocumentAsync(Lcom/pspdfkit/document/processor/PdfProcessorTask;Ljava/io/File;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 49
    invoke-virtual {p2}, Lio/reactivex/rxjava3/core/Flowable;->onBackpressureDrop()Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    .line 50
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p5

    check-cast p5, Lcom/pspdfkit/internal/u;

    invoke-virtual {p5}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p5

    invoke-virtual {p2, p5}, Lio/reactivex/rxjava3/core/Flowable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p2

    new-instance p5, Lcom/pspdfkit/internal/co$a;

    invoke-direct {p5, p0, p4, v2, p1}, Lcom/pspdfkit/internal/co$a;-><init>(Lcom/pspdfkit/internal/co;Lcom/pspdfkit/internal/co$b;Ljava/io/File;Z)V

    .line 51
    invoke-virtual {p2, p5}, Lio/reactivex/rxjava3/core/Flowable;->subscribeWith(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    .line 86
    new-instance p2, Lcom/pspdfkit/internal/co$$ExternalSyntheticLambda0;

    invoke-direct {p2, p1, p4}, Lcom/pspdfkit/internal/co$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/disposables/Disposable;Lcom/pspdfkit/internal/co$b;)V

    invoke-virtual {p3, p2}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    goto :goto_a

    .line 91
    :cond_a
    iput-boolean v0, p0, Lcom/pspdfkit/internal/co;->h:Z

    .line 92
    iget-object p2, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    if-lez p2, :cond_d

    .line 93
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 94
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->a:Lcom/pspdfkit/document/printing/PrintOptions;

    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/SharingOptions;->getDocumentName()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 95
    :cond_b
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-static {v0, v2}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->f()Lcom/pspdfkit/document/DocumentSource;

    move-result-object v0

    if-nez v0, :cond_c

    goto :goto_9

    :cond_c
    move-object p5, v1

    :goto_9
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 99
    check-cast p4, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p4, p3, p2, p1}, Lcom/pspdfkit/internal/bo$a;->a(Ljava/lang/String;IZ)V

    goto :goto_a

    .line 101
    :cond_d
    check-cast p4, Lcom/pspdfkit/internal/bo$a;

    invoke-virtual {p4}, Lcom/pspdfkit/internal/bo$a;->b()V

    :goto_a
    return-void
.end method

.method public final b()Lcom/pspdfkit/internal/zf;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->d:Lcom/pspdfkit/internal/zf;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/co;->f:Lcom/pspdfkit/utils/Size;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/co;->g:Z

    return v0
.end method

.class public final Lcom/pspdfkit/internal/n4;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/n4$a;
    }
.end annotation


# static fields
.field private static e:Z = false

.field private static f:I

.field private static g:I


# instance fields
.field final a:J

.field final b:Ljava/util/ArrayDeque;

.field private final c:Ljava/util/ArrayDeque;

.field d:J


# direct methods
.method public static synthetic $r8$lambda$2PPyB5Nx_bzZsGr2YYgSyHOjs78(Lcom/pspdfkit/internal/n4;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n4;->b(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$C0cetTKcoDtZOThR9BxZFEwRNDA(Lcom/pspdfkit/internal/n4;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/n4;->c(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sma(Landroid/graphics/Bitmap;)J
    .locals 2

    invoke-static {p0}, Lcom/pspdfkit/internal/n4;->a(Landroid/graphics/Bitmap;)J

    move-result-wide v0

    return-wide v0
.end method

.method public constructor <init>(J)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 2
    iput-wide v0, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 18
    iput-wide p1, p0, Lcom/pspdfkit/internal/n4;->a:J

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bitmap pool initialized to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v1, 0x400

    div-long/2addr p1, v1

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " KB."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.BitmapPool"

    invoke-static {v0, p1, p2}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    .line 24
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;)J
    .locals 3

    const-wide/16 v0, 0x0

    if-nez p0, :cond_0

    return-wide v0

    .line 1
    :cond_0
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_1

    monitor-exit p0

    return-wide v0

    .line 3
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    .line 4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    .line 5
    new-instance v1, Lcom/pspdfkit/internal/n4$a;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/n4$a;-><init>(Landroid/graphics/Bitmap;)V

    .line 6
    monitor-enter p0

    .line 7
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 8
    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v0, v1, Lcom/pspdfkit/internal/n4$a;->b:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/n4;->c()V

    .line 10
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 14
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 16
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.BitmapPool"

    const-string v1, "Placed bitmap into the pool %dx%d, cache size %d."

    .line 17
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    .line 18
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private c()V
    .locals 11

    .line 16
    monitor-enter p0

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    .line 18
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/n4$a;

    .line 20
    iget-object v2, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 22
    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v4, v1, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    goto :goto_0

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    .line 24
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/n4$a;

    .line 26
    iget-object v2, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 28
    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v4, v1, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    goto :goto_1

    .line 29
    :cond_3
    :goto_2
    iget-wide v0, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->a:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_5

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x4

    if-nez v0, :cond_4

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n4$a;

    .line 32
    iget-wide v6, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v8, v0, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/pspdfkit/internal/n4;->d:J

    const-string v6, "PSPDFKit.BitmapPool"

    const-string v7, "Evicting bitmap %dx%d, cache size %d/%d."

    new-array v8, v5, [Ljava/lang/Object;

    .line 33
    iget-object v9, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 36
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    iget-object v9, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 37
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    iget-wide v9, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 38
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    iget-wide v9, p0, Lcom/pspdfkit/internal/n4;->a:J

    .line 39
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v1

    .line 40
    invoke-static {v6, v7, v8}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    iget-object v6, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 48
    :try_start_1
    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 49
    monitor-exit v6

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 52
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n4$a;

    .line 54
    iget-wide v6, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v8, v0, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/pspdfkit/internal/n4;->d:J

    const-string v6, "PSPDFKit.BitmapPool"

    const-string v7, "Evicting bitmap tile %dx%d, cache size %d/%d."

    new-array v5, v5, [Ljava/lang/Object;

    .line 55
    iget-object v8, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 58
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v4

    iget-object v4, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 59
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v3

    iget-wide v3, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 60
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v2

    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->a:J

    .line 61
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    .line 62
    invoke-static {v6, v7, v5}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iget-object v1, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 70
    :try_start_3
    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 71
    monitor-exit v1

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    .line 74
    :cond_5
    monitor-exit p0

    return-void

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method

.method private c(Landroid/graphics/Bitmap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/n4$a;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/n4$a;-><init>(Landroid/graphics/Bitmap;)V

    .line 3
    monitor-enter p0

    .line 4
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 5
    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v0, v1, Lcom/pspdfkit/internal/n4$a;->b:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/n4;->c()V

    .line 7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 12
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 13
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.BitmapPool"

    const-string v1, "Placed bitmap tile the pool %dx%d, cache size %d."

    .line 14
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    .line 15
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Bitmap;
    .locals 9

    .line 5
    monitor-enter p0

    .line 6
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    .line 7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/n4$a;

    .line 8
    iget-object v5, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v5, p1, :cond_0

    iget-object v5, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-ne v5, p2, :cond_0

    .line 9
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 10
    iget-wide v5, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v7, v1, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 11
    iget-object v5, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_0

    const-string p1, "PSPDFKit.BitmapPool"

    const-string p2, "Got allocated bitmap %dx%d, cache size %d."

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 12
    iget-object v5, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 15
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v3

    iget-object v3, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 16
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    iget-wide v2, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 17
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v4

    .line 18
    invoke-static {p1, p2, v0}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    iget-object p1, v1, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    monitor-exit p0

    return-object p1

    :cond_1
    const-string v0, "PSPDFKit.BitmapPool"

    const-string v1, "Allocating new bitmap %dx%d."

    new-array v4, v4, [Ljava/lang/Object;

    .line 29
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v0, v1, v4}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 31
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a()V
    .locals 2

    .line 32
    monitor-enter p0

    .line 33
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n4$a;

    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 36
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 37
    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 38
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 41
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n4$a;

    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 44
    monitor-enter v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 45
    :try_start_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 46
    monitor-exit v0

    goto :goto_1

    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v1

    :cond_1
    const-wide/16 v0, 0x0

    .line 49
    iput-wide v0, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 50
    monitor-exit p0

    return-void

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 7

    .line 19
    monitor-enter p0

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/n4;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/n4$a;

    .line 22
    iget-object v1, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget v2, Lcom/pspdfkit/internal/n4;->f:I

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget v2, Lcom/pspdfkit/internal/n4;->g:I

    if-ne v1, v2, :cond_0

    .line 23
    iget-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v3, v0, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 24
    iget-object v1, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "PSPDFKit.BitmapPool"

    const-string v2, "Got allocated bitmap tile %dx%d, cache size %d."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 25
    iget-object v5, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    .line 29
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-wide v5, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 30
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 31
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    monitor-exit p0

    return-object v0

    .line 40
    :cond_0
    iget-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    iget-wide v3, v0, Lcom/pspdfkit/internal/n4$a;->b:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/pspdfkit/internal/n4;->d:J

    .line 41
    iget-object v0, v0, Lcom/pspdfkit/internal/n4$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 45
    :cond_1
    sget v0, Lcom/pspdfkit/internal/n4;->f:I

    sget v1, Lcom/pspdfkit/internal/n4;->g:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 46
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b(II)V
    .locals 0

    monitor-enter p0

    .line 1
    :try_start_0
    sput p1, Lcom/pspdfkit/internal/n4;->f:I

    .line 2
    sput p2, Lcom/pspdfkit/internal/n4;->g:I

    const/4 p1, 0x1

    .line 3
    sput-boolean p1, Lcom/pspdfkit/internal/n4;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final d(Landroid/graphics/Bitmap;)V
    .locals 5

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/pspdfkit/internal/n4;->a:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/n4$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/n4$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/n4;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method

.method public final e(Landroid/graphics/Bitmap;)V
    .locals 5

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/pspdfkit/internal/n4;->a:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    sget-boolean v0, Lcom/pspdfkit/internal/n4;->e:Z

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget v1, Lcom/pspdfkit/internal/n4;->g:I

    if-ne v0, v1, :cond_1

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v1, Lcom/pspdfkit/internal/n4;->f:I

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/n4$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/n4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/n4;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 19
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/j9;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/document/PdfDocument;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private d:Landroidx/fragment/app/FragmentActivity;

.field private e:Z

.field private final f:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

.field private final g:Lcom/pspdfkit/document/printing/PrintOptionsProvider;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/j9;)Lcom/pspdfkit/document/PdfDocument;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fpute(Lcom/pspdfkit/internal/j9;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/j9;->e:Z

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;Lcom/pspdfkit/document/printing/PrintOptionsProvider;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/j9;->f:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/j9;->g:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    .line 6
    iput p5, p0, Lcom/pspdfkit/internal/j9;->b:I

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/internal/j9;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    if-nez v0, :cond_0

    return-void

    .line 11
    :cond_0
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/dialog/DocumentPrintDialog;->hide(Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method public final a(Landroidx/fragment/app/FragmentActivity;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    .line 3
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/dialog/DocumentPrintDialog;->isVisible(Landroidx/fragment/app/FragmentManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/pspdfkit/internal/i9;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/i9;-><init>(Lcom/pspdfkit/internal/j9;Landroidx/fragment/app/FragmentActivity;)V

    .line 6
    invoke-static {v0, v1}, Lcom/pspdfkit/ui/dialog/DocumentPrintDialog;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentPrintDialog$PrintDialogListener;)V

    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/pspdfkit/internal/j9;->e:Z

    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/j9;->e:Z

    return v0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    return-void
.end method

.method public final d()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDF_CREATION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->g:Lcom/pspdfkit/document/printing/PrintOptionsProvider;

    if-eqz v0, :cond_1

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget v2, p0, Lcom/pspdfkit/internal/j9;->b:I

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/document/printing/PrintOptionsProvider;->createPrintOptions(Lcom/pspdfkit/document/PdfDocument;I)Lcom/pspdfkit/document/printing/PrintOptions;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 13
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    iget-object v3, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {v1, v2, v3, v0}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->print(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/printing/PrintOptions;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/j9;->f:Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;

    if-eqz v1, :cond_2

    .line 21
    invoke-interface {v1}, Lcom/pspdfkit/ui/dialog/DocumentPrintDialogFactory;->createDocumentPrintDialog()Lcom/pspdfkit/ui/dialog/BaseDocumentPrintDialog;

    move-result-object v0

    :cond_2
    move-object v1, v0

    const/4 v0, 0x1

    .line 24
    iput-boolean v0, p0, Lcom/pspdfkit/internal/j9;->e:Z

    .line 25
    iget-object v2, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    .line 28
    invoke-virtual {v2}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    iget v4, p0, Lcom/pspdfkit/internal/j9;->b:I

    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 30
    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getPageCount()I

    move-result v5

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 33
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    iget-object v6, p0, Lcom/pspdfkit/internal/j9;->a:Lcom/pspdfkit/document/PdfDocument;

    invoke-static {v0, v6}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v6, v0

    iget-object v0, p0, Lcom/pspdfkit/internal/j9;->d:Landroidx/fragment/app/FragmentActivity;

    .line 34
    new-instance v7, Lcom/pspdfkit/internal/i9;

    invoke-direct {v7, p0, v0}, Lcom/pspdfkit/internal/i9;-><init>(Lcom/pspdfkit/internal/j9;Landroidx/fragment/app/FragmentActivity;)V

    .line 35
    invoke-static/range {v1 .. v7}, Lcom/pspdfkit/ui/dialog/DocumentPrintDialog;->show(Lcom/pspdfkit/ui/dialog/BaseDocumentPrintDialog;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;IILjava/lang/String;Lcom/pspdfkit/ui/dialog/DocumentPrintDialog$PrintDialogListener;)V

    return-void

    .line 36
    :cond_4
    new-instance v0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string v1, "Your current license does not allow creation of new PDF documents. This is mandatory for printing to work!"

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

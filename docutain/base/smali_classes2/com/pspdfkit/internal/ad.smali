.class public final Lcom/pspdfkit/internal/ad;
.super Lcom/pspdfkit/internal/z5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ad$a;,
        Lcom/pspdfkit/internal/ad$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/z5<",
        "Lcom/pspdfkit/internal/ad$b;",
        "Ljava/util/List<",
        "+",
        "Lcom/pspdfkit/internal/rt;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/utils/Size;

.field private final d:Ljava/lang/String;

.field private final e:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/ad$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/ad$b;


# direct methods
.method public constructor <init>(ILcom/pspdfkit/utils/Size;)V
    .locals 1

    const-string v0, "pageSize"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/z5;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/ad;->c:Lcom/pspdfkit/utils/Size;

    .line 6
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "(page "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/ad;->d:Ljava/lang/String;

    .line 11
    sget-object p2, Lcom/pspdfkit/internal/ad$b;->Companion:Lcom/pspdfkit/internal/ad$b$b;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/ad$b$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/ad;->e:Lkotlinx/serialization/KSerializer;

    .line 12
    new-instance p2, Lcom/pspdfkit/internal/ad$b;

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/ad$b;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/ad;->f:Lcom/pspdfkit/internal/ad$b;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ad;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ad;->f:Lcom/pspdfkit/internal/ad$b;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ad;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->GET_TEXT_BLOCKS:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

.method public final e()Lkotlin/jvm/functions/Function2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/rt;",
            ">;",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ad;->c:Lcom/pspdfkit/utils/Size;

    const-string v1, "pageSize"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v1, Lcom/pspdfkit/internal/zc;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/zc;-><init>(Lcom/pspdfkit/utils/Size;)V

    return-object v1
.end method

.method public final g()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/rt;->Companion:Lcom/pspdfkit/internal/rt$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/rt$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    invoke-static {v0}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->ListSerializer(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    move-result-object v0

    return-object v0
.end method

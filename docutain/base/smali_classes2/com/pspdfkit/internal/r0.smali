.class public final Lcom/pspdfkit/internal/r0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/core/CompletableObserver;


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/q0;

.field final synthetic b:Lcom/pspdfkit/annotations/Annotation;

.field final synthetic c:I

.field final synthetic d:Lcom/pspdfkit/annotations/AnnotationProvider;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;ILcom/pspdfkit/annotations/AnnotationProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/r0;->a:Lcom/pspdfkit/internal/q0;

    iput-object p2, p0, Lcom/pspdfkit/internal/r0;->b:Lcom/pspdfkit/annotations/Annotation;

    iput p3, p0, Lcom/pspdfkit/internal/r0;->c:I

    iput-object p4, p0, Lcom/pspdfkit/internal/r0;->d:Lcom/pspdfkit/annotations/AnnotationProvider;

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r0;->a:Lcom/pspdfkit/internal/q0;

    iget-object v1, p0, Lcom/pspdfkit/internal/r0;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;Lcom/pspdfkit/annotations/Annotation;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r0;->a:Lcom/pspdfkit/internal/q0;

    invoke-static {v0}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/r0;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 4
    iget v2, p0, Lcom/pspdfkit/internal/r0;->c:I

    .line 5
    iget-object v3, p0, Lcom/pspdfkit/internal/r0;->d:Lcom/pspdfkit/annotations/AnnotationProvider;

    invoke-interface {v3, v1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getZIndex(Lcom/pspdfkit/annotations/Annotation;)I

    move-result v3

    .line 6
    invoke-interface {v0, v1, v2, v3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->recordAnnotationZIndexEdit(Lcom/pspdfkit/annotations/Annotation;II)V

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 2

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "PSPDFKit.Annotations"

    const-string v1, "Annotation z-index reordering action could not be performed"

    .line 1
    invoke-static {p1, v1, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onSubscribe(Lio/reactivex/rxjava3/disposables/Disposable;)V
    .locals 1

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/r0;->a:Lcom/pspdfkit/internal/q0;

    invoke-static {p1}, Lcom/pspdfkit/internal/q0;->a(Lcom/pspdfkit/internal/q0;)Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/r0;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationEditingController;->showEditedAnnotationPositionOnThePage(I)V

    return-void
.end method

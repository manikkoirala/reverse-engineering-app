.class final Lcom/pspdfkit/internal/vd;
.super Lokhttp3/RequestBody;
.source "SourceFile"


# instance fields
.field final synthetic a:Lokhttp3/MediaType;

.field final synthetic b:[B

.field final synthetic c:Lcom/pspdfkit/internal/yd;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/yd;Lokhttp3/MediaType;[B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/vd;->c:Lcom/pspdfkit/internal/yd;

    iput-object p2, p0, Lcom/pspdfkit/internal/vd;->a:Lokhttp3/MediaType;

    iput-object p3, p0, Lcom/pspdfkit/internal/vd;->b:[B

    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    return-void
.end method


# virtual methods
.method public final contentLength()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vd;->b:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final contentType()Lokhttp3/MediaType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vd;->a:Lokhttp3/MediaType;

    return-object v0
.end method

.method public final writeTo(Lokio/BufferedSink;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/vd;->c:Lcom/pspdfkit/internal/yd;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/pspdfkit/internal/vd;->b:[B

    array-length v3, v2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-static {v1}, Lokio/Okio;->source(Ljava/io/InputStream;)Lokio/Source;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/yd;->-$$Nest$ma(Lcom/pspdfkit/internal/yd;Lokio/Source;Lokio/BufferedSink;)V

    return-void
.end method

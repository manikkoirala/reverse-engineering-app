.class public final Lcom/pspdfkit/internal/gp;
.super Lcom/pspdfkit/internal/z5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/gp$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/z5<",
        "Lcom/pspdfkit/internal/gp$a;",
        "Lcom/pspdfkit/internal/ip;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/internal/pt;

.field private final d:Landroid/graphics/Matrix;

.field private final e:Lcom/pspdfkit/utils/Size;

.field private final f:Z

.field private final g:Lcom/pspdfkit/internal/uq;

.field private final h:Lcom/pspdfkit/internal/m7;

.field private final i:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final j:Ljava/lang/String;

.field private final k:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/gp$a;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/pspdfkit/internal/gp$a;

.field private final m:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/ip;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/pspdfkit/internal/pt;Landroid/graphics/Matrix;Lcom/pspdfkit/utils/Size;ZLcom/pspdfkit/internal/uq;Lcom/pspdfkit/internal/m7;)V
    .locals 1

    const-string v0, "textBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/z5;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/gp;->c:Lcom/pspdfkit/internal/pt;

    .line 3
    iput-object p3, p0, Lcom/pspdfkit/internal/gp;->d:Landroid/graphics/Matrix;

    .line 4
    iput-object p4, p0, Lcom/pspdfkit/internal/gp;->e:Lcom/pspdfkit/utils/Size;

    .line 5
    iput-boolean p5, p0, Lcom/pspdfkit/internal/gp;->f:Z

    .line 6
    iput-object p6, p0, Lcom/pspdfkit/internal/gp;->g:Lcom/pspdfkit/internal/uq;

    .line 7
    iput-object p7, p0, Lcom/pspdfkit/internal/gp;->h:Lcom/pspdfkit/internal/m7;

    .line 11
    sget-object p3, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->RENDER_TEXT_BLOCK:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object p3, p0, Lcom/pspdfkit/internal/gp;->i:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 12
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "(page "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/gp;->j:Ljava/lang/String;

    .line 21
    sget-object p1, Lcom/pspdfkit/internal/gp$a;->Companion:Lcom/pspdfkit/internal/gp$a$b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/gp$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/gp;->k:Lkotlinx/serialization/KSerializer;

    .line 22
    new-instance p1, Lcom/pspdfkit/internal/gp$a;

    .line 23
    invoke-virtual {p2}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object p3

    .line 24
    invoke-virtual {p2}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object p2

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/internal/gp;->j()Lcom/pspdfkit/internal/hp;

    move-result-object p4

    .line 26
    invoke-direct {p1, p3, p2, p4}, Lcom/pspdfkit/internal/gp$a;-><init>(Ljava/util/UUID;Lcom/pspdfkit/internal/cb;Lcom/pspdfkit/internal/hp;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/gp;->l:Lcom/pspdfkit/internal/gp$a;

    .line 47
    sget-object p1, Lcom/pspdfkit/internal/ip;->Companion:Lcom/pspdfkit/internal/ip$b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ip$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/gp;->m:Lkotlinx/serialization/KSerializer;

    return-void
.end method

.method private final j()Lcom/pspdfkit/internal/hp;
    .locals 11

    .line 1
    new-instance v0, Lcom/pspdfkit/utils/PageRect;

    iget-object v1, p0, Lcom/pspdfkit/internal/gp;->e:Lcom/pspdfkit/utils/Size;

    iget v2, v1, Lcom/pspdfkit/utils/Size;->width:F

    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v2, v1}, Lcom/pspdfkit/utils/PageRect;-><init>(FFFF)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/gp;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 3
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/gp;->c:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/st;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/iv;->a()F

    move-result v2

    iget-object v4, p0, Lcom/pspdfkit/internal/gp;->e:Lcom/pspdfkit/utils/Size;

    iget v4, v4, Lcom/pspdfkit/utils/Size;->height:F

    iget-object v5, p0, Lcom/pspdfkit/internal/gp;->c:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v5}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/st;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/iv;->b()F

    move-result v5

    sub-float/2addr v4, v5

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/gp;->d:Landroid/graphics/Matrix;

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 5
    new-instance v2, Lcom/pspdfkit/internal/hp;

    .line 6
    new-instance v5, Lcom/pspdfkit/internal/iv;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-direct {v5, v4, v6}, Lcom/pspdfkit/internal/iv;-><init>(FF)V

    .line 7
    new-instance v6, Lcom/pspdfkit/internal/c7;

    new-instance v4, Lcom/pspdfkit/internal/iv;

    invoke-direct {v4, v3, v3}, Lcom/pspdfkit/internal/iv;-><init>(FF)V

    new-instance v3, Lcom/pspdfkit/internal/iv;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-direct {v3, v7, v0}, Lcom/pspdfkit/internal/iv;-><init>(FF)V

    invoke-direct {v6, v4, v3}, Lcom/pspdfkit/internal/c7;-><init>(Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/iv;)V

    .line 8
    new-instance v7, Lcom/pspdfkit/internal/iv;

    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v7, v0, v1}, Lcom/pspdfkit/internal/iv;-><init>(FF)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->c:Lcom/pspdfkit/internal/pt;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/st;->c()Lcom/pspdfkit/internal/bd;

    move-result-object v8

    .line 10
    iget-object v9, p0, Lcom/pspdfkit/internal/gp;->h:Lcom/pspdfkit/internal/m7;

    .line 11
    iget-object v10, p0, Lcom/pspdfkit/internal/gp;->g:Lcom/pspdfkit/internal/uq;

    move-object v4, v2

    .line 12
    invoke-direct/range {v4 .. v10}, Lcom/pspdfkit/internal/hp;-><init>(Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/c7;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/m7;Lcom/pspdfkit/internal/uq;)V

    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->l:Lcom/pspdfkit/internal/gp$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->k:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->i:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

.method public final e()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/pspdfkit/internal/ip;",
            "Lcom/pspdfkit/internal/jni/NativeContentEditingResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/gp$b;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/gp$b;-><init>(Lcom/pspdfkit/internal/gp;)V

    return-object v0
.end method

.method public final g()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->m:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/gp;->f:Z

    return v0
.end method

.method public final i()Lcom/pspdfkit/internal/pt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gp;->c:Lcom/pspdfkit/internal/pt;

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/vo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Function;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/uo;

.field final synthetic b:Lcom/pspdfkit/internal/zf;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/uo;Lcom/pspdfkit/internal/zf;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/vo;->a:Lcom/pspdfkit/internal/uo;

    iput-object p2, p0, Lcom/pspdfkit/internal/vo;->b:Lcom/pspdfkit/internal/zf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/vo;->a:Lcom/pspdfkit/internal/uo;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/vo;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getDocumentSources()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/pspdfkit/document/PdfDocumentLoader;->openDocumentsAsync(Landroid/content/Context;Ljava/util/List;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

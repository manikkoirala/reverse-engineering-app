.class final Lcom/pspdfkit/internal/xe$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/s2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/xe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/internal/xe$c;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/pspdfkit/internal/xe;


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/xe$b;)Lcom/pspdfkit/internal/nh;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/xe$b;->a:Lcom/pspdfkit/internal/nh;

    return-object p0
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/xe;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/xe$b;->b:Lcom/pspdfkit/internal/xe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/nh;

    invoke-direct {p1}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/xe$b;->a:Lcom/pspdfkit/internal/nh;

    return-void
.end method

.method synthetic constructor <init>(Lcom/pspdfkit/internal/xe;Lcom/pspdfkit/internal/xe$b-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/xe$b;-><init>(Lcom/pspdfkit/internal/xe;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/instant/internal/jni/NativeAsset;)V
    .locals 0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeAsset;->getIdentifier()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/xe$b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xe$b;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xe$c;

    .line 2
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xe$c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;Lcom/pspdfkit/instant/exceptions/InstantException;)V
    .locals 2

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/xe$b;->b:Lcom/pspdfkit/internal/xe;

    monitor-enter v0

    .line 7
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/xe$b;->b:Lcom/pspdfkit/internal/xe;

    invoke-static {v1}, Lcom/pspdfkit/internal/xe;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xe;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 8
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/xe$b;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xe$c;

    .line 10
    invoke-interface {v1, p1, p2}, Lcom/pspdfkit/internal/xe$c;->a(Ljava/lang/String;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    goto :goto_0

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 11
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 0

    .line 4
    invoke-static {p2}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)Lcom/pspdfkit/instant/exceptions/InstantException;

    move-result-object p2

    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/xe$b;->a(Ljava/lang/String;Lcom/pspdfkit/instant/exceptions/InstantException;)V

    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/xe$b;->b:Lcom/pspdfkit/internal/xe;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/pspdfkit/internal/xe$b;->b:Lcom/pspdfkit/internal/xe;

    invoke-static {v1}, Lcom/pspdfkit/internal/xe;->-$$Nest$fgetd(Lcom/pspdfkit/internal/xe;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/xe$b;->a:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/xe$c;

    .line 5
    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/xe$c;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 6
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

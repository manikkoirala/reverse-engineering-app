.class public final Lcom/pspdfkit/internal/r4;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static synthetic $r8$lambda$5B4HLofEKEgHiopqgms-2Hvb35M(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/r4;->d(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "imageUri"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/m4$a;->a(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/m4;

    move-result-object v0

    .line 177
    new-instance v1, Lcom/pspdfkit/internal/o4;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/o4;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 181
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "imageUri.toString()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 182
    invoke-static {v0, v1, p0, p1, p1}, Lcom/pspdfkit/internal/r4;->a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/graphics/Bitmap;
    .locals 11

    .line 253
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->d()I

    move-result v0

    .line 254
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->b()I

    move-result v1

    if-eqz p3, :cond_0

    .line 255
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_0

    .line 256
    :cond_0
    invoke-static {v0}, Lcom/pspdfkit/internal/ga;->b(I)I

    move-result p3

    :goto_0
    if-eqz p4, :cond_1

    .line 257
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    goto :goto_1

    .line 258
    :cond_1
    invoke-static {v1}, Lcom/pspdfkit/internal/ga;->a(I)I

    move-result p4

    .line 262
    :goto_1
    :try_start_0
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/Closeable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-object v2, p1

    check-cast v2, Ljava/io/InputStream;

    .line 263
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    int-to-float v0, v0

    int-to-float v4, p3

    div-float/2addr v0, v4

    float-to-double v4, v0

    .line 266
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    int-to-float v0, v1

    int-to-float v1, p4

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 267
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 270
    invoke-static {v0}, Lcom/pspdfkit/internal/di;->a(I)I

    move-result v0

    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v0, 0x0

    .line 273
    invoke-static {v2, v0, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 274
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    :try_start_2
    invoke-static {p1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v4, :cond_5

    .line 295
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 296
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 297
    invoke-static {v7, p3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 298
    invoke-static {v8, p4}, Ljava/lang/Math;->min(II)I

    move-result p2

    const-string p3, "createBitmap(resultBitma\u2026idth, srcHeight, m, true)"

    if-ne v7, p1, :cond_3

    if-eq v8, p2, :cond_2

    goto :goto_2

    .line 308
    :cond_2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->a()I

    move-result p1

    if-eqz p1, :cond_4

    .line 309
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 310
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->a()I

    move-result p0

    invoke-static {p0, v9}, Lcom/pspdfkit/internal/r4;->a(ILandroid/graphics/Matrix;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 311
    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 312
    :cond_3
    :goto_2
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 314
    new-instance p4, Landroid/graphics/RectF;

    int-to-float v0, v7

    int-to-float v1, v8

    const/4 v2, 0x0

    invoke-direct {p4, v2, v2, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 315
    new-instance v0, Landroid/graphics/RectF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v0, v2, v2, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 316
    sget-object p1, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    .line 317
    invoke-virtual {v9, p4, v0, p1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 322
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->a()I

    move-result p0

    invoke-static {p0, v9}, Lcom/pspdfkit/internal/r4;->a(ILandroid/graphics/Matrix;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 323
    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_4
    :goto_3
    return-object v4

    .line 324
    :cond_5
    new-instance p0, Ljava/io/IOException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Could not decode bitmap: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :catchall_0
    move-exception p0

    .line 325
    :try_start_3
    throw p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p3

    :try_start_4
    invoke-static {p1, p0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception p0

    .line 339
    new-instance p1, Ljava/io/IOException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Could not open image input stream: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static a(Lcom/pspdfkit/document/providers/DataProvider;)Lcom/pspdfkit/internal/l4;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "dataProvider"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-static {p0}, Lcom/pspdfkit/internal/m4$a;->a(Lcom/pspdfkit/document/providers/DataProvider;)Lcom/pspdfkit/internal/m4;

    move-result-object v0

    .line 184
    new-instance v1, Lcom/pspdfkit/internal/q4;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/q4;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 188
    invoke-interface {p0}, Lcom/pspdfkit/document/providers/DataProvider;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    const-string p0, "dataProvider.title ?: dataProvider.toString()"

    invoke-static {v2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 189
    invoke-static {v0, v1, p0, v2}, Lcom/pspdfkit/internal/r4;->a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;ILjava/lang/String;)Lcom/pspdfkit/internal/l4;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;ILjava/lang/String;)Lcom/pspdfkit/internal/l4;
    .locals 12

    .line 190
    rem-int/lit16 p2, p2, 0x168

    .line 193
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->d()I

    move-result v0

    .line 194
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->b()I

    move-result v1

    const/16 v2, 0x800

    .line 195
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 196
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 197
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image/jpeg"

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0

    .line 198
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image/png"

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 202
    :goto_0
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    if-eq v4, v5, :cond_2

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    if-ne v4, v5, :cond_4

    :cond_2
    if-ne v0, v3, :cond_4

    if-ne v1, v2, :cond_4

    .line 205
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m4;->a()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    if-nez p2, :cond_4

    .line 209
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/InputStream;

    .line 210
    new-instance p1, Ljava/io/ByteArrayOutputStream;

    const p2, 0x19000

    invoke-direct {p1, p2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const p2, 0xffff

    new-array p2, p2, [B

    .line 214
    :goto_1
    invoke-virtual {p0, p2}, Ljava/io/InputStream;->read([B)I

    move-result p3

    if-ltz p3, :cond_3

    const/4 v2, 0x0

    .line 215
    invoke-virtual {p1, p2, v2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 218
    :cond_3
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    const-string p2, "readToArray(inputStream)"

    .line 219
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    goto :goto_4

    .line 222
    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, p1, p3, v0, v1}, Lcom/pspdfkit/internal/r4;->a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 223
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 224
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eqz p2, :cond_5

    .line 229
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    int-to-float p0, p2

    .line 230
    invoke-virtual {v10, p0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 231
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x1

    invoke-static/range {v5 .. v11}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    const-string p0, "createBitmap(bitmap, 0, \u2026, bitmap.height, m, true)"

    invoke-static {v5, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    :cond_5
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 236
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    if-eq v4, p1, :cond_7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_2

    .line 240
    :cond_6
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 p2, 0x63

    .line 241
    invoke-virtual {v5, p1, p2, p0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    goto :goto_3

    .line 242
    :cond_7
    :goto_2
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 p2, 0x64

    .line 243
    invoke-virtual {v5, p1, p2, p0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    :goto_3
    move-object v4, p1

    .line 248
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 249
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    const-string p0, "compressedFile.toByteArray()"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    :goto_4
    new-instance p0, Lcom/pspdfkit/internal/l4;

    invoke-direct {p0, p1, v0, v1, v4}, Lcom/pspdfkit/internal/l4;-><init>([BIILandroid/graphics/Bitmap$CompressFormat;)V

    return-object p0
.end method

.method private static a(ILandroid/graphics/Matrix;)V
    .locals 4

    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v1, 0x42b40000    # 90.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 340
    :pswitch_0
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    .line 348
    :pswitch_1
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 349
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    .line 350
    :pswitch_2
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    .line 356
    :pswitch_3
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 357
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    .line 358
    :pswitch_4
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    :pswitch_5
    const/high16 p0, 0x43340000    # 180.0f

    .line 359
    invoke-virtual {p1, p0}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    .line 361
    :pswitch_6
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "context"

    .line 1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/m4$a;->a(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/m4;

    move-result-object v0

    .line 76
    new-instance v1, Lcom/pspdfkit/internal/p4;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/p4;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 80
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "imageUri.toString()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 81
    invoke-static {v0, v1, p1, p0}, Lcom/pspdfkit/internal/r4;->a(Lcom/pspdfkit/internal/m4;Lkotlin/jvm/functions/Function0;ILjava/lang/String;)Lcom/pspdfkit/internal/l4;

    move-result-object p0

    return-object p0
.end method

.method public static c(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/r4$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/r4$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string p1, "fromCallable { readBitma\u2026Scheduler.PRIORITY_HIGH))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final d(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;
    .locals 1

    const-string v0, "$context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$imageUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/r4;->b(Landroid/content/Context;Landroid/net/Uri;)Lcom/pspdfkit/internal/l4;

    move-result-object p0

    return-object p0
.end method

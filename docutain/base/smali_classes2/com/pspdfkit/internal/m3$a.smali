.class final Lcom/pspdfkit/internal/m3$a;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/m3;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/m3;

.field final synthetic b:Lcom/pspdfkit/annotations/SoundAnnotation;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;Z)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    iput-object p2, p0, Lcom/pspdfkit/internal/m3$a;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    iput-boolean p3, p0, Lcom/pspdfkit/internal/m3$a;->c:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {p1}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/annotations/SoundAnnotation;

    move-result-object p1

    if-nez p1, :cond_1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$a;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {p1}, Lcom/pspdfkit/internal/m3;->b(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/internal/a3;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/a3;->b(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    goto :goto_0

    .line 6
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$a;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;Lcom/pspdfkit/annotations/SoundAnnotation;)V

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {p1}, Lcom/pspdfkit/internal/m3;->b(Lcom/pspdfkit/internal/m3;)Lcom/pspdfkit/internal/a3;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/a3;->a(Lcom/pspdfkit/ui/audio/AudioRecordingController;)V

    .line 13
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    iget-boolean v0, p0, Lcom/pspdfkit/internal/m3$a;->c:Z

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/m3;->a(Lcom/pspdfkit/internal/m3;Z)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-static {p1}, Lcom/pspdfkit/internal/m3;->d(Lcom/pspdfkit/internal/m3;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/m3$a;->b:Lcom/pspdfkit/annotations/SoundAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/m3$a;->a:Lcom/pspdfkit/internal/m3;

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 18
    :goto_1
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.class final Lcom/pspdfkit/internal/k6$a;
.super Lcom/pspdfkit/internal/o7$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/k6;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/graphics/Point;

.field final synthetic b:Lcom/pspdfkit/internal/k6;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/k6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-direct {p0}, Lcom/pspdfkit/internal/o7$c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    .line 2
    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->c(Lcom/pspdfkit/internal/k6;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/k6;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    const/4 p1, 0x0

    .line 5
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Z)V

    :cond_0
    return-void
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/k6;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/lang/Boolean;)V

    .line 5
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/k6$a;->a:Landroid/graphics/Point;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Z)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/k6;->b(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/qt;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->a()Ljava/util/UUID;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    if-eqz v0, :cond_3

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->b(Lcom/pspdfkit/internal/k6;)Ljava/util/UUID;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 21
    iget-object v1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/lang/Boolean;)V

    .line 24
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    .line 25
    invoke-virtual {v1}, Lcom/pspdfkit/internal/views/contentediting/a;->getTextblockId()Ljava/util/UUID;

    move-result-object v3

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1, p1}, Lcom/pspdfkit/internal/k6;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    invoke-static {v2, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Z)V

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->a:Landroid/graphics/Point;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return v2

    .line 5
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/k6;->j()Landroid/content/Context;

    move-result-object v3

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v3, v4, v0, v5, v6}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/k6;->b(Landroid/view/MotionEvent;)Lcom/pspdfkit/internal/qt;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/qt;->a()Ljava/util/UUID;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {p1}, Lcom/pspdfkit/internal/k6;->j(Lcom/pspdfkit/internal/k6;)V

    return v2

    .line 19
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;)V

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->b(Lcom/pspdfkit/internal/k6;)Ljava/util/UUID;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Ljava/lang/Boolean;)V

    return v2

    .line 29
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v0

    .line 31
    iget-object v3, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v3, v1}, Lcom/pspdfkit/internal/k6;->b(Lcom/pspdfkit/internal/k6;Ljava/util/UUID;)V

    .line 36
    iget-object v1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {v1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;)Lcom/pspdfkit/internal/views/contentediting/a;

    move-result-object v3

    invoke-static {v1, v3, p1}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/views/contentediting/a;Landroid/view/MotionEvent;)V

    if-eqz v0, :cond_5

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/internal/k6$a;->b:Lcom/pspdfkit/internal/k6;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/k6;->a(Lcom/pspdfkit/internal/k6;Lcom/pspdfkit/internal/views/contentediting/a;)V

    :cond_5
    return v2
.end method

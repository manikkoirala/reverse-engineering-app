.class public final Lcom/pspdfkit/internal/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/d$a;
    }
.end annotation


# direct methods
.method public static final a(Lcom/pspdfkit/internal/b;)Lcom/pspdfkit/annotations/actions/Action;
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 1
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b;->a()I

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b;->a()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b;->a()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    .line 4
    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/b;->f(I)Lcom/pspdfkit/internal/b;

    move-result-object v5

    invoke-static {v5}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 6
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 11
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b;->b()S

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/cd;

    invoke-direct {v0}, Lcom/pspdfkit/internal/cd;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/cd;

    .line 14
    new-instance v0, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cd;->a()J

    move-result-wide v2

    long-to-int p0, v2

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/annotations/actions/GoToAction;-><init>(ILjava/util/List;)V

    return-object v0

    :cond_4
    const/4 v5, 0x2

    if-ne v3, v5, :cond_5

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/id;

    invoke-direct {v0}, Lcom/pspdfkit/internal/id;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/id;

    .line 18
    new-instance v0, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/id;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/id;->a()J

    move-result-wide v3

    long-to-int p0, v3

    invoke-direct {v0, v2, p0, v1}, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0

    :cond_5
    const/4 v5, 0x3

    if-ne v3, v5, :cond_7

    .line 21
    new-instance v0, Lcom/pspdfkit/internal/ed;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ed;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ed;

    .line 22
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ed;->c()Ljava/lang/String;

    move-result-object v3

    .line 24
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ed;->b()J

    move-result-wide v5

    long-to-int v6, v5

    .line 26
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ed;->a()B

    move-result p0

    if-nez p0, :cond_6

    const/4 v2, 0x1

    .line 27
    :cond_6
    invoke-virtual {v0, v3, v6, v2, v1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->createGoToEmbeddedAction(Ljava/lang/String;IZLjava/util/List;)Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    move-result-object p0

    return-object p0

    :cond_7
    const/4 v4, 0x6

    if-ne v3, v4, :cond_8

    .line 36
    new-instance v0, Lcom/pspdfkit/internal/pu;

    invoke-direct {v0}, Lcom/pspdfkit/internal/pu;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/pu;

    .line 37
    new-instance v0, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/pu;->a()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/annotations/actions/UriAction;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    :cond_8
    const/4 v4, 0x4

    if-ne v3, v4, :cond_9

    .line 40
    new-instance v0, Lcom/pspdfkit/internal/sg;

    invoke-direct {v0}, Lcom/pspdfkit/internal/sg;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/sg;

    .line 41
    new-instance v0, Lcom/pspdfkit/annotations/actions/LaunchAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/sg;->a()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/annotations/actions/LaunchAction;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    :cond_9
    const/16 v4, 0xa

    if-ne v3, v4, :cond_a

    .line 44
    new-instance v0, Lcom/pspdfkit/internal/jj;

    invoke-direct {v0}, Lcom/pspdfkit/internal/jj;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/jj;

    .line 45
    new-instance v0, Lcom/pspdfkit/annotations/actions/NamedAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/jj;->a()Ljava/lang/String;

    move-result-object p0

    const-string v2, "namedAction.namedAction()"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/pspdfkit/internal/lj;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/NamedAction$NamedActionType;

    move-result-object p0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/annotations/actions/NamedAction;-><init>(Lcom/pspdfkit/annotations/actions/NamedAction$NamedActionType;Ljava/util/List;)V

    return-object v0

    :cond_a
    const/16 v4, 0x9

    if-ne v3, v4, :cond_b

    .line 48
    new-instance v0, Lcom/pspdfkit/internal/ld;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ld;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/ld;

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/pd;->a(Lcom/pspdfkit/internal/ld;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/HideAction;

    move-result-object p0

    return-object p0

    :cond_b
    const/16 v4, 0xc

    if-ne v3, v4, :cond_c

    .line 51
    new-instance v0, Lcom/pspdfkit/internal/qp;

    invoke-direct {v0}, Lcom/pspdfkit/internal/qp;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/qp;

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/rb;->a(Lcom/pspdfkit/internal/qp;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/ResetFormAction;

    move-result-object p0

    return-object p0

    :cond_c
    const/16 v4, 0xb

    if-ne v3, v4, :cond_d

    .line 54
    new-instance v0, Lcom/pspdfkit/internal/kt;

    invoke-direct {v0}, Lcom/pspdfkit/internal/kt;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/kt;

    invoke-static {p0, v1}, Lcom/pspdfkit/internal/rb;->a(Lcom/pspdfkit/internal/kt;Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/actions/SubmitFormAction;

    move-result-object p0

    return-object p0

    :cond_d
    const/16 v4, 0xe

    if-ne v3, v4, :cond_e

    .line 57
    new-instance v0, Lcom/pspdfkit/internal/fg;

    invoke-direct {v0}, Lcom/pspdfkit/internal/fg;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/fg;

    .line 58
    new-instance v0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/fg;->a()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, v1}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    :cond_e
    const/16 v4, 0x10

    if-ne v3, v4, :cond_f

    .line 61
    new-instance v0, Lcom/pspdfkit/internal/mp;

    invoke-direct {v0}, Lcom/pspdfkit/internal/mp;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/mp;

    .line 62
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    .line 63
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mp;->c()I

    move-result v2

    invoke-static {v2}, Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;->fromValue(I)Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;

    move-result-object v2

    const-string v3, "fromValue(renditionAction.operationType())"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mp;->a()Lcom/pspdfkit/internal/t1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/t1;->c()I

    move-result v3

    .line 65
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mp;->b()Ljava/lang/String;

    move-result-object p0

    .line 66
    invoke-virtual {v0, v2, v3, p0, v1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->createRenditionAction(Lcom/pspdfkit/annotations/actions/RenditionAction$RenditionActionType;ILjava/lang/String;Ljava/util/List;)Lcom/pspdfkit/annotations/actions/RenditionAction;

    move-result-object p0

    return-object p0

    :cond_f
    const/16 v4, 0x13

    if-ne v3, v4, :cond_10

    .line 74
    new-instance v0, Lcom/pspdfkit/internal/yp;

    invoke-direct {v0}, Lcom/pspdfkit/internal/yp;-><init>()V

    invoke-static {p0, v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/yp;

    .line 75
    sget-object v0, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    .line 76
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/pspdfkit/internal/ij;->a(Ljava/lang/String;)Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;

    move-result-object v2

    .line 77
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yp;->a()Lcom/pspdfkit/internal/t1;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/t1;->c()I

    move-result p0

    .line 78
    invoke-virtual {v0, v2, p0, v1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->createRichMediaExecuteAction(Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction$RichMediaExecuteActionType;ILjava/util/List;)Lcom/pspdfkit/annotations/actions/RichMediaExecuteAction;

    move-result-object p0

    return-object p0

    :cond_10
    const/16 v4, 0xd

    if-ne v3, v4, :cond_11

    .line 85
    sget-object p0, Lcom/pspdfkit/annotations/actions/ActionAccessors;->Companion:Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/actions/ActionAccessors$Companion;->createImportDataAction(Ljava/util/List;)Lcom/pspdfkit/annotations/actions/ImportDataAction;

    move-result-object p0

    return-object p0

    .line 88
    :cond_11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b;->b()S

    move-result p0

    invoke-static {p0}, Lcom/pspdfkit/internal/j;->a(I)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported action type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Annotations"

    invoke-static {v2, p0, v1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private static final a(S)Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;
    .locals 3

    if-nez p0, :cond_0

    .line 246
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->CURSOR_ENTERS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 247
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->CURSOR_EXITS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 248
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_DOWN:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 249
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->MOUSE_UP:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_3
    const/4 v0, 0x4

    if-ne p0, v0, :cond_4

    .line 250
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->RECEIVE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_4
    const/4 v0, 0x5

    if-ne p0, v0, :cond_5

    .line 251
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->LOOSE_FOCUS:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_5
    const/4 v0, 0x6

    if-ne p0, v0, :cond_6

    .line 252
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_OPENED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_6
    const/4 v0, 0x7

    if-ne p0, v0, :cond_7

    .line 253
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_CLOSED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_7
    const/16 v0, 0x8

    if-ne p0, v0, :cond_8

    .line 254
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->PAGE_VISIBLE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_8
    const/16 v0, 0x9

    if-ne p0, v0, :cond_9

    .line 255
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_CHANGED:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_9
    const/16 v0, 0xa

    if-ne p0, v0, :cond_a

    .line 256
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FIELD_FORMAT:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_a
    const/16 v0, 0xb

    if-ne p0, v0, :cond_b

    .line 257
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_VALIDATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    :cond_b
    const/16 v0, 0xc

    if-ne p0, v0, :cond_c

    .line 258
    sget-object p0, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;->FORM_CALCULATE:Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    return-object p0

    .line 259
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown trigger event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final a(Lcom/pspdfkit/internal/m1;)Lcom/pspdfkit/internal/o;
    .locals 7

    const-string v0, "properties"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m1;->b()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 208
    :cond_0
    new-instance v2, Lcom/pspdfkit/internal/o;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/o;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_4

    .line 210
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/m1;->f(I)Lcom/pspdfkit/internal/w;

    move-result-object v4

    if-nez v4, :cond_1

    goto :goto_1

    .line 211
    :cond_1
    invoke-virtual {v4}, Lcom/pspdfkit/internal/w;->b()S

    move-result v5

    invoke-static {v5}, Lcom/pspdfkit/internal/d;->a(S)Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    move-result-object v5

    .line 212
    invoke-virtual {v4}, Lcom/pspdfkit/internal/w;->a()Lcom/pspdfkit/internal/b;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v4

    if-nez v4, :cond_2

    :goto_1
    move-object v6, v1

    goto :goto_2

    .line 214
    :cond_2
    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    if-eqz v6, :cond_3

    .line 215
    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v5, "action.first"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    iget-object v5, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/pspdfkit/annotations/actions/Action;

    invoke-virtual {v2, v4, v5}, Lcom/pspdfkit/internal/o;->a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;Lcom/pspdfkit/annotations/actions/Action;)V

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return-object v2
.end method

.method public static final a(Lcom/pspdfkit/internal/b;Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/pspdfkit/internal/ot;",
            ">(",
            "Lcom/pspdfkit/internal/b;",
            "TT;)TT;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "obj"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/b;->a(Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;

    move-result-object p0

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.actions.flatbuffers.ActionFlatbufferConverters.typeSafeAction"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;
    .locals 7

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/Action;->getSubActions()Ljava/util/List;

    move-result-object v1

    const-string v2, "action.subActions"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_2

    .line 92
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_2

    .line 93
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/annotations/actions/Action;

    .line 94
    invoke-static {v6, p1}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 96
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 103
    :cond_2
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/GoToAction;

    if-eqz v1, :cond_3

    .line 106
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/GoToAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToAction;->getPageIndex()I

    move-result v0

    int-to-long v0, v0

    .line 107
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/cd;->a(Lcom/pspdfkit/internal/mb;J)I

    move-result v0

    goto/16 :goto_1

    .line 112
    :cond_3
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;

    if-eqz v1, :cond_4

    .line 115
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;->getPdfPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v1

    .line 116
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToRemoteAction;->getPageIndex()I

    move-result v0

    int-to-long v3, v0

    .line 117
    invoke-static {p1, v1, v3, v4}, Lcom/pspdfkit/internal/id;->a(Lcom/pspdfkit/internal/mb;IJ)I

    move-result v0

    goto/16 :goto_1

    .line 123
    :cond_4
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    if-eqz v1, :cond_5

    .line 125
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->isNewWindow()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 131
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPdfPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v3

    .line 132
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPageIndex()I

    move-result v0

    int-to-long v4, v0

    .line 133
    invoke-static {p1, v1, v3, v4, v5}, Lcom/pspdfkit/internal/ed;->a(Lcom/pspdfkit/internal/mb;BIJ)I

    move-result v0

    goto/16 :goto_1

    .line 141
    :cond_5
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/UriAction;

    if-eqz v1, :cond_6

    .line 144
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    .line 145
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/pu;->a(Lcom/pspdfkit/internal/mb;I)I

    move-result v0

    goto :goto_1

    .line 150
    :cond_6
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/LaunchAction;

    if-eqz v1, :cond_7

    .line 153
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/LaunchAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/LaunchAction;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    .line 154
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/sg;->a(Lcom/pspdfkit/internal/mb;I)I

    move-result v0

    goto :goto_1

    .line 159
    :cond_7
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/NamedAction;

    if-eqz v1, :cond_8

    .line 162
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/NamedAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/NamedAction;->getNamedActionType()Lcom/pspdfkit/annotations/actions/NamedAction$NamedActionType;

    move-result-object v0

    const-string v1, "action.namedActionType"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/lj;->a(Lcom/pspdfkit/annotations/actions/NamedAction$NamedActionType;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    .line 163
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/jj;->a(Lcom/pspdfkit/internal/mb;I)I

    move-result v0

    goto :goto_1

    .line 169
    :cond_8
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    if-eqz v1, :cond_9

    .line 172
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/JavaScriptAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/JavaScriptAction;->getScript()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/mb;->a(Ljava/lang/String;)I

    move-result v0

    .line 173
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/fg;->a(Lcom/pspdfkit/internal/mb;I)I

    move-result v0

    goto :goto_1

    .line 178
    :cond_9
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/HideAction;

    if-eqz v1, :cond_a

    .line 179
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/HideAction;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/pd;->a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/HideAction;)I

    move-result v0

    goto :goto_1

    .line 181
    :cond_a
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/ResetFormAction;

    if-eqz v1, :cond_b

    .line 182
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/ResetFormAction;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rb;->a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/ResetFormAction;)I

    move-result v0

    goto :goto_1

    .line 184
    :cond_b
    instance-of v1, p0, Lcom/pspdfkit/annotations/actions/SubmitFormAction;

    if-eqz v1, :cond_c

    .line 185
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/actions/SubmitFormAction;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/rb;->a(Lcom/pspdfkit/internal/mb;Lcom/pspdfkit/annotations/actions/SubmitFormAction;)I

    move-result v0

    .line 198
    :goto_1
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object v1

    invoke-static {p1, v1}, Lcom/pspdfkit/internal/b;->a(Lcom/pspdfkit/internal/mb;[I)I

    move-result v1

    .line 199
    invoke-static {p1}, Lcom/pspdfkit/internal/b;->b(Lcom/pspdfkit/internal/mb;)V

    .line 200
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object p0

    const-string v2, "action.type"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/annotations/actions/ActionType;)S

    move-result p0

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/b;->a(Lcom/pspdfkit/internal/mb;S)V

    .line 201
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/b;->a(Lcom/pspdfkit/internal/mb;I)V

    .line 202
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/b;->b(Lcom/pspdfkit/internal/mb;I)V

    .line 203
    invoke-static {p1}, Lcom/pspdfkit/internal/b;->a(Lcom/pspdfkit/internal/mb;)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    .line 204
    :cond_c
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported action type for writing to flatbuffers: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array p1, v4, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.Annotations"

    invoke-static {v1, p0, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static final a(Lcom/pspdfkit/internal/o;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;
    .locals 5

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/pspdfkit/internal/o;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 218
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 219
    invoke-virtual {p0}, Lcom/pspdfkit/internal/o;->a()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/actions/Action;

    .line 220
    invoke-static {v2, p1}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/internal/mb;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_1

    .line 222
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported action type for writing to flatbuffers: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "PSPDFKit.Annotations"

    invoke-static {v4, v2, v3}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 224
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/w;->b(Lcom/pspdfkit/internal/mb;)V

    .line 225
    invoke-static {v3}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)S

    move-result v2

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/w;->a(Lcom/pspdfkit/internal/mb;S)V

    .line 226
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p1, v2}, Lcom/pspdfkit/internal/w;->a(Lcom/pspdfkit/internal/mb;I)V

    .line 227
    invoke-static {p1}, Lcom/pspdfkit/internal/w;->a(Lcom/pspdfkit/internal/mb;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object p0

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/m1;->a(Lcom/pspdfkit/internal/mb;[I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    return-object v1
.end method

.method private static final a(Lcom/pspdfkit/annotations/actions/ActionType;)S
    .locals 3

    .line 231
    sget-object v0, Lcom/pspdfkit/internal/d$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown action type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 p0, 0xe

    goto :goto_0

    :pswitch_1
    const/16 p0, 0xd

    goto :goto_0

    :pswitch_2
    const/16 p0, 0x13

    goto :goto_0

    :pswitch_3
    const/16 p0, 0x10

    goto :goto_0

    :pswitch_4
    const/16 p0, 0xc

    goto :goto_0

    :pswitch_5
    const/16 p0, 0xb

    goto :goto_0

    :pswitch_6
    const/16 p0, 0xa

    goto :goto_0

    :pswitch_7
    const/16 p0, 0x9

    goto :goto_0

    :pswitch_8
    const/4 p0, 0x6

    goto :goto_0

    :pswitch_9
    const/4 p0, 0x4

    goto :goto_0

    :pswitch_a
    const/4 p0, 0x3

    goto :goto_0

    :pswitch_b
    const/4 p0, 0x2

    goto :goto_0

    :pswitch_c
    const/4 p0, 0x1

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final a(Lcom/pspdfkit/annotations/actions/AnnotationTriggerEvent;)S
    .locals 3

    .line 260
    sget-object v0, Lcom/pspdfkit/internal/d$a;->b:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 274
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown trigger event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 p0, 0xc

    return p0

    :pswitch_1
    const/16 p0, 0xb

    return p0

    :pswitch_2
    const/16 p0, 0xa

    return p0

    :pswitch_3
    const/16 p0, 0x9

    return p0

    :pswitch_4
    const/16 p0, 0x8

    return p0

    :pswitch_5
    const/4 p0, 0x7

    return p0

    :pswitch_6
    const/4 p0, 0x6

    return p0

    :pswitch_7
    const/4 p0, 0x5

    return p0

    :pswitch_8
    const/4 p0, 0x4

    return p0

    :pswitch_9
    const/4 p0, 0x3

    return p0

    :pswitch_a
    const/4 p0, 0x2

    return p0

    :pswitch_b
    const/4 p0, 0x1

    return p0

    :pswitch_c
    const/4 p0, 0x0

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

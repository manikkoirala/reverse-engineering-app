.class public final Lcom/pspdfkit/internal/gm$a;
.super Lcom/pspdfkit/internal/g4$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/gm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/g4$a<",
        "Lcom/pspdfkit/internal/gm;",
        "Lcom/pspdfkit/internal/gm$a;",
        ">;"
    }
.end annotation


# instance fields
.field t:I

.field u:I

.field v:I

.field w:I


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/zf;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/g4$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/pspdfkit/internal/g4$a;
    .locals 0

    return-object p0
.end method

.method public final b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/gm$a;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g4$a;->a(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gm$a;

    iget v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionFullPageWidth:I

    .line 2
    iput v1, v0, Lcom/pspdfkit/internal/gm$a;->v:I

    .line 3
    iget v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionFullPageHeight:I

    .line 4
    iput v1, v0, Lcom/pspdfkit/internal/gm$a;->w:I

    .line 5
    iget v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionX:I

    .line 6
    iput v1, v0, Lcom/pspdfkit/internal/gm$a;->t:I

    .line 7
    iget p1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->regionY:I

    .line 8
    iput p1, v0, Lcom/pspdfkit/internal/gm$a;->u:I

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/gm;
    .locals 28

    move-object/from16 v0, p0

    .line 9
    new-instance v26, Lcom/pspdfkit/internal/gm;

    move-object/from16 v1, v26

    iget-object v2, v0, Lcom/pspdfkit/internal/g4$a;->a:Lcom/pspdfkit/internal/zf;

    iget v3, v0, Lcom/pspdfkit/internal/g4$a;->b:I

    iget v4, v0, Lcom/pspdfkit/internal/g4$a;->e:I

    iget-object v5, v0, Lcom/pspdfkit/internal/g4$a;->f:Landroid/graphics/Bitmap;

    iget v6, v0, Lcom/pspdfkit/internal/g4$a;->g:I

    iget v7, v0, Lcom/pspdfkit/internal/g4$a;->h:I

    iget v8, v0, Lcom/pspdfkit/internal/g4$a;->i:I

    iget-object v9, v0, Lcom/pspdfkit/internal/g4$a;->j:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/pspdfkit/internal/g4$a;->k:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/pspdfkit/internal/g4$a;->m:Ljava/lang/Integer;

    iget-object v12, v0, Lcom/pspdfkit/internal/g4$a;->l:Ljava/lang/Integer;

    iget-boolean v13, v0, Lcom/pspdfkit/internal/g4$a;->n:Z

    iget-boolean v14, v0, Lcom/pspdfkit/internal/g4$a;->o:Z

    iget-object v15, v0, Lcom/pspdfkit/internal/g4$a;->q:Ljava/util/ArrayList;

    move-object/from16 v27, v1

    iget-object v1, v0, Lcom/pspdfkit/internal/g4$a;->c:Ljava/util/ArrayList;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/pspdfkit/internal/g4$a;->d:Ljava/util/ArrayList;

    move-object/from16 v17, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->r:Z

    move/from16 v18, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->s:Z

    move/from16 v19, v1

    iget v1, v0, Lcom/pspdfkit/internal/gm$a;->t:I

    move/from16 v20, v1

    iget v1, v0, Lcom/pspdfkit/internal/gm$a;->u:I

    move/from16 v21, v1

    iget v1, v0, Lcom/pspdfkit/internal/gm$a;->v:I

    move/from16 v22, v1

    iget v1, v0, Lcom/pspdfkit/internal/gm$a;->w:I

    move/from16 v23, v1

    iget-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->p:Z

    move/from16 v24, v1

    const/16 v25, 0x0

    move-object/from16 v1, v27

    invoke-direct/range {v1 .. v25}, Lcom/pspdfkit/internal/gm;-><init>(Lcom/pspdfkit/internal/zf;IILandroid/graphics/Bitmap;IIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZIIIIZLcom/pspdfkit/internal/gm-IA;)V

    return-object v26
.end method

.method public final d(I)Lcom/pspdfkit/internal/gm$a;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/gm$a;->w:I

    return-object p0
.end method

.method public final e(I)Lcom/pspdfkit/internal/gm$a;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/gm$a;->v:I

    return-object p0
.end method

.method public final f(I)Lcom/pspdfkit/internal/gm$a;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/gm$a;->t:I

    return-object p0
.end method

.method public final g(I)Lcom/pspdfkit/internal/gm$a;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/gm$a;->u:I

    return-object p0
.end method

.class final Lcom/pspdfkit/internal/yu$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/yu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/yu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/yu$c;->b:Lcom/pspdfkit/internal/yu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$c;->b:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetn(Lcom/pspdfkit/internal/yu;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fputn(Lcom/pspdfkit/internal/yu;Z)V

    if-eqz v0, :cond_0

    .line 2
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetp(Lcom/pspdfkit/internal/yu;)Landroidx/appcompat/widget/AppCompatImageButton;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__uvv_player_scale_out_btn:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetp(Lcom/pspdfkit/internal/yu;)Landroidx/appcompat/widget/AppCompatImageButton;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__uvv_player_scale_btn:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageButton;->setImageResource(I)V

    .line 5
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$c;->b:Lcom/pspdfkit/internal/yu;

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetq(Lcom/pspdfkit/internal/yu;)Landroid/view/View;

    move-result-object v0

    .line 7
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetn(Lcom/pspdfkit/internal/yu;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const/4 p1, 0x4

    :goto_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$c;->b:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetn(Lcom/pspdfkit/internal/yu;)Z

    move-result p1

    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->setFullscreen(Z)V

    return-void
.end method

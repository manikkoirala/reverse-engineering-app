.class public abstract Lcom/pspdfkit/internal/g4$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/g4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/pspdfkit/internal/g4;",
        "B:",
        "Lcom/pspdfkit/internal/g4$a<",
        "TT;TB;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Lcom/pspdfkit/internal/zf;

.field final b:I

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/ArrayList;

.field e:I

.field f:Landroid/graphics/Bitmap;

.field g:I

.field h:I

.field i:I

.field j:Ljava/lang/Integer;

.field k:Ljava/lang/Integer;

.field l:Ljava/lang/Integer;

.field m:Ljava/lang/Integer;

.field n:Z

.field o:Z

.field p:Z

.field q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field r:Z

.field s:Z


# direct methods
.method protected constructor <init>(Lcom/pspdfkit/internal/zf;I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/g4$a;->c:Ljava/util/ArrayList;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/g4$a;->d:Ljava/util/ArrayList;

    const/4 v0, 0x3

    .line 8
    iput v0, p0, Lcom/pspdfkit/internal/g4$a;->e:I

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/pspdfkit/internal/g4$a;->f:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    .line 14
    iput v1, p0, Lcom/pspdfkit/internal/g4$a;->g:I

    .line 17
    iput v1, p0, Lcom/pspdfkit/internal/g4$a;->h:I

    const/4 v2, -0x1

    .line 20
    iput v2, p0, Lcom/pspdfkit/internal/g4$a;->i:I

    .line 39
    iput-boolean v1, p0, Lcom/pspdfkit/internal/g4$a;->n:Z

    .line 40
    iput-boolean v1, p0, Lcom/pspdfkit/internal/g4$a;->o:Z

    .line 41
    iput-boolean v1, p0, Lcom/pspdfkit/internal/g4$a;->p:Z

    .line 43
    iput-object v0, p0, Lcom/pspdfkit/internal/g4$a;->q:Ljava/util/ArrayList;

    .line 46
    iput-boolean v1, p0, Lcom/pspdfkit/internal/g4$a;->r:Z

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/pspdfkit/internal/g4$a;->s:Z

    .line 56
    iput-object p1, p0, Lcom/pspdfkit/internal/g4$a;->a:Lcom/pspdfkit/internal/zf;

    .line 57
    iput p2, p0, Lcom/pspdfkit/internal/g4$a;->b:I

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/pspdfkit/internal/g4$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation
.end method

.method public final a(I)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .line 34
    iput p1, p0, Lcom/pspdfkit/internal/g4$a;->h:I

    .line 35
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")TB;"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/pspdfkit/internal/g4$a;->f:Landroid/graphics/Bitmap;

    .line 33
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/g4$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;",
            ")TB;"
        }
    .end annotation

    .line 1
    iget-object v0, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->reuseBitmap:Landroid/graphics/Bitmap;

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/g4$a;->f:Landroid/graphics/Bitmap;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 4
    iget v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    .line 5
    iput v1, v0, Lcom/pspdfkit/internal/g4$a;->i:I

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 7
    iget-object v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formHighlightColor:Ljava/lang/Integer;

    .line 8
    iput-object v1, v0, Lcom/pspdfkit/internal/g4$a;->j:Ljava/lang/Integer;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 10
    iget-object v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formItemHighlightColor:Ljava/lang/Integer;

    .line 11
    iput-object v1, v0, Lcom/pspdfkit/internal/g4$a;->k:Ljava/lang/Integer;

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 13
    iget-object v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->formRequiredFieldBorderColor:Ljava/lang/Integer;

    .line 14
    iput-object v1, v0, Lcom/pspdfkit/internal/g4$a;->m:Ljava/lang/Integer;

    .line 15
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 16
    iget-object v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->signHereOverlayBackgroundColor:Ljava/lang/Integer;

    .line 17
    iput-object v1, v0, Lcom/pspdfkit/internal/g4$a;->l:Ljava/lang/Integer;

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 19
    iget-boolean v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->toGrayscale:Z

    .line 20
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->o:Z

    .line 21
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 22
    iget-boolean v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    .line 23
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->n:Z

    .line 24
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 25
    iget-boolean v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->redactionAnnotationPreviewEnabled:Z

    .line 26
    iput-boolean v1, v0, Lcom/pspdfkit/internal/g4$a;->r:Z

    .line 27
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    .line 28
    iget-object v1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->renderedDrawables:Ljava/util/List;

    .line 29
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    iget-boolean p1, p1, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->showSignHereOverlay:Z

    .line 30
    iput-boolean p1, v0, Lcom/pspdfkit/internal/g4$a;->s:Z

    .line 31
    invoke-virtual {v0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/Integer;)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")TB;"
        }
    .end annotation

    .line 36
    iput-object p1, p0, Lcom/pspdfkit/internal/g4$a;->m:Ljava/lang/Integer;

    .line 37
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;)TB;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/g4$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 41
    iget-object v0, p0, Lcom/pspdfkit/internal/g4$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 42
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawable;",
            ">;)TB;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/g4$a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/g4$a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 45
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final a(Z)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .line 38
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g4$a;->p:Z

    .line 39
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final b(I)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/g4$a;->g:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)TB;"
        }
    .end annotation

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/g4$a;->q:Ljava/util/ArrayList;

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final b(Z)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/g4$a;->r:Z

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

.method public final c(I)Lcom/pspdfkit/internal/g4$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/g4$a;->e:I

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/g4$a;->a()Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    return-object p1
.end method

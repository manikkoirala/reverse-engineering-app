.class public final Lcom/pspdfkit/internal/hm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/pspdfkit/internal/hm;

.field public static final b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private static c:I


# direct methods
.method public static synthetic $r8$lambda$Ar8tj7B27iZwjNAFjYSxwKUM5bI(Lcom/pspdfkit/internal/gm;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/gm;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$ByGZbSlFXUhM_VA0ys7crFyvSZI(Lcom/pspdfkit/internal/rc;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/hm;->b(Lcom/pspdfkit/internal/rc;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$GUDkLMhnAsGoNBNSvDh_yh6Sjos(JLcom/pspdfkit/internal/gm;ILio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/hm;->a(JLcom/pspdfkit/internal/gm;ILio/reactivex/rxjava3/core/SingleEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$I7tRjztxdE10vj44ZaR4EB_PJlk(Lcom/pspdfkit/internal/rc;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$U_4GlqQIKKI3C8Ox_GiVbzvbYt8(Lcom/pspdfkit/internal/rc;JLcom/pspdfkit/internal/yl;Lio/reactivex/rxjava3/core/MaybeEmitter;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;JLcom/pspdfkit/internal/yl;Lio/reactivex/rxjava3/core/MaybeEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oZKGIb7vilDsOFJSmmybjIMiYzE(Lcom/pspdfkit/internal/gm;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/hm;->b(Lcom/pspdfkit/internal/gm;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$tKMumOy7ySp2oBU-EKw6ckUwJvU(JLcom/pspdfkit/internal/l8;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/hm;->a(JLcom/pspdfkit/internal/l8;Lio/reactivex/rxjava3/core/SingleEmitter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$uv5tC08qjfGsRXPRuihbmy_jq04(JLcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;ILio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/internal/hm;->a(JLcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;ILio/reactivex/rxjava3/core/SingleEmitter;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/hm;

    invoke-direct {v0}, Lcom/pspdfkit/internal/hm;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/hm;->a:Lcom/pspdfkit/internal/hm;

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->FILE:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    const-string v1, "of(AnnotationType.FILE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;J)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1

    .line 24
    new-instance v0, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/rc;JLcom/pspdfkit/internal/yl;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->create(Lio/reactivex/rxjava3/core/MaybeOnSubscribe;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    .line 77
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    iget p0, p0, Lcom/pspdfkit/internal/g4;->b:I

    check-cast p2, Lcom/pspdfkit/internal/u;

    invoke-virtual {p2, p0}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p0

    invoke-virtual {p1, p0}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    const-string p1, "create(\n            Mayb\u2026eduler(options.priority))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/gm;)Lio/reactivex/rxjava3/core/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/gm;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 372
    sget-object v2, Lcom/pspdfkit/internal/hm;->a:Lcom/pspdfkit/internal/hm;

    monitor-enter v2

    .line 373
    :try_start_0
    sget v3, Lcom/pspdfkit/internal/hm;->c:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/pspdfkit/internal/hm;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    .line 374
    new-instance v2, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda6;

    invoke-direct {v2, v0, v1, p0, v3}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda6;-><init>(JLcom/pspdfkit/internal/gm;I)V

    invoke-static {v2}, Lio/reactivex/rxjava3/core/Single;->create(Lio/reactivex/rxjava3/core/SingleOnSubscribe;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 431
    new-instance v1, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, v3}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/gm;I)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget p0, p0, Lcom/pspdfkit/internal/g4;->b:I

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p0

    invoke-virtual {v0, p0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string v0, "create(\n            Sing\u2026eduler(options.priority))"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v2

    throw p0
.end method

.method public static final a(Lcom/pspdfkit/internal/l8;)Lio/reactivex/rxjava3/core/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/l8;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 591
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 592
    new-instance v2, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda4;

    invoke-direct {v2, v0, v1, p0}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda4;-><init>(JLcom/pspdfkit/internal/l8;)V

    invoke-static {v2}, Lio/reactivex/rxjava3/core/Single;->create(Lio/reactivex/rxjava3/core/SingleOnSubscribe;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 636
    iget-object v1, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget p0, p0, Lcom/pspdfkit/internal/g4;->b:I

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p0

    invoke-virtual {v0, p0}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string v0, "create(\n            Sing\u2026eduler(options.priority))"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/rc;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object v2

    const-string v3, "getBitmapCache()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-boolean v3, p0, Lcom/pspdfkit/internal/rc;->t:Z

    if-eqz v3, :cond_0

    .line 8
    sget-object v3, Lcom/pspdfkit/internal/hm;->a:Lcom/pspdfkit/internal/hm;

    invoke-static {p0, v2, v0, v1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;J)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v4

    .line 13
    invoke-direct {v3, p0, v2, v0, v1}, Lcom/pspdfkit/internal/hm;->b(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;J)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-virtual {v4, p0}, Lio/reactivex/rxjava3/core/Maybe;->switchIfEmpty(Lio/reactivex/rxjava3/core/SingleSource;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    const-string v0, "{\n            // First t\u2026ueRequestTime))\n        }"

    .line 14
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :cond_0
    sget-object v3, Lcom/pspdfkit/internal/hm;->a:Lcom/pspdfkit/internal/hm;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/pspdfkit/internal/hm;->b(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;J)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final a(JLcom/pspdfkit/internal/gm;ILio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 18

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    const-string v2, "$options"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "emitter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, p0

    .line 444
    new-instance v4, Lcom/pspdfkit/internal/yh;

    iget-object v5, v0, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    iget v6, v0, Lcom/pspdfkit/internal/g4;->e:I

    iget v7, v0, Lcom/pspdfkit/internal/g4;->f:I

    invoke-direct {v4, v6, v7, v5}, Lcom/pspdfkit/internal/yh;-><init>(IILandroid/graphics/Bitmap;)V

    .line 445
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v5

    const-string v6, "managedBitmap.bitmap"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v5

    .line 446
    :try_start_0
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v6

    const-string v7, "managedBitmap.bitmap"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 447
    invoke-interface/range {p4 .. p4}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 448
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    monitor-exit v5

    return-void

    .line 450
    :cond_0
    :try_start_1
    iget-object v7, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v7}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v7

    iget v8, v0, Lcom/pspdfkit/internal/g4;->d:I

    check-cast v7, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v7, v8}, Lcom/pspdfkit/internal/r1;->c(I)V

    .line 451
    iget-object v7, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v7}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v7

    invoke-interface {v7}, Lcom/pspdfkit/internal/uf;->syncDirtyWidgetAnnotationsToNative()V

    .line 452
    sget-object v7, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 453
    invoke-static {v0, v7}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    move-result-object v14

    const-string v7, "baseRenderOptionsToNativ\u2026                        )"

    invoke-static {v14, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 457
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 458
    iget-object v7, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    .line 459
    iget v8, v0, Lcom/pspdfkit/internal/g4;->d:I

    .line 461
    iget v10, v0, Lcom/pspdfkit/internal/gm;->t:I

    .line 462
    iget v11, v0, Lcom/pspdfkit/internal/gm;->u:I

    .line 463
    iget v12, v0, Lcom/pspdfkit/internal/gm;->v:I

    .line 464
    iget v13, v0, Lcom/pspdfkit/internal/gm;->w:I

    move-object v9, v6

    move/from16 v15, p3

    .line 465
    invoke-virtual/range {v7 .. v15}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/Bitmap;IIIILcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 476
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->c()V

    .line 478
    new-instance v2, Lcom/pspdfkit/internal/kp;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/kp;-><init>(Lcom/pspdfkit/internal/g4;)V

    .line 479
    invoke-interface {v1, v2}, Lio/reactivex/rxjava3/core/SingleEmitter;->tryOnError(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    monitor-exit v5

    return-void

    .line 485
    :cond_1
    :try_start_2
    iget v7, v0, Lcom/pspdfkit/internal/g4;->g:I

    invoke-static {v7}, Landroid/graphics/Color;->alpha(I)I

    move-result v7

    const/16 v8, 0xff

    if-ge v7, v8, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    .line 486
    :goto_0
    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 487
    iget v7, v0, Lcom/pspdfkit/internal/gm;->t:I

    .line 488
    iget v8, v0, Lcom/pspdfkit/internal/gm;->u:I

    neg-int v8, v8

    .line 491
    iget-object v10, v0, Lcom/pspdfkit/internal/g4;->n:Ljava/util/List;

    const-string v11, "options.pdfDrawables"

    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    iget v11, v0, Lcom/pspdfkit/internal/gm;->v:I

    int-to-float v11, v11

    iget-object v12, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget v13, v0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v12, v13}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v12

    iget v12, v12, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v11, v12

    .line 495
    iget v12, v0, Lcom/pspdfkit/internal/gm;->v:I

    add-int/2addr v12, v7

    .line 496
    iget v13, v0, Lcom/pspdfkit/internal/gm;->w:I

    add-int/2addr v13, v8

    .line 497
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_3

    goto :goto_2

    .line 498
    :cond_3
    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 508
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 509
    new-instance v15, Lcom/pspdfkit/internal/bn;

    invoke-direct {v15, v10, v11}, Lcom/pspdfkit/internal/bn;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawable;F)V

    .line 510
    invoke-virtual {v15, v7, v8, v12, v13}, Lcom/pspdfkit/internal/bn;->setBounds(IIII)V

    .line 511
    invoke-virtual {v15, v14}, Lcom/pspdfkit/internal/bn;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 512
    :cond_4
    :goto_2
    invoke-interface/range {p4 .. p4}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 513
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 514
    monitor-exit v5

    return-void

    :cond_5
    :try_start_3
    const-string v6, "PSPDFKit.PageRenderer"

    .line 518
    iget v7, v0, Lcom/pspdfkit/internal/g4;->d:I

    .line 519
    iget v8, v0, Lcom/pspdfkit/internal/gm;->t:I

    iget v10, v0, Lcom/pspdfkit/internal/gm;->u:I

    iget v11, v0, Lcom/pspdfkit/internal/g4;->e:I

    iget v12, v0, Lcom/pspdfkit/internal/g4;->f:I

    .line 521
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long v13, v13, v16

    .line 522
    iget v0, v0, Lcom/pspdfkit/internal/g4;->b:I

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "renderPageRegion() report: [pageIndex = "

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", region = "

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", "

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", "

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "x"

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", queue_waiting_time = "

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " ms, total_rendering_time = "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "ms, priority = "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 523
    invoke-static {v6, v0, v2}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 532
    monitor-exit v5

    .line 582
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {v1, v0}, Lio/reactivex/rxjava3/core/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 583
    monitor-exit v5

    throw v0
.end method

.method private static final a(JLcom/pspdfkit/internal/l8;Lio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 11

    const-string v0, "$options"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emitter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 637
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p0

    .line 638
    new-instance p0, Lcom/pspdfkit/internal/yh;

    .line 639
    iget-object p1, p2, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    .line 640
    iget v2, p2, Lcom/pspdfkit/internal/g4;->e:I

    .line 641
    iget v3, p2, Lcom/pspdfkit/internal/g4;->f:I

    .line 642
    invoke-direct {p0, v2, v3, p1}, Lcom/pspdfkit/internal/yh;-><init>(IILandroid/graphics/Bitmap;)V

    .line 647
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v2, "managedBitmap.bitmap"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter p1

    .line 648
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    const-string v3, "managedBitmap.bitmap"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 649
    invoke-interface {p3}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 650
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 651
    monitor-exit p1

    return-void

    .line 656
    :cond_0
    :try_start_1
    sget-object v3, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 657
    invoke-static {p2, v3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    move-result-object v3

    const-string v4, "baseRenderOptionsToNativ\u2026                        )"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 661
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 662
    iget-object v6, p2, Lcom/pspdfkit/internal/l8;->t:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 663
    iget v7, p2, Lcom/pspdfkit/internal/g4;->d:I

    .line 664
    invoke-virtual {v6, v7, v2, v3}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->render(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;)V

    .line 670
    iget v3, p2, Lcom/pspdfkit/internal/g4;->g:I

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    const/16 v6, 0xff

    const/4 v7, 0x0

    if-ge v3, v6, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 671
    :goto_0
    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 674
    invoke-interface {p3}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 675
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 676
    monitor-exit p1

    return-void

    :cond_2
    :try_start_2
    const-string v2, "PSPDFKit.PageRenderer"

    .line 680
    iget v3, p2, Lcom/pspdfkit/internal/g4;->d:I

    .line 681
    iget v6, p2, Lcom/pspdfkit/internal/g4;->e:I

    iget v8, p2, Lcom/pspdfkit/internal/g4;->f:I

    .line 683
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v4

    .line 684
    iget p2, p2, Lcom/pspdfkit/internal/g4;->b:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "renderDocumentEditorPage() report: [pageIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", resolution = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "x"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", queue_waiting_time = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms, total_rendering_time = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "ms, priority = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "]"

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-array v0, v7, [Ljava/lang/Object;

    .line 685
    invoke-static {v2, p2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 693
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 694
    monitor-exit p1

    .line 727
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object p0

    invoke-interface {p3, p0}, Lio/reactivex/rxjava3/core/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p0

    .line 728
    monitor-exit p1

    throw p0
.end method

.method private static final a(JLcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;ILio/reactivex/rxjava3/core/SingleEmitter;)V
    .locals 17

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    const-string v2, "$options"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "$cache"

    move-object/from16 v6, p3

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "emitter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v9, v2, p0

    .line 203
    new-instance v2, Lcom/pspdfkit/internal/yh;

    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    iget v4, v0, Lcom/pspdfkit/internal/g4;->e:I

    iget v5, v0, Lcom/pspdfkit/internal/g4;->f:I

    invoke-direct {v2, v4, v5, v3}, Lcom/pspdfkit/internal/yh;-><init>(IILandroid/graphics/Bitmap;)V

    .line 204
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v11

    const-string v3, "managedBitmap.bitmap"

    invoke-static {v11, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v11

    .line 205
    :try_start_0
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v12

    const-string v3, "managedBitmap.bitmap"

    invoke-static {v12, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-interface/range {p5 .. p5}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 207
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    monitor-exit v11

    return-void

    .line 209
    :cond_0
    :try_start_1
    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v3

    iget v4, v0, Lcom/pspdfkit/internal/g4;->d:I

    check-cast v3, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/r1;->c(I)V

    .line 210
    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->e()Lcom/pspdfkit/internal/uf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/uf;->syncDirtyWidgetAnnotationsToNative()V

    .line 211
    sget-object v3, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 212
    invoke-static {v0, v3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    move-result-object v7

    const-string v3, "baseRenderOptionsToNativ\u2026                        )"

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 217
    iget-boolean v3, v0, Lcom/pspdfkit/internal/rc;->t:Z

    if-eqz v3, :cond_1

    .line 218
    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    .line 219
    iget v4, v0, Lcom/pspdfkit/internal/g4;->d:I

    move-object v5, v12

    move-object/from16 v6, p3

    move/from16 v8, p4

    .line 220
    invoke-virtual/range {v3 .. v8}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/yl;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 228
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->c()V

    .line 230
    new-instance v2, Lcom/pspdfkit/internal/kp;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/kp;-><init>(Lcom/pspdfkit/internal/g4;)V

    .line 231
    invoke-interface {v1, v2}, Lio/reactivex/rxjava3/core/SingleEmitter;->tryOnError(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234
    monitor-exit v11

    return-void

    .line 237
    :cond_1
    :try_start_2
    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    .line 238
    iget v4, v0, Lcom/pspdfkit/internal/g4;->d:I

    move/from16 v5, p4

    .line 239
    invoke-virtual {v3, v4, v12, v7, v5}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/Bitmap;Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 246
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->c()V

    .line 248
    new-instance v2, Lcom/pspdfkit/internal/kp;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/kp;-><init>(Lcom/pspdfkit/internal/g4;)V

    .line 249
    invoke-interface {v1, v2}, Lio/reactivex/rxjava3/core/SingleEmitter;->tryOnError(Ljava/lang/Throwable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 252
    monitor-exit v11

    return-void

    .line 256
    :cond_2
    :try_start_3
    iget v3, v0, Lcom/pspdfkit/internal/g4;->g:I

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    const/16 v4, 0xff

    const/4 v5, 0x0

    if-ge v3, v4, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    .line 257
    :goto_0
    invoke-virtual {v12, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 258
    iget-object v3, v0, Lcom/pspdfkit/internal/g4;->n:Ljava/util/List;

    const-string v4, "options.pdfDrawables"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget-object v6, v0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget v7, v0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v6, v7}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v6

    iget v6, v6, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v4, v6

    .line 262
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 263
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 264
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    goto :goto_2

    .line 265
    :cond_4
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 275
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 276
    new-instance v15, Lcom/pspdfkit/internal/bn;

    invoke-direct {v15, v12, v4}, Lcom/pspdfkit/internal/bn;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawable;F)V

    .line 277
    invoke-virtual {v15, v5, v5, v6, v7}, Lcom/pspdfkit/internal/bn;->setBounds(IIII)V

    .line 278
    invoke-virtual {v15, v8}, Lcom/pspdfkit/internal/bn;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 279
    :cond_5
    :goto_2
    invoke-interface/range {p5 .. p5}, Lio/reactivex/rxjava3/core/SingleEmitter;->isDisposed()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 280
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 281
    monitor-exit v11

    return-void

    :cond_6
    :try_start_4
    const-string v3, "PSPDFKit.PageRenderer"

    .line 285
    iget v4, v0, Lcom/pspdfkit/internal/g4;->d:I

    .line 286
    iget v6, v0, Lcom/pspdfkit/internal/g4;->e:I

    iget v7, v0, Lcom/pspdfkit/internal/g4;->f:I

    .line 288
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    sub-long v12, v15, v13

    .line 289
    iget v0, v0, Lcom/pspdfkit/internal/g4;->b:I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "renderFullPage() report: [pageIndex = "

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", resolution = "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "x"

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", queue_waiting_time = "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, "ms, total_rendering_time = "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, "ms, priority = "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v5, [Ljava/lang/Object;

    .line 290
    invoke-static {v3, v0, v4}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 299
    monitor-exit v11

    .line 362
    invoke-virtual {v2}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {v1, v0}, Lio/reactivex/rxjava3/core/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 363
    monitor-exit v11

    throw v0
.end method

.method private static final a(Lcom/pspdfkit/internal/gm;I)V
    .locals 1

    const-string v0, "$options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 584
    new-instance v0, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/gm;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 589
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 590
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/rc;I)V
    .locals 1

    const-string v0, "$options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    new-instance v0, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/rc;I)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Completable;->fromAction(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 369
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p0

    .line 370
    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Completable;->subscribe()Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/rc;JLcom/pspdfkit/internal/yl;Lio/reactivex/rxjava3/core/MaybeEmitter;)V
    .locals 11

    const-string v0, "$options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$cache"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emitter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget v0, p0, Lcom/pspdfkit/internal/g4;->f:I

    if-lez v0, :cond_7

    iget v0, p0, Lcom/pspdfkit/internal/g4;->e:I

    if-gtz v0, :cond_0

    goto/16 :goto_3

    .line 83
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 84
    new-instance p1, Lcom/pspdfkit/internal/yh;

    iget-object p2, p0, Lcom/pspdfkit/internal/g4;->c:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/pspdfkit/internal/g4;->e:I

    iget v3, p0, Lcom/pspdfkit/internal/g4;->f:I

    invoke-direct {p1, v2, v3, p2}, Lcom/pspdfkit/internal/yh;-><init>(IILandroid/graphics/Bitmap;)V

    .line 85
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object p2

    const-string v2, "managedBitmap.bitmap"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter p2

    .line 86
    :try_start_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    const-string v3, "managedBitmap.bitmap"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-interface {p4}, Lio/reactivex/rxjava3/core/MaybeEmitter;->isDisposed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 88
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p2

    return-void

    .line 94
    :cond_1
    :try_start_1
    sget-object v3, Lcom/pspdfkit/internal/hm;->b:Ljava/util/EnumSet;

    .line 95
    invoke-static {p0, v3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/g4;Ljava/util/EnumSet;)Lcom/pspdfkit/internal/jni/NativePageRenderingConfig;

    move-result-object v3

    const-string v4, "baseRenderOptionsToNativ\u2026                        )"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 102
    iget-object v6, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    .line 103
    iget v7, p0, Lcom/pspdfkit/internal/g4;->d:I

    .line 104
    invoke-virtual {p3, v2, v6, v7, v3}, Lcom/pspdfkit/internal/yl;->a(Landroid/graphics/Bitmap;Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativePageRenderingConfig;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 111
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->c()V

    .line 112
    invoke-interface {p4}, Lio/reactivex/rxjava3/core/MaybeEmitter;->onComplete()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    monitor-exit p2

    return-void

    .line 116
    :cond_2
    :try_start_2
    iget p3, p0, Lcom/pspdfkit/internal/g4;->g:I

    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    const/16 v3, 0xff

    const/4 v6, 0x0

    if-ge p3, v3, :cond_3

    const/4 p3, 0x1

    goto :goto_0

    :cond_3
    const/4 p3, 0x0

    .line 117
    :goto_0
    invoke-virtual {v2, p3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 118
    iget-object p3, p0, Lcom/pspdfkit/internal/g4;->n:Ljava/util/List;

    const-string v3, "options.pdfDrawables"

    invoke-static {p3, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v7, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget v8, p0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v7, v8}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v7

    iget v7, v7, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v3, v7

    .line 122
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 123
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 124
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    goto :goto_2

    .line 125
    :cond_4
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 135
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/drawable/PdfDrawable;

    .line 136
    new-instance v10, Lcom/pspdfkit/internal/bn;

    invoke-direct {v10, v2, v3}, Lcom/pspdfkit/internal/bn;-><init>(Lcom/pspdfkit/ui/drawable/PdfDrawable;F)V

    .line 137
    invoke-virtual {v10, v6, v6, v7, v8}, Lcom/pspdfkit/internal/bn;->setBounds(IIII)V

    .line 138
    invoke-virtual {v10, v9}, Lcom/pspdfkit/internal/bn;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 139
    :cond_5
    :goto_2
    invoke-interface {p4}, Lio/reactivex/rxjava3/core/MaybeEmitter;->isDisposed()Z

    move-result p3

    if-eqz p3, :cond_6

    .line 140
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    monitor-exit p2

    return-void

    :cond_6
    :try_start_3
    const-string p3, "PSPDFKit.PageRenderer"

    .line 145
    iget v2, p0, Lcom/pspdfkit/internal/g4;->d:I

    .line 146
    iget v3, p0, Lcom/pspdfkit/internal/g4;->e:I

    iget v7, p0, Lcom/pspdfkit/internal/g4;->f:I

    .line 148
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    .line 149
    iget p0, p0, Lcom/pspdfkit/internal/g4;->b:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "renderFullPage() report: [pageIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " resolution = "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "x"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", queue_waiting_time = "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms, total_rendering_time = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "ms, priority = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ", retrieved from cache]"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array v0, v6, [Ljava/lang/Object;

    .line 150
    invoke-static {p3, p0, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 159
    monitor-exit p2

    .line 199
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yh;->a()Landroid/graphics/Bitmap;

    move-result-object p0

    invoke-interface {p4, p0}, Lio/reactivex/rxjava3/core/MaybeEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p0

    .line 200
    monitor-exit p2

    throw p0

    .line 201
    :cond_7
    :goto_3
    invoke-interface {p4}, Lio/reactivex/rxjava3/core/MaybeEmitter;->onComplete()V

    return-void
.end method

.method private final b(Lcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;J)Lio/reactivex/rxjava3/core/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/rc;",
            "Lcom/pspdfkit/internal/yl;",
            "J)",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    sget v6, Lcom/pspdfkit/internal/hm;->c:I

    add-int/lit8 v0, v6, 0x1

    sput v0, Lcom/pspdfkit/internal/hm;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 3
    new-instance v7, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda2;

    move-object v0, v7

    move-wide v1, p3

    move-object v3, p1

    move-object v4, p2

    move v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda2;-><init>(JLcom/pspdfkit/internal/rc;Lcom/pspdfkit/internal/yl;I)V

    invoke-static {v7}, Lio/reactivex/rxjava3/core/Single;->create(Lio/reactivex/rxjava3/core/SingleOnSubscribe;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 73
    new-instance p3, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda3;

    invoke-direct {p3, p1, v6}, Lcom/pspdfkit/internal/hm$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/rc;I)V

    invoke-virtual {p2, p3}, Lio/reactivex/rxjava3/core/Single;->doOnDispose(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p2

    .line 84
    iget-object p3, p1, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget p1, p1, Lcom/pspdfkit/internal/g4;->b:I

    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/zf;->h(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    invoke-virtual {p2, p1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    const-string p2, "create(\n            Sing\u2026eduler(options.priority))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private static final b(Lcom/pspdfkit/internal/gm;I)V
    .locals 1

    const-string v0, "$options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget p0, p0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/zf;->a(II)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string p1, "PSPDFKit.PageRenderer"

    const-string v0, "renderPageRegion() report: [cancelled]"

    .line 88
    invoke-static {p1, v0, p0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/rc;I)V
    .locals 1

    const-string v0, "$options"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/pspdfkit/internal/g4;->a:Lcom/pspdfkit/internal/zf;

    iget p0, p0, Lcom/pspdfkit/internal/g4;->d:I

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/zf;->a(II)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string p1, "PSPDFKit.PageRenderer"

    const-string v0, "renderFullPage() report: [cancelled]"

    .line 86
    invoke-static {p1, v0, p0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

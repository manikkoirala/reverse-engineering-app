.class public final Lcom/pspdfkit/internal/od;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/pspdfkit/internal/c<",
        "Lcom/pspdfkit/annotations/actions/HideAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/views/document/DocumentView;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;)V
    .locals 1

    const-string v0, "documentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/od;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/od;)Lcom/pspdfkit/internal/views/document/DocumentView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/od;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    return-object p0
.end method


# virtual methods
.method public final executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)Z
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/actions/HideAction;

    const-string p2, "action"

    .line 2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object p2, p0, Lcom/pspdfkit/internal/od;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->getDocument()Lcom/pspdfkit/internal/zf;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/actions/HideAction;->getAnnotationsAsync(Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    .line 31
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p2

    .line 32
    new-instance v0, Lcom/pspdfkit/internal/md;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/md;-><init>(Lcom/pspdfkit/internal/od;Lcom/pspdfkit/annotations/actions/HideAction;)V

    sget-object p1, Lcom/pspdfkit/internal/nd;->a:Lcom/pspdfkit/internal/nd;

    invoke-virtual {p2, v0, p1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    const/4 p1, 0x1

    :goto_0
    return p1
.end method

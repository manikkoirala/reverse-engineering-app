.class public final Lcom/pspdfkit/internal/aj;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zu$h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/aj$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/document/PdfDocument;

.field private final c:Lcom/pspdfkit/internal/yu;

.field private d:Lio/reactivex/rxjava3/disposables/Disposable;

.field private final e:Landroidx/appcompat/widget/AppCompatImageView;

.field private final f:Landroidx/appcompat/widget/AppCompatImageView;

.field private g:Lio/reactivex/rxjava3/disposables/Disposable;

.field private h:Z

.field private i:Z

.field private final j:Lcom/pspdfkit/internal/zu;

.field private k:I

.field private l:Lcom/pspdfkit/internal/wi;

.field private m:Lcom/pspdfkit/internal/aj$a;


# direct methods
.method public static synthetic $r8$lambda$-GR8R3MH8MFcZPozyTSrkvJh-NI(Lcom/pspdfkit/internal/aj;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/aj;->b(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$BM5xTJ3GuS1j0xljnjyHFXIJW0c(Lcom/pspdfkit/internal/aj;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/aj;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$O90YV5yuhpDPX4eM7KCFk9qR0wU(Landroid/net/Uri;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/aj;->a(Landroid/net/Uri;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$UAAJJFohWf06r0j-20Gzli_Bs3Q(Lcom/pspdfkit/internal/aj;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->d()V

    return-void
.end method

.method public static synthetic $r8$lambda$VFKnA3_5j5byWo45yMcOmculIzc(Lcom/pspdfkit/internal/wi;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/aj;->a(Lcom/pspdfkit/internal/wi;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Z4aenkOqXC5EwPpIvhnfBVsjRY8(Lcom/pspdfkit/internal/aj;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method public static synthetic $r8$lambda$etSE7szV61PIjaeMMebqAwB_03E(Lcom/pspdfkit/internal/wi;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/aj;->a(Lcom/pspdfkit/internal/wi;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iBuOtu7R5sV8zE-4htZhadfjdWg(Lcom/pspdfkit/internal/aj;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->c()V

    return-void
.end method

.method public static synthetic $r8$lambda$ml6mcXQNzaQ-_g6VqFPqDkI2XWo(Lcom/pspdfkit/internal/aj;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/aj;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pNLbmW8IxN--4puaJnT5l7jsTJs(Lcom/pspdfkit/internal/aj;Lcom/pspdfkit/internal/wi;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/aj;->a(Lcom/pspdfkit/internal/wi;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic $r8$lambda$q_Yn8j8yDiRbwaJW8rA7tDtiNF0(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/aj;->b(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$qmuVQmKTlP78cK3tkND5pQjZhUA(Lcom/pspdfkit/internal/aj;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/aj;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/aj;->h:Z

    .line 7
    iput-boolean v0, p0, Lcom/pspdfkit/internal/aj;->i:Z

    const/4 v1, 0x1

    .line 15
    iput v1, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 31
    iput-object p2, p0, Lcom/pspdfkit/internal/aj;->b:Lcom/pspdfkit/document/PdfDocument;

    .line 33
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 35
    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p2, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0x11

    .line 37
    iput v0, p2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 39
    new-instance v1, Lcom/pspdfkit/internal/zu;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/pspdfkit/internal/zu;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    .line 40
    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/zu;->setVideoViewListener(Lcom/pspdfkit/internal/zu$h;)V

    const/4 v2, 0x0

    .line 43
    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 44
    invoke-virtual {p0, v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    new-instance v1, Lcom/pspdfkit/internal/yu;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/yu;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/aj;->c:Lcom/pspdfkit/internal/yu;

    .line 47
    sget p1, Lcom/pspdfkit/R$layout;->pspdf__uvv_on_error_layout:I

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/yu;->setOnErrorView(I)V

    .line 48
    sget p1, Lcom/pspdfkit/R$layout;->pspdf__loading_view:I

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/yu;->setOnLoadingView(I)V

    const/4 p1, 0x4

    .line 49
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 52
    new-instance v1, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    .line 53
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 54
    new-instance v2, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/aj;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 56
    invoke-virtual {p0, v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    new-instance p2, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    .line 59
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 60
    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__uvv_itv_player_play:I

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/aj;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 63
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {p1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 65
    iput v0, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 66
    invoke-virtual {p0, p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/r4;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 6

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    if-eqz v0, :cond_c

    .line 51
    iget v0, p0, Lcom/pspdfkit/internal/aj;->k:I

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v0, v5, :cond_6

    if-eq v0, v2, :cond_5

    if-eq v0, v1, :cond_3

    if-eq v0, v3, :cond_1

    goto :goto_0

    .line 52
    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/aj;->i:Z

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->g()V

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->f()V

    goto :goto_0

    .line 55
    :cond_3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/aj;->i:Z

    if-eqz v0, :cond_4

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->g()V

    .line 57
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->h()V

    goto :goto_0

    .line 58
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->i()V

    .line 59
    iput-boolean v5, p0, Lcom/pspdfkit/internal/aj;->i:Z

    goto :goto_0

    .line 60
    :cond_6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/aj;->i:Z

    if-eqz v0, :cond_7

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->g()V

    .line 62
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/zu;->b(I)V

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->h()V

    .line 82
    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/aj;->k:I

    if-ne v0, v1, :cond_a

    .line 83
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    .line 84
    iget-boolean v1, p0, Lcom/pspdfkit/internal/aj;->h:Z

    if-eqz v1, :cond_b

    .line 85
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 86
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 87
    invoke-virtual {v0}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v5, :cond_9

    if-eq v0, v2, :cond_8

    goto :goto_1

    .line 94
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 95
    :cond_9
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_a
    if-eq v0, v5, :cond_b

    const/high16 v0, -0x1000000

    .line 97
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 98
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 99
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    :cond_b
    :goto_1
    iput v5, p0, Lcom/pspdfkit/internal/aj;->k:I

    :cond_c
    return-void
.end method

.method private static synthetic a(Landroid/net/Uri;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 34
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "Couldn\'t generate preview from: "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.MediaView"

    invoke-static {v0, p0, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x2

    .line 47
    iput p1, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 48
    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/wi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cover mode set to IMAGE but no path specified. Annotation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wi;->d()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.MediaView"

    .line 46
    invoke-static {v1, p0, v0}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/wi;Landroid/net/Uri;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/zu;->setVideoURI(Landroid/net/Uri;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->i()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->c:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/yu;->setTitle(Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->c:Lcom/pspdfkit/internal/yu;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->c:Lcom/pspdfkit/internal/yu;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/zu;->setMediaController(Lcom/pspdfkit/internal/yu;)V

    .line 7
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wi;->c()I

    move-result v0

    invoke-static {v0}, Lcom/pspdfkit/internal/ue;->a(I)I

    move-result v0

    if-eqz v0, :cond_5

    const/4 p2, 0x1

    if-eq v0, p2, :cond_4

    const/4 p1, 0x2

    if-eq v0, p1, :cond_2

    const/4 p1, 0x3

    if-eq v0, p1, :cond_1

    goto :goto_0

    .line 18
    :cond_1
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 19
    iput-boolean p2, p0, Lcom/pspdfkit/internal/aj;->h:Z

    goto :goto_0

    .line 20
    :cond_2
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result p1

    if-nez p1, :cond_3

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 25
    :cond_3
    iput-boolean p2, p0, Lcom/pspdfkit/internal/aj;->h:Z

    goto :goto_0

    .line 26
    :cond_4
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/aj;->setupImageCover(Lcom/pspdfkit/internal/wi;)V

    goto :goto_0

    .line 27
    :cond_5
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/aj;->setupPreviewCover(Landroid/net/Uri;)V

    :goto_0
    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/wi;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 37
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "Couldn\'t load cover for from path. Annotation: "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wi;->d()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p0

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.MediaView"

    .line 41
    invoke-static {v0, p0, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.MediaView"

    const-string v2, "Failed to get playable URI!"

    .line 28
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 33
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private static synthetic b(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x2

    .line 2
    invoke-static {p0, v0}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private b(Landroid/view/View;)V
    .locals 0

    const/4 p1, 0x2

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/internal/aj;->h:Z

    return-void
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/pspdfkit/internal/aj;->h:Z

    return-void
.end method

.method private setupImageCover(Lcom/pspdfkit/internal/wi;)V
    .locals 4

    const/high16 v0, -0x1000000

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/wi;->a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/aj;)V

    .line 4
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 6
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/aj;)V

    .line 7
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    .line 16
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda2;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda2;-><init>(Landroid/widget/ImageView;)V

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda11;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/wi;)V

    new-instance v3, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda12;

    invoke-direct {v3, p1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/wi;)V

    .line 17
    invoke-virtual {v0, v2, v1, v3}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/aj;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private setupPreviewCover(Landroid/net/Uri;)V
    .locals 3

    const/high16 v0, -0x1000000

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda0;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 4
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 5
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/aj;)V

    .line 6
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doFinally(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    .line 15
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda2;

    invoke-direct {v2, v1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda2;-><init>(Landroid/widget/ImageView;)V

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda3;-><init>(Landroid/net/Uri;)V

    .line 16
    invoke-virtual {v0, v2, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/aj;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/zu;->b(I)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    if-eqz v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zu;->getDuration()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    check-cast v0, Lcom/pspdfkit/internal/zi;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/wi;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    check-cast v0, Lcom/pspdfkit/internal/zi;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x0

    .line 6
    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/wi;->a(Z)V

    :cond_1
    :goto_0
    const/high16 v0, -0x1000000

    .line 7
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    if-eqz v1, :cond_0

    .line 2
    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    check-cast v0, Lcom/pspdfkit/internal/zi;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zi;->a(Lcom/pspdfkit/internal/wi;)V

    :cond_0
    const/high16 v0, -0x1000000

    .line 3
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x5

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method public getPosition()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 1

    const/4 v0, 0x4

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x3

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/aj;->k:I

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/aj;->a()V

    return-void
.end method

.method public setMediaContent(Lcom/pspdfkit/internal/wi;)V
    .locals 3

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x0

    .line 4
    iput-object v1, p0, Lcom/pspdfkit/internal/aj;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    invoke-static {v2}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 7
    iput-object v1, p0, Lcom/pspdfkit/internal/aj;->g:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 8
    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/zu;->i()V

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/zu;->setMediaController(Lcom/pspdfkit/internal/yu;)V

    .line 10
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->j:Lcom/pspdfkit/internal/zu;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setAlpha(F)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->c:Lcom/pspdfkit/internal/yu;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->f:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->e:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 16
    iput-boolean v0, p0, Lcom/pspdfkit/internal/aj;->i:Z

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    if-eqz v0, :cond_1

    .line 19
    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    if-eqz v1, :cond_0

    .line 20
    check-cast v1, Lcom/pspdfkit/internal/zi;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/zi;->b(Lcom/pspdfkit/internal/wi;)V

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wi;->b()V

    .line 23
    :cond_1
    iput-object p1, p0, Lcom/pspdfkit/internal/aj;->l:Lcom/pspdfkit/internal/wi;

    if-eqz p1, :cond_2

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/aj;->b:Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/wi;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 30
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/aj;)V

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->doAfterTerminate(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/aj;Lcom/pspdfkit/internal/wi;)V

    new-instance p1, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda8;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/aj$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/aj;)V

    .line 32
    invoke-virtual {v0, v1, p1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/aj;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public setOnMediaPlaybackChangeListener(Lcom/pspdfkit/internal/aj$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/aj;->m:Lcom/pspdfkit/internal/aj$a;

    return-void
.end method

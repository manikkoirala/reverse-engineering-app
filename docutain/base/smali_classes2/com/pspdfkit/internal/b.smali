.class public final Lcom/pspdfkit/internal/b;
.super Lcom/pspdfkit/internal/ot;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/ot;-><init>()V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;)I
    .locals 0

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->a()I

    move-result p0

    return p0
.end method

.method public static a(Lcom/pspdfkit/internal/mb;[I)I
    .locals 2

    .line 6
    array-length v0, p1

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0, v1}, Lcom/pspdfkit/internal/mb;->a(III)V

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/mb;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mb;->b()I

    move-result p0

    return p0
.end method

.method public static a(Lcom/pspdfkit/internal/mb;I)V
    .locals 1

    const/4 v0, 0x3

    .line 5
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/mb;->b(II)V

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/mb;S)V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/mb;->a(IS)V

    return-void
.end method

.method public static b(Lcom/pspdfkit/internal/mb;)V
    .locals 1

    const/4 v0, 0x5

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/mb;->e(I)V

    return-void
.end method

.method public static b(Lcom/pspdfkit/internal/mb;I)V
    .locals 1

    const/4 v0, 0x4

    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/pspdfkit/internal/mb;->b(II)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0xc

    .line 3
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->e(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/b;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/ot;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method public final a(Lcom/pspdfkit/internal/ot;)Lcom/pspdfkit/internal/ot;
    .locals 1

    const/16 v0, 0xa

    .line 2
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/ot;->a(Lcom/pspdfkit/internal/ot;I)Lcom/pspdfkit/internal/ot;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final b()S
    .locals 3

    const/4 v0, 0x4

    .line 1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/pspdfkit/internal/ot;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f(I)Lcom/pspdfkit/internal/b;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/b;-><init>()V

    const/16 v1, 0xc

    .line 2
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->b(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/ot;->d(I)I

    move-result v1

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ot;->a(I)I

    move-result p1

    iget-object v1, p0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    .line 3
    iput p1, v0, Lcom/pspdfkit/internal/ot;->a:I

    .line 4
    iput-object v1, v0, Lcom/pspdfkit/internal/ot;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

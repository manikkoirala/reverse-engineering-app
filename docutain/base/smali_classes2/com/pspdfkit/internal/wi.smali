.class public final Lcom/pspdfkit/internal/wi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/annotations/Annotation;

.field private final b:I

.field private final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Z

.field private final f:Z

.field private final g:I

.field private final h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public static synthetic $r8$lambda$1GsJm4rd-jcxROpIH4OrDrZJFyA(Lcom/pspdfkit/internal/wi;Landroid/content/Context;)Landroid/net/Uri;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wi;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$ekAwemBFeft5ZGat3C8G147m6x4(Lcom/pspdfkit/internal/wi;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/wi;->h()V

    return-void
.end method

.method public static synthetic $r8$lambda$jjVlcEjUOeGVqjEIBf4XCIisDrs(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/LinkAnnotation;)Landroid/net/Uri;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/wi;->a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/LinkAnnotation;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method private constructor <init>(Lcom/pspdfkit/annotations/Annotation;IZZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/wi;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 9
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/pspdfkit/internal/wi;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 33
    iput-boolean v1, p0, Lcom/pspdfkit/internal/wi;->i:Z

    .line 54
    iput-object p1, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    .line 55
    iput p2, p0, Lcom/pspdfkit/internal/wi;->b:I

    .line 56
    iput-boolean p3, p0, Lcom/pspdfkit/internal/wi;->e:Z

    .line 57
    iput-boolean p4, p0, Lcom/pspdfkit/internal/wi;->f:Z

    .line 59
    invoke-static {p5}, Lcom/pspdfkit/internal/wi;->a(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/wi;->g:I

    .line 60
    iput-object p6, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    const-string v0, "preview"

    .line 21
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const-string v0, "image"

    .line 23
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p0, 0x2

    return p0

    :cond_1
    const-string v0, "clear"

    .line 25
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x3

    return p0

    :cond_2
    const-string v0, "none"

    .line 27
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    const/4 p0, 0x4

    return p0
.end method

.method private static a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/annotations/LinkAnnotation;)Landroid/net/Uri;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 45
    instance-of v0, p2, Lcom/pspdfkit/annotations/MediaAnnotation;

    if-eqz v0, :cond_0

    .line 46
    check-cast p2, Lcom/pspdfkit/annotations/MediaAnnotation;

    invoke-virtual {p2, p0, p1}, Lcom/pspdfkit/annotations/AssetAnnotation;->getFileUri(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Landroid/net/Uri;

    move-result-object p0

    return-object p0

    .line 47
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object p1

    .line 48
    instance-of p2, p1, Lcom/pspdfkit/annotations/actions/UriAction;

    if-eqz p2, :cond_1

    .line 49
    check-cast p1, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 51
    invoke-static {p1}, Lcom/pspdfkit/media/MediaUri;->parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object p2

    sget-object v0, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    if-ne p2, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 53
    invoke-virtual {p1, p0}, Lcom/pspdfkit/media/MediaUri;->getFileUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_1

    :cond_2
    sget-object p0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_1
    return-object p0
.end method

.method public static a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/wi;
    .locals 9

    .line 1
    instance-of v0, p0, Lcom/pspdfkit/annotations/MediaAnnotation;

    if-eqz v0, :cond_0

    .line 2
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/MediaAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/MediaAnnotation;->getMediaOptions()Ljava/util/EnumSet;

    move-result-object v0

    .line 3
    new-instance v8, Lcom/pspdfkit/internal/wi;

    sget-object v1, Lcom/pspdfkit/annotations/actions/MediaOptions;->AUTO_PLAY:Lcom/pspdfkit/annotations/actions/MediaOptions;

    .line 6
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v4

    sget-object v1, Lcom/pspdfkit/annotations/actions/MediaOptions;->CONTROLS_ENABLED:Lcom/pspdfkit/annotations/actions/MediaOptions;

    .line 7
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v5

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/wi;-><init>(Lcom/pspdfkit/annotations/Annotation;IZZLjava/lang/String;Ljava/lang/String;)V

    return-object v8

    .line 11
    :cond_0
    instance-of v0, p0, Lcom/pspdfkit/annotations/LinkAnnotation;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 12
    move-object v0, p0

    check-cast v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    .line 14
    instance-of v2, v0, Lcom/pspdfkit/annotations/actions/UriAction;

    if-eqz v2, :cond_1

    .line 15
    check-cast v0, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 17
    invoke-static {v0}, Lcom/pspdfkit/media/MediaUri;->parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/media/MediaUri;->getVideoSettingsFromOptions()Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;

    move-result-object v0

    .line 20
    new-instance v8, Lcom/pspdfkit/internal/wi;

    iget-boolean v4, v0, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->autoplay:Z

    iget-object v6, v0, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->coverMode:Ljava/lang/String;

    iget-object v7, v0, Lcom/pspdfkit/media/MediaLinkUtils$VideoSettings;->coverImage:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v5, 0x1

    move-object v1, v8

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/pspdfkit/internal/wi;-><init>(Lcom/pspdfkit/annotations/Annotation;IZZLjava/lang/String;Ljava/lang/String;)V

    return-object v8

    :cond_2
    return-object v1
.end method

.method private synthetic b(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 11
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    const-string v1, "file:///android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "localhost/"

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 25
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    :goto_0
    return-object p1

    .line 26
    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/pspdfkit/media/AssetsContentProvider;->getAuthority(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    const-string v3, ""

    .line 29
    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method private synthetic h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/wi;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 58
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/wi;Landroid/content/Context;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public final a(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/document/PdfDocument;",
            ")",
            "Lio/reactivex/rxjava3/core/Single<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 29
    const-class v1, Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;)V

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/wi;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 38
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda2;

    invoke-direct {v0, p2}, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda2;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Single;->doOnSuccess(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda3;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/wi$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/wi;)V

    .line 39
    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->doAfterTerminate(Lio/reactivex/rxjava3/functions/Action;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    .line 44
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->io()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method public final a(Z)V
    .locals 0

    .line 59
    iput-boolean p1, p0, Lcom/pspdfkit/internal/wi;->i:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/pspdfkit/internal/wi;->e:Z

    return v0
.end method

.method public final b()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/wi;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 7
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Deleting temporary media file for annotation: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "PSPDFKit.MediaContent"

    invoke-static {v4, v0, v3}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/wi;->g:I

    return v0
.end method

.method public final d()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wi;->a:Lcom/pspdfkit/annotations/Annotation;

    instance-of v1, v0, Lcom/pspdfkit/annotations/MediaAnnotation;

    if-eqz v1, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/annotations/MediaAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/AssetAnnotation;->getAssetName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3
    :cond_0
    instance-of v1, v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    if-eqz v1, :cond_2

    .line 4
    check-cast v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    .line 6
    instance-of v1, v0, Lcom/pspdfkit/annotations/actions/UriAction;

    if-eqz v1, :cond_1

    .line 7
    check-cast v0, Lcom/pspdfkit/annotations/actions/UriAction;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/UriAction;->getUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 9
    invoke-static {v0}, Lcom/pspdfkit/media/MediaUri;->parse(Ljava/lang/String;)Lcom/pspdfkit/media/MediaUri;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/media/MediaUri;->getType()Lcom/pspdfkit/media/MediaUri$UriType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/media/MediaUri$UriType;->MEDIA:Lcom/pspdfkit/media/MediaUri$UriType;

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/media/MediaUri;->getParsedUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v0, ""

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/wi;->i:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/wi;->f:Z

    return v0
.end method

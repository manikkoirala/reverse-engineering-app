.class public final Lcom/pspdfkit/internal/w5;
.super Lcom/pspdfkit/internal/k4;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/k4<",
        "Lcom/pspdfkit/internal/v5;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/internal/uu;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/uu;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/k4;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/w5;->c:Lcom/pspdfkit/internal/uu;

    return-void
.end method


# virtual methods
.method public final c(Lcom/pspdfkit/internal/ja;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/v5;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ja;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/w5;->c:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/tu;->c(Lcom/pspdfkit/internal/ja;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final d(Lcom/pspdfkit/internal/ja;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/v5;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ja;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/w5;->c:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/tu;->d(Lcom/pspdfkit/internal/ja;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final f(Lcom/pspdfkit/internal/ja;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/RedoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/v5;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ja;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/w5;->c:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v1

    .line 4
    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/tu;->a(Lcom/pspdfkit/internal/ja;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final g(Lcom/pspdfkit/internal/ja;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/v5;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/v5;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ja;

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/w5;->c:Lcom/pspdfkit/internal/uu;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/uu;->a(Lcom/pspdfkit/internal/ja;)Lcom/pspdfkit/internal/tu;

    move-result-object v2

    .line 5
    invoke-interface {v2, v1}, Lcom/pspdfkit/internal/tu;->b(Lcom/pspdfkit/internal/ja;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/oc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/oc$a;
    }
.end annotation


# direct methods
.method public static final a(FF)F
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    .line 336
    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float v0, v0, v1

    const/4 v1, 0x2

    int-to-float v1, v1

    div-float/2addr p0, v1

    add-float/2addr p0, v0

    add-float/2addr p0, p1

    mul-float p0, p0, v1

    return p0
.end method

.method public static final a(Lcom/pspdfkit/annotations/FreeTextAnnotation;)F
    .locals 2

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 334
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result p0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 335
    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float v0, v0, v1

    const/4 v1, 0x2

    int-to-float v1, v1

    div-float/2addr p0, v1

    add-float/2addr p0, v0

    return p0
.end method

.method public static final a(Lcom/pspdfkit/annotations/FreeTextAnnotation;FLandroid/text/TextPaint;)Lcom/pspdfkit/utils/Size;
    .locals 18

    move/from16 v0, p1

    const-string v1, "<this>"

    move-object/from16 v2, p0

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    const-string v3, "textInsets"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v4

    neg-int v4, v4

    invoke-static {v1, v3, v4}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    .line 148
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v5

    const/high16 v6, 0x3fc00000    # 1.5f

    mul-float v5, v5, v6

    const/4 v6, 0x1

    if-eqz p2, :cond_0

    move-object/from16 v11, p2

    goto :goto_1

    .line 153
    :cond_0
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7}, Landroid/text/TextPaint;-><init>()V

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getFontName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 155
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v9

    const-string v10, "getSystemFontManager()"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v9, v8}, Lcom/pspdfkit/internal/mt;->getFontByName(Ljava/lang/String;)Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 157
    invoke-virtual {v8}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 158
    invoke-virtual {v8}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0

    .line 160
    :cond_1
    invoke-virtual {v9}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v8

    invoke-virtual {v8}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {v8}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 163
    :cond_2
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextSize()F

    move-result v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 166
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move-object v11, v7

    .line 167
    :goto_1
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x1c

    const/4 v15, 0x0

    if-lt v7, v8, :cond_3

    const/4 v7, 0x1

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    :goto_2
    if-eqz v7, :cond_5

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    const-string v7, ""

    :cond_4
    float-to-int v0, v0

    .line 172
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 173
    invoke-static {v7, v11, v0}, Landroid/text/DynamicLayout$Builder;->obtain(Ljava/lang/CharSequence;Landroid/text/TextPaint;I)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 180
    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v7}, Landroid/text/DynamicLayout$Builder;->setAlignment(Landroid/text/Layout$Alignment;)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 181
    sget-object v7, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, v7}, Landroid/text/DynamicLayout$Builder;->setTextDirection(Landroid/text/TextDirectionHeuristic;)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    const/4 v7, 0x0

    .line 182
    invoke-virtual {v0, v7, v4}, Landroid/text/DynamicLayout$Builder;->setLineSpacing(FF)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 183
    invoke-virtual {v0, v15}, Landroid/text/DynamicLayout$Builder;->setIncludePad(Z)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 184
    invoke-virtual {v0, v6}, Landroid/text/DynamicLayout$Builder;->setUseLineSpacingFromFallbacks(Z)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 185
    invoke-virtual {v0, v15}, Landroid/text/DynamicLayout$Builder;->setBreakStrategy(I)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 186
    invoke-virtual {v0, v6}, Landroid/text/DynamicLayout$Builder;->setHyphenationFrequency(I)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 187
    invoke-virtual {v0, v15}, Landroid/text/DynamicLayout$Builder;->setJustificationMode(I)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    const/4 v4, 0x0

    .line 188
    invoke-virtual {v0, v4}, Landroid/text/DynamicLayout$Builder;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)Landroid/text/DynamicLayout$Builder;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Landroid/text/DynamicLayout$Builder;->build()Landroid/text/DynamicLayout;

    move-result-object v0

    const-string v4, "{\n        @SuppressLint(\u2026    builder.build()\n    }"

    .line 190
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    goto :goto_3

    .line 211
    :cond_5
    new-instance v4, Landroid/text/StaticLayout;

    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getContents()Ljava/lang/String;

    move-result-object v10

    float-to-int v0, v0

    .line 216
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 217
    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/16 v16, 0x0

    move-object v9, v4

    const/4 v7, 0x0

    move v15, v0

    .line 218
    invoke-direct/range {v9 .. v16}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object v0, v4

    .line 231
    :goto_3
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    sub-int/2addr v4, v6

    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v4

    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineTop(I)I

    move-result v8

    sub-int/2addr v4, v8

    .line 232
    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineRight(I)F

    move-result v8

    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v7

    sub-float/2addr v8, v7

    .line 233
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v7

    :goto_4
    if-ge v6, v7, :cond_6

    .line 234
    invoke-virtual {v0, v6}, Landroid/text/Layout;->getLineRight(I)F

    move-result v9

    invoke-virtual {v0, v6}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v10

    sub-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 242
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v0

    sget-object v6, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v0, v6, :cond_8

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    const/16 v6, 0x5a

    if-eq v0, v6, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v0

    const/16 v2, 0x10e

    if-ne v0, v2, :cond_8

    :cond_7
    int-to-float v0, v4

    move/from16 v17, v8

    move v8, v0

    move/from16 v0, v17

    goto :goto_5

    :cond_8
    int-to-float v0, v4

    .line 253
    :goto_5
    iget v2, v1, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    iget v4, v1, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    add-float/2addr v2, v4

    add-float/2addr v2, v8

    const/4 v4, 0x2

    int-to-float v4, v4

    mul-float v5, v5, v4

    add-float/2addr v2, v5

    mul-float v4, v4, v3

    add-float/2addr v4, v2

    .line 254
    iget v2, v1, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    iget v1, v1, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    add-float/2addr v2, v1

    add-float/2addr v2, v0

    add-float/2addr v2, v5

    add-float/2addr v2, v3

    .line 255
    new-instance v0, Lcom/pspdfkit/utils/Size;

    invoke-direct {v0, v4, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    return-object v0
.end method

.method public static final a(Lcom/pspdfkit/annotations/FreeTextAnnotation;I)V
    .locals 14

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getCallOutPoints()Ljava/util/List;

    move-result-object v0

    const-string v1, "callOutPoints"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v1

    const-string v2, "boundingBox"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v2

    const-string v3, "textInsets"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v3

    neg-int v3, v3

    invoke-static {v2, p1, v3}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v2

    .line 265
    new-instance v3, Landroid/graphics/RectF;

    .line 266
    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v2, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    add-float/2addr v4, v5

    .line 267
    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v6, v2, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    sub-float/2addr v5, v6

    .line 268
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget v7, v2, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    sub-float/2addr v6, v7

    .line 269
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    add-float/2addr v1, v2

    .line 270
    invoke-direct {v3, v4, v5, v6, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 277
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x0

    .line 278
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 282
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x2

    if-ne v4, v5, :cond_1

    .line 283
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 284
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    const-string v6, "points[2]"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroid/graphics/PointF;

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 288
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    const-string v6, "points[1]"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroid/graphics/PointF;

    .line 291
    :goto_0
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v8, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v8

    .line 292
    iget v8, v2, Landroid/graphics/PointF;->y:F

    iget v9, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    float-to-double v8, v8

    float-to-double v10, v6

    .line 293
    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    const/16 v6, 0x168

    int-to-double v10, v6

    add-double/2addr v8, v10

    rem-double/2addr v8, v10

    const-wide v10, 0x4046800000000000L    # 45.0

    const-wide v12, 0x4060e00000000000L    # 135.0

    cmpl-double v6, v8, v10

    if-lez v6, :cond_2

    cmpg-double v6, v8, v12

    if-gtz v6, :cond_2

    .line 297
    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v6}, Landroid/graphics/PointF;->set(FF)V

    if-eqz v4, :cond_5

    .line 299
    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, v2, Landroid/graphics/PointF;->y:F

    sub-float v6, v5, v6

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-virtual {v4, v1, v5}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    :cond_2
    const-wide v10, 0x406c200000000000L    # 225.0

    cmpl-double v6, v8, v12

    if-lez v6, :cond_3

    cmpg-double v6, v8, v10

    if-gtz v6, :cond_3

    .line 302
    iget v6, v3, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v1}, Landroid/graphics/PointF;->set(FF)V

    if-eqz v4, :cond_5

    .line 303
    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v6, v2, Landroid/graphics/PointF;->x:F

    sub-float v6, v1, v6

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v1, v6

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v1, v5}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    :cond_3
    cmpl-double v6, v8, v10

    if-lez v6, :cond_4

    const-wide v10, 0x4073b00000000000L    # 315.0

    cmpg-double v6, v8, v10

    if-gtz v6, :cond_4

    .line 306
    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v5, v1, v6}, Landroid/graphics/PointF;->set(FF)V

    if-eqz v4, :cond_5

    .line 307
    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, v2, Landroid/graphics/PointF;->y:F

    sub-float v6, v5, v6

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-virtual {v4, v1, v5}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    .line 310
    :cond_4
    iget v6, v3, Landroid/graphics/RectF;->right:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v1}, Landroid/graphics/PointF;->set(FF)V

    if-eqz v4, :cond_5

    .line 311
    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v6, v2, Landroid/graphics/PointF;->x:F

    sub-float v6, v1, v6

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v1, v6

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v1, v5}, Landroid/graphics/PointF;->set(FF)V

    :cond_5
    :goto_1
    const/16 v1, 0xf

    int-to-float v1, v1

    .line 315
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v4, v4, v5

    add-float/2addr v4, v1

    .line 318
    iget v1, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v4

    iget v5, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v5

    const/4 v5, 0x0

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 319
    iget v6, v3, Landroid/graphics/RectF;->left:F

    iget v7, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v4

    sub-float/2addr v6, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 320
    iget v7, v3, Landroid/graphics/RectF;->bottom:F

    iget v8, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v4

    sub-float/2addr v7, v8

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 321
    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v4

    iget v4, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v4

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 323
    new-instance v4, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-direct {v4, v1, v6, v7, v2}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    .line 326
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    iget v2, v4, Lcom/pspdfkit/utils/EdgeInsets;->bottom:F

    sub-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/RectF;->bottom:F

    .line 327
    iget v1, v3, Landroid/graphics/RectF;->top:F

    iget v2, v4, Lcom/pspdfkit/utils/EdgeInsets;->top:F

    add-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/RectF;->top:F

    .line 328
    iget v1, v3, Landroid/graphics/RectF;->left:F

    iget v2, v4, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    sub-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/RectF;->left:F

    .line 329
    iget v1, v3, Landroid/graphics/RectF;->right:F

    iget v2, v4, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    add-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/RectF;->right:F

    .line 331
    invoke-virtual {p0, v3}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    neg-int p1, p1

    .line 332
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v1

    invoke-static {v4, p1, v1}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setTextInsets(Lcom/pspdfkit/utils/EdgeInsets;)V

    .line 333
    invoke-virtual {p0, v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->setCallOutPoints(Ljava/util/List;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;Lcom/pspdfkit/utils/Size;Landroid/text/TextPaint;)V
    .locals 3

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationConfigurationRegistry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    const-class v1, Lcom/pspdfkit/annotations/configuration/FreeTextAnnotationConfiguration;

    invoke-interface {p1, v0, v1}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/annotations/AnnotationType;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/FreeTextAnnotationConfiguration;

    .line 3
    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->FIXED:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v1, v2, :cond_1

    .line 9
    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    :cond_0
    move-object p1, v0

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_0

    .line 12
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration;->isHorizontalResizingEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 13
    sget-object v1, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 15
    :goto_0
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/AnnotationTextResizingConfiguration;->isVerticalResizingEnabled()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 16
    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->SCALE:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    :cond_3
    move-object p1, v0

    move-object v0, v1

    .line 20
    :goto_1
    invoke-static {p0, p2, v0, p1, p3}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V

    .line 27
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object p1

    if-ne p1, v2, :cond_4

    .line 29
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result p1

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;I)V

    :cond_4
    return-void
.end method

.method public static final a(Lcom/pspdfkit/annotations/FreeTextAnnotation;Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;Landroid/text/TextPaint;)V
    .locals 7

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSize"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widthScaleMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "heightScaleMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;->FIXED:Lcom/pspdfkit/utils/FreeTextAnnotationUtils$ScaleMode;

    if-ne p2, v0, :cond_0

    if-ne p3, v0, :cond_0

    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getTextInsets()Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    const-string v2, "textInsets"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v3

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/ia;->a(Lcom/pspdfkit/utils/EdgeInsets;II)Lcom/pspdfkit/utils/EdgeInsets;

    move-result-object v1

    .line 36
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object v2

    const-string v3, "boundingBox"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    .line 38
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float v4, v4, v5

    .line 41
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v5

    const/16 v6, 0x5a

    if-eq v5, v6, :cond_2

    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getRotation()I

    move-result v5

    const/16 v6, 0x10e

    if-ne v5, v6, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    iget v5, p1, Lcom/pspdfkit/utils/Size;->width:F

    goto :goto_1

    .line 45
    :cond_2
    :goto_0
    iget v5, p1, Lcom/pspdfkit/utils/Size;->height:F

    :goto_1
    const/4 v6, 0x2

    if-ne p2, v0, :cond_3

    .line 52
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v5, v1, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    sub-float/2addr v0, v5

    iget v1, v1, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    sub-float/2addr v0, v1

    int-to-float v1, v6

    mul-float v4, v4, v1

    sub-float/2addr v0, v4

    sub-float/2addr v0, v3

    goto :goto_2

    .line 54
    :cond_3
    iget v0, v1, Lcom/pspdfkit/utils/EdgeInsets;->left:F

    sub-float/2addr v5, v0

    iget v0, v1, Lcom/pspdfkit/utils/EdgeInsets;->right:F

    sub-float/2addr v5, v0

    int-to-float v0, v6

    mul-float v4, v4, v0

    sub-float/2addr v5, v4

    mul-float v3, v3, v0

    sub-float v0, v5, v3

    .line 58
    :goto_2
    invoke-static {p0, v0, p4}, Lcom/pspdfkit/internal/oc;->a(Lcom/pspdfkit/annotations/FreeTextAnnotation;FLandroid/text/TextPaint;)Lcom/pspdfkit/utils/Size;

    move-result-object p4

    .line 59
    iget v0, p4, Lcom/pspdfkit/utils/Size;->width:F

    .line 60
    iget p4, p4, Lcom/pspdfkit/utils/Size;->height:F

    .line 62
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 63
    sget-object v3, Lcom/pspdfkit/internal/oc$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v3, p2

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-eq p2, v5, :cond_6

    if-eq p2, v6, :cond_5

    if-ne p2, v4, :cond_4

    goto :goto_3

    .line 71
    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 72
    :cond_5
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_3

    :cond_6
    move v0, v1

    .line 73
    :goto_3
    iget p2, p1, Lcom/pspdfkit/utils/Size;->width:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 76
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 77
    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    move-result p3

    aget p3, v3, p3

    if-eq p3, v5, :cond_9

    if-eq p3, v6, :cond_8

    if-ne p3, v4, :cond_7

    goto :goto_4

    .line 85
    :cond_7
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 86
    :cond_8
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p3

    invoke-static {p4, p3}, Ljava/lang/Math;->max(FF)F

    move-result p4

    goto :goto_4

    :cond_9
    move p4, v0

    .line 87
    :goto_4
    iget p3, p1, Lcom/pspdfkit/utils/Size;->height:F

    invoke-static {p3, p4}, Ljava/lang/Math;->min(FF)F

    move-result p3

    .line 90
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object p4

    sget-object v0, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    const/4 v1, 0x0

    if-ne p4, v0, :cond_c

    .line 91
    iget p4, v2, Landroid/graphics/RectF;->left:F

    add-float v0, p4, p2

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    cmpl-float p1, v0, p1

    if-lez p1, :cond_a

    .line 93
    iget v0, v2, Landroid/graphics/RectF;->right:F

    sub-float p4, v0, p2

    .line 101
    :cond_a
    iget p1, v2, Landroid/graphics/RectF;->top:F

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p2

    sub-float/2addr p1, p2

    cmpg-float p1, p1, v1

    if-gez p1, :cond_b

    .line 103
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto :goto_5

    .line 107
    :cond_b
    iget p1, v2, Landroid/graphics/RectF;->top:F

    .line 108
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p2

    sub-float v1, p1, p2

    .line 111
    :goto_5
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2, p4, p1, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 112
    invoke-virtual {p0, p2}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    goto :goto_7

    .line 113
    :cond_c
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p4

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, p3, p2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 p2, 0x0

    invoke-interface {p4, v0, p2}, Lcom/pspdfkit/internal/pf;->setContentSize(Landroid/graphics/RectF;Z)V

    .line 114
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->adjustBoundsForRotation()V

    .line 115
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p2

    const-string p3, "this.boundingBox"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result p3

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    .line 118
    iget p4, v2, Landroid/graphics/RectF;->left:F

    add-float v0, p4, p3

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    cmpl-float p1, v0, p1

    if-lez p1, :cond_d

    .line 120
    iget v0, v2, Landroid/graphics/RectF;->right:F

    sub-float p4, v0, p3

    .line 128
    :cond_d
    iget p1, v2, Landroid/graphics/RectF;->top:F

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p3

    sub-float/2addr p1, p3

    cmpg-float p1, p1, v1

    if-gez p1, :cond_e

    .line 130
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const/4 p2, 0x0

    goto :goto_6

    .line 134
    :cond_e
    iget p1, v2, Landroid/graphics/RectF;->top:F

    .line 135
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    sub-float p2, p1, p2

    .line 138
    :goto_6
    new-instance p3, Landroid/graphics/RectF;

    invoke-direct {p3, p4, p1, v0, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 139
    iget p1, p3, Landroid/graphics/RectF;->left:F

    cmpg-float p2, p1, v1

    if-gez p2, :cond_f

    neg-float p2, p1

    add-float/2addr p1, p2

    .line 141
    iput p1, p3, Landroid/graphics/RectF;->left:F

    .line 142
    iget p1, p3, Landroid/graphics/RectF;->right:F

    add-float/2addr p1, p2

    iput p1, p3, Landroid/graphics/RectF;->right:F

    .line 145
    :cond_f
    invoke-virtual {p0, p3}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    :goto_7
    return-void
.end method

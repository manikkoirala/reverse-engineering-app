.class public final Lcom/pspdfkit/internal/e$b;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/pspdfkit/internal/jd;

.field final synthetic b:Lcom/pspdfkit/internal/e;


# direct methods
.method public static synthetic $r8$lambda$-BUa7ungQhEHqUvhP9TA9whh8Nk(Lcom/pspdfkit/internal/e$b;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/e$b;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$u6npuy01T95oVEfP2h2VGPDyxnY(Lcom/pspdfkit/internal/e$b;Landroid/view/View;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/e$b;->b(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/e$b;)Lcom/pspdfkit/internal/jd;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/e$b;->a:Lcom/pspdfkit/internal/jd;

    return-object p0
.end method

.method constructor <init>(Lcom/pspdfkit/internal/e;Lcom/pspdfkit/internal/jd;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    .line 2
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/e$b;->a:Lcom/pspdfkit/internal/jd;

    .line 5
    invoke-static {p1}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetd(Lcom/pspdfkit/internal/e;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/jd;->setLabelTextColor(I)V

    .line 6
    invoke-static {p1}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/e;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__circle_shape:I

    invoke-static {p1}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetc(Lcom/pspdfkit/internal/e;)I

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 9
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/jd;->setIconBackground(Landroid/graphics/drawable/Drawable;)V

    .line 11
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/jd;->setIconPadding(I)V

    .line 14
    :cond_0
    new-instance p1, Lcom/pspdfkit/internal/e$b$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/e$b$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/e$b;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    new-instance p1, Lcom/pspdfkit/internal/e$b$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/e$b$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/e$b;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {p1}, Lcom/pspdfkit/internal/e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e;)Lcom/pspdfkit/internal/e$a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    if-ltz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetb(Lcom/pspdfkit/internal/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e;)Lcom/pspdfkit/internal/e$a;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetb(Lcom/pspdfkit/internal/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;

    check-cast v1, Lcom/pspdfkit/internal/g;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/g;->a(Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)V

    :cond_0
    return-void
.end method

.method private synthetic b(Landroid/view/View;)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {p1}, Lcom/pspdfkit/internal/e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e;)Lcom/pspdfkit/internal/e$a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    if-ltz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetb(Lcom/pspdfkit/internal/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/e$b;->b:Lcom/pspdfkit/internal/e;

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgeta(Lcom/pspdfkit/internal/e;)Lcom/pspdfkit/internal/e$a;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/e;->-$$Nest$fgetb(Lcom/pspdfkit/internal/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;

    check-cast v1, Lcom/pspdfkit/internal/g;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/g;->b(Lcom/pspdfkit/ui/actionmenu/ActionMenuItem;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.class public final Lcom/pspdfkit/internal/lq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroidx/fragment/app/FragmentManager;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/pspdfkit/internal/ne;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/pspdfkit/internal/ne;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/lq;->a:Landroidx/fragment/app/FragmentManager;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/lq;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/lq;->c:Lcom/pspdfkit/internal/ne;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lq;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/lq;->b:Ljava/lang/String;

    sget v2, Lcom/pspdfkit/internal/me;->d:I

    const-string v2, "fragmentManager"

    .line 2
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentTag"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    const-string v3, "removeFragment() may only be called from the main thread."

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/kc;->a(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;)V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lq;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/lq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/me;

    if-eqz v0, :cond_0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/lq;->c:Lcom/pspdfkit/internal/ne;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/me;->a(Lcom/pspdfkit/internal/ne;)V

    :cond_0
    return-void
.end method

.method public final c()Lcom/pspdfkit/internal/me;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/lq;->a:Landroidx/fragment/app/FragmentManager;

    iget-object v1, p0, Lcom/pspdfkit/internal/lq;->b:Ljava/lang/String;

    sget v2, Lcom/pspdfkit/internal/me;->d:I

    .line 2
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/me;

    if-nez v2, :cond_0

    .line 4
    new-instance v2, Lcom/pspdfkit/internal/me;

    invoke-direct {v2}, Lcom/pspdfkit/internal/me;-><init>()V

    const-string v3, "fragmentManager"

    .line 5
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragment"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragmentTag"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    const-string v4, "addFragment() may only be called from the main thread."

    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 63
    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "fragmentManager.beginTra\u2026dd(fragment, fragmentTag)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/lq;->c:Lcom/pspdfkit/internal/ne;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/me;->a(Lcom/pspdfkit/internal/ne;)V

    return-object v2
.end method

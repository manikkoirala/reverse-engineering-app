.class public final Lcom/pspdfkit/internal/io;
.super Lcom/pspdfkit/internal/gt;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/gt;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final a(ILjava/nio/ByteBuffer;)Lcom/pspdfkit/internal/io;
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/gt;->a:I

    iput-object p2, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method public final b()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final c()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x18

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final d()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x1c

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final e()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final f()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final g()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final h()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gt;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/pspdfkit/internal/gt;->a:I

    add-int/lit8 v1, v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    return v0
.end method

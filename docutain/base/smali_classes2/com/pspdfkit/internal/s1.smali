.class public final Lcom/pspdfkit/internal/s1;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/m2$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/s1$a;,
        Lcom/pspdfkit/internal/s1$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/s1$b;",
        ">;",
        "Lcom/pspdfkit/internal/m2$a;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/s1$a;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Ljava/util/ArrayList;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Lcom/pspdfkit/configuration/PdfConfiguration;

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public static synthetic $r8$lambda$UKPxzpbGaK7oMYqleXF-hjFDUCM(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/s1;->c(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$UyIwN97EkfqACNwShMUw931vXvg(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/s1;->a(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YEzoAK6TW-hU-TLegpN28D-wvLY(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/s1;->b(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/s1$a;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    .line 7
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/s1;->c:Landroid/view/LayoutInflater;

    .line 10
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    const/high16 p1, -0x1000000

    .line 13
    iput p1, p0, Lcom/pspdfkit/internal/s1;->e:I

    .line 15
    iput p1, p0, Lcom/pspdfkit/internal/s1;->f:I

    const/4 p2, -0x1

    .line 17
    iput p2, p0, Lcom/pspdfkit/internal/s1;->g:I

    .line 19
    iput p1, p0, Lcom/pspdfkit/internal/s1;->h:I

    const/4 p1, 0x1

    .line 52
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/s1$b$a;Lcom/pspdfkit/internal/mh;)V
    .locals 9

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/mh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->d()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->d()Landroid/widget/TextView;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/internal/s1;->e:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 19
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->d()Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/pspdfkit/internal/ov;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    goto :goto_0

    :cond_0
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 20
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->d()Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/mh;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->c()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->c()Landroid/widget/TextView;

    move-result-object v1

    iget v4, p0, Lcom/pspdfkit/internal/s1;->f:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 25
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->c()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    iget v1, p0, Lcom/pspdfkit/internal/s1;->e:I

    invoke-virtual {p2, v0, v1}, Lcom/pspdfkit/internal/mh;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 28
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->b()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->b()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    const/16 v0, 0x8

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 32
    iget-object v1, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    iget v4, p0, Lcom/pspdfkit/internal/s1;->i:I

    iget v5, p0, Lcom/pspdfkit/internal/s1;->h:I

    invoke-static {v1, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 33
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->e()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->e()Landroid/widget/ImageView;

    move-result-object v1

    iget-boolean v4, p0, Lcom/pspdfkit/internal/s1;->m:Z

    if-eqz v4, :cond_7

    if-eqz v0, :cond_7

    .line 35
    invoke-virtual {p2}, Lcom/pspdfkit/internal/mh;->e()I

    move-result v4

    .line 36
    iget-object v5, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    .line 37
    const-class v6, Lcom/pspdfkit/internal/mh$a;

    invoke-static {v5, v6}, Lkotlin/collections/CollectionsKt;->filterIsInstance(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v5

    .line 130
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 131
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Lcom/pspdfkit/internal/mh$a;

    .line 132
    invoke-virtual {v8}, Lcom/pspdfkit/internal/mh$a;->e()I

    move-result v8

    if-ne v8, v4, :cond_5

    const/4 v8, 0x1

    goto :goto_5

    :cond_5
    const/4 v8, 0x0

    :goto_5
    if-eqz v8, :cond_4

    .line 225
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 226
    :cond_6
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {p2, v0, v4}, Lcom/pspdfkit/internal/mh;->a(Lcom/pspdfkit/configuration/PdfConfiguration;I)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    goto :goto_6

    :cond_7
    const/16 v4, 0x8

    .line 227
    :goto_6
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 235
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v4, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0, p2}, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v4, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda1;

    invoke-direct {v4, p0, p2}, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 244
    iget-boolean v1, p0, Lcom/pspdfkit/internal/s1;->m:Z

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/mh;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 245
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->a()Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/s1$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->a()Landroid/widget/ImageButton;

    move-result-object p2

    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 247
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->a()Landroid/widget/ImageButton;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/s1;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 249
    :cond_8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$a;->a()Landroid/widget/ImageButton;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_7
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$item"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-object p0, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/s1$a;->c(Lcom/pspdfkit/internal/mh;)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)Z
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$item"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object p0, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/s1$a;->b(Lcom/pspdfkit/internal/mh;)V

    const/4 p0, 0x1

    return p0
.end method

.method private static final c(Lcom/pspdfkit/internal/s1;Lcom/pspdfkit/internal/mh;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$item"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    invoke-interface {p0, p1}, Lcom/pspdfkit/internal/s1$a;->a(Lcom/pspdfkit/internal/mh;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 14
    iput v0, p0, Lcom/pspdfkit/internal/s1;->l:I

    .line 15
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(II)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ge p1, p2, :cond_3

    .line 448
    iget-object v2, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mh;

    .line 449
    iget-object v3, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/mh;

    move v4, p1

    :goto_0
    if-ge v4, p2, :cond_0

    .line 453
    iget-object v5, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    add-int/lit8 v6, v4, 0x1

    invoke-static {v5, v4, v6}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    move v4, v6

    goto :goto_0

    :cond_0
    add-int/lit8 v4, p2, 0x1

    .line 455
    iget-object v5, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v1

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/pspdfkit/internal/mh$d;

    if-eqz v4, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 456
    :cond_2
    iget-object v4, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    xor-int/2addr v0, v1

    invoke-interface {v4, v3, v2, v0}, Lcom/pspdfkit/internal/s1$a;->a(Lcom/pspdfkit/internal/mh;Lcom/pspdfkit/internal/mh;I)V

    goto :goto_4

    .line 462
    :cond_3
    iget-object v2, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mh;

    .line 463
    iget-object v3, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/mh;

    add-int/lit8 v4, p2, 0x1

    if-gt v4, p1, :cond_4

    move v5, p1

    .line 467
    :goto_1
    iget-object v6, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    add-int/lit8 v7, v5, -0x1

    invoke-static {v6, v5, v7}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    if-eq v5, v4, :cond_4

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_4
    if-le p2, v1, :cond_6

    .line 469
    iget-object v4, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    add-int/lit8 v5, p2, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/pspdfkit/internal/mh$d;

    if-eqz v4, :cond_5

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    .line 470
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/pspdfkit/internal/s1;->b:Lcom/pspdfkit/internal/s1$a;

    if-eqz v1, :cond_7

    goto :goto_3

    :cond_7
    const/4 v0, -0x1

    :goto_3
    invoke-interface {v4, v3, v2, v0}, Lcom/pspdfkit/internal/s1$a;->a(Lcom/pspdfkit/internal/mh;Lcom/pspdfkit/internal/mh;I)V

    .line 473
    :goto_4
    invoke-virtual {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/s1;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 3

    const-string v0, "themeConfiguration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    iget v0, p1, Lcom/pspdfkit/internal/rl;->a:I

    iput v0, p0, Lcom/pspdfkit/internal/s1;->g:I

    .line 6
    iget v0, p1, Lcom/pspdfkit/internal/rl;->c:I

    iput v0, p0, Lcom/pspdfkit/internal/s1;->e:I

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/internal/s5;->a(I)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/s1;->f:I

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->r:I

    iget v2, p1, Lcom/pspdfkit/internal/rl;->s:I

    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/s1;->j:Landroid/graphics/drawable/Drawable;

    .line 9
    iget v0, p1, Lcom/pspdfkit/internal/rl;->w:I

    iput v0, p0, Lcom/pspdfkit/internal/s1;->h:I

    .line 10
    iget p1, p1, Lcom/pspdfkit/internal/rl;->v:I

    iput p1, p0, Lcom/pspdfkit/internal/s1;->i:I

    .line 12
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/internal/mh;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 474
    new-instance v0, Lcom/pspdfkit/internal/s1$c;

    invoke-direct {v0}, Lcom/pspdfkit/internal/s1$c;-><init>()V

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    .line 475
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    sget-object v1, Lcom/pspdfkit/internal/s1$d;->a:Lcom/pspdfkit/internal/s1$d;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    .line 477
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/mh;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/mh;->e()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 483
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/mh;

    .line 484
    instance-of v3, v2, Lcom/pspdfkit/internal/mh$d;

    if-nez v3, :cond_1

    .line 485
    instance-of v3, v2, Lcom/pspdfkit/internal/mh$b;

    if-eqz v3, :cond_2

    goto :goto_1

    .line 492
    :cond_2
    invoke-virtual {v2}, Lcom/pspdfkit/internal/mh;->e()I

    move-result v3

    if-eq v3, v0, :cond_3

    if-le v3, v1, :cond_3

    .line 495
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    new-instance v4, Lcom/pspdfkit/internal/mh$d;

    invoke-direct {v4, v3}, Lcom/pspdfkit/internal/mh$d;-><init>(I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v3

    .line 498
    :cond_3
    iget v3, p0, Lcom/pspdfkit/internal/s1;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/pspdfkit/internal/s1;->l:I

    .line 499
    iget-object v3, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 502
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_5

    .line 504
    iget-object p1, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    new-instance v0, Lcom/pspdfkit/internal/mh$b;

    invoke-direct {v0}, Lcom/pspdfkit/internal/mh$b;-><init>()V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_5
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/pspdfkit/internal/s1;->m:Z

    if-eq p1, v0, :cond_0

    .line 3
    iput-boolean p1, p0, Lcom/pspdfkit/internal/s1;->m:Z

    .line 4
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 8

    .line 251
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/mh;

    .line 252
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->k:Lcom/pspdfkit/configuration/PdfConfiguration;

    .line 253
    iget-boolean v1, p0, Lcom/pspdfkit/internal/s1;->m:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 255
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh;->e()I

    move-result v1

    .line 256
    iget-object v4, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    .line 257
    const-class v5, Lcom/pspdfkit/internal/mh$a;

    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->filterIsInstance(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 350
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 351
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/pspdfkit/internal/mh$a;

    .line 352
    invoke-virtual {v7}, Lcom/pspdfkit/internal/mh$a;->e()I

    move-result v7

    if-ne v7, v1, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_0

    .line 445
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 446
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 447
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/mh;->a(Lcom/pspdfkit/configuration/PdfConfiguration;I)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/s1;->l:I

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/s1;->n:Z

    .line 3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/mh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/mh;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/mh;

    .line 2
    instance-of v0, p1, Lcom/pspdfkit/internal/mh$d;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 3
    :cond_0
    instance-of p1, p1, Lcom/pspdfkit/internal/mh$b;

    if-eqz p1, :cond_1

    const/4 p1, 0x2

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 6

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/s1$b;

    const-string v0, "holder"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    iget v1, p0, Lcom/pspdfkit/internal/s1;->g:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 191
    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/mh;

    .line 192
    instance-of v0, p1, Lcom/pspdfkit/internal/s1$b$a;

    if-eqz v0, :cond_0

    .line 193
    check-cast p1, Lcom/pspdfkit/internal/s1$b$a;

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/s1;->a(Lcom/pspdfkit/internal/s1$b$a;Lcom/pspdfkit/internal/mh;)V

    goto :goto_0

    .line 194
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/internal/s1$b$b;

    if-eqz v0, :cond_2

    .line 196
    iget-object p2, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    .line 197
    sget v0, Lcom/pspdfkit/R$plurals;->pspdf__annotations_number:I

    .line 198
    check-cast p1, Lcom/pspdfkit/internal/s1$b$b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$b;->a()Landroid/widget/TextView;

    move-result-object v1

    .line 199
    iget v2, p0, Lcom/pspdfkit/internal/s1;->l:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 200
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 201
    invoke-static {p2, v0, v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/widget/TextView;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "getQuantityString(\n     \u2026tationCount\n            )"

    .line 204
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$b;->a()Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/pspdfkit/internal/s1;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 210
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$b;->a()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-boolean p2, p0, Lcom/pspdfkit/internal/s1;->n:Z

    if-eqz p2, :cond_1

    .line 213
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$b;->b()Landroid/widget/ProgressBar;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$b;->b()Landroid/widget/ProgressBar;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 217
    :cond_2
    instance-of v0, p1, Lcom/pspdfkit/internal/s1$b$c;

    if-eqz v0, :cond_3

    .line 218
    check-cast p1, Lcom/pspdfkit/internal/s1$b$c;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/s1$b$c;->a()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/s1;->a:Landroid/content/Context;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/mh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 3

    const-string v0, "parent"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    const/4 v2, 0x2

    if-eq p2, v2, :cond_0

    .line 180
    iget-object p2, p0, Lcom/pspdfkit/internal/s1;->c:Landroid/view/LayoutInflater;

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__outline_pager_annotation_list_item:I

    invoke-virtual {p2, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 181
    new-instance p2, Lcom/pspdfkit/internal/s1$b$a;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/s1$b$a;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 182
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/s1;->c:Landroid/view/LayoutInflater;

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__outline_pager_list_footer:I

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 183
    new-instance p2, Lcom/pspdfkit/internal/s1$b$b;

    const-string v0, "footer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/s1$b$b;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 184
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/s1;->c:Landroid/view/LayoutInflater;

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__outline_pager_annotation_page_item:I

    invoke-virtual {p2, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 185
    new-instance p2, Lcom/pspdfkit/internal/s1$b$c;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/s1$b$c;-><init>(Landroid/view/View;)V

    :goto_0
    return-object p2
.end method

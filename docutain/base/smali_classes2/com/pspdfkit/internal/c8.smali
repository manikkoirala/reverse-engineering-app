.class public final Lcom/pspdfkit/internal/c8;
.super Lcom/pspdfkit/internal/a6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/c8$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/a6<",
        "Lcom/pspdfkit/internal/c8$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final e:Lkotlinx/serialization/KSerializer;

.field private final f:Lcom/pspdfkit/internal/c8$a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/pt;II)V
    .locals 3

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/a6;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->DELETE_RANGE:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/c8;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 11
    sget-object v0, Lcom/pspdfkit/internal/c8$a;->Companion:Lcom/pspdfkit/internal/c8$a$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/c8$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/c8;->e:Lkotlinx/serialization/KSerializer;

    .line 12
    new-instance v0, Lcom/pspdfkit/internal/c8$a;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    .line 14
    new-instance v2, Lcom/pspdfkit/internal/l5;

    invoke-direct {v2, p2, p3}, Lcom/pspdfkit/internal/l5;-><init>(II)V

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object p1

    .line 16
    invoke-direct {v0, v1, v2, p1}, Lcom/pspdfkit/internal/c8$a;-><init>(Ljava/util/UUID;Lcom/pspdfkit/internal/l5;Lcom/pspdfkit/internal/cb;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/c8;->f:Lcom/pspdfkit/internal/c8$a;

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c8;->f:Lcom/pspdfkit/internal/c8$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c8;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c8;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

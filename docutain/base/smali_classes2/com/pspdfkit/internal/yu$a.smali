.class final Lcom/pspdfkit/internal/yu$a;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/yu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/yu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/yu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 1
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0xbb8

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 17
    :pswitch_0
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    sget v0, Lcom/pspdfkit/R$id;->pspdf__center_play_btn:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$mb(Lcom/pspdfkit/internal/yu;I)V

    goto/16 :goto_0

    .line 20
    :pswitch_1
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    .line 21
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    sget v0, Lcom/pspdfkit/R$id;->pspdf__error_layout:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$mb(Lcom/pspdfkit/internal/yu;I)V

    goto/16 :goto_0

    .line 27
    :pswitch_2
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    .line 28
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    .line 29
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetv(Lcom/pspdfkit/internal/yu;)Landroid/view/View;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-nez v0, :cond_0

    .line 31
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetv(Lcom/pspdfkit/internal/yu;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 33
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgets(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 34
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgets(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 36
    :cond_1
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetr(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 37
    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetr(Lcom/pspdfkit/internal/yu;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 38
    :pswitch_3
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    .line 39
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/yu;->a(I)V

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    sget v0, Lcom/pspdfkit/R$id;->pspdf__loading_layout:I

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$mb(Lcom/pspdfkit/internal/yu;I)V

    goto :goto_0

    .line 41
    :pswitch_4
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    invoke-static {p1}, Lcom/pspdfkit/internal/yu;->-$$Nest$md(Lcom/pspdfkit/internal/yu;)I

    move-result p1

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    invoke-static {v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetm(Lcom/pspdfkit/internal/yu;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetl(Lcom/pspdfkit/internal/yu;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/pspdfkit/internal/yu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/yu;)Lcom/pspdfkit/internal/yu$g;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/pspdfkit/internal/zu;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zu;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 43
    invoke-virtual {p0, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 44
    rem-int/lit16 p1, p1, 0x3e8

    rsub-int p1, p1, 0x3e8

    int-to-long v1, p1

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 45
    :pswitch_5
    iget-object p1, p0, Lcom/pspdfkit/internal/yu$a;->a:Lcom/pspdfkit/internal/yu;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->a()V

    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

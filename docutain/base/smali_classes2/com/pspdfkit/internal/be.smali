.class public final Lcom/pspdfkit/internal/be;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:F

.field private d:F


# direct methods
.method public static synthetic $r8$lambda$onkSfIORKmG5Z_-Y8FWe-xN8ucE(Lcom/pspdfkit/internal/be;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;Lcom/pspdfkit/internal/l4;)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/be;->a(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;Lcom/pspdfkit/internal/l4;)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/be;->b:Z

    const/high16 v0, 0x437a0000    # 250.0f

    .line 4
    iput v0, p0, Lcom/pspdfkit/internal/be;->c:F

    .line 5
    iput v0, p0, Lcom/pspdfkit/internal/be;->d:F

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/be;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;Lcom/pspdfkit/internal/l4;)Lcom/pspdfkit/annotations/StampAnnotation;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 8
    invoke-interface {p1, p2}, Lcom/pspdfkit/document/PdfDocument;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/be;->c:F

    .line 10
    iget v1, p0, Lcom/pspdfkit/internal/be;->d:F

    .line 11
    iget-boolean v2, p0, Lcom/pspdfkit/internal/be;->b:Z

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    .line 12
    invoke-virtual {p4}, Lcom/pspdfkit/internal/l4;->d()I

    move-result v2

    .line 13
    invoke-virtual {p4}, Lcom/pspdfkit/internal/l4;->c()I

    move-result v5

    div-float v6, v0, v1

    int-to-float v2, v2

    int-to-float v5, v5

    div-float v7, v2, v5

    .line 15
    invoke-static {v6, v7}, Lcom/pspdfkit/internal/di;->a(FF)Z

    move-result v6

    if-nez v6, :cond_2

    .line 16
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v4, v4, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v4, v2, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 17
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 18
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    goto :goto_1

    .line 21
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    .line 22
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v2, v5

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    .line 25
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    :goto_0
    div-float/2addr v1, v2

    .line 28
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float v2, v2, v1

    .line 29
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v0, v0, v1

    .line 30
    iget v1, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v5

    sub-float/2addr v5, v2

    div-float/2addr v5, v3

    add-float/2addr v5, v1

    .line 31
    iget v1, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v6, v0

    div-float/2addr v6, v3

    add-float/2addr v6, v1

    .line 33
    new-instance v1, Landroid/graphics/RectF;

    add-float/2addr v2, v5

    add-float/2addr v0, v6

    invoke-direct {v1, v5, v6, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 34
    :goto_1
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 35
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 38
    :cond_2
    iget v2, p1, Lcom/pspdfkit/utils/Size;->width:F

    .line 39
    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v2, 0x42000000    # 32.0f

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 40
    iget v5, p1, Lcom/pspdfkit/utils/Size;->height:F

    .line 41
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 42
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 43
    new-instance v5, Landroid/graphics/RectF;

    div-float/2addr v0, v3

    sub-float v6, v2, v0

    div-float/2addr v1, v3

    add-float v3, p3, v1

    add-float/2addr v2, v0

    sub-float/2addr p3, v1

    invoke-direct {v5, v6, v3, v2, p3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 44
    new-instance p3, Landroid/graphics/RectF;

    iget v0, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    invoke-direct {p3, v4, v0, p1, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v5, p3}, Lcom/pspdfkit/internal/ga;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 45
    new-instance p1, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {p4}, Lcom/pspdfkit/internal/l4;->b()[B

    move-result-object p3

    invoke-direct {p1, p2, v5, p3}, Lcom/pspdfkit/annotations/StampAnnotation;-><init>(ILandroid/graphics/RectF;[B)V

    .line 47
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result p3

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result p4

    neg-float p4, p4

    invoke-direct {p2, p3, p4}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    const/4 p3, 0x0

    invoke-virtual {p1, p3, p2}, Lcom/pspdfkit/annotations/StampAnnotation;->setRotation(ILcom/pspdfkit/utils/Size;)V

    return-object p1
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/be;
    .locals 1

    const/high16 v0, 0x437a0000    # 250.0f

    .line 1
    iput v0, p0, Lcom/pspdfkit/internal/be;->c:F

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/be;->d:F

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/pspdfkit/internal/be;->b:Z

    return-object p0
.end method

.method public final a(Lcom/pspdfkit/internal/zf;ILandroid/graphics/PointF;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/be;->a:Landroid/content/Context;

    .line 5
    invoke-static {v0, p4}, Lcom/pspdfkit/internal/r4;->c(Landroid/content/Context;Landroid/net/Uri;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p4

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/be$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/pspdfkit/internal/be$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/be;Lcom/pspdfkit/document/PdfDocument;ILandroid/graphics/PointF;)V

    .line 7
    invoke-virtual {p4, v0}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.class final Lcom/pspdfkit/internal/fd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/hd;

.field final synthetic b:Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/hd;Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/fd;->a:Lcom/pspdfkit/internal/hd;

    iput-object p2, p0, Lcom/pspdfkit/internal/fd;->b:Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/document/files/EmbeddedFile;

    const-string v0, "embeddedFile"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/fd;->a:Lcom/pspdfkit/internal/hd;

    iget-object v1, p0, Lcom/pspdfkit/internal/fd;->b:Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/actions/GoToEmbeddedAction;->getPageIndex()I

    move-result v1

    invoke-static {v0, p1, v1}, Lcom/pspdfkit/internal/hd;->a(Lcom/pspdfkit/internal/hd;Lcom/pspdfkit/document/files/EmbeddedFile;I)V

    return-void
.end method

.class final Lcom/pspdfkit/internal/o7$b;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/o7;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/o7;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/o7;Landroid/os/Handler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/o7$b;->a:Lcom/pspdfkit/internal/o7;

    .line 2
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    const/4 v3, 0x3

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_1

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/o7$b;->a:Lcom/pspdfkit/internal/o7;

    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgeth(Lcom/pspdfkit/internal/o7;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 13
    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgeti(Lcom/pspdfkit/internal/o7;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 14
    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgetn(Lcom/pspdfkit/internal/o7;)Landroid/view/MotionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 16
    :cond_0
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fputj(Lcom/pspdfkit/internal/o7;Z)V

    goto :goto_0

    .line 22
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/o7$b;->a:Lcom/pspdfkit/internal/o7;

    .line 24
    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgeta(Lcom/pspdfkit/internal/o7;)Landroid/os/Handler;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    .line 26
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/o7;->-$$Nest$fputj(Lcom/pspdfkit/internal/o7;Z)V

    .line 27
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fputk(Lcom/pspdfkit/internal/o7;Z)V

    .line 28
    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgetb(Lcom/pspdfkit/internal/o7;)Lcom/pspdfkit/internal/o7$a;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgetn(Lcom/pspdfkit/internal/o7;)Landroid/view/MotionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 29
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/o7$b;->a:Lcom/pspdfkit/internal/o7;

    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgetb(Lcom/pspdfkit/internal/o7;)Lcom/pspdfkit/internal/o7$a;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/o7;->-$$Nest$fgetn(Lcom/pspdfkit/internal/o7;)Landroid/view/MotionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    :cond_4
    :goto_0
    return-void
.end method

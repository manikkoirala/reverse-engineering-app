.class final Lcom/pspdfkit/internal/x7$a;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/x7;->getDestinationUri(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/rxjava3/core/Maybe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/net/Uri;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/x7;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/x7;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/x7$a;->a:Lcom/pspdfkit/internal/x7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 1
    check-cast p1, Landroid/net/Uri;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/x7$a;->a:Lcom/pspdfkit/internal/x7;

    invoke-static {v0}, Lcom/pspdfkit/internal/x7;->c(Lcom/pspdfkit/internal/x7;)Lio/reactivex/rxjava3/subjects/MaybeSubject;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "maybeSubject"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/x7$a;->a:Lcom/pspdfkit/internal/x7;

    if-nez p1, :cond_1

    .line 4
    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/MaybeSubject;->onComplete()V

    goto :goto_0

    .line 5
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/v;->a()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 6
    invoke-static {v1, p1}, Lcom/pspdfkit/internal/x7;->a(Lcom/pspdfkit/internal/x7;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 7
    invoke-static {v1}, Lcom/pspdfkit/internal/x7;->b(Lcom/pspdfkit/internal/x7;)Lcom/pspdfkit/internal/t;

    move-result-object v2

    .line 8
    invoke-static {v1}, Lcom/pspdfkit/internal/x7;->a(Lcom/pspdfkit/internal/x7;)Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v3

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/internal/x7;->a(Lcom/pspdfkit/internal/x7;)Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;)Landroidx/fragment/app/FragmentManager;

    move-result-object v4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 10
    invoke-static {v1}, Lcom/pspdfkit/internal/x7;->a(Lcom/pspdfkit/internal/x7;)Landroidx/appcompat/app/AppCompatActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/in$a;->a(Landroid/content/Context;)Lcom/pspdfkit/internal/in;

    move-result-object v1

    .line 11
    new-instance v5, Lcom/pspdfkit/internal/w7;

    invoke-direct {v5, v0, p1}, Lcom/pspdfkit/internal/w7;-><init>(Lio/reactivex/rxjava3/subjects/MaybeSubject;Landroid/net/Uri;)V

    invoke-interface {v2, v3, v4, v1, v5}, Lcom/pspdfkit/internal/t;->a(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/in;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 20
    :cond_2
    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/subjects/MaybeSubject;->onSuccess(Ljava/lang/Object;)V

    .line 21
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

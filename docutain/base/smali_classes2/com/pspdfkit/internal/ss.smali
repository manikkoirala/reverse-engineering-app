.class public final Lcom/pspdfkit/internal/ss;
.super Lcom/pspdfkit/internal/e0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/e0<",
        "Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;"
    }
.end annotation


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 1
    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/e0;-><init>([Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ss;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ss;->build()Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;
    .locals 4

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->B:Lcom/pspdfkit/internal/g0;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/h0;->a(Lcom/pspdfkit/internal/g0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    iget-object v2, p0, Lcom/pspdfkit/internal/ss;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultStampPickerItems(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    const-string v3, "getDefaultStampPickerItems(context)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    .line 6
    :cond_0
    new-instance v0, Lcom/pspdfkit/internal/ts;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ts;-><init>(Lcom/pspdfkit/internal/h0;)V

    return-object v0
.end method

.method public final setAvailableStampPickerItems(Ljava/util/List;)Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;)",
            "Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration$Builder;"
        }
    .end annotation

    const-string v0, "stampPickerItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/g0;->B:Lcom/pspdfkit/internal/g0;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/h0;->b(Lcom/pspdfkit/internal/g0;Ljava/lang/Object;)V

    const-string p1, "null cannot be cast to non-null type T of com.pspdfkit.internal.annotations.configuration.AnnotationConfigurationBuilder"

    .line 2
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

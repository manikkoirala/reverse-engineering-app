.class public final Lcom/pspdfkit/internal/ge;
.super Lcom/pspdfkit/internal/z3;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ua;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/z3<",
        "Lcom/pspdfkit/internal/ie;",
        ">;",
        "Lcom/pspdfkit/internal/ua;"
    }
.end annotation


# instance fields
.field private final b:Landroid/graphics/Matrix;

.field private c:I

.field private final d:J

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/ie;

    invoke-direct {v0}, Lcom/pspdfkit/internal/ie;-><init>()V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/z3;-><init>(Lcom/pspdfkit/internal/h4;)V

    .line 2
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/ge;->b:Landroid/graphics/Matrix;

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/pspdfkit/internal/ge;->d:J

    return-void
.end method

.method public constructor <init>(IIFF)V
    .locals 1

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/ie;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ie;-><init>(IIFF)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/z3;-><init>(Lcom/pspdfkit/internal/h4;)V

    .line 12
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ge;->b:Landroid/graphics/Matrix;

    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/ge;->d:J

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/InkAnnotation;Landroid/graphics/Matrix;F)Z
    .locals 7

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    .line 15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 16
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 17
    iget v1, p0, Lcom/pspdfkit/internal/ge;->c:I

    const/4 v2, 0x0

    if-ne v1, v0, :cond_1

    return v2

    .line 18
    :cond_1
    iput v0, p0, Lcom/pspdfkit/internal/ge;->c:I

    .line 22
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, p3

    .line 23
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 24
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 27
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 29
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 30
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    .line 31
    invoke-static {v5, v6, v0}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 32
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 35
    :cond_2
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    :cond_3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ge;->k()Ljava/util/ArrayList;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    const/4 v2, 0x1

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/ie;

    invoke-virtual {p1, v1, p2, p3}, Lcom/pspdfkit/internal/ie;->a(Ljava/util/ArrayList;Landroid/graphics/Matrix;F)V

    .line 41
    sget-object p1, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ge;->a(Lcom/pspdfkit/internal/br$a;)V

    :cond_4
    return v2
.end method


# virtual methods
.method public final a(ILandroid/graphics/Matrix;F)Lcom/pspdfkit/annotations/Annotation;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/internal/br;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;F)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/br$a;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/br$a;)V

    .line 113
    sget-object v0, Lcom/pspdfkit/internal/br$a;->b:Lcom/pspdfkit/internal/br$a;

    if-ne p1, v0, :cond_0

    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/pspdfkit/internal/ge;->e:J

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ri;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-interface {v0}, Lcom/pspdfkit/internal/br;->a()Z

    move-result v0

    return v0
.end method

.method public final a(FFF)Z
    .locals 12

    .line 44
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/ie;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ie;->q()Ljava/util/ArrayList;

    move-result-object p1

    .line 46
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/ie;

    .line 47
    invoke-virtual {p2}, Lcom/pspdfkit/internal/h4;->m()F

    move-result p2

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/graphics/PointF;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v0, "lines"

    .line 48
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "erasedPoints"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p2, v0

    add-float/2addr p2, p3

    float-to-double p2, p2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 75
    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p2

    double-to-float p2, p2

    .line 76
    new-instance p3, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 79
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 80
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 83
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_2

    .line 84
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    aget-object v9, v2, v3

    .line 86
    iget v10, v8, Landroid/graphics/PointF;->x:F

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget v11, v9, Landroid/graphics/PointF;->x:F

    iget v9, v9, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    mul-float v10, v10, v10

    sub-float/2addr v8, v9

    mul-float v8, v8, v8

    add-float/2addr v8, v10

    cmpg-float v8, v8, p2

    if-gez v8, :cond_1

    .line 87
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 93
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 96
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v7, 0x0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 97
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v9

    sub-int/2addr v9, v7

    if-lez v9, :cond_3

    const-string v9, "indexToDelete"

    .line 99
    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-interface {v5, v7, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_3
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v1

    goto :goto_2

    .line 104
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_0

    .line 105
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5, v7, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 109
    :cond_5
    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 110
    :cond_6
    invoke-interface {p1, p3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 111
    iget-object p1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p1, Lcom/pspdfkit/internal/ie;

    iget-object p2, p0, Lcom/pspdfkit/internal/ge;->b:Landroid/graphics/Matrix;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, p3, p2, v0}, Lcom/pspdfkit/internal/ie;->a(Ljava/util/ArrayList;Landroid/graphics/Matrix;F)V

    return v1

    :cond_7
    return v3
.end method

.method public final a(FLandroid/graphics/Matrix;)Z
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/h4;->a(FLandroid/graphics/Matrix;)Z

    move-result p1

    return p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 1

    const/4 v0, 0x1

    .line 42
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/pspdfkit/internal/ge;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p1

    return p1
.end method

.method public final a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ie;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 5
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v0, :cond_1

    .line 9
    move-object v0, p1

    check-cast v0, Lcom/pspdfkit/annotations/InkAnnotation;

    invoke-direct {p0, v0, p2, p3}, Lcom/pspdfkit/internal/ge;->a(Lcom/pspdfkit/annotations/InkAnnotation;Landroid/graphics/Matrix;F)Z

    move-result v0

    .line 10
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/z3;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p1

    or-int/2addr p1, v0

    .line 12
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/ie;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/ie;->p()V

    return p1

    .line 13
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "setAnnotation is implemented only for InkAnnotations."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a(Z)Z
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Z)Z

    move-result p1

    return p1
.end method

.method public final b(FLandroid/graphics/Matrix;)Ljava/util/ArrayList;
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ie;->q()Ljava/util/ArrayList;

    move-result-object v0

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 6
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 7
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v4, Landroid/graphics/PointF;->x:F

    mul-float v6, v6, p1

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float v4, v4, p1

    invoke-direct {v5, v6, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 9
    :cond_0
    invoke-static {v3, p2}, Lcom/pspdfkit/internal/nu;->b(Ljava/util/ArrayList;Landroid/graphics/Matrix;)V

    .line 10
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->k()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;F)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->f()F

    move-result v0

    return v0
.end method

.method public final f()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->m()F

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->g()I

    move-result v0

    return v0
.end method

.method public final h()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ge;->e:J

    return-wide v0
.end method

.method public final hide()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->hide()V

    return-void
.end method

.method public final i()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->j()I

    move-result v0

    return v0
.end method

.method public final j()Landroid/graphics/PointF;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ie;->q()Ljava/util/ArrayList;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    const/4 v1, 0x0

    .line 3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    .line 5
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    return-object v0
.end method

.method public final k()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ie;->q()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final l()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/ge;->d:J

    return-wide v0
.end method

.method public final m()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/ie;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ie;->r()Z

    move-result v0

    return v0
.end method

.class public final Lcom/pspdfkit/internal/qm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/pm$a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/rm;

.field private final c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private d:Lcom/pspdfkit/document/PdfDocument;

.field private e:I


# direct methods
.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/qm;->a:Landroid/content/Context;

    .line 8
    new-instance v0, Lcom/pspdfkit/internal/rm;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/rm;-><init>(Landroidx/appcompat/app/AppCompatActivity;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    return-void
.end method

.method private c(I)Z
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    :cond_0
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    :cond_1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    :cond_2
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    const/4 v1, 0x5

    if-ne p1, v0, :cond_3

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    if-eq v0, v1, :cond_6

    :cond_3
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    if-ne p1, v0, :cond_4

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    if-eq v0, v1, :cond_6

    :cond_4
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    if-ne p1, v0, :cond_5

    iget v0, p0, Lcom/pspdfkit/internal/qm;->e:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    :cond_5
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    if-ne p1, v0, :cond_7

    iget p1, p0, Lcom/pspdfkit/internal/qm;->e:I

    const/4 v0, 0x7

    if-ne p1, v0, :cond_7

    :cond_6
    const/4 p1, 0x1

    goto :goto_0

    :cond_7
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 191
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    if-ne p1, v0, :cond_1

    .line 192
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->j:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->i:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 195
    :cond_1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    if-ne p1, v0, :cond_3

    .line 196
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->l:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->k:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 199
    :cond_3
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    if-ne p1, v0, :cond_5

    .line 200
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 201
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->n:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 202
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->m:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 203
    :cond_5
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    if-ne p1, v0, :cond_7

    .line 204
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 205
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->f:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->e:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 207
    :cond_7
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    if-ne p1, v0, :cond_9

    .line 208
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 209
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->h:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 210
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->g:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 211
    :cond_9
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    if-ne p1, v0, :cond_b

    .line 212
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 213
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 214
    :cond_a
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 215
    :cond_b
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    if-ne p1, v0, :cond_d

    .line 216
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 217
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->v:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 218
    :cond_c
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->u:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 219
    :cond_d
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    if-ne p1, v0, :cond_f

    .line 220
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 221
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    .line 223
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 224
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_e
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->p:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 225
    :cond_f
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    if-ne p1, v0, :cond_11

    .line 226
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 227
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 228
    :cond_10
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 229
    :cond_11
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    if-ne p1, v0, :cond_13

    .line 230
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 231
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 232
    :cond_12
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget-object v0, v0, Lcom/pspdfkit/internal/rm;->s:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_13
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_16

    .line 239
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/qm;->d(I)Z

    move-result v1

    if-eqz v1, :cond_14

    const/16 v1, 0xff

    goto :goto_1

    :cond_14
    const/16 v1, 0x80

    .line 240
    :goto_1
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 246
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/qm;->c(I)Z

    move-result p1

    if-eqz p1, :cond_15

    .line 247
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget p1, p1, Lcom/pspdfkit/internal/rm;->b:I

    goto :goto_2

    .line 249
    :cond_15
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget p1, p1, Lcom/pspdfkit/internal/rm;->a:I

    .line 252
    :goto_2
    invoke-static {v0, p1}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    :cond_16
    return-object v0
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 5

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v1

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    monitor-enter v2

    :try_start_0
    const-string v4, "configuration"

    .line 7
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    sget-object v4, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ANNOTATION_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/PdfConfiguration;->isAnnotationEditingEnabled()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    monitor-exit v2

    if-eqz v3, :cond_1

    .line 123
    sget v2, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v3}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    sget v2, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_2
    iget-object v2, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    invoke-virtual {v1, v2, v3}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 143
    invoke-virtual {v1}, Lcom/pspdfkit/internal/hb;->c()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 144
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSignatureButtonPositionForcedInMainToolbar()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 145
    :cond_3
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 152
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 153
    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 154
    :cond_5
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_6
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isReaderViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/pspdfkit/ui/PdfReaderView;->doesDeviceSupportReaderView(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 164
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_7
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 169
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_8
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSettingsItemEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 174
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_9
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 180
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->isPrintingAvailable(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 181
    :cond_a
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_b
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 186
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_c
    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 190
    sget v1, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/pspdfkit/internal/zf;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    return-void
.end method

.method public final b()I
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->b:Lcom/pspdfkit/internal/rm;

    iget v0, v0, Lcom/pspdfkit/internal/rm;->a:I

    return v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    if-ne p1, v0, :cond_0

    .line 2
    sget p1, Lcom/pspdfkit/R$string;->pspdf__annotations:I

    goto :goto_0

    .line 3
    :cond_0
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    if-ne p1, v0, :cond_1

    .line 4
    sget p1, Lcom/pspdfkit/R$string;->pspdf__contentediting_title:I

    goto :goto_0

    .line 5
    :cond_1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    if-ne p1, v0, :cond_2

    .line 6
    sget p1, Lcom/pspdfkit/R$string;->pspdf__signature:I

    goto :goto_0

    .line 7
    :cond_2
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    if-ne p1, v0, :cond_3

    .line 8
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_outline:I

    goto :goto_0

    .line 9
    :cond_3
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SEARCH:I

    if-ne p1, v0, :cond_4

    .line 10
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_search:I

    goto :goto_0

    .line 11
    :cond_4
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SETTINGS:I

    if-ne p1, v0, :cond_5

    .line 12
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_settings:I

    goto :goto_0

    .line 13
    :cond_5
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_READER_VIEW:I

    if-ne p1, v0, :cond_6

    .line 14
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_reader_view:I

    goto :goto_0

    .line 15
    :cond_6
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    if-ne p1, v0, :cond_8

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 17
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    .line 18
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    .line 19
    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 20
    sget p1, Lcom/pspdfkit/R$string;->pspdf__print:I

    goto :goto_0

    .line 22
    :cond_7
    sget p1, Lcom/pspdfkit/R$string;->pspdf__share:I

    goto :goto_0

    .line 24
    :cond_8
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_THUMBNAIL_GRID:I

    if-ne p1, v0, :cond_9

    .line 25
    sget p1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_pagegrid:I

    goto :goto_0

    .line 26
    :cond_9
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_DOCUMENT_INFO:I

    if-ne p1, v0, :cond_a

    .line 27
    sget p1, Lcom/pspdfkit/R$string;->pspdf__document_info:I

    goto :goto_0

    :cond_a
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_b

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->a:Landroid/content/Context;

    const/4 v1, 0x0

    .line 30
    invoke-static {v0, p1, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_b
    const-string p1, ""

    :goto_1
    return-object p1
.end method

.method public final d(I)Z
    .locals 5

    .line 1
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_ANNOTATIONS:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_7

    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SIGNATURE:I

    if-ne p1, v0, :cond_0

    goto/16 :goto_1

    .line 3
    :cond_0
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_OUTLINE:I

    if-ne p1, v0, :cond_3

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-nez p1, :cond_1

    goto/16 :goto_2

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->hasOutline()Z

    move-result p1

    if-nez p1, :cond_9

    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result p1

    if-nez p1, :cond_9

    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_3

    .line 11
    :cond_3
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_SHARE:I

    if-ne p1, v0, :cond_5

    .line 12
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getEnabledShareFeatures()Ljava/util/EnumSet;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/sharing/ShareFeatures;->DOCUMENT_SHARING:Lcom/pspdfkit/configuration/sharing/ShareFeatures;

    .line 15
    invoke-virtual {p1, v0}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result p1

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v0, :cond_4

    .line 17
    invoke-static {}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->get()Lcom/pspdfkit/document/printing/DocumentPrintManager;

    move-result-object v0

    iget-object v3, p0, Lcom/pspdfkit/internal/qm;->c:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    iget-object v4, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    invoke-virtual {v0, v3, v4}, Lcom/pspdfkit/document/printing/DocumentPrintManager;->isPrintingEnabled(Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;Lcom/pspdfkit/document/PdfDocument;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    .line 18
    :goto_0
    iget-object v3, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-eqz v3, :cond_8

    if-nez v0, :cond_9

    if-eqz p1, :cond_8

    goto :goto_3

    .line 19
    :cond_5
    sget v0, Lcom/pspdfkit/ui/PdfActivity;->MENU_OPTION_EDIT_CONTENT:I

    if-ne p1, v0, :cond_6

    .line 20
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_8

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->isValidForEditing()Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_3

    .line 22
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_8

    goto :goto_3

    .line 23
    :cond_7
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/qm;->d:Lcom/pspdfkit/document/PdfDocument;

    if-eqz p1, :cond_8

    sget-object v0, Lcom/pspdfkit/document/DocumentPermissions;->ANNOTATIONS_AND_FORMS:Lcom/pspdfkit/document/DocumentPermissions;

    invoke-interface {p1, v0}, Lcom/pspdfkit/document/PdfDocument;->hasPermission(Lcom/pspdfkit/document/DocumentPermissions;)Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_3

    :cond_8
    :goto_2
    const/4 v1, 0x0

    :cond_9
    :goto_3
    return v1
.end method

.method public final e(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/qm;->e:I

    return-void
.end method

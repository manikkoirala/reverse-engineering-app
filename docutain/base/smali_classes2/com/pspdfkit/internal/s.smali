.class final Lcom/pspdfkit/internal/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/t;


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:I


# direct methods
.method public static synthetic $r8$lambda$ymdHA08z7erKzgprIEw0fhFcSDg(Lcom/pspdfkit/internal/hn;Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/s;->a(Lcom/pspdfkit/internal/hn;Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;I)V
    .locals 1

    const-string v0, "permissions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/s;->a:[Ljava/lang/String;

    .line 3
    iput p2, p0, Lcom/pspdfkit/internal/s;->b:I

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/hn;Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    const-string v0, "$fragment"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$fragmentManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    const-string v0, "com.pspdfkit.internal.permission.AndroidPermissionDialogHandler.FRAGMENT_TAG"

    .line 4
    invoke-virtual {p1, p0, v0}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    .line 5
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitNow()V

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hn;->a()Z

    move-result p1

    if-nez p1, :cond_1

    .line 9
    invoke-virtual {p0}, Lcom/pspdfkit/internal/hn;->c()V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/in;Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroidx/fragment/app/FragmentManager;",
            "Lcom/pspdfkit/internal/in;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "fragmentManager"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "permissionProvider"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "callback"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/s;->a:[Ljava/lang/String;

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 11
    move-object v4, p3

    check-cast v4, Lcom/pspdfkit/internal/jn;

    invoke-virtual {v4, v3}, Lcom/pspdfkit/internal/jn;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 12
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p4, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_2
    const-string p1, "com.pspdfkit.internal.permission.AndroidPermissionDialogHandler.FRAGMENT_TAG"

    .line 17
    invoke-virtual {p2, p1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    if-nez p1, :cond_3

    .line 18
    new-instance p1, Lcom/pspdfkit/internal/hn;

    invoke-direct {p1}, Lcom/pspdfkit/internal/hn;-><init>()V

    .line 19
    :cond_3
    check-cast p1, Lcom/pspdfkit/internal/hn;

    .line 23
    invoke-virtual {p1, p4}, Lcom/pspdfkit/internal/hn;->a(Lkotlin/jvm/functions/Function1;)V

    .line 24
    iget-object p3, p0, Lcom/pspdfkit/internal/s;->a:[Ljava/lang/String;

    const-string p4, "<set-?>"

    .line 25
    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    iput-object p3, p1, Lcom/pspdfkit/internal/hn;->d:[Ljava/lang/String;

    .line 166
    iget p3, p0, Lcom/pspdfkit/internal/s;->b:I

    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/hn;->a(I)V

    .line 170
    new-instance p3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p4

    invoke-direct {p3, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p4, Lcom/pspdfkit/internal/s$$ExternalSyntheticLambda0;

    invoke-direct {p4, p1, p2}, Lcom/pspdfkit/internal/s$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/hn;Landroidx/fragment/app/FragmentManager;)V

    invoke-virtual {p3, p4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

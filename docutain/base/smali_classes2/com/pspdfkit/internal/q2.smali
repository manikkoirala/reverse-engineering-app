.class public final Lcom/pspdfkit/internal/q2;
.super Lcom/pspdfkit/internal/a6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/q2$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/a6<",
        "Lcom/pspdfkit/internal/q2$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final e:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/q2$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/pspdfkit/internal/q2$a;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)V
    .locals 2

    const-string v0, "textBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "styleInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/a6;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->APPLY_FORMAT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/q2;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 11
    sget-object v0, Lcom/pspdfkit/internal/q2$a;->Companion:Lcom/pspdfkit/internal/q2$a$b;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/q2$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/q2;->e:Lkotlinx/serialization/KSerializer;

    .line 12
    new-instance v0, Lcom/pspdfkit/internal/q2$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object p1

    invoke-direct {v0, v1, p1, p2}, Lcom/pspdfkit/internal/q2$a;-><init>(Ljava/util/UUID;Lcom/pspdfkit/internal/cb;Lcom/pspdfkit/internal/jt;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/q2;->f:Lcom/pspdfkit/internal/q2$a;

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q2;->f:Lcom/pspdfkit/internal/q2$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q2;->e:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/q2;->d:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

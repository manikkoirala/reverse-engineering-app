.class public final Lcom/pspdfkit/internal/wr;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/wr$b;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/wr$b;

.field private c:Landroid/view/View;

.field private d:Lcom/google/android/material/textfield/TextInputEditText;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;


# direct methods
.method public static synthetic $r8$lambda$BcwVtGursIB9p7wgZKQDWGnoU5s(Lcom/pspdfkit/internal/wr;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wr;->b(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$CCHMKHyuuxZDdYC2EaE0RailtvE(Lcom/pspdfkit/internal/wr;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wr;->a(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetf(Lcom/pspdfkit/internal/wr;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/wr;->f:Landroid/widget/TextView;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wr;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__signature_list_dialog:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_text_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/wr;->c:Landroid/view/View;

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_password_edittext:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputEditText;

    iput-object v0, p0, Lcom/pspdfkit/internal/wr;->d:Lcom/google/android/material/textfield/TextInputEditText;

    .line 6
    new-instance v1, Lcom/pspdfkit/internal/wr$a;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/wr$a;-><init>(Lcom/pspdfkit/internal/wr;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_throbber:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/wr;->e:Landroid/view/View;

    .line 16
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_sign_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/wr;->f:Landroid/widget/TextView;

    .line 17
    new-instance v1, Lcom/pspdfkit/internal/wr$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/wr$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/wr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    sget v0, Lcom/pspdfkit/R$id;->pspdf__signature_sign_cancel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/wr$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/wr$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/wr;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 1

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/wr;->b:Lcom/pspdfkit/internal/wr$b;

    if-eqz p1, :cond_0

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->d:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/wr$b;->onPasswordEntered(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private synthetic b(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/wr;->b:Lcom/pspdfkit/internal/wr$b;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/internal/wr$b;->onPasswordCanceled()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->d:Lcom/google/android/material/textfield/TextInputEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->d:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->d:Lcom/google/android/material/textfield/TextInputEditText;

    const-string v1, "Invalid password."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/wr;->c:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setListener(Lcom/pspdfkit/internal/wr$b;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/wr;->b:Lcom/pspdfkit/internal/wr$b;

    return-void
.end method

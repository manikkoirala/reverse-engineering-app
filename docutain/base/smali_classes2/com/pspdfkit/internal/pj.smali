.class final Lcom/pspdfkit/internal/pj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/oj;


# instance fields
.field private final a:Lcom/pspdfkit/internal/mj;

.field private final b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

.field private final c:J

.field private final d:J

.field private final e:J

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/pspdfkit/internal/jni/NativeAnnotation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/mj;Lcom/pspdfkit/internal/jni/NativeAnnotationManager;Lcom/pspdfkit/internal/jni/NativeAnnotation;)V
    .locals 1

    const-string v0, "cache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nativeAnnotationManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nativeAnnotation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/pj;->a:Lcom/pspdfkit/internal/mj;

    .line 9
    iput-object p2, p0, Lcom/pspdfkit/internal/pj;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    .line 22
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getIdentifier()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/pj;->c:J

    .line 25
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAnnotationId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/pj;->d:J

    .line 31
    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeAnnotation;->getAbsolutePageIndex()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long p1, p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/pj;->e:J

    .line 34
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/pj;->f:Ljava/lang/ref/WeakReference;

    return-void

    .line 35
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t create native annotation holder: nativeAnnotation.absolutePageIndex() returned null."

    .line 36
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 37
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t create native annotation holder: nativeAnnotation.getAnnotationId() returned null."

    .line 38
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/pj;->c:J

    return-wide v0
.end method

.method public final getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pj;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/jni/NativeAnnotation;

    if-nez v0, :cond_2

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pj;->a:Lcom/pspdfkit/internal/mj;

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/mj;->a(Lcom/pspdfkit/internal/oj;)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/pj;->b:Lcom/pspdfkit/internal/jni/NativeAnnotationManager;

    iget-wide v1, p0, Lcom/pspdfkit/internal/pj;->d:J

    iget-wide v3, p0, Lcom/pspdfkit/internal/pj;->e:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/pspdfkit/internal/jni/NativeAnnotationManager;->getAnnotation(JJ)Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The NativeAnnotationHolder failed to retrieve a native annotation. It seems the NativeAnnotation was detached without updating the NativeAnnotationCache."

    .line 10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/pspdfkit/internal/pj;->f:Ljava/lang/ref/WeakReference;

    :cond_2
    return-object v0
.end method

.method public final release()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pj;->a:Lcom/pspdfkit/internal/mj;

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/mj;->b(Lcom/pspdfkit/internal/oj;)V

    return-void
.end method

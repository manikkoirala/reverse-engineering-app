.class public final Lcom/pspdfkit/internal/th;
.super Lcom/pspdfkit/internal/fe;
.source "SourceFile"


# instance fields
.field private K:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$9DRpmZ4sKbbOcOPRTLALa6VE4Ss(Lcom/pspdfkit/internal/th;Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/internal/gr;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/th;->a(Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/internal/gr;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$OfLKPhTo_h7B0lXd_I07XgqJqNw(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/th;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Orzf2rEKXgc0_wrAvRaVM8qa57s(Lcom/pspdfkit/internal/th;Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/annotations/ShapeAnnotation;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/th;->a(Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/annotations/ShapeAnnotation;)V

    return-void
.end method

.method public static synthetic $r8$lambda$gfLGeifOYPlF7fJGIK4iiJKOrDM(Ljava/util/List;)Lcom/pspdfkit/internal/gr;
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/th;->a(Ljava/util/List;)Lcom/pspdfkit/internal/gr;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/fe;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    return-void
.end method

.method private static synthetic a(Ljava/util/List;)Lcom/pspdfkit/internal/gr;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 10
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->r()Lcom/pspdfkit/internal/fr;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/fr;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/gr;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/internal/gr;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 11
    invoke-virtual {p2}, Lcom/pspdfkit/internal/gr;->a()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42960000    # 75.0f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_16

    .line 12
    invoke-virtual {p2}, Lcom/pspdfkit/internal/gr;->b()Lcom/pspdfkit/internal/ir;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ir;->d:Lcom/pspdfkit/internal/ir;

    if-eq v0, v1, :cond_16

    .line 13
    invoke-virtual {p2}, Lcom/pspdfkit/internal/gr;->b()Lcom/pspdfkit/internal/ir;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/ir;->e:Lcom/pspdfkit/internal/ir;

    if-ne v0, v1, :cond_0

    goto/16 :goto_7

    .line 19
    :cond_0
    invoke-virtual {p2}, Lcom/pspdfkit/internal/gr;->b()Lcom/pspdfkit/internal/ir;

    move-result-object p2

    const-string v0, "annotation"

    .line 20
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shapeTemplateIdentifier"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const-string v1, "annotation.lines"

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_d

    if-eq v0, v3, :cond_d

    if-eq v0, v2, :cond_1

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    const/4 v4, 0x4

    if-eq v0, v4, :cond_1

    goto/16 :goto_4

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    goto/16 :goto_4

    .line 58
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x0

    if-lt v1, v2, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    .line 59
    sget-object v5, Lcom/pspdfkit/internal/ir;->c:Lcom/pspdfkit/internal/ir;

    if-ne p2, v5, :cond_4

    const/4 v5, 0x1

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    :goto_1
    if-eqz v1, :cond_5

    .line 60
    sget-object v1, Lcom/pspdfkit/internal/ir;->b:Lcom/pspdfkit/internal/ir;

    if-ne p2, v1, :cond_5

    const/4 v4, 0x1

    .line 62
    :cond_5
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_12

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_6

    goto/16 :goto_4

    .line 63
    :cond_6
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    const-string v1, "lines.first()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/util/List;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/graphics/PointF;

    .line 64
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    const-string v6, "lines.last()"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    if-nez v5, :cond_8

    if-eqz v4, :cond_7

    goto :goto_2

    .line 77
    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_b

    goto/16 :goto_4

    :cond_8
    :goto_2
    if-eqz v5, :cond_9

    .line 78
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_3

    :cond_9
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    :goto_3
    check-cast p2, Ljava/util/List;

    .line 80
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v2, :cond_a

    goto/16 :goto_4

    :cond_a
    const-string v0, "line"

    .line 85
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 86
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Landroid/graphics/PointF;

    move-object p2, v0

    .line 92
    :cond_b
    new-instance v0, Lcom/pspdfkit/annotations/LineAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p1

    invoke-direct {v0, p1, p2, v1}, Lcom/pspdfkit/annotations/LineAnnotation;-><init>(ILandroid/graphics/PointF;Landroid/graphics/PointF;)V

    if-eqz v5, :cond_c

    .line 95
    sget-object p1, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    sget-object p2, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/annotations/LineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    goto :goto_5

    :cond_c
    if-eqz v4, :cond_13

    .line 97
    sget-object p1, Lcom/pspdfkit/annotations/LineEndType;->OPEN_ARROW:Lcom/pspdfkit/annotations/LineEndType;

    sget-object p2, Lcom/pspdfkit/annotations/LineEndType;->NONE:Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/annotations/LineAnnotation;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    goto :goto_5

    .line 98
    :cond_d
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v3, :cond_e

    goto :goto_4

    .line 106
    :cond_e
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_12

    .line 107
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v2, :cond_f

    goto :goto_4

    .line 111
    :cond_f
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    if-eqz p2, :cond_11

    if-eq p2, v3, :cond_10

    goto :goto_4

    .line 113
    :cond_10
    new-instance v0, Lcom/pspdfkit/annotations/CircleAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/annotations/CircleAnnotation;-><init>(ILandroid/graphics/RectF;)V

    goto :goto_5

    .line 114
    :cond_11
    new-instance v0, Lcom/pspdfkit/annotations/SquareAnnotation;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result p2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/pspdfkit/annotations/SquareAnnotation;-><init>(ILandroid/graphics/RectF;)V

    goto :goto_5

    :cond_12
    :goto_4
    const/4 v0, 0x0

    :cond_13
    :goto_5
    if-eqz v0, :cond_15

    .line 115
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p1

    iget-object p2, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object p2

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-nez p1, :cond_14

    goto :goto_6

    .line 119
    :cond_14
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 120
    :cond_15
    :goto_6
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1

    .line 121
    :cond_16
    :goto_7
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/pspdfkit/annotations/InkAnnotation;Lcom/pspdfkit/annotations/ShapeAnnotation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getFillColor()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/pspdfkit/annotations/Annotation;->setFillColor(I)V

    .line 124
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v1

    invoke-virtual {p2, v1}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 125
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderWidth()F

    move-result v1

    invoke-virtual {p2, v1}, Lcom/pspdfkit/annotations/Annotation;->setBorderWidth(F)V

    .line 126
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderColor()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/pspdfkit/annotations/Annotation;->setBorderColor(I)V

    .line 127
    invoke-static {p1}, Lcom/pspdfkit/internal/x;->b(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v1, p0, Lcom/pspdfkit/internal/i4;->k:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->removeAnnotationFromPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 131
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->k:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/r1;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;)V

    .line 132
    invoke-static {p2}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 139
    iget-object p1, p0, Lcom/pspdfkit/internal/i4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object p1

    new-instance p2, Lcom/pspdfkit/internal/v5;

    invoke-direct {p2, v0}, Lcom/pspdfkit/internal/v5;-><init>(Ljava/util/List;)V

    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.ShapeAnnotations"

    const-string v2, "Could not perform magic ink transformation"

    .line 140
    invoke-static {v1, p0, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private b(Lcom/pspdfkit/annotations/InkAnnotation;)Lio/reactivex/rxjava3/disposables/Disposable;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/InkAnnotation;->getLines()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    const/4 v1, 0x0

    .line 5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x2

    if-ge v1, v3, :cond_1

    return-object v2

    .line 8
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda0;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda0;-><init>(Ljava/util/List;)V

    invoke-static {v1}, Lio/reactivex/rxjava3/core/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x12c

    .line 16
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/core/Single;->delaySubscription(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 17
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/th;Lcom/pspdfkit/annotations/InkAnnotation;)V

    .line 18
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->flatMapMaybe(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/th;Lcom/pspdfkit/annotations/InkAnnotation;)V

    new-instance p1, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda3;

    invoke-direct {p1}, Lcom/pspdfkit/internal/th$$ExternalSyntheticLambda3;-><init>()V

    .line 39
    invoke-virtual {v0, v1, p1}, Lio/reactivex/rxjava3/core/Maybe;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected final a(FF)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/i4;->a(FF)V

    return-void
.end method

.method protected final a(Lcom/pspdfkit/annotations/InkAnnotation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/th;->b(Lcom/pspdfkit/annotations/InkAnnotation;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->MAGIC_INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/fe;->onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz p1, :cond_0

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/th;->b(Lcom/pspdfkit/annotations/InkAnnotation;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

.method protected final p()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/fe;->p()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/fe;->E:Lcom/pspdfkit/annotations/InkAnnotation;

    if-eqz v0, :cond_0

    .line 6
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/th;->b(Lcom/pspdfkit/annotations/InkAnnotation;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/th;->K:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method

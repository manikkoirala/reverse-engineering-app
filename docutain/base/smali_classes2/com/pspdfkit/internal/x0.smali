.class public final Lcom/pspdfkit/internal/x0;
.super Lcom/pspdfkit/internal/x1;
.source "SourceFile"


# instance fields
.field private final c:Lcom/pspdfkit/annotations/FileAnnotation;

.field private d:Lcom/pspdfkit/document/files/EmbeddedFileSource;

.field private e:Lcom/pspdfkit/internal/sa;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/FileAnnotation;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/x1;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/x0;->c:Lcom/pspdfkit/annotations/FileAnnotation;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/FileAnnotation;Lcom/pspdfkit/document/files/EmbeddedFileSource;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileSource"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/x0;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;)V

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/x0;->d:Lcom/pspdfkit/document/files/EmbeddedFileSource;

    const/4 p1, 0x1

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/x1;->a(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/FileAnnotation;Ljava/lang/String;)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/x0;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;)V

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/sa;

    invoke-direct {v0, p1, p2}, Lcom/pspdfkit/internal/sa;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/x0;->e:Lcom/pspdfkit/internal/sa;

    return-void
.end method


# virtual methods
.method public final g()Z
    .locals 14

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x0;->c:Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/pspdfkit/internal/x1;->e()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/x0;->d:Lcom/pspdfkit/document/files/EmbeddedFileSource;

    if-nez v0, :cond_1

    return v1

    .line 4
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/x0;->c:Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getNativeAnnotation()Lcom/pspdfkit/internal/jni/NativeAnnotation;

    move-result-object v2

    if-nez v2, :cond_2

    return v1

    .line 5
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/x0;->c:Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v3

    if-nez v3, :cond_3

    return v1

    .line 7
    :cond_3
    new-instance v4, Lcom/pspdfkit/internal/s7;

    invoke-virtual {v0}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getDataProvider()Lcom/pspdfkit/document/providers/DataProvider;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/pspdfkit/internal/s7;-><init>(Lcom/pspdfkit/document/providers/DataProvider;)V

    .line 8
    new-instance v5, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileName()Ljava/lang/String;

    move-result-object v7

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileSize()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    const/4 v13, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v8, v6

    goto :goto_0

    :cond_4
    move-object v8, v13

    .line 12
    :goto_0
    invoke-virtual {v0}, Lcom/pspdfkit/document/files/EmbeddedFileSource;->getFileDescription()Ljava/lang/String;

    move-result-object v10

    .line 13
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    const/4 v9, 0x0

    const/4 v12, 0x0

    move-object v6, v5

    .line 14
    invoke-direct/range {v6 .. v12}, Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    .line 23
    invoke-virtual {v3}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r1;->d()Lcom/pspdfkit/internal/jni/NativeResourceManager;

    move-result-object v0

    const-string v3, "document.annotationProvider.nativeResourceManager"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {v0, v2, v4, v5}, Lcom/pspdfkit/internal/jni/NativeResourceManager;->createFileResource(Lcom/pspdfkit/internal/jni/NativeAnnotation;Lcom/pspdfkit/internal/jni/NativeDataProvider;Lcom/pspdfkit/internal/jni/NativeFileResourceInformation;)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Annotations"

    const-string v3, "Couldn\'t attach file to annotation."

    .line 26
    invoke-static {v2, v3, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 30
    :cond_5
    new-instance v2, Lcom/pspdfkit/internal/sa;

    iget-object v3, p0, Lcom/pspdfkit/internal/x0;->c:Lcom/pspdfkit/annotations/FileAnnotation;

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-direct {v2, v3, v0}, Lcom/pspdfkit/internal/sa;-><init>(Lcom/pspdfkit/annotations/FileAnnotation;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/x0;->e:Lcom/pspdfkit/internal/sa;

    .line 31
    iput-object v13, p0, Lcom/pspdfkit/internal/x0;->d:Lcom/pspdfkit/document/files/EmbeddedFileSource;

    .line 32
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/x1;->b(Z)V

    const/4 v0, 0x1

    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method public final i()Lcom/pspdfkit/internal/sa;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/x0;->e:Lcom/pspdfkit/internal/sa;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/sa;->a()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

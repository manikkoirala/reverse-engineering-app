.class public final Lcom/pspdfkit/internal/pt;
.super Lcom/pspdfkit/internal/qt;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pt$a;,
        Lcom/pspdfkit/internal/pt$b;,
        Lcom/pspdfkit/internal/pt$c;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/pt$b;


# instance fields
.field private final a:Ljava/util/UUID;

.field private final b:Lcom/pspdfkit/internal/st;

.field private c:Lcom/pspdfkit/internal/av;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/pt$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/pt$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/pt;->Companion:Lcom/pspdfkit/internal/pt$b;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/UUID;Lcom/pspdfkit/internal/st;Lcom/pspdfkit/internal/av;)V
    .locals 2
    .param p3    # Lcom/pspdfkit/internal/st;
        .annotation runtime Lkotlinx/serialization/SerialName;
            value = "textBlock"
        .end annotation
    .end param
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x7

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/pt$a;->a:Lcom/pspdfkit/internal/pt$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/pt$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/qt;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/pt;->a:Ljava/util/UUID;

    iput-object p3, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    iput-object p4, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/fj;
    .locals 13

    .line 35
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 36
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    .line 38
    invoke-virtual {v1}, Lcom/pspdfkit/internal/st;->d()Lcom/pspdfkit/internal/fj;

    move-result-object v1

    .line 39
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p0, "fallback"

    .line 40
    invoke-static {v1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance p0, Lcom/pspdfkit/internal/fj;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 157
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->b()Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v2

    :goto_1
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 158
    new-instance v4, Lcom/pspdfkit/internal/ph;

    if-eqz p1, :cond_2

    .line 159
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->h()Ljava/lang/Float;

    move-result-object v5

    goto :goto_2

    :cond_2
    move-object v5, v2

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ph;->a()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    goto :goto_3

    :cond_3
    move-object v6, v2

    :goto_3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v7

    invoke-virtual {v7}, Lcom/pspdfkit/internal/ph;->a()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    if-eqz p1, :cond_4

    .line 160
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->i()Ljava/lang/Float;

    move-result-object v6

    goto :goto_4

    :cond_4
    move-object v6, v2

    :goto_4
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/pspdfkit/internal/ph;->b()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    goto :goto_5

    :cond_5
    move-object v7, v2

    :goto_5
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->b()Lcom/pspdfkit/internal/ph;

    move-result-object v8

    invoke-virtual {v8}, Lcom/pspdfkit/internal/ph;->b()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 161
    invoke-direct {v4, v5, v6}, Lcom/pspdfkit/internal/ph;-><init>(FF)V

    .line 165
    new-instance v5, Lcom/pspdfkit/internal/y3;

    .line 166
    new-instance v6, Lcom/pspdfkit/internal/w3;

    if-eqz p1, :cond_6

    .line 167
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, v2

    :goto_6
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lcom/pspdfkit/internal/w3;->a()Ljava/lang/String;

    move-result-object v8

    goto :goto_7

    :cond_7
    move-object v8, v2

    :goto_7
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v9

    invoke-virtual {v9}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v9

    invoke-virtual {v9}, Lcom/pspdfkit/internal/w3;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 168
    new-instance v8, Lcom/pspdfkit/internal/fb;

    if-eqz p1, :cond_8

    .line 169
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->a()Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_8

    :cond_8
    move-object v9, v2

    :goto_8
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v10

    if-eqz v10, :cond_9

    invoke-virtual {v10}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v10

    if-eqz v10, :cond_9

    invoke-virtual {v10}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v10

    if-eqz v10, :cond_9

    invoke-virtual {v10}, Lcom/pspdfkit/internal/fb;->a()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_9

    :cond_9
    move-object v10, v2

    :goto_9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v11

    invoke-virtual {v11}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v11

    invoke-virtual {v11}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v11

    invoke-virtual {v11}, Lcom/pspdfkit/internal/fb;->a()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz p1, :cond_a

    .line 170
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->f()Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_a

    :cond_a
    move-object v10, v2

    :goto_a
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v11

    if-eqz v11, :cond_b

    invoke-virtual {v11}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v11

    if-eqz v11, :cond_b

    invoke-virtual {v11}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v11

    if-eqz v11, :cond_b

    invoke-virtual {v11}, Lcom/pspdfkit/internal/fb;->c()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    goto :goto_b

    :cond_b
    move-object v11, v2

    :goto_b
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v12

    invoke-virtual {v12}, Lcom/pspdfkit/internal/y3;->a()Lcom/pspdfkit/internal/w3;

    move-result-object v12

    invoke-virtual {v12}, Lcom/pspdfkit/internal/w3;->b()Lcom/pspdfkit/internal/fb;

    move-result-object v12

    invoke-virtual {v12}, Lcom/pspdfkit/internal/fb;->c()Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 171
    invoke-direct {v8, v9, v10, v2, v2}, Lcom/pspdfkit/internal/fb;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-direct {v6, v7, v8}, Lcom/pspdfkit/internal/w3;-><init>(Ljava/lang/String;Lcom/pspdfkit/internal/fb;)V

    if-eqz p1, :cond_c

    .line 179
    invoke-virtual {p1}, Lcom/pspdfkit/internal/jt;->g()Ljava/lang/Float;

    move-result-object p1

    goto :goto_c

    :cond_c
    move-object p1, v2

    :goto_c
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y3;->b()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    :cond_d
    invoke-virtual {v1}, Lcom/pspdfkit/internal/fj;->c()Lcom/pspdfkit/internal/y3;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y3;->b()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lcom/pspdfkit/internal/pt;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    .line 180
    invoke-direct {v5, v6, p1}, Lcom/pspdfkit/internal/y3;-><init>(Lcom/pspdfkit/internal/w3;F)V

    .line 181
    invoke-direct {p0, v3, v4, v5}, Lcom/pspdfkit/internal/fj;-><init>(ILcom/pspdfkit/internal/ph;Lcom/pspdfkit/internal/y3;)V

    return-object p0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    move-object p0, p2

    goto :goto_0

    :cond_0
    move-object p0, p1

    :cond_1
    :goto_0
    return-object p0
.end method

.method public static final a(Lcom/pspdfkit/internal/pt;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/qu;->a:Lcom/pspdfkit/internal/qu;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->a:Ljava/util/UUID;

    const/4 v2, 0x0

    .line 3
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/st$a;->a:Lcom/pspdfkit/internal/st$a;

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    const/4 v2, 0x1

    .line 5
    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/av$a;->a:Lcom/pspdfkit/internal/av$a;

    .line 6
    iget-object p0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    const/4 v1, 0x2

    .line 7
    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method

.method public static b(Lcom/pspdfkit/internal/av;)V
    .locals 1

    const-string v0, "updateInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-virtual {p0, p0}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/av;)V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    const/4 v0, 0x1

    .line 269
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/pt;->a(IZ)Lcom/pspdfkit/internal/pt$c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt$c;->a()Lcom/pspdfkit/internal/ra;

    move-result-object p1

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ra;->b()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public final a(IZ)Lcom/pspdfkit/internal/pt$c;
    .locals 4

    if-ltz p1, :cond_1

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v0

    .line 26
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v1, p1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ah;

    .line 27
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ah;->b()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 28
    invoke-virtual {v2}, Lcom/pspdfkit/internal/ah;->b()I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Lcom/pspdfkit/internal/pt$c;

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/ah;->a(I)Lcom/pspdfkit/internal/ra;

    move-result-object p2

    invoke-direct {p1, v2, p2}, Lcom/pspdfkit/internal/pt$c;-><init>(Lcom/pspdfkit/internal/ah;Lcom/pspdfkit/internal/ra;)V

    return-object p1

    .line 34
    :cond_1
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No TextBlock Element at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " ("

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, ")."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Ljava/util/UUID;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->a:Ljava/util/UUID;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/internal/av;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    iput-object p1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/av;Lcom/pspdfkit/utils/Size;)V
    .locals 2

    const-string v0, "updateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v0

    const-string v1, "<set-?>"

    .line 184
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    iput-object p1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 235
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v1

    if-nez v1, :cond_0

    .line 236
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/d8;->a(Lcom/pspdfkit/internal/fj;)V

    :cond_0
    if-eqz p2, :cond_1

    const-string p1, "pageSize"

    .line 237
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/pspdfkit/internal/pt;->e()Lcom/pspdfkit/internal/bv;

    move-result-object p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/pt;->c()Lcom/pspdfkit/internal/tt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/tt;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/pspdfkit/internal/bv;->a(Lcom/pspdfkit/utils/Size;Lcom/pspdfkit/internal/iv;)V

    :cond_1
    return-void
.end method

.method public final b(I)I
    .locals 4

    const/4 v0, 0x0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/pt;->h()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 4
    :try_start_1
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/pt;->a(IZ)Lcom/pspdfkit/internal/pt$c;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 5
    :catch_0
    :try_start_2
    new-instance p1, Lcom/pspdfkit/internal/pt$c;

    .line 6
    iget-object v2, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 7
    invoke-virtual {v2}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ah;

    .line 8
    iget-object v3, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 9
    invoke-virtual {v3}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ah;

    .line 10
    invoke-virtual {v3}, Lcom/pspdfkit/internal/ah;->a()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ra;

    .line 11
    invoke-direct {p1, v2, v3}, Lcom/pspdfkit/internal/pt$c;-><init>(Lcom/pspdfkit/internal/ah;Lcom/pspdfkit/internal/ra;)V

    .line 12
    :goto_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pt$c;->b()Lcom/pspdfkit/internal/ah;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    :goto_1
    return v0
.end method

.method public final c(I)Lcom/pspdfkit/internal/ah;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ah;

    return-object p1
.end method

.method public final c(Lcom/pspdfkit/internal/av;)Lcom/pspdfkit/internal/jt;
    .locals 3

    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/d8;)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 9
    invoke-virtual {v1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/d8;->a()Lcom/pspdfkit/internal/fj;

    move-result-object v1

    if-nez v1, :cond_0

    .line 10
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 11
    invoke-virtual {v1}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/d8;->a(Lcom/pspdfkit/internal/fj;)V

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->e()Lcom/pspdfkit/internal/l7;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/l7;)V

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 15
    invoke-virtual {p1}, Lcom/pspdfkit/internal/av;->h()Lcom/pspdfkit/internal/vq;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/av;->a(Lcom/pspdfkit/internal/vq;)V

    .line 16
    invoke-virtual {p0}, Lcom/pspdfkit/internal/pt;->f()Lcom/pspdfkit/internal/jt;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lcom/pspdfkit/internal/tt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    return-object v0
.end method

.method public final e()Lcom/pspdfkit/internal/bv;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/jt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/d8;->d()Lcom/pspdfkit/internal/jt;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/pspdfkit/internal/cb;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/cb;

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    .line 3
    invoke-virtual {v1}, Lcom/pspdfkit/internal/st;->b()Lcom/pspdfkit/internal/p;

    move-result-object v1

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 5
    invoke-virtual {v2}, Lcom/pspdfkit/internal/av;->f()Lcom/pspdfkit/internal/d8;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/d8;->c()Lcom/pspdfkit/internal/jt;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/pspdfkit/internal/pt;->a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/fj;

    move-result-object v2

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/cb;-><init>(Lcom/pspdfkit/internal/p;Lcom/pspdfkit/internal/fj;)V

    return-object v0
.end method

.method public final h()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final i()Lcom/pspdfkit/internal/st;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->b:Lcom/pspdfkit/internal/st;

    return-object v0
.end method

.method public final j()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/av;->g()Lcom/pspdfkit/internal/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/ah;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/ah;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public final k()Lcom/pspdfkit/internal/av;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pt;->c:Lcom/pspdfkit/internal/av;

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/k5;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static final a(Landroid/graphics/PointF;Landroid/graphics/PointF;FZ)F
    .locals 4

    .line 239
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    mul-float v0, v0, v0

    iget v1, p0, Landroid/graphics/PointF;->y:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    mul-float v1, v1, v1

    add-float/2addr v1, v0

    float-to-double v0, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const/high16 v2, 0x3f000000    # 0.5f

    float-to-double v2, v2

    mul-double v0, v0, v2

    float-to-double v2, p2

    div-double/2addr v0, v2

    .line 240
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    .line 241
    iget p2, p1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr p2, v2

    float-to-double v2, p2

    iget p1, p1, Landroid/graphics/PointF;->x:F

    iget p0, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr p1, p0

    float-to-double p0, p1

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p0

    const-wide/16 v2, 0x0

    cmpg-double p2, p0, v2

    if-gez p2, :cond_0

    const p2, 0x40c90fdb

    float-to-double v2, p2

    add-double/2addr p0, v2

    :cond_0
    if-eqz p3, :cond_1

    add-double/2addr p0, v0

    goto :goto_0

    :cond_1
    sub-double/2addr p0, v0

    :goto_0
    double-to-float p0, p0

    return p0
.end method

.method public static final a(Ljava/util/ArrayList;FLandroid/graphics/Path;Z)Landroid/graphics/Path;
    .locals 16

    move-object/from16 v0, p0

    const-string v1, "points"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 63
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    goto :goto_0

    :cond_0
    move-object/from16 v1, p2

    .line 64
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    const/high16 v2, 0x40880000    # 4.25f

    mul-float v2, v2, p1

    const/high16 v3, 0x3fe00000    # 1.75f

    mul-float v3, v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz p3, :cond_1

    .line 69
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    .line 70
    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    const/4 v8, 0x0

    if-nez v7, :cond_3

    .line 71
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    .line 72
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v9, v5

    const/4 v10, 0x1

    const/4 v11, 0x0

    :goto_2
    if-ge v10, v9, :cond_4

    .line 73
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ne v10, v12, :cond_2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    goto :goto_3

    :cond_2
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    :goto_3
    check-cast v12, Landroid/graphics/PointF;

    .line 74
    iget v13, v7, Landroid/graphics/PointF;->x:F

    iget v14, v12, Landroid/graphics/PointF;->y:F

    mul-float v13, v13, v14

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget v14, v12, Landroid/graphics/PointF;->x:F

    mul-float v7, v7, v14

    sub-float/2addr v13, v7

    add-float/2addr v11, v13

    add-int/lit8 v10, v10, 0x1

    move-object v7, v12

    goto :goto_2

    :cond_3
    const/4 v11, 0x0

    :cond_4
    cmpl-float v7, v11, v8

    if-ltz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_4

    :cond_5
    const/4 v7, 0x0

    .line 75
    :goto_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-eqz v6, :cond_6

    .line 77
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v9, v5

    goto :goto_5

    :cond_6
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v9

    :goto_5
    const/4 v10, 0x1

    :goto_6
    if-ge v10, v9, :cond_a

    add-int/lit8 v11, v10, -0x1

    .line 79
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    .line 81
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ne v10, v12, :cond_7

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    goto :goto_7

    :cond_7
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    :goto_7
    check-cast v12, Landroid/graphics/PointF;

    .line 84
    invoke-static {v11, v12}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v13

    if-eqz v13, :cond_8

    goto :goto_9

    .line 85
    :cond_8
    iget v13, v11, Landroid/graphics/PointF;->x:F

    iget v14, v11, Landroid/graphics/PointF;->y:F

    iget v15, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    sub-float/2addr v13, v15

    mul-float v13, v13, v13

    sub-float/2addr v14, v4

    mul-float v14, v14, v14

    add-float/2addr v14, v13

    float-to-double v13, v14

    .line 86
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    double-to-float v4, v13

    div-float/2addr v4, v3

    float-to-int v4, v4

    add-int/2addr v4, v5

    .line 87
    iget v13, v12, Landroid/graphics/PointF;->x:F

    iget v14, v11, Landroid/graphics/PointF;->x:F

    sub-float/2addr v13, v14

    int-to-float v15, v4

    div-float/2addr v13, v15

    .line 88
    iget v12, v12, Landroid/graphics/PointF;->y:F

    iget v11, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v12, v11

    div-float/2addr v12, v15

    add-int/2addr v4, v5

    const/4 v15, 0x0

    :goto_8
    if-ge v15, v4, :cond_9

    .line 93
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, v14, v11}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v14, v13

    add-float/2addr v11, v12

    add-int/lit8 v15, v15, 0x1

    const/4 v5, 0x1

    goto :goto_8

    :cond_9
    :goto_9
    add-int/lit8 v10, v10, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    goto :goto_6

    .line 94
    :cond_a
    invoke-static {v8, v2, v7, v6, v1}, Lcom/pspdfkit/internal/k5;->a(Ljava/util/ArrayList;FZZLandroid/graphics/Path;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Ljava/util/ArrayList;FZZLandroid/graphics/Path;)Landroid/graphics/Path;
    .locals 27

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v10, p4

    .line 95
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v3, 0x2

    if-ge v11, v3, :cond_0

    return-object v10

    .line 100
    :cond_0
    invoke-static/range {p0 .. p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v11, :cond_20

    .line 104
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v13, v5

    check-cast v13, Landroid/graphics/PointF;

    .line 107
    invoke-static {v13, v3}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v5, v4, 0x1

    .line 112
    rem-int/2addr v5, v11

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    move v14, v4

    .line 116
    :goto_1
    invoke-static {v13, v5}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v6

    if-eqz v6, :cond_3

    add-int/lit8 v14, v14, 0x1

    .line 117
    rem-int v5, v14, v11

    if-ne v4, v5, :cond_2

    return-object v10

    :cond_2
    add-int/lit8 v5, v14, 0x1

    .line 121
    rem-int/2addr v5, v11

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    goto :goto_1

    :cond_3
    if-nez p3, :cond_5

    .line 125
    invoke-static/range {p0 .. p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    invoke-static {v13, v4}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static/range {p0 .. p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    invoke-static {v13, v4}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    move/from16 v20, v14

    goto/16 :goto_13

    .line 131
    :cond_5
    invoke-static {v13, v3, v1, v2}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;FZ)F

    move-result v4

    xor-int/lit8 v6, v2, 0x1

    .line 132
    invoke-static {v13, v5, v1, v6}, Lcom/pspdfkit/internal/k5;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;FZ)F

    move-result v6

    .line 133
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_6

    add-int/lit8 v4, v14, 0x1

    .line 134
    iget v6, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 135
    :cond_6
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    const v5, 0x40490fdb    # (float)Math.PI

    if-eqz v3, :cond_7

    add-float v4, v6, v5

    .line 136
    :cond_7
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_8

    add-float v6, v4, v5

    :cond_8
    if-eqz v2, :cond_9

    move v15, v4

    goto :goto_2

    :cond_9
    move v15, v6

    :goto_2
    const v3, 0x40c90fdb

    const/4 v5, 0x0

    sub-float v7, v4, v6

    if-eqz v2, :cond_b

    cmpl-float v7, v7, v5

    if-lez v7, :cond_a

    sub-float/2addr v3, v4

    add-float/2addr v3, v6

    goto :goto_3

    :cond_a
    sub-float v3, v6, v4

    goto :goto_3

    :cond_b
    cmpl-float v8, v7, v5

    if-lez v8, :cond_c

    move v3, v7

    goto :goto_3

    :cond_c
    sub-float/2addr v3, v6

    add-float/2addr v3, v4

    :goto_3
    const v4, 0x3e99999a    # 0.3f

    add-float/2addr v3, v4

    if-nez v2, :cond_1e

    const-string v4, "path"

    .line 137
    invoke-static {v10, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "center"

    invoke-static {v13, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v6, 0x3fc90fdb

    const/4 v7, 0x1

    cmpg-float v4, v4, v6

    if-gtz v4, :cond_d

    move/from16 v16, v3

    goto :goto_5

    .line 139
    :cond_d
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v6

    if-gtz v4, :cond_e

    const/4 v4, 0x1

    goto :goto_4

    .line 143
    :cond_e
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v4, v6

    float-to-int v4, v4

    :goto_4
    int-to-float v4, v4

    div-float v4, v3, v4

    move/from16 v16, v4

    :goto_5
    cmpl-float v4, v16, v5

    if-lez v4, :cond_f

    const/16 v17, 0x1

    goto :goto_6

    :cond_f
    const/16 v17, 0x0

    :goto_6
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v16, v4

    .line 144
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_10

    const/4 v3, 0x1

    goto :goto_7

    .line 148
    :cond_10
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v3, v6

    float-to-int v3, v3

    :goto_7
    const v5, 0x3faaaaab

    float-to-double v5, v5

    const/high16 v8, 0x3f800000    # 1.0f

    float-to-double v8, v8

    move-object/from16 v19, v13

    float-to-double v12, v4

    .line 149
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    sub-double v8, v8, v20

    mul-double v8, v8, v5

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    div-double/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    double-to-float v12, v4

    if-eqz v2, :cond_11

    const/4 v13, 0x0

    .line 154
    invoke-static {v13, v3}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    goto :goto_8

    :cond_11
    const/4 v13, 0x0

    .line 156
    invoke-static {v13, v3}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    invoke-static {v3}, Lkotlin/ranges/RangesKt;->reversed(Lkotlin/ranges/IntProgression;)Lkotlin/ranges/IntProgression;

    move-result-object v3

    .line 159
    :goto_8
    invoke-virtual {v3}, Lkotlin/ranges/IntProgression;->getFirst()I

    move-result v4

    invoke-virtual {v3}, Lkotlin/ranges/IntProgression;->getLast()I

    move-result v9

    invoke-virtual {v3}, Lkotlin/ranges/IntProgression;->getStep()I

    move-result v18

    if-lez v18, :cond_12

    if-le v4, v9, :cond_13

    :cond_12
    if-gez v18, :cond_1d

    if-gt v9, v4, :cond_1d

    :cond_13
    move v8, v4

    :goto_9
    int-to-float v3, v8

    mul-float v3, v3, v16

    add-float/2addr v3, v15

    add-int/lit8 v4, v8, 0x1

    int-to-float v4, v4

    mul-float v4, v4, v16

    add-float/2addr v4, v15

    float-to-double v5, v3

    move/from16 v20, v14

    .line 162
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    double-to-float v3, v13

    float-to-double v13, v4

    move/from16 v23, v8

    move/from16 v22, v9

    .line 163
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v4, v8

    .line 164
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    double-to-float v5, v5

    .line 165
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v6, v8

    move-object/from16 v13, v19

    .line 168
    iget v8, v13, Landroid/graphics/PointF;->x:F

    mul-float v9, v1, v3

    add-float/2addr v9, v8

    .line 169
    iget v14, v13, Landroid/graphics/PointF;->y:F

    mul-float v19, v1, v5

    add-float v0, v19, v14

    mul-float v19, v12, v5

    if-eqz v17, :cond_14

    sub-float v19, v3, v19

    goto :goto_a

    :cond_14
    add-float v19, v19, v3

    :goto_a
    mul-float v19, v19, v1

    add-float v19, v19, v8

    mul-float v3, v3, v12

    if-eqz v17, :cond_15

    add-float/2addr v3, v5

    mul-float v3, v3, v1

    add-float/2addr v3, v14

    move/from16 v24, v3

    goto :goto_b

    :cond_15
    sub-float/2addr v5, v3

    mul-float v5, v5, v1

    add-float/2addr v5, v14

    move/from16 v24, v5

    :goto_b
    mul-float v3, v12, v6

    if-eqz v17, :cond_16

    add-float/2addr v3, v4

    goto :goto_c

    :cond_16
    sub-float v3, v4, v3

    :goto_c
    mul-float v3, v3, v1

    add-float/2addr v3, v8

    move/from16 v25, v3

    mul-float v3, v12, v4

    if-eqz v17, :cond_17

    sub-float v3, v6, v3

    goto :goto_d

    :cond_17
    add-float/2addr v3, v6

    :goto_d
    mul-float v3, v3, v1

    add-float/2addr v3, v14

    move/from16 v26, v3

    mul-float v3, v1, v4

    add-float/2addr v8, v3

    mul-float v3, v1, v6

    add-float/2addr v14, v3

    if-eqz v2, :cond_1a

    if-eqz v7, :cond_19

    .line 206
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Path;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 207
    invoke-virtual {v10, v9, v0}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_e

    .line 209
    :cond_18
    invoke-virtual {v10, v9, v0}, Landroid/graphics/Path;->lineTo(FF)V

    :goto_e
    const/4 v0, 0x0

    goto :goto_f

    :cond_19
    move v0, v7

    :goto_f
    move-object/from16 v3, p4

    move/from16 v4, v19

    move/from16 v5, v24

    move/from16 v6, v25

    move/from16 v7, v26

    move/from16 v9, v23

    move/from16 v2, v22

    move/from16 v22, v12

    move v12, v9

    move v9, v14

    .line 213
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v7, v0

    goto :goto_12

    :cond_1a
    move/from16 v2, v22

    move/from16 v22, v12

    move/from16 v12, v23

    if-eqz v7, :cond_1c

    .line 216
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Path;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 217
    invoke-virtual {v10, v8, v14}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_10

    .line 219
    :cond_1b
    invoke-virtual {v10, v8, v14}, Landroid/graphics/Path;->lineTo(FF)V

    :goto_10
    const/4 v14, 0x0

    goto :goto_11

    :cond_1c
    move v14, v7

    :goto_11
    move-object/from16 v3, p4

    move/from16 v4, v25

    move/from16 v5, v26

    move/from16 v6, v19

    move/from16 v7, v24

    move v8, v9

    move v9, v0

    .line 223
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v7, v14

    :goto_12
    if-eq v12, v2, :cond_1f

    add-int v8, v12, v18

    move-object/from16 v0, p0

    move v9, v2

    move-object/from16 v19, v13

    move/from16 v14, v20

    move/from16 v12, v22

    const/4 v13, 0x0

    move/from16 v2, p2

    goto/16 :goto_9

    :cond_1d
    move/from16 v20, v14

    move-object/from16 v13, v19

    goto :goto_13

    :cond_1e
    move/from16 v20, v14

    .line 224
    new-instance v0, Landroid/graphics/RectF;

    .line 225
    iget v2, v13, Landroid/graphics/PointF;->x:F

    sub-float v4, v2, v1

    .line 226
    iget v5, v13, Landroid/graphics/PointF;->y:F

    sub-float v6, v5, v1

    add-float/2addr v2, v1

    add-float/2addr v5, v1

    .line 227
    invoke-direct {v0, v4, v6, v2, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    float-to-double v4, v15

    .line 234
    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v2, v4

    float-to-double v3, v3

    .line 236
    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    double-to-float v3, v3

    .line 237
    invoke-virtual {v10, v0, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    :cond_1f
    :goto_13
    add-int/lit8 v4, v20, 0x1

    move-object/from16 v0, p0

    move/from16 v2, p2

    move-object v3, v13

    goto/16 :goto_0

    :cond_20
    if-eqz p3, :cond_21

    const/4 v0, 0x3

    if-lt v11, v0, :cond_21

    .line 238
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Path;->close()V

    :cond_21
    return-object v10
.end method

.method public static final a(Landroid/graphics/RectF;FLandroid/graphics/Path;)V
    .locals 16

    move-object/from16 v0, p0

    const-string v1, "rect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 1
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    goto :goto_0

    :cond_0
    move-object/from16 v1, p2

    :goto_0
    const/high16 v2, 0x40880000    # 4.25f

    mul-float v2, v2, p1

    .line 5
    invoke-virtual {v0, v2, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 8
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 9
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    cmpg-float v8, v3, v7

    if-nez v8, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    if-nez v8, :cond_f

    cmpg-float v8, v5, v7

    if-nez v8, :cond_2

    const/4 v8, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x0

    :goto_2
    if-eqz v8, :cond_3

    goto/16 :goto_9

    .line 14
    :cond_3
    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/16 v8, 0x168

    .line 17
    new-instance v9, Ljava/util/ArrayList;

    const/16 v10, 0x169

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    int-to-float v11, v8

    const v12, 0x40c90fdb

    div-float/2addr v12, v11

    .line 20
    iget v11, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v3

    .line 21
    iget v0, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v5

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_3
    if-ge v13, v10, :cond_4

    float-to-double v7, v14

    move v15, v5

    .line 25
    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float v4, v4, v3

    add-float/2addr v4, v11

    .line 26
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    double-to-float v5, v7

    mul-float v5, v5, v15

    add-float/2addr v5, v0

    .line 27
    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v14, v12

    add-int/lit8 v13, v13, 0x1

    move v5, v15

    const/4 v7, 0x0

    const/16 v8, 0x168

    goto :goto_3

    .line 28
    :cond_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 29
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 30
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gt v6, v4, :cond_6

    move-object v5, v3

    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 31
    :goto_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v7, v8, :cond_5

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto :goto_5

    :cond_5
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    :goto_5
    check-cast v8, Landroid/graphics/PointF;

    .line 32
    iget v10, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v11, v8, Landroid/graphics/PointF;->x:F

    iget v12, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    mul-float v10, v10, v10

    sub-float/2addr v5, v12

    mul-float v5, v5, v5

    add-float/2addr v5, v10

    float-to-double v10, v5

    .line 33
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v5, v10

    add-float/2addr v3, v5

    if-eq v7, v4, :cond_7

    add-int/lit8 v7, v7, 0x1

    move-object v5, v8

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    :cond_7
    const/high16 v0, 0x3fe00000    # 1.75f

    mul-float v0, v0, v2

    div-float v4, v3, v0

    float-to-int v4, v4

    int-to-float v4, v4

    const/4 v5, 0x0

    cmpl-float v7, v4, v5

    if-lez v7, :cond_8

    rem-float/2addr v3, v0

    div-float v5, v3, v4

    goto :goto_6

    :cond_8
    const/4 v5, 0x0

    :goto_6
    add-float/2addr v0, v5

    .line 34
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x168

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_7
    add-int/lit8 v7, v4, -0x1

    .line 41
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    const-string v8, "points[i - 1]"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Landroid/graphics/PointF;

    .line 42
    rem-int/lit16 v8, v4, 0x168

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    const-string v10, "points[i % pointsCount]"

    invoke-static {v8, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Landroid/graphics/PointF;

    .line 43
    iget v10, v7, Landroid/graphics/PointF;->x:F

    iget v11, v7, Landroid/graphics/PointF;->y:F

    iget v12, v8, Landroid/graphics/PointF;->x:F

    iget v13, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v12

    mul-float v10, v10, v10

    sub-float/2addr v11, v13

    mul-float v11, v11, v11

    add-float/2addr v11, v10

    float-to-double v10, v11

    .line 44
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v10, v10

    div-float/2addr v10, v0

    .line 45
    iget v11, v8, Landroid/graphics/PointF;->x:F

    iget v12, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v11, v12

    div-float/2addr v11, v10

    .line 46
    iget v13, v8, Landroid/graphics/PointF;->y:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v13, v7

    div-float/2addr v13, v10

    div-float v10, v11, v0

    mul-float v10, v10, v5

    div-float v14, v13, v0

    mul-float v14, v14, v5

    add-float/2addr v12, v10

    add-float/2addr v7, v14

    const/4 v5, 0x0

    :goto_8
    cmpg-float v10, v11, v5

    if-gtz v10, :cond_9

    .line 52
    iget v10, v8, Landroid/graphics/PointF;->x:F

    cmpl-float v10, v12, v10

    if-gez v10, :cond_a

    :cond_9
    cmpl-float v10, v11, v5

    if-ltz v10, :cond_d

    iget v10, v8, Landroid/graphics/PointF;->x:F

    cmpg-float v10, v12, v10

    if-gtz v10, :cond_d

    :cond_a
    cmpg-float v10, v13, v5

    if-gtz v10, :cond_b

    .line 53
    iget v10, v8, Landroid/graphics/PointF;->y:F

    cmpl-float v10, v7, v10

    if-gez v10, :cond_c

    :cond_b
    cmpl-float v10, v13, v5

    if-ltz v10, :cond_d

    iget v10, v8, Landroid/graphics/PointF;->y:F

    cmpg-float v10, v7, v10

    if-gtz v10, :cond_d

    .line 55
    :cond_c
    new-instance v10, Landroid/graphics/PointF;

    invoke-direct {v10, v12, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v12, v11

    add-float/2addr v7, v13

    goto :goto_8

    .line 60
    :cond_d
    iget v7, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v12

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v8

    div-float v8, v0, v8

    mul-float v7, v7, v8

    const/16 v8, 0x168

    if-eq v4, v8, :cond_e

    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto/16 :goto_7

    .line 61
    :cond_e
    invoke-static {v3, v2, v6, v6, v1}, Lcom/pspdfkit/internal/k5;->a(Ljava/util/ArrayList;FZZLandroid/graphics/Path;)Landroid/graphics/Path;

    return-void

    .line 62
    :cond_f
    :goto_9
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    return-void
.end method

.method static a(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 2

    .line 242
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget p0, p0, Landroid/graphics/PointF;->y:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result p0

    cmpg-float p0, p0, v1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

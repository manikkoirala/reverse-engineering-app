.class public final Lcom/pspdfkit/internal/c9;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/t8;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/c9$a;
    }
.end annotation


# instance fields
.field private b:Lcom/pspdfkit/internal/s8;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private e:Lcom/pspdfkit/internal/p8;

.field private f:Lcom/pspdfkit/internal/b9;

.field private g:Lcom/pspdfkit/internal/rl;


# direct methods
.method public static synthetic $r8$lambda$TFOj1ebfNC0WQvVR7OtdbxUM5AM(Lcom/pspdfkit/internal/c9;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/c9;->a(Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/pspdfkit/internal/c9;->f:Lcom/pspdfkit/internal/b9;

    .line 10
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/c9;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$layout;->pspdf__document_info_view:I

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__document_info_view:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/p8;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/p8;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    .line 7
    sget p1, Lcom/pspdfkit/R$id;->pspdf__document_info_list_view:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/pspdfkit/internal/c9;->c:Landroid/widget/ListView;

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 10
    sget p1, Lcom/pspdfkit/R$id;->pspdf__document_info_edit_fab:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object p1, p0, Lcom/pspdfkit/internal/c9;->d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/c9$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/c9$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/c9;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/pspdfkit/internal/rl;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget v1, p2, Lcom/pspdfkit/internal/rl;->M:I

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundColor(I)V

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p8;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    iget v0, p2, Lcom/pspdfkit/internal/rl;->P:I

    goto :goto_0

    .line 23
    :cond_1
    iget v0, p2, Lcom/pspdfkit/internal/rl;->O:I

    .line 24
    :goto_0
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget p2, p2, Lcom/pspdfkit/internal/rl;->N:I

    .line 30
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 31
    invoke-static {p1, p2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 32
    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 0

    .line 14
    iget-object p1, p0, Lcom/pspdfkit/internal/c9;->b:Lcom/pspdfkit/internal/s8;

    if-eqz p1, :cond_0

    .line 15
    check-cast p1, Lcom/pspdfkit/internal/a9;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/a9;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p8;->a()V

    .line 34
    invoke-static {p0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/c9;->g:Lcom/pspdfkit/internal/rl;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/c9;->a(Landroid/content/Context;Lcom/pspdfkit/internal/rl;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 1

    .line 16
    iget v0, p1, Lcom/pspdfkit/internal/rl;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 17
    iput-object p1, p0, Lcom/pspdfkit/internal/c9;->g:Lcom/pspdfkit/internal/rl;

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/c9;->a(Landroid/content/Context;Lcom/pspdfkit/internal/rl;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/p8;->a(Lcom/pspdfkit/internal/rl;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p8;->b()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/c9;->g:Lcom/pspdfkit/internal/rl;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/c9;->a(Landroid/content/Context;Lcom/pspdfkit/internal/rl;)V

    return-void
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p8;->c()Z

    move-result v0

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u8;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/p8;->getItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/c9$a;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/c9$a;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-object p1, p1, Lcom/pspdfkit/internal/c9$a;->a:Lcom/pspdfkit/internal/b9;

    iput-object p1, p0, Lcom/pspdfkit/internal/c9;->f:Lcom/pspdfkit/internal/b9;

    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/c9$a;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/c9$a;-><init>(Landroid/os/Parcelable;)V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/c9;->b:Lcom/pspdfkit/internal/s8;

    if-eqz v1, :cond_0

    .line 3
    check-cast v1, Lcom/pspdfkit/internal/a9;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/a9;->a()Lcom/pspdfkit/internal/b9;

    move-result-object v1

    .line 4
    instance-of v2, v1, Lcom/pspdfkit/internal/b9;

    if-eqz v2, :cond_0

    .line 5
    iput-object v1, v0, Lcom/pspdfkit/internal/c9$a;->a:Lcom/pspdfkit/internal/b9;

    :cond_0
    return-object v0
.end method

.method public setEditingEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/c9;->d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/c9;->d:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u8;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->e:Lcom/pspdfkit/internal/p8;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/p8;->setItems(Ljava/util/List;)V

    return-void
.end method

.method public setPresenter(Lcom/pspdfkit/internal/s8;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->b:Lcom/pspdfkit/internal/s8;

    if-eqz v0, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/a9;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/a9;->c()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/c9;->b:Lcom/pspdfkit/internal/s8;

    :cond_0
    if-eqz p1, :cond_1

    .line 7
    iput-object p1, p0, Lcom/pspdfkit/internal/c9;->b:Lcom/pspdfkit/internal/s8;

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/c9;->f:Lcom/pspdfkit/internal/b9;

    check-cast p1, Lcom/pspdfkit/internal/a9;

    .line 9
    invoke-virtual {p1, p0, v0}, Lcom/pspdfkit/internal/a9;->a(Lcom/pspdfkit/internal/t8;Lcom/pspdfkit/internal/b9;)V

    :cond_1
    return-void
.end method

.class public Lcom/pspdfkit/internal/ek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;


# instance fields
.field private final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field c:Lcom/pspdfkit/internal/zf;

.field private d:Lcom/pspdfkit/internal/dm;

.field private e:I

.field private f:Landroid/graphics/Point;

.field private final g:Landroid/content/Context;

.field private final h:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;


# direct methods
.method static bridge synthetic -$$Nest$fgetb(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/specialMode/handler/a;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/dm;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/ek;->d:Lcom/pspdfkit/internal/dm;

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/ek;->h:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ek;->g:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/pspdfkit/annotations/NoteAnnotation;)V
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 34
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v2, p0, Lcom/pspdfkit/internal/ek;->h:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)I

    move-result v0

    .line 35
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ek;->d:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/ek;->c:Lcom/pspdfkit/internal/zf;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ek;->d:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ek;->e:I

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->f:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/internal/ek;->g:Landroid/content/Context;

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v5, v5

    .line 9
    invoke-static {v2, v3, v0, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {v0, v2, v3, v4, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/ek;->d:Lcom/pspdfkit/internal/dm;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    const/high16 p1, -0x3e800000    # -16.0f

    .line 14
    invoke-virtual {v0, p1, p1}, Landroid/graphics/RectF;->inset(FF)V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    .line 16
    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p1

    sget-object v3, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v4, p0, Lcom/pspdfkit/internal/ek;->h:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    .line 17
    invoke-interface {p1, v3, v4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getNoteAnnotationIcon(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/lang/String;

    move-result-object p1

    .line 18
    new-instance v3, Lcom/pspdfkit/annotations/NoteAnnotation;

    iget v4, p0, Lcom/pspdfkit/internal/ek;->e:I

    const-string v5, ""

    invoke-direct {v3, v4, v0, v5, p1}, Lcom/pspdfkit/annotations/NoteAnnotation;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/ek;->a(Lcom/pspdfkit/annotations/NoteAnnotation;)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/ek;->c:Lcom/pspdfkit/internal/zf;

    invoke-interface {p1}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p1

    .line 22
    invoke-interface {p1, v3}, Lcom/pspdfkit/annotations/AnnotationProvider;->addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 23
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const/4 v4, 0x5

    .line 24
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    .line 25
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    .line 26
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object p1

    new-instance v0, Lcom/pspdfkit/internal/dk;

    invoke-direct {v0, p0, v3}, Lcom/pspdfkit/internal/dk;-><init>(Lcom/pspdfkit/internal/ek;Lcom/pspdfkit/annotations/NoteAnnotation;)V

    .line 27
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/core/CompletableObserver;)V

    .line 28
    iput-object v2, p0, Lcom/pspdfkit/internal/ek;->f:Landroid/graphics/Point;

    return v1

    .line 30
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 31
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v0, v2, p1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/ek;->f:Landroid/graphics/Point;

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->h:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ek;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.class public interface abstract Lcom/pspdfkit/internal/a7;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/pspdfkit/internal/pt;Lcom/pspdfkit/internal/jt;)Lcom/pspdfkit/internal/o6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Lcom/pspdfkit/internal/jt;",
            ")",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;I)Lcom/pspdfkit/internal/o6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/pspdfkit/internal/pt;Ljava/lang/String;II)Lcom/pspdfkit/internal/o6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/pspdfkit/internal/l6;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation
.end method

.method public abstract b(Lcom/pspdfkit/internal/pt;II)Lcom/pspdfkit/internal/o6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/pt;",
            "II)",
            "Lcom/pspdfkit/internal/o6<",
            "Lcom/pspdfkit/internal/av;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lcom/pspdfkit/internal/l6;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/undo/exceptions/UndoEditFailedException;
        }
    .end annotation
.end method

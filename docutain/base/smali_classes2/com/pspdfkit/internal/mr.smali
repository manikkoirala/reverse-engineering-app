.class final Lcom/pspdfkit/internal/mr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/listeners/DocumentListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic onDocumentClick()Z
    .locals 1

    invoke-static {p0}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentClick(Lcom/pspdfkit/listeners/DocumentListener;)Z

    move-result v0

    return v0
.end method

.method public synthetic onDocumentLoadFailed(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoadFailed(Lcom/pspdfkit/listeners/DocumentListener;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentLoaded(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentLoaded(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public final onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public synthetic onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveCancelled(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaveFailed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V

    return-void
.end method

.method public synthetic onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentSaved(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;)V

    return-void
.end method

.method public synthetic onDocumentZoomed(Lcom/pspdfkit/document/PdfDocument;IF)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onDocumentZoomed(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;IF)V

    return-void
.end method

.method public synthetic onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageChanged(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.method public synthetic onPageClick(Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageClick(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;ILandroid/view/MotionEvent;Landroid/graphics/PointF;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    return p1
.end method

.method public synthetic onPageUpdated(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/listeners/DocumentListener$-CC;->$default$onPageUpdated(Lcom/pspdfkit/listeners/DocumentListener;Lcom/pspdfkit/document/PdfDocument;I)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/ht;
.super Lcom/pspdfkit/internal/wk;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/wk<",
        "Lcom/pspdfkit/internal/zk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;


# direct methods
.method public static synthetic $r8$lambda$gLJYhmEiiRsk88To4HcFt72rwSg(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ht;->a(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wk;-><init>(Landroid/view/View;)V

    .line 2
    sget v0, Lcom/pspdfkit/R$id;->pspdf__style_box_card:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->a:Landroid/widget/LinearLayout;

    .line 3
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->b:Landroid/view/View;

    .line 4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_detail_view_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->c:Landroid/widget/LinearLayout;

    .line 5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_preview_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->d:Landroid/widget/ImageView;

    .line 6
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_current_style:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->e:Landroid/widget/TextView;

    .line 7
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_chevron:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/ht;->f:Landroid/widget/ImageView;

    .line 8
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_style_box_details:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    iput-object p1, p0, Lcom/pspdfkit/internal/ht;->g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 48
    check-cast p0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/uk;->g()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/zk;Lcom/pspdfkit/internal/jk;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ht;->b:Landroid/view/View;

    new-instance v1, Lcom/pspdfkit/internal/ht$$ExternalSyntheticLambda0;

    invoke-direct {v1, p2}, Lcom/pspdfkit/internal/ht$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/jk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 9
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 11
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->b(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->c()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    .line 14
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 15
    sget v6, Lcom/pspdfkit/R$color;->pspdf__note_editor_style_box_icon_tint:I

    .line 16
    invoke-static {v2, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 17
    invoke-static {v6, v4}, Landroidx/core/graphics/ColorUtils;->compositeColors(II)I

    move-result v4

    .line 19
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v4, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 20
    iget-object v4, p0, Lcom/pspdfkit/internal/ht;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 21
    iget-object v3, p0, Lcom/pspdfkit/internal/ht;->d:Landroid/widget/ImageView;

    .line 22
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->a(Ljava/lang/String;)I

    move-result v4

    const/4 v6, 0x0

    .line 23
    invoke-static {v2, v4, v6}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 26
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->d:Landroid/widget/ImageView;

    const/16 v3, 0x12c

    .line 27
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_0

    .line 28
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 29
    :cond_0
    new-instance v6, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v7, 0x2

    new-array v7, v7, [Landroid/graphics/drawable/Drawable;

    aput-object v4, v7, v1

    const/4 v4, 0x1

    aput-object v5, v7, v4

    invoke-direct {v6, v7}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 30
    invoke-virtual {v6, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 31
    invoke-static {v2, v6}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 32
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 37
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->b()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 39
    iget-object v2, p0, Lcom/pspdfkit/internal/ht;->g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    invoke-virtual {v2, p2}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->setAdapterCallbacks(Lcom/pspdfkit/internal/jk;)V

    .line 40
    iget-object p2, p0, Lcom/pspdfkit/internal/ht;->g:Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/annotations/note/ui/NoteEditorStyleBoxDetailsView;->setSelectedIconItem(Ljava/lang/String;)V

    .line 42
    iget-object p2, p0, Lcom/pspdfkit/internal/ht;->f:Landroid/widget/ImageView;

    invoke-static {p2}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->rotation(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 43
    iget-object p2, p0, Lcom/pspdfkit/internal/ht;->a:Landroid/widget/LinearLayout;

    invoke-static {p2}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 44
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zk;->f()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 45
    iget-object p1, p0, Lcom/pspdfkit/internal/ht;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 47
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/ht;->c:Landroid/widget/LinearLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

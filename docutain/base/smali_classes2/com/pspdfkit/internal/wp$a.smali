.class public final Lcom/pspdfkit/internal/wp$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/wp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/wp$a$a;,
        Lcom/pspdfkit/internal/wp$a$b;
    }
.end annotation

.annotation runtime Lkotlinx/serialization/Serializable;
.end annotation


# static fields
.field public static final Companion:Lcom/pspdfkit/internal/wp$a$b;


# instance fields
.field private final a:Ljava/util/UUID;

.field private final b:I

.field private final c:Lcom/pspdfkit/internal/cb;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/pspdfkit/internal/wp$a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/wp$a$b;-><init>(I)V

    sput-object v0, Lcom/pspdfkit/internal/wp$a;->Companion:Lcom/pspdfkit/internal/wp$a$b;

    return-void
.end method

.method private constructor <init>(ILjava/util/UUID;Lkotlin/UInt;Lcom/pspdfkit/internal/cb;)V
    .locals 2

    and-int/lit8 v0, p1, 0x7

    const/4 v1, 0x7

    if-eq v1, v0, :cond_0

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/wp$a$a;->a:Lcom/pspdfkit/internal/wp$a$a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wp$a$a;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lkotlinx/serialization/internal/PluginExceptionsKt;->throwMissingFieldException(IILkotlinx/serialization/descriptors/SerialDescriptor;)V

    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/wp$a;->a:Ljava/util/UUID;

    invoke-virtual {p3}, Lkotlin/UInt;->unbox-impl()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/wp$a;->b:I

    iput-object p4, p0, Lcom/pspdfkit/internal/wp$a;->c:Lcom/pspdfkit/internal/cb;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/UUID;Lkotlin/UInt;Lcom/pspdfkit/internal/cb;I)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "This synthesized declaration should not be used directly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = ""
            imports = {}
        .end subannotation
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/wp$a;-><init>(ILjava/util/UUID;Lkotlin/UInt;Lcom/pspdfkit/internal/cb;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/pspdfkit/internal/wp$a;->a:Ljava/util/UUID;

    .line 5
    iput p2, p0, Lcom/pspdfkit/internal/wp$a;->b:I

    .line 6
    iput-object p3, p0, Lcom/pspdfkit/internal/wp$a;->c:Lcom/pspdfkit/internal/cb;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/wp$a;-><init>(Ljava/util/UUID;ILcom/pspdfkit/internal/cb;)V

    return-void
.end method

.method public static final a(Lcom/pspdfkit/internal/wp$a;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "self"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialDesc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/qu;->a:Lcom/pspdfkit/internal/qu;

    iget-object v1, p0, Lcom/pspdfkit/internal/wp$a;->a:Ljava/util/UUID;

    const/4 v2, 0x0

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lkotlinx/serialization/internal/UIntSerializer;->INSTANCE:Lkotlinx/serialization/internal/UIntSerializer;

    iget v1, p0, Lcom/pspdfkit/internal/wp$a;->b:I

    invoke-static {v1}, Lkotlin/UInt;->box-impl(I)Lkotlin/UInt;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, p2, v2, v0, v1}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    sget-object v0, Lcom/pspdfkit/internal/cb$a;->a:Lcom/pspdfkit/internal/cb$a;

    iget-object p0, p0, Lcom/pspdfkit/internal/wp$a;->c:Lcom/pspdfkit/internal/cb;

    const/4 v1, 0x2

    invoke-interface {p1, p2, v1, v0, p0}, Lkotlinx/serialization/encoding/CompositeEncoder;->encodeSerializableElement(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/SerializationStrategy;Ljava/lang/Object;)V

    return-void
.end method

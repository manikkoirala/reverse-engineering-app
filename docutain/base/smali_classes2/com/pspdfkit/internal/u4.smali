.class public final Lcom/pspdfkit/internal/u4;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/zf;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$YTYQFbjvM8CkFkdEspYP2WBgagc(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;Lcom/pspdfkit/utils/Size;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/u4;->a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;Lcom/pspdfkit/utils/Size;)Lio/reactivex/rxjava3/core/MaybeSource;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$akhiLX2E-CF2k9SQPniwNkebfTo(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/u4;->a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/zf;Landroid/content/Context;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    const-string v0, "pdfDocument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/u4;->b:Landroid/content/Context;

    .line 7
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/x5;->c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object p1

    const-string p2, "getPageRenderConfigurati\u2026nfiguration, pdfDocument)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/u4;->c:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 8
    invoke-virtual {p3}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object p1

    const-string p2, "configuration.excludedAnnotationTypes"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/u4;->d:Ljava/util/ArrayList;

    .line 14
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/u4;->f:Ljava/util/ArrayList;

    .line 20
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/u4;->h:Landroid/util/SparseArray;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;Lcom/pspdfkit/utils/Size;)Lio/reactivex/rxjava3/core/MaybeSource;
    .locals 5

    const-string v0, "$bookmark"

    .line 15
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$thumbnailSize"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 16
    iget-object v0, p1, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    const-string v1, "pdfDocument.getPageSize(pageIndex)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget v1, p2, Lcom/pspdfkit/utils/Size;->width:F

    iget v2, v0, Lcom/pspdfkit/utils/Size;->width:F

    div-float/2addr v1, v2

    .line 20
    iget p2, p2, Lcom/pspdfkit/utils/Size;->height:F

    iget v2, v0, Lcom/pspdfkit/utils/Size;->height:F

    div-float/2addr p2, v2

    .line 21
    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 26
    iget v1, v0, Lcom/pspdfkit/utils/Size;->width:F

    mul-float v1, v1, p2

    float-to-int v1, v1

    .line 27
    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    mul-float v0, v0, p2

    float-to-int p2, v0

    .line 29
    new-instance v0, Lcom/pspdfkit/internal/rc$a;

    iget-object v2, p1, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    const/16 v2, 0xa

    .line 30
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    .line 31
    iget-object v2, p1, Lcom/pspdfkit/internal/u4;->c:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/rc$a;

    .line 33
    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/lang/Integer;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 35
    iget-object v0, p1, Lcom/pspdfkit/internal/u4;->d:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    .line 36
    iget-object v0, p1, Lcom/pspdfkit/internal/u4;->b:Landroid/content/Context;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 38
    iget-object v2, p1, Lcom/pspdfkit/internal/u4;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 41
    iget-object v4, p1, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    .line 42
    invoke-virtual {v3, v0, v4, p0}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 48
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 49
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 51
    iget-boolean p1, p1, Lcom/pspdfkit/internal/u4;->e:Z

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/internal/rc$a;

    .line 52
    invoke-virtual {p0}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object p0

    const-string p1, "Builder(pdfDocument, pag\u2026\n                .build()"

    .line 53
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p0}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/rxjava3/core/Single;->toMaybe()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0

    .line 60
    :cond_2
    invoke-static {}, Lio/reactivex/rxjava3/core/Maybe;->empty()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private static final a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;)Ljava/lang/String;
    .locals 14

    const-string v0, "$bookmark"

    .line 9
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 10
    iget-object v0, p1, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageText(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "pdfDocument.getPageText(it)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v3, "\n"

    const-string v4, " \u2022 "

    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const-string v9, "\r"

    const-string v10, ""

    .line 11
    invoke-static/range {v8 .. v13}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "  "

    const-string v2, " "

    .line 12
    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 13
    iget-object p1, p1, Lcom/pspdfkit/internal/u4;->h:Landroid/util/SparseArray;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p1, p0, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/u4;->g:I

    return v0
.end method

.method public final a(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/utils/Size;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            "Lcom/pspdfkit/utils/Size;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    const-string v0, "bookmark"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "thumbnailSize"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/pspdfkit/internal/u4$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1, p0, p2}, Lcom/pspdfkit/internal/u4$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;Lcom/pspdfkit/utils/Size;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string p2, "defer {\n        bookmark\u2026ybe.empty<Bitmap>()\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/bookmarks/Bookmark;)Ljava/lang/String;
    .locals 1

    const-string v0, "bookmark"

    .line 7
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/u4;->h:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    const-string v0, "drawableProviders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/u4;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/u4;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 6
    iget p1, p0, Lcom/pspdfkit/internal/u4;->g:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/pspdfkit/internal/u4;->g:I

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/u4;->e:Z

    .line 2
    iget p1, p0, Lcom/pspdfkit/internal/u4;->g:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/pspdfkit/internal/u4;->g:I

    return-void
.end method

.method public final b(Lcom/pspdfkit/bookmarks/Bookmark;)Ljava/lang/String;
    .locals 2

    const-string v0, "bookmark"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/bookmarks/Bookmark;->getPageIndex()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/u4;->a:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/zf;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final c(Lcom/pspdfkit/bookmarks/Bookmark;)Lio/reactivex/rxjava3/core/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/bookmarks/Bookmark;",
            ")",
            "Lio/reactivex/rxjava3/core/Maybe<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "bookmark"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/u4$$ExternalSyntheticLambda1;

    invoke-direct {v0, p1, p0}, Lcom/pspdfkit/internal/u4$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/bookmarks/Bookmark;Lcom/pspdfkit/internal/u4;)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Maybe;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object p1

    const-string v0, "fromCallable {\n        b\u2026n@fromCallable null\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

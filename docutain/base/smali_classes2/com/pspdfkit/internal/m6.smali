.class public final Lcom/pspdfkit/internal/m6;
.super Lcom/pspdfkit/internal/y6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/y6<",
        "Lcom/pspdfkit/internal/l6;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/internal/a7;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/ContentEditingSpecialModeHandler;Lcom/pspdfkit/internal/k4$a;)V
    .locals 1

    const-string v0, "contentEditor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/y6;-><init>(Lcom/pspdfkit/internal/k4$a;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/m6;->c:Lcom/pspdfkit/internal/a7;

    return-void
.end method


# virtual methods
.method public final f(Lcom/pspdfkit/internal/ja;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/l6;

    const-string v0, "edit"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/pspdfkit/internal/m6;->c:Lcom/pspdfkit/internal/a7;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/a7;->b(Lcom/pspdfkit/internal/l6;)V

    return-void
.end method

.method public final g(Lcom/pspdfkit/internal/ja;)V
    .locals 1

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/l6;

    const-string v0, "edit"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/m6;->c:Lcom/pspdfkit/internal/a7;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/a7;->a(Lcom/pspdfkit/internal/l6;)V

    return-void
.end method

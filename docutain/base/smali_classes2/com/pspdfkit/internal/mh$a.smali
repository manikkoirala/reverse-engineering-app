.class public final Lcom/pspdfkit/internal/mh$a;
.super Lcom/pspdfkit/internal/mh;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/mh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/annotations/Annotation;

.field private final d:J


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Z)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/mh;-><init>(ZI)V

    iput-object p1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 41
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mh$a;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getUuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long p1, p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/mh$a;->d:J

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    const-string v1, "annotation"

    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 379
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->hasInstantComments()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 380
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_instant_comment:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 382
    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    sget-object v4, Lcom/pspdfkit/internal/ao$a;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    move-object v0, v3

    goto/16 :goto_1

    .line 411
    :pswitch_0
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_redaction:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 412
    :pswitch_1
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_polyline:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 413
    :pswitch_2
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_polygon:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 414
    :pswitch_3
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_sound:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 415
    :pswitch_4
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_square:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 416
    :pswitch_5
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_file:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 417
    :pswitch_6
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_widget:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 418
    :pswitch_7
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_richmedia:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 419
    :pswitch_8
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_caret:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    .line 420
    :pswitch_9
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_stamp:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 421
    :pswitch_a
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_line:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 422
    :pswitch_b
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_circle:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 423
    :pswitch_c
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_link:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 424
    :pswitch_d
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_stylus:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 425
    :pswitch_e
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_squiggly:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 426
    :pswitch_f
    check-cast v0, Lcom/pspdfkit/annotations/FreeTextAnnotation;

    .line 427
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/FreeTextAnnotation;->getIntent()Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    move-result-object v0

    .line 428
    sget-object v2, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;->FREE_TEXT_CALLOUT:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextAnnotationIntent;

    if-ne v0, v2, :cond_1

    .line 430
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_freetext_callout:I

    goto :goto_0

    .line 432
    :cond_1
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_freetext:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 433
    :pswitch_10
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_underline:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 434
    :pswitch_11
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_strikeout:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 435
    :pswitch_12
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_highlight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 436
    :pswitch_13
    check-cast v0, Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/NoteAnnotation;->getIconName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "annotation as NoteAnnotation).iconName"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    .line 437
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 439
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 440
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 441
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1136
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_2

    .line 1137
    check-cast v0, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-static {v0}, Lcom/pspdfkit/internal/ws;->a(Lcom/pspdfkit/annotations/StampAnnotation;)I

    move-result v0

    goto :goto_2

    .line 1139
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    :goto_2
    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    move p2, v0

    .line 1140
    :goto_3
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1141
    invoke-static {p1, p2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    return-object p1

    :cond_4
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getCreator()Ljava/lang/String;

    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 12
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getModifiedDate()Ljava/util/Date;

    move-result-object v1

    if-nez v1, :cond_0

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 14
    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getCreatedDate()Ljava/util/Date;

    move-result-object v1

    :cond_0
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 18
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 19
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, v2

    move-object v3, p1

    .line 22
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    aput-object v3, v1, v0

    const/4 v0, 0x2

    aput-object p1, v1, v0

    .line 23
    sget p1, Lcom/pspdfkit/internal/ft;->d:I

    .line 24
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const-string v0, ", "

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z
    .locals 2

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->WIDGET:Lcom/pspdfkit/annotations/AnnotationType;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;I)Z
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1142
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/mh$a;->a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mh;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    if-lt p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 3
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ao;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z
    .locals 2

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/mh;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final d()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/mh$a;->d:J

    return-wide v0
.end method

.method public final e()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$a;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    return v0
.end method

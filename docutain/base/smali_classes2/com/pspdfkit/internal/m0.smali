.class public final Lcom/pspdfkit/internal/m0;
.super Lcom/pspdfkit/internal/b1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/m0$a;
    }
.end annotation


# instance fields
.field private final d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;


# direct methods
.method public static synthetic $r8$lambda$11jKZRt0hEZ8Ph5_3HYsCgsiCjw(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$3SE6OZXs5OLy9Wr5rv27kRtB3mo(Lcom/pspdfkit/internal/m0;Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$3uSlTHoTGcXdA07h518jU7QhUF8(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->e(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$IAmHbiS2migk38XUXYWntKHWh7A(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$PH1wYbpUy5OsrOxlMZSJTdKWhAM(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->c(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$PtgGUvpbBwdDkFUaMmjzYvTyTJU(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void
.end method

.method public static synthetic $r8$lambda$TwxthKjCwpH_Z2WsSWxDYNDpNP0(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->b(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Wy1kmfGjxKFhS4SPceWJ0Z4MUoo(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method public static synthetic $r8$lambda$aD3cJJTkQELBKKxRso7k5-HIVhU(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$at1Ne9aPqqIvdNFvsgvoHVSsL84(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->d(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$f78WxnPP-pjdNBrkQzu02a-J1Z4(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.method public static synthetic $r8$lambda$k-1WpEXuwbe9nJoA9uj8kdt-tG0(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nON4D2GFCBpNUiob-KFHcUigIeg(ZZLcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 0

    invoke-static/range {p0 .. p6}, Lcom/pspdfkit/internal/m0;->a(ZZLcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pFKIZIzA3-ioFYV0TksRyUkBfJc(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$wXpwb8x9yRA6xbtKCD0OhrayWMY(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->b(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$yu9SOE-e4Az-AAwVdDHI_Ngzmi4(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/m0;->c(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 4

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "controller.fragment.requireContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationPreferences()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v1

    const-string v2, "controller.fragment.annotationPreferences"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    const-string v3, "controller.fragment.annotationConfiguration"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0, v0, v1, v2}, Lcom/pspdfkit/internal/b1;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;)V

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    return-void
.end method

.method private final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;ZZ)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;
    .locals 9

    .line 541
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v0, p1, p2, v1}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 544
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    invoke-interface {v0, p1, p2, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;

    .line 553
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v0

    if-eqz p4, :cond_1

    .line 554
    sget v2, Lcom/pspdfkit/R$string;->pspdf__picker_line_start:I

    goto :goto_0

    :cond_1
    sget v2, Lcom/pspdfkit/R$string;->pspdf__picker_line_end:I

    .line 555
    :goto_0
    invoke-static {v0, v2, v1}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "getString(\n             \u2026er_line_end\n            )"

    .line 556
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    new-instance v8, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda15;

    move-object v0, v8

    move v1, p4

    move v2, p5

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda15;-><init>(ZZLcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    move-object v2, p0

    move-object v3, v6

    move-object v4, p3

    move-object v5, v7

    move v6, p4

    move-object v7, v8

    invoke-virtual/range {v2 .. v7}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationLineEndsConfiguration;Lcom/pspdfkit/annotations/LineEndType;Ljava/lang/String;ZLcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView$LineEndTypePickerListener;)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFloatPrecision(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    .line 527
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setFloatPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 524
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setMeasurementScale(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/measurements/Scale;)V

    .line 525
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedFont"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 529
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFont(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/fonts/Font;)V

    .line 530
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setFont(Lcom/pspdfkit/ui/fonts/Font;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 535
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 536
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setFillColor(I)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "value"

    invoke-static {p4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 539
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setBorderStylePreset(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    .line 540
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setBorderStylePreset(Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 537
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    int-to-float p4, p4

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setThickness(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 538
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setThickness(F)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;Ljava/lang/String;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "value"

    invoke-static {p4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 531
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/String;)V

    .line 532
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setOverlayText(Ljava/lang/String;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;Z)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setRepeatOverlayText(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Z)V

    .line 534
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setRepeatOverlayText(Z)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/m0;Z)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 528
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setMeasurementSnappingEnabled(Z)V

    return-void
.end method

.method private static final a(ZZLcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;Lcom/pspdfkit/annotations/LineEndType;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p5, "value"

    invoke-static {p6, p5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-nez p0, :cond_2

    if-eqz p1, :cond_2

    .line 558
    :cond_1
    invoke-virtual {p2}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p0

    iget-object p1, p2, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-interface {p0, p3, p4, p6, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 559
    iget-object p0, p2, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-interface {p0, p6, p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    goto :goto_0

    .line 561
    :cond_2
    invoke-virtual {p2}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p0

    iget-object p1, p2, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-interface {p0, p3, p4, p1, p6}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setLineEnds(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    .line 562
    iget-object p0, p2, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Lcom/pspdfkit/annotations/LineEndType;

    invoke-interface {p0, p1, p6}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setLineEnds(Lcom/pspdfkit/annotations/LineEndType;Lcom/pspdfkit/annotations/LineEndType;)V

    :goto_0
    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1111
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setFillColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 1112
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setFillColor(I)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1109
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    int-to-float p4, p4

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setTextSize(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 1110
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setTextSize(F)V

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    .line 4
    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 5
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setColor(I)V

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;I)V
    .locals 0

    const-string p3, "this$0"

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$annotationTool"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$annotationToolVariant"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p3, p4

    const/high16 p4, 0x42c80000    # 100.0f

    div-float/2addr p3, p4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p4

    invoke-interface {p4, p1, p2, p3}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setAlpha(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;F)V

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p3}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setAlpha(F)V

    return-void
.end method

.method private static final d(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    .line 2
    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 3
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setColor(I)V

    return-void
.end method

.method private static final e(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/ui/inspector/PropertyInspectorView;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->b()Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    move-result-object p3

    invoke-interface {p3, p1, p2, p4}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setOutlineColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    .line 2
    iget-object p0, p0, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {p0, p4}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->setOutlineColor(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;",
            "Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;",
            ")",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/PropertyInspectorView;",
            ">;"
        }
    .end annotation

    move-object v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    const-string v0, "annotationTool"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationToolVariant"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/4 v1, 0x0

    const/4 v9, 0x1

    if-eq v7, v0, :cond_0

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v7, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration;

    invoke-interface {v0, v7, v8, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration;

    .line 8
    invoke-virtual/range {p1 .. p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v2

    const-string v3, "annotationTool.toAnnotationType()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 9
    invoke-interface {v0}, Lcom/pspdfkit/annotations/configuration/AnnotationPreviewConfiguration;->isPreviewEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    sget-object v0, Lcom/pspdfkit/internal/m0$a;->a:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 23
    :pswitch_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v0

    const-class v2, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    invoke-interface {v0, v7, v8, v2}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    if-eqz v0, :cond_2

    .line 24
    new-instance v2, Lcom/pspdfkit/ui/inspector/views/EraserPreviewInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {v2, v3, v4, v0}, Lcom/pspdfkit/ui/inspector/views/EraserPreviewInspectorView;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 25
    :pswitch_1
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/RedactionAnnotationPreviewInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {v0, v2, v3}, Lcom/pspdfkit/ui/inspector/views/RedactionAnnotationPreviewInspectorView;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 26
    :pswitch_2
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {v0, v3, v2, v4}, Lcom/pspdfkit/ui/inspector/views/ShapeAnnotationPreviewInspectorView;-><init>(Landroid/content/Context;Lcom/pspdfkit/annotations/AnnotationType;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 27
    :pswitch_3
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/InkAnnotationPreviewInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {v0, v2, v3}, Lcom/pspdfkit/ui/inspector/views/InkAnnotationPreviewInspectorView;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 28
    :pswitch_4
    new-instance v0, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-direct {v0, v2, v3}, Lcom/pspdfkit/ui/inspector/views/FreeTextAnnotationPreviewInspectorView;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_2
    :goto_1
    new-instance v11, Lcom/pspdfkit/internal/m0$b;

    invoke-direct {v11, v10}, Lcom/pspdfkit/internal/m0$b;-><init>(Ljava/util/ArrayList;)V

    .line 55
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    :cond_4
    const-string v2, "scale"

    .line 57
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 58
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 61
    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->SCALE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 62
    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    const/4 v12, 0x0

    if-nez v3, :cond_5

    move-object v0, v12

    goto :goto_2

    .line 70
    :cond_5
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 71
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;

    .line 76
    invoke-virtual {p0, v3, v0, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationScaleConfiguration;Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView$ScalePickerListener;)Lcom/pspdfkit/ui/inspector/views/ScalePickerInspectorView;

    move-result-object v0

    .line 77
    :goto_2
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/base/FragmentSpecialModeController;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfFragment;->getDocument()Lcom/pspdfkit/document/PdfDocument;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Lcom/pspdfkit/document/PdfDocument;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFloatPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v0

    :cond_7
    const-string v2, "precision"

    .line 88
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 89
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 92
    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FLOAT_PRECISION:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 93
    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_8

    move-object v0, v12

    goto :goto_3

    .line 101
    :cond_8
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 102
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;

    .line 107
    invoke-virtual {p0, v3, v0, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationPrecisionConfiguration;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView$PrecisionPickerListener;)Lcom/pspdfkit/ui/inspector/views/PrecisionPickerInspectorView;

    move-result-object v0

    .line 108
    :goto_3
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->isMeasurementSnappingEnabled()Z

    move-result v0

    new-instance v2, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda7;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/m0;)V

    invoke-virtual {p0, v7, v0, v2}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;ZLcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView$SnappingPickerListener;)Lcom/pspdfkit/ui/inspector/views/SnappingPickerInspectorView;

    move-result-object v0

    .line 117
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "tool"

    .line 118
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    sget-object v0, Lcom/pspdfkit/internal/jr;->b:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x4

    if-eq v0, v9, :cond_9

    const/4 v3, 0x2

    if-eq v0, v3, :cond_9

    const/4 v3, 0x3

    if-eq v0, v3, :cond_9

    if-eq v0, v2, :cond_9

    const/4 v3, 0x5

    if-eq v0, v3, :cond_9

    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_a

    .line 149
    new-instance v0, Lcom/pspdfkit/internal/le;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->c()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/pspdfkit/internal/le;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_a
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFont()Lcom/pspdfkit/ui/fonts/Font;

    move-result-object v0

    const-string v3, "controller.font"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda8;

    invoke-direct {v3, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 155
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FONT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_b

    move-object v0, v12

    goto :goto_5

    .line 158
    :cond_b
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    .line 159
    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;

    .line 164
    invoke-virtual {p0, v4, v0, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationFontConfiguration;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView;

    move-result-object v0

    .line 165
    :goto_5
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getOverlayText()Ljava/lang/String;

    move-result-object v0

    const-string v3, "controller.overlayText"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda9;

    invoke-direct {v3, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 173
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    .line 176
    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 177
    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_c

    move-object v0, v12

    goto :goto_6

    .line 185
    :cond_c
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    .line 186
    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    .line 191
    invoke-virtual {p0, v4, v0, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;Ljava/lang/String;Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView$TextInputListener;)Lcom/pspdfkit/ui/inspector/views/TextInputInspectorView;

    move-result-object v0

    .line 192
    :goto_6
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getRepeatOverlayText()Z

    move-result v0

    .line 203
    new-instance v3, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda10;

    invoke-direct {v3, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 204
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    .line 208
    sget-object v5, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->REPEAT_OVERLAY_TEXT:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 209
    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_d

    move-object v0, v12

    goto :goto_7

    .line 217
    :cond_d
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    .line 218
    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;

    .line 223
    invoke-virtual {p0, v4, v0, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOverlayTextConfiguration;ZLcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView$TogglePickerListener;)Lcom/pspdfkit/ui/inspector/views/TogglePickerInspectorView;

    move-result-object v0

    .line 224
    :goto_7
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v0

    new-instance v3, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda11;

    invoke-direct {v3, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 236
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    sget-object v13, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v4, v7, v8, v13}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v4

    if-nez v4, :cond_e

    move-object v0, v12

    goto :goto_8

    .line 239
    :cond_e
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v4

    const-class v5, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-interface {v4, v7, v8, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    .line 245
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v5

    .line 248
    sget-object v14, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 249
    invoke-interface {v5, v7, v8, v14}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v5

    .line 254
    invoke-virtual {p0, v4, v0, v5, v3}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;IZLcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    :goto_8
    if-eqz v0, :cond_f

    .line 255
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v14, 0x1

    goto :goto_9

    :cond_f
    const/4 v14, 0x0

    .line 262
    :goto_9
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getOutlineColor()I

    move-result v0

    .line 263
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda12;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 264
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->OUTLINE_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_10

    move-object v0, v12

    goto :goto_a

    .line 267
    :cond_10
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 268
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;

    .line 273
    invoke-virtual {p0, v3, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationOutlineColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    .line 274
    :goto_a
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFillColor()I

    move-result v0

    .line 289
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda13;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 290
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_11

    move-object v0, v12

    goto :goto_b

    .line 293
    :cond_11
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 294
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    .line 299
    invoke-virtual {p0, v3, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    .line 300
    :goto_b
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getThickness()F

    move-result v0

    .line 315
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda14;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 316
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_12

    move-object v0, v12

    goto :goto_c

    .line 319
    :cond_12
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 320
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;

    .line 325
    invoke-virtual {p0, v3, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationThicknessConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v0

    .line 326
    :goto_c
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getTextSize()F

    move-result v0

    .line 341
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 342
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->TEXT_SIZE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_13

    move-object v0, v12

    goto :goto_d

    .line 345
    :cond_13
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 346
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;

    .line 351
    invoke-virtual {p0, v3, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationTextSizeConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v0

    .line 352
    :goto_d
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getBorderStylePreset()Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;

    move-result-object v0

    const-string v1, "controller.borderStylePreset"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 367
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 368
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->BORDER_STYLE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v3

    if-nez v3, :cond_14

    move-object v0, v12

    goto :goto_e

    .line 371
    :cond_14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v3

    .line 372
    const-class v4, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    invoke-interface {v3, v7, v8, v4}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;

    .line 377
    invoke-virtual {p0, v3, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationBorderStyleConfiguration;Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView$BorderStylePickerListener;)Lcom/pspdfkit/ui/inspector/views/BorderStylePickerInspectorView;

    move-result-object v0

    .line 378
    :goto_e
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v0, Lcom/pspdfkit/internal/m0$a;->a:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const-string v1, "controller.lineEnds.first"

    if-eq v0, v2, :cond_16

    const/16 v2, 0x8

    if-eq v0, v2, :cond_16

    const/16 v2, 0xb

    if-eq v0, v2, :cond_15

    goto/16 :goto_10

    .line 397
    :cond_15
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/pspdfkit/annotations/LineEndType;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 398
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;ZZ)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v0

    .line 399
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    .line 414
    :cond_16
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/pspdfkit/annotations/LineEndType;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 415
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;ZZ)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v0

    .line 416
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getLineEnds()Landroidx/core/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    const-string v1, "controller.lineEnds.second"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/pspdfkit/annotations/LineEndType;

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    .line 430
    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/m0;->a(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/LineEndType;ZZ)Lcom/pspdfkit/ui/inspector/views/LineEndTypePickerInspectorView;

    move-result-object v0

    .line 431
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getFillColor()I

    move-result v0

    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 442
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 446
    sget-object v3, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->LINE_ENDS_FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 447
    invoke-interface {v2, v7, v8, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v2

    if-nez v2, :cond_17

    move-object v0, v12

    goto :goto_f

    .line 455
    :cond_17
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 456
    const-class v3, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    invoke-interface {v2, v7, v8, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;

    .line 461
    invoke-virtual {p0, v2, v0, v1}, Lcom/pspdfkit/internal/b1;->b(Lcom/pspdfkit/annotations/configuration/AnnotationFillColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView;

    move-result-object v0

    .line 462
    :goto_f
    invoke-virtual {v11, v0}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_10
    if-eqz v14, :cond_19

    .line 474
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v9, :cond_19

    .line 478
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getColor()I

    move-result v0

    .line 479
    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 480
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 481
    invoke-interface {v2, v7, v8, v13}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v2

    if-nez v2, :cond_18

    move-object v0, v12

    goto :goto_11

    .line 489
    :cond_18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 490
    const-class v3, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    invoke-interface {v2, v7, v8, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;

    .line 495
    invoke-virtual {p0, v2, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationColorConfiguration;ILcom/pspdfkit/ui/inspector/views/ColorPickerInspectorView$ColorPickerListener;)Lcom/pspdfkit/ui/inspector/views/ColorPickerInspectorDetailView;

    move-result-object v0

    :goto_11
    if-eqz v0, :cond_19

    .line 496
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 497
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    :cond_19
    iget-object v0, v6, Lcom/pspdfkit/internal/m0;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;

    invoke-interface {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAlpha()F

    move-result v0

    new-instance v1, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, v7, v8}, Lcom/pspdfkit/internal/m0$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/m0;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 503
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 507
    sget-object v3, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_ALPHA:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 508
    invoke-interface {v2, v7, v8, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v2

    if-nez v2, :cond_1a

    goto :goto_12

    .line 516
    :cond_1a
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v2

    .line 517
    const-class v3, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    invoke-interface {v2, v7, v8, v3}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;

    .line 522
    invoke-virtual {p0, v2, v0, v1}, Lcom/pspdfkit/internal/b1;->a(Lcom/pspdfkit/annotations/configuration/AnnotationAlphaConfiguration;FLcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView$SliderPickerListener;)Lcom/pspdfkit/ui/inspector/views/SliderPickerInspectorView;

    move-result-object v12

    .line 523
    :goto_12
    invoke-virtual {v11, v12}, Lcom/pspdfkit/internal/m0$b;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v10

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)Z
    .locals 7

    const-string v0, "annotationTool"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationToolVariant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SIGNATURE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return v2

    .line 2
    :cond_1
    invoke-static {}, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->values()[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_4

    aget-object v5, v0, v4

    .line 3
    sget v6, Lcom/pspdfkit/internal/ao;->a:I

    const-string v6, "property"

    .line 4
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1107
    sget-object v6, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    if-eq v5, v6, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_3

    .line 1108
    invoke-virtual {p0}, Lcom/pspdfkit/internal/b1;->a()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object v6

    invoke-interface {v6, p1, p2, v5}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->isAnnotationPropertySupported(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)Z

    move-result v5

    if-eqz v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    return v2
.end method

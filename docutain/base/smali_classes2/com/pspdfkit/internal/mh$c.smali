.class public final Lcom/pspdfkit/internal/mh$c;
.super Lcom/pspdfkit/internal/mh;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/mh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/mh$c$a;
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/annotations/Annotation;

.field private final d:Lcom/pspdfkit/forms/FormElement;

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/forms/FormElement;Z)V
    .locals 1

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formElement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p3, v0}, Lcom/pspdfkit/internal/mh;-><init>(ZI)V

    iput-object p1, p0, Lcom/pspdfkit/internal/mh$c;->c:Lcom/pspdfkit/annotations/Annotation;

    iput-object p2, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    .line 62
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mh$c;->b()Lcom/pspdfkit/annotations/Annotation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getUuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long p1, p1

    iput-wide p1, p0, Lcom/pspdfkit/internal/mh$c;->e:J

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    const-string v1, "formElement.type"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    sget-object v1, Lcom/pspdfkit/internal/mh$c$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 7
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_form_button:I

    goto :goto_0

    .line 8
    :cond_0
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_form_choice:I

    goto :goto_0

    .line 9
    :cond_1
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_form_signature:I

    goto :goto_0

    .line 10
    :cond_2
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_form_textfield:I

    goto :goto_0

    .line 11
    :cond_3
    sget v0, Lcom/pspdfkit/R$drawable;->pspdf__ic_form_button:I

    .line 12
    :goto_0
    invoke-static {p1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 14
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 15
    invoke-static {p1}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 16
    invoke-static {p1, p2}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    return-object p1

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;)Z
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    instance-of v0, p1, Lcom/pspdfkit/forms/ChoiceFormElement;

    if-nez v0, :cond_0

    .line 18
    instance-of v0, p1, Lcom/pspdfkit/forms/TextFormElement;

    if-eqz v0, :cond_1

    .line 20
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->isReadOnly()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final a(Lcom/pspdfkit/configuration/PdfConfiguration;I)Z
    .locals 0

    const-string p2, "configuration"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public final b()Lcom/pspdfkit/annotations/Annotation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$c;->c:Lcom/pspdfkit/annotations/Annotation;

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {v0}, Lcom/pspdfkit/forms/FormElement;->getType()Lcom/pspdfkit/forms/FormType;

    move-result-object v0

    const-string v1, "formElement.type"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    sget-object v1, Lcom/pspdfkit/internal/mh$c$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    sget v0, Lcom/pspdfkit/R$string;->pspdf__form_type_choice_field:I

    .line 8
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 9
    :cond_1
    sget v0, Lcom/pspdfkit/R$string;->pspdf__form_type_signature_field:I

    .line 10
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 11
    :cond_2
    sget v0, Lcom/pspdfkit/R$string;->pspdf__form_type_text_field:I

    .line 12
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 13
    :cond_3
    sget v0, Lcom/pspdfkit/R$string;->pspdf__form_type_button:I

    .line 14
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 15
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    invoke-virtual {p1}, Lcom/pspdfkit/forms/FormElement;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "formElement.name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v3, 0x0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_9

    .line 20
    invoke-static {}, Lcom/pspdfkit/internal/mh;->a()Ljava/util/List;

    move-result-object v0

    if-nez v2, :cond_5

    const-string v4, ""

    goto :goto_2

    :cond_5
    move-object v4, v2

    :goto_2
    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 25
    invoke-static {p1, v3, v1}, Lkotlin/text/StringsKt;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_7
    if-eqz v3, :cond_8

    move-object v2, p1

    goto :goto_3

    .line 31
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_9
    :goto_3
    return-object v2
.end method

.method public final b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/pspdfkit/internal/mh;->c()Z

    move-result p1

    return p1
.end method

.method public final d()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/pspdfkit/internal/mh$c;->e:J

    return-wide v0
.end method

.method public final e()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$c;->c:Lcom/pspdfkit/annotations/Annotation;

    .line 2
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getPageIndex()I

    move-result v0

    return v0
.end method

.method public final f()Lcom/pspdfkit/forms/FormElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/mh$c;->d:Lcom/pspdfkit/forms/FormElement;

    return-object v0
.end method

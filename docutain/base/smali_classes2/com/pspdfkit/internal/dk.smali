.class final Lcom/pspdfkit/internal/dk;
.super Lcom/pspdfkit/internal/yr;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/pspdfkit/annotations/NoteAnnotation;

.field final synthetic b:Lcom/pspdfkit/internal/ek;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ek;Lcom/pspdfkit/annotations/NoteAnnotation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    iput-object p2, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yr;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    invoke-static {v0}, Lcom/pspdfkit/internal/ek;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-static {v1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    invoke-static {v0}, Lcom/pspdfkit/internal/ek;->-$$Nest$fgetd(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/pspdfkit/annotations/Annotation;

    iget-object v2, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/am;->a([Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    invoke-static {v0}, Lcom/pspdfkit/internal/ek;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    invoke-static {v0}, Lcom/pspdfkit/internal/ek;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->enterAnnotationEditingMode(Lcom/pspdfkit/annotations/Annotation;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/dk;->b:Lcom/pspdfkit/internal/ek;

    invoke-static {v0}, Lcom/pspdfkit/internal/ek;->-$$Nest$fgetb(Lcom/pspdfkit/internal/ek;)Lcom/pspdfkit/internal/specialMode/handler/a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->showAnnotationEditor(Lcom/pspdfkit/annotations/Annotation;)V

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "create_annotation"

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/dk;->a:Lcom/pspdfkit/annotations/NoteAnnotation;

    .line 8
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method public final onError(Ljava/lang/Throwable;)V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.NoteAnnotations"

    const-string v2, "Failed to create note annotation."

    .line 1
    invoke-static {v1, p1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/f2;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection:[I

    sget v3, Lcom/pspdfkit/R$attr;->pspdf__annotationSelectionStyle:I

    sget v4, Lcom/pspdfkit/R$style;->PSPDFKit_AnnotationSelection:I

    const/4 v5, 0x0

    .line 4
    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 10
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__borderColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__annotation_selection_border:I

    .line 12
    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 13
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->b:I

    .line 16
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__borderWidth:I

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__annotation_selection_border_width:I

    .line 18
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 19
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->a:I

    .line 22
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__scaleHandleColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__annotation_selection_scalehandle:I

    .line 24
    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 25
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->c:I

    .line 29
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__topLeftScaleHandleDrawable:I

    const/4 v3, -0x1

    .line 30
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->m:I

    .line 32
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__topCenterScaleHandleDrawable:I

    .line 33
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->n:I

    .line 35
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__topRightScaleHandleDrawable:I

    .line 36
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->o:I

    .line 38
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__centerLeftScaleHandleDrawable:I

    .line 39
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->p:I

    .line 41
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__centerRightScaleHandleDrawable:I

    .line 42
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->q:I

    .line 44
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__bottomLeftScaleHandleDrawable:I

    .line 45
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->r:I

    .line 47
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__bottomCenterScaleHandleDrawable:I

    .line 48
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->s:I

    .line 50
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__bottomRightScaleHandleDrawable:I

    .line 51
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->t:I

    .line 53
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__rotationHandleDrawable:I

    .line 54
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->u:I

    .line 56
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__backgroundDrawable:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->v:I

    .line 58
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__editHandleColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__annotation_selection_edithandle:I

    .line 60
    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 61
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->d:I

    .line 64
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__padding:I

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__annotation_selection_padding:I

    .line 66
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 67
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->e:I

    .line 71
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__guideLineWidth:I

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__annotation_selection_guide_line_width:I

    .line 73
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 74
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->f:I

    .line 77
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__guideLineColor:I

    sget v3, Lcom/pspdfkit/R$color;->pspdf__annotation_selection_guide_line_color:I

    .line 79
    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 80
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/f2;->g:I

    .line 83
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__AnnotationSelection_pspdf__guideLineIncrease:I

    sget v3, Lcom/pspdfkit/R$dimen;->pspdf__annotation_selection_guide_line_increase:I

    .line 85
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 86
    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/f2;->h:I

    .line 90
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__Annotation:[I

    sget v2, Lcom/pspdfkit/R$attr;->pspdf__annotationStyle:I

    sget v3, Lcom/pspdfkit/R$style;->PSPDFKit_Annotation:I

    .line 93
    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 98
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__Annotation_pspdf__linkAnnotationBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_link_annotation_background:I

    .line 100
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 101
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/f2;->i:I

    .line 104
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__Annotation_pspdf__linkAnnotationBorderColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_link_annotation_border:I

    .line 106
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 107
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/f2;->j:I

    .line 110
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__Annotation_pspdf__linkAnnotationHighlightBackgroundColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_link_annotation_highlight_background:I

    .line 112
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 113
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/f2;->k:I

    .line 116
    sget v1, Lcom/pspdfkit/R$styleable;->pspdf__Annotation_pspdf__linkAnnotationHighlightBorderColor:I

    sget v2, Lcom/pspdfkit/R$color;->pspdf__color_link_annotation_highlight_border:I

    .line 118
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 119
    invoke-virtual {v0, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/f2;->l:I

    .line 122
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.class public final Lcom/pspdfkit/internal/z1;
.super Lcom/pspdfkit/internal/en;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/z1$a;,
        Lcom/pspdfkit/internal/z1$b;
    }
.end annotation


# static fields
.field public static final synthetic z:I


# instance fields
.field private final e:Lcom/pspdfkit/internal/en;

.field private f:Lcom/pspdfkit/internal/a2;

.field private final g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/drawable/Drawable;

.field private final i:Lcom/pspdfkit/internal/wq;

.field public j:Lcom/pspdfkit/internal/up;

.field private final k:Ljava/util/EnumMap;

.field private final l:Landroid/graphics/Paint;

.field private final m:Landroid/graphics/Paint;

.field private final n:Ljava/util/EnumMap;

.field private final o:I

.field private p:I

.field private final q:Landroid/os/Handler;

.field private final r:Ljava/util/ArrayList;

.field private s:Z

.field private t:Z

.field private u:F

.field private v:I

.field private w:F

.field private x:Z

.field private final y:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/en;Lcom/pspdfkit/configuration/PdfConfiguration;Lcom/pspdfkit/internal/f2;)V
    .locals 3

    const-string v0, "pdfViewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfConfiguration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themeConfiguration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/z1;->e:Lcom/pspdfkit/internal/en;

    .line 18
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    .line 34
    new-instance v2, Lcom/pspdfkit/internal/up;

    invoke-direct {v2, p0, p2}, Lcom/pspdfkit/internal/up;-><init>(Lcom/pspdfkit/internal/z1;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/z1;->j:Lcom/pspdfkit/internal/up;

    .line 67
    new-instance p2, Lcom/pspdfkit/internal/z1$a;

    invoke-direct {p2, p0}, Lcom/pspdfkit/internal/z1$a;-><init>(Lcom/pspdfkit/internal/z1;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->q:Landroid/os/Handler;

    .line 72
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    .line 97
    iput-boolean v1, p0, Lcom/pspdfkit/internal/z1;->x:Z

    .line 102
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->y:Landroid/graphics/Rect;

    .line 929
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 930
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    .line 931
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 932
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/pspdfkit/internal/z1;->m:Landroid/graphics/Paint;

    .line 933
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 934
    new-instance p2, Ljava/util/EnumMap;

    const-class v0, Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p2, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    .line 935
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 936
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 937
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 938
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 939
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 942
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    sget-object v0, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p2, v0, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    new-instance p2, Ljava/util/EnumMap;

    const-class v0, Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p2, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    .line 945
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/16 p2, 0x18

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/z1;->o:I

    const/4 p1, 0x0

    .line 948
    invoke-virtual {p0, p1}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 949
    new-instance p1, Lcom/pspdfkit/internal/wq;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/wq;-><init>(Lcom/pspdfkit/internal/z1;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    .line 951
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/z1;->a(Lcom/pspdfkit/internal/f2;)V

    .line 954
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 955
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 956
    invoke-static {}, Lcom/pspdfkit/internal/z1;->c()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private final a(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final a(IIII)V
    .locals 5

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->h()Z

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    sub-int v0, p3, p1

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    add-int/2addr v4, v3

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    .line 36
    iget v3, p0, Lcom/pspdfkit/internal/z1;->o:I

    if-lt v0, v3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 37
    :goto_1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/z1;->s:Z

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->h()Z

    move-result v0

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_3

    sub-int v0, p4, p2

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    add-int/2addr v4, v3

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    .line 45
    iget v3, p0, Lcom/pspdfkit/internal/z1;->o:I

    if-lt v0, v3, :cond_3

    const/4 v1, 0x1

    .line 46
    :cond_3
    iput-boolean v1, p0, Lcom/pspdfkit/internal/z1;->t:Z

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_4

    iget v1, p0, Lcom/pspdfkit/internal/z1;->p:I

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 52
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_5

    sub-int v1, p3, p1

    iget v2, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 53
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_6

    iget v1, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int v2, p4, p2

    sub-int/2addr v2, v1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 54
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_7

    sub-int v1, p3, p1

    iget v2, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int/2addr v1, v2

    sub-int v3, p4, p2

    sub-int/2addr v3, v2

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Point;->set(II)V

    .line 55
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_8

    sub-int v1, p3, p1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/pspdfkit/internal/z1;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 56
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_9

    sub-int v1, p3, p1

    div-int/lit8 v1, v1, 0x2

    sub-int v2, p4, p2

    iget v3, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 57
    :cond_9
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_a

    iget v1, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int v2, p4, p2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 58
    :cond_a
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_b

    sub-int/2addr p3, p1

    iget p1, p0, Lcom/pspdfkit/internal/z1;->p:I

    sub-int/2addr p3, p1

    sub-int/2addr p4, p2

    div-int/lit8 p4, p4, 0x2

    invoke-virtual {v0, p3, p4}, Landroid/graphics/Point;->set(II)V

    .line 60
    :cond_b
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    iget-object p2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/wq;->a(Ljava/util/EnumMap;)V

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method private final a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V
    .locals 10

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcom/pspdfkit/internal/a2;->a(Lcom/pspdfkit/internal/z1$b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 68
    iget-object v3, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    if-nez v3, :cond_2

    if-nez v0, :cond_2

    return-void

    .line 71
    :cond_2
    iget-object v3, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    invoke-virtual {v3, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Point;

    if-eqz v3, :cond_7

    if-eqz v0, :cond_5

    .line 78
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 79
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 84
    iget v6, v3, Landroid/graphics/Point;->x:I

    sub-int v7, v6, v4

    .line 85
    iget v8, v3, Landroid/graphics/Point;->y:I

    sub-int v9, v8, v5

    add-int/2addr v6, v4

    add-int/2addr v8, v5

    .line 86
    invoke-virtual {v0, v7, v9, v6, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    if-eq p2, v4, :cond_3

    iget-boolean p2, p0, Lcom/pspdfkit/internal/z1;->x:Z

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 96
    iget p2, p0, Lcom/pspdfkit/internal/z1;->u:F

    iget v2, v3, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v4, v3, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-virtual {p1, p2, v2, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 98
    :cond_4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    if-eqz v1, :cond_6

    .line 100
    iget p2, p0, Lcom/pspdfkit/internal/z1;->u:F

    neg-float p2, p2

    iget v0, v3, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, v3, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto :goto_2

    .line 104
    :cond_5
    iget p2, v3, Landroid/graphics/Point;->x:I

    int-to-float p2, p2

    iget v0, v3, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget v1, p0, Lcom/pspdfkit/internal/z1;->p:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_6
    :goto_2
    return-void

    .line 105
    :cond_7
    new-instance p1, Ljava/lang/AssertionError;

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Scale handle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " must be part of scaleHandleCenters map."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 107
    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method private final b(II)V
    .locals 4

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 6
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v0, :cond_3

    .line 8
    invoke-interface {v0}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 9
    :cond_1
    invoke-static {v0}, Lcom/pspdfkit/internal/ao;->d(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/List;

    move-result-object v0

    .line 10
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    .line 11
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 15
    iget-object v3, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    .line 16
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    int-to-float v1, p1

    neg-float v1, v1

    int-to-float v3, p2

    neg-float v3, v3

    .line 21
    invoke-virtual {v2, v1, v3}, Landroid/graphics/PointF;->offset(FF)V

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_3
    :goto_1
    return-void
.end method

.method protected static c()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;
    .locals 2

    .line 1
    new-instance v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    invoke-direct {v0}, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;-><init>()V

    .line 2
    sget-object v1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;->CENTER:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    iput-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->layoutPosition:Lcom/pspdfkit/ui/overlay/OverlayLayoutParams$LayoutPosition;

    return-object v0
.end method

.method private final getRotationHandleRadius()D
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    .line 7
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-double v5, v0

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v3, v1

    .line 8
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const/4 v2, 0x2

    int-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0

    .line 15
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/z1;->p:I

    int-to-double v0, v0

    :goto_0
    return-wide v0
.end method

.method private final getSelectionBoundingBox()Landroid/graphics/Rect;
    .locals 7

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2
    new-instance v1, Landroid/graphics/Rect;

    const v2, 0x7fffffff

    const/high16 v3, -0x80000000

    invoke-direct {v1, v2, v2, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 6
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4, v0}, Lcom/pspdfkit/internal/en;->a(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    const-string v5, "getChildBoundingBox(getChildAt(i), reuseRect)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->left:I

    .line 8
    iget v5, v4, Landroid/graphics/Rect;->top:I

    iget v6, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 9
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->bottom:I

    .line 10
    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget v5, v1, Landroid/graphics/Rect;->right:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;Z)I
    .locals 7

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    if-nez p2, :cond_0

    return v0

    .line 128
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr p2, v1

    .line 129
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr p1, v1

    const/4 v1, 0x0

    .line 130
    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_2

    .line 131
    iget-object v3, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 132
    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget v5, p0, Lcom/pspdfkit/internal/z1;->v:I

    int-to-float v5, v5

    sub-float v6, v4, v5

    cmpl-float v6, p2, v6

    if-ltz v6, :cond_1

    add-float/2addr v4, v5

    cmpg-float v4, p2, v4

    if-gez v4, :cond_1

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v4, v3, v5

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_1

    add-float/2addr v3, v5

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    return v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public final a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->e:Lcom/pspdfkit/internal/en;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/en;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    const-string v0, "pdfViewGroup.getPdfToViewTransformation(reuse)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/z1$b;)Landroid/graphics/Point;
    .locals 1

    const-string v0, "scaleHandle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Point;

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/f2;)V
    .locals 3

    const-string v0, "themeConfiguration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    iget v1, p1, Lcom/pspdfkit/internal/f2;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2
    iget v0, p1, Lcom/pspdfkit/internal/f2;->a:I

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    iget v1, p1, Lcom/pspdfkit/internal/f2;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->m:Landroid/graphics/Paint;

    iget v1, p1, Lcom/pspdfkit/internal/f2;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->m:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->n:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->o:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->p:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->q:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->r:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->s:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->t:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    iget v2, p1, Lcom/pspdfkit/internal/f2;->u:I

    invoke-direct {p0, v2}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    iget v0, p1, Lcom/pspdfkit/internal/f2;->v:I

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/z1;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    .line 18
    iget p1, p1, Lcom/pspdfkit/internal/f2;->e:I

    .line 19
    invoke-virtual {p0, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    const/4 v0, 0x0

    .line 20
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 21
    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/pspdfkit/internal/z1;->p:I

    .line 31
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/wq;->a(I)V

    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 0

    if-nez p1, :cond_1

    .line 133
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->l()Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v2, Landroid/graphics/Point;

    invoke-direct {v1, v2}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v2, Landroid/graphics/Point;

    invoke-direct {v1, v2}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v2, Landroid/graphics/Point;

    invoke-direct {v1, v2}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v2, Landroid/graphics/Point;

    invoke-direct {v1, v2}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v2, Landroid/graphics/Point;

    invoke-direct {v1, v2}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr p1, v3

    invoke-direct {v1, v2, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 122
    invoke-static {v1, v0}, Lcom/pspdfkit/internal/di;->a(Landroid/graphics/PointF;Ljava/util/ArrayList;)Z

    move-result p1

    goto :goto_1

    .line 127
    :cond_1
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/ov;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    :goto_1
    return p1
.end method

.method public final b(I)Lcom/pspdfkit/internal/views/annotations/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/internal/views/annotations/a;

    return-object p1
.end method

.method public final b(Landroid/view/MotionEvent;Z)Lcom/pspdfkit/internal/z1$b;
    .locals 7

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    .line 25
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr p2, v1

    .line 26
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr p1, v1

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/z1$b;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 28
    iget-boolean v4, p0, Lcom/pspdfkit/internal/z1;->s:Z

    if-nez v4, :cond_2

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    if-ne v3, v4, :cond_2

    goto :goto_0

    .line 31
    :cond_2
    iget-boolean v4, p0, Lcom/pspdfkit/internal/z1;->t:Z

    if-nez v4, :cond_3

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    if-ne v3, v4, :cond_3

    goto :goto_0

    .line 34
    :cond_3
    sget-object v4, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    if-ne v3, v4, :cond_4

    iget-object v4, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    invoke-virtual {v4}, Lcom/pspdfkit/internal/wq;->b()Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_0

    .line 37
    :cond_4
    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, p0, Lcom/pspdfkit/internal/z1;->v:I

    sub-int v6, v4, v5

    int-to-float v6, v6

    cmpl-float v6, p2, v6

    if-ltz v6, :cond_1

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, p2, v4

    if-gez v4, :cond_1

    .line 39
    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int v4, v2, v5

    int-to-float v4, v4

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_1

    add-int/2addr v2, v5

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_1

    .line 42
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    invoke-virtual {p1, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result p1

    if-eqz p1, :cond_5

    move-object v0, v3

    :cond_5
    return-object v0
.end method

.method public final b()Z
    .locals 8

    .line 43
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 46
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/pspdfkit/internal/views/annotations/a;

    .line 48
    iget-object v5, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    invoke-virtual {v5, v4}, Lcom/pspdfkit/internal/wq;->a(Lcom/pspdfkit/internal/views/annotations/a;)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_0

    :goto_1
    const/4 v3, 0x1

    goto :goto_3

    .line 51
    :cond_0
    iget-object v5, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v5, :cond_1

    const/4 v7, 0x0

    invoke-interface {v5, v4, v7}, Lcom/pspdfkit/internal/a2;->a(Lcom/pspdfkit/internal/views/annotations/a;Lcom/pspdfkit/internal/k0;)Z

    move-result v5

    if-ne v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_2

    .line 52
    invoke-interface {v4}, Lcom/pspdfkit/internal/views/annotations/a;->b()V

    goto :goto_1

    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_4

    .line 60
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->b()V

    :cond_4
    return v3
.end method

.method public final d()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/z1;->b(II)V

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/pspdfkit/internal/z1;->a(IIII)V

    return-void
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 6
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->y:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v1, v3, v4, v2}, Lcom/pspdfkit/internal/wq;->a(IILandroid/graphics/Rect;)V

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->y:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    .line 11
    iget v4, v2, Landroid/graphics/Rect;->top:I

    .line 12
    iget v5, v2, Landroid/graphics/Rect;->right:I

    .line 13
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 14
    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->y:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    .line 21
    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->y:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    .line 29
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 30
    iget v3, p0, Lcom/pspdfkit/internal/z1;->u:F

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->getPageRotation()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p1, v3, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 32
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 35
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 38
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->j:Lcom/pspdfkit/internal/up;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/internal/up;->a(Landroid/graphics/Canvas;)V

    .line 39
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    .line 44
    iget v3, p0, Lcom/pspdfkit/internal/z1;->p:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x1c

    invoke-static {v2, v3}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 46
    iput v1, p0, Lcom/pspdfkit/internal/z1;->v:I

    .line 50
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 51
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 52
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 55
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 56
    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v3, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 57
    iget-object v3, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v4, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v3, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Point;

    .line 58
    iget-object v4, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v5, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v4, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    .line 60
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v7, v5

    .line 61
    iget v5, v1, Landroid/graphics/Point;->y:I

    int-to-float v8, v5

    .line 62
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v9, v5

    .line 63
    iget v5, v3, Landroid/graphics/Point;->y:I

    int-to-float v10, v5

    .line 64
    iget-object v11, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    move-object v6, p1

    .line 65
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 73
    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v7, v5

    .line 74
    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v8, v3

    .line 75
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v9, v3

    .line 76
    iget v3, v2, Landroid/graphics/Point;->y:I

    int-to-float v10, v3

    .line 77
    iget-object v11, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    .line 78
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 86
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v6, v3

    .line 87
    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v7, v2

    .line 88
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget v2, v4, Landroid/graphics/Point;->x:I

    int-to-float v8, v2

    .line 89
    iget v2, v4, Landroid/graphics/Point;->y:I

    int-to-float v9, v2

    .line 90
    iget-object v10, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    move-object v5, p1

    .line 91
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 99
    iget v2, v4, Landroid/graphics/Point;->x:I

    int-to-float v6, v2

    .line 100
    iget v2, v4, Landroid/graphics/Point;->y:I

    int-to-float v7, v2

    .line 101
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v8, v2

    .line 102
    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v9, v1

    .line 103
    iget-object v10, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    .line 104
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 113
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2}, Lcom/pspdfkit/internal/wq;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 117
    :cond_3
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 118
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 119
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 120
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 121
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 122
    iget-boolean v1, p0, Lcom/pspdfkit/internal/z1;->s:Z

    if-eqz v1, :cond_4

    .line 123
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 124
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 126
    :cond_4
    iget-boolean v1, p0, Lcom/pspdfkit/internal/z1;->t:Z

    if-eqz v1, :cond_5

    .line 127
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 128
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 130
    :cond_5
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->l()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 131
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 133
    iget-object v2, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    if-eqz v2, :cond_7

    .line 134
    invoke-direct {p0}, Lcom/pspdfkit/internal/z1;->getRotationHandleRadius()D

    move-result-wide v2

    .line 136
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 137
    iget-object v4, p0, Lcom/pspdfkit/internal/z1;->n:Ljava/util/EnumMap;

    sget-object v5, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v4, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    check-cast v4, Landroid/graphics/Point;

    const-wide/16 v5, 0x0

    cmpg-double v7, v2, v5

    if-ltz v7, :cond_6

    .line 138
    iget v5, v4, Landroid/graphics/Point;->x:I

    iget v6, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, v4, Landroid/graphics/Point;->y:I

    iget v7, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Landroid/graphics/PointF;->length(FF)F

    move-result v5

    float-to-double v5, v5

    sub-double v2, v5, v2

    div-double/2addr v2, v5

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v5, v2

    .line 143
    iget v7, v1, Landroid/graphics/Point;->x:I

    int-to-double v7, v7

    mul-double v7, v7, v5

    iget v9, v4, Landroid/graphics/Point;->x:I

    int-to-double v9, v9

    mul-double v9, v9, v2

    add-double/2addr v9, v7

    double-to-int v7, v9

    .line 145
    iget v8, v1, Landroid/graphics/Point;->y:I

    int-to-double v8, v8

    mul-double v5, v5, v8

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-double v8, v4

    mul-double v2, v2, v8

    add-double/2addr v2, v5

    double-to-int v2, v2

    .line 148
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, v7, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 149
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v2

    .line 150
    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v1

    .line 151
    iget v1, v3, Landroid/graphics/Point;->x:I

    int-to-float v7, v1

    .line 152
    iget v1, v3, Landroid/graphics/Point;->y:I

    int-to-float v8, v1

    .line 153
    iget-object v9, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    move-object v4, p1

    .line 154
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 155
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Length must be a non-negative value, was: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 156
    :cond_7
    :goto_0
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/z1;->a(Landroid/graphics/Canvas;Lcom/pspdfkit/internal/z1$b;)V

    .line 161
    :cond_8
    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->e()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 162
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    .line 498
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_9

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_9
    check-cast v4, Landroid/graphics/PointF;

    .line 499
    invoke-interface {v0, v3}, Lcom/pspdfkit/internal/a2;->b(I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 500
    iget v3, v4, Landroid/graphics/PointF;->x:F

    .line 501
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 502
    iget v6, p0, Lcom/pspdfkit/internal/z1;->p:I

    int-to-float v6, v6

    .line 503
    iget-object v7, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    const/4 v8, 0x1

    if-eqz v7, :cond_a

    invoke-interface {v7}, Lcom/pspdfkit/internal/a2;->f()Z

    move-result v7

    if-ne v7, v8, :cond_a

    goto :goto_2

    :cond_a
    const/4 v8, 0x0

    :goto_2
    if-eqz v8, :cond_b

    iget-object v7, p0, Lcom/pspdfkit/internal/z1;->m:Landroid/graphics/Paint;

    goto :goto_3

    :cond_b
    iget-object v7, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    .line 504
    :goto_3
    invoke-virtual {p1, v3, v4, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_c
    move v3, v5

    goto :goto_1

    :cond_d
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const-string v0, "ev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->i()Z

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 5
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    neg-float v0, v0

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    neg-float v1, v1

    .line 7
    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 8
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    neg-float v0, v0

    neg-float v1, v1

    .line 9
    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move v1, v2

    :cond_2
    return v1
.end method

.method public final e()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 3
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    .line 4
    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v4}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5
    invoke-interface {v3}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    .line 6
    invoke-virtual {v3}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    const/4 v4, 0x1

    .line 7
    invoke-interface {v3, v4}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached(Z)Z

    move-result v3

    or-int/2addr v2, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public final f()[Lcom/pspdfkit/internal/views/annotations/a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lcom/pspdfkit/internal/views/annotations/a<",
            "*>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->q:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z1;->b()Z

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->q:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wq;->a()V

    .line 9
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 10
    new-array v1, v0, [Lcom/pspdfkit/internal/views/annotations/a;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 12
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.AnnotationView<*>"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/pspdfkit/internal/views/annotations/a;

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/pspdfkit/internal/a2;->j()V

    .line 15
    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-object v1
.end method

.method public final bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/z1;->c()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final getAnnotationSelectionViewThemeConfiguration()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setSelectionBorderColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setSelectionBorderWidth(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setSelectionScaleHandleColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->m:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setSelectionEditHandleColor(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setTopLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setTopCenterScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setTopRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setCenterLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 12
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setCenterRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setBottomLeftScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 18
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setBottomCenterScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 23
    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setBottomRightScaleHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v2, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setRotationHandleDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->setSelectionPadding(I)Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration$Builder;->build()Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getBorderColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public final getEditHandleCenters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;
    .locals 1

    .line 2
    invoke-super {p0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    return-object v0
.end method

.method public getPdfRect()Landroid/graphics/RectF;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->e:Lcom/pspdfkit/internal/en;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/en;->getPdfRect()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "pdfViewGroup.pdfRect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getPdfToViewTransformation()Landroid/graphics/Matrix;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    const-string v1, "pdfToViewTransformation"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getPdfViewGroup()Lcom/pspdfkit/internal/en;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->e:Lcom/pspdfkit/internal/en;

    return-object v0
.end method

.method public final getPresenter()Lcom/pspdfkit/internal/a2;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    return-object v0
.end method

.method public final getRotationHandler()Lcom/pspdfkit/internal/wq;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    return-object v0
.end method

.method public final getScaleHandleColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public final getScaleHandleDrawables()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/pspdfkit/internal/z1$b;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    return-object v0
.end method

.method public final getScaleHandleDrawablesSupportRotation()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/z1;->x:Z

    return v0
.end method

.method public final getScaleHandleRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/z1;->p:I

    return v0
.end method

.method public final getSelectionBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getSelectionLayoutHandler()Landroid/os/Handler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->q:Landroid/os/Handler;

    return-object v0
.end method

.method public getZoomScale()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->e:Lcom/pspdfkit/internal/en;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/en;->getZoomScale()F

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object v0

    .line 2
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0}, Lcom/pspdfkit/internal/z1;->getSelectionBoundingBox()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3
    iget-object v1, v0, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    iget-object v2, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/utils/PageRect;->updatePageRect(Landroid/graphics/Matrix;)V

    .line 4
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/en;->a()V

    .line 2
    invoke-virtual {p0, p2, p3}, Lcom/pspdfkit/internal/en;->a(II)V

    .line 6
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z1;->getZoomScale()F

    move-result p1

    const/4 v0, 0x0

    .line 11
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    .line 13
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 14
    instance-of v3, v2, Lcom/pspdfkit/internal/views/annotations/a;

    if-eqz v3, :cond_1

    .line 15
    check-cast v2, Lcom/pspdfkit/internal/views/annotations/a;

    .line 16
    iget-object v3, p0, Lcom/pspdfkit/internal/en;->b:Landroid/graphics/Matrix;

    invoke-interface {v2, p1, v3}, Lcom/pspdfkit/internal/views/annotations/a;->a(FLandroid/graphics/Matrix;)V

    .line 17
    invoke-interface {v2}, Lcom/pspdfkit/internal/views/annotations/a;->getAnnotation()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 18
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 19
    iget-object v3, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getPageRotation()I

    move-result v2

    invoke-interface {v3, v2}, Lcom/pspdfkit/internal/a2;->a(I)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->i:Lcom/pspdfkit/internal/wq;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wq;->c()V

    .line 27
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/pspdfkit/internal/z1;->a(IIII)V

    .line 28
    invoke-direct {p0, p2, p3}, Lcom/pspdfkit/internal/z1;->b(II)V

    .line 30
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->j:Lcom/pspdfkit/internal/up;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/up;->b()V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Lcom/pspdfkit/internal/en;->onMeasure(II)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/internal/a2;->k()V

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/z1;->getLayoutParams()Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    move-result-object p1

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    iget-object p1, p1, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;->pageRect:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {p1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object p1

    const-string p2, "layoutParams!!.pageRect.screenRect"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p2

    float-to-int p2, p2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p2

    add-int/2addr p2, v0

    .line 10
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p1

    add-int/2addr p1, v0

    .line 11
    invoke-virtual {p0, p2, p1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public final setAnnotationSelectionViewThemeConfiguration(Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;)V
    .locals 3

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderColor()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderColor()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderWidth()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionBorderWidth()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 7
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->g:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    if-lt v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/a2;->a(Z)V

    .line 11
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionScaleHandleColor()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->l:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionScaleHandleColor()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 14
    :cond_3
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionEditHandleColor()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->m:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionEditHandleColor()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 17
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getTopRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getCenterLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getCenterRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomLeftScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomCenterScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBottomRightScaleHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/z1;->k:Ljava/util/EnumMap;

    sget-object v1, Lcom/pspdfkit/internal/z1$b;->i:Lcom/pspdfkit/internal/z1$b;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getRotationHandleDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/z1;->h:Landroid/graphics/drawable/Drawable;

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionPadding()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/theming/AnnotationSelectionViewThemeConfiguration;->getSelectionPadding()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 30
    invoke-virtual {p0, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 31
    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/pspdfkit/internal/z1;->p:I

    :cond_5
    return-void
.end method

.method public final setPresenter(Lcom/pspdfkit/internal/a2;)V
    .locals 1

    const-string v0, "selectionPresenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/z1;->f:Lcom/pspdfkit/internal/a2;

    return-void
.end method

.method public final setScaleHandleDrawableInitialRotation(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/pspdfkit/internal/z1;->w:F

    return-void
.end method

.method public final setScaleHandleDrawableRotation(F)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/z1;->w:F

    add-float/2addr p1, v0

    iput p1, p0, Lcom/pspdfkit/internal/z1;->u:F

    return-void
.end method

.method public final setScaleHandleDrawablesSupportRotation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/z1;->x:Z

    return-void
.end method

.class public final Lcom/pspdfkit/internal/j7;
.super Lcom/pspdfkit/internal/jni/NativePlatformLogger;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/jni/NativePlatformLogger;-><init>()V

    return-void
.end method


# virtual methods
.method public final log(Lcom/pspdfkit/internal/jni/NativeLoggingLevel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    const-string p2, "PSPDFKit"

    .line 3
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/j7$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-array p1, v0, [Ljava/lang/Object;

    .line 18
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    new-array p1, v0, [Ljava/lang/Object;

    .line 19
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    new-array p1, v0, [Ljava/lang/Object;

    .line 20
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    new-array p1, v0, [Ljava/lang/Object;

    .line 21
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_4
    new-array p1, v0, [Ljava/lang/Object;

    .line 22
    invoke-static {p2, p3, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public final Lcom/pspdfkit/internal/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lkotlin/Lazy;

.field private final c:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/l;->a:Landroid/content/Context;

    .line 3
    new-instance p1, Lcom/pspdfkit/internal/l$a;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/l$a;-><init>(Lcom/pspdfkit/internal/l;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/l;->b:Lkotlin/Lazy;

    .line 7
    new-instance p1, Lcom/pspdfkit/internal/l$b;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/l$b;-><init>(Lcom/pspdfkit/internal/l;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/l;->c:Lkotlin/Lazy;

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final b()Lcom/pspdfkit/internal/f7;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/f7;

    return-object v0
.end method

.method public final c()Lcom/pspdfkit/internal/wh;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/l;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/wh;

    return-object v0
.end method

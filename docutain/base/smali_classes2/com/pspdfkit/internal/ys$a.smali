.class final Lcom/pspdfkit/internal/ys$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/zs$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/ys;->setupDialog(Landroid/app/Dialog;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/ys;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/ys;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ys$a;->a:Lcom/pspdfkit/internal/ys;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ys$a;->a:Lcom/pspdfkit/internal/ys;

    invoke-static {v0}, Lcom/pspdfkit/internal/ys;->-$$Nest$fgetj(Lcom/pspdfkit/internal/ys;)Lcom/pspdfkit/internal/zs;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ys$a;->a:Lcom/pspdfkit/internal/ys;

    invoke-static {v0}, Lcom/pspdfkit/internal/ys;->-$$Nest$fgetj(Lcom/pspdfkit/internal/ys;)Lcom/pspdfkit/internal/zs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zs;->d()V

    goto :goto_0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ys$a;->a:Lcom/pspdfkit/internal/ys;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ys$a;->a:Lcom/pspdfkit/internal/ys;

    invoke-static {v0}, Lcom/pspdfkit/internal/ys;->-$$Nest$fgetc(Lcom/pspdfkit/internal/ys;)Lcom/pspdfkit/internal/ys$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/us;

    invoke-virtual {v0, p1, p2}, Lcom/pspdfkit/internal/us;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V

    :cond_0
    return-void
.end method

.class final Lcom/pspdfkit/internal/rs;
.super Lcom/pspdfkit/internal/c0;
.source "SourceFile"


# instance fields
.field private b:Landroid/net/Uri;

.field private c:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$h4ksmAifD2e8lkYsFW5hHo3cw-A(Lcom/pspdfkit/internal/rs;Lcom/pspdfkit/annotations/Annotation;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/rs;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/net/Uri;)V

    return-void
.end method

.method constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/c0;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/annotations/StampAnnotation;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/c0;-><init>()V

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/c0;->b(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;Landroid/net/Uri;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 20
    iput-object p2, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    .line 21
    new-instance v0, Landroid/content/ClipData;

    .line 22
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "image/jpeg"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ClipData$Item;

    invoke-direct {v2, p2}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-direct {v0, p1, v1, v2}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    const-string p1, "data"

    .line 23
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 24
    invoke-static {v0, p1, p2, p2, p2}, Lcom/pspdfkit/internal/j5;->a(Landroid/content/ClipData;Landroid/content/Context;III)Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/annotations/Annotation;
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/c0;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 4
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 5
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v2, 0x0

    .line 9
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 10
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 15
    invoke-static {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$BitmapStampBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$BitmapStampBuilder;->build()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->createStampAnnotation(I)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object v0

    .line 16
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/c0;->b(Lcom/pspdfkit/annotations/Annotation;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Internal.StampAnnotationClipboardSource"

    const-string v3, "File for the current imageFileUri was not found and the exception was ignored.This is not an issue; just means the current annotation does not have a bitmap attached to it."

    .line 19
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/c0;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final c()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/c0;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 3
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 10
    iget-object v2, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/pspdfkit/document/sharing/DocumentSharingProvider;->deleteFile(Landroid/content/Context;Landroid/net/Uri;)Z

    .line 12
    :cond_0
    iput-object v0, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    :cond_1
    return-void
.end method

.method public final d()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/rs;->a()Lcom/pspdfkit/annotations/Annotation;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/pspdfkit/annotations/StampAnnotation;

    if-eqz v1, :cond_3

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 7
    iput-object v2, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    .line 9
    new-instance v3, Landroid/content/ClipData;

    .line 10
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "image/jpeg"

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/content/ClipData$Item;

    invoke-direct {v5, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-direct {v3, v0, v4, v5}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    const-string v0, "data"

    .line 11
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 12
    invoke-static {v3, v2, v0, v0, v0}, Lcom/pspdfkit/internal/j5;->a(Landroid/content/ClipData;Landroid/content/Context;III)Z

    return-void

    .line 13
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->e()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 16
    :cond_1
    move-object v3, v0

    check-cast v3, Lcom/pspdfkit/annotations/StampAnnotation;

    invoke-virtual {v3}, Lcom/pspdfkit/annotations/StampAnnotation;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_2

    return-void

    .line 17
    :cond_2
    iget-object v4, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 18
    invoke-static {v4}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 19
    iput-object v2, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 20
    invoke-static {v1, v3}, Lcom/pspdfkit/document/sharing/DocumentSharingProviderProcessor;->prepareBitmapForSharing(Landroid/content/Context;Landroid/graphics/Bitmap;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 21
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/u;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    .line 22
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/rs$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/rs$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/rs;Lcom/pspdfkit/annotations/Annotation;)V

    .line 23
    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/rs;->c:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_3
    return-void
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1
    :cond_0
    instance-of v0, p1, Lcom/pspdfkit/internal/rs;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/rs;

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    iget-object p1, p1, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1
    iget-object v1, p0, Lcom/pspdfkit/internal/rs;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

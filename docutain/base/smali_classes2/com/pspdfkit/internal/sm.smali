.class public final Lcom/pspdfkit/internal/sm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/yf;


# instance fields
.field private a:Lcom/pspdfkit/ui/PdfFragment;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/pspdfkit/ui/tabs/PdfTabBar;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Z

.field private final h:Ljava/util/ArrayList;

.field i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

.field private final j:Lcom/pspdfkit/ui/PdfThumbnailBar;

.field private final k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

.field private final l:Lcom/pspdfkit/ui/PdfOutlineView;

.field private final m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

.field private final n:Lcom/pspdfkit/ui/PdfReaderView;

.field private final o:Lcom/pspdfkit/ui/forms/FormEditingBar;

.field private final p:Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

.field private final q:Lcom/pspdfkit/ui/redaction/RedactionView;

.field private final r:Lcom/pspdfkit/ui/audio/AudioView;

.field private final s:Landroid/view/View;

.field private t:Lcom/pspdfkit/ui/search/PdfSearchView;

.field private final u:Landroid/util/SparseBooleanArray;


# direct methods
.method public static synthetic $r8$lambda$gwTZSjdQFUPia3oHggtJ_BffSSE(Lcom/pspdfkit/internal/sm;Lcom/pspdfkit/ui/search/PdfSearchViewLazy;Lcom/pspdfkit/ui/search/PdfSearchView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/sm;->a(Lcom/pspdfkit/ui/search/PdfSearchViewLazy;Lcom/pspdfkit/ui/search/PdfSearchView;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;)V
    .locals 11

    const-string v0, "navigation buttons"

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/sm;->u:Landroid/util/SparseBooleanArray;

    const-string v1, "rootView"

    .line 5
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "configuration"

    .line 6
    invoke-static {p2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    .line 8
    iput-object p2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 9
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentEditorEnabled()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    .line 10
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v2

    sget-object v5, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->DOCUMENT_EDITING:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v2, v5}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/pspdfkit/internal/sm;->g:Z

    .line 13
    :try_start_0
    sget v2, Lcom/pspdfkit/R$id;->pspdf__activity_page_overlay:I

    .line 16
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageNumberOverlay()Z

    move-result v5

    const-string v6, "R.id.pspdf__activity_page_overlay"

    const-string v7, "page number overlay"

    .line 17
    invoke-static {v2, p1, v5, v6, v7}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/pspdfkit/internal/sm;->b:Landroid/widget/TextView;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_e

    .line 31
    :try_start_1
    sget v2, Lcom/pspdfkit/R$id;->pspdf__activity_title_overlay:I

    .line 34
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowDocumentTitleOverlayEnabled()Z

    move-result v5

    const-string v6, "R.id.pspdf__activity_title_overlay"

    const-string v7, "document title overlay"

    .line 35
    invoke-static {v2, p1, v5, v6, v7}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/pspdfkit/internal/sm;->c:Landroid/widget/TextView;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_d

    .line 49
    :try_start_2
    sget v2, Lcom/pspdfkit/R$id;->pspdf__activity_tab_bar:I

    .line 52
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getTabBarHidingMode()Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    move-result-object v5

    sget-object v6, Lcom/pspdfkit/configuration/activity/TabBarHidingMode;->HIDE:Lcom/pspdfkit/configuration/activity/TabBarHidingMode;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    const-string v6, "R.id.pspdf__activity_tab_bar"

    const-string v7, "the tab bar"

    .line 53
    invoke-static {v2, p1, v5, v6, v7}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/tabs/PdfTabBar;

    iput-object v2, p0, Lcom/pspdfkit/internal/sm;->d:Lcom/pspdfkit/ui/tabs/PdfTabBar;
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_c

    .line 67
    :try_start_3
    sget v2, Lcom/pspdfkit/R$id;->pspdf__navigate_back:I

    .line 70
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v5

    const-string v6, "R.id.pspdf__navigate_back"

    .line 71
    invoke-static {v2, p1, v5, v6, v0}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/pspdfkit/internal/sm;->e:Landroid/view/View;
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_b

    .line 85
    :try_start_4
    sget v2, Lcom/pspdfkit/R$id;->pspdf__navigate_forward:I

    .line 88
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowNavigationButtonsEnabled()Z

    move-result v5

    const-string v6, "R.id.pspdf__navigate_forward"

    .line 89
    invoke-static {v2, p1, v5, v6, v0}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/sm;->f:Landroid/view/View;
    :try_end_4
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_a

    .line 103
    :try_start_5
    sget v0, Lcom/pspdfkit/R$id;->pspdf__activity_thumbnail_bar:I

    .line 106
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v2

    sget-object v5, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    if-eq v2, v5, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    const-string v5, "R.id.pspdf__activity_thumbnail_bar"

    const-string v6, "the thumbnail bar"

    .line 107
    invoke-static {v0, p1, v2, v5, v6}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/ui/PdfThumbnailBar;

    iput-object v0, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_5} :catch_9

    .line 121
    :try_start_6
    sget v2, Lcom/pspdfkit/R$id;->pspdf__activity_thumbnail_grid:I

    .line 124
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result v5

    const-string v6, "R.id.pspdf__activity_thumbnail_grid"

    const-string v7, "the thumbnail grid"

    .line 125
    invoke-static {v2, p1, v5, v6, v7}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/PdfThumbnailGrid;

    iput-object v2, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;
    :try_end_6
    .catch Ljava/lang/ClassCastException; {:try_start_6 .. :try_end_6} :catch_8

    .line 139
    :try_start_7
    sget v5, Lcom/pspdfkit/R$id;->pspdf__activity_outline_view:I

    .line 142
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v6

    const-string v7, "R.id.pspdf__activity_outline_view"

    const-string v8, "the document outline"

    .line 143
    invoke-static {v5, p1, v6, v7, v8}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/pspdfkit/ui/PdfOutlineView;

    iput-object v5, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_7} :catch_7

    .line 156
    sget v6, Lcom/pspdfkit/R$id;->pspdf__activity_document_info_view:I

    .line 157
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    const/4 v7, 0x0

    if-nez v6, :cond_6

    .line 159
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 160
    new-instance v6, Lcom/pspdfkit/ui/PdfDocumentInfoView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/pspdfkit/ui/PdfDocumentInfoView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

    if-eqz v5, :cond_4

    .line 166
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 167
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 168
    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getClipToPadding()Z

    move-result v10

    invoke-virtual {v6, v10}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 169
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v10

    invoke-virtual {v6, v10}, Lcom/pspdfkit/ui/PdfOutlineView;->setVisibility(I)V

    goto :goto_4

    .line 171
    :cond_4
    new-instance v9, Landroid/view/ViewGroup$LayoutParams;

    const/4 v8, -0x1

    invoke-direct {v9, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 173
    invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    const/4 v8, 0x4

    .line 174
    invoke-virtual {v6, v8}, Lcom/pspdfkit/ui/PdfOutlineView;->setVisibility(I)V

    move-object v8, v7

    .line 176
    :goto_4
    invoke-virtual {v6, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-nez v8, :cond_5

    .line 178
    sget v8, Lcom/pspdfkit/R$id;->pspdf__activity_content:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    :cond_5
    if-eqz v8, :cond_8

    .line 181
    invoke-virtual {v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_6

    .line 184
    :cond_6
    :try_start_8
    sget v6, Lcom/pspdfkit/R$id;->pspdf__activity_document_info_view:I

    .line 187
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v8, 0x1

    goto :goto_5

    :cond_7
    const/4 v8, 0x0

    :goto_5
    const-string v9, "R.id.pspdf__activity_document_info_view"

    const-string v10, "the document info"

    .line 188
    invoke-static {v6, p1, v8, v9, v10}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/ui/PdfDocumentInfoView;

    iput-object v6, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;
    :try_end_8
    .catch Ljava/lang/ClassCastException; {:try_start_8 .. :try_end_8} :catch_6

    .line 204
    :cond_8
    :goto_6
    :try_start_9
    sget v6, Lcom/pspdfkit/R$id;->pspdf__activity_reader_view:I

    .line 207
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isReaderViewEnabled()Z

    move-result v8

    const-string v9, "R.id.pspdf__activity_reader_view"

    const-string v10, "the document reader view"

    .line 208
    invoke-static {v6, p1, v8, v9, v10}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/pspdfkit/ui/PdfReaderView;

    iput-object v6, p0, Lcom/pspdfkit/internal/sm;->n:Lcom/pspdfkit/ui/PdfReaderView;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_9} :catch_5

    .line 221
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 222
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchType()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_9

    .line 224
    :try_start_a
    sget v7, Lcom/pspdfkit/R$id;->pspdf__activity_search_view_modular:I

    .line 227
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result v8

    const-string v9, "R.id.pspdf__activity_search_view_modular"

    const-string v10, "the modular search"

    .line 228
    invoke-static {v7, p1, v8, v9, v10}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/ui/search/PdfSearchView;

    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;
    :try_end_a
    .catch Ljava/lang/ClassCastException; {:try_start_a .. :try_end_a} :catch_0

    goto :goto_7

    :catch_0
    move-exception p1

    .line 235
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_search_view_modular\' has to be of type com.pspdfkit.ui.search.PdfSearchViewModular or com.pspdfkit.ui.search.PdfSearchViewLazy."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 244
    :cond_9
    new-instance v7, Lcom/pspdfkit/internal/sm$a;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8, p1}, Lcom/pspdfkit/internal/sm$a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    .line 257
    :goto_7
    iget-object v7, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v8, v7, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    if-eqz v8, :cond_b

    .line 258
    check-cast v7, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    new-instance v8, Lcom/pspdfkit/internal/sm$$ExternalSyntheticLambda1;

    invoke-direct {v8, p0}, Lcom/pspdfkit/internal/sm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/sm;)V

    .line 259
    invoke-virtual {v7, v8}, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;->setOnViewReadyListener(Lcom/pspdfkit/ui/search/PdfSearchViewLazy$OnViewReadyListener;)V

    goto :goto_8

    .line 263
    :cond_a
    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    .line 266
    :cond_b
    :goto_8
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/pspdfkit/configuration/PdfConfiguration;->isFormEditingEnabled()Z

    move-result v7

    .line 268
    :try_start_b
    sget v8, Lcom/pspdfkit/R$id;->pspdf__activity_form_editing_bar:I

    const-string v9, "R.id.pspdf__activity_form_editing_bar"

    const-string v10, "the form editing"

    invoke-static {v8, p1, v7, v9, v10}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/ui/forms/FormEditingBar;

    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->o:Lcom/pspdfkit/ui/forms/FormEditingBar;
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_b .. :try_end_b} :catch_4

    .line 283
    :try_start_c
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v7

    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result v7

    .line 284
    sget v8, Lcom/pspdfkit/R$id;->pspdf__activity_content_editing_bar:I

    const-string v9, "R.id.pspdf__activity_content_editing_bar"

    const-string v10, "the content editing"

    invoke-static {v8, p1, v7, v9, v10}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->p:Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;
    :try_end_c
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_c} :catch_3

    .line 298
    :try_start_d
    sget v7, Lcom/pspdfkit/R$id;->pspdf__activity_audio_inspector:I

    const-string v8, "R.id.pspdf__activity_audio_inspector"

    const-string v9, "the sound annotations"

    invoke-static {v7, p1, v4, v8, v9}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/pspdfkit/ui/audio/AudioView;

    iput-object v7, p0, Lcom/pspdfkit/internal/sm;->r:Lcom/pspdfkit/ui/audio/AudioView;
    :try_end_d
    .catch Ljava/lang/ClassCastException; {:try_start_d .. :try_end_d} :catch_2

    .line 311
    invoke-virtual {p2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isRedactionUiEnabled()Z

    move-result p2

    if-eqz p2, :cond_c

    .line 312
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p2

    sget-object v7, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p2, v7}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result p2

    if-eqz p2, :cond_c

    goto :goto_9

    :cond_c
    const/4 v3, 0x0

    .line 314
    :goto_9
    :try_start_e
    sget p2, Lcom/pspdfkit/R$id;->pspdf__redaction_view:I

    const-string v4, "R.id.pspdf__redaction_view"

    const-string v7, "the redaction UI"

    invoke-static {p2, p1, v3, v4, v7}, Lcom/pspdfkit/internal/sm;->a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/redaction/RedactionView;

    iput-object p2, p0, Lcom/pspdfkit/internal/sm;->q:Lcom/pspdfkit/ui/redaction/RedactionView;
    :try_end_e
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_e} :catch_1

    .line 327
    sget p2, Lcom/pspdfkit/R$id;->pspdf__activity_empty_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/sm;->s:Landroid/view/View;

    if-eqz p1, :cond_d

    const/16 p2, 0x8

    .line 329
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 332
    :cond_d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    new-instance p1, Lcom/pspdfkit/internal/sm$b;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/sm$b;-><init>(Lcom/pspdfkit/internal/sm;)V

    if-eqz v6, :cond_e

    .line 352
    invoke-virtual {v6, p1}, Lcom/pspdfkit/ui/PdfReaderView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    :cond_e
    if-eqz v5, :cond_f

    .line 355
    invoke-virtual {v5, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    .line 357
    :cond_f
    iget-object p2, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

    if-eqz p2, :cond_10

    .line 358
    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    :cond_10
    if-eqz v2, :cond_11

    .line 361
    invoke-virtual {v2, p1}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    :cond_11
    return-void

    :catch_1
    move-exception p1

    .line 362
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__redaction_view\' has to be of type com.pspdfkit.ui.redaction.RedactionView"

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_2
    move-exception p1

    .line 363
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_audio_inspector\' has to be of type com.pspdfkit.ui.audio.AudioInspector"

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_3
    move-exception p1

    .line 364
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'pspdf__activity_content_editing_bar\' has to be of type com.pspdfkit.ui.contentediting.ContentEditingStylingBar"

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_4
    move-exception p1

    .line 365
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_form_input_bar\' has to be of type com.pspdfkit.ui.forms.FormEditingBar"

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_5
    move-exception p1

    .line 366
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_reader_view\' has to be of type com.pspdfkit.ui.PdfReaderView."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_6
    move-exception p1

    .line 367
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_document_info_view\' has to be of type com.pspdfkit.ui.PSPDFDocumentInfoView."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_7
    move-exception p1

    .line 368
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_outline_view\' has to be of type com.pspdfkit.ui.PSPDFOutlineView."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_8
    move-exception p1

    .line 369
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_thumbnail_grid\' has to be of type com.pspdfkit.ui.PSPDFThumbnailGrid."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_9
    move-exception p1

    .line 370
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_thumbnail_bar\' has to be of type com.pspdfkit.ui.PSPDFThumbnailBar."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_a
    move-exception p1

    .line 371
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__navigate_forward\' has to be of type android.view.View."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_b
    move-exception p1

    .line 372
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__navigate_back\' has to be of type android.view.View."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_c
    move-exception p1

    .line 373
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_tab_bar\' has to be of type com.pspdfkit.ui.tabs.PdfTabsBar."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_d
    move-exception p1

    .line 374
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_title_overlay\' has to be of type android.widget.TextView."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_e
    move-exception p1

    .line 375
    new-instance p2, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    const-string v0, "Exception while inflating activity layout. View with id \'R.id.pspdf__activity_page_overlay\' has to be of type android.widget.TextView."

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private static a(ILandroid/view/View;ZLjava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 0

    .line 2
    invoke-virtual {p1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-nez p0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidLayoutException;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "The activity layout was missing a View with id \'"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'. Add this view to your layout file or deactivate "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " in your PdfActivityConfiguration."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidLayoutException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    :goto_0
    return-object p0
.end method

.method private synthetic a(Lcom/pspdfkit/ui/search/PdfSearchViewLazy;Lcom/pspdfkit/ui/search/PdfSearchView;)V
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/ui/PdfFragment;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    return-object v0
.end method

.method public final a(Lcom/pspdfkit/ui/PdfFragment;)V
    .locals 5

    .line 6
    iput-object p1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    const/16 v1, 0x8

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object p1

    sget-object v0, Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;->THUMBNAIL_BAR_MODE_NONE:Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    if-eq p1, v0, :cond_1

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getThumbnailBarMode()Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->setThumbnailBarMode(Lcom/pspdfkit/configuration/activity/ThumbnailBarMode;)V

    .line 11
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/PdfThumbnailBar;->getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    goto :goto_0

    .line 13
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    if-nez v0, :cond_3

    goto :goto_1

    .line 16
    :cond_3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isThumbnailGridEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->setShowPageLabels(Z)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    iget-boolean v0, p0, Lcom/pspdfkit/internal/sm;->g:Z

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/PdfThumbnailGrid;->setDocumentEditorEnabled(Z)V

    goto :goto_1

    .line 21
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 22
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    if-nez p1, :cond_6

    goto :goto_2

    .line 24
    :cond_6
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isSearchEnabled()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getSearchConfiguration()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object p1

    if-nez p1, :cond_7

    .line 27
    new-instance p1, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;

    invoke-direct {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/search/SearchConfiguration$Builder;->build()Lcom/pspdfkit/configuration/search/SearchConfiguration;

    move-result-object p1

    .line 28
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/search/PdfSearchView;->setSearchConfiguration(Lcom/pspdfkit/configuration/search/SearchConfiguration;)V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v0, p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    if-eqz v0, :cond_8

    .line 30
    check-cast p1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v0}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/ui/search/PdfSearchViewModular;->setShowPageLabels(Z)V

    .line 33
    :cond_8
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v0, p1, Lcom/pspdfkit/listeners/DocumentListener;

    if-eqz v0, :cond_9

    .line 34
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    check-cast p1, Lcom/pspdfkit/listeners/DocumentListener;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 35
    :cond_9
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    const/4 v0, 0x0

    if-eqz p1, :cond_11

    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    if-nez p1, :cond_a

    goto/16 :goto_6

    .line 37
    :cond_a
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 38
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result p1

    const/4 v2, 0x1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result p1

    if-nez p1, :cond_b

    const/4 p1, 0x1

    goto :goto_3

    :cond_b
    const/4 p1, 0x0

    .line 40
    :goto_3
    iget-object v3, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v4, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v4}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewSeparated()Z

    move-result v4

    xor-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/pspdfkit/ui/PdfOutlineView;->setMayContainDocumentInfoView(Z)V

    .line 42
    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 43
    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    .line 44
    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v2

    if-nez v2, :cond_d

    if-eqz p1, :cond_c

    goto :goto_4

    .line 69
    :cond_c
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setVisibility(I)V

    goto :goto_5

    .line 70
    :cond_d
    :goto_4
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    instance-of v2, v1, Lcom/pspdfkit/instant/ui/InstantPdfFragment;

    if-nez v2, :cond_e

    .line 71
    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfFragment;->getUndoManager()Lcom/pspdfkit/undo/UndoManager;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setUndoManager(Lcom/pspdfkit/undo/UndoManager;)V

    .line 79
    :cond_e
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setOutlineViewEnabled(ZZ)V

    .line 80
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1, p1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setDocumentInfoViewEnabled(ZZ)V

    .line 81
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v1

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setAnnotationListViewEnabled(ZZ)V

    .line 82
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result v1

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setBookmarkViewEnabled(ZZ)V

    .line 84
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfOutlineView;->refreshViewPager()V

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkEditingEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setBookmarkEditingEnabled(Z)V

    .line 87
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isShowPageLabels()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setShowPageLabels(Z)V

    .line 88
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getListedAnnotationTypes()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setListedAnnotationTypes(Ljava/util/EnumSet;)V

    .line 89
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListReorderingEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setAnnotationListReorderingEnabled(Z)V

    .line 94
    :goto_5
    new-instance p1, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {p1, v1}, Lcom/pspdfkit/ui/outline/DefaultOutlineViewListener;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    .line 95
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isAnnotationListEnabled()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 96
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->setOnAnnotationTapListener(Lcom/pspdfkit/ui/PdfOutlineView$OnAnnotationTapListener;)V

    .line 98
    :cond_f
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isOutlineEnabled()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 99
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/PdfOutlineView;->setOnOutlineElementTapListener(Lcom/pspdfkit/ui/PdfOutlineView$OnOutlineElementTapListener;)V

    .line 102
    :cond_10
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {p1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isBookmarkListEnabled()Z

    move-result p1

    if-eqz p1, :cond_11

    .line 103
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    new-instance v1, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;

    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-direct {v1, v2}, Lcom/pspdfkit/ui/outline/DefaultBookmarkAdapter;-><init>(Lcom/pspdfkit/ui/PdfFragment;)V

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfOutlineView;->setBookmarkAdapter(Lcom/pspdfkit/ui/outline/BookmarkViewAdapter;)V

    .line 104
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    invoke-virtual {v1}, Lcom/pspdfkit/ui/PdfOutlineView;->getDocumentListener()Lcom/pspdfkit/listeners/DocumentListener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/pspdfkit/ui/PdfFragment;->addDocumentListener(Lcom/pspdfkit/listeners/DocumentListener;)V

    .line 105
    :cond_11
    :goto_6
    iget-object p1, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

    if-eqz p1, :cond_13

    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    if-nez v1, :cond_12

    goto :goto_7

    .line 106
    :cond_12
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v1}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->isDocumentInfoViewEnabled()Z

    move-result v1

    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/ui/PdfOutlineView;->setDocumentInfoViewEnabled(ZZ)V

    :cond_13
    :goto_7
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->a:Lcom/pspdfkit/ui/PdfFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->d:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->e:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 116
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->f:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 119
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->q:Lcom/pspdfkit/ui/redaction/RedactionView;

    if-eqz v1, :cond_4

    .line 122
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    if-eqz v1, :cond_5

    .line 125
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 129
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_6

    .line 130
    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    if-eqz p1, :cond_7

    const/high16 v4, 0x40000

    .line 133
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 136
    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->u:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 139
    :cond_7
    iget-object v4, p0, Lcom/pspdfkit/internal/sm;->u:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 140
    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    const/high16 v1, 0x60000

    .line 143
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    goto :goto_0

    :cond_8
    return-void
.end method

.method public final addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 55
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->addOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b()Lcom/pspdfkit/ui/search/PdfSearchView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    return-object v0
.end method

.method public final getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 2
    invoke-interface {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->isDisplayed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v2

    sget-object v3, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-eq v2, v3, :cond_0

    .line 3
    invoke-interface {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    return-object v0

    .line 6
    :cond_1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    return-object v0
.end method

.method public final getAudioInspector()Lcom/pspdfkit/ui/audio/AudioView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->r:Lcom/pspdfkit/ui/audio/AudioView;

    return-object v0
.end method

.method public final getContentEditingStylingBarView()Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->p:Lcom/pspdfkit/ui/contentediting/ContentEditingStylingBar;

    return-object v0
.end method

.method public final getDocumentInfoView()Lcom/pspdfkit/ui/PdfDocumentInfoView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->m:Lcom/pspdfkit/ui/PdfDocumentInfoView;

    return-object v0
.end method

.method public final getDocumentTitleOverlayView()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getEmptyView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->s:Landroid/view/View;

    return-object v0
.end method

.method public final getFormEditingBarView()Lcom/pspdfkit/ui/forms/FormEditingBar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->o:Lcom/pspdfkit/ui/forms/FormEditingBar;

    return-object v0
.end method

.method public final getNavigateBackButton()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->e:Landroid/view/View;

    return-object v0
.end method

.method public final getNavigateForwardButton()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->f:Landroid/view/View;

    return-object v0
.end method

.method public final getOutlineView()Lcom/pspdfkit/ui/PdfOutlineView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->l:Lcom/pspdfkit/ui/PdfOutlineView;

    return-object v0
.end method

.method public final getPageNumberOverlayView()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getReaderView()Lcom/pspdfkit/ui/PdfReaderView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->n:Lcom/pspdfkit/ui/PdfReaderView;

    return-object v0
.end method

.method public final getRedactionView()Lcom/pspdfkit/ui/redaction/RedactionView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->q:Lcom/pspdfkit/ui/redaction/RedactionView;

    return-object v0
.end method

.method public final getSearchView()Lcom/pspdfkit/ui/search/PdfSearchView;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    if-eqz v1, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;->prepareForDisplay()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final getTabBar()Lcom/pspdfkit/ui/tabs/PdfTabBar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->d:Lcom/pspdfkit/ui/tabs/PdfTabBar;

    return-object v0
.end method

.method public final getThumbnailBarView()Lcom/pspdfkit/ui/PdfThumbnailBar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->j:Lcom/pspdfkit/ui/PdfThumbnailBar;

    return-object v0
.end method

.method public final getThumbnailGridView()Lcom/pspdfkit/ui/PdfThumbnailGrid;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->k:Lcom/pspdfkit/ui/PdfThumbnailGrid;

    return-object v0
.end method

.method public final getViewByType(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 2
    invoke-interface {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v2

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final onRestoreViewHierarchyState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "PSPDFKitViews.HierarchyState"

    .line 1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 5
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-virtual {v0, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 9
    :cond_1
    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    if-eqz v1, :cond_3

    .line 13
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;

    invoke-virtual {v0}, Lcom/pspdfkit/ui/search/PdfSearchViewLazy;->getSearchView()Lcom/pspdfkit/ui/search/PdfSearchView;

    move-result-object v0

    .line 14
    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    if-eqz v1, :cond_2

    .line 15
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {v0, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 16
    :cond_2
    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    if-eqz v1, :cond_4

    .line 17
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-virtual {v0, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 19
    :cond_3
    instance-of v1, v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    if-eqz v1, :cond_4

    .line 20
    check-cast v0, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {v0, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public final onSaveViewHierarchyState(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/sm;->t:Lcom/pspdfkit/ui/search/PdfSearchView;

    instance-of v2, v1, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    if-eqz v2, :cond_0

    .line 6
    check-cast v1, Lcom/pspdfkit/ui/search/PdfSearchViewInline;

    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 7
    :cond_0
    instance-of v2, v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    if-eqz v2, :cond_1

    .line 8
    check-cast v1, Lcom/pspdfkit/ui/search/PdfSearchViewModular;

    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    :cond_1
    :goto_0
    const-string v1, "PSPDFKitViews.HierarchyState"

    .line 10
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    return-void
.end method

.method public final removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V
    .locals 2

    const-string v0, "listener"

    const-string v1, "argumentName"

    .line 1
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 55
    invoke-interface {v1, p1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->removeOnVisibilityChangedListener(Lcom/pspdfkit/listeners/OnVisibilityChangedListener;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final resetDocument()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {v1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->clearDocument()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final setDocument(Lcom/pspdfkit/document/PdfDocument;)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "setDocument() must be called on the main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    const-string v0, "document"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/pspdfkit/internal/sm;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    if-eqz v1, :cond_0

    .line 56
    iget-object v2, p0, Lcom/pspdfkit/internal/sm;->i:Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;

    invoke-virtual {v2}, Lcom/pspdfkit/configuration/activity/PdfActivityConfiguration;->getConfiguration()Lcom/pspdfkit/configuration/PdfConfiguration;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->setDocument(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final showView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Z
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    const/4 v1, 0x0

    if-eq p1, v0, :cond_3

    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    if-ne v0, p1, :cond_1

    return v1

    .line 10
    :cond_1
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sm;->getViewByType(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    move-result-object v0

    .line 11
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/sm;->getViewByType(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->show()V

    if-eqz v0, :cond_2

    .line 16
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->hide()V

    :cond_2
    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    return v1
.end method

.method public final toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Z
    .locals 2

    const-wide/16 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0, v1}, Lcom/pspdfkit/internal/sm;->toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z

    move-result p1

    return p1
.end method

.method public final toggleView(Lcom/pspdfkit/ui/PSPDFKitViews$Type;J)Z
    .locals 3

    .line 2
    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_THUMBNAIL_BAR:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    return v1

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/internal/sm;->getActiveViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/sm;->getViewByType(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 7
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->hide()V

    .line 10
    invoke-interface {v0}, Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;->getPSPDFViewType()Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    move-result-object v0

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/pspdfkit/ui/PSPDFKitViews$Type;->VIEW_NONE:Lcom/pspdfkit/ui/PSPDFKitViews$Type;

    if-ne p1, v0, :cond_2

    :cond_1
    return v2

    .line 14
    :cond_2
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/sm;->getViewByType(Lcom/pspdfkit/ui/PSPDFKitViews$Type;)Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 16
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/pspdfkit/internal/sm$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/sm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/ui/PSPDFKitViews$PSPDFView;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return v2

    :cond_3
    return v1
.end method

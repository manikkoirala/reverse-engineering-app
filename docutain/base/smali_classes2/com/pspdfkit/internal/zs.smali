.class public final Lcom/pspdfkit/internal/zs;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/zs$a;
    }
.end annotation


# static fields
.field private static final l:[I

.field private static final m:I

.field private static final n:I


# instance fields
.field private final b:Lcom/pspdfkit/internal/zs$a;

.field private c:Lcom/pspdfkit/internal/r7;

.field private d:Lcom/pspdfkit/internal/xs;

.field private e:Landroid/view/ViewGroup;

.field private f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private g:Landroid/widget/FrameLayout;

.field private h:I

.field private i:I

.field private j:Z

.field private final k:Z


# direct methods
.method public static synthetic $r8$lambda$0St1HLpgZr9ntM-VtIRUrrXgbl8(Lcom/pspdfkit/internal/zs;Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zs;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    return-void
.end method

.method public static synthetic $r8$lambda$1TiQufZ7BFA08cU-Si4mPzbWW-E(Lcom/pspdfkit/internal/zs;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zs;->a(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$BU2_YK1hRMauZLOsqXtBxUbH7Lg(Lcom/pspdfkit/internal/zs;Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/zs;->b(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker:[I

    sput-object v0, Lcom/pspdfkit/internal/zs;->l:[I

    .line 2
    sget v0, Lcom/pspdfkit/R$attr;->pspdf__stampPickerStyle:I

    sput v0, Lcom/pspdfkit/internal/zs;->m:I

    .line 3
    sget v0, Lcom/pspdfkit/R$style;->PSPDFKit_StampPicker:I

    sput v0, Lcom/pspdfkit/internal/zs;->n:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/pspdfkit/internal/zs$a;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {p1}, Lcom/pspdfkit/internal/zs;->b(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zs;->j:Z

    .line 10
    iput-object p3, p0, Lcom/pspdfkit/internal/zs;->b:Lcom/pspdfkit/internal/zs$a;

    .line 11
    iput-boolean p2, p0, Lcom/pspdfkit/internal/zs;->k:Z

    .line 12
    invoke-direct {p0}, Lcom/pspdfkit/internal/zs;->a()V

    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/content/res/TypedArray;
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    sget-object v0, Lcom/pspdfkit/internal/zs;->l:[I

    sget v1, Lcom/pspdfkit/internal/zs;->m:I

    sget v2, Lcom/pspdfkit/internal/zs;->n:I

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    return-object p0
.end method

.method private a()V
    .locals 5

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;-><init>(Landroid/content/Context;)V

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->getCornerRadius()I

    move-result v1

    iput v1, p0, Lcom/pspdfkit/internal/zs;->i:I

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/zs;->a(Landroid/content/Context;)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 6
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__StampPicker_pspdf__backgroundColor:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/pspdfkit/internal/zs;->h:I

    .line 7
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 10
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    .line 12
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 15
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    .line 16
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 20
    new-instance v2, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 21
    invoke-virtual {v2, p0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 25
    invoke-virtual {p0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 27
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 30
    new-instance v0, Lcom/pspdfkit/internal/r7;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda1;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/zs;)V

    invoke-direct {v0, v3, v4}, Lcom/pspdfkit/internal/r7;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/r7$b;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/xs;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda2;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/zs;)V

    invoke-direct {v0, v3, v4}, Lcom/pspdfkit/internal/xs;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/xs$a;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    .line 44
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zs;->k:Z

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/pspdfkit/R$string;->pspdf__create_stamp:I

    invoke-static {v3, v4, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    iput-object v0, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__annotation_type_stamp:I

    invoke-static {v2, v3, p0}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    iput-object v0, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    .line 54
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 57
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/zs;->setFullscreen(Z)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 1

    const/16 v0, 0x8

    .line 60
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;I)V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v0, 0x0

    .line 64
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 66
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 67
    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v1, 0xc8

    .line 68
    invoke-virtual {v0, v1, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 70
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    neg-int p2, v0

    int-to-float p2, p2

    goto :goto_0

    :cond_0
    int-to-float p2, v0

    .line 73
    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 75
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 77
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 78
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->b:Lcom/pspdfkit/internal/zs$a;

    if-eqz v0, :cond_0

    .line 59
    check-cast v0, Lcom/pspdfkit/internal/ys$a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ys$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V

    :cond_0
    return-void
.end method

.method static b(Landroid/content/Context;)I
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/internal/zs;->m:I

    sget v1, Lcom/pspdfkit/internal/zs;->n:I

    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result p0

    return p0
.end method

.method private b(Landroid/view/ViewGroup;I)V
    .locals 4

    .line 4
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 6
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 7
    invoke-virtual {v0, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    const-wide/16 v1, 0xc8

    .line 8
    invoke-virtual {v0, v1, v2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    .line 11
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 13
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    int-to-float p2, v0

    goto :goto_0

    :cond_0
    neg-int p2, v0

    int-to-float p2, p2

    .line 14
    :goto_0
    invoke-virtual {v2, p2}, Landroidx/core/view/ViewPropertyAnimatorCompat;->translationX(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    const/high16 p2, 0x3f800000    # 1.0f

    .line 16
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 17
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    .line 19
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object p2

    new-instance v0, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/zs$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/zs;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->b:Lcom/pspdfkit/internal/zs$a;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->isCustomStamp()Z

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/ys$a;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/ys$a;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    if-ne v0, v1, :cond_0

    return-void

    .line 4
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    .line 5
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/zs;->b(Landroid/view/ViewGroup;I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/zs;->a(Landroid/view/ViewGroup;I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/pspdfkit/R$string;->pspdf__create_stamp:I

    const/4 v3, 0x0

    .line 10
    invoke-static {v1, v2, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r7;->b()V

    return-void
.end method

.method public final d()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    if-ne v0, v1, :cond_0

    return-void

    .line 4
    :cond_0
    iput-object v1, p0, Lcom/pspdfkit/internal/zs;->e:Landroid/view/ViewGroup;

    .line 5
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/zs;->b(Landroid/view/ViewGroup;I)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/zs;->a(Landroid/view/ViewGroup;I)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget-boolean v2, p0, Lcom/pspdfkit/internal/zs;->j:Z

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__annotation_type_stamp:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/zs;->b:Lcom/pspdfkit/internal/zs$a;

    if-eqz p1, :cond_0

    .line 4
    check-cast p1, Lcom/pspdfkit/internal/ys$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ys$a;->a()V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 10
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/zs;->j:Z

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 4
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public getCustomStampAnnotation()Lcom/pspdfkit/annotations/stamps/StampPickerItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r7;->getCustomStamp()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object v0

    return-object v0
.end method

.method public getDateSwitchState()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r7;->getDateSwitchState()Z

    move-result v0

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/xs;->getItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTimeSwitchState()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/r7;->getTimeSwitchState()Z

    move-result v0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->getBackButton()Lcom/pspdfkit/ui/toolbar/ContextualToolbarMenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/zs;->b:Lcom/pspdfkit/internal/zs$a;

    if-eqz p1, :cond_0

    .line 3
    check-cast p1, Lcom/pspdfkit/internal/ys$a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ys$a;->a()V

    :cond_0
    return-void
.end method

.method public setCustomStampAnnotation(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r7;->setCustomStamp(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    return-void
.end method

.method public setDateSwitchState(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r7;->setDateSwitchState(Z)V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/zs;->j:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/zs;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    if-nez p1, :cond_2

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 8
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    iget v1, p0, Lcom/pspdfkit/internal/zs;->h:I

    iget v2, p0, Lcom/pspdfkit/internal/zs;->i:I

    invoke-static {p0, v0, v1, v2, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/b;->setRoundedBackground(Landroid/view/View;Lcom/pspdfkit/internal/ui/dialog/utils/a;IIZ)V

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    iget v1, p0, Lcom/pspdfkit/internal/zs;->i:I

    invoke-virtual {v0, v1, p1}, Lcom/pspdfkit/internal/r7;->a(IZ)V

    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->d:Lcom/pspdfkit/internal/xs;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/xs;->setItems(Ljava/util/List;)V

    return-void
.end method

.method public setTimeSwitchState(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zs;->c:Lcom/pspdfkit/internal/r7;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/r7;->setTimeSwitchState(Z)V

    return-void
.end method

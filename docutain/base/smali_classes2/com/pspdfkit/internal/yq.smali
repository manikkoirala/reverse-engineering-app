.class public final Lcom/pspdfkit/internal/yq;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/yq$a;,
        Lcom/pspdfkit/internal/yq$b;,
        Lcom/pspdfkit/internal/yq$c;,
        Lcom/pspdfkit/internal/yq$d;
    }
.end annotation


# instance fields
.field private final b:Lcom/pspdfkit/internal/ar;

.field private final c:Lcom/pspdfkit/internal/zq;

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/pspdfkit/internal/yq$b;",
            "Lcom/pspdfkit/internal/yq$c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/internal/ar;

.field private f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

.field private final g:Landroidx/webkit/WebViewAssetLoader;

.field private final h:Lkotlin/Lazy;

.field private final i:Lkotlin/Lazy;

.field private final j:I

.field private final k:I

.field private final l:I


# direct methods
.method public static synthetic $r8$lambda$20_Xzn0x_SouKlBn_qhMII85zL8(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->g(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$7balyncH-Poojf38Kimb1-OmZ2M(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->k(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$9BmDED1SQpwj5pzPcmAj1QG3OTo(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->l(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$AsD0HXkWvaFjIJCc-64o6lvk5kI(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Cw947hJyb3w9t4rgAsVBezkDfAI(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->m(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$GPsntkFd_F-x8osuq8gZrPm1evg(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->d(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JU_wnJdV18Wadp-YUlSu_XHYkIk(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/yq;->c(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$MLj5HcJ9gau4L_imz6aGR7tVYDg(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->e(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$SVqyaiMEW076QVVvgKGg_PDB914(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->b(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Uu1Ct6F99l1quCakegEawBG6nIo(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->f(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YgmjG8YxMUXky10KhGz7ox32Gsk(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/yq;->b(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$YlzGTKD97vq_bfaeT2r9wfT7ScI(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->c(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fFYNp_-l5Oa-b6YZhI4rWEUwVvw(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->j(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$iJUCR9MW9_FIqkw7CiGauZkUEC4(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->b(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nAIxG_CQLkDg8W3_do6qLvl0XwM(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oaaPc3n2ZE8pEXL08to99gvaLkQ(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$smiKPaumAxJqM4kN_2U4YLQb2SE(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->h(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xCUsg3vZ9MxjYFU6yHeBTvmAobg(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/yq;->i(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/ar;Lcom/pspdfkit/internal/ar;Lcom/pspdfkit/internal/zq;)V
    .locals 3

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settingsDialogListener"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Landroidx/appcompat/view/ContextThemeWrapper;

    .line 5
    sget v1, Lcom/pspdfkit/R$attr;->pspdf__settingsDialogStyle:I

    .line 6
    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_SettingsDialog:I

    .line 7
    invoke-static {p1, v1, v2}, Lcom/pspdfkit/internal/cu;->b(Landroid/content/Context;II)I

    move-result v1

    .line 8
    invoke-direct {v0, p1, v1}, Landroidx/appcompat/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 9
    invoke-direct {p0, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 10
    iput-object p2, p0, Lcom/pspdfkit/internal/yq;->b:Lcom/pspdfkit/internal/ar;

    .line 12
    iput-object p4, p0, Lcom/pspdfkit/internal/yq;->c:Lcom/pspdfkit/internal/zq;

    .line 34
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/yq;->d:Ljava/util/HashMap;

    if-nez p3, :cond_0

    .line 36
    invoke-static {p2}, Lcom/pspdfkit/internal/ar;->a(Lcom/pspdfkit/internal/ar;)Lcom/pspdfkit/internal/ar;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    .line 91
    new-instance p1, Landroidx/webkit/WebViewAssetLoader$Builder;

    invoke-direct {p1}, Landroidx/webkit/WebViewAssetLoader$Builder;-><init>()V

    .line 92
    new-instance p2, Landroidx/webkit/WebViewAssetLoader$AssetsPathHandler;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Landroidx/webkit/WebViewAssetLoader$AssetsPathHandler;-><init>(Landroid/content/Context;)V

    const-string p3, "/assets/"

    invoke-virtual {p1, p3, p2}, Landroidx/webkit/WebViewAssetLoader$Builder;->addPathHandler(Ljava/lang/String;Landroidx/webkit/WebViewAssetLoader$PathHandler;)Landroidx/webkit/WebViewAssetLoader$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Landroidx/webkit/WebViewAssetLoader$Builder;->build()Landroidx/webkit/WebViewAssetLoader;

    move-result-object p1

    const-string p2, "Builder()\n        .addPa\u2026ontext))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/yq;->g:Landroidx/webkit/WebViewAssetLoader;

    .line 96
    new-instance p1, Lcom/pspdfkit/internal/yq$f;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/yq$f;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/yq;->h:Lkotlin/Lazy;

    .line 97
    new-instance p1, Lcom/pspdfkit/internal/yq$g;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/yq$g;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/yq;->i:Lkotlin/Lazy;

    .line 99
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Landroidx/appcompat/R$attr;->colorAccent:I

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/yq;->j:I

    .line 100
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$attr;->pspdf__settings_preset_unselected_border_color:I

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/yq;->k:I

    .line 101
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/pspdfkit/R$attr;->pspdf__settings_preset_label_textcolor:I

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/yq;->l:I

    const/4 p1, 0x1

    .line 105
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 106
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yq;->b(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic a(Lcom/pspdfkit/internal/yq;)Landroidx/webkit/WebViewAssetLoader;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/yq;->g:Landroidx/webkit/WebViewAssetLoader;

    return-object p0
.end method

.method private final a()V
    .locals 9

    .line 101
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/R$styleable;->pspdf__SettingsDialog:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v1, "context.theme.obtainStyl\u2026le.pspdf__SettingsDialog)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPreset()Landroid/view/View;

    move-result-object v1

    .line 104
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SettingsDialog_pspdf__settings_preset_animation_url_vertical:I

    .line 105
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "https://appassets.androidplatform.net/assets/pspdfkit/settings-animations/vertical-single-scroll-light.html"

    :cond_0
    const-string v3, "typedArray.getString(\n  \u2026: VERTICAL_ANIMATION_PATH"

    .line 109
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/yq$b;->e:Lcom/pspdfkit/internal/yq$b;

    iget-object v5, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/yq$b;->a(Lcom/pspdfkit/internal/ar;)Z

    move-result v5

    .line 115
    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    .line 116
    invoke-virtual {v6, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const/4 v7, 0x0

    .line 120
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 122
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 125
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 126
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 128
    new-instance v6, Lcom/pspdfkit/internal/yq$a;

    invoke-direct {v6, p0, v3, v5}, Lcom/pspdfkit/internal/yq$a;-><init>(Lcom/pspdfkit/internal/yq;Landroid/webkit/WebView;Z)V

    invoke-virtual {v3, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 129
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 130
    new-instance v2, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetRadio()Landroid/widget/RadioButton;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 132
    iget-object v1, p0, Lcom/pspdfkit/internal/yq;->d:Ljava/util/HashMap;

    new-instance v2, Lcom/pspdfkit/internal/yq$c;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetRadio()Landroid/widget/RadioButton;

    move-result-object v5

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v6

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getVerticalPresetLabel()Landroid/widget/TextView;

    move-result-object v8

    invoke-direct {v2, v3, v5, v6, v8}, Lcom/pspdfkit/internal/yq$c;-><init>(Lcom/google/android/material/card/MaterialCardView;Landroid/widget/RadioButton;Landroid/webkit/WebView;Landroid/widget/TextView;)V

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPreset()Landroid/view/View;

    move-result-object v1

    .line 136
    sget v2, Lcom/pspdfkit/R$styleable;->pspdf__SettingsDialog_pspdf__settings_preset_animation_url_horizontal:I

    .line 137
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "https://appassets.androidplatform.net/assets/pspdfkit/settings-animations/horizontal-single-scroll-light.html"

    :cond_1
    const-string v3, "typedArray.getString(\n  \u2026HORIZONTAL_ANIMATION_PATH"

    .line 141
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v3

    sget-object v4, Lcom/pspdfkit/internal/yq$b;->d:Lcom/pspdfkit/internal/yq$b;

    iget-object v5, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/yq$b;->a(Lcom/pspdfkit/internal/ar;)Z

    move-result v5

    .line 147
    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    .line 148
    invoke-virtual {v6, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 152
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 154
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 157
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 158
    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 160
    new-instance v6, Lcom/pspdfkit/internal/yq$a;

    invoke-direct {v6, p0, v3, v5}, Lcom/pspdfkit/internal/yq$a;-><init>(Lcom/pspdfkit/internal/yq;Landroid/webkit/WebView;Z)V

    invoke-virtual {v3, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 161
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 162
    new-instance v2, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetRadio()Landroid/widget/RadioButton;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 164
    iget-object v1, p0, Lcom/pspdfkit/internal/yq;->d:Ljava/util/HashMap;

    .line 165
    new-instance v2, Lcom/pspdfkit/internal/yq$c;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetRadio()Landroid/widget/RadioButton;

    move-result-object v5

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetAnimationView()Landroid/webkit/WebView;

    move-result-object v6

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getHorizontalPresetLabel()Landroid/widget/TextView;

    move-result-object v7

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/pspdfkit/internal/yq$c;-><init>(Lcom/google/android/material/card/MaterialCardView;Landroid/widget/RadioButton;Landroid/webkit/WebView;Landroid/widget/TextView;)V

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 3

    .line 89
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__settings_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 90
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->a()V

    .line 94
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getSaveButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 95
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getSaveButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    .line 100
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->b()V

    return-void
.end method

.method private final a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ar;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;)V

    .line 31
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->SINGLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 32
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->DOUBLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 33
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    if-ne v0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 35
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 36
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 37
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 39
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutLabel()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/yq$d;->b:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v3, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 42
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 43
    :cond_4
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    goto :goto_2

    .line 44
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    .line 47
    :goto_2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getLabelText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->d()Z

    if-eqz p2, :cond_6

    .line 51
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_6
    return-void
.end method

.method private final a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V
    .locals 4

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ar;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;)V

    .line 53
    sget-object v0, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne p1, v0, :cond_0

    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings_continuous_horizontal:I

    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v1, Lcom/pspdfkit/R$drawable;->pspdf__ic_settings_continuous_vertical:I

    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_1

    .line 59
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 60
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollHorizontalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v1, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 61
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollVerticalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    if-ne v0, v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 62
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollLabel()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/yq$d;->c:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v3, :cond_5

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 64
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollVerticalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 65
    :cond_5
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollHorizontalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    .line 67
    :goto_2
    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getLabelText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 68
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->d()Z

    if-eqz p2, :cond_6

    .line 72
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_6
    return-void
.end method

.method private final a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V
    .locals 4

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ar;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;)V

    .line 13
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionJumpButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 14
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    if-ne v0, v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 15
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionLabel()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/yq$d;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v3, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 17
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 18
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionJumpButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    .line 20
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getLabelText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 21
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->d()Z

    move-result p1

    if-nez p1, :cond_4

    .line 23
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    :cond_4
    if-eqz p2, :cond_5

    .line 29
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_5
    return-void
.end method

.method private final a(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ar;->a(Lcom/pspdfkit/configuration/theming/ThemeMode;)V

    .line 74
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeDefaultButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 75
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeNightButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    if-ne v0, v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 76
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeLabel()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/yq$d;->d:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v3, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 78
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeNightButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 79
    :cond_3
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeDefaultButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    .line 81
    :goto_1
    invoke-virtual {v0}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->getLabelText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 82
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_4

    .line 88
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_4
    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V
    .locals 1

    const-string v0, "$ui"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 213
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yq$c;->b()Lcom/google/android/material/card/MaterialCardView;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/google/android/material/card/MaterialCardView;->setStrokeColor(I)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/yq$c;Z)V
    .locals 9

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Integer;

    .line 180
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getUnselectedStrokeWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getSelectedStrokeWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Integer;

    .line 181
    iget v5, p0, Lcom/pspdfkit/internal/yq;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    iget v5, p0, Lcom/pspdfkit/internal/yq;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-array v5, v0, [Ljava/lang/Integer;

    .line 182
    iget v6, p0, Lcom/pspdfkit/internal/yq;->l:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    iget v6, p0, Lcom/pspdfkit/internal/yq;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    if-nez p2, :cond_0

    .line 185
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->reverse(Ljava/util/List;)V

    .line 186
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->reverse(Ljava/util/List;)V

    .line 187
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->reverse(Ljava/util/List;)V

    .line 190
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$c;->b()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/material/card/MaterialCardView;->getStrokeWidth()I

    move-result v6

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    if-ne v6, v7, :cond_1

    return-void

    .line 192
    :cond_1
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object v2

    array-length v6, v2

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 193
    new-instance v6, Landroid/animation/ArgbEvaluator;

    invoke-direct {v6}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v2, v6}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 194
    new-instance v6, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda0;

    invoke-direct {v6, p1}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/yq$c;)V

    invoke-virtual {v2, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 199
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object v5

    array-length v6, v5

    invoke-static {v5, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v5

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 200
    new-instance v6, Landroid/animation/ArgbEvaluator;

    invoke-direct {v6}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 201
    new-instance v6, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda1;

    invoke-direct {v6, p1}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/yq$c;)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 206
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$c;->b()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v6

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object v1

    array-length v7, v1

    invoke-static {v1, v7}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    const-string v7, "strokeWidth"

    invoke-static {v6, v7, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 208
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v7, 0x1f4

    invoke-virtual {v6, v7, v8}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v6

    const/4 v7, 0x3

    new-array v7, v7, [Landroid/animation/Animator;

    aput-object v2, v7, v3

    aput-object v1, v7, v4

    aput-object v5, v7, v0

    .line 209
    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 210
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 211
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$c;->d()Landroid/widget/RadioButton;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/yq$e;)V
    .locals 4

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ui/dialog/utils/a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/ui/dialog/utils/b;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    .line 3
    sget p1, Lcom/pspdfkit/R$id;->pspdf__electronic_signatures_layout_title_view:I

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    const/4 v0, 0x0

    const-string v1, "titleView"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    sget v2, Lcom/pspdfkit/R$string;->pspdf__activity_menu_settings:I

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTitle(I)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_1
    new-instance v2, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setBackButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    move-object p1, v0

    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->b(ZZ)V

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 10
    :goto_0
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {p1, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 11
    invoke-virtual {p0, v0, v3, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    sget-object p1, Lcom/pspdfkit/internal/yq$b;->e:Lcom/pspdfkit/internal/yq$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 169
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->c()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    .line 170
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    .line 171
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->b()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    .line 173
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object p1, Lcom/pspdfkit/internal/yq$b;->e:Lcom/pspdfkit/internal/yq$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 175
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->c()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object p2

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    .line 176
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    .line 177
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->b()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    .line 179
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_0
    return-void
.end method

.method private final a(Ljava/util/EnumSet;Ljava/util/List;Ljava/util/HashMap;)V
    .locals 7

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 216
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 217
    move-object v5, v2

    check-cast v5, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    .line 218
    invoke-virtual {p1, v5}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 219
    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-nez v5, :cond_0

    goto :goto_2

    :cond_0
    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    if-eqz v6, :cond_2

    .line 431
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 433
    :cond_2
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 436
    :cond_3
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 437
    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 444
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 445
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getPageSettingsContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 449
    :cond_4
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    const/4 p2, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    goto :goto_3

    :cond_5
    move-object p1, p2

    :goto_3
    instance-of p3, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p3, :cond_6

    move-object p2, p1

    check-cast p2, Landroid/view/ViewGroup$MarginLayoutParams;

    :cond_6
    if-nez p2, :cond_7

    goto :goto_4

    :cond_7
    iput v3, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :goto_4
    return-void
.end method

.method private static a(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v2, p0, v0

    if-nez v2, :cond_1

    :goto_0
    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method private final b()V
    .locals 6

    .line 18
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionJumpButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda8;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda9;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda10;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda11;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda11;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda12;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda12;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollHorizontalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda13;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda13;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollVerticalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda14;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda14;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeDefaultButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda15;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda15;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeNightButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda16;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda16;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScreenAwakeSwitch()Landroid/widget/CompoundButton;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda17;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/yq$$ExternalSyntheticLambda17;-><init>(Lcom/pspdfkit/internal/yq;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    .line 40
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    .line 41
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->c()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object v0

    .line 43
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->e()Lcom/pspdfkit/configuration/theming/ThemeMode;

    move-result-object v0

    .line 45
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->b()J

    move-result-wide v2

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    .line 48
    invoke-virtual {v0, v2, v3}, Lcom/pspdfkit/internal/ar;->a(J)V

    .line 50
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScreenAwakeContainer()Landroid/view/ViewGroup;

    move-result-object v0

    .line 51
    invoke-static {v2, v3}, Lcom/pspdfkit/internal/yq;->a(J)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    .line 52
    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 62
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScreenAwakeSwitch()Landroid/widget/CompoundButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v4, v2, v0

    if-nez v4, :cond_2

    .line 66
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScreenAwakeSwitch()Landroid/widget/CompoundButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 67
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/yq;->setItemsVisibility(Lcom/pspdfkit/internal/ar;)V

    .line 68
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    return-void
.end method

.method private final b(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/yq$e;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/yq$e;-><init>(Landroid/content/Context;)V

    .line 9
    sget v1, Lcom/pspdfkit/R$color;->pspdf__color_white:I

    const v2, 0x1010031

    .line 10
    invoke-static {p1, v2, v1}, Lcom/pspdfkit/internal/cu;->a(Landroid/content/Context;II)I

    move-result v1

    .line 15
    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 16
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/internal/yq$e;)V

    .line 17
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/yq;->a(Landroid/content/Context;)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/yq$c;Landroid/animation/ValueAnimator;)V
    .locals 1

    const-string v0, "$ui"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 82
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yq$c;->c()Landroid/widget/TextView;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object p1, Lcom/pspdfkit/internal/yq$b;->d:Lcom/pspdfkit/internal/yq$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 70
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->c()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    .line 71
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    .line 72
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->b()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    .line 74
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    return-void
.end method

.method private static final b(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object p1, Lcom/pspdfkit/internal/yq$b;->d:Lcom/pspdfkit/internal/yq$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 76
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->c()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object p2

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    .line 77
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->a()Lcom/pspdfkit/configuration/page/PageLayoutMode;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    .line 78
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yq$b;->b()Lcom/pspdfkit/configuration/page/PageScrollDirection;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    .line 80
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_0
    return-void
.end method

.method private final c()V
    .locals 8

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/yq$b;->values()[Lcom/pspdfkit/internal/yq$b;

    move-result-object v0

    .line 109
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 110
    iget-object v5, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v4, v5}, Lcom/pspdfkit/internal/yq$b;->a(Lcom/pspdfkit/internal/ar;)Z

    move-result v5

    .line 111
    iget-object v6, p0, Lcom/pspdfkit/internal/yq;->d:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/pspdfkit/internal/yq$c;

    if-eqz v4, :cond_1

    .line 112
    invoke-direct {p0, v4, v5}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/internal/yq$c;Z)V

    .line 114
    invoke-virtual {v4}, Lcom/pspdfkit/internal/yq$c;->a()Landroid/webkit/WebView;

    move-result-object v4

    if-nez v5, :cond_0

    const/4 v5, 0x4

    .line 116
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 117
    :cond_0
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    .line 118
    invoke-virtual {v6, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 122
    invoke-virtual {v6, v2}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 124
    invoke-virtual {v6, v2}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 127
    invoke-virtual {v6, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 128
    invoke-virtual {v6, v2}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 130
    new-instance v6, Lcom/pspdfkit/internal/yq$a;

    invoke-direct {v6, p0, v4, v5}, Lcom/pspdfkit/internal/yq$a;-><init>(Lcom/pspdfkit/internal/yq;Landroid/webkit/WebView;Z)V

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 131
    invoke-virtual {v4}, Landroid/webkit/WebView;->reload()V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 132
    :cond_2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getOptionItems()Ljava/util/List;

    move-result-object v0

    .line 257
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 258
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    instance-of v5, v4, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    if-eqz v5, :cond_4

    check-cast v4, Landroid/view/ViewGroup;

    goto :goto_3

    :cond_4
    move-object v4, v6

    :goto_3
    if-nez v4, :cond_5

    goto :goto_2

    .line 259
    :cond_5
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v5, v4, Landroid/widget/ImageView;

    if-eqz v5, :cond_6

    move-object v6, v4

    check-cast v6, Landroid/widget/ImageView;

    :cond_6
    if-nez v6, :cond_7

    goto :goto_2

    .line 262
    :cond_7
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->isActivated()Z

    move-result v4

    const/4 v5, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    if-eqz v4, :cond_9

    invoke-virtual {v6}, Landroid/widget/ImageView;->getAlpha()F

    move-result v4

    cmpg-float v4, v4, v7

    if-nez v4, :cond_8

    const/4 v4, 0x1

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_4
    if-nez v4, :cond_9

    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_6

    .line 263
    :cond_9
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->isActivated()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v6}, Landroid/widget/ImageView;->getAlpha()F

    move-result v1

    cmpg-float v1, v1, v5

    if-nez v1, :cond_a

    goto :goto_5

    :cond_a
    const/4 v3, 0x0

    :goto_5
    if-nez v3, :cond_3

    .line 267
    :goto_6
    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    .line 268
    :cond_b
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getSaveButton()Landroid/widget/Button;

    move-result-object v0

    if-nez v0, :cond_c

    goto :goto_7

    :cond_c
    iget-object v1, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    iget-object v2, p0, Lcom/pspdfkit/internal/yq;->b:Lcom/pspdfkit/internal/ar;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_7
    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->c:Lcom/pspdfkit/internal/zq;

    invoke-interface {p1}, Lcom/pspdfkit/internal/zq;->onSettingsClose()V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->c:Lcom/pspdfkit/internal/zq;

    iget-object p0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/zq;->onSettingsSave(Lcom/pspdfkit/internal/ar;)V

    return-void
.end method

.method private static final c(Lcom/pspdfkit/internal/yq;Landroid/widget/CompoundButton;Z)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    if-eqz p2, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/ar;->a(J)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->c()V

    :cond_1
    return-void
.end method

.method private static final d(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageScrollMode;->PER_PAGE:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    return-void
.end method

.method private final d()Z
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ar;->d()Lcom/pspdfkit/configuration/page/PageScrollMode;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 4
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 5
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 6
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setActivated(Z)V

    .line 7
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 8
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    .line 9
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private static final e(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageScrollMode;->CONTINUOUS:Lcom/pspdfkit/configuration/page/PageScrollMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollMode;Z)V

    return-void
.end method

.method private static final f(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->SINGLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    return-void
.end method

.method private static final g(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->DOUBLE:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    return-void
.end method

.method private final getHorizontalPreset()Landroid/view/View;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_horizontal:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_horizontal)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getHorizontalPresetAnimationView()Landroid/webkit/WebView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__animation_view_horizontal:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026nimation_view_horizontal)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/webkit/WebView;

    return-object v0
.end method

.method private final getHorizontalPresetCard()Lcom/google/android/material/card/MaterialCardView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_horizontal_card:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_horizontal_card)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/card/MaterialCardView;

    return-object v0
.end method

.method private final getHorizontalPresetLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_horizontal_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_horizontal_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHorizontalPresetRadio()Landroid/widget/RadioButton;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_horizontal_radio:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_horizontal_radio)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method private final getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_auto_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__layout_auto_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_double_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__layout_double_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getLayoutLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__layout_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLayoutSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_settings:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__layout_settings)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__layout_single_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__layout_single_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getMiscSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__misc_settings_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__misc_settings_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getOptionItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/settings/SettingsModePickerItem;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionJumpButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSingleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutDoubleButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutAutoButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollHorizontalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollVerticalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeDefaultButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeNightButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 3
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final getPageSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__page_settings_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__page_settings_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getSaveButton()Landroid/widget/Button;
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__save_settings:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getScreenAwakeContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__screen_awake_container:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__screen_awake_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getScreenAwakeSwitch()Landroid/widget/CompoundButton;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__screen_awake_switch:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__screen_awake_switch)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/CompoundButton;

    return-object v0
.end method

.method private final getScrollHorizontalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_horizontal_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__scroll_horizontal_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getScrollLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__scroll_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getScrollSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_settings:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__scroll_settings)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getScrollVerticalButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__scroll_vertical_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__scroll_vertical_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getSelectedStrokeWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->h:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private final getThemeDefaultButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_default_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__theme_default_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getThemeLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__theme_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getThemeNightButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_night_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__theme_night_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getThemeSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__theme_settings:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__theme_settings)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getTransitionContinuousButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_continuous_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf_\u2026sition_continuous_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getTransitionJumpButton()Lcom/pspdfkit/ui/settings/SettingsModePickerItem;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_jump_button:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__transition_jump_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/pspdfkit/ui/settings/SettingsModePickerItem;

    return-object v0
.end method

.method private final getTransitionLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__transition_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTransitionSettingsContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__transition_settings:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__transition_settings)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getUnselectedStrokeWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->i:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private final getVerticalPreset()Landroid/view/View;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_vertical:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_vertical)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getVerticalPresetAnimationView()Landroid/webkit/WebView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__animation_view_vertical:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__animation_view_vertical)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/webkit/WebView;

    return-object v0
.end method

.method private final getVerticalPresetCard()Lcom/google/android/material/card/MaterialCardView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_vertical_card:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_vertical_card)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/card/MaterialCardView;

    return-object v0
.end method

.method private final getVerticalPresetLabel()Landroid/widget/TextView;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_vertical_label:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_vertical_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getVerticalPresetRadio()Landroid/widget/RadioButton;
    .locals 2

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__preset_vertical_radio:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pspdf__preset_vertical_radio)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method private static final h(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageLayoutMode;->AUTO:Lcom/pspdfkit/configuration/page/PageLayoutMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageLayoutMode;Z)V

    return-void
.end method

.method private static final i(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->HORIZONTAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    return-void
.end method

.method private static final j(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/page/PageScrollDirection;->VERTICAL:Lcom/pspdfkit/configuration/page/PageScrollDirection;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/page/PageScrollDirection;Z)V

    return-void
.end method

.method private static final k(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/theming/ThemeMode;->DEFAULT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V

    return-void
.end method

.method private static final l(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object p1, Lcom/pspdfkit/configuration/theming/ThemeMode;->NIGHT:Lcom/pspdfkit/configuration/theming/ThemeMode;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/yq;->a(Lcom/pspdfkit/configuration/theming/ThemeMode;Z)V

    return-void
.end method

.method private static final m(Lcom/pspdfkit/internal/yq;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object p0, p0, Lcom/pspdfkit/internal/yq;->c:Lcom/pspdfkit/internal/zq;

    invoke-interface {p0}, Lcom/pspdfkit/internal/zq;->onSettingsClose()V

    return-void
.end method

.method private final setItemsVisibility(Lcom/pspdfkit/internal/ar;)V
    .locals 14

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->f()Ljava/util/EnumSet;

    move-result-object v0

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ar;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/pspdfkit/internal/yq;->a(J)Z

    move-result p1

    if-nez p1, :cond_0

    .line 3
    sget-object p1, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->SCREEN_AWAKE:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    :cond_0
    const/4 p1, 0x3

    new-array v1, p1, [Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    .line 6
    sget-object v2, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->PAGE_TRANSITION:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v4, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->PAGE_LAYOUT:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    const/4 v5, 0x1

    aput-object v4, v1, v5

    sget-object v6, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->SCROLL_DIRECTION:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    const/4 v7, 0x2

    aput-object v6, v1, v7

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-array v8, v7, [Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    .line 7
    sget-object v9, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->THEME:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    aput-object v9, v8, v3

    sget-object v10, Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;->SCREEN_AWAKE:Lcom/pspdfkit/configuration/settings/SettingsMenuItemType;

    aput-object v10, v8, v5

    invoke-static {v8}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v11, 0x5

    new-array v11, v11, [Lkotlin/Pair;

    .line 10
    new-instance v12, Lkotlin/Pair;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getTransitionSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v13

    invoke-direct {v12, v2, v13}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v12, v11, v3

    .line 11
    new-instance v2, Lkotlin/Pair;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getLayoutSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-direct {v2, v4, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v11, v5

    .line 12
    new-instance v2, Lkotlin/Pair;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScrollSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-direct {v2, v6, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v11, v7

    .line 13
    new-instance v2, Lkotlin/Pair;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getThemeSettingsContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-direct {v2, v9, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v11, p1

    .line 14
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getScreenAwakeContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-direct {p1, v10, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x4

    aput-object p1, v11, v2

    .line 15
    invoke-static {v11}, Lkotlin/collections/MapsKt;->hashMapOf([Lkotlin/Pair;)Ljava/util/HashMap;

    move-result-object p1

    .line 23
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getPageSettingsContainer()Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1, p1}, Lcom/pspdfkit/internal/yq;->a(Ljava/util/EnumSet;Ljava/util/List;Ljava/util/HashMap;)V

    .line 24
    invoke-direct {p0}, Lcom/pspdfkit/internal/yq;->getMiscSettingsContainer()Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v8, p1}, Lcom/pspdfkit/internal/yq;->a(Ljava/util/EnumSet;Ljava/util/List;Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method protected final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3
    .annotation runtime Lkotlin/Deprecated;
        message = "Deprecated in Java"
    .end annotation

    const-string v0, "insets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget v0, Lcom/pspdfkit/internal/xq;->f:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    sget v1, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_width:I

    .line 192
    sget v2, Lcom/pspdfkit/R$dimen;->pspdf__electronic_signature_dialog_height:I

    .line 193
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/e8;->a(Landroid/content/res/Resources;II)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->f:Lcom/pspdfkit/internal/ui/dialog/utils/a;

    if-nez v0, :cond_0

    const-string v0, "titleView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ui/dialog/utils/a;->setTopInset(I)V

    .line 196
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public final getOptions()Lcom/pspdfkit/internal/ar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/yq;->e:Lcom/pspdfkit/internal/ar;

    return-object v0
.end method

.method public final getPicker()Lcom/pspdfkit/ui/settings/SettingsModePicker;
    .locals 1

    const-string v0, "picker"

    .line 1
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

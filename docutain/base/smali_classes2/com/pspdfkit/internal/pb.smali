.class public Lcom/pspdfkit/internal/pb;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pb$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/pb$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroidx/recyclerview/widget/RecyclerView;

.field private final c:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/ui/fonts/Font;

.field private final f:Landroid/view/LayoutInflater;


# direct methods
.method public static synthetic $r8$lambda$TTh_KzQI7JQDZx8qakeGQeJvoyk(Lcom/pspdfkit/internal/pb;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/pb$a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/pspdfkit/internal/pb;->a(Lcom/pspdfkit/internal/pb;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/pb$a;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Lcom/pspdfkit/ui/fonts/Font;Ljava/util/List;Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialFonts"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultFont"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/pb;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/pb;->b:Landroidx/recyclerview/widget/RecyclerView;

    .line 6
    iput-object p5, p0, Lcom/pspdfkit/internal/pb;->c:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;

    .line 9
    invoke-static {p4}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    .line 12
    iput-object p3, p0, Lcom/pspdfkit/internal/pb;->e:Lcom/pspdfkit/ui/fonts/Font;

    .line 14
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/pb;->f:Landroid/view/LayoutInflater;

    return-void
.end method

.method private static final a(Lcom/pspdfkit/internal/pb;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/pb$a;Landroid/view/View;)V
    .locals 2

    const-string p3, "this$0"

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$font"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$viewHolder"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object p3, p0, Lcom/pspdfkit/internal/pb;->c:Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;

    invoke-interface {p3, p1}, Lcom/pspdfkit/ui/inspector/views/FontPickerInspectorView$FontPickerListener;->onFontSelected(Lcom/pspdfkit/ui/fonts/Font;)V

    .line 41
    iget-object p3, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->e:Lcom/pspdfkit/ui/fonts/Font;

    invoke-interface {p3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p3

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p3}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    instance-of v1, v0, Lcom/pspdfkit/internal/pb$a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/pspdfkit/internal/pb$a;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v0}, Lcom/pspdfkit/internal/pb$a;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    invoke-virtual {p0, p3}, Lcom/pspdfkit/internal/pb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 51
    invoke-virtual {p0, p3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 52
    :cond_1
    invoke-virtual {p0, p3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 54
    :catch_0
    :cond_2
    :goto_1
    iput-object p1, p0, Lcom/pspdfkit/internal/pb;->e:Lcom/pspdfkit/ui/fonts/Font;

    .line 56
    invoke-virtual {p2}, Lcom/pspdfkit/internal/pb$a;->a()Landroid/view/View;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->a:Landroid/content/Context;

    return-object v0
.end method

.method protected final a(I)Lcom/pspdfkit/ui/fonts/Font;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/ui/fonts/Font;

    return-object p1
.end method

.method protected a(Landroid/widget/TextView;Lcom/pspdfkit/ui/fonts/Font;)Ljava/lang/String;
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "font"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->a:Landroid/content/Context;

    sget v1, Lcom/pspdfkit/R$string;->pspdf__font_missing:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p2

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, p1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "getString(context, R.str\u2026missing, view, font.name)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/pb$a;I)V
    .locals 3

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/fonts/Font;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p2}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 7
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/pb;->a(Lcom/pspdfkit/ui/fonts/Font;)Z

    move-result v0

    .line 9
    invoke-virtual {p0, p1, v0, p2}, Lcom/pspdfkit/internal/pb;->a(Lcom/pspdfkit/internal/pb$a;ZLcom/pspdfkit/ui/fonts/Font;)V

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/pb;->e:Lcom/pspdfkit/ui/fonts/Font;

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_1

    .line 14
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/pspdfkit/internal/pb$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, p1}, Lcom/pspdfkit/internal/pb$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/pb;Lcom/pspdfkit/ui/fonts/Font;Lcom/pspdfkit/internal/pb$a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    :cond_1
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Lcom/pspdfkit/internal/pb$a;ZLcom/pspdfkit/ui/fonts/Font;)V
    .locals 1

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "font"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p3}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object p2

    const/high16 p3, 0x3f800000    # 1.0f

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 59
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/pspdfkit/internal/pb;->a(Landroid/widget/TextView;Lcom/pspdfkit/ui/fonts/Font;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p1}, Lcom/pspdfkit/internal/pb$a;->c()Landroid/widget/TextView;

    move-result-object p2

    const/high16 p3, 0x3f000000    # 0.5f

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 64
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method protected a(Lcom/pspdfkit/ui/fonts/Font;)Z
    .locals 1

    const-string v0, "font"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/ui/fonts/Font;->getDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected b(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/pb$a;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/pb;->a(Lcom/pspdfkit/internal/pb$a;I)V

    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const-string p2, "parent"

    .line 1
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p2, p0, Lcom/pspdfkit/internal/pb;->f:Landroid/view/LayoutInflater;

    sget v0, Lcom/pspdfkit/R$layout;->pspdf__view_inspector_font_list_item:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 50
    new-instance p2, Lcom/pspdfkit/internal/pb$a;

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/pspdfkit/internal/pb$a;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.class public final Lcom/pspdfkit/internal/js;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;


# instance fields
.field private final b:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private e:Lcom/pspdfkit/internal/dm;

.field private f:I

.field private g:Landroid/graphics/Point;


# direct methods
.method public static synthetic $r8$lambda$Tmfh6BO1mh6rR3UI7dOYbAvyJ40(Lcom/pspdfkit/internal/js;Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/js;->a(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->e()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/js;->c:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lcom/pspdfkit/internal/js;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-void
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 3

    .line 20
    new-instance v0, Lcom/pspdfkit/annotations/SoundAnnotation;

    iget v1, p0, Lcom/pspdfkit/internal/js;->f:I

    invoke-direct {v0, v1, p1}, Lcom/pspdfkit/annotations/SoundAnnotation;-><init>(ILandroid/graphics/RectF;)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    new-instance v1, Lcom/pspdfkit/internal/js$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/js$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/js;Lcom/pspdfkit/annotations/SoundAnnotation;)V

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2, v1}, Lcom/pspdfkit/ui/PdfFragment;->addAnnotationToPage(Lcom/pspdfkit/annotations/Annotation;ZLjava/lang/Runnable;)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/SoundAnnotation;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->d()Lcom/pspdfkit/ui/audio/AudioModeManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/pspdfkit/ui/audio/AudioModeManager;->enterAudioRecordingMode(Lcom/pspdfkit/annotations/SoundAnnotation;)V

    .line 26
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "create_annotation"

    .line 27
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/js;->e:Lcom/pspdfkit/internal/dm;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/js;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/js;->f:I

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/js;->g:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/internal/js;->c:Landroid/content/Context;

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v5, v5

    .line 9
    invoke-static {v2, v3, v0, v4, v5}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-direct {v0, v2, v3, v4, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 13
    iget-object p1, p0, Lcom/pspdfkit/internal/js;->e:Lcom/pspdfkit/internal/dm;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    const/high16 p1, -0x3ee00000    # -10.0f

    const/high16 v3, -0x3f100000    # -7.5f

    .line 14
    invoke-virtual {v0, p1, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 15
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/js;->a(Landroid/graphics/RectF;)V

    .line 16
    iput-object v2, p0, Lcom/pspdfkit/internal/js;->g:Landroid/graphics/Point;

    return v1

    .line 18
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 19
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-int p1, p1

    invoke-direct {v0, v2, p1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/pspdfkit/internal/js;->g:Landroid/graphics/Point;

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/js;->d:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->SOUND:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/js;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

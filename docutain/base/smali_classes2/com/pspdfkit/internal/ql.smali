.class public abstract Lcom/pspdfkit/internal/ql;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/ql$a;,
        Lcom/pspdfkit/internal/ql$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/LinearLayout;"
    }
.end annotation


# instance fields
.field protected b:Lcom/pspdfkit/internal/ql$a;

.field private c:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 2
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public abstract a(Lcom/pspdfkit/internal/rl;)V
.end method

.method public abstract a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
.end method

.method public b()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ql;->c:Z

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->c()V

    :cond_0
    return-void
.end method

.method public abstract getTabButtonId()I
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public setOnHideListener(Lcom/pspdfkit/internal/ql$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ql;->b:Lcom/pspdfkit/internal/ql$a;

    return-void
.end method

.method public setPageSelected(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ql;->c:Z

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

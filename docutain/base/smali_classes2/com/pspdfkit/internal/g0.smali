.class public final Lcom/pspdfkit/internal/g0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final A:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final B:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/stamps/StampPickerItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final C:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final E:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final G:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final I:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final J:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final K:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Lcom/pspdfkit/annotations/measurements/Scale;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Lcom/pspdfkit/annotations/measurements/FloatPrecision;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/configuration/AnnotationProperty;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final f:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final h:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final j:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Lcom/pspdfkit/configuration/annotations/AnnotationAggregationStrategy;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/inspector/views/BorderStylePreset;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final x:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Landroidx/core/util/Pair<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final y:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/LineEndType;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final z:Lcom/pspdfkit/internal/g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/g0<",
            "Lcom/pspdfkit/ui/fonts/Font;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->a:Lcom/pspdfkit/internal/g0;

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->b:Lcom/pspdfkit/internal/g0;

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->c:Lcom/pspdfkit/internal/g0;

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->d:Lcom/pspdfkit/internal/g0;

    .line 9
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->e:Lcom/pspdfkit/internal/g0;

    .line 11
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->f:Lcom/pspdfkit/internal/g0;

    .line 13
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->g:Lcom/pspdfkit/internal/g0;

    .line 15
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->h:Lcom/pspdfkit/internal/g0;

    .line 17
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->i:Lcom/pspdfkit/internal/g0;

    .line 19
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->j:Lcom/pspdfkit/internal/g0;

    .line 21
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->k:Lcom/pspdfkit/internal/g0;

    .line 23
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->l:Lcom/pspdfkit/internal/g0;

    .line 25
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->m:Lcom/pspdfkit/internal/g0;

    .line 27
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->n:Lcom/pspdfkit/internal/g0;

    .line 29
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->o:Lcom/pspdfkit/internal/g0;

    .line 31
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->p:Lcom/pspdfkit/internal/g0;

    .line 33
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->q:Lcom/pspdfkit/internal/g0;

    .line 35
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->r:Lcom/pspdfkit/internal/g0;

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->s:Lcom/pspdfkit/internal/g0;

    .line 39
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->t:Lcom/pspdfkit/internal/g0;

    .line 41
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->u:Lcom/pspdfkit/internal/g0;

    .line 43
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->v:Lcom/pspdfkit/internal/g0;

    .line 45
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->w:Lcom/pspdfkit/internal/g0;

    .line 47
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->x:Lcom/pspdfkit/internal/g0;

    .line 49
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->y:Lcom/pspdfkit/internal/g0;

    .line 51
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->z:Lcom/pspdfkit/internal/g0;

    .line 53
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->A:Lcom/pspdfkit/internal/g0;

    .line 55
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->B:Lcom/pspdfkit/internal/g0;

    .line 57
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->C:Lcom/pspdfkit/internal/g0;

    .line 59
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->D:Lcom/pspdfkit/internal/g0;

    .line 61
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->E:Lcom/pspdfkit/internal/g0;

    .line 63
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->F:Lcom/pspdfkit/internal/g0;

    .line 65
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->G:Lcom/pspdfkit/internal/g0;

    .line 67
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->H:Lcom/pspdfkit/internal/g0;

    .line 69
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->I:Lcom/pspdfkit/internal/g0;

    .line 71
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->J:Lcom/pspdfkit/internal/g0;

    .line 73
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->K:Lcom/pspdfkit/internal/g0;

    .line 75
    new-instance v0, Lcom/pspdfkit/internal/g0;

    invoke-direct {v0}, Lcom/pspdfkit/internal/g0;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/g0;->L:Lcom/pspdfkit/internal/g0;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

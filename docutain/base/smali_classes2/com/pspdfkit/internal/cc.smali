.class final Lcom/pspdfkit/internal/cc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/bc;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/bc;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/cc;->a:Lcom/pspdfkit/internal/bc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    .line 1
    check-cast p1, Lcom/pspdfkit/forms/FormField;

    const-string v0, "formField"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/pspdfkit/internal/cc;->a:Lcom/pspdfkit/internal/bc;

    invoke-static {v0}, Lcom/pspdfkit/internal/bc;->c(Lcom/pspdfkit/internal/bc;)Lcom/pspdfkit/internal/nh;

    move-result-object v0

    .line 420
    invoke-virtual {v0}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;

    .line 421
    invoke-interface {v1, p1}, Lcom/pspdfkit/forms/FormListeners$OnFormFieldUpdatedListener;->onFormFieldUpdated(Lcom/pspdfkit/forms/FormField;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.class public interface abstract Lcom/pspdfkit/internal/p9$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/p9;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract onDocumentSave(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/DocumentSaveOptions;)Z
.end method

.method public abstract onDocumentSaveCancelled(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.method public abstract onDocumentSaveFailed(Lcom/pspdfkit/document/PdfDocument;Ljava/lang/Throwable;)V
.end method

.method public abstract onDocumentSaved(Lcom/pspdfkit/document/PdfDocument;)V
.end method

.class public final Lcom/pspdfkit/internal/ka;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/la;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private f:Landroid/graphics/RectF;

.field private g:F

.field private h:F


# direct methods
.method private constructor <init>()V
    .locals 6

    .line 10
    new-instance v1, Lcom/pspdfkit/internal/la;

    const/4 v0, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;II)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/la;ZZZZ)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 6

    .line 12
    new-instance v1, Lcom/pspdfkit/internal/la;

    const/4 v0, 0x0

    invoke-direct {v1, v0, p1}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/la;ZZZZ)V

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/la;ZZZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    .line 6
    iput-boolean p2, p0, Lcom/pspdfkit/internal/ka;->b:Z

    .line 7
    iput-boolean p3, p0, Lcom/pspdfkit/internal/ka;->c:Z

    .line 8
    iput-boolean p4, p0, Lcom/pspdfkit/internal/ka;->d:Z

    .line 9
    iput-boolean p5, p0, Lcom/pspdfkit/internal/ka;->e:Z

    return-void
.end method

.method private constructor <init>(Lcom/pspdfkit/internal/z1$b;ZZZZ)V
    .locals 6

    .line 11
    new-instance v1, Lcom/pspdfkit/internal/la;

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lcom/pspdfkit/internal/la;-><init>(Lcom/pspdfkit/internal/z1$b;II)V

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/la;ZZZZ)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/pspdfkit/internal/z1$b;ZZZZI)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/ka;-><init>(Lcom/pspdfkit/internal/z1$b;ZZZZ)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/ka;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/la;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    return-object v0
.end method

.method public final a(F)V
    .locals 0

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/ka;->g:F

    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/ka;->f:Landroid/graphics/RectF;

    return-void
.end method

.method public final b(F)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/pspdfkit/internal/ka;->h:F

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ka;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ka;->d:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ka;->c:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ka;->b:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    const-string v0, "null cannot be cast to non-null type com.pspdfkit.internal.views.annotations.EditMode"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/pspdfkit/internal/ka;

    .line 5
    iget-boolean v0, p1, Lcom/pspdfkit/internal/ka;->e:Z

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ka;->e:Z

    if-ne v0, v1, :cond_0

    .line 6
    iget-boolean v0, p1, Lcom/pspdfkit/internal/ka;->d:Z

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ka;->d:Z

    if-ne v0, v1, :cond_0

    .line 7
    iget-boolean v0, p1, Lcom/pspdfkit/internal/ka;->c:Z

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ka;->c:Z

    if-ne v0, v1, :cond_0

    .line 8
    iget-boolean v0, p1, Lcom/pspdfkit/internal/ka;->b:Z

    iget-boolean v1, p0, Lcom/pspdfkit/internal/ka;->b:Z

    if-ne v0, v1, :cond_0

    .line 9
    iget-object p1, p1, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    iget-object v0, p0, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final f()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ka;->f:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final g()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ka;->g:F

    return v0
.end method

.method public final h()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ka;->h:F

    return v0
.end method

.method public final i()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->b()Lcom/pspdfkit/internal/z1$b;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/ka;->a:Lcom/pspdfkit/internal/la;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/la;->a()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

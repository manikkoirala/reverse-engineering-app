.class public final Lcom/pspdfkit/internal/xh;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Landroid/content/Context;)Landroid/content/res/TypedArray;
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    .line 3
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar:[I

    .line 4
    sget v1, Lcom/pspdfkit/R$attr;->pspdf__mainToolbarStyle:I

    .line 5
    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_MainToolbar:I

    const/4 v3, 0x0

    .line 6
    invoke-virtual {p0, v3, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    const-string v0, "context.theme.obtainStyl\u2026SPDFKit_MainToolbar\n    )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final b(Landroid/content/Context;)I
    .locals 4

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    .line 3
    sget-object v0, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar:[I

    .line 4
    sget v1, Lcom/pspdfkit/R$attr;->pspdf__mainToolbarStyle:I

    .line 5
    sget v2, Lcom/pspdfkit/R$style;->PSPDFKit_MainToolbar:I

    const/4 v3, 0x0

    .line 6
    invoke-virtual {p0, v3, v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    const-string v0, "context.theme.obtainStyl\u2026SPDFKit_MainToolbar\n    )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    sget v0, Lcom/pspdfkit/R$styleable;->pspdf__MainToolbar_pspdf__toolbarTheme:I

    sget v1, Landroidx/appcompat/R$style;->ThemeOverlay_AppCompat_Dark_ActionBar:I

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 8
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    return v0
.end method

.class public final Lcom/pspdfkit/internal/r9;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/document/PdfDocument;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/pspdfkit/document/sharing/ShareAction;

.field private e:Landroidx/fragment/app/FragmentActivity;

.field private f:Lcom/pspdfkit/document/sharing/DocumentSharingController;

.field private final g:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

.field private h:Z


# direct methods
.method static bridge synthetic -$$Nest$fgeta(Lcom/pspdfkit/internal/r9;)Lcom/pspdfkit/document/PdfDocument;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/r9;->a:Lcom/pspdfkit/document/PdfDocument;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetd(Lcom/pspdfkit/internal/r9;)Lcom/pspdfkit/document/sharing/ShareAction;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/r9;->d:Lcom/pspdfkit/document/sharing/ShareAction;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgete(Lcom/pspdfkit/internal/r9;)Landroidx/fragment/app/FragmentActivity;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputf(Lcom/pspdfkit/internal/r9;Lcom/pspdfkit/document/sharing/DocumentSharingController;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/r9;->f:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputh(Lcom/pspdfkit/internal/r9;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/pspdfkit/internal/r9;->h:Z

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;Lcom/pspdfkit/document/sharing/ShareAction;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    .line 3
    iput-object p2, p0, Lcom/pspdfkit/internal/r9;->a:Lcom/pspdfkit/document/PdfDocument;

    .line 4
    iput-object p3, p0, Lcom/pspdfkit/internal/r9;->g:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    .line 5
    iput-object p4, p0, Lcom/pspdfkit/internal/r9;->d:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 6
    iput p5, p0, Lcom/pspdfkit/internal/r9;->b:I

    .line 7
    iput-object p6, p0, Lcom/pspdfkit/internal/r9;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Landroidx/fragment/app/FragmentActivity;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r9;->f:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onAttach(Landroid/content/Context;)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->isVisible(Landroidx/fragment/app/FragmentManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 6
    new-instance v0, Lcom/pspdfkit/internal/q9;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/q9;-><init>(Lcom/pspdfkit/internal/r9;)V

    .line 7
    invoke-static {p1, v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->restore(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/r9;->h:Z

    :cond_1
    :goto_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/pspdfkit/internal/r9;->h:Z

    return v0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/r9;->f:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/document/sharing/DocumentSharingController;->onDetach()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    if-eqz v0, :cond_5

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->PDF_CREATION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3
    new-instance v0, Lcom/pspdfkit/document/sharing/SharingOptions;

    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-direct {v0, v1}, Lcom/pspdfkit/document/sharing/SharingOptions;-><init>(Ljava/lang/String;)V

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    if-nez v1, :cond_1

    goto :goto_0

    .line 5
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/r9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget-object v3, p0, Lcom/pspdfkit/internal/r9;->d:Lcom/pspdfkit/document/sharing/ShareAction;

    invoke-static {v1, v2, v3, v0}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareDocument(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/sharing/SharingOptions;)Lcom/pspdfkit/document/sharing/DocumentSharingController;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/r9;->f:Lcom/pspdfkit/document/sharing/DocumentSharingController;

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "share"

    .line 7
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->d:Lcom/pspdfkit/document/sharing/ShareAction;

    .line 8
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    goto :goto_0

    .line 10
    :cond_2
    new-instance v0, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    iget-object v2, p0, Lcom/pspdfkit/internal/r9;->d:Lcom/pspdfkit/document/sharing/ShareAction;

    iget-object v3, p0, Lcom/pspdfkit/internal/r9;->a:Lcom/pspdfkit/document/PdfDocument;

    iget v4, p0, Lcom/pspdfkit/internal/r9;->b:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;-><init>(Landroid/content/Context;Lcom/pspdfkit/document/sharing/ShareAction;Lcom/pspdfkit/document/PdfDocument;I)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->initialDocumentName(Ljava/lang/String;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    .line 16
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->setSavingFlow(ZLandroid/content/Context;)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    .line 20
    invoke-virtual {v0, v2}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->setInitialPagesSpinnerAllPages(Z)Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;

    const/4 v1, 0x0

    .line 24
    iget-object v3, p0, Lcom/pspdfkit/internal/r9;->g:Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;

    if-eqz v3, :cond_4

    .line 25
    invoke-interface {v3}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogFactory;->createDocumentSharingDialog()Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;

    move-result-object v1

    .line 29
    :cond_4
    iput-boolean v2, p0, Lcom/pspdfkit/internal/r9;->h:Z

    .line 30
    iget-object v2, p0, Lcom/pspdfkit/internal/r9;->e:Landroidx/fragment/app/FragmentActivity;

    .line 32
    invoke-virtual {v2}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    .line 33
    invoke-virtual {v0}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration$Builder;->build()Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;

    move-result-object v0

    .line 34
    new-instance v3, Lcom/pspdfkit/internal/q9;

    invoke-direct {v3, p0}, Lcom/pspdfkit/internal/q9;-><init>(Lcom/pspdfkit/internal/r9;)V

    .line 35
    invoke-static {v1, v2, v0, v3}, Lcom/pspdfkit/ui/dialog/DocumentSharingDialog;->show(Lcom/pspdfkit/ui/dialog/BaseDocumentSharingDialog;Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/ui/dialog/DocumentSharingDialogConfiguration;Lcom/pspdfkit/ui/dialog/DocumentSharingDialog$SharingDialogListener;)V

    :cond_5
    :goto_0
    return-void
.end method

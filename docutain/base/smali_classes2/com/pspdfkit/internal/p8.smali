.class public final Lcom/pspdfkit/internal/p8;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/q8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/pspdfkit/internal/u8;",
        ">;",
        "Lcom/pspdfkit/internal/q8;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private d:Z

.field private e:Lcom/pspdfkit/internal/rl;


# direct methods
.method public static synthetic $r8$lambda$QuKdglOUUBqJe_e7YGfjto6ZJ1o(Lcom/pspdfkit/internal/p8;Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/p8;->a(Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ZrcRgjPmC6SEtjRHkDlZEJdTpX0(Lcom/pspdfkit/internal/p8;Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/pspdfkit/internal/p8;->b(Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    .line 6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    .line 9
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p8;->d:Z

    .line 16
    new-instance v0, Lcom/pspdfkit/internal/rl;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/rl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    return-void
.end method

.method private a(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/xl;)Landroid/view/View;
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__document_info_page_binding_item:I

    const/4 v2, 0x0

    .line 2
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 4
    sget v0, Lcom/pspdfkit/R$id;->pspdf__document_info_item_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 5
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/pspdfkit/internal/xl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v1, v1, Lcom/pspdfkit/internal/rl;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 8
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_info_left_binding:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;

    .line 9
    sget-object v3, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setPageBinding(Lcom/pspdfkit/document/PageBinding;)V

    .line 10
    iget-object v4, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v5, v4, Lcom/pspdfkit/internal/rl;->H:I

    iget v6, v4, Lcom/pspdfkit/internal/rl;->G:I

    iget v4, v4, Lcom/pspdfkit/internal/rl;->E:I

    invoke-virtual {v1, v5, v6, v4}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->a(III)V

    .line 14
    invoke-virtual {p2}, Lcom/pspdfkit/internal/xl;->e()Lcom/pspdfkit/document/PageBinding;

    move-result-object v4

    const/4 v5, 0x1

    if-ne v4, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v3}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    .line 15
    iget-boolean v3, p0, Lcom/pspdfkit/internal/p8;->d:Z

    const/16 v4, 0x8

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    .line 16
    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 18
    sget v3, Lcom/pspdfkit/R$id;->pspdf__document_info_right_binding:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;

    .line 19
    sget-object v3, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    invoke-virtual {v9, v3}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setPageBinding(Lcom/pspdfkit/document/PageBinding;)V

    .line 20
    iget-object v6, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v7, v6, Lcom/pspdfkit/internal/rl;->H:I

    iget v8, v6, Lcom/pspdfkit/internal/rl;->G:I

    iget v6, v6, Lcom/pspdfkit/internal/rl;->E:I

    invoke-virtual {v9, v7, v8, v6}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->a(III)V

    .line 24
    invoke-virtual {p2}, Lcom/pspdfkit/internal/xl;->e()Lcom/pspdfkit/document/PageBinding;

    move-result-object v6

    if-ne v6, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v9, v5}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    .line 25
    iget-boolean v3, p0, Lcom/pspdfkit/internal/p8;->d:Z

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_3
    const/16 v2, 0x8

    .line 26
    :goto_3
    invoke-virtual {v9, v2}, Landroid/view/View;->setVisibility(I)V

    .line 27
    iget-boolean v2, p0, Lcom/pspdfkit/internal/p8;->d:Z

    if-eqz v2, :cond_4

    .line 28
    new-instance v2, Lcom/pspdfkit/internal/p8$$ExternalSyntheticLambda0;

    move-object v3, v2

    move-object v4, p0

    move-object v5, p2

    move-object v6, v0

    move-object v7, v1

    move-object v8, v9

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/p8$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/p8;Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    new-instance v2, Lcom/pspdfkit/internal/p8$$ExternalSyntheticLambda1;

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/pspdfkit/internal/p8$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/p8;Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;)V

    invoke-virtual {v9, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V
    .locals 0

    .line 35
    sget-object p5, Lcom/pspdfkit/document/PageBinding;->LEFT_EDGE:Lcom/pspdfkit/document/PageBinding;

    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/xl;->a(Lcom/pspdfkit/document/PageBinding;)V

    .line 36
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object p5

    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/xl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 37
    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    const/4 p1, 0x0

    .line 38
    invoke-virtual {p4, p1}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/u8;)Z
    .locals 3

    .line 41
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p8;->d:Z

    xor-int/lit8 v0, v0, 0x1

    .line 42
    invoke-virtual {p1}, Lcom/pspdfkit/internal/u8;->b()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/w8;

    .line 43
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    and-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private synthetic b(Lcom/pspdfkit/internal/xl;Landroid/widget/EditText;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;Landroid/view/View;)V
    .locals 0

    .line 1
    sget-object p5, Lcom/pspdfkit/document/PageBinding;->RIGHT_EDGE:Lcom/pspdfkit/document/PageBinding;

    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/xl;->a(Lcom/pspdfkit/document/PageBinding;)V

    .line 2
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object p5

    invoke-virtual {p1, p5}, Lcom/pspdfkit/internal/xl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 3
    invoke-virtual {p3, p1}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    const/4 p1, 0x1

    .line 4
    invoke-virtual {p4, p1}, Lcom/pspdfkit/internal/ui/documentinfo/PageBindingView;->setSelected(Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 44
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p8;->d:Z

    .line 45
    invoke-virtual {p0}, Lcom/pspdfkit/internal/p8;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/internal/p8;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/pspdfkit/internal/p8;->d:Z

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/internal/p8;->notifyDataSetChanged()V

    return-void
.end method

.method public final c()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/p8;->d:Z

    return v0
.end method

.method public final getCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u8;

    return-object p1
.end method

.method public final getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u8;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 1
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/pspdfkit/R$layout;->pspdf__document_info_group:I

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u8;

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    if-nez p1, :cond_1

    goto/16 :goto_6

    .line 4
    :cond_1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/u8;->d()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v1, v1, Lcom/pspdfkit/internal/rl;->L:I

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/u8;->a(I)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v1, v1, Lcom/pspdfkit/internal/rl;->K:I

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/u8;->a(I)V

    goto :goto_0

    .line 10
    :cond_3
    iget-object v1, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v1, v1, Lcom/pspdfkit/internal/rl;->J:I

    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/u8;->a(I)V

    .line 11
    :goto_0
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_info_group_title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/u8;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v2, v2, Lcom/pspdfkit/internal/rl;->E:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 17
    :cond_4
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_info_group_icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 20
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/internal/u8;->a()I

    move-result v3

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v3, v3, Lcom/pspdfkit/internal/rl;->I:I

    .line 21
    invoke-static {v2}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 22
    invoke-static {v2, v3}, Landroidx/core/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 23
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    :cond_5
    sget v1, Lcom/pspdfkit/R$id;->pspdf__document_info_group_content_layout:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 30
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 31
    invoke-virtual {p1}, Lcom/pspdfkit/internal/u8;->b()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/w8;

    .line 32
    iget-boolean v3, p0, Lcom/pspdfkit/internal/p8;->d:Z

    const/4 v4, 0x1

    if-nez v3, :cond_6

    invoke-virtual {v2}, Lcom/pspdfkit/internal/w8;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_7

    goto :goto_1

    .line 33
    :cond_7
    instance-of v3, v2, Lcom/pspdfkit/internal/xl;

    if-eqz v3, :cond_8

    .line 34
    move-object v3, v2

    check-cast v3, Lcom/pspdfkit/internal/xl;

    invoke-direct {p0, p3, v3}, Lcom/pspdfkit/internal/p8;->a(Landroid/view/ViewGroup;Lcom/pspdfkit/internal/xl;)Landroid/view/View;

    move-result-object v3

    goto :goto_5

    .line 35
    :cond_8
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v5, Lcom/pspdfkit/R$layout;->pspdf__document_info_item:I

    invoke-virtual {v3, v5, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 37
    sget v5, Lcom/pspdfkit/R$id;->pspdf__document_info_item_label:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 38
    iget-boolean v6, p0, Lcom/pspdfkit/internal/p8;->d:Z

    if-eqz v6, :cond_9

    invoke-virtual {v2}, Lcom/pspdfkit/internal/w8;->c()Z

    move-result v6

    if-eqz v6, :cond_9

    goto :goto_3

    :cond_9
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v5, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 39
    new-instance v4, Lcom/pspdfkit/internal/o8;

    invoke-direct {v4, v2}, Lcom/pspdfkit/internal/o8;-><init>(Lcom/pspdfkit/internal/w8;)V

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/w8;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v4, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v4, v4, Lcom/pspdfkit/internal/rl;->G:I

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 49
    invoke-virtual {v2}, Lcom/pspdfkit/internal/w8;->c()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v6, Lcom/pspdfkit/R$string;->pspdf__document_info_not_set:I

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_a
    const-string v4, "-"

    .line 50
    :goto_4
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v4, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v4, v4, Lcom/pspdfkit/internal/rl;->H:I

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 53
    :goto_5
    sget v4, Lcom/pspdfkit/R$id;->pspdf__document_info_item_title:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 54
    invoke-virtual {v2}, Lcom/pspdfkit/internal/w8;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v2, p0, Lcom/pspdfkit/internal/p8;->e:Lcom/pspdfkit/internal/rl;

    iget v2, v2, Lcom/pspdfkit/internal/rl;->F:I

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 57
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_b
    :goto_6
    return-object p2
.end method

.method public final notifyDataSetChanged()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u8;

    .line 3
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/p8;->a(Lcom/pspdfkit/internal/u8;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/p8;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5
    :cond_1
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/u8;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/p8;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/p8;->notifyDataSetChanged()V

    return-void
.end method

.class abstract Lcom/pspdfkit/internal/lm;
.super Lcom/pspdfkit/internal/gs;
.source "SourceFile"


# instance fields
.field A:I

.field B:I

.field C:I

.field D:Landroid/widget/OverScroller;

.field E:Lcom/pspdfkit/internal/qq;

.field F:I

.field G:I

.field H:Z

.field I:Z

.field private J:I

.field private final K:Lcom/pspdfkit/internal/rv;

.field private L:Z

.field private final w:Landroid/graphics/PointF;

.field private final x:Landroid/graphics/PointF;

.field private final y:Landroid/graphics/Matrix;

.field z:F


# direct methods
.method public static synthetic $r8$lambda$A0i-3oNQvvpyDDOR1USQEbSEL-w(Lcom/pspdfkit/internal/lm;IIIFJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/pspdfkit/internal/lm;->c(IIIFJ)V

    return-void
.end method

.method public static synthetic $r8$lambda$_1lsqgdnjyl12FXJVf-sN30TUyc(Lcom/pspdfkit/internal/lm;Landroid/graphics/RectF;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/lm;->c(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p8}, Lcom/pspdfkit/internal/gs;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    .line 2
    new-instance p4, Landroid/graphics/PointF;

    invoke-direct {p4}, Landroid/graphics/PointF;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/lm;->w:Landroid/graphics/PointF;

    .line 6
    new-instance p4, Landroid/graphics/PointF;

    invoke-direct {p4}, Landroid/graphics/PointF;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/lm;->x:Landroid/graphics/PointF;

    .line 8
    new-instance p4, Landroid/graphics/Matrix;

    invoke-direct {p4}, Landroid/graphics/Matrix;-><init>()V

    iput-object p4, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    const/4 p4, 0x0

    .line 17
    iput p4, p0, Lcom/pspdfkit/internal/lm;->A:I

    const/4 p5, 0x1

    .line 36
    iput-boolean p5, p0, Lcom/pspdfkit/internal/lm;->H:Z

    .line 41
    iput p4, p0, Lcom/pspdfkit/internal/lm;->J:I

    .line 45
    iput-boolean p4, p0, Lcom/pspdfkit/internal/lm;->L:Z

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p4

    .line 68
    new-instance p5, Landroid/widget/OverScroller;

    invoke-direct {p5, p4}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object p5, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    .line 69
    new-instance p5, Lcom/pspdfkit/internal/qq;

    invoke-direct {p5, p4}, Lcom/pspdfkit/internal/qq;-><init>(Landroid/content/Context;)V

    iput-object p5, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    .line 70
    new-instance p4, Lcom/pspdfkit/internal/rv;

    invoke-direct {p4, p1, p0}, Lcom/pspdfkit/internal/rv;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;Lcom/pspdfkit/internal/ug;)V

    iput-object p4, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p2

    .line 73
    iget-object p2, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget p4, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/utils/Size;

    iget p2, p2, Lcom/pspdfkit/utils/Size;->width:F

    const/high16 p4, 0x3f800000    # 1.0f

    mul-float p2, p2, p4

    sub-float/2addr p1, p2

    const/high16 p2, 0x40000000    # 2.0f

    div-float/2addr p1, p2

    const/4 p5, 0x0

    invoke-static {p1, p5}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/lm;->B:I

    int-to-float p1, p3

    .line 74
    iget-object p3, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget p6, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p3, p6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/pspdfkit/utils/Size;

    iget p3, p3, Lcom/pspdfkit/utils/Size;->height:F

    mul-float p3, p3, p4

    sub-float/2addr p1, p3

    div-float/2addr p1, p2

    invoke-static {p1, p5}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/pspdfkit/internal/lm;->C:I

    return-void
.end method

.method private b(Landroid/graphics/RectF;IJZ)V
    .locals 6

    .line 24
    new-instance v1, Landroid/graphics/RectF;

    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v0, v0

    iget v2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 25
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/pspdfkit/internal/lm;->B:I

    int-to-float v3, v2

    add-float/2addr v0, v3

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 26
    iget v0, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v3

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 27
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/pspdfkit/internal/lm;->C:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 28
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v3

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    if-eqz p5, :cond_0

    const/4 p5, 0x0

    .line 29
    invoke-static {v2, p5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 30
    iget v2, p0, Lcom/pspdfkit/internal/lm;->C:I

    invoke-static {v2, p5}, Ljava/lang/Math;->min(II)I

    move-result p5

    .line 32
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/lm;->l(I)I

    move-result v2

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 33
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/lm;->a(I)I

    move-result p2

    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 35
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v0, v0

    int-to-float p5, p5

    int-to-float v2, v2

    int-to-float p2, p2

    invoke-direct {v3, v0, p5, v2, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p1, v3}, Lcom/pspdfkit/internal/ga;->b(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    move-object v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/pspdfkit/internal/rv;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;FJ)V

    return-void
.end method

.method private b(IZ)Z
    .locals 8

    .line 37
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    .line 38
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int v5, v0, v1

    .line 39
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result p1

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    sub-int v6, p1, v0

    if-nez v5, :cond_1

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 43
    iget-object v2, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v3

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    const/16 v7, 0x190

    .line 45
    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 47
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private b(Z)Z
    .locals 9

    .line 48
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    const v1, 0x3c23d70a    # 0.01f

    add-float/2addr v0, v1

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    const/4 v2, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    if-eqz p1, :cond_1

    .line 51
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/utils/Size;->toRect()Landroid/graphics/RectF;

    move-result-object p1

    .line 52
    iget-boolean v0, p0, Lcom/pspdfkit/internal/gs;->v:Z

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->r()Landroid/graphics/RectF;

    move-result-object v0

    .line 61
    new-instance v1, Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v0

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v4, v0

    const/high16 v0, 0x3f800000    # 1.0f

    add-float v5, v4, v0

    iget p1, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v0

    invoke-direct {v1, v3, v5, p1, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object p1, v1

    .line 69
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    const-wide/16 v3, 0x190

    invoke-virtual {p0, p1, v0, v3, v4}, Lcom/pspdfkit/internal/lm;->a(Landroid/graphics/RectF;IJ)V

    :cond_1
    return v2

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 75
    iget-object v1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/Size;

    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v1, v1, v3

    float-to-int v1, v1

    .line 77
    iget v4, p0, Lcom/pspdfkit/internal/lm;->B:I

    .line 78
    iget v5, p0, Lcom/pspdfkit/internal/lm;->C:I

    .line 79
    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-gt v0, v3, :cond_3

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    goto :goto_0

    :cond_3
    move v3, v4

    .line 80
    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-gt v1, v0, :cond_4

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_4
    move v0, v5

    :goto_1
    sub-int v6, v3, v4

    const/4 v1, 0x1

    if-gt v6, v1, :cond_6

    sub-int v3, v0, v5

    if-le v3, v1, :cond_5

    goto :goto_2

    :cond_5
    return v1

    :cond_6
    :goto_2
    if-eqz p1, :cond_7

    .line 88
    iget-object v3, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    sub-int v7, v0, v5

    const/16 v8, 0x190

    invoke-virtual/range {v3 .. v8}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 90
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_7
    return v2
.end method

.method private synthetic c(IIIFJ)V
    .locals 4

    .line 68
    new-instance v0, Landroid/graphics/PointF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 p1, 0x0

    .line 70
    invoke-virtual {p0, p3, p1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    .line 71
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 73
    iget p1, p0, Lcom/pspdfkit/internal/lm;->z:F

    div-float/2addr p4, p1

    .line 74
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float p1, p1

    div-float/2addr p1, p4

    float-to-int p1, p1

    .line 75
    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float p2, p2

    div-float/2addr p2, p4

    float-to-int p2, p2

    .line 76
    new-instance p4, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    sub-float v2, v1, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    sub-float v3, v0, p2

    add-float/2addr v1, p1

    add-float/2addr v0, p2

    invoke-direct {p4, v2, v3, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 81
    invoke-virtual {p0, p4, p3, p5, p6}, Lcom/pspdfkit/internal/lm;->b(Landroid/graphics/RectF;IJ)V

    return-void
.end method

.method private c(Landroid/graphics/RectF;IJ)V
    .locals 2

    .line 82
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 85
    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 86
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 87
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/pspdfkit/internal/lm;->b(Landroid/graphics/RectF;IJ)V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->a()Z

    move-result v0

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/lm;->b(Z)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->L:Z

    if-nez v0, :cond_2

    .line 6
    iget-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    if-eqz v0, :cond_1

    return-void

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    .line 11
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/lm;->b(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/pspdfkit/internal/lm;->H:Z

    .line 14
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->L:Z

    xor-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/lm;->b(Z)Z

    :cond_2
    return-void
.end method

.method final C()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method final D()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method final E()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method final F()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 2
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne p1, v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    mul-float p1, p1, v0

    float-to-int p1, p1

    return p1
.end method

.method public a(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 6

    .line 134
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 135
    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->c()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/lm;->a(I)I

    move-result v1

    .line 136
    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->c()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/pspdfkit/internal/lm;->l(I)I

    move-result v2

    int-to-float v1, v1

    .line 139
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/4 v4, 0x0

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    .line 140
    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v5

    sub-float/2addr v5, v1

    float-to-int v1, v5

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v3, v1

    neg-float v1, v3

    goto :goto_0

    .line 142
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, v1

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    neg-float v1, v1

    :goto_0
    int-to-float v2, v2

    .line 146
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v3

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1

    .line 147
    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    sub-float/2addr p1, v2

    float-to-int p1, p1

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    add-float/2addr v3, p1

    neg-float p1, v3

    goto :goto_1

    .line 149
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget p1, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr p1, v2

    invoke-static {p1, v4}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    neg-float p1, p1

    .line 152
    :goto_1
    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 153
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 154
    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 155
    iget v1, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    return-object v0
.end method

.method public a(F)V
    .locals 0

    const/4 p1, 0x0

    .line 90
    iput-boolean p1, p0, Lcom/pspdfkit/internal/lm;->L:Z

    .line 91
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    return-void
.end method

.method public a(III)V
    .locals 6

    .line 104
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    iget v2, p0, Lcom/pspdfkit/internal/lm;->C:I

    neg-int p1, p1

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p1

    neg-int p1, p2

    iget p2, p0, Lcom/pspdfkit/internal/ug;->j:I

    div-int/lit8 p2, p2, 0x2

    add-int v4, p2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 105
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method protected final a(IIIFJJ)V
    .locals 13

    move-object v8, p0

    move/from16 v4, p3

    .line 92
    iget v0, v8, Lcom/pspdfkit/internal/lm;->A:I

    if-eq v0, v4, :cond_0

    const/4 v0, 0x0

    .line 93
    invoke-virtual {p0, v4, v0}, Lcom/pspdfkit/internal/lm;->a(IZ)V

    move-wide/from16 v9, p7

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    move-wide v9, v0

    .line 97
    :goto_0
    iget-object v11, v8, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v12, Lcom/pspdfkit/internal/lm$$ExternalSyntheticLambda1;

    move-object v0, v12

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/pspdfkit/internal/lm$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/lm;IIIFJ)V

    invoke-virtual {v11, v12, v9, v10}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(IZ)V
    .locals 8

    .line 4
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->m(I)V

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    const/4 v1, 0x0

    .line 7
    invoke-direct {p0, p1, v1}, Lcom/pspdfkit/internal/lm;->b(IZ)Z

    move-result p1

    if-nez p1, :cond_1

    .line 8
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v3

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v4

    .line 10
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->h()I

    move-result v2

    .line 11
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->i()I

    move-result v2

    .line 13
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 14
    iget-object v2, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    sub-int v5, p1, v3

    sub-int v6, v0, v4

    if-eqz p2, :cond_0

    const/16 v1, 0x190

    const/16 v7, 0x190

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    .line 17
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/lm;->b(Z)Z

    :goto_1
    return-void
.end method

.method public a(Landroid/graphics/RectF;IJ)V
    .locals 9

    .line 98
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    const/4 v1, 0x0

    if-eq v0, p2, :cond_0

    .line 99
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/lm;->a(IZ)V

    const/16 v1, 0x1f4

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    new-instance v8, Lcom/pspdfkit/internal/lm$$ExternalSyntheticLambda0;

    move-object v2, v8

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move-wide v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/pspdfkit/internal/lm$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/lm;Landroid/graphics/RectF;IJ)V

    int-to-long p1, v1

    invoke-virtual {v0, v8, p1, p2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Landroid/graphics/RectF;IJZ)V
    .locals 10

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v1, 0x0

    .line 107
    invoke-virtual {p0, p2, v1}, Lcom/pspdfkit/internal/ug;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 108
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 109
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->q()Landroid/graphics/RectF;

    move-result-object v1

    .line 112
    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->c()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-nez p5, :cond_0

    if-ne p2, v2, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->q()Landroid/graphics/RectF;

    move-result-object p5

    invoke-virtual {p5, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result p5

    if-nez p5, :cond_1

    .line 118
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result p5

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr p5, v2

    .line 119
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v1, v0

    .line 120
    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/lm;->i(I)F

    move-result v0

    mul-float p5, p5, v0

    mul-float v1, v1, v0

    .line 124
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 125
    invoke-static {v0, p5}, Ljava/lang/Math;->min(FF)F

    move-result p5

    .line 128
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->k()F

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 129
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->j()F

    move-result v1

    .line 130
    invoke-static {p5, v1}, Ljava/lang/Math;->min(FF)F

    move-result p5

    invoke-static {v0, p5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 131
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p5

    float-to-int v2, p5

    .line 132
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result p1

    float-to-int v3, p1

    const-wide/16 v8, 0x64

    move-object v1, p0

    move v4, p2

    move-wide v6, p3

    .line 133
    invoke-virtual/range {v1 .. v9}, Lcom/pspdfkit/internal/lm;->a(IIIFJJ)V

    :cond_1
    return-void
.end method

.method public abstract a(Lcom/pspdfkit/internal/dm;)V
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_1

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/rv;->a()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 19
    iput-boolean v0, p0, Lcom/pspdfkit/internal/lm;->L:Z

    .line 21
    :cond_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 23
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->y()V

    :cond_1
    return-void
.end method

.method public a()Z
    .locals 5

    .line 24
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->h()I

    move-result v3

    .line 26
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 27
    iget-object v3, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->i()I

    move-result v4

    .line 28
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 29
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3, v0, v2}, Landroid/view/View;->scrollTo(II)V

    .line 32
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p0, v0, v2}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->g(I)V

    return v1

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/pspdfkit/internal/ug;->b(II)I

    move-result v0

    .line 39
    invoke-direct {p0, v0, v2}, Lcom/pspdfkit/internal/lm;->b(IZ)Z

    move-result v3

    .line 40
    iput-boolean v3, p0, Lcom/pspdfkit/internal/lm;->H:Z

    if-eqz v3, :cond_1

    .line 41
    iget v3, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-eq v3, v0, :cond_1

    .line 42
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/lm;->m(I)V

    .line 43
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->o()V

    .line 44
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v2

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->o()V

    .line 50
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->H:Z

    if-eqz v0, :cond_4

    .line 51
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->L:Z

    if-eqz v0, :cond_2

    .line 52
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/lm;->B:I

    .line 53
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/lm;->C:I

    goto :goto_0

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->E()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->C()I

    move-result v3

    .line 56
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 57
    iput v0, p0, Lcom/pspdfkit/internal/lm;->B:I

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->F()I

    move-result v2

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->D()I

    move-result v3

    .line 59
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 60
    iput v0, p0, Lcom/pspdfkit/internal/lm;->C:I

    .line 61
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 63
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/lm;->a(Lcom/pspdfkit/internal/dm;)V

    :cond_3
    return v1

    :cond_4
    return v2
.end method

.method public a(FFF)Z
    .locals 8

    .line 64
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float p1, p1, v0

    iget v0, p0, Lcom/pspdfkit/internal/ug;->d:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->e:F

    .line 65
    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 66
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    const/4 v1, 0x1

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    return v1

    .line 67
    :cond_0
    iput p1, p0, Lcom/pspdfkit/internal/lm;->z:F

    .line 71
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->x:Landroid/graphics/PointF;

    .line 72
    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 73
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p3, p0, Lcom/pspdfkit/internal/lm;->A:I

    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    invoke-virtual {p2, p3, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 74
    iget-object p2, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 77
    iget p2, p1, Landroid/graphics/PointF;->x:F

    iget-object p3, p0, Lcom/pspdfkit/internal/lm;->w:Landroid/graphics/PointF;

    iget p3, p3, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p3

    iget-object p3, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    .line 78
    invoke-static {p2, p3}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p2

    float-to-int v5, p2

    .line 79
    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget-object p2, p0, Lcom/pspdfkit/internal/lm;->w:Landroid/graphics/PointF;

    iget p2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    iget-object p2, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    .line 80
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->c(FLandroid/graphics/Matrix;)F

    move-result p1

    neg-float p1, p1

    float-to-int v6, p1

    .line 82
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p2, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 84
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->b(Lcom/pspdfkit/internal/dm;)V

    .line 85
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->a(Lcom/pspdfkit/internal/dm;)V

    .line 86
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->B:I

    iget v4, p0, Lcom/pspdfkit/internal/lm;->C:I

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/widget/Scroller;->startScroll(IIIII)V

    return v1
.end method

.method public b(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/Size;

    iget v1, v1, Lcom/pspdfkit/utils/Size;->width:F

    .line 4
    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne p1, v2, :cond_0

    .line 5
    iget p1, p0, Lcom/pspdfkit/internal/lm;->B:I

    goto :goto_0

    .line 6
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float p1, p1

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    mul-float v1, v1, v2

    sub-float/2addr p1, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    .line 7
    :goto_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method protected final b(Landroid/graphics/RectF;)V
    .locals 6

    .line 23
    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/lm;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method protected final b(Landroid/graphics/RectF;IJ)V
    .locals 6

    .line 22
    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/lm;->b(Landroid/graphics/RectF;IJZ)V

    return-void
.end method

.method public abstract b(Lcom/pspdfkit/internal/dm;)V
.end method

.method public b(FFF)Z
    .locals 1

    const/4 p1, 0x1

    .line 8
    iput-boolean p1, p0, Lcom/pspdfkit/internal/lm;->I:Z

    .line 9
    iget p1, p0, Lcom/pspdfkit/internal/lm;->A:I

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1, v0}, Lcom/pspdfkit/internal/lm;->b(IZ)Z

    move-result p1

    .line 11
    iput-boolean p1, p0, Lcom/pspdfkit/internal/lm;->L:Z

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->w:Landroid/graphics/PointF;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget p2, p0, Lcom/pspdfkit/internal/lm;->A:I

    iget-object p3, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/views/document/DocumentView;->a(ILandroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->w:Landroid/graphics/PointF;

    iget-object p2, p0, Lcom/pspdfkit/internal/lm;->y:Landroid/graphics/Matrix;

    invoke-static {p1, p2}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 21
    iget-boolean p1, p0, Lcom/pspdfkit/internal/lm;->L:Z

    return p1
.end method

.method public abstract c()I
.end method

.method public c(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/Size;

    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    .line 4
    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne p1, v2, :cond_0

    .line 5
    iget p1, p0, Lcom/pspdfkit/internal/lm;->C:I

    goto :goto_0

    .line 6
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float p1, p1

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    mul-float v1, v1, v2

    sub-float/2addr p1, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    .line 7
    :goto_0
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method public c(II)Z
    .locals 9

    .line 8
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->H:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 9
    :cond_0
    iput-boolean v1, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 13
    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v4, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 14
    iget-object v3, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    .line 15
    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    move-result v3

    add-int/2addr v3, p1

    iget v4, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p0, v4}, Lcom/pspdfkit/internal/lm;->b(I)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v4}, Landroid/view/View;->getScrollY()I

    move-result v4

    add-int/2addr v4, p2

    iget v5, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p0, v5}, Lcom/pspdfkit/internal/lm;->c(I)I

    move-result v5

    sub-int/2addr v4, v5

    .line 16
    invoke-virtual {v0, v3, v4}, Lcom/pspdfkit/internal/dm;->b(II)Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 17
    iget p1, v0, Landroid/graphics/RectF;->left:F

    float-to-int p1, p1

    iget p2, p0, Lcom/pspdfkit/internal/lm;->B:I

    add-int/2addr p1, p2

    iget v3, v0, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/2addr v3, p2

    iget p2, p0, Lcom/pspdfkit/internal/ug;->i:I

    .line 18
    invoke-static {p1, v3, v1, p2}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    .line 19
    iget p2, v0, Landroid/graphics/RectF;->top:F

    float-to-int p2, p2

    iget v3, p0, Lcom/pspdfkit/internal/lm;->C:I

    add-int/2addr p2, v3

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    add-int/2addr v4, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    invoke-static {p2, v4, v1, v3}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p2

    .line 21
    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v3, v3

    mul-float v1, v1, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v1, v0

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    int-to-float p2, p2

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    invoke-virtual {v0, p1, p2, v3, v1}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    return v2

    .line 23
    :cond_1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    .line 26
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    .line 28
    iget v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    .line 29
    iget v3, p0, Lcom/pspdfkit/internal/lm;->C:I

    .line 31
    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    int-to-float v4, v4

    sub-float/2addr v4, p1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    .line 32
    iget v6, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v6, v6

    sub-float/2addr v6, v0

    div-float/2addr v6, v5

    float-to-int v5, v4

    add-float/2addr v4, p1

    float-to-int v4, v4

    int-to-float v7, v1

    .line 35
    iget v8, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float p1, p1, v8

    add-float/2addr p1, v7

    float-to-int p1, p1

    invoke-static {v5, v4, v1, p1}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result p1

    float-to-int v1, v6

    add-float/2addr v6, v0

    float-to-int v4, v6

    int-to-float v5, v3

    .line 37
    iget v6, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v6, v6, v0

    add-float/2addr v6, v5

    float-to-int v5, v6

    invoke-static {v1, v4, v3, v5}, Lcom/pspdfkit/internal/rv;->a(IIII)I

    move-result v1

    .line 41
    iget-object v3, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    int-to-float p1, p1

    .line 43
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    int-to-float p2, p2

    goto :goto_0

    :cond_2
    int-to-float p2, v1

    :goto_0
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    .line 44
    invoke-virtual {v3, p1, p2, v0, v1}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    goto :goto_3

    :cond_3
    const/high16 v1, 0x40200000    # 2.5f

    mul-float v0, v0, v1

    .line 57
    iget v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    int-to-float v1, v1

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v0, v3

    div-float v3, v0, v3

    mul-float v1, v1, v3

    float-to-int v1, v1

    .line 58
    iget v4, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int v5, v4, v1

    if-lt v1, v5, :cond_4

    .line 60
    div-int/lit8 v4, v4, 0x2

    goto :goto_1

    .line 61
    :cond_4
    invoke-static {p1, v5}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 62
    :goto_1
    iget p1, p0, Lcom/pspdfkit/internal/lm;->C:I

    int-to-float p1, p1

    mul-float p1, p1, v3

    float-to-int p1, p1

    .line 63
    iget v1, p0, Lcom/pspdfkit/internal/ug;->j:I

    sub-int v3, v1, p1

    if-lt p1, v3, :cond_5

    .line 65
    div-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 66
    :cond_5
    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 67
    :goto_2
    iget-object p1, p0, Lcom/pspdfkit/internal/lm;->K:Lcom/pspdfkit/internal/rv;

    int-to-float p2, v4

    int-to-float v1, v1

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    invoke-virtual {p1, p2, v1, v3, v0}, Lcom/pspdfkit/internal/rv;->a(FFFF)V

    :goto_3
    return v2
.end method

.method final c(Z)Z
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/lm;->b(Z)Z

    move-result p1

    return p1
.end method

.method public i(I)F
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne v0, p1, :cond_0

    iget p1, p0, Lcom/pspdfkit/internal/lm;->z:F

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    return p1
.end method

.method public l(I)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne p1, v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    iget p1, p1, Lcom/pspdfkit/utils/Size;->width:F

    iget v0, p0, Lcom/pspdfkit/internal/ug;->c:F

    :goto_0
    mul-float p1, p1, v0

    float-to-int p1, p1

    return p1
.end method

.method final m(I)V
    .locals 3

    const/4 v0, 0x0

    .line 1
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 4
    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    iput v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    .line 5
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->b(I)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->f(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    .line 6
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->c(I)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ug;->g(I)I

    move-result v0

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/pspdfkit/internal/lm;->C:I

    .line 7
    iput p1, p0, Lcom/pspdfkit/internal/lm;->A:I

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    iget v0, p0, Lcom/pspdfkit/internal/lm;->J:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/views/document/DocumentView;->b(I)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 12
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->b(Lcom/pspdfkit/internal/dm;)V

    .line 13
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->a(Lcom/pspdfkit/internal/dm;)V

    .line 16
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/lm;->A:I

    iput p1, p0, Lcom/pspdfkit/internal/lm;->J:I

    return-void
.end method

.method public w()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->L:Z

    return v0
.end method

.method public x()V
    .locals 2

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/lm;->I:Z

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/ug;->o:Z

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {v1, v0}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/lm;->F:I

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/pspdfkit/internal/lm;->G:I

    return-void
.end method

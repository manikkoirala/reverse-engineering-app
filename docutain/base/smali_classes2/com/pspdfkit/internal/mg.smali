.class public final Lcom/pspdfkit/internal/mg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/datastructures/Range;

.field private final b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/datastructures/Range;ZZ)V
    .locals 1

    const-string v0, "printRange"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/pspdfkit/internal/mg;->a:Lcom/pspdfkit/datastructures/Range;

    .line 6
    iput-boolean p2, p0, Lcom/pspdfkit/internal/mg;->b:Z

    .line 9
    iput-boolean p3, p0, Lcom/pspdfkit/internal/mg;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/document/printing/PrintOptions;
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/document/printing/PrintOptions;

    iget-boolean v1, p0, Lcom/pspdfkit/internal/mg;->c:Z

    iget-object v2, p0, Lcom/pspdfkit/internal/mg;->a:Lcom/pspdfkit/datastructures/Range;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/document/printing/PrintOptions;-><init>(ZLjava/util/List;)V

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/mg;->b:Z

    return v0
.end method

.class public final Lcom/pspdfkit/internal/gu;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/gu$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/pspdfkit/internal/views/document/editor/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/pspdfkit/internal/zf;

.field private final c:Lcom/pspdfkit/internal/views/document/editor/b;

.field private final d:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

.field private final e:Lcom/pspdfkit/internal/iu;

.field private final f:I

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue<",
            "Lcom/pspdfkit/internal/gu$a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/os/Handler;

.field private j:Z

.field private final k:Ljava/util/ArrayList;

.field private l:I

.field private m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

.field private final n:Ljava/lang/Runnable;

.field private o:Z

.field private final p:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;


# direct methods
.method public static synthetic $r8$lambda$8i4FY1B3iRPZ4gLm_RNUTgEeZpQ(Lcom/pspdfkit/internal/gu$a;Lcom/pspdfkit/internal/gu$a;)I
    .locals 0

    invoke-static {p0, p1}, Lcom/pspdfkit/internal/gu;->a(Lcom/pspdfkit/internal/gu$a;Lcom/pspdfkit/internal/gu$a;)I

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$F1JBu6afD6WPqSaBodWnaQy1hFI(Lcom/pspdfkit/internal/gu;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/gu;->b()V

    return-void
.end method

.method public static synthetic $r8$lambda$wATdRZ-2J5mSogl9g-nABVmLfLc(Lcom/pspdfkit/internal/gu;JLandroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/gb;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/gu;->a(JLandroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/gb;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$yABf6yJENov0RKnwOZ07mxWxV0I(Lcom/pspdfkit/internal/gu;Lcom/pspdfkit/internal/views/document/editor/a;III)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/gu;->a(Lcom/pspdfkit/internal/views/document/editor/a;III)Lio/reactivex/rxjava3/core/SingleSource;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mb(Lcom/pspdfkit/internal/gu;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/gu;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/iu;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;Lcom/pspdfkit/internal/views/document/editor/b;Lcom/pspdfkit/configuration/PdfConfiguration;IZZ)V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    new-instance v0, Ljava/util/PriorityQueue;

    new-instance v1, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda1;-><init>()V

    const/16 v2, 0xf

    invoke-direct {v0, v2, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/gu;->h:Ljava/util/PriorityQueue;

    .line 6
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/gu;->i:Landroid/os/Handler;

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/gu;->k:Ljava/util/ArrayList;

    const/4 v0, -0x1

    .line 15
    iput v0, p0, Lcom/pspdfkit/internal/gu;->l:I

    .line 20
    new-instance v0, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/gu;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/gu;->n:Ljava/lang/Runnable;

    .line 38
    iput-object p1, p0, Lcom/pspdfkit/internal/gu;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    .line 40
    iput-object p3, p0, Lcom/pspdfkit/internal/gu;->e:Lcom/pspdfkit/internal/iu;

    .line 41
    invoke-static {p2, p6}, Lcom/pspdfkit/internal/x5;->c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/gu;->p:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 42
    iput-boolean p8, p0, Lcom/pspdfkit/internal/gu;->j:Z

    .line 43
    iput-object p4, p0, Lcom/pspdfkit/internal/gu;->d:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    .line 44
    iput-object p5, p0, Lcom/pspdfkit/internal/gu;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    .line 45
    iput p7, p0, Lcom/pspdfkit/internal/gu;->f:I

    .line 46
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {p6}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/gu;->g:Ljava/util/ArrayList;

    .line 47
    iput-boolean p9, p0, Lcom/pspdfkit/internal/gu;->o:Z

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/gu$a;Lcom/pspdfkit/internal/gu$a;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/pspdfkit/internal/gu$a;->b:I

    iget p1, p1, Lcom/pspdfkit/internal/gu$a;->b:I

    if-ge p0, p1, :cond_0

    const/4 p0, -0x1

    goto :goto_0

    :cond_0
    if-ne p0, p1, :cond_1

    const/4 p0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method private synthetic a(JLandroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/gb;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 38
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 p1, 0x32

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 40
    :goto_0
    new-instance p2, Lcom/pspdfkit/internal/gb;

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p2, v0, p4, p3, p1}, Lcom/pspdfkit/internal/gb;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    return-object p2
.end method

.method private a(Lcom/pspdfkit/internal/views/document/editor/a;III)Lio/reactivex/rxjava3/core/SingleSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 41
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    iget-object v1, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/n4;->d(Landroid/graphics/Bitmap;)V

    .line 42
    invoke-static {}, Lcom/pspdfkit/internal/gj;->h()Lcom/pspdfkit/internal/n4;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/pspdfkit/internal/n4;->a(II)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 44
    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    if-nez p2, :cond_2

    .line 45
    new-instance p2, Lcom/pspdfkit/internal/rc$a;

    iget-object p3, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    invoke-direct {p2, p3, p4}, Lcom/pspdfkit/internal/rc$a;-><init>(Lcom/pspdfkit/internal/zf;I)V

    const/4 p3, 0x5

    .line 46
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    iget-object p3, p0, Lcom/pspdfkit/internal/gu;->p:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 47
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/rc$a;->b(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/rc$a;

    move-result-object p2

    iget-object p3, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 48
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    iget-object p3, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 49
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/rc$a;

    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 50
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    const/4 p2, 0x0

    .line 51
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/lang/Integer;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->g:Ljava/util/ArrayList;

    .line 52
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->a:Landroid/content/Context;

    .line 53
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 55
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;

    .line 57
    iget-object v2, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    .line 58
    invoke-virtual {v1, p2, v2, p4}, Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;->getDrawablesForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 60
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p1, p3}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/List;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    iget-boolean p2, p0, Lcom/pspdfkit/internal/gu;->o:Z

    .line 63
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/rc$a;

    .line 64
    invoke-virtual {p1}, Lcom/pspdfkit/internal/rc$a;->b()Lcom/pspdfkit/internal/rc;

    move-result-object p1

    .line 65
    invoke-static {p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/rc;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1

    .line 67
    :cond_2
    new-instance p3, Lcom/pspdfkit/internal/l8$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    invoke-direct {p3, v0, p4, p2}, Lcom/pspdfkit/internal/l8$a;-><init>(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/internal/jni/NativeDocumentEditor;)V

    const/16 p2, 0xa

    .line 69
    invoke-virtual {p3, p2}, Lcom/pspdfkit/internal/g4$a;->c(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    iget-object p3, p0, Lcom/pspdfkit/internal/gu;->p:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    .line 70
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->a(Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    iget-object p3, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 71
    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->a(Landroid/graphics/Bitmap;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    iget-object p3, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 72
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/g4$a;->b(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/l8$a;

    iget-object p1, p1, Lcom/pspdfkit/internal/views/document/editor/a;->d:Landroid/graphics/Bitmap;

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/g4$a;->a(I)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/l8$a;

    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->g:Ljava/util/ArrayList;

    .line 74
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/l8$a;

    iget-boolean p2, p0, Lcom/pspdfkit/internal/gu;->o:Z

    .line 75
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/g4$a;->b(Z)Lcom/pspdfkit/internal/g4$a;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/l8$a;

    .line 76
    invoke-virtual {p1}, Lcom/pspdfkit/internal/l8$a;->b()Lcom/pspdfkit/internal/l8;

    move-result-object p1

    .line 77
    invoke-static {p1}, Lcom/pspdfkit/internal/hm;->a(Lcom/pspdfkit/internal/l8;)Lio/reactivex/rxjava3/core/Single;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/graphics/drawable/Drawable;J)Lio/reactivex/rxjava3/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/drawable/Drawable;",
            "J)",
            "Lio/reactivex/rxjava3/functions/Function<",
            "Landroid/graphics/Bitmap;",
            "Lcom/pspdfkit/internal/gb;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/gu;JLandroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method private b(Lcom/pspdfkit/internal/views/document/editor/a;III)Lio/reactivex/rxjava3/functions/Supplier;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/views/document/editor/a;",
            "III)",
            "Lio/reactivex/rxjava3/functions/Supplier<",
            "Lio/reactivex/rxjava3/core/SingleSource<",
            "+",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .line 17
    new-instance v6, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda0;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p4

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/pspdfkit/internal/gu$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/gu;Lcom/pspdfkit/internal/views/document/editor/a;III)V

    return-object v6
.end method

.method private b()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/gu$a;

    if-eqz v0, :cond_0

    .line 3
    iget-object v1, v0, Lcom/pspdfkit/internal/gu$a;->a:Lcom/pspdfkit/internal/views/document/editor/a;

    iget v2, v0, Lcom/pspdfkit/internal/gu$a;->b:I

    iget v3, v0, Lcom/pspdfkit/internal/gu$a;->c:I

    iget v0, v0, Lcom/pspdfkit/internal/gu$a;->d:I

    .line 4
    iget-object v4, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 5
    check-cast v4, Lcom/pspdfkit/internal/ju;

    .line 6
    invoke-virtual {v4}, Lcom/pspdfkit/internal/ju;->getThumbnailDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 9
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/pspdfkit/internal/gu;->b(Lcom/pspdfkit/internal/views/document/editor/a;III)Lio/reactivex/rxjava3/functions/Supplier;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 10
    invoke-direct {p0, v4, v5, v6}, Lcom/pspdfkit/internal/gu;->a(Landroid/graphics/drawable/Drawable;J)Lio/reactivex/rxjava3/functions/Function;

    move-result-object v3

    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Single;->map(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 11
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/u;

    const/4 v4, 0x5

    .line 12
    invoke-virtual {v3, v4}, Lcom/pspdfkit/internal/u;->a(I)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    .line 13
    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v3

    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 15
    new-instance v3, Lcom/pspdfkit/internal/fu;

    invoke-direct {v3, p0, v1, v2}, Lcom/pspdfkit/internal/fu;-><init>(Lcom/pspdfkit/internal/gu;Lcom/pspdfkit/internal/views/document/editor/a;I)V

    .line 16
    invoke-virtual {v0, v3}, Lio/reactivex/rxjava3/core/Single;->subscribeWith(Lio/reactivex/rxjava3/core/SingleObserver;)Lio/reactivex/rxjava3/core/SingleObserver;

    move-result-object v0

    check-cast v0, Lio/reactivex/rxjava3/disposables/Disposable;

    iput-object v0, v1, Lcom/pspdfkit/internal/views/document/editor/a;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroidx/recyclerview/widget/RecyclerView;)V
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget v0, p0, Lcom/pspdfkit/internal/gu;->l:I

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-le v0, v1, :cond_1

    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    return-void

    .line 11
    :cond_2
    iput p1, p0, Lcom/pspdfkit/internal/gu;->l:I

    .line 13
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_5

    .line 14
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 15
    invoke-virtual {p2, v5}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v6

    if-eq v6, p1, :cond_3

    if-eq v6, v0, :cond_3

    goto :goto_3

    .line 22
    :cond_3
    invoke-virtual {p2, v5}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v5

    .line 23
    check-cast v5, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 24
    iget-object v5, v5, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 25
    check-cast v5, Lcom/pspdfkit/internal/ju;

    if-ne v6, p1, :cond_4

    const/4 v6, 0x1

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    .line 26
    :goto_2
    invoke-virtual {v5, v6}, Lcom/pspdfkit/internal/ju;->setHighlighted(Z)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/jni/NativeDocumentEditor;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    .line 27
    iput-object p1, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 30
    iget p1, p0, Lcom/pspdfkit/internal/gu;->l:I

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    .line 32
    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 34
    check-cast p1, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 35
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast p1, Lcom/pspdfkit/internal/ju;

    const/4 p2, 0x0

    .line 36
    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/ju;->setHighlighted(Z)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/ui/drawable/PdfDrawableProvider;",
            ">;)V"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 82
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 79
    iput-boolean p1, p0, Lcom/pspdfkit/internal/gu;->o:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/pspdfkit/internal/gu;->o:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .line 18
    iput-boolean p1, p0, Lcom/pspdfkit/internal/gu;->j:Z

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->getPageCount()I

    move-result v0

    :goto_0
    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 13

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 3
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/gu$a;

    .line 5
    iget-object v1, v1, Lcom/pspdfkit/internal/gu$a;->a:Lcom/pspdfkit/internal/views/document/editor/a;

    if-ne v1, p1, :cond_0

    .line 6
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 7
    :cond_1
    iget-object v0, p1, Lcom/pspdfkit/internal/views/document/editor/a;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 8
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v0, 0x0

    .line 9
    iput-object v0, p1, Lcom/pspdfkit/internal/views/document/editor/a;->e:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 10
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/pspdfkit/internal/ju;

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_3

    .line 13
    iget-boolean v4, p0, Lcom/pspdfkit/internal/gu;->j:Z

    if-eqz v4, :cond_3

    .line 14
    iget-object v4, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v4, p2, v3}, Lcom/pspdfkit/internal/zf;->getPageLabel(IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setItemLabelText(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    add-int/lit8 v4, p2, 0x1

    .line 17
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setItemLabelText(Ljava/lang/String;)V

    .line 19
    :goto_2
    iget-object v4, p0, Lcom/pspdfkit/internal/gu;->e:Lcom/pspdfkit/internal/iu;

    iget v4, v4, Lcom/pspdfkit/internal/iu;->a:I

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setItemLabelStyle(I)V

    .line 20
    iget-object v4, p0, Lcom/pspdfkit/internal/gu;->e:Lcom/pspdfkit/internal/iu;

    iget v4, v4, Lcom/pspdfkit/internal/iu;->b:I

    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setItemLabelBackground(I)V

    if-nez v1, :cond_4

    .line 25
    iget v4, p0, Lcom/pspdfkit/internal/gu;->l:I

    if-ne p2, v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setHighlighted(Z)V

    if-eqz v1, :cond_5

    .line 28
    iget-object v1, p0, Lcom/pspdfkit/internal/gu;->m:Lcom/pspdfkit/internal/jni/NativeDocumentEditor;

    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/jni/NativeDocumentEditor;->getRotatedPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    goto :goto_4

    .line 29
    :cond_5
    iget-object v1, p0, Lcom/pspdfkit/internal/gu;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1, p2}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v1

    .line 30
    :goto_4
    iget v4, v1, Lcom/pspdfkit/utils/Size;->width:F

    .line 31
    iget v1, v1, Lcom/pspdfkit/utils/Size;->height:F

    const/4 v5, 0x0

    cmpl-float v6, v4, v5

    if-eqz v6, :cond_b

    cmpl-float v5, v1, v5

    if-nez v5, :cond_6

    goto/16 :goto_7

    :cond_6
    div-float v5, v1, v4

    .line 40
    iget v6, p0, Lcom/pspdfkit/internal/gu;->f:I

    int-to-float v7, v6

    mul-float v5, v5, v7

    float-to-int v5, v5

    int-to-float v8, v5

    div-float/2addr v8, v1

    div-float/2addr v7, v4

    cmpg-float v7, v8, v7

    if-gez v7, :cond_7

    float-to-double v6, v4

    int-to-double v8, v5

    float-to-double v10, v1

    div-double/2addr v8, v10

    mul-double v8, v8, v6

    double-to-int v6, v8

    move v1, v5

    goto :goto_5

    :cond_7
    float-to-double v7, v1

    int-to-double v9, v6

    float-to-double v11, v4

    div-double/2addr v9, v11

    mul-double v9, v9, v7

    double-to-int v1, v9

    .line 52
    :goto_5
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ju;->getThumbnailView()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 53
    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v7, v6, :cond_8

    iget v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v7, v1, :cond_9

    .line 54
    :cond_8
    iget v7, p0, Lcom/pspdfkit/internal/gu;->f:I

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 55
    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ju;->getThumbnailView()Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    :cond_9
    new-instance v4, Lcom/pspdfkit/internal/c5;

    .line 60
    iget-object v7, p0, Lcom/pspdfkit/internal/gu;->p:Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    iget-boolean v8, v7, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->invertColors:Z

    if-eqz v8, :cond_a

    .line 61
    iget v7, v7, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    invoke-static {v7}, Lcom/pspdfkit/internal/ga;->c(I)I

    move-result v7

    goto :goto_6

    .line 62
    :cond_a
    iget v7, v7, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;->paperColor:I

    :goto_6
    iget v8, p0, Lcom/pspdfkit/internal/gu;->f:I

    invoke-direct {v4, v7, v8, v5}, Lcom/pspdfkit/internal/c5;-><init>(III)V

    .line 66
    invoke-virtual {v0, v4}, Lcom/pspdfkit/internal/ju;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    iget-object v4, p0, Lcom/pspdfkit/internal/gu;->a:Landroid/content/Context;

    sget v5, Lcom/pspdfkit/R$string;->pspdf__page_with_number:I

    new-array v3, v3, [Ljava/lang/Object;

    add-int/lit8 v7, p2, 0x1

    .line 68
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v2

    invoke-static {v4, v5, v0, v3}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 71
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->h:Ljava/util/PriorityQueue;

    new-instance v2, Lcom/pspdfkit/internal/gu$a;

    invoke-direct {v2, p1, p2, v6, v1}, Lcom/pspdfkit/internal/gu$a;-><init>(Lcom/pspdfkit/internal/views/document/editor/a;III)V

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->n:Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->i:Landroid/os/Handler;

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->n:Ljava/lang/Runnable;

    const-wide/16 v1, 0x64

    invoke-virtual {p2, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 79
    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/views/document/editor/b;->b(Lcom/pspdfkit/internal/views/document/editor/a;)V

    goto :goto_8

    .line 80
    :cond_b
    :goto_7
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 p2, -0x1

    invoke-direct {p1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ju;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_8
    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    .line 1
    new-instance p1, Lcom/pspdfkit/internal/ju;

    iget-object p2, p0, Lcom/pspdfkit/internal/gu;->a:Landroid/content/Context;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/ju;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance p2, Lcom/pspdfkit/internal/views/document/editor/a;

    iget-object v0, p0, Lcom/pspdfkit/internal/gu;->d:Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;

    iget-object v1, p0, Lcom/pspdfkit/internal/gu;->c:Lcom/pspdfkit/internal/views/document/editor/b;

    invoke-direct {p2, p1, v0, v1}, Lcom/pspdfkit/internal/views/document/editor/a;-><init>(Lcom/pspdfkit/internal/ju;Lcom/pspdfkit/internal/views/document/editor/ThumbnailGridRecyclerView$a;Lcom/pspdfkit/internal/views/document/editor/b;)V

    return-object p2
.end method

.method public final onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/pspdfkit/internal/views/document/editor/a;

    .line 2
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    return-void
.end method

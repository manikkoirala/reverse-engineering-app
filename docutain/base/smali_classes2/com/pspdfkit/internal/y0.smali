.class public final Lcom/pspdfkit/internal/y0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/pspdfkit/internal/m1;


# direct methods
.method private constructor <init>(Lcom/pspdfkit/internal/m1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/m1;)Lcom/pspdfkit/internal/y0;
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/y0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/y0;-><init>(Lcom/pspdfkit/internal/m1;)V

    return-object v0
.end method

.method private static b(Lcom/pspdfkit/internal/m1;)Ljava/util/ArrayList;
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/m1;->H()I

    move-result v0

    .line 2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    .line 4
    invoke-virtual {p0, v3}, Lcom/pspdfkit/internal/m1;->i(I)Lcom/pspdfkit/internal/bh;

    move-result-object v4

    .line 5
    invoke-virtual {v4}, Lcom/pspdfkit/internal/bh;->a()I

    move-result v5

    if-lez v5, :cond_2

    .line 7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v5, :cond_1

    .line 9
    invoke-virtual {v4, v7}, Lcom/pspdfkit/internal/bh;->f(I)Lcom/pspdfkit/internal/ea;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 11
    new-instance v9, Landroid/graphics/PointF;

    invoke-virtual {v8}, Lcom/pspdfkit/internal/ea;->a()F

    move-result v10

    invoke-virtual {v8}, Lcom/pspdfkit/internal/ea;->b()F

    move-result v8

    invoke-direct {v9, v10, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/p1;)V
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 2
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->R()J

    move-result-wide v3

    long-to-int v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 3
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->Q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 4
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->M()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x2

    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 5
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->o()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x3

    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 6
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->e0()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x4

    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 7
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->q()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x6

    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 8
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->X()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v5, 0x12

    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 9
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->u()J

    move-result-wide v5

    const-class v0, Lcom/pspdfkit/annotations/AnnotationFlags;

    .line 12
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v7

    const/4 v8, 0x0

    .line 13
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Enum;

    array-length v9, v9

    const-wide/16 v10, 0x0

    if-ge v8, v9, :cond_1

    shl-int v9, v4, v8

    int-to-long v12, v9

    and-long/2addr v12, v5

    cmp-long v9, v12, v10

    if-eqz v9, :cond_0

    .line 15
    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Enum;

    aget-object v9, v9, v8

    invoke-virtual {v7, v9}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 23
    :cond_1
    invoke-virtual {v7}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v7, 0x0

    :cond_2
    const/16 v0, 0x10

    .line 24
    invoke-virtual {v2, v0, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 27
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 28
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->p()Lcom/pspdfkit/internal/u7;

    move-result-object v0

    const-wide/16 v6, 0x3e8

    if-eqz v0, :cond_3

    .line 29
    new-instance v8, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u7;->a()J

    move-result-wide v12

    mul-long v12, v12, v6

    invoke-direct {v8, v12, v13}, Ljava/util/Date;-><init>(J)V

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    :goto_1
    const/4 v0, 0x7

    .line 30
    invoke-virtual {v2, v0, v8}, Lcom/pspdfkit/internal/p1;->a(ILjava/util/Date;)V

    .line 32
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 34
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->D()Lcom/pspdfkit/internal/u7;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 35
    new-instance v8, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/u7;->a()J

    move-result-wide v12

    mul-long v12, v12, v6

    invoke-direct {v8, v12, v13}, Ljava/util/Date;-><init>(J)V

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    :goto_2
    const/16 v0, 0x8

    .line 36
    invoke-virtual {v2, v0, v8}, Lcom/pspdfkit/internal/p1;->a(ILjava/util/Date;)V

    .line 39
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->g()Lcom/pspdfkit/internal/ko;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 40
    new-instance v6, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->b()F

    move-result v7

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->d()F

    move-result v8

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->c()F

    move-result v9

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->a()F

    move-result v0

    invoke-direct {v6, v7, v8, v9, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_3

    :cond_5
    const/4 v6, 0x0

    :goto_3
    const/16 v0, 0x9

    .line 41
    invoke-virtual {v2, v0, v6}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    const/16 v0, 0x3ef

    .line 42
    iget-object v6, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 44
    invoke-virtual {v6}, Lcom/pspdfkit/internal/m1;->f0()Lcom/pspdfkit/internal/ha;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 45
    new-instance v7, Lcom/pspdfkit/utils/EdgeInsets;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ha;->d()F

    move-result v8

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ha;->b()F

    move-result v9

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ha;->a()F

    move-result v12

    invoke-virtual {v6}, Lcom/pspdfkit/internal/ha;->c()F

    move-result v6

    invoke-direct {v7, v8, v9, v12, v6}, Lcom/pspdfkit/utils/EdgeInsets;-><init>(FFFF)V

    goto :goto_4

    :cond_6
    const/4 v7, 0x0

    .line 46
    :goto_4
    invoke-virtual {v2, v0, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 49
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 50
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->n()Lcom/pspdfkit/internal/ko;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 51
    new-instance v6, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->b()F

    move-result v7

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->d()F

    move-result v8

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->c()F

    move-result v9

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ko;->a()F

    move-result v0

    invoke-direct {v6, v7, v8, v9, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_5

    :cond_7
    const/4 v6, 0x0

    :goto_5
    const/16 v0, 0x16

    .line 52
    invoke-virtual {v2, v0, v6}, Lcom/pspdfkit/internal/p1;->a(ILandroid/graphics/RectF;)V

    .line 54
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->m()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v6, 0xa

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 55
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 56
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->t()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v6, 0xb

    .line 57
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    const/16 v0, 0xc

    .line 59
    iget-object v6, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v6}, Lcom/pspdfkit/internal/m1;->c()Lcom/pspdfkit/internal/ob;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 60
    invoke-virtual {v6}, Lcom/pspdfkit/internal/ob;->a()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    .line 61
    :goto_6
    invoke-virtual {v2, v0, v6}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 62
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->W()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x5

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 64
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 65
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->i()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v6, 0xd

    .line 66
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 68
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 70
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->l()S

    move-result v0

    .line 71
    const-class v6, Lcom/pspdfkit/annotations/BorderStyle;

    invoke-virtual {v6}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Enum;

    aget-object v0, v6, v0

    const/16 v6, 0xe

    .line 72
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    const/16 v0, 0xf

    .line 75
    iget-object v6, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 76
    invoke-virtual {v6}, Lcom/pspdfkit/internal/m1;->s()I

    move-result v7

    if-nez v7, :cond_9

    const/4 v8, 0x0

    goto :goto_8

    .line 79
    :cond_9
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v9, 0x0

    :goto_7
    if-ge v9, v7, :cond_a

    .line 81
    invoke-virtual {v6, v9}, Lcom/pspdfkit/internal/m1;->g(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 82
    :cond_a
    :goto_8
    invoke-virtual {v2, v0, v8}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 83
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 85
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->j()S

    move-result v0

    .line 86
    const-class v6, Lcom/pspdfkit/annotations/BorderEffect;

    invoke-virtual {v6}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Enum;

    aget-object v0, v6, v0

    const/16 v6, 0x18

    .line 87
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 90
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->k()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/16 v6, 0x19

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 92
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->k0()Ljava/lang/String;

    move-result-object v0

    const/16 v6, 0x1a

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 94
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 96
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->h()S

    move-result v0

    .line 97
    const-class v6, Lcom/pspdfkit/annotations/BlendMode;

    invoke-virtual {v6}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Enum;

    aget-object v0, v6, v0

    const/16 v6, 0x17

    .line 98
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 101
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 103
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->f()S

    move-result v0

    .line 104
    const-class v6, Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-virtual {v6}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Enum;

    aget-object v0, v6, v0

    const/16 v6, 0x13

    .line 105
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 108
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->j0()Ljava/lang/String;

    move-result-object v0

    const/16 v6, 0x14

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 109
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->B()Ljava/lang/String;

    move-result-object v0

    const/16 v6, 0x15

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 110
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 111
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->a()Lcom/pspdfkit/internal/b;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/b;)Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    const/16 v6, 0xbb8

    .line 112
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 114
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 116
    invoke-static {v0}, Lcom/pspdfkit/internal/d;->a(Lcom/pspdfkit/internal/m1;)Lcom/pspdfkit/internal/o;

    move-result-object v0

    const/16 v6, 0xbb9

    .line 117
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    const/16 v6, 0x2329

    .line 120
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 122
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->r()Lcom/pspdfkit/internal/ng;

    move-result-object v0

    if-nez v0, :cond_b

    goto :goto_9

    .line 123
    :cond_b
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ng;->a()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_c

    :goto_9
    const/4 v0, 0x0

    goto :goto_a

    .line 126
    :cond_c
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_a

    :catch_0
    move-exception v0

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v7, v8, v3

    const-string v7, "PSPDFKit.Annotations"

    const-string v9, "Can\'t parse custom data json: %s"

    .line 128
    invoke-static {v7, v0, v9, v8}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_9

    .line 129
    :goto_a
    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 133
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->i0()S

    move-result v0

    .line 134
    const-class v6, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v6}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Enum;

    aget-object v0, v6, v0

    .line 135
    check-cast v0, Lcom/pspdfkit/annotations/AnnotationType;

    .line 136
    sget-object v6, Lcom/pspdfkit/internal/y0$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v6, v0

    const/16 v6, 0x67

    const/16 v7, 0x1389

    const/16 v8, 0x7d0

    const/16 v13, 0x3e9

    const/16 v14, 0x66

    const/16 v15, 0x64

    const/16 v3, 0x2afa

    const/16 v5, 0x2af9

    const/16 v9, 0xfa0

    const/16 v12, 0x65

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1a

    .line 256
    :pswitch_0
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 257
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->U()I

    move-result v3

    .line 258
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_b
    if-ge v5, v3, :cond_f

    .line 260
    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/m1;->k(I)Lcom/pspdfkit/internal/io;

    move-result-object v6

    if-nez v6, :cond_d

    const/4 v6, 0x0

    goto :goto_c

    .line 261
    :cond_d
    new-instance v17, Lcom/pspdfkit/internal/ho;

    .line 262
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->e()F

    move-result v9

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->f()F

    move-result v10

    .line 263
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->g()F

    move-result v11

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->h()F

    move-result v12

    .line 264
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->a()F

    move-result v13

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->b()F

    move-result v14

    .line 265
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->c()F

    move-result v15

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->d()F

    move-result v16

    move-object/from16 v8, v17

    invoke-direct/range {v8 .. v16}, Lcom/pspdfkit/internal/ho;-><init>(FFFFFFFF)V

    move-object/from16 v6, v17

    :goto_c
    if-eqz v6, :cond_e

    .line 266
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 267
    :cond_f
    invoke-virtual {v2, v7, v4}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 268
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 270
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->O()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x1f41

    .line 271
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 274
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->P()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1f42

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 275
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->V()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/16 v3, 0x1f43

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Boolean;)V

    goto/16 :goto_1a

    .line 276
    :pswitch_1
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 277
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    if-eqz v0, :cond_10

    const/16 v16, 0x1

    goto :goto_d

    :cond_10
    const/16 v16, 0x0

    :goto_d
    if-eqz v16, :cond_20

    .line 278
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 280
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->I()S

    move-result v0

    .line 281
    const-class v4, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v4}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Enum;

    aget-object v0, v4, v0

    .line 282
    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 285
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 287
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/si;)Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    .line 288
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 289
    :pswitch_2
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 290
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-static {v0}, Lcom/pspdfkit/internal/y0;->b(Lcom/pspdfkit/internal/m1;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v15, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 291
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 292
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->E()I

    move-result v6

    .line 293
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v8, 0x0

    :goto_e
    if-ge v8, v6, :cond_11

    .line 295
    invoke-virtual {v0, v8}, Lcom/pspdfkit/internal/m1;->h(I)S

    move-result v9

    .line 296
    const-class v10, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v10}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/Enum;

    aget-object v9, v10, v9

    .line 297
    check-cast v9, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_e

    .line 298
    :cond_11
    invoke-virtual {v2, v14, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 299
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->F()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    const/16 v6, 0x68

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 300
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    if-eqz v0, :cond_12

    const/16 v16, 0x1

    goto :goto_f

    :cond_12
    const/16 v16, 0x0

    :goto_f
    if-eqz v16, :cond_20

    .line 301
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 303
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->I()S

    move-result v0

    .line 304
    const-class v4, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v4}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Enum;

    aget-object v0, v4, v0

    .line 305
    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 308
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 310
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/si;)Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    .line 311
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 312
    :pswitch_3
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 313
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 314
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->S()I

    move-result v7

    .line 315
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v9, 0x0

    :goto_10
    if-ge v9, v7, :cond_13

    .line 317
    invoke-virtual {v0, v9}, Lcom/pspdfkit/internal/m1;->j(I)Lcom/pspdfkit/internal/ln;

    move-result-object v10

    .line 318
    new-instance v11, Landroid/graphics/PointF;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/ln;->a()F

    move-result v12

    invoke-virtual {v10}, Lcom/pspdfkit/internal/ln;->b()F

    move-result v10

    invoke-direct {v11, v12, v10}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_10

    .line 319
    :cond_13
    invoke-virtual {v2, v6, v8}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 320
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 321
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->E()I

    move-result v6

    .line 322
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v8, 0x0

    :goto_11
    if-ge v8, v6, :cond_14

    .line 324
    invoke-virtual {v0, v8}, Lcom/pspdfkit/internal/m1;->h(I)S

    move-result v9

    .line 325
    const-class v10, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v10}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/Enum;

    aget-object v9, v10, v9

    .line 326
    check-cast v9, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_11

    .line 327
    :cond_14
    invoke-virtual {v2, v14, v7}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 328
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->T()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    const/16 v6, 0x69

    invoke-virtual {v2, v6, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 329
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    if-eqz v0, :cond_15

    const/16 v16, 0x1

    goto :goto_12

    :cond_15
    const/16 v16, 0x0

    :goto_12
    if-eqz v16, :cond_20

    .line 330
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 332
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->I()S

    move-result v0

    .line 333
    const-class v4, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v4}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Enum;

    aget-object v0, v4, v0

    .line 334
    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 337
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 339
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/si;)Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    .line 340
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 341
    :pswitch_4
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 342
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 343
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->S()I

    move-result v7

    .line 344
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v9, 0x0

    :goto_13
    if-ge v9, v7, :cond_16

    .line 346
    invoke-virtual {v0, v9}, Lcom/pspdfkit/internal/m1;->j(I)Lcom/pspdfkit/internal/ln;

    move-result-object v10

    .line 347
    new-instance v11, Landroid/graphics/PointF;

    invoke-virtual {v10}, Lcom/pspdfkit/internal/ln;->a()F

    move-result v12

    invoke-virtual {v10}, Lcom/pspdfkit/internal/ln;->b()F

    move-result v10

    invoke-direct {v11, v12, v10}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_13

    .line 348
    :cond_16
    invoke-virtual {v2, v6, v8}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 349
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    if-eqz v0, :cond_17

    const/16 v16, 0x1

    goto :goto_14

    :cond_17
    const/16 v16, 0x0

    :goto_14
    if-eqz v16, :cond_20

    .line 350
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 352
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->I()S

    move-result v0

    .line 353
    const-class v4, Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    invoke-virtual {v4}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Enum;

    aget-object v0, v4, v0

    .line 354
    invoke-virtual {v2, v5, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 357
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 359
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->J()Lcom/pspdfkit/internal/si;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/si;)Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v0

    .line 360
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 361
    :pswitch_5
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->c0()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1772

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 362
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->d0()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1771

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 363
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v9, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 364
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->C()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v8, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Boolean;)V

    goto/16 :goto_1a

    .line 365
    :pswitch_6
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v9, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 366
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->Y()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x2711

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 367
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->b0()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x2712

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 368
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->Z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x2713

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 369
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 371
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->a0()I

    move-result v0

    .line 372
    const-class v3, Lcom/pspdfkit/annotations/sound/AudioEncoding;

    invoke-virtual {v3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Enum;

    aget-object v0, v3, v0

    const/16 v3, 0x2714

    .line 373
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 374
    :pswitch_7
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v9, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    goto/16 :goto_1a

    .line 375
    :pswitch_8
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v9, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 376
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->N()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/16 v3, 0xfa1

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Boolean;)V

    .line 377
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->A()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x11

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    goto/16 :goto_1a

    .line 378
    :pswitch_9
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 379
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->U()I

    move-result v3

    .line 380
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_15
    if-ge v5, v3, :cond_1a

    .line 382
    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/m1;->k(I)Lcom/pspdfkit/internal/io;

    move-result-object v6

    if-nez v6, :cond_18

    const/4 v6, 0x0

    goto :goto_16

    .line 383
    :cond_18
    new-instance v17, Lcom/pspdfkit/internal/ho;

    .line 384
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->e()F

    move-result v9

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->f()F

    move-result v10

    .line 385
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->g()F

    move-result v11

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->h()F

    move-result v12

    .line 386
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->a()F

    move-result v13

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->b()F

    move-result v14

    .line 387
    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->c()F

    move-result v15

    invoke-virtual {v6}, Lcom/pspdfkit/internal/io;->d()F

    move-result v16

    move-object/from16 v8, v17

    invoke-direct/range {v8 .. v16}, Lcom/pspdfkit/internal/ho;-><init>(FFFFFFFF)V

    move-object/from16 v6, v17

    :goto_16
    if-eqz v6, :cond_19

    .line 388
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_19
    add-int/lit8 v5, v5, 0x1

    goto :goto_15

    .line 389
    :cond_1a
    invoke-virtual {v2, v7, v4}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 390
    :pswitch_a
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->d()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b5a

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 391
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 392
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->e()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b5b

    .line 393
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 395
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->L()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x1b58

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 396
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 399
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->K()I

    move-result v0

    int-to-long v5, v0

    sget-object v0, Lcom/pspdfkit/annotations/actions/MediaOptions;->NO_FLAGS:Lcom/pspdfkit/annotations/actions/MediaOptions;

    .line 400
    const-class v3, Lcom/pspdfkit/annotations/actions/MediaOptions;

    .line 401
    invoke-static {v3}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v7

    const/4 v8, 0x0

    .line 402
    :goto_17
    invoke-virtual {v3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Enum;

    array-length v9, v9

    if-ge v8, v9, :cond_1c

    shl-int v9, v4, v8

    int-to-long v12, v9

    and-long/2addr v12, v5

    cmp-long v9, v12, v10

    if-eqz v9, :cond_1b

    .line 404
    invoke-virtual {v3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Enum;

    aget-object v9, v9, v8

    invoke-virtual {v7, v9}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    :cond_1b
    add-int/lit8 v8, v8, 0x1

    goto :goto_17

    :cond_1c
    if-eqz v0, :cond_1d

    .line 408
    invoke-virtual {v7}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 409
    invoke-virtual {v7, v0}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_1d
    invoke-virtual {v7}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v5, 0x0

    goto :goto_18

    :cond_1e
    move-object v5, v7

    :goto_18
    const/16 v0, 0x1b59

    .line 413
    invoke-virtual {v2, v0, v5}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 414
    :pswitch_b
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v13, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 415
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->w()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/16 v3, 0x3ea

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 416
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 418
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->x()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x3ec

    .line 419
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    goto/16 :goto_1a

    .line 420
    :pswitch_c
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->C()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v8, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Boolean;)V

    .line 421
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 422
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-static {v0}, Lcom/pspdfkit/internal/y0;->b(Lcom/pspdfkit/internal/m1;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v15, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 423
    :pswitch_d
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v13, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 424
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->w()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/16 v3, 0x3ea

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 425
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 427
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->x()Lcom/pspdfkit/internal/n5;

    move-result-object v0

    invoke-static {v0}, Lcom/pspdfkit/internal/nb;->a(Lcom/pspdfkit/internal/n5;)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x3ec

    .line 428
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 431
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->y()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Integer;)V

    .line 432
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->g0()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    const/16 v3, 0x3ed

    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 433
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 434
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->h0()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    const/16 v3, 0x3ee

    .line 435
    invoke-virtual {v2, v3, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 437
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->G()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v12, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 438
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    invoke-static {v0}, Lcom/pspdfkit/internal/y0;->b(Lcom/pspdfkit/internal/m1;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v15, v0}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 439
    iget-object v0, v1, Lcom/pspdfkit/internal/y0;->a:Lcom/pspdfkit/internal/m1;

    .line 440
    invoke-virtual {v0}, Lcom/pspdfkit/internal/m1;->E()I

    move-result v3

    .line 441
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_19
    if-ge v5, v3, :cond_1f

    .line 443
    invoke-virtual {v0, v5}, Lcom/pspdfkit/internal/m1;->h(I)S

    move-result v6

    .line 444
    const-class v7, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v7}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Enum;

    aget-object v6, v7, v6

    .line 445
    check-cast v6, Lcom/pspdfkit/annotations/LineEndType;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_19

    .line 446
    :cond_1f
    invoke-virtual {v2, v14, v4}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    :cond_20
    :goto_1a
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

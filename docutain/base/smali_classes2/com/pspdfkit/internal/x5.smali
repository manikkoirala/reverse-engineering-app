.class public final Lcom/pspdfkit/internal/x5;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/pspdfkit/internal/f2;

.field private static b:Lcom/pspdfkit/internal/ic;


# direct methods
.method public static a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I
    .locals 1

    .line 14
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 18
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/x5;->c()Lcom/pspdfkit/internal/ic;

    move-result-object p0

    iget p0, p0, Lcom/pspdfkit/internal/ic;->a:I

    return p0
.end method

.method public static a()Lcom/pspdfkit/internal/f2;
    .locals 2

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/x5;->a:Lcom/pspdfkit/internal/f2;

    if-eqz v0, :cond_0

    return-object v0

    .line 4
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Make sure to call ConfigurationUtils#parseThemeConfigurations() before calling getAnnotationThemeConfiguration()"

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/pspdfkit/configuration/PdfConfiguration;)Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/configuration/PdfConfiguration;",
            ")",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/PdfConfiguration;->getExcludedAnnotationTypes()Ljava/util/ArrayList;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 7
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object p0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->REDACTION:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 9
    sget-object p0, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 12
    const-class p0, Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {p0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object p0

    goto :goto_0

    .line 13
    :cond_1
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/f2;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/f2;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/pspdfkit/internal/x5;->a:Lcom/pspdfkit/internal/f2;

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/ic;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/ic;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/pspdfkit/internal/x5;->b:Lcom/pspdfkit/internal/ic;

    return-void
.end method

.method public static b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I
    .locals 1

    .line 9
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 13
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/x5;->c()Lcom/pspdfkit/internal/ic;

    move-result-object p0

    iget p0, p0, Lcom/pspdfkit/internal/ic;->f:I

    return p0
.end method

.method public static b()Ljava/lang/Integer;
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;->ACRO_FORMS:Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hb;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 7
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/x5;->c()Lcom/pspdfkit/internal/ic;

    move-result-object v0

    iget v0, v0, Lcom/pspdfkit/internal/ic;->b:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 8
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static c(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;
    .locals 2

    .line 3
    new-instance v0, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    invoke-direct {v0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;-><init>()V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->paperColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object v0

    .line 5
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/x5;->a(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->formHighlightColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object v0

    .line 6
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/x5;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->formRequiredFieldBorderColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object v0

    .line 7
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/x5;->d(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->signHereOverlayBackgroundColor(Ljava/lang/Integer;)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object p0

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isToGrayscale()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->toGrayscale(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object p0

    .line 9
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->isInvertColors()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->invertColors(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object p0

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/configuration/PdfConfiguration;->showSignHereOverlay()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->showSignHereOverlay(Z)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    move-result-object p0

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/x5;->b()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 14
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->formItemHighlightColor(I)Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration$Builder;->build()Lcom/pspdfkit/configuration/rendering/PageRenderConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public static c()Lcom/pspdfkit/internal/ic;
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/x5;->b:Lcom/pspdfkit/internal/ic;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Lcom/pspdfkit/exceptions/PSPDFKitException;

    const-string v1, "Make sure to call ConfigurationUtils#parseThemeConfigurations() before calling getFormSelectionThemeConfiguration()"

    invoke-direct {v0, v1}, Lcom/pspdfkit/exceptions/PSPDFKitException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)I
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->j()Lcom/pspdfkit/internal/hb;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/pspdfkit/internal/hb;->b(Lcom/pspdfkit/document/PdfDocument;Lcom/pspdfkit/configuration/PdfConfiguration;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 5
    :cond_0
    invoke-static {}, Lcom/pspdfkit/internal/x5;->c()Lcom/pspdfkit/internal/ic;

    move-result-object p0

    iget p0, p0, Lcom/pspdfkit/internal/ic;->g:I

    return p0
.end method

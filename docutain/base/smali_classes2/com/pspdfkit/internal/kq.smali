.class public final Lcom/pspdfkit/internal/kq;
.super Lcom/pspdfkit/internal/z5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/kq$a;,
        Lcom/pspdfkit/internal/kq$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/z5<",
        "Lcom/pspdfkit/internal/kq$a;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/pspdfkit/internal/kq$a;

.field private final f:Lkotlinx/serialization/KSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/serialization/KSerializer<",
            "Lcom/pspdfkit/internal/kq$a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lkotlinx/serialization/KSerializer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6

    const-string v0, "pathToSave"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textBlocksToSave"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/pspdfkit/internal/z5;-><init>()V

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;->SAVE_TO_DOCUMENT:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    iput-object v0, p0, Lcom/pspdfkit/internal/kq;->c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    .line 10
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " textblocks changed]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/kq;->d:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 43
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 44
    check-cast v1, Lcom/pspdfkit/internal/pt;

    .line 45
    new-instance v2, Lcom/pspdfkit/internal/kq$b;

    .line 46
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->a()Ljava/util/UUID;

    move-result-object v3

    .line 47
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v4

    invoke-virtual {v4}, Lcom/pspdfkit/internal/st;->a()Lcom/pspdfkit/internal/iv;

    move-result-object v4

    .line 48
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->i()Lcom/pspdfkit/internal/st;

    move-result-object v5

    invoke-virtual {v5}, Lcom/pspdfkit/internal/st;->c()Lcom/pspdfkit/internal/bd;

    move-result-object v5

    .line 49
    invoke-virtual {v1}, Lcom/pspdfkit/internal/pt;->g()Lcom/pspdfkit/internal/cb;

    move-result-object v1

    .line 50
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/pspdfkit/internal/kq$b;-><init>(Ljava/util/UUID;Lcom/pspdfkit/internal/iv;Lcom/pspdfkit/internal/bd;Lcom/pspdfkit/internal/cb;)V

    .line 71
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    :cond_0
    new-instance p2, Lcom/pspdfkit/internal/kq$a;

    invoke-direct {p2, p1, v0}, Lcom/pspdfkit/internal/kq$a;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/kq;->e:Lcom/pspdfkit/internal/kq$a;

    .line 90
    sget-object p1, Lcom/pspdfkit/internal/kq$a;->Companion:Lcom/pspdfkit/internal/kq$a$b;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/kq$a$b;->serializer()Lkotlinx/serialization/KSerializer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/kq;->f:Lkotlinx/serialization/KSerializer;

    .line 91
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->serializer(Lkotlin/Unit;)Lkotlinx/serialization/KSerializer;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/kq;->g:Lkotlinx/serialization/KSerializer;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->e:Lcom/pspdfkit/internal/kq$a;

    return-object v0
.end method

.method public final c()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->f:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

.method public final d()Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->c:Lcom/pspdfkit/internal/jni/NativeContentEditingCommand;

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/q6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/pspdfkit/internal/q6<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->g:Lkotlinx/serialization/KSerializer;

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/kq$c;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/kq$c;-><init>(Lkotlinx/serialization/KSerializer;)V

    return-object v1
.end method

.method public final g()Lkotlinx/serialization/KSerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/kq;->g:Lkotlinx/serialization/KSerializer;

    return-object v0
.end method

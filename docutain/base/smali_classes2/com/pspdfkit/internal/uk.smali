.class public final Lcom/pspdfkit/internal/uk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ok;
.implements Lcom/pspdfkit/internal/nk;


# instance fields
.field private final a:Lcom/pspdfkit/internal/mk;

.field private b:Lcom/pspdfkit/internal/qk;

.field private c:Z

.field private d:Lio/reactivex/rxjava3/disposables/Disposable;


# direct methods
.method public static synthetic $r8$lambda$69r7eAmHq4DMOt4hcCm-j9B8Zk8(Lcom/pspdfkit/internal/uk;Lcom/pspdfkit/internal/qk;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/qk;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/mk;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uk;->c:Z

    .line 9
    iput-object p1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/ik;)V
    .locals 2

    .line 174
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 175
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    sget-object v1, Lcom/pspdfkit/internal/kk;->b:Lcom/pspdfkit/internal/kk;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1, p1}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/ik;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    sget-object v1, Lcom/pspdfkit/internal/kk;->d:Lcom/pspdfkit/internal/kk;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 182
    sget-object v1, Lcom/pspdfkit/internal/kk;->c:Lcom/pspdfkit/internal/kk;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_2
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/ik;->a(Ljava/util/HashSet;)V

    return-void
.end method

.method private a(Lcom/pspdfkit/internal/qk;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 67
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ik;

    .line 68
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;)V

    goto :goto_0

    .line 69
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ik;

    .line 70
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 71
    :cond_1
    check-cast p1, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p1, p2, v1}, Lcom/pspdfkit/internal/cl;->a(Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/pspdfkit/internal/yk;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_0

    .line 74
    new-instance v1, Lcom/pspdfkit/internal/yk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->e()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/yk;-><init>(Z)V

    return-object v1

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to fetch the presenter state while not subscribed to the view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)V
    .locals 7

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-nez v0, :cond_0

    return-void

    .line 137
    :cond_0
    check-cast v0, Lcom/pspdfkit/internal/cl;

    .line 138
    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 139
    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->a()V

    .line 141
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cl;->setStyleBoxSelectedColor(I)V

    .line 142
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_2

    const v1, 0x3e4ccccd    # 0.2f

    .line 143
    invoke-static {p1, v1}, Lcom/pspdfkit/internal/s5;->a(IF)I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/cl;

    .line 144
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/16 v1, 0x12c

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    .line 146
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 147
    :cond_1
    new-instance v5, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    aput-object v3, v6, v4

    const/4 v3, 0x1

    aput-object v2, v6, v3

    invoke-direct {v5, v6}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 148
    invoke-virtual {v5, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 149
    invoke-static {v0, v5}, Landroidx/core/view/ViewCompat;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 150
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, p1, v3}, Lcom/pspdfkit/internal/cl;->a(IZ)V

    .line 151
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    invoke-static {p1}, Lcom/pspdfkit/internal/s5;->d(I)I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setToolbarForegroundColor(I)V

    .line 152
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cl;->setStatusBarColor(I)V

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/mk;->a(I)V

    .line 155
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    .line 156
    instance-of v1, v0, Lcom/pspdfkit/internal/hk;

    if-eqz v1, :cond_3

    .line 157
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    check-cast v0, Lcom/pspdfkit/internal/hk;

    invoke-interface {v1, v0, p1}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/hk;I)V

    :cond_3
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/cl;Lcom/pspdfkit/internal/pk;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    .line 3
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/cl;->setPresenter(Lcom/pspdfkit/internal/ok;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/cl;->setToolbarTitle(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->q()I

    move-result v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v1, :cond_0

    const v2, 0x3e4ccccd    # 0.2f

    .line 9
    invoke-static {v0, v2}, Lcom/pspdfkit/internal/s5;->a(IF)I

    move-result v2

    check-cast v1, Lcom/pspdfkit/internal/cl;

    .line 10
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v1, Lcom/pspdfkit/internal/cl;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/pspdfkit/internal/cl;->a(IZ)V

    .line 12
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    invoke-static {v0}, Lcom/pspdfkit/internal/s5;->d(I)I

    move-result v2

    check-cast v1, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/cl;->setToolbarForegroundColor(I)V

    .line 13
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v1, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/cl;->setStatusBarColor(I)V

    :cond_0
    const/4 v1, 0x1

    .line 14
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/cl;->b(I)V

    const/4 v1, 0x2

    .line 15
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/cl;->b(I)V

    .line 16
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    .line 17
    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->b()Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/cl;->a(Z)V

    .line 20
    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->p()Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/cl;->setAddNewReplyBoxDisplayed(Z)V

    .line 23
    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->j()Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/cl;->setStyleBoxDisplayed(Z)V

    .line 25
    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->r()Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 26
    :goto_0
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/cl;->setStyleBoxPickerColors(Ljava/util/List;)V

    .line 29
    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->o()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->e()Ljava/util/List;

    move-result-object v1

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    :goto_1
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/cl;->setStyleBoxPickerIcons(Ljava/util/List;)V

    .line 31
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1}, Lcom/pspdfkit/internal/mk;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 33
    invoke-static {v1}, Lcom/pspdfkit/internal/ao;->a(Ljava/lang/String;)I

    move-result v2

    .line 34
    invoke-virtual {p1, v1}, Lcom/pspdfkit/internal/cl;->setStyleBoxSelectedIcon(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/cl;->setStyleBoxSelectedColor(I)V

    .line 36
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/cl;->setStyleBoxText(I)V

    .line 39
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0, p0}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/uk;)V

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->l()Z

    move-result v0

    if-nez v0, :cond_4

    .line 46
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    .line 47
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;)V

    .line 49
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 50
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 51
    invoke-virtual {p1, v1, v0}, Lcom/pspdfkit/internal/cl;->a(Ljava/util/List;Z)V

    goto :goto_2

    .line 54
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->u()Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 55
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/uk$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/pspdfkit/internal/uk$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/uk;Lcom/pspdfkit/internal/qk;)V

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/uk;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :goto_2
    if-eqz p2, :cond_5

    .line 65
    invoke-virtual {p1}, Lcom/pspdfkit/internal/cl;->g()V

    .line 66
    check-cast p2, Lcom/pspdfkit/internal/yk;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/yk;->a()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/cl;->setStyleBoxExpanded(Z)V

    :cond_5
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/kk;)V
    .locals 1

    .line 158
    sget-object v0, Lcom/pspdfkit/internal/kk;->d:Lcom/pspdfkit/internal/kk;

    if-ne p2, v0, :cond_0

    .line 159
    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {p2, p1}, Lcom/pspdfkit/internal/mk;->b(Lcom/pspdfkit/internal/ik;)Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz p2, :cond_3

    .line 160
    check-cast p2, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/cl;->b(Lcom/pspdfkit/internal/ik;)V

    goto :goto_0

    .line 162
    :cond_0
    sget-object v0, Lcom/pspdfkit/internal/kk;->b:Lcom/pspdfkit/internal/kk;

    if-ne p2, v0, :cond_2

    .line 163
    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz p2, :cond_3

    .line 164
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, ""

    .line 166
    :cond_1
    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast p2, Lcom/pspdfkit/internal/cl;

    .line 167
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 168
    invoke-static {p2, p1}, Lcom/pspdfkit/document/sharing/DocumentSharingManager;->shareText(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_2
    sget-object v0, Lcom/pspdfkit/internal/kk;->c:Lcom/pspdfkit/internal/kk;

    if-ne p2, v0, :cond_3

    .line 170
    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz p2, :cond_3

    .line 171
    check-cast p2, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/cl;->c(Lcom/pspdfkit/internal/ik;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/lk;)V
    .locals 5

    .line 76
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 78
    :cond_0
    sget-object p2, Lcom/pspdfkit/annotations/note/AuthorState;->NONE:Lcom/pspdfkit/annotations/note/AuthorState;

    goto :goto_0

    .line 90
    :cond_1
    sget-object p2, Lcom/pspdfkit/annotations/note/AuthorState;->COMPLETED:Lcom/pspdfkit/annotations/note/AuthorState;

    goto :goto_0

    .line 91
    :cond_2
    sget-object p2, Lcom/pspdfkit/annotations/note/AuthorState;->CANCELLED:Lcom/pspdfkit/annotations/note/AuthorState;

    goto :goto_0

    .line 92
    :cond_3
    sget-object p2, Lcom/pspdfkit/annotations/note/AuthorState;->REJECTED:Lcom/pspdfkit/annotations/note/AuthorState;

    goto :goto_0

    .line 93
    :cond_4
    sget-object p2, Lcom/pspdfkit/annotations/note/AuthorState;->ACCEPTED:Lcom/pspdfkit/annotations/note/AuthorState;

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    move-object v1, p1

    check-cast v1, Lcom/pspdfkit/internal/hk;

    new-instance v2, Lcom/pspdfkit/annotations/note/AnnotationStateChange;

    .line 109
    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->i()Ljava/lang/String;

    move-result-object v3

    .line 111
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-direct {v2, v3, p2, v4}, Lcom/pspdfkit/annotations/note/AnnotationStateChange;-><init>(Ljava/lang/String;Lcom/pspdfkit/annotations/note/AuthorState;Ljava/util/Date;)V

    .line 112
    invoke-interface {v0, v1, v2}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/hk;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    .line 119
    iget-object p2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz p2, :cond_5

    .line 120
    check-cast p2, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/cl;->d(Lcom/pspdfkit/internal/ik;)V

    :cond_5
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V
    .locals 1

    .line 172
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0, p1, p2}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/mk;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_2

    .line 186
    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->b()Z

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->a(Z)V

    .line 189
    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setAddNewReplyBoxDisplayed(Z)V

    .line 192
    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setStyleBoxDisplayed(Z)V

    .line 194
    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->r()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 195
    :goto_0
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setStyleBoxPickerColors(Ljava/util/List;)V

    .line 198
    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lcom/pspdfkit/internal/mk;->e()Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 199
    :goto_1
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cl;->setStyleBoxPickerIcons(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_0

    .line 122
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cl;->setStyleBoxSelectedIcon(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    invoke-static {p1}, Lcom/pspdfkit/internal/ao;->a(Ljava/lang/String;)I

    move-result v1

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setStyleBoxText(I)V

    .line 124
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 125
    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 126
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->a()V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/mk;->a(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    .line 132
    instance-of v1, v0, Lcom/pspdfkit/internal/hk;

    if-eqz v1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    check-cast v0, Lcom/pspdfkit/internal/hk;

    invoke-interface {v1, v0, p1}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/hk;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz p1, :cond_0

    .line 3
    check-cast p1, Lcom/pspdfkit/internal/cl;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/cl;->h()V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/internal/ik;)V
    .locals 1

    .line 4
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->k()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/ik;->a(Z)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_0

    .line 6
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/cl;->d(Lcom/pspdfkit/internal/ik;)V

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    new-instance v2, Lcom/pspdfkit/internal/uk$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/pspdfkit/internal/uk$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/uk;)V

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/cl;->a(Ljava/lang/Runnable;)V

    return v1

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->n()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    .line 11
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;)V

    .line 12
    iget-object v2, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v2, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v2, v0}, Lcom/pspdfkit/internal/cl;->a(Lcom/pspdfkit/internal/ik;)V

    return v1
.end method

.method public final d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->s()V

    return-void
.end method

.method public final e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->f()Lcom/pspdfkit/internal/ik;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->e()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->FREETEXT:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 9
    iput-boolean v1, p0, Lcom/pspdfkit/internal/uk;->c:Z

    .line 13
    invoke-interface {v0}, Lcom/pspdfkit/internal/ik;->e()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->NOTE:Lcom/pspdfkit/annotations/AnnotationType;

    if-ne v1, v2, :cond_1

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/mk;->b(Lcom/pspdfkit/internal/ik;)Z

    goto :goto_0

    .line 16
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V

    .line 17
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/mk;->c(Lcom/pspdfkit/internal/ik;)V

    .line 18
    iget-object v1, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v1, v0}, Lcom/pspdfkit/internal/mk;->d(Lcom/pspdfkit/internal/ik;)V

    .line 21
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_2

    .line 22
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->b()V

    :cond_2
    return-void
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->c()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0}, Lcom/pspdfkit/internal/mk;->m()V

    return-void
.end method

.method public final g()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_1

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->a()V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 5
    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->i()V

    :cond_1
    return-void
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_0

    .line 2
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->b()V

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/mk;->a(Lcom/pspdfkit/internal/uk;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    if-eqz v0, :cond_3

    .line 4
    iget-boolean v2, p0, Lcom/pspdfkit/internal/uk;->c:Z

    if-nez v2, :cond_2

    .line 5
    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/cl;->getNoteEditorContentCards()Ljava/util/List;

    move-result-object v0

    .line 6
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/ik;

    .line 9
    invoke-interface {v3}, Lcom/pspdfkit/internal/ik;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 10
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->a:Lcom/pspdfkit/internal/mk;

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/mk;->a(Ljava/util/List;)V

    .line 16
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    check-cast v0, Lcom/pspdfkit/internal/cl;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/cl;->setPresenter(Lcom/pspdfkit/internal/ok;)V

    .line 17
    iput-object v1, p0, Lcom/pspdfkit/internal/uk;->b:Lcom/pspdfkit/internal/qk;

    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/pspdfkit/internal/uk;->c:Z

    .line 20
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/uk;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    if-eqz v0, :cond_4

    .line 21
    invoke-interface {v0}, Lio/reactivex/rxjava3/disposables/Disposable;->dispose()V

    .line 22
    iput-object v1, p0, Lcom/pspdfkit/internal/uk;->d:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_4
    return-void
.end method

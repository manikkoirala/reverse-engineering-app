.class public final Lcom/pspdfkit/internal/zg;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/zg$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lio/reactivex/rxjava3/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/ReplaySubject<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lio/reactivex/rxjava3/core/Scheduler;

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->create(I)Lio/reactivex/rxjava3/subjects/ReplaySubject;

    move-result-object v0

    const-string v1, "create(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    .line 7
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/rxjava3/schedulers/Schedulers;->from(Ljava/util/concurrent/Executor;)Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v0

    const-string v1, "from(Executors.newSingleThreadExecutor())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/zg;->b:Lio/reactivex/rxjava3/core/Scheduler;

    .line 16
    new-instance v0, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/zg;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 18
    invoke-virtual {p0}, Lcom/pspdfkit/internal/zg;->e()Z

    move-result v0

    const/4 v1, 0x0

    .line 19
    iput-object v1, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    .line 20
    iget-object v1, p0, Lcom/pspdfkit/internal/zg;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->clear()V

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onComplete()V

    :cond_0
    const/4 v0, 0x1

    .line 24
    invoke-static {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->create(I)Lio/reactivex/rxjava3/subjects/ReplaySubject;

    move-result-object v0

    const-string v1, "create(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zg$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zg$a<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "function"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/zg;->a(Lcom/pspdfkit/internal/zg$a;Z)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zg$a;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/internal/zg$a<",
            "TT;>;Z)V"
        }
    .end annotation

    const-string v0, "function"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 9
    iget-object p2, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {p2}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->hasObservers()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/internal/u;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/u;->d()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 10
    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/zg$a;->apply(Ljava/lang/Object;)V

    goto :goto_0

    .line 12
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/zg;->d:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->firstElement()Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/zg;->b:Lio/reactivex/rxjava3/core/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 15
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Maybe;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/pspdfkit/internal/zg$b;

    invoke-direct {v1, p1}, Lcom/pspdfkit/internal/zg$b;-><init>(Lcom/pspdfkit/internal/zg$a;)V

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Maybe;->subscribeWith(Lio/reactivex/rxjava3/core/MaybeObserver;)Lio/reactivex/rxjava3/core/MaybeObserver;

    move-result-object p1

    check-cast p1, Lio/reactivex/rxjava3/disposables/Disposable;

    .line 17
    invoke-virtual {p2, p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;->add(Lio/reactivex/rxjava3/disposables/Disposable;)Z

    :goto_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "target"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->hasComplete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {p1}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onComplete()V

    :cond_1
    return-void
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final c()Lio/reactivex/rxjava3/core/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Single<",
            "TT;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 3
    invoke-static {v0}, Lio/reactivex/rxjava3/core/Single;->just(Ljava/lang/Object;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    const-string v1, "{\n            Single.just(lazyObject)\n        }"

    .line 4
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->a:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->firstOrError()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/zg;->b:Lio/reactivex/rxjava3/core/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 9
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    const-string v1, "{\n            tasksSubje\u2026s.mainThread())\n        }"

    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "lazy object was null"

    .line 2
    invoke-static {v2, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/String;Z)V

    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/zg;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

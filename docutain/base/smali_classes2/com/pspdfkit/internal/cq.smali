.class public final Lcom/pspdfkit/internal/cq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/cq$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/z1$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/z1$b;",
            ">;"
        }
    .end annotation
.end field

.field public static final synthetic c:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x4

    new-array v1, v0, [Lcom/pspdfkit/internal/z1$b;

    .line 1
    sget-object v2, Lcom/pspdfkit/internal/z1$b;->a:Lcom/pspdfkit/internal/z1$b;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 2
    sget-object v2, Lcom/pspdfkit/internal/z1$b;->c:Lcom/pspdfkit/internal/z1$b;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 3
    sget-object v2, Lcom/pspdfkit/internal/z1$b;->h:Lcom/pspdfkit/internal/z1$b;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    .line 4
    sget-object v2, Lcom/pspdfkit/internal/z1$b;->f:Lcom/pspdfkit/internal/z1$b;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    .line 5
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/pspdfkit/internal/cq;->a:Ljava/util/List;

    new-array v0, v0, [Lcom/pspdfkit/internal/z1$b;

    .line 15
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->b:Lcom/pspdfkit/internal/z1$b;

    aput-object v1, v0, v3

    .line 16
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->e:Lcom/pspdfkit/internal/z1$b;

    aput-object v1, v0, v4

    .line 17
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->g:Lcom/pspdfkit/internal/z1$b;

    aput-object v1, v0, v5

    .line 18
    sget-object v1, Lcom/pspdfkit/internal/z1$b;->d:Lcom/pspdfkit/internal/z1$b;

    aput-object v1, v0, v6

    .line 19
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/cq;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/cq;->a:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic b()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/cq;->b:Ljava/util/List;

    return-object v0
.end method

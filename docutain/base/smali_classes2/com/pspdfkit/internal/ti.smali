.class public final Lcom/pspdfkit/internal/ti;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/Matrix;

.field private final b:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

.field private final c:Lcom/pspdfkit/internal/jni/NativePDFSnapper;

.field private final d:I

.field private e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/pspdfkit/internal/zf;Landroid/graphics/Matrix;Lcom/pspdfkit/preferences/PSPDFKitPreferences;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "document"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pdfToViewTransformation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pspdfKitPreferences"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p4, p0, Lcom/pspdfkit/internal/ti;->a:Landroid/graphics/Matrix;

    .line 7
    iput-object p5, p0, Lcom/pspdfkit/internal/ti;->b:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p4, Lcom/pspdfkit/R$dimen;->pspdf__measurement_snapping_threshold:I

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/ti;->d:I

    .line 21
    invoke-virtual {p3}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/jni/NativeDocument;->getPage(I)Lcom/pspdfkit/internal/jni/NativePage;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 23
    invoke-static {p1}, Lcom/pspdfkit/internal/jni/NativePDFSnapper;->create(Lcom/pspdfkit/internal/jni/NativePage;)Lcom/pspdfkit/internal/jni/NativePDFSnapper;

    move-result-object p1

    const-string p2, "create(nativePage)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/ti;->c:Lcom/pspdfkit/internal/jni/NativePDFSnapper;

    return-void

    .line 24
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Measurement snapper could not get page "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " from document."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4

    const-string v0, "pdfPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ti;->b:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "pspdfKitPreferences.isMeasurementSnappingEnabled"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/ti;->e:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return-object p1

    .line 11
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/ti;->c:Lcom/pspdfkit/internal/jni/NativePDFSnapper;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/jni/NativePDFSnapper;->snap(Landroid/graphics/PointF;)Lcom/pspdfkit/internal/jni/NativeSnapResult;

    move-result-object v0

    const-string v2, "corePdfSnapper.snap(pdfPoint)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeSnapResult;->getHasError()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 13
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeSnapResult;->getError()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Measurement tools: Couldn\'t snap point "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.Annotations"

    invoke-static {v2, v0, v1}, Lcom/pspdfkit/utils/PdfLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    .line 16
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeSnapResult;->getSnapPoint()Lcom/pspdfkit/internal/jni/NativeSnapPoint;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/pspdfkit/internal/jni/NativeSnapPoint;->getPoint()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_4

    return-object p1

    :cond_4
    return-object v0
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 3

    const-string v0, "pdfToViewMatrix"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/ti;->a:Landroid/graphics/Matrix;

    .line 3
    iget v0, p0, Lcom/pspdfkit/internal/ti;->d:I

    int-to-float v0, v0

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(FLandroid/graphics/Matrix;)F

    move-result p1

    .line 4
    iget v0, p0, Lcom/pspdfkit/internal/ti;->e:F

    cmpg-float v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 5
    iput p1, p0, Lcom/pspdfkit/internal/ti;->e:F

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/ti;->c:Lcom/pspdfkit/internal/jni/NativePDFSnapper;

    new-instance v0, Lcom/pspdfkit/internal/jni/NativeSnapperConfiguration;

    .line 7
    new-instance v1, Lcom/pspdfkit/utils/Size;

    iget v2, p0, Lcom/pspdfkit/internal/ti;->e:F

    invoke-direct {v1, v2, v2}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    .line 8
    const-class v2, Lcom/pspdfkit/internal/jni/NativeSnapPointType;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 9
    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/internal/jni/NativeSnapperConfiguration;-><init>(Lcom/pspdfkit/utils/Size;Ljava/util/EnumSet;)V

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/jni/NativePDFSnapper;->setConfiguration(Lcom/pspdfkit/internal/jni/NativeSnapperConfiguration;)V

    :cond_1
    return-void
.end method

.method public final b(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 2

    const-string v0, "viewPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ti;->b:Lcom/pspdfkit/preferences/PSPDFKitPreferences;

    invoke-virtual {v0}, Lcom/pspdfkit/preferences/PSPDFKitPreferences;->isMeasurementSnappingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "pspdfKitPreferences.isMeasurementSnappingEnabled"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/ti;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return-object p1

    .line 2
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/ti;->a:Landroid/graphics/Matrix;

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 7
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/ti;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object p1

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/ti;->a:Landroid/graphics/Matrix;

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    return-object p1
.end method

.class public final Lcom/pspdfkit/internal/om;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/lg;


# instance fields
.field private final a:Lcom/pspdfkit/internal/ui/f;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/ui/f;)V
    .locals 1

    const-string v0, "pdfUiImpl"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/om;->a:Lcom/pspdfkit/internal/ui/f;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/jg;
    .locals 1

    const-string v0, "title"

    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "message"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public final a()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final a(II)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final a(Lcom/pspdfkit/internal/kg;)Z
    .locals 1

    const-string v0, "jsMailParams"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public final a(Lcom/pspdfkit/internal/mg;)Z
    .locals 1

    const-string v0, "jsPrintParams"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/mg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object p1, p0, Lcom/pspdfkit/internal/om;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ui/f;->showPrintDialog()V

    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/om;->a:Lcom/pspdfkit/internal/ui/f;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/mg;->a()Lcom/pspdfkit/document/printing/PrintOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ui/f;->performPrint(Lcom/pspdfkit/document/printing/PrintOptions;)V

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "url"

    .line 1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.class final Lcom/pspdfkit/internal/pl$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/appcompat/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/pl;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/rxjava3/subjects/PublishSubject;

.field final synthetic b:Lcom/pspdfkit/internal/pl;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/pl;Lio/reactivex/rxjava3/subjects/PublishSubject;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/pl$a;->b:Lcom/pspdfkit/internal/pl;

    iput-object p2, p0, Lcom/pspdfkit/internal/pl$a;->a:Lio/reactivex/rxjava3/subjects/PublishSubject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onQueryTextChange(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl$a;->a:Lio/reactivex/rxjava3/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/pl$a;->b:Lcom/pspdfkit/internal/pl;

    invoke-static {p1}, Lcom/pspdfkit/internal/pl;->-$$Nest$fgetg(Lcom/pspdfkit/internal/pl;)Landroidx/appcompat/widget/SearchView;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/widget/SearchView;->clearFocus()V

    const/4 p1, 0x1

    return p1
.end method

.class final Lcom/pspdfkit/internal/zu$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/zu;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/zu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/zu$e;->a:Lcom/pspdfkit/internal/zu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "Error: "

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.VideoView"

    invoke-static {v1, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$e;->a:Lcom/pspdfkit/internal/zu;

    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputc(Lcom/pspdfkit/internal/zu;I)V

    .line 3
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/zu;->-$$Nest$fputd(Lcom/pspdfkit/internal/zu;I)V

    .line 4
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgeti(Lcom/pspdfkit/internal/zu;)Lcom/pspdfkit/internal/yu;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/yu;->f()V

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/zu$e;->a:Lcom/pspdfkit/internal/zu;

    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgett(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 10
    invoke-static {p1}, Lcom/pspdfkit/internal/zu;->-$$Nest$fgetf(Lcom/pspdfkit/internal/zu;)Landroid/media/MediaPlayer;

    move-result-object p1

    invoke-interface {v0, p1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    :cond_1
    return v1
.end method

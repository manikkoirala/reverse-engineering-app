.class public final Lcom/pspdfkit/internal/cp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/cp;->a:Z

    .line 3
    iput-boolean p2, p0, Lcom/pspdfkit/internal/cp;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/ui/c;)Lcom/pspdfkit/internal/ap;
    .locals 1

    const-string v0, "pdfActivityUserInterfaceCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/cp;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/pspdfkit/internal/cp;->b:Z

    if-eqz v0, :cond_0

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/bp;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/bp;-><init>(Lcom/pspdfkit/internal/ui/c;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

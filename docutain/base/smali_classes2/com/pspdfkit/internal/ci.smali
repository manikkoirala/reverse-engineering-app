.class public final Lcom/pspdfkit/internal/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/annotations/AnnotationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/pspdfkit/annotations/UnderlineAnnotation;


# direct methods
.method public static synthetic $r8$lambda$r4_OilQjUcB60tIxoUFcAbbFQiw(Lcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 1
    sget-object v0, Lcom/pspdfkit/annotations/AnnotationType;->REDACT:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v1, Lcom/pspdfkit/annotations/AnnotationType;->HIGHLIGHT:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v2, Lcom/pspdfkit/annotations/AnnotationType;->SQUIGGLY:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v3, Lcom/pspdfkit/annotations/AnnotationType;->STRIKEOUT:Lcom/pspdfkit/annotations/AnnotationType;

    sget-object v4, Lcom/pspdfkit/annotations/AnnotationType;->UNDERLINE:Lcom/pspdfkit/annotations/AnnotationType;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/pspdfkit/internal/ci;->a:Ljava/util/EnumSet;

    .line 8
    new-instance v0, Lcom/pspdfkit/annotations/UnderlineAnnotation;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/pspdfkit/annotations/UnderlineAnnotation;-><init>(ILjava/util/List;)V

    sput-object v0, Lcom/pspdfkit/internal/ci;->b:Lcom/pspdfkit/annotations/UnderlineAnnotation;

    return-void
.end method

.method public static a(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;
    .locals 2

    .line 1
    sget-object v0, Lcom/pspdfkit/internal/ci;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p2}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    sget-object v0, Lcom/pspdfkit/internal/ci;->b:Lcom/pspdfkit/annotations/UnderlineAnnotation;

    const-string v1, "com.pspdfkit.internal.annotations.markup.default-rect-name"

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setName(Ljava/lang/String;)V

    .line 8
    invoke-interface {p0}, Lcom/pspdfkit/document/PdfDocument;->getAnnotationProvider()Lcom/pspdfkit/annotations/AnnotationProvider;

    move-result-object p0

    .line 9
    invoke-interface {p0, p1}, Lcom/pspdfkit/annotations/AnnotationProvider;->getAnnotationsAsync(I)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    new-instance p1, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda0;

    invoke-direct {p1}, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda0;-><init>()V

    .line 10
    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Observable;->flatMap(Lio/reactivex/rxjava3/functions/Function;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    new-instance p1, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda1;

    invoke-direct {p1, p2, p3, p4, p5}, Lcom/pspdfkit/internal/ci$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;)V

    .line 11
    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Observable;->filter(Lio/reactivex/rxjava3/functions/Predicate;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    .line 26
    const-class p1, Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    invoke-virtual {p0, p1}, Lio/reactivex/rxjava3/core/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p0

    .line 27
    invoke-virtual {p0, v0}, Lio/reactivex/rxjava3/core/Observable;->blockingFirst(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    return-object p0

    .line 28
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    const/4 p2, 0x1

    aput-object v0, p1, p2

    const-string p2, "The passed annotation type (%s) is not a markup annotation (%s)"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/pspdfkit/annotations/BaseRectsAnnotation;",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->getRects()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 41
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 42
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 43
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 44
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 46
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 47
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 48
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 49
    invoke-virtual {v0}, Landroid/graphics/RectF;->sort()V

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    .line 50
    :goto_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_7

    .line 51
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    add-int/lit8 v2, p1, 0x1

    .line 52
    :goto_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 53
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 54
    invoke-virtual {v3, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v4

    if-eqz v4, :cond_4

    add-int/lit8 v0, p1, -0x1

    .line 55
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move p1, v0

    goto :goto_5

    .line 57
    :cond_4
    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 58
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    :goto_5
    add-int/lit8 p1, p1, 0x1

    goto :goto_3

    .line 59
    :cond_7
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_8
    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 60
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v4, v2, v3

    if-lez v4, :cond_9

    .line 62
    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 63
    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 65
    :cond_9
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v4, v2, v3

    if-lez v4, :cond_8

    .line 67
    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 68
    iput v2, v0, Landroid/graphics/RectF;->top:F

    goto :goto_6

    .line 69
    :cond_a
    invoke-virtual {p0, v1}, Lcom/pspdfkit/annotations/BaseRectsAnnotation;->setRects(Ljava/util/List;)V

    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 29
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    const/4 v1, 0x0

    if-eq v0, p0, :cond_0

    return v1

    .line 30
    :cond_0
    check-cast p4, Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 31
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result p0

    if-eq p0, p1, :cond_1

    return v1

    .line 32
    :cond_1
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result p0

    cmpl-float p0, p0, p2

    if-eqz p0, :cond_2

    return v1

    .line 33
    :cond_2
    invoke-virtual {p4}, Lcom/pspdfkit/annotations/Annotation;->getBoundingBox()Landroid/graphics/RectF;

    move-result-object p0

    .line 34
    invoke-virtual {p0}, Landroid/graphics/RectF;->sort()V

    .line 35
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 36
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/graphics/RectF;

    .line 37
    invoke-virtual {p1, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 38
    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 39
    invoke-static {p1, p0}, Landroid/graphics/RectF;->intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result p3

    if-eqz p3, :cond_3

    const/4 p0, 0x1

    return p0

    :cond_4
    return v1
.end method

.class public final Lcom/pspdfkit/internal/ul;
.super Lcom/pspdfkit/internal/en;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/ui/overlay/OverlayViewProvider$OverlayViewProviderObserver;


# static fields
.field private static final i:I


# instance fields
.field private final e:Lcom/pspdfkit/internal/dm;

.field private f:Lcom/pspdfkit/internal/dm$e;

.field private g:Z

.field private final h:Ljava/util/LinkedHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__tag_key_overlay_provider:I

    sput v0, Lcom/pspdfkit/internal/ul;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/dm;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/en;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    .line 13
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private final a(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 7

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    invoke-virtual {p2, v0, v1, v2}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->getViewsForPage(Landroid/content/Context;Lcom/pspdfkit/document/PdfDocument;I)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 4
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-nez v2, :cond_1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    const-string v4, "format(format, *args)"

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v3, :cond_3

    .line 10
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    instance-of v3, v3, Lcom/pspdfkit/ui/overlay/OverlayLayoutParams;

    if-eqz v3, :cond_2

    .line 14
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 15
    sget v3, Lcom/pspdfkit/internal/ul;->i:I

    invoke-virtual {v2, v3, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 16
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object p2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array p2, v6, [Ljava/lang/Object;

    aput-object v2, p2, v5

    invoke-static {p2, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string v0, "You need to set OverlayLayoutParams on the view before returning it. (%s)"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 17
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object p2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array p2, v6, [Ljava/lang/Object;

    aput-object v2, p2, v5

    invoke-static {p2, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string v0, "You can\'t add views that already have a parent. (%s)"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 26
    :cond_4
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ul;->g:Z

    if-eqz v1, :cond_5

    .line 27
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    invoke-virtual {p2, p1, v0}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->onViewsShown(ILjava/util/List;)V

    .line 29
    :cond_5
    iget-object p1, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final b()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/u;

    const-string v1, "Overlay views touched from non-main thread."

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 5
    invoke-virtual {v2, p0}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->removeOverlayViewProviderObserver(Lcom/pspdfkit/ui/overlay/OverlayViewProvider$OverlayViewProviderObserver;)V

    .line 6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 7
    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 9
    :cond_0
    iget-object v3, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v3

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v2, v3, v1}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->onViewsRecycled(ILjava/util/List;)V

    goto :goto_0

    .line 12
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    const-string v0, "parent.getPdfToViewTransformation(reuse)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/pspdfkit/internal/dm$e;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public final a(Z)V
    .locals 5

    .line 38
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 39
    :cond_0
    iget-boolean v1, p0, Lcom/pspdfkit/internal/ul;->g:Z

    if-eq p1, v1, :cond_2

    .line 40
    iget-object v1, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz p1, :cond_1

    .line 42
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->onViewsShown(ILjava/util/List;)V

    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->onViewsHidden(ILjava/util/List;)V

    goto :goto_0

    .line 48
    :cond_2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/ul;->g:Z

    return-void
.end method

.method public final c()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/en;->a()V

    return-void
.end method

.method public getPdfRect()Landroid/graphics/RectF;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getPdfRect()Landroid/graphics/RectF;

    move-result-object v0

    const-string v1, "parent.pdfRect"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getZoomScale()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getZoomScale()F

    move-result v0

    return v0
.end method

.method public final onOverlayViewsChanged(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V
    .locals 4

    const-string v0, "overlayViewProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/ul;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 7
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 8
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 10
    :cond_2
    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v2

    invoke-virtual {p1, v2, v1}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->onViewsRecycled(ILjava/util/List;)V

    .line 12
    invoke-direct {p0, v0, p1}, Lcom/pspdfkit/internal/ul;->a(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 15
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->measure(II)V

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public final onOverlayViewsChanged(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;I)V
    .locals 1

    const-string v0, "overlayViewProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_1

    return-void

    .line 2
    :cond_1
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/ul;->onOverlayViewsChanged(Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    return-void
.end method

.method public final recycle()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->e:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/ul;->b()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    return-void
.end method

.method public final setCurrentOverlayViewProviders(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/pspdfkit/ui/overlay/OverlayViewProvider;",
            ">;)V"
        }
    .end annotation

    const-string v0, "overlayViewProviders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ul;->f:Lcom/pspdfkit/internal/dm$e;

    if-eqz v0, :cond_1

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/u;

    const-string v2, "Overlay views touched from non-main thread."

    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/u;->b(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lcom/pspdfkit/internal/ul;->b()V

    .line 5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;

    .line 6
    invoke-virtual {v1, p0}, Lcom/pspdfkit/ui/overlay/OverlayViewProvider;->addOverlayViewProviderObserver(Lcom/pspdfkit/ui/overlay/OverlayViewProvider$OverlayViewProviderObserver;)V

    .line 7
    invoke-direct {p0, v0, v1}, Lcom/pspdfkit/internal/ul;->a(Lcom/pspdfkit/internal/dm$e;Lcom/pspdfkit/ui/overlay/OverlayViewProvider;)V

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 10
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->measure(II)V

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    return-void

    .line 15
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "setCurrentOverlayViewProviders() should be called after bind() was called."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public final Lcom/pspdfkit/internal/re;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/e2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/re$a;
    }
.end annotation


# static fields
.field private static final g:Lcom/pspdfkit/instant/client/InstantProgress;


# instance fields
.field private final a:Lcom/pspdfkit/internal/wf;

.field private final b:Lcom/pspdfkit/internal/vf;

.field private final c:Lcom/pspdfkit/internal/ef;

.field private d:Lio/reactivex/rxjava3/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/PublishSubject<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

.field private f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;


# direct methods
.method public static synthetic $r8$lambda$ZoLGQ4jahjBajY3AQ6zilg-XrCo(Lcom/pspdfkit/internal/re;ZZ)Lorg/reactivestreams/Publisher;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/re;->a(ZZ)Lorg/reactivestreams/Publisher;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/client/InstantProgress;

    const/16 v1, 0x64

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/instant/client/InstantProgress;-><init>(IZ)V

    sput-object v0, Lcom/pspdfkit/internal/re;->g:Lcom/pspdfkit/instant/client/InstantProgress;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/internal/wf;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    .line 12
    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->getInstantDocumentDescriptor()Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/instant/client/InstantDocumentDescriptor;->getInternal()Lcom/pspdfkit/internal/vf;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/vf;->d()Lcom/pspdfkit/internal/ef;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    .line 14
    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/ef;->a(Lcom/pspdfkit/internal/e2;)V

    return-void
.end method

.method private a(ZZ)Lorg/reactivestreams/Publisher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 82
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->LISTEN_FOR_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->FETCH_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    :goto_0
    if-eqz p1, :cond_1

    .line 84
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    .line 89
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object p1

    monitor-enter p1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    .line 92
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->startSyncingWithHint(Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;)Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 96
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/re;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V

    .line 98
    :cond_2
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    sget-object p2, Lio/reactivex/rxjava3/core/BackpressureStrategy;->LATEST:Lio/reactivex/rxjava3/core/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/rxjava3/subjects/PublishSubject;->toFlowable(Lio/reactivex/rxjava3/core/BackpressureStrategy;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p2

    .line 100
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2
.end method

.method private b(ZZ)V
    .locals 1

    if-eqz p2, :cond_0

    .line 1
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->LISTEN_FOR_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->FETCH_UPDATES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    :goto_0
    if-eqz p1, :cond_1

    .line 3
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;->PUSH_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object p1

    monitor-enter p1

    .line 10
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    .line 11
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->startSyncingWithHint(Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestHint;)Lcom/pspdfkit/instant/internal/jni/NativeInstantError;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0, p2}, Lcom/pspdfkit/internal/re;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V

    .line 17
    :cond_2
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ef;->c()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/instant/internal/jni/NativeInstantError;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/pspdfkit/instant/exceptions/InstantSyncException;

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getCode()Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;

    move-result-object v1

    invoke-static {v1}, Lcom/pspdfkit/internal/rj;->a(Lcom/pspdfkit/instant/internal/jni/NativeInstantErrorCode;)Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v1

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeInstantError;->getUnderlyingError()Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/pspdfkit/instant/exceptions/InstantSyncException;-><init>(Lcom/pspdfkit/instant/exceptions/InstantErrorCode;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onError(Ljava/lang/Throwable;)V

    .line 8
    monitor-enter p0

    .line 9
    :try_start_0
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/re;->f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;

    if-eqz v1, :cond_0

    .line 10
    invoke-virtual {p1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;->removeObserver(Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;)V

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/re;->f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;

    .line 14
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ef;->a(Lcom/pspdfkit/instant/exceptions/InstantSyncException;)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ef;->c()V

    .line 23
    invoke-virtual {v0}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object p1

    const-string v1, "errorCode"

    .line 24
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v1, Lcom/pspdfkit/internal/kd;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    .line 68
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 72
    invoke-virtual {v0}, Lcom/pspdfkit/instant/exceptions/InstantException;->getErrorCode()Lcom/pspdfkit/instant/exceptions/InstantErrorCode;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v2

    .line 73
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v1

    const-string v0, "Instant"

    const-string v1, "Failed sync. ID: %s; Message: %s"

    .line 74
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 81
    :cond_1
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/wf;->isListeningToServerChanges()Z

    move-result p1

    invoke-direct {p0, v2, p1}, Lcom/pspdfkit/internal/re;->b(ZZ)V

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicator;)V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->m()Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v0

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115
    :try_start_1
    iget-object v1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/internal/qe;->a()V

    .line 119
    invoke-virtual {p1}, Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicator;->apply()Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicatorChanges;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicatorChanges;->getInvalidatedPages()Ljava/util/HashSet;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v2, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/wf;->s()Lcom/pspdfkit/internal/qe;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/pspdfkit/internal/qe;->a(Ljava/util/Set;)Ljava/util/ArrayList;

    .line 125
    :cond_0
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->m()Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 130
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->b:Lcom/pspdfkit/internal/vf;

    .line 131
    invoke-virtual {v0}, Lcom/pspdfkit/internal/vf;->j()Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    .line 132
    invoke-virtual {v1}, Lcom/pspdfkit/internal/wf;->isListeningToServerChanges()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/instant/internal/jni/NativeServerDocumentLayer;->didRefreshAfterApplyingChanges(Lcom/pspdfkit/instant/internal/jni/NativeServerChangeApplicator;Z)V

    return-void

    :catchall_0
    move-exception p1

    .line 133
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    .line 134
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->a:Lcom/pspdfkit/internal/wf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->m()Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 135
    throw p1
.end method

.method public final declared-synchronized a(Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;)V
    .locals 2

    monitor-enter p0

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    monitor-exit p0

    return-void

    .line 104
    :cond_0
    :try_start_1
    iput-object p3, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

    .line 105
    new-instance p2, Lcom/pspdfkit/internal/re$a;

    iget-object v0, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    const/4 v1, 0x0

    invoke-direct {p2, v0, v1}, Lcom/pspdfkit/internal/re$a;-><init>(Lio/reactivex/rxjava3/subjects/PublishSubject;Lcom/pspdfkit/internal/re$a-IA;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/re;->f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;

    .line 106
    invoke-virtual {p3, p2}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;->addObserver(Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;)V

    .line 109
    sget-object p2, Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;->LISTEN_FOR_CHANGES:Lcom/pspdfkit/instant/internal/jni/NativeSyncRequestType;

    if-eq p1, p2, :cond_1

    .line 110
    iget-object p1, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ef;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ef;->e()V

    return-void
.end method

.method public final c(ZZ)Lio/reactivex/rxjava3/core/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Lio/reactivex/rxjava3/core/Flowable<",
            "Lcom/pspdfkit/instant/client/InstantProgress;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/re$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lcom/pspdfkit/internal/re$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/re;ZZ)V

    invoke-static {v0}, Lio/reactivex/rxjava3/core/Flowable;->defer(Lio/reactivex/rxjava3/functions/Supplier;)Lio/reactivex/rxjava3/core/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public final c()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    sget-object v1, Lcom/pspdfkit/internal/re;->g:Lcom/pspdfkit/instant/client/InstantProgress;

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/rxjava3/subjects/PublishSubject;->onComplete()V

    .line 6
    monitor-enter p0

    .line 7
    :try_start_0
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/pspdfkit/internal/re;->f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;

    if-eqz v1, :cond_0

    .line 8
    invoke-virtual {v0, v1}, Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;->removeObserver(Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;)V

    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/pspdfkit/internal/re;->e:Lcom/pspdfkit/instant/internal/jni/NativeProgressReporter;

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/internal/re;->f:Lcom/pspdfkit/instant/internal/jni/NativeProgressObserver;

    .line 12
    :cond_0
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/re;->d:Lio/reactivex/rxjava3/subjects/PublishSubject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 13
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ef;->d()V

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/re;->c:Lcom/pspdfkit/internal/ef;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/ef;->c()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.class public final Lcom/pspdfkit/internal/s4$b;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/s4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field public final h:Landroid/widget/ImageView;

.field public final i:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

.field final synthetic j:Lcom/pspdfkit/internal/s4;


# direct methods
.method public static synthetic $r8$lambda$eKprxZ812esZzh9cCf9NkabJK-o(Lcom/pspdfkit/internal/s4$b;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/s4$b;->a(Landroid/view/View;)V

    return-void
.end method

.method constructor <init>(Lcom/pspdfkit/internal/s4;Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->j:Lcom/pspdfkit/internal/s4;

    .line 2
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const/4 p1, -0x1

    .line 3
    iput p1, p0, Lcom/pspdfkit/internal/s4$b;->a:I

    .line 4
    iput p1, p0, Lcom/pspdfkit/internal/s4$b;->b:I

    .line 12
    new-instance p1, Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/rxjava3/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->i:Lio/reactivex/rxjava3/disposables/CompositeDisposable;

    .line 17
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_page_image_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->c:Landroid/view/View;

    .line 18
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_page_image:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->d:Landroid/widget/ImageView;

    .line 19
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_item_title:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->e:Landroid/widget/TextView;

    .line 20
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_item_page_number:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->f:Landroid/widget/TextView;

    .line 21
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_item_description:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->g:Landroid/widget/TextView;

    .line 22
    sget p1, Lcom/pspdfkit/R$id;->pspdf__bookmark_list_drag_handle:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/pspdfkit/internal/s4$b;->h:Landroid/widget/ImageView;

    .line 23
    new-instance p1, Lcom/pspdfkit/internal/s4$b$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/pspdfkit/internal/s4$b$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/s4$b;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic a(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/s4$b;->j:Lcom/pspdfkit/internal/s4;

    invoke-static {p1}, Lcom/pspdfkit/internal/s4;->-$$Nest$fgetm(Lcom/pspdfkit/internal/s4;)Lcom/pspdfkit/internal/s4$a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/s4$b;->j:Lcom/pspdfkit/internal/s4;

    invoke-static {v0}, Lcom/pspdfkit/internal/s4;->-$$Nest$fgetm(Lcom/pspdfkit/internal/s4;)Lcom/pspdfkit/internal/s4$a;

    move-result-object v1

    invoke-static {v0}, Lcom/pspdfkit/internal/s4;->-$$Nest$fgetc(Lcom/pspdfkit/internal/s4;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/bookmarks/Bookmark;

    invoke-interface {v1, v0, p1}, Lcom/pspdfkit/internal/s4$a;->b(Lcom/pspdfkit/bookmarks/Bookmark;I)V

    :cond_0
    return-void
.end method

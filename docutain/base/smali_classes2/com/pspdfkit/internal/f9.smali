.class public abstract Lcom/pspdfkit/internal/f9;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

.field private final c:Ljava/util/HashMap;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/zf;Z)V
    .locals 1

    const-string v0, "document"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-boolean p2, p0, Lcom/pspdfkit/internal/f9;->a:Z

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p2

    invoke-static {p2}, Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;->create(Lcom/pspdfkit/internal/jni/NativeDocument;)Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    move-result-object p2

    const-string v0, "create(document.nativeDocument)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/f9;->b:Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    .line 11
    invoke-virtual {p1}, Lcom/pspdfkit/internal/zf;->i()Lcom/pspdfkit/internal/jni/NativeDocument;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/jni/NativeDocument;->getMetadata()Ljava/util/HashMap;

    move-result-object p1

    const-string p2, "document.nativeDocument.metadata"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/f9;->c:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method protected final a()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/f9;->a:Z

    return v0
.end method

.method protected final b()Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/f9;->b:Lcom/pspdfkit/internal/jni/NativeDocumentMetadata;

    return-object v0
.end method

.method protected final c()Ljava/util/HashMap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/f9;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .line 1
    monitor-enter p0

    const/4 v0, 0x0

    .line 2
    :try_start_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/f9;->d:Z

    .line 3
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final e()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/f9;->d:Z

    return-void
.end method

.method public final hasUnsavedChanges()Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/f9;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

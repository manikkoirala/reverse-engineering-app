.class public final Lcom/pspdfkit/internal/us;
.super Lcom/pspdfkit/internal/j4;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/ys$b;


# instance fields
.field private final j:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

.field private k:Landroid/graphics/PointF;

.field private l:Lcom/pspdfkit/internal/ys;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/j4;-><init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V

    .line 2
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/ui/PdfFragment;->getAnnotationConfiguration()Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/us;->j:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method public final a(FF)V
    .locals 2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getActiveAnnotationTool()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    if-eq v0, v1, :cond_0

    return-void

    .line 14
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/pspdfkit/internal/us;->k:Landroid/graphics/PointF;

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->f:Lcom/pspdfkit/internal/dm;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->b(Landroid/graphics/PointF;Landroid/graphics/Matrix;)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/us;->j:Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;

    sget-object p2, Lcom/pspdfkit/annotations/AnnotationType;->STAMP:Lcom/pspdfkit/annotations/AnnotationType;

    .line 17
    const-class v0, Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;

    invoke-interface {p1, p2, v0}, Lcom/pspdfkit/annotations/configuration/AnnotationConfigurationRegistry;->get(Lcom/pspdfkit/annotations/AnnotationType;Ljava/lang/Class;)Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;

    if-nez p1, :cond_1

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 22
    :cond_1
    invoke-interface {p1}, Lcom/pspdfkit/annotations/configuration/StampAnnotationConfiguration;->getStampsForPicker()Ljava/util/List;

    move-result-object p1

    .line 23
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    const/4 p2, 0x0

    .line 24
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/us;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V

    goto :goto_1

    .line 27
    :cond_2
    iget-object p2, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    invoke-static {p2, p0}, Lcom/pspdfkit/internal/ys;->b(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/ys$b;)Lcom/pspdfkit/internal/ys;

    move-result-object p2

    iput-object p2, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    .line 28
    iget v0, p0, Lcom/pspdfkit/internal/j4;->g:I

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/ys;->a(I)V

    .line 29
    iget-object p2, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    iget-object v0, p0, Lcom/pspdfkit/internal/us;->k:Landroid/graphics/PointF;

    invoke-virtual {p2, v0}, Lcom/pspdfkit/internal/ys;->a(Landroid/graphics/PointF;)V

    .line 30
    iget-object p2, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/ys;->a(Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public final a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;Z)V
    .locals 7

    if-eqz p2, :cond_0

    .line 31
    iget-object p2, p0, Lcom/pspdfkit/internal/j4;->d:Landroid/content/Context;

    sget-object v0, Lcom/pspdfkit/annotations/stamps/PredefinedStampType;->CUSTOM:Lcom/pspdfkit/annotations/stamps/PredefinedStampType;

    invoke-static {p2, v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->fromPredefinedType(Landroid/content/Context;Lcom/pspdfkit/annotations/stamps/PredefinedStampType;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object p2

    const-string v0, ""

    .line 33
    invoke-virtual {p2, v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withTitle(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object p2

    .line 34
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withSubtitle(Ljava/lang/String;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object p2

    .line 35
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfWidth()F

    move-result v0

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfHeight()F

    move-result p1

    invoke-virtual {p2, v0, p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withSize(FF)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object p1

    const p2, -0xebe4b1

    .line 36
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->withTextColor(Ljava/lang/Integer;)Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem$Builder;->build()Lcom/pspdfkit/annotations/stamps/StampPickerItem;

    move-result-object p1

    .line 38
    iget-object p2, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    if-eqz p2, :cond_1

    .line 39
    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/ys;->a(Lcom/pspdfkit/annotations/stamps/StampPickerItem;)V

    .line 40
    iget-object p1, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/ys;->c()V

    goto :goto_0

    .line 41
    :cond_0
    iget-object p2, p0, Lcom/pspdfkit/internal/us;->k:Landroid/graphics/PointF;

    if-eqz p2, :cond_1

    .line 42
    iget-object v0, p0, Lcom/pspdfkit/internal/j4;->e:Lcom/pspdfkit/internal/zf;

    iget v1, p0, Lcom/pspdfkit/internal/j4;->g:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/zf;->getPageSize(I)Lcom/pspdfkit/utils/Size;

    move-result-object v0

    .line 45
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfWidth()F

    move-result v1

    iget v2, v0, Lcom/pspdfkit/utils/Size;->width:F

    .line 46
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/high16 v2, 0x42000000    # 32.0f

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 47
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->getDefaultPdfHeight()F

    move-result v3

    iget v4, v0, Lcom/pspdfkit/utils/Size;->height:F

    .line 48
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 49
    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 50
    new-instance v4, Landroid/graphics/RectF;

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    sub-float v6, v3, v1

    div-float/2addr v2, v5

    add-float v5, p2, v2

    add-float/2addr v3, v1

    sub-float/2addr p2, v2

    invoke-direct {v4, v6, v5, v3, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 51
    new-instance p2, Landroid/graphics/RectF;

    iget v1, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    const/4 v2, 0x0

    invoke-direct {p2, v2, v1, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v4, p2}, Lcom/pspdfkit/internal/ga;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 52
    iget p2, p0, Lcom/pspdfkit/internal/j4;->g:I

    invoke-virtual {p1, p2}, Lcom/pspdfkit/annotations/stamps/StampPickerItem;->createStampAnnotation(I)Lcom/pspdfkit/annotations/StampAnnotation;

    move-result-object p1

    .line 53
    invoke-virtual {p1, v4}, Lcom/pspdfkit/annotations/Annotation;->setBoundingBox(Landroid/graphics/RectF;)V

    .line 55
    new-instance p2, Lcom/pspdfkit/utils/Size;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-direct {p2, v0, v1}, Lcom/pspdfkit/utils/Size;-><init>(FF)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/annotations/StampAnnotation;->setRotation(ILcom/pspdfkit/utils/Size;)V

    .line 56
    iget-object p2, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p2, p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 58
    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/j4;->a(Lcom/pspdfkit/annotations/StampAnnotation;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/j4;->a(Lcom/pspdfkit/internal/os;)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    sget v0, Lcom/pspdfkit/internal/ys;->k:I

    const-string v0, "com.pspdfkit.ui.dialog.stamps.StampPickerDialog.FRAGMENT_TAG"

    .line 5
    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/ys;

    if-eqz p1, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ys;->a()I

    move-result p1

    iget v0, p0, Lcom/pspdfkit/internal/j4;->g:I

    if-ne p1, v0, :cond_0

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/j4;->b:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/pspdfkit/internal/ys;->a(Landroidx/fragment/app/FragmentManager;Lcom/pspdfkit/internal/ys$b;)Lcom/pspdfkit/internal/ys;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    if-eqz p1, :cond_0

    .line 10
    invoke-virtual {p1}, Lcom/pspdfkit/internal/ys;->b()Landroid/graphics/PointF;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/us;->k:Landroid/graphics/PointF;

    :cond_0
    return-void
.end method

.method public final e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->STAMP:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/us;->l:Lcom/pspdfkit/internal/ys;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

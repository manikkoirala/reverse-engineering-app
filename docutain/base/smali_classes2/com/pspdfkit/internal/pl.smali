.class public final Lcom/pspdfkit/internal/pl;
.super Lcom/pspdfkit/internal/ql;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/pl$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/ql<",
        "Lcom/pspdfkit/document/OutlineElement;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Landroidx/recyclerview/widget/RecyclerView;

.field private final e:Landroid/widget/ProgressBar;

.field private final f:Landroid/widget/FrameLayout;

.field private final g:Landroidx/appcompat/widget/SearchView;

.field private final h:Lcom/pspdfkit/internal/ql$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/ql$b<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Lcom/pspdfkit/internal/rl;

.field private k:Lcom/pspdfkit/internal/ml;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lio/reactivex/rxjava3/disposables/Disposable;

.field private r:Lio/reactivex/rxjava3/disposables/Disposable;

.field private s:Lcom/pspdfkit/internal/zf;

.field private t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;


# direct methods
.method public static synthetic $r8$lambda$1ZayGlFHj506mu8sa76xMQhUsOk(Lcom/pspdfkit/internal/pl;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/pl;->f()V

    return-void
.end method

.method public static synthetic $r8$lambda$9YTjbhTF9kNfV2nEWEg8AboHm04(Lcom/pspdfkit/internal/pl;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pl;->setAdapter(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$RCF0Ty9T2-TDzXmO1XXXp-onH5U(Lcom/pspdfkit/internal/pl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pl;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$co5PDP0ScCtBSfaEKGHeMioogww(Lcom/pspdfkit/internal/pl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pl;->a(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$mAE1q3qDC4uCA2nks6wAgvUEioE(Lcom/pspdfkit/internal/pl;)Lio/reactivex/rxjava3/core/Single;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/pl;->e()Lio/reactivex/rxjava3/core/Single;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetg(Lcom/pspdfkit/internal/pl;)Landroidx/appcompat/widget/SearchView;
    .locals 0

    iget-object p0, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/internal/ql$b;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/pspdfkit/internal/ql$b<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ql;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pl;->i:Z

    const/4 v1, 0x0

    .line 13
    iput-boolean v1, p0, Lcom/pspdfkit/internal/pl;->m:Z

    .line 14
    iput-boolean v1, p0, Lcom/pspdfkit/internal/pl;->n:Z

    const/4 v2, 0x0

    .line 23
    iput-object v2, p0, Lcom/pspdfkit/internal/pl;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 38
    sget v2, Lcom/pspdfkit/R$id;->pspdf__outline_list_view:I

    invoke-virtual {p0, v2}, Landroid/view/View;->setId(I)V

    .line 39
    invoke-virtual {p0, v0}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 40
    iput-object p2, p0, Lcom/pspdfkit/internal/pl;->h:Lcom/pspdfkit/internal/ql$b;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__outline_list_view:I

    invoke-virtual {p2, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 44
    sget v2, Lcom/pspdfkit/R$id;->pspdf__outline_progress:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/pspdfkit/internal/pl;->e:Landroid/widget/ProgressBar;

    .line 45
    sget v2, Lcom/pspdfkit/R$id;->pspdf__outline_recycler_view:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v2, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    .line 46
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v3, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 48
    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 49
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 51
    new-instance v0, Landroidx/recyclerview/widget/DividerItemDecoration;

    .line 52
    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->getOrientation()I

    move-result v3

    invoke-direct {v0, p1, v3}, Landroidx/recyclerview/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    .line 53
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    const/4 v0, 0x2

    .line 54
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setOverScrollMode(I)V

    .line 57
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/pspdfkit/R$layout;->pspdf__outline_pager_outline_list_no_match:I

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->f:Landroid/widget/FrameLayout;

    const-string v2, ""

    .line 59
    iput-object v2, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    .line 60
    new-instance v2, Landroidx/appcompat/widget/SearchView;

    invoke-direct {v2, p1}, Landroidx/appcompat/widget/SearchView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    .line 61
    sget v3, Lcom/pspdfkit/R$id;->pspdf__outline_list_search_view:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 62
    invoke-virtual {v2, v1}, Landroidx/appcompat/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 63
    sget v3, Lcom/pspdfkit/R$string;->pspdf__search_outline_hint:I

    .line 64
    invoke-static {p1, v3, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 65
    invoke-virtual {v2, v3}, Landroidx/appcompat/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    const/4 v3, 0x3

    .line 67
    invoke-virtual {v2, v3}, Landroidx/appcompat/widget/SearchView;->setImeOptions(I)V

    const/16 v3, 0x8

    .line 68
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 70
    sget v3, Landroidx/appcompat/R$id;->search_edit_frame:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 71
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 73
    invoke-virtual {v4, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/16 v8, 0x10

    .line 74
    iput v8, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 75
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    sget v3, Landroidx/appcompat/R$id;->search_plate:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 78
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 81
    sget v3, Landroidx/appcompat/R$id;->search_src_text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 82
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    .line 83
    invoke-virtual {v3, v1, v1, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 85
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v3, Lcom/pspdfkit/R$layout;->pspdf__outline_list_divider:I

    invoke-virtual {p1, v3, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 87
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 91
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p1, v6, v1, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p0, p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p1, v6, v1, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p0, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/pl;->m:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-nez v0, :cond_2

    :cond_1
    return-void

    .line 5
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/pspdfkit/internal/pl;->m:Z

    if-eqz v0, :cond_4

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 8
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/hj;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    .line 12
    :cond_3
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    .line 14
    iget-boolean v0, p0, Lcom/pspdfkit/internal/pl;->n:Z

    if-eqz v0, :cond_5

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ml;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string p1, ""

    .line 19
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ml;->a(Ljava/util/ArrayList;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    :cond_5
    :goto_0
    return-void
.end method

.method private synthetic a(Z)V
    .locals 4

    .line 47
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/16 v3, 0x8

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 48
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->f:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private synthetic e()Lio/reactivex/rxjava3/core/Single;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->s:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getOutlineAsync()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 3

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pl;->n:Z

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/pl;->setOutlineListViewLoading(Z)V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->o:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/pspdfkit/internal/pl;->m:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v2, :cond_0

    .line 4
    invoke-virtual {v2, v1, v0}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/ArrayList;Z)V

    .line 5
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/pl;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v0, :cond_1

    .line 6
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ml;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private setAdapter(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/document/OutlineElement;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/pl;->n:Z

    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/pl;->setOutlineListViewLoading(Z)V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/ml;

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v5, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda2;

    invoke-direct {v5, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/pl;)V

    new-instance v6, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda3;

    invoke-direct {v6, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/pl;)V

    new-instance v7, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda4;

    invoke-direct {v7, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/pl;)V

    iget-object v8, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/pspdfkit/internal/ml;-><init>(Landroid/content/Context;Ljava/util/List;Landroidx/recyclerview/widget/RecyclerView;Lcom/pspdfkit/internal/ml$b;Lcom/pspdfkit/internal/ml$c;Lcom/pspdfkit/internal/ml$a;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->j:Lcom/pspdfkit/internal/rl;

    if-eqz p1, :cond_0

    .line 25
    iget p1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ml;->e(I)V

    .line 26
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->j:Lcom/pspdfkit/internal/rl;

    iget v0, v0, Lcom/pspdfkit/internal/rl;->j:I

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ml;->f(I)V

    .line 28
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    iget-boolean v0, p0, Lcom/pspdfkit/internal/pl;->i:Z

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/ml;->b(Z)V

    .line 29
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method private setOutlineListViewLoading(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    if-eqz p1, :cond_1

    const/16 v3, 0x8

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_2

    const/16 v1, 0x8

    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/document/OutlineElement;)V
    .locals 3

    .line 49
    invoke-virtual {p1}, Lcom/pspdfkit/document/OutlineElement;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    .line 51
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v1

    const-string v2, "tap_outline_element_in_outline_list"

    .line 52
    invoke-virtual {v1, v2}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v1

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/actions/Action;->getType()Lcom/pspdfkit/annotations/actions/ActionType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "null"

    :goto_0
    const-string v2, "action_type"

    invoke-virtual {v1, v2, v0}, Lcom/pspdfkit/internal/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/pspdfkit/internal/q$a;->a()V

    .line 56
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->h:Lcom/pspdfkit/internal/ql$b;

    invoke-interface {v0, p0, p1}, Lcom/pspdfkit/internal/ql$b;->a(Lcom/pspdfkit/internal/ql;Ljava/lang/Object;)V

    .line 57
    iget-object p1, p0, Lcom/pspdfkit/internal/ql;->b:Lcom/pspdfkit/internal/ql$a;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ql$a;->hide()V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 2

    .line 29
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->j:Lcom/pspdfkit/internal/rl;

    .line 31
    iget v0, p1, Lcom/pspdfkit/internal/rl;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 33
    iget v0, p1, Lcom/pspdfkit/internal/rl;->b:I

    if-eqz v0, :cond_0

    .line 34
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->d:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v0, :cond_1

    .line 38
    iget v1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ml;->e(I)V

    .line 39
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    iget v1, p1, Lcom/pspdfkit/internal/rl;->j:I

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/ml;->f(I)V

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    sget v1, Landroidx/appcompat/R$id;->search_src_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 43
    iget v1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 45
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->f:Landroid/widget/FrameLayout;

    sget v1, Lcom/pspdfkit/R$id;->pspdf__outline_no_match_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    iget p1, p1, Lcom/pspdfkit/internal/rl;->c:I

    invoke-static {p1}, Lcom/pspdfkit/internal/s5;->a(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 23
    iget-object p2, p0, Lcom/pspdfkit/internal/pl;->s:Lcom/pspdfkit/internal/zf;

    if-eq p2, p1, :cond_0

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->s:Lcom/pspdfkit/internal/zf;

    const/4 p1, 0x0

    .line 26
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    if-nez v0, :cond_1

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->s:Lcom/pspdfkit/internal/zf;

    if-eqz v1, :cond_1

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/pl;)V

    :cond_1
    if-eqz v0, :cond_2

    .line 8
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 9
    invoke-static {v1}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    const/4 v1, 0x1

    .line 10
    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/pl;->setOutlineListViewLoading(Z)V

    .line 12
    invoke-interface {v0}, Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;->getOutlineElements()Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 13
    invoke-static {}, Lio/reactivex/rxjava3/schedulers/Schedulers;->computation()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    .line 14
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/pl;)V

    .line 15
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    :cond_2
    return-void
.end method

.method public getDocumentOutlineProvider()Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    return-object v0
.end method

.method public getTabButtonId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_outline:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__activity_menu_outline:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final onAttachedToWindow()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 2
    invoke-static {}, Lio/reactivex/rxjava3/subjects/PublishSubject;->create()Lio/reactivex/rxjava3/subjects/PublishSubject;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    new-instance v2, Lcom/pspdfkit/internal/pl$a;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/pl$a;-><init>(Lcom/pspdfkit/internal/pl;Lio/reactivex/rxjava3/subjects/PublishSubject;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/SearchView;->setOnQueryTextListener(Landroidx/appcompat/widget/SearchView$OnQueryTextListener;)V

    .line 17
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc8

    .line 18
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/rxjava3/subjects/PublishSubject;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    .line 19
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object v0

    new-instance v1, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/pspdfkit/internal/pl$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/pl;)V

    .line 20
    invoke-virtual {v0, v1}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->g:Landroidx/appcompat/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SearchView;->setOnQueryTextListener(Landroidx/appcompat/widget/SearchView$OnQueryTextListener;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 4
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 5
    iput-object v1, p0, Lcom/pspdfkit/internal/pl;->q:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 7
    invoke-static {v0}, Lcom/pspdfkit/internal/iq;->a(Lio/reactivex/rxjava3/disposables/Disposable;)V

    .line 8
    iput-object v1, p0, Lcom/pspdfkit/internal/pl;->r:Lio/reactivex/rxjava3/disposables/Disposable;

    .line 11
    iput-object v1, p0, Lcom/pspdfkit/internal/pl;->t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/pspdfkit/internal/pl$b;

    if-nez v0, :cond_0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/pspdfkit/internal/pl$b;

    .line 7
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 8
    iget-object v0, p1, Lcom/pspdfkit/internal/pl$b;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 9
    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->o:Ljava/util/ArrayList;

    .line 11
    :cond_1
    iget-object v1, p1, Lcom/pspdfkit/internal/pl$b;->b:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    .line 12
    iget-boolean v1, p1, Lcom/pspdfkit/internal/pl$b;->c:Z

    iput-boolean v1, p0, Lcom/pspdfkit/internal/pl;->m:Z

    .line 13
    iget-object p1, p1, Lcom/pspdfkit/internal/pl$b;->d:Ljava/lang/String;

    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    .line 16
    invoke-virtual {p1, v0, v1}, Lcom/pspdfkit/internal/hj;->a(Ljava/util/ArrayList;Z)V

    .line 17
    iget-boolean p1, p0, Lcom/pspdfkit/internal/pl;->m:Z

    if-eqz p1, :cond_2

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/pl;->a(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/pspdfkit/internal/pl$b;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/pl$b;-><init>(Landroid/os/Parcelable;)V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->p:Ljava/util/ArrayList;

    iput-object v0, v1, Lcom/pspdfkit/internal/pl$b;->b:Ljava/util/ArrayList;

    .line 4
    iget-boolean v0, p0, Lcom/pspdfkit/internal/pl;->m:Z

    iput-boolean v0, v1, Lcom/pspdfkit/internal/pl$b;->c:Z

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->l:Ljava/lang/String;

    iput-object v0, v1, Lcom/pspdfkit/internal/pl$b;->d:Ljava/lang/String;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/pspdfkit/internal/pl;->n:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 7
    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/hj;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/pl;->o:Ljava/util/ArrayList;

    .line 8
    iput-object v0, v1, Lcom/pspdfkit/internal/pl$b;->a:Ljava/util/ArrayList;

    .line 10
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->o:Ljava/util/ArrayList;

    iput-object v0, v1, Lcom/pspdfkit/internal/pl$b;->a:Ljava/util/ArrayList;

    :goto_0
    return-object v1
.end method

.method public setDocumentOutlineProvider(Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    if-ne v0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->t:Lcom/pspdfkit/ui/PdfOutlineView$DocumentOutlineProvider;

    const/4 p1, 0x0

    .line 5
    iput-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    .line 7
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ql;->d()V

    return-void
.end method

.method public setShowPageLabels(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/pspdfkit/internal/pl;->i:Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/ml;->b(Z)V

    .line 4
    iget-object p1, p0, Lcom/pspdfkit/internal/pl;->k:Lcom/pspdfkit/internal/ml;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.class public interface abstract Lcom/pspdfkit/internal/zf$f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/internal/zf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "f"
.end annotation


# virtual methods
.method public abstract onInternalDocumentSaveFailed(Lcom/pspdfkit/internal/zf;Ljava/lang/Throwable;)V
.end method

.method public abstract onInternalDocumentSaved(Lcom/pspdfkit/internal/zf;)V
.end method

.method public abstract onPageBindingChanged()V
.end method

.method public abstract onPageRotationOffsetChanged()V
.end method

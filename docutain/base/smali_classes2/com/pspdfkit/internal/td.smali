.class public final Lcom/pspdfkit/internal/td;
.super Lcom/pspdfkit/internal/lm;
.source "SourceFile"


# instance fields
.field private final M:[I

.field private N:Z

.field private O:Z


# direct methods
.method public constructor <init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p8}, Lcom/pspdfkit/internal/lm;-><init>(Lcom/pspdfkit/internal/views/document/DocumentView;IIFFIZLcom/pspdfkit/internal/cm;)V

    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/td;->N:Z

    .line 4
    iput-boolean p1, p0, Lcom/pspdfkit/internal/td;->O:Z

    .line 28
    iget-object p3, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p3

    .line 29
    new-array p4, p3, [I

    iput-object p4, p0, Lcom/pspdfkit/internal/td;->M:[I

    .line 30
    aput p1, p4, p1

    const/4 p1, 0x1

    :goto_0
    if-ge p1, p3, :cond_0

    .line 32
    iget-object p4, p0, Lcom/pspdfkit/internal/td;->M:[I

    add-int/lit8 p5, p1, -0x1

    aget p5, p4, p5

    add-int/2addr p5, p2

    add-int/2addr p5, p6

    aput p5, p4, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 34
    :cond_0
    iget p1, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/lm;->m(I)V

    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    .line 1
    iget p2, p0, Lcom/pspdfkit/internal/ug;->i:I

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p2, p1

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-interface {p1, v0}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result p1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/td;->M:[I

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_0

    move p1, v1

    goto :goto_1

    .line 7
    :cond_0
    iget-object v2, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget v3, v2, v1

    if-gt v3, p2, :cond_1

    add-int/lit8 v3, v1, 0x1

    aget v2, v2, v3

    if-ge p2, v2, :cond_1

    move p1, v1

    goto :goto_2

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    return p1
.end method

.method public final a(IIIFJ)V
    .locals 8

    .line 15
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v5, v0, p4

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/pspdfkit/internal/ug;->b(IIIFJ)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/dm;)V
    .locals 4

    .line 8
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->b(I)I

    move-result v1

    .line 10
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->c(I)I

    move-result v2

    .line 11
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->l(I)I

    move-result v3

    add-int/2addr v3, v1

    .line 12
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->a(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 14
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public final b(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-interface {v1, v2}, Lcom/pspdfkit/internal/cm;->a(I)I

    move-result v1

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/utils/Size;

    iget v2, v2, Lcom/pspdfkit/utils/Size;->width:F

    iget v3, p0, Lcom/pspdfkit/internal/ug;->c:F

    mul-float v2, v2, v3

    float-to-int v2, v2

    if-ge v0, v1, :cond_1

    .line 7
    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 8
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-le p1, v2, :cond_0

    sub-int/2addr p1, v2

    div-int/lit8 p1, p1, 0x2

    goto :goto_0

    :cond_0
    sub-int/2addr p1, v2

    goto :goto_0

    .line 10
    :cond_1
    iget v1, p0, Lcom/pspdfkit/internal/lm;->A:I

    if-ne p1, v1, :cond_2

    iget p1, p0, Lcom/pspdfkit/internal/lm;->B:I

    goto :goto_0

    :cond_2
    iget p1, p0, Lcom/pspdfkit/internal/ug;->i:I

    sub-int/2addr p1, v2

    div-int/lit8 p1, p1, 0x2

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 11
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget v0, v1, v0

    add-int/2addr v0, p1

    return v0
.end method

.method public final b(II)I
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->s:Lcom/pspdfkit/internal/cm;

    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/td;->a(II)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/pspdfkit/internal/cm;->b(I)I

    move-result p1

    return p1
.end method

.method public final b(Lcom/pspdfkit/internal/dm;)V
    .locals 3

    .line 13
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result v0

    .line 14
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->l(I)I

    move-result v1

    .line 15
    invoke-virtual {p0, v0}, Lcom/pspdfkit/internal/td;->a(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    .line 16
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 17
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 19
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    return v0
.end method

.method public final d(II)Z
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->I:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 3
    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/td;->N:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-ltz p1, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/td;->O:Z

    if-eqz v0, :cond_3

    if-lez p1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    .line 5
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->H:Z

    if-nez v0, :cond_5

    .line 6
    :cond_4
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/lm;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 7
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v3, 0x7d0

    if-lt v0, v3, :cond_5

    .line 8
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result p2

    .line 9
    iget v0, p0, Lcom/pspdfkit/internal/lm;->F:I

    iget v3, p0, Lcom/pspdfkit/internal/lm;->G:I

    invoke-virtual {p0, v0, v3}, Lcom/pspdfkit/internal/td;->a(II)I

    move-result v0

    .line 10
    iget-object v3, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget v3, v3, v0

    .line 11
    iget-object v4, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    invoke-virtual {v4}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v4

    sub-int/2addr v4, v3

    .line 14
    invoke-virtual {p0}, Lcom/pspdfkit/internal/ug;->e()Lcom/pspdfkit/internal/views/document/DocumentView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 15
    invoke-static {v4, v3, p1}, Lcom/pspdfkit/internal/ug;->a(IFI)I

    move-result p1

    int-to-float p1, p1

    .line 18
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result p1

    float-to-int p1, p1

    add-int/2addr v0, p1

    sub-int/2addr p2, v2

    .line 19
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 20
    iget-object p2, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget p1, p2, p1

    .line 21
    iget-object p2, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p2}, Landroid/view/View;->getScrollX()I

    move-result p2

    sub-int v6, p1, p2

    .line 22
    iget-object v3, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v4

    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v5

    const/4 v7, 0x0

    const/16 v8, 0x190

    invoke-virtual/range {v3 .. v8}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_1

    .line 24
    :cond_5
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 25
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 26
    iget-object v3, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v4, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/utils/Size;

    iget v3, v3, Lcom/pspdfkit/utils/Size;->width:F

    iget v4, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v3, v3, v4

    float-to-int v3, v3

    .line 29
    iget v4, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-ge v0, v4, :cond_6

    const/4 p2, 0x0

    .line 30
    :cond_6
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-ge v3, v0, :cond_7

    const/4 p1, 0x0

    .line 32
    :cond_7
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    iget v3, p0, Lcom/pspdfkit/internal/lm;->C:I

    neg-int p1, p1

    neg-int p2, p2

    invoke-virtual {v0, v1, v3, p1, p2}, Lcom/pspdfkit/internal/qq;->a(IIII)V

    .line 35
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return v2
.end method

.method public final e(I)Lcom/pspdfkit/utils/Size;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/utils/Size;

    return-object p1
.end method

.method public final e(II)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/td;->N:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/pspdfkit/internal/td;->O:Z

    if-eqz v0, :cond_2

    if-lez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 3
    iget-boolean v0, p0, Lcom/pspdfkit/internal/lm;->H:Z

    if-nez v0, :cond_4

    :cond_3
    invoke-virtual {p0, v1}, Lcom/pspdfkit/internal/lm;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4
    iget-object v2, p0, Lcom/pspdfkit/internal/lm;->D:Landroid/widget/OverScroller;

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v3

    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v4

    const/4 v7, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v2 .. v7}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_1

    .line 6
    :cond_4
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v2, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->height:F

    iget v2, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v2

    float-to-int v0, v0

    .line 7
    iget-object v2, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/utils/Size;

    iget v2, v2, Lcom/pspdfkit/utils/Size;->width:F

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v2, v2, v3

    float-to-int v2, v2

    .line 13
    iget v3, p0, Lcom/pspdfkit/internal/ug;->j:I

    if-ge v0, v3, :cond_5

    const/4 p2, 0x0

    .line 14
    :cond_5
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-ge v2, v0, :cond_6

    const/4 p1, 0x0

    .line 16
    :cond_6
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    iget v1, p0, Lcom/pspdfkit/internal/lm;->B:I

    iget v2, p0, Lcom/pspdfkit/internal/lm;->C:I

    neg-int v3, p1

    neg-int v4, p2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 19
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public final f()I
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->a:Lcom/pspdfkit/internal/views/document/DocumentView;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final f(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget p1, v0, p1

    return p1
.end method

.method public final g()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->i:I

    invoke-virtual {p0}, Lcom/pspdfkit/internal/td;->h()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public final g(I)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public final h()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/ug;->b:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/zf;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/td;->M:[I

    aget v0, v1, v0

    return v0
.end method

.method public final i()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final j(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->A:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/td;->a(IZ)V

    return-void
.end method

.method public final n()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v1, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/pspdfkit/internal/lm;->C:I

    neg-int v0, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final o()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/pspdfkit/internal/ug;->j:I

    iget v1, p0, Lcom/pspdfkit/internal/lm;->z:F

    iget v2, p0, Lcom/pspdfkit/internal/ug;->c:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->D()I

    move-result v1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/lm;->F()I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final x()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/lm;->x()V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/lm;->E:Lcom/pspdfkit/internal/qq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 6
    iget v0, p0, Lcom/pspdfkit/internal/lm;->B:I

    const/4 v2, 0x0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/pspdfkit/internal/td;->N:Z

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/gs;->u:Ljava/util/ArrayList;

    iget v3, p0, Lcom/pspdfkit/internal/lm;->A:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/utils/Size;

    iget v0, v0, Lcom/pspdfkit/utils/Size;->width:F

    iget v3, p0, Lcom/pspdfkit/internal/lm;->z:F

    mul-float v0, v0, v3

    float-to-int v0, v0

    .line 10
    iget v3, p0, Lcom/pspdfkit/internal/lm;->B:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/pspdfkit/internal/ug;->i:I

    if-gt v0, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, p0, Lcom/pspdfkit/internal/td;->O:Z

    return-void
.end method

.method public final z()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/pspdfkit/internal/lm;->A()V

    return-void
.end method

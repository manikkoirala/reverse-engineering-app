.class public abstract Lcom/pspdfkit/internal/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/k1;
.implements Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;


# static fields
.field private static final m:Landroid/graphics/Paint;


# instance fields
.field private final b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

.field private final c:Lcom/pspdfkit/internal/specialMode/handler/a;

.field private final d:Landroid/graphics/RectF;

.field private final e:Landroid/graphics/RectF;

.field protected f:I

.field protected g:Lcom/pspdfkit/internal/zf;

.field protected h:Lcom/pspdfkit/internal/dm;

.field private final i:Ljava/util/ArrayList;

.field private j:I

.field protected k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

.field private l:Lcom/pspdfkit/internal/o1;


# direct methods
.method public static synthetic $r8$lambda$dTBOGrTUF17QVAI0VMUmLSdFpUQ(Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {p0}, Lcom/pspdfkit/internal/bi;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic $r8$lambda$dbYePtWOU2LcMQJZJlj08elNWRk(Lcom/pspdfkit/internal/bi;Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/bi;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/bi;->m:Landroid/graphics/Paint;

    const/16 v1, 0xfd

    const/16 v2, 0x98

    const/16 v3, 0xaf

    const/16 v4, 0xc7

    .line 4
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 5
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 6
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method

.method protected constructor <init>(Lcom/pspdfkit/internal/specialMode/handler/a;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    .line 8
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    .line 24
    iput-object p1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 25
    iput-object p2, p0, Lcom/pspdfkit/internal/bi;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p0, p1, v0}, Lcom/pspdfkit/internal/bi;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/specialMode/handler/a;)V

    .line 75
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    const-string v1, "create_annotation"

    .line 76
    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/q;->a(Ljava/lang/String;)Lcom/pspdfkit/internal/q$a;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/q$a;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/q$a;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/pspdfkit/internal/q$a;->a()V

    return-void
.end method

.method private static synthetic a(Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.MarkupAnnotations"

    const-string v1, "Unable to update annotation data"

    .line 79
    invoke-static {v0, v1, p0}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private k()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    .line 2
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    .line 3
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v2}, Lcom/pspdfkit/utils/PageRect;->getPageRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-eqz v0, :cond_1

    .line 6
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 7
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getAlpha()F

    move-result v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v0

    invoke-interface {p0}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 10
    :cond_1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    .line 15
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    iget v2, p0, Lcom/pspdfkit/internal/bi;->f:I

    .line 18
    invoke-interface {p0}, Lcom/pspdfkit/internal/k1;->e()Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->toAnnotationType()Lcom/pspdfkit/annotations/AnnotationType;

    move-result-object v3

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 19
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v4

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 20
    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v5

    move-object v6, v7

    .line 21
    invoke-static/range {v1 .. v6}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/internal/zf;ILcom/pspdfkit/annotations/AnnotationType;IFLjava/util/List;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    move-result-object v0

    .line 28
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->i()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 29
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.pspdfkit.internal.annotations.markup.default-rect-name"

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    .line 55
    :cond_2
    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 56
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 57
    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->l:Lcom/pspdfkit/internal/o1;

    .line 58
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 64
    :cond_3
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->l:Lcom/pspdfkit/internal/o1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->a()V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v7, v1}, Lcom/pspdfkit/internal/bi;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/ArrayList;Landroid/graphics/RectF;)V

    .line 66
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->synchronizeToNativeObjectIfAttached()Z

    .line 67
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    .line 68
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->l:Lcom/pspdfkit/internal/o1;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/o1;->b()V

    return-void

    .line 69
    :cond_4
    :goto_1
    invoke-virtual {p0, v7}, Lcom/pspdfkit/internal/bi;->a(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    goto :goto_2

    .line 73
    :cond_5
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 74
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setColor(I)V

    .line 75
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/a;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    :goto_2
    if-nez v0, :cond_6

    return-void

    .line 76
    :cond_6
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v7, v1}, Lcom/pspdfkit/internal/bi;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/ArrayList;Landroid/graphics/RectF;)V

    .line 77
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->c()Lcom/pspdfkit/internal/qf;

    move-result-object v1

    .line 78
    check-cast v1, Lcom/pspdfkit/internal/r1;

    invoke-virtual {v1, v0}, Lcom/pspdfkit/internal/r1;->addAnnotationToPageAsync(Lcom/pspdfkit/annotations/Annotation;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    .line 79
    invoke-static {}, Lio/reactivex/rxjava3/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/rxjava3/core/Completable;->observeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Completable;

    move-result-object v1

    new-instance v2, Lcom/pspdfkit/internal/bi$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/pspdfkit/internal/bi$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/bi;Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V

    new-instance v3, Lcom/pspdfkit/internal/bi$$ExternalSyntheticLambda1;

    invoke-direct {v3}, Lcom/pspdfkit/internal/bi$$ExternalSyntheticLambda1;-><init>()V

    .line 80
    invoke-virtual {v1, v2, v3}, Lio/reactivex/rxjava3/core/Completable;->subscribe(Lio/reactivex/rxjava3/functions/Action;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    .line 91
    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    .line 92
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    .line 93
    invoke-virtual {v1}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/pspdfkit/internal/o1;->a(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/internal/fl;)Lcom/pspdfkit/internal/o1;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->l:Lcom/pspdfkit/internal/o1;

    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/util/ArrayList;)Lcom/pspdfkit/annotations/BaseRectsAnnotation;
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    iget v1, p0, Lcom/pspdfkit/internal/bi;->j:I

    int-to-float v1, v1

    sget-object v2, Lcom/pspdfkit/internal/bi;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 71
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/PageRect;

    .line 73
    invoke-virtual {v1}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    iget v2, p0, Lcom/pspdfkit/internal/bi;->j:I

    int-to-float v2, v2

    sget-object v3, Lcom/pspdfkit/internal/bi;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 2

    const/4 v0, 0x0

    .line 67
    :goto_0
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v1, p1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Lcom/pspdfkit/internal/specialMode/handler/a;)V
    .locals 2

    .line 80
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/d;->a()Lcom/pspdfkit/internal/fl;

    move-result-object v0

    invoke-static {p1}, Lcom/pspdfkit/internal/x;->a(Lcom/pspdfkit/annotations/Annotation;)Lcom/pspdfkit/internal/x;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/fl;->a(Lcom/pspdfkit/internal/ja;)V

    .line 81
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/Annotation;Z)V

    .line 82
    invoke-virtual {p2}, Lcom/pspdfkit/internal/specialMode/handler/a;->getFragment()Lcom/pspdfkit/ui/PdfFragment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/pspdfkit/ui/PdfFragment;->notifyAnnotationHasChanged(Lcom/pspdfkit/annotations/Annotation;)V

    return-void
.end method

.method protected a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/ArrayList;Landroid/graphics/RectF;)V
    .locals 0

    .line 83
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-lez p3, :cond_0

    .line 84
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/ci;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/os;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/internal/os;->getParentView()Lcom/pspdfkit/internal/dm;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    .line 3
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    .line 4
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->a()Lcom/pspdfkit/internal/zf;

    move-result-object v0

    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    .line 5
    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/bi;->f:I

    .line 7
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/pspdfkit/internal/ov;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lcom/pspdfkit/internal/bi;->j:I

    .line 9
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->a(Lcom/pspdfkit/internal/k1;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/i1;

    invoke-virtual {p1, p0}, Lcom/pspdfkit/internal/i1;->addOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "PSPDFKit.MarkupAnnotations"

    const-string v1, "Entering markup editing mode."

    .line 12
    invoke-static {v0, v1, p1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_8

    const/4 v2, 0x0

    if-eq v0, v1, :cond_6

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    return v2

    .line 14
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->setEmpty()V

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->setEmpty()V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    .line 18
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/dm;->a(Z)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 20
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->i()Z

    move-result p1

    if-nez p1, :cond_1

    .line 21
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    :cond_1
    return v1

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 23
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, v0, Landroid/graphics/RectF;->right:F

    .line 24
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 25
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->sort()V

    .line 27
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/dm;->a(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object p1

    .line 30
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 31
    iget-object v3, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    .line 32
    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 33
    invoke-static {v0, p1}, Lcom/pspdfkit/internal/nu;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;)V

    .line 34
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 36
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->g:Lcom/pspdfkit/internal/zf;

    iget v4, p0, Lcom/pspdfkit/internal/bi;->f:I

    .line 37
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->j()Z

    move-result v5

    invoke-virtual {v0, v4, v3, v5}, Lcom/pspdfkit/internal/zf;->a(ILandroid/graphics/RectF;Z)Ljava/util/List;

    move-result-object v0

    .line 38
    iget-object v3, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 39
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 41
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    .line 42
    new-instance v5, Lcom/pspdfkit/utils/PageRect;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/RectF;

    invoke-direct {v5, v6}, Lcom/pspdfkit/utils/PageRect;-><init>(Landroid/graphics/RectF;)V

    .line 43
    invoke-virtual {v5, p1}, Lcom/pspdfkit/utils/PageRect;->updateScreenRect(Landroid/graphics/Matrix;)V

    .line 44
    invoke-virtual {v5}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->sort()V

    .line 45
    iget-object v6, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 47
    :cond_3
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_1

    .line 49
    :cond_4
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 50
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v3, "Got "

    invoke-direct {p1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " selected rects, see: "

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    const-string v3, "PSPDFKit.MarkupAnnotations"

    invoke-static {v3, p1, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    .line 52
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/dm;->a(Z)V

    return v1

    .line 53
    :cond_6
    invoke-direct {p0}, Lcom/pspdfkit/internal/bi;->k()V

    .line 54
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->setEmpty()V

    .line 55
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->setEmpty()V

    .line 56
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 57
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    .line 58
    invoke-virtual {p1, v2}, Lcom/pspdfkit/internal/dm;->a(Z)V

    .line 59
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 60
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->i()Z

    move-result p1

    if-nez p1, :cond_7

    .line 61
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    :cond_7
    return v1

    .line 62
    :cond_8
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 64
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, v0, Landroid/graphics/RectF;->right:F

    .line 66
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->d:Landroid/graphics/RectF;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/bi;->a(Landroid/graphics/RectF;)V

    return v1
.end method

.method public final b()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->b:Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    return-object v0
.end method

.method public final d()Z
    .locals 4

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "PSPDFKit.MarkupAnnotations"

    const-string v3, "Exiting highlight editing mode."

    .line 1
    invoke-static {v2, v3, v1}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    .line 3
    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->b(Lcom/pspdfkit/internal/k1;)V

    return v0
.end method

.method protected f()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c()Lcom/pspdfkit/internal/w0;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/i1;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/i1;->removeOnAnnotationCreationModeSettingsChangeListener(Lcom/pspdfkit/ui/special_mode/manager/AnnotationManager$OnAnnotationCreationModeSettingsChangeListener;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/dm;->getAnnotationRenderingCoordinator()Lcom/pspdfkit/internal/w1;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/w1;->a(Lcom/pspdfkit/annotations/BaseRectsAnnotation;)V

    :cond_0
    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    :cond_1
    return-void
.end method

.method protected g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final h()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PSPDFKit.MarkupAnnotations"

    const-string v2, "Exiting highlight editing mode due to page recycling."

    .line 1
    invoke-static {v1, v2, v0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/bi;->f()V

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->c:Lcom/pspdfkit/internal/specialMode/handler/a;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/specialMode/handler/a;->c(Lcom/pspdfkit/internal/k1;)V

    return-void
.end method

.method protected i()Z
    .locals 1

    instance-of v0, p0, Lcom/pspdfkit/internal/jf;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected j()Z
    .locals 1

    instance-of v0, p0, Lcom/pspdfkit/internal/to;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final onAnnotationCreationModeSettingsChange(Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/bi;->k:Lcom/pspdfkit/annotations/BaseRectsAnnotation;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/pspdfkit/ui/special_mode/controller/AnnotationCreationController;->getAlpha()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/pspdfkit/annotations/Annotation;->setAlpha(F)V

    .line 3
    iget-object p1, p0, Lcom/pspdfkit/internal/bi;->h:Lcom/pspdfkit/internal/dm;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/am;->j()V

    :cond_0
    return-void
.end method

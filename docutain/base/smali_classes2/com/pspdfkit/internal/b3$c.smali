.class final Lcom/pspdfkit/internal/b3$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lio/reactivex/rxjava3/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/b3;->a(Landroid/content/Context;Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/internal/v3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/rxjava3/functions/Consumer;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/pspdfkit/internal/b3;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/pspdfkit/internal/v3;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/b3;Landroid/content/Context;Lcom/pspdfkit/internal/v3;)V
    .locals 0

    iput-object p1, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    iput-object p2, p0, Lcom/pspdfkit/internal/b3$c;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/pspdfkit/internal/b3$c;->c:Lcom/pspdfkit/internal/v3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 4

    .line 1
    check-cast p1, Lcom/pspdfkit/annotations/SoundAnnotation;

    const-string v0, "annotation"

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    invoke-static {v0}, Lcom/pspdfkit/internal/b3;->a(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/annotations/SoundAnnotation;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object p1, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    invoke-static {p1}, Lcom/pspdfkit/internal/b3;->b(Lcom/pspdfkit/internal/b3;)Lcom/pspdfkit/internal/a3;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    invoke-virtual {p1, v0}, Lcom/pspdfkit/internal/a3;->b(Lcom/pspdfkit/ui/audio/AudioPlaybackController;)V

    .line 125
    iget-object p1, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/b3;->isReady()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 126
    iget-object p1, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    .line 127
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 128
    new-instance v0, Lcom/pspdfkit/internal/f3;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/f3;-><init>(Lcom/pspdfkit/internal/b3;)V

    invoke-static {v0}, Lcom/pspdfkit/internal/eu;->a(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/b3$c;->a:Lcom/pspdfkit/internal/b3;

    iget-object v1, p0, Lcom/pspdfkit/internal/b3$c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/pspdfkit/internal/b3$c;->c:Lcom/pspdfkit/internal/v3;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/v3;->c()Z

    move-result v2

    iget-object v3, p0, Lcom/pspdfkit/internal/b3$c;->c:Lcom/pspdfkit/internal/v3;

    invoke-virtual {v3}, Lcom/pspdfkit/internal/v3;->a()I

    move-result v3

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/pspdfkit/internal/b3;->a(Landroid/content/Context;Lcom/pspdfkit/annotations/SoundAnnotation;ZI)V

    :cond_1
    :goto_0
    return-void
.end method

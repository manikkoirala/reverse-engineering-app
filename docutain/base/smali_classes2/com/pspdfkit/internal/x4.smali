.class Lcom/pspdfkit/internal/x4;
.super Lcom/pspdfkit/internal/z3;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DrawingShape:",
        "Lcom/pspdfkit/internal/y4;",
        ">",
        "Lcom/pspdfkit/internal/z3<",
        "TDrawingShape;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/y4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDrawingShape;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/z3;-><init>(Lcom/pspdfkit/internal/h4;)V

    return-void
.end method

.method private b(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->k()Lcom/pspdfkit/internal/ri;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    .line 7
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v3

    invoke-interface {v3}, Lcom/pspdfkit/internal/pf;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v3

    .line 8
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_1

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/pspdfkit/annotations/measurements/Scale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    new-instance v4, Lcom/pspdfkit/internal/ri;

    .line 12
    invoke-virtual {v0}, Lcom/pspdfkit/internal/ri;->a()Lcom/pspdfkit/annotations/measurements/MeasurementMode;

    move-result-object v0

    invoke-direct {v4, v3, v2, v0}, Lcom/pspdfkit/internal/ri;-><init>(Lcom/pspdfkit/annotations/measurements/Scale;Lcom/pspdfkit/annotations/measurements/FloatPrecision;Lcom/pspdfkit/annotations/measurements/MeasurementMode;)V

    .line 13
    invoke-virtual {v1, v4}, Lcom/pspdfkit/internal/h4;->a(Lcom/pspdfkit/internal/ri;)V

    const/4 v1, 0x1

    .line 18
    :cond_2
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 20
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/h4;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v5, v1

    :goto_0
    return v5
.end method


# virtual methods
.method protected a(Lcom/pspdfkit/annotations/Annotation;)Z
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/pspdfkit/internal/z3;->a(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result v0

    .line 2
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y4;->r()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v2

    const/4 v3, 0x1

    if-eq v1, v2, :cond_0

    .line 3
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y4;->r()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderStyle(Lcom/pspdfkit/annotations/BorderStyle;)V

    const/4 v0, 0x1

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y4;->p()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 7
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y4;->p()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffect(Lcom/pspdfkit/annotations/BorderEffect;)V

    const/4 v0, 0x1

    .line 10
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y4;->q()F

    move-result v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result v2

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    .line 11
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y4;->q()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderEffectIntensity(F)V

    const/4 v0, 0x1

    .line 14
    :cond_2
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/y4;->s()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 15
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/y4;->s()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    .line 16
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 17
    :goto_0
    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setBorderDashArray(Ljava/util/List;)V

    const/4 v0, 0x1

    .line 22
    :cond_4
    iget-object v1, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v1, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/h4;->k()Lcom/pspdfkit/internal/ri;

    move-result-object v1

    if-nez v1, :cond_5

    return v0

    .line 26
    :cond_5
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v2

    const/16 v4, 0x3e9

    invoke-virtual {v2, v4}, Lcom/pspdfkit/internal/p1;->c(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    .line 27
    sget v0, Lcom/pspdfkit/internal/ao;->a:I

    const-string v0, "annotation"

    .line 28
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1332
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v0

    .line 1334
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/mt;->a()Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/rxjava3/core/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/ui/fonts/Font;

    invoke-virtual {v2}, Lcom/pspdfkit/ui/fonts/Font;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1335
    invoke-virtual {v0, v4, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/String;)V

    .line 1339
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v0

    const/high16 v2, 0x41900000    # 18.0f

    .line 1341
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/16 v4, 0x3ea

    .line 1342
    invoke-virtual {v0, v4, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    .line 1346
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-interface {v0}, Lcom/pspdfkit/internal/pf;->getProperties()Lcom/pspdfkit/internal/p1;

    move-result-object v0

    .line 1348
    sget-object v2, Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextTextJustification;->CENTER:Lcom/pspdfkit/annotations/FreeTextAnnotation$FreeTextTextJustification;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    int-to-byte v2, v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    const/16 v4, 0x3ed

    .line 1349
    invoke-virtual {v0, v4, v2}, Lcom/pspdfkit/internal/p1;->a(ILjava/lang/Object;)V

    const/4 v0, 0x1

    .line 1350
    :cond_6
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->getMeasurementPrecision()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1351
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ri;->b()Lcom/pspdfkit/annotations/measurements/FloatPrecision;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/pspdfkit/internal/pf;->setMeasurementPrecision(Lcom/pspdfkit/annotations/measurements/FloatPrecision;)V

    const/4 v0, 0x1

    .line 1354
    :cond_7
    invoke-virtual {v1}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v2

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v4

    invoke-interface {v4}, Lcom/pspdfkit/internal/pf;->getMeasurementScale()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/pspdfkit/annotations/measurements/Scale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1355
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v0

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ri;->c()Lcom/pspdfkit/annotations/measurements/Scale;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/pspdfkit/internal/pf;->setMeasurementScale(Lcom/pspdfkit/annotations/measurements/Scale;)V

    const/4 v0, 0x1

    .line 1358
    :cond_8
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getContents()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v2, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v2}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1359
    iget-object v0, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast v0, Lcom/pspdfkit/internal/y4;

    invoke-virtual {v0}, Lcom/pspdfkit/internal/h4;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/pspdfkit/annotations/Annotation;->setContents(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move v3, v0

    :goto_1
    return v3
.end method

.method public a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z
    .locals 1

    .line 1360
    invoke-super {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/z3;->a(Lcom/pspdfkit/annotations/Annotation;Landroid/graphics/Matrix;FZ)Z

    move-result p2

    .line 1361
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p3, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/y4;->r()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p4

    const/4 v0, 0x1

    if-eq p3, p4, :cond_0

    .line 1362
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderStyle()Lcom/pspdfkit/annotations/BorderStyle;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/y4;->a(Lcom/pspdfkit/annotations/BorderStyle;)V

    const/4 p2, 0x1

    .line 1365
    :cond_0
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p3, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/y4;->p()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object p4

    if-eq p3, p4, :cond_1

    .line 1366
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffect()Lcom/pspdfkit/annotations/BorderEffect;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/y4;->a(Lcom/pspdfkit/annotations/BorderEffect;)V

    const/4 p2, 0x1

    .line 1369
    :cond_1
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p3, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/y4;->q()F

    move-result p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result p4

    cmpl-float p3, p3, p4

    if-eqz p3, :cond_2

    .line 1370
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderEffectIntensity()F

    move-result p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/y4;->c(F)V

    const/4 p2, 0x1

    .line 1373
    :cond_2
    iget-object p3, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p3, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p3}, Lcom/pspdfkit/internal/y4;->s()Ljava/util/List;

    move-result-object p3

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object p4

    invoke-static {p3, p4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_3

    .line 1374
    iget-object p2, p0, Lcom/pspdfkit/internal/z3;->a:Lcom/pspdfkit/internal/h4;

    check-cast p2, Lcom/pspdfkit/internal/y4;

    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getBorderDashArray()Ljava/util/List;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/pspdfkit/internal/y4;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_3
    move v0, p2

    .line 1378
    :goto_0
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/x4;->b(Lcom/pspdfkit/annotations/Annotation;)Z

    move-result p1

    or-int/2addr p1, v0

    return p1
.end method

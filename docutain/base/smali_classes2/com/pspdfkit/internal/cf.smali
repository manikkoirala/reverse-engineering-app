.class public final Lcom/pspdfkit/internal/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/internal/mk;
.implements Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;


# instance fields
.field private final b:Lcom/pspdfkit/annotations/Annotation;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

.field private final e:Lcom/pspdfkit/internal/qe;

.field private f:Lcom/pspdfkit/internal/fk;

.field private g:Ljava/util/ArrayList;

.field private final h:Lio/reactivex/rxjava3/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/rxjava3/subjects/ReplaySubject<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lio/reactivex/rxjava3/core/Scheduler;

.field private j:Lcom/pspdfkit/internal/nk;

.field private k:Z

.field private l:J


# direct methods
.method public static synthetic $r8$lambda$3JfCfRyrvcUMqrb58CVgqhlO3yw(Lcom/pspdfkit/internal/cf;Lcom/pspdfkit/internal/bf;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cf;->a(Lcom/pspdfkit/internal/bf;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$bPg-a70xFHxpKxrcWzyjxt_iDkQ(Lcom/pspdfkit/internal/cf;Ljava/util/concurrent/Callable;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cf;->a(Ljava/util/concurrent/Callable;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$jjWYP-lkWIIwufjBn9FaA5L1ous(Lcom/pspdfkit/internal/cf;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/cf;->v()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$ldDIOjr6d-OVaOj1DNuabi5zM6E(Lcom/pspdfkit/internal/cf;Lcom/pspdfkit/internal/fk;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/cf;->a(Lcom/pspdfkit/internal/fk;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;Lcom/pspdfkit/internal/qe;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 2
    iput-wide v0, p0, Lcom/pspdfkit/internal/cf;->l:J

    const-string v0, "context"

    .line 9
    invoke-static {p1, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotation"

    .line 10
    invoke-static {p2, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationPreferences"

    .line 11
    invoke-static {p3, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationProvider"

    .line 12
    invoke-static {p4, v0}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iput-object p2, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 14
    sget v0, Lcom/pspdfkit/R$string;->pspdf__annotation_type_instantComments:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/cf;->c:Ljava/lang/String;

    .line 15
    invoke-direct {p0, p2}, Lcom/pspdfkit/internal/cf;->a(Lcom/pspdfkit/annotations/Annotation;)V

    .line 16
    iput-object p3, p0, Lcom/pspdfkit/internal/cf;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    .line 17
    iput-object p4, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    const/4 p1, 0x1

    .line 18
    invoke-static {p1}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->createWithSize(I)Lio/reactivex/rxjava3/subjects/ReplaySubject;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    .line 19
    invoke-static {}, Lcom/pspdfkit/internal/gj;->v()Lcom/pspdfkit/internal/du;

    move-result-object p1

    check-cast p1, Lcom/pspdfkit/internal/u;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/u;->b()Lio/reactivex/rxjava3/core/Scheduler;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/cf;->i:Lio/reactivex/rxjava3/core/Scheduler;

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/bf;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    iget-object v1, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, p1, v1}, Lcom/pspdfkit/internal/qe;->a(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private synthetic a(Lcom/pspdfkit/internal/fk;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    .line 126
    invoke-virtual {p1}, Lcom/pspdfkit/internal/fk;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cf;->i()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 127
    invoke-virtual {v0, p1, v1, v2}, Lcom/pspdfkit/internal/qe;->a(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/concurrent/Callable;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    const-string v0, "comments"

    const-string v1, "argumentName"

    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 113
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 115
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/bf;

    .line 116
    new-instance v2, Lcom/pspdfkit/internal/sk;

    iget-object v3, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {v2, v1, v3}, Lcom/pspdfkit/internal/sk;-><init>(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 118
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-eqz p1, :cond_1

    .line 119
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method private a(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/pf;->addOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/pspdfkit/internal/cf;->k:Z

    return-void
.end method

.method private b(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/bf;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/cf;Ljava/util/concurrent/Callable;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/cf;->c(Ljava/util/concurrent/Callable;)V

    return-void
.end method

.method private c(Ljava/util/concurrent/Callable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lio/reactivex/rxjava3/core/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->i:Lio/reactivex/rxjava3/core/Scheduler;

    .line 2
    invoke-virtual {p1, v0}, Lio/reactivex/rxjava3/core/Observable;->subscribeOn(Lio/reactivex/rxjava3/core/Scheduler;)Lio/reactivex/rxjava3/core/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    .line 5
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda0;

    invoke-direct {v1, v0}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda0;-><init>(Lio/reactivex/rxjava3/subjects/ReplaySubject;)V

    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda1;

    invoke-direct {v2, v0}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda1;-><init>(Lio/reactivex/rxjava3/subjects/ReplaySubject;)V

    invoke-virtual {p1, v1, v2}, Lio/reactivex/rxjava3/core/Observable;->subscribe(Lio/reactivex/rxjava3/functions/Consumer;Lio/reactivex/rxjava3/functions/Consumer;)Lio/reactivex/rxjava3/disposables/Disposable;

    return-void
.end method

.method private v()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    iget-object v1, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v1}, Lcom/pspdfkit/internal/qe;->k(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "comments"

    const-string v2, "argumentName"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/pspdfkit/internal/bf;

    .line 56
    new-instance v3, Lcom/pspdfkit/internal/sk;

    iget-object v4, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {v3, v2, v4}, Lcom/pspdfkit/internal/sk;-><init>(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-eqz v0, :cond_1

    .line 59
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_1
    iput-object v1, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    return-object v1
.end method

.method private w()V
    .locals 1

    .line 1
    new-instance v0, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/cf;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/cf;->c(Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    sget-object v1, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->NOTE:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    iget-object v2, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 123
    invoke-virtual {v2}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v2

    invoke-interface {v2}, Lcom/pspdfkit/internal/pf;->getVariant()Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;

    move-result-object v2

    .line 124
    invoke-interface {v0, v1, v2, p1}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->setColor(Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;Lcom/pspdfkit/ui/special_mode/controller/AnnotationToolVariant;I)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;I)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V
    .locals 1

    .line 58
    iget-object p1, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {p1, v0, p2}, Lcom/pspdfkit/internal/r1;->appendAnnotationState(Lcom/pspdfkit/annotations/Annotation;Lcom/pspdfkit/annotations/note/AnnotationStateChange;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/hk;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Ljava/lang/String;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-ne p1, v0, :cond_0

    .line 60
    invoke-interface {p1, p2}, Lcom/pspdfkit/internal/ik;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/uk;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/pspdfkit/internal/cf;->j:Lcom/pspdfkit/internal/nk;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;)V"
        }
    .end annotation

    .line 120
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ik;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;)Z
    .locals 2

    .line 3
    instance-of v0, p1, Lcom/pspdfkit/internal/sk;

    if-eqz v0, :cond_0

    .line 4
    check-cast p1, Lcom/pspdfkit/internal/sk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sk;->n()Lcom/pspdfkit/internal/bf;

    move-result-object p1

    const-string v0, "comment"

    const-string v1, "argumentName"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 56
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/internal/bf;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cf;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/pspdfkit/internal/ik;)Z
    .locals 2

    .line 2
    instance-of v0, p1, Lcom/pspdfkit/internal/sk;

    if-eqz v0, :cond_0

    .line 4
    check-cast p1, Lcom/pspdfkit/internal/sk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/sk;->n()Lcom/pspdfkit/internal/bf;

    move-result-object p1

    const-string v0, "comment"

    const-string v1, "argumentName"

    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 56
    invoke-static {p1, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    invoke-virtual {p1}, Lcom/pspdfkit/internal/bf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/pspdfkit/internal/cf;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/cf;Lcom/pspdfkit/internal/bf;)V

    invoke-direct {p0, v0}, Lcom/pspdfkit/internal/cf;->b(Ljava/util/concurrent/Callable;)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final c(Lcom/pspdfkit/internal/ik;)V
    .locals 0

    return-void
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/pspdfkit/internal/ik;)V
    .locals 0

    return-void
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/pspdfkit/internal/ik;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ik;

    return-object v0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->e:Lcom/pspdfkit/internal/qe;

    iget-object v2, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0, v2}, Lcom/pspdfkit/internal/qe;->k(Lcom/pspdfkit/annotations/Annotation;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "comments"

    const-string v3, "argumentName"

    .line 4
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 55
    invoke-static {v0, v2, v3}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/bf;

    .line 58
    new-instance v4, Lcom/pspdfkit/internal/sk;

    iget-object v5, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {v4, v3, v5}, Lcom/pspdfkit/internal/sk;-><init>(Lcom/pspdfkit/internal/bf;Lcom/pspdfkit/annotations/Annotation;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-eqz v0, :cond_2

    .line 61
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_2
    iput-object v2, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    .line 63
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {v0, v2}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/ik;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->d:Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;

    invoke-interface {v0}, Lcom/pspdfkit/annotations/defaults/AnnotationPreferencesManager;->getAnnotationCreator()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public final j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 9
    iput-object v1, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    .line 14
    iget-object v1, p0, Lcom/pspdfkit/internal/cf;->j:Lcom/pspdfkit/internal/nk;

    if-eqz v1, :cond_1

    .line 15
    check-cast v1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v1, p0}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/mk;)V

    .line 17
    :cond_1
    new-instance v1, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, v0}, Lcom/pspdfkit/internal/cf$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/cf;Lcom/pspdfkit/internal/fk;)V

    invoke-direct {p0, v1}, Lcom/pspdfkit/internal/cf;->b(Ljava/util/concurrent/Callable;)V

    return-void
.end method

.method public final n()Lcom/pspdfkit/internal/ik;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/cf;->i()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/pspdfkit/internal/fk;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 5
    new-instance v0, Lcom/pspdfkit/internal/fk;

    iget-object v4, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 6
    iget-wide v5, p0, Lcom/pspdfkit/internal/cf;->l:J

    add-long/2addr v5, v2

    iput-wide v5, p0, Lcom/pspdfkit/internal/cf;->l:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 7
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/pspdfkit/internal/fk;-><init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    goto :goto_0

    :cond_0
    if-nez v0, :cond_2

    .line 12
    new-instance v0, Lcom/pspdfkit/internal/fk;

    iget-object v4, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    .line 13
    iget-wide v5, p0, Lcom/pspdfkit/internal/cf;->l:J

    add-long/2addr v5, v2

    iput-wide v5, p0, Lcom/pspdfkit/internal/cf;->l:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 14
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/pspdfkit/internal/fk;-><init>(Lcom/pspdfkit/annotations/Annotation;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    .line 16
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->j:Lcom/pspdfkit/internal/nk;

    if-eqz v0, :cond_1

    .line 17
    check-cast v0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/mk;)V

    .line 22
    :cond_1
    invoke-direct {p0}, Lcom/pspdfkit/internal/cf;->w()V

    .line 25
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onAnnotationCreated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 0

    return-void
.end method

.method public final onAnnotationRemoved(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    .line 3
    iget-boolean p1, p0, Lcom/pspdfkit/internal/cf;->k:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 7
    iput-boolean p1, p0, Lcom/pspdfkit/internal/cf;->k:Z

    .line 9
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/pspdfkit/internal/pf;->removeOnAnnotationUpdatedListener(Lcom/pspdfkit/annotations/AnnotationProvider$OnAnnotationUpdatedListener;)V

    .line 10
    iget-object p1, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    invoke-virtual {p1}, Lio/reactivex/rxjava3/subjects/ReplaySubject;->onComplete()V

    .line 11
    :goto_0
    iget-object p1, p0, Lcom/pspdfkit/internal/cf;->j:Lcom/pspdfkit/internal/nk;

    if-eqz p1, :cond_1

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    :cond_1
    return-void
.end method

.method public final onAnnotationUpdated(Lcom/pspdfkit/annotations/Annotation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    if-ne p1, v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/cf;->w()V

    :cond_0
    return-void
.end method

.method public final onAnnotationZOrderChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/pspdfkit/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final p()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final q()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->b:Lcom/pspdfkit/annotations/Annotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/Annotation;->getColor()I

    move-result v0

    return v0
.end method

.method public final r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/pspdfkit/internal/cf;->f:Lcom/pspdfkit/internal/fk;

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->j:Lcom/pspdfkit/internal/nk;

    if-eqz v0, :cond_0

    .line 3
    check-cast v0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/mk;)V

    .line 5
    :cond_0
    invoke-direct {p0}, Lcom/pspdfkit/internal/cf;->w()V

    return-void
.end method

.method public final t()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final u()Lio/reactivex/rxjava3/core/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/rxjava3/core/Observable<",
            "Ljava/util/List<",
            "Lcom/pspdfkit/internal/ik;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/pspdfkit/internal/cf;->w()V

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/pspdfkit/internal/cf;->h:Lio/reactivex/rxjava3/subjects/ReplaySubject;

    return-object v0
.end method

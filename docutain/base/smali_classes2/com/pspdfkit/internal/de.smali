.class public final Lcom/pspdfkit/internal/de;
.super Lcom/pspdfkit/internal/f0;
.source "SourceFile"

# interfaces
.implements Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/f0<",
        "Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;",
        ">;",
        "Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration$Builder;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    sget-object v0, Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;->INK:Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    .line 2
    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 3
    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->FILL_COLOR:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 4
    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->THICKNESS:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 5
    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_NOTE:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 6
    sget-object v2, Lcom/pspdfkit/annotations/configuration/AnnotationProperty;->ANNOTATION_ALPHA:Lcom/pspdfkit/annotations/configuration/AnnotationProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    .line 7
    invoke-direct {p0, p1, v0, v1}, Lcom/pspdfkit/internal/f0;-><init>(Landroid/content/Context;Lcom/pspdfkit/ui/special_mode/controller/AnnotationTool;[Lcom/pspdfkit/annotations/configuration/AnnotationProperty;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/pspdfkit/annotations/configuration/AnnotationConfiguration;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/pspdfkit/internal/de;->build()Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/pspdfkit/annotations/configuration/InkAnnotationConfiguration;
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/pspdfkit/internal/f0;->c()V

    .line 3
    new-instance v0, Lcom/pspdfkit/internal/ee;

    invoke-virtual {p0}, Lcom/pspdfkit/internal/e0;->a()Lcom/pspdfkit/internal/h0;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/pspdfkit/internal/ee;-><init>(Lcom/pspdfkit/internal/h0;)V

    return-object v0
.end method

.class final Lcom/pspdfkit/internal/pc$a;
.super Lcom/pspdfkit/listeners/SimpleDocumentListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pspdfkit/internal/pc;->a(Lcom/pspdfkit/internal/os;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/pspdfkit/internal/pc;


# direct methods
.method constructor <init>(Lcom/pspdfkit/internal/pc;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/pspdfkit/internal/pc$a;->b:Lcom/pspdfkit/internal/pc;

    invoke-direct {p0}, Lcom/pspdfkit/listeners/SimpleDocumentListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageChanged(Lcom/pspdfkit/document/PdfDocument;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/pspdfkit/internal/pc$a;->b:Lcom/pspdfkit/internal/pc;

    invoke-static {p1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetf(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm;->getState()Lcom/pspdfkit/internal/dm$e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/pspdfkit/internal/dm$e;->c()I

    move-result p1

    if-eq p2, p1, :cond_1

    iget-object p1, p0, Lcom/pspdfkit/internal/pc$a;->b:Lcom/pspdfkit/internal/pc;

    invoke-static {p1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetf(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/dm;

    move-result-object p1

    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, p2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2
    iget-object p1, p0, Lcom/pspdfkit/internal/pc$a;->b:Lcom/pspdfkit/internal/pc;

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgeti(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/annotations/FreeTextAnnotation;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-static {p1}, Lcom/pspdfkit/internal/pc;->-$$Nest$fgetf(Lcom/pspdfkit/internal/pc;)Lcom/pspdfkit/internal/dm;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/internal/dm;->getPageEditor()Lcom/pspdfkit/internal/am;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0}, Lcom/pspdfkit/internal/am;->a(ZZ)Z

    const/4 p2, 0x0

    .line 6
    invoke-static {p1, p2}, Lcom/pspdfkit/internal/pc;->-$$Nest$fputi(Lcom/pspdfkit/internal/pc;Lcom/pspdfkit/annotations/FreeTextAnnotation;)V

    :cond_1
    :goto_0
    return-void
.end method

.class public final Lcom/pspdfkit/internal/y8;
.super Lcom/pspdfkit/internal/ql;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/ql<",
        "Lcom/pspdfkit/internal/w8;",
        ">;"
    }
.end annotation


# instance fields
.field private d:Lcom/pspdfkit/internal/z8;

.field private e:Lcom/pspdfkit/internal/a9;

.field private final f:Lcom/pspdfkit/internal/c9;

.field private final g:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/pspdfkit/internal/nh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/pspdfkit/internal/nh<",
            "Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/ql;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/y8;->g:Lcom/pspdfkit/internal/nh;

    .line 7
    new-instance v0, Lcom/pspdfkit/internal/nh;

    invoke-direct {v0}, Lcom/pspdfkit/internal/nh;-><init>()V

    iput-object v0, p0, Lcom/pspdfkit/internal/y8;->h:Lcom/pspdfkit/internal/nh;

    .line 14
    new-instance v0, Lcom/pspdfkit/internal/c9;

    invoke-direct {v0, p1}, Lcom/pspdfkit/internal/c9;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/pspdfkit/internal/y8;->f:Lcom/pspdfkit/internal/c9;

    .line 15
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {p1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/pspdfkit/internal/rl;)V
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->f:Lcom/pspdfkit/internal/c9;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/c9;->a(Lcom/pspdfkit/internal/rl;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/zf;Lcom/pspdfkit/configuration/PdfConfiguration;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 1
    new-instance p2, Lcom/pspdfkit/internal/z8;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/pspdfkit/internal/z8;-><init>(Landroid/content/Context;Lcom/pspdfkit/internal/zf;)V

    iput-object p2, p0, Lcom/pspdfkit/internal/y8;->d:Lcom/pspdfkit/internal/z8;

    .line 2
    new-instance p1, Lcom/pspdfkit/internal/a9;

    invoke-direct {p1, p2}, Lcom/pspdfkit/internal/a9;-><init>(Lcom/pspdfkit/internal/z8;)V

    iput-object p1, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/y8;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;

    .line 6
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/a9;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/pspdfkit/internal/y8;->h:Lcom/pspdfkit/internal/nh;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/nh;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;

    .line 9
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->d:Lcom/pspdfkit/internal/z8;

    invoke-virtual {v0, p2}, Lcom/pspdfkit/internal/z8;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 12
    iput-object p1, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    .line 13
    iput-object p1, p0, Lcom/pspdfkit/internal/y8;->d:Lcom/pspdfkit/internal/z8;

    .line 16
    :cond_2
    iget-object p1, p0, Lcom/pspdfkit/internal/y8;->f:Lcom/pspdfkit/internal/c9;

    iget-object p2, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/internal/c9;->setPresenter(Lcom/pspdfkit/internal/s8;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/a9;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->h:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->a(Ljava/lang/Object;)V

    .line 22
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->d:Lcom/pspdfkit/internal/z8;

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z8;->a(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->g:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->e:Lcom/pspdfkit/internal/a9;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/a9;->b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewModeChangeListener;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->h:Lcom/pspdfkit/internal/nh;

    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/nh;->b(Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lcom/pspdfkit/internal/y8;->d:Lcom/pspdfkit/internal/z8;

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/pspdfkit/internal/z8;->b(Lcom/pspdfkit/ui/documentinfo/OnDocumentInfoViewSaveListener;)V

    :cond_0
    return-void
.end method

.method public getTabButtonId()I
    .locals 1

    .line 1
    sget v0, Lcom/pspdfkit/R$id;->pspdf__menu_pdf_outline_view_document_info:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/pspdfkit/R$string;->pspdf__document_info:I

    const/4 v2, 0x0

    .line 2
    invoke-static {v0, v1, v2}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/pspdfkit/internal/wt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field private final a:Lcom/pspdfkit/forms/TextFormElement;


# direct methods
.method public constructor <init>(Lcom/pspdfkit/forms/TextFormElement;)V
    .locals 1

    const-string v0, "formElement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/pspdfkit/internal/wt;->a:Lcom/pspdfkit/forms/TextFormElement;

    return-void
.end method


# virtual methods
.method public final filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 6

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dest"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 4
    iget-object p2, p0, Lcom/pspdfkit/internal/wt;->a:Lcom/pspdfkit/forms/TextFormElement;

    .line 5
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    .line 7
    new-instance v0, Lcom/pspdfkit/datastructures/Range;

    sub-int v1, p6, p5

    invoke-direct {v0, p5, v1}, Lcom/pspdfkit/datastructures/Range;-><init>(II)V

    const-string v1, "<this>"

    .line 8
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "contents"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "change"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "range"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormElement;->getAnnotation()Lcom/pspdfkit/annotations/WidgetAnnotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/pspdfkit/annotations/Annotation;->getInternal()Lcom/pspdfkit/internal/pf;

    move-result-object v1

    invoke-interface {v1}, Lcom/pspdfkit/internal/pf;->getInternalDocument()Lcom/pspdfkit/internal/zf;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 96
    invoke-virtual {v1}, Lcom/pspdfkit/internal/zf;->h()Lcom/pspdfkit/internal/xf;

    move-result-object v1

    check-cast v1, Lcom/pspdfkit/internal/ig;

    invoke-virtual {v1}, Lcom/pspdfkit/internal/ig;->isJavaScriptEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    .line 100
    :cond_0
    new-instance v1, Lcom/pspdfkit/internal/jni/NativeTextRange;

    .line 101
    invoke-virtual {v0}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v3

    .line 102
    invoke-virtual {v0}, Lcom/pspdfkit/datastructures/Range;->getLength()I

    move-result v0

    .line 103
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 104
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 105
    invoke-direct {v1, v3, v0, v4, v5}, Lcom/pspdfkit/internal/jni/NativeTextRange;-><init>(IILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 111
    invoke-virtual {p2}, Lcom/pspdfkit/forms/TextFormElement;->getFormField()Lcom/pspdfkit/forms/TextFormField;

    move-result-object p2

    invoke-virtual {p2}, Lcom/pspdfkit/forms/FormField;->getInternal()Lcom/pspdfkit/internal/tf;

    move-result-object p2

    invoke-interface {p2}, Lcom/pspdfkit/internal/tf;->getNativeFormControl()Lcom/pspdfkit/internal/jni/NativeFormControl;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, p3, p1, v1, v0}, Lcom/pspdfkit/internal/jni/NativeFormControl;->executeKeystrokeEventForTextSelection(Ljava/lang/String;Ljava/lang/String;Lcom/pspdfkit/internal/jni/NativeTextRange;Z)Lcom/pspdfkit/internal/jni/NativeJSResult;

    move-result-object p2

    const-string p3, "formField.internal.nativ\u2026ge,\n        isFinal\n    )"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getError()Lcom/pspdfkit/internal/jni/NativeJSError;

    move-result-object p3

    if-nez p3, :cond_4

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getEvent()Lcom/pspdfkit/internal/jni/NativeJSEvent;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/pspdfkit/internal/jni/NativeJSEvent;->getRc()Z

    move-result p3

    if-nez p3, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    goto :goto_1

    .line 122
    :cond_2
    new-instance p3, Lcom/pspdfkit/internal/qg;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getValue()Lcom/pspdfkit/internal/jni/NativeJSValue;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSValue;->getStringValue()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_3
    move-object p2, v2

    :goto_0
    invoke-direct {p3, p2, v2}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 123
    :cond_4
    :goto_1
    new-instance p3, Lcom/pspdfkit/internal/qg;

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSResult;->getError()Lcom/pspdfkit/internal/jni/NativeJSError;

    move-result-object p2

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/pspdfkit/internal/jni/NativeJSError;->getMessage()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_5
    move-object p2, v2

    :goto_2
    invoke-direct {p3, v2, p2}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 124
    :cond_6
    :goto_3
    new-instance p2, Lcom/pspdfkit/internal/qg;

    invoke-virtual {v0}, Lcom/pspdfkit/datastructures/Range;->getStartPosition()I

    move-result v1

    invoke-virtual {v0}, Lcom/pspdfkit/datastructures/Range;->getEndPosition()I

    move-result v0

    invoke-static {p3, v1, v0, p1}, Lkotlin/text/StringsKt;->replaceRange(Ljava/lang/CharSequence;IILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, v2}, Lcom/pspdfkit/internal/qg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p3, p2

    .line 125
    :goto_4
    invoke-virtual {p3}, Lcom/pspdfkit/internal/qg;->a()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_8

    .line 133
    invoke-static {p4, p5, p6, p1}, Lkotlin/text/StringsKt;->replaceRange(Ljava/lang/CharSequence;IILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 137
    iget-object p1, p0, Lcom/pspdfkit/internal/wt;->a:Lcom/pspdfkit/forms/TextFormElement;

    invoke-virtual {p1, p2}, Lcom/pspdfkit/forms/TextFormElement;->setText(Ljava/lang/String;)Z

    goto :goto_5

    :cond_7
    return-object v2

    .line 147
    :cond_8
    :goto_5
    invoke-interface {p4, p5, p6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/pspdfkit/internal/kh;
.super Lcom/pspdfkit/internal/o0;
.source "SourceFile"


# static fields
.field private static g:Z = false

.field private static final h:Landroid/graphics/Paint;

.field private static final i:Landroid/graphics/Paint;

.field private static final j:Landroid/graphics/Paint;

.field private static final k:Landroid/graphics/Paint;


# instance fields
.field private final c:Lcom/pspdfkit/annotations/actions/ActionResolver;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/kh;->h:Landroid/graphics/Paint;

    .line 4
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/kh;->i:Landroid/graphics/Paint;

    .line 8
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/kh;->j:Landroid/graphics/Paint;

    .line 11
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/pspdfkit/internal/kh;->k:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Lcom/pspdfkit/annotations/LinkAnnotation;Lcom/pspdfkit/annotations/actions/ActionResolver;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/o0;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    .line 3
    sget-object p1, Lcom/pspdfkit/internal/kh;->h:Landroid/graphics/Paint;

    iput-object p1, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    .line 4
    sget-object p1, Lcom/pspdfkit/internal/kh;->i:Landroid/graphics/Paint;

    iput-object p1, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/pspdfkit/internal/kh;->f:Z

    .line 7
    iput-object p2, p0, Lcom/pspdfkit/internal/kh;->c:Lcom/pspdfkit/annotations/actions/ActionResolver;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    sget-boolean v0, Lcom/pspdfkit/internal/kh;->g:Z

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/x5;->a()Lcom/pspdfkit/internal/f2;

    move-result-object v0

    const/4 v1, 0x1

    .line 4
    sput-boolean v1, Lcom/pspdfkit/internal/kh;->g:Z

    .line 6
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 7
    sget-object v2, Lcom/pspdfkit/internal/kh;->h:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 8
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 9
    iget v3, v0, Lcom/pspdfkit/internal/f2;->i:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 11
    sget-object v3, Lcom/pspdfkit/internal/kh;->i:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 12
    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 13
    iget v4, v0, Lcom/pspdfkit/internal/f2;->j:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    sget-object v4, Lcom/pspdfkit/internal/kh;->j:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 16
    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 17
    iget v5, v0, Lcom/pspdfkit/internal/f2;->k:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 19
    sget-object v4, Lcom/pspdfkit/internal/kh;->k:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 21
    iget v0, v0, Lcom/pspdfkit/internal/f2;->l:I

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    iput-object v2, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    .line 23
    iput-object v3, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    .line 26
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v0, 0x40000000    # 2.0f

    mul-float p1, p1, v0

    .line 29
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->b:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, p1, p1, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 30
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->b:Lcom/pspdfkit/utils/PageRect;

    invoke-virtual {v0}, Lcom/pspdfkit/utils/PageRect;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, p1, p1, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/o0;->a:Lcom/pspdfkit/annotations/Annotation;

    instance-of v1, v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    if-nez v1, :cond_0

    return-void

    .line 2
    :cond_0
    check-cast v0, Lcom/pspdfkit/annotations/LinkAnnotation;

    invoke-virtual {v0}, Lcom/pspdfkit/annotations/LinkAnnotation;->getAction()Lcom/pspdfkit/annotations/actions/Action;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 4
    :cond_1
    iget-object v1, p0, Lcom/pspdfkit/internal/kh;->c:Lcom/pspdfkit/annotations/actions/ActionResolver;

    new-instance v2, Lcom/pspdfkit/annotations/actions/ActionSender;

    iget-object v3, p0, Lcom/pspdfkit/internal/o0;->a:Lcom/pspdfkit/annotations/Annotation;

    invoke-direct {v2, v3}, Lcom/pspdfkit/annotations/actions/ActionSender;-><init>(Lcom/pspdfkit/annotations/Annotation;)V

    invoke-interface {v1, v0, v2}, Lcom/pspdfkit/annotations/actions/ActionResolver;->executeAction(Lcom/pspdfkit/annotations/actions/Action;Lcom/pspdfkit/annotations/actions/ActionSender;)V

    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/kh;->f:Z

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/kh;->j:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/kh;->k:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    return-void
.end method

.method public final e()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/pspdfkit/internal/kh;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/kh;->f:Z

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/kh;->h:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    .line 4
    sget-object v0, Lcom/pspdfkit/internal/kh;->i:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-boolean v0, p0, Lcom/pspdfkit/internal/kh;->f:Z

    .line 2
    sget-object v0, Lcom/pspdfkit/internal/kh;->h:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->d:Landroid/graphics/Paint;

    .line 3
    sget-object v0, Lcom/pspdfkit/internal/kh;->i:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/pspdfkit/internal/kh;->e:Landroid/graphics/Paint;

    return-void
.end method

.class public final Lcom/pspdfkit/internal/y5;
.super Lcom/pspdfkit/internal/wk;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/pspdfkit/internal/y5$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/pspdfkit/internal/wk<",
        "Lcom/pspdfkit/internal/ik;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/EditText;

.field private final e:Landroid/widget/LinearLayout;

.field private final f:Landroid/widget/Button;

.field private final g:Landroid/widget/Button;

.field private final h:Landroid/widget/LinearLayout;

.field private final i:Landroid/widget/LinearLayout;

.field private final j:Landroid/widget/LinearLayout;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/TextView;

.field private final u:Landroid/widget/TextView;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/view/View;

.field private x:Z


# direct methods
.method public static synthetic $r8$lambda$9DwQCTseibm7KEFi2FxvFnYfBec(Lcom/pspdfkit/internal/y5;)V
    .locals 0

    invoke-direct {p0}, Lcom/pspdfkit/internal/y5;->b()V

    return-void
.end method

.method public static synthetic $r8$lambda$KcTy4B9qHEzhWhIJpFj0bdvOEVk(Lcom/pspdfkit/internal/y5;Ljava/util/Set;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/pspdfkit/internal/y5;->a(Ljava/util/Set;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$YINan4Ye642LVVG-oHRrPn2AolU(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/y5;->b(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$a8rgAajcbCC3El-ZdE_BwOcNQ1s(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$bDseyqDYhPtoMwWTGO__xFDZMVM(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/jk;Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$hoWJkGmlIpGLaVr92hK_j6etxBs(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/MenuItem;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$m8KzngIzy7Yreh5myRvcH589PcE(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$t4x0rgylwoGgQfU1UXQ-4KlBPjw(Lcom/pspdfkit/internal/y5;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/y5;->a(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/pspdfkit/internal/wk;-><init>(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/pspdfkit/internal/y5;->x:Z

    .line 6
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_author_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->a:Landroid/widget/TextView;

    .line 7
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_created_date:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->b:Landroid/widget/TextView;

    .line 8
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_options_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    .line 10
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_content:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    .line 12
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_explicit_editing_controls_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->e:Landroid/widget/LinearLayout;

    .line 13
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_cancel_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->f:Landroid/widget/Button;

    .line 14
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_editor_item_save_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->g:Landroid/widget/Button;

    .line 16
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_reviews_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->h:Landroid/widget/LinearLayout;

    .line 17
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_review_state_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->i:Landroid/widget/LinearLayout;

    .line 18
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_status_details:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->j:Landroid/widget/LinearLayout;

    .line 20
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_status_accepted_text_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    .line 21
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_status_completed_text_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    .line 22
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_status_cancelled_text_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    .line 23
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_status_rejected_text_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    .line 25
    sget v0, Lcom/pspdfkit/R$id;->pspdf__accepted_authors_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->o:Landroid/widget/TextView;

    .line 26
    sget v0, Lcom/pspdfkit/R$id;->pspdf__completed_authors_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->p:Landroid/widget/TextView;

    .line 27
    sget v0, Lcom/pspdfkit/R$id;->pspdf__cancelled_authors_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->q:Landroid/widget/TextView;

    .line 28
    sget v0, Lcom/pspdfkit/R$id;->pspdf__rejected_authors_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->r:Landroid/widget/TextView;

    .line 30
    sget v0, Lcom/pspdfkit/R$id;->pspdf__accepted_authors_text_box:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->s:Landroid/widget/TextView;

    .line 31
    sget v0, Lcom/pspdfkit/R$id;->pspdf__completed_authors_text_box:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->t:Landroid/widget/TextView;

    .line 32
    sget v0, Lcom/pspdfkit/R$id;->pspdf__cancelled_authors_text_box:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->u:Landroid/widget/TextView;

    .line 33
    sget v0, Lcom/pspdfkit/R$id;->pspdf__rejected_authors_text_box:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/pspdfkit/internal/y5;->v:Landroid/widget/TextView;

    .line 34
    sget v0, Lcom/pspdfkit/R$id;->pspdf__note_item_bottom_padding:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/pspdfkit/internal/y5;->w:Landroid/view/View;

    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 224
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 225
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    const-string v2, ", "

    .line 227
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private synthetic a(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    .line 173
    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/uk;->f()V

    .line 174
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/jk;Landroid/view/View;Z)V
    .locals 0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 169
    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/uk;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 171
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    :cond_0
    return-void
.end method

.method private static synthetic a(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 175
    check-cast p0, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p0, p1}, Lcom/pspdfkit/internal/uk;->b(Lcom/pspdfkit/internal/ik;)V

    :cond_0
    return-void
.end method

.method private synthetic a(Ljava/util/Set;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/View;)V
    .locals 3

    .line 176
    new-instance p4, Landroid/widget/PopupMenu;

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    invoke-direct {p4, v0, v1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 177
    invoke-virtual {p4}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 178
    sget v1, Lcom/pspdfkit/R$menu;->pspdf__menu_note_annotation_editor_options:I

    invoke-virtual {p4}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 180
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/pspdfkit/internal/kk;

    .line 181
    invoke-virtual {p4}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {v0}, Lcom/pspdfkit/internal/kk;->a()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 183
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 186
    :cond_1
    new-instance p1, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0, p2, p3}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda1;-><init>(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;)V

    invoke-virtual {p4, p1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 206
    invoke-virtual {p4}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method private synthetic a(Z)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->g:Landroid/widget/Button;

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private synthetic a(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;Landroid/view/MenuItem;)Z
    .locals 4

    .line 207
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 208
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 211
    :cond_0
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_delete_reply:I

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    .line 212
    sget-object p3, Lcom/pspdfkit/internal/kk;->d:Lcom/pspdfkit/internal/kk;

    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/kk;)V

    return v3

    .line 215
    :cond_1
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_set_reply_status:I

    if-ne v1, v2, :cond_2

    .line 216
    sget-object p3, Lcom/pspdfkit/internal/kk;->c:Lcom/pspdfkit/internal/kk;

    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/kk;)V

    return v3

    .line 219
    :cond_2
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result p3

    sget v1, Lcom/pspdfkit/R$id;->pspdf__note_editor_option_share:I

    if-ne p3, v1, :cond_3

    .line 220
    sget-object p3, Lcom/pspdfkit/internal/kk;->b:Lcom/pspdfkit/internal/kk;

    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1, p2, p3}, Lcom/pspdfkit/internal/uk;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/kk;)V

    :cond_3
    return v0
.end method

.method private b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    const/4 v1, 0x0

    .line 3
    invoke-static {v0, v1}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;Lcom/pspdfkit/internal/pg$d;)V

    return-void
.end method

.method private b(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;)V
    .locals 3

    const-string v0, "contentCard"

    const-string v1, "argumentName"

    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 58
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "adapterCallbacks"

    .line 60
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {p2, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 112
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->a()Ljava/util/Set;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 117
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    new-instance v2, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda0;-><init>(Lcom/pspdfkit/internal/y5;Ljava/util/Set;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private synthetic b(Lcom/pspdfkit/internal/jk;Landroid/view/View;)V
    .locals 0

    .line 4
    check-cast p1, Lcom/pspdfkit/internal/uk;

    invoke-virtual {p1}, Lcom/pspdfkit/internal/uk;->d()V

    .line 5
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 222
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/pspdfkit/internal/pg;->b(Landroid/view/View;)V

    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;)V
    .locals 8

    .line 3
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->b()Z

    move-result v0

    .line 4
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 5
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    .line 6
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/pspdfkit/R$string;->pspdf__hint_add_your_comment:I

    const/4 v4, 0x0

    .line 7
    invoke-static {v2, v3, v4}, Lcom/pspdfkit/internal/rh;->a(Landroid/content/Context;ILandroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 8
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 11
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->f()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 15
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->e:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 16
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->a:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 17
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 18
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->c:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 19
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 21
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->w:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 22
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 23
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    new-instance v0, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p2}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda2;-><init>(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_9

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 35
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->a:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 37
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->b:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/pspdfkit/internal/y5;->b(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;)V

    .line 41
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->g()Ljava/lang/String;

    move-result-object v1

    .line 42
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    invoke-virtual {v5, v4}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 44
    iget-object v4, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    new-instance v5, Lcom/pspdfkit/internal/y5$a;

    new-instance v6, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda3;

    invoke-direct {v6, p0}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda3;-><init>(Lcom/pspdfkit/internal/y5;)V

    invoke-direct {v5, p1, p2, v6}, Lcom/pspdfkit/internal/y5$a;-><init>(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/y5$a$a;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 47
    iget-boolean v4, p0, Lcom/pspdfkit/internal/y5;->x:Z

    if-eqz v4, :cond_2

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->d:Landroid/widget/EditText;

    new-instance v4, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda4;

    invoke-direct {v4, p0}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda4;-><init>(Lcom/pspdfkit/internal/y5;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 55
    :cond_1
    iput-boolean v3, p0, Lcom/pspdfkit/internal/y5;->x:Z

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->g:Landroid/widget/Button;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v4, 0x1

    xor-int/2addr v1, v4

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 61
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->h()Z

    move-result v0

    .line 62
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    const/4 v5, 0x0

    goto :goto_0

    :cond_3
    const/16 v5, 0x8

    :goto_0
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->w:Landroid/view/View;

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->g:Landroid/widget/Button;

    new-instance v1, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda5;-><init>(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->f:Landroid/widget/Button;

    new-instance v1, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0, p2}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda6;-><init>(Lcom/pspdfkit/internal/y5;Lcom/pspdfkit/internal/jk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->m()Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;

    move-result-object v0

    if-nez v0, :cond_5

    .line 80
    iget-object p1, p0, Lcom/pspdfkit/internal/y5;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 83
    :cond_5
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 89
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 90
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 91
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 93
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->v:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 105
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->i:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda7;

    invoke-direct {v5, p2, p1}, Lcom/pspdfkit/internal/y5$$ExternalSyntheticLambda7;-><init>(Lcom/pspdfkit/internal/jk;Lcom/pspdfkit/internal/ik;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->getReviewNames()Ljava/util/Map;

    move-result-object p2

    .line 112
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 113
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v1, v4, :cond_6

    sget-object v1, Lcom/pspdfkit/annotations/note/AuthorState;->NONE:Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    :goto_2
    const/4 v1, 0x1

    .line 114
    :goto_3
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->h:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    :goto_4
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    sget-object v1, Lcom/pspdfkit/annotations/note/AuthorState;->ACCEPTED:Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz v5, :cond_a

    .line 117
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_a

    .line 118
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->k:Landroid/widget/TextView;

    .line 121
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->getCurrentUserState()Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v7

    if-ne v7, v1, :cond_9

    const/4 v1, 0x1

    goto :goto_5

    :cond_9
    const/4 v1, 0x0

    .line 122
    :goto_5
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 124
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->s:Landroid/widget/TextView;

    invoke-static {v5}, Lcom/pspdfkit/internal/y5;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 129
    :cond_a
    sget-object v1, Lcom/pspdfkit/annotations/note/AuthorState;->COMPLETED:Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz v5, :cond_c

    .line 130
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_c

    .line 131
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->l:Landroid/widget/TextView;

    .line 134
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->getCurrentUserState()Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v7

    if-ne v7, v1, :cond_b

    const/4 v1, 0x1

    goto :goto_6

    :cond_b
    const/4 v1, 0x0

    .line 135
    :goto_6
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 137
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->t:Landroid/widget/TextView;

    invoke-static {v5}, Lcom/pspdfkit/internal/y5;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :cond_c
    sget-object v1, Lcom/pspdfkit/annotations/note/AuthorState;->CANCELLED:Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz v5, :cond_e

    .line 143
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_e

    .line 144
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v6, p0, Lcom/pspdfkit/internal/y5;->m:Landroid/widget/TextView;

    .line 147
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->getCurrentUserState()Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v7

    if-ne v7, v1, :cond_d

    const/4 v1, 0x1

    goto :goto_7

    :cond_d
    const/4 v1, 0x0

    .line 148
    :goto_7
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 150
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->u:Landroid/widget/TextView;

    invoke-static {v5}, Lcom/pspdfkit/internal/y5;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, Lcom/pspdfkit/internal/y5;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 155
    :cond_e
    sget-object v1, Lcom/pspdfkit/annotations/note/AuthorState;->REJECTED:Lcom/pspdfkit/annotations/note/AuthorState;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    if-eqz p2, :cond_10

    .line 156
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_10

    .line 157
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v5, p0, Lcom/pspdfkit/internal/y5;->n:Landroid/widget/TextView;

    .line 160
    invoke-virtual {v0}, Lcom/pspdfkit/annotations/note/AnnotationReviewSummary;->getCurrentUserState()Lcom/pspdfkit/annotations/note/AuthorState;

    move-result-object v0

    if-ne v0, v1, :cond_f

    goto :goto_8

    :cond_f
    const/4 v4, 0x0

    .line 161
    :goto_8
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 163
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/pspdfkit/internal/y5;->v:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/pspdfkit/internal/y5;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object p2, p0, Lcom/pspdfkit/internal/y5;->r:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :cond_10
    iget-object p2, p0, Lcom/pspdfkit/internal/y5;->j:Landroid/widget/LinearLayout;

    invoke-interface {p1}, Lcom/pspdfkit/internal/ik;->k()Z

    move-result p1

    if-eqz p1, :cond_11

    const/4 v2, 0x0

    :cond_11
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_9
    return-void
.end method

.method public final a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;Z)V
    .locals 0

    .line 1
    iput-boolean p3, p0, Lcom/pspdfkit/internal/y5;->x:Z

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/pspdfkit/internal/y5;->a(Lcom/pspdfkit/internal/ik;Lcom/pspdfkit/internal/jk;)V

    return-void
.end method

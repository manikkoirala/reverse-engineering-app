.class public final enum Lcom/pspdfkit/LicenseFeature;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/pspdfkit/LicenseFeature;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANNOTATION_EDITING:Lcom/pspdfkit/LicenseFeature;

.field public static final enum ANNOTATION_REPLIES:Lcom/pspdfkit/LicenseFeature;

.field public static final enum COMPARISON:Lcom/pspdfkit/LicenseFeature;

.field public static final enum CONTENT_EDITING:Lcom/pspdfkit/LicenseFeature;

.field public static final enum DIGITAL_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

.field public static final enum DOCUMENT_EDITING:Lcom/pspdfkit/LicenseFeature;

.field public static final enum ELECTRONIC_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

.field public static final enum FORMS:Lcom/pspdfkit/LicenseFeature;

.field public static final enum IMAGE_DOCUMENT:Lcom/pspdfkit/LicenseFeature;

.field public static final enum INDEXED_FTS:Lcom/pspdfkit/LicenseFeature;

.field public static final enum MEASUREMENT_TOOLS:Lcom/pspdfkit/LicenseFeature;

.field public static final enum PDF_CREATION:Lcom/pspdfkit/LicenseFeature;

.field public static final enum READER_VIEW:Lcom/pspdfkit/LicenseFeature;

.field public static final enum REDACTION:Lcom/pspdfkit/LicenseFeature;

.field public static final enum WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/LicenseFeature;

.field private static final synthetic a:[Lcom/pspdfkit/LicenseFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 1
    new-instance v0, Lcom/pspdfkit/LicenseFeature;

    const-string v1, "PDF_CREATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/pspdfkit/LicenseFeature;->PDF_CREATION:Lcom/pspdfkit/LicenseFeature;

    .line 3
    new-instance v1, Lcom/pspdfkit/LicenseFeature;

    const-string v3, "DIGITAL_SIGNATURES"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/pspdfkit/LicenseFeature;->DIGITAL_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

    .line 5
    new-instance v3, Lcom/pspdfkit/LicenseFeature;

    const-string v5, "ANNOTATION_EDITING"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/pspdfkit/LicenseFeature;->ANNOTATION_EDITING:Lcom/pspdfkit/LicenseFeature;

    .line 7
    new-instance v5, Lcom/pspdfkit/LicenseFeature;

    const-string v7, "INDEXED_FTS"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/pspdfkit/LicenseFeature;->INDEXED_FTS:Lcom/pspdfkit/LicenseFeature;

    .line 9
    new-instance v7, Lcom/pspdfkit/LicenseFeature;

    const-string v9, "ANNOTATION_REPLIES"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/pspdfkit/LicenseFeature;->ANNOTATION_REPLIES:Lcom/pspdfkit/LicenseFeature;

    .line 11
    new-instance v9, Lcom/pspdfkit/LicenseFeature;

    const-string v11, "IMAGE_DOCUMENT"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/pspdfkit/LicenseFeature;->IMAGE_DOCUMENT:Lcom/pspdfkit/LicenseFeature;

    .line 13
    new-instance v11, Lcom/pspdfkit/LicenseFeature;

    const-string v13, "DOCUMENT_EDITING"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/pspdfkit/LicenseFeature;->DOCUMENT_EDITING:Lcom/pspdfkit/LicenseFeature;

    .line 15
    new-instance v13, Lcom/pspdfkit/LicenseFeature;

    const-string v15, "FORMS"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/pspdfkit/LicenseFeature;->FORMS:Lcom/pspdfkit/LicenseFeature;

    .line 17
    new-instance v15, Lcom/pspdfkit/LicenseFeature;

    const-string v14, "REDACTION"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/pspdfkit/LicenseFeature;->REDACTION:Lcom/pspdfkit/LicenseFeature;

    .line 19
    new-instance v14, Lcom/pspdfkit/LicenseFeature;

    const-string v12, "COMPARISON"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/pspdfkit/LicenseFeature;->COMPARISON:Lcom/pspdfkit/LicenseFeature;

    .line 21
    new-instance v12, Lcom/pspdfkit/LicenseFeature;

    const-string v10, "WEBKIT_HTML_CONVERSION"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/pspdfkit/LicenseFeature;->WEBKIT_HTML_CONVERSION:Lcom/pspdfkit/LicenseFeature;

    .line 23
    new-instance v10, Lcom/pspdfkit/LicenseFeature;

    const-string v8, "READER_VIEW"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/pspdfkit/LicenseFeature;->READER_VIEW:Lcom/pspdfkit/LicenseFeature;

    .line 25
    new-instance v8, Lcom/pspdfkit/LicenseFeature;

    const-string v6, "ELECTRONIC_SIGNATURES"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/pspdfkit/LicenseFeature;->ELECTRONIC_SIGNATURES:Lcom/pspdfkit/LicenseFeature;

    .line 27
    new-instance v6, Lcom/pspdfkit/LicenseFeature;

    const-string v4, "MEASUREMENT_TOOLS"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/pspdfkit/LicenseFeature;->MEASUREMENT_TOOLS:Lcom/pspdfkit/LicenseFeature;

    .line 29
    new-instance v4, Lcom/pspdfkit/LicenseFeature;

    const-string v2, "CONTENT_EDITING"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/pspdfkit/LicenseFeature;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/pspdfkit/LicenseFeature;->CONTENT_EDITING:Lcom/pspdfkit/LicenseFeature;

    const/16 v2, 0xf

    new-array v2, v2, [Lcom/pspdfkit/LicenseFeature;

    const/16 v16, 0x0

    aput-object v0, v2, v16

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    aput-object v4, v2, v6

    .line 30
    sput-object v2, Lcom/pspdfkit/LicenseFeature;->a:[Lcom/pspdfkit/LicenseFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/pspdfkit/LicenseFeature;
    .locals 1

    .line 1
    const-class v0, Lcom/pspdfkit/LicenseFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/pspdfkit/LicenseFeature;

    return-object p0
.end method

.method public static values()[Lcom/pspdfkit/LicenseFeature;
    .locals 1

    .line 1
    sget-object v0, Lcom/pspdfkit/LicenseFeature;->a:[Lcom/pspdfkit/LicenseFeature;

    invoke-virtual {v0}, [Lcom/pspdfkit/LicenseFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/pspdfkit/LicenseFeature;

    return-object v0
.end method

.class public final Lcom/pspdfkit/analytics/Analytics$Data;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/analytics/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# static fields
.field public static final ACTION:Ljava/lang/String; = "action"

.field public static final ACTION_TYPE:Ljava/lang/String; = "action_type"

.field public static final ANNOTATION_PROCESSING_MODE:Ljava/lang/String; = "processing_mode"

.field public static final ANNOTATION_TOOL:Ljava/lang/String; = "annotation_tool"

.field public static final ANNOTATION_TYPE:Ljava/lang/String; = "annotation_type"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final LENGTH:Ljava/lang/String; = "length"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final PAGE_INDEX:Ljava/lang/String; = "page_index"

.field public static final SEARCH_TYPE:Ljava/lang/String; = "search_type"

.field public static final SORT:Ljava/lang/String; = "sort"

.field public static final TARGET_PAGE_INDEX:Ljava/lang/String; = "target_page_index"

.field public static final VALUE:Ljava/lang/String; = "value"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

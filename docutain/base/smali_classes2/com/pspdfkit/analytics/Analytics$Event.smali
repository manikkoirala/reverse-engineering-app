.class public final Lcom/pspdfkit/analytics/Analytics$Event;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/analytics/Analytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Event"
.end annotation


# static fields
.field public static final ADD_BOOKMARK:Ljava/lang/String; = "add_bookmark"

.field public static final CANCEL_ANNOTATION_CREATOR_DIALOG:Ljava/lang/String; = "cancel_annotation_creator_dialog"

.field public static final CHANGE_PAGE:Ljava/lang/String; = "change_page"

.field public static final CHANGE_PROPERTY_IN_INSPECTOR:Ljava/lang/String; = "change_property_in_inspector"

.field public static final CREATE_ANNOTATION:Ljava/lang/String; = "create_annotation"

.field public static final DELETE_ANNOTATION:Ljava/lang/String; = "delete_annotation"

.field public static final EDIT_BOOKMARKS:Ljava/lang/String; = "edit_bookmarks"

.field public static final ENTER_ANNOTATION_CREATION_MODE:Ljava/lang/String; = "enter_annotation_creation_mode"

.field public static final EXIT_ANNOTATION_CREATION_MODE:Ljava/lang/String; = "exit_annotation_creation_mode"

.field public static final EXIT_SEARCH:Ljava/lang/String; = "exit_search"

.field public static final FAILED_DOCUMENT_LOAD:Ljava/lang/String; = "failed_document_load"

.field public static final LOAD_DOCUMENT:Ljava/lang/String; = "load_document"

.field public static final MOVE_TOOLBAR:Ljava/lang/String; = "move_toolbar"

.field public static final NAVIGATE_THUMBNAIL_BAR:Ljava/lang/String; = "navigate_thumbnail_bar"

.field public static final OPEN_DOCUMENT_EDITOR:Ljava/lang/String; = "open_document_editor"

.field public static final OPEN_OUTLINE_VIEW:Ljava/lang/String; = "open_outline_view"

.field public static final OPEN_READER_VIEW:Ljava/lang/String; = "open_reader_view"

.field public static final OPEN_THUMBNAIL_GRID:Ljava/lang/String; = "open_thumbnail_grid"

.field public static final PERFORM_DOCUMENT_EDITOR_ACTION:Ljava/lang/String; = "perform_document_editor_action"

.field public static final PERFORM_SEARCH:Ljava/lang/String; = "perform_search"

.field public static final PERFORM_TEXT_SELECTION_ACTION:Ljava/lang/String; = "perform_text_selection_action"

.field public static final PRINT:Ljava/lang/String; = "print"

.field public static final REMOVE_BOOKMARK:Ljava/lang/String; = "remove_bookmark"

.field public static final RENAME_BOOKMARK:Ljava/lang/String; = "rename_bookmark"

.field public static final SELECT_ANNOTATION:Ljava/lang/String; = "select_annotation"

.field public static final SELECT_SEARCH_RESULT:Ljava/lang/String; = "select_search_result"

.field public static final SELECT_TEXT:Ljava/lang/String; = "select_text"

.field public static final SET_ANNOTATION_CREATOR:Ljava/lang/String; = "set_annotation_creator"

.field public static final SHARE:Ljava/lang/String; = "share"

.field public static final SHOW_ANNOTATION_CREATOR_DIALOG:Ljava/lang/String; = "show_annotation_creator_dialog"

.field public static final SHOW_ANNOTATION_INSPECTOR:Ljava/lang/String; = "show_annotation_inspector"

.field public static final SORT_BOOKMARK:Ljava/lang/String; = "sort_bookmark"

.field public static final START_SEARCH:Ljava/lang/String; = "start_search"

.field public static final TAP_ANNOTATION_IN_OUTLINE_LIST:Ljava/lang/String; = "tap_annotation_in_outline_list"

.field public static final TAP_BOOKMARK_IN_BOOKMARK_LIST:Ljava/lang/String; = "tap_bookmark_in_bookmark_list"

.field public static final TAP_OUTLINE_ELEMENT_IN_OUTLINE_LIST:Ljava/lang/String; = "tap_outline_element_in_outline_list"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

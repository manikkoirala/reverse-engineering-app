.class public final Lcom/pspdfkit/PSPDFKit;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final VERSION:Ljava/lang/String; = "8.7.3"

.field private static final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v0, ""

    .line 7
    sput-object v0, Lcom/pspdfkit/PSPDFKit;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/pspdfkit/internal/gj;->b(Landroid/content/Context;)V

    .line 3
    invoke-static {p1}, Lcom/pspdfkit/internal/gj;->a(Ljava/util/List;)V

    .line 5
    const-class p1, Lcom/pspdfkit/internal/gj;

    monitor-enter p1

    .line 6
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/internal/gj;->m()Lcom/pspdfkit/internal/dj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/dj;->b(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return-void

    :catchall_0
    move-exception p0

    monitor-exit p1

    throw p0
.end method

.method public static addAnalyticsClient(Lcom/pspdfkit/analytics/AnalyticsClient;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "client"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/q;->a(Lcom/pspdfkit/analytics/AnalyticsClient;)Z

    move-result p0

    return p0
.end method

.method public static clearCaches(Landroid/content/Context;Z)V
    .locals 0

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->g()Lcom/pspdfkit/internal/yl;

    move-result-object p0

    .line 3
    invoke-virtual {p0}, Lcom/pspdfkit/internal/yl;->a()V

    return-void
.end method

.method public static declared-synchronized ensureInitialized()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    .line 2
    :cond_0
    :try_start_1
    new-instance v1, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;

    const-string v2, "PSPDFKit must be initialized with the initialize() call before use."

    invoke-direct {v1, v2}, Lcom/pspdfkit/exceptions/PSPDFKitNotInitializedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getApplicationPolicy()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;
    .locals 1

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    .line 3
    invoke-static {}, Lcom/pspdfkit/internal/gj;->f()Lcom/pspdfkit/configuration/policy/ApplicationPolicy;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getLicenseFeatures()Ljava/util/EnumSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/pspdfkit/LicenseFeature;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2
    const-class v1, Lcom/pspdfkit/LicenseFeature;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 4
    :cond_0
    :try_start_1
    const-class v1, Lcom/pspdfkit/LicenseFeature;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 6
    invoke-static {}, Lcom/pspdfkit/internal/jni/NativeLicense;->license()Lcom/pspdfkit/internal/jni/NativeLicense;

    move-result-object v2

    invoke-virtual {v2}, Lcom/pspdfkit/internal/jni/NativeLicense;->features()Ljava/util/EnumSet;

    move-result-object v2

    .line 8
    invoke-virtual {v2}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;

    .line 9
    invoke-static {v3}, Lcom/pspdfkit/internal/sj;->a(Lcom/pspdfkit/internal/jni/NativeLicenseFeatures;)Lcom/pspdfkit/LicenseFeature;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 11
    invoke-virtual {v1, v3}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_2
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getSystemFontManager()Lcom/pspdfkit/ui/fonts/FontManager;
    .locals 2

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 1
    :try_start_0
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->u()Lcom/pspdfkit/internal/mt;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 77
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p1, v1}, Lcom/pspdfkit/PSPDFKit;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V
    .locals 1

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 78
    :try_start_0
    invoke-static {p0, p1}, Lcom/pspdfkit/PSPDFKit;->initialize(Landroid/content/Context;Ljava/lang/String;)V

    .line 79
    invoke-static {p2}, Lcom/pspdfkit/PSPDFKit;->setApplicationPolicy(Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;Lcom/pspdfkit/configuration/policy/ApplicationPolicy;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/pspdfkit/configuration/policy/ApplicationPolicy;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 80
    :try_start_0
    invoke-static {p0, p1, p3}, Lcom/pspdfkit/PSPDFKit;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 81
    invoke-static {p2}, Lcom/pspdfkit/PSPDFKit;->setApplicationPolicy(Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;,
            Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;,
            Lcom/pspdfkit/exceptions/MissingDependencyException;
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    const/4 v1, 0x0

    .line 76
    :try_start_0
    invoke-static {p0, p1, p2, v1}, Lcom/pspdfkit/PSPDFKit;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;,
            Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;,
            Lcom/pspdfkit/exceptions/MissingDependencyException;
        }
    .end annotation

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    :try_start_0
    const-string v1, "PSPDFKit.initialize() may not be called with a null context."

    const-string v2, "message"

    .line 2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_e

    const-string v1, "PSPDFKit.initialize() may not be called with a null fontPaths."

    const-string v2, "message"

    .line 4
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_d

    .line 5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_c

    .line 12
    invoke-static {}, Lcom/pspdfkit/internal/gj;->y()V

    .line 15
    sget-object v1, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_7

    .line 16
    invoke-static {p0, p2}, Lcom/pspdfkit/PSPDFKit;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 17
    invoke-static {p0}, Lcom/pspdfkit/internal/a;->a(Landroid/content/Context;)Lio/reactivex/rxjava3/core/Single;

    move-result-object v2

    new-instance v3, Lcom/pspdfkit/internal/es;

    invoke-direct {v3}, Lcom/pspdfkit/internal/es;-><init>()V

    invoke-virtual {v2, v3}, Lio/reactivex/rxjava3/core/Single;->subscribe(Lio/reactivex/rxjava3/core/SingleObserver;)V

    .line 18
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-static {p0, p2}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->initialize(Landroid/content/Context;Ljava/util/List;)V

    .line 19
    sget p2, Lcom/pspdfkit/internal/zd;->b:I

    .line 20
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 21
    invoke-static {}, Lcom/pspdfkit/internal/zd;->a()Ljava/util/HashMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 23
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :catchall_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 25
    :try_start_1
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 27
    invoke-virtual {p2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 28
    :cond_2
    :try_start_2
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_3

    new-array v2, v3, [Ljava/lang/Object;

    const-string v5, ", "

    .line 32
    invoke-static {v5, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, v4

    const-string p2, "PSPDFKit"

    const-string v5, "Detected Hybrid Technology: %s."

    .line 33
    invoke-static {p2, v5, v2}, Lcom/pspdfkit/utils/PdfLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p2

    iget p2, p2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v2, 0x100000

    and-int/2addr p2, v2

    if-eqz p2, :cond_4

    const/4 p2, 0x1

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_1
    if-nez p2, :cond_5

    new-array p2, v4, [Ljava/lang/Object;

    const-string v2, "PSPDFKit"

    const-string v5, "It seems your app did not declare android:largeHeap=\"true\" on the <application> tag of your AndroidManifest.xml.\nRendering PDF documents is a memory intensive task. To prevent the chance of out-of-memory errors, consider adding this flag to your manifest.\nMore information: http://developer.android.com/guide/topics/manifest/application-element.html#largeHeap"

    .line 35
    invoke-static {v2, v5, p2}, Lcom/pspdfkit/utils/PdfLog;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string p2, "always_finish_activities"

    invoke-static {p0, p2, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v3, :cond_6

    new-array p0, v4, [Ljava/lang/Object;

    const-string p2, "PSPDFKit"

    const-string v2, "It seems that the \"Don\'t keep activities\" developer option is enabled. If you are encountering issues make sure to disable this option and see if the issues persist."

    .line 46
    invoke-static {p2, v2, p0}, Lcom/pspdfkit/utils/PdfLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    :cond_6
    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_7
    if-eqz p1, :cond_b

    .line 53
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez p0, :cond_a

    .line 59
    :try_start_3
    invoke-static {p1}, Lcom/pspdfkit/internal/ft;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 62
    :catch_0
    :try_start_4
    invoke-static {p1}, Lcom/pspdfkit/internal/ft;->a(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    .line 66
    :goto_2
    sget-object p2, Lcom/pspdfkit/PSPDFKit;->b:Ljava/lang/String;

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    goto :goto_3

    .line 67
    :cond_8
    invoke-static {p1, p3}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->setLicenseKey(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_9

    .line 70
    sput-object p0, Lcom/pspdfkit/PSPDFKit;->b:Ljava/lang/String;

    goto :goto_3

    .line 71
    :cond_9
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p1, "Failed to initialize PSPDFKit."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 72
    :cond_a
    new-instance p0, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;

    const-string p1, "licenseKey must be a valid license key or null for trial mode."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/InvalidPSPDFKitLicenseException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_b
    :goto_3
    monitor-exit v0

    return-void

    .line 73
    :cond_c
    :try_start_5
    new-instance p0, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;

    const-string p1, "Current Android version is too old, needs Android Lollipop 5.0 (API 21) or newer."

    invoke-direct {p0, p1}, Lcom/pspdfkit/exceptions/PSPDFKitInitializationFailedException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 74
    :cond_d
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 75
    :cond_e
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized isInitialized()Z
    .locals 2

    const-class v0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static isLocalFileUri(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 3

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "uri"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isOpenableUri(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 8

    const-string v0, "context"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "uri"

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1, v0, v2}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    invoke-static {p0, p1}, Lcom/pspdfkit/internal/kb;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 108
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file:///android_asset/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 111
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string p0, "_size"

    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    const/4 p1, 0x0

    if-nez p0, :cond_2

    return p1

    .line 113
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    .line 114
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return v1
.end method

.method public static declared-synchronized release(Landroid/content/Context;)V
    .locals 3

    const-class p0, Lcom/pspdfkit/PSPDFKit;

    monitor-enter p0

    .line 1
    :try_start_0
    sget-object v0, Lcom/pspdfkit/PSPDFKit;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-static {}, Lcom/pspdfkit/internal/gj;->x()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static removeAnalyticsClient(Lcom/pspdfkit/analytics/AnalyticsClient;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "client"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-static {}, Lcom/pspdfkit/internal/gj;->c()Lcom/pspdfkit/internal/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/pspdfkit/internal/q;->b(Lcom/pspdfkit/analytics/AnalyticsClient;)Z

    move-result p0

    return p0
.end method

.method public static setApplicationPolicy(Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "applicationPolicy"

    const-string v1, "argumentName"

    .line 3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 54
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 55
    invoke-static {p0}, Lcom/pspdfkit/internal/gj;->a(Lcom/pspdfkit/configuration/policy/ApplicationPolicy;)V

    return-void
.end method

.method public static setLocalizationListener(Lcom/pspdfkit/listeners/LocalizationListener;)V
    .locals 2

    const-string v0, "localizationListener"

    const-string v1, "argumentName"

    .line 2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    invoke-static {p0}, Lcom/pspdfkit/internal/gj;->a(Lcom/pspdfkit/listeners/LocalizationListener;)V

    return-void
.end method

.method public static setNativeCrashDumpPath(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/pspdfkit/PSPDFKit;->ensureInitialized()V

    const-string v0, "path"

    const-string v1, "Native crash dump path may not be null."

    .line 2
    invoke-static {p0, v0, v1}, Lcom/pspdfkit/internal/wn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 3
    invoke-static {p0}, Lcom/pspdfkit/internal/jni/PSPDFKitNative;->setNativeCrashPath(Ljava/lang/String;)V

    return-void
.end method

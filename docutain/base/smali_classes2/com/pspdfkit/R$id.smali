.class public final Lcom/pspdfkit/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/pspdfkit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BOTTOM_END:I = 0x7f0a0001

.field public static final BOTTOM_START:I = 0x7f0a0002

.field public static final NO_DEBUG:I = 0x7f0a000e

.field public static final SHOW_ALL:I = 0x7f0a0013

.field public static final SHOW_PATH:I = 0x7f0a0014

.field public static final SHOW_PROGRESS:I = 0x7f0a0015

.field public static final TOP_END:I = 0x7f0a001b

.field public static final TOP_START:I = 0x7f0a001c

.field public static final accelerate:I = 0x7f0a0025

.field public static final accessibility_action_clickable_span:I = 0x7f0a0026

.field public static final accessibility_custom_action_0:I = 0x7f0a0027

.field public static final accessibility_custom_action_1:I = 0x7f0a0028

.field public static final accessibility_custom_action_10:I = 0x7f0a0029

.field public static final accessibility_custom_action_11:I = 0x7f0a002a

.field public static final accessibility_custom_action_12:I = 0x7f0a002b

.field public static final accessibility_custom_action_13:I = 0x7f0a002c

.field public static final accessibility_custom_action_14:I = 0x7f0a002d

.field public static final accessibility_custom_action_15:I = 0x7f0a002e

.field public static final accessibility_custom_action_16:I = 0x7f0a002f

.field public static final accessibility_custom_action_17:I = 0x7f0a0030

.field public static final accessibility_custom_action_18:I = 0x7f0a0031

.field public static final accessibility_custom_action_19:I = 0x7f0a0032

.field public static final accessibility_custom_action_2:I = 0x7f0a0033

.field public static final accessibility_custom_action_20:I = 0x7f0a0034

.field public static final accessibility_custom_action_21:I = 0x7f0a0035

.field public static final accessibility_custom_action_22:I = 0x7f0a0036

.field public static final accessibility_custom_action_23:I = 0x7f0a0037

.field public static final accessibility_custom_action_24:I = 0x7f0a0038

.field public static final accessibility_custom_action_25:I = 0x7f0a0039

.field public static final accessibility_custom_action_26:I = 0x7f0a003a

.field public static final accessibility_custom_action_27:I = 0x7f0a003b

.field public static final accessibility_custom_action_28:I = 0x7f0a003c

.field public static final accessibility_custom_action_29:I = 0x7f0a003d

.field public static final accessibility_custom_action_3:I = 0x7f0a003e

.field public static final accessibility_custom_action_30:I = 0x7f0a003f

.field public static final accessibility_custom_action_31:I = 0x7f0a0040

.field public static final accessibility_custom_action_4:I = 0x7f0a0041

.field public static final accessibility_custom_action_5:I = 0x7f0a0042

.field public static final accessibility_custom_action_6:I = 0x7f0a0043

.field public static final accessibility_custom_action_7:I = 0x7f0a0044

.field public static final accessibility_custom_action_8:I = 0x7f0a0045

.field public static final accessibility_custom_action_9:I = 0x7f0a0046

.field public static final action0:I = 0x7f0a0047

.field public static final actionDown:I = 0x7f0a0048

.field public static final actionDownUp:I = 0x7f0a0049

.field public static final actionUp:I = 0x7f0a004a

.field public static final action_bar:I = 0x7f0a004c

.field public static final action_bar_activity_content:I = 0x7f0a004d

.field public static final action_bar_container:I = 0x7f0a004e

.field public static final action_bar_root:I = 0x7f0a004f

.field public static final action_bar_spinner:I = 0x7f0a0050

.field public static final action_bar_subtitle:I = 0x7f0a0051

.field public static final action_bar_title:I = 0x7f0a0052

.field public static final action_container:I = 0x7f0a0053

.field public static final action_context_bar:I = 0x7f0a0054

.field public static final action_divider:I = 0x7f0a0057

.field public static final action_image:I = 0x7f0a005d

.field public static final action_menu_divider:I = 0x7f0a005f

.field public static final action_menu_presenter:I = 0x7f0a0060

.field public static final action_mode_bar:I = 0x7f0a0061

.field public static final action_mode_bar_stub:I = 0x7f0a0062

.field public static final action_mode_close_button:I = 0x7f0a0063

.field public static final action_text:I = 0x7f0a0069

.field public static final actions:I = 0x7f0a006a

.field public static final activity_chooser_view_content:I = 0x7f0a006b

.field public static final add:I = 0x7f0a006d

.field public static final alertTitle:I = 0x7f0a0077

.field public static final alignBounds:I = 0x7f0a0078

.field public static final alignMargins:I = 0x7f0a0079

.field public static final aligned:I = 0x7f0a007a

.field public static final allStates:I = 0x7f0a007c

.field public static final animateToEnd:I = 0x7f0a0081

.field public static final animateToStart:I = 0x7f0a0082

.field public static final antiClockwise:I = 0x7f0a0083

.field public static final anticipate:I = 0x7f0a0084

.field public static final arc:I = 0x7f0a0086

.field public static final asConfigured:I = 0x7f0a0087

.field public static final async:I = 0x7f0a0088

.field public static final auto:I = 0x7f0a008a

.field public static final autoComplete:I = 0x7f0a008b

.field public static final autoCompleteToEnd:I = 0x7f0a008d

.field public static final autoCompleteToStart:I = 0x7f0a008e

.field public static final barrier:I = 0x7f0a0094

.field public static final baseline:I = 0x7f0a0095

.field public static final bestChoice:I = 0x7f0a009a

.field public static final blocking:I = 0x7f0a00a0

.field public static final body:I = 0x7f0a00a1

.field public static final bottom:I = 0x7f0a00a2

.field public static final bounce:I = 0x7f0a00a9

.field public static final buttonPanel:I = 0x7f0a00b9

.field public static final callMeasure:I = 0x7f0a00bc

.field public static final cancel_action:I = 0x7f0a00c1

.field public static final cancel_button:I = 0x7f0a00c2

.field public static final card_view:I = 0x7f0a00c4

.field public static final carryVelocity:I = 0x7f0a00c5

.field public static final center:I = 0x7f0a00c7

.field public static final centerCrop:I = 0x7f0a00c8

.field public static final centerInside:I = 0x7f0a00c9

.field public static final chain:I = 0x7f0a00cc

.field public static final chain2:I = 0x7f0a00cd

.field public static final checkbox:I = 0x7f0a00d0

.field public static final checked:I = 0x7f0a00d1

.field public static final chronometer:I = 0x7f0a00d2

.field public static final circle_center:I = 0x7f0a00d3

.field public static final clear_text:I = 0x7f0a00d4

.field public static final clockwise:I = 0x7f0a00d7

.field public static final closest:I = 0x7f0a00d9

.field public static final compress:I = 0x7f0a00df

.field public static final confirm_button:I = 0x7f0a00e0

.field public static final constraint:I = 0x7f0a00e1

.field public static final container:I = 0x7f0a00e2

.field public static final content:I = 0x7f0a00e3

.field public static final contentPanel:I = 0x7f0a00e4

.field public static final contiguous:I = 0x7f0a00e5

.field public static final continuousVelocity:I = 0x7f0a00e6

.field public static final coordinator:I = 0x7f0a00e7

.field public static final cos:I = 0x7f0a00e8

.field public static final counterclockwise:I = 0x7f0a00e9

.field public static final cradle:I = 0x7f0a00ea

.field public static final currentState:I = 0x7f0a00ef

.field public static final custom:I = 0x7f0a00f0

.field public static final customPanel:I = 0x7f0a00f1

.field public static final cut:I = 0x7f0a00f2

.field public static final date_picker_actions:I = 0x7f0a00f4

.field public static final decelerate:I = 0x7f0a00f6

.field public static final decelerateAndComplete:I = 0x7f0a00f7

.field public static final decor_content_parent:I = 0x7f0a00f8

.field public static final default_activity_button:I = 0x7f0a00f9

.field public static final deltaRelative:I = 0x7f0a00fb

.field public static final design_bottom_sheet:I = 0x7f0a00fe

.field public static final design_menu_item_action_area:I = 0x7f0a00ff

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0100

.field public static final design_menu_item_text:I = 0x7f0a0101

.field public static final design_navigation_view:I = 0x7f0a0102

.field public static final dialog_button:I = 0x7f0a0104

.field public static final disjoint:I = 0x7f0a010b

.field public static final dragAnticlockwise:I = 0x7f0a0111

.field public static final dragClockwise:I = 0x7f0a0112

.field public static final dragDown:I = 0x7f0a0113

.field public static final dragEnd:I = 0x7f0a0114

.field public static final dragLeft:I = 0x7f0a0115

.field public static final dragRight:I = 0x7f0a0116

.field public static final dragStart:I = 0x7f0a0117

.field public static final dragUp:I = 0x7f0a0118

.field public static final dropdown_menu:I = 0x7f0a011a

.field public static final easeIn:I = 0x7f0a011b

.field public static final easeInOut:I = 0x7f0a011c

.field public static final easeOut:I = 0x7f0a011d

.field public static final east:I = 0x7f0a011e

.field public static final edit_query:I = 0x7f0a0120

.field public static final elastic:I = 0x7f0a0123

.field public static final embed:I = 0x7f0a0124

.field public static final end:I = 0x7f0a0125

.field public static final endToStart:I = 0x7f0a0126

.field public static final end_padder:I = 0x7f0a0127

.field public static final error_text:I = 0x7f0a012c

.field public static final expand_activities_button:I = 0x7f0a012e

.field public static final expanded_menu:I = 0x7f0a012f

.field public static final fade:I = 0x7f0a0133

.field public static final fill:I = 0x7f0a0134

.field public static final filled:I = 0x7f0a013a

.field public static final fitCenter:I = 0x7f0a0142

.field public static final fitEnd:I = 0x7f0a0143

.field public static final fitStart:I = 0x7f0a0144

.field public static final fitXY:I = 0x7f0a0146

.field public static final fixed:I = 0x7f0a0147

.field public static final flip:I = 0x7f0a0148

.field public static final floating:I = 0x7f0a0149

.field public static final forever:I = 0x7f0a014b

.field public static final fragment_container_view_tag:I = 0x7f0a014d

.field public static final frost:I = 0x7f0a014e

.field public static final fullscreen_header:I = 0x7f0a014f

.field public static final ghost_view:I = 0x7f0a0150

.field public static final ghost_view_holder:I = 0x7f0a0151

.field public static final gone:I = 0x7f0a0153

.field public static final group_divider:I = 0x7f0a0159

.field public static final header_title:I = 0x7f0a015c

.field public static final home:I = 0x7f0a0160

.field public static final honorRequest:I = 0x7f0a0162

.field public static final horizontal:I = 0x7f0a0163

.field public static final horizontal_only:I = 0x7f0a0164

.field public static final icon:I = 0x7f0a0167

.field public static final icon_frame:I = 0x7f0a0168

.field public static final icon_group:I = 0x7f0a0169

.field public static final ignore:I = 0x7f0a016e

.field public static final ignoreRequest:I = 0x7f0a016f

.field public static final image:I = 0x7f0a0170

.field public static final immediateStop:I = 0x7f0a0174

.field public static final included:I = 0x7f0a0177

.field public static final indeterminate:I = 0x7f0a0178

.field public static final info:I = 0x7f0a017a

.field public static final invisible:I = 0x7f0a017b

.field public static final inward:I = 0x7f0a017c

.field public static final italic:I = 0x7f0a017e

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a017f

.field public static final jumpToEnd:I = 0x7f0a0180

.field public static final jumpToStart:I = 0x7f0a0181

.field public static final labeled:I = 0x7f0a0182

.field public static final layout:I = 0x7f0a0183

.field public static final left:I = 0x7f0a0184

.field public static final leftToRight:I = 0x7f0a0185

.field public static final line1:I = 0x7f0a0188

.field public static final line3:I = 0x7f0a0189

.field public static final linear:I = 0x7f0a018a

.field public static final listMode:I = 0x7f0a018d

.field public static final list_item:I = 0x7f0a018e

.field public static final loading_text:I = 0x7f0a0191

.field public static final m3_side_sheet:I = 0x7f0a0194

.field public static final marquee:I = 0x7f0a0199

.field public static final masked:I = 0x7f0a019a

.field public static final match_constraint:I = 0x7f0a019b

.field public static final match_parent:I = 0x7f0a019c

.field public static final material_clock_display:I = 0x7f0a019d

.field public static final material_clock_display_and_toggle:I = 0x7f0a019e

.field public static final material_clock_face:I = 0x7f0a019f

.field public static final material_clock_hand:I = 0x7f0a01a0

.field public static final material_clock_level:I = 0x7f0a01a1

.field public static final material_clock_period_am_button:I = 0x7f0a01a2

.field public static final material_clock_period_pm_button:I = 0x7f0a01a3

.field public static final material_clock_period_toggle:I = 0x7f0a01a4

.field public static final material_hour_text_input:I = 0x7f0a01a5

.field public static final material_hour_tv:I = 0x7f0a01a6

.field public static final material_label:I = 0x7f0a01a7

.field public static final material_minute_text_input:I = 0x7f0a01a8

.field public static final material_minute_tv:I = 0x7f0a01a9

.field public static final material_textinput_timepicker:I = 0x7f0a01aa

.field public static final material_timepicker_cancel_button:I = 0x7f0a01ab

.field public static final material_timepicker_container:I = 0x7f0a01ac

.field public static final material_timepicker_mode_button:I = 0x7f0a01ad

.field public static final material_timepicker_ok_button:I = 0x7f0a01ae

.field public static final material_timepicker_view:I = 0x7f0a01af

.field public static final material_value_index:I = 0x7f0a01b0

.field public static final matrix:I = 0x7f0a01b2

.field public static final media_actions:I = 0x7f0a01b3

.field public static final message:I = 0x7f0a01b7

.field public static final middle:I = 0x7f0a01b8

.field public static final mini:I = 0x7f0a01b9

.field public static final month_grid:I = 0x7f0a01ba

.field public static final month_navigation_bar:I = 0x7f0a01bb

.field public static final month_navigation_fragment_toggle:I = 0x7f0a01bc

.field public static final month_navigation_next:I = 0x7f0a01bd

.field public static final month_navigation_previous:I = 0x7f0a01be

.field public static final month_title:I = 0x7f0a01bf

.field public static final motion_base:I = 0x7f0a01c1

.field public static final mtrl_anchor_parent:I = 0x7f0a01c2

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a01c3

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a01c4

.field public static final mtrl_calendar_frame:I = 0x7f0a01c5

.field public static final mtrl_calendar_main_pane:I = 0x7f0a01c6

.field public static final mtrl_calendar_months:I = 0x7f0a01c7

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a01c8

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a01c9

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a01ca

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a01cb

.field public static final mtrl_child_content_container:I = 0x7f0a01cc

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a01cd

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a01ce

.field public static final mtrl_picker_fullscreen:I = 0x7f0a01cf

.field public static final mtrl_picker_header:I = 0x7f0a01d0

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a01d1

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a01d2

.field public static final mtrl_picker_header_toggle:I = 0x7f0a01d3

.field public static final mtrl_picker_text_input_date:I = 0x7f0a01d4

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a01d5

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a01d6

.field public static final mtrl_picker_title_text:I = 0x7f0a01d7

.field public static final mtrl_view_tag_bottom_padding:I = 0x7f0a01d8

.field public static final multiply:I = 0x7f0a01d9

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a01e1

.field public static final navigation_bar_item_icon_container:I = 0x7f0a01e2

.field public static final navigation_bar_item_icon_view:I = 0x7f0a01e3

.field public static final navigation_bar_item_labels_group:I = 0x7f0a01e4

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a01e5

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a01e6

.field public static final navigation_header_container:I = 0x7f0a01e7

.field public static final neverCompleteToEnd:I = 0x7f0a01e9

.field public static final neverCompleteToStart:I = 0x7f0a01ea

.field public static final noState:I = 0x7f0a01ec

.field public static final none:I = 0x7f0a01ed

.field public static final normal:I = 0x7f0a01ee

.field public static final north:I = 0x7f0a01ef

.field public static final notification_background:I = 0x7f0a01f0

.field public static final notification_main_column:I = 0x7f0a01f5

.field public static final notification_main_column_container:I = 0x7f0a01f6

.field public static final off:I = 0x7f0a01f8

.field public static final on:I = 0x7f0a01f9

.field public static final outline:I = 0x7f0a01fc

.field public static final outward:I = 0x7f0a01fd

.field public static final overshoot:I = 0x7f0a01fe

.field public static final packed:I = 0x7f0a01ff

.field public static final parallax:I = 0x7f0a0201

.field public static final parent:I = 0x7f0a0202

.field public static final parentPanel:I = 0x7f0a0203

.field public static final parentRelative:I = 0x7f0a0204

.field public static final parent_matrix:I = 0x7f0a0205

.field public static final password_toggle:I = 0x7f0a0206

.field public static final path:I = 0x7f0a0207

.field public static final pathRelative:I = 0x7f0a0208

.field public static final percent:I = 0x7f0a020f

.field public static final pin:I = 0x7f0a0211

.field public static final position:I = 0x7f0a0213

.field public static final postLayout:I = 0x7f0a0214

.field public static final preferences_detail:I = 0x7f0a0215

.field public static final preferences_header:I = 0x7f0a0216

.field public static final preferences_sliding_pane_layout:I = 0x7f0a0217

.field public static final pressed:I = 0x7f0a0218

.field public static final progress_circular:I = 0x7f0a021d

.field public static final progress_horizontal:I = 0x7f0a021e

.field public static final pspdf__accepted_authors_label:I = 0x7f0a0220

.field public static final pspdf__accepted_authors_text_box:I = 0x7f0a0221

.field public static final pspdf__activity_audio_inspector:I = 0x7f0a0222

.field public static final pspdf__activity_content:I = 0x7f0a0223

.field public static final pspdf__activity_content_editing_bar:I = 0x7f0a0224

.field public static final pspdf__activity_document_info_view:I = 0x7f0a0225

.field public static final pspdf__activity_empty_view:I = 0x7f0a0226

.field public static final pspdf__activity_form_editing_bar:I = 0x7f0a0227

.field public static final pspdf__activity_fragment_container:I = 0x7f0a0228

.field public static final pspdf__activity_outline_view:I = 0x7f0a0229

.field public static final pspdf__activity_page_overlay:I = 0x7f0a022a

.field public static final pspdf__activity_reader_view:I = 0x7f0a022b

.field public static final pspdf__activity_search_view_modular:I = 0x7f0a022c

.field public static final pspdf__activity_tab_bar:I = 0x7f0a022d

.field public static final pspdf__activity_thumbnail_bar:I = 0x7f0a022e

.field public static final pspdf__activity_thumbnail_grid:I = 0x7f0a022f

.field public static final pspdf__activity_title_overlay:I = 0x7f0a0230

.field public static final pspdf__alert_dialog_img:I = 0x7f0a0231

.field public static final pspdf__alert_dialog_label:I = 0x7f0a0232

.field public static final pspdf__align_progressbar:I = 0x7f0a0233

.field public static final pspdf__animation_view_horizontal:I = 0x7f0a0234

.field public static final pspdf__animation_view_vertical:I = 0x7f0a0235

.field public static final pspdf__annotation_creation_inspector:I = 0x7f0a0236

.field public static final pspdf__annotation_creation_toolbar:I = 0x7f0a0237

.field public static final pspdf__annotation_creation_toolbar_group_drawing:I = 0x7f0a0238

.field public static final pspdf__annotation_creation_toolbar_group_markup:I = 0x7f0a0239

.field public static final pspdf__annotation_creation_toolbar_group_measurement:I = 0x7f0a023a

.field public static final pspdf__annotation_creation_toolbar_group_multimedia:I = 0x7f0a023b

.field public static final pspdf__annotation_creation_toolbar_group_undo_redo:I = 0x7f0a023c

.field public static final pspdf__annotation_creation_toolbar_group_writing:I = 0x7f0a023d

.field public static final pspdf__annotation_creation_toolbar_item_camera:I = 0x7f0a023e

.field public static final pspdf__annotation_creation_toolbar_item_circle:I = 0x7f0a023f

.field public static final pspdf__annotation_creation_toolbar_item_cloudy:I = 0x7f0a0240

.field public static final pspdf__annotation_creation_toolbar_item_cloudy_circle:I = 0x7f0a0241

.field public static final pspdf__annotation_creation_toolbar_item_cloudy_polygon:I = 0x7f0a0242

.field public static final pspdf__annotation_creation_toolbar_item_cloudy_square:I = 0x7f0a0243

.field public static final pspdf__annotation_creation_toolbar_item_dashed_circle:I = 0x7f0a0244

.field public static final pspdf__annotation_creation_toolbar_item_dashed_polygon:I = 0x7f0a0245

.field public static final pspdf__annotation_creation_toolbar_item_dashed_square:I = 0x7f0a0246

.field public static final pspdf__annotation_creation_toolbar_item_drawing:I = 0x7f0a0247

.field public static final pspdf__annotation_creation_toolbar_item_eraser:I = 0x7f0a0248

.field public static final pspdf__annotation_creation_toolbar_item_freetext:I = 0x7f0a0249

.field public static final pspdf__annotation_creation_toolbar_item_freetext_callout:I = 0x7f0a024a

.field public static final pspdf__annotation_creation_toolbar_item_highlight:I = 0x7f0a024b

.field public static final pspdf__annotation_creation_toolbar_item_image:I = 0x7f0a024c

.field public static final pspdf__annotation_creation_toolbar_item_ink_highlighter:I = 0x7f0a024d

.field public static final pspdf__annotation_creation_toolbar_item_ink_pen:I = 0x7f0a024e

.field public static final pspdf__annotation_creation_toolbar_item_instant_comment_marker:I = 0x7f0a024f

.field public static final pspdf__annotation_creation_toolbar_item_instant_highlight_comment:I = 0x7f0a0250

.field public static final pspdf__annotation_creation_toolbar_item_line:I = 0x7f0a0251

.field public static final pspdf__annotation_creation_toolbar_item_line_arrow:I = 0x7f0a0252

.field public static final pspdf__annotation_creation_toolbar_item_magic_ink:I = 0x7f0a0253

.field public static final pspdf__annotation_creation_toolbar_item_markup:I = 0x7f0a0254

.field public static final pspdf__annotation_creation_toolbar_item_measurement:I = 0x7f0a0255

.field public static final pspdf__annotation_creation_toolbar_item_measurement_area_ellipse:I = 0x7f0a0256

.field public static final pspdf__annotation_creation_toolbar_item_measurement_area_polygon:I = 0x7f0a0257

.field public static final pspdf__annotation_creation_toolbar_item_measurement_area_rect:I = 0x7f0a0258

.field public static final pspdf__annotation_creation_toolbar_item_measurement_distance:I = 0x7f0a0259

.field public static final pspdf__annotation_creation_toolbar_item_measurement_perimeter:I = 0x7f0a025a

.field public static final pspdf__annotation_creation_toolbar_item_multimedia:I = 0x7f0a025b

.field public static final pspdf__annotation_creation_toolbar_item_note:I = 0x7f0a025c

.field public static final pspdf__annotation_creation_toolbar_item_picker:I = 0x7f0a025d

.field public static final pspdf__annotation_creation_toolbar_item_polygon:I = 0x7f0a025e

.field public static final pspdf__annotation_creation_toolbar_item_polyline:I = 0x7f0a025f

.field public static final pspdf__annotation_creation_toolbar_item_redaction:I = 0x7f0a0260

.field public static final pspdf__annotation_creation_toolbar_item_redo:I = 0x7f0a0261

.field public static final pspdf__annotation_creation_toolbar_item_signature:I = 0x7f0a0262

.field public static final pspdf__annotation_creation_toolbar_item_sound:I = 0x7f0a0263

.field public static final pspdf__annotation_creation_toolbar_item_square:I = 0x7f0a0264

.field public static final pspdf__annotation_creation_toolbar_item_squiggly:I = 0x7f0a0265

.field public static final pspdf__annotation_creation_toolbar_item_stamp:I = 0x7f0a0266

.field public static final pspdf__annotation_creation_toolbar_item_strikeout:I = 0x7f0a0267

.field public static final pspdf__annotation_creation_toolbar_item_underline:I = 0x7f0a0268

.field public static final pspdf__annotation_creation_toolbar_item_undo:I = 0x7f0a0269

.field public static final pspdf__annotation_creation_toolbar_item_undo_redo:I = 0x7f0a026a

.field public static final pspdf__annotation_creation_toolbar_item_writing:I = 0x7f0a026b

.field public static final pspdf__annotation_editing_inspector:I = 0x7f0a026c

.field public static final pspdf__annotation_editing_toolbar:I = 0x7f0a026d

.field public static final pspdf__annotation_editing_toolbar_group_copy_cut:I = 0x7f0a026e

.field public static final pspdf__annotation_editing_toolbar_group_edit_share:I = 0x7f0a026f

.field public static final pspdf__annotation_editing_toolbar_group_inspector:I = 0x7f0a0270

.field public static final pspdf__annotation_editing_toolbar_group_undo_redo:I = 0x7f0a0271

.field public static final pspdf__annotation_editing_toolbar_item_annotation_note:I = 0x7f0a0272

.field public static final pspdf__annotation_editing_toolbar_item_copy:I = 0x7f0a0273

.field public static final pspdf__annotation_editing_toolbar_item_copy_cut:I = 0x7f0a0274

.field public static final pspdf__annotation_editing_toolbar_item_cut:I = 0x7f0a0275

.field public static final pspdf__annotation_editing_toolbar_item_delete:I = 0x7f0a0276

.field public static final pspdf__annotation_editing_toolbar_item_edit:I = 0x7f0a0277

.field public static final pspdf__annotation_editing_toolbar_item_edit_share:I = 0x7f0a0278

.field public static final pspdf__annotation_editing_toolbar_item_inspector:I = 0x7f0a0279

.field public static final pspdf__annotation_editing_toolbar_item_picker:I = 0x7f0a027a

.field public static final pspdf__annotation_editing_toolbar_item_play:I = 0x7f0a027b

.field public static final pspdf__annotation_editing_toolbar_item_record:I = 0x7f0a027c

.field public static final pspdf__annotation_editing_toolbar_item_redo:I = 0x7f0a027d

.field public static final pspdf__annotation_editing_toolbar_item_share:I = 0x7f0a027e

.field public static final pspdf__annotation_editing_toolbar_item_undo:I = 0x7f0a027f

.field public static final pspdf__annotation_editing_toolbar_item_undo_redo:I = 0x7f0a0280

.field public static final pspdf__annotation_inspector_view_alpha_picker:I = 0x7f0a0281

.field public static final pspdf__annotation_inspector_view_border_style_picker:I = 0x7f0a0282

.field public static final pspdf__annotation_inspector_view_close:I = 0x7f0a0283

.field public static final pspdf__annotation_inspector_view_fill_color_picker:I = 0x7f0a0284

.field public static final pspdf__annotation_inspector_view_font_picker:I = 0x7f0a0285

.field public static final pspdf__annotation_inspector_view_foreground_color_picker:I = 0x7f0a0286

.field public static final pspdf__annotation_inspector_view_line_end_fill_color_picker:I = 0x7f0a0287

.field public static final pspdf__annotation_inspector_view_line_end_picker:I = 0x7f0a0288

.field public static final pspdf__annotation_inspector_view_line_start_picker:I = 0x7f0a0289

.field public static final pspdf__annotation_inspector_view_measurement_value:I = 0x7f0a028a

.field public static final pspdf__annotation_inspector_view_outline_color_picker:I = 0x7f0a028b

.field public static final pspdf__annotation_inspector_view_overlay_text_picker:I = 0x7f0a028c

.field public static final pspdf__annotation_inspector_view_precision_picker:I = 0x7f0a028d

.field public static final pspdf__annotation_inspector_view_repeat_overlay_text_picker:I = 0x7f0a028e

.field public static final pspdf__annotation_inspector_view_scale_calibration_picker:I = 0x7f0a028f

.field public static final pspdf__annotation_inspector_view_scale_picker:I = 0x7f0a0290

.field public static final pspdf__annotation_inspector_view_snapping_picker:I = 0x7f0a0291

.field public static final pspdf__annotation_inspector_view_text_size_picker:I = 0x7f0a0292

.field public static final pspdf__annotation_inspector_view_thickness_picker:I = 0x7f0a0293

.field public static final pspdf__annotation_inspector_view_z_index_picker:I = 0x7f0a0294

.field public static final pspdf__annotation_list_clear_all:I = 0x7f0a0295

.field public static final pspdf__annotation_list_delete:I = 0x7f0a0296

.field public static final pspdf__annotation_list_drag_handle:I = 0x7f0a0297

.field public static final pspdf__annotation_list_edit:I = 0x7f0a0298

.field public static final pspdf__annotation_list_empty_text:I = 0x7f0a0299

.field public static final pspdf__annotation_list_item_icon:I = 0x7f0a029a

.field public static final pspdf__annotation_list_item_info:I = 0x7f0a029b

.field public static final pspdf__annotation_list_item_title:I = 0x7f0a029c

.field public static final pspdf__annotation_list_progress_bar:I = 0x7f0a029d

.field public static final pspdf__annotation_list_toolbar:I = 0x7f0a029e

.field public static final pspdf__annotation_list_view:I = 0x7f0a029f

.field public static final pspdf__apply_redactions_button:I = 0x7f0a02a0

.field public static final pspdf__audio_controls_layout:I = 0x7f0a02a1

.field public static final pspdf__audio_current_time:I = 0x7f0a02a2

.field public static final pspdf__audio_error:I = 0x7f0a02a3

.field public static final pspdf__audio_loading_bar:I = 0x7f0a02a4

.field public static final pspdf__audio_play:I = 0x7f0a02a5

.field public static final pspdf__audio_seek_bar:I = 0x7f0a02a6

.field public static final pspdf__audio_stop:I = 0x7f0a02a7

.field public static final pspdf__audio_total_time:I = 0x7f0a02a8

.field public static final pspdf__audio_visualizer:I = 0x7f0a02a9

.field public static final pspdf__back_btn:I = 0x7f0a02aa

.field public static final pspdf__bookmark_list_add:I = 0x7f0a02ab

.field public static final pspdf__bookmark_list_drag_handle:I = 0x7f0a02ac

.field public static final pspdf__bookmark_list_edit:I = 0x7f0a02ad

.field public static final pspdf__bookmark_list_empty_text:I = 0x7f0a02ae

.field public static final pspdf__bookmark_list_item_description:I = 0x7f0a02af

.field public static final pspdf__bookmark_list_item_page_number:I = 0x7f0a02b0

.field public static final pspdf__bookmark_list_item_title:I = 0x7f0a02b1

.field public static final pspdf__bookmark_list_page_image:I = 0x7f0a02b2

.field public static final pspdf__bookmark_list_page_image_container:I = 0x7f0a02b3

.field public static final pspdf__bookmark_list_recycler_view:I = 0x7f0a02b4

.field public static final pspdf__bookmark_list_toolbar:I = 0x7f0a02b5

.field public static final pspdf__bookmark_list_view:I = 0x7f0a02b6

.field public static final pspdf__bottom_sheet_drag_to_resize_view:I = 0x7f0a02b7

.field public static final pspdf__bottom_sheet_layout:I = 0x7f0a02b8

.field public static final pspdf__calibrate_unit_spinner:I = 0x7f0a02b9

.field public static final pspdf__calibrate_unit_text:I = 0x7f0a02ba

.field public static final pspdf__calibrate_value_text:I = 0x7f0a02bb

.field public static final pspdf__cancelled_authors_label:I = 0x7f0a02bc

.field public static final pspdf__cancelled_authors_text_box:I = 0x7f0a02bd

.field public static final pspdf__center_play_btn:I = 0x7f0a02be

.field public static final pspdf__check_view:I = 0x7f0a02bf

.field public static final pspdf__clear_redactions_button:I = 0x7f0a02c0

.field public static final pspdf__color:I = 0x7f0a02c1

.field public static final pspdf__color_mode_pager:I = 0x7f0a02c2

.field public static final pspdf__color_mode_tabs:I = 0x7f0a02c3

.field public static final pspdf__color_preview_view:I = 0x7f0a02c4

.field public static final pspdf__color_variations_palette:I = 0x7f0a02c5

.field public static final pspdf__comparison_breadcrumbs:I = 0x7f0a02c6

.field public static final pspdf__comparison_dialog_toolbar:I = 0x7f0a02c7

.field public static final pspdf__comparison_fragment_frame:I = 0x7f0a02c8

.field public static final pspdf__comparison_hint_dismiss:I = 0x7f0a02c9

.field public static final pspdf__comparison_hint_text_card:I = 0x7f0a02ca

.field public static final pspdf__completed_authors_label:I = 0x7f0a02cb

.field public static final pspdf__completed_authors_text_box:I = 0x7f0a02cc

.field public static final pspdf__compose_fragment_container:I = 0x7f0a02cd

.field public static final pspdf__content_editing_bar_layout:I = 0x7f0a02ce

.field public static final pspdf__content_editing_clear_button:I = 0x7f0a02cf

.field public static final pspdf__content_editing_decrease_font_size_button:I = 0x7f0a02d0

.field public static final pspdf__content_editing_font_color:I = 0x7f0a02d1

.field public static final pspdf__content_editing_font_name_imagebutton:I = 0x7f0a02d2

.field public static final pspdf__content_editing_font_name_textbutton:I = 0x7f0a02d3

.field public static final pspdf__content_editing_font_size_imagebutton:I = 0x7f0a02d4

.field public static final pspdf__content_editing_font_size_text:I = 0x7f0a02d5

.field public static final pspdf__content_editing_font_size_unit_text:I = 0x7f0a02d6

.field public static final pspdf__content_editing_increase_font_size_button:I = 0x7f0a02d7

.field public static final pspdf__content_editing_inspector:I = 0x7f0a02d8

.field public static final pspdf__content_editing_toolbar:I = 0x7f0a02d9

.field public static final pspdf__control_layout:I = 0x7f0a02da

.field public static final pspdf__creator_name_input:I = 0x7f0a02db

.field public static final pspdf__cross_hair_target:I = 0x7f0a02dc

.field public static final pspdf__current_color_view:I = 0x7f0a02dd

.field public static final pspdf__custom_color_picker_hex:I = 0x7f0a02de

.field public static final pspdf__custom_color_picker_hsl:I = 0x7f0a02df

.field public static final pspdf__custom_color_picker_rgb:I = 0x7f0a02e0

.field public static final pspdf__custom_color_picker_switcher:I = 0x7f0a02e1

.field public static final pspdf__custom_color_slider_1:I = 0x7f0a02e2

.field public static final pspdf__custom_color_slider_2:I = 0x7f0a02e3

.field public static final pspdf__custom_color_slider_3:I = 0x7f0a02e4

.field public static final pspdf__custom_stamp_container:I = 0x7f0a02e5

.field public static final pspdf__custom_stamp_creator_dialog_color_date_switch:I = 0x7f0a02e6

.field public static final pspdf__custom_stamp_creator_dialog_color_picker:I = 0x7f0a02e7

.field public static final pspdf__custom_stamp_creator_dialog_color_time_switch:I = 0x7f0a02e8

.field public static final pspdf__custom_stamp_creator_dialog_floating_button:I = 0x7f0a02e9

.field public static final pspdf__custom_stamp_creator_dialog_image:I = 0x7f0a02ea

.field public static final pspdf__custom_stamp_creator_dialog_linear_container:I = 0x7f0a02eb

.field public static final pspdf__custom_stamp_creator_dialog_text:I = 0x7f0a02ec

.field public static final pspdf__custom_stamp_creator_dialog_title:I = 0x7f0a02ed

.field public static final pspdf__custom_value_edit_text:I = 0x7f0a02ee

.field public static final pspdf__custom_value_layout:I = 0x7f0a02ef

.field public static final pspdf__default_palette:I = 0x7f0a02f0

.field public static final pspdf__dialog_root:I = 0x7f0a02f1

.field public static final pspdf__document_editing_toolbar:I = 0x7f0a02f2

.field public static final pspdf__document_editing_toolbar_group_more:I = 0x7f0a02f3

.field public static final pspdf__document_editing_toolbar_item_done:I = 0x7f0a02f4

.field public static final pspdf__document_editing_toolbar_item_duplicate_pages:I = 0x7f0a02f5

.field public static final pspdf__document_editing_toolbar_item_export_pages:I = 0x7f0a02f6

.field public static final pspdf__document_editing_toolbar_item_import_document:I = 0x7f0a02f7

.field public static final pspdf__document_editing_toolbar_item_more:I = 0x7f0a02f8

.field public static final pspdf__document_editing_toolbar_item_redo:I = 0x7f0a02f9

.field public static final pspdf__document_editing_toolbar_item_remove_pages:I = 0x7f0a02fa

.field public static final pspdf__document_editing_toolbar_item_rotate_pages:I = 0x7f0a02fb

.field public static final pspdf__document_editing_toolbar_item_save:I = 0x7f0a02fc

.field public static final pspdf__document_editing_toolbar_item_undo:I = 0x7f0a02fd

.field public static final pspdf__document_info_binding_icon:I = 0x7f0a02fe

.field public static final pspdf__document_info_binding_title:I = 0x7f0a02ff

.field public static final pspdf__document_info_dialog_toolbar:I = 0x7f0a0300

.field public static final pspdf__document_info_edit_fab:I = 0x7f0a0301

.field public static final pspdf__document_info_group_content_layout:I = 0x7f0a0302

.field public static final pspdf__document_info_group_icon:I = 0x7f0a0303

.field public static final pspdf__document_info_group_title:I = 0x7f0a0304

.field public static final pspdf__document_info_item_label:I = 0x7f0a0305

.field public static final pspdf__document_info_item_title:I = 0x7f0a0306

.field public static final pspdf__document_info_left_binding:I = 0x7f0a0307

.field public static final pspdf__document_info_list_view:I = 0x7f0a0308

.field public static final pspdf__document_info_right_binding:I = 0x7f0a0309

.field public static final pspdf__document_info_view:I = 0x7f0a030a

.field public static final pspdf__document_view:I = 0x7f0a030b

.field public static final pspdf__duration:I = 0x7f0a030c

.field public static final pspdf__electronic_signature_font_view_holder_button:I = 0x7f0a030d

.field public static final pspdf__electronic_signature_font_view_holder_container:I = 0x7f0a030e

.field public static final pspdf__electronic_signature_font_view_holder_text:I = 0x7f0a030f

.field public static final pspdf__electronic_signature_layout_add_new_signature_container:I = 0x7f0a0310

.field public static final pspdf__electronic_signature_save_chip:I = 0x7f0a0311

.field public static final pspdf__electronic_signature_selected_image:I = 0x7f0a0312

.field public static final pspdf__electronic_signature_type_signature:I = 0x7f0a0313

.field public static final pspdf__electronic_signature_type_signature_hint:I = 0x7f0a0314

.field public static final pspdf__electronic_signature_type_signature_measure_helper:I = 0x7f0a0315

.field public static final pspdf__electronic_signature_typing_font_list:I = 0x7f0a0316

.field public static final pspdf__electronic_signatures_back_button:I = 0x7f0a0317

.field public static final pspdf__electronic_signatures_color_option_primary:I = 0x7f0a0318

.field public static final pspdf__electronic_signatures_color_option_secondary:I = 0x7f0a0319

.field public static final pspdf__electronic_signatures_color_option_tertiary:I = 0x7f0a031a

.field public static final pspdf__electronic_signatures_draw_signature:I = 0x7f0a031b

.field public static final pspdf__electronic_signatures_fab_delete_selected_signatures:I = 0x7f0a031c

.field public static final pspdf__electronic_signatures_font_selection_spinner:I = 0x7f0a031d

.field public static final pspdf__electronic_signatures_font_selection_spinner_image:I = 0x7f0a031e

.field public static final pspdf__electronic_signatures_image_signature:I = 0x7f0a031f

.field public static final pspdf__electronic_signatures_layout_title_view:I = 0x7f0a0320

.field public static final pspdf__electronic_signatures_signature_fab_add_new_signature:I = 0x7f0a0321

.field public static final pspdf__electronic_signatures_typing_signature:I = 0x7f0a0322

.field public static final pspdf__electronic_signatures_view_pager:I = 0x7f0a0323

.field public static final pspdf__electronic_signatures_view_pager_tab_layout:I = 0x7f0a0324

.field public static final pspdf__empty_text:I = 0x7f0a0325

.field public static final pspdf__error_layout:I = 0x7f0a0326

.field public static final pspdf__expand_icon:I = 0x7f0a0327

.field public static final pspdf__fab:I = 0x7f0a0328

.field public static final pspdf__fixed_menu_recycler_view:I = 0x7f0a0329

.field public static final pspdf__font_bold:I = 0x7f0a032a

.field public static final pspdf__font_checkmark:I = 0x7f0a032b

.field public static final pspdf__font_italic:I = 0x7f0a032c

.field public static final pspdf__font_view:I = 0x7f0a032d

.field public static final pspdf__fontname_image:I = 0x7f0a032e

.field public static final pspdf__form_editing_bar_layout:I = 0x7f0a032f

.field public static final pspdf__form_editing_inspector:I = 0x7f0a0330

.field public static final pspdf__forms_clear_field_button:I = 0x7f0a0331

.field public static final pspdf__forms_done_button:I = 0x7f0a0332

.field public static final pspdf__forms_navigation_button_next:I = 0x7f0a0333

.field public static final pspdf__forms_navigation_button_previous:I = 0x7f0a0334

.field public static final pspdf__forms_validation_error:I = 0x7f0a0335

.field public static final pspdf__fragment_error_cross:I = 0x7f0a0336

.field public static final pspdf__fragment_loading_view:I = 0x7f0a0337

.field public static final pspdf__fragment_password:I = 0x7f0a0338

.field public static final pspdf__fragment_password_icon:I = 0x7f0a0339

.field public static final pspdf__fragment_password_view:I = 0x7f0a033a

.field public static final pspdf__fragment_progressbar:I = 0x7f0a033b

.field public static final pspdf__fragment_throbber:I = 0x7f0a033c

.field public static final pspdf__gallery_caption:I = 0x7f0a033d

.field public static final pspdf__gallery_item_img:I = 0x7f0a033e

.field public static final pspdf__guideline_left:I = 0x7f0a033f

.field public static final pspdf__guideline_right:I = 0x7f0a0340

.field public static final pspdf__has_played:I = 0x7f0a0341

.field public static final pspdf__hex_container:I = 0x7f0a0342

.field public static final pspdf__hex_entry:I = 0x7f0a0343

.field public static final pspdf__hex_entry_container:I = 0x7f0a0344

.field public static final pspdf__hex_title:I = 0x7f0a0345

.field public static final pspdf__horizontal_line:I = 0x7f0a0346

.field public static final pspdf__hsl_title:I = 0x7f0a0347

.field public static final pspdf__icon:I = 0x7f0a0348

.field public static final pspdf__icon_background:I = 0x7f0a0349

.field public static final pspdf__inspector_coordinator:I = 0x7f0a034a

.field public static final pspdf__label:I = 0x7f0a034b

.field public static final pspdf__layout_auto_button:I = 0x7f0a034c

.field public static final pspdf__layout_container:I = 0x7f0a034d

.field public static final pspdf__layout_content_editing_font_size_compound_button:I = 0x7f0a034e

.field public static final pspdf__layout_double_button:I = 0x7f0a034f

.field public static final pspdf__layout_header:I = 0x7f0a0350

.field public static final pspdf__layout_label:I = 0x7f0a0351

.field public static final pspdf__layout_separator:I = 0x7f0a0352

.field public static final pspdf__layout_settings:I = 0x7f0a0353

.field public static final pspdf__layout_single_button:I = 0x7f0a0354

.field public static final pspdf__linearlayout:I = 0x7f0a0355

.field public static final pspdf__link_creator_dialog_edit_text:I = 0x7f0a0356

.field public static final pspdf__loading_layout:I = 0x7f0a0357

.field public static final pspdf__loading_progress:I = 0x7f0a0358

.field public static final pspdf__loading_progress_bar:I = 0x7f0a0359

.field public static final pspdf__measurement_value_popup_text:I = 0x7f0a035a

.field public static final pspdf__media_dialog_root:I = 0x7f0a035b

.field public static final pspdf__menu_document_editor_save:I = 0x7f0a035c

.field public static final pspdf__menu_document_editor_save_as:I = 0x7f0a035d

.field public static final pspdf__menu_option_edit_annotations:I = 0x7f0a035e

.field public static final pspdf__menu_option_edit_content:I = 0x7f0a035f

.field public static final pspdf__menu_option_info:I = 0x7f0a0360

.field public static final pspdf__menu_option_info_view:I = 0x7f0a0361

.field public static final pspdf__menu_option_open:I = 0x7f0a0362

.field public static final pspdf__menu_option_outline:I = 0x7f0a0363

.field public static final pspdf__menu_option_print:I = 0x7f0a0364

.field public static final pspdf__menu_option_reader_view:I = 0x7f0a0365

.field public static final pspdf__menu_option_save_as:I = 0x7f0a0366

.field public static final pspdf__menu_option_search:I = 0x7f0a0367

.field public static final pspdf__menu_option_settings:I = 0x7f0a0368

.field public static final pspdf__menu_option_share:I = 0x7f0a0369

.field public static final pspdf__menu_option_signature:I = 0x7f0a036a

.field public static final pspdf__menu_option_thumbnail_grid:I = 0x7f0a036b

.field public static final pspdf__menu_pdf_outline_view_annotations:I = 0x7f0a036c

.field public static final pspdf__menu_pdf_outline_view_bookmarks:I = 0x7f0a036d

.field public static final pspdf__menu_pdf_outline_view_document_info:I = 0x7f0a036e

.field public static final pspdf__menu_pdf_outline_view_outline:I = 0x7f0a036f

.field public static final pspdf__message:I = 0x7f0a0370

.field public static final pspdf__misc_settings_container:I = 0x7f0a0371

.field public static final pspdf__move_backward:I = 0x7f0a0372

.field public static final pspdf__move_forward:I = 0x7f0a0373

.field public static final pspdf__move_to_back:I = 0x7f0a0374

.field public static final pspdf__move_to_front:I = 0x7f0a0375

.field public static final pspdf__navigate_back:I = 0x7f0a0376

.field public static final pspdf__navigate_forward:I = 0x7f0a0377

.field public static final pspdf__note_editor_item_author_name:I = 0x7f0a0378

.field public static final pspdf__note_editor_item_cancel_button:I = 0x7f0a0379

.field public static final pspdf__note_editor_item_card_header:I = 0x7f0a037a

.field public static final pspdf__note_editor_item_content:I = 0x7f0a037b

.field public static final pspdf__note_editor_item_created_date:I = 0x7f0a037c

.field public static final pspdf__note_editor_item_options_item:I = 0x7f0a037d

.field public static final pspdf__note_editor_item_save_button:I = 0x7f0a037e

.field public static final pspdf__note_editor_layout:I = 0x7f0a037f

.field public static final pspdf__note_editor_option_delete_reply:I = 0x7f0a0380

.field public static final pspdf__note_editor_option_set_reply_status:I = 0x7f0a0381

.field public static final pspdf__note_editor_option_share:I = 0x7f0a0382

.field public static final pspdf__note_editor_recycler_view:I = 0x7f0a0383

.field public static final pspdf__note_editor_toolbar:I = 0x7f0a0384

.field public static final pspdf__note_editor_toolbar_item_delete:I = 0x7f0a0385

.field public static final pspdf__note_editor_toolbar_item_redo:I = 0x7f0a0386

.field public static final pspdf__note_editor_toolbar_item_undo:I = 0x7f0a0387

.field public static final pspdf__note_item_bottom_padding:I = 0x7f0a0388

.field public static final pspdf__note_item_explicit_editing_controls_layout:I = 0x7f0a0389

.field public static final pspdf__note_item_review_state_list_layout:I = 0x7f0a038a

.field public static final pspdf__note_item_reviews_layout:I = 0x7f0a038b

.field public static final pspdf__note_item_status_details:I = 0x7f0a038c

.field public static final pspdf__note_item_style_box_chevron:I = 0x7f0a038d

.field public static final pspdf__note_item_style_box_current_style:I = 0x7f0a038e

.field public static final pspdf__note_item_style_box_detail_view_root:I = 0x7f0a038f

.field public static final pspdf__note_item_style_box_details:I = 0x7f0a0390

.field public static final pspdf__note_item_style_box_header:I = 0x7f0a0391

.field public static final pspdf__note_item_style_box_preview_image:I = 0x7f0a0392

.field public static final pspdf__note_item_style_box_title:I = 0x7f0a0393

.field public static final pspdf__note_reply_status_dialog_list_view:I = 0x7f0a0394

.field public static final pspdf__note_status_accepted_text_view:I = 0x7f0a0395

.field public static final pspdf__note_status_cancelled_text_view:I = 0x7f0a0396

.field public static final pspdf__note_status_completed_text_view:I = 0x7f0a0397

.field public static final pspdf__note_status_rejected_text_view:I = 0x7f0a0398

.field public static final pspdf__note_toolbar_item_comments:I = 0x7f0a0399

.field public static final pspdf__open_redact_button:I = 0x7f0a039a

.field public static final pspdf__options_layout:I = 0x7f0a039b

.field public static final pspdf__options_picker_detail_view:I = 0x7f0a039c

.field public static final pspdf__options_picker_title_row:I = 0x7f0a039d

.field public static final pspdf__outline_bookmark_item_container:I = 0x7f0a039e

.field public static final pspdf__outline_bookmarks_name_dialog_edit_text:I = 0x7f0a039f

.field public static final pspdf__outline_expand_group:I = 0x7f0a03a0

.field public static final pspdf__outline_list_search_view:I = 0x7f0a03a1

.field public static final pspdf__outline_list_view:I = 0x7f0a03a2

.field public static final pspdf__outline_no_match_text:I = 0x7f0a03a3

.field public static final pspdf__outline_page_number:I = 0x7f0a03a4

.field public static final pspdf__outline_pager:I = 0x7f0a03a5

.field public static final pspdf__outline_progress:I = 0x7f0a03a6

.field public static final pspdf__outline_recycler_view:I = 0x7f0a03a7

.field public static final pspdf__outline_text:I = 0x7f0a03a8

.field public static final pspdf__page_creator_add_btn:I = 0x7f0a03a9

.field public static final pspdf__page_creator_cancel_btn:I = 0x7f0a03aa

.field public static final pspdf__page_creator_color_checkmark:I = 0x7f0a03ab

.field public static final pspdf__page_creator_color_item:I = 0x7f0a03ac

.field public static final pspdf__page_creator_color_recycler_view:I = 0x7f0a03ad

.field public static final pspdf__page_creator_content:I = 0x7f0a03ae

.field public static final pspdf__page_creator_footer:I = 0x7f0a03af

.field public static final pspdf__page_creator_orientation_spinner:I = 0x7f0a03b0

.field public static final pspdf__page_creator_page_type_image:I = 0x7f0a03b1

.field public static final pspdf__page_creator_page_type_label:I = 0x7f0a03b2

.field public static final pspdf__page_creator_page_types_pager:I = 0x7f0a03b3

.field public static final pspdf__page_creator_size_spinner:I = 0x7f0a03b4

.field public static final pspdf__page_settings_container:I = 0x7f0a03b5

.field public static final pspdf__pager_list_view_footer:I = 0x7f0a03b6

.field public static final pspdf__pager_list_view_footer_progress_bar:I = 0x7f0a03b7

.field public static final pspdf__paste_hex_button:I = 0x7f0a03b8

.field public static final pspdf__pointSelectionStepperView:I = 0x7f0a03b9

.field public static final pspdf__popup_toolbar:I = 0x7f0a03ba

.field public static final pspdf__positive_button:I = 0x7f0a03bb

.field public static final pspdf__precision_spinner:I = 0x7f0a03bc

.field public static final pspdf__precision_spinner_text:I = 0x7f0a03bd

.field public static final pspdf__preset_horizontal:I = 0x7f0a03be

.field public static final pspdf__preset_horizontal_card:I = 0x7f0a03bf

.field public static final pspdf__preset_horizontal_label:I = 0x7f0a03c0

.field public static final pspdf__preset_horizontal_radio:I = 0x7f0a03c1

.field public static final pspdf__preset_vertical:I = 0x7f0a03c2

.field public static final pspdf__preset_vertical_card:I = 0x7f0a03c3

.field public static final pspdf__preset_vertical_label:I = 0x7f0a03c4

.field public static final pspdf__preset_vertical_radio:I = 0x7f0a03c5

.field public static final pspdf__presets:I = 0x7f0a03c6

.field public static final pspdf__previous_color_view:I = 0x7f0a03c7

.field public static final pspdf__progress:I = 0x7f0a03c8

.field public static final pspdf__progress_number:I = 0x7f0a03c9

.field public static final pspdf__progress_percent:I = 0x7f0a03ca

.field public static final pspdf__reader_container:I = 0x7f0a03cb

.field public static final pspdf__reader_wev_view:I = 0x7f0a03cc

.field public static final pspdf__recently_used_palette:I = 0x7f0a03cd

.field public static final pspdf__recently_used_palette_title:I = 0x7f0a03ce

.field public static final pspdf__recycler_view:I = 0x7f0a03cf

.field public static final pspdf__redaction_actions_container:I = 0x7f0a03d0

.field public static final pspdf__redaction_container:I = 0x7f0a03d1

.field public static final pspdf__redaction_preview_button:I = 0x7f0a03d2

.field public static final pspdf__redaction_view:I = 0x7f0a03d3

.field public static final pspdf__rejected_authors_label:I = 0x7f0a03d4

.field public static final pspdf__rejected_authors_text_box:I = 0x7f0a03d5

.field public static final pspdf__remove_signature_button:I = 0x7f0a03d6

.field public static final pspdf__rgb_title:I = 0x7f0a03d7

.field public static final pspdf__save_settings:I = 0x7f0a03d8

.field public static final pspdf__scale_button:I = 0x7f0a03d9

.field public static final pspdf__screen_awake_container:I = 0x7f0a03da

.field public static final pspdf__screen_awake_separator:I = 0x7f0a03db

.field public static final pspdf__screen_awake_switch:I = 0x7f0a03dc

.field public static final pspdf__screen_awake_text:I = 0x7f0a03dd

.field public static final pspdf__scroll_container:I = 0x7f0a03de

.field public static final pspdf__scroll_header:I = 0x7f0a03df

.field public static final pspdf__scroll_horizontal_button:I = 0x7f0a03e0

.field public static final pspdf__scroll_label:I = 0x7f0a03e1

.field public static final pspdf__scroll_separator:I = 0x7f0a03e2

.field public static final pspdf__scroll_settings:I = 0x7f0a03e3

.field public static final pspdf__scroll_vertical_button:I = 0x7f0a03e4

.field public static final pspdf__search_btn_back:I = 0x7f0a03e5

.field public static final pspdf__search_btn_next:I = 0x7f0a03e6

.field public static final pspdf__search_btn_prev:I = 0x7f0a03e7

.field public static final pspdf__search_edit_text_inline:I = 0x7f0a03e8

.field public static final pspdf__search_edit_text_modular:I = 0x7f0a03e9

.field public static final pspdf__search_inline_layout:I = 0x7f0a03ea

.field public static final pspdf__search_item_footer:I = 0x7f0a03eb

.field public static final pspdf__search_item_page:I = 0x7f0a03ec

.field public static final pspdf__search_item_snippet:I = 0x7f0a03ed

.field public static final pspdf__search_progress_inline:I = 0x7f0a03ee

.field public static final pspdf__search_progress_modular:I = 0x7f0a03ef

.field public static final pspdf__search_resultlist:I = 0x7f0a03f0

.field public static final pspdf__search_tv_current_result:I = 0x7f0a03f1

.field public static final pspdf__search_tv_no_matches_found:I = 0x7f0a03f2

.field public static final pspdf__search_view_inline:I = 0x7f0a03f3

.field public static final pspdf__seekbar:I = 0x7f0a03f4

.field public static final pspdf__select_point_fab:I = 0x7f0a03f5

.field public static final pspdf__separator:I = 0x7f0a03f6

.field public static final pspdf__settings_mode_picker:I = 0x7f0a03f7

.field public static final pspdf__settings_view:I = 0x7f0a03f8

.field public static final pspdf__share_dialog_annotations_description:I = 0x7f0a03f9

.field public static final pspdf__share_dialog_annotations_spinner:I = 0x7f0a03fa

.field public static final pspdf__share_dialog_document_name:I = 0x7f0a03fb

.field public static final pspdf__share_dialog_footer:I = 0x7f0a03fc

.field public static final pspdf__share_dialog_pages_range:I = 0x7f0a03fd

.field public static final pspdf__share_dialog_pages_spinner:I = 0x7f0a03fe

.field public static final pspdf__share_dialog_title:I = 0x7f0a03ff

.field public static final pspdf__signature_canvas_container:I = 0x7f0a0400

.field public static final pspdf__signature_canvas_view:I = 0x7f0a0401

.field public static final pspdf__signature_controller_container:I = 0x7f0a0402

.field public static final pspdf__signature_controller_view:I = 0x7f0a0403

.field public static final pspdf__signature_controls_view:I = 0x7f0a0404

.field public static final pspdf__signature_fab_accept_edited_signature:I = 0x7f0a0405

.field public static final pspdf__signature_fab_add_new_signature:I = 0x7f0a0406

.field public static final pspdf__signature_fab_clear_edited_signature:I = 0x7f0a0407

.field public static final pspdf__signature_fab_delete_selected_signatures:I = 0x7f0a0408

.field public static final pspdf__signature_info_content:I = 0x7f0a0409

.field public static final pspdf__signature_info_summary:I = 0x7f0a040a

.field public static final pspdf__signature_info_throbber:I = 0x7f0a040b

.field public static final pspdf__signature_items_list:I = 0x7f0a040c

.field public static final pspdf__signature_layout:I = 0x7f0a040d

.field public static final pspdf__signature_layout_add_new_signature:I = 0x7f0a040e

.field public static final pspdf__signature_layout_title_view:I = 0x7f0a040f

.field public static final pspdf__signature_list_signer_item_deletebutton:I = 0x7f0a0410

.field public static final pspdf__signature_list_signer_item_textview:I = 0x7f0a0411

.field public static final pspdf__signature_password_edittext:I = 0x7f0a0412

.field public static final pspdf__signature_sign_button:I = 0x7f0a0413

.field public static final pspdf__signature_sign_cancel:I = 0x7f0a0414

.field public static final pspdf__signature_signer_chip:I = 0x7f0a0415

.field public static final pspdf__signature_store_checkbox:I = 0x7f0a0416

.field public static final pspdf__signature_text:I = 0x7f0a0417

.field public static final pspdf__signature_text_container:I = 0x7f0a0418

.field public static final pspdf__signature_throbber:I = 0x7f0a0419

.field public static final pspdf__signature_view:I = 0x7f0a041a

.field public static final pspdf__signerRecyclerList:I = 0x7f0a041b

.field public static final pspdf__signer_chip:I = 0x7f0a041c

.field public static final pspdf__signer_chip_container:I = 0x7f0a041d

.field public static final pspdf__sliderLabel:I = 0x7f0a041e

.field public static final pspdf__sliderSeekBar:I = 0x7f0a041f

.field public static final pspdf__sliderUnitEditText:I = 0x7f0a0420

.field public static final pspdf__slider_container:I = 0x7f0a0421

.field public static final pspdf__snapping_switch:I = 0x7f0a0422

.field public static final pspdf__stamp_selection_recyclerview:I = 0x7f0a0423

.field public static final pspdf__stamps_custom_section:I = 0x7f0a0424

.field public static final pspdf__standard_menu_recycler_view:I = 0x7f0a0425

.field public static final pspdf__static_thumbnail_bar:I = 0x7f0a0426

.field public static final pspdf__status_layout:I = 0x7f0a0427

.field public static final pspdf__status_title:I = 0x7f0a0428

.field public static final pspdf__style_box_card:I = 0x7f0a0429

.field public static final pspdf__tab_close:I = 0x7f0a042a

.field public static final pspdf__tab_item_container:I = 0x7f0a042b

.field public static final pspdf__tab_selection_indicator:I = 0x7f0a042c

.field public static final pspdf__tab_text:I = 0x7f0a042d

.field public static final pspdf__tabs_bar_list:I = 0x7f0a042e

.field public static final pspdf__tag_key_bitmap:I = 0x7f0a042f

.field public static final pspdf__tag_key_overlay_provider:I = 0x7f0a0430

.field public static final pspdf__tag_key_page_index:I = 0x7f0a0431

.field public static final pspdf__tag_key_thumbnail_position:I = 0x7f0a0432

.field public static final pspdf__text:I = 0x7f0a0433

.field public static final pspdf__text_input:I = 0x7f0a0434

.field public static final pspdf__text_input_container:I = 0x7f0a0435

.field public static final pspdf__text_picker_title_row:I = 0x7f0a0436

.field public static final pspdf__text_selection_toolbar:I = 0x7f0a0437

.field public static final pspdf__text_selection_toolbar_item_copy:I = 0x7f0a0438

.field public static final pspdf__text_selection_toolbar_item_highlight:I = 0x7f0a0439

.field public static final pspdf__text_selection_toolbar_item_instantHighlightComment:I = 0x7f0a043a

.field public static final pspdf__text_selection_toolbar_item_link:I = 0x7f0a043b

.field public static final pspdf__text_selection_toolbar_item_paste_annotation:I = 0x7f0a043c

.field public static final pspdf__text_selection_toolbar_item_redact:I = 0x7f0a043d

.field public static final pspdf__text_selection_toolbar_item_search:I = 0x7f0a043e

.field public static final pspdf__text_selection_toolbar_item_share:I = 0x7f0a043f

.field public static final pspdf__text_selection_toolbar_item_speak:I = 0x7f0a0440

.field public static final pspdf__text_selection_toolbar_item_strikeout:I = 0x7f0a0441

.field public static final pspdf__text_selection_toolbar_item_underline:I = 0x7f0a0442

.field public static final pspdf__theme_container:I = 0x7f0a0443

.field public static final pspdf__theme_default_button:I = 0x7f0a0444

.field public static final pspdf__theme_header:I = 0x7f0a0445

.field public static final pspdf__theme_label:I = 0x7f0a0446

.field public static final pspdf__theme_night_button:I = 0x7f0a0447

.field public static final pspdf__theme_separator:I = 0x7f0a0448

.field public static final pspdf__theme_settings:I = 0x7f0a0449

.field public static final pspdf__thumbnail_grid_item_bg_card:I = 0x7f0a044a

.field public static final pspdf__thumbnail_grid_item_content:I = 0x7f0a044b

.field public static final pspdf__thumbnail_grid_item_highlight_bg:I = 0x7f0a044c

.field public static final pspdf__thumbnail_grid_item_label:I = 0x7f0a044d

.field public static final pspdf__thumbnail_grid_item_selected_ic:I = 0x7f0a044e

.field public static final pspdf__thumbnail_grid_item_wrapper:I = 0x7f0a044f

.field public static final pspdf__thumbnail_grid_recycler_view:I = 0x7f0a0450

.field public static final pspdf__title:I = 0x7f0a0451

.field public static final pspdf__title_part:I = 0x7f0a0452

.field public static final pspdf__toggle:I = 0x7f0a0453

.field public static final pspdf__toolbar_back_button:I = 0x7f0a0454

.field public static final pspdf__toolbar_close_button:I = 0x7f0a0455

.field public static final pspdf__toolbar_coordinator:I = 0x7f0a0456

.field public static final pspdf__toolbar_drag_button:I = 0x7f0a0457

.field public static final pspdf__toolbar_main:I = 0x7f0a0458

.field public static final pspdf__toolbar_more_items:I = 0x7f0a0459

.field public static final pspdf__transition_container:I = 0x7f0a045a

.field public static final pspdf__transition_continuous_button:I = 0x7f0a045b

.field public static final pspdf__transition_header:I = 0x7f0a045c

.field public static final pspdf__transition_jump_button:I = 0x7f0a045d

.field public static final pspdf__transition_label:I = 0x7f0a045e

.field public static final pspdf__transition_settings:I = 0x7f0a045f

.field public static final pspdf__turn_button:I = 0x7f0a0460

.field public static final pspdf__unit_from_spinner:I = 0x7f0a0461

.field public static final pspdf__unit_from_spinner_text:I = 0x7f0a0462

.field public static final pspdf__unit_to_spinner:I = 0x7f0a0463

.field public static final pspdf__unit_to_spinner_text:I = 0x7f0a0464

.field public static final pspdf__uri_item_copy:I = 0x7f0a0465

.field public static final pspdf__uri_item_link:I = 0x7f0a0466

.field public static final pspdf__uri_item_open:I = 0x7f0a0467

.field public static final pspdf__value:I = 0x7f0a0468

.field public static final pspdf__value_from_text:I = 0x7f0a0469

.field public static final pspdf__value_text:I = 0x7f0a046a

.field public static final pspdf__value_to_text:I = 0x7f0a046b

.field public static final pspdf__view_pager_tab_buttons_bar:I = 0x7f0a046c

.field public static final pspdf__view_pager_tab_view:I = 0x7f0a046d

.field public static final pspdf__youtube_player:I = 0x7f0a046e

.field public static final pspdf__youtube_progressbar:I = 0x7f0a046f

.field public static final pspdf_divider:I = 0x7f0a0470

.field public static final pspdf_unknown_color_overlay:I = 0x7f0a0471

.field public static final pspsdf__style_header_container:I = 0x7f0a0472

.field public static final radio:I = 0x7f0a0474

.field public static final rectangles:I = 0x7f0a047c

.field public static final recycler_view:I = 0x7f0a047d

.field public static final report_drawn:I = 0x7f0a047e

.field public static final reverseSawtooth:I = 0x7f0a047f

.field public static final right:I = 0x7f0a0480

.field public static final rightToLeft:I = 0x7f0a0481

.field public static final right_icon:I = 0x7f0a0482

.field public static final right_side:I = 0x7f0a0483

.field public static final rounded:I = 0x7f0a0486

.field public static final row_index_key:I = 0x7f0a0487

.field public static final save_non_transition_alpha:I = 0x7f0a048a

.field public static final save_overlay_view:I = 0x7f0a048b

.field public static final sawtooth:I = 0x7f0a048c

.field public static final scale:I = 0x7f0a048d

.field public static final screen:I = 0x7f0a048e

.field public static final scrollIndicatorDown:I = 0x7f0a0490

.field public static final scrollIndicatorUp:I = 0x7f0a0491

.field public static final scrollView:I = 0x7f0a0492

.field public static final scrollable:I = 0x7f0a0493

.field public static final search_badge:I = 0x7f0a0499

.field public static final search_bar:I = 0x7f0a049a

.field public static final search_bar_text_view:I = 0x7f0a049b

.field public static final search_button:I = 0x7f0a049c

.field public static final search_close_btn:I = 0x7f0a049d

.field public static final search_edit_frame:I = 0x7f0a049e

.field public static final search_go_btn:I = 0x7f0a049f

.field public static final search_mag_icon:I = 0x7f0a04a0

.field public static final search_plate:I = 0x7f0a04a1

.field public static final search_src_text:I = 0x7f0a04a2

.field public static final search_view_background:I = 0x7f0a04a3

.field public static final search_view_clear_button:I = 0x7f0a04a4

.field public static final search_view_content_container:I = 0x7f0a04a5

.field public static final search_view_divider:I = 0x7f0a04a6

.field public static final search_view_dummy_toolbar:I = 0x7f0a04a7

.field public static final search_view_edit_text:I = 0x7f0a04a8

.field public static final search_view_header_container:I = 0x7f0a04a9

.field public static final search_view_root:I = 0x7f0a04aa

.field public static final search_view_scrim:I = 0x7f0a04ab

.field public static final search_view_search_prefix:I = 0x7f0a04ac

.field public static final search_view_status_bar_spacer:I = 0x7f0a04ad

.field public static final search_view_toolbar:I = 0x7f0a04ae

.field public static final search_view_toolbar_container:I = 0x7f0a04af

.field public static final search_voice_btn:I = 0x7f0a04b0

.field public static final seekbar:I = 0x7f0a04b3

.field public static final seekbar_value:I = 0x7f0a04b4

.field public static final select_dialog_listview:I = 0x7f0a04b5

.field public static final selected:I = 0x7f0a04b6

.field public static final selection_type:I = 0x7f0a04b7

.field public static final sharedValueSet:I = 0x7f0a04ba

.field public static final sharedValueUnset:I = 0x7f0a04bb

.field public static final shortcut:I = 0x7f0a04be

.field public static final sin:I = 0x7f0a04c4

.field public static final skipped:I = 0x7f0a04c6

.field public static final slide:I = 0x7f0a04c7

.field public static final snackbar_action:I = 0x7f0a04c9

.field public static final snackbar_text:I = 0x7f0a04ca

.field public static final south:I = 0x7f0a04ce

.field public static final spacer:I = 0x7f0a04cf

.field public static final special_effects_controller_view_tag:I = 0x7f0a04d0

.field public static final spinner:I = 0x7f0a04d1

.field public static final spline:I = 0x7f0a04d2

.field public static final split_action_bar:I = 0x7f0a04d3

.field public static final spread:I = 0x7f0a04d4

.field public static final spread_inside:I = 0x7f0a04d5

.field public static final spring:I = 0x7f0a04d6

.field public static final square:I = 0x7f0a04d7

.field public static final src_atop:I = 0x7f0a04d8

.field public static final src_in:I = 0x7f0a04d9

.field public static final src_over:I = 0x7f0a04da

.field public static final standard:I = 0x7f0a04dc

.field public static final start:I = 0x7f0a04dd

.field public static final startHorizontal:I = 0x7f0a04de

.field public static final startToEnd:I = 0x7f0a04df

.field public static final startVertical:I = 0x7f0a04e0

.field public static final staticLayout:I = 0x7f0a04e1

.field public static final staticPostLayout:I = 0x7f0a04e2

.field public static final status_bar_latest_event_content:I = 0x7f0a04e3

.field public static final stop:I = 0x7f0a04e5

.field public static final stretch:I = 0x7f0a04e6

.field public static final submenuarrow:I = 0x7f0a04ed

.field public static final submit_area:I = 0x7f0a04ee

.field public static final switchWidget:I = 0x7f0a04f1

.field public static final tabMode:I = 0x7f0a04f2

.field public static final tag_accessibility_actions:I = 0x7f0a04f3

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a04f4

.field public static final tag_accessibility_heading:I = 0x7f0a04f5

.field public static final tag_accessibility_pane_title:I = 0x7f0a04f6

.field public static final tag_on_apply_window_listener:I = 0x7f0a04f7

.field public static final tag_on_receive_content_listener:I = 0x7f0a04f8

.field public static final tag_on_receive_content_mime_types:I = 0x7f0a04f9

.field public static final tag_screen_reader_focusable:I = 0x7f0a04fa

.field public static final tag_state_description:I = 0x7f0a04fb

.field public static final tag_transition_group:I = 0x7f0a04fc

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a04fd

.field public static final tag_unhandled_key_listeners:I = 0x7f0a04fe

.field public static final tag_window_insets_animation_callback:I = 0x7f0a04ff

.field public static final text:I = 0x7f0a0502

.field public static final text2:I = 0x7f0a0503

.field public static final textSpacerNoButtons:I = 0x7f0a0505

.field public static final textSpacerNoTitle:I = 0x7f0a0506

.field public static final text_input_end_icon:I = 0x7f0a050c

.field public static final text_input_error_icon:I = 0x7f0a050d

.field public static final text_input_start_icon:I = 0x7f0a050e

.field public static final textinput_counter:I = 0x7f0a050f

.field public static final textinput_error:I = 0x7f0a0510

.field public static final textinput_helper_text:I = 0x7f0a0511

.field public static final textinput_placeholder:I = 0x7f0a0512

.field public static final textinput_prefix_text:I = 0x7f0a0513

.field public static final textinput_suffix_text:I = 0x7f0a0514

.field public static final time:I = 0x7f0a0519

.field public static final title:I = 0x7f0a051a

.field public static final titleDividerNoCustom:I = 0x7f0a051b

.field public static final title_template:I = 0x7f0a051d

.field public static final top:I = 0x7f0a0522

.field public static final topPanel:I = 0x7f0a0525

.field public static final top_divider:I = 0x7f0a0528

.field public static final touch_outside:I = 0x7f0a0529

.field public static final transition_current_scene:I = 0x7f0a052c

.field public static final transition_layout_save:I = 0x7f0a052d

.field public static final transition_position:I = 0x7f0a052e

.field public static final transition_scene_layoutid_cache:I = 0x7f0a052f

.field public static final transition_transform:I = 0x7f0a0530

.field public static final triangle:I = 0x7f0a0531

.field public static final unchecked:I = 0x7f0a0534

.field public static final uniform:I = 0x7f0a0535

.field public static final unlabeled:I = 0x7f0a0536

.field public static final up:I = 0x7f0a0537

.field public static final vertical:I = 0x7f0a0539

.field public static final vertical_only:I = 0x7f0a053a

.field public static final view_offset_helper:I = 0x7f0a053b

.field public static final view_transition:I = 0x7f0a053c

.field public static final view_tree_lifecycle_owner:I = 0x7f0a053d

.field public static final view_tree_on_back_pressed_dispatcher_owner:I = 0x7f0a053e

.field public static final view_tree_saved_state_registry_owner:I = 0x7f0a053f

.field public static final view_tree_view_model_store_owner:I = 0x7f0a0540

.field public static final visible:I = 0x7f0a0541

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a0542

.field public static final west:I = 0x7f0a0544

.field public static final with_icon:I = 0x7f0a0547

.field public static final withinBounds:I = 0x7f0a0548

.field public static final wrap:I = 0x7f0a0549

.field public static final wrap_content:I = 0x7f0a054a

.field public static final wrap_content_constrained:I = 0x7f0a054b

.field public static final x_left:I = 0x7f0a054c

.field public static final x_right:I = 0x7f0a054d


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.io.FileOutputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import android.os.Build$VERSION;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.io.File;
import android.content.res.AssetManager;

public class DeviceProfileWriter
{
    private final String mApkName;
    private final AssetManager mAssetManager;
    private final File mCurProfile;
    private final byte[] mDesiredVersion;
    private boolean mDeviceSupportsAotProfile;
    private final ProfileInstaller.DiagnosticsCallback mDiagnostics;
    private final Executor mExecutor;
    private DexProfileData[] mProfile;
    private final String mProfileMetaSourceLocation;
    private final String mProfileSourceLocation;
    private byte[] mTranscodedProfile;
    
    public DeviceProfileWriter(final AssetManager mAssetManager, final Executor mExecutor, final ProfileInstaller.DiagnosticsCallback mDiagnostics, final String mApkName, final String mProfileSourceLocation, final String mProfileMetaSourceLocation, final File mCurProfile) {
        this.mDeviceSupportsAotProfile = false;
        this.mAssetManager = mAssetManager;
        this.mExecutor = mExecutor;
        this.mDiagnostics = mDiagnostics;
        this.mApkName = mApkName;
        this.mProfileSourceLocation = mProfileSourceLocation;
        this.mProfileMetaSourceLocation = mProfileMetaSourceLocation;
        this.mCurProfile = mCurProfile;
        this.mDesiredVersion = desiredVersion();
    }
    
    private DeviceProfileWriter addMetadata(final DexProfileData[] array, final byte[] array2) {
        try {
            final InputStream openStreamFromAssets = this.openStreamFromAssets(this.mAssetManager, this.mProfileMetaSourceLocation);
            if (openStreamFromAssets != null) {
                try {
                    this.mProfile = ProfileTranscoder.readMeta(openStreamFromAssets, ProfileTranscoder.readHeader(openStreamFromAssets, ProfileTranscoder.MAGIC_PROFM), array2, array);
                    if (openStreamFromAssets != null) {
                        openStreamFromAssets.close();
                    }
                    return this;
                }
                finally {
                    if (openStreamFromAssets != null) {
                        try {
                            openStreamFromAssets.close();
                        }
                        finally {
                            final Throwable exception;
                            ((Throwable)(Object)array).addSuppressed(exception);
                        }
                    }
                }
            }
            if (openStreamFromAssets != null) {
                openStreamFromAssets.close();
            }
        }
        catch (final IllegalStateException ex) {
            this.mProfile = null;
            this.mDiagnostics.onResultReceived(8, ex);
        }
        catch (final IOException ex2) {
            this.mDiagnostics.onResultReceived(7, ex2);
        }
        catch (final FileNotFoundException ex3) {
            this.mDiagnostics.onResultReceived(9, ex3);
        }
        return null;
    }
    
    private void assertDeviceAllowsProfileInstallerAotWritesCalled() {
        if (this.mDeviceSupportsAotProfile) {
            return;
        }
        throw new IllegalStateException("This device doesn't support aot. Did you call deviceSupportsAotProfile()?");
    }
    
    private static byte[] desiredVersion() {
        if (Build$VERSION.SDK_INT < 24 || Build$VERSION.SDK_INT > 34) {
            return null;
        }
        switch (Build$VERSION.SDK_INT) {
            default: {
                return null;
            }
            case 31:
            case 32:
            case 33:
            case 34: {
                return ProfileVersion.V015_S;
            }
            case 28:
            case 29:
            case 30: {
                return ProfileVersion.V010_P;
            }
            case 27: {
                return ProfileVersion.V009_O_MR1;
            }
            case 26: {
                return ProfileVersion.V005_O;
            }
            case 24:
            case 25: {
                return ProfileVersion.V001_N;
            }
        }
    }
    
    private InputStream getProfileInputStream(final AssetManager assetManager) {
        try {
            return this.openStreamFromAssets(assetManager, this.mProfileSourceLocation);
        }
        catch (final IOException ex) {
            this.mDiagnostics.onResultReceived(7, ex);
        }
        catch (final FileNotFoundException ex2) {
            this.mDiagnostics.onResultReceived(6, ex2);
        }
        return null;
    }
    
    private InputStream openStreamFromAssets(final AssetManager assetManager, String message) throws IOException {
        final FileInputStream fileInputStream = null;
        FileInputStream inputStream;
        try {
            inputStream = assetManager.openFd(message).createInputStream();
        }
        catch (final FileNotFoundException ex) {
            message = ex.getMessage();
            inputStream = fileInputStream;
            if (message != null) {
                inputStream = fileInputStream;
                if (message.contains("compressed")) {
                    this.mDiagnostics.onDiagnosticReceived(5, null);
                    inputStream = fileInputStream;
                }
            }
        }
        return inputStream;
    }
    
    private DexProfileData[] readProfileInternal(final InputStream p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: getstatic       androidx/profileinstaller/ProfileTranscoder.MAGIC_PROF:[B
        //     5: invokestatic    androidx/profileinstaller/ProfileTranscoder.readHeader:(Ljava/io/InputStream;[B)[B
        //     8: aload_0        
        //     9: getfield        androidx/profileinstaller/DeviceProfileWriter.mApkName:Ljava/lang/String;
        //    12: invokestatic    androidx/profileinstaller/ProfileTranscoder.readProfile:(Ljava/io/InputStream;[BLjava/lang/String;)[Landroidx/profileinstaller/DexProfileData;
        //    15: astore_2       
        //    16: aload_1        
        //    17: invokevirtual   java/io/InputStream.close:()V
        //    20: aload_2        
        //    21: astore_1       
        //    22: goto            102
        //    25: astore_1       
        //    26: aload_0        
        //    27: getfield        androidx/profileinstaller/DeviceProfileWriter.mDiagnostics:Landroidx/profileinstaller/ProfileInstaller$DiagnosticsCallback;
        //    30: bipush          7
        //    32: aload_1        
        //    33: invokeinterface androidx/profileinstaller/ProfileInstaller$DiagnosticsCallback.onResultReceived:(ILjava/lang/Object;)V
        //    38: aload_2        
        //    39: astore_1       
        //    40: goto            102
        //    43: astore_2       
        //    44: goto            104
        //    47: astore_2       
        //    48: aload_0        
        //    49: getfield        androidx/profileinstaller/DeviceProfileWriter.mDiagnostics:Landroidx/profileinstaller/ProfileInstaller$DiagnosticsCallback;
        //    52: bipush          8
        //    54: aload_2        
        //    55: invokeinterface androidx/profileinstaller/ProfileInstaller$DiagnosticsCallback.onResultReceived:(ILjava/lang/Object;)V
        //    60: aload_1        
        //    61: invokevirtual   java/io/InputStream.close:()V
        //    64: goto            100
        //    67: astore_2       
        //    68: aload_0        
        //    69: getfield        androidx/profileinstaller/DeviceProfileWriter.mDiagnostics:Landroidx/profileinstaller/ProfileInstaller$DiagnosticsCallback;
        //    72: bipush          7
        //    74: aload_2        
        //    75: invokeinterface androidx/profileinstaller/ProfileInstaller$DiagnosticsCallback.onResultReceived:(ILjava/lang/Object;)V
        //    80: aload_1        
        //    81: invokevirtual   java/io/InputStream.close:()V
        //    84: goto            100
        //    87: astore_1       
        //    88: aload_0        
        //    89: getfield        androidx/profileinstaller/DeviceProfileWriter.mDiagnostics:Landroidx/profileinstaller/ProfileInstaller$DiagnosticsCallback;
        //    92: bipush          7
        //    94: aload_1        
        //    95: invokeinterface androidx/profileinstaller/ProfileInstaller$DiagnosticsCallback.onResultReceived:(ILjava/lang/Object;)V
        //   100: aconst_null    
        //   101: astore_1       
        //   102: aload_1        
        //   103: areturn        
        //   104: aload_1        
        //   105: invokevirtual   java/io/InputStream.close:()V
        //   108: goto            124
        //   111: astore_1       
        //   112: aload_0        
        //   113: getfield        androidx/profileinstaller/DeviceProfileWriter.mDiagnostics:Landroidx/profileinstaller/ProfileInstaller$DiagnosticsCallback;
        //   116: bipush          7
        //   118: aload_1        
        //   119: invokeinterface androidx/profileinstaller/ProfileInstaller$DiagnosticsCallback.onResultReceived:(ILjava/lang/Object;)V
        //   124: aload_2        
        //   125: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  0      16     67     87     Ljava/io/IOException;
        //  0      16     47     67     Ljava/lang/IllegalStateException;
        //  0      16     43     126    Any
        //  16     20     25     43     Ljava/io/IOException;
        //  48     60     43     126    Any
        //  60     64     87     100    Ljava/io/IOException;
        //  68     80     43     126    Any
        //  80     84     87     100    Ljava/io/IOException;
        //  104    108    111    124    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 63 out of bounds for length 63
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3611)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static boolean requiresMetadata() {
        if (Build$VERSION.SDK_INT >= 24 && Build$VERSION.SDK_INT <= 34) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT != 24 && sdk_INT != 25) {
                switch (sdk_INT) {
                    default: {
                        return false;
                    }
                    case 31:
                    case 32:
                    case 33:
                    case 34: {
                        break;
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    private void result(final int n, final Object o) {
        this.mExecutor.execute(new DeviceProfileWriter$$ExternalSyntheticLambda0(this, n, o));
    }
    
    public boolean deviceAllowsProfileInstallerAotWrites() {
        if (this.mDesiredVersion == null) {
            this.result(3, Build$VERSION.SDK_INT);
            return false;
        }
        Label_0056: {
            if (this.mCurProfile.exists()) {
                if (!this.mCurProfile.canWrite()) {
                    this.result(4, null);
                    return false;
                }
                break Label_0056;
            }
            try {
                this.mCurProfile.createNewFile();
                return this.mDeviceSupportsAotProfile = true;
            }
            catch (final IOException ex) {
                this.result(4, null);
                return false;
            }
        }
    }
    
    public DeviceProfileWriter read() {
        this.assertDeviceAllowsProfileInstallerAotWritesCalled();
        if (this.mDesiredVersion == null) {
            return this;
        }
        final InputStream profileInputStream = this.getProfileInputStream(this.mAssetManager);
        if (profileInputStream != null) {
            this.mProfile = this.readProfileInternal(profileInputStream);
        }
        final DexProfileData[] mProfile = this.mProfile;
        if (mProfile != null && requiresMetadata()) {
            final DeviceProfileWriter addMetadata = this.addMetadata(mProfile, this.mDesiredVersion);
            if (addMetadata != null) {
                return addMetadata;
            }
        }
        return this;
    }
    
    public DeviceProfileWriter transcodeIfNeeded() {
        final DexProfileData[] mProfile = this.mProfile;
        final byte[] mDesiredVersion = this.mDesiredVersion;
        if (mProfile != null) {
            if (mDesiredVersion != null) {
                this.assertDeviceAllowsProfileInstallerAotWritesCalled();
                try {
                    final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    try {
                        ProfileTranscoder.writeHeader(byteArrayOutputStream, mDesiredVersion);
                        if (!ProfileTranscoder.transcodeAndWriteBody(byteArrayOutputStream, mDesiredVersion, mProfile)) {
                            this.mDiagnostics.onResultReceived(5, null);
                            this.mProfile = null;
                            byteArrayOutputStream.close();
                            return this;
                        }
                        this.mTranscodedProfile = byteArrayOutputStream.toByteArray();
                        byteArrayOutputStream.close();
                    }
                    finally {
                        try {
                            byteArrayOutputStream.close();
                        }
                        finally {
                            final Throwable t;
                            final Throwable exception;
                            t.addSuppressed(exception);
                        }
                    }
                }
                catch (final IllegalStateException ex) {
                    this.mDiagnostics.onResultReceived(8, ex);
                }
                catch (final IOException ex2) {
                    this.mDiagnostics.onResultReceived(7, ex2);
                }
                this.mProfile = null;
            }
        }
        return this;
    }
    
    public boolean write() {
        final byte[] mTranscodedProfile = this.mTranscodedProfile;
        if (mTranscodedProfile == null) {
            return false;
        }
        this.assertDeviceAllowsProfileInstallerAotWritesCalled();
        Label_0134: {
            try {
                final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mTranscodedProfile);
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(this.mCurProfile);
                    try {
                        Encoding.writeAll(byteArrayInputStream, fileOutputStream);
                        this.result(1, null);
                        fileOutputStream.close();
                        byteArrayInputStream.close();
                        this.mTranscodedProfile = null;
                        this.mProfile = null;
                        return true;
                    }
                    finally {
                        try {
                            fileOutputStream.close();
                        }
                        finally {
                            final Throwable t;
                            final Throwable exception;
                            t.addSuppressed(exception);
                        }
                    }
                }
                finally {
                    try {
                        byteArrayInputStream.close();
                    }
                    finally {
                        final Throwable t2;
                        final Throwable exception2;
                        t2.addSuppressed(exception2);
                    }
                }
            }
            catch (final IOException ex) {
                this.result(7, ex);
            }
            catch (final FileNotFoundException ex2) {
                this.result(6, ex2);
            }
            finally {
                break Label_0134;
            }
            this.mTranscodedProfile = null;
            this.mProfile = null;
            return false;
        }
        this.mTranscodedProfile = null;
        this.mProfile = null;
    }
}

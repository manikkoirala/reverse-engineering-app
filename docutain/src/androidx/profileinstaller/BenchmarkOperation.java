// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.os.Build$VERSION;
import android.content.Context;
import java.io.File;

class BenchmarkOperation
{
    private BenchmarkOperation() {
    }
    
    static boolean deleteFilesRecursively(final File file) {
        if (!file.isDirectory()) {
            file.delete();
            return true;
        }
        final File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return false;
        }
        final int length = listFiles.length;
        int i = 0;
        boolean b = true;
        while (i < length) {
            b = (deleteFilesRecursively(listFiles[i]) && b);
            ++i;
        }
        return b;
    }
    
    static void dropShaderCache(final Context context, final ProfileInstallReceiver.ResultDiagnostics resultDiagnostics) {
        File file;
        if (Build$VERSION.SDK_INT >= 24) {
            file = Api24ContextHelper.getDeviceProtectedCodeCacheDir(context);
        }
        else if (Build$VERSION.SDK_INT >= 23) {
            file = Api21ContextHelper.getCodeCacheDir(context);
        }
        else {
            file = context.getCacheDir();
        }
        if (deleteFilesRecursively(file)) {
            resultDiagnostics.onResultReceived(14, null);
        }
        else {
            resultDiagnostics.onResultReceived(15, null);
        }
    }
    
    private static class Api21ContextHelper
    {
        static File getCodeCacheDir(final Context context) {
            return context.getCodeCacheDir();
        }
    }
    
    private static class Api24ContextHelper
    {
        static File getDeviceProtectedCodeCacheDir(final Context context) {
            return context.createDeviceProtectedStorageContext().getCodeCacheDir();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.nio.charset.StandardCharsets;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;

class Encoding
{
    static final int SIZEOF_BYTE = 8;
    static final int UINT_16_SIZE = 2;
    static final int UINT_32_SIZE = 4;
    static final int UINT_8_SIZE = 1;
    
    private Encoding() {
    }
    
    static int bitsToBytes(final int n) {
        return (n + 8 - 1 & 0xFFFFFFF8) / 8;
    }
    
    static byte[] compress(final byte[] b) throws IOException {
        final Deflater def = new Deflater(1);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            final DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out, def);
            try {
                deflaterOutputStream.write(b);
                deflaterOutputStream.close();
                def.end();
                return out.toByteArray();
            }
            finally {
                try {
                    deflaterOutputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)(Object)b).addSuppressed(exception);
                }
            }
        }
        finally {
            def.end();
        }
    }
    
    static RuntimeException error(final String s) {
        return new IllegalStateException(s);
    }
    
    static byte[] read(final InputStream inputStream, final int i) throws IOException {
        final byte[] b = new byte[i];
        int read;
        for (int j = 0; j < i; j += read) {
            read = inputStream.read(b, j, i - j);
            if (read < 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Not enough bytes to read: ");
                sb.append(i);
                throw error(sb.toString());
            }
        }
        return b;
    }
    
    static byte[] readCompressed(final InputStream inputStream, final int n, final int n2) throws IOException {
        final Inflater inflater = new Inflater();
        try {
            final byte[] output = new byte[n2];
            final byte[] array = new byte[2048];
            int i = 0;
            int off = 0;
            while (!inflater.finished() && !inflater.needsDictionary() && i < n) {
                final int read = inputStream.read(array);
                if (read >= 0) {
                    inflater.setInput(array, 0, read);
                    try {
                        off += inflater.inflate(output, off, n2 - off);
                        i += read;
                        continue;
                    }
                    catch (final DataFormatException ex) {
                        throw error(ex.getMessage());
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid zip data. Stream ended after $totalBytesRead bytes. Expected ");
                sb.append(n);
                sb.append(" bytes");
                throw error(sb.toString());
            }
            if (i != n) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Didn't read enough bytes during decompression. expected=");
                sb2.append(n);
                sb2.append(" actual=");
                sb2.append(i);
                throw error(sb2.toString());
            }
            if (inflater.finished()) {
                return output;
            }
            throw error("Inflater did not finish");
        }
        finally {
            inflater.end();
        }
    }
    
    static String readString(final InputStream inputStream, final int n) throws IOException {
        return new String(read(inputStream, n), StandardCharsets.UTF_8);
    }
    
    static long readUInt(final InputStream inputStream, final int n) throws IOException {
        final byte[] read = read(inputStream, n);
        long n2 = 0L;
        for (int i = 0; i < n; ++i) {
            n2 += (long)(read[i] & 0xFF) << i * 8;
        }
        return n2;
    }
    
    static int readUInt16(final InputStream inputStream) throws IOException {
        return (int)readUInt(inputStream, 2);
    }
    
    static long readUInt32(final InputStream inputStream) throws IOException {
        return readUInt(inputStream, 4);
    }
    
    static int readUInt8(final InputStream inputStream) throws IOException {
        return (int)readUInt(inputStream, 1);
    }
    
    static int utf8Length(final String s) {
        return s.getBytes(StandardCharsets.UTF_8).length;
    }
    
    static void writeAll(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final byte[] array = new byte[512];
        while (true) {
            final int read = inputStream.read(array);
            if (read <= 0) {
                break;
            }
            outputStream.write(array, 0, read);
        }
    }
    
    static void writeCompressed(final OutputStream outputStream, byte[] compress) throws IOException {
        writeUInt32(outputStream, compress.length);
        compress = compress(compress);
        writeUInt32(outputStream, compress.length);
        outputStream.write(compress);
    }
    
    static void writeString(final OutputStream outputStream, final String s) throws IOException {
        outputStream.write(s.getBytes(StandardCharsets.UTF_8));
    }
    
    static void writeUInt(final OutputStream outputStream, final long n, final int n2) throws IOException {
        final byte[] b = new byte[n2];
        for (int i = 0; i < n2; ++i) {
            b[i] = (byte)(n >> i * 8 & 0xFFL);
        }
        outputStream.write(b);
    }
    
    static void writeUInt16(final OutputStream outputStream, final int n) throws IOException {
        writeUInt(outputStream, n, 2);
    }
    
    static void writeUInt32(final OutputStream outputStream, final long n) throws IOException {
        writeUInt(outputStream, n, 4);
    }
    
    static void writeUInt8(final OutputStream outputStream, final int n) throws IOException {
        writeUInt(outputStream, n, 1);
    }
}

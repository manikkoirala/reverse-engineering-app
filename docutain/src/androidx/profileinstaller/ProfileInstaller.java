// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.io.FilterOutputStream;
import java.io.FilterInputStream;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.Build$VERSION;
import android.content.res.AssetManager;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import android.content.pm.PackageInfo;
import java.util.concurrent.Executor;
import android.content.Context;
import java.io.File;
import android.util.Log;

public class ProfileInstaller
{
    public static final int DIAGNOSTIC_CURRENT_PROFILE_DOES_NOT_EXIST = 2;
    public static final int DIAGNOSTIC_CURRENT_PROFILE_EXISTS = 1;
    public static final int DIAGNOSTIC_PROFILE_IS_COMPRESSED = 5;
    public static final int DIAGNOSTIC_REF_PROFILE_DOES_NOT_EXIST = 4;
    public static final int DIAGNOSTIC_REF_PROFILE_EXISTS = 3;
    private static final DiagnosticsCallback EMPTY_DIAGNOSTICS;
    static final DiagnosticsCallback LOG_DIAGNOSTICS;
    private static final String PROFILE_BASE_DIR = "/data/misc/profiles/cur/0";
    private static final String PROFILE_FILE = "primary.prof";
    private static final String PROFILE_INSTALLER_SKIP_FILE_NAME = "profileinstaller_profileWrittenFor_lastUpdateTime.dat";
    private static final String PROFILE_META_LOCATION = "dexopt/baseline.profm";
    private static final String PROFILE_SOURCE_LOCATION = "dexopt/baseline.prof";
    public static final int RESULT_ALREADY_INSTALLED = 2;
    public static final int RESULT_BASELINE_PROFILE_NOT_FOUND = 6;
    public static final int RESULT_BENCHMARK_OPERATION_FAILURE = 15;
    public static final int RESULT_BENCHMARK_OPERATION_SUCCESS = 14;
    public static final int RESULT_BENCHMARK_OPERATION_UNKNOWN = 16;
    public static final int RESULT_DELETE_SKIP_FILE_SUCCESS = 11;
    public static final int RESULT_DESIRED_FORMAT_UNSUPPORTED = 5;
    public static final int RESULT_INSTALL_SKIP_FILE_SUCCESS = 10;
    public static final int RESULT_INSTALL_SUCCESS = 1;
    public static final int RESULT_IO_EXCEPTION = 7;
    public static final int RESULT_META_FILE_REQUIRED_BUT_NOT_FOUND = 9;
    public static final int RESULT_NOT_WRITABLE = 4;
    public static final int RESULT_PARSE_EXCEPTION = 8;
    public static final int RESULT_SAVE_PROFILE_SIGNALLED = 12;
    public static final int RESULT_SAVE_PROFILE_SKIPPED = 13;
    public static final int RESULT_UNSUPPORTED_ART_VERSION = 3;
    private static final String TAG = "ProfileInstaller";
    
    static {
        EMPTY_DIAGNOSTICS = (DiagnosticsCallback)new DiagnosticsCallback() {
            @Override
            public void onDiagnosticReceived(final int n, final Object o) {
            }
            
            @Override
            public void onResultReceived(final int n, final Object o) {
            }
        };
        LOG_DIAGNOSTICS = (DiagnosticsCallback)new DiagnosticsCallback() {
            static final String TAG = "ProfileInstaller";
            
            @Override
            public void onDiagnosticReceived(final int n, final Object o) {
                String s;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n != 4) {
                                if (n != 5) {
                                    s = "";
                                }
                                else {
                                    s = "DIAGNOSTIC_PROFILE_IS_COMPRESSED";
                                }
                            }
                            else {
                                s = "DIAGNOSTIC_REF_PROFILE_DOES_NOT_EXIST";
                            }
                        }
                        else {
                            s = "DIAGNOSTIC_REF_PROFILE_EXISTS";
                        }
                    }
                    else {
                        s = "DIAGNOSTIC_CURRENT_PROFILE_DOES_NOT_EXIST";
                    }
                }
                else {
                    s = "DIAGNOSTIC_CURRENT_PROFILE_EXISTS";
                }
                Log.d("ProfileInstaller", s);
            }
            
            @Override
            public void onResultReceived(final int n, final Object o) {
                String s = null;
                switch (n) {
                    default: {
                        s = "";
                        break;
                    }
                    case 11: {
                        s = "RESULT_DELETE_SKIP_FILE_SUCCESS";
                        break;
                    }
                    case 10: {
                        s = "RESULT_INSTALL_SKIP_FILE_SUCCESS";
                        break;
                    }
                    case 8: {
                        s = "RESULT_PARSE_EXCEPTION";
                        break;
                    }
                    case 7: {
                        s = "RESULT_IO_EXCEPTION";
                        break;
                    }
                    case 6: {
                        s = "RESULT_BASELINE_PROFILE_NOT_FOUND";
                        break;
                    }
                    case 5: {
                        s = "RESULT_DESIRED_FORMAT_UNSUPPORTED";
                        break;
                    }
                    case 4: {
                        s = "RESULT_NOT_WRITABLE";
                        break;
                    }
                    case 3: {
                        s = "RESULT_UNSUPPORTED_ART_VERSION";
                        break;
                    }
                    case 2: {
                        s = "RESULT_ALREADY_INSTALLED";
                        break;
                    }
                    case 1: {
                        s = "RESULT_INSTALL_SUCCESS";
                        break;
                    }
                }
                if (n != 6 && n != 7 && n != 8) {
                    Log.d("ProfileInstaller", s);
                }
                else {
                    Log.e("ProfileInstaller", s, (Throwable)o);
                }
            }
        };
    }
    
    private ProfileInstaller() {
    }
    
    static boolean deleteProfileWrittenFor(final File parent) {
        return new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat").delete();
    }
    
    static void deleteSkipFile(final Context context, final Executor executor, final DiagnosticsCallback diagnosticsCallback) {
        deleteProfileWrittenFor(context.getFilesDir());
        result(executor, diagnosticsCallback, 11, null);
    }
    
    static void diagnostic(final Executor executor, final DiagnosticsCallback diagnosticsCallback, final int n, final Object o) {
        executor.execute(new ProfileInstaller$$ExternalSyntheticLambda1(diagnosticsCallback, n, o));
    }
    
    static boolean hasAlreadyWrittenProfileForThisInstall(final PackageInfo packageInfo, File parent, final DiagnosticsCallback diagnosticsCallback) {
        final File file = new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat");
        final boolean exists = file.exists();
        boolean b = false;
        if (!exists) {
            return false;
        }
        try {
            parent = (File)new DataInputStream(new FileInputStream(file));
            try {
                final long long1 = ((DataInputStream)parent).readLong();
                ((FilterInputStream)parent).close();
                if (long1 == packageInfo.lastUpdateTime) {
                    b = true;
                }
                if (b) {
                    diagnosticsCallback.onResultReceived(2, null);
                }
                return b;
            }
            finally {
                try {
                    ((FilterInputStream)parent).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)packageInfo).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {
            return false;
        }
    }
    
    static void noteProfileWrittenFor(final PackageInfo packageInfo, File parent) {
        final File file = new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat");
        try {
            parent = (File)new DataOutputStream(new FileOutputStream(file));
            try {
                ((DataOutputStream)parent).writeLong(packageInfo.lastUpdateTime);
                ((FilterOutputStream)parent).close();
            }
            finally {
                try {
                    ((FilterOutputStream)parent).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)packageInfo).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {}
    }
    
    static void result(final Executor executor, final DiagnosticsCallback diagnosticsCallback, final int n, final Object o) {
        executor.execute(new ProfileInstaller$$ExternalSyntheticLambda0(diagnosticsCallback, n, o));
    }
    
    private static boolean transcodeAndWrite(final AssetManager assetManager, final String child, final PackageInfo packageInfo, final File file, final String s, final Executor executor, final DiagnosticsCallback diagnosticsCallback) {
        if (Build$VERSION.SDK_INT < 19) {
            result(executor, diagnosticsCallback, 3, null);
            return false;
        }
        final DeviceProfileWriter deviceProfileWriter = new DeviceProfileWriter(assetManager, executor, diagnosticsCallback, s, "dexopt/baseline.prof", "dexopt/baseline.profm", new File(new File("/data/misc/profiles/cur/0", child), "primary.prof"));
        if (!deviceProfileWriter.deviceAllowsProfileInstallerAotWrites()) {
            return false;
        }
        final boolean write = deviceProfileWriter.read().transcodeIfNeeded().write();
        if (write) {
            noteProfileWrittenFor(packageInfo, file);
        }
        return write;
    }
    
    public static void writeProfile(final Context context) {
        writeProfile(context, new ProfileInstallReceiver$$ExternalSyntheticLambda0(), ProfileInstaller.EMPTY_DIAGNOSTICS);
    }
    
    public static void writeProfile(final Context context, final Executor executor, final DiagnosticsCallback diagnosticsCallback) {
        writeProfile(context, executor, diagnosticsCallback, false);
    }
    
    static void writeProfile(final Context context, final Executor executor, final DiagnosticsCallback diagnosticsCallback, final boolean b) {
        final Context applicationContext = context.getApplicationContext();
        final String packageName = applicationContext.getPackageName();
        final ApplicationInfo applicationInfo = applicationContext.getApplicationInfo();
        final AssetManager assets = applicationContext.getAssets();
        final String name = new File(applicationInfo.sourceDir).getName();
        final PackageManager packageManager = context.getPackageManager();
        final boolean b2 = false;
        try {
            final PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            final File filesDir = context.getFilesDir();
            if (!b && hasAlreadyWrittenProfileForThisInstall(packageInfo, filesDir, diagnosticsCallback)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping profile installation for ");
                sb.append(context.getPackageName());
                Log.d("ProfileInstaller", sb.toString());
                ProfileVerifier.writeProfileVerification(context, false);
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Installing profile for ");
                sb2.append(context.getPackageName());
                Log.d("ProfileInstaller", sb2.toString());
                boolean b3 = b2;
                if (transcodeAndWrite(assets, packageName, packageInfo, filesDir, name, executor, diagnosticsCallback)) {
                    b3 = b2;
                    if (b) {
                        b3 = true;
                    }
                }
                ProfileVerifier.writeProfileVerification(context, b3);
            }
        }
        catch (final PackageManager$NameNotFoundException ex) {
            diagnosticsCallback.onResultReceived(7, ex);
            ProfileVerifier.writeProfileVerification(context, false);
        }
    }
    
    static void writeSkipFile(final Context context, final Executor executor, final DiagnosticsCallback diagnosticsCallback) {
        final String packageName = context.getApplicationContext().getPackageName();
        final PackageManager packageManager = context.getPackageManager();
        try {
            noteProfileWrittenFor(packageManager.getPackageInfo(packageName, 0), context.getFilesDir());
            result(executor, diagnosticsCallback, 10, null);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            result(executor, diagnosticsCallback, 7, ex);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface DiagnosticCode {
    }
    
    public interface DiagnosticsCallback
    {
        void onDiagnosticReceived(final int p0, final Object p1);
        
        void onResultReceived(final int p0, final Object p1);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ResultCode {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

class WritableFileSection
{
    final byte[] mContents;
    final int mExpectedInflateSize;
    final boolean mNeedsCompression;
    final FileSectionType mType;
    
    WritableFileSection(final FileSectionType mType, final int mExpectedInflateSize, final byte[] mContents, final boolean mNeedsCompression) {
        this.mType = mType;
        this.mExpectedInflateSize = mExpectedInflateSize;
        this.mContents = mContents;
        this.mNeedsCompression = mNeedsCompression;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.Objects;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import android.content.pm.PackageManager$PackageInfoFlags;
import android.content.pm.PackageInfo;
import java.io.IOException;
import java.io.File;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.PackageManager;
import android.os.Build$VERSION;
import android.content.Context;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.ResolvableFuture;

public final class ProfileVerifier
{
    private static final String CUR_PROFILES_BASE_DIR = "/data/misc/profiles/cur/0/";
    private static final String PROFILE_FILE_NAME = "primary.prof";
    private static final String PROFILE_INSTALLED_CACHE_FILE_NAME = "profileInstalled";
    private static final String REF_PROFILES_BASE_DIR = "/data/misc/profiles/ref/";
    private static final Object SYNC_OBJ;
    private static final String TAG = "ProfileVerifier";
    private static CompilationStatus sCompilationStatus;
    private static final ResolvableFuture<CompilationStatus> sFuture;
    
    static {
        sFuture = ResolvableFuture.create();
        SYNC_OBJ = new Object();
        ProfileVerifier.sCompilationStatus = null;
    }
    
    private ProfileVerifier() {
    }
    
    public static ListenableFuture<CompilationStatus> getCompilationStatusAsync() {
        return (ListenableFuture<CompilationStatus>)ProfileVerifier.sFuture;
    }
    
    private static long getPackageLastUpdateTime(final Context context) throws PackageManager$NameNotFoundException {
        final PackageManager packageManager = context.getApplicationContext().getPackageManager();
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getPackageInfo(packageManager, context).lastUpdateTime;
        }
        return packageManager.getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
    }
    
    private static CompilationStatus setCompilationStatus(final int n, final boolean b, final boolean b2) {
        ProfileVerifier.sFuture.set(ProfileVerifier.sCompilationStatus = new CompilationStatus(n, b, b2));
        return ProfileVerifier.sCompilationStatus;
    }
    
    public static CompilationStatus writeProfileVerification(final Context context) {
        return writeProfileVerification(context, false);
    }
    
    static CompilationStatus writeProfileVerification(final Context context, final boolean b) {
        if (!b) {
            final CompilationStatus sCompilationStatus = ProfileVerifier.sCompilationStatus;
            if (sCompilationStatus != null) {
                return sCompilationStatus;
            }
        }
        final Object sync_OBJ = ProfileVerifier.SYNC_OBJ;
        monitorenter(sync_OBJ);
        Label_0045: {
            if (b) {
                break Label_0045;
            }
            try {
                final CompilationStatus sCompilationStatus2 = ProfileVerifier.sCompilationStatus;
                if (sCompilationStatus2 != null) {
                    return sCompilationStatus2;
                }
                final int sdk_INT = Build$VERSION.SDK_INT;
                int mResultCode = 0;
                if (sdk_INT >= 28) {
                    if (Build$VERSION.SDK_INT != 30) {
                        final File file = new File(new File("/data/misc/profiles/ref/", context.getPackageName()), "primary.prof");
                        final long length = file.length();
                        final boolean b2 = file.exists() && length > 0L;
                        final File file2 = new File(new File("/data/misc/profiles/cur/0/", context.getPackageName()), "primary.prof");
                        final long length2 = file2.length();
                        boolean b3;
                        if (file2.exists() && length2 > 0L) {
                            b3 = true;
                        }
                        else {
                            b3 = false;
                        }
                        try {
                            final long packageLastUpdateTime = getPackageLastUpdateTime(context);
                            final File file3 = new File(context.getFilesDir(), "profileInstalled");
                            Cache fromFile = null;
                            if (file3.exists()) {
                                try {
                                    fromFile = Cache.readFromFile(file3);
                                }
                                catch (final IOException ex) {
                                    return setCompilationStatus(131072, b2, b3);
                                }
                            }
                            if (fromFile != null && fromFile.mPackageLastUpdateTime == packageLastUpdateTime && fromFile.mResultCode != 2) {
                                mResultCode = fromFile.mResultCode;
                            }
                            else if (b2) {
                                mResultCode = 1;
                            }
                            else if (b3) {
                                mResultCode = 2;
                            }
                            int n = mResultCode;
                            if (b) {
                                n = mResultCode;
                                if (b3 && (n = mResultCode) != 1) {
                                    n = 2;
                                }
                            }
                            int n2 = n;
                            if (fromFile != null) {
                                n2 = n;
                                if (fromFile.mResultCode == 2 && (n2 = n) == 1) {
                                    n2 = n;
                                    if (length < fromFile.mInstalledCurrentProfileSize) {
                                        n2 = 3;
                                    }
                                }
                            }
                            final Cache cache = new Cache(1, n2, packageLastUpdateTime, length2);
                            if (fromFile != null) {
                                final boolean equals = fromFile.equals(cache);
                                final int n3 = n2;
                                if (equals) {
                                    return setCompilationStatus(n3, b2, b3);
                                }
                            }
                            int n3;
                            try {
                                cache.writeOnFile(file3);
                                n3 = n2;
                            }
                            catch (final IOException ex2) {
                                n3 = 196608;
                            }
                            return setCompilationStatus(n3, b2, b3);
                        }
                        catch (final PackageManager$NameNotFoundException ex3) {
                            return setCompilationStatus(65536, b2, b3);
                        }
                    }
                }
                return setCompilationStatus(262144, false, false);
            }
            finally {
                monitorexit(sync_OBJ);
            }
        }
    }
    
    private static class Api33Impl
    {
        static PackageInfo getPackageInfo(final PackageManager packageManager, final Context context) throws PackageManager$NameNotFoundException {
            return packageManager.getPackageInfo(context.getPackageName(), PackageManager$PackageInfoFlags.of(0L));
        }
    }
    
    static class Cache
    {
        private static final int SCHEMA = 1;
        final long mInstalledCurrentProfileSize;
        final long mPackageLastUpdateTime;
        final int mResultCode;
        final int mSchema;
        
        Cache(final int mSchema, final int mResultCode, final long mPackageLastUpdateTime, final long mInstalledCurrentProfileSize) {
            this.mSchema = mSchema;
            this.mResultCode = mResultCode;
            this.mPackageLastUpdateTime = mPackageLastUpdateTime;
            this.mInstalledCurrentProfileSize = mInstalledCurrentProfileSize;
        }
        
        static Cache readFromFile(final File file) throws IOException {
            final DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
            try {
                final Cache cache = new Cache(dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readLong(), dataInputStream.readLong());
                dataInputStream.close();
                return cache;
            }
            finally {
                try {
                    dataInputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && o instanceof Cache) {
                final Cache cache = (Cache)o;
                if (this.mResultCode != cache.mResultCode || this.mPackageLastUpdateTime != cache.mPackageLastUpdateTime || this.mSchema != cache.mSchema || this.mInstalledCurrentProfileSize != cache.mInstalledCurrentProfileSize) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.mResultCode, this.mPackageLastUpdateTime, this.mSchema, this.mInstalledCurrentProfileSize);
        }
        
        void writeOnFile(final File file) throws IOException {
            file.delete();
            final DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
            try {
                dataOutputStream.writeInt(this.mSchema);
                dataOutputStream.writeInt(this.mResultCode);
                dataOutputStream.writeLong(this.mPackageLastUpdateTime);
                dataOutputStream.writeLong(this.mInstalledCurrentProfileSize);
                dataOutputStream.close();
            }
            finally {
                try {
                    dataOutputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
    }
    
    public static class CompilationStatus
    {
        public static final int RESULT_CODE_COMPILED_WITH_PROFILE = 1;
        public static final int RESULT_CODE_COMPILED_WITH_PROFILE_NON_MATCHING = 3;
        public static final int RESULT_CODE_ERROR_CACHE_FILE_EXISTS_BUT_CANNOT_BE_READ = 131072;
        public static final int RESULT_CODE_ERROR_CANT_WRITE_PROFILE_VERIFICATION_RESULT_CACHE_FILE = 196608;
        private static final int RESULT_CODE_ERROR_CODE_BIT_SHIFT = 16;
        public static final int RESULT_CODE_ERROR_PACKAGE_NAME_DOES_NOT_EXIST = 65536;
        public static final int RESULT_CODE_ERROR_UNSUPPORTED_API_VERSION = 262144;
        public static final int RESULT_CODE_NO_PROFILE = 0;
        public static final int RESULT_CODE_PROFILE_ENQUEUED_FOR_COMPILATION = 2;
        private final boolean mHasCurrentProfile;
        private final boolean mHasReferenceProfile;
        final int mResultCode;
        
        CompilationStatus(final int mResultCode, final boolean mHasReferenceProfile, final boolean mHasCurrentProfile) {
            this.mResultCode = mResultCode;
            this.mHasCurrentProfile = mHasCurrentProfile;
            this.mHasReferenceProfile = mHasReferenceProfile;
        }
        
        public int getProfileInstallResultCode() {
            return this.mResultCode;
        }
        
        public boolean hasProfileEnqueuedForCompilation() {
            return this.mHasCurrentProfile;
        }
        
        public boolean isCompiledWithProfile() {
            return this.mHasReferenceProfile;
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface ResultCode {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.os.Bundle;
import java.util.concurrent.Executor;
import android.content.Intent;
import android.content.Context;
import android.os.Process;
import android.os.Build$VERSION;
import android.content.BroadcastReceiver;

public class ProfileInstallReceiver extends BroadcastReceiver
{
    public static final String ACTION_BENCHMARK_OPERATION = "androidx.profileinstaller.action.BENCHMARK_OPERATION";
    public static final String ACTION_INSTALL_PROFILE = "androidx.profileinstaller.action.INSTALL_PROFILE";
    public static final String ACTION_SAVE_PROFILE = "androidx.profileinstaller.action.SAVE_PROFILE";
    public static final String ACTION_SKIP_FILE = "androidx.profileinstaller.action.SKIP_FILE";
    private static final String EXTRA_BENCHMARK_OPERATION = "EXTRA_BENCHMARK_OPERATION";
    private static final String EXTRA_BENCHMARK_OPERATION_DROP_SHADER_CACHE = "DROP_SHADER_CACHE";
    private static final String EXTRA_SKIP_FILE_OPERATION = "EXTRA_SKIP_FILE_OPERATION";
    private static final String EXTRA_SKIP_FILE_OPERATION_DELETE = "DELETE_SKIP_FILE";
    private static final String EXTRA_SKIP_FILE_OPERATION_WRITE = "WRITE_SKIP_FILE";
    
    static void saveProfile(final ProfileInstaller.DiagnosticsCallback diagnosticsCallback) {
        if (Build$VERSION.SDK_INT >= 24) {
            Process.sendSignal(Process.myPid(), 10);
            diagnosticsCallback.onResultReceived(12, null);
        }
        else {
            diagnosticsCallback.onResultReceived(13, null);
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        final String action = intent.getAction();
        if ("androidx.profileinstaller.action.INSTALL_PROFILE".equals(action)) {
            ProfileInstaller.writeProfile(context, new ProfileInstallReceiver$$ExternalSyntheticLambda0(), (ProfileInstaller.DiagnosticsCallback)new ResultDiagnostics(), true);
        }
        else if ("androidx.profileinstaller.action.SKIP_FILE".equals(action)) {
            final Bundle extras = intent.getExtras();
            if (extras != null) {
                final String string = extras.getString("EXTRA_SKIP_FILE_OPERATION");
                if ("WRITE_SKIP_FILE".equals(string)) {
                    ProfileInstaller.writeSkipFile(context, new ProfileInstallReceiver$$ExternalSyntheticLambda0(), (ProfileInstaller.DiagnosticsCallback)new ResultDiagnostics());
                }
                else if ("DELETE_SKIP_FILE".equals(string)) {
                    ProfileInstaller.deleteSkipFile(context, new ProfileInstallReceiver$$ExternalSyntheticLambda0(), (ProfileInstaller.DiagnosticsCallback)new ResultDiagnostics());
                }
            }
        }
        else if ("androidx.profileinstaller.action.SAVE_PROFILE".equals(action)) {
            saveProfile(new ResultDiagnostics());
        }
        else if ("androidx.profileinstaller.action.BENCHMARK_OPERATION".equals(action)) {
            final Bundle extras2 = intent.getExtras();
            if (extras2 != null) {
                final String string2 = extras2.getString("EXTRA_BENCHMARK_OPERATION");
                final ResultDiagnostics resultDiagnostics = new ResultDiagnostics();
                if ("DROP_SHADER_CACHE".equals(string2)) {
                    BenchmarkOperation.dropShaderCache(context, resultDiagnostics);
                }
                else {
                    resultDiagnostics.onResultReceived(16, null);
                }
            }
        }
    }
    
    class ResultDiagnostics implements DiagnosticsCallback
    {
        final ProfileInstallReceiver this$0;
        
        ResultDiagnostics(final ProfileInstallReceiver this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onDiagnosticReceived(final int n, final Object o) {
            ProfileInstaller.LOG_DIAGNOSTICS.onDiagnosticReceived(n, o);
        }
        
        @Override
        public void onResultReceived(final int resultCode, final Object o) {
            ProfileInstaller.LOG_DIAGNOSTICS.onResultReceived(resultCode, o);
            this.this$0.setResultCode(resultCode);
        }
    }
}

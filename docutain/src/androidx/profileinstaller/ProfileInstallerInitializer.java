// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.view.Choreographer$FrameCallback;
import android.view.Choreographer;
import java.util.Random;
import android.os.Handler;
import android.os.Looper;
import java.util.Collections;
import java.util.List;
import android.os.Build$VERSION;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import androidx.startup.Initializer;

public class ProfileInstallerInitializer implements Initializer<Result>
{
    private static final int DELAY_MS = 5000;
    
    private static void writeInBackground(final Context context) {
        new ThreadPoolExecutor(0, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()).execute(new ProfileInstallerInitializer$$ExternalSyntheticLambda2(context));
    }
    
    @Override
    public Result create(final Context context) {
        if (Build$VERSION.SDK_INT < 24) {
            return new Result();
        }
        this.delayAfterFirstFrame(context.getApplicationContext());
        return new Result();
    }
    
    void delayAfterFirstFrame(final Context context) {
        Choreographer16Impl.postFrameCallback(new ProfileInstallerInitializer$$ExternalSyntheticLambda1(this, context));
    }
    
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Collections.emptyList();
    }
    
    void installAfterDelay(final Context context) {
        Handler async;
        if (Build$VERSION.SDK_INT >= 28) {
            async = Handler28Impl.createAsync(Looper.getMainLooper());
        }
        else {
            async = new Handler(Looper.getMainLooper());
        }
        async.postDelayed((Runnable)new ProfileInstallerInitializer$$ExternalSyntheticLambda0(context), (long)(new Random().nextInt(Math.max(1000, 1)) + 5000));
    }
    
    private static class Choreographer16Impl
    {
        public static void postFrameCallback(final Runnable runnable) {
            Choreographer.getInstance().postFrameCallback((Choreographer$FrameCallback)new ProfileInstallerInitializer$Choreographer16Impl$$ExternalSyntheticLambda0(runnable));
        }
    }
    
    private static class Handler28Impl
    {
        public static Handler createAsync(final Looper looper) {
            return Handler.createAsync(looper);
        }
    }
    
    public static class Result
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.util.ArrayList;
import java.util.TreeMap;
import java.io.ByteArrayInputStream;
import java.util.BitSet;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Map;

class ProfileTranscoder
{
    private static final int HOT = 1;
    private static final int INLINE_CACHE_MEGAMORPHIC_ENCODING = 7;
    private static final int INLINE_CACHE_MISSING_TYPES_ENCODING = 6;
    static final byte[] MAGIC_PROF;
    static final byte[] MAGIC_PROFM;
    private static final int POST_STARTUP = 4;
    private static final int STARTUP = 2;
    
    static {
        MAGIC_PROF = new byte[] { 112, 114, 111, 0 };
        MAGIC_PROFM = new byte[] { 112, 114, 109, 0 };
    }
    
    private ProfileTranscoder() {
    }
    
    private static int computeMethodFlags(final DexProfileData dexProfileData) {
        final Iterator<Map.Entry<Integer, Integer>> iterator = dexProfileData.methods.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n |= ((Map.Entry<K, Integer>)iterator.next()).getValue();
        }
        return n;
    }
    
    private static byte[] createCompressibleBody(final DexProfileData[] array, final byte[] a) throws IOException {
        final int length = array.length;
        final int n = 0;
        final int n2 = 0;
        int i = 0;
        int n3 = 0;
        while (i < length) {
            final DexProfileData dexProfileData = array[i];
            n3 += Encoding.utf8Length(generateDexKey(dexProfileData.apkName, dexProfileData.dexName, a)) + 16 + dexProfileData.classSetSize * 2 + dexProfileData.hotMethodRegionSize + getMethodBitmapStorageSize(dexProfileData.numMethodIds);
            ++i;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(n3);
        if (Arrays.equals(a, ProfileVersion.V009_O_MR1)) {
            for (int length2 = array.length, j = n2; j < length2; ++j) {
                final DexProfileData dexProfileData2 = array[j];
                writeLineHeader(byteArrayOutputStream, dexProfileData2, generateDexKey(dexProfileData2.apkName, dexProfileData2.dexName, a));
                writeLineData(byteArrayOutputStream, dexProfileData2);
            }
        }
        else {
            for (final DexProfileData dexProfileData3 : array) {
                writeLineHeader(byteArrayOutputStream, dexProfileData3, generateDexKey(dexProfileData3.apkName, dexProfileData3.dexName, a));
            }
            for (int length4 = array.length, l = n; l < length4; ++l) {
                writeLineData(byteArrayOutputStream, array[l]);
            }
        }
        if (byteArrayOutputStream.size() == n3) {
            return byteArrayOutputStream.toByteArray();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("The bytes saved do not match expectation. actual=");
        sb.append(byteArrayOutputStream.size());
        sb.append(" expected=");
        sb.append(n3);
        throw Encoding.error(sb.toString());
    }
    
    private static WritableFileSection createCompressibleClassSection(final DexProfileData[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int j = 0;
        try {
            while (i < array.length) {
                final DexProfileData dexProfileData = array[i];
                Encoding.writeUInt16(byteArrayOutputStream, i);
                Encoding.writeUInt16(byteArrayOutputStream, dexProfileData.classSetSize);
                j = j + 2 + 2 + dexProfileData.classSetSize * 2;
                writeClasses(byteArrayOutputStream, dexProfileData);
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final WritableFileSection writableFileSection = new WritableFileSection(FileSectionType.CLASSES, j, byteArray, true);
                byteArrayOutputStream.close();
                return writableFileSection;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw Encoding.error(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    private static WritableFileSection createCompressibleMethodsSection(final DexProfileData[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int j = 0;
        try {
            while (i < array.length) {
                final DexProfileData dexProfileData = array[i];
                final int computeMethodFlags = computeMethodFlags(dexProfileData);
                final byte[] methodBitmapRegion = createMethodBitmapRegion(dexProfileData);
                final byte[] methodsWithInlineCaches = createMethodsWithInlineCaches(dexProfileData);
                Encoding.writeUInt16(byteArrayOutputStream, i);
                final int n = methodBitmapRegion.length + 2 + methodsWithInlineCaches.length;
                Encoding.writeUInt32(byteArrayOutputStream, n);
                Encoding.writeUInt16(byteArrayOutputStream, computeMethodFlags);
                byteArrayOutputStream.write(methodBitmapRegion);
                byteArrayOutputStream.write(methodsWithInlineCaches);
                j = j + 2 + 4 + n;
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final WritableFileSection writableFileSection = new WritableFileSection(FileSectionType.METHODS, j, byteArray, true);
                byteArrayOutputStream.close();
                return writableFileSection;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw Encoding.error(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    private static byte[] createMethodBitmapRegion(final DexProfileData dexProfileData) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            writeMethodBitmap(byteArrayOutputStream, dexProfileData);
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)dexProfileData).addSuppressed(exception);
            }
        }
    }
    
    private static byte[] createMethodsWithInlineCaches(final DexProfileData dexProfileData) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            writeMethodsWithInlineCaches(byteArrayOutputStream, dexProfileData);
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)dexProfileData).addSuppressed(exception);
            }
        }
    }
    
    private static String enforceSeparator(final String s, final String s2) {
        if ("!".equals(s2)) {
            return s.replace(":", "!");
        }
        String replace = s;
        if (":".equals(s2)) {
            replace = s.replace("!", ":");
        }
        return replace;
    }
    
    private static String extractKey(final String s) {
        int n;
        if ((n = s.indexOf("!")) < 0) {
            n = s.indexOf(":");
        }
        String substring = s;
        if (n > 0) {
            substring = s.substring(n + 1);
        }
        return substring;
    }
    
    private static DexProfileData findByDexName(final DexProfileData[] array, String key) {
        if (array.length <= 0) {
            return null;
        }
        key = extractKey(key);
        for (int i = 0; i < array.length; ++i) {
            if (array[i].dexName.equals(key)) {
                return array[i];
            }
        }
        return null;
    }
    
    private static String generateDexKey(final String str, final String str2, final byte[] array) {
        final String dexKeySeparator = ProfileVersion.dexKeySeparator(array);
        if (str.length() <= 0) {
            return enforceSeparator(str2, dexKeySeparator);
        }
        if (str2.equals("classes.dex")) {
            return str;
        }
        if (str2.contains("!") || str2.contains(":")) {
            return enforceSeparator(str2, dexKeySeparator);
        }
        if (str2.endsWith(".apk")) {
            return str2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(ProfileVersion.dexKeySeparator(array));
        sb.append(str2);
        return sb.toString();
    }
    
    private static int getMethodBitmapStorageSize(final int n) {
        return roundUpToByte(n * 2) / 8;
    }
    
    private static int methodFlagBitmapIndex(final int i, final int n, final int n2) {
        if (i == 1) {
            throw Encoding.error("HOT methods are not stored in the bitmap");
        }
        if (i == 2) {
            return n;
        }
        if (i == 4) {
            return n + n2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected flag: ");
        sb.append(i);
        throw Encoding.error(sb.toString());
    }
    
    private static int[] readClasses(final InputStream inputStream, final int n) throws IOException {
        final int[] array = new int[n];
        int i = 0;
        int n2 = 0;
        while (i < n) {
            n2 += Encoding.readUInt16(inputStream);
            array[i] = n2;
            ++i;
        }
        return array;
    }
    
    private static int readFlagsFromBitmap(final BitSet set, final int n, final int n2) {
        int n3 = 2;
        if (!set.get(methodFlagBitmapIndex(2, n, n2))) {
            n3 = 0;
        }
        int n4 = n3;
        if (set.get(methodFlagBitmapIndex(4, n, n2))) {
            n4 = (n3 | 0x4);
        }
        return n4;
    }
    
    static byte[] readHeader(final InputStream inputStream, final byte[] a) throws IOException {
        if (Arrays.equals(a, Encoding.read(inputStream, a.length))) {
            return Encoding.read(inputStream, ProfileVersion.V010_P.length);
        }
        throw Encoding.error("Invalid magic");
    }
    
    private static void readHotMethodRegion(final InputStream inputStream, final DexProfileData dexProfileData) throws IOException {
        final int n = inputStream.available() - dexProfileData.hotMethodRegionSize;
        int n2 = 0;
        while (inputStream.available() > n) {
            final int i = n2 + Encoding.readUInt16(inputStream);
            dexProfileData.methods.put(i, 1);
            int uInt16 = Encoding.readUInt16(inputStream);
            while (true) {
                n2 = i;
                if (uInt16 <= 0) {
                    break;
                }
                skipInlineCache(inputStream);
                --uInt16;
            }
        }
        if (inputStream.available() == n) {
            return;
        }
        throw Encoding.error("Read too much data during profile line parse");
    }
    
    static DexProfileData[] readMeta(final InputStream inputStream, final byte[] array, final byte[] a2, final DexProfileData[] array2) throws IOException {
        if (Arrays.equals(array, ProfileVersion.METADATA_V001_N)) {
            if (!Arrays.equals(ProfileVersion.V015_S, a2)) {
                return readMetadata001(inputStream, array, array2);
            }
            throw Encoding.error("Requires new Baseline Profile Metadata. Please rebuild the APK with Android Gradle Plugin 7.2 Canary 7 or higher");
        }
        else {
            if (Arrays.equals(array, ProfileVersion.METADATA_V002)) {
                return readMetadataV002(inputStream, a2, array2);
            }
            throw Encoding.error("Unsupported meta version");
        }
    }
    
    static DexProfileData[] readMetadata001(InputStream inputStream, byte[] compressed, final DexProfileData[] array) throws IOException {
        if (Arrays.equals((byte[])compressed, ProfileVersion.METADATA_V001_N)) {
            final int uInt8 = Encoding.readUInt8(inputStream);
            compressed = Encoding.readCompressed(inputStream, (int)Encoding.readUInt32(inputStream), (int)Encoding.readUInt32(inputStream));
            if (inputStream.read() <= 0) {
                inputStream = new ByteArrayInputStream((byte[])compressed);
                try {
                    final DexProfileData[] metadataForNBody = readMetadataForNBody(inputStream, uInt8, array);
                    inputStream.close();
                    return metadataForNBody;
                }
                finally {
                    try {
                        inputStream.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)compressed).addSuppressed(exception);
                    }
                }
            }
            throw Encoding.error("Content found after the end of file");
        }
        throw Encoding.error("Unsupported meta version");
    }
    
    private static DexProfileData[] readMetadataForNBody(final InputStream inputStream, final int n, final DexProfileData[] array) throws IOException {
        final int available = inputStream.available();
        final int n2 = 0;
        if (available == 0) {
            return new DexProfileData[0];
        }
        if (n == array.length) {
            final String[] array2 = new String[n];
            final int[] array3 = new int[n];
            int n3 = 0;
            int i;
            while (true) {
                i = n2;
                if (n3 >= n) {
                    break;
                }
                final int uInt16 = Encoding.readUInt16(inputStream);
                array3[n3] = Encoding.readUInt16(inputStream);
                array2[n3] = Encoding.readString(inputStream, uInt16);
                ++n3;
            }
            while (i < n) {
                final DexProfileData dexProfileData = array[i];
                if (!dexProfileData.dexName.equals(array2[i])) {
                    throw Encoding.error("Order of dexfiles in metadata did not match baseline");
                }
                dexProfileData.classSetSize = array3[i];
                dexProfileData.classes = readClasses(inputStream, dexProfileData.classSetSize);
                ++i;
            }
            return array;
        }
        throw Encoding.error("Mismatched number of dex files found in metadata");
    }
    
    static DexProfileData[] readMetadataV002(InputStream inputStream, final byte[] array, final DexProfileData[] array2) throws IOException {
        final int uInt16 = Encoding.readUInt16(inputStream);
        final byte[] compressed = Encoding.readCompressed(inputStream, (int)Encoding.readUInt32(inputStream), (int)Encoding.readUInt32(inputStream));
        if (inputStream.read() <= 0) {
            inputStream = new ByteArrayInputStream(compressed);
            try {
                final DexProfileData[] metadataV002Body = readMetadataV002Body(inputStream, array, uInt16, array2);
                inputStream.close();
                return metadataV002Body;
            }
            finally {
                try {
                    inputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)(Object)array).addSuppressed(exception);
                }
            }
        }
        throw Encoding.error("Content found after the end of file");
    }
    
    private static DexProfileData[] readMetadataV002Body(final InputStream inputStream, final byte[] a, final int n, final DexProfileData[] array) throws IOException {
        final int available = inputStream.available();
        int i = 0;
        if (available == 0) {
            return new DexProfileData[0];
        }
        if (n == array.length) {
            while (i < n) {
                Encoding.readUInt16(inputStream);
                final String string = Encoding.readString(inputStream, Encoding.readUInt16(inputStream));
                final long uInt32 = Encoding.readUInt32(inputStream);
                final int uInt33 = Encoding.readUInt16(inputStream);
                final DexProfileData byDexName = findByDexName(array, string);
                if (byDexName == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Missing profile key: ");
                    sb.append(string);
                    throw Encoding.error(sb.toString());
                }
                byDexName.mTypeIdCount = uInt32;
                final int[] classes = readClasses(inputStream, uInt33);
                if (Arrays.equals(a, ProfileVersion.V001_N)) {
                    byDexName.classSetSize = uInt33;
                    byDexName.classes = classes;
                }
                ++i;
            }
            return array;
        }
        throw Encoding.error("Mismatched number of dex files found in metadata");
    }
    
    private static void readMethodBitmap(final InputStream inputStream, final DexProfileData dexProfileData) throws IOException {
        final BitSet value = BitSet.valueOf(Encoding.read(inputStream, Encoding.bitsToBytes(dexProfileData.numMethodIds * 2)));
        for (int i = 0; i < dexProfileData.numMethodIds; ++i) {
            final int flagsFromBitmap = readFlagsFromBitmap(value, i, dexProfileData.numMethodIds);
            if (flagsFromBitmap != 0) {
                Integer value2;
                if ((value2 = dexProfileData.methods.get(i)) == null) {
                    value2 = 0;
                }
                dexProfileData.methods.put(i, flagsFromBitmap | value2);
            }
        }
    }
    
    static DexProfileData[] readProfile(InputStream inputStream, byte[] compressed, final String s) throws IOException {
        if (Arrays.equals((byte[])compressed, ProfileVersion.V010_P)) {
            final int uInt8 = Encoding.readUInt8(inputStream);
            compressed = Encoding.readCompressed(inputStream, (int)Encoding.readUInt32(inputStream), (int)Encoding.readUInt32(inputStream));
            if (inputStream.read() <= 0) {
                inputStream = new ByteArrayInputStream((byte[])compressed);
                try {
                    final DexProfileData[] uncompressedBody = readUncompressedBody(inputStream, s, uInt8);
                    inputStream.close();
                    return uncompressedBody;
                }
                finally {
                    try {
                        inputStream.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)compressed).addSuppressed(exception);
                    }
                }
            }
            throw Encoding.error("Content found after the end of file");
        }
        throw Encoding.error("Unsupported version");
    }
    
    private static DexProfileData[] readUncompressedBody(final InputStream inputStream, final String s, final int n) throws IOException {
        final int available = inputStream.available();
        final int n2 = 0;
        if (available == 0) {
            return new DexProfileData[0];
        }
        final DexProfileData[] array = new DexProfileData[n];
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= n) {
                break;
            }
            final int uInt16 = Encoding.readUInt16(inputStream);
            final int uInt17 = Encoding.readUInt16(inputStream);
            array[n3] = new DexProfileData(s, Encoding.readString(inputStream, uInt16), Encoding.readUInt32(inputStream), 0L, uInt17, (int)Encoding.readUInt32(inputStream), (int)Encoding.readUInt32(inputStream), new int[uInt17], new TreeMap<Integer, Integer>());
            ++n3;
        }
        while (i < n) {
            final DexProfileData dexProfileData = array[i];
            readHotMethodRegion(inputStream, dexProfileData);
            dexProfileData.classes = readClasses(inputStream, dexProfileData.classSetSize);
            readMethodBitmap(inputStream, dexProfileData);
            ++i;
        }
        return array;
    }
    
    private static int roundUpToByte(final int n) {
        return n + 8 - 1 & 0xFFFFFFF8;
    }
    
    private static void setMethodBitmapBit(final byte[] array, int methodFlagBitmapIndex, int n, final DexProfileData dexProfileData) {
        methodFlagBitmapIndex = methodFlagBitmapIndex(methodFlagBitmapIndex, n, dexProfileData.numMethodIds);
        n = methodFlagBitmapIndex / 8;
        array[n] |= (byte)(1 << methodFlagBitmapIndex % 8);
    }
    
    private static void skipInlineCache(final InputStream inputStream) throws IOException {
        Encoding.readUInt16(inputStream);
        final int uInt8 = Encoding.readUInt8(inputStream);
        if (uInt8 == 6) {
            return;
        }
        int i;
        if ((i = uInt8) == 7) {
            return;
        }
        while (i > 0) {
            Encoding.readUInt8(inputStream);
            for (int j = Encoding.readUInt8(inputStream); j > 0; --j) {
                Encoding.readUInt16(inputStream);
            }
            --i;
        }
    }
    
    static boolean transcodeAndWriteBody(final OutputStream outputStream, final byte[] a, final DexProfileData[] array) throws IOException {
        if (Arrays.equals(a, ProfileVersion.V015_S)) {
            writeProfileForS(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, ProfileVersion.V010_P)) {
            writeProfileForP(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, ProfileVersion.V005_O)) {
            writeProfileForO(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, ProfileVersion.V009_O_MR1)) {
            writeProfileForO_MR1(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, ProfileVersion.V001_N)) {
            writeProfileForN(outputStream, array);
            return true;
        }
        return false;
    }
    
    private static void writeClasses(final OutputStream outputStream, final DexProfileData dexProfileData) throws IOException {
        final int[] classes = dexProfileData.classes;
        final int length = classes.length;
        int i = 0;
        int intValue = 0;
        while (i < length) {
            final Integer value = classes[i];
            Encoding.writeUInt16(outputStream, value - intValue);
            intValue = value;
            ++i;
        }
    }
    
    private static WritableFileSection writeDexFileSection(final DexProfileData[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            Encoding.writeUInt16(byteArrayOutputStream, array.length);
            int i = 0;
            int j = 2;
            while (i < array.length) {
                final DexProfileData dexProfileData = array[i];
                Encoding.writeUInt32(byteArrayOutputStream, dexProfileData.dexChecksum);
                Encoding.writeUInt32(byteArrayOutputStream, dexProfileData.mTypeIdCount);
                Encoding.writeUInt32(byteArrayOutputStream, dexProfileData.numMethodIds);
                final String generateDexKey = generateDexKey(dexProfileData.apkName, dexProfileData.dexName, ProfileVersion.V015_S);
                final int utf8Length = Encoding.utf8Length(generateDexKey);
                Encoding.writeUInt16(byteArrayOutputStream, utf8Length);
                j = j + 4 + 4 + 4 + 2 + utf8Length * 1;
                Encoding.writeString(byteArrayOutputStream, generateDexKey);
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final WritableFileSection writableFileSection = new WritableFileSection(FileSectionType.DEX_FILES, j, byteArray, false);
                byteArrayOutputStream.close();
                return writableFileSection;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw Encoding.error(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    static void writeHeader(final OutputStream outputStream, final byte[] b) throws IOException {
        outputStream.write(ProfileTranscoder.MAGIC_PROF);
        outputStream.write(b);
    }
    
    private static void writeLineData(final OutputStream outputStream, final DexProfileData dexProfileData) throws IOException {
        writeMethodsWithInlineCaches(outputStream, dexProfileData);
        writeClasses(outputStream, dexProfileData);
        writeMethodBitmap(outputStream, dexProfileData);
    }
    
    private static void writeLineHeader(final OutputStream outputStream, final DexProfileData dexProfileData, final String s) throws IOException {
        Encoding.writeUInt16(outputStream, Encoding.utf8Length(s));
        Encoding.writeUInt16(outputStream, dexProfileData.classSetSize);
        Encoding.writeUInt32(outputStream, dexProfileData.hotMethodRegionSize);
        Encoding.writeUInt32(outputStream, dexProfileData.dexChecksum);
        Encoding.writeUInt32(outputStream, dexProfileData.numMethodIds);
        Encoding.writeString(outputStream, s);
    }
    
    private static void writeMethodBitmap(final OutputStream outputStream, final DexProfileData dexProfileData) throws IOException {
        final byte[] b = new byte[getMethodBitmapStorageSize(dexProfileData.numMethodIds)];
        for (final Map.Entry<Integer, V> entry : dexProfileData.methods.entrySet()) {
            final int intValue = entry.getKey();
            final int intValue2 = (int)entry.getValue();
            if ((intValue2 & 0x2) != 0x0) {
                setMethodBitmapBit(b, 2, intValue, dexProfileData);
            }
            if ((intValue2 & 0x4) != 0x0) {
                setMethodBitmapBit(b, 4, intValue, dexProfileData);
            }
        }
        outputStream.write(b);
    }
    
    private static void writeMethodsWithInlineCaches(final OutputStream outputStream, final DexProfileData dexProfileData) throws IOException {
        final Iterator<Map.Entry<Integer, Integer>> iterator = dexProfileData.methods.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<Integer, V> entry = (Map.Entry<Integer, V>)iterator.next();
            final int intValue = entry.getKey();
            if (((int)entry.getValue() & 0x1) == 0x0) {
                continue;
            }
            Encoding.writeUInt16(outputStream, intValue - n);
            Encoding.writeUInt16(outputStream, 0);
            n = intValue;
        }
    }
    
    private static void writeProfileForN(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        Encoding.writeUInt16(outputStream, array.length);
        for (final DexProfileData dexProfileData : array) {
            final String generateDexKey = generateDexKey(dexProfileData.apkName, dexProfileData.dexName, ProfileVersion.V001_N);
            Encoding.writeUInt16(outputStream, Encoding.utf8Length(generateDexKey));
            Encoding.writeUInt16(outputStream, dexProfileData.methods.size());
            Encoding.writeUInt16(outputStream, dexProfileData.classes.length);
            Encoding.writeUInt32(outputStream, dexProfileData.dexChecksum);
            Encoding.writeString(outputStream, generateDexKey);
            final Iterator<Integer> iterator = dexProfileData.methods.keySet().iterator();
            while (iterator.hasNext()) {
                Encoding.writeUInt16(outputStream, iterator.next());
            }
            final int[] classes = dexProfileData.classes;
            for (int length2 = classes.length, j = 0; j < length2; ++j) {
                Encoding.writeUInt16(outputStream, classes[j]);
            }
        }
    }
    
    private static void writeProfileForO(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        Encoding.writeUInt8(outputStream, array.length);
        for (final DexProfileData dexProfileData : array) {
            final int size = dexProfileData.methods.size();
            final String generateDexKey = generateDexKey(dexProfileData.apkName, dexProfileData.dexName, ProfileVersion.V005_O);
            Encoding.writeUInt16(outputStream, Encoding.utf8Length(generateDexKey));
            Encoding.writeUInt16(outputStream, dexProfileData.classes.length);
            Encoding.writeUInt32(outputStream, size * 4);
            Encoding.writeUInt32(outputStream, dexProfileData.dexChecksum);
            Encoding.writeString(outputStream, generateDexKey);
            final Iterator<Integer> iterator = dexProfileData.methods.keySet().iterator();
            while (iterator.hasNext()) {
                Encoding.writeUInt16(outputStream, iterator.next());
                Encoding.writeUInt16(outputStream, 0);
            }
            final int[] classes = dexProfileData.classes;
            for (int length2 = classes.length, j = 0; j < length2; ++j) {
                Encoding.writeUInt16(outputStream, classes[j]);
            }
        }
    }
    
    private static void writeProfileForO_MR1(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        final byte[] compressibleBody = createCompressibleBody(array, ProfileVersion.V009_O_MR1);
        Encoding.writeUInt8(outputStream, array.length);
        Encoding.writeCompressed(outputStream, compressibleBody);
    }
    
    private static void writeProfileForP(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        final byte[] compressibleBody = createCompressibleBody(array, ProfileVersion.V010_P);
        Encoding.writeUInt8(outputStream, array.length);
        Encoding.writeCompressed(outputStream, compressibleBody);
    }
    
    private static void writeProfileForS(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        writeProfileSections(outputStream, array);
    }
    
    private static void writeProfileSections(final OutputStream outputStream, final DexProfileData[] array) throws IOException {
        final ArrayList list = new ArrayList(3);
        final ArrayList list2 = new ArrayList(3);
        list.add(writeDexFileSection(array));
        list.add(createCompressibleClassSection(array));
        list.add(createCompressibleMethodsSection(array));
        long n = ProfileVersion.V015_S.length + (long)ProfileTranscoder.MAGIC_PROF.length + 4L + list.size() * 16;
        Encoding.writeUInt32(outputStream, list.size());
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= list.size()) {
                break;
            }
            final WritableFileSection writableFileSection = (WritableFileSection)list.get(n3);
            Encoding.writeUInt32(outputStream, writableFileSection.mType.getValue());
            Encoding.writeUInt32(outputStream, n);
            int n5;
            if (writableFileSection.mNeedsCompression) {
                final long n4 = writableFileSection.mContents.length;
                final byte[] compress = Encoding.compress(writableFileSection.mContents);
                list2.add(compress);
                Encoding.writeUInt32(outputStream, compress.length);
                Encoding.writeUInt32(outputStream, n4);
                n5 = compress.length;
            }
            else {
                list2.add(writableFileSection.mContents);
                Encoding.writeUInt32(outputStream, writableFileSection.mContents.length);
                Encoding.writeUInt32(outputStream, 0L);
                n5 = writableFileSection.mContents.length;
            }
            n += n5;
            ++n3;
        }
        while (i < list2.size()) {
            outputStream.write((byte[])list2.get(i));
            ++i;
        }
    }
}

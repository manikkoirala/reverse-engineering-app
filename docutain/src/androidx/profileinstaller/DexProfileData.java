// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.util.TreeMap;

class DexProfileData
{
    final String apkName;
    int classSetSize;
    int[] classes;
    final long dexChecksum;
    final String dexName;
    final int hotMethodRegionSize;
    long mTypeIdCount;
    final TreeMap<Integer, Integer> methods;
    final int numMethodIds;
    
    DexProfileData(final String apkName, final String dexName, final long dexChecksum, final long mTypeIdCount, final int classSetSize, final int hotMethodRegionSize, final int numMethodIds, final int[] classes, final TreeMap<Integer, Integer> methods) {
        this.apkName = apkName;
        this.dexName = dexName;
        this.dexChecksum = dexChecksum;
        this.mTypeIdCount = mTypeIdCount;
        this.classSetSize = classSetSize;
        this.hotMethodRegionSize = hotMethodRegionSize;
        this.numMethodIds = numMethodIds;
        this.classes = classes;
        this.methods = methods;
    }
}

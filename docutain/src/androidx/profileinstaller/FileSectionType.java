// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

enum FileSectionType
{
    private static final FileSectionType[] $VALUES;
    
    AGGREGATION_COUNT(4L), 
    CLASSES(2L), 
    DEX_FILES(0L), 
    EXTRA_DESCRIPTORS(1L), 
    METHODS(3L);
    
    private final long mValue;
    
    private static /* synthetic */ FileSectionType[] $values() {
        return new FileSectionType[] { FileSectionType.DEX_FILES, FileSectionType.EXTRA_DESCRIPTORS, FileSectionType.CLASSES, FileSectionType.METHODS, FileSectionType.AGGREGATION_COUNT };
    }
    
    static {
        $VALUES = $values();
    }
    
    private FileSectionType(final long mValue) {
        this.mValue = mValue;
    }
    
    static FileSectionType fromValue(final long lng) {
        final FileSectionType[] values = values();
        for (int i = 0; i < values.length; ++i) {
            if (values[i].getValue() == lng) {
                return values[i];
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported FileSection Type ");
        sb.append(lng);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public long getValue() {
        return this.mValue;
    }
}

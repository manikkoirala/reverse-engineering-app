// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public final class WorkInfo
{
    private final int mGeneration;
    private UUID mId;
    private Data mOutputData;
    private Data mProgress;
    private int mRunAttemptCount;
    private State mState;
    private Set<String> mTags;
    
    public WorkInfo(final UUID mId, final State mState, final Data mOutputData, final List<String> c, final Data mProgress, final int mRunAttemptCount, final int mGeneration) {
        this.mId = mId;
        this.mState = mState;
        this.mOutputData = mOutputData;
        this.mTags = new HashSet<String>(c);
        this.mProgress = mProgress;
        this.mRunAttemptCount = mRunAttemptCount;
        this.mGeneration = mGeneration;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final WorkInfo workInfo = (WorkInfo)o;
            return this.mRunAttemptCount == workInfo.mRunAttemptCount && this.mGeneration == workInfo.mGeneration && this.mId.equals(workInfo.mId) && this.mState == workInfo.mState && this.mOutputData.equals(workInfo.mOutputData) && this.mTags.equals(workInfo.mTags) && this.mProgress.equals(workInfo.mProgress);
        }
        return false;
    }
    
    public int getGeneration() {
        return this.mGeneration;
    }
    
    public UUID getId() {
        return this.mId;
    }
    
    public Data getOutputData() {
        return this.mOutputData;
    }
    
    public Data getProgress() {
        return this.mProgress;
    }
    
    public int getRunAttemptCount() {
        return this.mRunAttemptCount;
    }
    
    public State getState() {
        return this.mState;
    }
    
    public Set<String> getTags() {
        return this.mTags;
    }
    
    @Override
    public int hashCode() {
        return (((((this.mId.hashCode() * 31 + this.mState.hashCode()) * 31 + this.mOutputData.hashCode()) * 31 + this.mTags.hashCode()) * 31 + this.mProgress.hashCode()) * 31 + this.mRunAttemptCount) * 31 + this.mGeneration;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkInfo{mId='");
        sb.append(this.mId);
        sb.append('\'');
        sb.append(", mState=");
        sb.append(this.mState);
        sb.append(", mOutputData=");
        sb.append(this.mOutputData);
        sb.append(", mTags=");
        sb.append(this.mTags);
        sb.append(", mProgress=");
        sb.append(this.mProgress);
        sb.append('}');
        return sb.toString();
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        BLOCKED, 
        CANCELLED, 
        ENQUEUED, 
        FAILED, 
        RUNNING, 
        SUCCEEDED;
        
        private static /* synthetic */ State[] $values() {
            return new State[] { State.ENQUEUED, State.RUNNING, State.SUCCEEDED, State.FAILED, State.BLOCKED, State.CANCELLED };
        }
        
        static {
            $VALUES = $values();
        }
        
        public boolean isFinished() {
            return this == State.SUCCEEDED || this == State.FAILED || this == State.CANCELLED;
        }
    }
}

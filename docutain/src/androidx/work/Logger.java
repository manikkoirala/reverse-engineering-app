// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.util.Log;

public abstract class Logger
{
    private static final int MAX_PREFIXED_TAG_LENGTH = 20;
    private static final int MAX_TAG_LENGTH = 23;
    private static final String TAG_PREFIX = "WM-";
    private static final Object sLock;
    private static volatile Logger sLogger;
    
    static {
        sLock = new Object();
    }
    
    public Logger(final int n) {
    }
    
    public static Logger get() {
        synchronized (Logger.sLock) {
            if (Logger.sLogger == null) {
                Logger.sLogger = new LogcatLogger(3);
            }
            return Logger.sLogger;
        }
    }
    
    public static void setLogger(final Logger sLogger) {
        synchronized (Logger.sLock) {
            Logger.sLogger = sLogger;
        }
    }
    
    public static String tagWithPrefix(final String str) {
        final int length = str.length();
        final StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        final int max_PREFIXED_TAG_LENGTH = Logger.MAX_PREFIXED_TAG_LENGTH;
        if (length >= max_PREFIXED_TAG_LENGTH) {
            sb.append(str.substring(0, max_PREFIXED_TAG_LENGTH));
        }
        else {
            sb.append(str);
        }
        return sb.toString();
    }
    
    public abstract void debug(final String p0, final String p1);
    
    public abstract void debug(final String p0, final String p1, final Throwable p2);
    
    public abstract void error(final String p0, final String p1);
    
    public abstract void error(final String p0, final String p1, final Throwable p2);
    
    public abstract void info(final String p0, final String p1);
    
    public abstract void info(final String p0, final String p1, final Throwable p2);
    
    public abstract void verbose(final String p0, final String p1);
    
    public abstract void verbose(final String p0, final String p1, final Throwable p2);
    
    public abstract void warning(final String p0, final String p1);
    
    public abstract void warning(final String p0, final String p1, final Throwable p2);
    
    public static class LogcatLogger extends Logger
    {
        private final int mLoggingLevel;
        
        public LogcatLogger(final int mLoggingLevel) {
            super(mLoggingLevel);
            this.mLoggingLevel = mLoggingLevel;
        }
        
        @Override
        public void debug(final String s, final String s2) {
            if (this.mLoggingLevel <= 3) {
                Log.d(s, s2);
            }
        }
        
        @Override
        public void debug(final String s, final String s2, final Throwable t) {
            if (this.mLoggingLevel <= 3) {
                Log.d(s, s2, t);
            }
        }
        
        @Override
        public void error(final String s, final String s2) {
            if (this.mLoggingLevel <= 6) {
                Log.e(s, s2);
            }
        }
        
        @Override
        public void error(final String s, final String s2, final Throwable t) {
            if (this.mLoggingLevel <= 6) {
                Log.e(s, s2, t);
            }
        }
        
        @Override
        public void info(final String s, final String s2) {
            if (this.mLoggingLevel <= 4) {
                Log.i(s, s2);
            }
        }
        
        @Override
        public void info(final String s, final String s2, final Throwable t) {
            if (this.mLoggingLevel <= 4) {
                Log.i(s, s2, t);
            }
        }
        
        @Override
        public void verbose(final String s, final String s2) {
            if (this.mLoggingLevel <= 2) {
                Log.v(s, s2);
            }
        }
        
        @Override
        public void verbose(final String s, final String s2, final Throwable t) {
            if (this.mLoggingLevel <= 2) {
                Log.v(s, s2, t);
            }
        }
        
        @Override
        public void warning(final String s, final String s2) {
            if (this.mLoggingLevel <= 5) {
                Log.w(s, s2);
            }
        }
        
        @Override
        public void warning(final String s, final String s2, final Throwable t) {
            if (this.mLoggingLevel <= 5) {
                Log.w(s, s2, t);
            }
        }
    }
}

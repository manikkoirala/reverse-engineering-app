// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;

public abstract class WorkContinuation
{
    public static WorkContinuation combine(final List<WorkContinuation> list) {
        return list.get(0).combineInternal(list);
    }
    
    protected abstract WorkContinuation combineInternal(final List<WorkContinuation> p0);
    
    public abstract Operation enqueue();
    
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos();
    
    public abstract LiveData<List<WorkInfo>> getWorkInfosLiveData();
    
    public final WorkContinuation then(final OneTimeWorkRequest o) {
        return this.then(Collections.singletonList(o));
    }
    
    public abstract WorkContinuation then(final List<OneTimeWorkRequest> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.List;

public abstract class InputMerger
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("InputMerger");
    }
    
    public static InputMerger fromClassName(final String s) {
        try {
            return (InputMerger)Class.forName(s).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            final Logger value = Logger.get();
            final String tag = InputMerger.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Trouble instantiating + ");
            sb.append(s);
            value.error(tag, sb.toString(), ex);
            return null;
        }
    }
    
    public abstract Data merge(final List<Data> p0);
}

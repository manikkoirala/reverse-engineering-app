// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IWorkManagerImplCallback extends IInterface
{
    public static final String DESCRIPTOR = "androidx.work.multiprocess.IWorkManagerImplCallback";
    
    void onFailure(final String p0) throws RemoteException;
    
    void onSuccess(final byte[] p0) throws RemoteException;
    
    public static class Default implements IWorkManagerImplCallback
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void onFailure(final String s) throws RemoteException {
        }
        
        @Override
        public void onSuccess(final byte[] array) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IWorkManagerImplCallback
    {
        static final int TRANSACTION_onFailure = 2;
        static final int TRANSACTION_onSuccess = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.work.multiprocess.IWorkManagerImplCallback");
        }
        
        public static IWorkManagerImplCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.work.multiprocess.IWorkManagerImplCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof IWorkManagerImplCallback) {
                return (IWorkManagerImplCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImplCallback");
            }
            if (n != 1598968902) {
                if (n != 1) {
                    if (n != 2) {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    this.onFailure(parcel.readString());
                }
                else {
                    this.onSuccess(parcel.createByteArray());
                }
                return true;
            }
            parcel2.writeString("androidx.work.multiprocess.IWorkManagerImplCallback");
            return true;
        }
        
        private static class Proxy implements IWorkManagerImplCallback
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.work.multiprocess.IWorkManagerImplCallback";
            }
            
            @Override
            public void onFailure(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImplCallback");
                    obtain.writeString(s);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onSuccess(final byte[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImplCallback");
                    obtain.writeByteArray(array);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.WorkInfo;
import androidx.work.WorkQuery;
import androidx.work.PeriodicWorkRequest;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.WorkRequest;
import androidx.work.WorkContinuation;
import java.util.UUID;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;
import java.util.Collections;
import androidx.work.OneTimeWorkRequest;
import androidx.work.ExistingWorkPolicy;
import androidx.work.impl.WorkManagerImpl;
import android.content.Context;

public abstract class RemoteWorkManager
{
    protected RemoteWorkManager() {
    }
    
    public static RemoteWorkManager getInstance(final Context context) {
        final RemoteWorkManager remoteWorkManager = WorkManagerImpl.getInstance(context).getRemoteWorkManager();
        if (remoteWorkManager != null) {
            return remoteWorkManager;
        }
        throw new IllegalStateException("Unable to initialize RemoteWorkManager");
    }
    
    public final RemoteWorkContinuation beginUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final OneTimeWorkRequest o) {
        return this.beginUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    public abstract RemoteWorkContinuation beginUniqueWork(final String p0, final ExistingWorkPolicy p1, final List<OneTimeWorkRequest> p2);
    
    public final RemoteWorkContinuation beginWith(final OneTimeWorkRequest o) {
        return this.beginWith(Collections.singletonList(o));
    }
    
    public abstract RemoteWorkContinuation beginWith(final List<OneTimeWorkRequest> p0);
    
    public abstract ListenableFuture<Void> cancelAllWork();
    
    public abstract ListenableFuture<Void> cancelAllWorkByTag(final String p0);
    
    public abstract ListenableFuture<Void> cancelUniqueWork(final String p0);
    
    public abstract ListenableFuture<Void> cancelWorkById(final UUID p0);
    
    public abstract ListenableFuture<Void> enqueue(final WorkContinuation p0);
    
    public abstract ListenableFuture<Void> enqueue(final WorkRequest p0);
    
    public abstract ListenableFuture<Void> enqueue(final List<WorkRequest> p0);
    
    public abstract ListenableFuture<Void> enqueueUniquePeriodicWork(final String p0, final ExistingPeriodicWorkPolicy p1, final PeriodicWorkRequest p2);
    
    public final ListenableFuture<Void> enqueueUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final OneTimeWorkRequest o) {
        return this.enqueueUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    public abstract ListenableFuture<Void> enqueueUniqueWork(final String p0, final ExistingWorkPolicy p1, final List<OneTimeWorkRequest> p2);
    
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos(final WorkQuery p0);
    
    public abstract ListenableFuture<Void> setForegroundAsync(final String p0, final ForegroundInfo p1);
    
    public abstract ListenableFuture<Void> setProgress(final UUID p0, final Data p1);
}

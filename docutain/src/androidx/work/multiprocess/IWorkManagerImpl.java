// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IWorkManagerImpl extends IInterface
{
    public static final String DESCRIPTOR = "androidx.work.multiprocess.IWorkManagerImpl";
    
    void cancelAllWork(final IWorkManagerImplCallback p0) throws RemoteException;
    
    void cancelAllWorkByTag(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void cancelUniqueWork(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void cancelWorkById(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void enqueueContinuation(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void enqueueWorkRequests(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void queryWorkInfo(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void setForegroundAsync(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void setProgress(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void updateUniquePeriodicWorkRequest(final String p0, final byte[] p1, final IWorkManagerImplCallback p2) throws RemoteException;
    
    public static class Default implements IWorkManagerImpl
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void cancelAllWork(final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelAllWorkByTag(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelUniqueWork(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelWorkById(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void enqueueContinuation(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void enqueueWorkRequests(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void queryWorkInfo(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void setForegroundAsync(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void setProgress(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void updateUniquePeriodicWorkRequest(final String s, final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IWorkManagerImpl
    {
        static final int TRANSACTION_cancelAllWork = 7;
        static final int TRANSACTION_cancelAllWorkByTag = 5;
        static final int TRANSACTION_cancelUniqueWork = 6;
        static final int TRANSACTION_cancelWorkById = 4;
        static final int TRANSACTION_enqueueContinuation = 3;
        static final int TRANSACTION_enqueueWorkRequests = 1;
        static final int TRANSACTION_queryWorkInfo = 8;
        static final int TRANSACTION_setForegroundAsync = 10;
        static final int TRANSACTION_setProgress = 9;
        static final int TRANSACTION_updateUniquePeriodicWorkRequest = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.work.multiprocess.IWorkManagerImpl");
        }
        
        public static IWorkManagerImpl asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.work.multiprocess.IWorkManagerImpl");
            if (queryLocalInterface != null && queryLocalInterface instanceof IWorkManagerImpl) {
                return (IWorkManagerImpl)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
            }
            if (n != 1598968902) {
                switch (n) {
                    default: {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    case 10: {
                        this.setForegroundAsync(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 9: {
                        this.setProgress(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 8: {
                        this.queryWorkInfo(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 7: {
                        this.cancelAllWork(IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 6: {
                        this.cancelUniqueWork(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 5: {
                        this.cancelAllWorkByTag(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 4: {
                        this.cancelWorkById(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 3: {
                        this.enqueueContinuation(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 2: {
                        this.updateUniquePeriodicWorkRequest(parcel.readString(), parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                    case 1: {
                        this.enqueueWorkRequests(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                        break;
                    }
                }
                return true;
            }
            parcel2.writeString("androidx.work.multiprocess.IWorkManagerImpl");
            return true;
        }
        
        private static class Proxy implements IWorkManagerImpl
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void cancelAllWork(final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(7, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelAllWorkByTag(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(5, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelUniqueWork(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(6, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelWorkById(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(4, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void enqueueContinuation(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void enqueueWorkRequests(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.work.multiprocess.IWorkManagerImpl";
            }
            
            @Override
            public void queryWorkInfo(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(8, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setForegroundAsync(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(10, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setProgress(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(9, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void updateUniquePeriodicWorkRequest(final String s, final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IListenableWorkerImpl extends IInterface
{
    public static final String DESCRIPTOR = "androidx.work.multiprocess.IListenableWorkerImpl";
    
    void interrupt(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void startWork(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    public static class Default implements IListenableWorkerImpl
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void interrupt(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void startWork(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IListenableWorkerImpl
    {
        static final int TRANSACTION_interrupt = 2;
        static final int TRANSACTION_startWork = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.work.multiprocess.IListenableWorkerImpl");
        }
        
        public static IListenableWorkerImpl asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.work.multiprocess.IListenableWorkerImpl");
            if (queryLocalInterface != null && queryLocalInterface instanceof IListenableWorkerImpl) {
                return (IListenableWorkerImpl)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("androidx.work.multiprocess.IListenableWorkerImpl");
            }
            if (n != 1598968902) {
                if (n != 1) {
                    if (n != 2) {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    this.interrupt(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                }
                else {
                    this.startWork(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                }
                return true;
            }
            parcel2.writeString("androidx.work.multiprocess.IListenableWorkerImpl");
            return true;
        }
        
        private static class Proxy implements IListenableWorkerImpl
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.work.multiprocess.IListenableWorkerImpl";
            }
            
            @Override
            public void interrupt(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IListenableWorkerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void startWork(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IListenableWorkerImpl");
                    obtain.writeByteArray(array);
                    obtain.writeStrongInterface((IInterface)workManagerImplCallback);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

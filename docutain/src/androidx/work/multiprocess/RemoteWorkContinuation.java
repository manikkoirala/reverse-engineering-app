// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import java.util.Collections;
import androidx.work.OneTimeWorkRequest;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;

public abstract class RemoteWorkContinuation
{
    protected RemoteWorkContinuation() {
    }
    
    public static RemoteWorkContinuation combine(final List<RemoteWorkContinuation> list) {
        return list.get(0).combineInternal(list);
    }
    
    protected abstract RemoteWorkContinuation combineInternal(final List<RemoteWorkContinuation> p0);
    
    public abstract ListenableFuture<Void> enqueue();
    
    public final RemoteWorkContinuation then(final OneTimeWorkRequest o) {
        return this.then(Collections.singletonList(o));
    }
    
    public abstract RemoteWorkContinuation then(final List<OneTimeWorkRequest> p0);
}

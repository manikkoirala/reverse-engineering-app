// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.content.Context;

public abstract class WorkerFactory
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("WorkerFactory");
    }
    
    public static WorkerFactory getDefaultWorkerFactory() {
        return new WorkerFactory() {
            @Override
            public ListenableWorker createWorker(final Context context, final String s, final WorkerParameters workerParameters) {
                return null;
            }
        };
    }
    
    public abstract ListenableWorker createWorker(final Context p0, final String p1, final WorkerParameters p2);
    
    public final ListenableWorker createWorkerWithDefaultFallback(final Context context, final String s, final WorkerParameters workerParameters) {
        ListenableWorker worker;
        final ListenableWorker listenableWorker = worker = this.createWorker(context, s, workerParameters);
        if (listenableWorker == null) {
            Class<? extends ListenableWorker> subclass = null;
            try {
                subclass = Class.forName(s).asSubclass(ListenableWorker.class);
            }
            finally {
                final Logger value = Logger.get();
                final String tag = WorkerFactory.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid class: ");
                sb.append(s);
                final Throwable t;
                value.error(tag, sb.toString(), t);
            }
            worker = listenableWorker;
            if (subclass != null) {
                try {
                    final ListenableWorker listenableWorker2 = (ListenableWorker)subclass.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                }
                finally {
                    final Logger value2 = Logger.get();
                    final String tag2 = WorkerFactory.TAG;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not instantiate ");
                    sb2.append(s);
                    final Throwable t2;
                    value2.error(tag2, sb2.toString(), t2);
                    worker = listenableWorker;
                }
            }
        }
        if (worker != null && worker.isUsed()) {
            final String name = this.getClass().getName();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("WorkerFactory (");
            sb3.append(name);
            sb3.append(") returned an instance of a ListenableWorker (");
            sb3.append(s);
            sb3.append(") which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.");
            throw new IllegalStateException(sb3.toString());
        }
        return worker;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.UUID;
import android.content.Context;

public interface ForegroundUpdater
{
    ListenableFuture<Void> setForegroundAsync(final Context p0, final UUID p1, final ForegroundInfo p2);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;

public interface Operation
{
    public static final IN_PROGRESS IN_PROGRESS = new IN_PROGRESS();
    public static final SUCCESS SUCCESS = new SUCCESS();
    
    ListenableFuture<SUCCESS> getResult();
    
    LiveData<State> getState();
    
    public abstract static class State
    {
        State() {
        }
        
        public static final class FAILURE extends State
        {
            private final Throwable mThrowable;
            
            public FAILURE(final Throwable mThrowable) {
                this.mThrowable = mThrowable;
            }
            
            public Throwable getThrowable() {
                return this.mThrowable;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("FAILURE (");
                sb.append(this.mThrowable.getMessage());
                sb.append(")");
                return sb.toString();
            }
        }
        
        public static final class IN_PROGRESS extends State
        {
            private IN_PROGRESS() {
            }
            
            @Override
            public String toString() {
                return "IN_PROGRESS";
            }
        }
        
        public static final class SUCCESS extends State
        {
            private SUCCESS() {
            }
            
            @Override
            public String toString() {
                return "SUCCESS";
            }
        }
    }
}

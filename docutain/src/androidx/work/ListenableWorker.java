// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.net.Uri;
import java.util.List;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.Set;
import android.net.Network;
import java.util.UUID;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import android.content.Context;

public abstract class ListenableWorker
{
    private Context mAppContext;
    private volatile boolean mStopped;
    private boolean mUsed;
    private WorkerParameters mWorkerParams;
    
    public ListenableWorker(final Context mAppContext, final WorkerParameters mWorkerParams) {
        if (mAppContext == null) {
            throw new IllegalArgumentException("Application Context is null");
        }
        if (mWorkerParams != null) {
            this.mAppContext = mAppContext;
            this.mWorkerParams = mWorkerParams;
            return;
        }
        throw new IllegalArgumentException("WorkerParameters is null");
    }
    
    public final Context getApplicationContext() {
        return this.mAppContext;
    }
    
    public Executor getBackgroundExecutor() {
        return this.mWorkerParams.getBackgroundExecutor();
    }
    
    public ListenableFuture<ForegroundInfo> getForegroundInfoAsync() {
        final SettableFuture<Object> create = SettableFuture.create();
        create.setException(new IllegalStateException("Expedited WorkRequests require a ListenableWorker to provide an implementation for `getForegroundInfoAsync()`"));
        return (ListenableFuture<ForegroundInfo>)create;
    }
    
    public final UUID getId() {
        return this.mWorkerParams.getId();
    }
    
    public final Data getInputData() {
        return this.mWorkerParams.getInputData();
    }
    
    public final Network getNetwork() {
        return this.mWorkerParams.getNetwork();
    }
    
    public final int getRunAttemptCount() {
        return this.mWorkerParams.getRunAttemptCount();
    }
    
    public final Set<String> getTags() {
        return this.mWorkerParams.getTags();
    }
    
    public TaskExecutor getTaskExecutor() {
        return this.mWorkerParams.getTaskExecutor();
    }
    
    public final List<String> getTriggeredContentAuthorities() {
        return this.mWorkerParams.getTriggeredContentAuthorities();
    }
    
    public final List<Uri> getTriggeredContentUris() {
        return this.mWorkerParams.getTriggeredContentUris();
    }
    
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerParams.getWorkerFactory();
    }
    
    public final boolean isStopped() {
        return this.mStopped;
    }
    
    public final boolean isUsed() {
        return this.mUsed;
    }
    
    public void onStopped() {
    }
    
    public final ListenableFuture<Void> setForegroundAsync(final ForegroundInfo foregroundInfo) {
        return this.mWorkerParams.getForegroundUpdater().setForegroundAsync(this.getApplicationContext(), this.getId(), foregroundInfo);
    }
    
    public ListenableFuture<Void> setProgressAsync(final Data data) {
        return this.mWorkerParams.getProgressUpdater().updateProgress(this.getApplicationContext(), this.getId(), data);
    }
    
    public final void setUsed() {
        this.mUsed = true;
    }
    
    public abstract ListenableFuture<Result> startWork();
    
    public final void stop() {
        this.mStopped = true;
        this.onStopped();
    }
    
    public abstract static class Result
    {
        Result() {
        }
        
        public static Result failure() {
            return new Failure();
        }
        
        public static Result failure(final Data data) {
            return new Failure(data);
        }
        
        public static Result retry() {
            return new Retry();
        }
        
        public static Result success() {
            return new Success();
        }
        
        public static Result success(final Data data) {
            return new Success(data);
        }
        
        public abstract Data getOutputData();
        
        public static final class Failure extends Result
        {
            private final Data mOutputData;
            
            public Failure() {
                this(Data.EMPTY);
            }
            
            public Failure(final Data mOutputData) {
                this.mOutputData = mOutputData;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && this.getClass() == o.getClass() && this.mOutputData.equals(((Failure)o).mOutputData));
            }
            
            @Override
            public Data getOutputData() {
                return this.mOutputData;
            }
            
            @Override
            public int hashCode() {
                return 846803280 + this.mOutputData.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failure {mOutputData=");
                sb.append(this.mOutputData);
                sb.append('}');
                return sb.toString();
            }
        }
        
        public static final class Retry extends Result
        {
            @Override
            public boolean equals(final Object o) {
                boolean b = true;
                if (this == o) {
                    return true;
                }
                if (o == null || this.getClass() != o.getClass()) {
                    b = false;
                }
                return b;
            }
            
            @Override
            public Data getOutputData() {
                return Data.EMPTY;
            }
            
            @Override
            public int hashCode() {
                return 25945934;
            }
            
            @Override
            public String toString() {
                return "Retry";
            }
        }
        
        public static final class Success extends Result
        {
            private final Data mOutputData;
            
            public Success() {
                this(Data.EMPTY);
            }
            
            public Success(final Data mOutputData) {
                this.mOutputData = mOutputData;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && this.getClass() == o.getClass() && this.mOutputData.equals(((Success)o).mOutputData));
            }
            
            @Override
            public Data getOutputData() {
                return this.mOutputData;
            }
            
            @Override
            public int hashCode() {
                return -1876823561 + this.mOutputData.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Success {mOutputData=");
                sb.append(this.mOutputData);
                sb.append('}');
                return sb.toString();
            }
        }
    }
}

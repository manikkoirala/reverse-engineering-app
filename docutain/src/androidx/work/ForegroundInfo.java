// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.app.Notification;

public final class ForegroundInfo
{
    private final int mForegroundServiceType;
    private final Notification mNotification;
    private final int mNotificationId;
    
    public ForegroundInfo(final int n, final Notification notification) {
        this(n, notification, 0);
    }
    
    public ForegroundInfo(final int mNotificationId, final Notification mNotification, final int mForegroundServiceType) {
        this.mNotificationId = mNotificationId;
        this.mNotification = mNotification;
        this.mForegroundServiceType = mForegroundServiceType;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final ForegroundInfo foregroundInfo = (ForegroundInfo)o;
            return this.mNotificationId == foregroundInfo.mNotificationId && this.mForegroundServiceType == foregroundInfo.mForegroundServiceType && this.mNotification.equals(foregroundInfo.mNotification);
        }
        return false;
    }
    
    public int getForegroundServiceType() {
        return this.mForegroundServiceType;
    }
    
    public Notification getNotification() {
        return this.mNotification;
    }
    
    public int getNotificationId() {
        return this.mNotificationId;
    }
    
    @Override
    public int hashCode() {
        return (this.mNotificationId * 31 + this.mForegroundServiceType) * 31 + this.mNotification.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ForegroundInfo{");
        sb.append("mNotificationId=");
        sb.append(this.mNotificationId);
        sb.append(", mForegroundServiceType=");
        sb.append(this.mForegroundServiceType);
        sb.append(", mNotification=");
        sb.append(this.mNotification);
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.Arrays;
import java.io.IOException;
import android.util.Log;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public final class Data
{
    public static final Data EMPTY;
    public static final int MAX_DATA_BYTES = 10240;
    private static final String TAG;
    Map<String, Object> mValues;
    
    static {
        TAG = Logger.tagWithPrefix("Data");
        EMPTY = new Builder().build();
    }
    
    Data() {
    }
    
    public Data(final Data data) {
        this.mValues = new HashMap<String, Object>(data.mValues);
    }
    
    public Data(final Map<String, ?> m) {
        this.mValues = new HashMap<String, Object>(m);
    }
    
    public static Boolean[] convertPrimitiveBooleanArray(final boolean[] array) {
        final Boolean[] array2 = new Boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Byte[] convertPrimitiveByteArray(final byte[] array) {
        final Byte[] array2 = new Byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Double[] convertPrimitiveDoubleArray(final double[] array) {
        final Double[] array2 = new Double[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Float[] convertPrimitiveFloatArray(final float[] array) {
        final Float[] array2 = new Float[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Integer[] convertPrimitiveIntArray(final int[] array) {
        final Integer[] array2 = new Integer[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Long[] convertPrimitiveLongArray(final long[] array) {
        final Long[] array2 = new Long[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static byte[] convertToPrimitiveArray(final Byte[] array) {
        final byte[] array2 = new byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static double[] convertToPrimitiveArray(final Double[] array) {
        final double[] array2 = new double[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static float[] convertToPrimitiveArray(final Float[] array) {
        final float[] array2 = new float[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static int[] convertToPrimitiveArray(final Integer[] array) {
        final int[] array2 = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static long[] convertToPrimitiveArray(final Long[] array) {
        final long[] array2 = new long[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static boolean[] convertToPrimitiveArray(final Boolean[] array) {
        final boolean[] array2 = new boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static Data fromByteArray(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: arraylength    
        //     2: sipush          10240
        //     5: if_icmpgt       228
        //     8: new             Ljava/util/HashMap;
        //    11: dup            
        //    12: invokespecial   java/util/HashMap.<init>:()V
        //    15: astore          5
        //    17: new             Ljava/io/ByteArrayInputStream;
        //    20: dup            
        //    21: aload_0        
        //    22: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //    25: astore          4
        //    27: new             Ljava/io/ObjectInputStream;
        //    30: astore_2       
        //    31: aload_2        
        //    32: aload           4
        //    34: invokespecial   java/io/ObjectInputStream.<init>:(Ljava/io/InputStream;)V
        //    37: aload_2        
        //    38: astore_0       
        //    39: aload_2        
        //    40: invokevirtual   java/io/ObjectInputStream.readInt:()I
        //    43: istore_1       
        //    44: iload_1        
        //    45: ifle            72
        //    48: aload_2        
        //    49: astore_0       
        //    50: aload           5
        //    52: aload_2        
        //    53: invokevirtual   java/io/ObjectInputStream.readUTF:()Ljava/lang/String;
        //    56: aload_2        
        //    57: invokevirtual   java/io/ObjectInputStream.readObject:()Ljava/lang/Object;
        //    60: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    65: pop            
        //    66: iinc            1, -1
        //    69: goto            44
        //    72: aload_2        
        //    73: invokevirtual   java/io/ObjectInputStream.close:()V
        //    76: goto            90
        //    79: astore_0       
        //    80: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //    83: ldc             "Error in Data#fromByteArray: "
        //    85: aload_0        
        //    86: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //    89: pop            
        //    90: aload           4
        //    92: invokevirtual   java/io/ByteArrayInputStream.close:()V
        //    95: goto            174
        //    98: astore_3       
        //    99: goto            121
        //   102: astore_3       
        //   103: goto            121
        //   106: astore_2       
        //   107: aconst_null    
        //   108: astore_0       
        //   109: goto            185
        //   112: astore_0       
        //   113: goto            117
        //   116: astore_0       
        //   117: aconst_null    
        //   118: astore_2       
        //   119: aload_0        
        //   120: astore_3       
        //   121: aload_2        
        //   122: astore_0       
        //   123: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //   126: ldc             "Error in Data#fromByteArray: "
        //   128: aload_3        
        //   129: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   132: pop            
        //   133: aload_2        
        //   134: ifnull          155
        //   137: aload_2        
        //   138: invokevirtual   java/io/ObjectInputStream.close:()V
        //   141: goto            155
        //   144: astore_0       
        //   145: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //   148: ldc             "Error in Data#fromByteArray: "
        //   150: aload_0        
        //   151: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   154: pop            
        //   155: aload           4
        //   157: invokevirtual   java/io/ByteArrayInputStream.close:()V
        //   160: goto            174
        //   163: astore_0       
        //   164: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //   167: ldc             "Error in Data#fromByteArray: "
        //   169: aload_0        
        //   170: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   173: pop            
        //   174: new             Landroidx/work/Data;
        //   177: dup            
        //   178: aload           5
        //   180: invokespecial   androidx/work/Data.<init>:(Ljava/util/Map;)V
        //   183: areturn        
        //   184: astore_2       
        //   185: aload_0        
        //   186: ifnull          207
        //   189: aload_0        
        //   190: invokevirtual   java/io/ObjectInputStream.close:()V
        //   193: goto            207
        //   196: astore_0       
        //   197: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //   200: ldc             "Error in Data#fromByteArray: "
        //   202: aload_0        
        //   203: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   206: pop            
        //   207: aload           4
        //   209: invokevirtual   java/io/ByteArrayInputStream.close:()V
        //   212: goto            226
        //   215: astore_0       
        //   216: getstatic       androidx/work/Data.TAG:Ljava/lang/String;
        //   219: ldc             "Error in Data#fromByteArray: "
        //   221: aload_0        
        //   222: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   225: pop            
        //   226: aload_2        
        //   227: athrow         
        //   228: new             Ljava/lang/IllegalStateException;
        //   231: dup            
        //   232: ldc             "Data cannot occupy more than 10240 bytes when serialized"
        //   234: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   237: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  27     37     116    117    Ljava/io/IOException;
        //  27     37     112    116    Ljava/lang/ClassNotFoundException;
        //  27     37     106    112    Any
        //  39     44     102    106    Ljava/io/IOException;
        //  39     44     98     102    Ljava/lang/ClassNotFoundException;
        //  39     44     184    185    Any
        //  50     66     102    106    Ljava/io/IOException;
        //  50     66     98     102    Ljava/lang/ClassNotFoundException;
        //  50     66     184    185    Any
        //  72     76     79     90     Ljava/io/IOException;
        //  90     95     163    174    Ljava/io/IOException;
        //  123    133    184    185    Any
        //  137    141    144    155    Ljava/io/IOException;
        //  155    160    163    174    Ljava/io/IOException;
        //  189    193    196    207    Ljava/io/IOException;
        //  207    212    215    226    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0044:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static byte[] toByteArrayInternal(final Data data) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = null;
        Object iterator;
        final Object o = iterator = null;
        ObjectOutputStream objectOutputStream2 = null;
        ObjectOutputStream objectOutputStream3;
        try {
            try {
                iterator = o;
                objectOutputStream2 = new ObjectOutputStream(out);
                try {
                    objectOutputStream2.writeInt(data.size());
                    iterator = data.mValues.entrySet().iterator();
                    while (((Iterator)iterator).hasNext()) {
                        final Map.Entry<String, Object> entry = (Map.Entry<String, Object>)((Iterator)iterator).next();
                        objectOutputStream2.writeUTF(entry.getKey());
                        objectOutputStream2.writeObject(entry.getValue());
                    }
                    try {
                        objectOutputStream2.close();
                    }
                    catch (final IOException ex) {
                        Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex);
                    }
                    try {
                        out.close();
                    }
                    catch (final IOException ex2) {
                        Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex2);
                    }
                    if (out.size() <= 10240) {
                        return out.toByteArray();
                    }
                    throw new IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
                }
                catch (final IOException iterator) {}
                finally {
                    iterator = objectOutputStream2;
                }
            }
            finally {}
        }
        catch (final IOException objectOutputStream2) {
            objectOutputStream3 = objectOutputStream;
        }
        Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)objectOutputStream2);
        final byte[] byteArray = out.toByteArray();
        if (objectOutputStream3 != null) {
            try {
                objectOutputStream3.close();
            }
            catch (final IOException ex3) {
                Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex3);
            }
        }
        try {
            out.close();
        }
        catch (final IOException ex4) {
            Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex4);
        }
        return byteArray;
        if (iterator != null) {
            try {
                ((ObjectOutputStream)iterator).close();
            }
            catch (final IOException ex5) {
                Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex5);
            }
        }
        try {
            out.close();
        }
        catch (final IOException ex6) {
            Log.e(Data.TAG, "Error in Data#toByteArray: ", (Throwable)ex6);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final Data data = (Data)o;
        final Set<String> keySet = this.mValues.keySet();
        if (!keySet.equals(data.mValues.keySet())) {
            return false;
        }
        for (final String s : keySet) {
            final Object value = this.mValues.get(s);
            final Object value2 = data.mValues.get(s);
            boolean b;
            if (value != null && value2 != null) {
                if (value instanceof Object[] && value2 instanceof Object[]) {
                    b = Arrays.deepEquals((Object[])value, (Object[])value2);
                }
                else {
                    b = value.equals(value2);
                }
            }
            else {
                b = (value == value2);
            }
            if (!b) {
                return false;
            }
        }
        return true;
    }
    
    public boolean getBoolean(final String s, final boolean b) {
        final Boolean value = this.mValues.get(s);
        if (value instanceof Boolean) {
            return value;
        }
        return b;
    }
    
    public boolean[] getBooleanArray(final String s) {
        final Boolean[] value = this.mValues.get(s);
        if (value instanceof Boolean[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public byte getByte(final String s, final byte b) {
        final Byte value = this.mValues.get(s);
        if (value instanceof Byte) {
            return value;
        }
        return b;
    }
    
    public byte[] getByteArray(final String s) {
        final Byte[] value = this.mValues.get(s);
        if (value instanceof Byte[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public double getDouble(final String s, final double n) {
        final Double value = this.mValues.get(s);
        if (value instanceof Double) {
            return value;
        }
        return n;
    }
    
    public double[] getDoubleArray(final String s) {
        final Double[] value = this.mValues.get(s);
        if (value instanceof Double[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public float getFloat(final String s, final float n) {
        final Float value = this.mValues.get(s);
        if (value instanceof Float) {
            return value;
        }
        return n;
    }
    
    public float[] getFloatArray(final String s) {
        final Float[] value = this.mValues.get(s);
        if (value instanceof Float[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public int getInt(final String s, final int n) {
        final Integer value = this.mValues.get(s);
        if (value instanceof Integer) {
            return value;
        }
        return n;
    }
    
    public int[] getIntArray(final String s) {
        final Integer[] value = this.mValues.get(s);
        if (value instanceof Integer[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public Map<String, Object> getKeyValueMap() {
        return Collections.unmodifiableMap((Map<? extends String, ?>)this.mValues);
    }
    
    public long getLong(final String s, final long n) {
        final Long value = this.mValues.get(s);
        if (value instanceof Long) {
            return value;
        }
        return n;
    }
    
    public long[] getLongArray(final String s) {
        final Long[] value = this.mValues.get(s);
        if (value instanceof Long[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public String getString(final String s) {
        final String value = this.mValues.get(s);
        if (value instanceof String) {
            return value;
        }
        return null;
    }
    
    public String[] getStringArray(final String s) {
        final String[] value = this.mValues.get(s);
        if (value instanceof String[]) {
            return value;
        }
        return null;
    }
    
    public <T> boolean hasKeyWithValueOfType(final String s, final Class<T> clazz) {
        final Object value = this.mValues.get(s);
        return value != null && clazz.isAssignableFrom(value.getClass());
    }
    
    @Override
    public int hashCode() {
        return this.mValues.hashCode() * 31;
    }
    
    public int size() {
        return this.mValues.size();
    }
    
    public byte[] toByteArray() {
        return toByteArrayInternal(this);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Data {");
        if (!this.mValues.isEmpty()) {
            for (final String str : this.mValues.keySet()) {
                sb.append(str);
                sb.append(" : ");
                final Object value = this.mValues.get(str);
                if (value instanceof Object[]) {
                    sb.append(Arrays.toString((Object[])value));
                }
                else {
                    sb.append(value);
                }
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder
    {
        private Map<String, Object> mValues;
        
        public Builder() {
            this.mValues = new HashMap<String, Object>();
        }
        
        public Data build() {
            final Data data = new Data(this.mValues);
            Data.toByteArrayInternal(data);
            return data;
        }
        
        public Builder put(final String str, final Object o) {
            if (o == null) {
                this.mValues.put(str, null);
            }
            else {
                final Class<?> class1 = o.getClass();
                if (class1 != Boolean.class && class1 != Byte.class && class1 != Integer.class && class1 != Long.class && class1 != Float.class && class1 != Double.class && class1 != String.class && class1 != Boolean[].class && class1 != Byte[].class && class1 != Integer[].class && class1 != Long[].class && class1 != Float[].class && class1 != Double[].class && class1 != String[].class) {
                    if (class1 == boolean[].class) {
                        this.mValues.put(str, Data.convertPrimitiveBooleanArray((boolean[])o));
                    }
                    else if (class1 == byte[].class) {
                        this.mValues.put(str, Data.convertPrimitiveByteArray((byte[])o));
                    }
                    else if (class1 == int[].class) {
                        this.mValues.put(str, Data.convertPrimitiveIntArray((int[])o));
                    }
                    else if (class1 == long[].class) {
                        this.mValues.put(str, Data.convertPrimitiveLongArray((long[])o));
                    }
                    else if (class1 == float[].class) {
                        this.mValues.put(str, Data.convertPrimitiveFloatArray((float[])o));
                    }
                    else {
                        if (class1 != double[].class) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Key ");
                            sb.append(str);
                            sb.append("has invalid type ");
                            sb.append(class1);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        this.mValues.put(str, Data.convertPrimitiveDoubleArray((double[])o));
                    }
                }
                else {
                    this.mValues.put(str, o);
                }
            }
            return this;
        }
        
        public Builder putAll(final Data data) {
            this.putAll(data.mValues);
            return this;
        }
        
        public Builder putAll(final Map<String, Object> map) {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                this.put(entry.getKey(), entry.getValue());
            }
            return this;
        }
        
        public Builder putBoolean(final String s, final boolean b) {
            this.mValues.put(s, b);
            return this;
        }
        
        public Builder putBooleanArray(final String s, final boolean[] array) {
            this.mValues.put(s, Data.convertPrimitiveBooleanArray(array));
            return this;
        }
        
        public Builder putByte(final String s, final byte b) {
            this.mValues.put(s, b);
            return this;
        }
        
        public Builder putByteArray(final String s, final byte[] array) {
            this.mValues.put(s, Data.convertPrimitiveByteArray(array));
            return this;
        }
        
        public Builder putDouble(final String s, final double d) {
            this.mValues.put(s, d);
            return this;
        }
        
        public Builder putDoubleArray(final String s, final double[] array) {
            this.mValues.put(s, Data.convertPrimitiveDoubleArray(array));
            return this;
        }
        
        public Builder putFloat(final String s, final float f) {
            this.mValues.put(s, f);
            return this;
        }
        
        public Builder putFloatArray(final String s, final float[] array) {
            this.mValues.put(s, Data.convertPrimitiveFloatArray(array));
            return this;
        }
        
        public Builder putInt(final String s, final int i) {
            this.mValues.put(s, i);
            return this;
        }
        
        public Builder putIntArray(final String s, final int[] array) {
            this.mValues.put(s, Data.convertPrimitiveIntArray(array));
            return this;
        }
        
        public Builder putLong(final String s, final long l) {
            this.mValues.put(s, l);
            return this;
        }
        
        public Builder putLongArray(final String s, final long[] array) {
            this.mValues.put(s, Data.convertPrimitiveLongArray(array));
            return this;
        }
        
        public Builder putString(final String s, final String s2) {
            this.mValues.put(s, s2);
            return this;
        }
        
        public Builder putStringArray(final String s, final String[] array) {
            this.mValues.put(s, array);
            return this;
        }
    }
}

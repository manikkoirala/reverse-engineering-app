// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.List;

public final class WorkQuery
{
    private final List<UUID> mIds;
    private final List<WorkInfo.State> mStates;
    private final List<String> mTags;
    private final List<String> mUniqueWorkNames;
    
    WorkQuery(final Builder builder) {
        this.mIds = builder.mIds;
        this.mUniqueWorkNames = builder.mUniqueWorkNames;
        this.mTags = builder.mTags;
        this.mStates = builder.mStates;
    }
    
    public static WorkQuery fromIds(final List<UUID> list) {
        return Builder.fromIds(list).build();
    }
    
    public static WorkQuery fromIds(final UUID... a) {
        return fromIds(Arrays.asList(a));
    }
    
    public static WorkQuery fromStates(final List<WorkInfo.State> list) {
        return Builder.fromStates(list).build();
    }
    
    public static WorkQuery fromStates(final WorkInfo.State... a) {
        return Builder.fromStates(Arrays.asList(a)).build();
    }
    
    public static WorkQuery fromTags(final List<String> list) {
        return Builder.fromTags(list).build();
    }
    
    public static WorkQuery fromTags(final String... a) {
        return fromTags(Arrays.asList(a));
    }
    
    public static WorkQuery fromUniqueWorkNames(final List<String> list) {
        return Builder.fromUniqueWorkNames(list).build();
    }
    
    public static WorkQuery fromUniqueWorkNames(final String... a) {
        return Builder.fromUniqueWorkNames(Arrays.asList(a)).build();
    }
    
    public List<UUID> getIds() {
        return this.mIds;
    }
    
    public List<WorkInfo.State> getStates() {
        return this.mStates;
    }
    
    public List<String> getTags() {
        return this.mTags;
    }
    
    public List<String> getUniqueWorkNames() {
        return this.mUniqueWorkNames;
    }
    
    public static final class Builder
    {
        List<UUID> mIds;
        List<WorkInfo.State> mStates;
        List<String> mTags;
        List<String> mUniqueWorkNames;
        
        private Builder() {
            this.mIds = new ArrayList<UUID>();
            this.mUniqueWorkNames = new ArrayList<String>();
            this.mTags = new ArrayList<String>();
            this.mStates = new ArrayList<WorkInfo.State>();
        }
        
        public static Builder fromIds(final List<UUID> list) {
            final Builder builder = new Builder();
            builder.addIds(list);
            return builder;
        }
        
        public static Builder fromStates(final List<WorkInfo.State> list) {
            final Builder builder = new Builder();
            builder.addStates(list);
            return builder;
        }
        
        public static Builder fromTags(final List<String> list) {
            final Builder builder = new Builder();
            builder.addTags(list);
            return builder;
        }
        
        public static Builder fromUniqueWorkNames(final List<String> list) {
            final Builder builder = new Builder();
            builder.addUniqueWorkNames(list);
            return builder;
        }
        
        public Builder addIds(final List<UUID> list) {
            this.mIds.addAll(list);
            return this;
        }
        
        public Builder addStates(final List<WorkInfo.State> list) {
            this.mStates.addAll(list);
            return this;
        }
        
        public Builder addTags(final List<String> list) {
            this.mTags.addAll(list);
            return this;
        }
        
        public Builder addUniqueWorkNames(final List<String> list) {
            this.mUniqueWorkNames.addAll(list);
            return this;
        }
        
        public WorkQuery build() {
            if (this.mIds.isEmpty() && this.mUniqueWorkNames.isEmpty() && this.mTags.isEmpty() && this.mStates.isEmpty()) {
                throw new IllegalArgumentException("Must specify ids, uniqueNames, tags or states when building a WorkQuery");
            }
            return new WorkQuery(this);
        }
    }
}

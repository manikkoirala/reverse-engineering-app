// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import com.google.common.util.concurrent.ListenableFuture;
import android.content.Context;
import androidx.work.impl.utils.futures.SettableFuture;

public abstract class Worker extends ListenableWorker
{
    SettableFuture<Result> mFuture;
    
    public Worker(final Context context, final WorkerParameters workerParameters) {
        super(context, workerParameters);
    }
    
    public abstract Result doWork();
    
    public ForegroundInfo getForegroundInfo() {
        throw new IllegalStateException("Expedited WorkRequests require a Worker to provide an implementation for \n `getForegroundInfo()`");
    }
    
    @Override
    public ListenableFuture<ForegroundInfo> getForegroundInfoAsync() {
        final SettableFuture<Object> create = SettableFuture.create();
        this.getBackgroundExecutor().execute(new Runnable(this, create) {
            final Worker this$0;
            final SettableFuture val$future;
            
            @Override
            public void run() {
                try {
                    this.val$future.set(this.this$0.getForegroundInfo());
                }
                finally {
                    final Throwable exception;
                    this.val$future.setException(exception);
                }
            }
        });
        return (ListenableFuture<ForegroundInfo>)create;
    }
    
    @Override
    public final ListenableFuture<Result> startWork() {
        this.mFuture = SettableFuture.create();
        this.getBackgroundExecutor().execute(new Runnable(this) {
            final Worker this$0;
            
            @Override
            public void run() {
                try {
                    this.this$0.mFuture.set(this.this$0.doWork());
                }
                finally {
                    final Throwable exception;
                    this.this$0.mFuture.setException(exception);
                }
            }
        });
        return (ListenableFuture<Result>)this.mFuture;
    }
}

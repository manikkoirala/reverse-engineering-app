// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Objects;
import android.os.Build$VERSION;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import androidx.work.impl.DefaultRunnableScheduler;
import java.util.concurrent.Executor;
import androidx.core.util.Consumer;

public final class Configuration
{
    public static final int MIN_SCHEDULER_LIMIT = 20;
    final String mDefaultProcessName;
    final Consumer<Throwable> mExceptionHandler;
    final Executor mExecutor;
    final InputMergerFactory mInputMergerFactory;
    private final boolean mIsUsingDefaultTaskExecutor;
    final int mLoggingLevel;
    final int mMaxJobSchedulerId;
    final int mMaxSchedulerLimit;
    final int mMinJobSchedulerId;
    final RunnableScheduler mRunnableScheduler;
    final Consumer<Throwable> mSchedulingExceptionHandler;
    final Executor mTaskExecutor;
    final WorkerFactory mWorkerFactory;
    
    Configuration(final Builder builder) {
        if (builder.mExecutor == null) {
            this.mExecutor = this.createDefaultExecutor(false);
        }
        else {
            this.mExecutor = builder.mExecutor;
        }
        if (builder.mTaskExecutor == null) {
            this.mIsUsingDefaultTaskExecutor = true;
            this.mTaskExecutor = this.createDefaultExecutor(true);
        }
        else {
            this.mIsUsingDefaultTaskExecutor = false;
            this.mTaskExecutor = builder.mTaskExecutor;
        }
        if (builder.mWorkerFactory == null) {
            this.mWorkerFactory = WorkerFactory.getDefaultWorkerFactory();
        }
        else {
            this.mWorkerFactory = builder.mWorkerFactory;
        }
        if (builder.mInputMergerFactory == null) {
            this.mInputMergerFactory = InputMergerFactory.getDefaultInputMergerFactory();
        }
        else {
            this.mInputMergerFactory = builder.mInputMergerFactory;
        }
        if (builder.mRunnableScheduler == null) {
            this.mRunnableScheduler = new DefaultRunnableScheduler();
        }
        else {
            this.mRunnableScheduler = builder.mRunnableScheduler;
        }
        this.mLoggingLevel = builder.mLoggingLevel;
        this.mMinJobSchedulerId = builder.mMinJobSchedulerId;
        this.mMaxJobSchedulerId = builder.mMaxJobSchedulerId;
        this.mMaxSchedulerLimit = builder.mMaxSchedulerLimit;
        this.mExceptionHandler = builder.mExceptionHandler;
        this.mSchedulingExceptionHandler = builder.mSchedulingExceptionHandler;
        this.mDefaultProcessName = builder.mDefaultProcessName;
    }
    
    private Executor createDefaultExecutor(final boolean b) {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), this.createDefaultThreadFactory(b));
    }
    
    private ThreadFactory createDefaultThreadFactory(final boolean b) {
        return new ThreadFactory(this, b) {
            private final AtomicInteger mThreadCount = new AtomicInteger(0);
            final Configuration this$0;
            final boolean val$isTaskExecutor;
            
            @Override
            public Thread newThread(final Runnable target) {
                String str;
                if (this.val$isTaskExecutor) {
                    str = "WM.task-";
                }
                else {
                    str = "androidx.work-";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(this.mThreadCount.incrementAndGet());
                return new Thread(target, sb.toString());
            }
        };
    }
    
    public String getDefaultProcessName() {
        return this.mDefaultProcessName;
    }
    
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    public Consumer<Throwable> getInitializationExceptionHandler() {
        return this.mExceptionHandler;
    }
    
    public InputMergerFactory getInputMergerFactory() {
        return this.mInputMergerFactory;
    }
    
    public int getMaxJobSchedulerId() {
        return this.mMaxJobSchedulerId;
    }
    
    public int getMaxSchedulerLimit() {
        if (Build$VERSION.SDK_INT == 23) {
            return this.mMaxSchedulerLimit / 2;
        }
        return this.mMaxSchedulerLimit;
    }
    
    public int getMinJobSchedulerId() {
        return this.mMinJobSchedulerId;
    }
    
    public int getMinimumLoggingLevel() {
        return this.mLoggingLevel;
    }
    
    public RunnableScheduler getRunnableScheduler() {
        return this.mRunnableScheduler;
    }
    
    public Consumer<Throwable> getSchedulingExceptionHandler() {
        return this.mSchedulingExceptionHandler;
    }
    
    public Executor getTaskExecutor() {
        return this.mTaskExecutor;
    }
    
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerFactory;
    }
    
    public boolean isUsingDefaultTaskExecutor() {
        return this.mIsUsingDefaultTaskExecutor;
    }
    
    public static final class Builder
    {
        String mDefaultProcessName;
        Consumer<Throwable> mExceptionHandler;
        Executor mExecutor;
        InputMergerFactory mInputMergerFactory;
        int mLoggingLevel;
        int mMaxJobSchedulerId;
        int mMaxSchedulerLimit;
        int mMinJobSchedulerId;
        RunnableScheduler mRunnableScheduler;
        Consumer<Throwable> mSchedulingExceptionHandler;
        Executor mTaskExecutor;
        WorkerFactory mWorkerFactory;
        
        public Builder() {
            this.mLoggingLevel = 4;
            this.mMinJobSchedulerId = 0;
            this.mMaxJobSchedulerId = Integer.MAX_VALUE;
            this.mMaxSchedulerLimit = 20;
        }
        
        public Builder(final Configuration configuration) {
            this.mExecutor = configuration.mExecutor;
            this.mWorkerFactory = configuration.mWorkerFactory;
            this.mInputMergerFactory = configuration.mInputMergerFactory;
            this.mTaskExecutor = configuration.mTaskExecutor;
            this.mLoggingLevel = configuration.mLoggingLevel;
            this.mMinJobSchedulerId = configuration.mMinJobSchedulerId;
            this.mMaxJobSchedulerId = configuration.mMaxJobSchedulerId;
            this.mMaxSchedulerLimit = configuration.mMaxSchedulerLimit;
            this.mRunnableScheduler = configuration.mRunnableScheduler;
            this.mExceptionHandler = configuration.mExceptionHandler;
            this.mSchedulingExceptionHandler = configuration.mSchedulingExceptionHandler;
            this.mDefaultProcessName = configuration.mDefaultProcessName;
        }
        
        public Configuration build() {
            return new Configuration(this);
        }
        
        public Builder setDefaultProcessName(final String mDefaultProcessName) {
            this.mDefaultProcessName = mDefaultProcessName;
            return this;
        }
        
        public Builder setExecutor(final Executor mExecutor) {
            this.mExecutor = mExecutor;
            return this;
        }
        
        public Builder setInitializationExceptionHandler(final Consumer<Throwable> mExceptionHandler) {
            this.mExceptionHandler = mExceptionHandler;
            return this;
        }
        
        public Builder setInitializationExceptionHandler(final InitializationExceptionHandler obj) {
            Objects.requireNonNull(obj);
            this.mExceptionHandler = new Configuration$Builder$$ExternalSyntheticLambda0(obj);
            return this;
        }
        
        public Builder setInputMergerFactory(final InputMergerFactory mInputMergerFactory) {
            this.mInputMergerFactory = mInputMergerFactory;
            return this;
        }
        
        public Builder setJobSchedulerJobIdRange(final int mMinJobSchedulerId, final int mMaxJobSchedulerId) {
            if (mMaxJobSchedulerId - mMinJobSchedulerId >= 1000) {
                this.mMinJobSchedulerId = mMinJobSchedulerId;
                this.mMaxJobSchedulerId = mMaxJobSchedulerId;
                return this;
            }
            throw new IllegalArgumentException("WorkManager needs a range of at least 1000 job ids.");
        }
        
        public Builder setMaxSchedulerLimit(final int a) {
            if (a >= 20) {
                this.mMaxSchedulerLimit = Math.min(a, 50);
                return this;
            }
            throw new IllegalArgumentException("WorkManager needs to be able to schedule at least 20 jobs in JobScheduler.");
        }
        
        public Builder setMinimumLoggingLevel(final int mLoggingLevel) {
            this.mLoggingLevel = mLoggingLevel;
            return this;
        }
        
        public Builder setRunnableScheduler(final RunnableScheduler mRunnableScheduler) {
            this.mRunnableScheduler = mRunnableScheduler;
            return this;
        }
        
        public Builder setSchedulingExceptionHandler(final Consumer<Throwable> mSchedulingExceptionHandler) {
            this.mSchedulingExceptionHandler = mSchedulingExceptionHandler;
            return this;
        }
        
        public Builder setTaskExecutor(final Executor mTaskExecutor) {
            this.mTaskExecutor = mTaskExecutor;
            return this;
        }
        
        public Builder setWorkerFactory(final WorkerFactory mWorkerFactory) {
            this.mWorkerFactory = mWorkerFactory;
            return this;
        }
    }
    
    public interface Provider
    {
        Configuration getWorkManagerConfiguration();
    }
}

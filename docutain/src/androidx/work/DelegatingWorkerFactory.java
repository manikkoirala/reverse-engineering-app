// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Iterator;
import android.content.Context;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;

public class DelegatingWorkerFactory extends WorkerFactory
{
    private static final String TAG;
    private final List<WorkerFactory> mFactories;
    
    static {
        TAG = Logger.tagWithPrefix("DelegatingWkrFctry");
    }
    
    public DelegatingWorkerFactory() {
        this.mFactories = new CopyOnWriteArrayList<WorkerFactory>();
    }
    
    public final void addFactory(final WorkerFactory workerFactory) {
        this.mFactories.add(workerFactory);
    }
    
    @Override
    public final ListenableWorker createWorker(final Context context, String string, WorkerParameters workerParameters) {
        for (final WorkerFactory workerFactory : this.mFactories) {
            try {
                final ListenableWorker worker = workerFactory.createWorker(context, string, workerParameters);
                if (worker != null) {
                    return worker;
                }
                continue;
            }
            finally {
                workerParameters = (WorkerParameters)new StringBuilder();
                ((StringBuilder)workerParameters).append("Unable to instantiate a ListenableWorker (");
                ((StringBuilder)workerParameters).append(string);
                ((StringBuilder)workerParameters).append(")");
                string = ((StringBuilder)workerParameters).toString();
                Logger.get().error(DelegatingWorkerFactory.TAG, string, (Throwable)context);
            }
            break;
        }
        return null;
    }
    
    List<WorkerFactory> getFactories() {
        return this.mFactories;
    }
}

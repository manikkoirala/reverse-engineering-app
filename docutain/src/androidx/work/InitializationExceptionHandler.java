// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public interface InitializationExceptionHandler
{
    void handleException(final Throwable p0);
}

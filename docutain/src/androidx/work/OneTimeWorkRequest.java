// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Iterator;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import android.os.Build$VERSION;
import java.util.List;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00062\u00020\u0001:\u0002\u0005\u0006B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0007" }, d2 = { "Landroidx/work/OneTimeWorkRequest;", "Landroidx/work/WorkRequest;", "builder", "Landroidx/work/OneTimeWorkRequest$Builder;", "(Landroidx/work/OneTimeWorkRequest$Builder;)V", "Builder", "Companion", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class OneTimeWorkRequest extends WorkRequest
{
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public OneTimeWorkRequest(final Builder builder) {
        Intrinsics.checkNotNullParameter((Object)builder, "builder");
        super(((WorkRequest.Builder)builder).getId$work_runtime_release(), ((WorkRequest.Builder)builder).getWorkSpec$work_runtime_release(), ((WorkRequest.Builder)builder).getTags$work_runtime_release());
    }
    
    @JvmStatic
    public static final OneTimeWorkRequest from(final Class<? extends ListenableWorker> clazz) {
        return OneTimeWorkRequest.Companion.from(clazz);
    }
    
    @JvmStatic
    public static final List<OneTimeWorkRequest> from(final List<? extends Class<? extends ListenableWorker>> list) {
        return OneTimeWorkRequest.Companion.from(list);
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u000e\u0010\u0003\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006J\r\u0010\n\u001a\u00020\u0002H\u0010¢\u0006\u0002\b\u000bJ\u0016\u0010\f\u001a\u00020\u00002\u000e\u0010\r\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000e0\u0004R\u0014\u0010\u0007\u001a\u00020\u00008PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\u000f" }, d2 = { "Landroidx/work/OneTimeWorkRequest$Builder;", "Landroidx/work/WorkRequest$Builder;", "Landroidx/work/OneTimeWorkRequest;", "workerClass", "Ljava/lang/Class;", "Landroidx/work/ListenableWorker;", "(Ljava/lang/Class;)V", "thisObject", "getThisObject$work_runtime_release", "()Landroidx/work/OneTimeWorkRequest$Builder;", "buildInternal", "buildInternal$work_runtime_release", "setInputMerger", "inputMerger", "Landroidx/work/InputMerger;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Builder extends WorkRequest.Builder<Builder, OneTimeWorkRequest>
    {
        public Builder(final Class<? extends ListenableWorker> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "workerClass");
            super(clazz);
            ((WorkRequest.Builder)this).getWorkSpec$work_runtime_release().inputMergerClassName = OverwritingInputMerger.class.getName();
        }
        
        public OneTimeWorkRequest buildInternal$work_runtime_release() {
            if (!((WorkRequest.Builder)this).getBackoffCriteriaSet$work_runtime_release() || Build$VERSION.SDK_INT < 23 || !((WorkRequest.Builder)this).getWorkSpec$work_runtime_release().constraints.requiresDeviceIdle()) {
                return new OneTimeWorkRequest(this);
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job".toString());
        }
        
        public Builder getThisObject$work_runtime_release() {
            return this;
        }
        
        public final Builder setInputMerger(final Class<? extends InputMerger> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "inputMerger");
            ((WorkRequest.Builder)this).getWorkSpec$work_runtime_release().inputMergerClassName = clazz.getName();
            return this;
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u000e\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00070\u0006H\u0007J$\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00040\b2\u0014\u0010\t\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00070\u00060\bH\u0007¨\u0006\n" }, d2 = { "Landroidx/work/OneTimeWorkRequest$Companion;", "", "()V", "from", "Landroidx/work/OneTimeWorkRequest;", "workerClass", "Ljava/lang/Class;", "Landroidx/work/ListenableWorker;", "", "workerClasses", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final OneTimeWorkRequest from(final Class<? extends ListenableWorker> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "workerClass");
            return ((WorkRequest.Builder<B, OneTimeWorkRequest>)new Builder(clazz)).build();
        }
        
        @JvmStatic
        public final List<OneTimeWorkRequest> from(final List<? extends Class<? extends ListenableWorker>> list) {
            Intrinsics.checkNotNullParameter((Object)list, "workerClasses");
            final Iterable iterable = list;
            final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                collection.add(new Builder((Class<? extends ListenableWorker>)iterator.next()).build());
            }
            return (List<OneTimeWorkRequest>)collection;
        }
    }
}

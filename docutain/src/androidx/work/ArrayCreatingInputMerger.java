// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Array;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00042\n\u0010\u0007\u001a\u0006\u0012\u0002\b\u00030\bH\u0002J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u0004H\u0002J\u001e\u0010\f\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00042\n\u0010\u0007\u001a\u0006\u0012\u0002\b\u00030\bH\u0002J\u0016\u0010\r\u001a\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0010H\u0016¨\u0006\u0011" }, d2 = { "Landroidx/work/ArrayCreatingInputMerger;", "Landroidx/work/InputMerger;", "()V", "concatenateArrayAndNonArray", "", "array", "obj", "valueClass", "Ljava/lang/Class;", "concatenateArrays", "array1", "array2", "createArrayFor", "merge", "Landroidx/work/Data;", "inputs", "", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class ArrayCreatingInputMerger extends InputMerger
{
    private final Object concatenateArrayAndNonArray(final Object o, final Object o2, final Class<?> componentType) {
        final int length = Array.getLength(o);
        final Object instance = Array.newInstance(componentType, length + 1);
        System.arraycopy(o, 0, instance, 0, length);
        Array.set(instance, length, o2);
        Intrinsics.checkNotNullExpressionValue(instance, "newArray");
        return instance;
    }
    
    private final Object concatenateArrays(final Object o, final Object o2) {
        final int length = Array.getLength(o);
        final int length2 = Array.getLength(o2);
        final Class<?> componentType = o.getClass().getComponentType();
        Intrinsics.checkNotNull((Object)componentType);
        final Object instance = Array.newInstance(componentType, length + length2);
        System.arraycopy(o, 0, instance, 0, length);
        System.arraycopy(o2, 0, instance, length, length2);
        Intrinsics.checkNotNullExpressionValue(instance, "newArray");
        return instance;
    }
    
    private final Object createArrayFor(final Object o, final Class<?> componentType) {
        final Object instance = Array.newInstance(componentType, 1);
        Array.set(instance, 0, o);
        Intrinsics.checkNotNullExpressionValue(instance, "newArray");
        return instance;
    }
    
    @Override
    public Data merge(final List<Data> list) {
        Intrinsics.checkNotNullParameter((Object)list, "inputs");
        final Data.Builder builder = new Data.Builder();
        final Map map = new HashMap();
        final Iterator<Data> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Map<String, Object> keyValueMap = iterator.next().getKeyValueMap();
            Intrinsics.checkNotNullExpressionValue((Object)keyValueMap, "input.keyValueMap");
            for (final Map.Entry<String, V> entry : keyValueMap.entrySet()) {
                final String s = entry.getKey();
                final V value = entry.getValue();
                Class<?> class1;
                if (value == null || (class1 = value.getClass()) == null) {
                    class1 = String.class;
                }
                final Object value2 = map.get(s);
                Intrinsics.checkNotNullExpressionValue((Object)s, "key");
                Object o;
                if (value2 == null) {
                    if (class1.isArray()) {
                        o = value;
                    }
                    else {
                        o = this.createArrayFor(value, class1);
                    }
                }
                else {
                    final Class<?> class2 = value2.getClass();
                    if (Intrinsics.areEqual((Object)class2, (Object)class1)) {
                        Intrinsics.checkNotNullExpressionValue((Object)value, "value");
                        o = this.concatenateArrays(value2, value);
                    }
                    else {
                        if (!Intrinsics.areEqual((Object)class2.getComponentType(), (Object)class1)) {
                            throw new IllegalArgumentException();
                        }
                        o = this.concatenateArrayAndNonArray(value2, value, class1);
                    }
                }
                Intrinsics.checkNotNullExpressionValue(o, "if (existingValue == nul\u2026      }\n                }");
                map.put(s, o);
            }
        }
        builder.putAll(map);
        final Data build = builder.build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "output.build()");
        return build;
    }
}

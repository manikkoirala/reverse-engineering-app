// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;
import android.app.PendingIntent;
import java.util.UUID;
import java.util.List;
import java.util.Collections;
import android.content.Context;
import androidx.work.impl.WorkManagerImpl;

public abstract class WorkManager
{
    protected WorkManager() {
    }
    
    @Deprecated
    public static WorkManager getInstance() {
        final WorkManagerImpl instance = WorkManagerImpl.getInstance();
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }
    
    public static WorkManager getInstance(final Context context) {
        return WorkManagerImpl.getInstance(context);
    }
    
    public static void initialize(final Context context, final Configuration configuration) {
        WorkManagerImpl.initialize(context, configuration);
    }
    
    public static boolean isInitialized() {
        return WorkManagerImpl.isInitialized();
    }
    
    public final WorkContinuation beginUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final OneTimeWorkRequest o) {
        return this.beginUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    public abstract WorkContinuation beginUniqueWork(final String p0, final ExistingWorkPolicy p1, final List<OneTimeWorkRequest> p2);
    
    public final WorkContinuation beginWith(final OneTimeWorkRequest o) {
        return this.beginWith(Collections.singletonList(o));
    }
    
    public abstract WorkContinuation beginWith(final List<OneTimeWorkRequest> p0);
    
    public abstract Operation cancelAllWork();
    
    public abstract Operation cancelAllWorkByTag(final String p0);
    
    public abstract Operation cancelUniqueWork(final String p0);
    
    public abstract Operation cancelWorkById(final UUID p0);
    
    public abstract PendingIntent createCancelPendingIntent(final UUID p0);
    
    public final Operation enqueue(final WorkRequest o) {
        return this.enqueue(Collections.singletonList(o));
    }
    
    public abstract Operation enqueue(final List<? extends WorkRequest> p0);
    
    public abstract Operation enqueueUniquePeriodicWork(final String p0, final ExistingPeriodicWorkPolicy p1, final PeriodicWorkRequest p2);
    
    public Operation enqueueUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final OneTimeWorkRequest o) {
        return this.enqueueUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    public abstract Operation enqueueUniqueWork(final String p0, final ExistingWorkPolicy p1, final List<OneTimeWorkRequest> p2);
    
    public abstract Configuration getConfiguration();
    
    public abstract ListenableFuture<Long> getLastCancelAllTimeMillis();
    
    public abstract LiveData<Long> getLastCancelAllTimeMillisLiveData();
    
    public abstract ListenableFuture<WorkInfo> getWorkInfoById(final UUID p0);
    
    public abstract LiveData<WorkInfo> getWorkInfoByIdLiveData(final UUID p0);
    
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos(final WorkQuery p0);
    
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfosByTag(final String p0);
    
    public abstract LiveData<List<WorkInfo>> getWorkInfosByTagLiveData(final String p0);
    
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfosForUniqueWork(final String p0);
    
    public abstract LiveData<List<WorkInfo>> getWorkInfosForUniqueWorkLiveData(final String p0);
    
    public abstract LiveData<List<WorkInfo>> getWorkInfosLiveData(final WorkQuery p0);
    
    public abstract Operation pruneWork();
    
    public abstract ListenableFuture<UpdateResult> updateWork(final WorkRequest p0);
    
    public enum UpdateResult
    {
        private static final UpdateResult[] $VALUES;
        
        APPLIED_FOR_NEXT_RUN, 
        APPLIED_IMMEDIATELY, 
        NOT_APPLIED;
        
        private static /* synthetic */ UpdateResult[] $values() {
            return new UpdateResult[] { UpdateResult.NOT_APPLIED, UpdateResult.APPLIED_IMMEDIATELY, UpdateResult.APPLIED_FOR_NEXT_RUN };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

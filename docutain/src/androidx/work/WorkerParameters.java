// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import android.net.Uri;
import java.util.List;
import android.net.Network;
import java.util.HashSet;
import java.util.Collection;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;

public final class WorkerParameters
{
    private Executor mBackgroundExecutor;
    private ForegroundUpdater mForegroundUpdater;
    private int mGeneration;
    private UUID mId;
    private Data mInputData;
    private ProgressUpdater mProgressUpdater;
    private int mRunAttemptCount;
    private RuntimeExtras mRuntimeExtras;
    private Set<String> mTags;
    private TaskExecutor mWorkTaskExecutor;
    private WorkerFactory mWorkerFactory;
    
    public WorkerParameters(final UUID mId, final Data mInputData, final Collection<String> c, final RuntimeExtras mRuntimeExtras, final int mRunAttemptCount, final int mGeneration, final Executor mBackgroundExecutor, final TaskExecutor mWorkTaskExecutor, final WorkerFactory mWorkerFactory, final ProgressUpdater mProgressUpdater, final ForegroundUpdater mForegroundUpdater) {
        this.mId = mId;
        this.mInputData = mInputData;
        this.mTags = new HashSet<String>(c);
        this.mRuntimeExtras = mRuntimeExtras;
        this.mRunAttemptCount = mRunAttemptCount;
        this.mGeneration = mGeneration;
        this.mBackgroundExecutor = mBackgroundExecutor;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkerFactory = mWorkerFactory;
        this.mProgressUpdater = mProgressUpdater;
        this.mForegroundUpdater = mForegroundUpdater;
    }
    
    public Executor getBackgroundExecutor() {
        return this.mBackgroundExecutor;
    }
    
    public ForegroundUpdater getForegroundUpdater() {
        return this.mForegroundUpdater;
    }
    
    public int getGeneration() {
        return this.mGeneration;
    }
    
    public UUID getId() {
        return this.mId;
    }
    
    public Data getInputData() {
        return this.mInputData;
    }
    
    public Network getNetwork() {
        return this.mRuntimeExtras.network;
    }
    
    public ProgressUpdater getProgressUpdater() {
        return this.mProgressUpdater;
    }
    
    public int getRunAttemptCount() {
        return this.mRunAttemptCount;
    }
    
    public RuntimeExtras getRuntimeExtras() {
        return this.mRuntimeExtras;
    }
    
    public Set<String> getTags() {
        return this.mTags;
    }
    
    public TaskExecutor getTaskExecutor() {
        return this.mWorkTaskExecutor;
    }
    
    public List<String> getTriggeredContentAuthorities() {
        return this.mRuntimeExtras.triggeredContentAuthorities;
    }
    
    public List<Uri> getTriggeredContentUris() {
        return this.mRuntimeExtras.triggeredContentUris;
    }
    
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerFactory;
    }
    
    public static class RuntimeExtras
    {
        public Network network;
        public List<String> triggeredContentAuthorities;
        public List<Uri> triggeredContentUris;
        
        public RuntimeExtras() {
            this.triggeredContentAuthorities = Collections.emptyList();
            this.triggeredContentUris = Collections.emptyList();
        }
    }
}

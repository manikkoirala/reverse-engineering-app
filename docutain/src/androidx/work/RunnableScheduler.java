// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public interface RunnableScheduler
{
    void cancel(final Runnable p0);
    
    void scheduleWithDelay(final long p0, final Runnable p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.Set;
import androidx.work.impl.model.WorkSpec;
import java.util.UUID;
import androidx.work.WorkRequest;

public class WorkRequestHolder extends WorkRequest
{
    public WorkRequestHolder(final UUID uuid, final WorkSpec workSpec, final Set<String> set) {
        super(uuid, workSpec, set);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.utils.StopWorkRunnable;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.impl.utils.StartWorkRunnable;
import androidx.work.WorkerParameters;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import androidx.work.impl.utils.PruneWorkRunnable;
import androidx.work.impl.utils.RawQueries;
import androidx.work.WorkQuery;
import androidx.work.impl.utils.LiveDataUtils;
import androidx.work.impl.model.WorkSpec;
import androidx.arch.core.util.Function;
import androidx.work.impl.utils.StatusRunnable;
import androidx.work.WorkInfo;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collections;
import androidx.work.PeriodicWorkRequest;
import androidx.work.ExistingPeriodicWorkPolicy;
import java.util.Arrays;
import androidx.work.impl.background.greedy.GreedyScheduler;
import android.content.Intent;
import androidx.work.impl.foreground.SystemForegroundDispatcher;
import android.app.PendingIntent;
import java.util.UUID;
import androidx.work.impl.utils.CancelWorkRunnable;
import androidx.work.Operation;
import androidx.work.WorkRequest;
import androidx.work.WorkContinuation;
import androidx.work.OneTimeWorkRequest;
import androidx.work.ExistingWorkPolicy;
import androidx.work.impl.utils.ForceStopRunnable;
import android.os.Build$VERSION;
import androidx.work.impl.utils.taskexecutor.WorkManagerTaskExecutor;
import java.util.concurrent.Executor;
import androidx.work.R;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.constraints.trackers.Trackers;
import java.util.List;
import android.content.BroadcastReceiver$PendingResult;
import androidx.work.multiprocess.RemoteWorkManager;
import androidx.work.impl.utils.PreferenceUtils;
import android.content.Context;
import androidx.work.Configuration;
import androidx.work.WorkManager;

public class WorkManagerImpl extends WorkManager
{
    public static final int MAX_PRE_JOB_SCHEDULER_API_LEVEL = 22;
    public static final int MIN_JOB_SCHEDULER_API_LEVEL = 23;
    public static final String REMOTE_WORK_MANAGER_CLIENT = "androidx.work.multiprocess.RemoteWorkManagerClient";
    private static final String TAG;
    private static WorkManagerImpl sDefaultInstance;
    private static WorkManagerImpl sDelegatedInstance;
    private static final Object sLock;
    private Configuration mConfiguration;
    private Context mContext;
    private boolean mForceStopRunnableCompleted;
    private PreferenceUtils mPreferenceUtils;
    private Processor mProcessor;
    private volatile RemoteWorkManager mRemoteWorkManager;
    private BroadcastReceiver$PendingResult mRescheduleReceiverResult;
    private List<Scheduler> mSchedulers;
    private final Trackers mTrackers;
    private WorkDatabase mWorkDatabase;
    private TaskExecutor mWorkTaskExecutor;
    
    static {
        TAG = Logger.tagWithPrefix("WorkManagerImpl");
        WorkManagerImpl.sDelegatedInstance = null;
        WorkManagerImpl.sDefaultInstance = null;
        sLock = new Object();
    }
    
    public WorkManagerImpl(final Context context, final Configuration configuration, final TaskExecutor taskExecutor) {
        this(context, configuration, taskExecutor, context.getResources().getBoolean(R.bool.workmanager_test_configuration));
    }
    
    public WorkManagerImpl(final Context context, final Configuration configuration, final TaskExecutor taskExecutor, final WorkDatabase workDatabase) {
        final Context applicationContext = context.getApplicationContext();
        Logger.setLogger(new Logger.LogcatLogger(configuration.getMinimumLoggingLevel()));
        final Trackers mTrackers = new Trackers(applicationContext, taskExecutor);
        this.mTrackers = mTrackers;
        final List<Scheduler> schedulers = this.createSchedulers(applicationContext, configuration, mTrackers);
        this.internalInit(context, configuration, taskExecutor, workDatabase, schedulers, new Processor(context, configuration, taskExecutor, workDatabase, schedulers));
    }
    
    public WorkManagerImpl(final Context context, final Configuration configuration, final TaskExecutor taskExecutor, final WorkDatabase workDatabase, final List<Scheduler> list, final Processor processor) {
        this(context, configuration, taskExecutor, workDatabase, list, processor, new Trackers(context.getApplicationContext(), taskExecutor));
    }
    
    public WorkManagerImpl(final Context context, final Configuration configuration, final TaskExecutor taskExecutor, final WorkDatabase workDatabase, final List<Scheduler> list, final Processor processor, final Trackers mTrackers) {
        this.mTrackers = mTrackers;
        this.internalInit(context, configuration, taskExecutor, workDatabase, list, processor);
    }
    
    public WorkManagerImpl(final Context context, final Configuration configuration, final TaskExecutor taskExecutor, final boolean b) {
        this(context, configuration, taskExecutor, WorkDatabase.create(context.getApplicationContext(), taskExecutor.getSerialTaskExecutor(), b));
    }
    
    @Deprecated
    public static WorkManagerImpl getInstance() {
        synchronized (WorkManagerImpl.sLock) {
            final WorkManagerImpl sDelegatedInstance = WorkManagerImpl.sDelegatedInstance;
            if (sDelegatedInstance != null) {
                return sDelegatedInstance;
            }
            return WorkManagerImpl.sDefaultInstance;
        }
    }
    
    public static WorkManagerImpl getInstance(Context applicationContext) {
        synchronized (WorkManagerImpl.sLock) {
            WorkManagerImpl workManagerImpl;
            if ((workManagerImpl = getInstance()) == null) {
                applicationContext = applicationContext.getApplicationContext();
                if (!(applicationContext instanceof Configuration.Provider)) {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
                initialize(applicationContext, ((Configuration.Provider)applicationContext).getWorkManagerConfiguration());
                workManagerImpl = getInstance(applicationContext);
            }
            return workManagerImpl;
        }
    }
    
    public static void initialize(Context applicationContext, final Configuration configuration) {
        synchronized (WorkManagerImpl.sLock) {
            final WorkManagerImpl sDelegatedInstance = WorkManagerImpl.sDelegatedInstance;
            if (sDelegatedInstance != null && WorkManagerImpl.sDefaultInstance != null) {
                throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
            }
            if (sDelegatedInstance == null) {
                applicationContext = applicationContext.getApplicationContext();
                if (WorkManagerImpl.sDefaultInstance == null) {
                    WorkManagerImpl.sDefaultInstance = new WorkManagerImpl(applicationContext, configuration, new WorkManagerTaskExecutor(configuration.getTaskExecutor()));
                }
                WorkManagerImpl.sDelegatedInstance = WorkManagerImpl.sDefaultInstance;
            }
        }
    }
    
    private void internalInit(Context applicationContext, final Configuration mConfiguration, final TaskExecutor mWorkTaskExecutor, final WorkDatabase mWorkDatabase, final List<Scheduler> mSchedulers, final Processor mProcessor) {
        applicationContext = applicationContext.getApplicationContext();
        this.mContext = applicationContext;
        this.mConfiguration = mConfiguration;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkDatabase = mWorkDatabase;
        this.mSchedulers = mSchedulers;
        this.mProcessor = mProcessor;
        this.mPreferenceUtils = new PreferenceUtils(mWorkDatabase);
        this.mForceStopRunnableCompleted = false;
        if (Build$VERSION.SDK_INT >= 24 && Api24Impl.isDeviceProtectedStorage(applicationContext)) {
            throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
        }
        this.mWorkTaskExecutor.executeOnTaskThread(new ForceStopRunnable(applicationContext, this));
    }
    
    public static boolean isInitialized() {
        return getInstance() != null;
    }
    
    public static void setDelegate(final WorkManagerImpl sDelegatedInstance) {
        synchronized (WorkManagerImpl.sLock) {
            WorkManagerImpl.sDelegatedInstance = sDelegatedInstance;
        }
    }
    
    private void tryInitializeMultiProcessSupport() {
        try {
            this.mRemoteWorkManager = (RemoteWorkManager)Class.forName("androidx.work.multiprocess.RemoteWorkManagerClient").getConstructor(Context.class, WorkManagerImpl.class).newInstance(this.mContext, this);
        }
        finally {
            final Throwable t;
            Logger.get().debug(WorkManagerImpl.TAG, "Unable to initialize multi-process support", t);
        }
    }
    
    @Override
    public WorkContinuation beginUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final List<OneTimeWorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, s, existingWorkPolicy, list);
        }
        throw new IllegalArgumentException("beginUniqueWork needs at least one OneTimeWorkRequest.");
    }
    
    @Override
    public WorkContinuation beginWith(final List<OneTimeWorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, list);
        }
        throw new IllegalArgumentException("beginWith needs at least one OneTimeWorkRequest.");
    }
    
    @Override
    public Operation cancelAllWork() {
        final CancelWorkRunnable forAll = CancelWorkRunnable.forAll(this);
        this.mWorkTaskExecutor.executeOnTaskThread(forAll);
        return forAll.getOperation();
    }
    
    @Override
    public Operation cancelAllWorkByTag(final String s) {
        final CancelWorkRunnable forTag = CancelWorkRunnable.forTag(s, this);
        this.mWorkTaskExecutor.executeOnTaskThread(forTag);
        return forTag.getOperation();
    }
    
    @Override
    public Operation cancelUniqueWork(final String s) {
        final CancelWorkRunnable forName = CancelWorkRunnable.forName(s, this, true);
        this.mWorkTaskExecutor.executeOnTaskThread(forName);
        return forName.getOperation();
    }
    
    @Override
    public Operation cancelWorkById(final UUID uuid) {
        final CancelWorkRunnable forId = CancelWorkRunnable.forId(uuid, this);
        this.mWorkTaskExecutor.executeOnTaskThread(forId);
        return forId.getOperation();
    }
    
    @Override
    public PendingIntent createCancelPendingIntent(final UUID uuid) {
        final Intent cancelWorkIntent = SystemForegroundDispatcher.createCancelWorkIntent(this.mContext, uuid.toString());
        int n;
        if (Build$VERSION.SDK_INT >= 31) {
            n = 167772160;
        }
        else {
            n = 134217728;
        }
        return PendingIntent.getService(this.mContext, 0, cancelWorkIntent, n);
    }
    
    public List<Scheduler> createSchedulers(final Context context, final Configuration configuration, final Trackers trackers) {
        return Arrays.asList(Schedulers.createBestAvailableBackgroundScheduler(context, this), new GreedyScheduler(context, configuration, trackers, this));
    }
    
    public WorkContinuationImpl createWorkContinuationForUniquePeriodicWork(final String s, final ExistingPeriodicWorkPolicy existingPeriodicWorkPolicy, final PeriodicWorkRequest o) {
        ExistingWorkPolicy existingWorkPolicy;
        if (existingPeriodicWorkPolicy == ExistingPeriodicWorkPolicy.KEEP) {
            existingWorkPolicy = ExistingWorkPolicy.KEEP;
        }
        else {
            existingWorkPolicy = ExistingWorkPolicy.REPLACE;
        }
        return new WorkContinuationImpl(this, s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @Override
    public Operation enqueue(final List<? extends WorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, list).enqueue();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }
    
    @Override
    public Operation enqueueUniquePeriodicWork(final String s, final ExistingPeriodicWorkPolicy existingPeriodicWorkPolicy, final PeriodicWorkRequest periodicWorkRequest) {
        if (existingPeriodicWorkPolicy == ExistingPeriodicWorkPolicy.UPDATE) {
            return WorkerUpdater.enqueueUniquelyNamedPeriodic(this, s, periodicWorkRequest);
        }
        return this.createWorkContinuationForUniquePeriodicWork(s, existingPeriodicWorkPolicy, periodicWorkRequest).enqueue();
    }
    
    @Override
    public Operation enqueueUniqueWork(final String s, final ExistingWorkPolicy existingWorkPolicy, final List<OneTimeWorkRequest> list) {
        return new WorkContinuationImpl(this, s, existingWorkPolicy, list).enqueue();
    }
    
    public Context getApplicationContext() {
        return this.mContext;
    }
    
    @Override
    public Configuration getConfiguration() {
        return this.mConfiguration;
    }
    
    @Override
    public ListenableFuture<Long> getLastCancelAllTimeMillis() {
        final SettableFuture<Object> create = SettableFuture.create();
        this.mWorkTaskExecutor.executeOnTaskThread(new Runnable(this, create, this.mPreferenceUtils) {
            final WorkManagerImpl this$0;
            final SettableFuture val$future;
            final PreferenceUtils val$preferenceUtils;
            
            @Override
            public void run() {
                try {
                    this.val$future.set(this.val$preferenceUtils.getLastCancelAllTimeMillis());
                }
                finally {
                    final Throwable exception;
                    this.val$future.setException(exception);
                }
            }
        });
        return (ListenableFuture<Long>)create;
    }
    
    @Override
    public LiveData<Long> getLastCancelAllTimeMillisLiveData() {
        return this.mPreferenceUtils.getLastCancelAllTimeMillisLiveData();
    }
    
    public PreferenceUtils getPreferenceUtils() {
        return this.mPreferenceUtils;
    }
    
    public Processor getProcessor() {
        return this.mProcessor;
    }
    
    public RemoteWorkManager getRemoteWorkManager() {
        if (this.mRemoteWorkManager == null) {
            synchronized (WorkManagerImpl.sLock) {
                if (this.mRemoteWorkManager == null) {
                    this.tryInitializeMultiProcessSupport();
                    if (this.mRemoteWorkManager == null) {
                        if (!TextUtils.isEmpty((CharSequence)this.mConfiguration.getDefaultProcessName())) {
                            throw new IllegalStateException("Invalid multiprocess configuration. Define an `implementation` dependency on :work:work-multiprocess library");
                        }
                    }
                }
            }
        }
        return this.mRemoteWorkManager;
    }
    
    public List<Scheduler> getSchedulers() {
        return this.mSchedulers;
    }
    
    public Trackers getTrackers() {
        return this.mTrackers;
    }
    
    public WorkDatabase getWorkDatabase() {
        return this.mWorkDatabase;
    }
    
    @Override
    public ListenableFuture<WorkInfo> getWorkInfoById(final UUID uuid) {
        final StatusRunnable<WorkInfo> forUUID = StatusRunnable.forUUID(this, uuid);
        this.mWorkTaskExecutor.getSerialTaskExecutor().execute(forUUID);
        return forUUID.getFuture();
    }
    
    @Override
    public LiveData<WorkInfo> getWorkInfoByIdLiveData(final UUID uuid) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForIds(Collections.singletonList(uuid.toString())), (Function<List<WorkSpec.WorkInfoPojo>, WorkInfo>)new Function<List<WorkSpec.WorkInfoPojo>, WorkInfo>(this) {
            final WorkManagerImpl this$0;
            
            @Override
            public WorkInfo apply(final List<WorkSpec.WorkInfoPojo> list) {
                WorkInfo workInfo;
                if (list != null && list.size() > 0) {
                    workInfo = ((WorkSpec.WorkInfoPojo)list.get(0)).toWorkInfo();
                }
                else {
                    workInfo = null;
                }
                return workInfo;
            }
        }, this.mWorkTaskExecutor);
    }
    
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfos(final WorkQuery workQuery) {
        final StatusRunnable<List<WorkInfo>> forWorkQuerySpec = StatusRunnable.forWorkQuerySpec(this, workQuery);
        this.mWorkTaskExecutor.getSerialTaskExecutor().execute(forWorkQuerySpec);
        return forWorkQuerySpec.getFuture();
    }
    
    LiveData<List<WorkInfo>> getWorkInfosById(final List<String> list) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForIds(list), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfosByTag(final String s) {
        final StatusRunnable<List<WorkInfo>> forTag = StatusRunnable.forTag(this, s);
        this.mWorkTaskExecutor.getSerialTaskExecutor().execute(forTag);
        return forTag.getFuture();
    }
    
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosByTagLiveData(final String s) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForTag(s), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfosForUniqueWork(final String s) {
        final StatusRunnable<List<WorkInfo>> forUniqueWork = StatusRunnable.forUniqueWork(this, s);
        this.mWorkTaskExecutor.getSerialTaskExecutor().execute(forUniqueWork);
        return forUniqueWork.getFuture();
    }
    
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosForUniqueWorkLiveData(final String s) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForName(s), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosLiveData(final WorkQuery workQuery) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.rawWorkInfoDao().getWorkInfoPojosLiveData(RawQueries.toRawQuery(workQuery)), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    public TaskExecutor getWorkTaskExecutor() {
        return this.mWorkTaskExecutor;
    }
    
    public void onForceStopRunnableCompleted() {
        synchronized (WorkManagerImpl.sLock) {
            this.mForceStopRunnableCompleted = true;
            final BroadcastReceiver$PendingResult mRescheduleReceiverResult = this.mRescheduleReceiverResult;
            if (mRescheduleReceiverResult != null) {
                mRescheduleReceiverResult.finish();
                this.mRescheduleReceiverResult = null;
            }
        }
    }
    
    @Override
    public Operation pruneWork() {
        final PruneWorkRunnable pruneWorkRunnable = new PruneWorkRunnable(this);
        this.mWorkTaskExecutor.executeOnTaskThread(pruneWorkRunnable);
        return pruneWorkRunnable.getOperation();
    }
    
    public void rescheduleEligibleWork() {
        if (Build$VERSION.SDK_INT >= 23) {
            SystemJobScheduler.cancelAll(this.getApplicationContext());
        }
        this.getWorkDatabase().workSpecDao().resetScheduledState();
        Schedulers.schedule(this.getConfiguration(), this.getWorkDatabase(), this.getSchedulers());
    }
    
    public void setReschedulePendingResult(final BroadcastReceiver$PendingResult mRescheduleReceiverResult) {
        synchronized (WorkManagerImpl.sLock) {
            final BroadcastReceiver$PendingResult mRescheduleReceiverResult2 = this.mRescheduleReceiverResult;
            if (mRescheduleReceiverResult2 != null) {
                mRescheduleReceiverResult2.finish();
            }
            this.mRescheduleReceiverResult = mRescheduleReceiverResult;
            if (this.mForceStopRunnableCompleted) {
                mRescheduleReceiverResult.finish();
                this.mRescheduleReceiverResult = null;
            }
        }
    }
    
    public void startWork(final StartStopToken startStopToken) {
        this.startWork(startStopToken, null);
    }
    
    public void startWork(final StartStopToken startStopToken, final WorkerParameters.RuntimeExtras runtimeExtras) {
        this.mWorkTaskExecutor.executeOnTaskThread(new StartWorkRunnable(this, startStopToken, runtimeExtras));
    }
    
    public void stopForegroundWork(final WorkGenerationalId workGenerationalId) {
        this.mWorkTaskExecutor.executeOnTaskThread(new StopWorkRunnable(this, new StartStopToken(workGenerationalId), true));
    }
    
    public void stopWork(final StartStopToken startStopToken) {
        this.mWorkTaskExecutor.executeOnTaskThread(new StopWorkRunnable(this, startStopToken, false));
    }
    
    @Override
    public ListenableFuture<UpdateResult> updateWork(final WorkRequest workRequest) {
        return WorkerUpdater.updateWorkImpl(this, workRequest);
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean isDeviceProtectedStorage(final Context context) {
            return context.isDeviceProtectedStorage();
        }
    }
}

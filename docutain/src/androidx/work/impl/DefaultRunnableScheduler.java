// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.core.os.HandlerCompat;
import android.os.Looper;
import android.os.Handler;
import androidx.work.RunnableScheduler;

public class DefaultRunnableScheduler implements RunnableScheduler
{
    private final Handler mHandler;
    
    public DefaultRunnableScheduler() {
        this.mHandler = HandlerCompat.createAsync(Looper.getMainLooper());
    }
    
    public DefaultRunnableScheduler(final Handler mHandler) {
        this.mHandler = mHandler;
    }
    
    @Override
    public void cancel(final Runnable runnable) {
        this.mHandler.removeCallbacks(runnable);
    }
    
    public Handler getHandler() {
        return this.mHandler;
    }
    
    @Override
    public void scheduleWithDelay(final long n, final Runnable runnable) {
        this.mHandler.postDelayed(runnable, n);
    }
}

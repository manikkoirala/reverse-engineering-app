// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import kotlin.jvm.internal.Intrinsics;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;
import androidx.room.RoomDatabase;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\b8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\u000f" }, d2 = { "Landroidx/work/impl/CleanupCallback;", "Landroidx/room/RoomDatabase$Callback;", "()V", "pruneDate", "", "getPruneDate", "()J", "pruneSQL", "", "getPruneSQL", "()Ljava/lang/String;", "onOpen", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class CleanupCallback extends Callback
{
    public static final CleanupCallback INSTANCE;
    
    static {
        INSTANCE = new CleanupCallback();
    }
    
    private CleanupCallback() {
    }
    
    private final String getPruneSQL() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM workspec WHERE state IN (2, 3, 5) AND (last_enqueue_time + minimum_retention_duration) < ");
        sb.append(this.getPruneDate());
        sb.append(" AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))");
        return sb.toString();
    }
    
    public final long getPruneDate() {
        return System.currentTimeMillis() - WorkDatabaseKt.access$getPRUNE_THRESHOLD_MILLIS$p();
    }
    
    @Override
    public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        super.onOpen(supportSQLiteDatabase);
        supportSQLiteDatabase.beginTransaction();
        try {
            supportSQLiteDatabase.execSQL(this.getPruneSQL());
            supportSQLiteDatabase.setTransactionSuccessful();
        }
        finally {
            supportSQLiteDatabase.endTransaction();
        }
    }
}

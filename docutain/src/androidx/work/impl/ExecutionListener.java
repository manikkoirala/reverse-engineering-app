// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkGenerationalId;

public interface ExecutionListener
{
    void onExecuted(final WorkGenerationalId p0, final boolean p1);
}

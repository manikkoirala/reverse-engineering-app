// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.greedy;

import java.util.Collection;
import android.text.TextUtils;
import android.os.Build$VERSION;
import androidx.work.WorkInfo;
import java.util.List;
import androidx.work.impl.StartStopToken;
import java.util.Iterator;
import androidx.work.impl.model.WorkSpecKt;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.impl.utils.ProcessUtils;
import androidx.work.impl.constraints.WorkConstraintsTrackerImpl;
import java.util.HashSet;
import androidx.work.impl.constraints.trackers.Trackers;
import androidx.work.Configuration;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.work.impl.StartStopTokens;
import android.content.Context;
import androidx.work.impl.model.WorkSpec;
import java.util.Set;
import androidx.work.impl.ExecutionListener;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.work.impl.Scheduler;

public class GreedyScheduler implements Scheduler, WorkConstraintsCallback, ExecutionListener
{
    private static final String TAG;
    private final Set<WorkSpec> mConstrainedWorkSpecs;
    private final Context mContext;
    private DelayedWorkTracker mDelayedWorkTracker;
    Boolean mInDefaultProcess;
    private final Object mLock;
    private boolean mRegisteredExecutionListener;
    private final StartStopTokens mStartStopTokens;
    private final WorkConstraintsTracker mWorkConstraintsTracker;
    private final WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("GreedyScheduler");
    }
    
    public GreedyScheduler(final Context mContext, final Configuration configuration, final Trackers trackers, final WorkManagerImpl mWorkManagerImpl) {
        this.mConstrainedWorkSpecs = new HashSet<WorkSpec>();
        this.mStartStopTokens = new StartStopTokens();
        this.mContext = mContext;
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mWorkConstraintsTracker = new WorkConstraintsTrackerImpl(trackers, this);
        this.mDelayedWorkTracker = new DelayedWorkTracker(this, configuration.getRunnableScheduler());
        this.mLock = new Object();
    }
    
    public GreedyScheduler(final Context mContext, final WorkManagerImpl mWorkManagerImpl, final WorkConstraintsTracker mWorkConstraintsTracker) {
        this.mConstrainedWorkSpecs = new HashSet<WorkSpec>();
        this.mStartStopTokens = new StartStopTokens();
        this.mContext = mContext;
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mWorkConstraintsTracker = mWorkConstraintsTracker;
        this.mLock = new Object();
    }
    
    private void checkDefaultProcess() {
        this.mInDefaultProcess = ProcessUtils.isDefaultProcess(this.mContext, this.mWorkManagerImpl.getConfiguration());
    }
    
    private void registerExecutionListenerIfNeeded() {
        if (!this.mRegisteredExecutionListener) {
            this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
            this.mRegisteredExecutionListener = true;
        }
    }
    
    private void removeConstraintTrackingFor(final WorkGenerationalId obj) {
        synchronized (this.mLock) {
            for (final WorkSpec workSpec : this.mConstrainedWorkSpecs) {
                if (WorkSpecKt.generationalId(workSpec).equals(obj)) {
                    final Logger value = Logger.get();
                    final String tag = GreedyScheduler.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Stopping tracking for ");
                    sb.append(obj);
                    value.debug(tag, sb.toString());
                    this.mConstrainedWorkSpecs.remove(workSpec);
                    this.mWorkConstraintsTracker.replace(this.mConstrainedWorkSpecs);
                    break;
                }
            }
        }
    }
    
    @Override
    public void cancel(final String str) {
        if (this.mInDefaultProcess == null) {
            this.checkDefaultProcess();
        }
        if (!this.mInDefaultProcess) {
            Logger.get().info(GreedyScheduler.TAG, "Ignoring schedule request in non-main process");
            return;
        }
        this.registerExecutionListenerIfNeeded();
        final Logger value = Logger.get();
        final String tag = GreedyScheduler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Cancelling work ID ");
        sb.append(str);
        value.debug(tag, sb.toString());
        final DelayedWorkTracker mDelayedWorkTracker = this.mDelayedWorkTracker;
        if (mDelayedWorkTracker != null) {
            mDelayedWorkTracker.unschedule(str);
        }
        final Iterator<StartStopToken> iterator = this.mStartStopTokens.remove(str).iterator();
        while (iterator.hasNext()) {
            this.mWorkManagerImpl.stopWork(iterator.next());
        }
    }
    
    @Override
    public boolean hasLimitedSchedulingSlots() {
        return false;
    }
    
    @Override
    public void onAllConstraintsMet(final List<WorkSpec> list) {
        final Iterator<WorkSpec> iterator = list.iterator();
        while (iterator.hasNext()) {
            final WorkGenerationalId generationalId = WorkSpecKt.generationalId(iterator.next());
            if (!this.mStartStopTokens.contains(generationalId)) {
                final Logger value = Logger.get();
                final String tag = GreedyScheduler.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Constraints met: Scheduling work ID ");
                sb.append(generationalId);
                value.debug(tag, sb.toString());
                this.mWorkManagerImpl.startWork(this.mStartStopTokens.tokenFor(generationalId));
            }
        }
    }
    
    @Override
    public void onAllConstraintsNotMet(final List<WorkSpec> list) {
        final Iterator<WorkSpec> iterator = list.iterator();
        while (iterator.hasNext()) {
            final WorkGenerationalId generationalId = WorkSpecKt.generationalId(iterator.next());
            final Logger value = Logger.get();
            final String tag = GreedyScheduler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Constraints not met: Cancelling work ID ");
            sb.append(generationalId);
            value.debug(tag, sb.toString());
            final StartStopToken remove = this.mStartStopTokens.remove(generationalId);
            if (remove != null) {
                this.mWorkManagerImpl.stopWork(remove);
            }
        }
    }
    
    @Override
    public void onExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        this.mStartStopTokens.remove(workGenerationalId);
        this.removeConstraintTrackingFor(workGenerationalId);
    }
    
    @Override
    public void schedule(final WorkSpec... array) {
        if (this.mInDefaultProcess == null) {
            this.checkDefaultProcess();
        }
        if (!this.mInDefaultProcess) {
            Logger.get().info(GreedyScheduler.TAG, "Ignoring schedule request in a secondary process");
            return;
        }
        this.registerExecutionListenerIfNeeded();
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        for (final WorkSpec workSpec : array) {
            if (!this.mStartStopTokens.contains(WorkSpecKt.generationalId(workSpec))) {
                final long calculateNextRunTime = workSpec.calculateNextRunTime();
                final long currentTimeMillis = System.currentTimeMillis();
                if (workSpec.state == WorkInfo.State.ENQUEUED) {
                    if (currentTimeMillis < calculateNextRunTime) {
                        final DelayedWorkTracker mDelayedWorkTracker = this.mDelayedWorkTracker;
                        if (mDelayedWorkTracker != null) {
                            mDelayedWorkTracker.schedule(workSpec);
                        }
                    }
                    else if (workSpec.hasConstraints()) {
                        if (Build$VERSION.SDK_INT >= 23 && workSpec.constraints.requiresDeviceIdle()) {
                            final Logger value = Logger.get();
                            final String tag = GreedyScheduler.TAG;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Ignoring ");
                            sb.append(workSpec);
                            sb.append(". Requires device idle.");
                            value.debug(tag, sb.toString());
                        }
                        else if (Build$VERSION.SDK_INT >= 24 && workSpec.constraints.hasContentUriTriggers()) {
                            final Logger value2 = Logger.get();
                            final String tag2 = GreedyScheduler.TAG;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Ignoring ");
                            sb2.append(workSpec);
                            sb2.append(". Requires ContentUri triggers.");
                            value2.debug(tag2, sb2.toString());
                        }
                        else {
                            set.add(workSpec);
                            set2.add(workSpec.id);
                        }
                    }
                    else if (!this.mStartStopTokens.contains(WorkSpecKt.generationalId(workSpec))) {
                        final Logger value3 = Logger.get();
                        final String tag3 = GreedyScheduler.TAG;
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Starting work for ");
                        sb3.append(workSpec.id);
                        value3.debug(tag3, sb3.toString());
                        this.mWorkManagerImpl.startWork(this.mStartStopTokens.tokenFor(workSpec));
                    }
                }
            }
        }
        synchronized (this.mLock) {
            if (!set.isEmpty()) {
                final String join = TextUtils.join((CharSequence)",", (Iterable)set2);
                final Logger value4 = Logger.get();
                final String tag4 = GreedyScheduler.TAG;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Starting tracking for ");
                sb4.append(join);
                value4.debug(tag4, sb4.toString());
                this.mConstrainedWorkSpecs.addAll(set);
                this.mWorkConstraintsTracker.replace(this.mConstrainedWorkSpecs);
            }
        }
    }
    
    public void setDelayedWorkTracker(final DelayedWorkTracker mDelayedWorkTracker) {
        this.mDelayedWorkTracker = mDelayedWorkTracker;
    }
}

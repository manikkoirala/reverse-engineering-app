// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.model.SystemIdInfoKt;
import androidx.work.impl.utils.IdGenerator;
import android.content.Intent;
import android.app.PendingIntent;
import android.os.Build$VERSION;
import android.app.AlarmManager;
import androidx.work.impl.model.SystemIdInfo;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.impl.WorkDatabase;
import android.content.Context;
import androidx.work.Logger;

class Alarms
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("Alarms");
    }
    
    private Alarms() {
    }
    
    public static void cancelAlarm(final Context context, final WorkDatabase workDatabase, final WorkGenerationalId obj) {
        final SystemIdInfoDao systemIdInfoDao = workDatabase.systemIdInfoDao();
        final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(obj);
        if (systemIdInfo != null) {
            cancelExactAlarm(context, obj, systemIdInfo.systemId);
            final Logger value = Logger.get();
            final String tag = Alarms.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Removing SystemIdInfo for workSpecId (");
            sb.append(obj);
            sb.append(")");
            value.debug(tag, sb.toString());
            systemIdInfoDao.removeSystemIdInfo(obj);
        }
    }
    
    private static void cancelExactAlarm(final Context context, final WorkGenerationalId obj, final int i) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        final Intent delayMetIntent = CommandHandler.createDelayMetIntent(context, obj);
        int n;
        if (Build$VERSION.SDK_INT >= 23) {
            n = 603979776;
        }
        else {
            n = 536870912;
        }
        final PendingIntent service = PendingIntent.getService(context, i, delayMetIntent, n);
        if (service != null && alarmManager != null) {
            final Logger value = Logger.get();
            final String tag = Alarms.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Cancelling existing alarm with (workSpecId, systemId) (");
            sb.append(obj);
            sb.append(", ");
            sb.append(i);
            sb.append(")");
            value.debug(tag, sb.toString());
            alarmManager.cancel(service);
        }
    }
    
    public static void setAlarm(final Context context, final WorkDatabase workDatabase, final WorkGenerationalId workGenerationalId, final long n) {
        final SystemIdInfoDao systemIdInfoDao = workDatabase.systemIdInfoDao();
        final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(workGenerationalId);
        if (systemIdInfo != null) {
            cancelExactAlarm(context, workGenerationalId, systemIdInfo.systemId);
            setExactAlarm(context, workGenerationalId, systemIdInfo.systemId, n);
        }
        else {
            final int nextAlarmManagerId = new IdGenerator(workDatabase).nextAlarmManagerId();
            systemIdInfoDao.insertSystemIdInfo(SystemIdInfoKt.systemIdInfo(workGenerationalId, nextAlarmManagerId));
            setExactAlarm(context, workGenerationalId, nextAlarmManagerId, n);
        }
    }
    
    private static void setExactAlarm(final Context context, final WorkGenerationalId workGenerationalId, final int n, final long n2) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        int n3;
        if (Build$VERSION.SDK_INT >= 23) {
            n3 = 201326592;
        }
        else {
            n3 = 134217728;
        }
        final PendingIntent service = PendingIntent.getService(context, n, CommandHandler.createDelayMetIntent(context, workGenerationalId), n3);
        if (alarmManager != null) {
            if (Build$VERSION.SDK_INT >= 19) {
                Api19Impl.setExact(alarmManager, 0, n2, service);
            }
            else {
                alarmManager.set(0, n2, service);
            }
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void setExact(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setExact(n, n2, pendingIntent);
        }
    }
}

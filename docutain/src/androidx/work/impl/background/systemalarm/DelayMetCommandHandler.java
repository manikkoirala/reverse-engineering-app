// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import java.util.Iterator;
import androidx.work.impl.model.WorkSpecKt;
import java.util.List;
import androidx.work.impl.model.WorkSpec;
import java.util.Collections;
import androidx.work.impl.utils.WakeLocks;
import androidx.work.impl.constraints.trackers.Trackers;
import androidx.work.Logger;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.impl.constraints.WorkConstraintsTrackerImpl;
import android.os.PowerManager$WakeLock;
import androidx.work.impl.StartStopToken;
import java.util.concurrent.Executor;
import android.content.Context;
import androidx.work.impl.utils.WorkTimer;
import androidx.work.impl.constraints.WorkConstraintsCallback;

public class DelayMetCommandHandler implements WorkConstraintsCallback, TimeLimitExceededListener
{
    private static final int STATE_INITIAL = 0;
    private static final int STATE_START_REQUESTED = 1;
    private static final int STATE_STOP_REQUESTED = 2;
    private static final String TAG;
    private final Context mContext;
    private int mCurrentState;
    private final SystemAlarmDispatcher mDispatcher;
    private boolean mHasConstraints;
    private final Object mLock;
    private final Executor mMainThreadExecutor;
    private final Executor mSerialExecutor;
    private final int mStartId;
    private final StartStopToken mToken;
    private PowerManager$WakeLock mWakeLock;
    private final WorkConstraintsTrackerImpl mWorkConstraintsTracker;
    private final WorkGenerationalId mWorkGenerationalId;
    
    static {
        TAG = Logger.tagWithPrefix("DelayMetCommandHandler");
    }
    
    DelayMetCommandHandler(final Context mContext, final int mStartId, final SystemAlarmDispatcher mDispatcher, final StartStopToken mToken) {
        this.mContext = mContext;
        this.mStartId = mStartId;
        this.mDispatcher = mDispatcher;
        this.mWorkGenerationalId = mToken.getId();
        this.mToken = mToken;
        final Trackers trackers = mDispatcher.getWorkManager().getTrackers();
        this.mSerialExecutor = mDispatcher.getTaskExecutor().getSerialTaskExecutor();
        this.mMainThreadExecutor = mDispatcher.getTaskExecutor().getMainThreadExecutor();
        this.mWorkConstraintsTracker = new WorkConstraintsTrackerImpl(trackers, this);
        this.mHasConstraints = false;
        this.mCurrentState = 0;
        this.mLock = new Object();
    }
    
    private void cleanUp() {
        synchronized (this.mLock) {
            this.mWorkConstraintsTracker.reset();
            this.mDispatcher.getWorkTimer().stopTimer(this.mWorkGenerationalId);
            final PowerManager$WakeLock mWakeLock = this.mWakeLock;
            if (mWakeLock != null && mWakeLock.isHeld()) {
                final Logger value = Logger.get();
                final String tag = DelayMetCommandHandler.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Releasing wakelock ");
                sb.append(this.mWakeLock);
                sb.append("for WorkSpec ");
                sb.append(this.mWorkGenerationalId);
                value.debug(tag, sb.toString());
                this.mWakeLock.release();
            }
        }
    }
    
    private void startWork() {
        if (this.mCurrentState == 0) {
            this.mCurrentState = 1;
            final Logger value = Logger.get();
            final String tag = DelayMetCommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("onAllConstraintsMet for ");
            sb.append(this.mWorkGenerationalId);
            value.debug(tag, sb.toString());
            if (this.mDispatcher.getProcessor().startWork(this.mToken)) {
                this.mDispatcher.getWorkTimer().startTimer(this.mWorkGenerationalId, 600000L, (WorkTimer.TimeLimitExceededListener)this);
            }
            else {
                this.cleanUp();
            }
        }
        else {
            final Logger value2 = Logger.get();
            final String tag2 = DelayMetCommandHandler.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Already started work for ");
            sb2.append(this.mWorkGenerationalId);
            value2.debug(tag2, sb2.toString());
        }
    }
    
    private void stopWork() {
        final String workSpecId = this.mWorkGenerationalId.getWorkSpecId();
        if (this.mCurrentState < 2) {
            this.mCurrentState = 2;
            final Logger value = Logger.get();
            final String tag = DelayMetCommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Stopping work for WorkSpec ");
            sb.append(workSpecId);
            value.debug(tag, sb.toString());
            this.mMainThreadExecutor.execute(new SystemAlarmDispatcher.AddRunnable(this.mDispatcher, CommandHandler.createStopWorkIntent(this.mContext, this.mWorkGenerationalId), this.mStartId));
            if (this.mDispatcher.getProcessor().isEnqueued(this.mWorkGenerationalId.getWorkSpecId())) {
                final Logger value2 = Logger.get();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("WorkSpec ");
                sb2.append(workSpecId);
                sb2.append(" needs to be rescheduled");
                value2.debug(tag, sb2.toString());
                this.mMainThreadExecutor.execute(new SystemAlarmDispatcher.AddRunnable(this.mDispatcher, CommandHandler.createScheduleWorkIntent(this.mContext, this.mWorkGenerationalId), this.mStartId));
            }
            else {
                final Logger value3 = Logger.get();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Processor does not have WorkSpec ");
                sb3.append(workSpecId);
                sb3.append(". No need to reschedule");
                value3.debug(tag, sb3.toString());
            }
        }
        else {
            final Logger value4 = Logger.get();
            final String tag2 = DelayMetCommandHandler.TAG;
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Already stopped work for ");
            sb4.append(workSpecId);
            value4.debug(tag2, sb4.toString());
        }
    }
    
    void handleProcessWork() {
        final String workSpecId = this.mWorkGenerationalId.getWorkSpecId();
        final Context mContext = this.mContext;
        final StringBuilder sb = new StringBuilder();
        sb.append(workSpecId);
        sb.append(" (");
        sb.append(this.mStartId);
        sb.append(")");
        this.mWakeLock = WakeLocks.newWakeLock(mContext, sb.toString());
        final Logger value = Logger.get();
        final String tag = DelayMetCommandHandler.TAG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Acquiring wakelock ");
        sb2.append(this.mWakeLock);
        sb2.append("for WorkSpec ");
        sb2.append(workSpecId);
        value.debug(tag, sb2.toString());
        this.mWakeLock.acquire();
        final WorkSpec workSpec = this.mDispatcher.getWorkManager().getWorkDatabase().workSpecDao().getWorkSpec(workSpecId);
        if (workSpec == null) {
            this.mSerialExecutor.execute(new DelayMetCommandHandler$$ExternalSyntheticLambda0(this));
            return;
        }
        if (!(this.mHasConstraints = workSpec.hasConstraints())) {
            final Logger value2 = Logger.get();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("No constraints for ");
            sb3.append(workSpecId);
            value2.debug(tag, sb3.toString());
            this.onAllConstraintsMet(Collections.singletonList(workSpec));
        }
        else {
            this.mWorkConstraintsTracker.replace(Collections.singletonList(workSpec));
        }
    }
    
    @Override
    public void onAllConstraintsMet(final List<WorkSpec> list) {
        final Iterator<WorkSpec> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (WorkSpecKt.generationalId(iterator.next()).equals(this.mWorkGenerationalId)) {
                this.mSerialExecutor.execute(new DelayMetCommandHandler$$ExternalSyntheticLambda1(this));
                break;
            }
        }
    }
    
    @Override
    public void onAllConstraintsNotMet(final List<WorkSpec> list) {
        this.mSerialExecutor.execute(new DelayMetCommandHandler$$ExternalSyntheticLambda0(this));
    }
    
    void onExecuted(final boolean b) {
        final Logger value = Logger.get();
        final String tag = DelayMetCommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("onExecuted ");
        sb.append(this.mWorkGenerationalId);
        sb.append(", ");
        sb.append(b);
        value.debug(tag, sb.toString());
        this.cleanUp();
        if (b) {
            this.mMainThreadExecutor.execute(new SystemAlarmDispatcher.AddRunnable(this.mDispatcher, CommandHandler.createScheduleWorkIntent(this.mContext, this.mWorkGenerationalId), this.mStartId));
        }
        if (this.mHasConstraints) {
            this.mMainThreadExecutor.execute(new SystemAlarmDispatcher.AddRunnable(this.mDispatcher, CommandHandler.createConstraintsChangedIntent(this.mContext), this.mStartId));
        }
    }
    
    @Override
    public void onTimeLimitExceeded(final WorkGenerationalId obj) {
        final Logger value = Logger.get();
        final String tag = DelayMetCommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Exceeded time limits on execution for ");
        sb.append(obj);
        value.debug(tag, sb.toString());
        this.mSerialExecutor.execute(new DelayMetCommandHandler$$ExternalSyntheticLambda0(this));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import java.util.Iterator;
import java.util.List;
import androidx.work.impl.model.WorkSpecKt;
import java.util.ArrayList;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.work.Logger;
import androidx.work.impl.constraints.WorkConstraintsTrackerImpl;
import android.content.Context;

class ConstraintsCommandHandler
{
    private static final String TAG;
    private final Context mContext;
    private final SystemAlarmDispatcher mDispatcher;
    private final int mStartId;
    private final WorkConstraintsTrackerImpl mWorkConstraintsTracker;
    
    static {
        TAG = Logger.tagWithPrefix("ConstraintsCmdHandler");
    }
    
    ConstraintsCommandHandler(final Context mContext, final int mStartId, final SystemAlarmDispatcher mDispatcher) {
        this.mContext = mContext;
        this.mStartId = mStartId;
        this.mDispatcher = mDispatcher;
        this.mWorkConstraintsTracker = new WorkConstraintsTrackerImpl(mDispatcher.getWorkManager().getTrackers(), null);
    }
    
    void handleConstraintsChanged() {
        final List<WorkSpec> scheduledWork = this.mDispatcher.getWorkManager().getWorkDatabase().workSpecDao().getScheduledWork();
        ConstraintProxy.updateAll(this.mContext, scheduledWork);
        this.mWorkConstraintsTracker.replace(scheduledWork);
        final ArrayList list = new ArrayList(scheduledWork.size());
        final long currentTimeMillis = System.currentTimeMillis();
        for (final WorkSpec workSpec : scheduledWork) {
            final String id = workSpec.id;
            if (currentTimeMillis >= workSpec.calculateNextRunTime() && (!workSpec.hasConstraints() || this.mWorkConstraintsTracker.areAllConstraintsMet(id))) {
                list.add((Object)workSpec);
            }
        }
        for (final WorkSpec workSpec2 : list) {
            final String id2 = workSpec2.id;
            final Intent delayMetIntent = CommandHandler.createDelayMetIntent(this.mContext, WorkSpecKt.generationalId(workSpec2));
            final Logger value = Logger.get();
            final String tag = ConstraintsCommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Creating a delay_met command for workSpec with id (");
            sb.append(id2);
            sb.append(")");
            value.debug(tag, sb.toString());
            this.mDispatcher.getTaskExecutor().getMainThreadExecutor().execute(new SystemAlarmDispatcher.AddRunnable(this.mDispatcher, delayMetIntent, this.mStartId));
        }
        this.mWorkConstraintsTracker.reset();
    }
}

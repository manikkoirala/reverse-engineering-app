// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import androidx.work.impl.utils.WakeLocks;
import android.content.Context;
import androidx.work.Logger;
import androidx.lifecycle.LifecycleService;

public class SystemAlarmService extends LifecycleService implements CommandsCompletedListener
{
    private static final String TAG;
    private SystemAlarmDispatcher mDispatcher;
    private boolean mIsShutdown;
    
    static {
        TAG = Logger.tagWithPrefix("SystemAlarmService");
    }
    
    private void initializeDispatcher() {
        (this.mDispatcher = new SystemAlarmDispatcher((Context)this)).setCompletedListener((SystemAlarmDispatcher.CommandsCompletedListener)this);
    }
    
    @Override
    public void onAllCommandsCompleted() {
        this.mIsShutdown = true;
        Logger.get().debug(SystemAlarmService.TAG, "All commands completed in dispatcher");
        WakeLocks.checkWakeLocks();
        this.stopSelf();
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeDispatcher();
        this.mIsShutdown = false;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mIsShutdown = true;
        this.mDispatcher.onDestroy();
    }
    
    @Override
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.mIsShutdown) {
            Logger.get().info(SystemAlarmService.TAG, "Re-initializing SystemAlarmDispatcher after a request to shut-down.");
            this.mDispatcher.onDestroy();
            this.initializeDispatcher();
            this.mIsShutdown = false;
        }
        if (intent != null) {
            this.mDispatcher.add(intent, n2);
        }
        return 3;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.model.WorkSpecKt;
import androidx.work.impl.model.WorkSpec;
import androidx.work.Logger;
import android.content.Context;
import androidx.work.impl.Scheduler;

public class SystemAlarmScheduler implements Scheduler
{
    private static final String TAG;
    private final Context mContext;
    
    static {
        TAG = Logger.tagWithPrefix("SystemAlarmScheduler");
    }
    
    public SystemAlarmScheduler(final Context context) {
        this.mContext = context.getApplicationContext();
    }
    
    private void scheduleWorkSpec(final WorkSpec workSpec) {
        final Logger value = Logger.get();
        final String tag = SystemAlarmScheduler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Scheduling work with workSpecId ");
        sb.append(workSpec.id);
        value.debug(tag, sb.toString());
        this.mContext.startService(CommandHandler.createScheduleWorkIntent(this.mContext, WorkSpecKt.generationalId(workSpec)));
    }
    
    @Override
    public void cancel(final String s) {
        this.mContext.startService(CommandHandler.createStopWorkIntent(this.mContext, s));
    }
    
    @Override
    public boolean hasLimitedSchedulingSlots() {
        return true;
    }
    
    @Override
    public void schedule(final WorkSpec... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.scheduleWorkSpec(array[i]);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import java.util.concurrent.Executor;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.impl.utils.taskexecutor.SerialExecutor;
import android.text.TextUtils;
import android.os.PowerManager$WakeLock;
import androidx.work.impl.utils.WakeLocks;
import java.util.Iterator;
import android.os.Looper;
import java.util.ArrayList;
import androidx.work.Logger;
import androidx.work.impl.utils.WorkTimer;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.StartStopTokens;
import androidx.work.impl.Processor;
import java.util.List;
import android.content.Intent;
import android.content.Context;
import androidx.work.impl.ExecutionListener;

public class SystemAlarmDispatcher implements ExecutionListener
{
    private static final int DEFAULT_START_ID = 0;
    private static final String KEY_START_ID = "KEY_START_ID";
    private static final String PROCESS_COMMAND_TAG = "ProcessCommand";
    static final String TAG;
    final CommandHandler mCommandHandler;
    private CommandsCompletedListener mCompletedListener;
    final Context mContext;
    Intent mCurrentIntent;
    final List<Intent> mIntents;
    private final Processor mProcessor;
    private StartStopTokens mStartStopTokens;
    final TaskExecutor mTaskExecutor;
    private final WorkManagerImpl mWorkManager;
    private final WorkTimer mWorkTimer;
    
    static {
        TAG = Logger.tagWithPrefix("SystemAlarmDispatcher");
    }
    
    SystemAlarmDispatcher(final Context context) {
        this(context, null, null);
    }
    
    SystemAlarmDispatcher(final Context context, Processor processor, WorkManagerImpl instance) {
        final Context applicationContext = context.getApplicationContext();
        this.mContext = applicationContext;
        this.mStartStopTokens = new StartStopTokens();
        this.mCommandHandler = new CommandHandler(applicationContext, this.mStartStopTokens);
        if (instance == null) {
            instance = WorkManagerImpl.getInstance(context);
        }
        this.mWorkManager = instance;
        this.mWorkTimer = new WorkTimer(instance.getConfiguration().getRunnableScheduler());
        if (processor == null) {
            processor = instance.getProcessor();
        }
        this.mProcessor = processor;
        this.mTaskExecutor = instance.getWorkTaskExecutor();
        processor.addExecutionListener(this);
        this.mIntents = new ArrayList<Intent>();
        this.mCurrentIntent = null;
    }
    
    private void assertMainThread() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            return;
        }
        throw new IllegalStateException("Needs to be invoked on the main thread.");
    }
    
    private boolean hasIntentWithAction(final String s) {
        this.assertMainThread();
        synchronized (this.mIntents) {
            final Iterator<Intent> iterator = this.mIntents.iterator();
            while (iterator.hasNext()) {
                if (s.equals(iterator.next().getAction())) {
                    return true;
                }
            }
            return false;
        }
    }
    
    private void processCommand() {
        this.assertMainThread();
        final PowerManager$WakeLock wakeLock = WakeLocks.newWakeLock(this.mContext, "ProcessCommand");
        try {
            wakeLock.acquire();
            this.mWorkManager.getWorkTaskExecutor().executeOnTaskThread(new Runnable(this) {
                final SystemAlarmDispatcher this$0;
                
                @Override
                public void run() {
                    Object o = this.this$0.mIntents;
                    synchronized (o) {
                        final SystemAlarmDispatcher this$0 = this.this$0;
                        this$0.mCurrentIntent = this$0.mIntents.get(0);
                        monitorexit(o);
                        if (this.this$0.mCurrentIntent != null) {
                            o = this.this$0.mCurrentIntent.getAction();
                            final int intExtra = this.this$0.mCurrentIntent.getIntExtra("KEY_START_ID", 0);
                            final Logger value = Logger.get();
                            final String tag = SystemAlarmDispatcher.TAG;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Processing command ");
                            sb.append(this.this$0.mCurrentIntent);
                            sb.append(", ");
                            sb.append(intExtra);
                            value.debug(tag, sb.toString());
                            final Context mContext = this.this$0.mContext;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append((String)o);
                            sb2.append(" (");
                            sb2.append(intExtra);
                            sb2.append(")");
                            Object obj = WakeLocks.newWakeLock(mContext, sb2.toString());
                            Label_0477: {
                                final Throwable t2;
                                try {
                                    final Logger value2 = Logger.get();
                                    final String tag2 = SystemAlarmDispatcher.TAG;
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Acquiring operation wake lock (");
                                    sb3.append((String)o);
                                    sb3.append(") ");
                                    sb3.append(obj);
                                    value2.debug(tag2, sb3.toString());
                                    ((PowerManager$WakeLock)obj).acquire();
                                    this.this$0.mCommandHandler.onHandleIntent(this.this$0.mCurrentIntent, intExtra, this.this$0);
                                    final Logger value3 = Logger.get();
                                    final String tag3 = SystemAlarmDispatcher.TAG;
                                    final StringBuilder sb4 = new StringBuilder();
                                    sb4.append("Releasing operation wake lock (");
                                    sb4.append((String)o);
                                    sb4.append(") ");
                                    sb4.append(obj);
                                    value3.debug(tag3, sb4.toString());
                                    ((PowerManager$WakeLock)obj).release();
                                    obj = this.this$0.mTaskExecutor.getMainThreadExecutor();
                                    o = new DequeueAndCheckForCompletion(this.this$0);
                                    break Label_0477;
                                }
                                finally {
                                    final Logger logger = Logger.get();
                                    final String s = SystemAlarmDispatcher.TAG;
                                    final String s2 = "Unexpected error in onHandleIntent";
                                    final Throwable t = t2;
                                    logger.error(s, s2, t);
                                    final Logger logger2 = Logger.get();
                                    final String s3 = SystemAlarmDispatcher.TAG;
                                    final StringBuilder sb5 = new StringBuilder();
                                    final StringBuilder sb7;
                                    final StringBuilder sb6 = sb7 = sb5;
                                    final String s4 = "Releasing operation wake lock (";
                                    sb7.append(s4);
                                    final StringBuilder sb8 = sb6;
                                    sb8.append((String)o);
                                    final StringBuilder sb9 = sb6;
                                    final String s5 = ") ";
                                    sb9.append(s5);
                                    final StringBuilder sb10 = sb6;
                                    final Object o2 = obj;
                                    sb10.append(o2);
                                    final Logger logger3 = logger2;
                                    final String s6 = s3;
                                    final StringBuilder sb11 = sb6;
                                    final String s7 = sb11.toString();
                                    logger3.debug(s6, s7);
                                    final Object o3 = obj;
                                    ((PowerManager$WakeLock)o3).release();
                                    final Runnable runnable = this;
                                    final SystemAlarmDispatcher systemAlarmDispatcher = runnable.this$0;
                                    final TaskExecutor taskExecutor = systemAlarmDispatcher.mTaskExecutor;
                                    obj = taskExecutor.getMainThreadExecutor();
                                    final Runnable runnable2 = this;
                                    final SystemAlarmDispatcher systemAlarmDispatcher2 = runnable2.this$0;
                                    final DequeueAndCheckForCompletion dequeueAndCheckForCompletion = (DequeueAndCheckForCompletion)(o = new DequeueAndCheckForCompletion(systemAlarmDispatcher2));
                                }
                                try {
                                    final Logger logger = Logger.get();
                                    final String s = SystemAlarmDispatcher.TAG;
                                    final String s2 = "Unexpected error in onHandleIntent";
                                    final Throwable t = t2;
                                    logger.error(s, s2, t);
                                    final Logger logger2 = Logger.get();
                                    final String s3 = SystemAlarmDispatcher.TAG;
                                    final StringBuilder sb5 = new StringBuilder();
                                    final StringBuilder sb7;
                                    final StringBuilder sb6 = sb7 = sb5;
                                    final String s4 = "Releasing operation wake lock (";
                                    sb7.append(s4);
                                    final StringBuilder sb8 = sb6;
                                    sb8.append((String)o);
                                    final StringBuilder sb9 = sb6;
                                    final String s5 = ") ";
                                    sb9.append(s5);
                                    final StringBuilder sb10 = sb6;
                                    final Object o2 = obj;
                                    sb10.append(o2);
                                    final Logger logger3 = logger2;
                                    final String s6 = s3;
                                    final StringBuilder sb11 = sb6;
                                    final String s7 = sb11.toString();
                                    logger3.debug(s6, s7);
                                    final Object o3 = obj;
                                    ((PowerManager$WakeLock)o3).release();
                                    final Runnable runnable = this;
                                    final SystemAlarmDispatcher systemAlarmDispatcher = runnable.this$0;
                                    final TaskExecutor taskExecutor = systemAlarmDispatcher.mTaskExecutor;
                                    obj = taskExecutor.getMainThreadExecutor();
                                    final Runnable runnable2 = this;
                                    final SystemAlarmDispatcher systemAlarmDispatcher2 = runnable2.this$0;
                                    o = new DequeueAndCheckForCompletion(systemAlarmDispatcher2);
                                    ((Executor)obj).execute((Runnable)o);
                                }
                                finally {
                                    final Logger value4 = Logger.get();
                                    final String tag4 = SystemAlarmDispatcher.TAG;
                                    final StringBuilder sb12 = new StringBuilder();
                                    sb12.append("Releasing operation wake lock (");
                                    sb12.append((String)o);
                                    sb12.append(") ");
                                    sb12.append(obj);
                                    value4.debug(tag4, sb12.toString());
                                    ((PowerManager$WakeLock)obj).release();
                                    this.this$0.mTaskExecutor.getMainThreadExecutor().execute(new DequeueAndCheckForCompletion(this.this$0));
                                }
                            }
                        }
                    }
                }
            });
        }
        finally {
            wakeLock.release();
        }
    }
    
    public boolean add(final Intent obj, int i) {
        final Logger value = Logger.get();
        final String tag = SystemAlarmDispatcher.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Adding command ");
        sb.append(obj);
        sb.append(" (");
        sb.append(i);
        sb.append(")");
        value.debug(tag, sb.toString());
        this.assertMainThread();
        final String action = obj.getAction();
        final boolean empty = TextUtils.isEmpty((CharSequence)action);
        final int n = 0;
        if (empty) {
            Logger.get().warning(tag, "Unknown command. Ignoring");
            return false;
        }
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && this.hasIntentWithAction("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        }
        obj.putExtra("KEY_START_ID", i);
        final List<Intent> mIntents = this.mIntents;
        monitorenter(mIntents);
        i = n;
        try {
            if (!this.mIntents.isEmpty()) {
                i = 1;
            }
            this.mIntents.add(obj);
            if (i == 0) {
                this.processCommand();
            }
            return true;
        }
        finally {
            monitorexit(mIntents);
        }
    }
    
    void dequeueAndCheckForCompletion() {
        final Logger value = Logger.get();
        final String tag = SystemAlarmDispatcher.TAG;
        value.debug(tag, "Checking if commands are complete.");
        this.assertMainThread();
        synchronized (this.mIntents) {
            if (this.mCurrentIntent != null) {
                final Logger value2 = Logger.get();
                final StringBuilder sb = new StringBuilder();
                sb.append("Removing command ");
                sb.append(this.mCurrentIntent);
                value2.debug(tag, sb.toString());
                if (!this.mIntents.remove(0).equals(this.mCurrentIntent)) {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
                this.mCurrentIntent = null;
            }
            final SerialExecutor serialTaskExecutor = this.mTaskExecutor.getSerialTaskExecutor();
            if (!this.mCommandHandler.hasPendingCommands() && this.mIntents.isEmpty() && !serialTaskExecutor.hasPendingTasks()) {
                Logger.get().debug(tag, "No more commands & intents.");
                final CommandsCompletedListener mCompletedListener = this.mCompletedListener;
                if (mCompletedListener != null) {
                    mCompletedListener.onAllCommandsCompleted();
                }
            }
            else if (!this.mIntents.isEmpty()) {
                this.processCommand();
            }
        }
    }
    
    Processor getProcessor() {
        return this.mProcessor;
    }
    
    TaskExecutor getTaskExecutor() {
        return this.mTaskExecutor;
    }
    
    WorkManagerImpl getWorkManager() {
        return this.mWorkManager;
    }
    
    WorkTimer getWorkTimer() {
        return this.mWorkTimer;
    }
    
    void onDestroy() {
        Logger.get().debug(SystemAlarmDispatcher.TAG, "Destroying SystemAlarmDispatcher");
        this.mProcessor.removeExecutionListener(this);
        this.mCompletedListener = null;
    }
    
    @Override
    public void onExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        this.mTaskExecutor.getMainThreadExecutor().execute(new AddRunnable(this, CommandHandler.createExecutionCompletedIntent(this.mContext, workGenerationalId, b), 0));
    }
    
    void setCompletedListener(final CommandsCompletedListener mCompletedListener) {
        if (this.mCompletedListener != null) {
            Logger.get().error(SystemAlarmDispatcher.TAG, "A completion listener for SystemAlarmDispatcher already exists.");
            return;
        }
        this.mCompletedListener = mCompletedListener;
    }
    
    static class AddRunnable implements Runnable
    {
        private final SystemAlarmDispatcher mDispatcher;
        private final Intent mIntent;
        private final int mStartId;
        
        AddRunnable(final SystemAlarmDispatcher mDispatcher, final Intent mIntent, final int mStartId) {
            this.mDispatcher = mDispatcher;
            this.mIntent = mIntent;
            this.mStartId = mStartId;
        }
        
        @Override
        public void run() {
            this.mDispatcher.add(this.mIntent, this.mStartId);
        }
    }
    
    interface CommandsCompletedListener
    {
        void onAllCommandsCompleted();
    }
    
    static class DequeueAndCheckForCompletion implements Runnable
    {
        private final SystemAlarmDispatcher mDispatcher;
        
        DequeueAndCheckForCompletion(final SystemAlarmDispatcher mDispatcher) {
            this.mDispatcher = mDispatcher;
        }
        
        @Override
        public void run() {
            this.mDispatcher.dequeueAndCheckForCompletion();
        }
    }
}

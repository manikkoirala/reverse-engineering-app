// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.room.RoomDatabase;
import java.util.Iterator;
import java.util.List;
import android.os.Bundle;
import androidx.work.impl.StartStopToken;
import java.util.ArrayList;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.WorkDatabase;
import android.content.Intent;
import java.util.HashMap;
import androidx.work.Logger;
import androidx.work.impl.StartStopTokens;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.Map;
import android.content.Context;
import androidx.work.impl.ExecutionListener;

public class CommandHandler implements ExecutionListener
{
    static final String ACTION_CONSTRAINTS_CHANGED = "ACTION_CONSTRAINTS_CHANGED";
    static final String ACTION_DELAY_MET = "ACTION_DELAY_MET";
    static final String ACTION_EXECUTION_COMPLETED = "ACTION_EXECUTION_COMPLETED";
    static final String ACTION_RESCHEDULE = "ACTION_RESCHEDULE";
    static final String ACTION_SCHEDULE_WORK = "ACTION_SCHEDULE_WORK";
    static final String ACTION_STOP_WORK = "ACTION_STOP_WORK";
    private static final String KEY_NEEDS_RESCHEDULE = "KEY_NEEDS_RESCHEDULE";
    private static final String KEY_WORKSPEC_GENERATION = "KEY_WORKSPEC_GENERATION";
    private static final String KEY_WORKSPEC_ID = "KEY_WORKSPEC_ID";
    private static final String TAG;
    static final long WORK_PROCESSING_TIME_IN_MS = 600000L;
    private final Context mContext;
    private final Object mLock;
    private final Map<WorkGenerationalId, DelayMetCommandHandler> mPendingDelayMet;
    private final StartStopTokens mStartStopTokens;
    
    static {
        TAG = Logger.tagWithPrefix("CommandHandler");
    }
    
    CommandHandler(final Context mContext, final StartStopTokens mStartStopTokens) {
        this.mContext = mContext;
        this.mStartStopTokens = mStartStopTokens;
        this.mPendingDelayMet = new HashMap<WorkGenerationalId, DelayMetCommandHandler>();
        this.mLock = new Object();
    }
    
    static Intent createConstraintsChangedIntent(final Context context) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }
    
    static Intent createDelayMetIntent(final Context context, final WorkGenerationalId workGenerationalId) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        return writeWorkGenerationalId(intent, workGenerationalId);
    }
    
    static Intent createExecutionCompletedIntent(final Context context, final WorkGenerationalId workGenerationalId, final boolean b) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_NEEDS_RESCHEDULE", b);
        return writeWorkGenerationalId(intent, workGenerationalId);
    }
    
    static Intent createRescheduleIntent(final Context context) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }
    
    static Intent createScheduleWorkIntent(final Context context, final WorkGenerationalId workGenerationalId) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        return writeWorkGenerationalId(intent, workGenerationalId);
    }
    
    static Intent createStopWorkIntent(final Context context, final WorkGenerationalId workGenerationalId) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        return writeWorkGenerationalId(intent, workGenerationalId);
    }
    
    static Intent createStopWorkIntent(final Context context, final String s) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    private void handleConstraintsChanged(final Intent obj, final int n, final SystemAlarmDispatcher systemAlarmDispatcher) {
        final Logger value = Logger.get();
        final String tag = CommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling constraints changed ");
        sb.append(obj);
        value.debug(tag, sb.toString());
        new ConstraintsCommandHandler(this.mContext, n, systemAlarmDispatcher).handleConstraintsChanged();
    }
    
    private void handleDelayMet(final Intent intent, final int n, final SystemAlarmDispatcher systemAlarmDispatcher) {
        synchronized (this.mLock) {
            final WorkGenerationalId workGenerationalId = readWorkGenerationalId(intent);
            final Logger value = Logger.get();
            final String tag = CommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Handing delay met for ");
            sb.append(workGenerationalId);
            value.debug(tag, sb.toString());
            if (!this.mPendingDelayMet.containsKey(workGenerationalId)) {
                final DelayMetCommandHandler delayMetCommandHandler = new DelayMetCommandHandler(this.mContext, n, systemAlarmDispatcher, this.mStartStopTokens.tokenFor(workGenerationalId));
                this.mPendingDelayMet.put(workGenerationalId, delayMetCommandHandler);
                delayMetCommandHandler.handleProcessWork();
            }
            else {
                final Logger value2 = Logger.get();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("WorkSpec ");
                sb2.append(workGenerationalId);
                sb2.append(" is is already being handled for ACTION_DELAY_MET");
                value2.debug(tag, sb2.toString());
            }
        }
    }
    
    private void handleExecutionCompleted(final Intent obj, final int i) {
        final WorkGenerationalId workGenerationalId = readWorkGenerationalId(obj);
        final boolean boolean1 = obj.getExtras().getBoolean("KEY_NEEDS_RESCHEDULE");
        final Logger value = Logger.get();
        final String tag = CommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling onExecutionCompleted ");
        sb.append(obj);
        sb.append(", ");
        sb.append(i);
        value.debug(tag, sb.toString());
        this.onExecuted(workGenerationalId, boolean1);
    }
    
    private void handleReschedule(final Intent obj, final int i, final SystemAlarmDispatcher systemAlarmDispatcher) {
        final Logger value = Logger.get();
        final String tag = CommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling reschedule ");
        sb.append(obj);
        sb.append(", ");
        sb.append(i);
        value.debug(tag, sb.toString());
        systemAlarmDispatcher.getWorkManager().rescheduleEligibleWork();
    }
    
    private void handleScheduleWorkIntent(Intent workDatabase, final int n, final SystemAlarmDispatcher systemAlarmDispatcher) {
        final WorkGenerationalId workGenerationalId = readWorkGenerationalId(workDatabase);
        final Logger value = Logger.get();
        final String tag = CommandHandler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling schedule work for ");
        sb.append(workGenerationalId);
        value.debug(tag, sb.toString());
        workDatabase = (Intent)systemAlarmDispatcher.getWorkManager().getWorkDatabase();
        ((RoomDatabase)workDatabase).beginTransaction();
        try {
            final WorkSpec workSpec = ((WorkDatabase)workDatabase).workSpecDao().getWorkSpec(workGenerationalId.getWorkSpecId());
            if (workSpec == null) {
                final Logger value2 = Logger.get();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Skipping scheduling ");
                sb2.append(workGenerationalId);
                sb2.append(" because it's no longer in the DB");
                value2.warning(tag, sb2.toString());
                return;
            }
            if (workSpec.state.isFinished()) {
                final Logger value3 = Logger.get();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Skipping scheduling ");
                sb3.append(workGenerationalId);
                sb3.append("because it is finished.");
                value3.warning(tag, sb3.toString());
                return;
            }
            final long calculateNextRunTime = workSpec.calculateNextRunTime();
            if (!workSpec.hasConstraints()) {
                final Logger value4 = Logger.get();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Setting up Alarms for ");
                sb4.append(workGenerationalId);
                sb4.append("at ");
                sb4.append(calculateNextRunTime);
                value4.debug(tag, sb4.toString());
                Alarms.setAlarm(this.mContext, (WorkDatabase)workDatabase, workGenerationalId, calculateNextRunTime);
            }
            else {
                final Logger value5 = Logger.get();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Opportunistically setting an alarm for ");
                sb5.append(workGenerationalId);
                sb5.append("at ");
                sb5.append(calculateNextRunTime);
                value5.debug(tag, sb5.toString());
                Alarms.setAlarm(this.mContext, (WorkDatabase)workDatabase, workGenerationalId, calculateNextRunTime);
                systemAlarmDispatcher.getTaskExecutor().getMainThreadExecutor().execute(new SystemAlarmDispatcher.AddRunnable(systemAlarmDispatcher, createConstraintsChangedIntent(this.mContext), n));
            }
            ((RoomDatabase)workDatabase).setTransactionSuccessful();
        }
        finally {
            ((RoomDatabase)workDatabase).endTransaction();
        }
    }
    
    private void handleStopWork(final Intent intent, final SystemAlarmDispatcher systemAlarmDispatcher) {
        final Bundle extras = intent.getExtras();
        final String string = extras.getString("KEY_WORKSPEC_ID");
        List<StartStopToken> remove2;
        if (extras.containsKey("KEY_WORKSPEC_GENERATION")) {
            final int int1 = extras.getInt("KEY_WORKSPEC_GENERATION");
            final ArrayList list = new ArrayList(1);
            final StartStopToken remove = this.mStartStopTokens.remove(new WorkGenerationalId(string, int1));
            remove2 = list;
            if (remove != null) {
                list.add(remove);
                remove2 = list;
            }
        }
        else {
            remove2 = this.mStartStopTokens.remove(string);
        }
        for (final StartStopToken startStopToken : remove2) {
            final Logger value = Logger.get();
            final String tag = CommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Handing stopWork work for ");
            sb.append(string);
            value.debug(tag, sb.toString());
            systemAlarmDispatcher.getWorkManager().stopWork(startStopToken);
            Alarms.cancelAlarm(this.mContext, systemAlarmDispatcher.getWorkManager().getWorkDatabase(), startStopToken.getId());
            systemAlarmDispatcher.onExecuted(startStopToken.getId(), false);
        }
    }
    
    private static boolean hasKeys(final Bundle bundle, final String... array) {
        if (bundle != null && !bundle.isEmpty()) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (bundle.get(array[i]) == null) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    static WorkGenerationalId readWorkGenerationalId(final Intent intent) {
        return new WorkGenerationalId(intent.getStringExtra("KEY_WORKSPEC_ID"), intent.getIntExtra("KEY_WORKSPEC_GENERATION", 0));
    }
    
    private static Intent writeWorkGenerationalId(final Intent intent, final WorkGenerationalId workGenerationalId) {
        intent.putExtra("KEY_WORKSPEC_ID", workGenerationalId.getWorkSpecId());
        intent.putExtra("KEY_WORKSPEC_GENERATION", workGenerationalId.getGeneration());
        return intent;
    }
    
    boolean hasPendingCommands() {
        synchronized (this.mLock) {
            return !this.mPendingDelayMet.isEmpty();
        }
    }
    
    @Override
    public void onExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        synchronized (this.mLock) {
            final DelayMetCommandHandler delayMetCommandHandler = this.mPendingDelayMet.remove(workGenerationalId);
            this.mStartStopTokens.remove(workGenerationalId);
            if (delayMetCommandHandler != null) {
                delayMetCommandHandler.onExecuted(b);
            }
        }
    }
    
    void onHandleIntent(final Intent obj, final int n, final SystemAlarmDispatcher systemAlarmDispatcher) {
        final String action = obj.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            this.handleConstraintsChanged(obj, n, systemAlarmDispatcher);
        }
        else if ("ACTION_RESCHEDULE".equals(action)) {
            this.handleReschedule(obj, n, systemAlarmDispatcher);
        }
        else if (!hasKeys(obj.getExtras(), "KEY_WORKSPEC_ID")) {
            final Logger value = Logger.get();
            final String tag = CommandHandler.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid request for ");
            sb.append(action);
            sb.append(" , requires ");
            sb.append("KEY_WORKSPEC_ID");
            sb.append(" .");
            value.error(tag, sb.toString());
        }
        else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            this.handleScheduleWorkIntent(obj, n, systemAlarmDispatcher);
        }
        else if ("ACTION_DELAY_MET".equals(action)) {
            this.handleDelayMet(obj, n, systemAlarmDispatcher);
        }
        else if ("ACTION_STOP_WORK".equals(action)) {
            this.handleStopWork(obj, systemAlarmDispatcher);
        }
        else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            this.handleExecutionCompleted(obj, n);
        }
        else {
            final Logger value2 = Logger.get();
            final String tag2 = CommandHandler.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring intent ");
            sb2.append(obj);
            value2.warning(tag2, sb2.toString());
        }
    }
}

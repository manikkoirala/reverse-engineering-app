// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.utils.PackageManagerHelper;
import android.content.BroadcastReceiver$PendingResult;
import androidx.work.impl.WorkManagerImpl;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import androidx.work.Logger;
import android.content.BroadcastReceiver;

public class ConstraintProxyUpdateReceiver extends BroadcastReceiver
{
    static final String ACTION = "androidx.work.impl.background.systemalarm.UpdateProxies";
    static final String KEY_BATTERY_CHARGING_PROXY_ENABLED = "KEY_BATTERY_CHARGING_PROXY_ENABLED";
    static final String KEY_BATTERY_NOT_LOW_PROXY_ENABLED = "KEY_BATTERY_NOT_LOW_PROXY_ENABLED";
    static final String KEY_NETWORK_STATE_PROXY_ENABLED = "KEY_NETWORK_STATE_PROXY_ENABLED";
    static final String KEY_STORAGE_NOT_LOW_PROXY_ENABLED = "KEY_STORAGE_NOT_LOW_PROXY_ENABLED";
    static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("ConstrntProxyUpdtRecvr");
    }
    
    public static Intent newConstraintProxyUpdateIntent(final Context context, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        final Intent intent = new Intent("androidx.work.impl.background.systemalarm.UpdateProxies");
        intent.setComponent(new ComponentName(context, (Class)ConstraintProxyUpdateReceiver.class));
        intent.putExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", b).putExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", b2).putExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", b3).putExtra("KEY_NETWORK_STATE_PROXY_ENABLED", b4);
        return intent;
    }
    
    public void onReceive(final Context context, final Intent intent) {
        String action;
        if (intent != null) {
            action = intent.getAction();
        }
        else {
            action = null;
        }
        if (!"androidx.work.impl.background.systemalarm.UpdateProxies".equals(action)) {
            final Logger value = Logger.get();
            final String tag = ConstraintProxyUpdateReceiver.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring unknown action ");
            sb.append(action);
            value.debug(tag, sb.toString());
        }
        else {
            WorkManagerImpl.getInstance(context).getWorkTaskExecutor().executeOnTaskThread(new Runnable(this, intent, context, this.goAsync()) {
                final ConstraintProxyUpdateReceiver this$0;
                final Context val$context;
                final Intent val$intent;
                final BroadcastReceiver$PendingResult val$pendingResult;
                
                @Override
                public void run() {
                    try {
                        final boolean booleanExtra = this.val$intent.getBooleanExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", false);
                        final boolean booleanExtra2 = this.val$intent.getBooleanExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", false);
                        final boolean booleanExtra3 = this.val$intent.getBooleanExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", false);
                        final boolean booleanExtra4 = this.val$intent.getBooleanExtra("KEY_NETWORK_STATE_PROXY_ENABLED", false);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Updating proxies: (BatteryNotLowProxy (");
                        sb.append(booleanExtra);
                        sb.append("), BatteryChargingProxy (");
                        sb.append(booleanExtra2);
                        sb.append("), StorageNotLowProxy (");
                        sb.append(booleanExtra3);
                        sb.append("), NetworkStateProxy (");
                        sb.append(booleanExtra4);
                        sb.append("), ");
                        Logger.get().debug(ConstraintProxyUpdateReceiver.TAG, sb.toString());
                        PackageManagerHelper.setComponentEnabled(this.val$context, ConstraintProxy.BatteryNotLowProxy.class, booleanExtra);
                        PackageManagerHelper.setComponentEnabled(this.val$context, ConstraintProxy.BatteryChargingProxy.class, booleanExtra2);
                        PackageManagerHelper.setComponentEnabled(this.val$context, ConstraintProxy.StorageNotLowProxy.class, booleanExtra3);
                        PackageManagerHelper.setComponentEnabled(this.val$context, ConstraintProxy.NetworkStateProxy.class, booleanExtra4);
                    }
                    finally {
                        this.val$pendingResult.finish();
                    }
                }
            });
        }
    }
}

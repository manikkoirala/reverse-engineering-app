// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import android.net.Network;
import android.net.Uri;
import androidx.work.impl.StartStopToken;
import java.util.Arrays;
import androidx.work.WorkerParameters;
import android.os.Build$VERSION;
import android.app.Application;
import android.os.PersistableBundle;
import java.util.HashMap;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.StartStopTokens;
import android.app.job.JobParameters;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.Map;
import androidx.work.impl.ExecutionListener;
import android.app.job.JobService;

public class SystemJobService extends JobService implements ExecutionListener
{
    private static final String TAG;
    private final Map<WorkGenerationalId, JobParameters> mJobParameters;
    private final StartStopTokens mStartStopTokens;
    private WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobService");
    }
    
    public SystemJobService() {
        this.mJobParameters = new HashMap<WorkGenerationalId, JobParameters>();
        this.mStartStopTokens = new StartStopTokens();
    }
    
    private static WorkGenerationalId workGenerationalIdFromJobParameters(final JobParameters jobParameters) {
        try {
            final PersistableBundle extras = jobParameters.getExtras();
            if (extras != null && extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return new WorkGenerationalId(extras.getString("EXTRA_WORK_SPEC_ID"), extras.getInt("EXTRA_WORK_SPEC_GENERATION"));
            }
            return null;
        }
        catch (final NullPointerException ex) {
            return null;
        }
    }
    
    public void onCreate() {
        super.onCreate();
        try {
            final WorkManagerImpl instance = WorkManagerImpl.getInstance(this.getApplicationContext());
            this.mWorkManagerImpl = instance;
            instance.getProcessor().addExecutionListener(this);
        }
        catch (final IllegalStateException ex) {
            if (!Application.class.equals(this.getApplication().getClass())) {
                throw new IllegalStateException("WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().");
            }
            Logger.get().warning(SystemJobService.TAG, "Could not find WorkManager instance; this may be because an auto-backup is in progress. Ignoring JobScheduler commands for now. Please make sure that you are initializing WorkManager if you have manually disabled WorkManagerInitializer.");
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        final WorkManagerImpl mWorkManagerImpl = this.mWorkManagerImpl;
        if (mWorkManagerImpl != null) {
            mWorkManagerImpl.getProcessor().removeExecutionListener(this);
        }
    }
    
    public void onExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        final Logger value = Logger.get();
        final String tag = SystemJobService.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append(workGenerationalId.getWorkSpecId());
        sb.append(" executed on JobScheduler");
        value.debug(tag, sb.toString());
        synchronized (this.mJobParameters) {
            final JobParameters jobParameters = this.mJobParameters.remove(workGenerationalId);
            monitorexit(this.mJobParameters);
            this.mStartStopTokens.remove(workGenerationalId);
            if (jobParameters != null) {
                this.jobFinished(jobParameters, b);
            }
        }
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        if (this.mWorkManagerImpl == null) {
            Logger.get().debug(SystemJobService.TAG, "WorkManager is not initialized; requesting retry.");
            this.jobFinished(jobParameters, true);
            return false;
        }
        final WorkGenerationalId workGenerationalIdFromJobParameters = workGenerationalIdFromJobParameters(jobParameters);
        if (workGenerationalIdFromJobParameters == null) {
            Logger.get().error(SystemJobService.TAG, "WorkSpec id not found!");
            return false;
        }
        Object mJobParameters = this.mJobParameters;
        synchronized (mJobParameters) {
            if (this.mJobParameters.containsKey(workGenerationalIdFromJobParameters)) {
                final Logger value = Logger.get();
                final String tag = SystemJobService.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Job is already being executed by SystemJobService: ");
                sb.append(workGenerationalIdFromJobParameters);
                value.debug(tag, sb.toString());
                return false;
            }
            final Logger value2 = Logger.get();
            final String tag2 = SystemJobService.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("onStartJob for ");
            sb2.append(workGenerationalIdFromJobParameters);
            value2.debug(tag2, sb2.toString());
            this.mJobParameters.put(workGenerationalIdFromJobParameters, jobParameters);
            monitorexit(mJobParameters);
            mJobParameters = null;
            if (Build$VERSION.SDK_INT >= 24) {
                final WorkerParameters.RuntimeExtras runtimeExtras = new WorkerParameters.RuntimeExtras();
                if (Api24Impl.getTriggeredContentUris(jobParameters) != null) {
                    runtimeExtras.triggeredContentUris = Arrays.asList(Api24Impl.getTriggeredContentUris(jobParameters));
                }
                if (Api24Impl.getTriggeredContentAuthorities(jobParameters) != null) {
                    runtimeExtras.triggeredContentAuthorities = Arrays.asList(Api24Impl.getTriggeredContentAuthorities(jobParameters));
                }
                mJobParameters = runtimeExtras;
                if (Build$VERSION.SDK_INT >= 28) {
                    runtimeExtras.network = Api28Impl.getNetwork(jobParameters);
                    mJobParameters = runtimeExtras;
                }
            }
            this.mWorkManagerImpl.startWork(this.mStartStopTokens.tokenFor(workGenerationalIdFromJobParameters), (WorkerParameters.RuntimeExtras)mJobParameters);
            return true;
        }
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        if (this.mWorkManagerImpl == null) {
            Logger.get().debug(SystemJobService.TAG, "WorkManager is not initialized; requesting retry.");
            return true;
        }
        final WorkGenerationalId workGenerationalIdFromJobParameters = workGenerationalIdFromJobParameters(jobParameters);
        if (workGenerationalIdFromJobParameters == null) {
            Logger.get().error(SystemJobService.TAG, "WorkSpec id not found!");
            return false;
        }
        final Logger value = Logger.get();
        final String tag = SystemJobService.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("onStopJob for ");
        sb.append(workGenerationalIdFromJobParameters);
        value.debug(tag, sb.toString());
        Object o = this.mJobParameters;
        synchronized (o) {
            this.mJobParameters.remove(workGenerationalIdFromJobParameters);
            monitorexit(o);
            o = this.mStartStopTokens.remove(workGenerationalIdFromJobParameters);
            if (o != null) {
                this.mWorkManagerImpl.stopWork((StartStopToken)o);
            }
            return this.mWorkManagerImpl.getProcessor().isCancelled(workGenerationalIdFromJobParameters.getWorkSpecId()) ^ true;
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static String[] getTriggeredContentAuthorities(final JobParameters jobParameters) {
            return jobParameters.getTriggeredContentAuthorities();
        }
        
        static Uri[] getTriggeredContentUris(final JobParameters jobParameters) {
            return jobParameters.getTriggeredContentUris();
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static Network getNetwork(final JobParameters jobParameters) {
            return jobParameters.getNetwork();
        }
    }
}

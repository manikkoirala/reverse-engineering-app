// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import androidx.core.util.Consumer;
import androidx.work.impl.WorkDatabase;
import androidx.room.RoomDatabase;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.WorkSpecDao;
import java.util.HashSet;
import android.os.PersistableBundle;
import android.content.ComponentName;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Iterator;
import java.util.List;
import android.app.job.JobInfo;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import android.app.job.JobScheduler;
import android.content.Context;
import androidx.work.impl.Scheduler;

public class SystemJobScheduler implements Scheduler
{
    private static final String TAG;
    private final Context mContext;
    private final JobScheduler mJobScheduler;
    private final SystemJobInfoConverter mSystemJobInfoConverter;
    private final WorkManagerImpl mWorkManager;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobScheduler");
    }
    
    public SystemJobScheduler(final Context context, final WorkManagerImpl workManagerImpl) {
        this(context, workManagerImpl, (JobScheduler)context.getSystemService("jobscheduler"), new SystemJobInfoConverter(context));
    }
    
    public SystemJobScheduler(final Context mContext, final WorkManagerImpl mWorkManager, final JobScheduler mJobScheduler, final SystemJobInfoConverter mSystemJobInfoConverter) {
        this.mContext = mContext;
        this.mWorkManager = mWorkManager;
        this.mJobScheduler = mJobScheduler;
        this.mSystemJobInfoConverter = mSystemJobInfoConverter;
    }
    
    public static void cancelAll(final Context context) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
            if (pendingJobs != null && !pendingJobs.isEmpty()) {
                final Iterator iterator = pendingJobs.iterator();
                while (iterator.hasNext()) {
                    cancelJobById(jobScheduler, ((JobInfo)iterator.next()).getId());
                }
            }
        }
    }
    
    private static void cancelJobById(final JobScheduler jobScheduler, final int i) {
        try {
            jobScheduler.cancel(i);
        }
        finally {
            final Throwable t;
            Logger.get().error(SystemJobScheduler.TAG, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", i), t);
        }
    }
    
    private static List<Integer> getPendingJobIds(final Context context, final JobScheduler jobScheduler, final String s) {
        final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
        if (pendingJobs == null) {
            return null;
        }
        final ArrayList list = new ArrayList(2);
        for (final JobInfo jobInfo : pendingJobs) {
            final WorkGenerationalId workGenerationalIdFromJobInfo = getWorkGenerationalIdFromJobInfo(jobInfo);
            if (workGenerationalIdFromJobInfo != null && s.equals(workGenerationalIdFromJobInfo.getWorkSpecId())) {
                list.add(jobInfo.getId());
            }
        }
        return list;
    }
    
    private static List<JobInfo> getPendingJobs(final Context context, final JobScheduler jobScheduler) {
        List list = null;
        try {
            jobScheduler.getAllPendingJobs();
        }
        finally {
            final Throwable t;
            Logger.get().error(SystemJobScheduler.TAG, "getAllPendingJobs() is not reliable on this device.", t);
            list = null;
        }
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final ComponentName componentName = new ComponentName(context, (Class)SystemJobService.class);
        for (final JobInfo jobInfo : list) {
            if (componentName.equals((Object)jobInfo.getService())) {
                list2.add((Object)jobInfo);
            }
        }
        return (List<JobInfo>)list2;
    }
    
    private static WorkGenerationalId getWorkGenerationalIdFromJobInfo(final JobInfo jobInfo) {
        final PersistableBundle extras = jobInfo.getExtras();
        Label_0043: {
            if (extras == null) {
                break Label_0043;
            }
            try {
                if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                    return new WorkGenerationalId(extras.getString("EXTRA_WORK_SPEC_ID"), extras.getInt("EXTRA_WORK_SPEC_GENERATION", 0));
                }
                return null;
            }
            catch (final NullPointerException ex) {
                return null;
            }
        }
    }
    
    public static boolean reconcileJobs(final Context context, WorkManagerImpl workDatabase) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
        final List<String> workSpecIds = workDatabase.getWorkDatabase().systemIdInfoDao().getWorkSpecIds();
        final boolean b = false;
        int size;
        if (pendingJobs != null) {
            size = pendingJobs.size();
        }
        else {
            size = 0;
        }
        final HashSet set = new HashSet(size);
        if (pendingJobs != null && !pendingJobs.isEmpty()) {
            for (final JobInfo jobInfo : pendingJobs) {
                final WorkGenerationalId workGenerationalIdFromJobInfo = getWorkGenerationalIdFromJobInfo(jobInfo);
                if (workGenerationalIdFromJobInfo != null) {
                    set.add((Object)workGenerationalIdFromJobInfo.getWorkSpecId());
                }
                else {
                    cancelJobById(jobScheduler, jobInfo.getId());
                }
            }
        }
        final Iterator<String> iterator2 = workSpecIds.iterator();
        while (true) {
            do {
                final boolean b2 = b;
                if (iterator2.hasNext()) {
                    continue;
                }
                if (b2) {
                    workDatabase = (WorkManagerImpl)workDatabase.getWorkDatabase();
                    ((RoomDatabase)workDatabase).beginTransaction();
                    try {
                        final WorkSpecDao workSpecDao = ((WorkDatabase)workDatabase).workSpecDao();
                        final Iterator<String> iterator3 = workSpecIds.iterator();
                        while (iterator3.hasNext()) {
                            workSpecDao.markWorkSpecScheduled(iterator3.next(), -1L);
                        }
                        ((RoomDatabase)workDatabase).setTransactionSuccessful();
                    }
                    finally {
                        ((RoomDatabase)workDatabase).endTransaction();
                    }
                }
                return b2;
            } while (set.contains(iterator2.next()));
            Logger.get().debug(SystemJobScheduler.TAG, "Reconciling jobs");
            final boolean b2 = true;
            continue;
        }
    }
    
    @Override
    public void cancel(final String s) {
        final List<Integer> pendingJobIds = getPendingJobIds(this.mContext, this.mJobScheduler, s);
        if (pendingJobIds != null && !pendingJobIds.isEmpty()) {
            final Iterator iterator = pendingJobIds.iterator();
            while (iterator.hasNext()) {
                cancelJobById(this.mJobScheduler, (int)iterator.next());
            }
            this.mWorkManager.getWorkDatabase().systemIdInfoDao().removeSystemIdInfo(s);
        }
    }
    
    @Override
    public boolean hasLimitedSchedulingSlots() {
        return true;
    }
    
    @Override
    public void schedule(final WorkSpec... p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //     4: invokevirtual   androidx/work/impl/WorkManagerImpl.getWorkDatabase:()Landroidx/work/impl/WorkDatabase;
        //     7: astore          5
        //     9: new             Landroidx/work/impl/utils/IdGenerator;
        //    12: dup            
        //    13: aload           5
        //    15: invokespecial   androidx/work/impl/utils/IdGenerator.<init>:(Landroidx/work/impl/WorkDatabase;)V
        //    18: astore          6
        //    20: aload_1        
        //    21: arraylength    
        //    22: istore          4
        //    24: iconst_0       
        //    25: istore_2       
        //    26: iload_2        
        //    27: iload           4
        //    29: if_icmpge       455
        //    32: aload_1        
        //    33: iload_2        
        //    34: aaload         
        //    35: astore          7
        //    37: aload           5
        //    39: invokevirtual   androidx/work/impl/WorkDatabase.beginTransaction:()V
        //    42: aload           5
        //    44: invokevirtual   androidx/work/impl/WorkDatabase.workSpecDao:()Landroidx/work/impl/model/WorkSpecDao;
        //    47: aload           7
        //    49: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //    52: invokeinterface androidx/work/impl/model/WorkSpecDao.getWorkSpec:(Ljava/lang/String;)Landroidx/work/impl/model/WorkSpec;
        //    57: astore          8
        //    59: aload           8
        //    61: ifnonnull       138
        //    64: invokestatic    androidx/work/Logger.get:()Landroidx/work/Logger;
        //    67: astore          10
        //    69: getstatic       androidx/work/impl/background/systemjob/SystemJobScheduler.TAG:Ljava/lang/String;
        //    72: astore          8
        //    74: new             Ljava/lang/StringBuilder;
        //    77: astore          9
        //    79: aload           9
        //    81: invokespecial   java/lang/StringBuilder.<init>:()V
        //    84: aload           9
        //    86: ldc_w           "Skipping scheduling "
        //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    92: pop            
        //    93: aload           9
        //    95: aload           7
        //    97: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   100: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   103: pop            
        //   104: aload           9
        //   106: ldc_w           " because it's no longer in the DB"
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: pop            
        //   113: aload           10
        //   115: aload           8
        //   117: aload           9
        //   119: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   122: invokevirtual   androidx/work/Logger.warning:(Ljava/lang/String;Ljava/lang/String;)V
        //   125: aload           5
        //   127: invokevirtual   androidx/work/impl/WorkDatabase.setTransactionSuccessful:()V
        //   130: aload           5
        //   132: invokevirtual   androidx/work/impl/WorkDatabase.endTransaction:()V
        //   135: goto            441
        //   138: aload           8
        //   140: getfield        androidx/work/impl/model/WorkSpec.state:Landroidx/work/WorkInfo$State;
        //   143: getstatic       androidx/work/WorkInfo$State.ENQUEUED:Landroidx/work/WorkInfo$State;
        //   146: if_acmpeq       218
        //   149: invokestatic    androidx/work/Logger.get:()Landroidx/work/Logger;
        //   152: astore          8
        //   154: getstatic       androidx/work/impl/background/systemjob/SystemJobScheduler.TAG:Ljava/lang/String;
        //   157: astore          10
        //   159: new             Ljava/lang/StringBuilder;
        //   162: astore          9
        //   164: aload           9
        //   166: invokespecial   java/lang/StringBuilder.<init>:()V
        //   169: aload           9
        //   171: ldc_w           "Skipping scheduling "
        //   174: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   177: pop            
        //   178: aload           9
        //   180: aload           7
        //   182: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   185: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   188: pop            
        //   189: aload           9
        //   191: ldc_w           " because it is no longer enqueued"
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: pop            
        //   198: aload           8
        //   200: aload           10
        //   202: aload           9
        //   204: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   207: invokevirtual   androidx/work/Logger.warning:(Ljava/lang/String;Ljava/lang/String;)V
        //   210: aload           5
        //   212: invokevirtual   androidx/work/impl/WorkDatabase.setTransactionSuccessful:()V
        //   215: goto            130
        //   218: aload           7
        //   220: invokestatic    androidx/work/impl/model/WorkSpecKt.generationalId:(Landroidx/work/impl/model/WorkSpec;)Landroidx/work/impl/model/WorkGenerationalId;
        //   223: astore          8
        //   225: aload           5
        //   227: invokevirtual   androidx/work/impl/WorkDatabase.systemIdInfoDao:()Landroidx/work/impl/model/SystemIdInfoDao;
        //   230: aload           8
        //   232: invokeinterface androidx/work/impl/model/SystemIdInfoDao.getSystemIdInfo:(Landroidx/work/impl/model/WorkGenerationalId;)Landroidx/work/impl/model/SystemIdInfo;
        //   237: astore          9
        //   239: aload           9
        //   241: ifnull          253
        //   244: aload           9
        //   246: getfield        androidx/work/impl/model/SystemIdInfo.systemId:I
        //   249: istore_3       
        //   250: goto            279
        //   253: aload           6
        //   255: aload_0        
        //   256: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   259: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   262: invokevirtual   androidx/work/Configuration.getMinJobSchedulerId:()I
        //   265: aload_0        
        //   266: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   269: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   272: invokevirtual   androidx/work/Configuration.getMaxJobSchedulerId:()I
        //   275: invokevirtual   androidx/work/impl/utils/IdGenerator.nextJobSchedulerIdWithRange:(II)I
        //   278: istore_3       
        //   279: aload           9
        //   281: ifnonnull       309
        //   284: aload           8
        //   286: iload_3        
        //   287: invokestatic    androidx/work/impl/model/SystemIdInfoKt.systemIdInfo:(Landroidx/work/impl/model/WorkGenerationalId;I)Landroidx/work/impl/model/SystemIdInfo;
        //   290: astore          8
        //   292: aload_0        
        //   293: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   296: invokevirtual   androidx/work/impl/WorkManagerImpl.getWorkDatabase:()Landroidx/work/impl/WorkDatabase;
        //   299: invokevirtual   androidx/work/impl/WorkDatabase.systemIdInfoDao:()Landroidx/work/impl/model/SystemIdInfoDao;
        //   302: aload           8
        //   304: invokeinterface androidx/work/impl/model/SystemIdInfoDao.insertSystemIdInfo:(Landroidx/work/impl/model/SystemIdInfo;)V
        //   309: aload_0        
        //   310: aload           7
        //   312: iload_3        
        //   313: invokevirtual   androidx/work/impl/background/systemjob/SystemJobScheduler.scheduleInternal:(Landroidx/work/impl/model/WorkSpec;I)V
        //   316: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   319: bipush          23
        //   321: if_icmpne       433
        //   324: aload_0        
        //   325: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mContext:Landroid/content/Context;
        //   328: aload_0        
        //   329: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mJobScheduler:Landroid/app/job/JobScheduler;
        //   332: aload           7
        //   334: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   337: invokestatic    androidx/work/impl/background/systemjob/SystemJobScheduler.getPendingJobIds:(Landroid/content/Context;Landroid/app/job/JobScheduler;Ljava/lang/String;)Ljava/util/List;
        //   340: astore          8
        //   342: aload           8
        //   344: ifnull          433
        //   347: aload           8
        //   349: iload_3        
        //   350: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   353: invokeinterface java/util/List.indexOf:(Ljava/lang/Object;)I
        //   358: istore_3       
        //   359: iload_3        
        //   360: iflt            372
        //   363: aload           8
        //   365: iload_3        
        //   366: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
        //   371: pop            
        //   372: aload           8
        //   374: invokeinterface java/util/List.isEmpty:()Z
        //   379: ifne            400
        //   382: aload           8
        //   384: iconst_0       
        //   385: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   390: checkcast       Ljava/lang/Integer;
        //   393: invokevirtual   java/lang/Integer.intValue:()I
        //   396: istore_3       
        //   397: goto            426
        //   400: aload           6
        //   402: aload_0        
        //   403: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   406: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   409: invokevirtual   androidx/work/Configuration.getMinJobSchedulerId:()I
        //   412: aload_0        
        //   413: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   416: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   419: invokevirtual   androidx/work/Configuration.getMaxJobSchedulerId:()I
        //   422: invokevirtual   androidx/work/impl/utils/IdGenerator.nextJobSchedulerIdWithRange:(II)I
        //   425: istore_3       
        //   426: aload_0        
        //   427: aload           7
        //   429: iload_3        
        //   430: invokevirtual   androidx/work/impl/background/systemjob/SystemJobScheduler.scheduleInternal:(Landroidx/work/impl/model/WorkSpec;I)V
        //   433: aload           5
        //   435: invokevirtual   androidx/work/impl/WorkDatabase.setTransactionSuccessful:()V
        //   438: goto            130
        //   441: iinc            2, 1
        //   444: goto            26
        //   447: astore_1       
        //   448: aload           5
        //   450: invokevirtual   androidx/work/impl/WorkDatabase.endTransaction:()V
        //   453: aload_1        
        //   454: athrow         
        //   455: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  42     59     447    455    Any
        //  64     130    447    455    Any
        //  138    215    447    455    Any
        //  218    239    447    455    Any
        //  244    250    447    455    Any
        //  253    279    447    455    Any
        //  284    309    447    455    Any
        //  309    342    447    455    Any
        //  347    359    447    455    Any
        //  363    372    447    455    Any
        //  372    397    447    455    Any
        //  400    426    447    455    Any
        //  426    433    447    455    Any
        //  433    438    447    455    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void scheduleInternal(WorkSpec schedulingExceptionHandler, int size) {
        final JobInfo convert = this.mSystemJobInfoConverter.convert((WorkSpec)schedulingExceptionHandler, size);
        final Logger value = Logger.get();
        final String tag = SystemJobScheduler.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Scheduling work ID ");
        sb.append(((WorkSpec)schedulingExceptionHandler).id);
        sb.append("Job ID ");
        sb.append(size);
        value.debug(tag, sb.toString());
        try {
            if (this.mJobScheduler.schedule(convert) == 0) {
                final Logger value2 = Logger.get();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to schedule work ID ");
                sb2.append(((WorkSpec)schedulingExceptionHandler).id);
                value2.warning(tag, sb2.toString());
                if (((WorkSpec)schedulingExceptionHandler).expedited && ((WorkSpec)schedulingExceptionHandler).outOfQuotaPolicy == OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST) {
                    ((WorkSpec)schedulingExceptionHandler).expedited = false;
                    Logger.get().debug(tag, String.format("Scheduling a non-expedited job (work ID %s)", ((WorkSpec)schedulingExceptionHandler).id));
                    this.scheduleInternal((WorkSpec)schedulingExceptionHandler, size);
                }
            }
        }
        catch (IllegalStateException schedulingExceptionHandler) {
            final List<JobInfo> pendingJobs = getPendingJobs(this.mContext, this.mJobScheduler);
            if (pendingJobs != null) {
                size = pendingJobs.size();
            }
            else {
                size = 0;
            }
            final String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", size, this.mWorkManager.getWorkDatabase().workSpecDao().getScheduledWork().size(), this.mWorkManager.getConfiguration().getMaxSchedulerLimit());
            Logger.get().error(SystemJobScheduler.TAG, format);
            final IllegalStateException ex = new IllegalStateException(format, schedulingExceptionHandler);
            schedulingExceptionHandler = (IllegalStateException)this.mWorkManager.getConfiguration().getSchedulingExceptionHandler();
            if (schedulingExceptionHandler == null) {
                throw;
            }
            ((Consumer<IllegalStateException>)schedulingExceptionHandler).accept(ex);
        }
        finally {
            final Logger value3 = Logger.get();
            final String tag2 = SystemJobScheduler.TAG;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to schedule ");
            sb3.append(schedulingExceptionHandler);
            final Throwable t;
            value3.error(tag2, sb3.toString(), t);
        }
    }
}

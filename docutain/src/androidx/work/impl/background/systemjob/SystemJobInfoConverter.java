// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import java.util.Iterator;
import androidx.work.BackoffPolicy;
import android.os.PersistableBundle;
import android.app.job.JobInfo;
import androidx.work.impl.model.WorkSpec;
import android.net.NetworkRequest$Builder;
import android.app.job.JobInfo$Builder;
import android.os.Build$VERSION;
import androidx.work.NetworkType;
import android.app.job.JobInfo$TriggerContentUri;
import androidx.work.Constraints;
import android.content.Context;
import androidx.work.Logger;
import android.content.ComponentName;

class SystemJobInfoConverter
{
    static final String EXTRA_IS_PERIODIC = "EXTRA_IS_PERIODIC";
    static final String EXTRA_WORK_SPEC_GENERATION = "EXTRA_WORK_SPEC_GENERATION";
    static final String EXTRA_WORK_SPEC_ID = "EXTRA_WORK_SPEC_ID";
    private static final String TAG;
    private final ComponentName mWorkServiceComponent;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobInfoConverter");
    }
    
    SystemJobInfoConverter(final Context context) {
        this.mWorkServiceComponent = new ComponentName(context.getApplicationContext(), (Class)SystemJobService.class);
    }
    
    private static JobInfo$TriggerContentUri convertContentUriTrigger(final Constraints.ContentUriTrigger contentUriTrigger) {
        return new JobInfo$TriggerContentUri(contentUriTrigger.getUri(), (int)(contentUriTrigger.isTriggeredForDescendants() ? 1 : 0));
    }
    
    static int convertNetworkType(final NetworkType obj) {
        final int n = SystemJobInfoConverter$1.$SwitchMap$androidx$work$NetworkType[obj.ordinal()];
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        }
        if (n != 3) {
            if (n != 4) {
                if (n == 5) {
                    if (Build$VERSION.SDK_INT >= 26) {
                        return 4;
                    }
                }
            }
            else if (Build$VERSION.SDK_INT >= 24) {
                return 3;
            }
            final Logger value = Logger.get();
            final String tag = SystemJobInfoConverter.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("API version too low. Cannot convert network type value ");
            sb.append(obj);
            value.debug(tag, sb.toString());
            return 1;
        }
        return 2;
    }
    
    static void setRequiredNetwork(final JobInfo$Builder jobInfo$Builder, final NetworkType networkType) {
        if (Build$VERSION.SDK_INT >= 30 && networkType == NetworkType.TEMPORARILY_UNMETERED) {
            jobInfo$Builder.setRequiredNetwork(new NetworkRequest$Builder().addCapability(25).build());
        }
        else {
            jobInfo$Builder.setRequiredNetworkType(convertNetworkType(networkType));
        }
    }
    
    JobInfo convert(final WorkSpec workSpec, int n) {
        final Constraints constraints = workSpec.constraints;
        final PersistableBundle extras = new PersistableBundle();
        extras.putString("EXTRA_WORK_SPEC_ID", workSpec.id);
        extras.putInt("EXTRA_WORK_SPEC_GENERATION", workSpec.getGeneration());
        extras.putBoolean("EXTRA_IS_PERIODIC", workSpec.isPeriodic());
        final JobInfo$Builder setExtras = new JobInfo$Builder(n, this.mWorkServiceComponent).setRequiresCharging(constraints.requiresCharging()).setRequiresDeviceIdle(constraints.requiresDeviceIdle()).setExtras(extras);
        setRequiredNetwork(setExtras, constraints.getRequiredNetworkType());
        final boolean requiresDeviceIdle = constraints.requiresDeviceIdle();
        boolean b = false;
        if (!requiresDeviceIdle) {
            if (workSpec.backoffPolicy == BackoffPolicy.LINEAR) {
                n = 0;
            }
            else {
                n = 1;
            }
            setExtras.setBackoffCriteria(workSpec.backoffDelayDuration, n);
        }
        final long max = Math.max(workSpec.calculateNextRunTime() - System.currentTimeMillis(), 0L);
        if (Build$VERSION.SDK_INT <= 28) {
            setExtras.setMinimumLatency(max);
        }
        else if (max > 0L) {
            setExtras.setMinimumLatency(max);
        }
        else if (!workSpec.expedited) {
            setExtras.setImportantWhileForeground(true);
        }
        if (Build$VERSION.SDK_INT >= 24 && constraints.hasContentUriTriggers()) {
            final Iterator<Constraints.ContentUriTrigger> iterator = constraints.getContentUriTriggers().iterator();
            while (iterator.hasNext()) {
                setExtras.addTriggerContentUri(convertContentUriTrigger(iterator.next()));
            }
            setExtras.setTriggerContentUpdateDelay(constraints.getContentTriggerUpdateDelayMillis());
            setExtras.setTriggerContentMaxDelay(constraints.getContentTriggerMaxDelayMillis());
        }
        setExtras.setPersisted(false);
        if (Build$VERSION.SDK_INT >= 26) {
            setExtras.setRequiresBatteryNotLow(constraints.requiresBatteryNotLow());
            setExtras.setRequiresStorageNotLow(constraints.requiresStorageNotLow());
        }
        if (workSpec.runAttemptCount > 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (max > 0L) {
            b = true;
        }
        if (Build$VERSION.SDK_INT >= 31 && workSpec.expedited && n == 0 && !b) {
            setExtras.setExpedited(true);
        }
        return setExtras.build();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.model.WorkNameDao;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.WorkDatabase;
import java.util.List;
import androidx.work.Logger;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.ListenableWorker;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.WorkerParameters;
import android.content.Context;
import kotlin.Metadata;
import androidx.work.Worker;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t" }, d2 = { "Landroidx/work/impl/workers/DiagnosticsWorker;", "Landroidx/work/Worker;", "context", "Landroid/content/Context;", "parameters", "Landroidx/work/WorkerParameters;", "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "doWork", "Landroidx/work/ListenableWorker$Result;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class DiagnosticsWorker extends Worker
{
    public DiagnosticsWorker(final Context context, final WorkerParameters workerParameters) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)workerParameters, "parameters");
        super(context, workerParameters);
    }
    
    @Override
    public Result doWork() {
        final WorkManagerImpl instance = WorkManagerImpl.getInstance(this.getApplicationContext());
        Intrinsics.checkNotNullExpressionValue((Object)instance, "getInstance(applicationContext)");
        final WorkDatabase workDatabase = instance.getWorkDatabase();
        Intrinsics.checkNotNullExpressionValue((Object)workDatabase, "workManager.workDatabase");
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final WorkNameDao workNameDao = workDatabase.workNameDao();
        final WorkTagDao workTagDao = workDatabase.workTagDao();
        final SystemIdInfoDao systemIdInfoDao = workDatabase.systemIdInfoDao();
        final List<WorkSpec> recentlyCompletedWork = workSpecDao.getRecentlyCompletedWork(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1L));
        final List<WorkSpec> runningWork = workSpecDao.getRunningWork();
        final List<WorkSpec> allEligibleWorkSpecsForScheduling = workSpecDao.getAllEligibleWorkSpecsForScheduling(200);
        if (recentlyCompletedWork.isEmpty() ^ true) {
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), "Recently completed work:\n\n");
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), DiagnosticsWorkerKt.access$workSpecRows(workNameDao, workTagDao, systemIdInfoDao, recentlyCompletedWork));
        }
        if (runningWork.isEmpty() ^ true) {
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), "Running work:\n\n");
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), DiagnosticsWorkerKt.access$workSpecRows(workNameDao, workTagDao, systemIdInfoDao, runningWork));
        }
        if (allEligibleWorkSpecsForScheduling.isEmpty() ^ true) {
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), "Enqueued work:\n\n");
            Logger.get().info(DiagnosticsWorkerKt.access$getTAG$p(), DiagnosticsWorkerKt.access$workSpecRows(workNameDao, workTagDao, systemIdInfoDao, allEligibleWorkSpecsForScheduling));
        }
        final Result success = Result.success();
        Intrinsics.checkNotNullExpressionValue((Object)success, "success()");
        return success;
    }
}

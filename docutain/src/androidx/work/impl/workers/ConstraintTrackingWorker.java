// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import java.util.List;
import kotlin.Unit;
import androidx.work.impl.constraints.trackers.Trackers;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkSpec;
import kotlin.collections.CollectionsKt;
import androidx.work.impl.constraints.WorkConstraintsTrackerImpl;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.Logger;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.WorkerParameters;
import androidx.work.impl.utils.futures.SettableFuture;
import kotlin.Metadata;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.work.ListenableWorker;

@Metadata(d1 = { "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0016\u0010\u0014\u001a\u00020\u00152\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u0016\u0010\u0019\u001a\u00020\u00152\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\b\u0010\u001a\u001a\u00020\u0015H\u0016J\b\u0010\u001b\u001a\u00020\u0015H\u0002J\u000e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00100\u001dH\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u0004\u0018\u00010\u00012\b\u0010\n\u001a\u0004\u0018\u00010\u00018G@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR2\u0010\u000e\u001a&\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010 \u0011*\u0012\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010\u0018\u00010\u000f0\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Landroidx/work/impl/workers/ConstraintTrackingWorker;", "Landroidx/work/ListenableWorker;", "Landroidx/work/impl/constraints/WorkConstraintsCallback;", "appContext", "Landroid/content/Context;", "workerParameters", "Landroidx/work/WorkerParameters;", "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "areConstraintsUnmet", "", "<set-?>", "delegate", "getDelegate", "()Landroidx/work/ListenableWorker;", "future", "Landroidx/work/impl/utils/futures/SettableFuture;", "Landroidx/work/ListenableWorker$Result;", "kotlin.jvm.PlatformType", "lock", "", "onAllConstraintsMet", "", "workSpecs", "", "Landroidx/work/impl/model/WorkSpec;", "onAllConstraintsNotMet", "onStopped", "setupAndRunConstraintTrackingWork", "startWork", "Lcom/google/common/util/concurrent/ListenableFuture;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class ConstraintTrackingWorker extends ListenableWorker implements WorkConstraintsCallback
{
    private volatile boolean areConstraintsUnmet;
    private ListenableWorker delegate;
    private final SettableFuture<Result> future;
    private final Object lock;
    private final WorkerParameters workerParameters;
    
    public ConstraintTrackingWorker(final Context context, final WorkerParameters workerParameters) {
        Intrinsics.checkNotNullParameter((Object)context, "appContext");
        Intrinsics.checkNotNullParameter((Object)workerParameters, "workerParameters");
        super(context, workerParameters);
        this.workerParameters = workerParameters;
        this.lock = new Object();
        this.future = SettableFuture.create();
    }
    
    private final void setupAndRunConstraintTrackingWork() {
        if (this.future.isCancelled()) {
            return;
        }
        final String string = this.getInputData().getString("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        final Logger value = Logger.get();
        Intrinsics.checkNotNullExpressionValue((Object)value, "get()");
        final CharSequence charSequence = string;
        if (charSequence == null || charSequence.length() == 0) {
            value.error(ConstraintTrackingWorkerKt.access$getTAG$p(), "No worker to delegate to.");
            final SettableFuture<Result> future = this.future;
            Intrinsics.checkNotNullExpressionValue((Object)future, "future");
            ConstraintTrackingWorkerKt.access$setFailed(future);
            return;
        }
        if ((this.delegate = this.getWorkerFactory().createWorkerWithDefaultFallback(this.getApplicationContext(), string, this.workerParameters)) == null) {
            value.debug(ConstraintTrackingWorkerKt.access$getTAG$p(), "No worker to delegate to.");
            final SettableFuture<Result> future2 = this.future;
            Intrinsics.checkNotNullExpressionValue((Object)future2, "future");
            ConstraintTrackingWorkerKt.access$setFailed(future2);
            return;
        }
        final WorkManagerImpl instance = WorkManagerImpl.getInstance(this.getApplicationContext());
        Intrinsics.checkNotNullExpressionValue((Object)instance, "getInstance(applicationContext)");
        final WorkSpecDao workSpecDao = instance.getWorkDatabase().workSpecDao();
        final String string2 = this.getId().toString();
        Intrinsics.checkNotNullExpressionValue((Object)string2, "id.toString()");
        final WorkSpec workSpec = workSpecDao.getWorkSpec(string2);
        if (workSpec == null) {
            final SettableFuture<Result> future3 = this.future;
            Intrinsics.checkNotNullExpressionValue((Object)future3, "future");
            ConstraintTrackingWorkerKt.access$setFailed(future3);
            return;
        }
        final Trackers trackers = instance.getTrackers();
        Intrinsics.checkNotNullExpressionValue((Object)trackers, "workManagerImpl.trackers");
        final WorkConstraintsTrackerImpl workConstraintsTrackerImpl = new WorkConstraintsTrackerImpl(trackers, this);
        workConstraintsTrackerImpl.replace(CollectionsKt.listOf((Object)workSpec));
        final String string3 = this.getId().toString();
        Intrinsics.checkNotNullExpressionValue((Object)string3, "id.toString()");
        if (workConstraintsTrackerImpl.areAllConstraintsMet(string3)) {
            final String access$getTAG$p = ConstraintTrackingWorkerKt.access$getTAG$p();
            final StringBuilder sb = new StringBuilder();
            sb.append("Constraints met for delegate ");
            sb.append(string);
            value.debug(access$getTAG$p, sb.toString());
            try {
                final ListenableWorker delegate = this.delegate;
                Intrinsics.checkNotNull((Object)delegate);
                final ListenableFuture<Result> startWork = delegate.startWork();
                Intrinsics.checkNotNullExpressionValue((Object)startWork, "delegate!!.startWork()");
                startWork.addListener((Runnable)new ConstraintTrackingWorker$$ExternalSyntheticLambda1(this, startWork), this.getBackgroundExecutor());
                return;
            }
            finally {
                final String access$getTAG$p2 = ConstraintTrackingWorkerKt.access$getTAG$p();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Delegated worker ");
                sb2.append(string);
                sb2.append(" threw exception in startWork.");
                final Throwable t;
                value.debug(access$getTAG$p2, sb2.toString(), t);
                synchronized (this.lock) {
                    if (this.areConstraintsUnmet) {
                        value.debug(ConstraintTrackingWorkerKt.access$getTAG$p(), "Constraints were unmet, Retrying.");
                        final SettableFuture<Result> future4 = this.future;
                        Intrinsics.checkNotNullExpressionValue((Object)future4, "future");
                        ConstraintTrackingWorkerKt.access$setRetry(future4);
                    }
                    else {
                        final SettableFuture<Result> future5 = this.future;
                        Intrinsics.checkNotNullExpressionValue((Object)future5, "future");
                        ConstraintTrackingWorkerKt.access$setFailed(future5);
                    }
                }
            }
        }
        final String access$getTAG$p3 = ConstraintTrackingWorkerKt.access$getTAG$p();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Constraints not met for delegate ");
        sb3.append(string);
        sb3.append(". Requesting retry.");
        value.debug(access$getTAG$p3, sb3.toString());
        final SettableFuture<Result> future6 = this.future;
        Intrinsics.checkNotNullExpressionValue((Object)future6, "future");
        ConstraintTrackingWorkerKt.access$setRetry(future6);
    }
    
    private static final void setupAndRunConstraintTrackingWork$lambda$2(final ConstraintTrackingWorker constraintTrackingWorker, final ListenableFuture future) {
        Intrinsics.checkNotNullParameter((Object)constraintTrackingWorker, "this$0");
        Intrinsics.checkNotNullParameter((Object)future, "$innerFuture");
        synchronized (constraintTrackingWorker.lock) {
            if (constraintTrackingWorker.areConstraintsUnmet) {
                final SettableFuture<Result> future2 = constraintTrackingWorker.future;
                Intrinsics.checkNotNullExpressionValue((Object)future2, "future");
                ConstraintTrackingWorkerKt.access$setRetry(future2);
            }
            else {
                constraintTrackingWorker.future.setFuture((com.google.common.util.concurrent.ListenableFuture<? extends Result>)future);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    private static final void startWork$lambda$0(final ConstraintTrackingWorker constraintTrackingWorker) {
        Intrinsics.checkNotNullParameter((Object)constraintTrackingWorker, "this$0");
        constraintTrackingWorker.setupAndRunConstraintTrackingWork();
    }
    
    public final ListenableWorker getDelegate() {
        return this.delegate;
    }
    
    @Override
    public void onAllConstraintsMet(final List<WorkSpec> list) {
        Intrinsics.checkNotNullParameter((Object)list, "workSpecs");
    }
    
    @Override
    public void onAllConstraintsNotMet(final List<WorkSpec> obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "workSpecs");
        final Logger value = Logger.get();
        final String access$getTAG$p = ConstraintTrackingWorkerKt.access$getTAG$p();
        final StringBuilder sb = new StringBuilder();
        sb.append("Constraints changed for ");
        sb.append(obj);
        value.debug(access$getTAG$p, sb.toString());
        synchronized (this.lock) {
            this.areConstraintsUnmet = true;
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Override
    public void onStopped() {
        super.onStopped();
        final ListenableWorker delegate = this.delegate;
        if (delegate != null && !delegate.isStopped()) {
            delegate.stop();
        }
    }
    
    @Override
    public ListenableFuture<Result> startWork() {
        this.getBackgroundExecutor().execute(new ConstraintTrackingWorker$$ExternalSyntheticLambda0(this));
        final SettableFuture<Result> future = this.future;
        Intrinsics.checkNotNullExpressionValue((Object)future, "future");
        return (ListenableFuture<Result>)future;
    }
}

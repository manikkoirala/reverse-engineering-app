// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import androidx.work.impl.model.SystemIdInfo;
import java.util.Iterator;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import androidx.work.impl.model.WorkSpecKt;
import android.os.Build$VERSION;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.model.WorkNameDao;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.Metadata;

@Metadata(d1 = { "\u00002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u001a/\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\u0001H\u0002¢\u0006\u0002\u0010\t\u001a.\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\u0012H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013" }, d2 = { "TAG", "", "workSpecRow", "workSpec", "Landroidx/work/impl/model/WorkSpec;", "name", "systemId", "", "tags", "(Landroidx/work/impl/model/WorkSpec;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;", "workSpecRows", "workNameDao", "Landroidx/work/impl/model/WorkNameDao;", "workTagDao", "Landroidx/work/impl/model/WorkTagDao;", "systemIdInfoDao", "Landroidx/work/impl/model/SystemIdInfoDao;", "workSpecs", "", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class DiagnosticsWorkerKt
{
    private static final String TAG;
    
    static {
        final String tagWithPrefix = Logger.tagWithPrefix("DiagnosticsWrkr");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"DiagnosticsWrkr\")");
        TAG = tagWithPrefix;
    }
    
    private static final String workSpecRow(final WorkSpec workSpec, final String str, final Integer obj, final String str2) {
        final StringBuilder sb = new StringBuilder();
        sb.append('\n');
        sb.append(workSpec.id);
        sb.append("\t ");
        sb.append(workSpec.workerClassName);
        sb.append("\t ");
        sb.append(obj);
        sb.append("\t ");
        sb.append(workSpec.state.name());
        sb.append("\t ");
        sb.append(str);
        sb.append("\t ");
        sb.append(str2);
        sb.append('\t');
        return sb.toString();
    }
    
    private static final String workSpecRows(final WorkNameDao workNameDao, final WorkTagDao workTagDao, final SystemIdInfoDao systemIdInfoDao, final List<WorkSpec> list) {
        final StringBuilder sb = new StringBuilder();
        String str;
        if (Build$VERSION.SDK_INT >= 23) {
            str = "Job Id";
        }
        else {
            str = "Alarm Id";
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("\n Id \t Class Name\t ");
        sb2.append(str);
        sb2.append("\t State\t Unique Name\t Tags\t");
        sb.append(sb2.toString());
        for (final WorkSpec workSpec : list) {
            final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(WorkSpecKt.generationalId(workSpec));
            Integer value;
            if (systemIdInfo != null) {
                value = systemIdInfo.systemId;
            }
            else {
                value = null;
            }
            sb.append(workSpecRow(workSpec, CollectionsKt.joinToString$default((Iterable)workNameDao.getNamesForWorkSpecId(workSpec.id), (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null), value, CollectionsKt.joinToString$default((Iterable)workTagDao.getTagsForWorkSpecId(workSpec.id), (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null)));
        }
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
        return string;
    }
}

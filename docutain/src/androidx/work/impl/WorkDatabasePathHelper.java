// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import kotlin.Pair;
import kotlin.TuplesKt;
import java.util.LinkedHashMap;
import kotlin.ranges.RangesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import java.util.Iterator;
import java.util.Map;
import androidx.work.Logger;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import java.io.File;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0000\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0003J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\f2\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\r" }, d2 = { "Landroidx/work/impl/WorkDatabasePathHelper;", "", "()V", "getDatabasePath", "Ljava/io/File;", "context", "Landroid/content/Context;", "getDefaultDatabasePath", "getNoBackupPath", "migrateDatabase", "", "migrationPaths", "", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class WorkDatabasePathHelper
{
    public static final WorkDatabasePathHelper INSTANCE;
    
    static {
        INSTANCE = new WorkDatabasePathHelper();
    }
    
    private WorkDatabasePathHelper() {
    }
    
    private final File getNoBackupPath(final Context context) {
        return new File(Api21Impl.INSTANCE.getNoBackupFilesDir(context), "androidx.work.workdb");
    }
    
    @JvmStatic
    public static final void migrateDatabase(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final WorkDatabasePathHelper instance = WorkDatabasePathHelper.INSTANCE;
        final File defaultDatabasePath = instance.getDefaultDatabasePath(context);
        if (Build$VERSION.SDK_INT >= 23 && defaultDatabasePath.exists()) {
            Logger.get().debug(WorkDatabasePathHelperKt.access$getTAG$p(), "Migrating WorkDatabase to the no-backup directory");
            for (final Map.Entry<File, V> entry : instance.migrationPaths(context).entrySet()) {
                final File file = entry.getKey();
                final File file2 = (File)entry.getValue();
                if (file.exists()) {
                    if (file2.exists()) {
                        final Logger value = Logger.get();
                        final String access$getTAG$p = WorkDatabasePathHelperKt.access$getTAG$p();
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Over-writing contents of ");
                        sb.append(file2);
                        value.warning(access$getTAG$p, sb.toString());
                    }
                    String s;
                    if (file.renameTo(file2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Migrated ");
                        sb2.append(file);
                        sb2.append("to ");
                        sb2.append(file2);
                        s = sb2.toString();
                    }
                    else {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Renaming ");
                        sb3.append(file);
                        sb3.append(" to ");
                        sb3.append(file2);
                        sb3.append(" failed");
                        s = sb3.toString();
                    }
                    Logger.get().debug(WorkDatabasePathHelperKt.access$getTAG$p(), s);
                }
            }
        }
    }
    
    public final File getDatabasePath(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        File file;
        if (Build$VERSION.SDK_INT < 23) {
            file = this.getDefaultDatabasePath(context);
        }
        else {
            file = this.getNoBackupPath(context);
        }
        return file;
    }
    
    public final File getDefaultDatabasePath(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final File databasePath = context.getDatabasePath("androidx.work.workdb");
        Intrinsics.checkNotNullExpressionValue((Object)databasePath, "context.getDatabasePath(WORK_DATABASE_NAME)");
        return databasePath;
    }
    
    public final Map<File, File> migrationPaths(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Map map2;
        if (Build$VERSION.SDK_INT >= 23) {
            final File defaultDatabasePath = this.getDefaultDatabasePath(context);
            final File databasePath = this.getDatabasePath(context);
            final String[] access$getDATABASE_EXTRA_FILES$p = WorkDatabasePathHelperKt.access$getDATABASE_EXTRA_FILES$p();
            final Map map = new LinkedHashMap(RangesKt.coerceAtLeast(MapsKt.mapCapacity(access$getDATABASE_EXTRA_FILES$p.length), 16));
            for (int i = 0; i < access$getDATABASE_EXTRA_FILES$p.length; ++i) {
                final String s = access$getDATABASE_EXTRA_FILES$p[i];
                final StringBuilder sb = new StringBuilder();
                sb.append(defaultDatabasePath.getPath());
                sb.append(s);
                final File file = new File(sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(databasePath.getPath());
                sb2.append(s);
                final Pair to = TuplesKt.to((Object)file, (Object)new File(sb2.toString()));
                map.put(to.getFirst(), to.getSecond());
            }
            map2 = MapsKt.plus(map, TuplesKt.to((Object)defaultDatabasePath, (Object)databasePath));
        }
        else {
            map2 = MapsKt.emptyMap();
        }
        return map2;
    }
}

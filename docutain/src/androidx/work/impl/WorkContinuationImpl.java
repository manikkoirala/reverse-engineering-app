// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.lifecycle.LiveData;
import androidx.work.impl.utils.StatusRunnable;
import androidx.work.WorkInfo;
import com.google.common.util.concurrent.ListenableFuture;
import android.text.TextUtils;
import androidx.work.impl.utils.EnqueueRunnable;
import java.util.Collections;
import androidx.work.InputMerger;
import androidx.work.ArrayCreatingInputMerger;
import androidx.work.ListenableWorker;
import androidx.work.impl.workers.CombineContinuationsWorker;
import androidx.work.OneTimeWorkRequest;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import androidx.work.Logger;
import androidx.work.WorkRequest;
import androidx.work.Operation;
import androidx.work.ExistingWorkPolicy;
import java.util.List;
import androidx.work.WorkContinuation;

public class WorkContinuationImpl extends WorkContinuation
{
    private static final String TAG;
    private final List<String> mAllIds;
    private boolean mEnqueued;
    private final ExistingWorkPolicy mExistingWorkPolicy;
    private final List<String> mIds;
    private final String mName;
    private Operation mOperation;
    private final List<WorkContinuationImpl> mParents;
    private final List<? extends WorkRequest> mWork;
    private final WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("WorkContinuationImpl");
    }
    
    public WorkContinuationImpl(final WorkManagerImpl workManagerImpl, final String s, final ExistingWorkPolicy existingWorkPolicy, final List<? extends WorkRequest> list) {
        this(workManagerImpl, s, existingWorkPolicy, list, null);
    }
    
    public WorkContinuationImpl(final WorkManagerImpl mWorkManagerImpl, final String mName, final ExistingWorkPolicy mExistingWorkPolicy, final List<? extends WorkRequest> mWork, final List<WorkContinuationImpl> mParents) {
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mName = mName;
        this.mExistingWorkPolicy = mExistingWorkPolicy;
        this.mWork = mWork;
        this.mParents = mParents;
        this.mIds = new ArrayList<String>(mWork.size());
        this.mAllIds = new ArrayList<String>();
        if (mParents != null) {
            final Iterator<WorkContinuationImpl> iterator = mParents.iterator();
            while (iterator.hasNext()) {
                this.mAllIds.addAll(iterator.next().mAllIds);
            }
        }
        for (int i = 0; i < mWork.size(); ++i) {
            final String stringId = mWork.get(i).getStringId();
            this.mIds.add(stringId);
            this.mAllIds.add(stringId);
        }
    }
    
    public WorkContinuationImpl(final WorkManagerImpl workManagerImpl, final List<? extends WorkRequest> list) {
        this(workManagerImpl, null, ExistingWorkPolicy.KEEP, list, null);
    }
    
    private static boolean hasCycles(final WorkContinuationImpl workContinuationImpl, final Set<String> set) {
        set.addAll(workContinuationImpl.getIds());
        final Set<String> prerequisites = prerequisitesFor(workContinuationImpl);
        final Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            if (prerequisites.contains(iterator.next())) {
                return true;
            }
        }
        final List<WorkContinuationImpl> parents = workContinuationImpl.getParents();
        if (parents != null && !parents.isEmpty()) {
            final Iterator iterator2 = parents.iterator();
            while (iterator2.hasNext()) {
                if (hasCycles((WorkContinuationImpl)iterator2.next(), set)) {
                    return true;
                }
            }
        }
        set.removeAll(workContinuationImpl.getIds());
        return false;
    }
    
    public static Set<String> prerequisitesFor(final WorkContinuationImpl workContinuationImpl) {
        final HashSet set = new HashSet();
        final List<WorkContinuationImpl> parents = workContinuationImpl.getParents();
        if (parents != null && !parents.isEmpty()) {
            final Iterator iterator = parents.iterator();
            while (iterator.hasNext()) {
                set.addAll(((WorkContinuationImpl)iterator.next()).getIds());
            }
        }
        return set;
    }
    
    @Override
    protected WorkContinuation combineInternal(final List<WorkContinuation> list) {
        final OneTimeWorkRequest o = ((WorkRequest.Builder<B, OneTimeWorkRequest>)new OneTimeWorkRequest.Builder(CombineContinuationsWorker.class).setInputMerger(ArrayCreatingInputMerger.class)).build();
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next());
        }
        return new WorkContinuationImpl(this.mWorkManagerImpl, null, ExistingWorkPolicy.KEEP, Collections.singletonList(o), list2);
    }
    
    @Override
    public Operation enqueue() {
        if (!this.mEnqueued) {
            final EnqueueRunnable enqueueRunnable = new EnqueueRunnable(this);
            this.mWorkManagerImpl.getWorkTaskExecutor().executeOnTaskThread(enqueueRunnable);
            this.mOperation = enqueueRunnable.getOperation();
        }
        else {
            final Logger value = Logger.get();
            final String tag = WorkContinuationImpl.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Already enqueued work ids (");
            sb.append(TextUtils.join((CharSequence)", ", (Iterable)this.mIds));
            sb.append(")");
            value.warning(tag, sb.toString());
        }
        return this.mOperation;
    }
    
    public List<String> getAllIds() {
        return this.mAllIds;
    }
    
    public ExistingWorkPolicy getExistingWorkPolicy() {
        return this.mExistingWorkPolicy;
    }
    
    public List<String> getIds() {
        return this.mIds;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public List<WorkContinuationImpl> getParents() {
        return this.mParents;
    }
    
    public List<? extends WorkRequest> getWork() {
        return this.mWork;
    }
    
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfos() {
        final StatusRunnable<List<WorkInfo>> forStringIds = StatusRunnable.forStringIds(this.mWorkManagerImpl, this.mAllIds);
        this.mWorkManagerImpl.getWorkTaskExecutor().executeOnTaskThread(forStringIds);
        return forStringIds.getFuture();
    }
    
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosLiveData() {
        return this.mWorkManagerImpl.getWorkInfosById(this.mAllIds);
    }
    
    public WorkManagerImpl getWorkManagerImpl() {
        return this.mWorkManagerImpl;
    }
    
    public boolean hasCycles() {
        return hasCycles(this, new HashSet<String>());
    }
    
    public boolean isEnqueued() {
        return this.mEnqueued;
    }
    
    public void markEnqueued() {
        this.mEnqueued = true;
    }
    
    @Override
    public WorkContinuation then(final List<OneTimeWorkRequest> list) {
        if (list.isEmpty()) {
            return this;
        }
        return new WorkContinuationImpl(this.mWorkManagerImpl, this.mName, ExistingWorkPolicy.KEEP, list, Collections.singletonList(this));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.Iterator;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.Configuration;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemalarm.SystemAlarmScheduler;
import androidx.work.impl.utils.PackageManagerHelper;
import androidx.work.impl.background.systemjob.SystemJobService;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import android.os.Build$VERSION;
import android.content.Context;
import androidx.work.Logger;

public class Schedulers
{
    public static final String GCM_SCHEDULER = "androidx.work.impl.background.gcm.GcmScheduler";
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("Schedulers");
    }
    
    private Schedulers() {
    }
    
    static Scheduler createBestAvailableBackgroundScheduler(final Context context, final WorkManagerImpl workManagerImpl) {
        Scheduler tryCreateGcmBasedScheduler;
        if (Build$VERSION.SDK_INT >= 23) {
            tryCreateGcmBasedScheduler = new SystemJobScheduler(context, workManagerImpl);
            PackageManagerHelper.setComponentEnabled(context, SystemJobService.class, true);
            Logger.get().debug(Schedulers.TAG, "Created SystemJobScheduler and enabled SystemJobService");
        }
        else if ((tryCreateGcmBasedScheduler = tryCreateGcmBasedScheduler(context)) == null) {
            tryCreateGcmBasedScheduler = new SystemAlarmScheduler(context);
            PackageManagerHelper.setComponentEnabled(context, SystemAlarmService.class, true);
            Logger.get().debug(Schedulers.TAG, "Created SystemAlarmScheduler");
        }
        return tryCreateGcmBasedScheduler;
    }
    
    public static void schedule(final Configuration configuration, WorkDatabase workDatabase, final List<Scheduler> list) {
        if (list != null) {
            if (list.size() != 0) {
                final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
                workDatabase.beginTransaction();
                try {
                    final List<WorkSpec> eligibleWorkForScheduling = workSpecDao.getEligibleWorkForScheduling(configuration.getMaxSchedulerLimit());
                    final List<WorkSpec> allEligibleWorkSpecsForScheduling = workSpecDao.getAllEligibleWorkSpecsForScheduling(200);
                    if (eligibleWorkForScheduling != null && eligibleWorkForScheduling.size() > 0) {
                        final long currentTimeMillis = System.currentTimeMillis();
                        final Iterator iterator = eligibleWorkForScheduling.iterator();
                        while (iterator.hasNext()) {
                            workSpecDao.markWorkSpecScheduled(((WorkSpec)iterator.next()).id, currentTimeMillis);
                        }
                    }
                    workDatabase.setTransactionSuccessful();
                    workDatabase.endTransaction();
                    if (eligibleWorkForScheduling != null && eligibleWorkForScheduling.size() > 0) {
                        workDatabase = (WorkDatabase)(Object)eligibleWorkForScheduling.toArray(new WorkSpec[eligibleWorkForScheduling.size()]);
                        for (final Scheduler scheduler : list) {
                            if (scheduler.hasLimitedSchedulingSlots()) {
                                scheduler.schedule((WorkSpec[])(Object)workDatabase);
                            }
                        }
                    }
                    if (allEligibleWorkSpecsForScheduling != null && allEligibleWorkSpecsForScheduling.size() > 0) {
                        final WorkSpec[] array = allEligibleWorkSpecsForScheduling.toArray(new WorkSpec[allEligibleWorkSpecsForScheduling.size()]);
                        final Iterator iterator3 = list.iterator();
                        while (iterator3.hasNext()) {
                            workDatabase = (WorkDatabase)iterator3.next();
                            if (!((Scheduler)workDatabase).hasLimitedSchedulingSlots()) {
                                ((Scheduler)workDatabase).schedule(array);
                            }
                        }
                    }
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        }
    }
    
    private static Scheduler tryCreateGcmBasedScheduler(final Context context) {
        try {
            final Scheduler scheduler = (Scheduler)Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            Logger.get().debug(Schedulers.TAG, "Created androidx.work.impl.background.gcm.GcmScheduler");
            return scheduler;
        }
        finally {
            final Throwable t;
            Logger.get().debug(Schedulers.TAG, "Unable to create GCM Scheduler", t);
            return null;
        }
    }
}

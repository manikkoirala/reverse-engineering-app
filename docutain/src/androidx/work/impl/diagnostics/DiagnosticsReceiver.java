// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.diagnostics;

import androidx.work.WorkRequest;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.impl.workers.DiagnosticsWorker;
import androidx.work.WorkManager;
import android.content.Intent;
import android.content.Context;
import androidx.work.Logger;
import android.content.BroadcastReceiver;

public class DiagnosticsReceiver extends BroadcastReceiver
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("DiagnosticsRcvr");
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        Logger.get().debug(DiagnosticsReceiver.TAG, "Requesting diagnostics");
        try {
            WorkManager.getInstance(context).enqueue(OneTimeWorkRequest.from(DiagnosticsWorker.class));
        }
        catch (final IllegalStateException ex) {
            Logger.get().error(DiagnosticsReceiver.TAG, "WorkManager is not initialized", ex);
        }
    }
}

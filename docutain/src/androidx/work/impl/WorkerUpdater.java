// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.utils.EnqueueUtilsKt;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Iterator;
import kotlin.jvm.functions.Function1;
import androidx.work.WorkManager;
import androidx.work.Configuration;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.WorkInfo;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Operation;
import androidx.work.impl.utils.futures.SettableFuture;
import java.util.Set;
import java.util.List;
import androidx.work.impl.model.WorkSpec;
import androidx.work.WorkRequest;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000V\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aD\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002\u001a\u001c\u0010\u0010\u001a\u00020\u0011*\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0007\u001a\u0014\u0010\u0016\u001a\u00020\u0017*\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000fH\u0002\u001a\u001a\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00010\u001a*\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0000¨\u0006\u001b" }, d2 = { "updateWorkImpl", "Landroidx/work/WorkManager$UpdateResult;", "processor", "Landroidx/work/impl/Processor;", "workDatabase", "Landroidx/work/impl/WorkDatabase;", "configuration", "Landroidx/work/Configuration;", "schedulers", "", "Landroidx/work/impl/Scheduler;", "newWorkSpec", "Landroidx/work/impl/model/WorkSpec;", "tags", "", "", "enqueueUniquelyNamedPeriodic", "Landroidx/work/Operation;", "Landroidx/work/impl/WorkManagerImpl;", "name", "workRequest", "Landroidx/work/WorkRequest;", "failWorkTypeChanged", "", "Landroidx/work/impl/OperationImpl;", "message", "Lcom/google/common/util/concurrent/ListenableFuture;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class WorkerUpdater
{
    public static final Operation enqueueUniquelyNamedPeriodic(final WorkManagerImpl workManagerImpl, final String s, final WorkRequest workRequest) {
        Intrinsics.checkNotNullParameter((Object)workManagerImpl, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "name");
        Intrinsics.checkNotNullParameter((Object)workRequest, "workRequest");
        final OperationImpl operationImpl = new OperationImpl();
        workManagerImpl.getWorkTaskExecutor().getSerialTaskExecutor().execute(new WorkerUpdater$$ExternalSyntheticLambda2(workManagerImpl, s, operationImpl, (Function0)new WorkerUpdater$enqueueUniquelyNamedPeriodic$enqueueNew.WorkerUpdater$enqueueUniquelyNamedPeriodic$enqueueNew$1(workRequest, workManagerImpl, s, operationImpl), workRequest));
        return operationImpl;
    }
    
    private static final void enqueueUniquelyNamedPeriodic$lambda$3(final WorkManagerImpl workManagerImpl, final String str, final OperationImpl operationImpl, final Function0 function0, final WorkRequest workRequest) {
        Intrinsics.checkNotNullParameter((Object)workManagerImpl, "$this_enqueueUniquelyNamedPeriodic");
        Intrinsics.checkNotNullParameter((Object)str, "$name");
        Intrinsics.checkNotNullParameter((Object)operationImpl, "$operation");
        Intrinsics.checkNotNullParameter((Object)function0, "$enqueueNew");
        Intrinsics.checkNotNullParameter((Object)workRequest, "$workRequest");
        final WorkSpecDao workSpecDao = workManagerImpl.getWorkDatabase().workSpecDao();
        final List<WorkSpec.IdAndState> workSpecIdAndStatesForName = workSpecDao.getWorkSpecIdAndStatesForName(str);
        if (workSpecIdAndStatesForName.size() > 1) {
            failWorkTypeChanged(operationImpl, "Can't apply UPDATE policy to the chains of work.");
            return;
        }
        final WorkSpec.IdAndState idAndState = (WorkSpec.IdAndState)CollectionsKt.firstOrNull((List)workSpecIdAndStatesForName);
        if (idAndState == null) {
            function0.invoke();
            return;
        }
        final WorkSpec workSpec = workSpecDao.getWorkSpec(idAndState.id);
        if (workSpec == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkSpec with ");
            sb.append(idAndState.id);
            sb.append(", that matches a name \"");
            sb.append(str);
            sb.append("\", wasn't found");
            operationImpl.markState((Operation.State)new Operation.State.FAILURE(new IllegalStateException(sb.toString())));
            return;
        }
        if (!workSpec.isPeriodic()) {
            failWorkTypeChanged(operationImpl, "Can't update OneTimeWorker to Periodic Worker. Update operation must preserve worker's type.");
            return;
        }
        if (idAndState.state == WorkInfo.State.CANCELLED) {
            workSpecDao.delete(idAndState.id);
            function0.invoke();
            return;
        }
        final WorkSpec copy$default = WorkSpec.copy$default(workRequest.getWorkSpec(), idAndState.id, null, null, null, null, null, 0L, 0L, 0L, null, 0, null, 0L, 0L, 0L, 0L, false, null, 0, 0, 1048574, null);
        try {
            final Processor processor = workManagerImpl.getProcessor();
            Intrinsics.checkNotNullExpressionValue((Object)processor, "processor");
            final WorkDatabase workDatabase = workManagerImpl.getWorkDatabase();
            Intrinsics.checkNotNullExpressionValue((Object)workDatabase, "workDatabase");
            final Configuration configuration = workManagerImpl.getConfiguration();
            Intrinsics.checkNotNullExpressionValue((Object)configuration, "configuration");
            final List<Scheduler> schedulers = workManagerImpl.getSchedulers();
            Intrinsics.checkNotNullExpressionValue((Object)schedulers, "schedulers");
            updateWorkImpl(processor, workDatabase, configuration, schedulers, copy$default, workRequest.getTags());
            operationImpl.markState((Operation.State)Operation.SUCCESS);
        }
        finally {
            final Throwable t;
            operationImpl.markState((Operation.State)new Operation.State.FAILURE(t));
        }
    }
    
    private static final void failWorkTypeChanged(final OperationImpl operationImpl, final String message) {
        operationImpl.markState((Operation.State)new Operation.State.FAILURE(new UnsupportedOperationException(message)));
    }
    
    private static final WorkManager.UpdateResult updateWorkImpl(final Processor processor, final WorkDatabase workDatabase, final Configuration configuration, final List<? extends Scheduler> list, final WorkSpec workSpec, final Set<String> set) {
        final String id = workSpec.id;
        final WorkSpec workSpec2 = workDatabase.workSpecDao().getWorkSpec(id);
        if (workSpec2 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Worker with ");
            sb.append(id);
            sb.append(" doesn't exist");
            throw new IllegalArgumentException(sb.toString());
        }
        if (workSpec2.state.isFinished()) {
            return WorkManager.UpdateResult.NOT_APPLIED;
        }
        if (!(workSpec2.isPeriodic() ^ workSpec.isPeriodic())) {
            final boolean enqueued = processor.isEnqueued(id);
            if (!enqueued) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((Scheduler)iterator.next()).cancel(id);
                }
            }
            workDatabase.runInTransaction(new WorkerUpdater$$ExternalSyntheticLambda0(workDatabase, workSpec, workSpec2, list, id, set, enqueued));
            if (!enqueued) {
                Schedulers.schedule(configuration, workDatabase, (List<Scheduler>)list);
            }
            WorkManager.UpdateResult updateResult;
            if (enqueued) {
                updateResult = WorkManager.UpdateResult.APPLIED_FOR_NEXT_RUN;
            }
            else {
                updateResult = WorkManager.UpdateResult.APPLIED_IMMEDIATELY;
            }
            return updateResult;
        }
        final Function1 function1 = (Function1)WorkerUpdater$updateWorkImpl$type.WorkerUpdater$updateWorkImpl$type$1.INSTANCE;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Can't update ");
        sb2.append((String)function1.invoke((Object)workSpec2));
        sb2.append(" Worker to ");
        sb2.append((String)function1.invoke((Object)workSpec));
        sb2.append(" Worker. Update operation must preserve worker's type.");
        throw new UnsupportedOperationException(sb2.toString());
    }
    
    public static final ListenableFuture<WorkManager.UpdateResult> updateWorkImpl(final WorkManagerImpl workManagerImpl, final WorkRequest workRequest) {
        Intrinsics.checkNotNullParameter((Object)workManagerImpl, "<this>");
        Intrinsics.checkNotNullParameter((Object)workRequest, "workRequest");
        final SettableFuture<Object> create = SettableFuture.create();
        workManagerImpl.getWorkTaskExecutor().getSerialTaskExecutor().execute(new WorkerUpdater$$ExternalSyntheticLambda1(create, workManagerImpl, workRequest));
        Intrinsics.checkNotNullExpressionValue((Object)create, "future");
        return (ListenableFuture<WorkManager.UpdateResult>)create;
    }
    
    private static final void updateWorkImpl$lambda$1(final WorkDatabase workDatabase, final WorkSpec workSpec, final WorkSpec workSpec2, final List list, final String s, final Set set, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)workDatabase, "$workDatabase");
        Intrinsics.checkNotNullParameter((Object)workSpec, "$newWorkSpec");
        Intrinsics.checkNotNullParameter((Object)workSpec2, "$oldWorkSpec");
        Intrinsics.checkNotNullParameter((Object)list, "$schedulers");
        Intrinsics.checkNotNullParameter((Object)s, "$workSpecId");
        Intrinsics.checkNotNullParameter((Object)set, "$tags");
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final WorkTagDao workTagDao = workDatabase.workTagDao();
        workSpecDao.updateWorkSpec(EnqueueUtilsKt.wrapInConstraintTrackingWorkerIfNeeded(list, WorkSpec.copy$default(workSpec, null, workSpec2.state, null, null, null, null, 0L, 0L, 0L, null, workSpec2.runAttemptCount, null, 0L, workSpec2.lastEnqueueTime, 0L, 0L, false, null, 0, workSpec2.getGeneration() + 1, 515069, null)));
        workTagDao.deleteByWorkSpecId(s);
        workTagDao.insertTags(s, set);
        if (!b) {
            workSpecDao.markWorkSpecScheduled(s, -1L);
            workDatabase.workProgressDao().delete(s);
        }
    }
    
    private static final void updateWorkImpl$lambda$2(final SettableFuture settableFuture, final WorkManagerImpl workManagerImpl, final WorkRequest workRequest) {
        Intrinsics.checkNotNullParameter((Object)workManagerImpl, "$this_updateWorkImpl");
        Intrinsics.checkNotNullParameter((Object)workRequest, "$workRequest");
        if (settableFuture.isCancelled()) {
            return;
        }
        try {
            final Processor processor = workManagerImpl.getProcessor();
            Intrinsics.checkNotNullExpressionValue((Object)processor, "processor");
            final WorkDatabase workDatabase = workManagerImpl.getWorkDatabase();
            Intrinsics.checkNotNullExpressionValue((Object)workDatabase, "workDatabase");
            final Configuration configuration = workManagerImpl.getConfiguration();
            Intrinsics.checkNotNullExpressionValue((Object)configuration, "configuration");
            final List<Scheduler> schedulers = workManagerImpl.getSchedulers();
            Intrinsics.checkNotNullExpressionValue((Object)schedulers, "schedulers");
            settableFuture.set(updateWorkImpl(processor, workDatabase, configuration, schedulers, workRequest.getWorkSpec(), workRequest.getTags()));
        }
        finally {
            final Throwable exception;
            settableFuture.setException(exception);
        }
    }
}

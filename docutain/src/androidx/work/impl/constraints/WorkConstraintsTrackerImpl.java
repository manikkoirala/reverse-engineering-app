// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import java.util.Iterator;
import kotlin.Unit;
import java.util.ArrayList;
import java.util.Collection;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.Logger;
import androidx.work.impl.constraints.controllers.NetworkMeteredController;
import androidx.work.impl.constraints.controllers.NetworkNotRoamingController;
import androidx.work.impl.constraints.controllers.NetworkUnmeteredController;
import androidx.work.impl.constraints.controllers.NetworkConnectedController;
import androidx.work.impl.constraints.controllers.StorageNotLowController;
import androidx.work.impl.constraints.controllers.BatteryNotLowController;
import androidx.work.impl.constraints.controllers.BatteryChargingController;
import androidx.work.impl.constraints.trackers.Trackers;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.work.impl.constraints.controllers.ConstraintController;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001c\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0019\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007B#\b\u0001\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0010\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\t¢\u0006\u0002\u0010\u000bJ\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0016\u0010\u0013\u001a\u00020\u00142\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0016\u0010\u0018\u001a\u00020\u00142\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0016\u0010\u0019\u001a\u00020\u00142\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u0014H\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0082\u0004¢\u0006\u0004\n\u0002\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c" }, d2 = { "Landroidx/work/impl/constraints/WorkConstraintsTrackerImpl;", "Landroidx/work/impl/constraints/WorkConstraintsTracker;", "Landroidx/work/impl/constraints/controllers/ConstraintController$OnConstraintUpdatedCallback;", "trackers", "Landroidx/work/impl/constraints/trackers/Trackers;", "callback", "Landroidx/work/impl/constraints/WorkConstraintsCallback;", "(Landroidx/work/impl/constraints/trackers/Trackers;Landroidx/work/impl/constraints/WorkConstraintsCallback;)V", "constraintControllers", "", "Landroidx/work/impl/constraints/controllers/ConstraintController;", "(Landroidx/work/impl/constraints/WorkConstraintsCallback;[Landroidx/work/impl/constraints/controllers/ConstraintController;)V", "[Landroidx/work/impl/constraints/controllers/ConstraintController;", "lock", "", "areAllConstraintsMet", "", "workSpecId", "", "onConstraintMet", "", "workSpecs", "", "Landroidx/work/impl/model/WorkSpec;", "onConstraintNotMet", "replace", "", "reset", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class WorkConstraintsTrackerImpl implements WorkConstraintsTracker, OnConstraintUpdatedCallback
{
    private final WorkConstraintsCallback callback;
    private final ConstraintController<?>[] constraintControllers;
    private final Object lock;
    
    public WorkConstraintsTrackerImpl(final WorkConstraintsCallback callback, final ConstraintController<?>[] constraintControllers) {
        Intrinsics.checkNotNullParameter((Object)constraintControllers, "constraintControllers");
        this.callback = callback;
        this.constraintControllers = constraintControllers;
        this.lock = new Object();
    }
    
    public WorkConstraintsTrackerImpl(final Trackers trackers, final WorkConstraintsCallback workConstraintsCallback) {
        Intrinsics.checkNotNullParameter((Object)trackers, "trackers");
        this(workConstraintsCallback, new ConstraintController[] { new BatteryChargingController(trackers.getBatteryChargingTracker()), new BatteryNotLowController(trackers.getBatteryNotLowTracker()), new StorageNotLowController(trackers.getStorageNotLowTracker()), new NetworkConnectedController(trackers.getNetworkStateTracker()), new NetworkUnmeteredController(trackers.getNetworkStateTracker()), new NetworkNotRoamingController(trackers.getNetworkStateTracker()), new NetworkMeteredController(trackers.getNetworkStateTracker()) });
    }
    
    public final boolean areAllConstraintsMet(final String str) {
        Intrinsics.checkNotNullParameter((Object)str, "workSpecId");
        synchronized (this.lock) {
            final ConstraintController<?>[] constraintControllers = this.constraintControllers;
            final int length = constraintControllers.length;
            boolean b = false;
            while (true) {
                for (final ConstraintController<?> constraintController : constraintControllers) {
                    if (constraintController.isWorkSpecConstrained(str)) {
                        if (constraintController != null) {
                            final Logger value = Logger.get();
                            final String access$getTAG$p = WorkConstraintsTrackerKt.access$getTAG$p();
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Work ");
                            sb.append(str);
                            sb.append(" constrained by ");
                            sb.append(constraintController.getClass().getSimpleName());
                            value.debug(access$getTAG$p, sb.toString());
                        }
                        if (constraintController == null) {
                            b = true;
                        }
                        return b;
                    }
                }
                ConstraintController<?> constraintController = null;
                continue;
            }
        }
    }
    
    @Override
    public void onConstraintMet(final List<WorkSpec> list) {
        Intrinsics.checkNotNullParameter((Object)list, "workSpecs");
        synchronized (this.lock) {
            final Iterable iterable = list;
            final Collection collection = new ArrayList();
            for (final Object next : iterable) {
                if (this.areAllConstraintsMet(((WorkSpec)next).id)) {
                    collection.add(next);
                }
            }
            final List list2 = (List)collection;
            for (final WorkSpec obj : list2) {
                final Logger value = Logger.get();
                final String access$getTAG$p = WorkConstraintsTrackerKt.access$getTAG$p();
                final StringBuilder sb = new StringBuilder();
                sb.append("Constraints met for ");
                sb.append(obj);
                value.debug(access$getTAG$p, sb.toString());
            }
            final WorkConstraintsCallback callback = this.callback;
            if (callback != null) {
                callback.onAllConstraintsMet(list2);
                final Unit instance = Unit.INSTANCE;
            }
        }
    }
    
    @Override
    public void onConstraintNotMet(final List<WorkSpec> list) {
        Intrinsics.checkNotNullParameter((Object)list, "workSpecs");
        synchronized (this.lock) {
            final WorkConstraintsCallback callback = this.callback;
            if (callback != null) {
                callback.onAllConstraintsNotMet(list);
                final Unit instance = Unit.INSTANCE;
            }
        }
    }
    
    @Override
    public void replace(final Iterable<WorkSpec> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "workSpecs");
        synchronized (this.lock) {
            final ConstraintController<?>[] constraintControllers = this.constraintControllers;
            final int length = constraintControllers.length;
            final int n = 0;
            for (int i = 0; i < length; ++i) {
                constraintControllers[i].setCallback(null);
            }
            final ConstraintController<?>[] constraintControllers2 = this.constraintControllers;
            for (int length2 = constraintControllers2.length, j = 0; j < length2; ++j) {
                constraintControllers2[j].replace(iterable);
            }
            final ConstraintController<?>[] constraintControllers3 = this.constraintControllers;
            for (int length3 = constraintControllers3.length, k = n; k < length3; ++k) {
                constraintControllers3[k].setCallback((OnConstraintUpdatedCallback)this);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Override
    public void reset() {
        synchronized (this.lock) {
            final ConstraintController<?>[] constraintControllers = this.constraintControllers;
            for (int i = 0; i < constraintControllers.length; ++i) {
                constraintControllers[i].reset();
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
}

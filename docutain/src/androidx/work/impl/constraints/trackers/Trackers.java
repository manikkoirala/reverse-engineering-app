// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import androidx.work.impl.constraints.NetworkState;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001BQ\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007\u0012\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\u000eR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0010R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010¨\u0006\u0015" }, d2 = { "Landroidx/work/impl/constraints/trackers/Trackers;", "", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "batteryChargingTracker", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "", "batteryNotLowTracker", "Landroidx/work/impl/constraints/trackers/BatteryNotLowTracker;", "networkStateTracker", "Landroidx/work/impl/constraints/NetworkState;", "storageNotLowTracker", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;Landroidx/work/impl/constraints/trackers/ConstraintTracker;Landroidx/work/impl/constraints/trackers/BatteryNotLowTracker;Landroidx/work/impl/constraints/trackers/ConstraintTracker;Landroidx/work/impl/constraints/trackers/ConstraintTracker;)V", "getBatteryChargingTracker", "()Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "getBatteryNotLowTracker", "()Landroidx/work/impl/constraints/trackers/BatteryNotLowTracker;", "getNetworkStateTracker", "getStorageNotLowTracker", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class Trackers
{
    private final ConstraintTracker<Boolean> batteryChargingTracker;
    private final BatteryNotLowTracker batteryNotLowTracker;
    private final ConstraintTracker<NetworkState> networkStateTracker;
    private final ConstraintTracker<Boolean> storageNotLowTracker;
    
    public Trackers(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        this(context, taskExecutor, null, null, null, null, 60, null);
    }
    
    public Trackers(final Context context, final TaskExecutor taskExecutor, final ConstraintTracker<Boolean> constraintTracker) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "batteryChargingTracker");
        this(context, taskExecutor, constraintTracker, null, null, null, 56, null);
    }
    
    public Trackers(final Context context, final TaskExecutor taskExecutor, final ConstraintTracker<Boolean> constraintTracker, final BatteryNotLowTracker batteryNotLowTracker) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "batteryChargingTracker");
        Intrinsics.checkNotNullParameter((Object)batteryNotLowTracker, "batteryNotLowTracker");
        this(context, taskExecutor, constraintTracker, batteryNotLowTracker, null, null, 48, null);
    }
    
    public Trackers(final Context context, final TaskExecutor taskExecutor, final ConstraintTracker<Boolean> constraintTracker, final BatteryNotLowTracker batteryNotLowTracker, final ConstraintTracker<NetworkState> constraintTracker2) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "batteryChargingTracker");
        Intrinsics.checkNotNullParameter((Object)batteryNotLowTracker, "batteryNotLowTracker");
        Intrinsics.checkNotNullParameter((Object)constraintTracker2, "networkStateTracker");
        this(context, taskExecutor, constraintTracker, batteryNotLowTracker, constraintTracker2, null, 32, null);
    }
    
    public Trackers(final Context context, final TaskExecutor taskExecutor, final ConstraintTracker<Boolean> batteryChargingTracker, final BatteryNotLowTracker batteryNotLowTracker, final ConstraintTracker<NetworkState> networkStateTracker, final ConstraintTracker<Boolean> storageNotLowTracker) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        Intrinsics.checkNotNullParameter((Object)batteryChargingTracker, "batteryChargingTracker");
        Intrinsics.checkNotNullParameter((Object)batteryNotLowTracker, "batteryNotLowTracker");
        Intrinsics.checkNotNullParameter((Object)networkStateTracker, "networkStateTracker");
        Intrinsics.checkNotNullParameter((Object)storageNotLowTracker, "storageNotLowTracker");
        this.batteryChargingTracker = batteryChargingTracker;
        this.batteryNotLowTracker = batteryNotLowTracker;
        this.networkStateTracker = networkStateTracker;
        this.storageNotLowTracker = storageNotLowTracker;
    }
    
    public final ConstraintTracker<Boolean> getBatteryChargingTracker() {
        return this.batteryChargingTracker;
    }
    
    public final BatteryNotLowTracker getBatteryNotLowTracker() {
        return this.batteryNotLowTracker;
    }
    
    public final ConstraintTracker<NetworkState> getNetworkStateTracker() {
        return this.networkStateTracker;
    }
    
    public final ConstraintTracker<Boolean> getStorageNotLowTracker() {
        return this.storageNotLowTracker;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.work.Logger;
import android.content.Intent;
import android.content.IntentFilter;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import android.content.BroadcastReceiver;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b'\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H&J\b\u0010\u0012\u001a\u00020\u000fH\u0016J\b\u0010\u0013\u001a\u00020\u000fH\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00020\u000bX¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\r¨\u0006\u0014" }, d2 = { "Landroidx/work/impl/constraints/trackers/BroadcastReceiverConstraintTracker;", "T", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "broadcastReceiver", "Landroid/content/BroadcastReceiver;", "intentFilter", "Landroid/content/IntentFilter;", "getIntentFilter", "()Landroid/content/IntentFilter;", "onBroadcastReceive", "", "intent", "Landroid/content/Intent;", "startTracking", "stopTracking", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public abstract class BroadcastReceiverConstraintTracker<T> extends ConstraintTracker<T>
{
    private final BroadcastReceiver broadcastReceiver;
    
    public BroadcastReceiverConstraintTracker(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        super(context, taskExecutor);
        this.broadcastReceiver = (BroadcastReceiver)new BroadcastReceiverConstraintTracker$broadcastReceiver.BroadcastReceiverConstraintTracker$broadcastReceiver$1(this);
    }
    
    public abstract IntentFilter getIntentFilter();
    
    public abstract void onBroadcastReceive(final Intent p0);
    
    @Override
    public void startTracking() {
        final Logger value = Logger.get();
        final String access$getTAG$p = BroadcastReceiverConstraintTrackerKt.access$getTAG$p();
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(": registering receiver");
        value.debug(access$getTAG$p, sb.toString());
        this.getAppContext().registerReceiver(this.broadcastReceiver, this.getIntentFilter());
    }
    
    @Override
    public void stopTracking() {
        final Logger value = Logger.get();
        final String access$getTAG$p = BroadcastReceiverConstraintTrackerKt.access$getTAG$p();
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(": unregistering receiver");
        value.debug(access$getTAG$p, sb.toString());
        this.getAppContext().unregisterReceiver(this.broadcastReceiver);
    }
}

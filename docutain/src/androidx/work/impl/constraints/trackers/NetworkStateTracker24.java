// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.work.impl.utils.NetworkApi21;
import androidx.work.impl.utils.NetworkApi24;
import android.net.ConnectivityManager$NetworkCallback;
import androidx.work.Logger;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import android.net.ConnectivityManager;
import kotlin.Metadata;
import androidx.work.impl.constraints.NetworkState;

@Metadata(d1 = { "\u00003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002*\u0001\u000e\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u0011H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0010\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u000f¨\u0006\u0013" }, d2 = { "Landroidx/work/impl/constraints/trackers/NetworkStateTracker24;", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "Landroidx/work/impl/constraints/NetworkState;", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "connectivityManager", "Landroid/net/ConnectivityManager;", "initialState", "getInitialState", "()Landroidx/work/impl/constraints/NetworkState;", "networkCallback", "androidx/work/impl/constraints/trackers/NetworkStateTracker24$networkCallback$1", "Landroidx/work/impl/constraints/trackers/NetworkStateTracker24$networkCallback$1;", "startTracking", "", "stopTracking", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkStateTracker24 extends ConstraintTracker<NetworkState>
{
    private final ConnectivityManager connectivityManager;
    private final NetworkStateTracker24$networkCallback.NetworkStateTracker24$networkCallback$1 networkCallback;
    
    public NetworkStateTracker24(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        super(context, taskExecutor);
        final Object systemService = this.getAppContext().getSystemService("connectivity");
        Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.net.ConnectivityManager");
        this.connectivityManager = (ConnectivityManager)systemService;
        this.networkCallback = new NetworkStateTracker24$networkCallback.NetworkStateTracker24$networkCallback$1(this);
    }
    
    @Override
    public NetworkState getInitialState() {
        return NetworkStateTrackerKt.getActiveNetworkState(this.connectivityManager);
    }
    
    @Override
    public void startTracking() {
        try {
            Logger.get().debug(NetworkStateTrackerKt.access$getTAG$p(), "Registering network callback");
            NetworkApi24.registerDefaultNetworkCallbackCompat(this.connectivityManager, (ConnectivityManager$NetworkCallback)this.networkCallback);
        }
        catch (final SecurityException ex) {
            Logger.get().error(NetworkStateTrackerKt.access$getTAG$p(), "Received exception while registering network callback", ex);
        }
        catch (final IllegalArgumentException ex2) {
            Logger.get().error(NetworkStateTrackerKt.access$getTAG$p(), "Received exception while registering network callback", ex2);
        }
    }
    
    @Override
    public void stopTracking() {
        try {
            Logger.get().debug(NetworkStateTrackerKt.access$getTAG$p(), "Unregistering network callback");
            NetworkApi21.unregisterNetworkCallbackCompat(this.connectivityManager, (ConnectivityManager$NetworkCallback)this.networkCallback);
        }
        catch (final SecurityException ex) {
            Logger.get().error(NetworkStateTrackerKt.access$getTAG$p(), "Received exception while unregistering network callback", ex);
        }
        catch (final IllegalArgumentException ex2) {
            Logger.get().error(NetworkStateTrackerKt.access$getTAG$p(), "Received exception while unregistering network callback", ex2);
        }
    }
}

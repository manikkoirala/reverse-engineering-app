// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.work.Logger;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u0014\u0010\b\u001a\u00020\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013" }, d2 = { "Landroidx/work/impl/constraints/trackers/StorageNotLowTracker;", "Landroidx/work/impl/constraints/trackers/BroadcastReceiverConstraintTracker;", "", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "initialState", "getInitialState", "()Ljava/lang/Boolean;", "intentFilter", "Landroid/content/IntentFilter;", "getIntentFilter", "()Landroid/content/IntentFilter;", "onBroadcastReceive", "", "intent", "Landroid/content/Intent;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class StorageNotLowTracker extends BroadcastReceiverConstraintTracker<Boolean>
{
    public StorageNotLowTracker(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        super(context, taskExecutor);
    }
    
    @Override
    public Boolean getInitialState() {
        final Intent registerReceiver = this.getAppContext().registerReceiver((BroadcastReceiver)null, this.getIntentFilter());
        final boolean b = false;
        if (registerReceiver != null) {
            if (registerReceiver.getAction() != null) {
                final String action = registerReceiver.getAction();
                boolean b2 = b;
                if (action == null) {
                    return b2;
                }
                final int hashCode = action.hashCode();
                if (hashCode == -1181163412) {
                    action.equals("android.intent.action.DEVICE_STORAGE_LOW");
                    b2 = b;
                    return b2;
                }
                if (hashCode != -730838620) {
                    b2 = b;
                    return b2;
                }
                if (!action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    b2 = b;
                    return b2;
                }
            }
        }
        return true;
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }
    
    @Override
    public void onBroadcastReceive(final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)intent, "intent");
        if (intent.getAction() == null) {
            return;
        }
        final Logger value = Logger.get();
        final String access$getTAG$p = StorageNotLowTrackerKt.access$getTAG$p();
        final StringBuilder sb = new StringBuilder();
        sb.append("Received ");
        sb.append(intent.getAction());
        value.debug(access$getTAG$p, sb.toString());
        final String action = intent.getAction();
        if (action != null) {
            final int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620) {
                    if (action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                        this.setState(true);
                    }
                }
            }
            else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                this.setState(false);
            }
        }
    }
}

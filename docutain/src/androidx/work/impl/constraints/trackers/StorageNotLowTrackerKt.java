// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0002" }, d2 = { "TAG", "", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class StorageNotLowTrackerKt
{
    private static final String TAG;
    
    static {
        final String tagWithPrefix = Logger.tagWithPrefix("StorageNotLowTracker");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"StorageNotLowTracker\")");
        TAG = tagWithPrefix;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.work.Logger;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Build$VERSION;
import android.content.Intent;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\b\u001a\u00020\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e¨\u0006\u0014" }, d2 = { "Landroidx/work/impl/constraints/trackers/BatteryChargingTracker;", "Landroidx/work/impl/constraints/trackers/BroadcastReceiverConstraintTracker;", "", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "initialState", "getInitialState", "()Ljava/lang/Boolean;", "intentFilter", "Landroid/content/IntentFilter;", "getIntentFilter", "()Landroid/content/IntentFilter;", "isBatteryChangedIntentCharging", "intent", "Landroid/content/Intent;", "onBroadcastReceive", "", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class BatteryChargingTracker extends BroadcastReceiverConstraintTracker<Boolean>
{
    public BatteryChargingTracker(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        super(context, taskExecutor);
    }
    
    private final boolean isBatteryChangedIntentCharging(final Intent intent) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        if (sdk_INT >= 23) {
            final int intExtra = intent.getIntExtra("status", -1);
            boolean b2 = b;
            if (intExtra == 2) {
                return b2;
            }
            if (intExtra == 5) {
                b2 = b;
                return b2;
            }
        }
        else if (intent.getIntExtra("plugged", 0) != 0) {
            return b;
        }
        return false;
    }
    
    @Override
    public Boolean getInitialState() {
        final Intent registerReceiver = this.getAppContext().registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            Logger.get().error(BatteryChargingTrackerKt.access$getTAG$p(), "getInitialState - null intent received");
            return false;
        }
        return this.isBatteryChangedIntentCharging(registerReceiver);
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        if (Build$VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            intentFilter.addAction("android.os.action.DISCHARGING");
        }
        else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        }
        return intentFilter;
    }
    
    @Override
    public void onBroadcastReceive(final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)intent, "intent");
        final String action = intent.getAction();
        if (action == null) {
            return;
        }
        final Logger value = Logger.get();
        final String access$getTAG$p = BatteryChargingTrackerKt.access$getTAG$p();
        final StringBuilder sb = new StringBuilder();
        sb.append("Received ");
        sb.append(action);
        value.debug(access$getTAG$p, sb.toString());
        switch (action) {
            case "android.intent.action.ACTION_POWER_CONNECTED": {
                this.setState(true);
                break;
            }
            case "android.os.action.CHARGING": {
                this.setState(true);
                break;
            }
            case "android.os.action.DISCHARGING": {
                this.setState(false);
                break;
            }
            case "android.intent.action.ACTION_POWER_DISCONNECTED": {
                this.setState(false);
                break;
            }
            default:
                break;
        }
    }
}

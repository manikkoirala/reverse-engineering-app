// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import kotlin.collections.CollectionsKt;
import kotlin.Unit;
import androidx.work.Logger;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.constraints.ConstraintListener;
import java.util.LinkedHashSet;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0005\b'\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0017\b\u0004\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0014\u0010\u0019\u001a\u00020\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012J\u0014\u0010\u001c\u001a\u00020\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012J\b\u0010\u001d\u001a\u00020\u001aH&J\b\u0010\u001e\u001a\u00020\u001aH&R\u0014\u0010\b\u001a\u00020\u0004X\u0084\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u0004\u0018\u00018\u0000X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\fR\u0012\u0010\r\u001a\u00028\u0000X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00120\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0002X\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00028\u00008F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f" }, d2 = { "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "T", "", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "appContext", "getAppContext", "()Landroid/content/Context;", "currentState", "Ljava/lang/Object;", "initialState", "getInitialState", "()Ljava/lang/Object;", "listeners", "Ljava/util/LinkedHashSet;", "Landroidx/work/impl/constraints/ConstraintListener;", "lock", "newState", "state", "getState", "setState", "(Ljava/lang/Object;)V", "addListener", "", "listener", "removeListener", "startTracking", "stopTracking", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public abstract class ConstraintTracker<T>
{
    private final Context appContext;
    private T currentState;
    private final LinkedHashSet<ConstraintListener<T>> listeners;
    private final Object lock;
    private final TaskExecutor taskExecutor;
    
    protected ConstraintTracker(Context applicationContext, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)applicationContext, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        this.taskExecutor = taskExecutor;
        applicationContext = applicationContext.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue((Object)applicationContext, "context.applicationContext");
        this.appContext = applicationContext;
        this.lock = new Object();
        this.listeners = new LinkedHashSet<ConstraintListener<T>>();
    }
    
    private static final void _set_state_$lambda$4$lambda$3(final List list, final ConstraintTracker constraintTracker) {
        Intrinsics.checkNotNullParameter((Object)list, "$listenersList");
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "this$0");
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ((ConstraintListener<T>)iterator.next()).onConstraintChanged(constraintTracker.currentState);
        }
    }
    
    public final void addListener(final ConstraintListener<T> e) {
        Intrinsics.checkNotNullParameter((Object)e, "listener");
        synchronized (this.lock) {
            if (this.listeners.add(e)) {
                if (this.listeners.size() == 1) {
                    this.currentState = this.getInitialState();
                    final Logger value = Logger.get();
                    final String access$getTAG$p = ConstraintTrackerKt.access$getTAG$p();
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.getClass().getSimpleName());
                    sb.append(": initial state = ");
                    sb.append(this.currentState);
                    value.debug(access$getTAG$p, sb.toString());
                    this.startTracking();
                }
                e.onConstraintChanged(this.currentState);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    protected final Context getAppContext() {
        return this.appContext;
    }
    
    public abstract T getInitialState();
    
    public final T getState() {
        T t;
        if ((t = this.currentState) == null) {
            t = this.getInitialState();
        }
        return t;
    }
    
    public final void removeListener(final ConstraintListener<T> o) {
        Intrinsics.checkNotNullParameter((Object)o, "listener");
        synchronized (this.lock) {
            if (this.listeners.remove(o) && this.listeners.isEmpty()) {
                this.stopTracking();
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void setState(final T currentState) {
        synchronized (this.lock) {
            final T currentState2 = this.currentState;
            if (currentState2 != null && Intrinsics.areEqual((Object)currentState2, (Object)currentState)) {
                return;
            }
            this.currentState = currentState;
            this.taskExecutor.getMainThreadExecutor().execute(new ConstraintTracker$$ExternalSyntheticLambda0(CollectionsKt.toList((Iterable)this.listeners), this));
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public abstract void startTracking();
    
    public abstract void stopTracking();
}

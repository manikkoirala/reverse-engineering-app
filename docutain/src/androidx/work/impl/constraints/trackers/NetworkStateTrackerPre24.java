// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.work.Logger;
import android.content.Intent;
import android.content.IntentFilter;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import android.net.ConnectivityManager;
import kotlin.Metadata;
import androidx.work.impl.constraints.NetworkState;

@Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000e8VX\u0096\u0004¢\u0006\f\u0012\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0017" }, d2 = { "Landroidx/work/impl/constraints/trackers/NetworkStateTrackerPre24;", "Landroidx/work/impl/constraints/trackers/BroadcastReceiverConstraintTracker;", "Landroidx/work/impl/constraints/NetworkState;", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "(Landroid/content/Context;Landroidx/work/impl/utils/taskexecutor/TaskExecutor;)V", "connectivityManager", "Landroid/net/ConnectivityManager;", "initialState", "getInitialState", "()Landroidx/work/impl/constraints/NetworkState;", "intentFilter", "Landroid/content/IntentFilter;", "getIntentFilter$annotations", "()V", "getIntentFilter", "()Landroid/content/IntentFilter;", "onBroadcastReceive", "", "intent", "Landroid/content/Intent;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkStateTrackerPre24 extends BroadcastReceiverConstraintTracker<NetworkState>
{
    private final ConnectivityManager connectivityManager;
    
    public NetworkStateTrackerPre24(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        super(context, taskExecutor);
        final Object systemService = this.getAppContext().getSystemService("connectivity");
        Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.net.ConnectivityManager");
        this.connectivityManager = (ConnectivityManager)systemService;
    }
    
    @Override
    public NetworkState getInitialState() {
        return NetworkStateTrackerKt.getActiveNetworkState(this.connectivityManager);
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        return new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    }
    
    @Override
    public void onBroadcastReceive(final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)intent, "intent");
        if (Intrinsics.areEqual((Object)intent.getAction(), (Object)"android.net.conn.CONNECTIVITY_CHANGE")) {
            Logger.get().debug(NetworkStateTrackerKt.access$getTAG$p(), "Network broadcast received");
            this.setState(NetworkStateTrackerKt.getActiveNetworkState(this.connectivityManager));
        }
    }
}

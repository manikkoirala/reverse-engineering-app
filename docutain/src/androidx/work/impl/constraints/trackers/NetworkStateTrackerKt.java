// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.net.NetworkCapabilities;
import androidx.work.impl.utils.NetworkApi21;
import androidx.work.impl.utils.NetworkApi23;
import android.net.NetworkInfo;
import androidx.core.net.ConnectivityManagerCompat;
import android.net.ConnectivityManager;
import android.os.Build$VERSION;
import androidx.work.impl.constraints.NetworkState;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u001e\u0010\u0002\u001a\u00020\u0003*\u00020\u00048@X\u0080\u0004¢\u0006\f\u0012\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\b\"\u0018\u0010\t\u001a\u00020\n*\u00020\u00048@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u000b¨\u0006\u0012" }, d2 = { "TAG", "", "activeNetworkState", "Landroidx/work/impl/constraints/NetworkState;", "Landroid/net/ConnectivityManager;", "getActiveNetworkState$annotations", "(Landroid/net/ConnectivityManager;)V", "getActiveNetworkState", "(Landroid/net/ConnectivityManager;)Landroidx/work/impl/constraints/NetworkState;", "isActiveNetworkValidated", "", "(Landroid/net/ConnectivityManager;)Z", "NetworkStateTracker", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "context", "Landroid/content/Context;", "taskExecutor", "Landroidx/work/impl/utils/taskexecutor/TaskExecutor;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkStateTrackerKt
{
    private static final String TAG;
    
    static {
        final String tagWithPrefix = Logger.tagWithPrefix("NetworkStateTracker");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"NetworkStateTracker\")");
        TAG = tagWithPrefix;
    }
    
    public static final ConstraintTracker<NetworkState> NetworkStateTracker(final Context context, final TaskExecutor taskExecutor) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)taskExecutor, "taskExecutor");
        ConstraintTracker constraintTracker;
        if (Build$VERSION.SDK_INT >= 24) {
            constraintTracker = new NetworkStateTracker24(context, taskExecutor);
        }
        else {
            constraintTracker = new NetworkStateTrackerPre24(context, taskExecutor);
        }
        return constraintTracker;
    }
    
    public static final NetworkState getActiveNetworkState(final ConnectivityManager connectivityManager) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean b = true;
        final boolean b2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        final boolean activeNetworkValidated = isActiveNetworkValidated(connectivityManager);
        final boolean activeNetworkMetered = ConnectivityManagerCompat.isActiveNetworkMetered(connectivityManager);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            b = false;
        }
        return new NetworkState(b2, activeNetworkValidated, activeNetworkMetered, b);
    }
    
    public static final boolean isActiveNetworkValidated(final ConnectivityManager connectivityManager) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        boolean hasCapabilityCompat;
        if (sdk_INT < 23) {
            hasCapabilityCompat = b;
        }
        else {
            try {
                final NetworkCapabilities networkCapabilitiesCompat = NetworkApi21.getNetworkCapabilitiesCompat(connectivityManager, NetworkApi23.getActiveNetworkCompat(connectivityManager));
                hasCapabilityCompat = b;
                if (networkCapabilitiesCompat != null) {
                    hasCapabilityCompat = NetworkApi21.hasCapabilityCompat(networkCapabilitiesCompat, 16);
                }
            }
            catch (final SecurityException ex) {
                Logger.get().error(NetworkStateTrackerKt.TAG, "Unable to validate active network", ex);
                hasCapabilityCompat = b;
            }
        }
        return hasCapabilityCompat;
    }
}

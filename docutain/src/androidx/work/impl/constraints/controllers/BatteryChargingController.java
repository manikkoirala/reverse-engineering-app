// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

import androidx.work.impl.model.WorkSpec;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.constraints.trackers.ConstraintTracker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016¨\u0006\u000b" }, d2 = { "Landroidx/work/impl/constraints/controllers/BatteryChargingController;", "Landroidx/work/impl/constraints/controllers/ConstraintController;", "", "tracker", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "(Landroidx/work/impl/constraints/trackers/ConstraintTracker;)V", "hasConstraint", "workSpec", "Landroidx/work/impl/model/WorkSpec;", "isConstrained", "value", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class BatteryChargingController extends ConstraintController<Boolean>
{
    public BatteryChargingController(final ConstraintTracker<Boolean> constraintTracker) {
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "tracker");
        super(constraintTracker);
    }
    
    @Override
    public boolean hasConstraint(final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)workSpec, "workSpec");
        return workSpec.constraints.requiresCharging();
    }
    
    public boolean isConstrained(final boolean b) {
        return b ^ true;
    }
}

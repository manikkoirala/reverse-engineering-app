// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

import android.os.Build$VERSION;
import androidx.work.NetworkType;
import androidx.work.impl.model.WorkSpec;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.constraints.trackers.ConstraintTracker;
import kotlin.Metadata;
import androidx.work.impl.constraints.NetworkState;

@Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0016J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0002H\u0016¨\u0006\f" }, d2 = { "Landroidx/work/impl/constraints/controllers/NetworkConnectedController;", "Landroidx/work/impl/constraints/controllers/ConstraintController;", "Landroidx/work/impl/constraints/NetworkState;", "tracker", "Landroidx/work/impl/constraints/trackers/ConstraintTracker;", "(Landroidx/work/impl/constraints/trackers/ConstraintTracker;)V", "hasConstraint", "", "workSpec", "Landroidx/work/impl/model/WorkSpec;", "isConstrained", "value", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkConnectedController extends ConstraintController<NetworkState>
{
    public NetworkConnectedController(final ConstraintTracker<NetworkState> constraintTracker) {
        Intrinsics.checkNotNullParameter((Object)constraintTracker, "tracker");
        super(constraintTracker);
    }
    
    @Override
    public boolean hasConstraint(final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)workSpec, "workSpec");
        return workSpec.constraints.getRequiredNetworkType() == NetworkType.CONNECTED;
    }
    
    @Override
    public boolean isConstrained(final NetworkState networkState) {
        Intrinsics.checkNotNullParameter((Object)networkState, "value");
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        if (sdk_INT >= 26) {
            boolean b2 = b;
            if (!networkState.isConnected()) {
                return b2;
            }
            if (!networkState.isValidated()) {
                b2 = b;
                return b2;
            }
        }
        else if (!networkState.isConnected()) {
            return b;
        }
        return false;
    }
}

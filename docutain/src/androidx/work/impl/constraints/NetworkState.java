// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J1\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00032\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\b¨\u0006\u0014" }, d2 = { "Landroidx/work/impl/constraints/NetworkState;", "", "isConnected", "", "isValidated", "isMetered", "isNotRoaming", "(ZZZZ)V", "()Z", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkState
{
    private final boolean isConnected;
    private final boolean isMetered;
    private final boolean isNotRoaming;
    private final boolean isValidated;
    
    public NetworkState(final boolean isConnected, final boolean isValidated, final boolean isMetered, final boolean isNotRoaming) {
        this.isConnected = isConnected;
        this.isValidated = isValidated;
        this.isMetered = isMetered;
        this.isNotRoaming = isNotRoaming;
    }
    
    public final boolean component1() {
        return this.isConnected;
    }
    
    public final boolean component2() {
        return this.isValidated;
    }
    
    public final boolean component3() {
        return this.isMetered;
    }
    
    public final boolean component4() {
        return this.isNotRoaming;
    }
    
    public final NetworkState copy(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        return new NetworkState(b, b2, b3, b4);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NetworkState)) {
            return false;
        }
        final NetworkState networkState = (NetworkState)o;
        return this.isConnected == networkState.isConnected && this.isValidated == networkState.isValidated && this.isMetered == networkState.isMetered && this.isNotRoaming == networkState.isNotRoaming;
    }
    
    @Override
    public int hashCode() {
        final int isConnected = this.isConnected ? 1 : 0;
        int n = 1;
        int n2 = isConnected;
        if (isConnected != 0) {
            n2 = 1;
        }
        int isValidated;
        if ((isValidated = (this.isValidated ? 1 : 0)) != 0) {
            isValidated = 1;
        }
        int isMetered;
        if ((isMetered = (this.isMetered ? 1 : 0)) != 0) {
            isMetered = 1;
        }
        final int isNotRoaming = this.isNotRoaming ? 1 : 0;
        if (isNotRoaming == 0) {
            n = isNotRoaming;
        }
        return ((n2 * 31 + isValidated) * 31 + isMetered) * 31 + n;
    }
    
    public final boolean isConnected() {
        return this.isConnected;
    }
    
    public final boolean isMetered() {
        return this.isMetered;
    }
    
    public final boolean isNotRoaming() {
        return this.isNotRoaming;
    }
    
    public final boolean isValidated() {
        return this.isValidated;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NetworkState(isConnected=");
        sb.append(this.isConnected);
        sb.append(", isValidated=");
        sb.append(this.isValidated);
        sb.append(", isMetered=");
        sb.append(this.isMetered);
        sb.append(", isNotRoaming=");
        sb.append(this.isNotRoaming);
        sb.append(')');
        return sb.toString();
    }
}

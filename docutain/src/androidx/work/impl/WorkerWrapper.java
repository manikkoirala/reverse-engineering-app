// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkSpecKt;
import androidx.work.impl.model.WorkGenerationalId;
import androidx.work.InputMerger;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import androidx.work.impl.utils.SynchronousExecutor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.impl.utils.WorkForegroundRunnable;
import androidx.work.ForegroundUpdater;
import androidx.work.ProgressUpdater;
import androidx.work.impl.utils.WorkForegroundUpdater;
import androidx.work.impl.utils.WorkProgressUpdater;
import java.util.UUID;
import androidx.work.Data;
import java.util.ArrayList;
import androidx.work.impl.utils.PackageManagerHelper;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.Collection;
import androidx.work.WorkInfo;
import java.util.LinkedList;
import java.util.Iterator;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.WorkerParameters;
import androidx.work.ListenableWorker;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.work.impl.foreground.ForegroundProcessor;
import androidx.work.impl.model.DependencyDao;
import androidx.work.Configuration;
import android.content.Context;

public class WorkerWrapper implements Runnable
{
    static final String TAG;
    Context mAppContext;
    private Configuration mConfiguration;
    private DependencyDao mDependencyDao;
    private ForegroundProcessor mForegroundProcessor;
    SettableFuture<Boolean> mFuture;
    private volatile boolean mInterrupted;
    ListenableWorker.Result mResult;
    private WorkerParameters.RuntimeExtras mRuntimeExtras;
    private List<Scheduler> mSchedulers;
    private List<String> mTags;
    private WorkDatabase mWorkDatabase;
    private String mWorkDescription;
    WorkSpec mWorkSpec;
    private WorkSpecDao mWorkSpecDao;
    private final String mWorkSpecId;
    TaskExecutor mWorkTaskExecutor;
    ListenableWorker mWorker;
    final SettableFuture<ListenableWorker.Result> mWorkerResultFuture;
    
    static {
        TAG = Logger.tagWithPrefix("WorkerWrapper");
    }
    
    WorkerWrapper(final Builder builder) {
        this.mResult = ListenableWorker.Result.failure();
        this.mFuture = SettableFuture.create();
        this.mWorkerResultFuture = SettableFuture.create();
        this.mAppContext = builder.mAppContext;
        this.mWorkTaskExecutor = builder.mWorkTaskExecutor;
        this.mForegroundProcessor = builder.mForegroundProcessor;
        final WorkSpec mWorkSpec = builder.mWorkSpec;
        this.mWorkSpec = mWorkSpec;
        this.mWorkSpecId = mWorkSpec.id;
        this.mSchedulers = builder.mSchedulers;
        this.mRuntimeExtras = builder.mRuntimeExtras;
        this.mWorker = builder.mWorker;
        this.mConfiguration = builder.mConfiguration;
        final WorkDatabase mWorkDatabase = builder.mWorkDatabase;
        this.mWorkDatabase = mWorkDatabase;
        this.mWorkSpecDao = mWorkDatabase.workSpecDao();
        this.mDependencyDao = this.mWorkDatabase.dependencyDao();
        this.mTags = builder.mTags;
    }
    
    private String createWorkDescription(final List<String> list) {
        final StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.mWorkSpecId);
        sb.append(", tags={ ");
        final Iterator<String> iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String str = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }
    
    private void handleResult(final ListenableWorker.Result result) {
        if (result instanceof ListenableWorker.Result.Success) {
            final Logger value = Logger.get();
            final String tag = WorkerWrapper.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Worker result SUCCESS for ");
            sb.append(this.mWorkDescription);
            value.info(tag, sb.toString());
            if (this.mWorkSpec.isPeriodic()) {
                this.resetPeriodicAndResolve();
            }
            else {
                this.setSucceededAndResolve();
            }
        }
        else if (result instanceof ListenableWorker.Result.Retry) {
            final Logger value2 = Logger.get();
            final String tag2 = WorkerWrapper.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Worker result RETRY for ");
            sb2.append(this.mWorkDescription);
            value2.info(tag2, sb2.toString());
            this.rescheduleAndResolve();
        }
        else {
            final Logger value3 = Logger.get();
            final String tag3 = WorkerWrapper.TAG;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Worker result FAILURE for ");
            sb3.append(this.mWorkDescription);
            value3.info(tag3, sb3.toString());
            if (this.mWorkSpec.isPeriodic()) {
                this.resetPeriodicAndResolve();
            }
            else {
                this.setFailedAndResolve();
            }
        }
    }
    
    private void iterativelyFailWorkAndDependents(String e) {
        final LinkedList list = new LinkedList();
        list.add(e);
        while (!list.isEmpty()) {
            e = (String)list.remove();
            if (this.mWorkSpecDao.getState(e) != WorkInfo.State.CANCELLED) {
                this.mWorkSpecDao.setState(WorkInfo.State.FAILED, e);
            }
            list.addAll(this.mDependencyDao.getDependentWorkIds(e));
        }
    }
    
    private void rescheduleAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
            this.mWorkSpecDao.setLastEnqueuedTime(this.mWorkSpecId, System.currentTimeMillis());
            this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(true);
        }
    }
    
    private void resetPeriodicAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setLastEnqueuedTime(this.mWorkSpecId, System.currentTimeMillis());
            this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
            this.mWorkSpecDao.resetWorkSpecRunAttemptCount(this.mWorkSpecId);
            this.mWorkSpecDao.incrementPeriodCount(this.mWorkSpecId);
            this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    private void resolve(final boolean b) {
        this.mWorkDatabase.beginTransaction();
        try {
            if (!this.mWorkDatabase.workSpecDao().hasUnfinishedWork()) {
                PackageManagerHelper.setComponentEnabled(this.mAppContext, RescheduleReceiver.class, false);
            }
            if (b) {
                this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
                this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            }
            if (this.mWorkSpec != null && this.mWorker != null && this.mForegroundProcessor.isEnqueuedInForeground(this.mWorkSpecId)) {
                this.mForegroundProcessor.stopForeground(this.mWorkSpecId);
            }
            this.mWorkDatabase.setTransactionSuccessful();
            this.mWorkDatabase.endTransaction();
            this.mFuture.set(b);
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    private void resolveIncorrectStatus() {
        final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
        if (state == WorkInfo.State.RUNNING) {
            final Logger value = Logger.get();
            final String tag = WorkerWrapper.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Status for ");
            sb.append(this.mWorkSpecId);
            sb.append(" is RUNNING; not doing any work and rescheduling for later execution");
            value.debug(tag, sb.toString());
            this.resolve(true);
        }
        else {
            final Logger value2 = Logger.get();
            final String tag2 = WorkerWrapper.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Status for ");
            sb2.append(this.mWorkSpecId);
            sb2.append(" is ");
            sb2.append(state);
            sb2.append(" ; not doing any work");
            value2.debug(tag2, sb2.toString());
            this.resolve(false);
        }
    }
    
    private void runWorker() {
        if (this.tryCheckForInterruptionAndResolve()) {
            return;
        }
        this.mWorkDatabase.beginTransaction();
        try {
            if (this.mWorkSpec.state != WorkInfo.State.ENQUEUED) {
                this.resolveIncorrectStatus();
                this.mWorkDatabase.setTransactionSuccessful();
                final Logger value = Logger.get();
                final String tag = WorkerWrapper.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append(this.mWorkSpec.workerClassName);
                sb.append(" is not in ENQUEUED state. Nothing more to do");
                value.debug(tag, sb.toString());
                return;
            }
            if ((this.mWorkSpec.isPeriodic() || this.mWorkSpec.isBackedOff()) && System.currentTimeMillis() < this.mWorkSpec.calculateNextRunTime()) {
                Logger.get().debug(WorkerWrapper.TAG, String.format("Delaying execution for %s because it is being executed before schedule.", this.mWorkSpec.workerClassName));
                this.resolve(true);
                this.mWorkDatabase.setTransactionSuccessful();
                return;
            }
            this.mWorkDatabase.setTransactionSuccessful();
            this.mWorkDatabase.endTransaction();
            Data data;
            if (this.mWorkSpec.isPeriodic()) {
                data = this.mWorkSpec.input;
            }
            else {
                final InputMerger inputMergerWithDefaultFallback = this.mConfiguration.getInputMergerFactory().createInputMergerWithDefaultFallback(this.mWorkSpec.inputMergerClassName);
                if (inputMergerWithDefaultFallback == null) {
                    final Logger value2 = Logger.get();
                    final String tag2 = WorkerWrapper.TAG;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not create Input Merger ");
                    sb2.append(this.mWorkSpec.inputMergerClassName);
                    value2.error(tag2, sb2.toString());
                    this.setFailedAndResolve();
                    return;
                }
                final ArrayList list = new ArrayList();
                list.add(this.mWorkSpec.input);
                list.addAll(this.mWorkSpecDao.getInputsFromPrerequisites(this.mWorkSpecId));
                data = inputMergerWithDefaultFallback.merge(list);
            }
            final WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.mWorkSpecId), data, this.mTags, this.mRuntimeExtras, this.mWorkSpec.runAttemptCount, this.mWorkSpec.getGeneration(), this.mConfiguration.getExecutor(), this.mWorkTaskExecutor, this.mConfiguration.getWorkerFactory(), new WorkProgressUpdater(this.mWorkDatabase, this.mWorkTaskExecutor), new WorkForegroundUpdater(this.mWorkDatabase, this.mForegroundProcessor, this.mWorkTaskExecutor));
            if (this.mWorker == null) {
                this.mWorker = this.mConfiguration.getWorkerFactory().createWorkerWithDefaultFallback(this.mAppContext, this.mWorkSpec.workerClassName, workerParameters);
            }
            final ListenableWorker mWorker = this.mWorker;
            if (mWorker == null) {
                final Logger value3 = Logger.get();
                final String tag3 = WorkerWrapper.TAG;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Could not create Worker ");
                sb3.append(this.mWorkSpec.workerClassName);
                value3.error(tag3, sb3.toString());
                this.setFailedAndResolve();
                return;
            }
            if (mWorker.isUsed()) {
                final Logger value4 = Logger.get();
                final String tag4 = WorkerWrapper.TAG;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Received an already-used Worker ");
                sb4.append(this.mWorkSpec.workerClassName);
                sb4.append("; Worker Factory should return new instances");
                value4.error(tag4, sb4.toString());
                this.setFailedAndResolve();
                return;
            }
            this.mWorker.setUsed();
            if (this.trySetRunning()) {
                if (this.tryCheckForInterruptionAndResolve()) {
                    return;
                }
                final WorkForegroundRunnable workForegroundRunnable = new WorkForegroundRunnable(this.mAppContext, this.mWorkSpec, this.mWorker, workerParameters.getForegroundUpdater(), this.mWorkTaskExecutor);
                this.mWorkTaskExecutor.getMainThreadExecutor().execute(workForegroundRunnable);
                final ListenableFuture<Void> future = workForegroundRunnable.getFuture();
                this.mWorkerResultFuture.addListener(new WorkerWrapper$$ExternalSyntheticLambda0(this, future), new SynchronousExecutor());
                future.addListener((Runnable)new Runnable(this, future) {
                    final WorkerWrapper this$0;
                    final ListenableFuture val$runExpedited;
                    
                    @Override
                    public void run() {
                        if (this.this$0.mWorkerResultFuture.isCancelled()) {
                            return;
                        }
                        try {
                            this.val$runExpedited.get();
                            final Logger value = Logger.get();
                            final String tag = WorkerWrapper.TAG;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Starting work for ");
                            sb.append(this.this$0.mWorkSpec.workerClassName);
                            value.debug(tag, sb.toString());
                            this.this$0.mWorkerResultFuture.setFuture((com.google.common.util.concurrent.ListenableFuture<? extends ListenableWorker.Result>)this.this$0.mWorker.startWork());
                        }
                        finally {
                            final Throwable exception;
                            this.this$0.mWorkerResultFuture.setException(exception);
                        }
                    }
                }, this.mWorkTaskExecutor.getMainThreadExecutor());
                this.mWorkerResultFuture.addListener(new Runnable(this, this.mWorkDescription) {
                    final WorkerWrapper this$0;
                    final String val$workDescription;
                    
                    @Override
                    public void run() {
                        try {
                            try {
                                final ListenableWorker.Result result = this.this$0.mWorkerResultFuture.get();
                                if (result == null) {
                                    final Logger value = Logger.get();
                                    final String tag = WorkerWrapper.TAG;
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(this.this$0.mWorkSpec.workerClassName);
                                    sb.append(" returned a null result. Treating it as a failure.");
                                    value.error(tag, sb.toString());
                                }
                                final Logger value2 = Logger.get();
                                final String tag2 = WorkerWrapper.TAG;
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append(this.this$0.mWorkSpec.workerClassName);
                                sb2.append(" returned a ");
                                sb2.append(result);
                                sb2.append(".");
                                value2.debug(tag2, sb2.toString());
                                this.this$0.mResult = result;
                            }
                            finally {}
                        }
                        catch (final ExecutionException t) {
                            goto Label_0150;
                        }
                        catch (final InterruptedException ex) {}
                        catch (final CancellationException ex2) {
                            final Logger value3 = Logger.get();
                            final String tag3 = WorkerWrapper.TAG;
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append(this.val$workDescription);
                            sb3.append(" was cancelled");
                            value3.info(tag3, sb3.toString(), ex2);
                        }
                        this.this$0.onWorkFinished();
                        return;
                        this.this$0.onWorkFinished();
                    }
                }, this.mWorkTaskExecutor.getSerialTaskExecutor());
            }
            else {
                this.resolveIncorrectStatus();
            }
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    private void setSucceededAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setState(WorkInfo.State.SUCCEEDED, this.mWorkSpecId);
            this.mWorkSpecDao.setOutput(this.mWorkSpecId, ((ListenableWorker.Result.Success)this.mResult).getOutputData());
            final long currentTimeMillis = System.currentTimeMillis();
            for (final String str : this.mDependencyDao.getDependentWorkIds(this.mWorkSpecId)) {
                if (this.mWorkSpecDao.getState(str) == WorkInfo.State.BLOCKED && this.mDependencyDao.hasCompletedAllPrerequisites(str)) {
                    final Logger value = Logger.get();
                    final String tag = WorkerWrapper.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Setting status to enqueued for ");
                    sb.append(str);
                    value.info(tag, sb.toString());
                    this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, str);
                    this.mWorkSpecDao.setLastEnqueuedTime(str, currentTimeMillis);
                }
            }
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    private boolean tryCheckForInterruptionAndResolve() {
        if (this.mInterrupted) {
            final Logger value = Logger.get();
            final String tag = WorkerWrapper.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Work interrupted for ");
            sb.append(this.mWorkDescription);
            value.debug(tag, sb.toString());
            final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
            if (state == null) {
                this.resolve(false);
            }
            else {
                this.resolve(state.isFinished() ^ true);
            }
            return true;
        }
        return false;
    }
    
    private boolean trySetRunning() {
        this.mWorkDatabase.beginTransaction();
        try {
            boolean b;
            if (this.mWorkSpecDao.getState(this.mWorkSpecId) == WorkInfo.State.ENQUEUED) {
                this.mWorkSpecDao.setState(WorkInfo.State.RUNNING, this.mWorkSpecId);
                this.mWorkSpecDao.incrementWorkSpecRunAttemptCount(this.mWorkSpecId);
                b = true;
            }
            else {
                b = false;
            }
            this.mWorkDatabase.setTransactionSuccessful();
            return b;
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    public ListenableFuture<Boolean> getFuture() {
        return (ListenableFuture<Boolean>)this.mFuture;
    }
    
    public WorkGenerationalId getWorkGenerationalId() {
        return WorkSpecKt.generationalId(this.mWorkSpec);
    }
    
    public WorkSpec getWorkSpec() {
        return this.mWorkSpec;
    }
    
    public void interrupt() {
        this.mInterrupted = true;
        this.tryCheckForInterruptionAndResolve();
        this.mWorkerResultFuture.cancel(true);
        if (this.mWorker != null && this.mWorkerResultFuture.isCancelled()) {
            this.mWorker.stop();
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkSpec ");
            sb.append(this.mWorkSpec);
            sb.append(" is already done. Not interrupting.");
            Logger.get().debug(WorkerWrapper.TAG, sb.toString());
        }
    }
    
    void onWorkFinished() {
        if (!this.tryCheckForInterruptionAndResolve()) {
            this.mWorkDatabase.beginTransaction();
            try {
                final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
                this.mWorkDatabase.workProgressDao().delete(this.mWorkSpecId);
                if (state == null) {
                    this.resolve(false);
                }
                else if (state == WorkInfo.State.RUNNING) {
                    this.handleResult(this.mResult);
                }
                else if (!state.isFinished()) {
                    this.rescheduleAndResolve();
                }
                this.mWorkDatabase.setTransactionSuccessful();
            }
            finally {
                this.mWorkDatabase.endTransaction();
            }
        }
        final List<Scheduler> mSchedulers = this.mSchedulers;
        if (mSchedulers != null) {
            final Iterator<Scheduler> iterator = mSchedulers.iterator();
            while (iterator.hasNext()) {
                iterator.next().cancel(this.mWorkSpecId);
            }
            Schedulers.schedule(this.mConfiguration, this.mWorkDatabase, this.mSchedulers);
        }
    }
    
    @Override
    public void run() {
        this.mWorkDescription = this.createWorkDescription(this.mTags);
        this.runWorker();
    }
    
    void setFailedAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.iterativelyFailWorkAndDependents(this.mWorkSpecId);
            this.mWorkSpecDao.setOutput(this.mWorkSpecId, ((ListenableWorker.Result.Failure)this.mResult).getOutputData());
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    public static class Builder
    {
        Context mAppContext;
        Configuration mConfiguration;
        ForegroundProcessor mForegroundProcessor;
        WorkerParameters.RuntimeExtras mRuntimeExtras;
        List<Scheduler> mSchedulers;
        private final List<String> mTags;
        WorkDatabase mWorkDatabase;
        WorkSpec mWorkSpec;
        TaskExecutor mWorkTaskExecutor;
        ListenableWorker mWorker;
        
        public Builder(final Context context, final Configuration mConfiguration, final TaskExecutor mWorkTaskExecutor, final ForegroundProcessor mForegroundProcessor, final WorkDatabase mWorkDatabase, final WorkSpec mWorkSpec, final List<String> mTags) {
            this.mRuntimeExtras = new WorkerParameters.RuntimeExtras();
            this.mAppContext = context.getApplicationContext();
            this.mWorkTaskExecutor = mWorkTaskExecutor;
            this.mForegroundProcessor = mForegroundProcessor;
            this.mConfiguration = mConfiguration;
            this.mWorkDatabase = mWorkDatabase;
            this.mWorkSpec = mWorkSpec;
            this.mTags = mTags;
        }
        
        public WorkerWrapper build() {
            return new WorkerWrapper(this);
        }
        
        public Builder withRuntimeExtras(final WorkerParameters.RuntimeExtras mRuntimeExtras) {
            if (mRuntimeExtras != null) {
                this.mRuntimeExtras = mRuntimeExtras;
            }
            return this;
        }
        
        public Builder withSchedulers(final List<Scheduler> mSchedulers) {
            this.mSchedulers = mSchedulers;
            return this;
        }
        
        public Builder withWorker(final ListenableWorker mWorker) {
            this.mWorker = mWorker;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import java.util.List;
import androidx.work.impl.model.WorkSpecKt;
import androidx.work.impl.model.WorkSpec;
import kotlin.jvm.internal.Intrinsics;
import java.util.LinkedHashMap;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006J\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\n\u001a\u00020\u0006J\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\f\u001a\u00020\rJ\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0006J\u000e\u0010\u0011\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0003\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/work/impl/StartStopTokens;", "", "()V", "lock", "runs", "", "Landroidx/work/impl/model/WorkGenerationalId;", "Landroidx/work/impl/StartStopToken;", "contains", "", "id", "remove", "spec", "Landroidx/work/impl/model/WorkSpec;", "", "workSpecId", "", "tokenFor", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class StartStopTokens
{
    private final Object lock;
    private final Map<WorkGenerationalId, StartStopToken> runs;
    
    public StartStopTokens() {
        this.lock = new Object();
        this.runs = new LinkedHashMap<WorkGenerationalId, StartStopToken>();
    }
    
    public final boolean contains(final WorkGenerationalId workGenerationalId) {
        Intrinsics.checkNotNullParameter((Object)workGenerationalId, "id");
        synchronized (this.lock) {
            return this.runs.containsKey(workGenerationalId);
        }
    }
    
    public final StartStopToken remove(final WorkGenerationalId workGenerationalId) {
        Intrinsics.checkNotNullParameter((Object)workGenerationalId, "id");
        synchronized (this.lock) {
            return this.runs.remove(workGenerationalId);
        }
    }
    
    public final StartStopToken remove(final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)workSpec, "spec");
        return this.remove(WorkSpecKt.generationalId(workSpec));
    }
    
    public final List<StartStopToken> remove(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "workSpecId");
        synchronized (this.lock) {
            final Map<WorkGenerationalId, StartStopToken> runs = this.runs;
            final LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (final Map.Entry<WorkGenerationalId, V> entry : runs.entrySet()) {
                if (Intrinsics.areEqual((Object)entry.getKey().getWorkSpecId(), (Object)s)) {
                    linkedHashMap.put(entry.getKey(), entry.getValue());
                }
            }
            final Map map = linkedHashMap;
            final Iterator iterator2 = map.keySet().iterator();
            while (iterator2.hasNext()) {
                this.runs.remove(iterator2.next());
            }
            return CollectionsKt.toList((Iterable)map.values());
        }
    }
    
    public final StartStopToken tokenFor(final WorkGenerationalId workGenerationalId) {
        Intrinsics.checkNotNullParameter((Object)workGenerationalId, "id");
        synchronized (this.lock) {
            final Map<WorkGenerationalId, StartStopToken> runs = this.runs;
            StartStopToken value;
            if ((value = runs.get(workGenerationalId)) == null) {
                value = new StartStopToken(workGenerationalId);
                runs.put(workGenerationalId, value);
            }
            return value;
        }
    }
    
    public final StartStopToken tokenFor(final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)workSpec, "spec");
        return this.tokenFor(WorkSpecKt.generationalId(workSpec));
    }
}

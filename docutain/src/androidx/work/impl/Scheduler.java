// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkSpec;

public interface Scheduler
{
    public static final int MAX_GREEDY_SCHEDULER_LIMIT = 200;
    public static final int MAX_SCHEDULER_LIMIT = 50;
    
    void cancel(final String p0);
    
    boolean hasLimitedSchedulingSlots();
    
    void schedule(final WorkSpec... p0);
}

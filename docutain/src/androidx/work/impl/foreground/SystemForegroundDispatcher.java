// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import java.util.List;
import androidx.work.impl.model.WorkSpecKt;
import java.util.Iterator;
import android.os.Build$VERSION;
import android.app.Notification;
import java.util.UUID;
import android.text.TextUtils;
import android.os.Parcelable;
import android.net.Uri;
import android.content.Intent;
import androidx.work.impl.constraints.WorkConstraintsTrackerImpl;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.model.WorkSpec;
import java.util.Set;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.ForegroundInfo;
import java.util.Map;
import androidx.work.impl.model.WorkGenerationalId;
import android.content.Context;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.work.impl.ExecutionListener;
import androidx.work.impl.constraints.WorkConstraintsCallback;

public class SystemForegroundDispatcher implements WorkConstraintsCallback, ExecutionListener
{
    private static final String ACTION_CANCEL_WORK = "ACTION_CANCEL_WORK";
    private static final String ACTION_NOTIFY = "ACTION_NOTIFY";
    private static final String ACTION_START_FOREGROUND = "ACTION_START_FOREGROUND";
    private static final String ACTION_STOP_FOREGROUND = "ACTION_STOP_FOREGROUND";
    private static final String KEY_FOREGROUND_SERVICE_TYPE = "KEY_FOREGROUND_SERVICE_TYPE";
    private static final String KEY_GENERATION = "KEY_GENERATION";
    private static final String KEY_NOTIFICATION = "KEY_NOTIFICATION";
    private static final String KEY_NOTIFICATION_ID = "KEY_NOTIFICATION_ID";
    private static final String KEY_WORKSPEC_ID = "KEY_WORKSPEC_ID";
    static final String TAG;
    private Callback mCallback;
    final WorkConstraintsTracker mConstraintsTracker;
    private Context mContext;
    WorkGenerationalId mCurrentForegroundId;
    final Map<WorkGenerationalId, ForegroundInfo> mForegroundInfoById;
    final Object mLock;
    private final TaskExecutor mTaskExecutor;
    final Set<WorkSpec> mTrackedWorkSpecs;
    private WorkManagerImpl mWorkManagerImpl;
    final Map<WorkGenerationalId, WorkSpec> mWorkSpecById;
    
    static {
        TAG = Logger.tagWithPrefix("SystemFgDispatcher");
    }
    
    SystemForegroundDispatcher(final Context mContext) {
        this.mContext = mContext;
        this.mLock = new Object();
        final WorkManagerImpl instance = WorkManagerImpl.getInstance(this.mContext);
        this.mWorkManagerImpl = instance;
        this.mTaskExecutor = instance.getWorkTaskExecutor();
        this.mCurrentForegroundId = null;
        this.mForegroundInfoById = new LinkedHashMap<WorkGenerationalId, ForegroundInfo>();
        this.mTrackedWorkSpecs = new HashSet<WorkSpec>();
        this.mWorkSpecById = new HashMap<WorkGenerationalId, WorkSpec>();
        this.mConstraintsTracker = new WorkConstraintsTrackerImpl(this.mWorkManagerImpl.getTrackers(), this);
        this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
    }
    
    SystemForegroundDispatcher(final Context mContext, final WorkManagerImpl mWorkManagerImpl, final WorkConstraintsTracker mConstraintsTracker) {
        this.mContext = mContext;
        this.mLock = new Object();
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mTaskExecutor = mWorkManagerImpl.getWorkTaskExecutor();
        this.mCurrentForegroundId = null;
        this.mForegroundInfoById = new LinkedHashMap<WorkGenerationalId, ForegroundInfo>();
        this.mTrackedWorkSpecs = new HashSet<WorkSpec>();
        this.mWorkSpecById = new HashMap<WorkGenerationalId, WorkSpec>();
        this.mConstraintsTracker = mConstraintsTracker;
        this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
    }
    
    public static Intent createCancelWorkIntent(final Context context, final String str) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_CANCEL_WORK");
        final StringBuilder sb = new StringBuilder();
        sb.append("workspec://");
        sb.append(str);
        intent.setData(Uri.parse(sb.toString()));
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }
    
    public static Intent createNotifyIntent(final Context context, final WorkGenerationalId workGenerationalId, final ForegroundInfo foregroundInfo) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_NOTIFY");
        intent.putExtra("KEY_NOTIFICATION_ID", foregroundInfo.getNotificationId());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", foregroundInfo.getForegroundServiceType());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)foregroundInfo.getNotification());
        intent.putExtra("KEY_WORKSPEC_ID", workGenerationalId.getWorkSpecId());
        intent.putExtra("KEY_GENERATION", workGenerationalId.getGeneration());
        return intent;
    }
    
    public static Intent createStartForegroundIntent(final Context context, final WorkGenerationalId workGenerationalId, final ForegroundInfo foregroundInfo) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_START_FOREGROUND");
        intent.putExtra("KEY_WORKSPEC_ID", workGenerationalId.getWorkSpecId());
        intent.putExtra("KEY_GENERATION", workGenerationalId.getGeneration());
        intent.putExtra("KEY_NOTIFICATION_ID", foregroundInfo.getNotificationId());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", foregroundInfo.getForegroundServiceType());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)foregroundInfo.getNotification());
        return intent;
    }
    
    public static Intent createStopForegroundIntent(final Context context) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_STOP_FOREGROUND");
        return intent;
    }
    
    private void handleCancelWork(final Intent obj) {
        final Logger value = Logger.get();
        final String tag = SystemForegroundDispatcher.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Stopping foreground work for ");
        sb.append(obj);
        value.info(tag, sb.toString());
        final String stringExtra = obj.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.mWorkManagerImpl.cancelWorkById(UUID.fromString(stringExtra));
        }
    }
    
    private void handleNotify(final Intent intent) {
        int n = 0;
        final int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        final int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        final String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        final WorkGenerationalId mCurrentForegroundId = new WorkGenerationalId(stringExtra, intent.getIntExtra("KEY_GENERATION", 0));
        final Notification notification = (Notification)intent.getParcelableExtra("KEY_NOTIFICATION");
        final Logger value = Logger.get();
        final String tag = SystemForegroundDispatcher.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Notifying with (id:");
        sb.append(intExtra);
        sb.append(", workSpecId: ");
        sb.append(stringExtra);
        sb.append(", notificationType :");
        sb.append(intExtra2);
        sb.append(")");
        value.debug(tag, sb.toString());
        if (notification != null && this.mCallback != null) {
            this.mForegroundInfoById.put(mCurrentForegroundId, new ForegroundInfo(intExtra, notification, intExtra2));
            if (this.mCurrentForegroundId == null) {
                this.mCurrentForegroundId = mCurrentForegroundId;
                this.mCallback.startForeground(intExtra, intExtra2, notification);
            }
            else {
                this.mCallback.notify(intExtra, notification);
                if (intExtra2 != 0 && Build$VERSION.SDK_INT >= 29) {
                    final Iterator<Map.Entry<WorkGenerationalId, ForegroundInfo>> iterator = this.mForegroundInfoById.entrySet().iterator();
                    while (iterator.hasNext()) {
                        n |= ((Map.Entry<K, ForegroundInfo>)iterator.next()).getValue().getForegroundServiceType();
                    }
                    final ForegroundInfo foregroundInfo = this.mForegroundInfoById.get(this.mCurrentForegroundId);
                    if (foregroundInfo != null) {
                        this.mCallback.startForeground(foregroundInfo.getNotificationId(), n, foregroundInfo.getNotification());
                    }
                }
            }
        }
    }
    
    private void handleStartForeground(final Intent obj) {
        final Logger value = Logger.get();
        final String tag = SystemForegroundDispatcher.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Started foreground service ");
        sb.append(obj);
        value.info(tag, sb.toString());
        this.mTaskExecutor.executeOnTaskThread(new Runnable(this, obj.getStringExtra("KEY_WORKSPEC_ID")) {
            final SystemForegroundDispatcher this$0;
            final String val$workSpecId;
            
            @Override
            public void run() {
                final WorkSpec runningWorkSpec = this.this$0.mWorkManagerImpl.getProcessor().getRunningWorkSpec(this.val$workSpecId);
                if (runningWorkSpec != null && runningWorkSpec.hasConstraints()) {
                    synchronized (this.this$0.mLock) {
                        this.this$0.mWorkSpecById.put(WorkSpecKt.generationalId(runningWorkSpec), runningWorkSpec);
                        this.this$0.mTrackedWorkSpecs.add(runningWorkSpec);
                        this.this$0.mConstraintsTracker.replace(this.this$0.mTrackedWorkSpecs);
                    }
                }
            }
        });
    }
    
    void handleStop(final Intent intent) {
        Logger.get().info(SystemForegroundDispatcher.TAG, "Stopping foreground service");
        final Callback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.stop();
        }
    }
    
    @Override
    public void onAllConstraintsMet(final List<WorkSpec> list) {
    }
    
    @Override
    public void onAllConstraintsNotMet(final List<WorkSpec> list) {
        if (!list.isEmpty()) {
            for (final WorkSpec workSpec : list) {
                final String id = workSpec.id;
                final Logger value = Logger.get();
                final String tag = SystemForegroundDispatcher.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Constraints unmet for WorkSpec ");
                sb.append(id);
                value.debug(tag, sb.toString());
                this.mWorkManagerImpl.stopForegroundWork(WorkSpecKt.generationalId(workSpec));
            }
        }
    }
    
    void onDestroy() {
        this.mCallback = null;
        synchronized (this.mLock) {
            this.mConstraintsTracker.reset();
            monitorexit(this.mLock);
            this.mWorkManagerImpl.getProcessor().removeExecutionListener(this);
        }
    }
    
    @Override
    public void onExecuted(final WorkGenerationalId obj, final boolean b) {
        Object o = this.mLock;
        synchronized (o) {
            final WorkSpec workSpec = this.mWorkSpecById.remove(obj);
            if (workSpec != null && this.mTrackedWorkSpecs.remove(workSpec)) {
                this.mConstraintsTracker.replace(this.mTrackedWorkSpecs);
            }
            monitorexit(o);
            final ForegroundInfo foregroundInfo = this.mForegroundInfoById.remove(obj);
            if (obj.equals(this.mCurrentForegroundId) && this.mForegroundInfoById.size() > 0) {
                final Iterator<Map.Entry<WorkGenerationalId, ForegroundInfo>> iterator = this.mForegroundInfoById.entrySet().iterator();
                o = iterator.next();
                while (iterator.hasNext()) {
                    o = iterator.next();
                }
                this.mCurrentForegroundId = ((Map.Entry<WorkGenerationalId, ForegroundInfo>)o).getKey();
                if (this.mCallback != null) {
                    o = ((Map.Entry<WorkGenerationalId, ForegroundInfo>)o).getValue();
                    this.mCallback.startForeground(((ForegroundInfo)o).getNotificationId(), ((ForegroundInfo)o).getForegroundServiceType(), ((ForegroundInfo)o).getNotification());
                    this.mCallback.cancelNotification(((ForegroundInfo)o).getNotificationId());
                }
            }
            o = this.mCallback;
            if (foregroundInfo != null && o != null) {
                final Logger value = Logger.get();
                final String tag = SystemForegroundDispatcher.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Removing Notification (id: ");
                sb.append(foregroundInfo.getNotificationId());
                sb.append(", workSpecId: ");
                sb.append(obj);
                sb.append(", notificationType: ");
                sb.append(foregroundInfo.getForegroundServiceType());
                value.debug(tag, sb.toString());
                ((Callback)o).cancelNotification(foregroundInfo.getNotificationId());
            }
        }
    }
    
    void onStartCommand(final Intent intent) {
        final String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            this.handleStartForeground(intent);
            this.handleNotify(intent);
        }
        else if ("ACTION_NOTIFY".equals(action)) {
            this.handleNotify(intent);
        }
        else if ("ACTION_CANCEL_WORK".equals(action)) {
            this.handleCancelWork(intent);
        }
        else if ("ACTION_STOP_FOREGROUND".equals(action)) {
            this.handleStop(intent);
        }
    }
    
    void setCallback(final Callback mCallback) {
        if (this.mCallback != null) {
            Logger.get().error(SystemForegroundDispatcher.TAG, "A callback already exists.");
            return;
        }
        this.mCallback = mCallback;
    }
    
    interface Callback
    {
        void cancelNotification(final int p0);
        
        void notify(final int p0, final Notification p1);
        
        void startForeground(final int p0, final int p1, final Notification p2);
        
        void stop();
    }
}

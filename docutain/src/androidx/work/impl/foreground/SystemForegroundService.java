// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import android.app.ForegroundServiceStartNotAllowedException;
import android.app.Service;
import android.os.Build$VERSION;
import android.content.Intent;
import android.app.Notification;
import android.os.Looper;
import androidx.work.Logger;
import android.app.NotificationManager;
import android.os.Handler;
import androidx.lifecycle.LifecycleService;

public class SystemForegroundService extends LifecycleService implements Callback
{
    private static final String TAG;
    private static SystemForegroundService sForegroundService;
    SystemForegroundDispatcher mDispatcher;
    private Handler mHandler;
    private boolean mIsShutdown;
    NotificationManager mNotificationManager;
    
    static {
        TAG = Logger.tagWithPrefix("SystemFgService");
        SystemForegroundService.sForegroundService = null;
    }
    
    public static SystemForegroundService getInstance() {
        return SystemForegroundService.sForegroundService;
    }
    
    private void initializeDispatcher() {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mNotificationManager = (NotificationManager)this.getApplicationContext().getSystemService("notification");
        (this.mDispatcher = new SystemForegroundDispatcher(this.getApplicationContext())).setCallback((SystemForegroundDispatcher.Callback)this);
    }
    
    @Override
    public void cancelNotification(final int n) {
        this.mHandler.post((Runnable)new Runnable(this, n) {
            final SystemForegroundService this$0;
            final int val$notificationId;
            
            @Override
            public void run() {
                this.this$0.mNotificationManager.cancel(this.val$notificationId);
            }
        });
    }
    
    @Override
    public void notify(final int n, final Notification notification) {
        this.mHandler.post((Runnable)new Runnable(this, n, notification) {
            final SystemForegroundService this$0;
            final Notification val$notification;
            final int val$notificationId;
            
            @Override
            public void run() {
                this.this$0.mNotificationManager.notify(this.val$notificationId, this.val$notification);
            }
        });
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        (SystemForegroundService.sForegroundService = this).initializeDispatcher();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mDispatcher.onDestroy();
    }
    
    @Override
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.mIsShutdown) {
            Logger.get().info(SystemForegroundService.TAG, "Re-initializing SystemForegroundService after a request to shut-down.");
            this.mDispatcher.onDestroy();
            this.initializeDispatcher();
            this.mIsShutdown = false;
        }
        if (intent != null) {
            this.mDispatcher.onStartCommand(intent);
        }
        return 3;
    }
    
    @Override
    public void startForeground(final int n, final int n2, final Notification notification) {
        this.mHandler.post((Runnable)new Runnable(this, n, notification, n2) {
            final SystemForegroundService this$0;
            final Notification val$notification;
            final int val$notificationId;
            final int val$notificationType;
            
            @Override
            public void run() {
                if (Build$VERSION.SDK_INT >= 31) {
                    Api31Impl.startForeground(this.this$0, this.val$notificationId, this.val$notification, this.val$notificationType);
                }
                else if (Build$VERSION.SDK_INT >= 29) {
                    Api29Impl.startForeground(this.this$0, this.val$notificationId, this.val$notification, this.val$notificationType);
                }
                else {
                    this.this$0.startForeground(this.val$notificationId, this.val$notification);
                }
            }
        });
    }
    
    @Override
    public void stop() {
        this.mIsShutdown = true;
        Logger.get().debug(SystemForegroundService.TAG, "All commands completed.");
        if (Build$VERSION.SDK_INT >= 26) {
            this.stopForeground(true);
        }
        SystemForegroundService.sForegroundService = null;
        this.stopSelf();
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static void startForeground(final Service service, final int n, final Notification notification, final int n2) {
            service.startForeground(n, notification, n2);
        }
    }
    
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        static void startForeground(final Service service, final int n, final Notification notification, final int n2) {
            try {
                service.startForeground(n, notification, n2);
            }
            catch (final ForegroundServiceStartNotAllowedException ex) {
                Logger.get().warning(SystemForegroundService.TAG, "Unable to start foreground service", (Throwable)ex);
            }
        }
    }
}

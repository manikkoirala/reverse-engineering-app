// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import androidx.work.ForegroundInfo;

public interface ForegroundProcessor
{
    boolean isEnqueuedInForeground(final String p0);
    
    void startForeground(final String p0, final ForegroundInfo p1);
    
    void stopForeground(final String p0);
}

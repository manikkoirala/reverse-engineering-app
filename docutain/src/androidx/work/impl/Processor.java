// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.concurrent.ExecutionException;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Callable;
import androidx.work.WorkerParameters;
import androidx.core.content.ContextCompat;
import androidx.work.impl.utils.WakeLocks;
import androidx.work.ForegroundInfo;
import java.util.Iterator;
import java.util.Collection;
import androidx.work.impl.model.WorkSpec;
import android.content.Intent;
import androidx.work.impl.foreground.SystemForegroundDispatcher;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.List;
import android.os.PowerManager$WakeLock;
import java.util.Map;
import androidx.work.Configuration;
import java.util.Set;
import android.content.Context;
import androidx.work.impl.foreground.ForegroundProcessor;

public class Processor implements ExecutionListener, ForegroundProcessor
{
    private static final String FOREGROUND_WAKELOCK_TAG = "ProcessorForegroundLck";
    private static final String TAG;
    private Context mAppContext;
    private Set<String> mCancelledIds;
    private Configuration mConfiguration;
    private Map<String, WorkerWrapper> mEnqueuedWorkMap;
    private PowerManager$WakeLock mForegroundLock;
    private Map<String, WorkerWrapper> mForegroundWorkMap;
    private final Object mLock;
    private final List<ExecutionListener> mOuterListeners;
    private List<Scheduler> mSchedulers;
    private WorkDatabase mWorkDatabase;
    private Map<String, Set<StartStopToken>> mWorkRuns;
    private TaskExecutor mWorkTaskExecutor;
    
    static {
        TAG = Logger.tagWithPrefix("Processor");
    }
    
    public Processor(final Context mAppContext, final Configuration mConfiguration, final TaskExecutor mWorkTaskExecutor, final WorkDatabase mWorkDatabase, final List<Scheduler> mSchedulers) {
        this.mAppContext = mAppContext;
        this.mConfiguration = mConfiguration;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkDatabase = mWorkDatabase;
        this.mEnqueuedWorkMap = new HashMap<String, WorkerWrapper>();
        this.mForegroundWorkMap = new HashMap<String, WorkerWrapper>();
        this.mSchedulers = mSchedulers;
        this.mCancelledIds = new HashSet<String>();
        this.mOuterListeners = new ArrayList<ExecutionListener>();
        this.mForegroundLock = null;
        this.mLock = new Object();
        this.mWorkRuns = new HashMap<String, Set<StartStopToken>>();
    }
    
    private static boolean interrupt(final String s, final WorkerWrapper workerWrapper) {
        if (workerWrapper != null) {
            workerWrapper.interrupt();
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkerWrapper interrupted for ");
            sb.append(s);
            value.debug(tag, sb.toString());
            return true;
        }
        final Logger value2 = Logger.get();
        final String tag2 = Processor.TAG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("WorkerWrapper could not be found for ");
        sb2.append(s);
        value2.debug(tag2, sb2.toString());
        return false;
    }
    
    private void runOnExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        this.mWorkTaskExecutor.getMainThreadExecutor().execute(new Processor$$ExternalSyntheticLambda1(this, workGenerationalId, b));
    }
    
    private void stopForegroundService() {
        synchronized (this.mLock) {
            if (!(this.mForegroundWorkMap.isEmpty() ^ true)) {
                final Intent stopForegroundIntent = SystemForegroundDispatcher.createStopForegroundIntent(this.mAppContext);
                try {
                    this.mAppContext.startService(stopForegroundIntent);
                }
                finally {
                    final Throwable t;
                    Logger.get().error(Processor.TAG, "Unable to stop foreground service", t);
                }
                final PowerManager$WakeLock mForegroundLock = this.mForegroundLock;
                if (mForegroundLock != null) {
                    mForegroundLock.release();
                    this.mForegroundLock = null;
                }
            }
        }
    }
    
    public void addExecutionListener(final ExecutionListener executionListener) {
        synchronized (this.mLock) {
            this.mOuterListeners.add(executionListener);
        }
    }
    
    public WorkSpec getRunningWorkSpec(final String s) {
        synchronized (this.mLock) {
            WorkerWrapper workerWrapper;
            if ((workerWrapper = this.mForegroundWorkMap.get(s)) == null) {
                workerWrapper = this.mEnqueuedWorkMap.get(s);
            }
            if (workerWrapper != null) {
                return workerWrapper.getWorkSpec();
            }
            return null;
        }
    }
    
    public boolean hasWork() {
        synchronized (this.mLock) {
            return !this.mEnqueuedWorkMap.isEmpty() || !this.mForegroundWorkMap.isEmpty();
        }
    }
    
    public boolean isCancelled(final String s) {
        synchronized (this.mLock) {
            return this.mCancelledIds.contains(s);
        }
    }
    
    public boolean isEnqueued(final String s) {
        synchronized (this.mLock) {
            return this.mEnqueuedWorkMap.containsKey(s) || this.mForegroundWorkMap.containsKey(s);
        }
    }
    
    @Override
    public boolean isEnqueuedInForeground(final String s) {
        synchronized (this.mLock) {
            return this.mForegroundWorkMap.containsKey(s);
        }
    }
    
    @Override
    public void onExecuted(final WorkGenerationalId workGenerationalId, final boolean b) {
        synchronized (this.mLock) {
            final WorkerWrapper workerWrapper = this.mEnqueuedWorkMap.get(workGenerationalId.getWorkSpecId());
            if (workerWrapper != null && workGenerationalId.equals(workerWrapper.getWorkGenerationalId())) {
                this.mEnqueuedWorkMap.remove(workGenerationalId.getWorkSpecId());
            }
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getClass().getSimpleName());
            sb.append(" ");
            sb.append(workGenerationalId.getWorkSpecId());
            sb.append(" executed; reschedule = ");
            sb.append(b);
            value.debug(tag, sb.toString());
            final Iterator<ExecutionListener> iterator = this.mOuterListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().onExecuted(workGenerationalId, b);
            }
        }
    }
    
    public void removeExecutionListener(final ExecutionListener executionListener) {
        synchronized (this.mLock) {
            this.mOuterListeners.remove(executionListener);
        }
    }
    
    @Override
    public void startForeground(final String str, final ForegroundInfo foregroundInfo) {
        synchronized (this.mLock) {
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Moving WorkSpec (");
            sb.append(str);
            sb.append(") to the foreground");
            value.info(tag, sb.toString());
            final WorkerWrapper workerWrapper = this.mEnqueuedWorkMap.remove(str);
            if (workerWrapper != null) {
                if (this.mForegroundLock == null) {
                    (this.mForegroundLock = WakeLocks.newWakeLock(this.mAppContext, "ProcessorForegroundLck")).acquire();
                }
                this.mForegroundWorkMap.put(str, workerWrapper);
                ContextCompat.startForegroundService(this.mAppContext, SystemForegroundDispatcher.createStartForegroundIntent(this.mAppContext, workerWrapper.getWorkGenerationalId(), foregroundInfo));
            }
        }
    }
    
    public boolean startWork(final StartStopToken startStopToken) {
        return this.startWork(startStopToken, null);
    }
    
    public boolean startWork(final StartStopToken e, final WorkerParameters.RuntimeExtras runtimeExtras) {
        final WorkGenerationalId id = e.getId();
        final String workSpecId = id.getWorkSpecId();
        final ArrayList list = new ArrayList();
        final WorkSpec workSpec = this.mWorkDatabase.runInTransaction((Callable<WorkSpec>)new Processor$$ExternalSyntheticLambda0(this, list, workSpecId));
        if (workSpec == null) {
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Didn't find WorkSpec for id ");
            sb.append(id);
            value.warning(tag, sb.toString());
            this.runOnExecuted(id, false);
            return false;
        }
        Object mLock = this.mLock;
        synchronized (mLock) {
            if (this.isEnqueued(workSpecId)) {
                final Set set = this.mWorkRuns.get(workSpecId);
                if (((StartStopToken)set.iterator().next()).getId().getGeneration() == id.getGeneration()) {
                    set.add(e);
                    final Logger value2 = Logger.get();
                    final String tag2 = Processor.TAG;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Work ");
                    sb2.append(id);
                    sb2.append(" is already enqueued for processing");
                    value2.debug(tag2, sb2.toString());
                }
                else {
                    this.runOnExecuted(id, false);
                }
                return false;
            }
            if (workSpec.getGeneration() != id.getGeneration()) {
                this.runOnExecuted(id, false);
                return false;
            }
            final WorkerWrapper build = new WorkerWrapper.Builder(this.mAppContext, this.mConfiguration, this.mWorkTaskExecutor, this, this.mWorkDatabase, workSpec, list).withSchedulers(this.mSchedulers).withRuntimeExtras(runtimeExtras).build();
            final ListenableFuture<Boolean> future = build.getFuture();
            future.addListener((Runnable)new FutureListener(this, e.getId(), future), this.mWorkTaskExecutor.getMainThreadExecutor());
            this.mEnqueuedWorkMap.put(workSpecId, build);
            final HashSet<StartStopToken> set2 = new HashSet<StartStopToken>();
            set2.add(e);
            this.mWorkRuns.put(workSpecId, set2);
            monitorexit(mLock);
            this.mWorkTaskExecutor.getSerialTaskExecutor().execute(build);
            final Logger value3 = Logger.get();
            final String tag3 = Processor.TAG;
            mLock = new StringBuilder();
            ((StringBuilder)mLock).append(this.getClass().getSimpleName());
            ((StringBuilder)mLock).append(": processing ");
            ((StringBuilder)mLock).append(id);
            value3.debug(tag3, ((StringBuilder)mLock).toString());
            return true;
        }
    }
    
    public boolean stopAndCancelWork(final String str) {
        synchronized (this.mLock) {
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Processor cancelling ");
            sb.append(str);
            value.debug(tag, sb.toString());
            this.mCancelledIds.add(str);
            final WorkerWrapper workerWrapper = this.mForegroundWorkMap.remove(str);
            final boolean b = workerWrapper != null;
            WorkerWrapper workerWrapper2 = workerWrapper;
            if (workerWrapper == null) {
                workerWrapper2 = this.mEnqueuedWorkMap.remove(str);
            }
            if (workerWrapper2 != null) {
                this.mWorkRuns.remove(str);
            }
            monitorexit(this.mLock);
            final boolean interrupt = interrupt(str, workerWrapper2);
            if (b) {
                this.stopForegroundService();
            }
            return interrupt;
        }
    }
    
    @Override
    public void stopForeground(final String s) {
        synchronized (this.mLock) {
            this.mForegroundWorkMap.remove(s);
            this.stopForegroundService();
        }
    }
    
    public boolean stopForegroundWork(final StartStopToken startStopToken) {
        final String workSpecId = startStopToken.getId().getWorkSpecId();
        synchronized (this.mLock) {
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Processor stopping foreground work ");
            sb.append(workSpecId);
            value.debug(tag, sb.toString());
            final WorkerWrapper workerWrapper = this.mForegroundWorkMap.remove(workSpecId);
            if (workerWrapper != null) {
                this.mWorkRuns.remove(workSpecId);
            }
            monitorexit(this.mLock);
            return interrupt(workSpecId, workerWrapper);
        }
    }
    
    public boolean stopWork(final StartStopToken startStopToken) {
        final String workSpecId = startStopToken.getId().getWorkSpecId();
        synchronized (this.mLock) {
            final WorkerWrapper workerWrapper = this.mEnqueuedWorkMap.remove(workSpecId);
            if (workerWrapper == null) {
                final Logger value = Logger.get();
                final String tag = Processor.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("WorkerWrapper could not be found for ");
                sb.append(workSpecId);
                value.debug(tag, sb.toString());
                return false;
            }
            final Set set = this.mWorkRuns.get(workSpecId);
            if (set != null && set.contains(startStopToken)) {
                final Logger value2 = Logger.get();
                final String tag2 = Processor.TAG;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Processor stopping background work ");
                sb2.append(workSpecId);
                value2.debug(tag2, sb2.toString());
                this.mWorkRuns.remove(workSpecId);
                monitorexit(this.mLock);
                return interrupt(workSpecId, workerWrapper);
            }
            return false;
        }
    }
    
    private static class FutureListener implements Runnable
    {
        private ExecutionListener mExecutionListener;
        private ListenableFuture<Boolean> mFuture;
        private final WorkGenerationalId mWorkGenerationalId;
        
        FutureListener(final ExecutionListener mExecutionListener, final WorkGenerationalId mWorkGenerationalId, final ListenableFuture<Boolean> mFuture) {
            this.mExecutionListener = mExecutionListener;
            this.mWorkGenerationalId = mWorkGenerationalId;
            this.mFuture = mFuture;
        }
        
        @Override
        public void run() {
            boolean booleanValue;
            try {
                booleanValue = (boolean)this.mFuture.get();
            }
            catch (final InterruptedException | ExecutionException ex) {
                booleanValue = true;
            }
            this.mExecutionListener.onExecuted(this.mWorkGenerationalId, booleanValue);
        }
    }
}

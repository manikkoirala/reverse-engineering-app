// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;
import androidx.work.impl.utils.taskexecutor.SerialExecutor;

public class SerialExecutorImpl implements SerialExecutor
{
    private Runnable mActive;
    private final Executor mExecutor;
    final Object mLock;
    private final ArrayDeque<Task> mTasks;
    
    public SerialExecutorImpl(final Executor mExecutor) {
        this.mExecutor = mExecutor;
        this.mTasks = new ArrayDeque<Task>();
        this.mLock = new Object();
    }
    
    @Override
    public void execute(final Runnable runnable) {
        synchronized (this.mLock) {
            this.mTasks.add(new Task(this, runnable));
            if (this.mActive == null) {
                this.scheduleNext();
            }
        }
    }
    
    public Executor getDelegatedExecutor() {
        return this.mExecutor;
    }
    
    @Override
    public boolean hasPendingTasks() {
        synchronized (this.mLock) {
            return !this.mTasks.isEmpty();
        }
    }
    
    void scheduleNext() {
        final Runnable mActive = this.mTasks.poll();
        this.mActive = mActive;
        if (mActive != null) {
            this.mExecutor.execute(mActive);
        }
    }
    
    static class Task implements Runnable
    {
        final Runnable mRunnable;
        final SerialExecutorImpl mSerialExecutor;
        
        Task(final SerialExecutorImpl mSerialExecutor, final Runnable mRunnable) {
            this.mSerialExecutor = mSerialExecutor;
            this.mRunnable = mRunnable;
        }
        
        @Override
        public void run() {
            try {
                this.mRunnable.run();
                synchronized (this.mSerialExecutor.mLock) {
                    this.mSerialExecutor.scheduleNext();
                }
            }
            finally {
                synchronized (this.mSerialExecutor.mLock) {
                    this.mSerialExecutor.scheduleNext();
                    monitorexit(this.mSerialExecutor.mLock);
                }
            }
        }
    }
}

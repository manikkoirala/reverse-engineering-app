// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.Iterator;
import android.os.PowerManager;
import android.content.Context;
import android.os.PowerManager$WakeLock;
import kotlin.Unit;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u001c\u0010\u0004\u001a\u00060\u0005R\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0001H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "TAG", "", "checkWakeLocks", "", "newWakeLock", "Landroid/os/PowerManager$WakeLock;", "Landroid/os/PowerManager;", "context", "Landroid/content/Context;", "tag", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class WakeLocks
{
    private static final String TAG;
    
    static {
        final String tagWithPrefix = Logger.tagWithPrefix("WakeLocks");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"WakeLocks\")");
        TAG = tagWithPrefix;
    }
    
    public static final void checkWakeLocks() {
        final Map map = new LinkedHashMap();
        Object o = WakeLocksHolder.INSTANCE;
        synchronized (o) {
            map.putAll(WakeLocksHolder.INSTANCE.getWakeLocks());
            final Unit instance = Unit.INSTANCE;
            monitorexit(o);
            o = map.entrySet().iterator();
            while (((Iterator)o).hasNext()) {
                final Map.Entry<PowerManager$WakeLock, V> entry = (Map.Entry<PowerManager$WakeLock, V>)((Iterator)o).next();
                final PowerManager$WakeLock powerManager$WakeLock = entry.getKey();
                final String str = (String)entry.getValue();
                boolean b = true;
                if (powerManager$WakeLock == null || !powerManager$WakeLock.isHeld()) {
                    b = false;
                }
                if (b) {
                    final Logger value = Logger.get();
                    final String tag = WakeLocks.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("WakeLock held for ");
                    sb.append(str);
                    value.warning(tag, sb.toString());
                }
            }
        }
    }
    
    public static final PowerManager$WakeLock newWakeLock(final Context context, String string) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)string, "tag");
        final Object systemService = context.getApplicationContext().getSystemService("power");
        Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.os.PowerManager");
        final PowerManager powerManager = (PowerManager)systemService;
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkManager: ");
        sb.append(string);
        string = sb.toString();
        final PowerManager$WakeLock wakeLock = powerManager.newWakeLock(1, string);
        synchronized (WakeLocksHolder.INSTANCE) {
            string = WakeLocksHolder.INSTANCE.getWakeLocks().put(wakeLock, string);
            monitorexit(WakeLocksHolder.INSTANCE);
            Intrinsics.checkNotNullExpressionValue((Object)wakeLock, "wakeLock");
            return wakeLock;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.concurrent.Callable;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.WorkDatabase;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/work/impl/utils/IdGenerator;", "", "workDatabase", "Landroidx/work/impl/WorkDatabase;", "(Landroidx/work/impl/WorkDatabase;)V", "nextAlarmManagerId", "", "nextJobSchedulerIdWithRange", "minInclusive", "maxInclusive", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class IdGenerator
{
    private final WorkDatabase workDatabase;
    
    public IdGenerator(final WorkDatabase workDatabase) {
        Intrinsics.checkNotNullParameter((Object)workDatabase, "workDatabase");
        this.workDatabase = workDatabase;
    }
    
    private static final Integer nextAlarmManagerId$lambda$1(final IdGenerator idGenerator) {
        Intrinsics.checkNotNullParameter((Object)idGenerator, "this$0");
        return IdGeneratorKt.access$nextId(idGenerator.workDatabase, "next_alarm_manager_id");
    }
    
    private static final Integer nextJobSchedulerIdWithRange$lambda$0(final IdGenerator idGenerator, int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)idGenerator, "this$0");
        final int access$nextId = IdGeneratorKt.access$nextId(idGenerator.workDatabase, "next_job_scheduler_id");
        int n2 = 0;
        if (i <= access$nextId) {
            n2 = n2;
            if (access$nextId <= n) {
                n2 = 1;
            }
        }
        if (n2 == 0) {
            IdGeneratorKt.access$updatePreference(idGenerator.workDatabase, "next_job_scheduler_id", i + 1);
        }
        else {
            i = access$nextId;
        }
        return i;
    }
    
    public final int nextAlarmManagerId() {
        final Number runInTransaction = this.workDatabase.runInTransaction((Callable<Number>)new IdGenerator$$ExternalSyntheticLambda0(this));
        Intrinsics.checkNotNullExpressionValue((Object)runInTransaction, "workDatabase.runInTransa\u2026ANAGER_ID_KEY)\n        })");
        return runInTransaction.intValue();
    }
    
    public final int nextJobSchedulerIdWithRange(final int n, final int n2) {
        final Number runInTransaction = this.workDatabase.runInTransaction((Callable<Number>)new IdGenerator$$ExternalSyntheticLambda1(this, n, n2));
        Intrinsics.checkNotNullExpressionValue((Object)runInTransaction, "workDatabase.runInTransa\u2026            id\n        })");
        return runInTransaction.intValue();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.Iterator;
import androidx.sqlite.db.SimpleSQLiteQuery;
import java.util.UUID;
import androidx.work.impl.model.WorkTypeConverters;
import androidx.work.WorkInfo;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.work.WorkQuery;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import java.util.List;
import java.util.ArrayList;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\t¨\u0006\n" }, d2 = { "bindings", "", "builder", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "count", "", "toRawQuery", "Landroidx/sqlite/db/SupportSQLiteQuery;", "Landroidx/work/WorkQuery;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class RawQueries
{
    private static final void bindings(final StringBuilder sb, final int initialCapacity) {
        if (initialCapacity <= 0) {
            return;
        }
        final ArrayList list = new ArrayList(initialCapacity);
        for (int i = 0; i < initialCapacity; ++i) {
            list.add("?");
        }
        sb.append(CollectionsKt.joinToString$default((Iterable)list, (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null));
    }
    
    public static final SupportSQLiteQuery toRawQuery(final WorkQuery workQuery) {
        Intrinsics.checkNotNullParameter((Object)workQuery, "<this>");
        final List list = new ArrayList();
        final StringBuilder sb = new StringBuilder("SELECT * FROM workspec");
        final List<WorkInfo.State> states = workQuery.getStates();
        Intrinsics.checkNotNullExpressionValue((Object)states, "states");
        final boolean empty = states.isEmpty();
        final String s = " AND";
        String str;
        if (empty ^ true) {
            final List<WorkInfo.State> states2 = workQuery.getStates();
            Intrinsics.checkNotNullExpressionValue((Object)states2, "states");
            final Iterable iterable = states2;
            final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            for (final WorkInfo.State state : iterable) {
                Intrinsics.checkNotNull((Object)state);
                collection.add(WorkTypeConverters.stateToInt(state));
            }
            final List list2 = (List)collection;
            sb.append(" WHERE state IN (");
            bindings(sb, list2.size());
            sb.append(")");
            list.addAll(list2);
            str = " AND";
        }
        else {
            str = " WHERE";
        }
        final List<UUID> ids = workQuery.getIds();
        Intrinsics.checkNotNullExpressionValue((Object)ids, "ids");
        String str2 = str;
        if (ids.isEmpty() ^ true) {
            final List<UUID> ids2 = workQuery.getIds();
            Intrinsics.checkNotNullExpressionValue((Object)ids2, "ids");
            final Iterable iterable2 = ids2;
            final Collection collection2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable2, 10));
            final Iterator iterator2 = iterable2.iterator();
            while (iterator2.hasNext()) {
                collection2.add(((UUID)iterator2.next()).toString());
            }
            final List list3 = (List)collection2;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" id IN (");
            sb.append(sb2.toString());
            bindings(sb, workQuery.getIds().size());
            sb.append(")");
            list.addAll(list3);
            str2 = " AND";
        }
        final List<String> tags = workQuery.getTags();
        Intrinsics.checkNotNullExpressionValue((Object)tags, "tags");
        String str3;
        if (tags.isEmpty() ^ true) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append(" id IN (SELECT work_spec_id FROM worktag WHERE tag IN (");
            sb.append(sb3.toString());
            bindings(sb, workQuery.getTags().size());
            sb.append("))");
            final List<String> tags2 = workQuery.getTags();
            Intrinsics.checkNotNullExpressionValue((Object)tags2, "tags");
            list.addAll(tags2);
            str3 = s;
        }
        else {
            str3 = str2;
        }
        final List<String> uniqueWorkNames = workQuery.getUniqueWorkNames();
        Intrinsics.checkNotNullExpressionValue((Object)uniqueWorkNames, "uniqueWorkNames");
        if (uniqueWorkNames.isEmpty() ^ true) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str3);
            sb4.append(" id IN (SELECT work_spec_id FROM workname WHERE name IN (");
            sb.append(sb4.toString());
            bindings(sb, workQuery.getUniqueWorkNames().size());
            sb.append("))");
            final List<String> uniqueWorkNames2 = workQuery.getUniqueWorkNames();
            Intrinsics.checkNotNullExpressionValue((Object)uniqueWorkNames2, "uniqueWorkNames");
            list.addAll(uniqueWorkNames2);
        }
        sb.append(";");
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "builder.toString()");
        return new SimpleSQLiteQuery(string, list.toArray(new Object[0]));
    }
}

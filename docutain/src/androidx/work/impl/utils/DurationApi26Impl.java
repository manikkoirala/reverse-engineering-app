// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import kotlin.jvm.internal.Intrinsics;
import java.time.Duration;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\f\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0001¨\u0006\u0003" }, d2 = { "toMillisCompat", "", "Ljava/time/Duration;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class DurationApi26Impl
{
    public static final long toMillisCompat(final Duration duration) {
        Intrinsics.checkNotNullParameter((Object)duration, "<this>");
        return duration.toMillis();
    }
}

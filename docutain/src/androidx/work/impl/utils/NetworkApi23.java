// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import kotlin.jvm.internal.Intrinsics;
import android.net.Network;
import android.net.ConnectivityManager;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002H\u0007¨\u0006\u0003" }, d2 = { "getActiveNetworkCompat", "Landroid/net/Network;", "Landroid/net/ConnectivityManager;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkApi23
{
    public static final Network getActiveNetworkCompat(final ConnectivityManager connectivityManager) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        return connectivityManager.getActiveNetwork();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.Context;
import androidx.work.Logger;

public class PackageManagerHelper
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("PackageManagerHelper");
    }
    
    private PackageManagerHelper() {
    }
    
    public static boolean isComponentExplicitlyEnabled(final Context context, final Class<?> clazz) {
        return isComponentExplicitlyEnabled(context, clazz.getName());
    }
    
    public static boolean isComponentExplicitlyEnabled(final Context context, final String s) {
        final int componentEnabledSetting = context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, s));
        boolean b = true;
        if (componentEnabledSetting != 1) {
            b = false;
        }
        return b;
    }
    
    public static void setComponentEnabled(final Context context, final Class<?> clazz, final boolean b) {
        final String s = "enabled";
        try {
            final PackageManager packageManager = context.getPackageManager();
            final ComponentName componentName = new ComponentName(context, clazz.getName());
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = 2;
            }
            packageManager.setComponentEnabledSetting(componentName, n, 1);
            final Logger value = Logger.get();
            final String tag = PackageManagerHelper.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append(clazz.getName());
            sb.append(" ");
            String str;
            if (b) {
                str = "enabled";
            }
            else {
                str = "disabled";
            }
            sb.append(str);
            value.debug(tag, sb.toString());
        }
        catch (final Exception ex) {
            final Logger value2 = Logger.get();
            final String tag2 = PackageManagerHelper.TAG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(clazz.getName());
            sb2.append("could not be ");
            String str2;
            if (b) {
                str2 = s;
            }
            else {
                str2 = "disabled";
            }
            sb2.append(str2);
            value2.debug(tag2, sb2.toString(), ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.concurrent.Executor;

public class SynchronousExecutor implements Executor
{
    @Override
    public void execute(final Runnable runnable) {
        runnable.run();
    }
}

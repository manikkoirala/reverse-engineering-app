// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.ForegroundInfo;
import android.os.Build$VERSION;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.Logger;
import androidx.work.ListenableWorker;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.work.ForegroundUpdater;
import android.content.Context;

public class WorkForegroundRunnable implements Runnable
{
    static final String TAG;
    final Context mContext;
    final ForegroundUpdater mForegroundUpdater;
    final SettableFuture<Void> mFuture;
    final TaskExecutor mTaskExecutor;
    final WorkSpec mWorkSpec;
    final ListenableWorker mWorker;
    
    static {
        TAG = Logger.tagWithPrefix("WorkForegroundRunnable");
    }
    
    public WorkForegroundRunnable(final Context mContext, final WorkSpec mWorkSpec, final ListenableWorker mWorker, final ForegroundUpdater mForegroundUpdater, final TaskExecutor mTaskExecutor) {
        this.mFuture = SettableFuture.create();
        this.mContext = mContext;
        this.mWorkSpec = mWorkSpec;
        this.mWorker = mWorker;
        this.mForegroundUpdater = mForegroundUpdater;
        this.mTaskExecutor = mTaskExecutor;
    }
    
    public ListenableFuture<Void> getFuture() {
        return (ListenableFuture<Void>)this.mFuture;
    }
    
    @Override
    public void run() {
        if (this.mWorkSpec.expedited && Build$VERSION.SDK_INT < 31) {
            final SettableFuture<Object> create = SettableFuture.create();
            this.mTaskExecutor.getMainThreadExecutor().execute(new WorkForegroundRunnable$$ExternalSyntheticLambda0(this, create));
            create.addListener(new Runnable(this, create) {
                final WorkForegroundRunnable this$0;
                final SettableFuture val$foregroundFuture;
                
                @Override
                public void run() {
                    if (this.this$0.mFuture.isCancelled()) {
                        return;
                    }
                    try {
                        final ForegroundInfo foregroundInfo = (ForegroundInfo)this.val$foregroundFuture.get();
                        if (foregroundInfo == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Worker was marked important (");
                            sb.append(this.this$0.mWorkSpec.workerClassName);
                            sb.append(") but did not provide ForegroundInfo");
                            throw new IllegalStateException(sb.toString());
                        }
                        final Logger value = Logger.get();
                        final String tag = WorkForegroundRunnable.TAG;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Updating notification for ");
                        sb2.append(this.this$0.mWorkSpec.workerClassName);
                        value.debug(tag, sb2.toString());
                        this.this$0.mFuture.setFuture(this.this$0.mForegroundUpdater.setForegroundAsync(this.this$0.mContext, this.this$0.mWorker.getId(), foregroundInfo));
                    }
                    finally {
                        final Throwable exception;
                        this.this$0.mFuture.setException(exception);
                    }
                }
            }, this.mTaskExecutor.getMainThreadExecutor());
            return;
        }
        this.mFuture.set(null);
    }
}

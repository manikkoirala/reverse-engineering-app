// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import kotlin.jvm.internal.Intrinsics;
import android.net.ConnectivityManager$NetworkCallback;
import android.net.ConnectivityManager;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005" }, d2 = { "registerDefaultNetworkCallbackCompat", "", "Landroid/net/ConnectivityManager;", "networkCallback", "Landroid/net/ConnectivityManager$NetworkCallback;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkApi24
{
    public static final void registerDefaultNetworkCallbackCompat(final ConnectivityManager connectivityManager, final ConnectivityManager$NetworkCallback connectivityManager$NetworkCallback) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)connectivityManager$NetworkCallback, "networkCallback");
        connectivityManager.registerDefaultNetworkCallback(connectivityManager$NetworkCallback);
    }
}

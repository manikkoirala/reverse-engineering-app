// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.os.Build$VERSION;
import java.util.Iterator;
import java.util.Collection;
import androidx.work.impl.Scheduler;
import java.util.List;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.WorkInfo;
import androidx.work.Data;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.impl.model.WorkSpec;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0000\u001a\u001e\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tH\u0002\u001a\u001e\u0010\n\u001a\u00020\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0002\u001a\u00020\u0001H\u0000¨\u0006\u000b" }, d2 = { "tryDelegateConstrainedWorkSpec", "Landroidx/work/impl/model/WorkSpec;", "workSpec", "usesScheduler", "", "schedulers", "", "Landroidx/work/impl/Scheduler;", "className", "", "wrapInConstraintTrackingWorkerIfNeeded", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class EnqueueUtilsKt
{
    public static final WorkSpec tryDelegateConstrainedWorkSpec(final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)workSpec, "workSpec");
        final Constraints constraints = workSpec.constraints;
        final String workerClassName = workSpec.workerClassName;
        WorkSpec copy$default = workSpec;
        if (!Intrinsics.areEqual((Object)workerClassName, (Object)ConstraintTrackingWorker.class.getName())) {
            if (!constraints.requiresBatteryNotLow()) {
                copy$default = workSpec;
                if (!constraints.requiresStorageNotLow()) {
                    return copy$default;
                }
            }
            final Data build = new Data.Builder().putAll(workSpec.input).putString("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", workerClassName).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder().putAll(workSpe\u2026ame)\n            .build()");
            final String name = ConstraintTrackingWorker.class.getName();
            Intrinsics.checkNotNullExpressionValue((Object)name, "name");
            copy$default = WorkSpec.copy$default(workSpec, null, null, name, null, build, null, 0L, 0L, 0L, null, 0, null, 0L, 0L, 0L, 0L, false, null, 0, 0, 1048555, null);
        }
        return copy$default;
    }
    
    private static final boolean usesScheduler(final List<? extends Scheduler> list, final String className) {
        final boolean b = false;
        try {
            final Class<?> forName = Class.forName(className);
            final Iterable iterable = list;
            boolean b2;
            if (iterable instanceof Collection && ((Collection)iterable).isEmpty()) {
                b2 = b;
            }
            else {
                final Iterator iterator = iterable.iterator();
                do {
                    b2 = b;
                    if (iterator.hasNext()) {
                        continue;
                    }
                    return b2;
                } while (!forName.isAssignableFrom(((Scheduler)iterator.next()).getClass()));
                b2 = true;
            }
            return b2;
        }
        catch (final ClassNotFoundException ex) {
            return b;
        }
    }
    
    public static final WorkSpec wrapInConstraintTrackingWorkerIfNeeded(final List<? extends Scheduler> list, final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)list, "schedulers");
        Intrinsics.checkNotNullParameter((Object)workSpec, "workSpec");
        final int sdk_INT = Build$VERSION.SDK_INT;
        int n = 0;
        if (23 <= sdk_INT) {
            n = n;
            if (sdk_INT < 26) {
                n = 1;
            }
        }
        WorkSpec workSpec2;
        if (n != 0) {
            workSpec2 = tryDelegateConstrainedWorkSpec(workSpec);
        }
        else {
            workSpec2 = workSpec;
            if (Build$VERSION.SDK_INT <= 22) {
                workSpec2 = workSpec;
                if (usesScheduler(list, "androidx.work.impl.background.gcm.GcmScheduler")) {
                    workSpec2 = tryDelegateConstrainedWorkSpec(workSpec);
                }
            }
        }
        return workSpec2;
    }
}

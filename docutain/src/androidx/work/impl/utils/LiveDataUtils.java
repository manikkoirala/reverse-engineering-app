// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.lifecycle.Observer;
import androidx.lifecycle.MediatorLiveData;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;

public class LiveDataUtils
{
    private LiveDataUtils() {
    }
    
    public static <In, Out> LiveData<Out> dedupedMappedLiveDataFor(final LiveData<In> liveData, final Function<In, Out> function, final TaskExecutor taskExecutor) {
        final Object o = new Object();
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<In>(taskExecutor, o, function, mediatorLiveData) {
            Out mCurrentOutput = null;
            final Object val$lock;
            final Function val$mappingMethod;
            final MediatorLiveData val$outputLiveData;
            final TaskExecutor val$workTaskExecutor;
            
            @Override
            public void onChanged(final In in) {
                this.val$workTaskExecutor.executeOnTaskThread(new Runnable(this, in) {
                    final LiveDataUtils$1 this$0;
                    final Object val$input;
                    
                    @Override
                    public void run() {
                        synchronized (this.this$0.val$lock) {
                            final Out apply = this.this$0.val$mappingMethod.apply(this.val$input);
                            if (this.this$0.mCurrentOutput == null && apply != null) {
                                this.this$0.mCurrentOutput = apply;
                                this.this$0.val$outputLiveData.postValue(apply);
                            }
                            else if (this.this$0.mCurrentOutput != null && !this.this$0.mCurrentOutput.equals(apply)) {
                                this.this$0.mCurrentOutput = apply;
                                this.this$0.val$outputLiveData.postValue(apply);
                            }
                        }
                    }
                });
            }
        });
        return mediatorLiveData;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.HashMap;
import androidx.work.Logger;
import androidx.work.RunnableScheduler;
import androidx.work.impl.model.WorkGenerationalId;
import java.util.Map;

public class WorkTimer
{
    private static final String TAG;
    final Map<WorkGenerationalId, TimeLimitExceededListener> mListeners;
    final Object mLock;
    final RunnableScheduler mRunnableScheduler;
    final Map<WorkGenerationalId, WorkTimerRunnable> mTimerMap;
    
    static {
        TAG = Logger.tagWithPrefix("WorkTimer");
    }
    
    public WorkTimer(final RunnableScheduler mRunnableScheduler) {
        this.mTimerMap = new HashMap<WorkGenerationalId, WorkTimerRunnable>();
        this.mListeners = new HashMap<WorkGenerationalId, TimeLimitExceededListener>();
        this.mLock = new Object();
        this.mRunnableScheduler = mRunnableScheduler;
    }
    
    public Map<WorkGenerationalId, TimeLimitExceededListener> getListeners() {
        synchronized (this.mLock) {
            return this.mListeners;
        }
    }
    
    public Map<WorkGenerationalId, WorkTimerRunnable> getTimerMap() {
        synchronized (this.mLock) {
            return this.mTimerMap;
        }
    }
    
    public void startTimer(final WorkGenerationalId obj, final long n, final TimeLimitExceededListener timeLimitExceededListener) {
        synchronized (this.mLock) {
            final Logger value = Logger.get();
            final String tag = WorkTimer.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Starting timer for ");
            sb.append(obj);
            value.debug(tag, sb.toString());
            this.stopTimer(obj);
            final WorkTimerRunnable workTimerRunnable = new WorkTimerRunnable(this, obj);
            this.mTimerMap.put(obj, workTimerRunnable);
            this.mListeners.put(obj, timeLimitExceededListener);
            this.mRunnableScheduler.scheduleWithDelay(n, workTimerRunnable);
        }
    }
    
    public void stopTimer(final WorkGenerationalId obj) {
        synchronized (this.mLock) {
            if (this.mTimerMap.remove(obj) != null) {
                final Logger value = Logger.get();
                final String tag = WorkTimer.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Stopping timer for ");
                sb.append(obj);
                value.debug(tag, sb.toString());
                this.mListeners.remove(obj);
            }
        }
    }
    
    public interface TimeLimitExceededListener
    {
        void onTimeLimitExceeded(final WorkGenerationalId p0);
    }
    
    public static class WorkTimerRunnable implements Runnable
    {
        static final String TAG = "WrkTimerRunnable";
        private final WorkGenerationalId mWorkGenerationalId;
        private final WorkTimer mWorkTimer;
        
        WorkTimerRunnable(final WorkTimer mWorkTimer, final WorkGenerationalId mWorkGenerationalId) {
            this.mWorkTimer = mWorkTimer;
            this.mWorkGenerationalId = mWorkGenerationalId;
        }
        
        @Override
        public void run() {
            synchronized (this.mWorkTimer.mLock) {
                if (this.mWorkTimer.mTimerMap.remove(this.mWorkGenerationalId) != null) {
                    final TimeLimitExceededListener timeLimitExceededListener = this.mWorkTimer.mListeners.remove(this.mWorkGenerationalId);
                    if (timeLimitExceededListener != null) {
                        timeLimitExceededListener.onTimeLimitExceeded(this.mWorkGenerationalId);
                    }
                }
                else {
                    Logger.get().debug("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.mWorkGenerationalId));
                }
            }
        }
    }
}

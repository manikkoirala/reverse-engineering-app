// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.taskexecutor;

import android.os.Looper;
import android.os.Handler;
import java.util.concurrent.Executor;
import androidx.work.impl.utils.SerialExecutorImpl;

public class WorkManagerTaskExecutor implements TaskExecutor
{
    private final SerialExecutorImpl mBackgroundExecutor;
    private final Executor mMainThreadExecutor;
    final Handler mMainThreadHandler;
    
    public WorkManagerTaskExecutor(final Executor executor) {
        this.mMainThreadHandler = new Handler(Looper.getMainLooper());
        this.mMainThreadExecutor = new Executor() {
            final WorkManagerTaskExecutor this$0;
            
            @Override
            public void execute(final Runnable runnable) {
                this.this$0.mMainThreadHandler.post(runnable);
            }
        };
        this.mBackgroundExecutor = new SerialExecutorImpl(executor);
    }
    
    @Override
    public Executor getMainThreadExecutor() {
        return this.mMainThreadExecutor;
    }
    
    @Override
    public SerialExecutorImpl getSerialTaskExecutor() {
        return this.mBackgroundExecutor;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.taskexecutor;

import java.util.concurrent.Executor;

public interface TaskExecutor
{
    void executeOnTaskThread(final Runnable p0);
    
    Executor getMainThreadExecutor();
    
    SerialExecutor getSerialTaskExecutor();
}

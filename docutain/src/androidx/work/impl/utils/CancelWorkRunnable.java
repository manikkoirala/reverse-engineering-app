// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.Schedulers;
import androidx.work.Operation;
import androidx.work.impl.Scheduler;
import androidx.work.impl.model.DependencyDao;
import androidx.work.impl.model.WorkSpecDao;
import java.util.Collection;
import androidx.work.WorkInfo;
import java.util.LinkedList;
import java.util.UUID;
import java.util.Iterator;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.OperationImpl;

public abstract class CancelWorkRunnable implements Runnable
{
    private final OperationImpl mOperation;
    
    public CancelWorkRunnable() {
        this.mOperation = new OperationImpl();
    }
    
    public static CancelWorkRunnable forAll(final WorkManagerImpl workManagerImpl) {
        return new CancelWorkRunnable(workManagerImpl) {
            final WorkManagerImpl val$workManagerImpl;
            
            @Override
            void runInternal() {
                final WorkDatabase workDatabase = this.val$workManagerImpl.getWorkDatabase();
                workDatabase.beginTransaction();
                try {
                    final Iterator<String> iterator = workDatabase.workSpecDao().getAllUnfinishedWork().iterator();
                    while (iterator.hasNext()) {
                        this.cancel(this.val$workManagerImpl, iterator.next());
                    }
                    new PreferenceUtils(this.val$workManagerImpl.getWorkDatabase()).setLastCancelAllTimeMillis(System.currentTimeMillis());
                    workDatabase.setTransactionSuccessful();
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        };
    }
    
    public static CancelWorkRunnable forId(final UUID uuid, final WorkManagerImpl workManagerImpl) {
        return new CancelWorkRunnable(workManagerImpl, uuid) {
            final UUID val$id;
            final WorkManagerImpl val$workManagerImpl;
            
            @Override
            void runInternal() {
                final WorkDatabase workDatabase = this.val$workManagerImpl.getWorkDatabase();
                workDatabase.beginTransaction();
                try {
                    this.cancel(this.val$workManagerImpl, this.val$id.toString());
                    workDatabase.setTransactionSuccessful();
                    workDatabase.endTransaction();
                    this.reschedulePendingWorkers(this.val$workManagerImpl);
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        };
    }
    
    public static CancelWorkRunnable forName(final String s, final WorkManagerImpl workManagerImpl, final boolean b) {
        return new CancelWorkRunnable(workManagerImpl, s, b) {
            final boolean val$allowReschedule;
            final String val$name;
            final WorkManagerImpl val$workManagerImpl;
            
            @Override
            void runInternal() {
                final WorkDatabase workDatabase = this.val$workManagerImpl.getWorkDatabase();
                workDatabase.beginTransaction();
                try {
                    final Iterator<String> iterator = workDatabase.workSpecDao().getUnfinishedWorkWithName(this.val$name).iterator();
                    while (iterator.hasNext()) {
                        this.cancel(this.val$workManagerImpl, iterator.next());
                    }
                    workDatabase.setTransactionSuccessful();
                    workDatabase.endTransaction();
                    if (this.val$allowReschedule) {
                        this.reschedulePendingWorkers(this.val$workManagerImpl);
                    }
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        };
    }
    
    public static CancelWorkRunnable forTag(final String s, final WorkManagerImpl workManagerImpl) {
        return new CancelWorkRunnable(workManagerImpl, s) {
            final String val$tag;
            final WorkManagerImpl val$workManagerImpl;
            
            @Override
            void runInternal() {
                final WorkDatabase workDatabase = this.val$workManagerImpl.getWorkDatabase();
                workDatabase.beginTransaction();
                try {
                    final Iterator<String> iterator = workDatabase.workSpecDao().getUnfinishedWorkWithTag(this.val$tag).iterator();
                    while (iterator.hasNext()) {
                        this.cancel(this.val$workManagerImpl, iterator.next());
                    }
                    workDatabase.setTransactionSuccessful();
                    workDatabase.endTransaction();
                    this.reschedulePendingWorkers(this.val$workManagerImpl);
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        };
    }
    
    private void iterativelyCancelWorkAndDependents(final WorkDatabase workDatabase, String e) {
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final DependencyDao dependencyDao = workDatabase.dependencyDao();
        final LinkedList list = new LinkedList();
        list.add(e);
        while (!list.isEmpty()) {
            e = (String)list.remove();
            final WorkInfo.State state = workSpecDao.getState(e);
            if (state != WorkInfo.State.SUCCEEDED && state != WorkInfo.State.FAILED) {
                workSpecDao.setState(WorkInfo.State.CANCELLED, e);
            }
            list.addAll(dependencyDao.getDependentWorkIds(e));
        }
    }
    
    void cancel(final WorkManagerImpl workManagerImpl, final String s) {
        this.iterativelyCancelWorkAndDependents(workManagerImpl.getWorkDatabase(), s);
        workManagerImpl.getProcessor().stopAndCancelWork(s);
        final Iterator<Scheduler> iterator = workManagerImpl.getSchedulers().iterator();
        while (iterator.hasNext()) {
            iterator.next().cancel(s);
        }
    }
    
    public Operation getOperation() {
        return this.mOperation;
    }
    
    void reschedulePendingWorkers(final WorkManagerImpl workManagerImpl) {
        Schedulers.schedule(workManagerImpl.getConfiguration(), workManagerImpl.getWorkDatabase(), workManagerImpl.getSchedulers());
    }
    
    @Override
    public void run() {
        try {
            this.runInternal();
            this.mOperation.markState((Operation.State)Operation.SUCCESS);
        }
        finally {
            final Throwable t;
            this.mOperation.markState((Operation.State)new Operation.State.FAILURE(t));
        }
    }
    
    abstract void runInternal();
}

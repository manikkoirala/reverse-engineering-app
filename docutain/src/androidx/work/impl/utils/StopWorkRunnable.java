// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.StartStopToken;

public class StopWorkRunnable implements Runnable
{
    private static final String TAG;
    private final boolean mStopInForeground;
    private final StartStopToken mToken;
    private final WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("StopWorkRunnable");
    }
    
    public StopWorkRunnable(final WorkManagerImpl mWorkManagerImpl, final StartStopToken mToken, final boolean mStopInForeground) {
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mToken = mToken;
        this.mStopInForeground = mStopInForeground;
    }
    
    @Override
    public void run() {
        boolean b;
        if (this.mStopInForeground) {
            b = this.mWorkManagerImpl.getProcessor().stopForegroundWork(this.mToken);
        }
        else {
            b = this.mWorkManagerImpl.getProcessor().stopWork(this.mToken);
        }
        final Logger value = Logger.get();
        final String tag = StopWorkRunnable.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("StopWorkRunnable for ");
        sb.append(this.mToken.getId().getWorkSpecId());
        sb.append("; Processor.stopWork = ");
        sb.append(b);
        value.debug(tag, sb.toString());
    }
}

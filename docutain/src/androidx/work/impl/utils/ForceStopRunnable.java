// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.content.BroadcastReceiver;
import androidx.core.util.Consumer;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteTableLockedException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteAccessPermException;
import androidx.work.impl.WorkDatabasePathHelper;
import androidx.work.Configuration;
import android.text.TextUtils;
import android.app.ApplicationExitInfo;
import android.app.ActivityManager;
import androidx.work.impl.Schedulers;
import java.util.Iterator;
import java.util.List;
import androidx.work.impl.model.WorkProgressDao;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.WorkDatabase;
import androidx.work.WorkInfo;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import android.os.Build$VERSION;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import android.content.Context;

public class ForceStopRunnable implements Runnable
{
    static final String ACTION_FORCE_STOP_RESCHEDULE = "ACTION_FORCE_STOP_RESCHEDULE";
    private static final int ALARM_ID = -1;
    private static final long BACKOFF_DURATION_MS = 300L;
    static final int MAX_ATTEMPTS = 3;
    private static final String TAG;
    private static final long TEN_YEARS;
    private final Context mContext;
    private final PreferenceUtils mPreferenceUtils;
    private int mRetryCount;
    private final WorkManagerImpl mWorkManager;
    
    static {
        TAG = Logger.tagWithPrefix("ForceStopRunnable");
        TEN_YEARS = TimeUnit.DAYS.toMillis(3650L);
    }
    
    public ForceStopRunnable(final Context context, final WorkManagerImpl mWorkManager) {
        this.mContext = context.getApplicationContext();
        this.mWorkManager = mWorkManager;
        this.mPreferenceUtils = mWorkManager.getPreferenceUtils();
        this.mRetryCount = 0;
    }
    
    static Intent getIntent(final Context context) {
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, (Class)BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        return intent;
    }
    
    private static PendingIntent getPendingIntent(final Context context, final int n) {
        return PendingIntent.getBroadcast(context, -1, getIntent(context), n);
    }
    
    static void setAlarm(final Context context) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        int n;
        if (Build$VERSION.SDK_INT >= 31) {
            n = 167772160;
        }
        else {
            n = 134217728;
        }
        final PendingIntent pendingIntent = getPendingIntent(context, n);
        final long n2 = System.currentTimeMillis() + ForceStopRunnable.TEN_YEARS;
        if (alarmManager != null) {
            if (Build$VERSION.SDK_INT >= 19) {
                alarmManager.setExact(0, n2, pendingIntent);
            }
            else {
                alarmManager.set(0, n2, pendingIntent);
            }
        }
    }
    
    public boolean cleanUp() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        final boolean b2 = sdk_INT >= 23 && SystemJobScheduler.reconcileJobs(this.mContext, this.mWorkManager);
        final WorkDatabase workDatabase = this.mWorkManager.getWorkDatabase();
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final WorkProgressDao workProgressDao = workDatabase.workProgressDao();
        workDatabase.beginTransaction();
        try {
            final List<WorkSpec> runningWork = workSpecDao.getRunningWork();
            final boolean b3 = runningWork != null && !runningWork.isEmpty();
            if (b3) {
                for (final WorkSpec workSpec : runningWork) {
                    workSpecDao.setState(WorkInfo.State.ENQUEUED, workSpec.id);
                    workSpecDao.markWorkSpecScheduled(workSpec.id, -1L);
                }
            }
            workProgressDao.deleteAll();
            workDatabase.setTransactionSuccessful();
            workDatabase.endTransaction();
            if (b3 || b2) {
                b = true;
            }
            return b;
        }
        finally {
            workDatabase.endTransaction();
        }
    }
    
    public void forceStopRunnable() {
        final boolean cleanUp = this.cleanUp();
        if (this.shouldRescheduleWorkers()) {
            Logger.get().debug(ForceStopRunnable.TAG, "Rescheduling Workers.");
            this.mWorkManager.rescheduleEligibleWork();
            this.mWorkManager.getPreferenceUtils().setNeedsReschedule(false);
        }
        else if (this.isForceStopped()) {
            Logger.get().debug(ForceStopRunnable.TAG, "Application was force-stopped, rescheduling.");
            this.mWorkManager.rescheduleEligibleWork();
            this.mPreferenceUtils.setLastForceStopEventMillis(System.currentTimeMillis());
        }
        else if (cleanUp) {
            Logger.get().debug(ForceStopRunnable.TAG, "Found unfinished work, scheduling it.");
            Schedulers.schedule(this.mWorkManager.getConfiguration(), this.mWorkManager.getWorkDatabase(), this.mWorkManager.getSchedulers());
        }
    }
    
    public boolean isForceStopped() {
        int n = 536870912;
        Object pendingIntent = null;
        try {
            if (Build$VERSION.SDK_INT >= 31) {
                n = 570425344;
            }
            pendingIntent = getPendingIntent(this.mContext, n);
            if (Build$VERSION.SDK_INT >= 30) {
                if (pendingIntent != null) {
                    ((PendingIntent)pendingIntent).cancel();
                }
                final List historicalProcessExitReasons = ((ActivityManager)this.mContext.getSystemService("activity")).getHistoricalProcessExitReasons((String)null, 0, 0);
                if (historicalProcessExitReasons != null && !historicalProcessExitReasons.isEmpty()) {
                    final long lastForceStopEventMillis = this.mPreferenceUtils.getLastForceStopEventMillis();
                    for (int i = 0; i < historicalProcessExitReasons.size(); ++i) {
                        pendingIntent = historicalProcessExitReasons.get(i);
                        if (((ApplicationExitInfo)pendingIntent).getReason() == 10 && ((ApplicationExitInfo)pendingIntent).getTimestamp() >= lastForceStopEventMillis) {
                            return true;
                        }
                    }
                }
            }
            else if (pendingIntent == null) {
                setAlarm(this.mContext);
                return true;
            }
            return false;
        }
        catch (final IllegalArgumentException pendingIntent) {}
        catch (final SecurityException ex) {}
        Logger.get().warning(ForceStopRunnable.TAG, "Ignoring exception", (Throwable)pendingIntent);
        return true;
    }
    
    public boolean multiProcessChecks() {
        final Configuration configuration = this.mWorkManager.getConfiguration();
        if (TextUtils.isEmpty((CharSequence)configuration.getDefaultProcessName())) {
            Logger.get().debug(ForceStopRunnable.TAG, "The default process name was not specified.");
            return true;
        }
        final boolean defaultProcess = ProcessUtils.isDefaultProcess(this.mContext, configuration);
        final Logger value = Logger.get();
        final String tag = ForceStopRunnable.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Is default app process = ");
        sb.append(defaultProcess);
        value.debug(tag, sb.toString());
        return defaultProcess;
    }
    
    @Override
    public void run() {
        try {
            if (!this.multiProcessChecks()) {
                return;
            }
            Label_0019: {
                break Label_0019;
                try {
                    final SQLiteAccessPermException cause;
                    while (true) {
                        WorkDatabasePathHelper.migrateDatabase(this.mContext);
                        Logger.get().debug(ForceStopRunnable.TAG, "Performing cleanup operations.");
                        try {
                            this.forceStopRunnable();
                            return;
                        }
                        catch (final SQLiteAccessPermException cause) {}
                        catch (final SQLiteConstraintException cause) {}
                        catch (final SQLiteTableLockedException cause) {}
                        catch (final SQLiteDatabaseLockedException cause) {}
                        catch (final SQLiteDatabaseCorruptException cause) {}
                        catch (final SQLiteDiskIOException cause) {}
                        catch (final SQLiteCantOpenDatabaseException ex) {}
                        final int mRetryCount = this.mRetryCount + 1;
                        this.mRetryCount = mRetryCount;
                        if (mRetryCount >= 3) {
                            break;
                        }
                        final long n = mRetryCount;
                        final Logger value = Logger.get();
                        final String tag = ForceStopRunnable.TAG;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Retrying after ");
                        sb.append(n * 300L);
                        value.debug(tag, sb.toString(), (Throwable)cause);
                        this.sleep(this.mRetryCount * 300L);
                    }
                    final Logger value2 = Logger.get();
                    final String tag2 = ForceStopRunnable.TAG;
                    value2.error(tag2, "The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", (Throwable)cause);
                    final IllegalStateException ex2 = new IllegalStateException("The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", (Throwable)cause);
                    final Consumer<Throwable> initializationExceptionHandler = this.mWorkManager.getConfiguration().getInitializationExceptionHandler();
                    if (initializationExceptionHandler == null) {
                        throw ex2;
                    }
                    Logger.get().debug(tag2, "Routing exception to the specified exception handler", ex2);
                    initializationExceptionHandler.accept(ex2);
                }
                catch (final SQLiteException cause2) {
                    Logger.get().error(ForceStopRunnable.TAG, "Unexpected SQLite exception during migrations");
                    final IllegalStateException ex3 = new IllegalStateException("Unexpected SQLite exception during migrations", (Throwable)cause2);
                    final Consumer<Throwable> initializationExceptionHandler2 = this.mWorkManager.getConfiguration().getInitializationExceptionHandler();
                    if (initializationExceptionHandler2 == null) {
                        throw ex3;
                    }
                    initializationExceptionHandler2.accept(ex3);
                }
            }
        }
        finally {
            this.mWorkManager.onForceStopRunnableCompleted();
        }
    }
    
    public boolean shouldRescheduleWorkers() {
        return this.mWorkManager.getPreferenceUtils().getNeedsReschedule();
    }
    
    public void sleep(final long n) {
        try {
            Thread.sleep(n);
        }
        catch (final InterruptedException ex) {}
    }
    
    public static class BroadcastReceiver extends android.content.BroadcastReceiver
    {
        private static final String TAG;
        
        static {
            TAG = Logger.tagWithPrefix("ForceStopRunnable$Rcvr");
        }
        
        public void onReceive(final Context alarm, final Intent intent) {
            if (intent != null && "ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
                Logger.get().verbose(BroadcastReceiver.TAG, "Rescheduling alarm that keeps track of force-stops.");
                ForceStopRunnable.setAlarm(alarm);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.Schedulers;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.Operation;
import androidx.work.impl.model.DependencyDao;
import androidx.work.impl.model.WorkSpecDao;
import java.util.Iterator;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.model.WorkName;
import androidx.work.impl.model.Dependency;
import androidx.work.impl.Scheduler;
import java.util.Collections;
import java.util.ArrayList;
import androidx.work.impl.model.WorkSpec;
import android.text.TextUtils;
import androidx.work.WorkInfo;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkRequest;
import java.util.List;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.Logger;
import androidx.work.impl.WorkContinuationImpl;
import androidx.work.impl.OperationImpl;

public class EnqueueRunnable implements Runnable
{
    private static final String TAG;
    private final OperationImpl mOperation;
    private final WorkContinuationImpl mWorkContinuation;
    
    static {
        TAG = Logger.tagWithPrefix("EnqueueRunnable");
    }
    
    public EnqueueRunnable(final WorkContinuationImpl workContinuationImpl) {
        this(workContinuationImpl, new OperationImpl());
    }
    
    public EnqueueRunnable(final WorkContinuationImpl mWorkContinuation, final OperationImpl mOperation) {
        this.mWorkContinuation = mWorkContinuation;
        this.mOperation = mOperation;
    }
    
    private static boolean enqueueContinuation(final WorkContinuationImpl workContinuationImpl) {
        final boolean enqueueWorkWithPrerequisites = enqueueWorkWithPrerequisites(workContinuationImpl.getWorkManagerImpl(), workContinuationImpl.getWork(), WorkContinuationImpl.prerequisitesFor(workContinuationImpl).toArray(new String[0]), workContinuationImpl.getName(), workContinuationImpl.getExistingWorkPolicy());
        workContinuationImpl.markEnqueued();
        return enqueueWorkWithPrerequisites;
    }
    
    private static boolean enqueueWorkWithPrerequisites(final WorkManagerImpl workManagerImpl, final List<? extends WorkRequest> list, String[] array, final String s, final ExistingWorkPolicy existingWorkPolicy) {
        final long currentTimeMillis = System.currentTimeMillis();
        final WorkDatabase workDatabase = workManagerImpl.getWorkDatabase();
        int n;
        if (array != null && array.length > 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        int n6;
        int n7;
        int n8;
        if (n != 0) {
            final int length = array.length;
            int n2 = 0;
            int n3 = 1;
            int n4 = 0;
            int n5 = 0;
            while (true) {
                n6 = n3;
                n7 = n4;
                n8 = n5;
                if (n2 >= length) {
                    break;
                }
                final String str = array[n2];
                final WorkSpec workSpec = workDatabase.workSpecDao().getWorkSpec(str);
                if (workSpec == null) {
                    final Logger value = Logger.get();
                    final String tag = EnqueueRunnable.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Prerequisite ");
                    sb.append(str);
                    sb.append(" doesn't exist; not enqueuing");
                    value.error(tag, sb.toString());
                    return false;
                }
                final WorkInfo.State state = workSpec.state;
                n3 &= ((state == WorkInfo.State.SUCCEEDED) ? 1 : 0);
                int n9;
                if (state == WorkInfo.State.FAILED) {
                    n9 = 1;
                }
                else {
                    n9 = n5;
                    if (state == WorkInfo.State.CANCELLED) {
                        n4 = 1;
                        n9 = n5;
                    }
                }
                ++n2;
                n5 = n9;
            }
        }
        else {
            n6 = 1;
            n7 = 0;
            n8 = 0;
        }
        final boolean b = TextUtils.isEmpty((CharSequence)s) ^ true;
        final boolean b2 = b && n == 0;
        String[] array2 = array;
        int n10 = n;
        int n11 = n6;
        int n12 = n7;
        int n13 = n8;
        boolean b3 = false;
        Label_0808: {
            if (b2) {
                final List<WorkSpec.IdAndState> workSpecIdAndStatesForName = workDatabase.workSpecDao().getWorkSpecIdAndStatesForName(s);
                array2 = array;
                n10 = n;
                n11 = n6;
                n12 = n7;
                n13 = n8;
                if (!workSpecIdAndStatesForName.isEmpty()) {
                    if (existingWorkPolicy != ExistingWorkPolicy.APPEND && existingWorkPolicy != ExistingWorkPolicy.APPEND_OR_REPLACE) {
                        if (existingWorkPolicy == ExistingWorkPolicy.KEEP) {
                            for (final WorkSpec.IdAndState idAndState : workSpecIdAndStatesForName) {
                                if (idAndState.state != WorkInfo.State.ENQUEUED && idAndState.state != WorkInfo.State.RUNNING) {
                                    continue;
                                }
                                return false;
                            }
                        }
                        CancelWorkRunnable.forName(s, workManagerImpl, false).run();
                        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
                        final Iterator iterator2 = workSpecIdAndStatesForName.iterator();
                        while (iterator2.hasNext()) {
                            workSpecDao.delete(((WorkSpec.IdAndState)iterator2.next()).id);
                        }
                        b3 = true;
                        break Label_0808;
                    }
                    final DependencyDao dependencyDao = workDatabase.dependencyDao();
                    final ArrayList list2 = new ArrayList();
                    for (final WorkSpec.IdAndState idAndState2 : workSpecIdAndStatesForName) {
                        boolean b4 = n6 != 0;
                        int n14 = n7;
                        int n15 = n8;
                        if (!dependencyDao.hasDependents(idAndState2.id)) {
                            final boolean b5 = idAndState2.state == WorkInfo.State.SUCCEEDED;
                            if (idAndState2.state == WorkInfo.State.FAILED) {
                                n15 = 1;
                            }
                            else {
                                n15 = n8;
                                if (idAndState2.state == WorkInfo.State.CANCELLED) {
                                    n7 = 1;
                                    n15 = n8;
                                }
                            }
                            list2.add(idAndState2.id);
                            b4 = (((b5 ? 1 : 0) & n6) != 0x0);
                            n14 = n7;
                        }
                        n6 = (b4 ? 1 : 0);
                        n7 = n14;
                        n8 = n15;
                    }
                    n12 = n7;
                    n13 = n8;
                    List<Object> emptyList = list2;
                    Label_0750: {
                        if (existingWorkPolicy == ExistingWorkPolicy.APPEND_OR_REPLACE) {
                            if (n7 == 0) {
                                n12 = n7;
                                n13 = n8;
                                emptyList = list2;
                                if (n8 == 0) {
                                    break Label_0750;
                                }
                            }
                            final WorkSpecDao workSpecDao2 = workDatabase.workSpecDao();
                            final Iterator<WorkSpec.IdAndState> iterator4 = workSpecDao2.getWorkSpecIdAndStatesForName(s).iterator();
                            while (iterator4.hasNext()) {
                                workSpecDao2.delete(iterator4.next().id);
                            }
                            emptyList = Collections.emptyList();
                            n12 = 0;
                            n13 = 0;
                        }
                    }
                    array2 = emptyList.toArray(array);
                    if (array2.length > 0) {
                        n10 = 1;
                        n11 = n6;
                    }
                    else {
                        n10 = 0;
                        n11 = n6;
                    }
                }
            }
            b3 = false;
            n8 = n13;
            n7 = n12;
            n6 = n11;
            n = n10;
            array = array2;
        }
        final Iterator<? extends WorkRequest> iterator5 = list.iterator();
        String[] array3 = array;
        while (iterator5.hasNext()) {
            final WorkRequest workRequest = (WorkRequest)iterator5.next();
            final WorkSpec workSpec2 = workRequest.getWorkSpec();
            if (n != 0 && n6 == 0) {
                if (n8 != 0) {
                    workSpec2.state = WorkInfo.State.FAILED;
                }
                else if (n7 != 0) {
                    workSpec2.state = WorkInfo.State.CANCELLED;
                }
                else {
                    workSpec2.state = WorkInfo.State.BLOCKED;
                }
            }
            else {
                workSpec2.lastEnqueueTime = currentTimeMillis;
            }
            if (workSpec2.state == WorkInfo.State.ENQUEUED) {
                b3 = true;
            }
            workDatabase.workSpecDao().insertWorkSpec(EnqueueUtilsKt.wrapInConstraintTrackingWorkerIfNeeded(workManagerImpl.getSchedulers(), workSpec2));
            array = array3;
            boolean b6 = b3;
            if (n != 0) {
                final int length2 = array3.length;
                int n16 = 0;
                while (true) {
                    array = array3;
                    b6 = b3;
                    if (n16 >= length2) {
                        break;
                    }
                    workDatabase.dependencyDao().insertDependency(new Dependency(workRequest.getStringId(), array3[n16]));
                    ++n16;
                }
            }
            workDatabase.workTagDao().insertTags(workRequest.getStringId(), workRequest.getTags());
            if (b) {
                workDatabase.workNameDao().insert(new WorkName(s, workRequest.getStringId()));
            }
            b3 = b6;
            array3 = array;
        }
        return b3;
    }
    
    private static boolean processContinuation(final WorkContinuationImpl workContinuationImpl) {
        final List<WorkContinuationImpl> parents = workContinuationImpl.getParents();
        int n = 0;
        int n2 = 0;
        if (parents != null) {
            final Iterator<WorkContinuationImpl> iterator = parents.iterator();
            while (true) {
                n = n2;
                if (!iterator.hasNext()) {
                    break;
                }
                final WorkContinuationImpl workContinuationImpl2 = iterator.next();
                if (!workContinuationImpl2.isEnqueued()) {
                    n2 |= (processContinuation(workContinuationImpl2) ? 1 : 0);
                }
                else {
                    final Logger value = Logger.get();
                    final String tag = EnqueueRunnable.TAG;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Already enqueued work ids (");
                    sb.append(TextUtils.join((CharSequence)", ", (Iterable)workContinuationImpl2.getIds()));
                    sb.append(")");
                    value.warning(tag, sb.toString());
                }
            }
        }
        return ((enqueueContinuation(workContinuationImpl) ? 1 : 0) | n) != 0x0;
    }
    
    public boolean addToDatabase() {
        final WorkDatabase workDatabase = this.mWorkContinuation.getWorkManagerImpl().getWorkDatabase();
        workDatabase.beginTransaction();
        try {
            final boolean processContinuation = processContinuation(this.mWorkContinuation);
            workDatabase.setTransactionSuccessful();
            return processContinuation;
        }
        finally {
            workDatabase.endTransaction();
        }
    }
    
    public Operation getOperation() {
        return this.mOperation;
    }
    
    @Override
    public void run() {
        try {
            if (this.mWorkContinuation.hasCycles()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("WorkContinuation has cycles (");
                sb.append(this.mWorkContinuation);
                sb.append(")");
                throw new IllegalStateException(sb.toString());
            }
            if (this.addToDatabase()) {
                PackageManagerHelper.setComponentEnabled(this.mWorkContinuation.getWorkManagerImpl().getApplicationContext(), RescheduleReceiver.class, true);
                this.scheduleWorkInBackground();
            }
            this.mOperation.markState((Operation.State)Operation.SUCCESS);
        }
        finally {
            final Throwable t;
            this.mOperation.markState((Operation.State)new Operation.State.FAILURE(t));
        }
    }
    
    public void scheduleWorkInBackground() {
        final WorkManagerImpl workManagerImpl = this.mWorkContinuation.getWorkManagerImpl();
        Schedulers.schedule(workManagerImpl.getConfiguration(), workManagerImpl.getWorkDatabase(), workManagerImpl.getSchedulers());
    }
}

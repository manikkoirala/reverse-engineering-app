// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.WorkProgress;
import androidx.work.WorkInfo;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.Data;
import java.util.UUID;
import android.content.Context;
import androidx.work.Logger;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.ProgressUpdater;

public class WorkProgressUpdater implements ProgressUpdater
{
    static final String TAG;
    final TaskExecutor mTaskExecutor;
    final WorkDatabase mWorkDatabase;
    
    static {
        TAG = Logger.tagWithPrefix("WorkProgressUpdater");
    }
    
    public WorkProgressUpdater(final WorkDatabase mWorkDatabase, final TaskExecutor mTaskExecutor) {
        this.mWorkDatabase = mWorkDatabase;
        this.mTaskExecutor = mTaskExecutor;
    }
    
    @Override
    public ListenableFuture<Void> updateProgress(final Context context, final UUID uuid, final Data data) {
        final SettableFuture<Object> create = SettableFuture.create();
        this.mTaskExecutor.executeOnTaskThread(new Runnable(this, uuid, data, create) {
            final WorkProgressUpdater this$0;
            final Data val$data;
            final SettableFuture val$future;
            final UUID val$id;
            
            @Override
            public void run() {
                final String string = this.val$id.toString();
                final Logger value = Logger.get();
                final String tag = WorkProgressUpdater.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Updating progress for ");
                sb.append(this.val$id);
                sb.append(" (");
                sb.append(this.val$data);
                sb.append(")");
                value.debug(tag, sb.toString());
                this.this$0.mWorkDatabase.beginTransaction();
                final Throwable t2;
                try {
                    final WorkSpec workSpec = this.this$0.mWorkDatabase.workSpecDao().getWorkSpec(string);
                    if (workSpec != null) {
                        if (workSpec.state == WorkInfo.State.RUNNING) {
                            this.this$0.mWorkDatabase.workProgressDao().insert(new WorkProgress(string, this.val$data));
                        }
                        else {
                            final Logger value2 = Logger.get();
                            final String tag2 = WorkProgressUpdater.TAG;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Ignoring setProgressAsync(...). WorkSpec (");
                            sb2.append(string);
                            sb2.append(") is not in a RUNNING state.");
                            value2.warning(tag2, sb2.toString());
                        }
                        this.val$future.set(null);
                        this.this$0.mWorkDatabase.setTransactionSuccessful();
                        return;
                    }
                    throw new IllegalStateException("Calls to setProgressAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                }
                finally {
                    final Logger logger = Logger.get();
                    final String s = WorkProgressUpdater.TAG;
                    final String s2 = "Error updating Worker progress";
                    final Throwable t = t2;
                    logger.error(s, s2, t);
                    final Runnable runnable = this;
                    final SettableFuture settableFuture = runnable.val$future;
                    final Throwable t3 = t2;
                    settableFuture.setException(t3);
                }
                try {
                    final Logger logger = Logger.get();
                    final String s = WorkProgressUpdater.TAG;
                    final String s2 = "Error updating Worker progress";
                    final Throwable t = t2;
                    logger.error(s, s2, t);
                    final Runnable runnable = this;
                    final SettableFuture settableFuture = runnable.val$future;
                    final Throwable t3 = t2;
                    settableFuture.setException(t3);
                }
                finally {
                    this.this$0.mWorkDatabase.endTransaction();
                }
            }
        });
        return (ListenableFuture<Void>)create;
    }
}

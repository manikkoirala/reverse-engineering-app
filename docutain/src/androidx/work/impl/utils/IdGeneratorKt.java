// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.Preference;
import android.content.SharedPreferences;
import kotlin.jvm.internal.Intrinsics;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.content.Context;
import androidx.work.impl.WorkDatabase;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0000\u001a\u0014\u0010\f\u001a\u00020\u0001*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H\u0002\u001a\u001c\u0010\u000f\u001a\u00020\u0007*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0086T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0086T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "INITIAL_ID", "", "NEXT_ALARM_MANAGER_ID_KEY", "", "NEXT_JOB_SCHEDULER_ID_KEY", "PREFERENCE_FILE_KEY", "migrateLegacyIdGenerator", "", "context", "Landroid/content/Context;", "sqLiteDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "nextId", "Landroidx/work/impl/WorkDatabase;", "key", "updatePreference", "value", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class IdGeneratorKt
{
    public static final int INITIAL_ID = 0;
    public static final String NEXT_ALARM_MANAGER_ID_KEY = "next_alarm_manager_id";
    public static final String NEXT_JOB_SCHEDULER_ID_KEY = "next_job_scheduler_id";
    public static final String PREFERENCE_FILE_KEY = "androidx.work.util.id";
    
    public static final void migrateLegacyIdGenerator(final Context context, final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "sqLiteDatabase");
        final SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (!sharedPreferences.contains("next_job_scheduler_id") && !sharedPreferences.contains("next_job_scheduler_id")) {
            return;
        }
        final int int1 = sharedPreferences.getInt("next_job_scheduler_id", 0);
        final int int2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
        supportSQLiteDatabase.beginTransaction();
        try {
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_job_scheduler_id", int1 });
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_alarm_manager_id", int2 });
            sharedPreferences.edit().clear().apply();
            supportSQLiteDatabase.setTransactionSuccessful();
        }
        finally {
            supportSQLiteDatabase.endTransaction();
        }
    }
    
    private static final int nextId(final WorkDatabase workDatabase, final String s) {
        final Long longValue = workDatabase.preferenceDao().getLongValue(s);
        int n = 0;
        int n2;
        if (longValue != null) {
            n2 = (int)(long)longValue;
        }
        else {
            n2 = 0;
        }
        if (n2 != Integer.MAX_VALUE) {
            n = n2 + 1;
        }
        updatePreference(workDatabase, s, n);
        return n2;
    }
    
    private static final void updatePreference(final WorkDatabase workDatabase, final String s, final int n) {
        workDatabase.preferenceDao().insertPreference(new Preference(s, (long)n));
    }
}

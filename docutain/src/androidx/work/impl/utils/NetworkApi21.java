// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.net.ConnectivityManager$NetworkCallback;
import kotlin.jvm.internal.Intrinsics;
import android.net.NetworkCapabilities;
import android.net.Network;
import android.net.ConnectivityManager;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0001\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\u00012\u0006\u0010\u0007\u001a\u00020\bH\u0001\u001a\u0014\u0010\t\u001a\u00020\n*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\fH\u0001¨\u0006\r" }, d2 = { "getNetworkCapabilitiesCompat", "Landroid/net/NetworkCapabilities;", "Landroid/net/ConnectivityManager;", "network", "Landroid/net/Network;", "hasCapabilityCompat", "", "capability", "", "unregisterNetworkCallbackCompat", "", "networkCallback", "Landroid/net/ConnectivityManager$NetworkCallback;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class NetworkApi21
{
    public static final NetworkCapabilities getNetworkCapabilitiesCompat(final ConnectivityManager connectivityManager, final Network network) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        return connectivityManager.getNetworkCapabilities(network);
    }
    
    public static final boolean hasCapabilityCompat(final NetworkCapabilities networkCapabilities, final int n) {
        Intrinsics.checkNotNullParameter((Object)networkCapabilities, "<this>");
        return networkCapabilities.hasCapability(n);
    }
    
    public static final void unregisterNetworkCallbackCompat(final ConnectivityManager connectivityManager, final ConnectivityManager$NetworkCallback connectivityManager$NetworkCallback) {
        Intrinsics.checkNotNullParameter((Object)connectivityManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)connectivityManager$NetworkCallback, "networkCallback");
        connectivityManager.unregisterNetworkCallback(connectivityManager$NetworkCallback);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.Configuration;
import java.util.Iterator;
import java.util.List;
import java.lang.reflect.Method;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager;
import android.os.Process;
import androidx.work.WorkManager;
import android.os.Build$VERSION;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001c\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0003\u001a\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t" }, d2 = { "TAG", "", "getProcessName", "context", "Landroid/content/Context;", "isDefaultProcess", "", "configuration", "Landroidx/work/Configuration;", "work-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class ProcessUtils
{
    private static final String TAG;
    
    static {
        final String tagWithPrefix = Logger.tagWithPrefix("ProcessUtils");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"ProcessUtils\")");
        TAG = tagWithPrefix;
    }
    
    private static final String getProcessName(final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.INSTANCE.getProcessName();
        }
        final String s = null;
        try {
            final Class<?> forName = Class.forName("android.app.ActivityThread", false, WorkManager.class.getClassLoader());
            Object o;
            if (Build$VERSION.SDK_INT >= 18) {
                final Method declaredMethod = forName.getDeclaredMethod("currentProcessName", (Class[])new Class[0]);
                declaredMethod.setAccessible(true);
                o = declaredMethod.invoke(null, new Object[0]);
                Intrinsics.checkNotNull(o);
            }
            else {
                final Method declaredMethod2 = forName.getDeclaredMethod("currentActivityThread", (Class[])new Class[0]);
                declaredMethod2.setAccessible(true);
                final Method declaredMethod3 = forName.getDeclaredMethod("getProcessName", (Class[])new Class[0]);
                declaredMethod3.setAccessible(true);
                o = declaredMethod3.invoke(declaredMethod2.invoke(null, new Object[0]), new Object[0]);
                Intrinsics.checkNotNull(o);
            }
            if (o instanceof String) {
                return (String)o;
            }
        }
        finally {
            final Throwable t;
            Logger.get().debug(ProcessUtils.TAG, "Unable to check ActivityThread for processName", t);
        }
        final int myPid = Process.myPid();
        final Object systemService = context.getSystemService("activity");
        Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.app.ActivityManager");
        final List runningAppProcesses = ((ActivityManager)systemService).getRunningAppProcesses();
        String processName = s;
        if (runningAppProcesses != null) {
            while (true) {
                for (final Object next : runningAppProcesses) {
                    if (((ActivityManager$RunningAppProcessInfo)next).pid == myPid) {
                        final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo = (ActivityManager$RunningAppProcessInfo)next;
                        processName = s;
                        if (activityManager$RunningAppProcessInfo != null) {
                            processName = activityManager$RunningAppProcessInfo.processName;
                            return processName;
                        }
                        return processName;
                    }
                }
                Object next = null;
                continue;
            }
        }
        return processName;
    }
    
    public static final boolean isDefaultProcess(final Context context, final Configuration configuration) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)configuration, "configuration");
        final String processName = getProcessName(context);
        final CharSequence charSequence = configuration.getDefaultProcessName();
        boolean b;
        if (charSequence != null && charSequence.length() != 0) {
            b = Intrinsics.areEqual((Object)processName, (Object)configuration.getDefaultProcessName());
        }
        else {
            b = Intrinsics.areEqual((Object)processName, (Object)context.getApplicationInfo().processName);
        }
        return b;
    }
}

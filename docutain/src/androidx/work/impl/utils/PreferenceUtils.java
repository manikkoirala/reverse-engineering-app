// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.Preference;
import androidx.lifecycle.Transformations;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import android.content.SharedPreferences;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.content.Context;
import androidx.work.impl.WorkDatabase;

public class PreferenceUtils
{
    public static final String CREATE_PREFERENCE = "CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))";
    public static final String INSERT_PREFERENCE = "INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)";
    public static final String KEY_LAST_CANCEL_ALL_TIME_MS = "last_cancel_all_time_ms";
    private static final String KEY_LAST_FORCE_STOP_MS = "last_force_stop_ms";
    public static final String KEY_RESCHEDULE_NEEDED = "reschedule_needed";
    public static final String PREFERENCES_FILE_NAME = "androidx.work.util.preferences";
    private final WorkDatabase mWorkDatabase;
    
    public PreferenceUtils(final WorkDatabase mWorkDatabase) {
        this.mWorkDatabase = mWorkDatabase;
    }
    
    public static void migrateLegacyPreferences(final Context context, final SupportSQLiteDatabase supportSQLiteDatabase) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (!sharedPreferences.contains("reschedule_needed") && !sharedPreferences.contains("last_cancel_all_time_ms")) {
            return;
        }
        long l = 0L;
        final long long1 = sharedPreferences.getLong("last_cancel_all_time_ms", 0L);
        if (sharedPreferences.getBoolean("reschedule_needed", false)) {
            l = 1L;
        }
        supportSQLiteDatabase.beginTransaction();
        try {
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "last_cancel_all_time_ms", long1 });
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "reschedule_needed", l });
            sharedPreferences.edit().clear().apply();
            supportSQLiteDatabase.setTransactionSuccessful();
        }
        finally {
            supportSQLiteDatabase.endTransaction();
        }
    }
    
    public long getLastCancelAllTimeMillis() {
        final Long longValue = this.mWorkDatabase.preferenceDao().getLongValue("last_cancel_all_time_ms");
        long longValue2;
        if (longValue != null) {
            longValue2 = longValue;
        }
        else {
            longValue2 = 0L;
        }
        return longValue2;
    }
    
    public LiveData<Long> getLastCancelAllTimeMillisLiveData() {
        return Transformations.map(this.mWorkDatabase.preferenceDao().getObservableLongValue("last_cancel_all_time_ms"), new Function<Long, Long>(this) {
            final PreferenceUtils this$0;
            
            @Override
            public Long apply(final Long n) {
                long longValue;
                if (n != null) {
                    longValue = n;
                }
                else {
                    longValue = 0L;
                }
                return longValue;
            }
        });
    }
    
    public long getLastForceStopEventMillis() {
        final Long longValue = this.mWorkDatabase.preferenceDao().getLongValue("last_force_stop_ms");
        if (longValue != null) {
            return longValue;
        }
        return 0L;
    }
    
    public boolean getNeedsReschedule() {
        final Long longValue = this.mWorkDatabase.preferenceDao().getLongValue("reschedule_needed");
        return longValue != null && longValue == 1L;
    }
    
    public void setLastCancelAllTimeMillis(final long l) {
        this.mWorkDatabase.preferenceDao().insertPreference(new Preference("last_cancel_all_time_ms", l));
    }
    
    public void setLastForceStopEventMillis(final long l) {
        this.mWorkDatabase.preferenceDao().insertPreference(new Preference("last_force_stop_ms", l));
    }
    
    public void setNeedsReschedule(final boolean b) {
        this.mWorkDatabase.preferenceDao().insertPreference(new Preference("reschedule_needed", b));
    }
}

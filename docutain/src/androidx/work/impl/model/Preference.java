// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003¢\u0006\u0002\u0010\fJ$\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001¢\u0006\u0002\u0010\u0011J\u0013\u0010\u0012\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001a\u0010\u0004\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004¢\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000b\u0010\f¨\u0006\u0017" }, d2 = { "Landroidx/work/impl/model/Preference;", "", "key", "", "value", "", "(Ljava/lang/String;Z)V", "", "(Ljava/lang/String;Ljava/lang/Long;)V", "getKey", "()Ljava/lang/String;", "getValue", "()Ljava/lang/Long;", "Ljava/lang/Long;", "component1", "component2", "copy", "(Ljava/lang/String;Ljava/lang/Long;)Landroidx/work/impl/model/Preference;", "equals", "other", "hashCode", "", "toString", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class Preference
{
    private final String key;
    private final Long value;
    
    public Preference(final String key, final Long value) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        this.key = key;
        this.value = value;
    }
    
    public Preference(final String s, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        long l;
        if (b) {
            l = 1L;
        }
        else {
            l = 0L;
        }
        this(s, l);
    }
    
    public final String component1() {
        return this.key;
    }
    
    public final Long component2() {
        return this.value;
    }
    
    public final Preference copy(final String s, final Long n) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return new Preference(s, n);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Preference)) {
            return false;
        }
        final Preference preference = (Preference)o;
        return Intrinsics.areEqual((Object)this.key, (Object)preference.key) && Intrinsics.areEqual((Object)this.value, (Object)preference.value);
    }
    
    public final String getKey() {
        return this.key;
    }
    
    public final Long getValue() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.key.hashCode();
        final Long value = this.value;
        int hashCode2;
        if (value == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = value.hashCode();
        }
        return hashCode * 31 + hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Preference(key=");
        sb.append(this.key);
        sb.append(", value=");
        sb.append(this.value);
        sb.append(')');
        return sb.toString();
    }
}

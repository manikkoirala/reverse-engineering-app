// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;
import androidx.work.NetworkType;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo;
import java.util.Collections;
import java.util.List;
import android.database.Cursor;
import java.util.Iterator;
import java.util.Set;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.StringUtil;
import java.util.ArrayList;
import androidx.collection.ArrayMap;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkSpecDao_Impl implements WorkSpecDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkSpec> __insertionAdapterOfWorkSpec;
    private final SharedSQLiteStatement __preparedStmtOfDelete;
    private final SharedSQLiteStatement __preparedStmtOfIncrementGeneration;
    private final SharedSQLiteStatement __preparedStmtOfIncrementPeriodCount;
    private final SharedSQLiteStatement __preparedStmtOfIncrementWorkSpecRunAttemptCount;
    private final SharedSQLiteStatement __preparedStmtOfMarkWorkSpecScheduled;
    private final SharedSQLiteStatement __preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast;
    private final SharedSQLiteStatement __preparedStmtOfResetScheduledState;
    private final SharedSQLiteStatement __preparedStmtOfResetWorkSpecRunAttemptCount;
    private final SharedSQLiteStatement __preparedStmtOfSetLastEnqueuedTime;
    private final SharedSQLiteStatement __preparedStmtOfSetOutput;
    private final SharedSQLiteStatement __preparedStmtOfSetState;
    private final EntityDeletionOrUpdateAdapter<WorkSpec> __updateAdapterOfWorkSpec;
    
    public WorkSpecDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkSpec = new EntityInsertionAdapter<WorkSpec>(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkSpec workSpec) {
                if (workSpec.id == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workSpec.id);
                }
                final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(2, WorkTypeConverters.stateToInt(workSpec.state));
                if (workSpec.workerClassName == null) {
                    supportSQLiteStatement.bindNull(3);
                }
                else {
                    supportSQLiteStatement.bindString(3, workSpec.workerClassName);
                }
                if (workSpec.inputMergerClassName == null) {
                    supportSQLiteStatement.bindNull(4);
                }
                else {
                    supportSQLiteStatement.bindString(4, workSpec.inputMergerClassName);
                }
                final byte[] byteArrayInternal = Data.toByteArrayInternal(workSpec.input);
                if (byteArrayInternal == null) {
                    supportSQLiteStatement.bindNull(5);
                }
                else {
                    supportSQLiteStatement.bindBlob(5, byteArrayInternal);
                }
                final byte[] byteArrayInternal2 = Data.toByteArrayInternal(workSpec.output);
                if (byteArrayInternal2 == null) {
                    supportSQLiteStatement.bindNull(6);
                }
                else {
                    supportSQLiteStatement.bindBlob(6, byteArrayInternal2);
                }
                supportSQLiteStatement.bindLong(7, workSpec.initialDelay);
                supportSQLiteStatement.bindLong(8, workSpec.intervalDuration);
                supportSQLiteStatement.bindLong(9, workSpec.flexDuration);
                supportSQLiteStatement.bindLong(10, workSpec.runAttemptCount);
                final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(11, WorkTypeConverters.backoffPolicyToInt(workSpec.backoffPolicy));
                supportSQLiteStatement.bindLong(12, workSpec.backoffDelayDuration);
                supportSQLiteStatement.bindLong(13, workSpec.lastEnqueueTime);
                supportSQLiteStatement.bindLong(14, workSpec.minimumRetentionDuration);
                supportSQLiteStatement.bindLong(15, workSpec.scheduleRequestedAt);
                supportSQLiteStatement.bindLong(16, workSpec.expedited ? 1 : 0);
                final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(17, WorkTypeConverters.outOfQuotaPolicyToInt(workSpec.outOfQuotaPolicy));
                supportSQLiteStatement.bindLong(18, workSpec.getPeriodCount());
                supportSQLiteStatement.bindLong(19, workSpec.getGeneration());
                final Constraints constraints = workSpec.constraints;
                if (constraints != null) {
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    supportSQLiteStatement.bindLong(20, WorkTypeConverters.networkTypeToInt(constraints.getRequiredNetworkType()));
                    supportSQLiteStatement.bindLong(21, constraints.requiresCharging() ? 1 : 0);
                    supportSQLiteStatement.bindLong(22, constraints.requiresDeviceIdle() ? 1 : 0);
                    supportSQLiteStatement.bindLong(23, constraints.requiresBatteryNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(24, constraints.requiresStorageNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(25, constraints.getContentTriggerUpdateDelayMillis());
                    supportSQLiteStatement.bindLong(26, constraints.getContentTriggerMaxDelayMillis());
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    final byte[] setOfTriggersToByteArray = WorkTypeConverters.setOfTriggersToByteArray(constraints.getContentUriTriggers());
                    if (setOfTriggersToByteArray == null) {
                        supportSQLiteStatement.bindNull(27);
                    }
                    else {
                        supportSQLiteStatement.bindBlob(27, setOfTriggersToByteArray);
                    }
                }
                else {
                    supportSQLiteStatement.bindNull(20);
                    supportSQLiteStatement.bindNull(21);
                    supportSQLiteStatement.bindNull(22);
                    supportSQLiteStatement.bindNull(23);
                    supportSQLiteStatement.bindNull(24);
                    supportSQLiteStatement.bindNull(25);
                    supportSQLiteStatement.bindNull(26);
                    supportSQLiteStatement.bindNull(27);
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`last_enqueue_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`period_count`,`generation`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        };
        this.__updateAdapterOfWorkSpec = new EntityDeletionOrUpdateAdapter<WorkSpec>(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkSpec workSpec) {
                if (workSpec.id == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workSpec.id);
                }
                final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(2, WorkTypeConverters.stateToInt(workSpec.state));
                if (workSpec.workerClassName == null) {
                    supportSQLiteStatement.bindNull(3);
                }
                else {
                    supportSQLiteStatement.bindString(3, workSpec.workerClassName);
                }
                if (workSpec.inputMergerClassName == null) {
                    supportSQLiteStatement.bindNull(4);
                }
                else {
                    supportSQLiteStatement.bindString(4, workSpec.inputMergerClassName);
                }
                final byte[] byteArrayInternal = Data.toByteArrayInternal(workSpec.input);
                if (byteArrayInternal == null) {
                    supportSQLiteStatement.bindNull(5);
                }
                else {
                    supportSQLiteStatement.bindBlob(5, byteArrayInternal);
                }
                final byte[] byteArrayInternal2 = Data.toByteArrayInternal(workSpec.output);
                if (byteArrayInternal2 == null) {
                    supportSQLiteStatement.bindNull(6);
                }
                else {
                    supportSQLiteStatement.bindBlob(6, byteArrayInternal2);
                }
                supportSQLiteStatement.bindLong(7, workSpec.initialDelay);
                supportSQLiteStatement.bindLong(8, workSpec.intervalDuration);
                supportSQLiteStatement.bindLong(9, workSpec.flexDuration);
                supportSQLiteStatement.bindLong(10, workSpec.runAttemptCount);
                final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(11, WorkTypeConverters.backoffPolicyToInt(workSpec.backoffPolicy));
                supportSQLiteStatement.bindLong(12, workSpec.backoffDelayDuration);
                supportSQLiteStatement.bindLong(13, workSpec.lastEnqueueTime);
                supportSQLiteStatement.bindLong(14, workSpec.minimumRetentionDuration);
                supportSQLiteStatement.bindLong(15, workSpec.scheduleRequestedAt);
                supportSQLiteStatement.bindLong(16, workSpec.expedited ? 1 : 0);
                final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                supportSQLiteStatement.bindLong(17, WorkTypeConverters.outOfQuotaPolicyToInt(workSpec.outOfQuotaPolicy));
                supportSQLiteStatement.bindLong(18, workSpec.getPeriodCount());
                supportSQLiteStatement.bindLong(19, workSpec.getGeneration());
                final Constraints constraints = workSpec.constraints;
                if (constraints != null) {
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    supportSQLiteStatement.bindLong(20, WorkTypeConverters.networkTypeToInt(constraints.getRequiredNetworkType()));
                    supportSQLiteStatement.bindLong(21, constraints.requiresCharging() ? 1 : 0);
                    supportSQLiteStatement.bindLong(22, constraints.requiresDeviceIdle() ? 1 : 0);
                    supportSQLiteStatement.bindLong(23, constraints.requiresBatteryNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(24, constraints.requiresStorageNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(25, constraints.getContentTriggerUpdateDelayMillis());
                    supportSQLiteStatement.bindLong(26, constraints.getContentTriggerMaxDelayMillis());
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    final byte[] setOfTriggersToByteArray = WorkTypeConverters.setOfTriggersToByteArray(constraints.getContentUriTriggers());
                    if (setOfTriggersToByteArray == null) {
                        supportSQLiteStatement.bindNull(27);
                    }
                    else {
                        supportSQLiteStatement.bindBlob(27, setOfTriggersToByteArray);
                    }
                }
                else {
                    supportSQLiteStatement.bindNull(20);
                    supportSQLiteStatement.bindNull(21);
                    supportSQLiteStatement.bindNull(22);
                    supportSQLiteStatement.bindNull(23);
                    supportSQLiteStatement.bindNull(24);
                    supportSQLiteStatement.bindNull(25);
                    supportSQLiteStatement.bindNull(26);
                    supportSQLiteStatement.bindNull(27);
                }
                if (workSpec.id == null) {
                    supportSQLiteStatement.bindNull(28);
                }
                else {
                    supportSQLiteStatement.bindString(28, workSpec.id);
                }
            }
            
            public String createQuery() {
                return "UPDATE OR ABORT `WorkSpec` SET `id` = ?,`state` = ?,`worker_class_name` = ?,`input_merger_class_name` = ?,`input` = ?,`output` = ?,`initial_delay` = ?,`interval_duration` = ?,`flex_duration` = ?,`run_attempt_count` = ?,`backoff_policy` = ?,`backoff_delay_duration` = ?,`last_enqueue_time` = ?,`minimum_retention_duration` = ?,`schedule_requested_at` = ?,`run_in_foreground` = ?,`out_of_quota_policy` = ?,`period_count` = ?,`generation` = ?,`required_network_type` = ?,`requires_charging` = ?,`requires_device_idle` = ?,`requires_battery_not_low` = ?,`requires_storage_not_low` = ?,`trigger_content_update_delay` = ?,`trigger_max_content_delay` = ?,`content_uri_triggers` = ? WHERE `id` = ?";
            }
        };
        this.__preparedStmtOfDelete = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM workspec WHERE id=?";
            }
        };
        this.__preparedStmtOfSetState = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET state=? WHERE id=?";
            }
        };
        this.__preparedStmtOfIncrementPeriodCount = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET period_count=period_count+1 WHERE id=?";
            }
        };
        this.__preparedStmtOfSetOutput = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET output=? WHERE id=?";
            }
        };
        this.__preparedStmtOfSetLastEnqueuedTime = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET last_enqueue_time=? WHERE id=?";
            }
        };
        this.__preparedStmtOfIncrementWorkSpecRunAttemptCount = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
            }
        };
        this.__preparedStmtOfResetWorkSpecRunAttemptCount = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
            }
        };
        this.__preparedStmtOfMarkWorkSpecScheduled = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
            }
        };
        this.__preparedStmtOfResetScheduledState = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
            }
        };
        this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
            }
        };
        this.__preparedStmtOfIncrementGeneration = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET generation=generation+1 WHERE id=?";
            }
        };
    }
    
    private void __fetchRelationshipWorkProgressAsandroidxWorkData(final ArrayMap<String, ArrayList<Data>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `progress`,`work_spec_id` FROM `WorkProgress` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                final ArrayList list = arrayMap.get(query.getString(columnIndex));
                if (list != null) {
                    byte[] blob;
                    if (query.isNull(0)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(0);
                    }
                    list.add(Data.fromByteArray(blob));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    private void __fetchRelationshipWorkTagAsjavaLangString(final ArrayMap<String, ArrayList<String>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                final ArrayList list = arrayMap.get(query.getString(columnIndex));
                if (list != null) {
                    Object string;
                    if (query.isNull(0)) {
                        string = null;
                    }
                    else {
                        string = query.getString(0);
                    }
                    list.add(string);
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public void delete(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDelete.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }
    
    @Override
    public List<WorkSpec> getAllEligibleWorkSpecsForScheduling(int columnIndexOrThrow) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE state=0 ORDER BY last_enqueue_time LIMIT ?", 1);
        acquire.bindLong(1, columnIndexOrThrow);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string;
                    if (query.isNull(columnIndexOrThrow2)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow2);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow3);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow4);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow5)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow5);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow7)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow7);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long1 = query.getLong(columnIndexOrThrow8);
                    final long long2 = query.getLong(columnIndexOrThrow9);
                    final long long3 = query.getLong(columnIndexOrThrow10);
                    final int int2 = query.getInt(columnIndexOrThrow11);
                    final int int3 = query.getInt(columnIndexOrThrow12);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow13);
                    final long long5 = query.getLong(columnIndexOrThrow14);
                    final long long6 = query.getLong(columnIndexOrThrow);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    list.add((Object)new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long1, long2, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6));
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<String> getAllUnfinishedWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                Object string;
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add(string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getAllWorkSpecIds() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                Object string;
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add(string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public LiveData<List<String>> getAllWorkSpecIdsLiveData() {
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "workspec" }, true, (Callable<List<String>>)new Callable<List<String>>(this, RoomSQLiteQuery.acquire("SELECT id FROM workspec", 0)) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<String> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, false, null);
                    try {
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            Object string;
                            if (query.isNull(0)) {
                                string = null;
                            }
                            else {
                                string = query.getString(0);
                            }
                            list.add(string);
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<String>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public List<WorkSpec> getEligibleWorkForScheduling(int columnIndexOrThrow) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY last_enqueue_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        acquire.bindLong(1, columnIndexOrThrow);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string;
                    if (query.isNull(columnIndexOrThrow2)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow2);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow3);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow4);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow5)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow5);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow7)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow7);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long1 = query.getLong(columnIndexOrThrow8);
                    final long long2 = query.getLong(columnIndexOrThrow9);
                    final long long3 = query.getLong(columnIndexOrThrow10);
                    final int int2 = query.getInt(columnIndexOrThrow11);
                    final int int3 = query.getInt(columnIndexOrThrow12);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow13);
                    final long long5 = query.getLong(columnIndexOrThrow14);
                    final long long6 = query.getLong(columnIndexOrThrow);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    list.add((Object)new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long1, long2, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6));
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<Data> getInputsFromPrerequisites(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT output FROM workspec WHERE id IN\n             (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                byte[] blob;
                if (query.isNull(0)) {
                    blob = null;
                }
                else {
                    blob = query.getBlob(0);
                }
                list.add((Object)Data.fromByteArray(blob));
            }
            return (List<Data>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<WorkSpec> getRecentlyCompletedWork(long long1) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE last_enqueue_time >= ? AND state IN (2, 3, 5) ORDER BY last_enqueue_time DESC", 1);
        acquire.bindLong(1, long1);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string;
                    if (query.isNull(columnIndexOrThrow)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow2);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow3)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow3);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow4);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow5)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow5);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long2 = query.getLong(columnIndexOrThrow7);
                    long1 = query.getLong(columnIndexOrThrow8);
                    final long long3 = query.getLong(columnIndexOrThrow9);
                    final int int2 = query.getInt(columnIndexOrThrow10);
                    final int int3 = query.getInt(columnIndexOrThrow11);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow12);
                    final long long5 = query.getLong(columnIndexOrThrow13);
                    final long long6 = query.getLong(columnIndexOrThrow14);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    list.add((Object)new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long2, long1, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6));
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<WorkSpec> getRunningWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE state=1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string;
                    if (query.isNull(columnIndexOrThrow)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow2);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow3)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow3);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow4);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow5)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow5);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long1 = query.getLong(columnIndexOrThrow7);
                    final long long2 = query.getLong(columnIndexOrThrow8);
                    final long long3 = query.getLong(columnIndexOrThrow9);
                    final int int2 = query.getInt(columnIndexOrThrow10);
                    final int int3 = query.getInt(columnIndexOrThrow11);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow12);
                    final long long5 = query.getLong(columnIndexOrThrow13);
                    final long long6 = query.getLong(columnIndexOrThrow14);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    list.add((Object)new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long1, long2, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6));
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public LiveData<Long> getScheduleRequestedAtLiveData(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT schedule_requested_at FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "workspec" }, false, (Callable<Long>)new Callable<Long>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public Long call() throws Exception {
                final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, false, null);
                try {
                    long long1;
                    if (query.moveToFirst()) {
                        long1 = query.getLong(0);
                    }
                    else {
                        long1 = 0L;
                    }
                    query.close();
                    return long1;
                }
                finally {
                    query.close();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public List<WorkSpec> getScheduledWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string;
                    if (query.isNull(columnIndexOrThrow)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow2);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow3)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow3);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow4);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow5)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow5);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long1 = query.getLong(columnIndexOrThrow7);
                    final long long2 = query.getLong(columnIndexOrThrow8);
                    final long long3 = query.getLong(columnIndexOrThrow9);
                    final int int2 = query.getInt(columnIndexOrThrow10);
                    final int int3 = query.getInt(columnIndexOrThrow11);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow12);
                    final long long5 = query.getLong(columnIndexOrThrow13);
                    final long long6 = query.getLong(columnIndexOrThrow14);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    list.add((Object)new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long1, long2, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6));
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public WorkInfo.State getState(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT state FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        final WorkInfo.State state = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        WorkInfo.State intToState = state;
        try {
            if (query.moveToFirst()) {
                Integer value;
                if (query.isNull(0)) {
                    value = null;
                }
                else {
                    value = query.getInt(0);
                }
                if (value == null) {
                    intToState = state;
                }
                else {
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    intToState = WorkTypeConverters.intToState(value);
                }
            }
            return intToState;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getUnfinishedWorkWithName(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getUnfinishedWorkWithTag(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public WorkSpec getWorkSpec(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM workspec WHERE id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "output");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "last_enqueue_time");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "period_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "generation");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
                final int columnIndexOrThrow26 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
                final int columnIndexOrThrow27 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
                WorkSpec workSpec;
                if (query.moveToFirst()) {
                    if (query.isNull(columnIndexOrThrow)) {
                        string = null;
                    }
                    else {
                        string = query.getString(columnIndexOrThrow);
                    }
                    final int int1 = query.getInt(columnIndexOrThrow2);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    String string2;
                    if (query.isNull(columnIndexOrThrow3)) {
                        string2 = null;
                    }
                    else {
                        string2 = query.getString(columnIndexOrThrow3);
                    }
                    String string3;
                    if (query.isNull(columnIndexOrThrow4)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(columnIndexOrThrow4);
                    }
                    byte[] blob;
                    if (query.isNull(columnIndexOrThrow5)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndexOrThrow5);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    byte[] blob2;
                    if (query.isNull(columnIndexOrThrow6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = query.getBlob(columnIndexOrThrow6);
                    }
                    final Data fromByteArray2 = Data.fromByteArray(blob2);
                    final long long1 = query.getLong(columnIndexOrThrow7);
                    final long long2 = query.getLong(columnIndexOrThrow8);
                    final long long3 = query.getLong(columnIndexOrThrow9);
                    final int int2 = query.getInt(columnIndexOrThrow10);
                    final int int3 = query.getInt(columnIndexOrThrow11);
                    final WorkTypeConverters instance2 = WorkTypeConverters.INSTANCE;
                    final BackoffPolicy intToBackoffPolicy = WorkTypeConverters.intToBackoffPolicy(int3);
                    final long long4 = query.getLong(columnIndexOrThrow12);
                    final long long5 = query.getLong(columnIndexOrThrow13);
                    final long long6 = query.getLong(columnIndexOrThrow14);
                    final long long7 = query.getLong(columnIndexOrThrow15);
                    final boolean b = query.getInt(columnIndexOrThrow16) != 0;
                    final int int4 = query.getInt(columnIndexOrThrow17);
                    final WorkTypeConverters instance3 = WorkTypeConverters.INSTANCE;
                    final OutOfQuotaPolicy intToOutOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(int4);
                    final int int5 = query.getInt(columnIndexOrThrow18);
                    final int int6 = query.getInt(columnIndexOrThrow19);
                    final int int7 = query.getInt(columnIndexOrThrow20);
                    final WorkTypeConverters instance4 = WorkTypeConverters.INSTANCE;
                    final NetworkType intToNetworkType = WorkTypeConverters.intToNetworkType(int7);
                    final boolean b2 = query.getInt(columnIndexOrThrow21) != 0;
                    final boolean b3 = query.getInt(columnIndexOrThrow22) != 0;
                    final boolean b4 = query.getInt(columnIndexOrThrow23) != 0;
                    final boolean b5 = query.getInt(columnIndexOrThrow24) != 0;
                    final long long8 = query.getLong(columnIndexOrThrow25);
                    final long long9 = query.getLong(columnIndexOrThrow26);
                    byte[] blob3;
                    if (query.isNull(columnIndexOrThrow27)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = query.getBlob(columnIndexOrThrow27);
                    }
                    final WorkTypeConverters instance5 = WorkTypeConverters.INSTANCE;
                    workSpec = new WorkSpec(string, intToState, string2, string3, fromByteArray, fromByteArray2, long1, long2, long3, new Constraints(intToNetworkType, b2, b3, b4, b5, long8, long9, WorkTypeConverters.byteArrayToSetOfTriggers(blob3)), int2, intToBackoffPolicy, long4, long5, long6, long7, b, intToOutOfQuotaPolicy, int5, int6);
                }
                else {
                    workSpec = null;
                }
                query.close();
                acquire.release();
                return workSpec;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<WorkSpec.IdAndState> getWorkSpecIdAndStatesForName(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                final int int1 = query.getInt(1);
                final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                list.add((Object)new WorkSpec.IdAndState(string, WorkTypeConverters.intToState(int1)));
            }
            return (List<WorkSpec.IdAndState>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public WorkSpec.WorkInfoPojo getWorkStatusPojoForId(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final RoomDatabase _db = this.__db;
            WorkSpec.WorkInfoPojo workInfoPojo = null;
            byte[] blob = null;
            final Cursor query = DBUtil.query(_db, acquire, true, null);
            try {
                final ArrayMap arrayMap = new ArrayMap();
                final ArrayMap arrayMap2 = new ArrayMap();
                while (query.moveToNext()) {
                    final String string2 = query.getString(0);
                    if (arrayMap.get(string2) == null) {
                        arrayMap.put(string2, new ArrayList());
                    }
                    final String string3 = query.getString(0);
                    if (arrayMap2.get(string3) == null) {
                        arrayMap2.put(string3, new ArrayList());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                if (query.moveToFirst()) {
                    if (query.isNull(0)) {
                        string = null;
                    }
                    else {
                        string = query.getString(0);
                    }
                    final int int1 = query.getInt(1);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    if (!query.isNull(2)) {
                        blob = query.getBlob(2);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    final int int2 = query.getInt(3);
                    final int int3 = query.getInt(4);
                    ArrayList list;
                    if ((list = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                        list = new ArrayList();
                    }
                    ArrayList list2;
                    if ((list2 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                        list2 = new ArrayList();
                    }
                    workInfoPojo = new WorkSpec.WorkInfoPojo(string, intToState, fromByteArray, int2, int3, list, list2);
                }
                this.__db.setTransactionSuccessful();
                return workInfoPojo;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForIds(final List<String> list) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN (");
        final int size = list.size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final ArrayMap arrayMap = new ArrayMap();
                final ArrayMap arrayMap2 = new ArrayMap();
                while (query.moveToNext()) {
                    final String string = query.getString(0);
                    if (arrayMap.get(string) == null) {
                        arrayMap.put(string, new ArrayList());
                    }
                    final String string2 = query.getString(0);
                    if (arrayMap2.get(string2) == null) {
                        arrayMap2.put(string2, new ArrayList());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                final ArrayList list2 = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    String string3;
                    if (query.isNull(0)) {
                        string3 = null;
                    }
                    else {
                        string3 = query.getString(0);
                    }
                    final int int1 = query.getInt(1);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    byte[] blob;
                    if (query.isNull(2)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(2);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    final int int2 = query.getInt(3);
                    final int int3 = query.getInt(4);
                    ArrayList list3;
                    if ((list3 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                        list3 = new ArrayList();
                    }
                    ArrayList list4;
                    if ((list4 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                        list4 = new ArrayList();
                    }
                    list2.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list3, list4));
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list2;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForName(String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final ArrayMap arrayMap = new ArrayMap();
                final ArrayMap arrayMap2 = new ArrayMap();
                while (query.moveToNext()) {
                    final String string = query.getString(0);
                    if (arrayMap.get(string) == null) {
                        arrayMap.put(string, new ArrayList());
                    }
                    s = query.getString(0);
                    if (arrayMap2.get(s) == null) {
                        arrayMap2.put(s, new ArrayList());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    if (query.isNull(0)) {
                        s = null;
                    }
                    else {
                        s = query.getString(0);
                    }
                    final int int1 = query.getInt(1);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    byte[] blob;
                    if (query.isNull(2)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(2);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    final int int2 = query.getInt(3);
                    final int int3 = query.getInt(4);
                    ArrayList list2;
                    if ((list2 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                        list2 = new ArrayList();
                    }
                    ArrayList list3;
                    if ((list3 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                        list3 = new ArrayList();
                    }
                    list.add((Object)new WorkSpec.WorkInfoPojo(s, intToState, fromByteArray, int2, int3, list2, list3));
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForTag(String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN\n            (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final ArrayMap arrayMap = new ArrayMap();
                final ArrayMap arrayMap2 = new ArrayMap();
                while (query.moveToNext()) {
                    s = query.getString(0);
                    if (arrayMap.get(s) == null) {
                        arrayMap.put(s, new ArrayList());
                    }
                    final String string = query.getString(0);
                    if (arrayMap2.get(string) == null) {
                        arrayMap2.put(string, new ArrayList());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    if (query.isNull(0)) {
                        s = null;
                    }
                    else {
                        s = query.getString(0);
                    }
                    final int int1 = query.getInt(1);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                    byte[] blob;
                    if (query.isNull(2)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(2);
                    }
                    final Data fromByteArray = Data.fromByteArray(blob);
                    final int int2 = query.getInt(3);
                    final int int3 = query.getInt(4);
                    ArrayList list2;
                    if ((list2 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                        list2 = new ArrayList();
                    }
                    ArrayList list3;
                    if ((list3 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                        list3 = new ArrayList();
                    }
                    list.add((Object)new WorkSpec.WorkInfoPojo(s, intToState, fromByteArray, int2, int3, list2, list3));
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForIds(final List<String> list) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN (");
        final int size = list.size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            final String string = query.getString(0);
                            if (arrayMap.get(string) == null) {
                                arrayMap.put(string, new ArrayList());
                            }
                            final String string2 = query.getString(0);
                            if (arrayMap2.get(string2) == null) {
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            String string3;
                            if (query.isNull(0)) {
                                string3 = null;
                            }
                            else {
                                string3 = query.getString(0);
                            }
                            final int int1 = query.getInt(1);
                            final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                            final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                            byte[] blob;
                            if (query.isNull(2)) {
                                blob = null;
                            }
                            else {
                                blob = query.getBlob(2);
                            }
                            final Data fromByteArray = Data.fromByteArray(blob);
                            final int int2 = query.getInt(3);
                            final int int3 = query.getInt(4);
                            ArrayList list2;
                            if ((list2 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                                list2 = new ArrayList();
                            }
                            ArrayList list3;
                            if ((list3 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                                list3 = new ArrayList();
                            }
                            list.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list2, list3));
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForName(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec", "workname" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            final String string = query.getString(0);
                            if (arrayMap.get(string) == null) {
                                arrayMap.put(string, new ArrayList());
                            }
                            final String string2 = query.getString(0);
                            if (arrayMap2.get(string2) == null) {
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            String string3;
                            if (query.isNull(0)) {
                                string3 = null;
                            }
                            else {
                                string3 = query.getString(0);
                            }
                            final int int1 = query.getInt(1);
                            final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                            final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                            byte[] blob;
                            if (query.isNull(2)) {
                                blob = null;
                            }
                            else {
                                blob = query.getBlob(2);
                            }
                            final Data fromByteArray = Data.fromByteArray(blob);
                            final int int2 = query.getInt(3);
                            final int int3 = query.getInt(4);
                            ArrayList list2;
                            if ((list2 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                                list2 = new ArrayList();
                            }
                            ArrayList list3;
                            if ((list3 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                                list3 = new ArrayList();
                            }
                            list.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list2, list3));
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForTag(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count, generation FROM workspec WHERE id IN\n            (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec", "worktag" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            final String string = query.getString(0);
                            if (arrayMap.get(string) == null) {
                                arrayMap.put(string, new ArrayList());
                            }
                            final String string2 = query.getString(0);
                            if (arrayMap2.get(string2) == null) {
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            String string3;
                            if (query.isNull(0)) {
                                string3 = null;
                            }
                            else {
                                string3 = query.getString(0);
                            }
                            final int int1 = query.getInt(1);
                            final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                            final WorkInfo.State intToState = WorkTypeConverters.intToState(int1);
                            byte[] blob;
                            if (query.isNull(2)) {
                                blob = null;
                            }
                            else {
                                blob = query.getBlob(2);
                            }
                            final Data fromByteArray = Data.fromByteArray(blob);
                            final int int2 = query.getInt(3);
                            final int int3 = query.getInt(4);
                            ArrayList list2;
                            if ((list2 = (ArrayList)arrayMap.get(query.getString(0))) == null) {
                                list2 = new ArrayList();
                            }
                            ArrayList list3;
                            if ((list3 = (ArrayList)arrayMap2.get(query.getString(0))) == null) {
                                list3 = new ArrayList();
                            }
                            list.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list2, list3));
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public boolean hasUnfinishedWork() {
        final boolean b = false;
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT COUNT(*) > 0 FROM workspec WHERE state NOT IN (2, 3, 5) LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        boolean b2 = b;
        try {
            if (query.moveToFirst()) {
                final int int1 = query.getInt(0);
                b2 = b;
                if (int1 != 0) {
                    b2 = true;
                }
            }
            return b2;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void incrementGeneration(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfIncrementGeneration.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfIncrementGeneration.release(acquire);
        }
    }
    
    @Override
    public void incrementPeriodCount(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfIncrementPeriodCount.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfIncrementPeriodCount.release(acquire);
        }
    }
    
    @Override
    public int incrementWorkSpecRunAttemptCount(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfIncrementWorkSpecRunAttemptCount.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfIncrementWorkSpecRunAttemptCount.release(acquire);
        }
    }
    
    @Override
    public void insertWorkSpec(final WorkSpec workSpec) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkSpec.insert(workSpec);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public int markWorkSpecScheduled(final String s, final long n) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfMarkWorkSpecScheduled.acquire();
        acquire.bindLong(1, n);
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfMarkWorkSpecScheduled.release(acquire);
        }
    }
    
    @Override
    public void pruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast.release(acquire);
        }
    }
    
    @Override
    public int resetScheduledState() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfResetScheduledState.acquire();
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetScheduledState.release(acquire);
        }
    }
    
    @Override
    public int resetWorkSpecRunAttemptCount(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfResetWorkSpecRunAttemptCount.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetWorkSpecRunAttemptCount.release(acquire);
        }
    }
    
    @Override
    public void setLastEnqueuedTime(final String s, final long n) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfSetLastEnqueuedTime.acquire();
        acquire.bindLong(1, n);
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetLastEnqueuedTime.release(acquire);
        }
    }
    
    @Override
    public void setOutput(final String s, final Data data) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfSetOutput.acquire();
        final byte[] byteArrayInternal = Data.toByteArrayInternal(data);
        if (byteArrayInternal == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindBlob(1, byteArrayInternal);
        }
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetOutput.release(acquire);
        }
    }
    
    @Override
    public int setState(final WorkInfo.State state, final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfSetState.acquire();
        final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
        acquire.bindLong(1, WorkTypeConverters.stateToInt(state));
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetState.release(acquire);
        }
    }
    
    @Override
    public void updateWorkSpec(final WorkSpec workSpec) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__updateAdapterOfWorkSpec.handle(workSpec);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

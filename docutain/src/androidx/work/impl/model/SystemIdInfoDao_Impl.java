// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.ArrayList;
import android.database.Cursor;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.Collections;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class SystemIdInfoDao_Impl implements SystemIdInfoDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<SystemIdInfo> __insertionAdapterOfSystemIdInfo;
    private final SharedSQLiteStatement __preparedStmtOfRemoveSystemIdInfo;
    private final SharedSQLiteStatement __preparedStmtOfRemoveSystemIdInfo_1;
    
    public SystemIdInfoDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfSystemIdInfo = new EntityInsertionAdapter<SystemIdInfo>(this, _db) {
            final SystemIdInfoDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final SystemIdInfo systemIdInfo) {
                if (systemIdInfo.workSpecId == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, systemIdInfo.workSpecId);
                }
                supportSQLiteStatement.bindLong(2, systemIdInfo.getGeneration());
                supportSQLiteStatement.bindLong(3, systemIdInfo.systemId);
            }
            
            public String createQuery() {
                return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`generation`,`system_id`) VALUES (?,?,?)";
            }
        };
        this.__preparedStmtOfRemoveSystemIdInfo = new SharedSQLiteStatement(this, _db) {
            final SystemIdInfoDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM SystemIdInfo where work_spec_id=? AND generation=?";
            }
        };
        this.__preparedStmtOfRemoveSystemIdInfo_1 = new SharedSQLiteStatement(this, _db) {
            final SystemIdInfoDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM SystemIdInfo where work_spec_id=?";
            }
        };
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public SystemIdInfo getSystemIdInfo(final WorkGenerationalId workGenerationalId) {
        return DefaultImpls.getSystemIdInfo(this, workGenerationalId);
    }
    
    @Override
    public SystemIdInfo getSystemIdInfo(String string, int columnIndexOrThrow) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT * FROM SystemIdInfo WHERE work_spec_id=? AND generation=?", 2);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        acquire.bindLong(2, columnIndexOrThrow);
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        SystemIdInfo systemIdInfo = null;
        final String s = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "work_spec_id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "generation");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "system_id");
            if (query.moveToFirst()) {
                if (query.isNull(columnIndexOrThrow)) {
                    string = s;
                }
                else {
                    string = query.getString(columnIndexOrThrow);
                }
                systemIdInfo = new SystemIdInfo(string, query.getInt(columnIndexOrThrow2), query.getInt(columnIndexOrThrow3));
            }
            return systemIdInfo;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getWorkSpecIds() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT DISTINCT work_spec_id FROM SystemIdInfo", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                Object string;
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add(string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insertSystemIdInfo(final SystemIdInfo systemIdInfo) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSystemIdInfo.insert(systemIdInfo);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public void removeSystemIdInfo(final WorkGenerationalId workGenerationalId) {
        DefaultImpls.removeSystemIdInfo(this, workGenerationalId);
    }
    
    @Override
    public void removeSystemIdInfo(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfRemoveSystemIdInfo_1.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveSystemIdInfo_1.release(acquire);
        }
    }
    
    @Override
    public void removeSystemIdInfo(final String s, final int n) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfRemoveSystemIdInfo.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        acquire.bindLong(2, n);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveSystemIdInfo.release(acquire);
        }
    }
}

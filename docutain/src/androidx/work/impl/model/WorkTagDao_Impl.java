// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.Set;
import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.Collections;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkTagDao_Impl implements WorkTagDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkTag> __insertionAdapterOfWorkTag;
    private final SharedSQLiteStatement __preparedStmtOfDeleteByWorkSpecId;
    
    public WorkTagDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkTag = new EntityInsertionAdapter<WorkTag>(this, _db) {
            final WorkTagDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkTag workTag) {
                if (workTag.getTag() == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workTag.getTag());
                }
                if (workTag.getWorkSpecId() == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindString(2, workTag.getWorkSpecId());
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
            }
        };
        this.__preparedStmtOfDeleteByWorkSpecId = new SharedSQLiteStatement(this, _db) {
            final WorkTagDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM worktag WHERE work_spec_id=?";
            }
        };
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public void deleteByWorkSpecId(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDeleteByWorkSpecId.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteByWorkSpecId.release(acquire);
        }
    }
    
    @Override
    public List<String> getTagsForWorkSpecId(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getWorkSpecIdsWithTag(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT work_spec_id FROM worktag WHERE tag=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final WorkTag workTag) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkTag.insert(workTag);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public void insertTags(final String s, final Set<String> set) {
        DefaultImpls.insertTags(this, s, set);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import androidx.work.WorkInfo;
import androidx.work.OutOfQuotaPolicy;
import android.os.Build$VERSION;
import androidx.work.NetworkType;
import java.io.IOException;
import kotlin.io.CloseableKt;
import kotlin.Unit;
import android.net.Uri;
import java.io.ObjectInputStream;
import java.io.InputStream;
import java.io.Closeable;
import java.io.ByteArrayInputStream;
import java.util.LinkedHashSet;
import androidx.work.Constraints;
import java.util.Set;
import kotlin.jvm.JvmStatic;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.BackoffPolicy;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0004\u001c\u001d\u001e\u001fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u0004H\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\u0004H\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u0004H\u0007J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u0004H\u0007J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u000fH\u0007J\u0010\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0011H\u0007J\u0016\u0010\u0018\u001a\u00020\u000b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0007J\u0010\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u0013H\u0007¨\u0006 " }, d2 = { "Landroidx/work/impl/model/WorkTypeConverters;", "", "()V", "backoffPolicyToInt", "", "backoffPolicy", "Landroidx/work/BackoffPolicy;", "byteArrayToSetOfTriggers", "", "Landroidx/work/Constraints$ContentUriTrigger;", "bytes", "", "intToBackoffPolicy", "value", "intToNetworkType", "Landroidx/work/NetworkType;", "intToOutOfQuotaPolicy", "Landroidx/work/OutOfQuotaPolicy;", "intToState", "Landroidx/work/WorkInfo$State;", "networkTypeToInt", "networkType", "outOfQuotaPolicyToInt", "policy", "setOfTriggersToByteArray", "triggers", "stateToInt", "state", "BackoffPolicyIds", "NetworkTypeIds", "OutOfPolicyIds", "StateIds", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class WorkTypeConverters
{
    public static final WorkTypeConverters INSTANCE;
    
    static {
        INSTANCE = new WorkTypeConverters();
    }
    
    private WorkTypeConverters() {
    }
    
    @JvmStatic
    public static final int backoffPolicyToInt(final BackoffPolicy backoffPolicy) {
        Intrinsics.checkNotNullParameter((Object)backoffPolicy, "backoffPolicy");
        final int n = WhenMappings.$EnumSwitchMapping$1[backoffPolicy.ordinal()];
        int n2 = 1;
        if (n != 1) {
            if (n != 2) {
                throw new NoWhenBranchMatchedException();
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    @JvmStatic
    public static final Set<Constraints.ContentUriTrigger> byteArrayToSetOfTriggers(byte[] buf) {
        Intrinsics.checkNotNullParameter((Object)buf, "bytes");
        final Set set = new LinkedHashSet();
        final int length = buf.length;
        final int n = 0;
        if (length == 0) {
            return set;
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        buf = (byte[])(Object)byteArrayInputStream;
        try {
            final ByteArrayInputStream byteArrayInputStream2 = (Object)buf;
            try {
                final Closeable closeable = new ObjectInputStream(byteArrayInputStream);
                try {
                    final ObjectInputStream objectInputStream = (ObjectInputStream)closeable;
                    for (int int1 = objectInputStream.readInt(), i = n; i < int1; ++i) {
                        final Uri parse = Uri.parse(objectInputStream.readUTF());
                        final boolean boolean1 = objectInputStream.readBoolean();
                        Intrinsics.checkNotNullExpressionValue((Object)parse, "uri");
                        set.add(new Constraints.ContentUriTrigger(parse, boolean1));
                    }
                    final Unit instance = Unit.INSTANCE;
                    CloseableKt.closeFinally(closeable, (Throwable)null);
                }
                finally {
                    try {}
                    finally {
                        final Throwable t;
                        CloseableKt.closeFinally(closeable, t);
                    }
                }
            }
            catch (final IOException ex) {
                ex.printStackTrace();
            }
            final Unit instance2 = Unit.INSTANCE;
            CloseableKt.closeFinally((Closeable)(Object)buf, (Throwable)null);
            return set;
        }
        finally {
            try {}
            finally {
                final Throwable t2;
                CloseableKt.closeFinally((Closeable)(Object)buf, t2);
            }
        }
    }
    
    @JvmStatic
    public static final BackoffPolicy intToBackoffPolicy(final int i) {
        BackoffPolicy backoffPolicy;
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not convert ");
                sb.append(i);
                sb.append(" to BackoffPolicy");
                throw new IllegalArgumentException(sb.toString());
            }
            backoffPolicy = BackoffPolicy.LINEAR;
        }
        else {
            backoffPolicy = BackoffPolicy.EXPONENTIAL;
        }
        return backoffPolicy;
    }
    
    @JvmStatic
    public static final NetworkType intToNetworkType(final int i) {
        NetworkType networkType;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            if (Build$VERSION.SDK_INT >= 30 && i == 5) {
                                return NetworkType.TEMPORARILY_UNMETERED;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Could not convert ");
                            sb.append(i);
                            sb.append(" to NetworkType");
                            throw new IllegalArgumentException(sb.toString());
                        }
                        else {
                            networkType = NetworkType.METERED;
                        }
                    }
                    else {
                        networkType = NetworkType.NOT_ROAMING;
                    }
                }
                else {
                    networkType = NetworkType.UNMETERED;
                }
            }
            else {
                networkType = NetworkType.CONNECTED;
            }
        }
        else {
            networkType = NetworkType.NOT_REQUIRED;
        }
        return networkType;
    }
    
    @JvmStatic
    public static final OutOfQuotaPolicy intToOutOfQuotaPolicy(final int i) {
        OutOfQuotaPolicy outOfQuotaPolicy;
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not convert ");
                sb.append(i);
                sb.append(" to OutOfQuotaPolicy");
                throw new IllegalArgumentException(sb.toString());
            }
            outOfQuotaPolicy = OutOfQuotaPolicy.DROP_WORK_REQUEST;
        }
        else {
            outOfQuotaPolicy = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        }
        return outOfQuotaPolicy;
    }
    
    @JvmStatic
    public static final WorkInfo.State intToState(final int i) {
        WorkInfo.State state;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            if (i != 5) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Could not convert ");
                                sb.append(i);
                                sb.append(" to State");
                                throw new IllegalArgumentException(sb.toString());
                            }
                            state = WorkInfo.State.CANCELLED;
                        }
                        else {
                            state = WorkInfo.State.BLOCKED;
                        }
                    }
                    else {
                        state = WorkInfo.State.FAILED;
                    }
                }
                else {
                    state = WorkInfo.State.SUCCEEDED;
                }
            }
            else {
                state = WorkInfo.State.RUNNING;
            }
        }
        else {
            state = WorkInfo.State.ENQUEUED;
        }
        return state;
    }
    
    @JvmStatic
    public static final int networkTypeToInt(final NetworkType obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "networkType");
        final int n = WhenMappings.$EnumSwitchMapping$2[obj.ordinal()];
        int n2 = 5;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n != 5) {
                            if (Build$VERSION.SDK_INT < 30 || obj != NetworkType.TEMPORARILY_UNMETERED) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Could not convert ");
                                sb.append(obj);
                                sb.append(" to int");
                                throw new IllegalArgumentException(sb.toString());
                            }
                        }
                        else {
                            n2 = 4;
                        }
                    }
                    else {
                        n2 = 3;
                    }
                }
                else {
                    n2 = 2;
                }
            }
            else {
                n2 = 1;
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    @JvmStatic
    public static final int outOfQuotaPolicyToInt(final OutOfQuotaPolicy outOfQuotaPolicy) {
        Intrinsics.checkNotNullParameter((Object)outOfQuotaPolicy, "policy");
        final int n = WhenMappings.$EnumSwitchMapping$3[outOfQuotaPolicy.ordinal()];
        int n2 = 1;
        if (n != 1) {
            if (n != 2) {
                throw new NoWhenBranchMatchedException();
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    @JvmStatic
    public static final byte[] setOfTriggersToByteArray(final Set<Constraints.ContentUriTrigger> set) {
        Intrinsics.checkNotNullParameter((Object)set, "triggers");
        if (set.isEmpty()) {
            return new byte[0];
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final Closeable closeable = byteArrayOutputStream;
        try {
            final ByteArrayOutputStream byteArrayOutputStream2 = (ByteArrayOutputStream)closeable;
            final Closeable closeable2 = new ObjectOutputStream(byteArrayOutputStream);
            try {
                final ObjectOutputStream objectOutputStream = (ObjectOutputStream)closeable2;
                objectOutputStream.writeInt(set.size());
                for (final Constraints.ContentUriTrigger contentUriTrigger : set) {
                    objectOutputStream.writeUTF(contentUriTrigger.getUri().toString());
                    objectOutputStream.writeBoolean(contentUriTrigger.isTriggeredForDescendants());
                }
                final Unit instance = Unit.INSTANCE;
                CloseableKt.closeFinally(closeable2, (Throwable)null);
                final Unit instance2 = Unit.INSTANCE;
                CloseableKt.closeFinally(closeable, (Throwable)null);
                final byte[] byteArray = byteArrayOutputStream.toByteArray();
                Intrinsics.checkNotNullExpressionValue((Object)byteArray, "outputStream.toByteArray()");
                return byteArray;
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    CloseableKt.closeFinally(closeable2, t);
                }
            }
        }
        finally {
            try {}
            finally {
                final Throwable t2;
                CloseableKt.closeFinally(closeable, t2);
            }
        }
    }
    
    @JvmStatic
    public static final int stateToInt(final WorkInfo.State state) {
        Intrinsics.checkNotNullParameter((Object)state, "state");
        int n = 0;
        switch (WhenMappings.$EnumSwitchMapping$0[state.ordinal()]) {
            default: {
                throw new NoWhenBranchMatchedException();
            }
            case 6: {
                n = 5;
                break;
            }
            case 5: {
                n = 4;
                break;
            }
            case 4: {
                n = 3;
                break;
            }
            case 3: {
                n = 2;
                break;
            }
            case 2: {
                n = 1;
                break;
            }
            case 1: {
                n = 0;
                break;
            }
        }
        return n;
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Landroidx/work/impl/model/WorkTypeConverters$BackoffPolicyIds;", "", "()V", "EXPONENTIAL", "", "LINEAR", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class BackoffPolicyIds
    {
        public static final int EXPONENTIAL = 0;
        public static final BackoffPolicyIds INSTANCE;
        public static final int LINEAR = 1;
        
        static {
            INSTANCE = new BackoffPolicyIds();
        }
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/work/impl/model/WorkTypeConverters$NetworkTypeIds;", "", "()V", "CONNECTED", "", "METERED", "NOT_REQUIRED", "NOT_ROAMING", "TEMPORARILY_UNMETERED", "UNMETERED", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class NetworkTypeIds
    {
        public static final int CONNECTED = 1;
        public static final NetworkTypeIds INSTANCE;
        public static final int METERED = 4;
        public static final int NOT_REQUIRED = 0;
        public static final int NOT_ROAMING = 3;
        public static final int TEMPORARILY_UNMETERED = 5;
        public static final int UNMETERED = 2;
        
        static {
            INSTANCE = new NetworkTypeIds();
        }
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Landroidx/work/impl/model/WorkTypeConverters$OutOfPolicyIds;", "", "()V", "DROP_WORK_REQUEST", "", "RUN_AS_NON_EXPEDITED_WORK_REQUEST", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class OutOfPolicyIds
    {
        public static final int DROP_WORK_REQUEST = 1;
        public static final OutOfPolicyIds INSTANCE;
        public static final int RUN_AS_NON_EXPEDITED_WORK_REQUEST = 0;
        
        static {
            INSTANCE = new OutOfPolicyIds();
        }
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Landroidx/work/impl/model/WorkTypeConverters$StateIds;", "", "()V", "BLOCKED", "", "CANCELLED", "COMPLETED_STATES", "", "ENQUEUED", "FAILED", "RUNNING", "SUCCEEDED", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class StateIds
    {
        public static final int BLOCKED = 4;
        public static final int CANCELLED = 5;
        public static final String COMPLETED_STATES = "(2, 3, 5)";
        public static final int ENQUEUED = 0;
        public static final int FAILED = 3;
        public static final StateIds INSTANCE;
        public static final int RUNNING = 1;
        public static final int SUCCEEDED = 2;
        
        static {
            INSTANCE = new StateIds();
        }
        
        private StateIds() {
        }
    }
}

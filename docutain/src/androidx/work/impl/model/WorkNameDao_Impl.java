// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.Collections;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkNameDao_Impl implements WorkNameDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkName> __insertionAdapterOfWorkName;
    
    public WorkNameDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkName = new EntityInsertionAdapter<WorkName>(this, _db) {
            final WorkNameDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkName workName) {
                if (workName.getName() == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workName.getName());
                }
                if (workName.getWorkSpecId() == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindString(2, workName.getWorkSpecId());
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
            }
        };
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public List<String> getNamesForWorkSpecId(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT name FROM workname WHERE work_spec_id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getWorkSpecIdsWithName(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT work_spec_id FROM workname WHERE name=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final WorkName workName) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkName.insert(workName);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

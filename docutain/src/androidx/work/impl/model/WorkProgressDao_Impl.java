// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.Collections;
import java.util.List;
import androidx.work.Data;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkProgressDao_Impl implements WorkProgressDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkProgress> __insertionAdapterOfWorkProgress;
    private final SharedSQLiteStatement __preparedStmtOfDelete;
    private final SharedSQLiteStatement __preparedStmtOfDeleteAll;
    
    public WorkProgressDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkProgress = new EntityInsertionAdapter<WorkProgress>(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkProgress workProgress) {
                if (workProgress.getWorkSpecId() == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workProgress.getWorkSpecId());
                }
                final byte[] byteArrayInternal = Data.toByteArrayInternal(workProgress.getProgress());
                if (byteArrayInternal == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindBlob(2, byteArrayInternal);
                }
            }
            
            public String createQuery() {
                return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
            }
        };
        this.__preparedStmtOfDelete = new SharedSQLiteStatement(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE from WorkProgress where work_spec_id=?";
            }
        };
        this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM WorkProgress";
            }
        };
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public void delete(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDelete.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }
    
    @Override
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }
    
    @Override
    public Data getProgressForWorkSpecId(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT progress FROM WorkProgress WHERE work_spec_id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        final Data data = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        Data fromByteArray = data;
        try {
            if (query.moveToFirst()) {
                byte[] blob;
                if (query.isNull(0)) {
                    blob = null;
                }
                else {
                    blob = query.getBlob(0);
                }
                if (blob == null) {
                    fromByteArray = data;
                }
                else {
                    fromByteArray = Data.fromByteArray(blob);
                }
            }
            return fromByteArray;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final WorkProgress workProgress) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkProgress.insert(workProgress);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

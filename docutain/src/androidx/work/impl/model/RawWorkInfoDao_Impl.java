// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;
import androidx.work.WorkInfo;
import java.util.Collections;
import java.util.List;
import android.database.Cursor;
import java.util.Iterator;
import java.util.Set;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.StringUtil;
import androidx.work.Data;
import java.util.ArrayList;
import androidx.collection.ArrayMap;
import androidx.room.RoomDatabase;

public final class RawWorkInfoDao_Impl implements RawWorkInfoDao
{
    private final RoomDatabase __db;
    
    public RawWorkInfoDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
    }
    
    private void __fetchRelationshipWorkProgressAsandroidxWorkData(final ArrayMap<String, ArrayList<Data>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `progress`,`work_spec_id` FROM `WorkProgress` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                final ArrayList list = arrayMap.get(query.getString(columnIndex));
                if (list != null) {
                    byte[] blob;
                    if (query.isNull(0)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(0);
                    }
                    list.add(Data.fromByteArray(blob));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    private void __fetchRelationshipWorkTagAsjavaLangString(final ArrayMap<String, ArrayList<String>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                final ArrayList list = arrayMap.get(query.getString(columnIndex));
                if (list != null) {
                    Object string;
                    if (query.isNull(0)) {
                        string = null;
                    }
                    else {
                        string = query.getString(0);
                    }
                    list.add(string);
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkInfoPojos(final SupportSQLiteQuery supportSQLiteQuery) {
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, supportSQLiteQuery, true, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "id");
            final int columnIndex2 = CursorUtil.getColumnIndex(query, "state");
            final int columnIndex3 = CursorUtil.getColumnIndex(query, "output");
            final int columnIndex4 = CursorUtil.getColumnIndex(query, "run_attempt_count");
            final int columnIndex5 = CursorUtil.getColumnIndex(query, "generation");
            final ArrayMap arrayMap = new ArrayMap();
            final ArrayMap arrayMap2 = new ArrayMap();
            while (query.moveToNext()) {
                final String string = query.getString(columnIndex);
                if (arrayMap.get(string) == null) {
                    arrayMap.put(string, new ArrayList());
                }
                final String string2 = query.getString(columnIndex);
                if (arrayMap2.get(string2) == null) {
                    arrayMap2.put(string2, new ArrayList());
                }
            }
            query.moveToPosition(-1);
            this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
            this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                String string3;
                if (columnIndex != -1 && !query.isNull(columnIndex)) {
                    string3 = query.getString(columnIndex);
                }
                else {
                    string3 = null;
                }
                WorkInfo.State intToState;
                if (columnIndex2 == -1) {
                    intToState = null;
                }
                else {
                    final int int1 = query.getInt(columnIndex2);
                    final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                    intToState = WorkTypeConverters.intToState(int1);
                }
                Data fromByteArray;
                if (columnIndex3 == -1) {
                    fromByteArray = null;
                }
                else {
                    byte[] blob;
                    if (query.isNull(columnIndex3)) {
                        blob = null;
                    }
                    else {
                        blob = query.getBlob(columnIndex3);
                    }
                    fromByteArray = Data.fromByteArray(blob);
                }
                int int2;
                if (columnIndex4 == -1) {
                    int2 = 0;
                }
                else {
                    int2 = query.getInt(columnIndex4);
                }
                int int3;
                if (columnIndex5 == -1) {
                    int3 = 0;
                }
                else {
                    int3 = query.getInt(columnIndex5);
                }
                ArrayList list2;
                if ((list2 = (ArrayList)arrayMap.get(query.getString(columnIndex))) == null) {
                    list2 = new ArrayList();
                }
                ArrayList list3;
                if ((list3 = (ArrayList)arrayMap2.get(query.getString(columnIndex))) == null) {
                    list3 = new ArrayList();
                }
                list.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list2, list3));
            }
            return (List<WorkSpec.WorkInfoPojo>)list;
        }
        finally {
            query.close();
        }
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkInfoPojosLiveData(final SupportSQLiteQuery supportSQLiteQuery) {
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "WorkSpec" }, false, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, supportSQLiteQuery) {
            final RawWorkInfoDao_Impl this$0;
            final SupportSQLiteQuery val$_internalQuery;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                final Cursor query = DBUtil.query(this.this$0.__db, this.val$_internalQuery, true, null);
                try {
                    final int columnIndex = CursorUtil.getColumnIndex(query, "id");
                    final int columnIndex2 = CursorUtil.getColumnIndex(query, "state");
                    final int columnIndex3 = CursorUtil.getColumnIndex(query, "output");
                    final int columnIndex4 = CursorUtil.getColumnIndex(query, "run_attempt_count");
                    final int columnIndex5 = CursorUtil.getColumnIndex(query, "generation");
                    final ArrayMap arrayMap = new ArrayMap();
                    final ArrayMap arrayMap2 = new ArrayMap();
                    while (query.moveToNext()) {
                        final String string = query.getString(columnIndex);
                        if (arrayMap.get(string) == null) {
                            arrayMap.put(string, new ArrayList());
                        }
                        final String string2 = query.getString(columnIndex);
                        if (arrayMap2.get(string2) == null) {
                            arrayMap2.put(string2, new ArrayList());
                        }
                    }
                    query.moveToPosition(-1);
                    this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                    this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                    final ArrayList list = new ArrayList(query.getCount());
                    while (query.moveToNext()) {
                        String string3;
                        if (columnIndex != -1 && !query.isNull(columnIndex)) {
                            string3 = query.getString(columnIndex);
                        }
                        else {
                            string3 = null;
                        }
                        WorkInfo.State intToState;
                        if (columnIndex2 == -1) {
                            intToState = null;
                        }
                        else {
                            final int int1 = query.getInt(columnIndex2);
                            final WorkTypeConverters instance = WorkTypeConverters.INSTANCE;
                            intToState = WorkTypeConverters.intToState(int1);
                        }
                        Data fromByteArray;
                        if (columnIndex3 == -1) {
                            fromByteArray = null;
                        }
                        else {
                            byte[] blob;
                            if (query.isNull(columnIndex3)) {
                                blob = null;
                            }
                            else {
                                blob = query.getBlob(columnIndex3);
                            }
                            fromByteArray = Data.fromByteArray(blob);
                        }
                        int int2;
                        if (columnIndex4 == -1) {
                            int2 = 0;
                        }
                        else {
                            int2 = query.getInt(columnIndex4);
                        }
                        int int3;
                        if (columnIndex5 == -1) {
                            int3 = 0;
                        }
                        else {
                            int3 = query.getInt(columnIndex5);
                        }
                        ArrayList list2;
                        if ((list2 = (ArrayList)arrayMap.get(query.getString(columnIndex))) == null) {
                            list2 = new ArrayList();
                        }
                        ArrayList list3;
                        if ((list3 = (ArrayList)arrayMap2.get(query.getString(columnIndex))) == null) {
                            list3 = new ArrayList();
                        }
                        list.add((Object)new WorkSpec.WorkInfoPojo(string3, intToState, fromByteArray, int2, int3, list2, list3));
                    }
                    return (List<WorkSpec.WorkInfoPojo>)list;
                }
                finally {
                    query.close();
                }
            }
        });
    }
}

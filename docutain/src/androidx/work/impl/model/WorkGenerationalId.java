// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0013" }, d2 = { "Landroidx/work/impl/model/WorkGenerationalId;", "", "workSpecId", "", "generation", "", "(Ljava/lang/String;I)V", "getGeneration", "()I", "getWorkSpecId", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class WorkGenerationalId
{
    private final int generation;
    private final String workSpecId;
    
    public WorkGenerationalId(final String workSpecId, final int generation) {
        Intrinsics.checkNotNullParameter((Object)workSpecId, "workSpecId");
        this.workSpecId = workSpecId;
        this.generation = generation;
    }
    
    public final String component1() {
        return this.workSpecId;
    }
    
    public final int component2() {
        return this.generation;
    }
    
    public final WorkGenerationalId copy(final String s, final int n) {
        Intrinsics.checkNotNullParameter((Object)s, "workSpecId");
        return new WorkGenerationalId(s, n);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WorkGenerationalId)) {
            return false;
        }
        final WorkGenerationalId workGenerationalId = (WorkGenerationalId)o;
        return Intrinsics.areEqual((Object)this.workSpecId, (Object)workGenerationalId.workSpecId) && this.generation == workGenerationalId.generation;
    }
    
    public final int getGeneration() {
        return this.generation;
    }
    
    public final String getWorkSpecId() {
        return this.workSpecId;
    }
    
    @Override
    public int hashCode() {
        return this.workSpecId.hashCode() * 31 + this.generation;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkGenerationalId(workSpecId=");
        sb.append(this.workSpecId);
        sb.append(", generation=");
        sb.append(this.generation);
        sb.append(')');
        return sb.toString();
    }
}

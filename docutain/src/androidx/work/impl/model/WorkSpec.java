// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.UUID;
import kotlin.ranges.RangesKt;
import java.util.Iterator;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import androidx.work.Logger;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.Data;
import androidx.work.Constraints;
import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo;
import java.util.List;
import androidx.arch.core.util.Function;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b%\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0087\b\u0018\u0000 J2\u00020\u0001:\u0003JKLB\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0000¢\u0006\u0002\u0010\bB\u00cb\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0015\u0012\b\b\u0003\u0010\u0016\u001a\u00020\u0017\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0019\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001f\u0012\b\b\u0002\u0010 \u001a\u00020!\u0012\b\b\u0002\u0010\"\u001a\u00020\u0017\u0012\b\b\u0002\u0010#\u001a\u00020\u0017¢\u0006\u0002\u0010$J\u0006\u0010-\u001a\u00020\u0011J\t\u0010.\u001a\u00020\u0003H\u00c6\u0003J\t\u0010/\u001a\u00020\u0015H\u00c6\u0003J\t\u00100\u001a\u00020\u0017H\u00c6\u0003J\t\u00101\u001a\u00020\u0019H\u00c6\u0003J\t\u00102\u001a\u00020\u0011H\u00c6\u0003J\t\u00103\u001a\u00020\u0011H\u00c6\u0003J\t\u00104\u001a\u00020\u0011H\u00c6\u0003J\t\u00105\u001a\u00020\u0011H\u00c6\u0003J\t\u00106\u001a\u00020\u001fH\u00c6\u0003J\t\u00107\u001a\u00020!H\u00c6\u0003J\t\u00108\u001a\u00020\u0017H\u00c6\u0003J\t\u00109\u001a\u00020\nH\u00c6\u0003J\t\u0010:\u001a\u00020\u0017H\u00c6\u0003J\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010<\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010=\u001a\u00020\u000eH\u00c6\u0003J\t\u0010>\u001a\u00020\u000eH\u00c6\u0003J\t\u0010?\u001a\u00020\u0011H\u00c6\u0003J\t\u0010@\u001a\u00020\u0011H\u00c6\u0003J\t\u0010A\u001a\u00020\u0011H\u00c6\u0003J\u00d3\u0001\u0010B\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u000e2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0013\u001a\u00020\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00152\b\b\u0003\u0010\u0016\u001a\u00020\u00172\b\b\u0002\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u00112\b\b\u0002\u0010\u001b\u001a\u00020\u00112\b\b\u0002\u0010\u001c\u001a\u00020\u00112\b\b\u0002\u0010\u001d\u001a\u00020\u00112\b\b\u0002\u0010\u001e\u001a\u00020\u001f2\b\b\u0002\u0010 \u001a\u00020!2\b\b\u0002\u0010\"\u001a\u00020\u00172\b\b\u0002\u0010#\u001a\u00020\u0017H\u00c6\u0001J\u0013\u0010C\u001a\u00020\u001f2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010D\u001a\u00020\u001fJ\t\u0010E\u001a\u00020\u0017H\u00d6\u0001J\u000e\u0010F\u001a\u00020G2\u0006\u0010\u001a\u001a\u00020\u0011J\u000e\u0010H\u001a\u00020G2\u0006\u0010\u0012\u001a\u00020\u0011J\u0016\u0010H\u001a\u00020G2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011J\b\u0010I\u001a\u00020\u0003H\u0016R\u0012\u0010\u001a\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u00020\u00198\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u00020\u00158\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u001e\u001a\u00020\u001f8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0016\u0010#\u001a\u00020\u00178\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0011\u0010'\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b'\u0010(R\u0011\u0010)\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b)\u0010(R\u0012\u0010\u001b\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u001c\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010 \u001a\u00020!8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\u000e8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\"\u001a\u00020\u00178\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010&\"\u0004\b+\u0010,R\u0012\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u001d\u001a\u00020\u00118\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006M" }, d2 = { "Landroidx/work/impl/model/WorkSpec;", "", "id", "", "workerClassName_", "(Ljava/lang/String;Ljava/lang/String;)V", "newId", "other", "(Ljava/lang/String;Landroidx/work/impl/model/WorkSpec;)V", "state", "Landroidx/work/WorkInfo$State;", "workerClassName", "inputMergerClassName", "input", "Landroidx/work/Data;", "output", "initialDelay", "", "intervalDuration", "flexDuration", "constraints", "Landroidx/work/Constraints;", "runAttemptCount", "", "backoffPolicy", "Landroidx/work/BackoffPolicy;", "backoffDelayDuration", "lastEnqueueTime", "minimumRetentionDuration", "scheduleRequestedAt", "expedited", "", "outOfQuotaPolicy", "Landroidx/work/OutOfQuotaPolicy;", "periodCount", "generation", "(Ljava/lang/String;Landroidx/work/WorkInfo$State;Ljava/lang/String;Ljava/lang/String;Landroidx/work/Data;Landroidx/work/Data;JJJLandroidx/work/Constraints;ILandroidx/work/BackoffPolicy;JJJJZLandroidx/work/OutOfQuotaPolicy;II)V", "getGeneration", "()I", "isBackedOff", "()Z", "isPeriodic", "getPeriodCount", "setPeriodCount", "(I)V", "calculateNextRunTime", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "hasConstraints", "hashCode", "setBackoffDelayDuration", "", "setPeriodic", "toString", "Companion", "IdAndState", "WorkInfoPojo", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class WorkSpec
{
    public static final Companion Companion;
    public static final long SCHEDULE_NOT_REQUESTED_YET = -1L;
    private static final String TAG;
    public static final Function<List<WorkInfoPojo>, List<WorkInfo>> WORK_INFO_MAPPER;
    public long backoffDelayDuration;
    public BackoffPolicy backoffPolicy;
    public Constraints constraints;
    public boolean expedited;
    public long flexDuration;
    private final int generation;
    public final String id;
    public long initialDelay;
    public Data input;
    public String inputMergerClassName;
    public long intervalDuration;
    public long lastEnqueueTime;
    public long minimumRetentionDuration;
    public OutOfQuotaPolicy outOfQuotaPolicy;
    public Data output;
    private int periodCount;
    public int runAttemptCount;
    public long scheduleRequestedAt;
    public WorkInfo.State state;
    public String workerClassName;
    
    static {
        Companion = new Companion(null);
        final String tagWithPrefix = Logger.tagWithPrefix("WorkSpec");
        Intrinsics.checkNotNullExpressionValue((Object)tagWithPrefix, "tagWithPrefix(\"WorkSpec\")");
        TAG = tagWithPrefix;
        WORK_INFO_MAPPER = new WorkSpec$$ExternalSyntheticLambda1();
    }
    
    public WorkSpec(final String id, final WorkInfo.State state, final String workerClassName, final String inputMergerClassName, final Data input, final Data output, final long initialDelay, final long intervalDuration, final long flexDuration, final Constraints constraints, final int runAttemptCount, final BackoffPolicy backoffPolicy, final long backoffDelayDuration, final long lastEnqueueTime, final long minimumRetentionDuration, final long scheduleRequestedAt, final boolean expedited, final OutOfQuotaPolicy outOfQuotaPolicy, final int periodCount, final int generation) {
        Intrinsics.checkNotNullParameter((Object)id, "id");
        Intrinsics.checkNotNullParameter((Object)state, "state");
        Intrinsics.checkNotNullParameter((Object)workerClassName, "workerClassName");
        Intrinsics.checkNotNullParameter((Object)input, "input");
        Intrinsics.checkNotNullParameter((Object)output, "output");
        Intrinsics.checkNotNullParameter((Object)constraints, "constraints");
        Intrinsics.checkNotNullParameter((Object)backoffPolicy, "backoffPolicy");
        Intrinsics.checkNotNullParameter((Object)outOfQuotaPolicy, "outOfQuotaPolicy");
        this.id = id;
        this.state = state;
        this.workerClassName = workerClassName;
        this.inputMergerClassName = inputMergerClassName;
        this.input = input;
        this.output = output;
        this.initialDelay = initialDelay;
        this.intervalDuration = intervalDuration;
        this.flexDuration = flexDuration;
        this.constraints = constraints;
        this.runAttemptCount = runAttemptCount;
        this.backoffPolicy = backoffPolicy;
        this.backoffDelayDuration = backoffDelayDuration;
        this.lastEnqueueTime = lastEnqueueTime;
        this.minimumRetentionDuration = minimumRetentionDuration;
        this.scheduleRequestedAt = scheduleRequestedAt;
        this.expedited = expedited;
        this.outOfQuotaPolicy = outOfQuotaPolicy;
        this.periodCount = periodCount;
        this.generation = generation;
    }
    
    public WorkSpec(final String s, final WorkSpec workSpec) {
        Intrinsics.checkNotNullParameter((Object)s, "newId");
        Intrinsics.checkNotNullParameter((Object)workSpec, "other");
        this(s, workSpec.state, workSpec.workerClassName, workSpec.inputMergerClassName, new Data(workSpec.input), new Data(workSpec.output), workSpec.initialDelay, workSpec.intervalDuration, workSpec.flexDuration, new Constraints(workSpec.constraints), workSpec.runAttemptCount, workSpec.backoffPolicy, workSpec.backoffDelayDuration, workSpec.lastEnqueueTime, workSpec.minimumRetentionDuration, workSpec.scheduleRequestedAt, workSpec.expedited, workSpec.outOfQuotaPolicy, workSpec.periodCount, 0, 524288, null);
    }
    
    public WorkSpec(final String s, final String s2) {
        Intrinsics.checkNotNullParameter((Object)s, "id");
        Intrinsics.checkNotNullParameter((Object)s2, "workerClassName_");
        this(s, null, s2, null, null, null, 0L, 0L, 0L, null, 0, null, 0L, 0L, 0L, 0L, false, null, 0, 0, 1048570, null);
    }
    
    private static final List WORK_INFO_MAPPER$lambda$1(List list) {
        if (list != null) {
            final Iterable iterable = list;
            final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                collection.add(((WorkInfoPojo)iterator.next()).toWorkInfo());
            }
            list = (List)collection;
        }
        else {
            list = null;
        }
        return list;
    }
    
    public final long calculateNextRunTime() {
        final boolean backedOff = this.isBackedOff();
        boolean b = false;
        final int n = 0;
        long n4;
        if (backedOff) {
            int n2 = n;
            if (this.backoffPolicy == BackoffPolicy.LINEAR) {
                n2 = 1;
            }
            long n3;
            if (n2 != 0) {
                n3 = this.backoffDelayDuration * this.runAttemptCount;
            }
            else {
                n3 = (long)Math.scalb((float)this.backoffDelayDuration, this.runAttemptCount - 1);
            }
            n4 = this.lastEnqueueTime + RangesKt.coerceAtMost(n3, 18000000L);
        }
        else {
            final boolean periodic = this.isPeriodic();
            long n5 = 0L;
            if (periodic) {
                final int periodCount = this.periodCount;
                long lastEnqueueTime;
                final long n6 = lastEnqueueTime = this.lastEnqueueTime;
                if (periodCount == 0) {
                    lastEnqueueTime = n6 + this.initialDelay;
                }
                final long flexDuration = this.flexDuration;
                final long intervalDuration = this.intervalDuration;
                if (flexDuration != intervalDuration) {
                    b = true;
                }
                if (b) {
                    if (periodCount == 0) {
                        n5 = -1 * flexDuration;
                    }
                    lastEnqueueTime += intervalDuration;
                }
                else if (periodCount != 0) {
                    n5 = intervalDuration;
                }
                n4 = lastEnqueueTime + n5;
            }
            else {
                long n7;
                if ((n7 = this.lastEnqueueTime) == 0L) {
                    n7 = System.currentTimeMillis();
                }
                n4 = this.initialDelay + n7;
            }
        }
        return n4;
    }
    
    public final String component1() {
        return this.id;
    }
    
    public final Constraints component10() {
        return this.constraints;
    }
    
    public final int component11() {
        return this.runAttemptCount;
    }
    
    public final BackoffPolicy component12() {
        return this.backoffPolicy;
    }
    
    public final long component13() {
        return this.backoffDelayDuration;
    }
    
    public final long component14() {
        return this.lastEnqueueTime;
    }
    
    public final long component15() {
        return this.minimumRetentionDuration;
    }
    
    public final long component16() {
        return this.scheduleRequestedAt;
    }
    
    public final boolean component17() {
        return this.expedited;
    }
    
    public final OutOfQuotaPolicy component18() {
        return this.outOfQuotaPolicy;
    }
    
    public final int component19() {
        return this.periodCount;
    }
    
    public final WorkInfo.State component2() {
        return this.state;
    }
    
    public final int component20() {
        return this.generation;
    }
    
    public final String component3() {
        return this.workerClassName;
    }
    
    public final String component4() {
        return this.inputMergerClassName;
    }
    
    public final Data component5() {
        return this.input;
    }
    
    public final Data component6() {
        return this.output;
    }
    
    public final long component7() {
        return this.initialDelay;
    }
    
    public final long component8() {
        return this.intervalDuration;
    }
    
    public final long component9() {
        return this.flexDuration;
    }
    
    public final WorkSpec copy(final String s, final WorkInfo.State state, final String s2, final String s3, final Data data, final Data data2, final long n, final long n2, final long n3, final Constraints constraints, final int n4, final BackoffPolicy backoffPolicy, final long n5, final long n6, final long n7, final long n8, final boolean b, final OutOfQuotaPolicy outOfQuotaPolicy, final int n9, final int n10) {
        Intrinsics.checkNotNullParameter((Object)s, "id");
        Intrinsics.checkNotNullParameter((Object)state, "state");
        Intrinsics.checkNotNullParameter((Object)s2, "workerClassName");
        Intrinsics.checkNotNullParameter((Object)data, "input");
        Intrinsics.checkNotNullParameter((Object)data2, "output");
        Intrinsics.checkNotNullParameter((Object)constraints, "constraints");
        Intrinsics.checkNotNullParameter((Object)backoffPolicy, "backoffPolicy");
        Intrinsics.checkNotNullParameter((Object)outOfQuotaPolicy, "outOfQuotaPolicy");
        return new WorkSpec(s, state, s2, s3, data, data2, n, n2, n3, constraints, n4, backoffPolicy, n5, n6, n7, n8, b, outOfQuotaPolicy, n9, n10);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WorkSpec)) {
            return false;
        }
        final WorkSpec workSpec = (WorkSpec)o;
        return Intrinsics.areEqual((Object)this.id, (Object)workSpec.id) && this.state == workSpec.state && Intrinsics.areEqual((Object)this.workerClassName, (Object)workSpec.workerClassName) && Intrinsics.areEqual((Object)this.inputMergerClassName, (Object)workSpec.inputMergerClassName) && Intrinsics.areEqual((Object)this.input, (Object)workSpec.input) && Intrinsics.areEqual((Object)this.output, (Object)workSpec.output) && this.initialDelay == workSpec.initialDelay && this.intervalDuration == workSpec.intervalDuration && this.flexDuration == workSpec.flexDuration && Intrinsics.areEqual((Object)this.constraints, (Object)workSpec.constraints) && this.runAttemptCount == workSpec.runAttemptCount && this.backoffPolicy == workSpec.backoffPolicy && this.backoffDelayDuration == workSpec.backoffDelayDuration && this.lastEnqueueTime == workSpec.lastEnqueueTime && this.minimumRetentionDuration == workSpec.minimumRetentionDuration && this.scheduleRequestedAt == workSpec.scheduleRequestedAt && this.expedited == workSpec.expedited && this.outOfQuotaPolicy == workSpec.outOfQuotaPolicy && this.periodCount == workSpec.periodCount && this.generation == workSpec.generation;
    }
    
    public final int getGeneration() {
        return this.generation;
    }
    
    public final int getPeriodCount() {
        return this.periodCount;
    }
    
    public final boolean hasConstraints() {
        return Intrinsics.areEqual((Object)Constraints.NONE, (Object)this.constraints) ^ true;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.id.hashCode();
        final int hashCode2 = this.state.hashCode();
        final int hashCode3 = this.workerClassName.hashCode();
        final String inputMergerClassName = this.inputMergerClassName;
        int hashCode4;
        if (inputMergerClassName == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = inputMergerClassName.hashCode();
        }
        final int hashCode5 = this.input.hashCode();
        final int hashCode6 = this.output.hashCode();
        final int m = WorkSpec$$ExternalSyntheticBackport0.m(this.initialDelay);
        final int i = WorkSpec$$ExternalSyntheticBackport0.m(this.intervalDuration);
        final int j = WorkSpec$$ExternalSyntheticBackport0.m(this.flexDuration);
        final int hashCode7 = this.constraints.hashCode();
        final int runAttemptCount = this.runAttemptCount;
        final int hashCode8 = this.backoffPolicy.hashCode();
        final int k = WorkSpec$$ExternalSyntheticBackport0.m(this.backoffDelayDuration);
        final int l = WorkSpec$$ExternalSyntheticBackport0.m(this.lastEnqueueTime);
        final int m2 = WorkSpec$$ExternalSyntheticBackport0.m(this.minimumRetentionDuration);
        final int m3 = WorkSpec$$ExternalSyntheticBackport0.m(this.scheduleRequestedAt);
        int expedited;
        if ((expedited = (this.expedited ? 1 : 0)) != 0) {
            expedited = 1;
        }
        return ((((((((((((((((((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + m) * 31 + i) * 31 + j) * 31 + hashCode7) * 31 + runAttemptCount) * 31 + hashCode8) * 31 + k) * 31 + l) * 31 + m2) * 31 + m3) * 31 + expedited) * 31 + this.outOfQuotaPolicy.hashCode()) * 31 + this.periodCount) * 31 + this.generation;
    }
    
    public final boolean isBackedOff() {
        return this.state == WorkInfo.State.ENQUEUED && this.runAttemptCount > 0;
    }
    
    public final boolean isPeriodic() {
        return this.intervalDuration != 0L;
    }
    
    public final void setBackoffDelayDuration(final long n) {
        if (n > 18000000L) {
            Logger.get().warning(WorkSpec.TAG, "Backoff delay duration exceeds maximum value");
        }
        if (n < 10000L) {
            Logger.get().warning(WorkSpec.TAG, "Backoff delay duration less than minimum value");
        }
        this.backoffDelayDuration = RangesKt.coerceIn(n, 10000L, 18000000L);
    }
    
    public final void setPeriodCount(final int periodCount) {
        this.periodCount = periodCount;
    }
    
    public final void setPeriodic(final long n) {
        if (n < 900000L) {
            Logger.get().warning(WorkSpec.TAG, "Interval duration lesser than minimum allowed value; Changed to 900000");
        }
        this.setPeriodic(RangesKt.coerceAtLeast(n, 900000L), RangesKt.coerceAtLeast(n, 900000L));
    }
    
    public final void setPeriodic(final long lng, final long n) {
        if (lng < 900000L) {
            Logger.get().warning(WorkSpec.TAG, "Interval duration lesser than minimum allowed value; Changed to 900000");
        }
        this.intervalDuration = RangesKt.coerceAtLeast(lng, 900000L);
        if (n < 300000L) {
            Logger.get().warning(WorkSpec.TAG, "Flex duration lesser than minimum allowed value; Changed to 300000");
        }
        if (n > this.intervalDuration) {
            final Logger value = Logger.get();
            final String tag = WorkSpec.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Flex duration greater than interval duration; Changed to ");
            sb.append(lng);
            value.warning(tag, sb.toString());
        }
        this.flexDuration = RangesKt.coerceIn(n, 300000L, this.intervalDuration);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{WorkSpec: ");
        sb.append(this.id);
        sb.append('}');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R(\u0010\u0007\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\t0\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Landroidx/work/impl/model/WorkSpec$Companion;", "", "()V", "SCHEDULE_NOT_REQUESTED_YET", "", "TAG", "", "WORK_INFO_MAPPER", "Landroidx/arch/core/util/Function;", "", "Landroidx/work/impl/model/WorkSpec$WorkInfoPojo;", "Landroidx/work/WorkInfo;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\b\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0012\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/work/impl/model/WorkSpec$IdAndState;", "", "id", "", "state", "Landroidx/work/WorkInfo$State;", "(Ljava/lang/String;Landroidx/work/WorkInfo$State;)V", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class IdAndState
    {
        public String id;
        public WorkInfo.State state;
        
        public IdAndState(final String id, final WorkInfo.State state) {
            Intrinsics.checkNotNullParameter((Object)id, "id");
            Intrinsics.checkNotNullParameter((Object)state, "state");
            this.id = id;
            this.state = state;
        }
        
        public final String component1() {
            return this.id;
        }
        
        public final WorkInfo.State component2() {
            return this.state;
        }
        
        public final IdAndState copy(final String s, final WorkInfo.State state) {
            Intrinsics.checkNotNullParameter((Object)s, "id");
            Intrinsics.checkNotNullParameter((Object)state, "state");
            return new IdAndState(s, state);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof IdAndState)) {
                return false;
            }
            final IdAndState idAndState = (IdAndState)o;
            return Intrinsics.areEqual((Object)this.id, (Object)idAndState.id) && this.state == idAndState.state;
        }
        
        @Override
        public int hashCode() {
            return this.id.hashCode() * 31 + this.state.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("IdAndState(id=");
            sb.append(this.id);
            sb.append(", state=");
            sb.append(this.state);
            sb.append(')');
            return sb.toString();
        }
    }
    
    @Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\"\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\f\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\f¢\u0006\u0002\u0010\u000eJ\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010'\u001a\u00020\u0005H\u00c6\u0003J\t\u0010(\u001a\u00020\u0007H\u00c6\u0003J\t\u0010)\u001a\u00020\tH\u00c6\u0003J\t\u0010*\u001a\u00020\tH\u00c6\u0003J\u000f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00030\fH\u00c6\u0003J\u000f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00070\fH\u00c6\u0003J[\u0010-\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\fH\u00c6\u0001J\u0013\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00101\u001a\u00020\tH\u00d6\u0001J\t\u00102\u001a\u00020\u0003H\u00d6\u0001J\u0006\u00103\u001a\u000204R\u0016\u0010\n\u001a\u00020\t8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R$\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\f8\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001e\u0010\b\u001a\u00020\t8\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0010\"\u0004\b\u001e\u0010\u001fR\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R$\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\f8\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u001a\"\u0004\b%\u0010\u001c¨\u00065" }, d2 = { "Landroidx/work/impl/model/WorkSpec$WorkInfoPojo;", "", "id", "", "state", "Landroidx/work/WorkInfo$State;", "output", "Landroidx/work/Data;", "runAttemptCount", "", "generation", "tags", "", "progress", "(Ljava/lang/String;Landroidx/work/WorkInfo$State;Landroidx/work/Data;IILjava/util/List;Ljava/util/List;)V", "getGeneration", "()I", "getId", "()Ljava/lang/String;", "setId", "(Ljava/lang/String;)V", "getOutput", "()Landroidx/work/Data;", "setOutput", "(Landroidx/work/Data;)V", "getProgress", "()Ljava/util/List;", "setProgress", "(Ljava/util/List;)V", "getRunAttemptCount", "setRunAttemptCount", "(I)V", "getState", "()Landroidx/work/WorkInfo$State;", "setState", "(Landroidx/work/WorkInfo$State;)V", "getTags", "setTags", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "toString", "toWorkInfo", "Landroidx/work/WorkInfo;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class WorkInfoPojo
    {
        private final int generation;
        private String id;
        private Data output;
        private List<Data> progress;
        private int runAttemptCount;
        private WorkInfo.State state;
        private List<String> tags;
        
        public WorkInfoPojo(final String id, final WorkInfo.State state, final Data output, final int runAttemptCount, final int generation, final List<String> tags, final List<Data> progress) {
            Intrinsics.checkNotNullParameter((Object)id, "id");
            Intrinsics.checkNotNullParameter((Object)state, "state");
            Intrinsics.checkNotNullParameter((Object)output, "output");
            Intrinsics.checkNotNullParameter((Object)tags, "tags");
            Intrinsics.checkNotNullParameter((Object)progress, "progress");
            this.id = id;
            this.state = state;
            this.output = output;
            this.runAttemptCount = runAttemptCount;
            this.generation = generation;
            this.tags = tags;
            this.progress = progress;
        }
        
        public final String component1() {
            return this.id;
        }
        
        public final WorkInfo.State component2() {
            return this.state;
        }
        
        public final Data component3() {
            return this.output;
        }
        
        public final int component4() {
            return this.runAttemptCount;
        }
        
        public final int component5() {
            return this.generation;
        }
        
        public final List<String> component6() {
            return this.tags;
        }
        
        public final List<Data> component7() {
            return this.progress;
        }
        
        public final WorkInfoPojo copy(final String s, final WorkInfo.State state, final Data data, final int n, final int n2, final List<String> list, final List<Data> list2) {
            Intrinsics.checkNotNullParameter((Object)s, "id");
            Intrinsics.checkNotNullParameter((Object)state, "state");
            Intrinsics.checkNotNullParameter((Object)data, "output");
            Intrinsics.checkNotNullParameter((Object)list, "tags");
            Intrinsics.checkNotNullParameter((Object)list2, "progress");
            return new WorkInfoPojo(s, state, data, n, n2, list, list2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof WorkInfoPojo)) {
                return false;
            }
            final WorkInfoPojo workInfoPojo = (WorkInfoPojo)o;
            return Intrinsics.areEqual((Object)this.id, (Object)workInfoPojo.id) && this.state == workInfoPojo.state && Intrinsics.areEqual((Object)this.output, (Object)workInfoPojo.output) && this.runAttemptCount == workInfoPojo.runAttemptCount && this.generation == workInfoPojo.generation && Intrinsics.areEqual((Object)this.tags, (Object)workInfoPojo.tags) && Intrinsics.areEqual((Object)this.progress, (Object)workInfoPojo.progress);
        }
        
        public final int getGeneration() {
            return this.generation;
        }
        
        public final String getId() {
            return this.id;
        }
        
        public final Data getOutput() {
            return this.output;
        }
        
        public final List<Data> getProgress() {
            return this.progress;
        }
        
        public final int getRunAttemptCount() {
            return this.runAttemptCount;
        }
        
        public final WorkInfo.State getState() {
            return this.state;
        }
        
        public final List<String> getTags() {
            return this.tags;
        }
        
        @Override
        public int hashCode() {
            return (((((this.id.hashCode() * 31 + this.state.hashCode()) * 31 + this.output.hashCode()) * 31 + this.runAttemptCount) * 31 + this.generation) * 31 + this.tags.hashCode()) * 31 + this.progress.hashCode();
        }
        
        public final void setId(final String id) {
            Intrinsics.checkNotNullParameter((Object)id, "<set-?>");
            this.id = id;
        }
        
        public final void setOutput(final Data output) {
            Intrinsics.checkNotNullParameter((Object)output, "<set-?>");
            this.output = output;
        }
        
        public final void setProgress(final List<Data> progress) {
            Intrinsics.checkNotNullParameter((Object)progress, "<set-?>");
            this.progress = progress;
        }
        
        public final void setRunAttemptCount(final int runAttemptCount) {
            this.runAttemptCount = runAttemptCount;
        }
        
        public final void setState(final WorkInfo.State state) {
            Intrinsics.checkNotNullParameter((Object)state, "<set-?>");
            this.state = state;
        }
        
        public final void setTags(final List<String> tags) {
            Intrinsics.checkNotNullParameter((Object)tags, "<set-?>");
            this.tags = tags;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkInfoPojo(id=");
            sb.append(this.id);
            sb.append(", state=");
            sb.append(this.state);
            sb.append(", output=");
            sb.append(this.output);
            sb.append(", runAttemptCount=");
            sb.append(this.runAttemptCount);
            sb.append(", generation=");
            sb.append(this.generation);
            sb.append(", tags=");
            sb.append(this.tags);
            sb.append(", progress=");
            sb.append(this.progress);
            sb.append(')');
            return sb.toString();
        }
        
        public final WorkInfo toWorkInfo() {
            Data empty;
            if (this.progress.isEmpty() ^ true) {
                empty = this.progress.get(0);
            }
            else {
                empty = Data.EMPTY;
            }
            return new WorkInfo(UUID.fromString(this.id), this.state, this.output, this.tags, empty, this.runAttemptCount, this.generation);
        }
    }
}

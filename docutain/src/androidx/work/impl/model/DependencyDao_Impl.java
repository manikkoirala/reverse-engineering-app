// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.Collections;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class DependencyDao_Impl implements DependencyDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<Dependency> __insertionAdapterOfDependency;
    
    public DependencyDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfDependency = new EntityInsertionAdapter<Dependency>(this, _db) {
            final DependencyDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final Dependency dependency) {
                if (dependency.getWorkSpecId() == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, dependency.getWorkSpecId());
                }
                if (dependency.getPrerequisiteId() == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindString(2, dependency.getPrerequisiteId());
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
            }
        };
    }
    
    public static List<Class<?>> getRequiredConverters() {
        return Collections.emptyList();
    }
    
    @Override
    public List<String> getDependentWorkIds(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getPrerequisites(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT prerequisite_id FROM dependency WHERE work_spec_id=?", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                if (query.isNull(0)) {
                    string = null;
                }
                else {
                    string = query.getString(0);
                }
                list.add((Object)string);
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public boolean hasCompletedAllPrerequisites(String query) {
        final boolean b = true;
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        boolean b2 = false;
        query = (String)DBUtil.query(_db, acquire, false, null);
        try {
            if (((Cursor)query).moveToFirst()) {
                b2 = (((Cursor)query).getInt(0) != 0 && b);
            }
            return b2;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public boolean hasDependents(final String s) {
        final boolean b = true;
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        boolean b2 = false;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            if (query.moveToFirst()) {
                b2 = (query.getInt(0) != 0 && b);
            }
            return b2;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insertDependency(final Dependency dependency) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDependency.insert(dependency);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

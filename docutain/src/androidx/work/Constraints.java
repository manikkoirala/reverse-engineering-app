// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.window.embedding.ActivityRule$$ExternalSyntheticBackport0;
import androidx.work.impl.utils.DurationApi26Impl;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import android.net.Uri;
import kotlin.collections.CollectionsKt;
import android.os.Build$VERSION;
import java.util.LinkedHashSet;
import java.util.Collection;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u0000 \u001e2\u00020\u0001:\u0003\u001d\u001e\u001fB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0000¢\u0006\u0002\u0010\u0003B[\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\b\b\u0002\u0010\r\u001a\u00020\f\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\u0002\u0010\u0011J\u0013\u0010\u0019\u001a\u00020\u00072\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u001a\u001a\u00020\u0007H\u0007J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u0006\u0010\t\u001a\u00020\u0007J\u0006\u0010\u0006\u001a\u00020\u0007J\b\u0010\b\u001a\u00020\u0007H\u0007J\u0006\u0010\n\u001a\u00020\u0007R\u0016\u0010\r\u001a\u00020\f8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0016\u0010\u000b\u001a\u00020\f8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013R\u001c\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0010\u0010\t\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Landroidx/work/Constraints;", "", "other", "(Landroidx/work/Constraints;)V", "requiredNetworkType", "Landroidx/work/NetworkType;", "requiresCharging", "", "requiresDeviceIdle", "requiresBatteryNotLow", "requiresStorageNotLow", "contentTriggerUpdateDelayMillis", "", "contentTriggerMaxDelayMillis", "contentUriTriggers", "", "Landroidx/work/Constraints$ContentUriTrigger;", "(Landroidx/work/NetworkType;ZZZZJJLjava/util/Set;)V", "getContentTriggerMaxDelayMillis", "()J", "getContentTriggerUpdateDelayMillis", "getContentUriTriggers", "()Ljava/util/Set;", "getRequiredNetworkType", "()Landroidx/work/NetworkType;", "equals", "hasContentUriTriggers", "hashCode", "", "Builder", "Companion", "ContentUriTrigger", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class Constraints
{
    public static final Companion Companion;
    public static final Constraints NONE;
    private final long contentTriggerMaxDelayMillis;
    private final long contentTriggerUpdateDelayMillis;
    private final Set<ContentUriTrigger> contentUriTriggers;
    private final NetworkType requiredNetworkType;
    private final boolean requiresBatteryNotLow;
    private final boolean requiresCharging;
    private final boolean requiresDeviceIdle;
    private final boolean requiresStorageNotLow;
    
    static {
        Companion = new Companion(null);
        NONE = new Constraints(null, false, false, false, false, 0L, 0L, null, 255, null);
    }
    
    public Constraints() {
        this(null, false, false, false, false, 0L, 0L, null, 255, null);
    }
    
    public Constraints(final Constraints constraints) {
        Intrinsics.checkNotNullParameter((Object)constraints, "other");
        this(constraints.requiredNetworkType, constraints.requiresCharging, constraints.requiresDeviceIdle, constraints.requiresBatteryNotLow, constraints.requiresStorageNotLow, constraints.contentTriggerUpdateDelayMillis, constraints.contentTriggerMaxDelayMillis, constraints.contentUriTriggers);
    }
    
    public Constraints(final NetworkType requiredNetworkType, final boolean requiresCharging, final boolean requiresDeviceIdle, final boolean requiresBatteryNotLow, final boolean requiresStorageNotLow, final long contentTriggerUpdateDelayMillis, final long contentTriggerMaxDelayMillis, final Set<ContentUriTrigger> contentUriTriggers) {
        Intrinsics.checkNotNullParameter((Object)requiredNetworkType, "requiredNetworkType");
        Intrinsics.checkNotNullParameter((Object)contentUriTriggers, "contentUriTriggers");
        this.requiredNetworkType = requiredNetworkType;
        this.requiresCharging = requiresCharging;
        this.requiresDeviceIdle = requiresDeviceIdle;
        this.requiresBatteryNotLow = requiresBatteryNotLow;
        this.requiresStorageNotLow = requiresStorageNotLow;
        this.contentTriggerUpdateDelayMillis = contentTriggerUpdateDelayMillis;
        this.contentTriggerMaxDelayMillis = contentTriggerMaxDelayMillis;
        this.contentUriTriggers = contentUriTriggers;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        boolean equal;
        final boolean b = equal = false;
        if (o != null) {
            if (!Intrinsics.areEqual((Object)this.getClass(), (Object)o.getClass())) {
                equal = b;
            }
            else {
                final Constraints constraints = (Constraints)o;
                if (this.requiresCharging != constraints.requiresCharging) {
                    return false;
                }
                if (this.requiresDeviceIdle != constraints.requiresDeviceIdle) {
                    return false;
                }
                if (this.requiresBatteryNotLow != constraints.requiresBatteryNotLow) {
                    return false;
                }
                if (this.requiresStorageNotLow != constraints.requiresStorageNotLow) {
                    return false;
                }
                if (this.contentTriggerUpdateDelayMillis != constraints.contentTriggerUpdateDelayMillis) {
                    return false;
                }
                if (this.contentTriggerMaxDelayMillis != constraints.contentTriggerMaxDelayMillis) {
                    return false;
                }
                if (this.requiredNetworkType != constraints.requiredNetworkType) {
                    equal = b;
                }
                else {
                    equal = Intrinsics.areEqual((Object)this.contentUriTriggers, (Object)constraints.contentUriTriggers);
                }
            }
        }
        return equal;
    }
    
    public final long getContentTriggerMaxDelayMillis() {
        return this.contentTriggerMaxDelayMillis;
    }
    
    public final long getContentTriggerUpdateDelayMillis() {
        return this.contentTriggerUpdateDelayMillis;
    }
    
    public final Set<ContentUriTrigger> getContentUriTriggers() {
        return this.contentUriTriggers;
    }
    
    public final NetworkType getRequiredNetworkType() {
        return this.requiredNetworkType;
    }
    
    public final boolean hasContentUriTriggers() {
        return this.contentUriTriggers.isEmpty() ^ true;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.requiredNetworkType.hashCode();
        final int requiresCharging = this.requiresCharging ? 1 : 0;
        final int requiresDeviceIdle = this.requiresDeviceIdle ? 1 : 0;
        final int requiresBatteryNotLow = this.requiresBatteryNotLow ? 1 : 0;
        final int requiresStorageNotLow = this.requiresStorageNotLow ? 1 : 0;
        final long contentTriggerUpdateDelayMillis = this.contentTriggerUpdateDelayMillis;
        final int n = (int)(contentTriggerUpdateDelayMillis ^ contentTriggerUpdateDelayMillis >>> 32);
        final long contentTriggerMaxDelayMillis = this.contentTriggerMaxDelayMillis;
        return ((((((hashCode * 31 + requiresCharging) * 31 + requiresDeviceIdle) * 31 + requiresBatteryNotLow) * 31 + requiresStorageNotLow) * 31 + n) * 31 + (int)(contentTriggerMaxDelayMillis ^ contentTriggerMaxDelayMillis >>> 32)) * 31 + this.contentUriTriggers.hashCode();
    }
    
    public final boolean requiresBatteryNotLow() {
        return this.requiresBatteryNotLow;
    }
    
    public final boolean requiresCharging() {
        return this.requiresCharging;
    }
    
    public final boolean requiresDeviceIdle() {
        return this.requiresDeviceIdle;
    }
    
    public final boolean requiresStorageNotLow() {
        return this.requiresStorageNotLow;
    }
    
    @Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u000f\b\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\fH\u0007J\u0006\u0010\u0017\u001a\u00020\u0004J\u000e\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0019\u001a\u00020\nJ\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\fJ\u0010\u0010\u001c\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\fH\u0007J\u000e\u0010\u001d\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\fJ\u0010\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020 H\u0007J\u0018\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"H\u0007J\u0010\u0010#\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020 H\u0007J\u0018\u0010#\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"H\u0007R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006$" }, d2 = { "Landroidx/work/Constraints$Builder;", "", "()V", "constraints", "Landroidx/work/Constraints;", "(Landroidx/work/Constraints;)V", "contentUriTriggers", "", "Landroidx/work/Constraints$ContentUriTrigger;", "requiredNetworkType", "Landroidx/work/NetworkType;", "requiresBatteryNotLow", "", "requiresCharging", "requiresDeviceIdle", "requiresStorageNotLow", "triggerContentMaxDelay", "", "triggerContentUpdateDelay", "addContentUriTrigger", "uri", "Landroid/net/Uri;", "triggerForDescendants", "build", "setRequiredNetworkType", "networkType", "setRequiresBatteryNotLow", "setRequiresCharging", "setRequiresDeviceIdle", "setRequiresStorageNotLow", "setTriggerContentMaxDelay", "duration", "Ljava/time/Duration;", "timeUnit", "Ljava/util/concurrent/TimeUnit;", "setTriggerContentUpdateDelay", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Builder
    {
        private Set<ContentUriTrigger> contentUriTriggers;
        private NetworkType requiredNetworkType;
        private boolean requiresBatteryNotLow;
        private boolean requiresCharging;
        private boolean requiresDeviceIdle;
        private boolean requiresStorageNotLow;
        private long triggerContentMaxDelay;
        private long triggerContentUpdateDelay;
        
        public Builder() {
            this.requiredNetworkType = NetworkType.NOT_REQUIRED;
            this.triggerContentUpdateDelay = -1L;
            this.triggerContentMaxDelay = -1L;
            this.contentUriTriggers = new LinkedHashSet<ContentUriTrigger>();
        }
        
        public Builder(final Constraints constraints) {
            Intrinsics.checkNotNullParameter((Object)constraints, "constraints");
            this.requiredNetworkType = NetworkType.NOT_REQUIRED;
            this.triggerContentUpdateDelay = -1L;
            this.triggerContentMaxDelay = -1L;
            this.contentUriTriggers = new LinkedHashSet<ContentUriTrigger>();
            this.requiresCharging = constraints.requiresCharging();
            this.requiresDeviceIdle = (Build$VERSION.SDK_INT >= 23 && constraints.requiresDeviceIdle());
            this.requiredNetworkType = constraints.getRequiredNetworkType();
            this.requiresBatteryNotLow = constraints.requiresBatteryNotLow();
            this.requiresStorageNotLow = constraints.requiresStorageNotLow();
            if (Build$VERSION.SDK_INT >= 24) {
                this.triggerContentUpdateDelay = constraints.getContentTriggerUpdateDelayMillis();
                this.triggerContentMaxDelay = constraints.getContentTriggerMaxDelayMillis();
                this.contentUriTriggers = CollectionsKt.toMutableSet((Iterable)constraints.getContentUriTriggers());
            }
        }
        
        public final Builder addContentUriTrigger(final Uri uri, final boolean b) {
            Intrinsics.checkNotNullParameter((Object)uri, "uri");
            this.contentUriTriggers.add(new ContentUriTrigger(uri, b));
            return this;
        }
        
        public final Constraints build() {
            Set set;
            long triggerContentUpdateDelay;
            long triggerContentMaxDelay;
            if (Build$VERSION.SDK_INT >= 24) {
                set = CollectionsKt.toSet((Iterable)this.contentUriTriggers);
                triggerContentUpdateDelay = this.triggerContentUpdateDelay;
                triggerContentMaxDelay = this.triggerContentMaxDelay;
            }
            else {
                set = SetsKt.emptySet();
                triggerContentUpdateDelay = -1L;
                triggerContentMaxDelay = -1L;
            }
            return new Constraints(this.requiredNetworkType, this.requiresCharging, Build$VERSION.SDK_INT >= 23 && this.requiresDeviceIdle, this.requiresBatteryNotLow, this.requiresStorageNotLow, triggerContentUpdateDelay, triggerContentMaxDelay, set);
        }
        
        public final Builder setRequiredNetworkType(final NetworkType requiredNetworkType) {
            Intrinsics.checkNotNullParameter((Object)requiredNetworkType, "networkType");
            this.requiredNetworkType = requiredNetworkType;
            return this;
        }
        
        public final Builder setRequiresBatteryNotLow(final boolean requiresBatteryNotLow) {
            this.requiresBatteryNotLow = requiresBatteryNotLow;
            return this;
        }
        
        public final Builder setRequiresCharging(final boolean requiresCharging) {
            this.requiresCharging = requiresCharging;
            return this;
        }
        
        public final Builder setRequiresDeviceIdle(final boolean requiresDeviceIdle) {
            this.requiresDeviceIdle = requiresDeviceIdle;
            return this;
        }
        
        public final Builder setRequiresStorageNotLow(final boolean requiresStorageNotLow) {
            this.requiresStorageNotLow = requiresStorageNotLow;
            return this;
        }
        
        public final Builder setTriggerContentMaxDelay(final long duration, final TimeUnit timeUnit) {
            Intrinsics.checkNotNullParameter((Object)timeUnit, "timeUnit");
            this.triggerContentMaxDelay = timeUnit.toMillis(duration);
            return this;
        }
        
        public final Builder setTriggerContentMaxDelay(final Duration duration) {
            Intrinsics.checkNotNullParameter((Object)duration, "duration");
            this.triggerContentMaxDelay = DurationApi26Impl.toMillisCompat(duration);
            return this;
        }
        
        public final Builder setTriggerContentUpdateDelay(final long duration, final TimeUnit timeUnit) {
            Intrinsics.checkNotNullParameter((Object)timeUnit, "timeUnit");
            this.triggerContentUpdateDelay = timeUnit.toMillis(duration);
            return this;
        }
        
        public final Builder setTriggerContentUpdateDelay(final Duration duration) {
            Intrinsics.checkNotNullParameter((Object)duration, "duration");
            this.triggerContentUpdateDelay = DurationApi26Impl.toMillisCompat(duration);
            return this;
        }
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/work/Constraints$Companion;", "", "()V", "NONE", "Landroidx/work/Constraints;", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010\n\u001a\u00020\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\f\u001a\u00020\rH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u000e" }, d2 = { "Landroidx/work/Constraints$ContentUriTrigger;", "", "uri", "Landroid/net/Uri;", "isTriggeredForDescendants", "", "(Landroid/net/Uri;Z)V", "()Z", "getUri", "()Landroid/net/Uri;", "equals", "other", "hashCode", "", "work-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class ContentUriTrigger
    {
        private final boolean isTriggeredForDescendants;
        private final Uri uri;
        
        public ContentUriTrigger(final Uri uri, final boolean isTriggeredForDescendants) {
            Intrinsics.checkNotNullParameter((Object)uri, "uri");
            this.uri = uri;
            this.isTriggeredForDescendants = isTriggeredForDescendants;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            final Class<? extends ContentUriTrigger> class1 = this.getClass();
            Class<?> class2;
            if (o != null) {
                class2 = o.getClass();
            }
            else {
                class2 = null;
            }
            if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
                return false;
            }
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.work.Constraints.ContentUriTrigger");
            final ContentUriTrigger contentUriTrigger = (ContentUriTrigger)o;
            return Intrinsics.areEqual((Object)this.uri, (Object)contentUriTrigger.uri) && this.isTriggeredForDescendants == contentUriTrigger.isTriggeredForDescendants;
        }
        
        public final Uri getUri() {
            return this.uri;
        }
        
        @Override
        public int hashCode() {
            return this.uri.hashCode() * 31 + ActivityRule$$ExternalSyntheticBackport0.m(this.isTriggeredForDescendants);
        }
        
        public final boolean isTriggeredForDescendants() {
            return this.isTriggeredForDescendants;
        }
    }
}

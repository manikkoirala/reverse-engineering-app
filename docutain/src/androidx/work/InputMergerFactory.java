// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public abstract class InputMergerFactory
{
    public static InputMergerFactory getDefaultInputMergerFactory() {
        return new InputMergerFactory() {
            @Override
            public InputMerger createInputMerger(final String s) {
                return null;
            }
        };
    }
    
    public abstract InputMerger createInputMerger(final String p0);
    
    public final InputMerger createInputMergerWithDefaultFallback(final String s) {
        InputMerger inputMerger;
        if ((inputMerger = this.createInputMerger(s)) == null) {
            inputMerger = InputMerger.fromClassName(s);
        }
        return inputMerger;
    }
}

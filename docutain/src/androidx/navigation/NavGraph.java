// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.content.res.TypedArray;
import androidx.navigation.common.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.NoSuchElementException;
import java.util.Collection;
import java.util.Iterator;
import androidx.collection.SparseArrayCompat;

public class NavGraph extends NavDestination implements Iterable<NavDestination>
{
    final SparseArrayCompat<NavDestination> mNodes;
    private int mStartDestId;
    private String mStartDestIdName;
    
    public NavGraph(final Navigator<? extends NavGraph> navigator) {
        super(navigator);
        this.mNodes = new SparseArrayCompat<NavDestination>();
    }
    
    public final void addAll(final NavGraph navGraph) {
        final Iterator<NavDestination> iterator = navGraph.iterator();
        while (iterator.hasNext()) {
            final NavDestination navDestination = iterator.next();
            iterator.remove();
            this.addDestination(navDestination);
        }
    }
    
    public final void addDestination(final NavDestination obj) {
        final int id = obj.getId();
        if (id == 0) {
            throw new IllegalArgumentException("Destinations must have an id. Call setId() or include an android:id in your navigation XML.");
        }
        if (id == this.getId()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Destination ");
            sb.append(obj);
            sb.append(" cannot have the same id as graph ");
            sb.append(this);
            throw new IllegalArgumentException(sb.toString());
        }
        final NavDestination navDestination = this.mNodes.get(id);
        if (navDestination == obj) {
            return;
        }
        if (obj.getParent() == null) {
            if (navDestination != null) {
                navDestination.setParent(null);
            }
            obj.setParent(this);
            this.mNodes.put(obj.getId(), obj);
            return;
        }
        throw new IllegalStateException("Destination already has a parent set. Call NavGraph.remove() to remove the previous parent.");
    }
    
    public final void addDestinations(final Collection<NavDestination> collection) {
        for (final NavDestination navDestination : collection) {
            if (navDestination == null) {
                continue;
            }
            this.addDestination(navDestination);
        }
    }
    
    public final void addDestinations(final NavDestination... array) {
        for (final NavDestination navDestination : array) {
            if (navDestination != null) {
                this.addDestination(navDestination);
            }
        }
    }
    
    public final void clear() {
        final Iterator<NavDestination> iterator = this.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
    
    public final NavDestination findNode(final int n) {
        return this.findNode(n, true);
    }
    
    final NavDestination findNode(final int n, final boolean b) {
        NavDestination node = this.mNodes.get(n);
        if (node == null) {
            if (b && this.getParent() != null) {
                node = this.getParent().findNode(n);
            }
            else {
                node = null;
            }
        }
        return node;
    }
    
    @Override
    public String getDisplayName() {
        String displayName;
        if (this.getId() != 0) {
            displayName = super.getDisplayName();
        }
        else {
            displayName = "the root navigation";
        }
        return displayName;
    }
    
    String getStartDestDisplayName() {
        if (this.mStartDestIdName == null) {
            this.mStartDestIdName = Integer.toString(this.mStartDestId);
        }
        return this.mStartDestIdName;
    }
    
    public final int getStartDestination() {
        return this.mStartDestId;
    }
    
    @Override
    public final Iterator<NavDestination> iterator() {
        return new Iterator<NavDestination>(this) {
            private int mIndex = -1;
            private boolean mWentToNext = false;
            final NavGraph this$0;
            
            @Override
            public boolean hasNext() {
                final int mIndex = this.mIndex;
                boolean b = true;
                if (mIndex + 1 >= this.this$0.mNodes.size()) {
                    b = false;
                }
                return b;
            }
            
            @Override
            public NavDestination next() {
                if (this.hasNext()) {
                    this.mWentToNext = true;
                    final SparseArrayCompat<NavDestination> mNodes = this.this$0.mNodes;
                    final int mIndex = this.mIndex + 1;
                    this.mIndex = mIndex;
                    return mNodes.valueAt(mIndex);
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public void remove() {
                if (this.mWentToNext) {
                    this.this$0.mNodes.valueAt(this.mIndex).setParent(null);
                    this.this$0.mNodes.removeAt(this.mIndex);
                    --this.mIndex;
                    this.mWentToNext = false;
                    return;
                }
                throw new IllegalStateException("You must call next() before you can remove an element");
            }
        };
    }
    
    @Override
    DeepLinkMatch matchDeepLink(final NavDeepLinkRequest navDeepLinkRequest) {
        DeepLinkMatch matchDeepLink = super.matchDeepLink(navDeepLinkRequest);
        final Iterator<NavDestination> iterator = this.iterator();
        while (iterator.hasNext()) {
            final DeepLinkMatch matchDeepLink2 = iterator.next().matchDeepLink(navDeepLinkRequest);
            if (matchDeepLink2 != null && (matchDeepLink == null || matchDeepLink2.compareTo(matchDeepLink) > 0)) {
                matchDeepLink = matchDeepLink2;
            }
        }
        return matchDeepLink;
    }
    
    @Override
    public void onInflate(final Context context, final AttributeSet set) {
        super.onInflate(context, set);
        final TypedArray obtainAttributes = context.getResources().obtainAttributes(set, R.styleable.NavGraphNavigator);
        this.setStartDestination(obtainAttributes.getResourceId(R.styleable.NavGraphNavigator_startDestination, 0));
        this.mStartDestIdName = NavDestination.getDisplayName(context, this.mStartDestId);
        obtainAttributes.recycle();
    }
    
    public final void remove(final NavDestination navDestination) {
        final int indexOfKey = this.mNodes.indexOfKey(navDestination.getId());
        if (indexOfKey >= 0) {
            this.mNodes.valueAt(indexOfKey).setParent(null);
            this.mNodes.removeAt(indexOfKey);
        }
    }
    
    public final void setStartDestination(final int n) {
        if (n != this.getId()) {
            this.mStartDestId = n;
            this.mStartDestIdName = null;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Start destination ");
        sb.append(n);
        sb.append(" cannot use the same id as the graph ");
        sb.append(this);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" startDestination=");
        final NavDestination node = this.findNode(this.getStartDestination());
        if (node == null) {
            final String mStartDestIdName = this.mStartDestIdName;
            if (mStartDestIdName == null) {
                sb.append("0x");
                sb.append(Integer.toHexString(this.mStartDestId));
            }
            else {
                sb.append(mStartDestIdName);
            }
        }
        else {
            sb.append("{");
            sb.append(node.toString());
            sb.append("}");
        }
        return sb.toString();
    }
}

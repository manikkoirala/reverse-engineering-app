// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.util.Map;
import java.util.HashMap;

public class NavigatorProvider
{
    private static final HashMap<Class<?>, String> sAnnotationNames;
    private final HashMap<String, Navigator<? extends NavDestination>> mNavigators;
    
    static {
        sAnnotationNames = new HashMap<Class<?>, String>();
    }
    
    public NavigatorProvider() {
        this.mNavigators = new HashMap<String, Navigator<? extends NavDestination>>();
    }
    
    static String getNameForNavigator(final Class<? extends Navigator> clazz) {
        final HashMap<Class<?>, String> sAnnotationNames = NavigatorProvider.sAnnotationNames;
        String value;
        if ((value = sAnnotationNames.get(clazz)) == null) {
            final Navigator.Name name = clazz.getAnnotation(Navigator.Name.class);
            if (name != null) {
                value = name.value();
            }
            else {
                value = null;
            }
            if (!validateName(value)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("No @Navigator.Name annotation found for ");
                sb.append(clazz.getSimpleName());
                throw new IllegalArgumentException(sb.toString());
            }
            sAnnotationNames.put(clazz, value);
        }
        return value;
    }
    
    private static boolean validateName(final String s) {
        return s != null && !s.isEmpty();
    }
    
    public final Navigator<? extends NavDestination> addNavigator(final Navigator<? extends NavDestination> navigator) {
        return this.addNavigator(getNameForNavigator(navigator.getClass()), navigator);
    }
    
    public Navigator<? extends NavDestination> addNavigator(final String key, final Navigator<? extends NavDestination> value) {
        if (validateName(key)) {
            return this.mNavigators.put(key, value);
        }
        throw new IllegalArgumentException("navigator name cannot be an empty string");
    }
    
    public final <T extends Navigator<?>> T getNavigator(final Class<T> clazz) {
        return this.getNavigator(getNameForNavigator(clazz));
    }
    
    public <T extends Navigator<?>> T getNavigator(final String s) {
        if (!validateName(s)) {
            throw new IllegalArgumentException("navigator name cannot be an empty string");
        }
        final Navigator navigator = this.mNavigators.get(s);
        if (navigator != null) {
            return (T)navigator;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Could not find Navigator with name \"");
        sb.append(s);
        sb.append("\". You must call NavController.addNavigator() for each navigation type.");
        throw new IllegalStateException(sb.toString());
    }
    
    Map<String, Navigator<? extends NavDestination>> getNavigators() {
        return this.mNavigators;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.io.Serializable;
import android.os.Parcelable;
import android.os.Bundle;

public abstract class NavType<T>
{
    public static final NavType<boolean[]> BoolArrayType;
    public static final NavType<Boolean> BoolType;
    public static final NavType<float[]> FloatArrayType;
    public static final NavType<Float> FloatType;
    public static final NavType<int[]> IntArrayType;
    public static final NavType<Integer> IntType;
    public static final NavType<long[]> LongArrayType;
    public static final NavType<Long> LongType;
    public static final NavType<Integer> ReferenceType;
    public static final NavType<String[]> StringArrayType;
    public static final NavType<String> StringType;
    private final boolean mNullableAllowed;
    
    static {
        IntType = new NavType<Integer>() {
            @Override
            public Integer get(final Bundle bundle, final String s) {
                return (Integer)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "integer";
            }
            
            @Override
            public Integer parseValue(final String s) {
                if (s.startsWith("0x")) {
                    return Integer.parseInt(s.substring(2), 16);
                }
                return Integer.parseInt(s);
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final Integer n) {
                bundle.putInt(s, (int)n);
            }
        };
        ReferenceType = new NavType<Integer>() {
            @Override
            public Integer get(final Bundle bundle, final String s) {
                return (Integer)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "reference";
            }
            
            @Override
            public Integer parseValue(final String s) {
                if (s.startsWith("0x")) {
                    return Integer.parseInt(s.substring(2), 16);
                }
                return Integer.parseInt(s);
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final Integer n) {
                bundle.putInt(s, (int)n);
            }
        };
        IntArrayType = new NavType<int[]>() {
            @Override
            public int[] get(final Bundle bundle, final String s) {
                return (int[])bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "integer[]";
            }
            
            @Override
            public int[] parseValue(final String s) {
                throw new UnsupportedOperationException("Arrays don't support default values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final int[] array) {
                bundle.putIntArray(s, array);
            }
        };
        LongType = new NavType<Long>() {
            @Override
            public Long get(final Bundle bundle, final String s) {
                return (Long)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "long";
            }
            
            @Override
            public Long parseValue(final String s) {
                String substring = s;
                if (s.endsWith("L")) {
                    substring = s.substring(0, s.length() - 1);
                }
                if (substring.startsWith("0x")) {
                    return Long.parseLong(substring.substring(2), 16);
                }
                return Long.parseLong(substring);
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final Long n) {
                bundle.putLong(s, (long)n);
            }
        };
        LongArrayType = new NavType<long[]>() {
            @Override
            public long[] get(final Bundle bundle, final String s) {
                return (long[])bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "long[]";
            }
            
            @Override
            public long[] parseValue(final String s) {
                throw new UnsupportedOperationException("Arrays don't support default values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final long[] array) {
                bundle.putLongArray(s, array);
            }
        };
        FloatType = new NavType<Float>() {
            @Override
            public Float get(final Bundle bundle, final String s) {
                return (Float)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "float";
            }
            
            @Override
            public Float parseValue(final String s) {
                return Float.parseFloat(s);
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final Float n) {
                bundle.putFloat(s, (float)n);
            }
        };
        FloatArrayType = new NavType<float[]>() {
            @Override
            public float[] get(final Bundle bundle, final String s) {
                return (float[])bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "float[]";
            }
            
            @Override
            public float[] parseValue(final String s) {
                throw new UnsupportedOperationException("Arrays don't support default values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final float[] array) {
                bundle.putFloatArray(s, array);
            }
        };
        BoolType = new NavType<Boolean>() {
            @Override
            public Boolean get(final Bundle bundle, final String s) {
                return (Boolean)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "boolean";
            }
            
            @Override
            public Boolean parseValue(final String s) {
                if ("true".equals(s)) {
                    return true;
                }
                if ("false".equals(s)) {
                    return false;
                }
                throw new IllegalArgumentException("A boolean NavType only accepts \"true\" or \"false\" values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final Boolean b) {
                bundle.putBoolean(s, (boolean)b);
            }
        };
        BoolArrayType = new NavType<boolean[]>() {
            @Override
            public boolean[] get(final Bundle bundle, final String s) {
                return (boolean[])bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "boolean[]";
            }
            
            @Override
            public boolean[] parseValue(final String s) {
                throw new UnsupportedOperationException("Arrays don't support default values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final boolean[] array) {
                bundle.putBooleanArray(s, array);
            }
        };
        StringType = new NavType<String>() {
            @Override
            public String get(final Bundle bundle, final String s) {
                return (String)bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "string";
            }
            
            @Override
            public String parseValue(final String s) {
                return s;
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final String s2) {
                bundle.putString(s, s2);
            }
        };
        StringArrayType = new NavType<String[]>() {
            @Override
            public String[] get(final Bundle bundle, final String s) {
                return (String[])bundle.get(s);
            }
            
            @Override
            public String getName() {
                return "string[]";
            }
            
            @Override
            public String[] parseValue(final String s) {
                throw new UnsupportedOperationException("Arrays don't support default values.");
            }
            
            @Override
            public void put(final Bundle bundle, final String s, final String[] array) {
                bundle.putStringArray(s, array);
            }
        };
    }
    
    NavType(final boolean mNullableAllowed) {
        this.mNullableAllowed = mNullableAllowed;
    }
    
    public static NavType<?> fromArgType(final String s, String s2) {
        final NavType<Integer> intType = NavType.IntType;
        if (intType.getName().equals(s)) {
            return intType;
        }
        final NavType<int[]> intArrayType = NavType.IntArrayType;
        if (intArrayType.getName().equals(s)) {
            return intArrayType;
        }
        final NavType<Long> longType = NavType.LongType;
        if (longType.getName().equals(s)) {
            return longType;
        }
        final NavType<long[]> longArrayType = NavType.LongArrayType;
        if (longArrayType.getName().equals(s)) {
            return longArrayType;
        }
        final NavType<Boolean> boolType = NavType.BoolType;
        if (boolType.getName().equals(s)) {
            return boolType;
        }
        final NavType<boolean[]> boolArrayType = NavType.BoolArrayType;
        if (boolArrayType.getName().equals(s)) {
            return boolArrayType;
        }
        final NavType<String> stringType = NavType.StringType;
        if (stringType.getName().equals(s)) {
            return stringType;
        }
        final NavType<String[]> stringArrayType = NavType.StringArrayType;
        if (stringArrayType.getName().equals(s)) {
            return stringArrayType;
        }
        final NavType<Float> floatType = NavType.FloatType;
        if (floatType.getName().equals(s)) {
            return floatType;
        }
        final NavType<float[]> floatArrayType = NavType.FloatArrayType;
        if (floatArrayType.getName().equals(s)) {
            return floatArrayType;
        }
        final NavType<Integer> referenceType = NavType.ReferenceType;
        if (referenceType.getName().equals(s)) {
            return referenceType;
        }
        if (s != null && !s.isEmpty()) {
            try {
                if (s.startsWith(".") && s2 != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s2);
                    sb.append(s);
                    s2 = sb.toString();
                }
                else {
                    s2 = s;
                }
                if (s.endsWith("[]")) {
                    s2 = s2.substring(0, s2.length() - 2);
                    final Class<?> forName = Class.forName(s2);
                    if (Parcelable.class.isAssignableFrom(forName)) {
                        return new ParcelableArrayType<Object>((Class<Parcelable>)forName);
                    }
                    if (Serializable.class.isAssignableFrom(forName)) {
                        return new SerializableArrayType<Object>(forName);
                    }
                }
                else {
                    final Class<?> forName2 = Class.forName(s2);
                    if (Parcelable.class.isAssignableFrom(forName2)) {
                        return new ParcelableType<Object>((Class<Object>)forName2);
                    }
                    if (Enum.class.isAssignableFrom(forName2)) {
                        return new EnumType<Object>(forName2);
                    }
                    if (Serializable.class.isAssignableFrom(forName2)) {
                        return new SerializableType<Object>(forName2);
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s2);
                sb2.append(" is not Serializable or Parcelable.");
                throw new IllegalArgumentException(sb2.toString());
            }
            catch (final ClassNotFoundException cause) {
                throw new RuntimeException(cause);
            }
        }
        return stringType;
    }
    
    static NavType inferFromValue(final String s) {
        try {
            final NavType<Integer> intType = NavType.IntType;
            intType.parseValue(s);
            return intType;
        }
        catch (final IllegalArgumentException ex) {
            try {
                final NavType<Long> longType = NavType.LongType;
                longType.parseValue(s);
                return longType;
            }
            catch (final IllegalArgumentException ex2) {
                try {
                    final NavType<Float> floatType = NavType.FloatType;
                    floatType.parseValue(s);
                    return floatType;
                }
                catch (final IllegalArgumentException ex3) {
                    try {
                        final NavType<Boolean> boolType = NavType.BoolType;
                        boolType.parseValue(s);
                        return boolType;
                    }
                    catch (final IllegalArgumentException ex4) {
                        return NavType.StringType;
                    }
                }
            }
        }
    }
    
    static NavType inferFromValueType(final Object o) {
        if (o instanceof Integer) {
            return NavType.IntType;
        }
        if (o instanceof int[]) {
            return NavType.IntArrayType;
        }
        if (o instanceof Long) {
            return NavType.LongType;
        }
        if (o instanceof long[]) {
            return NavType.LongArrayType;
        }
        if (o instanceof Float) {
            return NavType.FloatType;
        }
        if (o instanceof float[]) {
            return NavType.FloatArrayType;
        }
        if (o instanceof Boolean) {
            return NavType.BoolType;
        }
        if (o instanceof boolean[]) {
            return NavType.BoolArrayType;
        }
        if (o instanceof String || o == null) {
            return NavType.StringType;
        }
        if (o instanceof String[]) {
            return NavType.StringArrayType;
        }
        if (o.getClass().isArray() && Parcelable.class.isAssignableFrom(o.getClass().getComponentType())) {
            return new ParcelableArrayType(o.getClass().getComponentType());
        }
        if (o.getClass().isArray() && Serializable.class.isAssignableFrom(o.getClass().getComponentType())) {
            return new SerializableArrayType(o.getClass().getComponentType());
        }
        if (o instanceof Parcelable) {
            return new ParcelableType(o.getClass());
        }
        if (o instanceof Enum) {
            return new EnumType(o.getClass());
        }
        if (o instanceof Serializable) {
            return new SerializableType(o.getClass());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Object of type ");
        sb.append(o.getClass().getName());
        sb.append(" is not supported for navigation arguments.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public abstract T get(final Bundle p0, final String p1);
    
    public abstract String getName();
    
    public boolean isNullableAllowed() {
        return this.mNullableAllowed;
    }
    
    T parseAndPut(final Bundle bundle, final String s, final String s2) {
        final T value = this.parseValue(s2);
        this.put(bundle, s, value);
        return value;
    }
    
    public abstract T parseValue(final String p0);
    
    public abstract void put(final Bundle p0, final String p1, final T p2);
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    public static final class EnumType<D extends Enum> extends SerializableType<D>
    {
        private final Class<D> mType;
        
        public EnumType(final Class<D> clazz) {
            super(false, clazz);
            if (clazz.isEnum()) {
                this.mType = clazz;
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(clazz);
            sb.append(" is not an Enum type.");
            throw new IllegalArgumentException(sb.toString());
        }
        
        @Override
        public String getName() {
            return this.mType.getName();
        }
        
        @Override
        public D parseValue(final String s) {
            for (final Enum enum1 : this.mType.getEnumConstants()) {
                final Enum enum2 = enum1;
                if (enum1.name().equals(s)) {
                    return (D)enum1;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Enum value ");
            sb.append(s);
            sb.append(" not found for type ");
            sb.append(this.mType.getName());
            sb.append(".");
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public static final class ParcelableArrayType<D extends Parcelable> extends NavType<D[]>
    {
        private final Class<D[]> mArrayType;
        
        public ParcelableArrayType(final Class<D> obj) {
            super(true);
            if (Parcelable.class.isAssignableFrom(obj)) {
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("[L");
                    sb.append(obj.getName());
                    sb.append(";");
                    this.mArrayType = (Class<D[]>)Class.forName(sb.toString());
                    return;
                }
                catch (final ClassNotFoundException cause) {
                    throw new RuntimeException(cause);
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(" does not implement Parcelable.");
            throw new IllegalArgumentException(sb2.toString());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && this.mArrayType.equals(((ParcelableArrayType)o).mArrayType));
        }
        
        @Override
        public D[] get(final Bundle bundle, final String s) {
            return (D[])bundle.get(s);
        }
        
        @Override
        public String getName() {
            return this.mArrayType.getName();
        }
        
        @Override
        public int hashCode() {
            return this.mArrayType.hashCode();
        }
        
        @Override
        public D[] parseValue(final String s) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }
        
        @Override
        public void put(final Bundle bundle, final String s, final D[] obj) {
            this.mArrayType.cast(obj);
            bundle.putParcelableArray(s, (Parcelable[])obj);
        }
    }
    
    public static final class ParcelableType<D> extends NavType<D>
    {
        private final Class<D> mType;
        
        public ParcelableType(final Class<D> clazz) {
            super(true);
            if (!Parcelable.class.isAssignableFrom(clazz) && !Serializable.class.isAssignableFrom(clazz)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz);
                sb.append(" does not implement Parcelable or Serializable.");
                throw new IllegalArgumentException(sb.toString());
            }
            this.mType = clazz;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && this.mType.equals(((ParcelableType)o).mType));
        }
        
        @Override
        public D get(final Bundle bundle, final String s) {
            return (D)bundle.get(s);
        }
        
        @Override
        public String getName() {
            return this.mType.getName();
        }
        
        @Override
        public int hashCode() {
            return this.mType.hashCode();
        }
        
        @Override
        public D parseValue(final String s) {
            throw new UnsupportedOperationException("Parcelables don't support default values.");
        }
        
        @Override
        public void put(final Bundle bundle, final String s, final D obj) {
            this.mType.cast(obj);
            if (obj != null && !(obj instanceof Parcelable)) {
                if (obj instanceof Serializable) {
                    bundle.putSerializable(s, (Serializable)obj);
                }
            }
            else {
                bundle.putParcelable(s, (Parcelable)obj);
            }
        }
    }
    
    public static final class SerializableArrayType<D extends Serializable> extends NavType<D[]>
    {
        private final Class<D[]> mArrayType;
        
        public SerializableArrayType(final Class<D> obj) {
            super(true);
            if (Serializable.class.isAssignableFrom(obj)) {
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("[L");
                    sb.append(obj.getName());
                    sb.append(";");
                    this.mArrayType = (Class<D[]>)Class.forName(sb.toString());
                    return;
                }
                catch (final ClassNotFoundException cause) {
                    throw new RuntimeException(cause);
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(" does not implement Serializable.");
            throw new IllegalArgumentException(sb2.toString());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && this.mArrayType.equals(((SerializableArrayType)o).mArrayType));
        }
        
        @Override
        public D[] get(final Bundle bundle, final String s) {
            return (D[])bundle.get(s);
        }
        
        @Override
        public String getName() {
            return this.mArrayType.getName();
        }
        
        @Override
        public int hashCode() {
            return this.mArrayType.hashCode();
        }
        
        @Override
        public D[] parseValue(final String s) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }
        
        @Override
        public void put(final Bundle bundle, final String s, final D[] obj) {
            this.mArrayType.cast(obj);
            bundle.putSerializable(s, (Serializable)obj);
        }
    }
    
    public static class SerializableType<D extends Serializable> extends NavType<D>
    {
        private final Class<D> mType;
        
        public SerializableType(final Class<D> obj) {
            super(true);
            if (!Serializable.class.isAssignableFrom(obj)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(obj);
                sb.append(" does not implement Serializable.");
                throw new IllegalArgumentException(sb.toString());
            }
            if (!obj.isEnum()) {
                this.mType = obj;
                return;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(" is an Enum. You should use EnumType instead.");
            throw new IllegalArgumentException(sb2.toString());
        }
        
        SerializableType(final boolean b, final Class<D> clazz) {
            super(b);
            if (Serializable.class.isAssignableFrom(clazz)) {
                this.mType = clazz;
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(clazz);
            sb.append(" does not implement Serializable.");
            throw new IllegalArgumentException(sb.toString());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof SerializableType && this.mType.equals(((SerializableType)o).mType));
        }
        
        @Override
        public D get(final Bundle bundle, final String s) {
            return (D)bundle.get(s);
        }
        
        @Override
        public String getName() {
            return this.mType.getName();
        }
        
        @Override
        public int hashCode() {
            return this.mType.hashCode();
        }
        
        @Override
        public D parseValue(final String s) {
            throw new UnsupportedOperationException("Serializables don't support default values.");
        }
        
        @Override
        public void put(final Bundle bundle, final String s, final D obj) {
            this.mType.cast(obj);
            bundle.putSerializable(s, (Serializable)obj);
        }
    }
}

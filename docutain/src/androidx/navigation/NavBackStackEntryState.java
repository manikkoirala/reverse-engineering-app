// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Parcel;
import java.util.UUID;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

final class NavBackStackEntryState implements Parcelable
{
    public static final Parcelable$Creator<NavBackStackEntryState> CREATOR;
    private final Bundle mArgs;
    private final int mDestinationId;
    private final Bundle mSavedState;
    private final UUID mUUID;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<NavBackStackEntryState>() {
            public NavBackStackEntryState createFromParcel(final Parcel parcel) {
                return new NavBackStackEntryState(parcel);
            }
            
            public NavBackStackEntryState[] newArray(final int n) {
                return new NavBackStackEntryState[n];
            }
        };
    }
    
    NavBackStackEntryState(final Parcel parcel) {
        this.mUUID = UUID.fromString(parcel.readString());
        this.mDestinationId = parcel.readInt();
        this.mArgs = parcel.readBundle(this.getClass().getClassLoader());
        this.mSavedState = parcel.readBundle(this.getClass().getClassLoader());
    }
    
    NavBackStackEntryState(final NavBackStackEntry navBackStackEntry) {
        this.mUUID = navBackStackEntry.mId;
        this.mDestinationId = navBackStackEntry.getDestination().getId();
        this.mArgs = navBackStackEntry.getArguments();
        navBackStackEntry.saveState(this.mSavedState = new Bundle());
    }
    
    public int describeContents() {
        return 0;
    }
    
    Bundle getArgs() {
        return this.mArgs;
    }
    
    int getDestinationId() {
        return this.mDestinationId;
    }
    
    Bundle getSavedState() {
        return this.mSavedState;
    }
    
    UUID getUUID() {
        return this.mUUID;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.mUUID.toString());
        parcel.writeInt(this.mDestinationId);
        parcel.writeBundle(this.mArgs);
        parcel.writeBundle(this.mSavedState);
    }
}

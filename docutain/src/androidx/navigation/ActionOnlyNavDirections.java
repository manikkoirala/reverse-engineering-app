// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

public final class ActionOnlyNavDirections implements NavDirections
{
    private final int mActionId;
    
    public ActionOnlyNavDirections(final int mActionId) {
        this.mActionId = mActionId;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && this.getActionId() == ((ActionOnlyNavDirections)o).getActionId());
    }
    
    @Override
    public int getActionId() {
        return this.mActionId;
    }
    
    @Override
    public Bundle getArguments() {
        return new Bundle();
    }
    
    @Override
    public int hashCode() {
        return 31 + this.getActionId();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActionOnlyNavDirections(actionId=");
        sb.append(this.getActionId());
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.ComponentName;
import androidx.core.app.ActivityOptionsCompat;
import android.content.res.Resources;
import java.util.regex.Matcher;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.net.Uri;
import java.util.regex.Pattern;
import android.text.TextUtils;
import android.os.Bundle;
import android.content.Intent;
import android.content.ContextWrapper;
import android.app.Activity;
import android.content.Context;

@Name("activity")
public class ActivityNavigator extends Navigator<Destination>
{
    private static final String EXTRA_NAV_CURRENT = "android-support-navigation:ActivityNavigator:current";
    private static final String EXTRA_NAV_SOURCE = "android-support-navigation:ActivityNavigator:source";
    private static final String EXTRA_POP_ENTER_ANIM = "android-support-navigation:ActivityNavigator:popEnterAnim";
    private static final String EXTRA_POP_EXIT_ANIM = "android-support-navigation:ActivityNavigator:popExitAnim";
    private static final String LOG_TAG = "ActivityNavigator";
    private Context mContext;
    private Activity mHostActivity;
    
    public ActivityNavigator(Context baseContext) {
        this.mContext = baseContext;
        while (baseContext instanceof ContextWrapper) {
            if (baseContext instanceof Activity) {
                this.mHostActivity = (Activity)baseContext;
                break;
            }
            baseContext = ((ContextWrapper)baseContext).getBaseContext();
        }
    }
    
    public static void applyPopAnimationsToPendingTransition(final Activity activity) {
        final Intent intent = activity.getIntent();
        if (intent == null) {
            return;
        }
        int intExtra = intent.getIntExtra("android-support-navigation:ActivityNavigator:popEnterAnim", -1);
        int intExtra2 = intent.getIntExtra("android-support-navigation:ActivityNavigator:popExitAnim", -1);
        if (intExtra != -1 || intExtra2 != -1) {
            if (intExtra == -1) {
                intExtra = 0;
            }
            if (intExtra2 == -1) {
                intExtra2 = 0;
            }
            activity.overridePendingTransition(intExtra, intExtra2);
        }
    }
    
    @Override
    public Destination createDestination() {
        return new Destination(this);
    }
    
    final Context getContext() {
        return this.mContext;
    }
    
    @Override
    public NavDestination navigate(final Destination destination, final Bundle obj, final NavOptions navOptions, final Navigator.Extras extras) {
        if (destination.getIntent() != null) {
            final Intent intent = new Intent(destination.getIntent());
            if (obj != null) {
                intent.putExtras(obj);
                final String dataPattern = destination.getDataPattern();
                if (!TextUtils.isEmpty((CharSequence)dataPattern)) {
                    final StringBuffer sb = new StringBuffer();
                    final Matcher matcher = Pattern.compile("\\{(.+?)\\}").matcher(dataPattern);
                    while (matcher.find()) {
                        final String group = matcher.group(1);
                        if (!obj.containsKey(group)) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Could not find ");
                            sb2.append(group);
                            sb2.append(" in ");
                            sb2.append(obj);
                            sb2.append(" to fill data pattern ");
                            sb2.append(dataPattern);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        matcher.appendReplacement(sb, "");
                        sb.append(Uri.encode(obj.get(group).toString()));
                    }
                    matcher.appendTail(sb);
                    intent.setData(Uri.parse(sb.toString()));
                }
            }
            final boolean b = extras instanceof Extras;
            if (b) {
                intent.addFlags(((Extras)extras).getFlags());
            }
            if (!(this.mContext instanceof Activity)) {
                intent.addFlags(268435456);
            }
            if (navOptions != null && navOptions.shouldLaunchSingleTop()) {
                intent.addFlags(536870912);
            }
            final Activity mHostActivity = this.mHostActivity;
            if (mHostActivity != null) {
                final Intent intent2 = mHostActivity.getIntent();
                if (intent2 != null) {
                    final int intExtra = intent2.getIntExtra("android-support-navigation:ActivityNavigator:current", 0);
                    if (intExtra != 0) {
                        intent.putExtra("android-support-navigation:ActivityNavigator:source", intExtra);
                    }
                }
            }
            intent.putExtra("android-support-navigation:ActivityNavigator:current", destination.getId());
            final Resources resources = this.getContext().getResources();
            if (navOptions != null) {
                final int popEnterAnim = navOptions.getPopEnterAnim();
                final int popExitAnim = navOptions.getPopExitAnim();
                if ((popEnterAnim > 0 && resources.getResourceTypeName(popEnterAnim).equals("animator")) || (popExitAnim > 0 && resources.getResourceTypeName(popExitAnim).equals("animator"))) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Activity destinations do not support Animator resource. Ignoring popEnter resource ");
                    sb3.append(resources.getResourceName(popEnterAnim));
                    sb3.append(" and popExit resource ");
                    sb3.append(resources.getResourceName(popExitAnim));
                    sb3.append("when launching ");
                    sb3.append(destination);
                    Log.w("ActivityNavigator", sb3.toString());
                }
                else {
                    intent.putExtra("android-support-navigation:ActivityNavigator:popEnterAnim", popEnterAnim);
                    intent.putExtra("android-support-navigation:ActivityNavigator:popExitAnim", popExitAnim);
                }
            }
            if (b) {
                final ActivityOptionsCompat activityOptions = ((Extras)extras).getActivityOptions();
                if (activityOptions != null) {
                    ContextCompat.startActivity(this.mContext, intent, activityOptions.toBundle());
                }
                else {
                    this.mContext.startActivity(intent);
                }
            }
            else {
                this.mContext.startActivity(intent);
            }
            if (navOptions != null && this.mHostActivity != null) {
                final int enterAnim = navOptions.getEnterAnim();
                final int exitAnim = navOptions.getExitAnim();
                if ((enterAnim > 0 && resources.getResourceTypeName(enterAnim).equals("animator")) || (exitAnim > 0 && resources.getResourceTypeName(exitAnim).equals("animator"))) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Activity destinations do not support Animator resource. Ignoring enter resource ");
                    sb4.append(resources.getResourceName(enterAnim));
                    sb4.append(" and exit resource ");
                    sb4.append(resources.getResourceName(exitAnim));
                    sb4.append("when launching ");
                    sb4.append(destination);
                    Log.w("ActivityNavigator", sb4.toString());
                }
                else if (enterAnim >= 0 || exitAnim >= 0) {
                    this.mHostActivity.overridePendingTransition(Math.max(enterAnim, 0), Math.max(exitAnim, 0));
                }
            }
            return null;
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("Destination ");
        sb5.append(destination.getId());
        sb5.append(" does not have an Intent set.");
        throw new IllegalStateException(sb5.toString());
    }
    
    @Override
    public boolean popBackStack() {
        final Activity mHostActivity = this.mHostActivity;
        if (mHostActivity != null) {
            mHostActivity.finish();
            return true;
        }
        return false;
    }
    
    public static class Destination extends NavDestination
    {
        private String mDataPattern;
        private Intent mIntent;
        
        public Destination(final Navigator<? extends Destination> navigator) {
            super(navigator);
        }
        
        public Destination(final NavigatorProvider navigatorProvider) {
            this(navigatorProvider.getNavigator(ActivityNavigator.class));
        }
        
        public final String getAction() {
            final Intent mIntent = this.mIntent;
            if (mIntent == null) {
                return null;
            }
            return mIntent.getAction();
        }
        
        public final ComponentName getComponent() {
            final Intent mIntent = this.mIntent;
            if (mIntent == null) {
                return null;
            }
            return mIntent.getComponent();
        }
        
        public final Uri getData() {
            final Intent mIntent = this.mIntent;
            if (mIntent == null) {
                return null;
            }
            return mIntent.getData();
        }
        
        public final String getDataPattern() {
            return this.mDataPattern;
        }
        
        public final Intent getIntent() {
            return this.mIntent;
        }
        
        public final String getTargetPackage() {
            final Intent mIntent = this.mIntent;
            if (mIntent == null) {
                return null;
            }
            return mIntent.getPackage();
        }
        
        @Override
        public void onInflate(final Context context, final AttributeSet set) {
            super.onInflate(context, set);
            final TypedArray obtainAttributes = context.getResources().obtainAttributes(set, R.styleable.ActivityNavigator);
            String targetPackage;
            final String s = targetPackage = obtainAttributes.getString(R.styleable.ActivityNavigator_targetPackage);
            if (s != null) {
                targetPackage = s.replace("${applicationId}", context.getPackageName());
            }
            this.setTargetPackage(targetPackage);
            final String string = obtainAttributes.getString(R.styleable.ActivityNavigator_android_name);
            if (string != null) {
                String string2 = string;
                if (string.charAt(0) == '.') {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(context.getPackageName());
                    sb.append(string);
                    string2 = sb.toString();
                }
                this.setComponentName(new ComponentName(context, string2));
            }
            this.setAction(obtainAttributes.getString(R.styleable.ActivityNavigator_action));
            final String string3 = obtainAttributes.getString(R.styleable.ActivityNavigator_data);
            if (string3 != null) {
                this.setData(Uri.parse(string3));
            }
            this.setDataPattern(obtainAttributes.getString(R.styleable.ActivityNavigator_dataPattern));
            obtainAttributes.recycle();
        }
        
        public final Destination setAction(final String action) {
            if (this.mIntent == null) {
                this.mIntent = new Intent();
            }
            this.mIntent.setAction(action);
            return this;
        }
        
        public final Destination setComponentName(final ComponentName component) {
            if (this.mIntent == null) {
                this.mIntent = new Intent();
            }
            this.mIntent.setComponent(component);
            return this;
        }
        
        public final Destination setData(final Uri data) {
            if (this.mIntent == null) {
                this.mIntent = new Intent();
            }
            this.mIntent.setData(data);
            return this;
        }
        
        public final Destination setDataPattern(final String mDataPattern) {
            this.mDataPattern = mDataPattern;
            return this;
        }
        
        public final Destination setIntent(final Intent mIntent) {
            this.mIntent = mIntent;
            return this;
        }
        
        public final Destination setTargetPackage(final String package1) {
            if (this.mIntent == null) {
                this.mIntent = new Intent();
            }
            this.mIntent.setPackage(package1);
            return this;
        }
        
        @Override
        boolean supportsActions() {
            return false;
        }
        
        @Override
        public String toString() {
            final ComponentName component = this.getComponent();
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            if (component != null) {
                sb.append(" class=");
                sb.append(component.getClassName());
            }
            else {
                final String action = this.getAction();
                if (action != null) {
                    sb.append(" action=");
                    sb.append(action);
                }
            }
            return sb.toString();
        }
    }
    
    public static final class Extras implements Navigator.Extras
    {
        private final ActivityOptionsCompat mActivityOptions;
        private final int mFlags;
        
        Extras(final int mFlags, final ActivityOptionsCompat mActivityOptions) {
            this.mFlags = mFlags;
            this.mActivityOptions = mActivityOptions;
        }
        
        public ActivityOptionsCompat getActivityOptions() {
            return this.mActivityOptions;
        }
        
        public int getFlags() {
            return this.mFlags;
        }
        
        public static final class Builder
        {
            private ActivityOptionsCompat mActivityOptions;
            private int mFlags;
            
            public Builder addFlags(final int n) {
                this.mFlags |= n;
                return this;
            }
            
            public Extras build() {
                return new Extras(this.mFlags, this.mActivityOptions);
            }
            
            public Builder setActivityOptions(final ActivityOptionsCompat mActivityOptions) {
                this.mActivityOptions = mActivityOptions;
                return this;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.util.regex.Pattern;
import android.content.Intent;
import android.net.Uri;

public class NavDeepLinkRequest
{
    private final String mAction;
    private final String mMimeType;
    private final Uri mUri;
    
    NavDeepLinkRequest(final Intent intent) {
        this(intent.getData(), intent.getAction(), intent.getType());
    }
    
    NavDeepLinkRequest(final Uri mUri, final String mAction, final String mMimeType) {
        this.mUri = mUri;
        this.mAction = mAction;
        this.mMimeType = mMimeType;
    }
    
    public String getAction() {
        return this.mAction;
    }
    
    public String getMimeType() {
        return this.mMimeType;
    }
    
    public Uri getUri() {
        return this.mUri;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NavDeepLinkRequest");
        sb.append("{");
        if (this.mUri != null) {
            sb.append(" uri=");
            sb.append(this.mUri.toString());
        }
        if (this.mAction != null) {
            sb.append(" action=");
            sb.append(this.mAction);
        }
        if (this.mMimeType != null) {
            sb.append(" mimetype=");
            sb.append(this.mMimeType);
        }
        sb.append(" }");
        return sb.toString();
    }
    
    public static final class Builder
    {
        private String mAction;
        private String mMimeType;
        private Uri mUri;
        
        private Builder() {
        }
        
        public static Builder fromAction(final String action) {
            if (!action.isEmpty()) {
                final Builder builder = new Builder();
                builder.setAction(action);
                return builder;
            }
            throw new IllegalArgumentException("The NavDeepLinkRequest cannot have an empty action.");
        }
        
        public static Builder fromMimeType(final String mimeType) {
            final Builder builder = new Builder();
            builder.setMimeType(mimeType);
            return builder;
        }
        
        public static Builder fromUri(final Uri uri) {
            final Builder builder = new Builder();
            builder.setUri(uri);
            return builder;
        }
        
        public NavDeepLinkRequest build() {
            return new NavDeepLinkRequest(this.mUri, this.mAction, this.mMimeType);
        }
        
        public Builder setAction(final String mAction) {
            if (!mAction.isEmpty()) {
                this.mAction = mAction;
                return this;
            }
            throw new IllegalArgumentException("The NavDeepLinkRequest cannot have an empty action.");
        }
        
        public Builder setMimeType(final String str) {
            if (Pattern.compile("^[-\\w*.]+/[-\\w+*.]+$").matcher(str).matches()) {
                this.mMimeType = str;
                return this;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("The given mimeType ");
            sb.append(str);
            sb.append(" does not match to required \"type/subtype\" format");
            throw new IllegalArgumentException(sb.toString());
        }
        
        public Builder setUri(final Uri mUri) {
            this.mUri = mUri;
            return this;
        }
    }
}

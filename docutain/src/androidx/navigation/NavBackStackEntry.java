// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.ViewModelStore;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.SavedStateViewModelFactory;
import android.app.Application;
import androidx.lifecycle.HasDefaultViewModelProviderFactory$_CC;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.savedstate.SavedStateRegistryController;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.LifecycleRegistry;
import java.util.UUID;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import android.content.Context;
import android.os.Bundle;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.LifecycleOwner;

public final class NavBackStackEntry implements LifecycleOwner, ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner
{
    private Bundle mArgs;
    private final Context mContext;
    private ViewModelProvider.Factory mDefaultFactory;
    private final NavDestination mDestination;
    private Lifecycle.State mHostLifecycle;
    final UUID mId;
    private final LifecycleRegistry mLifecycle;
    private Lifecycle.State mMaxLifecycle;
    private NavControllerViewModel mNavControllerViewModel;
    private SavedStateHandle mSavedStateHandle;
    private final SavedStateRegistryController mSavedStateRegistryController;
    
    NavBackStackEntry(final Context context, final NavDestination navDestination, final Bundle bundle, final LifecycleOwner lifecycleOwner, final NavControllerViewModel navControllerViewModel) {
        this(context, navDestination, bundle, lifecycleOwner, navControllerViewModel, UUID.randomUUID(), null);
    }
    
    NavBackStackEntry(final Context mContext, final NavDestination mDestination, final Bundle mArgs, final LifecycleOwner lifecycleOwner, final NavControllerViewModel mNavControllerViewModel, final UUID mId, final Bundle bundle) {
        this.mLifecycle = new LifecycleRegistry(this);
        final SavedStateRegistryController create = SavedStateRegistryController.create(this);
        this.mSavedStateRegistryController = create;
        this.mHostLifecycle = Lifecycle.State.CREATED;
        this.mMaxLifecycle = Lifecycle.State.RESUMED;
        this.mContext = mContext;
        this.mId = mId;
        this.mDestination = mDestination;
        this.mArgs = mArgs;
        this.mNavControllerViewModel = mNavControllerViewModel;
        create.performRestore(bundle);
        if (lifecycleOwner != null) {
            this.mHostLifecycle = lifecycleOwner.getLifecycle().getCurrentState();
        }
    }
    
    private static Lifecycle.State getStateAfter(final Lifecycle.Event obj) {
        switch (NavBackStackEntry$1.$SwitchMap$androidx$lifecycle$Lifecycle$Event[obj.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected event value ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            case 6: {
                return Lifecycle.State.DESTROYED;
            }
            case 5: {
                return Lifecycle.State.RESUMED;
            }
            case 3:
            case 4: {
                return Lifecycle.State.STARTED;
            }
            case 1:
            case 2: {
                return Lifecycle.State.CREATED;
            }
        }
    }
    
    public Bundle getArguments() {
        return this.mArgs;
    }
    
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        if (this.mDefaultFactory == null) {
            this.mDefaultFactory = new SavedStateViewModelFactory((Application)this.mContext.getApplicationContext(), this, this.mArgs);
        }
        return this.mDefaultFactory;
    }
    
    public NavDestination getDestination() {
        return this.mDestination;
    }
    
    @Override
    public Lifecycle getLifecycle() {
        return this.mLifecycle;
    }
    
    Lifecycle.State getMaxLifecycle() {
        return this.mMaxLifecycle;
    }
    
    public SavedStateHandle getSavedStateHandle() {
        if (this.mSavedStateHandle == null) {
            this.mSavedStateHandle = new ViewModelProvider(this, (ViewModelProvider.Factory)new NavResultSavedStateFactory(this, null)).get(SavedStateViewModel.class).getHandle();
        }
        return this.mSavedStateHandle;
    }
    
    @Override
    public SavedStateRegistry getSavedStateRegistry() {
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    @Override
    public ViewModelStore getViewModelStore() {
        final NavControllerViewModel mNavControllerViewModel = this.mNavControllerViewModel;
        if (mNavControllerViewModel != null) {
            return mNavControllerViewModel.getViewModelStore(this.mId);
        }
        throw new IllegalStateException("You must call setViewModelStore() on your NavHostController before accessing the ViewModelStore of a navigation graph.");
    }
    
    void handleLifecycleEvent(final Lifecycle.Event event) {
        this.mHostLifecycle = getStateAfter(event);
        this.updateState();
    }
    
    void replaceArguments(final Bundle mArgs) {
        this.mArgs = mArgs;
    }
    
    void saveState(final Bundle bundle) {
        this.mSavedStateRegistryController.performSave(bundle);
    }
    
    void setMaxLifecycle(final Lifecycle.State mMaxLifecycle) {
        this.mMaxLifecycle = mMaxLifecycle;
        this.updateState();
    }
    
    void updateState() {
        if (this.mHostLifecycle.ordinal() < this.mMaxLifecycle.ordinal()) {
            this.mLifecycle.setCurrentState(this.mHostLifecycle);
        }
        else {
            this.mLifecycle.setCurrentState(this.mMaxLifecycle);
        }
    }
    
    private static class NavResultSavedStateFactory extends AbstractSavedStateViewModelFactory
    {
        NavResultSavedStateFactory(final SavedStateRegistryOwner savedStateRegistryOwner, final Bundle bundle) {
            super(savedStateRegistryOwner, bundle);
        }
        
        @Override
        protected <T extends ViewModel> T create(final String s, final Class<T> clazz, final SavedStateHandle savedStateHandle) {
            return (T)new SavedStateViewModel(savedStateHandle);
        }
    }
    
    private static class SavedStateViewModel extends ViewModel
    {
        private SavedStateHandle mHandle;
        
        SavedStateViewModel(final SavedStateHandle mHandle) {
            this.mHandle = mHandle;
        }
        
        public SavedStateHandle getHandle() {
            return this.mHandle;
        }
    }
}

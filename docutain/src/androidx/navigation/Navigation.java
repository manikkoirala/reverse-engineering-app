// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.lang.ref.WeakReference;
import android.view.ViewParent;
import androidx.core.app.ActivityCompat;
import android.app.Activity;
import android.view.View;
import android.os.Bundle;
import android.view.View$OnClickListener;

public final class Navigation
{
    private Navigation() {
    }
    
    public static View$OnClickListener createNavigateOnClickListener(final int n) {
        return createNavigateOnClickListener(n, null);
    }
    
    public static View$OnClickListener createNavigateOnClickListener(final int n, final Bundle bundle) {
        return (View$OnClickListener)new View$OnClickListener(n, bundle) {
            final Bundle val$args;
            final int val$resId;
            
            public void onClick(final View view) {
                Navigation.findNavController(view).navigate(this.val$resId, this.val$args);
            }
        };
    }
    
    public static View$OnClickListener createNavigateOnClickListener(final NavDirections navDirections) {
        return (View$OnClickListener)new View$OnClickListener(navDirections) {
            final NavDirections val$directions;
            
            public void onClick(final View view) {
                Navigation.findNavController(view).navigate(this.val$directions);
            }
        };
    }
    
    public static NavController findNavController(final Activity obj, final int i) {
        final NavController viewNavController = findViewNavController(ActivityCompat.requireViewById(obj, i));
        if (viewNavController != null) {
            return viewNavController;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Activity ");
        sb.append(obj);
        sb.append(" does not have a NavController set on ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    public static NavController findNavController(final View obj) {
        final NavController viewNavController = findViewNavController(obj);
        if (viewNavController != null) {
            return viewNavController;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" does not have a NavController set");
        throw new IllegalStateException(sb.toString());
    }
    
    private static NavController findViewNavController(View view) {
        while (view != null) {
            final NavController viewNavController = getViewNavController(view);
            if (viewNavController != null) {
                return viewNavController;
            }
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View)parent;
            }
            else {
                view = null;
            }
        }
        return null;
    }
    
    private static NavController getViewNavController(final View view) {
        final Object tag = view.getTag(R.id.nav_controller_view_tag);
        NavController navController;
        if (tag instanceof WeakReference) {
            navController = (NavController)((WeakReference)tag).get();
        }
        else if (tag instanceof NavController) {
            navController = (NavController)tag;
        }
        else {
            navController = null;
        }
        return navController;
    }
    
    public static void setViewNavController(final View view, final NavController navController) {
        view.setTag(R.id.nav_controller_view_tag, (Object)navController);
    }
}

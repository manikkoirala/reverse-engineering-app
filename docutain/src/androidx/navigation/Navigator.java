// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Bundle;

public abstract class Navigator<D extends NavDestination>
{
    public abstract D createDestination();
    
    public abstract NavDestination navigate(final D p0, final Bundle p1, final NavOptions p2, final Extras p3);
    
    public void onRestoreState(final Bundle bundle) {
    }
    
    public Bundle onSaveState() {
        return null;
    }
    
    public abstract boolean popBackStack();
    
    public interface Extras
    {
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE })
    public @interface Name {
        String value();
    }
}

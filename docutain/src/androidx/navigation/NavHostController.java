// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import androidx.lifecycle.ViewModelStore;
import androidx.activity.OnBackPressedDispatcher;
import androidx.lifecycle.LifecycleOwner;
import android.content.Context;

public class NavHostController extends NavController
{
    public NavHostController(final Context context) {
        super(context);
    }
    
    public final void enableOnBackPressed(final boolean b) {
        super.enableOnBackPressed(b);
    }
    
    public final void setLifecycleOwner(final LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
    }
    
    public final void setOnBackPressedDispatcher(final OnBackPressedDispatcher onBackPressedDispatcher) {
        super.setOnBackPressedDispatcher(onBackPressedDispatcher);
    }
    
    public final void setViewModelStore(final ViewModelStore viewModelStore) {
        super.setViewModelStore(viewModelStore);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

public final class R
{
    public static final class attr
    {
        public static final int action = 2130968578;
        public static final int alpha = 2130968632;
        public static final int argType = 2130968645;
        public static final int data = 2130968955;
        public static final int dataPattern = 2130968956;
        public static final int destination = 2130968971;
        public static final int enterAnim = 2130969034;
        public static final int exitAnim = 2130969047;
        public static final int font = 2130969119;
        public static final int fontProviderAuthority = 2130969121;
        public static final int fontProviderCerts = 2130969122;
        public static final int fontProviderFetchStrategy = 2130969123;
        public static final int fontProviderFetchTimeout = 2130969124;
        public static final int fontProviderPackage = 2130969125;
        public static final int fontProviderQuery = 2130969126;
        public static final int fontStyle = 2130969128;
        public static final int fontVariationSettings = 2130969129;
        public static final int fontWeight = 2130969130;
        public static final int graph = 2130969140;
        public static final int launchSingleTop = 2130969238;
        public static final int mimeType = 2130969413;
        public static final int navGraph = 2130969473;
        public static final int nullable = 2130969484;
        public static final int popEnterAnim = 2130969532;
        public static final int popExitAnim = 2130969533;
        public static final int popUpTo = 2130969534;
        public static final int popUpToInclusive = 2130969535;
        public static final int startDestination = 2130970051;
        public static final int targetPackage = 2130970132;
        public static final int ttcIndex = 2130970276;
        public static final int uri = 2130970279;
    }
    
    public static final class color
    {
        public static final int notification_action_color_filter = 2131100416;
        public static final int notification_icon_bg_color = 2131100417;
        public static final int ripple_material_light = 2131100582;
        public static final int secondary_text_default_material_light = 2131100585;
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131165272;
        public static final int compat_button_inset_vertical_material = 2131165273;
        public static final int compat_button_padding_horizontal_material = 2131165274;
        public static final int compat_button_padding_vertical_material = 2131165275;
        public static final int compat_control_corner_material = 2131165276;
        public static final int compat_notification_large_icon_max_height = 2131165277;
        public static final int compat_notification_large_icon_max_width = 2131165278;
        public static final int notification_action_icon_size = 2131165958;
        public static final int notification_action_text_size = 2131165959;
        public static final int notification_big_circle_margin = 2131165960;
        public static final int notification_content_margin_start = 2131165961;
        public static final int notification_large_icon_height = 2131165962;
        public static final int notification_large_icon_width = 2131165963;
        public static final int notification_main_column_padding_top = 2131165964;
        public static final int notification_media_narrow_margin = 2131165965;
        public static final int notification_right_icon_size = 2131165966;
        public static final int notification_right_side_padding_top = 2131165967;
        public static final int notification_small_icon_background_padding = 2131165968;
        public static final int notification_small_icon_size_as_large = 2131165969;
        public static final int notification_subtext_size = 2131165970;
        public static final int notification_top_pad = 2131165971;
        public static final int notification_top_pad_large_text = 2131165972;
    }
    
    public static final class drawable
    {
        public static final int notification_action_background = 2131231107;
        public static final int notification_bg = 2131231108;
        public static final int notification_bg_low = 2131231109;
        public static final int notification_bg_low_normal = 2131231110;
        public static final int notification_bg_low_pressed = 2131231111;
        public static final int notification_bg_normal = 2131231112;
        public static final int notification_bg_normal_pressed = 2131231113;
        public static final int notification_icon_background = 2131231114;
        public static final int notification_template_icon_bg = 2131231116;
        public static final int notification_template_icon_low_bg = 2131231117;
        public static final int notification_tile_bg = 2131231118;
        public static final int notify_panel_notification_icon_bg = 2131231120;
    }
    
    public static final class id
    {
        public static final int accessibility_action_clickable_span = 2131361830;
        public static final int accessibility_custom_action_0 = 2131361831;
        public static final int accessibility_custom_action_1 = 2131361832;
        public static final int accessibility_custom_action_10 = 2131361833;
        public static final int accessibility_custom_action_11 = 2131361834;
        public static final int accessibility_custom_action_12 = 2131361835;
        public static final int accessibility_custom_action_13 = 2131361836;
        public static final int accessibility_custom_action_14 = 2131361837;
        public static final int accessibility_custom_action_15 = 2131361838;
        public static final int accessibility_custom_action_16 = 2131361839;
        public static final int accessibility_custom_action_17 = 2131361840;
        public static final int accessibility_custom_action_18 = 2131361841;
        public static final int accessibility_custom_action_19 = 2131361842;
        public static final int accessibility_custom_action_2 = 2131361843;
        public static final int accessibility_custom_action_20 = 2131361844;
        public static final int accessibility_custom_action_21 = 2131361845;
        public static final int accessibility_custom_action_22 = 2131361846;
        public static final int accessibility_custom_action_23 = 2131361847;
        public static final int accessibility_custom_action_24 = 2131361848;
        public static final int accessibility_custom_action_25 = 2131361849;
        public static final int accessibility_custom_action_26 = 2131361850;
        public static final int accessibility_custom_action_27 = 2131361851;
        public static final int accessibility_custom_action_28 = 2131361852;
        public static final int accessibility_custom_action_29 = 2131361853;
        public static final int accessibility_custom_action_3 = 2131361854;
        public static final int accessibility_custom_action_30 = 2131361855;
        public static final int accessibility_custom_action_31 = 2131361856;
        public static final int accessibility_custom_action_4 = 2131361857;
        public static final int accessibility_custom_action_5 = 2131361858;
        public static final int accessibility_custom_action_6 = 2131361859;
        public static final int accessibility_custom_action_7 = 2131361860;
        public static final int accessibility_custom_action_8 = 2131361861;
        public static final int accessibility_custom_action_9 = 2131361862;
        public static final int action_container = 2131361875;
        public static final int action_divider = 2131361879;
        public static final int action_image = 2131361885;
        public static final int action_text = 2131361897;
        public static final int actions = 2131361898;
        public static final int async = 2131361928;
        public static final int blocking = 2131361952;
        public static final int chronometer = 2131362002;
        public static final int dialog_button = 2131362052;
        public static final int forever = 2131362123;
        public static final int icon = 2131362151;
        public static final int icon_group = 2131362153;
        public static final int info = 2131362170;
        public static final int italic = 2131362174;
        public static final int line1 = 2131362184;
        public static final int line3 = 2131362185;
        public static final int nav_controller_view_tag = 2131362272;
        public static final int normal = 2131362286;
        public static final int notification_background = 2131362288;
        public static final int notification_main_column = 2131362293;
        public static final int notification_main_column_container = 2131362294;
        public static final int right_icon = 2131362946;
        public static final int right_side = 2131362947;
        public static final int tag_accessibility_actions = 2131363059;
        public static final int tag_accessibility_clickable_spans = 2131363060;
        public static final int tag_accessibility_heading = 2131363061;
        public static final int tag_accessibility_pane_title = 2131363062;
        public static final int tag_screen_reader_focusable = 2131363066;
        public static final int tag_transition_group = 2131363068;
        public static final int tag_unhandled_key_event_manager = 2131363069;
        public static final int tag_unhandled_key_listeners = 2131363070;
        public static final int text = 2131363074;
        public static final int text2 = 2131363075;
        public static final int time = 2131363097;
        public static final int title = 2131363098;
    }
    
    public static final class integer
    {
        public static final int status_bar_notification_info_maxnum = 2131427399;
    }
    
    public static final class layout
    {
        public static final int custom_dialog = 2131558443;
        public static final int notification_action = 2131558556;
        public static final int notification_action_tombstone = 2131558557;
        public static final int notification_template_custom_big = 2131558564;
        public static final int notification_template_icon_group = 2131558565;
        public static final int notification_template_part_chronometer = 2131558569;
        public static final int notification_template_part_time = 2131558570;
    }
    
    public static final class string
    {
        public static final int status_bar_notification_info_overflow = 2131886711;
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2131952228;
        public static final int TextAppearance_Compat_Notification_Info = 2131952229;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131952231;
        public static final int TextAppearance_Compat_Notification_Time = 2131952234;
        public static final int TextAppearance_Compat_Notification_Title = 2131952236;
        public static final int Widget_Compat_NotificationActionContainer = 2131952608;
        public static final int Widget_Compat_NotificationActionText = 2131952609;
    }
    
    public static final class styleable
    {
        public static final int[] ActivityNavigator;
        public static final int ActivityNavigator_action = 1;
        public static final int ActivityNavigator_android_name = 0;
        public static final int ActivityNavigator_data = 2;
        public static final int ActivityNavigator_dataPattern = 3;
        public static final int ActivityNavigator_targetPackage = 4;
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 3;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        public static final int[] NavAction;
        public static final int NavAction_android_id = 0;
        public static final int NavAction_destination = 1;
        public static final int NavAction_enterAnim = 2;
        public static final int NavAction_exitAnim = 3;
        public static final int NavAction_launchSingleTop = 4;
        public static final int NavAction_popEnterAnim = 5;
        public static final int NavAction_popExitAnim = 6;
        public static final int NavAction_popUpTo = 7;
        public static final int NavAction_popUpToInclusive = 8;
        public static final int[] NavArgument;
        public static final int NavArgument_android_defaultValue = 1;
        public static final int NavArgument_android_name = 0;
        public static final int NavArgument_argType = 2;
        public static final int NavArgument_nullable = 3;
        public static final int[] NavDeepLink;
        public static final int NavDeepLink_action = 1;
        public static final int NavDeepLink_android_autoVerify = 0;
        public static final int NavDeepLink_mimeType = 2;
        public static final int NavDeepLink_uri = 3;
        public static final int[] NavGraphNavigator;
        public static final int NavGraphNavigator_startDestination = 0;
        public static final int[] NavHost;
        public static final int NavHost_navGraph = 0;
        public static final int[] NavInclude;
        public static final int NavInclude_graph = 0;
        public static final int[] Navigator;
        public static final int Navigator_android_id = 1;
        public static final int Navigator_android_label = 0;
        
        static {
            ActivityNavigator = new int[] { 16842755, 2130968578, 2130968955, 2130968956, 2130970132 };
            ColorStateListItem = new int[] { 16843173, 16843551, 16844359, 2130968632, 2130969232 };
            FontFamily = new int[] { 2130969121, 2130969122, 2130969123, 2130969124, 2130969125, 2130969126, 2130969127 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130969119, 2130969128, 2130969129, 2130969130, 2130970276 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
            NavAction = new int[] { 16842960, 2130968971, 2130969034, 2130969047, 2130969238, 2130969532, 2130969533, 2130969534, 2130969535 };
            NavArgument = new int[] { 16842755, 16843245, 2130968645, 2130969484 };
            NavDeepLink = new int[] { 16844014, 2130968578, 2130969413, 2130970279 };
            NavGraphNavigator = new int[] { 2130970051 };
            NavHost = new int[] { 2130969473 };
            NavInclude = new int[] { 2130969140 };
            Navigator = new int[] { 16842753, 16842960 };
        }
    }
}

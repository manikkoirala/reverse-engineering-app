// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.res.TypedArray;
import androidx.navigation.common.R;
import android.util.AttributeSet;
import android.net.Uri;
import java.util.Collections;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Map;
import android.os.Bundle;
import android.content.res.Resources$NotFoundException;
import android.content.Context;
import java.util.ArrayList;
import androidx.collection.SparseArrayCompat;
import java.util.HashMap;

public class NavDestination
{
    private static final HashMap<String, Class<?>> sClasses;
    private SparseArrayCompat<NavAction> mActions;
    private HashMap<String, NavArgument> mArguments;
    private ArrayList<NavDeepLink> mDeepLinks;
    private int mId;
    private String mIdName;
    private CharSequence mLabel;
    private final String mNavigatorName;
    private NavGraph mParent;
    
    static {
        sClasses = new HashMap<String, Class<?>>();
    }
    
    public NavDestination(final Navigator<? extends NavDestination> navigator) {
        this(NavigatorProvider.getNameForNavigator(navigator.getClass()));
    }
    
    public NavDestination(final String mNavigatorName) {
        this.mNavigatorName = mNavigatorName;
    }
    
    static String getDisplayName(final Context context, final int n) {
        if (n <= 16777215) {
            return Integer.toString(n);
        }
        try {
            return context.getResources().getResourceName(n);
        }
        catch (final Resources$NotFoundException ex) {
            return Integer.toString(n);
        }
    }
    
    protected static <C> Class<? extends C> parseClassFromName(final Context context, final String str, final Class<? extends C> obj) {
        String string = str;
        if (str.charAt(0) == '.') {
            final StringBuilder sb = new StringBuilder();
            sb.append(context.getPackageName());
            sb.append(str);
            string = sb.toString();
        }
        final HashMap<String, Class<?>> sClasses = NavDestination.sClasses;
        Class<?> forName;
        if ((forName = sClasses.get(string)) == null) {
            try {
                forName = Class.forName(string, true, context.getClassLoader());
                sClasses.put(string, forName);
            }
            catch (final ClassNotFoundException cause) {
                throw new IllegalArgumentException(cause);
            }
        }
        if (obj.isAssignableFrom(forName)) {
            return (Class<? extends C>)forName;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(" must be a subclass of ");
        sb2.append(obj);
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public final void addArgument(final String key, final NavArgument value) {
        if (this.mArguments == null) {
            this.mArguments = new HashMap<String, NavArgument>();
        }
        this.mArguments.put(key, value);
    }
    
    public final void addDeepLink(final NavDeepLink e) {
        if (this.mDeepLinks == null) {
            this.mDeepLinks = new ArrayList<NavDeepLink>();
        }
        this.mDeepLinks.add(e);
    }
    
    public final void addDeepLink(final String uriPattern) {
        this.addDeepLink(new NavDeepLink.Builder().setUriPattern(uriPattern).build());
    }
    
    Bundle addInDefaultArgs(final Bundle bundle) {
        if (bundle == null) {
            final HashMap<String, NavArgument> mArguments = this.mArguments;
            if (mArguments == null || mArguments.isEmpty()) {
                return null;
            }
        }
        final Bundle bundle2 = new Bundle();
        final HashMap<String, NavArgument> mArguments2 = this.mArguments;
        if (mArguments2 != null) {
            for (final Map.Entry entry : mArguments2.entrySet()) {
                ((NavArgument)entry.getValue()).putDefaultValue((String)entry.getKey(), bundle2);
            }
        }
        if (bundle != null) {
            bundle2.putAll(bundle);
            final HashMap<String, NavArgument> mArguments3 = this.mArguments;
            if (mArguments3 != null) {
                for (final Map.Entry entry2 : mArguments3.entrySet()) {
                    if (((NavArgument)entry2.getValue()).verify((String)entry2.getKey(), bundle2)) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Wrong argument type for '");
                    sb.append((String)entry2.getKey());
                    sb.append("' in argument bundle. ");
                    sb.append(((NavArgument)entry2.getValue()).getType().getName());
                    sb.append(" expected.");
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        return bundle2;
    }
    
    int[] buildDeepLinkIds() {
        final ArrayDeque arrayDeque = new ArrayDeque();
        NavDestination e = this;
        while (true) {
            final NavGraph parent = e.getParent();
            if (parent == null || parent.getStartDestination() != e.getId()) {
                arrayDeque.addFirst(e);
            }
            if (parent == null) {
                break;
            }
            e = parent;
        }
        final int[] array = new int[arrayDeque.size()];
        int n = 0;
        final Iterator iterator = arrayDeque.iterator();
        while (iterator.hasNext()) {
            array[n] = ((NavDestination)iterator.next()).getId();
            ++n;
        }
        return array;
    }
    
    public final NavAction getAction(final int n) {
        final SparseArrayCompat<NavAction> mActions = this.mActions;
        final NavAction navAction = null;
        NavAction action;
        if (mActions == null) {
            action = null;
        }
        else {
            action = mActions.get(n);
        }
        if (action == null) {
            action = navAction;
            if (this.getParent() != null) {
                action = this.getParent().getAction(n);
            }
        }
        return action;
    }
    
    public final Map<String, NavArgument> getArguments() {
        final HashMap<String, NavArgument> mArguments = this.mArguments;
        Map<Object, Object> map;
        if (mArguments == null) {
            map = Collections.emptyMap();
        }
        else {
            map = Collections.unmodifiableMap((Map<?, ?>)mArguments);
        }
        return (Map<String, NavArgument>)map;
    }
    
    public String getDisplayName() {
        if (this.mIdName == null) {
            this.mIdName = Integer.toString(this.mId);
        }
        return this.mIdName;
    }
    
    public final int getId() {
        return this.mId;
    }
    
    public final CharSequence getLabel() {
        return this.mLabel;
    }
    
    public final String getNavigatorName() {
        return this.mNavigatorName;
    }
    
    public final NavGraph getParent() {
        return this.mParent;
    }
    
    public boolean hasDeepLink(final Uri uri) {
        return this.hasDeepLink(new NavDeepLinkRequest(uri, null, null));
    }
    
    public boolean hasDeepLink(final NavDeepLinkRequest navDeepLinkRequest) {
        return this.matchDeepLink(navDeepLinkRequest) != null;
    }
    
    DeepLinkMatch matchDeepLink(final NavDeepLinkRequest navDeepLinkRequest) {
        final ArrayList<NavDeepLink> mDeepLinks = this.mDeepLinks;
        if (mDeepLinks == null) {
            return null;
        }
        final Iterator<NavDeepLink> iterator = mDeepLinks.iterator();
        DeepLinkMatch deepLinkMatch = null;
        while (iterator.hasNext()) {
            final NavDeepLink navDeepLink = iterator.next();
            final Uri uri = navDeepLinkRequest.getUri();
            Bundle matchingArguments;
            if (uri != null) {
                matchingArguments = navDeepLink.getMatchingArguments(uri, this.getArguments());
            }
            else {
                matchingArguments = null;
            }
            final String action = navDeepLinkRequest.getAction();
            final boolean b = action != null && action.equals(navDeepLink.getAction());
            final String mimeType = navDeepLinkRequest.getMimeType();
            int mimeTypeMatchRating;
            if (mimeType != null) {
                mimeTypeMatchRating = navDeepLink.getMimeTypeMatchRating(mimeType);
            }
            else {
                mimeTypeMatchRating = -1;
            }
            if (matchingArguments != null || b || mimeTypeMatchRating > -1) {
                final DeepLinkMatch deepLinkMatch2 = new DeepLinkMatch(this, matchingArguments, navDeepLink.isExactDeepLink(), b, mimeTypeMatchRating);
                if (deepLinkMatch != null && deepLinkMatch2.compareTo(deepLinkMatch) <= 0) {
                    continue;
                }
                deepLinkMatch = deepLinkMatch2;
            }
        }
        return deepLinkMatch;
    }
    
    public void onInflate(final Context context, final AttributeSet set) {
        final TypedArray obtainAttributes = context.getResources().obtainAttributes(set, R.styleable.Navigator);
        this.setId(obtainAttributes.getResourceId(R.styleable.Navigator_android_id, 0));
        this.mIdName = getDisplayName(context, this.mId);
        this.setLabel(obtainAttributes.getText(R.styleable.Navigator_android_label));
        obtainAttributes.recycle();
    }
    
    public final void putAction(final int n, final int n2) {
        this.putAction(n, new NavAction(n2));
    }
    
    public final void putAction(final int i, final NavAction navAction) {
        if (!this.supportsActions()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot add action ");
            sb.append(i);
            sb.append(" to ");
            sb.append(this);
            sb.append(" as it does not support actions, indicating that it is a terminal destination in your navigation graph and will never trigger actions.");
            throw new UnsupportedOperationException(sb.toString());
        }
        if (i != 0) {
            if (this.mActions == null) {
                this.mActions = new SparseArrayCompat<NavAction>();
            }
            this.mActions.put(i, navAction);
            return;
        }
        throw new IllegalArgumentException("Cannot have an action with actionId 0");
    }
    
    public final void removeAction(final int n) {
        final SparseArrayCompat<NavAction> mActions = this.mActions;
        if (mActions == null) {
            return;
        }
        mActions.remove(n);
    }
    
    public final void removeArgument(final String key) {
        final HashMap<String, NavArgument> mArguments = this.mArguments;
        if (mArguments == null) {
            return;
        }
        mArguments.remove(key);
    }
    
    public final void setId(final int mId) {
        this.mId = mId;
        this.mIdName = null;
    }
    
    public final void setLabel(final CharSequence mLabel) {
        this.mLabel = mLabel;
    }
    
    final void setParent(final NavGraph mParent) {
        this.mParent = mParent;
    }
    
    boolean supportsActions() {
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("(");
        final String mIdName = this.mIdName;
        if (mIdName == null) {
            sb.append("0x");
            sb.append(Integer.toHexString(this.mId));
        }
        else {
            sb.append(mIdName);
        }
        sb.append(")");
        if (this.mLabel != null) {
            sb.append(" label=");
            sb.append(this.mLabel);
        }
        return sb.toString();
    }
    
    @Retention(RetentionPolicy.CLASS)
    @Target({ ElementType.TYPE })
    public @interface ClassType {
        Class<?> value();
    }
    
    static class DeepLinkMatch implements Comparable<DeepLinkMatch>
    {
        private final NavDestination mDestination;
        private final boolean mHasMatchingAction;
        private final boolean mIsExactDeepLink;
        private final Bundle mMatchingArgs;
        private final int mMimeTypeMatchLevel;
        
        DeepLinkMatch(final NavDestination mDestination, final Bundle mMatchingArgs, final boolean mIsExactDeepLink, final boolean mHasMatchingAction, final int mMimeTypeMatchLevel) {
            this.mDestination = mDestination;
            this.mMatchingArgs = mMatchingArgs;
            this.mIsExactDeepLink = mIsExactDeepLink;
            this.mHasMatchingAction = mHasMatchingAction;
            this.mMimeTypeMatchLevel = mMimeTypeMatchLevel;
        }
        
        @Override
        public int compareTo(final DeepLinkMatch deepLinkMatch) {
            final boolean mIsExactDeepLink = this.mIsExactDeepLink;
            if (mIsExactDeepLink && !deepLinkMatch.mIsExactDeepLink) {
                return 1;
            }
            if (!mIsExactDeepLink && deepLinkMatch.mIsExactDeepLink) {
                return -1;
            }
            final Bundle mMatchingArgs = this.mMatchingArgs;
            if (mMatchingArgs != null && deepLinkMatch.mMatchingArgs == null) {
                return 1;
            }
            if (mMatchingArgs == null && deepLinkMatch.mMatchingArgs != null) {
                return -1;
            }
            if (mMatchingArgs != null) {
                final int n = mMatchingArgs.size() - deepLinkMatch.mMatchingArgs.size();
                if (n > 0) {
                    return 1;
                }
                if (n < 0) {
                    return -1;
                }
            }
            final boolean mHasMatchingAction = this.mHasMatchingAction;
            if (mHasMatchingAction && !deepLinkMatch.mHasMatchingAction) {
                return 1;
            }
            if (!mHasMatchingAction && deepLinkMatch.mHasMatchingAction) {
                return -1;
            }
            return this.mMimeTypeMatchLevel - deepLinkMatch.mMimeTypeMatchLevel;
        }
        
        NavDestination getDestination() {
            return this.mDestination;
        }
        
        Bundle getMatchingArgs() {
            return this.mMatchingArgs;
        }
    }
}

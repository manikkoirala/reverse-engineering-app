// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

public final class NavOptions
{
    private int mEnterAnim;
    private int mExitAnim;
    private int mPopEnterAnim;
    private int mPopExitAnim;
    private int mPopUpTo;
    private boolean mPopUpToInclusive;
    private boolean mSingleTop;
    
    NavOptions(final boolean mSingleTop, final int mPopUpTo, final boolean mPopUpToInclusive, final int mEnterAnim, final int mExitAnim, final int mPopEnterAnim, final int mPopExitAnim) {
        this.mSingleTop = mSingleTop;
        this.mPopUpTo = mPopUpTo;
        this.mPopUpToInclusive = mPopUpToInclusive;
        this.mEnterAnim = mEnterAnim;
        this.mExitAnim = mExitAnim;
        this.mPopEnterAnim = mPopEnterAnim;
        this.mPopExitAnim = mPopExitAnim;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final NavOptions navOptions = (NavOptions)o;
            if (this.mSingleTop != navOptions.mSingleTop || this.mPopUpTo != navOptions.mPopUpTo || this.mPopUpToInclusive != navOptions.mPopUpToInclusive || this.mEnterAnim != navOptions.mEnterAnim || this.mExitAnim != navOptions.mExitAnim || this.mPopEnterAnim != navOptions.mPopEnterAnim || this.mPopExitAnim != navOptions.mPopExitAnim) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public int getEnterAnim() {
        return this.mEnterAnim;
    }
    
    public int getExitAnim() {
        return this.mExitAnim;
    }
    
    public int getPopEnterAnim() {
        return this.mPopEnterAnim;
    }
    
    public int getPopExitAnim() {
        return this.mPopExitAnim;
    }
    
    public int getPopUpTo() {
        return this.mPopUpTo;
    }
    
    @Override
    public int hashCode() {
        return ((((((this.shouldLaunchSingleTop() ? 1 : 0) * 31 + this.getPopUpTo()) * 31 + (this.isPopUpToInclusive() ? 1 : 0)) * 31 + this.getEnterAnim()) * 31 + this.getExitAnim()) * 31 + this.getPopEnterAnim()) * 31 + this.getPopExitAnim();
    }
    
    public boolean isPopUpToInclusive() {
        return this.mPopUpToInclusive;
    }
    
    public boolean shouldLaunchSingleTop() {
        return this.mSingleTop;
    }
    
    public static final class Builder
    {
        int mEnterAnim;
        int mExitAnim;
        int mPopEnterAnim;
        int mPopExitAnim;
        int mPopUpTo;
        boolean mPopUpToInclusive;
        boolean mSingleTop;
        
        public Builder() {
            this.mPopUpTo = -1;
            this.mEnterAnim = -1;
            this.mExitAnim = -1;
            this.mPopEnterAnim = -1;
            this.mPopExitAnim = -1;
        }
        
        public NavOptions build() {
            return new NavOptions(this.mSingleTop, this.mPopUpTo, this.mPopUpToInclusive, this.mEnterAnim, this.mExitAnim, this.mPopEnterAnim, this.mPopExitAnim);
        }
        
        public Builder setEnterAnim(final int mEnterAnim) {
            this.mEnterAnim = mEnterAnim;
            return this;
        }
        
        public Builder setExitAnim(final int mExitAnim) {
            this.mExitAnim = mExitAnim;
            return this;
        }
        
        public Builder setLaunchSingleTop(final boolean mSingleTop) {
            this.mSingleTop = mSingleTop;
            return this;
        }
        
        public Builder setPopEnterAnim(final int mPopEnterAnim) {
            this.mPopEnterAnim = mPopEnterAnim;
            return this;
        }
        
        public Builder setPopExitAnim(final int mPopExitAnim) {
            this.mPopExitAnim = mPopExitAnim;
            return this;
        }
        
        public Builder setPopUpTo(final int mPopUpTo, final boolean mPopUpToInclusive) {
            this.mPopUpTo = mPopUpTo;
            this.mPopUpToInclusive = mPopUpToInclusive;
            return this;
        }
    }
}

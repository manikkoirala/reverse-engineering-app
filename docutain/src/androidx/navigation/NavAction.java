// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

public final class NavAction
{
    private Bundle mDefaultArguments;
    private final int mDestinationId;
    private NavOptions mNavOptions;
    
    public NavAction(final int n) {
        this(n, null);
    }
    
    public NavAction(final int n, final NavOptions navOptions) {
        this(n, navOptions, null);
    }
    
    public NavAction(final int mDestinationId, final NavOptions mNavOptions, final Bundle mDefaultArguments) {
        this.mDestinationId = mDestinationId;
        this.mNavOptions = mNavOptions;
        this.mDefaultArguments = mDefaultArguments;
    }
    
    public Bundle getDefaultArguments() {
        return this.mDefaultArguments;
    }
    
    public int getDestinationId() {
        return this.mDestinationId;
    }
    
    public NavOptions getNavOptions() {
        return this.mNavOptions;
    }
    
    public void setDefaultArguments(final Bundle mDefaultArguments) {
        this.mDefaultArguments = mDefaultArguments;
    }
    
    public void setNavOptions(final NavOptions mNavOptions) {
        this.mNavOptions = mNavOptions;
    }
}

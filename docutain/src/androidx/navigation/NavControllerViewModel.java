// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import java.util.Iterator;
import androidx.lifecycle.ViewModelProvider$Factory$_CC;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.lifecycle.ViewModelStore;
import java.util.UUID;
import java.util.HashMap;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModel;

class NavControllerViewModel extends ViewModel
{
    private static final ViewModelProvider.Factory FACTORY;
    private final HashMap<UUID, ViewModelStore> mViewModelStores;
    
    static {
        FACTORY = new ViewModelProvider.Factory() {
            @Override
            public <T extends ViewModel> T create(final Class<T> clazz) {
                return (T)new NavControllerViewModel();
            }
        };
    }
    
    NavControllerViewModel() {
        this.mViewModelStores = new HashMap<UUID, ViewModelStore>();
    }
    
    static NavControllerViewModel getInstance(final ViewModelStore viewModelStore) {
        return new ViewModelProvider(viewModelStore, NavControllerViewModel.FACTORY).get(NavControllerViewModel.class);
    }
    
    void clear(final UUID key) {
        final ViewModelStore viewModelStore = this.mViewModelStores.remove(key);
        if (viewModelStore != null) {
            viewModelStore.clear();
        }
    }
    
    ViewModelStore getViewModelStore(final UUID uuid) {
        ViewModelStore value;
        if ((value = this.mViewModelStores.get(uuid)) == null) {
            value = new ViewModelStore();
            this.mViewModelStores.put(uuid, value);
        }
        return value;
    }
    
    @Override
    protected void onCleared() {
        final Iterator<ViewModelStore> iterator = this.mViewModelStores.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.mViewModelStores.clear();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NavControllerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} ViewModelStores (");
        final Iterator<UUID> iterator = this.mViewModelStores.keySet().iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;
import java.util.Iterator;
import java.util.regex.Matcher;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.regex.Pattern;

public final class NavDeepLink
{
    private static final Pattern SCHEME_PATTERN;
    private final String mAction;
    private final ArrayList<String> mArguments;
    private boolean mExactDeepLink;
    private boolean mIsParameterizedQuery;
    private final String mMimeType;
    private Pattern mMimeTypePattern;
    private final Map<String, ParamQuery> mParamArgMap;
    private Pattern mPattern;
    private final String mUri;
    
    static {
        SCHEME_PATTERN = Pattern.compile("^[a-zA-Z]+[+\\w\\-.]*:");
    }
    
    NavDeepLink(final String s) {
        this(s, null, null);
    }
    
    NavDeepLink(String queryParameter, final String mAction, final String str) {
        this.mArguments = new ArrayList<String>();
        this.mParamArgMap = new HashMap<String, ParamQuery>();
        this.mPattern = null;
        this.mExactDeepLink = false;
        this.mIsParameterizedQuery = false;
        this.mMimeTypePattern = null;
        this.mUri = queryParameter;
        this.mAction = mAction;
        this.mMimeType = str;
        if (queryParameter != null) {
            final Uri parse = Uri.parse(queryParameter);
            this.mIsParameterizedQuery = (parse.getQuery() != null);
            final StringBuilder sb = new StringBuilder("^");
            if (!NavDeepLink.SCHEME_PATTERN.matcher(queryParameter).find()) {
                sb.append("http[s]?://");
            }
            final Pattern compile = Pattern.compile("\\{(.+?)\\}");
            if (this.mIsParameterizedQuery) {
                final Matcher matcher = Pattern.compile("(\\?)").matcher(queryParameter);
                if (matcher.find()) {
                    this.buildPathRegex(queryParameter.substring(0, matcher.start()), sb, compile);
                }
                this.mExactDeepLink = false;
                for (final String s : parse.getQueryParameterNames()) {
                    final StringBuilder sb2 = new StringBuilder();
                    queryParameter = parse.getQueryParameter(s);
                    final Matcher matcher2 = compile.matcher(queryParameter);
                    final ParamQuery paramQuery = new ParamQuery();
                    int end = 0;
                    while (matcher2.find()) {
                        paramQuery.addArgumentName(matcher2.group(1));
                        sb2.append(Pattern.quote(queryParameter.substring(end, matcher2.start())));
                        sb2.append("(.+?)?");
                        end = matcher2.end();
                    }
                    if (end < queryParameter.length()) {
                        sb2.append(Pattern.quote(queryParameter.substring(end)));
                    }
                    paramQuery.setParamRegex(sb2.toString().replace(".*", "\\E.*\\Q"));
                    this.mParamArgMap.put(s, paramQuery);
                }
            }
            else {
                this.mExactDeepLink = this.buildPathRegex(queryParameter, sb, compile);
            }
            this.mPattern = Pattern.compile(sb.toString().replace(".*", "\\E.*\\Q"), 2);
        }
        if (str != null) {
            if (!Pattern.compile("^[\\s\\S]+/[\\s\\S]+$").matcher(str).matches()) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("The given mimeType ");
                sb3.append(str);
                sb3.append(" does not match to required \"type/subtype\" format");
                throw new IllegalArgumentException(sb3.toString());
            }
            final MimeType mimeType = new MimeType(str);
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("^(");
            sb4.append(mimeType.mType);
            sb4.append("|[*]+)/(");
            sb4.append(mimeType.mSubType);
            sb4.append("|[*]+)$");
            this.mMimeTypePattern = Pattern.compile(sb4.toString().replace("*|[*]", "[\\s\\S]"));
        }
    }
    
    private boolean buildPathRegex(final String input, final StringBuilder sb, final Pattern pattern) {
        final Matcher matcher = pattern.matcher(input);
        boolean b = input.contains(".*") ^ true;
        int end = 0;
        while (matcher.find()) {
            this.mArguments.add(matcher.group(1));
            sb.append(Pattern.quote(input.substring(end, matcher.start())));
            sb.append("(.+?)");
            end = matcher.end();
            b = false;
        }
        if (end < input.length()) {
            sb.append(Pattern.quote(input.substring(end)));
        }
        sb.append("($|(\\?(.)*))");
        return b;
    }
    
    private boolean matchAction(final String anObject) {
        final boolean b = true;
        final boolean b2 = anObject == null;
        final String mAction = this.mAction;
        if (b2 == (mAction != null)) {
            return false;
        }
        boolean b3 = b;
        if (anObject != null) {
            b3 = (mAction.equals(anObject) && b);
        }
        return b3;
    }
    
    private boolean matchMimeType(final String input) {
        final boolean b = true;
        if (input == null == (this.mMimeType != null)) {
            return false;
        }
        boolean b2 = b;
        if (input != null) {
            b2 = (this.mMimeTypePattern.matcher(input).matches() && b);
        }
        return b2;
    }
    
    private boolean matchUri(final Uri uri) {
        final boolean b = true;
        final boolean b2 = uri == null;
        final Pattern mPattern = this.mPattern;
        if (b2 == (mPattern != null)) {
            return false;
        }
        boolean b3 = b;
        if (uri != null) {
            b3 = (mPattern.matcher(uri.toString()).matches() && b);
        }
        return b3;
    }
    
    private boolean parseArgument(final Bundle bundle, final String s, final String s2, final NavArgument navArgument) {
        if (navArgument != null) {
            final NavType<?> type = navArgument.getType();
            try {
                type.parseAndPut(bundle, s, s2);
                return false;
            }
            catch (final IllegalArgumentException ex) {
                return true;
            }
        }
        bundle.putString(s, s2);
        return false;
    }
    
    public String getAction() {
        return this.mAction;
    }
    
    Bundle getMatchingArguments(final Uri uri, final Map<String, NavArgument> map) {
        final Matcher matcher = this.mPattern.matcher(uri.toString());
        if (!matcher.matches()) {
            return null;
        }
        final Bundle bundle = new Bundle();
        final int size = this.mArguments.size();
        int i = 0;
        while (i < size) {
            final String s = this.mArguments.get(i);
            if (this.parseArgument(bundle, s, Uri.decode(matcher.group(++i)), map.get(s))) {
                return null;
            }
        }
        if (this.mIsParameterizedQuery) {
            for (final String s2 : this.mParamArgMap.keySet()) {
                final ParamQuery paramQuery = this.mParamArgMap.get(s2);
                final String queryParameter = uri.getQueryParameter(s2);
                Matcher matcher2;
                if (queryParameter != null) {
                    if (!(matcher2 = Pattern.compile(paramQuery.getParamRegex()).matcher(queryParameter)).matches()) {
                        return null;
                    }
                }
                else {
                    matcher2 = null;
                }
                for (int j = 0; j < paramQuery.size(); ++j) {
                    String decode;
                    if (matcher2 != null) {
                        decode = Uri.decode(matcher2.group(j + 1));
                    }
                    else {
                        decode = null;
                    }
                    final String argumentName = paramQuery.getArgumentName(j);
                    final NavArgument navArgument = map.get(argumentName);
                    if (decode != null && !decode.replaceAll("[{}]", "").equals(argumentName) && this.parseArgument(bundle, argumentName, decode, navArgument)) {
                        return null;
                    }
                }
            }
        }
        return bundle;
    }
    
    public String getMimeType() {
        return this.mMimeType;
    }
    
    int getMimeTypeMatchRating(final String input) {
        if (this.mMimeType != null && this.mMimeTypePattern.matcher(input).matches()) {
            return new MimeType(this.mMimeType).compareTo(new MimeType(input));
        }
        return -1;
    }
    
    public String getUriPattern() {
        return this.mUri;
    }
    
    boolean isExactDeepLink() {
        return this.mExactDeepLink;
    }
    
    boolean matches(final Uri uri) {
        return this.matches(new NavDeepLinkRequest(uri, null, null));
    }
    
    boolean matches(final NavDeepLinkRequest navDeepLinkRequest) {
        return this.matchUri(navDeepLinkRequest.getUri()) && this.matchAction(navDeepLinkRequest.getAction()) && this.matchMimeType(navDeepLinkRequest.getMimeType());
    }
    
    public static final class Builder
    {
        private String mAction;
        private String mMimeType;
        private String mUriPattern;
        
        Builder() {
        }
        
        public static Builder fromAction(final String action) {
            if (!action.isEmpty()) {
                final Builder builder = new Builder();
                builder.setAction(action);
                return builder;
            }
            throw new IllegalArgumentException("The NavDeepLink cannot have an empty action.");
        }
        
        public static Builder fromMimeType(final String mimeType) {
            final Builder builder = new Builder();
            builder.setMimeType(mimeType);
            return builder;
        }
        
        public static Builder fromUriPattern(final String uriPattern) {
            final Builder builder = new Builder();
            builder.setUriPattern(uriPattern);
            return builder;
        }
        
        public NavDeepLink build() {
            return new NavDeepLink(this.mUriPattern, this.mAction, this.mMimeType);
        }
        
        public Builder setAction(final String mAction) {
            if (!mAction.isEmpty()) {
                this.mAction = mAction;
                return this;
            }
            throw new IllegalArgumentException("The NavDeepLink cannot have an empty action.");
        }
        
        public Builder setMimeType(final String mMimeType) {
            this.mMimeType = mMimeType;
            return this;
        }
        
        public Builder setUriPattern(final String mUriPattern) {
            this.mUriPattern = mUriPattern;
            return this;
        }
    }
    
    private static class MimeType implements Comparable<MimeType>
    {
        String mSubType;
        String mType;
        
        MimeType(final String s) {
            final String[] split = s.split("/", -1);
            this.mType = split[0];
            this.mSubType = split[1];
        }
        
        @Override
        public int compareTo(final MimeType mimeType) {
            int n;
            if (this.mType.equals(mimeType.mType)) {
                n = 2;
            }
            else {
                n = 0;
            }
            int n2 = n;
            if (this.mSubType.equals(mimeType.mSubType)) {
                n2 = n + 1;
            }
            return n2;
        }
    }
    
    private static class ParamQuery
    {
        private ArrayList<String> mArguments;
        private String mParamRegex;
        
        ParamQuery() {
            this.mArguments = new ArrayList<String>();
        }
        
        void addArgumentName(final String e) {
            this.mArguments.add(e);
        }
        
        String getArgumentName(final int index) {
            return this.mArguments.get(index);
        }
        
        String getParamRegex() {
            return this.mParamRegex;
        }
        
        void setParamRegex(final String mParamRegex) {
            this.mParamRegex = mParamRegex;
        }
        
        public int size() {
            return this.mArguments.size();
        }
    }
}

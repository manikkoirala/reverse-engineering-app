// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import androidx.lifecycle.ViewModelStore;
import androidx.activity.OnBackPressedDispatcher;
import java.util.Map;
import android.net.Uri;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;
import android.content.Intent;
import androidx.lifecycle.ViewModelStoreOwner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import android.content.ContextWrapper;
import java.util.Iterator;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import java.util.ArrayDeque;
import java.util.concurrent.CopyOnWriteArrayList;
import androidx.activity.OnBackPressedCallback;
import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleObserver;
import android.content.Context;
import android.os.Parcelable;
import java.util.Deque;
import android.app.Activity;

public class NavController
{
    private static final String KEY_BACK_STACK = "android-support-nav:controller:backStack";
    static final String KEY_DEEP_LINK_EXTRAS = "android-support-nav:controller:deepLinkExtras";
    static final String KEY_DEEP_LINK_HANDLED = "android-support-nav:controller:deepLinkHandled";
    static final String KEY_DEEP_LINK_IDS = "android-support-nav:controller:deepLinkIds";
    public static final String KEY_DEEP_LINK_INTENT = "android-support-nav:controller:deepLinkIntent";
    private static final String KEY_NAVIGATOR_STATE = "android-support-nav:controller:navigatorState";
    private static final String KEY_NAVIGATOR_STATE_NAMES = "android-support-nav:controller:navigatorState:names";
    private static final String TAG = "NavController";
    private Activity mActivity;
    final Deque<NavBackStackEntry> mBackStack;
    private Parcelable[] mBackStackToRestore;
    private final Context mContext;
    private boolean mDeepLinkHandled;
    private boolean mEnableOnBackPressedCallback;
    NavGraph mGraph;
    private NavInflater mInflater;
    private final LifecycleObserver mLifecycleObserver;
    private LifecycleOwner mLifecycleOwner;
    private NavigatorProvider mNavigatorProvider;
    private Bundle mNavigatorStateToRestore;
    private final OnBackPressedCallback mOnBackPressedCallback;
    private final CopyOnWriteArrayList<OnDestinationChangedListener> mOnDestinationChangedListeners;
    private NavControllerViewModel mViewModel;
    
    public NavController(Context baseContext) {
        this.mBackStack = new ArrayDeque<NavBackStackEntry>();
        this.mNavigatorProvider = new NavigatorProvider();
        this.mOnDestinationChangedListeners = new CopyOnWriteArrayList<OnDestinationChangedListener>();
        this.mLifecycleObserver = new LifecycleEventObserver() {
            final NavController this$0;
            
            @Override
            public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                if (this.this$0.mGraph != null) {
                    final Iterator<NavBackStackEntry> iterator = this.this$0.mBackStack.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().handleLifecycleEvent(event);
                    }
                }
            }
        };
        this.mOnBackPressedCallback = new OnBackPressedCallback(false) {
            final NavController this$0;
            
            @Override
            public void handleOnBackPressed() {
                this.this$0.popBackStack();
            }
        };
        this.mEnableOnBackPressedCallback = true;
        this.mContext = baseContext;
        while (baseContext instanceof ContextWrapper) {
            if (baseContext instanceof Activity) {
                this.mActivity = (Activity)baseContext;
                break;
            }
            baseContext = ((ContextWrapper)baseContext).getBaseContext();
        }
        final NavigatorProvider mNavigatorProvider = this.mNavigatorProvider;
        mNavigatorProvider.addNavigator(new NavGraphNavigator(mNavigatorProvider));
        this.mNavigatorProvider.addNavigator(new ActivityNavigator(this.mContext));
    }
    
    private boolean dispatchOnDestinationChanged() {
        while (!this.mBackStack.isEmpty() && this.mBackStack.peekLast().getDestination() instanceof NavGraph && this.popBackStackInternal(this.mBackStack.peekLast().getDestination().getId(), true)) {}
        if (!this.mBackStack.isEmpty()) {
            NavDestination navDestination = this.mBackStack.peekLast().getDestination();
            NavDestination navDestination3;
            final NavDestination navDestination2 = navDestination3 = null;
            if (navDestination instanceof FloatingWindow) {
                final Iterator<NavBackStackEntry> descendingIterator = this.mBackStack.descendingIterator();
                do {
                    navDestination3 = navDestination2;
                    if (!descendingIterator.hasNext()) {
                        break;
                    }
                    navDestination3 = descendingIterator.next().getDestination();
                } while (navDestination3 instanceof NavGraph || navDestination3 instanceof FloatingWindow);
            }
            final HashMap<NavBackStackEntry, Lifecycle.State> hashMap = new HashMap<NavBackStackEntry, Lifecycle.State>();
            final Iterator<NavBackStackEntry> descendingIterator2 = this.mBackStack.descendingIterator();
            while (descendingIterator2.hasNext()) {
                final NavBackStackEntry navBackStackEntry = descendingIterator2.next();
                final Lifecycle.State maxLifecycle = navBackStackEntry.getMaxLifecycle();
                final NavDestination destination = navBackStackEntry.getDestination();
                if (navDestination != null && destination.getId() == navDestination.getId()) {
                    if (maxLifecycle != Lifecycle.State.RESUMED) {
                        hashMap.put(navBackStackEntry, Lifecycle.State.RESUMED);
                    }
                    navDestination = navDestination.getParent();
                }
                else if (navDestination3 != null && destination.getId() == navDestination3.getId()) {
                    if (maxLifecycle == Lifecycle.State.RESUMED) {
                        navBackStackEntry.setMaxLifecycle(Lifecycle.State.STARTED);
                    }
                    else if (maxLifecycle != Lifecycle.State.STARTED) {
                        hashMap.put(navBackStackEntry, Lifecycle.State.STARTED);
                    }
                    navDestination3 = navDestination3.getParent();
                }
                else {
                    navBackStackEntry.setMaxLifecycle(Lifecycle.State.CREATED);
                }
            }
            for (final NavBackStackEntry key : this.mBackStack) {
                final Lifecycle.State maxLifecycle2 = hashMap.get(key);
                if (maxLifecycle2 != null) {
                    key.setMaxLifecycle(maxLifecycle2);
                }
                else {
                    key.updateState();
                }
            }
            final NavBackStackEntry navBackStackEntry2 = this.mBackStack.peekLast();
            final Iterator<OnDestinationChangedListener> iterator2 = this.mOnDestinationChangedListeners.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onDestinationChanged(this, navBackStackEntry2.getDestination(), navBackStackEntry2.getArguments());
            }
            return true;
        }
        return false;
    }
    
    private String findInvalidDestinationDisplayNameInDeepLink(final int[] array) {
        NavGraph mGraph = this.mGraph;
        int n = 0;
        while (true) {
            final int length = array.length;
            NavDestination navDestination = null;
            if (n >= length) {
                return null;
            }
            final int n2 = array[n];
            if (n == 0) {
                if (this.mGraph.getId() == n2) {
                    navDestination = this.mGraph;
                }
            }
            else {
                navDestination = mGraph.findNode(n2);
            }
            if (navDestination == null) {
                return NavDestination.getDisplayName(this.mContext, n2);
            }
            if (n != array.length - 1) {
                NavGraph navGraph;
                for (navGraph = (NavGraph)navDestination; navGraph.findNode(navGraph.getStartDestination()) instanceof NavGraph; navGraph = (NavGraph)navGraph.findNode(navGraph.getStartDestination())) {}
                mGraph = navGraph;
            }
            ++n;
        }
    }
    
    private int getDestinationCountOnBackStack() {
        final Iterator<NavBackStackEntry> iterator = this.mBackStack.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            if (!(iterator.next().getDestination() instanceof NavGraph)) {
                ++n;
            }
        }
        return n;
    }
    
    private void navigate(NavDestination navDestination, final Bundle bundle, final NavOptions navOptions, final Navigator.Extras extras) {
        final int n = 0;
        final boolean b = navOptions != null && navOptions.getPopUpTo() != -1 && this.popBackStackInternal(navOptions.getPopUpTo(), navOptions.isPopUpToInclusive());
        final Navigator navigator = this.mNavigatorProvider.getNavigator(navDestination.getNavigatorName());
        final Bundle addInDefaultArgs = navDestination.addInDefaultArgs(bundle);
        final NavDestination navigate = navigator.navigate(navDestination, addInDefaultArgs, navOptions, extras);
        int n2;
        if (navigate != null) {
            if (!(navigate instanceof FloatingWindow)) {
                while (!this.mBackStack.isEmpty() && this.mBackStack.peekLast().getDestination() instanceof FloatingWindow && this.popBackStackInternal(this.mBackStack.peekLast().getDestination().getId(), true)) {}
            }
            final ArrayDeque<NavBackStackEntry> arrayDeque = new ArrayDeque<NavBackStackEntry>();
            if (navDestination instanceof NavGraph) {
                NavDestination parent = navigate;
                do {
                    parent = parent.getParent();
                    if (parent != null) {
                        arrayDeque.addFirst(new NavBackStackEntry(this.mContext, parent, addInDefaultArgs, this.mLifecycleOwner, this.mViewModel));
                        if (!this.mBackStack.isEmpty() && this.mBackStack.getLast().getDestination() == parent) {
                            this.popBackStackInternal(parent.getId(), true);
                        }
                    }
                    if (parent == null) {
                        break;
                    }
                } while (parent != navDestination);
            }
            if (arrayDeque.isEmpty()) {
                navDestination = navigate;
            }
            else {
                navDestination = arrayDeque.getFirst().getDestination();
            }
            while (navDestination != null && this.findDestination(navDestination.getId()) == null) {
                final NavGraph parent2 = navDestination.getParent();
                if ((navDestination = parent2) != null) {
                    arrayDeque.addFirst(new NavBackStackEntry(this.mContext, parent2, addInDefaultArgs, this.mLifecycleOwner, this.mViewModel));
                    navDestination = parent2;
                }
            }
            if (arrayDeque.isEmpty()) {
                navDestination = navigate;
            }
            else {
                navDestination = arrayDeque.getLast().getDestination();
            }
            while (!this.mBackStack.isEmpty() && this.mBackStack.getLast().getDestination() instanceof NavGraph && ((NavGraph)this.mBackStack.getLast().getDestination()).findNode(navDestination.getId(), false) == null && this.popBackStackInternal(this.mBackStack.getLast().getDestination().getId(), true)) {}
            this.mBackStack.addAll(arrayDeque);
            if (this.mBackStack.isEmpty() || this.mBackStack.getFirst().getDestination() != this.mGraph) {
                this.mBackStack.addFirst(new NavBackStackEntry(this.mContext, this.mGraph, addInDefaultArgs, this.mLifecycleOwner, this.mViewModel));
            }
            this.mBackStack.add(new NavBackStackEntry(this.mContext, navigate, navigate.addInDefaultArgs(addInDefaultArgs), this.mLifecycleOwner, this.mViewModel));
            n2 = n;
        }
        else {
            n2 = n;
            if (navOptions != null) {
                n2 = n;
                if (navOptions.shouldLaunchSingleTop()) {
                    final NavBackStackEntry navBackStackEntry = this.mBackStack.peekLast();
                    if (navBackStackEntry != null) {
                        navBackStackEntry.replaceArguments(addInDefaultArgs);
                    }
                    n2 = 1;
                }
            }
        }
        this.updateOnBackPressedCallbackEnabled();
        if (b || navigate != null || n2 != 0) {
            this.dispatchOnDestinationChanged();
        }
    }
    
    private void onGraphCreated(final Bundle bundle) {
        final Bundle mNavigatorStateToRestore = this.mNavigatorStateToRestore;
        if (mNavigatorStateToRestore != null) {
            final ArrayList stringArrayList = mNavigatorStateToRestore.getStringArrayList("android-support-nav:controller:navigatorState:names");
            if (stringArrayList != null) {
                for (final String s : stringArrayList) {
                    final Navigator navigator = this.mNavigatorProvider.getNavigator(s);
                    final Bundle bundle2 = this.mNavigatorStateToRestore.getBundle(s);
                    if (bundle2 != null) {
                        navigator.onRestoreState(bundle2);
                    }
                }
            }
        }
        final Parcelable[] mBackStackToRestore = this.mBackStackToRestore;
        final boolean b = false;
        if (mBackStackToRestore != null) {
            for (int length = mBackStackToRestore.length, i = 0; i < length; ++i) {
                final NavBackStackEntryState navBackStackEntryState = (NavBackStackEntryState)mBackStackToRestore[i];
                final NavDestination destination = this.findDestination(navBackStackEntryState.getDestinationId());
                if (destination == null) {
                    final String displayName = NavDestination.getDisplayName(this.mContext, navBackStackEntryState.getDestinationId());
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Restoring the Navigation back stack failed: destination ");
                    sb.append(displayName);
                    sb.append(" cannot be found from the current destination ");
                    sb.append(this.getCurrentDestination());
                    throw new IllegalStateException(sb.toString());
                }
                final Bundle args = navBackStackEntryState.getArgs();
                if (args != null) {
                    args.setClassLoader(this.mContext.getClassLoader());
                }
                this.mBackStack.add(new NavBackStackEntry(this.mContext, destination, args, this.mLifecycleOwner, this.mViewModel, navBackStackEntryState.getUUID(), navBackStackEntryState.getSavedState()));
            }
            this.updateOnBackPressedCallbackEnabled();
            this.mBackStackToRestore = null;
        }
        if (this.mGraph != null && this.mBackStack.isEmpty()) {
            int n = b ? 1 : 0;
            if (!this.mDeepLinkHandled) {
                final Activity mActivity = this.mActivity;
                n = (b ? 1 : 0);
                if (mActivity != null) {
                    n = (b ? 1 : 0);
                    if (this.handleDeepLink(mActivity.getIntent())) {
                        n = 1;
                    }
                }
            }
            if (n == 0) {
                this.navigate(this.mGraph, bundle, null, null);
            }
        }
        else {
            this.dispatchOnDestinationChanged();
        }
    }
    
    private void updateOnBackPressedCallbackEnabled() {
        final OnBackPressedCallback mOnBackPressedCallback = this.mOnBackPressedCallback;
        final boolean mEnableOnBackPressedCallback = this.mEnableOnBackPressedCallback;
        boolean enabled = true;
        if (!mEnableOnBackPressedCallback || this.getDestinationCountOnBackStack() <= 1) {
            enabled = false;
        }
        mOnBackPressedCallback.setEnabled(enabled);
    }
    
    public void addOnDestinationChangedListener(final OnDestinationChangedListener e) {
        if (!this.mBackStack.isEmpty()) {
            final NavBackStackEntry navBackStackEntry = this.mBackStack.peekLast();
            e.onDestinationChanged(this, navBackStackEntry.getDestination(), navBackStackEntry.getArguments());
        }
        this.mOnDestinationChangedListeners.add(e);
    }
    
    public NavDeepLinkBuilder createDeepLink() {
        return new NavDeepLinkBuilder(this);
    }
    
    void enableOnBackPressed(final boolean mEnableOnBackPressedCallback) {
        this.mEnableOnBackPressedCallback = mEnableOnBackPressedCallback;
        this.updateOnBackPressedCallbackEnabled();
    }
    
    NavDestination findDestination(final int n) {
        final NavGraph mGraph = this.mGraph;
        if (mGraph == null) {
            return null;
        }
        if (mGraph.getId() == n) {
            return this.mGraph;
        }
        NavDestination navDestination;
        if (this.mBackStack.isEmpty()) {
            navDestination = this.mGraph;
        }
        else {
            navDestination = this.mBackStack.getLast().getDestination();
        }
        NavGraph parent;
        if (navDestination instanceof NavGraph) {
            parent = (NavGraph)navDestination;
        }
        else {
            parent = navDestination.getParent();
        }
        return parent.findNode(n);
    }
    
    public Deque<NavBackStackEntry> getBackStack() {
        return this.mBackStack;
    }
    
    public NavBackStackEntry getBackStackEntry(final int i) {
        final Iterator<NavBackStackEntry> descendingIterator = this.mBackStack.descendingIterator();
        while (true) {
            while (descendingIterator.hasNext()) {
                final NavBackStackEntry navBackStackEntry = descendingIterator.next();
                if (navBackStackEntry.getDestination().getId() == i) {
                    if (navBackStackEntry != null) {
                        return navBackStackEntry;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No destination with ID ");
                    sb.append(i);
                    sb.append(" is on the NavController's back stack. The current destination is ");
                    sb.append(this.getCurrentDestination());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final NavBackStackEntry navBackStackEntry = null;
            continue;
        }
    }
    
    Context getContext() {
        return this.mContext;
    }
    
    public NavBackStackEntry getCurrentBackStackEntry() {
        if (this.mBackStack.isEmpty()) {
            return null;
        }
        return this.mBackStack.getLast();
    }
    
    public NavDestination getCurrentDestination() {
        final NavBackStackEntry currentBackStackEntry = this.getCurrentBackStackEntry();
        NavDestination destination;
        if (currentBackStackEntry != null) {
            destination = currentBackStackEntry.getDestination();
        }
        else {
            destination = null;
        }
        return destination;
    }
    
    public NavGraph getGraph() {
        final NavGraph mGraph = this.mGraph;
        if (mGraph != null) {
            return mGraph;
        }
        throw new IllegalStateException("You must call setGraph() before calling getGraph()");
    }
    
    public NavInflater getNavInflater() {
        if (this.mInflater == null) {
            this.mInflater = new NavInflater(this.mContext, this.mNavigatorProvider);
        }
        return this.mInflater;
    }
    
    public NavigatorProvider getNavigatorProvider() {
        return this.mNavigatorProvider;
    }
    
    public NavBackStackEntry getPreviousBackStackEntry() {
        final Iterator<NavBackStackEntry> descendingIterator = this.mBackStack.descendingIterator();
        if (descendingIterator.hasNext()) {
            descendingIterator.next();
        }
        while (descendingIterator.hasNext()) {
            final NavBackStackEntry navBackStackEntry = descendingIterator.next();
            if (!(navBackStackEntry.getDestination() instanceof NavGraph)) {
                return navBackStackEntry;
            }
        }
        return null;
    }
    
    public ViewModelStoreOwner getViewModelStoreOwner(final int i) {
        if (this.mViewModel == null) {
            throw new IllegalStateException("You must call setViewModelStore() before calling getViewModelStoreOwner().");
        }
        final NavBackStackEntry backStackEntry = this.getBackStackEntry(i);
        if (backStackEntry.getDestination() instanceof NavGraph) {
            return backStackEntry;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No NavGraph with ID ");
        sb.append(i);
        sb.append(" is on the NavController's back stack");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public boolean handleDeepLink(final Intent obj) {
        if (obj == null) {
            return false;
        }
        final Bundle extras = obj.getExtras();
        int[] intArray;
        if (extras != null) {
            intArray = extras.getIntArray("android-support-nav:controller:deepLinkIds");
        }
        else {
            intArray = null;
        }
        final Bundle bundle = new Bundle();
        Bundle bundle2;
        if (extras != null) {
            bundle2 = extras.getBundle("android-support-nav:controller:deepLinkExtras");
        }
        else {
            bundle2 = null;
        }
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
        int[] buildDeepLinkIds = null;
        Label_0154: {
            if (intArray != null) {
                buildDeepLinkIds = intArray;
                if (intArray.length != 0) {
                    break Label_0154;
                }
            }
            buildDeepLinkIds = intArray;
            if (obj.getData() != null) {
                final NavDestination.DeepLinkMatch matchDeepLink = this.mGraph.matchDeepLink(new NavDeepLinkRequest(obj));
                buildDeepLinkIds = intArray;
                if (matchDeepLink != null) {
                    final NavDestination destination = matchDeepLink.getDestination();
                    buildDeepLinkIds = destination.buildDeepLinkIds();
                    bundle.putAll(destination.addInDefaultArgs(matchDeepLink.getMatchingArgs()));
                }
            }
        }
        if (buildDeepLinkIds == null || buildDeepLinkIds.length == 0) {
            return false;
        }
        final String invalidDestinationDisplayNameInDeepLink = this.findInvalidDestinationDisplayNameInDeepLink(buildDeepLinkIds);
        if (invalidDestinationDisplayNameInDeepLink != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not find destination ");
            sb.append(invalidDestinationDisplayNameInDeepLink);
            sb.append(" in the navigation graph, ignoring the deep link from ");
            sb.append(obj);
            Log.i("NavController", sb.toString());
            return false;
        }
        bundle.putParcelable("android-support-nav:controller:deepLinkIntent", (Parcelable)obj);
        final int flags = obj.getFlags();
        final int n = 0x10000000 & flags;
        if (n != 0 && (flags & 0x8000) == 0x0) {
            obj.addFlags(32768);
            TaskStackBuilder.create(this.mContext).addNextIntentWithParentStack(obj).startActivities();
            final Activity mActivity = this.mActivity;
            if (mActivity != null) {
                mActivity.finish();
                this.mActivity.overridePendingTransition(0, 0);
            }
            return true;
        }
        if (n != 0) {
            if (!this.mBackStack.isEmpty()) {
                this.popBackStackInternal(this.mGraph.getId(), true);
            }
            for (int i = 0; i < buildDeepLinkIds.length; ++i) {
                final int n2 = buildDeepLinkIds[i];
                final NavDestination destination2 = this.findDestination(n2);
                if (destination2 == null) {
                    final String displayName = NavDestination.getDisplayName(this.mContext, n2);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Deep Linking failed: destination ");
                    sb2.append(displayName);
                    sb2.append(" cannot be found from the current destination ");
                    sb2.append(this.getCurrentDestination());
                    throw new IllegalStateException(sb2.toString());
                }
                this.navigate(destination2, bundle, new NavOptions.Builder().setEnterAnim(0).setExitAnim(0).build(), null);
            }
            return true;
        }
        NavGraph mGraph = this.mGraph;
        for (int j = 0; j < buildDeepLinkIds.length; ++j) {
            final int n3 = buildDeepLinkIds[j];
            NavDestination navDestination;
            if (j == 0) {
                navDestination = this.mGraph;
            }
            else {
                navDestination = mGraph.findNode(n3);
            }
            if (navDestination == null) {
                final String displayName2 = NavDestination.getDisplayName(this.mContext, n3);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Deep Linking failed: destination ");
                sb3.append(displayName2);
                sb3.append(" cannot be found in graph ");
                sb3.append(mGraph);
                throw new IllegalStateException(sb3.toString());
            }
            if (j != buildDeepLinkIds.length - 1) {
                for (mGraph = (NavGraph)navDestination; mGraph.findNode(mGraph.getStartDestination()) instanceof NavGraph; mGraph = (NavGraph)mGraph.findNode(mGraph.getStartDestination())) {}
            }
            else {
                this.navigate(navDestination, navDestination.addInDefaultArgs(bundle), new NavOptions.Builder().setPopUpTo(this.mGraph.getId(), true).setEnterAnim(0).setExitAnim(0).build(), null);
            }
        }
        return this.mDeepLinkHandled = true;
    }
    
    public void navigate(final int n) {
        this.navigate(n, null);
    }
    
    public void navigate(final int n, final Bundle bundle) {
        this.navigate(n, bundle, null);
    }
    
    public void navigate(final int n, final Bundle bundle, final NavOptions navOptions) {
        this.navigate(n, bundle, navOptions, null);
    }
    
    public void navigate(final int n, final Bundle bundle, final NavOptions navOptions, final Navigator.Extras extras) {
        NavDestination navDestination;
        if (this.mBackStack.isEmpty()) {
            navDestination = this.mGraph;
        }
        else {
            navDestination = this.mBackStack.getLast().getDestination();
        }
        if (navDestination == null) {
            throw new IllegalStateException("no current navigation node");
        }
        final NavAction action = navDestination.getAction(n);
        final Bundle bundle2 = null;
        Bundle bundle3;
        int n2;
        NavOptions navOptions3;
        if (action != null) {
            NavOptions navOptions2;
            if ((navOptions2 = navOptions) == null) {
                navOptions2 = action.getNavOptions();
            }
            final int destinationId = action.getDestinationId();
            final Bundle defaultArguments = action.getDefaultArguments();
            bundle3 = bundle2;
            n2 = destinationId;
            navOptions3 = navOptions2;
            if (defaultArguments != null) {
                bundle3 = new Bundle();
                bundle3.putAll(defaultArguments);
                n2 = destinationId;
                navOptions3 = navOptions2;
            }
        }
        else {
            n2 = n;
            navOptions3 = navOptions;
            bundle3 = bundle2;
        }
        Bundle bundle4 = bundle3;
        if (bundle != null) {
            if ((bundle4 = bundle3) == null) {
                bundle4 = new Bundle();
            }
            bundle4.putAll(bundle);
        }
        if (n2 == 0 && navOptions3 != null && navOptions3.getPopUpTo() != -1) {
            this.popBackStack(navOptions3.getPopUpTo(), navOptions3.isPopUpToInclusive());
            return;
        }
        if (n2 == 0) {
            throw new IllegalArgumentException("Destination id == 0 can only be used in conjunction with a valid navOptions.popUpTo");
        }
        final NavDestination destination = this.findDestination(n2);
        if (destination != null) {
            this.navigate(destination, bundle4, navOptions3, extras);
            return;
        }
        final String displayName = NavDestination.getDisplayName(this.mContext, n2);
        if (action != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Navigation destination ");
            sb.append(displayName);
            sb.append(" referenced from action ");
            sb.append(NavDestination.getDisplayName(this.mContext, n));
            sb.append(" cannot be found from the current destination ");
            sb.append(navDestination);
            throw new IllegalArgumentException(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Navigation action/destination ");
        sb2.append(displayName);
        sb2.append(" cannot be found from the current destination ");
        sb2.append(navDestination);
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public void navigate(final Uri uri) {
        this.navigate(new NavDeepLinkRequest(uri, null, null));
    }
    
    public void navigate(final Uri uri, final NavOptions navOptions) {
        this.navigate(new NavDeepLinkRequest(uri, null, null), navOptions);
    }
    
    public void navigate(final Uri uri, final NavOptions navOptions, final Navigator.Extras extras) {
        this.navigate(new NavDeepLinkRequest(uri, null, null), navOptions, extras);
    }
    
    public void navigate(final NavDeepLinkRequest navDeepLinkRequest) {
        this.navigate(navDeepLinkRequest, null);
    }
    
    public void navigate(final NavDeepLinkRequest navDeepLinkRequest, final NavOptions navOptions) {
        this.navigate(navDeepLinkRequest, navOptions, null);
    }
    
    public void navigate(final NavDeepLinkRequest obj, final NavOptions navOptions, final Navigator.Extras extras) {
        final NavDestination.DeepLinkMatch matchDeepLink = this.mGraph.matchDeepLink(obj);
        if (matchDeepLink != null) {
            Bundle addInDefaultArgs;
            if ((addInDefaultArgs = matchDeepLink.getDestination().addInDefaultArgs(matchDeepLink.getMatchingArgs())) == null) {
                addInDefaultArgs = new Bundle();
            }
            final NavDestination destination = matchDeepLink.getDestination();
            final Intent intent = new Intent();
            intent.setDataAndType(obj.getUri(), obj.getMimeType());
            intent.setAction(obj.getAction());
            addInDefaultArgs.putParcelable("android-support-nav:controller:deepLinkIntent", (Parcelable)intent);
            this.navigate(destination, addInDefaultArgs, navOptions, extras);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Navigation destination that matches request ");
        sb.append(obj);
        sb.append(" cannot be found in the navigation graph ");
        sb.append(this.mGraph);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void navigate(final NavDirections navDirections) {
        this.navigate(navDirections.getActionId(), navDirections.getArguments());
    }
    
    public void navigate(final NavDirections navDirections, final NavOptions navOptions) {
        this.navigate(navDirections.getActionId(), navDirections.getArguments(), navOptions);
    }
    
    public void navigate(final NavDirections navDirections, final Navigator.Extras extras) {
        this.navigate(navDirections.getActionId(), navDirections.getArguments(), null, extras);
    }
    
    public boolean navigateUp() {
        if (this.getDestinationCountOnBackStack() == 1) {
            final NavDestination currentDestination = this.getCurrentDestination();
            int n = currentDestination.getId();
            for (NavGraph navGraph = currentDestination.getParent(); navGraph != null; navGraph = navGraph.getParent()) {
                if (navGraph.getStartDestination() != n) {
                    final Bundle arguments = new Bundle();
                    final Activity mActivity = this.mActivity;
                    if (mActivity != null && mActivity.getIntent() != null && this.mActivity.getIntent().getData() != null) {
                        arguments.putParcelable("android-support-nav:controller:deepLinkIntent", (Parcelable)this.mActivity.getIntent());
                        final NavDestination.DeepLinkMatch matchDeepLink = this.mGraph.matchDeepLink(new NavDeepLinkRequest(this.mActivity.getIntent()));
                        if (matchDeepLink != null) {
                            arguments.putAll(matchDeepLink.getDestination().addInDefaultArgs(matchDeepLink.getMatchingArgs()));
                        }
                    }
                    new NavDeepLinkBuilder(this).setDestination(navGraph.getId()).setArguments(arguments).createTaskStackBuilder().startActivities();
                    final Activity mActivity2 = this.mActivity;
                    if (mActivity2 != null) {
                        mActivity2.finish();
                    }
                    return true;
                }
                n = navGraph.getId();
            }
            return false;
        }
        return this.popBackStack();
    }
    
    public boolean popBackStack() {
        return !this.mBackStack.isEmpty() && this.popBackStack(this.getCurrentDestination().getId(), true);
    }
    
    public boolean popBackStack(final int n, final boolean b) {
        return this.popBackStackInternal(n, b) && this.dispatchOnDestinationChanged();
    }
    
    boolean popBackStackInternal(final int n, final boolean b) {
        final boolean empty = this.mBackStack.isEmpty();
        final boolean b2 = false;
        if (empty) {
            return false;
        }
        final ArrayList list = new ArrayList();
        final Iterator<NavBackStackEntry> descendingIterator = this.mBackStack.descendingIterator();
        while (true) {
            while (descendingIterator.hasNext()) {
                final NavDestination destination = descendingIterator.next().getDestination();
                final Navigator navigator = this.mNavigatorProvider.getNavigator(destination.getNavigatorName());
                if (b || destination.getId() != n) {
                    list.add(navigator);
                }
                if (destination.getId() == n) {
                    final boolean b3 = true;
                    if (!b3) {
                        final String displayName = NavDestination.getDisplayName(this.mContext, n);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Ignoring popBackStack to destination ");
                        sb.append(displayName);
                        sb.append(" as it was not found on the current back stack");
                        Log.i("NavController", sb.toString());
                        return false;
                    }
                    final Iterator iterator = list.iterator();
                    boolean b4 = b2;
                    while (iterator.hasNext() && ((Navigator)iterator.next()).popBackStack()) {
                        final NavBackStackEntry navBackStackEntry = this.mBackStack.removeLast();
                        if (navBackStackEntry.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
                            navBackStackEntry.setMaxLifecycle(Lifecycle.State.DESTROYED);
                        }
                        final NavControllerViewModel mViewModel = this.mViewModel;
                        if (mViewModel != null) {
                            mViewModel.clear(navBackStackEntry.mId);
                        }
                        b4 = true;
                    }
                    this.updateOnBackPressedCallbackEnabled();
                    return b4;
                }
            }
            final boolean b3 = false;
            continue;
        }
    }
    
    public void removeOnDestinationChangedListener(final OnDestinationChangedListener o) {
        this.mOnDestinationChangedListeners.remove(o);
    }
    
    public void restoreState(final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        bundle.setClassLoader(this.mContext.getClassLoader());
        this.mNavigatorStateToRestore = bundle.getBundle("android-support-nav:controller:navigatorState");
        this.mBackStackToRestore = bundle.getParcelableArray("android-support-nav:controller:backStack");
        this.mDeepLinkHandled = bundle.getBoolean("android-support-nav:controller:deepLinkHandled");
    }
    
    public Bundle saveState() {
        final ArrayList list = new ArrayList();
        final Bundle bundle = new Bundle();
        for (final Map.Entry<String, V> entry : this.mNavigatorProvider.getNavigators().entrySet()) {
            final String e = entry.getKey();
            final Bundle onSaveState = ((Navigator)entry.getValue()).onSaveState();
            if (onSaveState != null) {
                list.add(e);
                bundle.putBundle(e, onSaveState);
            }
        }
        Bundle bundle2;
        if (!list.isEmpty()) {
            bundle2 = new Bundle();
            bundle.putStringArrayList("android-support-nav:controller:navigatorState:names", list);
            bundle2.putBundle("android-support-nav:controller:navigatorState", bundle);
        }
        else {
            bundle2 = null;
        }
        Bundle bundle3 = bundle2;
        if (!this.mBackStack.isEmpty()) {
            if ((bundle3 = bundle2) == null) {
                bundle3 = new Bundle();
            }
            final Parcelable[] array = new Parcelable[this.mBackStack.size()];
            int n = 0;
            final Iterator<NavBackStackEntry> iterator2 = this.mBackStack.iterator();
            while (iterator2.hasNext()) {
                array[n] = (Parcelable)new NavBackStackEntryState(iterator2.next());
                ++n;
            }
            bundle3.putParcelableArray("android-support-nav:controller:backStack", array);
        }
        Bundle bundle4 = bundle3;
        if (this.mDeepLinkHandled) {
            if ((bundle4 = bundle3) == null) {
                bundle4 = new Bundle();
            }
            bundle4.putBoolean("android-support-nav:controller:deepLinkHandled", this.mDeepLinkHandled);
        }
        return bundle4;
    }
    
    public void setGraph(final int n) {
        this.setGraph(n, null);
    }
    
    public void setGraph(final int n, final Bundle bundle) {
        this.setGraph(this.getNavInflater().inflate(n), bundle);
    }
    
    public void setGraph(final NavGraph navGraph) {
        this.setGraph(navGraph, null);
    }
    
    public void setGraph(final NavGraph mGraph, final Bundle bundle) {
        final NavGraph mGraph2 = this.mGraph;
        if (mGraph2 != null) {
            this.popBackStackInternal(mGraph2.getId(), true);
        }
        this.mGraph = mGraph;
        this.onGraphCreated(bundle);
    }
    
    void setLifecycleOwner(final LifecycleOwner mLifecycleOwner) {
        if (mLifecycleOwner == this.mLifecycleOwner) {
            return;
        }
        this.mLifecycleOwner = mLifecycleOwner;
        mLifecycleOwner.getLifecycle().addObserver(this.mLifecycleObserver);
    }
    
    public void setNavigatorProvider(final NavigatorProvider mNavigatorProvider) {
        if (this.mBackStack.isEmpty()) {
            this.mNavigatorProvider = mNavigatorProvider;
            return;
        }
        throw new IllegalStateException("NavigatorProvider must be set before setGraph call");
    }
    
    void setOnBackPressedDispatcher(final OnBackPressedDispatcher onBackPressedDispatcher) {
        if (this.mLifecycleOwner != null) {
            this.mOnBackPressedCallback.remove();
            onBackPressedDispatcher.addCallback(this.mLifecycleOwner, this.mOnBackPressedCallback);
            this.mLifecycleOwner.getLifecycle().removeObserver(this.mLifecycleObserver);
            this.mLifecycleOwner.getLifecycle().addObserver(this.mLifecycleObserver);
            return;
        }
        throw new IllegalStateException("You must call setLifecycleOwner() before calling setOnBackPressedDispatcher()");
    }
    
    void setViewModelStore(final ViewModelStore viewModelStore) {
        if (this.mViewModel == NavControllerViewModel.getInstance(viewModelStore)) {
            return;
        }
        if (this.mBackStack.isEmpty()) {
            this.mViewModel = NavControllerViewModel.getInstance(viewModelStore);
            return;
        }
        throw new IllegalStateException("ViewModelStore should be set before setGraph call");
    }
    
    public interface OnDestinationChangedListener
    {
        void onDestinationChanged(final NavController p0, final NavDestination p1, final Bundle p2);
    }
}

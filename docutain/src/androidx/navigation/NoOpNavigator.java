// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

@Name("NoOp")
public class NoOpNavigator extends Navigator<NavDestination>
{
    @Override
    public NavDestination createDestination() {
        return new NavDestination(this);
    }
    
    @Override
    public NavDestination navigate(final NavDestination navDestination, final Bundle bundle, final NavOptions navOptions, final Extras extras) {
        return navDestination;
    }
    
    @Override
    public boolean popBackStack() {
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.content.ComponentName;
import android.os.Parcelable;
import androidx.core.app.TaskStackBuilder;
import android.app.PendingIntent;
import java.util.Iterator;
import java.util.ArrayDeque;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;

public final class NavDeepLinkBuilder
{
    private Bundle mArgs;
    private final Context mContext;
    private int mDestId;
    private NavGraph mGraph;
    private final Intent mIntent;
    
    public NavDeepLinkBuilder(final Context mContext) {
        this.mContext = mContext;
        if (mContext instanceof Activity) {
            this.mIntent = new Intent(mContext, (Class)mContext.getClass());
        }
        else {
            Intent launchIntentForPackage = mContext.getPackageManager().getLaunchIntentForPackage(mContext.getPackageName());
            if (launchIntentForPackage == null) {
                launchIntentForPackage = new Intent();
            }
            this.mIntent = launchIntentForPackage;
        }
        this.mIntent.addFlags(268468224);
    }
    
    NavDeepLinkBuilder(final NavController navController) {
        this(navController.getContext());
        this.mGraph = navController.getGraph();
    }
    
    private void fillInIntent() {
        final ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.add(this.mGraph);
        NavDestination navDestination = null;
        while (!arrayDeque.isEmpty() && navDestination == null) {
            final NavDestination navDestination2 = arrayDeque.poll();
            if (navDestination2.getId() == this.mDestId) {
                navDestination = navDestination2;
            }
            else {
                if (!(navDestination2 instanceof NavGraph)) {
                    continue;
                }
                final Iterator<NavDestination> iterator = ((NavGraph)navDestination2).iterator();
                while (iterator.hasNext()) {
                    arrayDeque.add(iterator.next());
                }
            }
        }
        if (navDestination != null) {
            this.mIntent.putExtra("android-support-nav:controller:deepLinkIds", navDestination.buildDeepLinkIds());
            return;
        }
        final String displayName = NavDestination.getDisplayName(this.mContext, this.mDestId);
        final StringBuilder sb = new StringBuilder();
        sb.append("Navigation destination ");
        sb.append(displayName);
        sb.append(" cannot be found in the navigation graph ");
        sb.append(this.mGraph);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public PendingIntent createPendingIntent() {
        final Bundle mArgs = this.mArgs;
        int n = 0;
        if (mArgs != null) {
            final Iterator iterator = mArgs.keySet().iterator();
            n = 0;
            while (iterator.hasNext()) {
                final Object value = this.mArgs.get((String)iterator.next());
                int hashCode;
                if (value != null) {
                    hashCode = value.hashCode();
                }
                else {
                    hashCode = 0;
                }
                n = n * 31 + hashCode;
            }
        }
        return this.createTaskStackBuilder().getPendingIntent(n * 31 + this.mDestId, 134217728);
    }
    
    public TaskStackBuilder createTaskStackBuilder() {
        if (this.mIntent.getIntArrayExtra("android-support-nav:controller:deepLinkIds") != null) {
            final TaskStackBuilder addNextIntentWithParentStack = TaskStackBuilder.create(this.mContext).addNextIntentWithParentStack(new Intent(this.mIntent));
            for (int i = 0; i < addNextIntentWithParentStack.getIntentCount(); ++i) {
                addNextIntentWithParentStack.editIntentAt(i).putExtra("android-support-nav:controller:deepLinkIntent", (Parcelable)this.mIntent);
            }
            return addNextIntentWithParentStack;
        }
        if (this.mGraph == null) {
            throw new IllegalStateException("You must call setGraph() before constructing the deep link");
        }
        throw new IllegalStateException("You must call setDestination() before constructing the deep link");
    }
    
    public NavDeepLinkBuilder setArguments(final Bundle mArgs) {
        this.mArgs = mArgs;
        this.mIntent.putExtra("android-support-nav:controller:deepLinkExtras", mArgs);
        return this;
    }
    
    public NavDeepLinkBuilder setComponentName(final ComponentName component) {
        this.mIntent.setComponent(component);
        return this;
    }
    
    public NavDeepLinkBuilder setComponentName(final Class<? extends Activity> clazz) {
        return this.setComponentName(new ComponentName(this.mContext, (Class)clazz));
    }
    
    public NavDeepLinkBuilder setDestination(final int mDestId) {
        this.mDestId = mDestId;
        if (this.mGraph != null) {
            this.fillInIntent();
        }
        return this;
    }
    
    public NavDeepLinkBuilder setGraph(final int n) {
        return this.setGraph(new NavInflater(this.mContext, new PermissiveNavigatorProvider()).inflate(n));
    }
    
    public NavDeepLinkBuilder setGraph(final NavGraph mGraph) {
        this.mGraph = mGraph;
        if (this.mDestId != 0) {
            this.fillInIntent();
        }
        return this;
    }
    
    private static class PermissiveNavigatorProvider extends NavigatorProvider
    {
        private final Navigator<NavDestination> mDestNavigator;
        
        PermissiveNavigatorProvider() {
            this.mDestNavigator = new Navigator<NavDestination>() {
                final PermissiveNavigatorProvider this$0;
                
                @Override
                public NavDestination createDestination() {
                    return new NavDestination("permissive");
                }
                
                @Override
                public NavDestination navigate(final NavDestination navDestination, final Bundle bundle, final NavOptions navOptions, final Extras extras) {
                    throw new IllegalStateException("navigate is not supported");
                }
                
                @Override
                public boolean popBackStack() {
                    throw new IllegalStateException("popBackStack is not supported");
                }
            };
            this.addNavigator(new NavGraphNavigator(this));
        }
        
        @Override
        public Navigator<? extends NavDestination> getNavigator(final String s) {
            try {
                return super.getNavigator(s);
            }
            catch (final IllegalStateException ex) {
                return this.mDestNavigator;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

public interface NavDirections
{
    int getActionId();
    
    Bundle getArguments();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

@Name("navigation")
public class NavGraphNavigator extends Navigator<NavGraph>
{
    private final NavigatorProvider mNavigatorProvider;
    
    public NavGraphNavigator(final NavigatorProvider mNavigatorProvider) {
        this.mNavigatorProvider = mNavigatorProvider;
    }
    
    @Override
    public NavGraph createDestination() {
        return new NavGraph(this);
    }
    
    @Override
    public NavDestination navigate(final NavGraph navGraph, final Bundle bundle, final NavOptions navOptions, final Extras extras) {
        final int startDestination = navGraph.getStartDestination();
        if (startDestination == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("no start destination defined via app:startDestination for ");
            sb.append(navGraph.getDisplayName());
            throw new IllegalStateException(sb.toString());
        }
        final NavDestination node = navGraph.findNode(startDestination, false);
        if (node != null) {
            return this.mNavigatorProvider.getNavigator(node.getNavigatorName()).navigate(node, node.addInDefaultArgs(bundle), navOptions, extras);
        }
        final String startDestDisplayName = navGraph.getStartDestDisplayName();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("navigation destination ");
        sb2.append(startDestDisplayName);
        sb2.append(" is not a direct child of this NavGraph");
        throw new IllegalArgumentException(sb2.toString());
    }
    
    @Override
    public boolean popBackStack() {
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import android.view.ViewGroup;
import androidx.transition.TransitionManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.navigation.NavDestination;
import androidx.navigation.NavController;
import androidx.appcompat.widget.Toolbar;
import java.lang.ref.WeakReference;

class ToolbarOnDestinationChangedListener extends AbstractAppBarOnDestinationChangedListener
{
    private final WeakReference<Toolbar> mToolbarWeakReference;
    
    ToolbarOnDestinationChangedListener(final Toolbar referent, final AppBarConfiguration appBarConfiguration) {
        super(referent.getContext(), appBarConfiguration);
        this.mToolbarWeakReference = new WeakReference<Toolbar>(referent);
    }
    
    @Override
    public void onDestinationChanged(final NavController navController, final NavDestination navDestination, final Bundle bundle) {
        if (this.mToolbarWeakReference.get() == null) {
            navController.removeOnDestinationChangedListener((NavController.OnDestinationChangedListener)this);
            return;
        }
        super.onDestinationChanged(navController, navDestination, bundle);
    }
    
    @Override
    protected void setNavigationIcon(final Drawable navigationIcon, final int navigationContentDescription) {
        final Toolbar toolbar = this.mToolbarWeakReference.get();
        if (toolbar != null) {
            final boolean b = navigationIcon == null && toolbar.getNavigationIcon() != null;
            toolbar.setNavigationIcon(navigationIcon);
            toolbar.setNavigationContentDescription(navigationContentDescription);
            if (b) {
                TransitionManager.beginDelayedTransition(toolbar);
            }
        }
    }
    
    @Override
    protected void setTitle(final CharSequence title) {
        this.mToolbarWeakReference.get().setTitle(title);
    }
}

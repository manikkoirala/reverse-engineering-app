// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import android.view.ViewGroup;
import androidx.transition.TransitionManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.navigation.NavDestination;
import androidx.navigation.NavController;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import java.lang.ref.WeakReference;

class CollapsingToolbarOnDestinationChangedListener extends AbstractAppBarOnDestinationChangedListener
{
    private final WeakReference<CollapsingToolbarLayout> mCollapsingToolbarLayoutWeakReference;
    private final WeakReference<Toolbar> mToolbarWeakReference;
    
    CollapsingToolbarOnDestinationChangedListener(final CollapsingToolbarLayout referent, final Toolbar referent2, final AppBarConfiguration appBarConfiguration) {
        super(referent.getContext(), appBarConfiguration);
        this.mCollapsingToolbarLayoutWeakReference = new WeakReference<CollapsingToolbarLayout>(referent);
        this.mToolbarWeakReference = new WeakReference<Toolbar>(referent2);
    }
    
    @Override
    public void onDestinationChanged(final NavController navController, final NavDestination navDestination, final Bundle bundle) {
        final CollapsingToolbarLayout collapsingToolbarLayout = this.mCollapsingToolbarLayoutWeakReference.get();
        final Toolbar toolbar = this.mToolbarWeakReference.get();
        if (collapsingToolbarLayout != null && toolbar != null) {
            super.onDestinationChanged(navController, navDestination, bundle);
            return;
        }
        navController.removeOnDestinationChangedListener((NavController.OnDestinationChangedListener)this);
    }
    
    @Override
    protected void setNavigationIcon(final Drawable navigationIcon, final int navigationContentDescription) {
        final Toolbar toolbar = this.mToolbarWeakReference.get();
        if (toolbar != null) {
            final boolean b = navigationIcon == null && toolbar.getNavigationIcon() != null;
            toolbar.setNavigationIcon(navigationIcon);
            toolbar.setNavigationContentDescription(navigationContentDescription);
            if (b) {
                TransitionManager.beginDelayedTransition(toolbar);
            }
        }
    }
    
    @Override
    protected void setTitle(final CharSequence title) {
        final CollapsingToolbarLayout collapsingToolbarLayout = this.mCollapsingToolbarLayoutWeakReference.get();
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setTitle(title);
        }
    }
}

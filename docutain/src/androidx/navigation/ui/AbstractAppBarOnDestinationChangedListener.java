// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import androidx.navigation.FloatingWindow;
import android.os.Bundle;
import androidx.navigation.NavDestination;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import java.util.Set;
import androidx.customview.widget.Openable;
import java.lang.ref.WeakReference;
import android.content.Context;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import android.animation.ValueAnimator;
import androidx.navigation.NavController;

abstract class AbstractAppBarOnDestinationChangedListener implements OnDestinationChangedListener
{
    private ValueAnimator mAnimator;
    private DrawerArrowDrawable mArrowDrawable;
    private final Context mContext;
    private final WeakReference<Openable> mOpenableLayoutWeakReference;
    private final Set<Integer> mTopLevelDestinations;
    
    AbstractAppBarOnDestinationChangedListener(final Context mContext, final AppBarConfiguration appBarConfiguration) {
        this.mContext = mContext;
        this.mTopLevelDestinations = appBarConfiguration.getTopLevelDestinations();
        final Openable openableLayout = appBarConfiguration.getOpenableLayout();
        if (openableLayout != null) {
            this.mOpenableLayoutWeakReference = new WeakReference<Openable>(openableLayout);
        }
        else {
            this.mOpenableLayoutWeakReference = null;
        }
    }
    
    private void setActionBarUpIndicator(final boolean b) {
        boolean b2;
        if (this.mArrowDrawable == null) {
            this.mArrowDrawable = new DrawerArrowDrawable(this.mContext);
            b2 = false;
        }
        else {
            b2 = true;
        }
        final DrawerArrowDrawable mArrowDrawable = this.mArrowDrawable;
        int n;
        if (b) {
            n = R.string.nav_app_bar_open_drawer_description;
        }
        else {
            n = R.string.nav_app_bar_navigate_up_description;
        }
        this.setNavigationIcon(mArrowDrawable, n);
        float progress;
        if (b) {
            progress = 0.0f;
        }
        else {
            progress = 1.0f;
        }
        if (b2) {
            final float progress2 = this.mArrowDrawable.getProgress();
            final ValueAnimator mAnimator = this.mAnimator;
            if (mAnimator != null) {
                mAnimator.cancel();
            }
            (this.mAnimator = (ValueAnimator)ObjectAnimator.ofFloat((Object)this.mArrowDrawable, "progress", new float[] { progress2, progress })).start();
        }
        else {
            this.mArrowDrawable.setProgress(progress);
        }
    }
    
    @Override
    public void onDestinationChanged(final NavController navController, final NavDestination navDestination, final Bundle obj) {
        if (navDestination instanceof FloatingWindow) {
            return;
        }
        final WeakReference<Openable> mOpenableLayoutWeakReference = this.mOpenableLayoutWeakReference;
        Openable openable;
        if (mOpenableLayoutWeakReference != null) {
            openable = mOpenableLayoutWeakReference.get();
        }
        else {
            openable = null;
        }
        if (this.mOpenableLayoutWeakReference != null && openable == null) {
            navController.removeOnDestinationChangedListener((NavController.OnDestinationChangedListener)this);
            return;
        }
        final CharSequence label = navDestination.getLabel();
        boolean actionBarUpIndicator = true;
        if (label != null) {
            final StringBuffer title = new StringBuffer();
            final Matcher matcher = Pattern.compile("\\{(.+?)\\}").matcher(label);
            while (matcher.find()) {
                final String group = matcher.group(1);
                if (obj == null || !obj.containsKey(group)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not find ");
                    sb.append(group);
                    sb.append(" in ");
                    sb.append(obj);
                    sb.append(" to fill label ");
                    sb.append((Object)label);
                    throw new IllegalArgumentException(sb.toString());
                }
                matcher.appendReplacement(title, "");
                title.append(obj.get(group).toString());
            }
            matcher.appendTail(title);
            this.setTitle(title);
        }
        final boolean matchDestinations = NavigationUI.matchDestinations(navDestination, this.mTopLevelDestinations);
        if (openable == null && matchDestinations) {
            this.setNavigationIcon(null, 0);
        }
        else {
            if (openable == null || !matchDestinations) {
                actionBarUpIndicator = false;
            }
            this.setActionBarUpIndicator(actionBarUpIndicator);
        }
    }
    
    protected abstract void setNavigationIcon(final Drawable p0, final int p1);
    
    protected abstract void setTitle(final CharSequence p0);
}

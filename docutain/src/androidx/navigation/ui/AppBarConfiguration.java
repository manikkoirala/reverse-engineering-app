// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import java.util.Collection;
import androidx.navigation.NavGraph;
import java.util.HashSet;
import android.view.Menu;
import androidx.drawerlayout.widget.DrawerLayout;
import java.util.Set;
import androidx.customview.widget.Openable;

public final class AppBarConfiguration
{
    private final OnNavigateUpListener mFallbackOnNavigateUpListener;
    private final Openable mOpenableLayout;
    private final Set<Integer> mTopLevelDestinations;
    
    private AppBarConfiguration(final Set<Integer> mTopLevelDestinations, final Openable mOpenableLayout, final OnNavigateUpListener mFallbackOnNavigateUpListener) {
        this.mTopLevelDestinations = mTopLevelDestinations;
        this.mOpenableLayout = mOpenableLayout;
        this.mFallbackOnNavigateUpListener = mFallbackOnNavigateUpListener;
    }
    
    @Deprecated
    public DrawerLayout getDrawerLayout() {
        final Openable mOpenableLayout = this.mOpenableLayout;
        if (mOpenableLayout instanceof DrawerLayout) {
            return (DrawerLayout)mOpenableLayout;
        }
        return null;
    }
    
    public OnNavigateUpListener getFallbackOnNavigateUpListener() {
        return this.mFallbackOnNavigateUpListener;
    }
    
    public Openable getOpenableLayout() {
        return this.mOpenableLayout;
    }
    
    public Set<Integer> getTopLevelDestinations() {
        return this.mTopLevelDestinations;
    }
    
    public static final class Builder
    {
        private OnNavigateUpListener mFallbackOnNavigateUpListener;
        private Openable mOpenableLayout;
        private final Set<Integer> mTopLevelDestinations;
        
        public Builder(final Menu menu) {
            this.mTopLevelDestinations = new HashSet<Integer>();
            for (int size = menu.size(), i = 0; i < size; ++i) {
                this.mTopLevelDestinations.add(menu.getItem(i).getItemId());
            }
        }
        
        public Builder(final NavGraph navGraph) {
            (this.mTopLevelDestinations = new HashSet<Integer>()).add(NavigationUI.findStartDestination(navGraph).getId());
        }
        
        public Builder(final Set<Integer> set) {
            (this.mTopLevelDestinations = new HashSet<Integer>()).addAll(set);
        }
        
        public Builder(final int... array) {
            this.mTopLevelDestinations = new HashSet<Integer>();
            for (int length = array.length, i = 0; i < length; ++i) {
                this.mTopLevelDestinations.add(array[i]);
            }
        }
        
        public AppBarConfiguration build() {
            return new AppBarConfiguration(this.mTopLevelDestinations, this.mOpenableLayout, this.mFallbackOnNavigateUpListener, null);
        }
        
        @Deprecated
        public Builder setDrawerLayout(final DrawerLayout mOpenableLayout) {
            this.mOpenableLayout = mOpenableLayout;
            return this;
        }
        
        public Builder setFallbackOnNavigateUpListener(final OnNavigateUpListener mFallbackOnNavigateUpListener) {
            this.mFallbackOnNavigateUpListener = mFallbackOnNavigateUpListener;
            return this;
        }
        
        public Builder setOpenableLayout(final Openable mOpenableLayout) {
            this.mOpenableLayout = mOpenableLayout;
            return this;
        }
    }
    
    public interface OnNavigateUpListener
    {
        boolean onNavigateUp();
    }
}

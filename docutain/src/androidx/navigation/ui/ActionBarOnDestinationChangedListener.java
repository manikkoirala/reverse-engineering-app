// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import androidx.appcompat.app.ActionBar;
import android.graphics.drawable.Drawable;
import androidx.appcompat.app.AppCompatActivity;

class ActionBarOnDestinationChangedListener extends AbstractAppBarOnDestinationChangedListener
{
    private final AppCompatActivity mActivity;
    
    ActionBarOnDestinationChangedListener(final AppCompatActivity mActivity, final AppBarConfiguration appBarConfiguration) {
        super(mActivity.getDrawerToggleDelegate().getActionBarThemedContext(), appBarConfiguration);
        this.mActivity = mActivity;
    }
    
    @Override
    protected void setNavigationIcon(final Drawable drawable, final int n) {
        final ActionBar supportActionBar = this.mActivity.getSupportActionBar();
        if (drawable == null) {
            supportActionBar.setDisplayHomeAsUpEnabled(false);
        }
        else {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            this.mActivity.getDrawerToggleDelegate().setActionBarUpIndicator(drawable, n);
        }
    }
    
    @Override
    protected void setTitle(final CharSequence title) {
        this.mActivity.getSupportActionBar().setTitle(title);
    }
}

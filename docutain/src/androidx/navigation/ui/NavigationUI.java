// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation.ui;

import com.google.android.material.navigation.NavigationView$OnNavigationItemSelectedListener;
import com.google.android.material.navigation.NavigationView;
import android.view.Menu;
import java.lang.ref.WeakReference;
import com.google.android.material.bottomnavigation.BottomNavigationView$OnNavigationItemSelectedListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.navigation.ActivityNavigator;
import androidx.navigation.NavOptions;
import android.view.MenuItem;
import androidx.customview.widget.Openable;
import androidx.navigation.NavController;
import java.util.Set;
import androidx.navigation.NavDestination;
import androidx.navigation.NavGraph;
import android.view.ViewParent;
import android.view.ViewGroup$LayoutParams;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;

public final class NavigationUI
{
    private NavigationUI() {
    }
    
    static BottomSheetBehavior findBottomSheetBehavior(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof CoordinatorLayout.LayoutParams)) {
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                return findBottomSheetBehavior((View)parent);
            }
            return null;
        }
        else {
            final CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams)layoutParams).getBehavior();
            if (!(behavior instanceof BottomSheetBehavior)) {
                return null;
            }
            return (BottomSheetBehavior)behavior;
        }
    }
    
    static NavDestination findStartDestination(NavGraph node) {
        while (node instanceof NavGraph) {
            node = node;
            node = ((NavGraph)node).findNode(((NavGraph)node).getStartDestination());
        }
        return node;
    }
    
    static boolean matchDestination(NavDestination parent, final int n) {
        while (parent.getId() != n && parent.getParent() != null) {
            parent = parent.getParent();
        }
        return parent.getId() == n;
    }
    
    static boolean matchDestinations(NavDestination parent, final Set<Integer> set) {
        while (!set.contains(parent.getId())) {
            if ((parent = parent.getParent()) == null) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean navigateUp(final NavController navController, final Openable openableLayout) {
        return navigateUp(navController, new AppBarConfiguration.Builder(navController.getGraph()).setOpenableLayout(openableLayout).build());
    }
    
    public static boolean navigateUp(final NavController navController, final AppBarConfiguration appBarConfiguration) {
        final Openable openableLayout = appBarConfiguration.getOpenableLayout();
        final NavDestination currentDestination = navController.getCurrentDestination();
        final Set<Integer> topLevelDestinations = appBarConfiguration.getTopLevelDestinations();
        if (openableLayout != null && currentDestination != null && matchDestinations(currentDestination, topLevelDestinations)) {
            openableLayout.open();
            return true;
        }
        return navController.navigateUp() || (appBarConfiguration.getFallbackOnNavigateUpListener() != null && appBarConfiguration.getFallbackOnNavigateUpListener().onNavigateUp());
    }
    
    public static boolean onNavDestinationSelected(final MenuItem menuItem, final NavController navController) {
        final NavOptions.Builder setLaunchSingleTop = new NavOptions.Builder().setLaunchSingleTop(true);
        if (navController.getCurrentDestination().getParent().findNode(menuItem.getItemId()) instanceof ActivityNavigator.Destination) {
            setLaunchSingleTop.setEnterAnim(R.anim.nav_default_enter_anim).setExitAnim(R.anim.nav_default_exit_anim).setPopEnterAnim(R.anim.nav_default_pop_enter_anim).setPopExitAnim(R.anim.nav_default_pop_exit_anim);
        }
        else {
            setLaunchSingleTop.setEnterAnim(R.animator.nav_default_enter_anim).setExitAnim(R.animator.nav_default_exit_anim).setPopEnterAnim(R.animator.nav_default_pop_enter_anim).setPopExitAnim(R.animator.nav_default_pop_exit_anim);
        }
        if ((menuItem.getOrder() & 0x30000) == 0x0) {
            setLaunchSingleTop.setPopUpTo(findStartDestination(navController.getGraph()).getId(), false);
        }
        final NavOptions build = setLaunchSingleTop.build();
        try {
            navController.navigate(menuItem.getItemId(), null, build);
            return true;
        }
        catch (final IllegalArgumentException ex) {
            return false;
        }
    }
    
    public static void setupActionBarWithNavController(final AppCompatActivity appCompatActivity, final NavController navController) {
        setupActionBarWithNavController(appCompatActivity, navController, new AppBarConfiguration.Builder(navController.getGraph()).build());
    }
    
    public static void setupActionBarWithNavController(final AppCompatActivity appCompatActivity, final NavController navController, final Openable openableLayout) {
        setupActionBarWithNavController(appCompatActivity, navController, new AppBarConfiguration.Builder(navController.getGraph()).setOpenableLayout(openableLayout).build());
    }
    
    public static void setupActionBarWithNavController(final AppCompatActivity appCompatActivity, final NavController navController, final AppBarConfiguration appBarConfiguration) {
        navController.addOnDestinationChangedListener((NavController.OnDestinationChangedListener)new ActionBarOnDestinationChangedListener(appCompatActivity, appBarConfiguration));
    }
    
    public static void setupWithNavController(final Toolbar toolbar, final NavController navController) {
        setupWithNavController(toolbar, navController, new AppBarConfiguration.Builder(navController.getGraph()).build());
    }
    
    public static void setupWithNavController(final Toolbar toolbar, final NavController navController, final Openable openableLayout) {
        setupWithNavController(toolbar, navController, new AppBarConfiguration.Builder(navController.getGraph()).setOpenableLayout(openableLayout).build());
    }
    
    public static void setupWithNavController(final Toolbar toolbar, final NavController navController, final AppBarConfiguration appBarConfiguration) {
        navController.addOnDestinationChangedListener((NavController.OnDestinationChangedListener)new ToolbarOnDestinationChangedListener(toolbar, appBarConfiguration));
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(navController, appBarConfiguration) {
            final AppBarConfiguration val$configuration;
            final NavController val$navController;
            
            public void onClick(final View view) {
                NavigationUI.navigateUp(this.val$navController, this.val$configuration);
            }
        });
    }
    
    public static void setupWithNavController(final CollapsingToolbarLayout collapsingToolbarLayout, final Toolbar toolbar, final NavController navController) {
        setupWithNavController(collapsingToolbarLayout, toolbar, navController, new AppBarConfiguration.Builder(navController.getGraph()).build());
    }
    
    public static void setupWithNavController(final CollapsingToolbarLayout collapsingToolbarLayout, final Toolbar toolbar, final NavController navController, final Openable openableLayout) {
        setupWithNavController(collapsingToolbarLayout, toolbar, navController, new AppBarConfiguration.Builder(navController.getGraph()).setOpenableLayout(openableLayout).build());
    }
    
    public static void setupWithNavController(final CollapsingToolbarLayout collapsingToolbarLayout, final Toolbar toolbar, final NavController navController, final AppBarConfiguration appBarConfiguration) {
        navController.addOnDestinationChangedListener((NavController.OnDestinationChangedListener)new CollapsingToolbarOnDestinationChangedListener(collapsingToolbarLayout, toolbar, appBarConfiguration));
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(navController, appBarConfiguration) {
            final AppBarConfiguration val$configuration;
            final NavController val$navController;
            
            public void onClick(final View view) {
                NavigationUI.navigateUp(this.val$navController, this.val$configuration);
            }
        });
    }
    
    public static void setupWithNavController(final BottomNavigationView referent, final NavController navController) {
        referent.setOnNavigationItemSelectedListener((BottomNavigationView$OnNavigationItemSelectedListener)new BottomNavigationView$OnNavigationItemSelectedListener(navController) {
            final NavController val$navController;
            
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                return NavigationUI.onNavDestinationSelected(menuItem, this.val$navController);
            }
        });
        navController.addOnDestinationChangedListener((NavController.OnDestinationChangedListener)new NavController.OnDestinationChangedListener(new WeakReference((T)referent), navController) {
            final NavController val$navController;
            final WeakReference val$weakReference;
            
            @Override
            public void onDestinationChanged(final NavController navController, final NavDestination navDestination, final Bundle bundle) {
                final BottomNavigationView bottomNavigationView = (BottomNavigationView)this.val$weakReference.get();
                if (bottomNavigationView == null) {
                    this.val$navController.removeOnDestinationChangedListener((NavController.OnDestinationChangedListener)this);
                    return;
                }
                final Menu menu = bottomNavigationView.getMenu();
                for (int i = 0; i < menu.size(); ++i) {
                    final MenuItem item = menu.getItem(i);
                    if (NavigationUI.matchDestination(navDestination, item.getItemId())) {
                        item.setChecked(true);
                    }
                }
            }
        });
    }
    
    public static void setupWithNavController(final NavigationView referent, final NavController navController) {
        referent.setNavigationItemSelectedListener((NavigationView$OnNavigationItemSelectedListener)new NavigationView$OnNavigationItemSelectedListener(navController, referent) {
            final NavController val$navController;
            final NavigationView val$navigationView;
            
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                final boolean onNavDestinationSelected = NavigationUI.onNavDestinationSelected(menuItem, this.val$navController);
                if (onNavDestinationSelected) {
                    final ViewParent parent = this.val$navigationView.getParent();
                    if (parent instanceof Openable) {
                        ((Openable)parent).close();
                    }
                    else {
                        final BottomSheetBehavior bottomSheetBehavior = NavigationUI.findBottomSheetBehavior((View)this.val$navigationView);
                        if (bottomSheetBehavior != null) {
                            bottomSheetBehavior.setState(5);
                        }
                    }
                }
                return onNavDestinationSelected;
            }
        });
        navController.addOnDestinationChangedListener((NavController.OnDestinationChangedListener)new NavController.OnDestinationChangedListener(new WeakReference((T)referent), navController) {
            final NavController val$navController;
            final WeakReference val$weakReference;
            
            @Override
            public void onDestinationChanged(final NavController navController, final NavDestination navDestination, final Bundle bundle) {
                final NavigationView navigationView = (NavigationView)this.val$weakReference.get();
                if (navigationView == null) {
                    this.val$navController.removeOnDestinationChangedListener((NavController.OnDestinationChangedListener)this);
                    return;
                }
                final Menu menu = navigationView.getMenu();
                for (int i = 0; i < menu.size(); ++i) {
                    final MenuItem item = menu.getItem(i);
                    item.setChecked(NavigationUI.matchDestination(navDestination, item.getItemId()));
                }
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import android.os.Bundle;

public final class NavArgument
{
    private final Object mDefaultValue;
    private final boolean mDefaultValuePresent;
    private final boolean mIsNullable;
    private final NavType mType;
    
    NavArgument(final NavType<?> mType, final boolean mIsNullable, final Object mDefaultValue, final boolean mDefaultValuePresent) {
        if (!mType.isNullableAllowed() && mIsNullable) {
            final StringBuilder sb = new StringBuilder();
            sb.append(mType.getName());
            sb.append(" does not allow nullable values");
            throw new IllegalArgumentException(sb.toString());
        }
        if (!mIsNullable && mDefaultValuePresent && mDefaultValue == null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Argument with type ");
            sb2.append(mType.getName());
            sb2.append(" has null value but is not nullable.");
            throw new IllegalArgumentException(sb2.toString());
        }
        this.mType = mType;
        this.mIsNullable = mIsNullable;
        this.mDefaultValue = mDefaultValue;
        this.mDefaultValuePresent = mDefaultValuePresent;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final NavArgument navArgument = (NavArgument)o;
        if (this.mIsNullable != navArgument.mIsNullable) {
            return false;
        }
        if (this.mDefaultValuePresent != navArgument.mDefaultValuePresent) {
            return false;
        }
        if (!this.mType.equals(navArgument.mType)) {
            return false;
        }
        final Object mDefaultValue = this.mDefaultValue;
        if (mDefaultValue != null) {
            equals = mDefaultValue.equals(navArgument.mDefaultValue);
        }
        else if (navArgument.mDefaultValue != null) {
            equals = false;
        }
        return equals;
    }
    
    public Object getDefaultValue() {
        return this.mDefaultValue;
    }
    
    public NavType<?> getType() {
        return this.mType;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.mType.hashCode();
        final int mIsNullable = this.mIsNullable ? 1 : 0;
        final int mDefaultValuePresent = this.mDefaultValuePresent ? 1 : 0;
        final Object mDefaultValue = this.mDefaultValue;
        int hashCode2;
        if (mDefaultValue != null) {
            hashCode2 = mDefaultValue.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return ((hashCode * 31 + mIsNullable) * 31 + mDefaultValuePresent) * 31 + hashCode2;
    }
    
    public boolean isDefaultValuePresent() {
        return this.mDefaultValuePresent;
    }
    
    public boolean isNullable() {
        return this.mIsNullable;
    }
    
    void putDefaultValue(final String s, final Bundle bundle) {
        if (this.mDefaultValuePresent) {
            this.mType.put(bundle, s, this.mDefaultValue);
        }
    }
    
    boolean verify(final String s, final Bundle bundle) {
        if (!this.mIsNullable && bundle.containsKey(s) && bundle.get(s) == null) {
            return false;
        }
        try {
            this.mType.get(bundle, s);
            return true;
        }
        catch (final ClassCastException ex) {
            return false;
        }
    }
    
    public static final class Builder
    {
        private Object mDefaultValue;
        private boolean mDefaultValuePresent;
        private boolean mIsNullable;
        private NavType<?> mType;
        
        public Builder() {
            this.mIsNullable = false;
            this.mDefaultValuePresent = false;
        }
        
        public NavArgument build() {
            if (this.mType == null) {
                this.mType = NavType.inferFromValueType(this.mDefaultValue);
            }
            return new NavArgument(this.mType, this.mIsNullable, this.mDefaultValue, this.mDefaultValuePresent);
        }
        
        public Builder setDefaultValue(final Object mDefaultValue) {
            this.mDefaultValue = mDefaultValue;
            this.mDefaultValuePresent = true;
            return this;
        }
        
        public Builder setIsNullable(final boolean mIsNullable) {
            this.mIsNullable = mIsNullable;
            return this;
        }
        
        public Builder setType(final NavType<?> mType) {
            this.mType = mType;
            return this;
        }
    }
}

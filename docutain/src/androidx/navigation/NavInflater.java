// 
// Decompiled by Procyon v0.6.0
// 

package androidx.navigation;

import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.text.TextUtils;
import android.os.Bundle;
import java.io.IOException;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import android.content.res.Resources;
import org.xmlpull.v1.XmlPullParserException;
import android.content.Context;
import android.util.TypedValue;

public final class NavInflater
{
    public static final String APPLICATION_ID_PLACEHOLDER = "${applicationId}";
    private static final String TAG_ACTION = "action";
    private static final String TAG_ARGUMENT = "argument";
    private static final String TAG_DEEP_LINK = "deepLink";
    private static final String TAG_INCLUDE = "include";
    private static final ThreadLocal<TypedValue> sTmpValue;
    private Context mContext;
    private NavigatorProvider mNavigatorProvider;
    
    static {
        sTmpValue = new ThreadLocal<TypedValue>();
    }
    
    public NavInflater(final Context mContext, final NavigatorProvider mNavigatorProvider) {
        this.mContext = mContext;
        this.mNavigatorProvider = mNavigatorProvider;
    }
    
    private static NavType checkNavType(final TypedValue typedValue, NavType navType, final NavType navType2, final String str, final String str2) throws XmlPullParserException {
        if (navType != null && navType != navType2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Type is ");
            sb.append(str);
            sb.append(" but found ");
            sb.append(str2);
            sb.append(": ");
            sb.append(typedValue.data);
            throw new XmlPullParserException(sb.toString());
        }
        if (navType == null) {
            navType = navType2;
        }
        return navType;
    }
    
    private NavDestination inflate(final Resources resources, final XmlResourceParser xmlResourceParser, final AttributeSet set, final int n) throws XmlPullParserException, IOException {
        final NavGraph destination = this.mNavigatorProvider.getNavigator(xmlResourceParser.getName()).createDestination();
        destination.onInflate(this.mContext, set);
        final int n2 = xmlResourceParser.getDepth() + 1;
        while (true) {
            final int next = xmlResourceParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlResourceParser.getDepth();
            if (depth < n2 && next == 3) {
                break;
            }
            if (next != 2) {
                continue;
            }
            if (depth > n2) {
                continue;
            }
            final String name = xmlResourceParser.getName();
            if ("argument".equals(name)) {
                this.inflateArgumentForDestination(resources, destination, set, n);
            }
            else if ("deepLink".equals(name)) {
                this.inflateDeepLink(resources, destination, set);
            }
            else if ("action".equals(name)) {
                this.inflateAction(resources, destination, set, xmlResourceParser, n);
            }
            else if ("include".equals(name) && destination instanceof NavGraph) {
                final TypedArray obtainAttributes = resources.obtainAttributes(set, R.styleable.NavInclude);
                destination.addDestination(this.inflate(obtainAttributes.getResourceId(R.styleable.NavInclude_graph, 0)));
                obtainAttributes.recycle();
            }
            else {
                if (!(destination instanceof NavGraph)) {
                    continue;
                }
                destination.addDestination(this.inflate(resources, xmlResourceParser, set, n));
            }
        }
        return destination;
    }
    
    private void inflateAction(final Resources resources, final NavDestination navDestination, final AttributeSet set, final XmlResourceParser xmlResourceParser, final int n) throws IOException, XmlPullParserException {
        final TypedArray obtainAttributes = resources.obtainAttributes(set, androidx.navigation.common.R.styleable.NavAction);
        final int resourceId = obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_android_id, 0);
        final NavAction navAction = new NavAction(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_destination, 0));
        final NavOptions.Builder builder = new NavOptions.Builder();
        builder.setLaunchSingleTop(obtainAttributes.getBoolean(androidx.navigation.common.R.styleable.NavAction_launchSingleTop, false));
        builder.setPopUpTo(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_popUpTo, -1), obtainAttributes.getBoolean(androidx.navigation.common.R.styleable.NavAction_popUpToInclusive, false));
        builder.setEnterAnim(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_enterAnim, -1));
        builder.setExitAnim(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_exitAnim, -1));
        builder.setPopEnterAnim(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_popEnterAnim, -1));
        builder.setPopExitAnim(obtainAttributes.getResourceId(androidx.navigation.common.R.styleable.NavAction_popExitAnim, -1));
        navAction.setNavOptions(builder.build());
        final Bundle defaultArguments = new Bundle();
        final int n2 = xmlResourceParser.getDepth() + 1;
        while (true) {
            final int next = xmlResourceParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlResourceParser.getDepth();
            if (depth < n2 && next == 3) {
                break;
            }
            if (next != 2) {
                continue;
            }
            if (depth > n2) {
                continue;
            }
            if (!"argument".equals(xmlResourceParser.getName())) {
                continue;
            }
            this.inflateArgumentForBundle(resources, defaultArguments, set, n);
        }
        if (!defaultArguments.isEmpty()) {
            navAction.setDefaultArguments(defaultArguments);
        }
        navDestination.putAction(resourceId, navAction);
        obtainAttributes.recycle();
    }
    
    private NavArgument inflateArgument(final TypedArray typedArray, final Resources resources, int type) throws XmlPullParserException {
        final NavArgument.Builder builder = new NavArgument.Builder();
        final int navArgument_nullable = androidx.navigation.common.R.styleable.NavArgument_nullable;
        boolean b = false;
        builder.setIsNullable(typedArray.getBoolean(navArgument_nullable, false));
        final ThreadLocal<TypedValue> sTmpValue = NavInflater.sTmpValue;
        TypedValue value;
        if ((value = sTmpValue.get()) == null) {
            value = new TypedValue();
            sTmpValue.set(value);
        }
        final String string = typedArray.getString(androidx.navigation.common.R.styleable.NavArgument_argType);
        Object defaultValue = null;
        NavType<?> fromArgType;
        if (string != null) {
            fromArgType = NavType.fromArgType(string, resources.getResourcePackageName(type));
        }
        else {
            fromArgType = null;
        }
        Object type2 = fromArgType;
        if (typedArray.getValue(androidx.navigation.common.R.styleable.NavArgument_android_defaultValue, value)) {
            if (fromArgType == NavType.ReferenceType) {
                if (value.resourceId != 0) {
                    defaultValue = value.resourceId;
                    type2 = fromArgType;
                }
                else {
                    if (value.type != 16 || value.data != 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unsupported value '");
                        sb.append((Object)value.string);
                        sb.append("' for ");
                        sb.append(fromArgType.getName());
                        sb.append(". Must be a reference to a resource.");
                        throw new XmlPullParserException(sb.toString());
                    }
                    defaultValue = 0;
                    type2 = fromArgType;
                }
            }
            else if (value.resourceId != 0) {
                if (fromArgType != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unsupported value '");
                    sb2.append((Object)value.string);
                    sb2.append("' for ");
                    sb2.append(fromArgType.getName());
                    sb2.append(". You must use a \"");
                    sb2.append(NavType.ReferenceType.getName());
                    sb2.append("\" type to reference other resources.");
                    throw new XmlPullParserException(sb2.toString());
                }
                type2 = NavType.ReferenceType;
                defaultValue = value.resourceId;
            }
            else if (fromArgType == NavType.StringType) {
                defaultValue = typedArray.getString(androidx.navigation.common.R.styleable.NavArgument_android_defaultValue);
                type2 = fromArgType;
            }
            else {
                type = value.type;
                if (type != 3) {
                    if (type != 4) {
                        if (type != 5) {
                            if (type != 18) {
                                if (value.type < 16 || value.type > 31) {
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("unsupported argument type ");
                                    sb3.append(value.type);
                                    throw new XmlPullParserException(sb3.toString());
                                }
                                if (fromArgType == NavType.FloatType) {
                                    type2 = checkNavType(value, fromArgType, NavType.FloatType, string, "float");
                                    defaultValue = value.data;
                                }
                                else {
                                    type2 = checkNavType(value, fromArgType, NavType.IntType, string, "integer");
                                    defaultValue = value.data;
                                }
                            }
                            else {
                                type2 = checkNavType(value, fromArgType, NavType.BoolType, string, "boolean");
                                if (value.data != 0) {
                                    b = true;
                                }
                                defaultValue = b;
                            }
                        }
                        else {
                            type2 = checkNavType(value, fromArgType, NavType.IntType, string, "dimension");
                            defaultValue = (int)value.getDimension(resources.getDisplayMetrics());
                        }
                    }
                    else {
                        type2 = checkNavType(value, fromArgType, NavType.FloatType, string, "float");
                        defaultValue = value.getFloat();
                    }
                }
                else {
                    final String string2 = value.string.toString();
                    if ((type2 = fromArgType) == null) {
                        type2 = NavType.inferFromValue(string2);
                    }
                    defaultValue = ((NavType<Object>)type2).parseValue(string2);
                }
            }
        }
        if (defaultValue != null) {
            builder.setDefaultValue(defaultValue);
        }
        if (type2 != null) {
            builder.setType((NavType<?>)type2);
        }
        return builder.build();
    }
    
    private void inflateArgumentForBundle(final Resources resources, final Bundle bundle, final AttributeSet set, final int n) throws XmlPullParserException {
        final TypedArray obtainAttributes = resources.obtainAttributes(set, androidx.navigation.common.R.styleable.NavArgument);
        final String string = obtainAttributes.getString(androidx.navigation.common.R.styleable.NavArgument_android_name);
        if (string != null) {
            final NavArgument inflateArgument = this.inflateArgument(obtainAttributes, resources, n);
            if (inflateArgument.isDefaultValuePresent()) {
                inflateArgument.putDefaultValue(string, bundle);
            }
            obtainAttributes.recycle();
            return;
        }
        throw new XmlPullParserException("Arguments must have a name");
    }
    
    private void inflateArgumentForDestination(final Resources resources, final NavDestination navDestination, final AttributeSet set, final int n) throws XmlPullParserException {
        final TypedArray obtainAttributes = resources.obtainAttributes(set, androidx.navigation.common.R.styleable.NavArgument);
        final String string = obtainAttributes.getString(androidx.navigation.common.R.styleable.NavArgument_android_name);
        if (string != null) {
            navDestination.addArgument(string, this.inflateArgument(obtainAttributes, resources, n));
            obtainAttributes.recycle();
            return;
        }
        throw new XmlPullParserException("Arguments must have a name");
    }
    
    private void inflateDeepLink(final Resources resources, final NavDestination navDestination, final AttributeSet set) throws XmlPullParserException {
        final TypedArray obtainAttributes = resources.obtainAttributes(set, androidx.navigation.common.R.styleable.NavDeepLink);
        final String string = obtainAttributes.getString(androidx.navigation.common.R.styleable.NavDeepLink_uri);
        final String string2 = obtainAttributes.getString(androidx.navigation.common.R.styleable.NavDeepLink_action);
        final String string3 = obtainAttributes.getString(androidx.navigation.common.R.styleable.NavDeepLink_mimeType);
        if (TextUtils.isEmpty((CharSequence)string) && TextUtils.isEmpty((CharSequence)string2) && TextUtils.isEmpty((CharSequence)string3)) {
            throw new XmlPullParserException("Every <deepLink> must include at least one of app:uri, app:action, or app:mimeType");
        }
        final NavDeepLink.Builder builder = new NavDeepLink.Builder();
        if (string != null) {
            builder.setUriPattern(string.replace("${applicationId}", this.mContext.getPackageName()));
        }
        if (!TextUtils.isEmpty((CharSequence)string2)) {
            builder.setAction(string2.replace("${applicationId}", this.mContext.getPackageName()));
        }
        if (string3 != null) {
            builder.setMimeType(string3.replace("${applicationId}", this.mContext.getPackageName()));
        }
        navDestination.addDeepLink(builder.build());
        obtainAttributes.recycle();
    }
    
    public NavGraph inflate(final int n) {
        final Resources resources = this.mContext.getResources();
        final XmlResourceParser xml = resources.getXml(n);
        final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
        try {
            int next;
            do {
                next = xml.next();
            } while (next != 2 && next != 1);
            if (next != 2) {
                throw new XmlPullParserException("No start tag found");
            }
            final String name = xml.getName();
            final NavDestination inflate = this.inflate(resources, xml, attributeSet, n);
            if (inflate instanceof NavGraph) {
                final NavGraph navGraph = (NavGraph)inflate;
                xml.close();
                return navGraph;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Root element <");
            sb.append(name);
            sb.append("> did not inflate into a NavGraph");
            throw new IllegalArgumentException(sb.toString());
        }
        catch (final Exception cause) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Exception inflating ");
            sb2.append(resources.getResourceName(n));
            sb2.append(" line ");
            sb2.append(xml.getLineNumber());
            throw new RuntimeException(sb2.toString(), cause);
        }
        xml.close();
    }
}

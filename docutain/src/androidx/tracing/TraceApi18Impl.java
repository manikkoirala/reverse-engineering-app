// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import android.os.Trace;

final class TraceApi18Impl
{
    private TraceApi18Impl() {
    }
    
    public static void beginSection(final String s) {
        Trace.beginSection(s);
    }
    
    public static void endSection() {
        Trace.endSection();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import android.os.Trace;

final class TraceApi29Impl
{
    private TraceApi29Impl() {
    }
    
    public static void beginAsyncSection(final String s, final int n) {
        Trace.beginAsyncSection(s, n);
    }
    
    public static void endAsyncSection(final String s, final int n) {
        Trace.endAsyncSection(s, n);
    }
    
    public static boolean isEnabled() {
        return Trace.isEnabled();
    }
    
    public static void setCounter(final String s, final int n) {
        Trace.setCounter(s, (long)n);
    }
}

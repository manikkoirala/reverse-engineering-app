// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class Trace
{
    static final String TAG = "Trace";
    private static Method sAsyncTraceBeginMethod;
    private static Method sAsyncTraceEndMethod;
    private static boolean sHasAppTracingEnabled;
    private static Method sIsTagEnabledMethod;
    private static Method sTraceCounterMethod;
    private static long sTraceTagApp;
    
    private Trace() {
    }
    
    public static void beginAsyncSection(final String s, final int n) {
        if (Build$VERSION.SDK_INT >= 29) {
            TraceApi29Impl.beginAsyncSection(s, n);
        }
        else {
            beginAsyncSectionFallback(s, n);
        }
    }
    
    private static void beginAsyncSectionFallback(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                if (Trace.sAsyncTraceBeginMethod == null) {
                    Trace.sAsyncTraceBeginMethod = android.os.Trace.class.getMethod("asyncTraceBegin", Long.TYPE, String.class, Integer.TYPE);
                }
                Trace.sAsyncTraceBeginMethod.invoke(null, Trace.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                handleException("asyncTraceBegin", ex);
            }
        }
    }
    
    public static void beginSection(final String s) {
        if (Build$VERSION.SDK_INT >= 18) {
            TraceApi18Impl.beginSection(s);
        }
    }
    
    public static void endAsyncSection(final String s, final int n) {
        if (Build$VERSION.SDK_INT >= 29) {
            TraceApi29Impl.endAsyncSection(s, n);
        }
        else {
            endAsyncSectionFallback(s, n);
        }
    }
    
    private static void endAsyncSectionFallback(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                if (Trace.sAsyncTraceEndMethod == null) {
                    Trace.sAsyncTraceEndMethod = android.os.Trace.class.getMethod("asyncTraceEnd", Long.TYPE, String.class, Integer.TYPE);
                }
                Trace.sAsyncTraceEndMethod.invoke(null, Trace.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                handleException("asyncTraceEnd", ex);
            }
        }
    }
    
    public static void endSection() {
        if (Build$VERSION.SDK_INT >= 18) {
            TraceApi18Impl.endSection();
        }
    }
    
    public static void forceEnableAppTracing() {
        if (Build$VERSION.SDK_INT >= 18 && Build$VERSION.SDK_INT < 31) {
            try {
                if (!Trace.sHasAppTracingEnabled) {
                    Trace.sHasAppTracingEnabled = true;
                    android.os.Trace.class.getMethod("setAppTracingAllowed", Boolean.TYPE).invoke(null, true);
                }
            }
            catch (final Exception ex) {
                handleException("setAppTracingAllowed", ex);
            }
        }
    }
    
    private static void handleException(final String str, final Exception ex) {
        if (!(ex instanceof InvocationTargetException)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to call ");
            sb.append(str);
            sb.append(" via reflection");
            Log.v("Trace", sb.toString(), (Throwable)ex);
            return;
        }
        final Throwable cause = ex.getCause();
        if (cause instanceof RuntimeException) {
            throw (RuntimeException)cause;
        }
        throw new RuntimeException(cause);
    }
    
    public static boolean isEnabled() {
        if (Build$VERSION.SDK_INT >= 29) {
            return TraceApi29Impl.isEnabled();
        }
        return isEnabledFallback();
    }
    
    private static boolean isEnabledFallback() {
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                if (Trace.sIsTagEnabledMethod == null) {
                    Trace.sTraceTagApp = android.os.Trace.class.getField("TRACE_TAG_APP").getLong(null);
                    Trace.sIsTagEnabledMethod = android.os.Trace.class.getMethod("isTagEnabled", Long.TYPE);
                }
                return (boolean)Trace.sIsTagEnabledMethod.invoke(null, Trace.sTraceTagApp);
            }
            catch (final Exception ex) {
                handleException("isTagEnabled", ex);
            }
        }
        return false;
    }
    
    public static void setCounter(final String s, final int n) {
        if (Build$VERSION.SDK_INT >= 29) {
            TraceApi29Impl.setCounter(s, n);
        }
        else {
            setCounterFallback(s, n);
        }
    }
    
    private static void setCounterFallback(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                if (Trace.sTraceCounterMethod == null) {
                    Trace.sTraceCounterMethod = android.os.Trace.class.getMethod("traceCounter", Long.TYPE, String.class, Integer.TYPE);
                }
                Trace.sTraceCounterMethod.invoke(null, Trace.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                handleException("traceCounter", ex);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.dynamicanimation.animation;

public final class FlingAnimation extends DynamicAnimation<FlingAnimation>
{
    private final DragForce mFlingForce;
    
    public FlingAnimation(final FloatValueHolder floatValueHolder) {
        super(floatValueHolder);
        (this.mFlingForce = new DragForce()).setValueThreshold(this.getValueThreshold());
    }
    
    public <K> FlingAnimation(final K k, final FloatPropertyCompat<K> floatPropertyCompat) {
        super(k, floatPropertyCompat);
        (this.mFlingForce = new DragForce()).setValueThreshold(this.getValueThreshold());
    }
    
    @Override
    float getAcceleration(final float n, final float n2) {
        return this.mFlingForce.getAcceleration(n, n2);
    }
    
    public float getFriction() {
        return this.mFlingForce.getFrictionScalar();
    }
    
    @Override
    boolean isAtEquilibrium(final float n, final float n2) {
        return n >= this.mMaxValue || n <= this.mMinValue || this.mFlingForce.isAtEquilibrium(n, n2);
    }
    
    public FlingAnimation setFriction(final float frictionScalar) {
        if (frictionScalar > 0.0f) {
            this.mFlingForce.setFrictionScalar(frictionScalar);
            return this;
        }
        throw new IllegalArgumentException("Friction must be positive");
    }
    
    @Override
    public FlingAnimation setMaxValue(final float maxValue) {
        super.setMaxValue(maxValue);
        return this;
    }
    
    @Override
    public FlingAnimation setMinValue(final float minValue) {
        super.setMinValue(minValue);
        return this;
    }
    
    @Override
    public FlingAnimation setStartVelocity(final float startVelocity) {
        super.setStartVelocity(startVelocity);
        return this;
    }
    
    @Override
    void setValueThreshold(final float valueThreshold) {
        this.mFlingForce.setValueThreshold(valueThreshold);
    }
    
    @Override
    boolean updateValueAndVelocity(final long n) {
        final MassState updateValueAndVelocity = this.mFlingForce.updateValueAndVelocity(this.mValue, this.mVelocity, n);
        this.mValue = updateValueAndVelocity.mValue;
        this.mVelocity = updateValueAndVelocity.mVelocity;
        if (this.mValue < this.mMinValue) {
            this.mValue = this.mMinValue;
            return true;
        }
        if (this.mValue > this.mMaxValue) {
            this.mValue = this.mMaxValue;
            return true;
        }
        return this.isAtEquilibrium(this.mValue, this.mVelocity);
    }
    
    static final class DragForce implements Force
    {
        private static final float DEFAULT_FRICTION = -4.2f;
        private static final float VELOCITY_THRESHOLD_MULTIPLIER = 62.5f;
        private float mFriction;
        private final MassState mMassState;
        private float mVelocityThreshold;
        
        DragForce() {
            this.mFriction = -4.2f;
            this.mMassState = new MassState();
        }
        
        @Override
        public float getAcceleration(final float n, final float n2) {
            return n2 * this.mFriction;
        }
        
        float getFrictionScalar() {
            return this.mFriction / -4.2f;
        }
        
        @Override
        public boolean isAtEquilibrium(final float n, final float a) {
            return Math.abs(a) < this.mVelocityThreshold;
        }
        
        void setFrictionScalar(final float n) {
            this.mFriction = n * -4.2f;
        }
        
        void setValueThreshold(final float n) {
            this.mVelocityThreshold = n * 62.5f;
        }
        
        MassState updateValueAndVelocity(final float n, final float n2, final long n3) {
            final MassState mMassState = this.mMassState;
            final double n4 = n2;
            final float n5 = (float)n3;
            mMassState.mVelocity = (float)(n4 * Math.exp(n5 / 1000.0f * this.mFriction));
            final MassState mMassState2 = this.mMassState;
            final float mFriction = this.mFriction;
            mMassState2.mValue = (float)(n - n2 / mFriction + n2 / mFriction * Math.exp(mFriction * n5 / 1000.0f));
            if (this.isAtEquilibrium(this.mMassState.mValue, this.mMassState.mVelocity)) {
                this.mMassState.mVelocity = 0.0f;
            }
            return this.mMassState;
        }
    }
}

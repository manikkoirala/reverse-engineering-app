// 
// Decompiled by Procyon v0.6.0
// 

package androidx.dynamicanimation.animation;

import android.util.FloatProperty;

public abstract class FloatPropertyCompat<T>
{
    final String mPropertyName;
    
    public FloatPropertyCompat(final String mPropertyName) {
        this.mPropertyName = mPropertyName;
    }
    
    public static <T> FloatPropertyCompat<T> createFloatPropertyCompat(final FloatProperty<T> floatProperty) {
        return new FloatPropertyCompat<T>(floatProperty.getName(), floatProperty) {
            final FloatProperty val$property;
            
            @Override
            public float getValue(final T t) {
                return (float)this.val$property.get((Object)t);
            }
            
            @Override
            public void setValue(final T t, final float n) {
                this.val$property.setValue((Object)t, n);
            }
        };
    }
    
    public abstract float getValue(final T p0);
    
    public abstract void setValue(final T p0, final float p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.CollectionsKt;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import androidx.collection.internal.LruHashMap;
import androidx.collection.internal.Lock;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010%\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0016\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00022\u00020\u0002B\u000f\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0017\u0010\u0011\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0012\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010\u0013J\u0006\u0010\u0007\u001a\u00020\u0005J/\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00028\u00012\b\u0010\u0019\u001a\u0004\u0018\u00018\u0001H\u0014¢\u0006\u0002\u0010\u001aJ\u0006\u0010\u001b\u001a\u00020\u0015J\u0006\u0010\b\u001a\u00020\u0005J\u0018\u0010\u001c\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0012\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010\u0013J\u0006\u0010\t\u001a\u00020\u0005J\u0006\u0010\u0004\u001a\u00020\u0005J\u0006\u0010\u000e\u001a\u00020\u0005J\u001d\u0010\u001d\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00028\u0001¢\u0006\u0002\u0010\u001fJ\u0006\u0010\u000f\u001a\u00020\u0005J\u0015\u0010 \u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0012\u001a\u00028\u0000¢\u0006\u0002\u0010\u0013J\u0012\u0010!\u001a\u00020\u00152\b\b\u0001\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\"\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00028\u0001H\u0002¢\u0006\u0002\u0010#J\u0006\u0010\u0010\u001a\u00020\u0005J\u001d\u0010$\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00028\u0001H\u0014¢\u0006\u0002\u0010#J\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010&J\b\u0010'\u001a\u00020(H\u0016J\u0010\u0010)\u001a\u00020\u00152\u0006\u0010\u0004\u001a\u00020\u0005H\u0016R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006*" }, d2 = { "Landroidx/collection/LruCache;", "K", "", "V", "maxSize", "", "(I)V", "createCount", "evictionCount", "hitCount", "lock", "Landroidx/collection/internal/Lock;", "map", "Landroidx/collection/internal/LruHashMap;", "missCount", "putCount", "size", "create", "key", "(Ljava/lang/Object;)Ljava/lang/Object;", "entryRemoved", "", "evicted", "", "oldValue", "newValue", "(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V", "evictAll", "get", "put", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "remove", "resize", "safeSizeOf", "(Ljava/lang/Object;Ljava/lang/Object;)I", "sizeOf", "snapshot", "", "toString", "", "trimToSize", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class LruCache<K, V>
{
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final Lock lock;
    private final LruHashMap<K, V> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;
    
    public LruCache(int maxSize) {
        this.maxSize = maxSize;
        if (maxSize > 0) {
            maxSize = 1;
        }
        else {
            maxSize = 0;
        }
        if (maxSize != 0) {
            this.map = new LruHashMap<K, V>(0, 0.75f);
            this.lock = new Lock();
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0".toString());
    }
    
    private final int safeSizeOf(final K obj, final V obj2) {
        final int size = this.sizeOf(obj, obj2);
        if (size >= 0) {
            return size;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Negative size: ");
        sb.append(obj);
        sb.append('=');
        sb.append(obj2);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    protected V create(final K k) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        return null;
    }
    
    public final int createCount() {
        synchronized (this.lock) {
            return this.createCount;
        }
    }
    
    protected void entryRemoved(final boolean b, final K k, final V v, final V v2) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        Intrinsics.checkNotNullParameter((Object)v, "oldValue");
    }
    
    public final void evictAll() {
        this.trimToSize(-1);
    }
    
    public final int evictionCount() {
        synchronized (this.lock) {
            return this.evictionCount;
        }
    }
    
    public final V get(final K k) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        Object o = this.lock;
        synchronized (o) {
            final V value = this.map.get(k);
            if (value != null) {
                ++this.hitCount;
                return value;
            }
            ++this.missCount;
            monitorexit(o);
            final V create = this.create(k);
            if (create == null) {
                return null;
            }
            synchronized (this.lock) {
                ++this.createCount;
                o = this.map.put(k, create);
                if (o != null) {
                    this.map.put(k, (V)o);
                }
                else {
                    this.size += this.safeSizeOf(k, create);
                    final Unit instance = Unit.INSTANCE;
                }
                monitorexit(this.lock);
                if (o != null) {
                    this.entryRemoved(false, k, create, (V)o);
                }
                else {
                    this.trimToSize(this.maxSize);
                }
                return (V)o;
            }
        }
    }
    
    public final int hitCount() {
        synchronized (this.lock) {
            return this.hitCount;
        }
    }
    
    public final int maxSize() {
        synchronized (this.lock) {
            return this.maxSize;
        }
    }
    
    public final int missCount() {
        synchronized (this.lock) {
            return this.missCount;
        }
    }
    
    public final V put(final K k, final V v) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        Intrinsics.checkNotNullParameter((Object)v, "value");
        synchronized (this.lock) {
            ++this.putCount;
            this.size += this.safeSizeOf(k, v);
            final V put = this.map.put(k, v);
            if (put != null) {
                this.size -= this.safeSizeOf(k, put);
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(this.lock);
            if (put != null) {
                this.entryRemoved(false, k, put, v);
            }
            this.trimToSize(this.maxSize);
            return put;
        }
    }
    
    public final int putCount() {
        synchronized (this.lock) {
            return this.putCount;
        }
    }
    
    public final V remove(final K k) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        synchronized (this.lock) {
            final V remove = this.map.remove(k);
            if (remove != null) {
                this.size -= this.safeSizeOf(k, remove);
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(this.lock);
            if (remove != null) {
                this.entryRemoved(false, k, remove, null);
            }
            return remove;
        }
    }
    
    public void resize(final int maxSize) {
        if (maxSize > 0) {
            synchronized (this.lock) {
                this.maxSize = maxSize;
                final Unit instance = Unit.INSTANCE;
                monitorexit(this.lock);
                this.trimToSize(maxSize);
                return;
            }
        }
        throw new IllegalArgumentException("maxSize <= 0".toString());
    }
    
    public final int size() {
        synchronized (this.lock) {
            return this.size;
        }
    }
    
    protected int sizeOf(final K k, final V v) {
        Intrinsics.checkNotNullParameter((Object)k, "key");
        Intrinsics.checkNotNullParameter((Object)v, "value");
        return 1;
    }
    
    public final Map<K, V> snapshot() {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        synchronized (this.lock) {
            for (final Map.Entry<Object, V> entry : this.map.getEntries()) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(this.lock);
            return linkedHashMap;
        }
    }
    
    @Override
    public String toString() {
        synchronized (this.lock) {
            final int hitCount = this.hitCount;
            final int n = this.missCount + hitCount;
            int i;
            if (n != 0) {
                i = hitCount * 100 / n;
            }
            else {
                i = 0;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("LruCache[maxSize=");
            sb.append(this.maxSize);
            sb.append(",hits=");
            sb.append(this.hitCount);
            sb.append(",misses=");
            sb.append(this.missCount);
            sb.append(",hitRate=");
            sb.append(i);
            sb.append("%]");
            return sb.toString();
        }
    }
    
    public void trimToSize(final int n) {
        while (true) {
            synchronized (this.lock) {
                if (this.size < 0 || (this.map.isEmpty() && this.size != 0)) {
                    throw new IllegalStateException("LruCache.sizeOf() is reporting inconsistent results!".toString());
                }
                if (this.size <= n || this.map.isEmpty()) {
                    return;
                }
                final Map.Entry entry = (Map.Entry)CollectionsKt.firstOrNull((Iterable)this.map.getEntries());
                if (entry == null) {
                    return;
                }
                final Object key = entry.getKey();
                final Object value = entry.getValue();
                this.map.remove((K)key);
                this.size -= this.safeSizeOf((K)key, (V)value);
                ++this.evictionCount;
                monitorexit(this.lock);
                this.entryRemoved(true, (K)key, (V)value, null);
            }
        }
    }
}

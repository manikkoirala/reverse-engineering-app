// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.lang.reflect.Array;

class ArraySetJvmUtil
{
    private ArraySetJvmUtil() {
    }
    
    static <T> T[] resizeForToArray(final T[] array, final int length) {
        if (array.length < length) {
            return (T[])Array.newInstance(array.getClass().getComponentType(), length);
        }
        if (array.length > length) {
            array[length] = null;
        }
        return array;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.collections.ArraysKt;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import kotlin.jvm.internal.ArrayIteratorKt;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableSet;
import kotlin.jvm.internal.markers.KMutableCollection;
import java.util.Set;
import java.util.Collection;

@Metadata(d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001f\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\r\n\u0002\u0010)\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001?B\u0019\b\u0016\u0012\u0010\u0010\u0004\u001a\f\u0012\u0006\b\u0001\u0012\u00028\u0000\u0018\u00010\u0000¢\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007B\u0019\b\u0016\u0012\u0010\u0010\b\u001a\f\u0012\u0006\b\u0001\u0012\u00028\u0000\u0018\u00010\t¢\u0006\u0002\u0010\nB\u0011\b\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\"J\u0016\u0010#\u001a\u00020$2\u000e\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0000J\u0016\u0010#\u001a\u00020 2\f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006H\u0016J\b\u0010&\u001a\u00020$H\u0016J\u0016\u0010'\u001a\u00020 2\u0006\u0010!\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\"J\u0016\u0010(\u001a\u00020 2\f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006H\u0016J\u000e\u0010)\u001a\u00020$2\u0006\u0010*\u001a\u00020\fJ\u0013\u0010+\u001a\u00020 2\b\u0010,\u001a\u0004\u0018\u00010\u0012H\u0096\u0002J\b\u0010-\u001a\u00020\fH\u0016J\u0010\u0010.\u001a\u00020\f2\b\u0010/\u001a\u0004\u0018\u00010\u0012J\b\u00100\u001a\u00020 H\u0016J\u000f\u00101\u001a\b\u0012\u0004\u0012\u00028\u000002H\u0096\u0002J\u0015\u00103\u001a\u00020 2\u0006\u0010!\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\"J\u0016\u00104\u001a\u00020 2\u000e\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0000J\u0016\u00104\u001a\u00020 2\f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006H\u0016J\u0013\u00105\u001a\u00028\u00002\u0006\u00106\u001a\u00020\f¢\u0006\u0002\u00107J\u0016\u00108\u001a\u00020 2\f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006H\u0016J\u0013\u00109\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\t¢\u0006\u0002\u0010\u0014J%\u00109\u001a\b\u0012\u0004\u0012\u0002H:0\t\"\u0004\b\u0001\u0010:2\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H:0\t¢\u0006\u0002\u0010;J\b\u0010<\u001a\u00020=H\u0016J\u0013\u0010>\u001a\u00028\u00002\u0006\u00106\u001a\u00020\f¢\u0006\u0002\u00107R\u001a\u0010\u000e\u001a\u00020\fX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\rR$\u0010\b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\tX\u0080\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\nR\u001a\u0010\u0017\u001a\u00020\u0018X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0010¨\u0006@" }, d2 = { "Landroidx/collection/ArraySet;", "E", "", "", "set", "(Landroidx/collection/ArraySet;)V", "", "(Ljava/util/Collection;)V", "array", "", "([Ljava/lang/Object;)V", "capacity", "", "(I)V", "_size", "get_size$collection", "()I", "set_size$collection", "", "getArray$collection", "()[Ljava/lang/Object;", "setArray$collection", "[Ljava/lang/Object;", "hashes", "", "getHashes$collection", "()[I", "setHashes$collection", "([I)V", "size", "getSize", "add", "", "element", "(Ljava/lang/Object;)Z", "addAll", "", "elements", "clear", "contains", "containsAll", "ensureCapacity", "minimumCapacity", "equals", "other", "hashCode", "indexOf", "key", "isEmpty", "iterator", "", "remove", "removeAll", "removeAt", "index", "(I)Ljava/lang/Object;", "retainAll", "toArray", "T", "([Ljava/lang/Object;)[Ljava/lang/Object;", "toString", "", "valueAt", "ElementIterator", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ArraySet<E> implements Collection<E>, Set<E>, KMutableCollection, KMutableSet
{
    private int _size;
    private Object[] array;
    private int[] hashes;
    
    public ArraySet() {
        this(0, 1, null);
    }
    
    public ArraySet(final int n) {
        this.hashes = ContainerHelpersKt.EMPTY_INTS;
        this.array = ContainerHelpersKt.EMPTY_OBJECTS;
        if (n > 0) {
            ArraySetKt.allocArrays((ArraySet<Object>)this, n);
        }
    }
    
    public ArraySet(final ArraySet<? extends E> set) {
        this(0);
        if (set != null) {
            this.addAll(set);
        }
    }
    
    public ArraySet(final Collection<? extends E> collection) {
        this(0);
        if (collection != null) {
            this.addAll(collection);
        }
    }
    
    public ArraySet(final E[] array) {
        this(0);
        if (array != null) {
            final Iterator iterator = ArrayIteratorKt.iterator((Object[])array);
            while (iterator.hasNext()) {
                this.add((E)iterator.next());
            }
        }
    }
    
    @Override
    public boolean add(final E e) {
        final int get_size$collection = this.get_size$collection();
        boolean b = false;
        final int n = 0;
        int n2;
        int hashCode;
        if (e == null) {
            n2 = ArraySetKt.indexOfNull((ArraySet<Object>)this);
            hashCode = 0;
        }
        else {
            hashCode = e.hashCode();
            n2 = ArraySetKt.indexOf((ArraySet<Object>)this, e, hashCode);
        }
        if (n2 < 0) {
            final int n3 = ~n2;
            if (get_size$collection >= this.getHashes$collection().length) {
                int n4 = 4;
                if (get_size$collection >= 8) {
                    n4 = (get_size$collection >> 1) + get_size$collection;
                }
                else if (get_size$collection >= 4) {
                    n4 = 8;
                }
                final int[] hashes$collection = this.getHashes$collection();
                final Object[] array$collection = this.getArray$collection();
                ArraySetKt.allocArrays((ArraySet<Object>)this, n4);
                if (get_size$collection != this.get_size$collection()) {
                    throw new ConcurrentModificationException();
                }
                int n5 = n;
                if (this.getHashes$collection().length == 0) {
                    n5 = 1;
                }
                if ((n5 ^ 0x1) != 0x0) {
                    ArraysKt.copyInto$default(hashes$collection, this.getHashes$collection(), 0, 0, hashes$collection.length, 6, (Object)null);
                    ArraysKt.copyInto$default(array$collection, this.getArray$collection(), 0, 0, array$collection.length, 6, (Object)null);
                }
            }
            if (n3 < get_size$collection) {
                final int[] hashes$collection2 = this.getHashes$collection();
                final int[] hashes$collection3 = this.getHashes$collection();
                final int n6 = n3 + 1;
                ArraysKt.copyInto(hashes$collection2, hashes$collection3, n6, n3, get_size$collection);
                ArraysKt.copyInto(this.getArray$collection(), this.getArray$collection(), n6, n3, get_size$collection);
            }
            if (get_size$collection != this.get_size$collection() || n3 >= this.getHashes$collection().length) {
                throw new ConcurrentModificationException();
            }
            this.getHashes$collection()[n3] = hashCode;
            this.getArray$collection()[n3] = e;
            this.set_size$collection(this.get_size$collection() + 1);
            b = true;
        }
        return b;
    }
    
    public final void addAll(final ArraySet<? extends E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "array");
        final int get_size$collection = set.get_size$collection();
        this.ensureCapacity(this.get_size$collection() + get_size$collection);
        if (this.get_size$collection() == 0) {
            if (get_size$collection > 0) {
                ArraysKt.copyInto$default(set.getHashes$collection(), this.getHashes$collection(), 0, 0, get_size$collection, 6, (Object)null);
                ArraysKt.copyInto$default(set.getArray$collection(), this.getArray$collection(), 0, 0, get_size$collection, 6, (Object)null);
                if (this.get_size$collection() != 0) {
                    throw new ConcurrentModificationException();
                }
                this.set_size$collection(get_size$collection);
            }
        }
        else {
            for (int i = 0; i < get_size$collection; ++i) {
                this.add((E)set.valueAt(i));
            }
        }
    }
    
    @Override
    public boolean addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        this.ensureCapacity(this.get_size$collection() + collection.size());
        final Iterator iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= this.add(iterator.next());
        }
        return b;
    }
    
    @Override
    public void clear() {
        if (this.get_size$collection() != 0) {
            this.setHashes$collection(ContainerHelpersKt.EMPTY_INTS);
            this.setArray$collection(ContainerHelpersKt.EMPTY_OBJECTS);
            this.set_size$collection(0);
        }
        if (this.get_size$collection() == 0) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<?> iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!this.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public final void ensureCapacity(final int n) {
        final int get_size$collection = this.get_size$collection();
        if (this.getHashes$collection().length < n) {
            final int[] hashes$collection = this.getHashes$collection();
            final Object[] array$collection = this.getArray$collection();
            ArraySetKt.allocArrays((ArraySet<Object>)this, n);
            if (this.get_size$collection() > 0) {
                ArraysKt.copyInto$default(hashes$collection, this.getHashes$collection(), 0, 0, this.get_size$collection(), 6, (Object)null);
                ArraysKt.copyInto$default(array$collection, this.getArray$collection(), 0, 0, this.get_size$collection(), 6, (Object)null);
            }
        }
        if (this.get_size$collection() == get_size$collection) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        if (this == o) {
            return b;
        }
        while (true) {
            if (!(o instanceof Set)) {
                break Label_0091;
            }
            if (this.size() != ((Set)o).size()) {
                break Label_0091;
            }
            try {
                final int get_size$collection = this.get_size$collection();
                int n = 0;
                boolean b2;
                while (true) {
                    b2 = b;
                    if (n >= get_size$collection) {
                        break;
                    }
                    if (!((Set)o).contains(this.valueAt(n))) {
                        b2 = false;
                        break;
                    }
                    ++n;
                }
                return b2;
            }
            catch (final NullPointerException | ClassCastException ex) {
                continue;
            }
            break;
        }
    }
    
    public final Object[] getArray$collection() {
        return this.array;
    }
    
    public final int[] getHashes$collection() {
        return this.hashes;
    }
    
    public int getSize() {
        return this._size;
    }
    
    public final int get_size$collection() {
        return this._size;
    }
    
    @Override
    public int hashCode() {
        final int[] hashes$collection = this.getHashes$collection();
        final int get_size$collection = this.get_size$collection();
        int i = 0;
        int n = 0;
        while (i < get_size$collection) {
            n += hashes$collection[i];
            ++i;
        }
        return n;
    }
    
    public final int indexOf(final Object o) {
        int n;
        if (o == null) {
            n = ArraySetKt.indexOfNull((ArraySet<Object>)this);
        }
        else {
            n = ArraySetKt.indexOf((ArraySet<Object>)this, o, o.hashCode());
        }
        return n;
    }
    
    @Override
    public boolean isEmpty() {
        return this.get_size$collection() <= 0;
    }
    
    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }
    
    @Override
    public boolean remove(final Object o) {
        final int index = this.indexOf(o);
        boolean b;
        if (index >= 0) {
            this.removeAt(index);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public final boolean removeAll(final ArraySet<? extends E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "array");
        final int get_size$collection = set.get_size$collection();
        final int get_size$collection2 = this.get_size$collection();
        boolean b = false;
        for (int i = 0; i < get_size$collection; ++i) {
            this.remove(set.valueAt(i));
        }
        if (get_size$collection2 != this.get_size$collection()) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<?> iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= this.remove(iterator.next());
        }
        return b;
    }
    
    public final E removeAt(final int n) {
        final int get_size$collection = this.get_size$collection();
        final Object o = this.getArray$collection()[n];
        if (get_size$collection <= 1) {
            this.clear();
        }
        else {
            final int n2 = get_size$collection - 1;
            final int length = this.getHashes$collection().length;
            int n3 = 8;
            if (length > 8 && this.get_size$collection() < this.getHashes$collection().length / 3) {
                if (this.get_size$collection() > 8) {
                    n3 = this.get_size$collection() + (this.get_size$collection() >> 1);
                }
                final int[] hashes$collection = this.getHashes$collection();
                final Object[] array$collection = this.getArray$collection();
                ArraySetKt.allocArrays((ArraySet<Object>)this, n3);
                if (n > 0) {
                    ArraysKt.copyInto$default(hashes$collection, this.getHashes$collection(), 0, 0, n, 6, (Object)null);
                    ArraysKt.copyInto$default(array$collection, this.getArray$collection(), 0, 0, n, 6, (Object)null);
                }
                if (n < n2) {
                    final int[] hashes$collection2 = this.getHashes$collection();
                    final int n4 = n + 1;
                    final int n5 = n2 + 1;
                    ArraysKt.copyInto(hashes$collection, hashes$collection2, n, n4, n5);
                    ArraysKt.copyInto(array$collection, this.getArray$collection(), n, n4, n5);
                }
            }
            else {
                if (n < n2) {
                    final int[] hashes$collection3 = this.getHashes$collection();
                    final int[] hashes$collection4 = this.getHashes$collection();
                    final int n6 = n + 1;
                    final int n7 = n2 + 1;
                    ArraysKt.copyInto(hashes$collection3, hashes$collection4, n, n6, n7);
                    ArraysKt.copyInto(this.getArray$collection(), this.getArray$collection(), n, n6, n7);
                }
                this.getArray$collection()[n2] = null;
            }
            if (get_size$collection != this.get_size$collection()) {
                throw new ConcurrentModificationException();
            }
            this.set_size$collection(n2);
        }
        return (E)o;
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        int n = this.get_size$collection() - 1;
        boolean b = false;
        while (-1 < n) {
            if (!CollectionsKt.contains((Iterable)collection, this.getArray$collection()[n])) {
                this.removeAt(n);
                b = true;
            }
            --n;
        }
        return b;
    }
    
    public final void setArray$collection(final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "<set-?>");
        this.array = array;
    }
    
    public final void setHashes$collection(final int[] hashes) {
        Intrinsics.checkNotNullParameter((Object)hashes, "<set-?>");
        this.hashes = hashes;
    }
    
    public final void set_size$collection(final int size) {
        this._size = size;
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public final Object[] toArray() {
        return ArraysKt.copyOfRange(this.array, 0, this._size);
    }
    
    @Override
    public final <T> T[] toArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "array");
        final T[] resizeForToArray = ArraySetJvmUtil.resizeForToArray(array, this._size);
        ArraysKt.copyInto(this.array, (Object[])resizeForToArray, 0, 0, this._size);
        Intrinsics.checkNotNullExpressionValue((Object)resizeForToArray, "result");
        return resizeForToArray;
    }
    
    @Override
    public String toString() {
        String string;
        if (this.isEmpty()) {
            string = "{}";
        }
        else {
            final StringBuilder sb = new StringBuilder(this.get_size$collection() * 14);
            sb.append('{');
            for (int i = 0; i < this.get_size$collection(); ++i) {
                if (i > 0) {
                    sb.append(", ");
                }
                final E value = this.valueAt(i);
                if (value != this) {
                    sb.append(value);
                }
                else {
                    sb.append("(this Set)");
                }
            }
            sb.append('}');
            string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
        }
        return string;
    }
    
    public final E valueAt(final int n) {
        return (E)this.getArray$collection()[n];
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00028\u00002\u0006\u0010\u0004\u001a\u00020\u0005H\u0014¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u0005H\u0014¨\u0006\t" }, d2 = { "Landroidx/collection/ArraySet$ElementIterator;", "Landroidx/collection/IndexBasedArrayIterator;", "(Landroidx/collection/ArraySet;)V", "elementAt", "index", "", "(I)Ljava/lang/Object;", "removeAt", "", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class ElementIterator extends IndexBasedArrayIterator<E>
    {
        final ArraySet<E> this$0;
        
        public ElementIterator(final ArraySet this$0) {
            this.this$0 = this$0;
            super(this$0.get_size$collection());
        }
        
        @Override
        protected E elementAt(final int n) {
            return this.this$0.valueAt(n);
        }
        
        @Override
        protected void removeAt(final int n) {
            this.this$0.removeAt(n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.util.NoSuchElementException;
import androidx.collection.internal.ContainerHelpersKt;
import java.lang.reflect.Array;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;
import java.util.Map;

public class ArrayMap<K, V> extends SimpleArrayMap<K, V> implements Map<K, V>
{
    EntrySet mEntrySet;
    KeySet mKeySet;
    ValueCollection mValues;
    
    public ArrayMap() {
    }
    
    public ArrayMap(final int n) {
        super(n);
    }
    
    public ArrayMap(final SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
    
    static <T> boolean equalsSetHelper(final Set<T> set, final Object o) {
        boolean b = true;
        if (set == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        final Set set2 = (Set)o;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                b = false;
            }
            return b;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public boolean containsAll(final Collection<?> collection) {
        final Iterator<?> iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!this.containsKey(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return super.containsKey((K)o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return super.containsValue((V)o);
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        EntrySet mEntrySet;
        if ((mEntrySet = this.mEntrySet) == null) {
            mEntrySet = new EntrySet();
            this.mEntrySet = mEntrySet;
        }
        return mEntrySet;
    }
    
    @Override
    public V get(final Object o) {
        return super.get((K)o);
    }
    
    @Override
    public Set<K> keySet() {
        KeySet mKeySet;
        if ((mKeySet = this.mKeySet) == null) {
            mKeySet = new KeySet();
            this.mKeySet = mKeySet;
        }
        return mKeySet;
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        this.ensureCapacity(this.size() + map.size());
        for (final Entry<K, V> entry : map.entrySet()) {
            this.put(entry.getKey(), (V)entry.getValue());
        }
    }
    
    @Override
    public V remove(final Object o) {
        return super.remove((K)o);
    }
    
    public boolean removeAll(final Collection<?> collection) {
        final int size = this.size();
        final Iterator<?> iterator = collection.iterator();
        while (iterator.hasNext()) {
            this.remove(iterator.next());
        }
        return size != this.size();
    }
    
    public boolean retainAll(final Collection<?> collection) {
        final int size = this.size();
        int i = this.size();
        boolean b = true;
        --i;
        while (i >= 0) {
            if (!collection.contains(this.keyAt(i))) {
                this.removeAt(i);
            }
            --i;
        }
        if (size == this.size()) {
            b = false;
        }
        return b;
    }
    
    @Override
    public Collection<V> values() {
        ValueCollection mValues;
        if ((mValues = this.mValues) == null) {
            mValues = new ValueCollection();
            this.mValues = mValues;
        }
        return mValues;
    }
    
    final class EntrySet extends AbstractSet<Entry<K, V>>
    {
        final ArrayMap this$0;
        
        EntrySet(final ArrayMap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return this.this$0.new MapIterator();
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
    }
    
    final class KeyIterator extends IndexBasedArrayIterator<K>
    {
        final ArrayMap this$0;
        
        KeyIterator(final ArrayMap this$0) {
            this.this$0 = this$0;
            super(this$0.size());
        }
        
        @Override
        protected K elementAt(final int n) {
            return this.this$0.keyAt(n);
        }
        
        @Override
        protected void removeAt(final int n) {
            this.this$0.removeAt(n);
        }
    }
    
    final class KeySet implements Set<K>
    {
        final ArrayMap this$0;
        
        KeySet(final ArrayMap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean add(final K k) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void clear() {
            this.this$0.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.this$0.containsKey(o);
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            return this.this$0.containsAll(collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            return ArrayMap.equalsSetHelper((Set<Object>)this, o);
        }
        
        @Override
        public int hashCode() {
            int i = this.this$0.size() - 1;
            int n = 0;
            while (i >= 0) {
                final Object key = this.this$0.keyAt(i);
                int hashCode;
                if (key == null) {
                    hashCode = 0;
                }
                else {
                    hashCode = key.hashCode();
                }
                n += hashCode;
                --i;
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return this.this$0.isEmpty();
        }
        
        @Override
        public Iterator<K> iterator() {
            return this.this$0.new KeyIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            final int indexOfKey = this.this$0.indexOfKey(o);
            if (indexOfKey >= 0) {
                this.this$0.removeAt(indexOfKey);
                return true;
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            return this.this$0.removeAll(collection);
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            return this.this$0.retainAll(collection);
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
        
        @Override
        public Object[] toArray() {
            final int size = this.this$0.size();
            final Object[] array = new Object[size];
            for (int i = 0; i < size; ++i) {
                array[i] = this.this$0.keyAt(i);
            }
            return array;
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            final int size = this.size();
            Object[] array2 = array;
            if (array.length < size) {
                array2 = (Object[])Array.newInstance(array.getClass().getComponentType(), size);
            }
            for (int i = 0; i < size; ++i) {
                array2[i] = this.this$0.keyAt(i);
            }
            if (array2.length > size) {
                array2[size] = null;
            }
            return (T[])array2;
        }
    }
    
    final class MapIterator implements Iterator<Entry<K, V>>, Entry<K, V>
    {
        int mEnd;
        boolean mEntryValid;
        int mIndex;
        final ArrayMap this$0;
        
        MapIterator(final ArrayMap this$0) {
            this.this$0 = this$0;
            this.mEnd = this$0.size() - 1;
            this.mIndex = -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!this.mEntryValid) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            }
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            boolean b3 = b2;
            if (ContainerHelpersKt.equal(entry.getKey(), this.this$0.keyAt(this.mIndex))) {
                b3 = b2;
                if (ContainerHelpersKt.equal(entry.getValue(), this.this$0.valueAt(this.mIndex))) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public K getKey() {
            if (this.mEntryValid) {
                return this.this$0.keyAt(this.mIndex);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public V getValue() {
            if (this.mEntryValid) {
                return this.this$0.valueAt(this.mIndex);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public boolean hasNext() {
            return this.mIndex < this.mEnd;
        }
        
        @Override
        public int hashCode() {
            if (this.mEntryValid) {
                final Object key = this.this$0.keyAt(this.mIndex);
                final Object value = this.this$0.valueAt(this.mIndex);
                int hashCode = 0;
                int hashCode2;
                if (key == null) {
                    hashCode2 = 0;
                }
                else {
                    hashCode2 = key.hashCode();
                }
                if (value != null) {
                    hashCode = value.hashCode();
                }
                return hashCode2 ^ hashCode;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public Entry<K, V> next() {
            if (this.hasNext()) {
                ++this.mIndex;
                this.mEntryValid = true;
                return this;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            if (this.mEntryValid) {
                this.this$0.removeAt(this.mIndex);
                --this.mIndex;
                --this.mEnd;
                this.mEntryValid = false;
                return;
            }
            throw new IllegalStateException();
        }
        
        @Override
        public V setValue(final V v) {
            if (this.mEntryValid) {
                return this.this$0.setValueAt(this.mIndex, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getKey());
            sb.append("=");
            sb.append(this.getValue());
            return sb.toString();
        }
    }
    
    final class ValueCollection implements Collection<V>
    {
        final ArrayMap this$0;
        
        ValueCollection(final ArrayMap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean add(final V v) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void clear() {
            this.this$0.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.this$0.__restricted$indexOfValue(o) >= 0;
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            final Iterator<?> iterator = collection.iterator();
            while (iterator.hasNext()) {
                if (!this.contains(iterator.next())) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public boolean isEmpty() {
            return this.this$0.isEmpty();
        }
        
        @Override
        public Iterator<V> iterator() {
            return this.this$0.new ValueIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            final int _restricted$indexOfValue = this.this$0.__restricted$indexOfValue(o);
            if (_restricted$indexOfValue >= 0) {
                this.this$0.removeAt(_restricted$indexOfValue);
                return true;
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            int size = this.this$0.size();
            int i = 0;
            boolean b = false;
            while (i < size) {
                int n = size;
                int n2 = i;
                if (collection.contains(this.this$0.valueAt(i))) {
                    this.this$0.removeAt(i);
                    n2 = i - 1;
                    n = size - 1;
                    b = true;
                }
                i = n2 + 1;
                size = n;
            }
            return b;
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            int size = this.this$0.size();
            int i = 0;
            boolean b = false;
            while (i < size) {
                int n = size;
                int n2 = i;
                if (!collection.contains(this.this$0.valueAt(i))) {
                    this.this$0.removeAt(i);
                    n2 = i - 1;
                    n = size - 1;
                    b = true;
                }
                i = n2 + 1;
                size = n;
            }
            return b;
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
        
        @Override
        public Object[] toArray() {
            final int size = this.this$0.size();
            final Object[] array = new Object[size];
            for (int i = 0; i < size; ++i) {
                array[i] = this.this$0.valueAt(i);
            }
            return array;
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            final int size = this.size();
            Object[] array2 = array;
            if (array.length < size) {
                array2 = (Object[])Array.newInstance(array.getClass().getComponentType(), size);
            }
            for (int i = 0; i < size; ++i) {
                array2[i] = this.this$0.valueAt(i);
            }
            if (array2.length > size) {
                array2[size] = null;
            }
            return (T[])array2;
        }
    }
    
    final class ValueIterator extends IndexBasedArrayIterator<V>
    {
        final ArrayMap this$0;
        
        ValueIterator(final ArrayMap this$0) {
            this.this$0 = this$0;
            super(this$0.size());
        }
        
        @Override
        protected V elementAt(final int n) {
            return this.this$0.valueAt(n);
        }
        
        @Override
        protected void removeAt(final int n) {
            this.this$0.removeAt(n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.ArraysKt;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import java.util.Arrays;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001a\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0016\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b \n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u001d\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\b\u0010\u0015\u001a\u00020\u0010H\u0016J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\u0010\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0015\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0017J\u0018\u0010\u001b\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0011\u001a\u00020\u0012H\u0096\u0002¢\u0006\u0002\u0010\u001cJ\u001d\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001eJ\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0015\u0010 \u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010!J\b\u0010\"\u001a\u00020\u0007H\u0016J\u0010\u0010#\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u0004H\u0016J\u001d\u0010%\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\u0018\u0010&\u001a\u00020\u00102\u000e\u0010'\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0000H\u0016J\u001f\u0010(\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001eJ\u0010\u0010)\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u001d\u0010)\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010*J\u0010\u0010+\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u0004H\u0016J\u001f\u0010,\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001eJ%\u0010,\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010-\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010/J\u001d\u00100\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00101J\b\u0010\n\u001a\u00020\u0004H\u0016J\b\u00102\u001a\u000203H\u0016J\u0015\u00104\u001a\u00028\u00002\u0006\u0010$\u001a\u00020\u0004H\u0016¢\u0006\u0002\u00105R\u0012\u0010\u0006\u001a\u00020\u00078\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u00020\t8\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00020\u00048\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\f8\u0000@\u0000X\u0081\u000e¢\u0006\u0004\n\u0002\u0010\u000e¨\u00066" }, d2 = { "Landroidx/collection/LongSparseArray;", "E", "", "initialCapacity", "", "(I)V", "garbage", "", "keys", "", "size", "values", "", "", "[Ljava/lang/Object;", "append", "", "key", "", "value", "(JLjava/lang/Object;)V", "clear", "clone", "containsKey", "containsValue", "(Ljava/lang/Object;)Z", "delete", "get", "(J)Ljava/lang/Object;", "defaultValue", "(JLjava/lang/Object;)Ljava/lang/Object;", "indexOfKey", "indexOfValue", "(Ljava/lang/Object;)I", "isEmpty", "keyAt", "index", "put", "putAll", "other", "putIfAbsent", "remove", "(JLjava/lang/Object;)Z", "removeAt", "replace", "oldValue", "newValue", "(JLjava/lang/Object;Ljava/lang/Object;)Z", "setValueAt", "(ILjava/lang/Object;)V", "toString", "", "valueAt", "(I)Ljava/lang/Object;", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class LongSparseArray<E> implements Cloneable
{
    public boolean garbage;
    public long[] keys;
    public int size;
    public Object[] values;
    
    public LongSparseArray() {
        this(0, 1, null);
    }
    
    public LongSparseArray(int idealLongArraySize) {
        if (idealLongArraySize == 0) {
            this.keys = ContainerHelpersKt.EMPTY_LONGS;
            this.values = ContainerHelpersKt.EMPTY_OBJECTS;
        }
        else {
            idealLongArraySize = ContainerHelpersKt.idealLongArraySize(idealLongArraySize);
            this.keys = new long[idealLongArraySize];
            this.values = new Object[idealLongArraySize];
        }
    }
    
    public void append(final long n, final E e) {
        final int size = this.size;
        if (size != 0 && n <= this.keys[size - 1]) {
            this.put(n, e);
        }
        else {
            if (this.garbage) {
                final long[] keys = this.keys;
                if (size >= keys.length) {
                    final Object[] values = this.values;
                    int i = 0;
                    int size2 = 0;
                    while (i < size) {
                        final Object o = values[i];
                        int n2 = size2;
                        if (o != LongSparseArrayKt.access$getDELETED$p()) {
                            if (i != size2) {
                                keys[size2] = keys[i];
                                values[size2] = o;
                                values[i] = null;
                            }
                            n2 = size2 + 1;
                        }
                        ++i;
                        size2 = n2;
                    }
                    this.garbage = false;
                    this.size = size2;
                }
            }
            final int size3 = this.size;
            if (size3 >= this.keys.length) {
                final int idealLongArraySize = ContainerHelpersKt.idealLongArraySize(size3 + 1);
                final long[] copy = Arrays.copyOf(this.keys, idealLongArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                this.keys = copy;
                final Object[] copy2 = Arrays.copyOf(this.values, idealLongArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                this.values = copy2;
            }
            this.keys[size3] = n;
            this.values[size3] = e;
            this.size = size3 + 1;
        }
    }
    
    public void clear() {
        final int size = this.size;
        final Object[] values = this.values;
        for (int i = 0; i < size; ++i) {
            values[i] = null;
        }
        this.size = 0;
        this.garbage = false;
    }
    
    public LongSparseArray<E> clone() {
        final Object clone = super.clone();
        Intrinsics.checkNotNull(clone, "null cannot be cast to non-null type androidx.collection.LongSparseArray<E of androidx.collection.LongSparseArray>");
        final LongSparseArray longSparseArray = (LongSparseArray)clone;
        longSparseArray.keys = this.keys.clone();
        longSparseArray.values = this.values.clone();
        return longSparseArray;
    }
    
    public boolean containsKey(final long n) {
        return this.indexOfKey(n) >= 0;
    }
    
    public boolean containsValue(final E e) {
        return this.indexOfValue(e) >= 0;
    }
    
    @Deprecated(message = "Alias for `remove(key)`.", replaceWith = @ReplaceWith(expression = "remove(key)", imports = {}))
    public void delete(final long n) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        if (binarySearch >= 0 && this.values[binarySearch] != LongSparseArrayKt.access$getDELETED$p()) {
            this.values[binarySearch] = LongSparseArrayKt.access$getDELETED$p();
            this.garbage = true;
        }
    }
    
    public E get(final long n) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        Object o;
        if (binarySearch >= 0 && this.values[binarySearch] != LongSparseArrayKt.access$getDELETED$p()) {
            o = this.values[binarySearch];
        }
        else {
            o = null;
        }
        return (E)o;
    }
    
    public E get(final long n, final E e) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        Object o = e;
        if (binarySearch >= 0) {
            if (this.values[binarySearch] == LongSparseArrayKt.access$getDELETED$p()) {
                o = e;
            }
            else {
                o = this.values[binarySearch];
            }
        }
        return (E)o;
    }
    
    public int indexOfKey(final long n) {
        if (this.garbage) {
            final int size = this.size;
            final long[] keys = this.keys;
            final Object[] values = this.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n2 = size2;
                if (o != LongSparseArrayKt.access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n2 = size2 + 1;
                }
                ++i;
                size2 = n2;
            }
            this.garbage = false;
            this.size = size2;
        }
        return ContainerHelpersKt.binarySearch(this.keys, this.size, n);
    }
    
    public int indexOfValue(final E e) {
        final boolean garbage = this.garbage;
        final int n = 0;
        if (garbage) {
            final int size = this.size;
            final long[] keys = this.keys;
            final Object[] values = this.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n2 = size2;
                if (o != LongSparseArrayKt.access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n2 = size2 + 1;
                }
                ++i;
                size2 = n2;
            }
            this.garbage = false;
            this.size = size2;
        }
        for (int size3 = this.size, j = n; j < size3; ++j) {
            if (this.values[j] == e) {
                return j;
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public long keyAt(final int i) {
        if (i >= 0 && i < this.size) {
            if (this.garbage) {
                final int size = this.size;
                final long[] keys = this.keys;
                final Object[] values = this.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != LongSparseArrayKt.access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                this.garbage = false;
                this.size = size2;
            }
            return this.keys[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public void put(final long n, final E e) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        if (binarySearch >= 0) {
            this.values[binarySearch] = e;
        }
        else {
            final int n2 = ~binarySearch;
            if (n2 < this.size && this.values[n2] == LongSparseArrayKt.access$getDELETED$p()) {
                this.keys[n2] = n;
                this.values[n2] = e;
            }
            else {
                int n3 = n2;
                if (this.garbage) {
                    final int size = this.size;
                    final long[] keys = this.keys;
                    n3 = n2;
                    if (size >= keys.length) {
                        final Object[] values = this.values;
                        int i = 0;
                        int size2 = 0;
                        while (i < size) {
                            final Object o = values[i];
                            int n4 = size2;
                            if (o != LongSparseArrayKt.access$getDELETED$p()) {
                                if (i != size2) {
                                    keys[size2] = keys[i];
                                    values[size2] = o;
                                    values[i] = null;
                                }
                                n4 = size2 + 1;
                            }
                            ++i;
                            size2 = n4;
                        }
                        this.garbage = false;
                        this.size = size2;
                        n3 = ~ContainerHelpersKt.binarySearch(this.keys, size2, n);
                    }
                }
                final int size3 = this.size;
                if (size3 >= this.keys.length) {
                    final int idealLongArraySize = ContainerHelpersKt.idealLongArraySize(size3 + 1);
                    final long[] copy = Arrays.copyOf(this.keys, idealLongArraySize);
                    Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                    this.keys = copy;
                    final Object[] copy2 = Arrays.copyOf(this.values, idealLongArraySize);
                    Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                    this.values = copy2;
                }
                final int size4 = this.size;
                if (size4 - n3 != 0) {
                    final long[] keys2 = this.keys;
                    final int n5 = n3 + 1;
                    ArraysKt.copyInto(keys2, keys2, n5, n3, size4);
                    final Object[] values2 = this.values;
                    ArraysKt.copyInto(values2, values2, n5, n3, this.size);
                }
                this.keys[n3] = n;
                this.values[n3] = e;
                ++this.size;
            }
        }
    }
    
    public void putAll(final LongSparseArray<? extends E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "other");
        for (int size = longSparseArray.size(), i = 0; i < size; ++i) {
            this.put(longSparseArray.keyAt(i), (E)longSparseArray.valueAt(i));
        }
    }
    
    public E putIfAbsent(final long n, final E e) {
        final E value = this.get(n);
        if (value == null) {
            this.put(n, e);
        }
        return value;
    }
    
    public void remove(final long n) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        if (binarySearch >= 0 && this.values[binarySearch] != LongSparseArrayKt.access$getDELETED$p()) {
            this.values[binarySearch] = LongSparseArrayKt.access$getDELETED$p();
            this.garbage = true;
        }
    }
    
    public boolean remove(final long n, final E e) {
        final int indexOfKey = this.indexOfKey(n);
        boolean b;
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)e, (Object)this.valueAt(indexOfKey))) {
            this.removeAt(indexOfKey);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void removeAt(final int n) {
        if (this.values[n] != LongSparseArrayKt.access$getDELETED$p()) {
            this.values[n] = LongSparseArrayKt.access$getDELETED$p();
            this.garbage = true;
        }
    }
    
    public E replace(final long n, final E e) {
        final int indexOfKey = this.indexOfKey(n);
        Object o2;
        if (indexOfKey >= 0) {
            final Object[] values = this.values;
            final Object o = values[indexOfKey];
            values[indexOfKey] = e;
            o2 = o;
        }
        else {
            o2 = null;
        }
        return (E)o2;
    }
    
    public boolean replace(final long n, final E e, final E e2) {
        final int indexOfKey = this.indexOfKey(n);
        boolean b;
        if (indexOfKey >= 0 && Intrinsics.areEqual(this.values[indexOfKey], (Object)e)) {
            this.values[indexOfKey] = e2;
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void setValueAt(final int i, final E e) {
        if (i >= 0 && i < this.size) {
            if (this.garbage) {
                final int size = this.size;
                final long[] keys = this.keys;
                final Object[] values = this.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != LongSparseArrayKt.access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                this.garbage = false;
                this.size = size2;
            }
            this.values[i] = e;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public int size() {
        if (this.garbage) {
            final int size = this.size;
            final long[] keys = this.keys;
            final Object[] values = this.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n = size2;
                if (o != LongSparseArrayKt.access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n = size2 + 1;
                }
                ++i;
                size2 = n;
            }
            this.garbage = false;
            this.size = size2;
        }
        return this.size;
    }
    
    @Override
    public String toString() {
        String string;
        if (this.size() <= 0) {
            string = "{}";
        }
        else {
            final StringBuilder sb = new StringBuilder(this.size * 28);
            sb.append('{');
            for (int i = 0; i < this.size; ++i) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(this.keyAt(i));
                sb.append('=');
                final E value = this.valueAt(i);
                if (value != sb) {
                    sb.append(value);
                }
                else {
                    sb.append("(this Map)");
                }
            }
            sb.append('}');
            string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
        }
        return string;
    }
    
    public E valueAt(final int i) {
        if (i >= 0 && i < this.size) {
            if (this.garbage) {
                final int size = this.size;
                final long[] keys = this.keys;
                final Object[] values = this.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != LongSparseArrayKt.access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                this.garbage = false;
                this.size = size2;
            }
            return (E)this.values[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
}

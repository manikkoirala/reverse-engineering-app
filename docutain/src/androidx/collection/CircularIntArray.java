// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003J\u000e\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003J\u0006\u0010\u0013\u001a\u00020\u0010J\b\u0010\u0014\u001a\u00020\u0010H\u0002J\u0011\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0003H\u0086\u0002J\u0006\u0010\u0017\u001a\u00020\u0018J\u0006\u0010\u0019\u001a\u00020\u0003J\u0006\u0010\u001a\u001a\u00020\u0003J\u000e\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u0003J\u000e\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u0003J\u0006\u0010\u001e\u001a\u00020\u0003R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\f\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\r\u0010\nR\u000e\u0010\u000e\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u001f" }, d2 = { "Landroidx/collection/CircularIntArray;", "", "minCapacity", "", "(I)V", "capacityBitmask", "elements", "", "first", "getFirst", "()I", "head", "last", "getLast", "tail", "addFirst", "", "element", "addLast", "clear", "doubleCapacity", "get", "index", "isEmpty", "", "popFirst", "popLast", "removeFromEnd", "count", "removeFromStart", "size", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class CircularIntArray
{
    private int capacityBitmask;
    private int[] elements;
    private int head;
    private int tail;
    
    public CircularIntArray() {
        this(0, 1, null);
    }
    
    public CircularIntArray(final int i) {
        final int n = 0;
        if (i < 1) {
            throw new IllegalArgumentException("capacity must be >= 1".toString());
        }
        int n2 = n;
        if (i <= 1073741824) {
            n2 = 1;
        }
        if (n2 != 0) {
            int n3 = i;
            if (Integer.bitCount(i) != 1) {
                n3 = Integer.highestOneBit(i - 1) << 1;
            }
            this.capacityBitmask = n3 - 1;
            this.elements = new int[n3];
            return;
        }
        throw new IllegalArgumentException("capacity must be <= 2^30".toString());
    }
    
    private final void doubleCapacity() {
        final int[] elements = this.elements;
        final int length = elements.length;
        final int head = this.head;
        final int n = length << 1;
        if (n >= 0) {
            final int[] elements2 = new int[n];
            ArraysKt.copyInto(elements, elements2, 0, head, length);
            ArraysKt.copyInto(this.elements, elements2, length - head, 0, this.head);
            this.elements = elements2;
            this.head = 0;
            this.tail = length;
            this.capacityBitmask = n - 1;
            return;
        }
        throw new RuntimeException("Max array capacity exceeded");
    }
    
    public final void addFirst(final int n) {
        final int head = this.head - 1 & this.capacityBitmask;
        this.head = head;
        this.elements[head] = n;
        if (head == this.tail) {
            this.doubleCapacity();
        }
    }
    
    public final void addLast(int tail) {
        final int[] elements = this.elements;
        final int tail2 = this.tail;
        elements[tail2] = tail;
        tail = (this.capacityBitmask & tail2 + 1);
        this.tail = tail;
        if (tail == this.head) {
            this.doubleCapacity();
        }
    }
    
    public final void clear() {
        this.tail = this.head;
    }
    
    public final int get(final int n) {
        if (n >= 0 && n < this.size()) {
            return this.elements[this.capacityBitmask & this.head + n];
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final int getFirst() {
        final int head = this.head;
        if (head != this.tail) {
            return this.elements[head];
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final int getLast() {
        final int head = this.head;
        final int tail = this.tail;
        if (head != tail) {
            return this.elements[tail - 1 & this.capacityBitmask];
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final boolean isEmpty() {
        return this.head == this.tail;
    }
    
    public final int popFirst() {
        final int head = this.head;
        if (head != this.tail) {
            final int n = this.elements[head];
            this.head = (head + 1 & this.capacityBitmask);
            return n;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final int popLast() {
        final int head = this.head;
        final int tail = this.tail;
        if (head != tail) {
            final int tail2 = this.capacityBitmask & tail - 1;
            final int n = this.elements[tail2];
            this.tail = tail2;
            return n;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final void removeFromEnd(final int n) {
        if (n <= 0) {
            return;
        }
        if (n <= this.size()) {
            this.tail = (this.capacityBitmask & this.tail - n);
            return;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final void removeFromStart(final int n) {
        if (n <= 0) {
            return;
        }
        if (n <= this.size()) {
            this.head = (this.capacityBitmask & this.head + n);
            return;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final int size() {
        return this.tail - this.head & this.capacityBitmask;
    }
}

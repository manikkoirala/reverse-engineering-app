// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.util.Iterator;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010(\n\u0000\u001a!\u0010\u0006\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u0001H\u0086\n\u001aT\u0010\t\u001a\u00020\n\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000326\u0010\u000b\u001a2\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\r\u0012\b\b\u000e\u0012\u0004\b\b(\b\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\r\u0012\b\b\u000e\u0012\u0004\b\b(\u000f\u0012\u0004\u0012\u00020\n0\fH\u0086\b\u00f8\u0001\u0000\u001a.\u0010\u0010\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u0011\u001a\u0002H\u0002H\u0086\b¢\u0006\u0002\u0010\u0012\u001a7\u0010\u0013\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0014H\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015\u001a\u0019\u0010\u0016\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\b\u001a\u0016\u0010\u0017\u001a\u00020\u0018\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\u001a-\u0010\u0019\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\u0002\u001a-\u0010\u001b\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u000f\u001a\u0002H\u0002H\u0007¢\u0006\u0002\u0010\u001c\u001a.\u0010\u001d\u001a\u00020\n\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u000f\u001a\u0002H\u0002H\u0086\n¢\u0006\u0002\u0010\u001e\u001a\u001c\u0010\u001f\u001a\b\u0012\u0004\u0012\u0002H\u00020 \"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\"\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006!" }, d2 = { "size", "", "T", "Landroidx/collection/SparseArrayCompat;", "getSize", "(Landroidx/collection/SparseArrayCompat;)I", "contains", "", "key", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "value", "getOrDefault", "defaultValue", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;)Ljava/lang/Object;", "getOrElse", "Lkotlin/Function0;", "(Landroidx/collection/SparseArrayCompat;ILkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "isNotEmpty", "keyIterator", "Lkotlin/collections/IntIterator;", "plus", "other", "remove", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;)Z", "set", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;)V", "valueIterator", "", "collection" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SparseArrayKt
{
    public static final <T> boolean contains(final SparseArrayCompat<T> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.containsKey(n);
    }
    
    public static final <T> void forEach(final SparseArrayCompat<T> sparseArrayCompat, final Function2<? super Integer, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = sparseArrayCompat.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseArrayCompat.keyAt(i), sparseArrayCompat.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(final SparseArrayCompat<T> sparseArrayCompat, final int n, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.get(n, t);
    }
    
    public static final <T> T getOrElse(final SparseArrayCompat<T> sparseArrayCompat, final int n, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        Object o;
        if ((o = sparseArrayCompat.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.size();
    }
    
    public static final <T> boolean isNotEmpty(final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.isEmpty() ^ true;
    }
    
    public static final <T> IntIterator keyIterator(final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return (IntIterator)new SparseArrayKt$keyIterator.SparseArrayKt$keyIterator$1((SparseArrayCompat)sparseArrayCompat);
    }
    
    public static final <T> SparseArrayCompat<T> plus(final SparseArrayCompat<T> sparseArrayCompat, final SparseArrayCompat<T> sparseArrayCompat2) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat2, "other");
        final SparseArrayCompat sparseArrayCompat3 = new SparseArrayCompat(sparseArrayCompat.size() + sparseArrayCompat2.size());
        sparseArrayCompat3.putAll(sparseArrayCompat);
        sparseArrayCompat3.putAll(sparseArrayCompat2);
        return sparseArrayCompat3;
    }
    
    public static final <T> void set(final SparseArrayCompat<T> sparseArrayCompat, final int n, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        sparseArrayCompat.put(n, t);
    }
    
    public static final <T> Iterator<T> valueIterator(final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return (Iterator<T>)new SparseArrayKt$valueIterator.SparseArrayKt$valueIterator$1((SparseArrayCompat)sparseArrayCompat);
    }
}

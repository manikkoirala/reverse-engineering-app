// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.ArraysKt;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import java.util.Arrays;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001a\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u001f\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u001d\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u00020\u0013H\u0016J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\u0010\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0015\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0017J\u0018\u0010\u001d\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u0004H\u0096\u0002¢\u0006\u0002\u0010\u001eJ\u001d\u0010\u001d\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010 J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0015\u0010\"\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010#J\b\u0010\b\u001a\u00020\u0007H\u0016J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u0004H\u0016J\u001d\u0010&\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0016J\u0018\u0010'\u001a\u00020\u00132\u000e\u0010(\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0000H\u0016J\u001f\u0010)\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010 J\u0010\u0010*\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u001a\u0010*\u001a\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00042\b\u0010\u0015\u001a\u0004\u0018\u00010\u0010H\u0016J\u0010\u0010+\u001a\u00020\u00132\u0006\u0010%\u001a\u00020\u0004H\u0016J\u0018\u0010,\u001a\u00020\u00132\u0006\u0010%\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0004H\u0016J\u001f\u0010-\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010 J%\u0010-\u001a\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010.\u001a\u00028\u00002\u0006\u0010/\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00100J\u001d\u00101\u001a\u00020\u00132\u0006\u0010%\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0016J\b\u0010\r\u001a\u00020\u0004H\u0016J\b\u00102\u001a\u000203H\u0016J\u0015\u00104\u001a\u00028\u00002\u0006\u0010%\u001a\u00020\u0004H\u0016¢\u0006\u0002\u0010\u001eR\u0012\u0010\u0006\u001a\u00020\u00078\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\u00078G¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\f8\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\r\u001a\u00020\u00048\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f8\u0000@\u0000X\u0081\u000e¢\u0006\u0004\n\u0002\u0010\u0011¨\u00065" }, d2 = { "Landroidx/collection/SparseArrayCompat;", "E", "", "initialCapacity", "", "(I)V", "garbage", "", "isEmpty", "getIsEmpty", "()Z", "keys", "", "size", "values", "", "", "[Ljava/lang/Object;", "append", "", "key", "value", "(ILjava/lang/Object;)V", "clear", "clone", "containsKey", "containsValue", "(Ljava/lang/Object;)Z", "delete", "get", "(I)Ljava/lang/Object;", "defaultValue", "(ILjava/lang/Object;)Ljava/lang/Object;", "indexOfKey", "indexOfValue", "(Ljava/lang/Object;)I", "keyAt", "index", "put", "putAll", "other", "putIfAbsent", "remove", "removeAt", "removeAtRange", "replace", "oldValue", "newValue", "(ILjava/lang/Object;Ljava/lang/Object;)Z", "setValueAt", "toString", "", "valueAt", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class SparseArrayCompat<E> implements Cloneable
{
    public boolean garbage;
    public int[] keys;
    public int size;
    public Object[] values;
    
    public SparseArrayCompat() {
        this(0, 1, null);
    }
    
    public SparseArrayCompat(int idealIntArraySize) {
        if (idealIntArraySize == 0) {
            this.keys = ContainerHelpersKt.EMPTY_INTS;
            this.values = ContainerHelpersKt.EMPTY_OBJECTS;
        }
        else {
            idealIntArraySize = ContainerHelpersKt.idealIntArraySize(idealIntArraySize);
            this.keys = new int[idealIntArraySize];
            this.values = new Object[idealIntArraySize];
        }
    }
    
    public void append(final int n, final E e) {
        final int size = this.size;
        if (size != 0 && n <= this.keys[size - 1]) {
            this.put(n, e);
        }
        else {
            if (this.garbage && size >= this.keys.length) {
                SparseArrayCompatKt.access$gc(this);
            }
            final int size2 = this.size;
            if (size2 >= this.keys.length) {
                final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(size2 + 1);
                final int[] copy = Arrays.copyOf(this.keys, idealIntArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                this.keys = copy;
                final Object[] copy2 = Arrays.copyOf(this.values, idealIntArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                this.values = copy2;
            }
            this.keys[size2] = n;
            this.values[size2] = e;
            this.size = size2 + 1;
        }
    }
    
    public void clear() {
        final int size = this.size;
        final Object[] values = this.values;
        for (int i = 0; i < size; ++i) {
            values[i] = null;
        }
        this.size = 0;
        this.garbage = false;
    }
    
    public SparseArrayCompat<E> clone() {
        final Object clone = super.clone();
        Intrinsics.checkNotNull(clone, "null cannot be cast to non-null type androidx.collection.SparseArrayCompat<E of androidx.collection.SparseArrayCompat>");
        final SparseArrayCompat sparseArrayCompat = (SparseArrayCompat)clone;
        sparseArrayCompat.keys = this.keys.clone();
        sparseArrayCompat.values = this.values.clone();
        return sparseArrayCompat;
    }
    
    public boolean containsKey(final int n) {
        return this.indexOfKey(n) >= 0;
    }
    
    public boolean containsValue(final E e) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        final int size = this.size;
        boolean b = false;
        while (true) {
            for (int i = 0; i < size; ++i) {
                if (this.values[i] == e) {
                    if (i >= 0) {
                        b = true;
                    }
                    return b;
                }
            }
            int i = -1;
            continue;
        }
    }
    
    @Deprecated(message = "Alias for remove(int).", replaceWith = @ReplaceWith(expression = "remove(key)", imports = {}))
    public void delete(final int n) {
        this.remove(n);
    }
    
    public E get(final int n) {
        return SparseArrayCompatKt.commonGet(this, n);
    }
    
    public E get(final int n, final E e) {
        return SparseArrayCompatKt.commonGet(this, n, e);
    }
    
    public final boolean getIsEmpty() {
        return this.isEmpty();
    }
    
    public int indexOfKey(final int n) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        return ContainerHelpersKt.binarySearch(this.keys, this.size, n);
    }
    
    public int indexOfValue(final E e) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        for (int i = 0; i < this.size; ++i) {
            if (this.values[i] == e) {
                return i;
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public int keyAt(final int n) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        return this.keys[n];
    }
    
    public void put(final int n, final E e) {
        final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
        if (binarySearch >= 0) {
            this.values[binarySearch] = e;
        }
        else {
            final int n2 = ~binarySearch;
            if (n2 < this.size && this.values[n2] == SparseArrayCompatKt.access$getDELETED$p()) {
                this.keys[n2] = n;
                this.values[n2] = e;
            }
            else {
                int n3 = n2;
                if (this.garbage) {
                    n3 = n2;
                    if (this.size >= this.keys.length) {
                        SparseArrayCompatKt.access$gc(this);
                        n3 = ~ContainerHelpersKt.binarySearch(this.keys, this.size, n);
                    }
                }
                final int size = this.size;
                if (size >= this.keys.length) {
                    final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(size + 1);
                    final int[] copy = Arrays.copyOf(this.keys, idealIntArraySize);
                    Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                    this.keys = copy;
                    final Object[] copy2 = Arrays.copyOf(this.values, idealIntArraySize);
                    Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                    this.values = copy2;
                }
                final int size2 = this.size;
                if (size2 - n3 != 0) {
                    final int[] keys = this.keys;
                    final int n4 = n3 + 1;
                    ArraysKt.copyInto(keys, keys, n4, n3, size2);
                    final Object[] values = this.values;
                    ArraysKt.copyInto(values, values, n4, n3, this.size);
                }
                this.keys[n3] = n;
                this.values[n3] = e;
                ++this.size;
            }
        }
    }
    
    public void putAll(final SparseArrayCompat<? extends E> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "other");
        for (int size = sparseArrayCompat.size(), i = 0; i < size; ++i) {
            final int key = sparseArrayCompat.keyAt(i);
            final Object value = sparseArrayCompat.valueAt(i);
            final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, key);
            if (binarySearch >= 0) {
                this.values[binarySearch] = value;
            }
            else {
                final int n = ~binarySearch;
                if (n < this.size && this.values[n] == SparseArrayCompatKt.access$getDELETED$p()) {
                    this.keys[n] = key;
                    this.values[n] = value;
                }
                else {
                    int n2 = n;
                    if (this.garbage) {
                        n2 = n;
                        if (this.size >= this.keys.length) {
                            SparseArrayCompatKt.access$gc(this);
                            n2 = ~ContainerHelpersKt.binarySearch(this.keys, this.size, key);
                        }
                    }
                    final int size2 = this.size;
                    if (size2 >= this.keys.length) {
                        final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(size2 + 1);
                        final int[] copy = Arrays.copyOf(this.keys, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                        this.keys = copy;
                        final Object[] copy2 = Arrays.copyOf(this.values, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                        this.values = copy2;
                    }
                    final int size3 = this.size;
                    if (size3 - n2 != 0) {
                        final int[] keys = this.keys;
                        final int n3 = n2 + 1;
                        ArraysKt.copyInto(keys, keys, n3, n2, size3);
                        final Object[] values = this.values;
                        ArraysKt.copyInto(values, values, n3, n2, this.size);
                    }
                    this.keys[n2] = key;
                    this.values[n2] = value;
                    ++this.size;
                }
            }
        }
    }
    
    public E putIfAbsent(final int n, final E e) {
        final E commonGet = SparseArrayCompatKt.commonGet(this, n);
        if (commonGet == null) {
            final int binarySearch = ContainerHelpersKt.binarySearch(this.keys, this.size, n);
            if (binarySearch >= 0) {
                this.values[binarySearch] = e;
            }
            else {
                final int n2 = ~binarySearch;
                if (n2 < this.size && this.values[n2] == SparseArrayCompatKt.access$getDELETED$p()) {
                    this.keys[n2] = n;
                    this.values[n2] = e;
                }
                else {
                    int n3 = n2;
                    if (this.garbage) {
                        n3 = n2;
                        if (this.size >= this.keys.length) {
                            SparseArrayCompatKt.access$gc(this);
                            n3 = ~ContainerHelpersKt.binarySearch(this.keys, this.size, n);
                        }
                    }
                    final int size = this.size;
                    if (size >= this.keys.length) {
                        final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(size + 1);
                        final int[] copy = Arrays.copyOf(this.keys, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                        this.keys = copy;
                        final Object[] copy2 = Arrays.copyOf(this.values, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                        this.values = copy2;
                    }
                    final int size2 = this.size;
                    if (size2 - n3 != 0) {
                        final int[] keys = this.keys;
                        final int n4 = n3 + 1;
                        ArraysKt.copyInto(keys, keys, n4, n3, size2);
                        final Object[] values = this.values;
                        ArraysKt.copyInto(values, values, n4, n3, this.size);
                    }
                    this.keys[n3] = n;
                    this.values[n3] = e;
                    ++this.size;
                }
            }
        }
        return commonGet;
    }
    
    public void remove(final int n) {
        SparseArrayCompatKt.commonRemove((SparseArrayCompat<Object>)this, n);
    }
    
    public boolean remove(int indexOfKey, final Object o) {
        indexOfKey = this.indexOfKey(indexOfKey);
        boolean b;
        if (indexOfKey >= 0 && Intrinsics.areEqual(o, this.valueAt(indexOfKey))) {
            this.removeAt(indexOfKey);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void removeAt(final int n) {
        if (this.values[n] != SparseArrayCompatKt.access$getDELETED$p()) {
            this.values[n] = SparseArrayCompatKt.access$getDELETED$p();
            this.garbage = true;
        }
    }
    
    public void removeAtRange(int i, int min) {
        for (min = Math.min(min, i + min); i < min; ++i) {
            this.removeAt(i);
        }
    }
    
    public E replace(int indexOfKey, final E e) {
        indexOfKey = this.indexOfKey(indexOfKey);
        Object o2;
        if (indexOfKey >= 0) {
            final Object[] values = this.values;
            final Object o = values[indexOfKey];
            values[indexOfKey] = e;
            o2 = o;
        }
        else {
            o2 = null;
        }
        return (E)o2;
    }
    
    public boolean replace(int indexOfKey, final E e, final E e2) {
        indexOfKey = this.indexOfKey(indexOfKey);
        boolean b;
        if (indexOfKey >= 0 && Intrinsics.areEqual(this.values[indexOfKey], (Object)e)) {
            this.values[indexOfKey] = e2;
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void setValueAt(final int n, final E e) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        this.values[n] = e;
    }
    
    public int size() {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        return this.size;
    }
    
    @Override
    public String toString() {
        String string;
        if (this.size() <= 0) {
            string = "{}";
        }
        else {
            final StringBuilder sb = new StringBuilder(this.size * 28);
            sb.append('{');
            for (int i = 0; i < this.size; ++i) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(this.keyAt(i));
                sb.append('=');
                final E value = this.valueAt(i);
                if (value != this) {
                    sb.append(value);
                }
                else {
                    sb.append("(this Map)");
                }
            }
            sb.append('}');
            string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "buffer.toString()");
        }
        return string;
    }
    
    public E valueAt(final int n) {
        if (this.garbage) {
            SparseArrayCompatKt.access$gc(this);
        }
        return (E)this.values[n];
    }
}

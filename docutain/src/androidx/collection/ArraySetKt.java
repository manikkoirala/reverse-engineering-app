// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.CollectionsKt;
import java.util.Set;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Iterator;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0015\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0003\"\u0004\b\u0000\u0010\u0004H\u0086\b\u001a+\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0003\"\u0004\b\u0000\u0010\u00042\u0012\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00040\u0006\"\u0002H\u0004¢\u0006\u0002\u0010\u0007\u001a)\u0010\b\u001a\u00020\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0003H\u0080\b\u001a'\u0010\b\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000eH\u0080\b\u001a&\u0010\u000f\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u0010\u001a\u0002H\nH\u0080\b¢\u0006\u0002\u0010\u0011\u001a \u0010\u0012\u001a\u00020\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u0013\u001a\u00020\u0001H\u0000\u001a \u0010\u0014\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u0015\u001a\u00020\u0001H\u0000\u001a\u0019\u0010\u0016\u001a\u00020\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u0003H\u0080\b\u001a'\u0010\u0017\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000eH\u0080\b\u001a&\u0010\u0018\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u0010\u001a\u0002H\nH\u0080\b¢\u0006\u0002\u0010\u0011\u001a!\u0010\u0019\u001a\u00020\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u001a\u001a\u00020\u0001H\u0080\b\u001a#\u0010\u001b\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0080\b\u001a\u0019\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u0003H\u0080\b\u001a*\u0010\u001f\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\b\u0010 \u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u0015\u001a\u00020\u0001H\u0000\u001a#\u0010!\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\b\u0010 \u001a\u0004\u0018\u00010\u001dH\u0080\b\u001a\u0018\u0010\"\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u0003H\u0000\u001a\u0019\u0010#\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u0003H\u0080\b\u001a)\u0010$\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0003H\u0080\b\u001a'\u0010$\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000eH\u0080\b\u001a&\u0010%\u001a\u0002H\n\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010&\u001a\u00020\u0001H\u0080\b¢\u0006\u0002\u0010'\u001a&\u0010(\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010\u0010\u001a\u0002H\nH\u0080\b¢\u0006\u0002\u0010\u0011\u001a'\u0010)\u001a\u00020\f\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000eH\u0080\b\u001a\u0019\u0010*\u001a\u00020+\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u0003H\u0080\b\u001a&\u0010,\u001a\u0002H\n\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00032\u0006\u0010&\u001a\u00020\u0001H\u0080\b¢\u0006\u0002\u0010'\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "ARRAY_SET_BASE_SIZE", "", "arraySetOf", "Landroidx/collection/ArraySet;", "T", "values", "", "([Ljava/lang/Object;)Landroidx/collection/ArraySet;", "addAllInternal", "", "E", "array", "", "elements", "", "addInternal", "element", "(Landroidx/collection/ArraySet;Ljava/lang/Object;)Z", "allocArrays", "size", "binarySearchInternal", "hash", "clearInternal", "containsAllInternal", "containsInternal", "ensureCapacityInternal", "minimumCapacity", "equalsInternal", "other", "", "hashCodeInternal", "indexOf", "key", "indexOfInternal", "indexOfNull", "isEmptyInternal", "removeAllInternal", "removeAtInternal", "index", "(Landroidx/collection/ArraySet;I)Ljava/lang/Object;", "removeInternal", "retainAllInternal", "toStringInternal", "", "valueAtInternal", "collection" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ArraySetKt
{
    public static final int ARRAY_SET_BASE_SIZE = 4;
    
    public static final <E> void addAllInternal(final ArraySet<E> set, final ArraySet<? extends E> set2) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)set2, "array");
        final int get_size$collection = set2.get_size$collection();
        set.ensureCapacity(set.get_size$collection() + get_size$collection);
        if (set.get_size$collection() == 0) {
            if (get_size$collection > 0) {
                ArraysKt.copyInto$default(set2.getHashes$collection(), set.getHashes$collection(), 0, 0, get_size$collection, 6, (Object)null);
                ArraysKt.copyInto$default(set2.getArray$collection(), set.getArray$collection(), 0, 0, get_size$collection, 6, (Object)null);
                if (set.get_size$collection() != 0) {
                    throw new ConcurrentModificationException();
                }
                set.set_size$collection(get_size$collection);
            }
        }
        else {
            for (int i = 0; i < get_size$collection; ++i) {
                set.add((E)set2.valueAt(i));
            }
        }
    }
    
    public static final <E> boolean addAllInternal(final ArraySet<E> set, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        set.ensureCapacity(set.get_size$collection() + collection.size());
        final Iterator iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= set.add((E)iterator.next());
        }
        return b;
    }
    
    public static final <E> boolean addInternal(final ArraySet<E> set, final E e) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int get_size$collection = set.get_size$collection();
        final int n = 0;
        int n2;
        int hashCode;
        if (e == null) {
            n2 = indexOfNull(set);
            hashCode = 0;
        }
        else {
            hashCode = e.hashCode();
            n2 = indexOf(set, e, hashCode);
        }
        if (n2 >= 0) {
            return false;
        }
        final int n3 = ~n2;
        if (get_size$collection >= set.getHashes$collection().length) {
            int n4 = 4;
            if (get_size$collection >= 8) {
                n4 = (get_size$collection >> 1) + get_size$collection;
            }
            else if (get_size$collection >= 4) {
                n4 = 8;
            }
            final int[] hashes$collection = set.getHashes$collection();
            final Object[] array$collection = set.getArray$collection();
            allocArrays(set, n4);
            if (get_size$collection != set.get_size$collection()) {
                throw new ConcurrentModificationException();
            }
            int n5 = n;
            if (set.getHashes$collection().length == 0) {
                n5 = 1;
            }
            if ((n5 ^ 0x1) != 0x0) {
                ArraysKt.copyInto$default(hashes$collection, set.getHashes$collection(), 0, 0, hashes$collection.length, 6, (Object)null);
                ArraysKt.copyInto$default(array$collection, set.getArray$collection(), 0, 0, array$collection.length, 6, (Object)null);
            }
        }
        if (n3 < get_size$collection) {
            final int[] hashes$collection2 = set.getHashes$collection();
            final int[] hashes$collection3 = set.getHashes$collection();
            final int n6 = n3 + 1;
            ArraysKt.copyInto(hashes$collection2, hashes$collection3, n6, n3, get_size$collection);
            ArraysKt.copyInto(set.getArray$collection(), set.getArray$collection(), n6, n3, get_size$collection);
        }
        if (get_size$collection == set.get_size$collection() && n3 < set.getHashes$collection().length) {
            set.getHashes$collection()[n3] = hashCode;
            set.getArray$collection()[n3] = e;
            set.set_size$collection(set.get_size$collection() + 1);
            return true;
        }
        throw new ConcurrentModificationException();
    }
    
    public static final <E> void allocArrays(final ArraySet<E> set, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        set.setHashes$collection(new int[n]);
        set.setArray$collection(new Object[n]);
    }
    
    public static final <T> ArraySet<T> arraySetOf() {
        return new ArraySet<T>(0, 1, null);
    }
    
    public static final <T> ArraySet<T> arraySetOf(final T... array) {
        Intrinsics.checkNotNullParameter((Object)array, "values");
        final ArraySet set = new ArraySet(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            set.add(array[i]);
        }
        return set;
    }
    
    public static final <E> int binarySearchInternal(final ArraySet<E> set, int binarySearch) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        try {
            binarySearch = ContainerHelpersKt.binarySearch(set.getHashes$collection(), set.get_size$collection(), binarySearch);
            return binarySearch;
        }
        catch (final IndexOutOfBoundsException ex) {
            throw new ConcurrentModificationException();
        }
    }
    
    public static final <E> void clearInternal(final ArraySet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        if (set.get_size$collection() != 0) {
            set.setHashes$collection(ContainerHelpersKt.EMPTY_INTS);
            set.setArray$collection(ContainerHelpersKt.EMPTY_OBJECTS);
            set.set_size$collection(0);
        }
        if (set.get_size$collection() == 0) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public static final <E> boolean containsAllInternal(final ArraySet<E> set, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<? extends E> iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!set.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public static final <E> boolean containsInternal(final ArraySet<E> set, final E e) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return set.indexOf(e) >= 0;
    }
    
    public static final <E> void ensureCapacityInternal(final ArraySet<E> set, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int get_size$collection = set.get_size$collection();
        if (set.getHashes$collection().length < n) {
            final int[] hashes$collection = set.getHashes$collection();
            final Object[] array$collection = set.getArray$collection();
            allocArrays((ArraySet<Object>)set, n);
            if (set.get_size$collection() > 0) {
                ArraysKt.copyInto$default(hashes$collection, set.getHashes$collection(), 0, 0, set.get_size$collection(), 6, (Object)null);
                ArraysKt.copyInto$default(array$collection, set.getArray$collection(), 0, 0, set.get_size$collection(), 6, (Object)null);
            }
        }
        if (set.get_size$collection() == get_size$collection) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public static final <E> boolean equalsInternal(final ArraySet<E> set, final Object o) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        if (set == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        if (set.size() != ((Set)o).size()) {
            return false;
        }
        try {
            for (int get_size$collection = set.get_size$collection(), i = 0; i < get_size$collection; ++i) {
                if (!((Set)o).contains(set.valueAt(i))) {
                    return false;
                }
            }
            return true;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public static final <E> int hashCodeInternal(final ArraySet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int[] hashes$collection = set.getHashes$collection();
        final int get_size$collection = set.get_size$collection();
        int i = 0;
        int n = 0;
        while (i < get_size$collection) {
            n += hashes$collection[i];
            ++i;
        }
        return n;
    }
    
    public static final <E> int indexOf(final ArraySet<E> set, final Object o, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int get_size$collection = set.get_size$collection();
        if (get_size$collection == 0) {
            return -1;
        }
        int binarySearchInternal = binarySearchInternal(set, n);
        if (binarySearchInternal < 0) {
            return binarySearchInternal;
        }
        if (Intrinsics.areEqual(o, set.getArray$collection()[binarySearchInternal])) {
            return binarySearchInternal;
        }
        int n2;
        for (n2 = binarySearchInternal + 1; n2 < get_size$collection && set.getHashes$collection()[n2] == n; ++n2) {
            if (Intrinsics.areEqual(o, set.getArray$collection()[n2])) {
                return n2;
            }
        }
        --binarySearchInternal;
        while (binarySearchInternal >= 0 && set.getHashes$collection()[binarySearchInternal] == n) {
            if (Intrinsics.areEqual(o, set.getArray$collection()[binarySearchInternal])) {
                return binarySearchInternal;
            }
            --binarySearchInternal;
        }
        return ~n2;
    }
    
    public static final <E> int indexOfInternal(final ArraySet<E> set, final Object o) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        int n;
        if (o == null) {
            n = indexOfNull(set);
        }
        else {
            n = indexOf(set, o, o.hashCode());
        }
        return n;
    }
    
    public static final <E> int indexOfNull(final ArraySet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return indexOf(set, null, 0);
    }
    
    public static final <E> boolean isEmptyInternal(final ArraySet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return set.get_size$collection() <= 0;
    }
    
    public static final <E> boolean removeAllInternal(final ArraySet<E> set, final ArraySet<? extends E> set2) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)set2, "array");
        final int get_size$collection = set2.get_size$collection();
        final int get_size$collection2 = set.get_size$collection();
        boolean b = false;
        for (int i = 0; i < get_size$collection; ++i) {
            set.remove(set2.valueAt(i));
        }
        if (get_size$collection2 != set.get_size$collection()) {
            b = true;
        }
        return b;
    }
    
    public static final <E> boolean removeAllInternal(final ArraySet<E> set, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<? extends E> iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= set.remove(iterator.next());
        }
        return b;
    }
    
    public static final <E> E removeAtInternal(final ArraySet<E> set, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int get_size$collection = set.get_size$collection();
        final Object o = set.getArray$collection()[n];
        if (get_size$collection <= 1) {
            set.clear();
        }
        else {
            final int n2 = get_size$collection - 1;
            final int length = set.getHashes$collection().length;
            int n3 = 8;
            if (length > 8 && set.get_size$collection() < set.getHashes$collection().length / 3) {
                if (set.get_size$collection() > 8) {
                    n3 = set.get_size$collection() + (set.get_size$collection() >> 1);
                }
                final int[] hashes$collection = set.getHashes$collection();
                final Object[] array$collection = set.getArray$collection();
                allocArrays(set, n3);
                if (n > 0) {
                    ArraysKt.copyInto$default(hashes$collection, set.getHashes$collection(), 0, 0, n, 6, (Object)null);
                    ArraysKt.copyInto$default(array$collection, set.getArray$collection(), 0, 0, n, 6, (Object)null);
                }
                if (n < n2) {
                    final int[] hashes$collection2 = set.getHashes$collection();
                    final int n4 = n + 1;
                    final int n5 = n2 + 1;
                    ArraysKt.copyInto(hashes$collection, hashes$collection2, n, n4, n5);
                    ArraysKt.copyInto(array$collection, set.getArray$collection(), n, n4, n5);
                }
            }
            else {
                if (n < n2) {
                    final int[] hashes$collection3 = set.getHashes$collection();
                    final int[] hashes$collection4 = set.getHashes$collection();
                    final int n6 = n + 1;
                    final int n7 = n2 + 1;
                    ArraysKt.copyInto(hashes$collection3, hashes$collection4, n, n6, n7);
                    ArraysKt.copyInto(set.getArray$collection(), set.getArray$collection(), n, n6, n7);
                }
                set.getArray$collection()[n2] = null;
            }
            if (get_size$collection != set.get_size$collection()) {
                throw new ConcurrentModificationException();
            }
            set.set_size$collection(n2);
        }
        return (E)o;
    }
    
    public static final <E> boolean removeInternal(final ArraySet<E> set, final E e) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        final int index = set.indexOf(e);
        if (index >= 0) {
            set.removeAt(index);
            return true;
        }
        return false;
    }
    
    public static final <E> boolean retainAllInternal(final ArraySet<E> set, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        int n = set.get_size$collection() - 1;
        boolean b = false;
        while (-1 < n) {
            if (!CollectionsKt.contains((Iterable)collection, set.getArray$collection()[n])) {
                set.removeAt(n);
                b = true;
            }
            --n;
        }
        return b;
    }
    
    public static final <E> String toStringInternal(final ArraySet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        if (set.isEmpty()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(set.get_size$collection() * 14);
        sb.append('{');
        for (int i = 0; i < set.get_size$collection(); ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            final Object value = set.valueAt(i);
            if (value != set) {
                sb.append(value);
            }
            else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
        return string;
    }
    
    public static final <E> E valueAtInternal(final ArraySet<E> set, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return (E)set.getArray$collection()[n];
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection.internal;

import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.LinkedHashMap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010&\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0000\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00022\u00020\u0002B\u001d\b\u0016\u0012\u0014\u0010\u0004\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000¢\u0006\u0002\u0010\u0005B\u0019\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0018\u0010\u0016\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0017\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010\u0018J\u001d\u0010\u0019\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0017\u001a\u00028\u00002\u0006\u0010\u001a\u001a\u00028\u0001¢\u0006\u0002\u0010\u001bJ\u0015\u0010\u001c\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0017\u001a\u00028\u0000¢\u0006\u0002\u0010\u0018R#\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r0\f8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0010\u001a\u00020\u00118F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0012R*\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0014j\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0015X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d" }, d2 = { "Landroidx/collection/internal/LruHashMap;", "K", "", "V", "original", "(Landroidx/collection/internal/LruHashMap;)V", "initialCapacity", "", "loadFactor", "", "(IF)V", "entries", "", "", "getEntries", "()Ljava/util/Set;", "isEmpty", "", "()Z", "map", "Ljava/util/LinkedHashMap;", "Lkotlin/collections/LinkedHashMap;", "get", "key", "(Ljava/lang/Object;)Ljava/lang/Object;", "put", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "remove", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class LruHashMap<K, V>
{
    private final LinkedHashMap<K, V> map;
    
    public LruHashMap() {
        this(0, 0.0f, 3, null);
    }
    
    public LruHashMap(final int initialCapacity, final float loadFactor) {
        this.map = new LinkedHashMap<K, V>(initialCapacity, loadFactor, true);
    }
    
    public LruHashMap(final LruHashMap<? extends K, V> lruHashMap) {
        Intrinsics.checkNotNullParameter((Object)lruHashMap, "original");
        this(0, 0.0f, 3, null);
        for (final Map.Entry<K, V> entry : lruHashMap.getEntries()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    
    public final V get(final K key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        return this.map.get(key);
    }
    
    public final Set<Map.Entry<K, V>> getEntries() {
        final Set<Map.Entry<K, V>> entrySet = this.map.entrySet();
        Intrinsics.checkNotNullExpressionValue((Object)entrySet, "map.entries");
        return entrySet;
    }
    
    public final boolean isEmpty() {
        return this.map.isEmpty();
    }
    
    public final V put(final K key, final V value) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        Intrinsics.checkNotNullParameter((Object)value, "value");
        return this.map.put(key, value);
    }
    
    public final V remove(final K key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        return this.map.remove(key);
    }
}

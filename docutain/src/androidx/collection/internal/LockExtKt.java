// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection.internal;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a6\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0004H\u0080\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0001¢\u0006\u0002\u0010\u0005\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u0006" }, d2 = { "synchronized", "T", "Landroidx/collection/internal/Lock;", "block", "Lkotlin/Function0;", "(Landroidx/collection/internal/Lock;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "collection" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class LockExtKt
{
    public static final <T> T synchronized(final Lock lock, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)lock, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        monitorenter(lock);
        try {
            return (T)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
}

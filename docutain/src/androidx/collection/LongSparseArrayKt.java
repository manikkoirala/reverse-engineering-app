// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.util.Iterator;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.collections.LongIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.collections.ArraysKt;
import java.util.Arrays;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000X\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u001d\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010(\n\u0000\u001a.\u0010\n\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u0010\u001a\u0019\u0010\u0011\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u0005H\u0080\b\u001a!\u0010\u0012\u001a\u00020\u0013\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0080\b\u001a&\u0010\u0014\u001a\u00020\u0013\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u0015\u001a\u0019\u0010\u0016\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u0005H\u0080\b\u001a(\u0010\u0017\u001a\u0004\u0018\u0001H\f\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0080\b¢\u0006\u0002\u0010\u0018\u001a.\u0010\u0017\u001a\u0002H\f\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u001a\u001a:\u0010\u001b\u001a\u0002H\u0004\"\n\b\u0000\u0010\u0004*\u0004\u0018\u0001H\f\"\u0004\b\u0001\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\u001a\u001a!\u0010\u001c\u001a\u00020\u0003\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0080\b\u001a&\u0010\u001d\u001a\u00020\u0003\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u001e\u001a\u0019\u0010\u001f\u001a\u00020\u0013\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u0005H\u0080\b\u001a!\u0010 \u001a\u00020\u000e\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010!\u001a\u00020\u0003H\u0080\b\u001a.\u0010\"\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u0010\u001a)\u0010#\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u000e\u0010$\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\f0\u0005H\u0080\b\u001a0\u0010%\u001a\u0004\u0018\u0001H\f\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u001a\u001a!\u0010&\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0080\b\u001a.\u0010&\u001a\u00020\u0013\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010'\u001a!\u0010(\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010!\u001a\u00020\u0003H\u0080\b\u001a0\u0010)\u001a\u0004\u0018\u0001H\f\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010\u001a\u001a6\u0010)\u001a\u00020\u0013\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010*\u001a\u0002H\f2\u0006\u0010+\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010,\u001a.\u0010-\u001a\u00020\u000b\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010!\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u0002H\fH\u0080\b¢\u0006\u0002\u0010.\u001a\u0019\u0010/\u001a\u00020\u0003\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u0005H\u0080\b\u001a\u0019\u00100\u001a\u000201\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u0005H\u0080\b\u001a&\u00102\u001a\u0002H\f\"\u0004\b\u0000\u0010\f*\b\u0012\u0004\u0012\u0002H\f0\u00052\u0006\u0010!\u001a\u00020\u0003H\u0080\b¢\u0006\u0002\u00103\u001a!\u00104\u001a\u00020\u0013\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0086\n\u001aT\u00105\u001a\u00020\u000b\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u000526\u00106\u001a2\u0012\u0013\u0012\u00110\u000e¢\u0006\f\b8\u0012\b\b9\u0012\u0004\b\b(\r\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b8\u0012\b\b9\u0012\u0004\b\b(\u000f\u0012\u0004\u0012\u00020\u000b07H\u0086\b\u00f8\u0001\u0000\u001a.\u0010:\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u0002H\u0004H\u0086\b¢\u0006\u0002\u0010\u001a\u001a7\u0010;\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u0002H\u00040<H\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010=\u001a\u0019\u0010>\u001a\u00020\u0013\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0086\b\u001a\u0016\u0010?\u001a\u00020@\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005\u001a-\u0010A\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0005\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\f\u0010$\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0086\u0002\u001a-\u0010B\u001a\u00020\u0013\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\u0004H\u0007¢\u0006\u0002\u0010'\u001a.\u0010C\u001a\u00020\u000b\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\u0004H\u0086\n¢\u0006\u0002\u0010\u0010\u001a\u001c\u0010D\u001a\b\u0012\u0004\u0012\u0002H\u00040E\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"(\u0010\u0002\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00058\u00c6\u0002¢\u0006\f\u0012\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\t\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006F" }, d2 = { "DELETED", "", "size", "", "T", "Landroidx/collection/LongSparseArray;", "getSize$annotations", "(Landroidx/collection/LongSparseArray;)V", "getSize", "(Landroidx/collection/LongSparseArray;)I", "commonAppend", "", "E", "key", "", "value", "(Landroidx/collection/LongSparseArray;JLjava/lang/Object;)V", "commonClear", "commonContainsKey", "", "commonContainsValue", "(Landroidx/collection/LongSparseArray;Ljava/lang/Object;)Z", "commonGc", "commonGet", "(Landroidx/collection/LongSparseArray;J)Ljava/lang/Object;", "defaultValue", "(Landroidx/collection/LongSparseArray;JLjava/lang/Object;)Ljava/lang/Object;", "commonGetInternal", "commonIndexOfKey", "commonIndexOfValue", "(Landroidx/collection/LongSparseArray;Ljava/lang/Object;)I", "commonIsEmpty", "commonKeyAt", "index", "commonPut", "commonPutAll", "other", "commonPutIfAbsent", "commonRemove", "(Landroidx/collection/LongSparseArray;JLjava/lang/Object;)Z", "commonRemoveAt", "commonReplace", "oldValue", "newValue", "(Landroidx/collection/LongSparseArray;JLjava/lang/Object;Ljava/lang/Object;)Z", "commonSetValueAt", "(Landroidx/collection/LongSparseArray;ILjava/lang/Object;)V", "commonSize", "commonToString", "", "commonValueAt", "(Landroidx/collection/LongSparseArray;I)Ljava/lang/Object;", "contains", "forEach", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "getOrElse", "Lkotlin/Function0;", "(Landroidx/collection/LongSparseArray;JLkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "isNotEmpty", "keyIterator", "Lkotlin/collections/LongIterator;", "plus", "remove", "set", "valueIterator", "", "collection" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class LongSparseArrayKt
{
    private static final Object DELETED;
    
    static {
        DELETED = new Object();
    }
    
    public static final /* synthetic */ Object access$getDELETED$p() {
        return LongSparseArrayKt.DELETED;
    }
    
    public static final <E> void commonAppend(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (longSparseArray.size != 0 && n <= longSparseArray.keys[longSparseArray.size - 1]) {
            longSparseArray.put(n, e);
            return;
        }
        if (longSparseArray.garbage && longSparseArray.size >= longSparseArray.keys.length) {
            final int size = longSparseArray.size;
            final long[] keys = longSparseArray.keys;
            final Object[] values = longSparseArray.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n2 = size2;
                if (o != access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n2 = size2 + 1;
                }
                ++i;
                size2 = n2;
            }
            longSparseArray.garbage = false;
            longSparseArray.size = size2;
        }
        final int size3 = longSparseArray.size;
        if (size3 >= longSparseArray.keys.length) {
            final int idealLongArraySize = ContainerHelpersKt.idealLongArraySize(size3 + 1);
            final long[] copy = Arrays.copyOf(longSparseArray.keys, idealLongArraySize);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            longSparseArray.keys = copy;
            final Object[] copy2 = Arrays.copyOf(longSparseArray.values, idealLongArraySize);
            Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
            longSparseArray.values = copy2;
        }
        longSparseArray.keys[size3] = n;
        longSparseArray.values[size3] = e;
        longSparseArray.size = size3 + 1;
    }
    
    public static final <E> void commonClear(final LongSparseArray<E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int size = longSparseArray.size;
        final Object[] values = longSparseArray.values;
        for (int i = 0; i < size; ++i) {
            values[i] = null;
        }
        longSparseArray.size = 0;
        longSparseArray.garbage = false;
    }
    
    public static final <E> boolean commonContainsKey(final LongSparseArray<E> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <E> boolean commonContainsValue(final LongSparseArray<E> longSparseArray, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.indexOfValue(e) >= 0;
    }
    
    public static final <E> void commonGc(final LongSparseArray<E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int size = longSparseArray.size;
        final long[] keys = longSparseArray.keys;
        final Object[] values = longSparseArray.values;
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final Object o = values[i];
            int n = size2;
            if (o != access$getDELETED$p()) {
                if (i != size2) {
                    keys[size2] = keys[i];
                    values[size2] = o;
                    values[i] = null;
                }
                n = size2 + 1;
            }
            ++i;
            size2 = n;
        }
        longSparseArray.garbage = false;
        longSparseArray.size = size2;
    }
    
    public static final <E> E commonGet(final LongSparseArray<E> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
        Object o;
        if (binarySearch >= 0 && longSparseArray.values[binarySearch] != access$getDELETED$p()) {
            o = longSparseArray.values[binarySearch];
        }
        else {
            o = null;
        }
        return (E)o;
    }
    
    public static final <E> E commonGet(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
        Object o = e;
        if (binarySearch >= 0) {
            if (longSparseArray.values[binarySearch] == access$getDELETED$p()) {
                o = e;
            }
            else {
                o = longSparseArray.values[binarySearch];
            }
        }
        return (E)o;
    }
    
    public static final <T extends E, E> T commonGetInternal(final LongSparseArray<E> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
        Object o = t;
        if (binarySearch >= 0) {
            if (longSparseArray.values[binarySearch] == access$getDELETED$p()) {
                o = t;
            }
            else {
                o = longSparseArray.values[binarySearch];
            }
        }
        return (T)o;
    }
    
    public static final <E> int commonIndexOfKey(final LongSparseArray<E> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (longSparseArray.garbage) {
            final int size = longSparseArray.size;
            final long[] keys = longSparseArray.keys;
            final Object[] values = longSparseArray.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n2 = size2;
                if (o != access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n2 = size2 + 1;
                }
                ++i;
                size2 = n2;
            }
            longSparseArray.garbage = false;
            longSparseArray.size = size2;
        }
        return ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
    }
    
    public static final <E> int commonIndexOfValue(final LongSparseArray<E> longSparseArray, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final boolean garbage = longSparseArray.garbage;
        final int n = 0;
        if (garbage) {
            final int size = longSparseArray.size;
            final long[] keys = longSparseArray.keys;
            final Object[] values = longSparseArray.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n2 = size2;
                if (o != access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n2 = size2 + 1;
                }
                ++i;
                size2 = n2;
            }
            longSparseArray.garbage = false;
            longSparseArray.size = size2;
        }
        for (int size3 = longSparseArray.size, j = n; j < size3; ++j) {
            if (longSparseArray.values[j] == e) {
                return j;
            }
        }
        return -1;
    }
    
    public static final <E> boolean commonIsEmpty(final LongSparseArray<E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.size() == 0;
    }
    
    public static final <E> long commonKeyAt(final LongSparseArray<E> longSparseArray, final int i) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (i >= 0 && i < longSparseArray.size) {
            if (longSparseArray.garbage) {
                final int size = longSparseArray.size;
                final long[] keys = longSparseArray.keys;
                final Object[] values = longSparseArray.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                longSparseArray.garbage = false;
                longSparseArray.size = size2;
            }
            return longSparseArray.keys[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final <E> void commonPut(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
        if (binarySearch >= 0) {
            longSparseArray.values[binarySearch] = e;
        }
        else {
            final int n2 = ~binarySearch;
            if (n2 < longSparseArray.size && longSparseArray.values[n2] == access$getDELETED$p()) {
                longSparseArray.keys[n2] = n;
                longSparseArray.values[n2] = e;
                return;
            }
            int n3 = n2;
            if (longSparseArray.garbage) {
                n3 = n2;
                if (longSparseArray.size >= longSparseArray.keys.length) {
                    final int size = longSparseArray.size;
                    final long[] keys = longSparseArray.keys;
                    final Object[] values = longSparseArray.values;
                    int i = 0;
                    int size2 = 0;
                    while (i < size) {
                        final Object o = values[i];
                        int n4 = size2;
                        if (o != access$getDELETED$p()) {
                            if (i != size2) {
                                keys[size2] = keys[i];
                                values[size2] = o;
                                values[i] = null;
                            }
                            n4 = size2 + 1;
                        }
                        ++i;
                        size2 = n4;
                    }
                    longSparseArray.garbage = false;
                    longSparseArray.size = size2;
                    n3 = ~ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
                }
            }
            if (longSparseArray.size >= longSparseArray.keys.length) {
                final int idealLongArraySize = ContainerHelpersKt.idealLongArraySize(longSparseArray.size + 1);
                final long[] copy = Arrays.copyOf(longSparseArray.keys, idealLongArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                longSparseArray.keys = copy;
                final Object[] copy2 = Arrays.copyOf(longSparseArray.values, idealLongArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                longSparseArray.values = copy2;
            }
            if (longSparseArray.size - n3 != 0) {
                final long[] keys2 = longSparseArray.keys;
                final long[] keys3 = longSparseArray.keys;
                final int n5 = n3 + 1;
                ArraysKt.copyInto(keys2, keys3, n5, n3, longSparseArray.size);
                ArraysKt.copyInto(longSparseArray.values, longSparseArray.values, n5, n3, longSparseArray.size);
            }
            longSparseArray.keys[n3] = n;
            longSparseArray.values[n3] = e;
            ++longSparseArray.size;
        }
    }
    
    public static final <E> void commonPutAll(final LongSparseArray<E> longSparseArray, final LongSparseArray<? extends E> longSparseArray2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)longSparseArray2, "other");
        for (int size = longSparseArray2.size(), i = 0; i < size; ++i) {
            longSparseArray.put(longSparseArray2.keyAt(i), (E)longSparseArray2.valueAt(i));
        }
    }
    
    public static final <E> E commonPutIfAbsent(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final E value = longSparseArray.get(n);
        if (value == null) {
            longSparseArray.put(n, e);
        }
        return value;
    }
    
    public static final <E> void commonRemove(final LongSparseArray<E> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(longSparseArray.keys, longSparseArray.size, n);
        if (binarySearch >= 0 && longSparseArray.values[binarySearch] != access$getDELETED$p()) {
            longSparseArray.values[binarySearch] = access$getDELETED$p();
            longSparseArray.garbage = true;
        }
    }
    
    public static final <E> boolean commonRemove(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int indexOfKey = longSparseArray.indexOfKey(n);
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)e, longSparseArray.valueAt(indexOfKey))) {
            longSparseArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final <E> void commonRemoveAt(final LongSparseArray<E> longSparseArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (longSparseArray.values[n] != access$getDELETED$p()) {
            longSparseArray.values[n] = access$getDELETED$p();
            longSparseArray.garbage = true;
        }
    }
    
    public static final <E> E commonReplace(final LongSparseArray<E> longSparseArray, final long n, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int indexOfKey = longSparseArray.indexOfKey(n);
        if (indexOfKey >= 0) {
            final Object o = longSparseArray.values[indexOfKey];
            longSparseArray.values[indexOfKey] = e;
            return (E)o;
        }
        return null;
    }
    
    public static final <E> boolean commonReplace(final LongSparseArray<E> longSparseArray, final long n, final E e, final E e2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int indexOfKey = longSparseArray.indexOfKey(n);
        if (indexOfKey >= 0 && Intrinsics.areEqual(longSparseArray.values[indexOfKey], (Object)e)) {
            longSparseArray.values[indexOfKey] = e2;
            return true;
        }
        return false;
    }
    
    public static final <E> void commonSetValueAt(final LongSparseArray<E> longSparseArray, final int i, final E e) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (i >= 0 && i < longSparseArray.size) {
            if (longSparseArray.garbage) {
                final int size = longSparseArray.size;
                final long[] keys = longSparseArray.keys;
                final Object[] values = longSparseArray.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                longSparseArray.garbage = false;
                longSparseArray.size = size2;
            }
            longSparseArray.values[i] = e;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final <E> int commonSize(final LongSparseArray<E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (longSparseArray.garbage) {
            final int size = longSparseArray.size;
            final long[] keys = longSparseArray.keys;
            final Object[] values = longSparseArray.values;
            int i = 0;
            int size2 = 0;
            while (i < size) {
                final Object o = values[i];
                int n = size2;
                if (o != access$getDELETED$p()) {
                    if (i != size2) {
                        keys[size2] = keys[i];
                        values[size2] = o;
                        values[i] = null;
                    }
                    n = size2 + 1;
                }
                ++i;
                size2 = n;
            }
            longSparseArray.garbage = false;
            longSparseArray.size = size2;
        }
        return longSparseArray.size;
    }
    
    public static final <E> String commonToString(final LongSparseArray<E> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (longSparseArray.size() <= 0) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(longSparseArray.size * 28);
        sb.append('{');
        for (int i = 0; i < longSparseArray.size; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(longSparseArray.keyAt(i));
            sb.append('=');
            final Object value = longSparseArray.valueAt(i);
            if (value != sb) {
                sb.append(value);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
        return string;
    }
    
    public static final <E> E commonValueAt(final LongSparseArray<E> longSparseArray, final int i) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        if (i >= 0 && i < longSparseArray.size) {
            if (longSparseArray.garbage) {
                final int size = longSparseArray.size;
                final long[] keys = longSparseArray.keys;
                final Object[] values = longSparseArray.values;
                int j = 0;
                int size2 = 0;
                while (j < size) {
                    final Object o = values[j];
                    int n = size2;
                    if (o != access$getDELETED$p()) {
                        if (j != size2) {
                            keys[size2] = keys[j];
                            values[size2] = o;
                            values[j] = null;
                        }
                        n = size2 + 1;
                    }
                    ++j;
                    size2 = n;
                }
                longSparseArray.garbage = false;
                longSparseArray.size = size2;
            }
            return (E)longSparseArray.values[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final <T> boolean contains(final LongSparseArray<T> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.containsKey(n);
    }
    
    public static final <T> void forEach(final LongSparseArray<T> longSparseArray, final Function2<? super Long, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = longSparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)longSparseArray.keyAt(i), longSparseArray.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.get(n, t);
    }
    
    public static final <T> T getOrElse(final LongSparseArray<T> longSparseArray, final long n, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        Object o;
        if ((o = longSparseArray.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.size();
    }
    
    public static final <T> boolean isNotEmpty(final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.isEmpty() ^ true;
    }
    
    public static final <T> LongIterator keyIterator(final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return (LongIterator)new LongSparseArrayKt$keyIterator.LongSparseArrayKt$keyIterator$1((LongSparseArray)longSparseArray);
    }
    
    public static final <T> LongSparseArray<T> plus(final LongSparseArray<T> longSparseArray, final LongSparseArray<T> longSparseArray2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)longSparseArray2, "other");
        final LongSparseArray longSparseArray3 = new LongSparseArray(longSparseArray.size() + longSparseArray2.size());
        longSparseArray3.putAll(longSparseArray);
        longSparseArray3.putAll(longSparseArray2);
        return longSparseArray3;
    }
    
    public static final <T> void set(final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        longSparseArray.put(n, t);
    }
    
    public static final <T> Iterator<T> valueIterator(final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return (Iterator<T>)new LongSparseArrayKt$valueIterator.LongSparseArrayKt$valueIterator$1((LongSparseArray)longSparseArray);
    }
}

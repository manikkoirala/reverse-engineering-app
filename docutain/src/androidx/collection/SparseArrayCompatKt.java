// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.ArraysKt;
import java.util.Arrays;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0010\u000e\n\u0002\b\u0005\u001a.\u0010\u0002\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\t\u001a\u0019\u0010\n\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0080\b\u001a!\u0010\u000b\u001a\u00020\f\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0080\b\u001a&\u0010\r\u001a\u00020\f\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\u000e\u001a'\u0010\u000f\u001a\u0004\u0018\u0001H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0000¢\u0006\u0002\u0010\u0010\u001a-\u0010\u000f\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u0002H\u0004H\u0000¢\u0006\u0002\u0010\u0012\u001a!\u0010\u0013\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0080\b\u001a&\u0010\u0014\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\u0015\u001a\u0019\u0010\u0016\u001a\u00020\f\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0080\b\u001a!\u0010\u0017\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0018\u001a\u00020\u0007H\u0080\b\u001a.\u0010\u0019\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\t\u001a)\u0010\u001a\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u000e\u0010\u001b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00040\u0005H\u0080\b\u001a0\u0010\u001c\u001a\u0004\u0018\u0001H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\u0012\u001a \u0010\u001d\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0000\u001a+\u0010\u001d\u001a\u00020\f\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0001H\u0080\b\u001a!\u0010\u001e\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0018\u001a\u00020\u0007H\u0080\b\u001a)\u0010\u001f\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0018\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u0007H\u0080\b\u001a0\u0010!\u001a\u0004\u0018\u0001H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\u0012\u001a6\u0010!\u001a\u00020\f\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\"\u001a\u0002H\u00042\u0006\u0010#\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010$\u001a.\u0010%\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0018\u001a\u00020\u00072\u0006\u0010\b\u001a\u0002H\u0004H\u0080\b¢\u0006\u0002\u0010\t\u001a\u0019\u0010&\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0080\b\u001a\u0019\u0010'\u001a\u00020(\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0080\b\u001a&\u0010)\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0018\u001a\u00020\u0007H\u0080\b¢\u0006\u0002\u0010\u0010\u001a\u0018\u0010*\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H\u0002\u001a:\u0010+\u001a\u0002H,\"\u0004\b\u0000\u0010\u0004\"\n\b\u0001\u0010,*\u0004\u0018\u0001H\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u0002H,H\u0082\b¢\u0006\u0002\u0010\u0012\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "DELETED", "", "commonAppend", "", "E", "Landroidx/collection/SparseArrayCompat;", "key", "", "value", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;)V", "commonClear", "commonContainsKey", "", "commonContainsValue", "(Landroidx/collection/SparseArrayCompat;Ljava/lang/Object;)Z", "commonGet", "(Landroidx/collection/SparseArrayCompat;I)Ljava/lang/Object;", "defaultValue", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;)Ljava/lang/Object;", "commonIndexOfKey", "commonIndexOfValue", "(Landroidx/collection/SparseArrayCompat;Ljava/lang/Object;)I", "commonIsEmpty", "commonKeyAt", "index", "commonPut", "commonPutAll", "other", "commonPutIfAbsent", "commonRemove", "commonRemoveAt", "commonRemoveAtRange", "size", "commonReplace", "oldValue", "newValue", "(Landroidx/collection/SparseArrayCompat;ILjava/lang/Object;Ljava/lang/Object;)Z", "commonSetValueAt", "commonSize", "commonToString", "", "commonValueAt", "gc", "internalGet", "T", "collection" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SparseArrayCompatKt
{
    private static final Object DELETED;
    
    static {
        DELETED = new Object();
    }
    
    public static final /* synthetic */ Object access$getDELETED$p() {
        return SparseArrayCompatKt.DELETED;
    }
    
    public static final <E> void commonAppend(final SparseArrayCompat<E> sparseArrayCompat, final int n, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.size != 0 && n <= sparseArrayCompat.keys[sparseArrayCompat.size - 1]) {
            sparseArrayCompat.put(n, e);
            return;
        }
        if (sparseArrayCompat.garbage && sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        final int size = sparseArrayCompat.size;
        if (size >= sparseArrayCompat.keys.length) {
            final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(size + 1);
            final int[] copy = Arrays.copyOf(sparseArrayCompat.keys, idealIntArraySize);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            sparseArrayCompat.keys = copy;
            final Object[] copy2 = Arrays.copyOf(sparseArrayCompat.values, idealIntArraySize);
            Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
            sparseArrayCompat.values = copy2;
        }
        sparseArrayCompat.keys[size] = n;
        sparseArrayCompat.values[size] = e;
        sparseArrayCompat.size = size + 1;
    }
    
    public static final <E> void commonClear(final SparseArrayCompat<E> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        final int size = sparseArrayCompat.size;
        final Object[] values = sparseArrayCompat.values;
        for (int i = 0; i < size; ++i) {
            values[i] = null;
        }
        sparseArrayCompat.size = 0;
        sparseArrayCompat.garbage = false;
    }
    
    public static final <E> boolean commonContainsKey(final SparseArrayCompat<E> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.indexOfKey(n) >= 0;
    }
    
    public static final <E> boolean commonContainsValue(final SparseArrayCompat<E> sparseArrayCompat, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        final int size = sparseArrayCompat.size;
        boolean b = false;
        while (true) {
            for (int i = 0; i < size; ++i) {
                if (sparseArrayCompat.values[i] == e) {
                    if (i >= 0) {
                        b = true;
                    }
                    return b;
                }
            }
            int i = -1;
            continue;
        }
    }
    
    public static final <E> E commonGet(final SparseArrayCompat<E> sparseArrayCompat, int binarySearch) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, binarySearch);
        Object o;
        if (binarySearch >= 0 && sparseArrayCompat.values[binarySearch] != SparseArrayCompatKt.DELETED) {
            o = sparseArrayCompat.values[binarySearch];
        }
        else {
            o = null;
        }
        return (E)o;
    }
    
    public static final <E> E commonGet(final SparseArrayCompat<E> sparseArrayCompat, int binarySearch, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, binarySearch);
        Object o = e;
        if (binarySearch >= 0) {
            if (sparseArrayCompat.values[binarySearch] == SparseArrayCompatKt.DELETED) {
                o = e;
            }
            else {
                o = sparseArrayCompat.values[binarySearch];
            }
        }
        return (E)o;
    }
    
    public static final <E> int commonIndexOfKey(final SparseArrayCompat<E> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        return ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, n);
    }
    
    public static final <E> int commonIndexOfValue(final SparseArrayCompat<E> sparseArrayCompat, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        for (int i = 0; i < sparseArrayCompat.size; ++i) {
            if (sparseArrayCompat.values[i] == e) {
                return i;
            }
        }
        return -1;
    }
    
    public static final <E> boolean commonIsEmpty(final SparseArrayCompat<E> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        return sparseArrayCompat.size() == 0;
    }
    
    public static final <E> int commonKeyAt(final SparseArrayCompat<E> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        return sparseArrayCompat.keys[n];
    }
    
    public static final <E> void commonPut(final SparseArrayCompat<E> sparseArrayCompat, final int n, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        final int binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, n);
        if (binarySearch >= 0) {
            sparseArrayCompat.values[binarySearch] = e;
        }
        else {
            final int n2 = ~binarySearch;
            if (n2 < sparseArrayCompat.size && sparseArrayCompat.values[n2] == access$getDELETED$p()) {
                sparseArrayCompat.keys[n2] = n;
                sparseArrayCompat.values[n2] = e;
                return;
            }
            int n3 = n2;
            if (sparseArrayCompat.garbage) {
                n3 = n2;
                if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                    gc((SparseArrayCompat<Object>)sparseArrayCompat);
                    n3 = ~ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, n);
                }
            }
            if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(sparseArrayCompat.size + 1);
                final int[] copy = Arrays.copyOf(sparseArrayCompat.keys, idealIntArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                sparseArrayCompat.keys = copy;
                final Object[] copy2 = Arrays.copyOf(sparseArrayCompat.values, idealIntArraySize);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                sparseArrayCompat.values = copy2;
            }
            if (sparseArrayCompat.size - n3 != 0) {
                final int[] keys = sparseArrayCompat.keys;
                final int[] keys2 = sparseArrayCompat.keys;
                final int n4 = n3 + 1;
                ArraysKt.copyInto(keys, keys2, n4, n3, sparseArrayCompat.size);
                ArraysKt.copyInto(sparseArrayCompat.values, sparseArrayCompat.values, n4, n3, sparseArrayCompat.size);
            }
            sparseArrayCompat.keys[n3] = n;
            sparseArrayCompat.values[n3] = e;
            ++sparseArrayCompat.size;
        }
    }
    
    public static final <E> void commonPutAll(final SparseArrayCompat<E> sparseArrayCompat, final SparseArrayCompat<? extends E> sparseArrayCompat2) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat2, "other");
        for (int size = sparseArrayCompat2.size(), i = 0; i < size; ++i) {
            final int key = sparseArrayCompat2.keyAt(i);
            final Object value = sparseArrayCompat2.valueAt(i);
            final int binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, key);
            if (binarySearch >= 0) {
                sparseArrayCompat.values[binarySearch] = value;
            }
            else {
                final int n = ~binarySearch;
                if (n < sparseArrayCompat.size && sparseArrayCompat.values[n] == access$getDELETED$p()) {
                    sparseArrayCompat.keys[n] = key;
                    sparseArrayCompat.values[n] = value;
                }
                else {
                    int n2 = n;
                    if (sparseArrayCompat.garbage) {
                        n2 = n;
                        if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                            gc((SparseArrayCompat<Object>)sparseArrayCompat);
                            n2 = ~ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, key);
                        }
                    }
                    if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                        final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(sparseArrayCompat.size + 1);
                        final int[] copy = Arrays.copyOf(sparseArrayCompat.keys, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                        sparseArrayCompat.keys = copy;
                        final Object[] copy2 = Arrays.copyOf(sparseArrayCompat.values, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                        sparseArrayCompat.values = copy2;
                    }
                    if (sparseArrayCompat.size - n2 != 0) {
                        final int[] keys = sparseArrayCompat.keys;
                        final int[] keys2 = sparseArrayCompat.keys;
                        final int n3 = n2 + 1;
                        ArraysKt.copyInto(keys, keys2, n3, n2, sparseArrayCompat.size);
                        ArraysKt.copyInto(sparseArrayCompat.values, sparseArrayCompat.values, n3, n2, sparseArrayCompat.size);
                    }
                    sparseArrayCompat.keys[n2] = key;
                    sparseArrayCompat.values[n2] = value;
                    ++sparseArrayCompat.size;
                }
            }
        }
    }
    
    public static final <E> E commonPutIfAbsent(final SparseArrayCompat<E> sparseArrayCompat, final int n, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        final E commonGet = commonGet(sparseArrayCompat, n);
        if (commonGet == null) {
            final int binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, n);
            if (binarySearch >= 0) {
                sparseArrayCompat.values[binarySearch] = e;
            }
            else {
                final int n2 = ~binarySearch;
                if (n2 < sparseArrayCompat.size && sparseArrayCompat.values[n2] == access$getDELETED$p()) {
                    sparseArrayCompat.keys[n2] = n;
                    sparseArrayCompat.values[n2] = e;
                }
                else {
                    int n3 = n2;
                    if (sparseArrayCompat.garbage) {
                        n3 = n2;
                        if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                            gc((SparseArrayCompat<Object>)sparseArrayCompat);
                            n3 = ~ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, n);
                        }
                    }
                    if (sparseArrayCompat.size >= sparseArrayCompat.keys.length) {
                        final int idealIntArraySize = ContainerHelpersKt.idealIntArraySize(sparseArrayCompat.size + 1);
                        final int[] copy = Arrays.copyOf(sparseArrayCompat.keys, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                        sparseArrayCompat.keys = copy;
                        final Object[] copy2 = Arrays.copyOf(sparseArrayCompat.values, idealIntArraySize);
                        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                        sparseArrayCompat.values = copy2;
                    }
                    if (sparseArrayCompat.size - n3 != 0) {
                        final int[] keys = sparseArrayCompat.keys;
                        final int[] keys2 = sparseArrayCompat.keys;
                        final int n4 = n3 + 1;
                        ArraysKt.copyInto(keys, keys2, n4, n3, sparseArrayCompat.size);
                        ArraysKt.copyInto(sparseArrayCompat.values, sparseArrayCompat.values, n4, n3, sparseArrayCompat.size);
                    }
                    sparseArrayCompat.keys[n3] = n;
                    sparseArrayCompat.values[n3] = e;
                    ++sparseArrayCompat.size;
                }
            }
        }
        return commonGet;
    }
    
    public static final <E> void commonRemove(final SparseArrayCompat<E> sparseArrayCompat, int binarySearch) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, binarySearch);
        if (binarySearch >= 0) {
            final Object o = sparseArrayCompat.values[binarySearch];
            final Object deleted = SparseArrayCompatKt.DELETED;
            if (o != deleted) {
                sparseArrayCompat.values[binarySearch] = deleted;
                sparseArrayCompat.garbage = true;
            }
        }
    }
    
    public static final <E> boolean commonRemove(final SparseArrayCompat<E> sparseArrayCompat, int indexOfKey, final Object o) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        indexOfKey = sparseArrayCompat.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && Intrinsics.areEqual(o, sparseArrayCompat.valueAt(indexOfKey))) {
            sparseArrayCompat.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final <E> void commonRemoveAt(final SparseArrayCompat<E> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.values[n] != access$getDELETED$p()) {
            sparseArrayCompat.values[n] = access$getDELETED$p();
            sparseArrayCompat.garbage = true;
        }
    }
    
    public static final <E> void commonRemoveAtRange(final SparseArrayCompat<E> sparseArrayCompat, int i, int min) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        for (min = Math.min(min, i + min); i < min; ++i) {
            sparseArrayCompat.removeAt(i);
        }
    }
    
    public static final <E> E commonReplace(final SparseArrayCompat<E> sparseArrayCompat, int indexOfKey, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        indexOfKey = sparseArrayCompat.indexOfKey(indexOfKey);
        if (indexOfKey >= 0) {
            final Object o = sparseArrayCompat.values[indexOfKey];
            sparseArrayCompat.values[indexOfKey] = e;
            return (E)o;
        }
        return null;
    }
    
    public static final <E> boolean commonReplace(final SparseArrayCompat<E> sparseArrayCompat, int indexOfKey, final E e, final E e2) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        indexOfKey = sparseArrayCompat.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && Intrinsics.areEqual(sparseArrayCompat.values[indexOfKey], (Object)e)) {
            sparseArrayCompat.values[indexOfKey] = e2;
            return true;
        }
        return false;
    }
    
    public static final <E> void commonSetValueAt(final SparseArrayCompat<E> sparseArrayCompat, final int n, final E e) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        sparseArrayCompat.values[n] = e;
    }
    
    public static final <E> int commonSize(final SparseArrayCompat<E> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        return sparseArrayCompat.size;
    }
    
    public static final <E> String commonToString(final SparseArrayCompat<E> sparseArrayCompat) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.size() <= 0) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(sparseArrayCompat.size * 28);
        sb.append('{');
        for (int i = 0; i < sparseArrayCompat.size; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(sparseArrayCompat.keyAt(i));
            sb.append('=');
            final Object value = sparseArrayCompat.valueAt(i);
            if (value != sparseArrayCompat) {
                sb.append(value);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "buffer.toString()");
        return string;
    }
    
    public static final <E> E commonValueAt(final SparseArrayCompat<E> sparseArrayCompat, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArrayCompat, "<this>");
        if (sparseArrayCompat.garbage) {
            gc((SparseArrayCompat<Object>)sparseArrayCompat);
        }
        return (E)sparseArrayCompat.values[n];
    }
    
    private static final <E> void gc(final SparseArrayCompat<E> sparseArrayCompat) {
        final int size = sparseArrayCompat.size;
        final int[] keys = sparseArrayCompat.keys;
        final Object[] values = sparseArrayCompat.values;
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final Object o = values[i];
            int n = size2;
            if (o != SparseArrayCompatKt.DELETED) {
                if (i != size2) {
                    keys[size2] = keys[i];
                    values[size2] = o;
                    values[i] = null;
                }
                n = size2 + 1;
            }
            ++i;
            size2 = n;
        }
        sparseArrayCompat.garbage = false;
        sparseArrayCompat.size = size2;
    }
    
    private static final <E, T extends E> T internalGet(final SparseArrayCompat<E> sparseArrayCompat, int binarySearch, final T t) {
        binarySearch = ContainerHelpersKt.binarySearch(sparseArrayCompat.keys, sparseArrayCompat.size, binarySearch);
        Object o = t;
        if (binarySearch >= 0) {
            if (sparseArrayCompat.values[binarySearch] == SparseArrayCompatKt.DELETED) {
                o = t;
            }
            else {
                o = sparseArrayCompat.values[binarySearch];
            }
        }
        return (T)o;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.jvm.internal.Intrinsics;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0013\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000¢\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000¢\u0006\u0002\u0010\u0014J\u0006\u0010\u0016\u001a\u00020\u0012J\b\u0010\u0017\u001a\u00020\u0012H\u0002J\u0016\u0010\u0018\u001a\u00028\u00002\u0006\u0010\u0019\u001a\u00020\u0004H\u0086\u0002¢\u0006\u0002\u0010\u001aJ\u0006\u0010\u001b\u001a\u00020\u001cJ\u000b\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010\fJ\u000b\u0010\u001e\u001a\u00028\u0000¢\u0006\u0002\u0010\fJ\u000e\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\u0004J\u000e\u0010!\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\u0004J\u0006\u0010\"\u001a\u00020\u0004R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\bX\u0082\u000e¢\u0006\u0004\n\u0002\u0010\tR\u0011\u0010\n\u001a\u00028\u00008F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u000e\u001a\u00028\u00008F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\fR\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Landroidx/collection/CircularArray;", "E", "", "minCapacity", "", "(I)V", "capacityBitmask", "elements", "", "[Ljava/lang/Object;", "first", "getFirst", "()Ljava/lang/Object;", "head", "last", "getLast", "tail", "addFirst", "", "element", "(Ljava/lang/Object;)V", "addLast", "clear", "doubleCapacity", "get", "index", "(I)Ljava/lang/Object;", "isEmpty", "", "popFirst", "popLast", "removeFromEnd", "count", "removeFromStart", "size", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class CircularArray<E>
{
    private int capacityBitmask;
    private E[] elements;
    private int head;
    private int tail;
    
    public CircularArray() {
        this(0, 1, null);
    }
    
    public CircularArray(final int i) {
        final int n = 0;
        if (i < 1) {
            throw new IllegalArgumentException("capacity must be >= 1".toString());
        }
        int n2 = n;
        if (i <= 1073741824) {
            n2 = 1;
        }
        if (n2 != 0) {
            int n3 = i;
            if (Integer.bitCount(i) != 1) {
                n3 = Integer.highestOneBit(i - 1) << 1;
            }
            this.capacityBitmask = n3 - 1;
            this.elements = (E[])new Object[n3];
            return;
        }
        throw new IllegalArgumentException("capacity must be <= 2^30".toString());
    }
    
    private final void doubleCapacity() {
        final E[] elements = this.elements;
        final int length = elements.length;
        final int head = this.head;
        final int n = length << 1;
        if (n >= 0) {
            final Object[] elements2 = new Object[n];
            ArraysKt.copyInto((Object[])elements, elements2, 0, head, length);
            ArraysKt.copyInto((Object[])this.elements, elements2, length - head, 0, this.head);
            this.elements = (E[])elements2;
            this.head = 0;
            this.tail = length;
            this.capacityBitmask = n - 1;
            return;
        }
        throw new RuntimeException("Max array capacity exceeded");
    }
    
    public final void addFirst(final E e) {
        final int head = this.head - 1 & this.capacityBitmask;
        this.head = head;
        this.elements[head] = e;
        if (head == this.tail) {
            this.doubleCapacity();
        }
    }
    
    public final void addLast(final E e) {
        final E[] elements = this.elements;
        final int tail = this.tail;
        elements[tail] = e;
        final int tail2 = this.capacityBitmask & tail + 1;
        this.tail = tail2;
        if (tail2 == this.head) {
            this.doubleCapacity();
        }
    }
    
    public final void clear() {
        this.removeFromStart(this.size());
    }
    
    public final E get(final int n) {
        if (n >= 0 && n < this.size()) {
            final E e = this.elements[this.capacityBitmask & this.head + n];
            Intrinsics.checkNotNull((Object)e);
            return e;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final E getFirst() {
        final int head = this.head;
        if (head != this.tail) {
            final E e = this.elements[head];
            Intrinsics.checkNotNull((Object)e);
            return e;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final E getLast() {
        final int head = this.head;
        final int tail = this.tail;
        if (head != tail) {
            final E e = this.elements[tail - 1 & this.capacityBitmask];
            Intrinsics.checkNotNull((Object)e);
            return e;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final boolean isEmpty() {
        return this.head == this.tail;
    }
    
    public final E popFirst() {
        final int head = this.head;
        if (head != this.tail) {
            final E[] elements = this.elements;
            final E e = elements[head];
            elements[head] = null;
            this.head = (head + 1 & this.capacityBitmask);
            return e;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final E popLast() {
        final int head = this.head;
        final int tail = this.tail;
        if (head != tail) {
            final int tail2 = this.capacityBitmask & tail - 1;
            final E[] elements = this.elements;
            final E e = elements[tail2];
            elements[tail2] = null;
            this.tail = tail2;
            return e;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final void removeFromEnd(int i) {
        if (i <= 0) {
            return;
        }
        if (i <= this.size()) {
            int n = 0;
            final int tail = this.tail;
            if (i < tail) {
                n = tail - i;
            }
            for (int j = n; j < tail; ++j) {
                this.elements[j] = null;
            }
            final int tail2 = this.tail;
            final int n2 = tail2 - n;
            i -= n2;
            this.tail = tail2 - n2;
            if (i > 0) {
                final int length = this.elements.length;
                this.tail = length;
                int tail3;
                for (tail3 = (i = length - i); i < length; ++i) {
                    this.elements[i] = null;
                }
                this.tail = tail3;
            }
            return;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final void removeFromStart(int i) {
        if (i <= 0) {
            return;
        }
        if (i <= this.size()) {
            final int length = this.elements.length;
            final int head = this.head;
            int n = length;
            int j = head;
            if (i < length - head) {
                n = head + i;
                j = head;
            }
            while (j < n) {
                this.elements[j] = null;
                ++j;
            }
            final int head2 = this.head;
            final int n2 = n - head2;
            final int head3 = i - n2;
            this.head = (this.capacityBitmask & head2 + n2);
            if (head3 > 0) {
                for (i = 0; i < head3; ++i) {
                    this.elements[i] = null;
                }
                this.head = head3;
            }
            return;
        }
        final CollectionPlatformUtils instance = CollectionPlatformUtils.INSTANCE;
        throw new ArrayIndexOutOfBoundsException();
    }
    
    public final int size() {
        return this.tail - this.head & this.capacityBitmask;
    }
}

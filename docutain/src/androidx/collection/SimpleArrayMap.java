// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.collections.ArraysKt;
import java.util.Map;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import kotlin.jvm.internal.Intrinsics;
import androidx.collection.internal.ContainerHelpersKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b)\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003B!\b\u0016\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0018\u00010\u0000¢\u0006\u0002\u0010\u0005B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0015\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u0007H\u0016J\u0013\u0010\u0019\u001a\u00020\u00122\b\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u0096\u0002J\u0018\u0010\u001b\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0013\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u001cJ\u001f\u0010\u001d\u001a\u00028\u00012\b\u0010\u0013\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u001e\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001fJ,\u0010 \u001a\u0002H!\"\n\b\u0002\u0010!*\u0004\u0018\u00018\u00012\b\u0010\u0013\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u001e\u001a\u0002H!H\u0082\b¢\u0006\u0002\u0010\u001fJ\b\u0010\"\u001a\u00020\u0007H\u0016J\u001d\u0010#\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010$\u001a\u00020\u0007H\u0002¢\u0006\u0002\u0010%J\u0015\u0010&\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010'J\b\u0010(\u001a\u00020\u0007H\u0002J\u0017\u0010)\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00028\u0001H\u0001¢\u0006\u0004\b*\u0010'J\b\u0010+\u001a\u00020\u0012H\u0016J\u0015\u0010,\u001a\u00028\u00002\u0006\u0010-\u001a\u00020\u0007H\u0016¢\u0006\u0002\u0010.J\u001f\u0010/\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001fJ \u00100\u001a\u00020\u00102\u0016\u0010\u0004\u001a\u0012\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u00010\u0000H\u0016J\u001f\u00101\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001fJ\u0017\u00102\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001cJ\u001d\u00102\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u00103J\u0015\u00104\u001a\u00028\u00012\u0006\u0010-\u001a\u00020\u0007H\u0016¢\u0006\u0002\u0010.J\u001f\u00105\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001fJ%\u00105\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u00106\u001a\u00028\u00012\u0006\u00107\u001a\u00028\u0001H\u0016¢\u0006\u0002\u00108J\u001d\u00109\u001a\u00028\u00012\u0006\u0010-\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010:J\b\u0010\u000e\u001a\u00020\u0007H\u0016J\b\u0010;\u001a\u00020<H\u0016J\u0015\u0010=\u001a\u00028\u00012\u0006\u0010-\u001a\u00020\u0007H\u0016¢\u0006\u0002\u0010.R\u0018\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\nX\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006>" }, d2 = { "Landroidx/collection/SimpleArrayMap;", "K", "V", "", "map", "(Landroidx/collection/SimpleArrayMap;)V", "capacity", "", "(I)V", "array", "", "[Ljava/lang/Object;", "hashes", "", "size", "clear", "", "containsKey", "", "key", "(Ljava/lang/Object;)Z", "containsValue", "value", "ensureCapacity", "minimumCapacity", "equals", "other", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "getOrDefault", "defaultValue", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "getOrDefaultInternal", "T", "hashCode", "indexOf", "hash", "(Ljava/lang/Object;I)I", "indexOfKey", "(Ljava/lang/Object;)I", "indexOfNull", "indexOfValue", "__restricted$indexOfValue", "isEmpty", "keyAt", "index", "(I)Ljava/lang/Object;", "put", "putAll", "putIfAbsent", "remove", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "removeAt", "replace", "oldValue", "newValue", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z", "setValueAt", "(ILjava/lang/Object;)Ljava/lang/Object;", "toString", "", "valueAt", "collection" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class SimpleArrayMap<K, V>
{
    private Object[] array;
    private int[] hashes;
    private int size;
    
    public SimpleArrayMap() {
        this(0, 1, null);
    }
    
    public SimpleArrayMap(final int n) {
        int[] empty_INTS;
        if (n == 0) {
            empty_INTS = ContainerHelpersKt.EMPTY_INTS;
        }
        else {
            empty_INTS = new int[n];
        }
        this.hashes = empty_INTS;
        Object[] empty_OBJECTS;
        if (n == 0) {
            empty_OBJECTS = ContainerHelpersKt.EMPTY_OBJECTS;
        }
        else {
            empty_OBJECTS = new Object[n << 1];
        }
        this.array = empty_OBJECTS;
    }
    
    public SimpleArrayMap(final SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this(0, 1, null);
        if (simpleArrayMap != null) {
            this.putAll(simpleArrayMap);
        }
    }
    
    private final <T extends V> T getOrDefaultInternal(final Object o, T t) {
        final int indexOfKey = this.indexOfKey(o);
        if (indexOfKey >= 0) {
            t = (T)this.array[(indexOfKey << 1) + 1];
        }
        return t;
    }
    
    private final int indexOf(final K k, final int n) {
        final int size = this.size;
        if (size == 0) {
            return -1;
        }
        int binarySearch = ContainerHelpersKt.binarySearch(this.hashes, size, n);
        if (binarySearch < 0) {
            return binarySearch;
        }
        if (Intrinsics.areEqual((Object)k, this.array[binarySearch << 1])) {
            return binarySearch;
        }
        int n2;
        for (n2 = binarySearch + 1; n2 < size && this.hashes[n2] == n; ++n2) {
            if (Intrinsics.areEqual((Object)k, this.array[n2 << 1])) {
                return n2;
            }
        }
        --binarySearch;
        while (binarySearch >= 0 && this.hashes[binarySearch] == n) {
            if (Intrinsics.areEqual((Object)k, this.array[binarySearch << 1])) {
                return binarySearch;
            }
            --binarySearch;
        }
        return ~n2;
    }
    
    private final int indexOfNull() {
        final int size = this.size;
        if (size == 0) {
            return -1;
        }
        final int binarySearch = ContainerHelpersKt.binarySearch(this.hashes, size, 0);
        if (binarySearch < 0) {
            return binarySearch;
        }
        if (this.array[binarySearch << 1] == null) {
            return binarySearch;
        }
        int n;
        for (n = binarySearch + 1; n < size && this.hashes[n] == 0; ++n) {
            if (this.array[n << 1] == null) {
                return n;
            }
        }
        for (int n2 = binarySearch - 1; n2 >= 0 && this.hashes[n2] == 0; --n2) {
            if (this.array[n2 << 1] == null) {
                return n2;
            }
        }
        return ~n;
    }
    
    public final int __restricted$indexOfValue(final V v) {
        final int n = this.size * 2;
        final Object[] array = this.array;
        if (v == null) {
            for (int i = 1; i < n; i += 2) {
                if (array[i] == null) {
                    return i >> 1;
                }
            }
        }
        else {
            for (int j = 1; j < n; j += 2) {
                if (Intrinsics.areEqual((Object)v, array[j])) {
                    return j >> 1;
                }
            }
        }
        return -1;
    }
    
    public void clear() {
        if (this.size > 0) {
            this.hashes = ContainerHelpersKt.EMPTY_INTS;
            this.array = ContainerHelpersKt.EMPTY_OBJECTS;
            this.size = 0;
        }
        if (this.size <= 0) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public boolean containsKey(final K k) {
        return this.indexOfKey(k) >= 0;
    }
    
    public boolean containsValue(final V v) {
        return this.__restricted$indexOfValue(v) >= 0;
    }
    
    public void ensureCapacity(final int newLength) {
        final int size = this.size;
        final int[] hashes = this.hashes;
        if (hashes.length < newLength) {
            final int[] copy = Arrays.copyOf(hashes, newLength);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            this.hashes = copy;
            final Object[] copy2 = Arrays.copyOf(this.array, newLength * 2);
            Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
            this.array = copy2;
        }
        if (this.size == size) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    @Override
    public boolean equals(Object key) {
        if (this == key) {
            return true;
        }
        try {
            if (key instanceof SimpleArrayMap) {
                if (this.size() != ((SimpleArrayMap)key).size()) {
                    return false;
                }
                final SimpleArrayMap simpleArrayMap = (SimpleArrayMap)key;
                for (int size = this.size, i = 0; i < size; ++i) {
                    key = this.keyAt(i);
                    final V value = this.valueAt(i);
                    final V value2 = simpleArrayMap.get(key);
                    if (value == null) {
                        if (value2 != null || !simpleArrayMap.containsKey(key)) {
                            return false;
                        }
                    }
                    else if (!Intrinsics.areEqual((Object)value, (Object)value2)) {
                        return false;
                    }
                }
                return true;
            }
            else {
                if (!(key instanceof Map)) {
                    return false;
                }
                if (this.size() != ((Map)key).size()) {
                    return false;
                }
                for (int size2 = this.size, j = 0; j < size2; ++j) {
                    final K key2 = this.keyAt(j);
                    final V value3 = this.valueAt(j);
                    final Object value4 = ((Map)key).get(key2);
                    if (value3 == null) {
                        if (value4 != null || !((Map)key).containsKey(key2)) {
                            return false;
                        }
                    }
                    else if (!Intrinsics.areEqual((Object)value3, value4)) {
                        return false;
                    }
                }
                return true;
            }
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public V get(final K k) {
        final int indexOfKey = this.indexOfKey(k);
        Object o;
        if (indexOfKey >= 0) {
            o = this.array[(indexOfKey << 1) + 1];
        }
        else {
            o = null;
        }
        return (V)o;
    }
    
    public V getOrDefault(final Object o, V v) {
        final int indexOfKey = this.indexOfKey(o);
        if (indexOfKey >= 0) {
            v = (V)this.array[(indexOfKey << 1) + 1];
        }
        return v;
    }
    
    @Override
    public int hashCode() {
        final int[] hashes = this.hashes;
        final Object[] array = this.array;
        final int size = this.size;
        int n = 1;
        int i = 0;
        int n2 = 0;
        while (i < size) {
            final Object o = array[n];
            final int n3 = hashes[i];
            int hashCode;
            if (o != null) {
                hashCode = o.hashCode();
            }
            else {
                hashCode = 0;
            }
            n2 += (hashCode ^ n3);
            ++i;
            n += 2;
        }
        return n2;
    }
    
    public int indexOfKey(final K k) {
        int n;
        if (k == null) {
            n = this.indexOfNull();
        }
        else {
            n = this.indexOf(k, k.hashCode());
        }
        return n;
    }
    
    public boolean isEmpty() {
        return this.size <= 0;
    }
    
    public K keyAt(final int i) {
        int n = 0;
        if (i >= 0) {
            n = n;
            if (i < this.size) {
                n = 1;
            }
        }
        if (n != 0) {
            return (K)this.array[i << 1];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public V put(final K k, final V v) {
        final int size = this.size;
        int hashCode;
        if (k != null) {
            hashCode = k.hashCode();
        }
        else {
            hashCode = 0;
        }
        int n;
        if (k != null) {
            n = this.indexOf(k, hashCode);
        }
        else {
            n = this.indexOfNull();
        }
        if (n >= 0) {
            final int n2 = (n << 1) + 1;
            final Object[] array = this.array;
            final Object o = array[n2];
            array[n2] = v;
            return (V)o;
        }
        final int n3 = ~n;
        final int[] hashes = this.hashes;
        if (size >= hashes.length) {
            int newLength = 4;
            if (size >= 8) {
                newLength = (size >> 1) + size;
            }
            else if (size >= 4) {
                newLength = 8;
            }
            final int[] copy = Arrays.copyOf(hashes, newLength);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            this.hashes = copy;
            final Object[] copy2 = Arrays.copyOf(this.array, newLength << 1);
            Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
            this.array = copy2;
            if (size != this.size) {
                throw new ConcurrentModificationException();
            }
        }
        if (n3 < size) {
            final int[] hashes2 = this.hashes;
            final int n4 = n3 + 1;
            ArraysKt.copyInto(hashes2, hashes2, n4, n3, size);
            final Object[] array2 = this.array;
            ArraysKt.copyInto(array2, array2, n4 << 1, n3 << 1, this.size << 1);
        }
        final int size2 = this.size;
        if (size == size2) {
            final int[] hashes3 = this.hashes;
            if (n3 < hashes3.length) {
                hashes3[n3] = hashCode;
                final Object[] array3 = this.array;
                final int n5 = n3 << 1;
                array3[n5] = k;
                array3[n5 + 1] = v;
                this.size = size2 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }
    
    public void putAll(final SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        Intrinsics.checkNotNullParameter((Object)simpleArrayMap, "map");
        final int size = simpleArrayMap.size;
        this.ensureCapacity(this.size + size);
        final int size2 = this.size;
        int i = 0;
        if (size2 == 0) {
            if (size > 0) {
                ArraysKt.copyInto(simpleArrayMap.hashes, this.hashes, 0, 0, size);
                ArraysKt.copyInto(simpleArrayMap.array, this.array, 0, 0, size << 1);
                this.size = size;
            }
        }
        else {
            while (i < size) {
                this.put(simpleArrayMap.keyAt(i), simpleArrayMap.valueAt(i));
                ++i;
            }
        }
    }
    
    public V putIfAbsent(final K k, final V v) {
        Object o;
        if ((o = this.get(k)) == null) {
            o = this.put(k, v);
        }
        return (V)o;
    }
    
    public V remove(final K k) {
        final int indexOfKey = this.indexOfKey(k);
        V remove;
        if (indexOfKey >= 0) {
            remove = this.removeAt(indexOfKey);
        }
        else {
            remove = null;
        }
        return remove;
    }
    
    public boolean remove(final K k, final V v) {
        final int indexOfKey = this.indexOfKey(k);
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)v, (Object)this.valueAt(indexOfKey))) {
            this.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public V removeAt(int i) {
        if (i >= 0 && i < this.size) {
            final Object[] array = this.array;
            final int n = i << 1;
            final Object o = array[n + 1];
            final int size = this.size;
            if (size <= 1) {
                this.clear();
            }
            else {
                final int size2 = size - 1;
                final int[] hashes = this.hashes;
                final int length = hashes.length;
                int newLength = 8;
                if (length > 8 && size < hashes.length / 3) {
                    if (size > 8) {
                        newLength = size + (size >> 1);
                    }
                    final int[] copy = Arrays.copyOf(hashes, newLength);
                    Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                    this.hashes = copy;
                    final Object[] copy2 = Arrays.copyOf(this.array, newLength << 1);
                    Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                    this.array = copy2;
                    if (size != this.size) {
                        throw new ConcurrentModificationException();
                    }
                    if (i > 0) {
                        ArraysKt.copyInto(hashes, this.hashes, 0, 0, i);
                        ArraysKt.copyInto(array, this.array, 0, 0, n);
                    }
                    if (i < size2) {
                        final int[] hashes2 = this.hashes;
                        final int n2 = i + 1;
                        final int n3 = size2 + 1;
                        ArraysKt.copyInto(hashes, hashes2, i, n2, n3);
                        ArraysKt.copyInto(array, this.array, n, n2 << 1, n3 << 1);
                    }
                }
                else {
                    if (i < size2) {
                        final int n4 = i + 1;
                        final int n5 = size2 + 1;
                        ArraysKt.copyInto(hashes, hashes, i, n4, n5);
                        final Object[] array2 = this.array;
                        ArraysKt.copyInto(array2, array2, n, n4 << 1, n5 << 1);
                    }
                    final Object[] array3 = this.array;
                    i = size2 << 1;
                    array3[i + 1] = (array3[i] = null);
                }
                if (size != this.size) {
                    throw new ConcurrentModificationException();
                }
                this.size = size2;
            }
            return (V)o;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public V replace(final K k, final V v) {
        final int indexOfKey = this.indexOfKey(k);
        Object setValue;
        if (indexOfKey >= 0) {
            setValue = this.setValueAt(indexOfKey, v);
        }
        else {
            setValue = null;
        }
        return (V)setValue;
    }
    
    public boolean replace(final K k, final V v, final V v2) {
        final int indexOfKey = this.indexOfKey(k);
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)v, (Object)this.valueAt(indexOfKey))) {
            this.setValueAt(indexOfKey, v2);
            return true;
        }
        return false;
    }
    
    public V setValueAt(int i, final V v) {
        int n = 0;
        if (i >= 0) {
            n = n;
            if (i < this.size) {
                n = 1;
            }
        }
        if (n != 0) {
            i = (i << 1) + 1;
            final Object[] array = this.array;
            final Object o = array[i];
            array[i] = v;
            return (V)o;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public int size() {
        return this.size;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.size * 28);
        sb.append('{');
        for (int i = 0; i < this.size; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            final K key = this.keyAt(i);
            if (key != sb) {
                sb.append(key);
            }
            else {
                sb.append("(this Map)");
            }
            sb.append('=');
            final V value = this.valueAt(i);
            if (value != sb) {
                sb.append(value);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
        return string;
    }
    
    public V valueAt(final int i) {
        int n = 0;
        if (i >= 0) {
            n = n;
            if (i < this.size) {
                n = 1;
            }
        }
        if (n != 0) {
            return (V)this.array[(i << 1) + 1];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected index to be within 0..size()-1, but was ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
}

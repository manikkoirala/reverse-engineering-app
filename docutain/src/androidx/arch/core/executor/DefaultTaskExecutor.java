// 
// Decompiled by Procyon v0.6.0
// 

package androidx.arch.core.executor;

import java.lang.reflect.InvocationTargetException;
import android.os.Handler$Callback;
import android.os.Build$VERSION;
import android.os.Looper;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import android.os.Handler;
import java.util.concurrent.ExecutorService;

public class DefaultTaskExecutor extends TaskExecutor
{
    private final ExecutorService mDiskIO;
    private final Object mLock;
    private volatile Handler mMainHandler;
    
    public DefaultTaskExecutor() {
        this.mLock = new Object();
        this.mDiskIO = Executors.newFixedThreadPool(4, new ThreadFactory() {
            private static final String THREAD_NAME_STEM = "arch_disk_io_";
            private final AtomicInteger mThreadId = new AtomicInteger(0);
            final DefaultTaskExecutor this$0;
            
            @Override
            public Thread newThread(final Runnable target) {
                final Thread thread = new Thread(target);
                final StringBuilder sb = new StringBuilder();
                sb.append("arch_disk_io_");
                sb.append(this.mThreadId.getAndIncrement());
                thread.setName(sb.toString());
                return thread;
            }
        });
    }
    
    private static Handler createAsync(final Looper looper) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createAsync(looper);
        }
        if (Build$VERSION.SDK_INT < 17) {
            goto Label_0084;
        }
        try {
            return Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, null, true);
        }
        catch (final InvocationTargetException ex) {
            return new Handler(looper);
        }
        catch (final IllegalAccessException | InstantiationException | NoSuchMethodException ex2) {
            goto Label_0084;
        }
    }
    
    @Override
    public void executeOnDiskIO(final Runnable runnable) {
        this.mDiskIO.execute(runnable);
    }
    
    @Override
    public boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    @Override
    public void postToMainThread(final Runnable runnable) {
        if (this.mMainHandler == null) {
            synchronized (this.mLock) {
                if (this.mMainHandler == null) {
                    this.mMainHandler = createAsync(Looper.getMainLooper());
                }
            }
        }
        this.mMainHandler.post(runnable);
    }
    
    private static class Api28Impl
    {
        public static Handler createAsync(final Looper looper) {
            return Handler.createAsync(looper);
        }
    }
}

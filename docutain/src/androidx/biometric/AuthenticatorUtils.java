// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.os.Build$VERSION;

class AuthenticatorUtils
{
    private static final int BIOMETRIC_CLASS_MASK = 32767;
    
    private AuthenticatorUtils() {
    }
    
    static String convertToString(final int i) {
        if (i == 15) {
            return "BIOMETRIC_STRONG";
        }
        if (i == 255) {
            return "BIOMETRIC_WEAK";
        }
        if (i == 32768) {
            return "DEVICE_CREDENTIAL";
        }
        if (i == 32783) {
            return "BIOMETRIC_STRONG | DEVICE_CREDENTIAL";
        }
        if (i != 33023) {
            return String.valueOf(i);
        }
        return "BIOMETRIC_WEAK | DEVICE_CREDENTIAL";
    }
    
    static int getConsolidatedAuthenticators(final BiometricPrompt.PromptInfo promptInfo, final BiometricPrompt.CryptoObject cryptoObject) {
        int allowedAuthenticators;
        if (promptInfo.getAllowedAuthenticators() != 0) {
            allowedAuthenticators = promptInfo.getAllowedAuthenticators();
        }
        else {
            if (cryptoObject != null) {
                allowedAuthenticators = 15;
            }
            else {
                allowedAuthenticators = 255;
            }
            if (promptInfo.isDeviceCredentialAllowed()) {
                allowedAuthenticators |= 0x8000;
            }
        }
        return allowedAuthenticators;
    }
    
    static boolean isDeviceCredentialAllowed(final int n) {
        return (n & 0x8000) != 0x0;
    }
    
    static boolean isSomeBiometricAllowed(final int n) {
        return (n & 0x7FFF) != 0x0;
    }
    
    static boolean isSupportedCombination(final int n) {
        final boolean b = true;
        final boolean b2 = true;
        final boolean b3 = true;
        boolean b4 = b2;
        if (n != 15) {
            b4 = b2;
            if (n != 255) {
                if (n != 32768) {
                    if (n == 32783) {
                        boolean b5 = b;
                        if (Build$VERSION.SDK_INT >= 28) {
                            b5 = (Build$VERSION.SDK_INT > 29 && b);
                        }
                        return b5;
                    }
                    b4 = b2;
                    if (n != 33023) {
                        return n == 0 && b3;
                    }
                }
                else {
                    b4 = (Build$VERSION.SDK_INT >= 30 && b2);
                }
            }
        }
        return b4;
    }
    
    static boolean isWeakBiometricAllowed(final int n) {
        return (n & 0xFF) == 0xFF;
    }
}

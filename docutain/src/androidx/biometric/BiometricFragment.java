// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import java.lang.ref.WeakReference;
import android.content.DialogInterface$OnClickListener;
import android.os.Bundle;
import android.hardware.biometrics.BiometricPrompt$AuthenticationCallback;
import android.os.CancellationSignal;
import android.hardware.biometrics.BiometricPrompt$CryptoObject;
import java.util.concurrent.Executor;
import android.hardware.biometrics.BiometricPrompt$Builder;
import android.text.TextUtils;
import android.content.Intent;
import android.app.KeyguardManager;
import android.util.Log;
import android.os.Build$VERSION;
import androidx.fragment.app.FragmentActivity;
import android.content.Context;
import android.os.Build;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import android.os.Looper;
import android.os.Handler;
import androidx.fragment.app.Fragment;

public class BiometricFragment extends Fragment
{
    static final int CANCELED_FROM_CLIENT = 3;
    static final int CANCELED_FROM_INTERNAL = 0;
    static final int CANCELED_FROM_NEGATIVE_BUTTON = 2;
    static final int CANCELED_FROM_USER = 1;
    private static final int DISMISS_INSTANTLY_DELAY_MS = 500;
    private static final String FINGERPRINT_DIALOG_FRAGMENT_TAG = "androidx.biometric.FingerprintDialogFragment";
    private static final int HIDE_DIALOG_DELAY_MS = 2000;
    private static final int REQUEST_CONFIRM_CREDENTIAL = 1;
    private static final int SHOW_PROMPT_DELAY_MS = 600;
    private static final String TAG = "BiometricFragment";
    Handler mHandler;
    BiometricViewModel mViewModel;
    
    public BiometricFragment() {
        this.mHandler = new Handler(Looper.getMainLooper());
    }
    
    private static int checkForFingerprintPreAuthenticationErrors(final FingerprintManagerCompat fingerprintManagerCompat) {
        if (!fingerprintManagerCompat.isHardwareDetected()) {
            return 12;
        }
        if (!fingerprintManagerCompat.hasEnrolledFingerprints()) {
            return 11;
        }
        return 0;
    }
    
    private void connectViewModel() {
        if (this.getActivity() == null) {
            return;
        }
        final BiometricViewModel mViewModel = new ViewModelProvider(this.getActivity()).get(BiometricViewModel.class);
        this.mViewModel = mViewModel;
        mViewModel.getAuthenticationResult().observe(this, new Observer<BiometricPrompt.AuthenticationResult>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final BiometricPrompt.AuthenticationResult authenticationResult) {
                if (authenticationResult != null) {
                    this.this$0.onAuthenticationSucceeded(authenticationResult);
                    this.this$0.mViewModel.setAuthenticationResult(null);
                }
            }
        });
        this.mViewModel.getAuthenticationError().observe(this, new Observer<BiometricErrorData>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final BiometricErrorData biometricErrorData) {
                if (biometricErrorData != null) {
                    this.this$0.onAuthenticationError(biometricErrorData.getErrorCode(), biometricErrorData.getErrorMessage());
                    this.this$0.mViewModel.setAuthenticationError(null);
                }
            }
        });
        this.mViewModel.getAuthenticationHelpMessage().observe(this, new Observer<CharSequence>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final CharSequence charSequence) {
                if (charSequence != null) {
                    this.this$0.onAuthenticationHelp(charSequence);
                    this.this$0.mViewModel.setAuthenticationError(null);
                }
            }
        });
        this.mViewModel.isAuthenticationFailurePending().observe(this, new Observer<Boolean>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final Boolean b) {
                if (b) {
                    this.this$0.onAuthenticationFailed();
                    this.this$0.mViewModel.setAuthenticationFailurePending(false);
                }
            }
        });
        this.mViewModel.isNegativeButtonPressPending().observe(this, new Observer<Boolean>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final Boolean b) {
                if (b) {
                    if (this.this$0.isManagingDeviceCredentialButton()) {
                        this.this$0.onDeviceCredentialButtonPressed();
                    }
                    else {
                        this.this$0.onCancelButtonPressed();
                    }
                    this.this$0.mViewModel.setNegativeButtonPressPending(false);
                }
            }
        });
        this.mViewModel.isFingerprintDialogCancelPending().observe(this, new Observer<Boolean>(this) {
            final BiometricFragment this$0;
            
            @Override
            public void onChanged(final Boolean b) {
                if (b) {
                    this.this$0.cancelAuthentication(1);
                    this.this$0.dismiss();
                    this.this$0.mViewModel.setFingerprintDialogCancelPending(false);
                }
            }
        });
    }
    
    private void dismissFingerprintDialog() {
        this.mViewModel.setPromptShowing(false);
        if (this.isAdded()) {
            final FragmentManager parentFragmentManager = this.getParentFragmentManager();
            final FingerprintDialogFragment fingerprintDialogFragment = (FingerprintDialogFragment)parentFragmentManager.findFragmentByTag("androidx.biometric.FingerprintDialogFragment");
            if (fingerprintDialogFragment != null) {
                if (fingerprintDialogFragment.isAdded()) {
                    fingerprintDialogFragment.dismissAllowingStateLoss();
                }
                else {
                    parentFragmentManager.beginTransaction().remove(fingerprintDialogFragment).commitAllowingStateLoss();
                }
            }
        }
    }
    
    private int getDismissDialogDelay() {
        final Context context = this.getContext();
        int n;
        if (context != null && DeviceUtils.shouldHideFingerprintDialog(context, Build.MODEL)) {
            n = 0;
        }
        else {
            n = 2000;
        }
        return n;
    }
    
    private void handleConfirmCredentialResult(final int n) {
        if (n == -1) {
            this.sendSuccessAndDismiss(new BiometricPrompt.AuthenticationResult(null, 1));
        }
        else {
            this.sendErrorAndDismiss(10, this.getString(R.string.generic_error_user_canceled));
        }
    }
    
    private boolean isChangingConfigurations() {
        final FragmentActivity activity = this.getActivity();
        return activity != null && activity.isChangingConfigurations();
    }
    
    private boolean isFingerprintDialogNeededForCrypto() {
        final FragmentActivity activity = this.getActivity();
        return activity != null && this.mViewModel.getCryptoObject() != null && DeviceUtils.shouldUseFingerprintForCrypto((Context)activity, Build.MANUFACTURER, Build.MODEL);
    }
    
    private boolean isFingerprintDialogNeededForErrorHandling() {
        return Build$VERSION.SDK_INT == 28 && !PackageUtils.hasSystemFeatureFingerprint(this.getContext());
    }
    
    private boolean isUsingFingerprintDialog() {
        return Build$VERSION.SDK_INT < 28 || this.isFingerprintDialogNeededForCrypto() || this.isFingerprintDialogNeededForErrorHandling();
    }
    
    private void launchConfirmCredentialActivity() {
        final FragmentActivity activity = this.getActivity();
        if (activity == null) {
            Log.e("BiometricFragment", "Failed to check device credential. Client FragmentActivity not found.");
            return;
        }
        final KeyguardManager keyguardManager = KeyguardUtils.getKeyguardManager((Context)activity);
        if (keyguardManager == null) {
            this.sendErrorAndDismiss(12, this.getString(R.string.generic_error_no_keyguard));
            return;
        }
        final CharSequence title = this.mViewModel.getTitle();
        final CharSequence subtitle = this.mViewModel.getSubtitle();
        CharSequence description = this.mViewModel.getDescription();
        if (subtitle != null) {
            description = subtitle;
        }
        final Intent confirmDeviceCredentialIntent = Api21Impl.createConfirmDeviceCredentialIntent(keyguardManager, title, description);
        if (confirmDeviceCredentialIntent == null) {
            this.sendErrorAndDismiss(14, this.getString(R.string.generic_error_no_device_credential));
            return;
        }
        this.mViewModel.setConfirmingDeviceCredential(true);
        if (this.isUsingFingerprintDialog()) {
            this.dismissFingerprintDialog();
        }
        confirmDeviceCredentialIntent.setFlags(134742016);
        this.startActivityForResult(confirmDeviceCredentialIntent, 1);
    }
    
    static BiometricFragment newInstance() {
        return new BiometricFragment();
    }
    
    private void sendErrorToClient(final int n, final CharSequence charSequence) {
        if (this.mViewModel.isConfirmingDeviceCredential()) {
            Log.v("BiometricFragment", "Error not sent to client. User is confirming their device credential.");
            return;
        }
        if (!this.mViewModel.isAwaitingResult()) {
            Log.w("BiometricFragment", "Error not sent to client. Client is not awaiting a result.");
            return;
        }
        this.mViewModel.setAwaitingResult(false);
        this.mViewModel.getClientExecutor().execute(new Runnable(this, n, charSequence) {
            final BiometricFragment this$0;
            final int val$errorCode;
            final CharSequence val$errorString;
            
            @Override
            public void run() {
                this.this$0.mViewModel.getClientCallback().onAuthenticationError(this.val$errorCode, this.val$errorString);
            }
        });
    }
    
    private void sendFailureToClient() {
        if (!this.mViewModel.isAwaitingResult()) {
            Log.w("BiometricFragment", "Failure not sent to client. Client is not awaiting a result.");
            return;
        }
        this.mViewModel.getClientExecutor().execute(new Runnable(this) {
            final BiometricFragment this$0;
            
            @Override
            public void run() {
                this.this$0.mViewModel.getClientCallback().onAuthenticationFailed();
            }
        });
    }
    
    private void sendSuccessAndDismiss(final BiometricPrompt.AuthenticationResult authenticationResult) {
        this.sendSuccessToClient(authenticationResult);
        this.dismiss();
    }
    
    private void sendSuccessToClient(final BiometricPrompt.AuthenticationResult authenticationResult) {
        if (!this.mViewModel.isAwaitingResult()) {
            Log.w("BiometricFragment", "Success not sent to client. Client is not awaiting a result.");
            return;
        }
        this.mViewModel.setAwaitingResult(false);
        this.mViewModel.getClientExecutor().execute(new Runnable(this, authenticationResult) {
            final BiometricFragment this$0;
            final BiometricPrompt.AuthenticationResult val$result;
            
            @Override
            public void run() {
                this.this$0.mViewModel.getClientCallback().onAuthenticationSucceeded(this.val$result);
            }
        });
    }
    
    private void showBiometricPromptForAuthentication() {
        final BiometricPrompt$Builder promptBuilder = Api28Impl.createPromptBuilder(this.requireContext().getApplicationContext());
        final CharSequence title = this.mViewModel.getTitle();
        final CharSequence subtitle = this.mViewModel.getSubtitle();
        final CharSequence description = this.mViewModel.getDescription();
        if (title != null) {
            Api28Impl.setTitle(promptBuilder, title);
        }
        if (subtitle != null) {
            Api28Impl.setSubtitle(promptBuilder, subtitle);
        }
        if (description != null) {
            Api28Impl.setDescription(promptBuilder, description);
        }
        final CharSequence negativeButtonText = this.mViewModel.getNegativeButtonText();
        if (!TextUtils.isEmpty(negativeButtonText)) {
            Api28Impl.setNegativeButton(promptBuilder, negativeButtonText, this.mViewModel.getClientExecutor(), this.mViewModel.getNegativeButtonListener());
        }
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setConfirmationRequired(promptBuilder, this.mViewModel.isConfirmationRequired());
        }
        final int allowedAuthenticators = this.mViewModel.getAllowedAuthenticators();
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setAllowedAuthenticators(promptBuilder, allowedAuthenticators);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setDeviceCredentialAllowed(promptBuilder, AuthenticatorUtils.isDeviceCredentialAllowed(allowedAuthenticators));
        }
        this.authenticateWithBiometricPrompt(Api28Impl.buildPrompt(promptBuilder), this.getContext());
    }
    
    private void showFingerprintDialogForAuthentication() {
        final Context applicationContext = this.requireContext().getApplicationContext();
        final FingerprintManagerCompat from = FingerprintManagerCompat.from(applicationContext);
        final int checkForFingerprintPreAuthenticationErrors = checkForFingerprintPreAuthenticationErrors(from);
        if (checkForFingerprintPreAuthenticationErrors != 0) {
            this.sendErrorAndDismiss(checkForFingerprintPreAuthenticationErrors, ErrorUtils.getFingerprintErrorString(applicationContext, checkForFingerprintPreAuthenticationErrors));
            return;
        }
        if (this.isAdded()) {
            this.mViewModel.setFingerprintDialogDismissedInstantly(true);
            if (!DeviceUtils.shouldHideFingerprintDialog(applicationContext, Build.MODEL)) {
                this.mHandler.postDelayed((Runnable)new Runnable(this) {
                    final BiometricFragment this$0;
                    
                    @Override
                    public void run() {
                        this.this$0.mViewModel.setFingerprintDialogDismissedInstantly(false);
                    }
                }, 500L);
                FingerprintDialogFragment.newInstance().show(this.getParentFragmentManager(), "androidx.biometric.FingerprintDialogFragment");
            }
            this.mViewModel.setCanceledFrom(0);
            this.authenticateWithFingerprint(from, applicationContext);
        }
    }
    
    private void showFingerprintErrorMessage(CharSequence string) {
        if (string == null) {
            string = this.getString(R.string.default_error_msg);
        }
        this.mViewModel.setFingerprintDialogState(2);
        this.mViewModel.setFingerprintDialogHelpMessage(string);
    }
    
    void authenticate(final BiometricPrompt.PromptInfo promptInfo, final BiometricPrompt.CryptoObject cryptoObject) {
        final FragmentActivity activity = this.getActivity();
        if (activity == null) {
            Log.e("BiometricFragment", "Not launching prompt. Client activity was null.");
            return;
        }
        this.mViewModel.setPromptInfo(promptInfo);
        final int consolidatedAuthenticators = AuthenticatorUtils.getConsolidatedAuthenticators(promptInfo, cryptoObject);
        if (Build$VERSION.SDK_INT >= 23 && Build$VERSION.SDK_INT < 30 && consolidatedAuthenticators == 15 && cryptoObject == null) {
            this.mViewModel.setCryptoObject(CryptoObjectUtils.createFakeCryptoObject());
        }
        else {
            this.mViewModel.setCryptoObject(cryptoObject);
        }
        if (this.isManagingDeviceCredentialButton()) {
            this.mViewModel.setNegativeButtonTextOverride(this.getString(R.string.confirm_device_credential_password));
        }
        else {
            this.mViewModel.setNegativeButtonTextOverride(null);
        }
        if (Build$VERSION.SDK_INT >= 21 && this.isManagingDeviceCredentialButton() && BiometricManager.from((Context)activity).canAuthenticate(255) != 0) {
            this.mViewModel.setAwaitingResult(true);
            this.launchConfirmCredentialActivity();
            return;
        }
        if (this.mViewModel.isDelayingPrompt()) {
            this.mHandler.postDelayed((Runnable)new ShowPromptForAuthenticationRunnable(this), 600L);
        }
        else {
            this.showPromptForAuthentication();
        }
    }
    
    void authenticateWithBiometricPrompt(final android.hardware.biometrics.BiometricPrompt biometricPrompt, final Context context) {
        final BiometricPrompt$CryptoObject wrapForBiometricPrompt = CryptoObjectUtils.wrapForBiometricPrompt(this.mViewModel.getCryptoObject());
        final CancellationSignal biometricCancellationSignal = this.mViewModel.getCancellationSignalProvider().getBiometricCancellationSignal();
        final PromptExecutor promptExecutor = new PromptExecutor();
        final BiometricPrompt$AuthenticationCallback biometricCallback = this.mViewModel.getAuthenticationCallbackProvider().getBiometricCallback();
        while (true) {
            if (wrapForBiometricPrompt == null) {
                try {
                    Api28Impl.authenticate(biometricPrompt, biometricCancellationSignal, promptExecutor, biometricCallback);
                    return;
                    Api28Impl.authenticate(biometricPrompt, wrapForBiometricPrompt, biometricCancellationSignal, promptExecutor, biometricCallback);
                }
                catch (final NullPointerException ex) {
                    Log.e("BiometricFragment", "Got NPE while authenticating with biometric prompt.", (Throwable)ex);
                    String string;
                    if (context != null) {
                        string = context.getString(R.string.default_error_msg);
                    }
                    else {
                        string = "";
                    }
                    this.sendErrorAndDismiss(1, string);
                }
                return;
            }
            continue;
        }
    }
    
    void authenticateWithFingerprint(final FingerprintManagerCompat fingerprintManagerCompat, final Context context) {
        final FingerprintManagerCompat.CryptoObject wrapForFingerprintManager = CryptoObjectUtils.wrapForFingerprintManager(this.mViewModel.getCryptoObject());
        final androidx.core.os.CancellationSignal fingerprintCancellationSignal = this.mViewModel.getCancellationSignalProvider().getFingerprintCancellationSignal();
        final FingerprintManagerCompat.AuthenticationCallback fingerprintCallback = this.mViewModel.getAuthenticationCallbackProvider().getFingerprintCallback();
        try {
            fingerprintManagerCompat.authenticate(wrapForFingerprintManager, 0, fingerprintCancellationSignal, fingerprintCallback, null);
        }
        catch (final NullPointerException ex) {
            Log.e("BiometricFragment", "Got NPE while authenticating with fingerprint.", (Throwable)ex);
            this.sendErrorAndDismiss(1, ErrorUtils.getFingerprintErrorString(context, 1));
        }
    }
    
    void cancelAuthentication(final int canceledFrom) {
        if (canceledFrom != 3 && this.mViewModel.isIgnoringCancel()) {
            return;
        }
        if (this.isUsingFingerprintDialog()) {
            this.mViewModel.setCanceledFrom(canceledFrom);
            if (canceledFrom == 1) {
                this.sendErrorToClient(10, ErrorUtils.getFingerprintErrorString(this.getContext(), 10));
            }
        }
        this.mViewModel.getCancellationSignalProvider().cancel();
    }
    
    void dismiss() {
        this.mViewModel.setPromptShowing(false);
        this.dismissFingerprintDialog();
        if (!this.mViewModel.isConfirmingDeviceCredential() && this.isAdded()) {
            this.getParentFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
        }
        final Context context = this.getContext();
        if (context != null && DeviceUtils.shouldDelayShowingPrompt(context, Build.MODEL)) {
            this.mViewModel.setDelayingPrompt(true);
            this.mHandler.postDelayed((Runnable)new StopDelayingPromptRunnable(this.mViewModel), 600L);
        }
    }
    
    boolean isManagingDeviceCredentialButton() {
        return Build$VERSION.SDK_INT <= 28 && AuthenticatorUtils.isDeviceCredentialAllowed(this.mViewModel.getAllowedAuthenticators());
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 1) {
            this.mViewModel.setConfirmingDeviceCredential(false);
            this.handleConfirmCredentialResult(n2);
        }
    }
    
    void onAuthenticationError(int i, CharSequence charSequence) {
        if (!ErrorUtils.isKnownError(i)) {
            i = 8;
        }
        final Context context = this.getContext();
        if (Build$VERSION.SDK_INT >= 21 && Build$VERSION.SDK_INT < 29 && ErrorUtils.isLockoutError(i) && context != null && KeyguardUtils.isDeviceSecuredWithCredential(context) && AuthenticatorUtils.isDeviceCredentialAllowed(this.mViewModel.getAllowedAuthenticators())) {
            this.launchConfirmCredentialActivity();
            return;
        }
        if (this.isUsingFingerprintDialog()) {
            if (charSequence == null) {
                charSequence = ErrorUtils.getFingerprintErrorString(this.getContext(), i);
            }
            if (i == 5) {
                final int canceled = this.mViewModel.getCanceledFrom();
                if (canceled == 0 || canceled == 3) {
                    this.sendErrorToClient(i, charSequence);
                }
                this.dismiss();
            }
            else {
                if (this.mViewModel.isFingerprintDialogDismissedInstantly()) {
                    this.sendErrorAndDismiss(i, charSequence);
                }
                else {
                    this.showFingerprintErrorMessage(charSequence);
                    this.mHandler.postDelayed((Runnable)new Runnable(this, i, charSequence) {
                        final BiometricFragment this$0;
                        final CharSequence val$errorString;
                        final int val$knownErrorCode;
                        
                        @Override
                        public void run() {
                            this.this$0.sendErrorAndDismiss(this.val$knownErrorCode, this.val$errorString);
                        }
                    }, (long)this.getDismissDialogDelay());
                }
                this.mViewModel.setFingerprintDialogDismissedInstantly(true);
            }
        }
        else {
            if (charSequence == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.getString(R.string.default_error_msg));
                sb.append(" ");
                sb.append(i);
                charSequence = sb.toString();
            }
            this.sendErrorAndDismiss(i, charSequence);
        }
    }
    
    void onAuthenticationFailed() {
        if (this.isUsingFingerprintDialog()) {
            this.showFingerprintErrorMessage(this.getString(R.string.fingerprint_not_recognized));
        }
        this.sendFailureToClient();
    }
    
    void onAuthenticationHelp(final CharSequence charSequence) {
        if (this.isUsingFingerprintDialog()) {
            this.showFingerprintErrorMessage(charSequence);
        }
    }
    
    void onAuthenticationSucceeded(final BiometricPrompt.AuthenticationResult authenticationResult) {
        this.sendSuccessAndDismiss(authenticationResult);
    }
    
    void onCancelButtonPressed() {
        CharSequence charSequence = this.mViewModel.getNegativeButtonText();
        if (charSequence == null) {
            charSequence = this.getString(R.string.default_error_msg);
        }
        this.sendErrorAndDismiss(13, charSequence);
        this.cancelAuthentication(2);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.connectViewModel();
    }
    
    void onDeviceCredentialButtonPressed() {
        if (Build$VERSION.SDK_INT < 21) {
            Log.e("BiometricFragment", "Failed to check device credential. Not supported prior to API 21.");
            return;
        }
        this.launchConfirmCredentialActivity();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        if (Build$VERSION.SDK_INT == 29 && AuthenticatorUtils.isDeviceCredentialAllowed(this.mViewModel.getAllowedAuthenticators())) {
            this.mViewModel.setIgnoringCancel(true);
            this.mHandler.postDelayed((Runnable)new StopIgnoringCancelRunnable(this.mViewModel), 250L);
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (Build$VERSION.SDK_INT < 29 && !this.mViewModel.isConfirmingDeviceCredential() && !this.isChangingConfigurations()) {
            this.cancelAuthentication(0);
        }
    }
    
    void sendErrorAndDismiss(final int n, final CharSequence charSequence) {
        this.sendErrorToClient(n, charSequence);
        this.dismiss();
    }
    
    void showPromptForAuthentication() {
        if (!this.mViewModel.isPromptShowing()) {
            if (this.getContext() == null) {
                Log.w("BiometricFragment", "Not showing biometric prompt. Context is null.");
                return;
            }
            this.mViewModel.setPromptShowing(true);
            this.mViewModel.setAwaitingResult(true);
            if (this.isUsingFingerprintDialog()) {
                this.showFingerprintDialogForAuthentication();
            }
            else {
                this.showBiometricPromptForAuthentication();
            }
        }
    }
    
    private static class Api21Impl
    {
        static Intent createConfirmDeviceCredentialIntent(final KeyguardManager keyguardManager, final CharSequence charSequence, final CharSequence charSequence2) {
            return keyguardManager.createConfirmDeviceCredentialIntent(charSequence, charSequence2);
        }
    }
    
    private static class Api28Impl
    {
        static void authenticate(final android.hardware.biometrics.BiometricPrompt biometricPrompt, final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject, final CancellationSignal cancellationSignal, final Executor executor, final BiometricPrompt$AuthenticationCallback biometricPrompt$AuthenticationCallback) {
            biometricPrompt.authenticate(biometricPrompt$CryptoObject, cancellationSignal, executor, biometricPrompt$AuthenticationCallback);
        }
        
        static void authenticate(final android.hardware.biometrics.BiometricPrompt biometricPrompt, final CancellationSignal cancellationSignal, final Executor executor, final BiometricPrompt$AuthenticationCallback biometricPrompt$AuthenticationCallback) {
            biometricPrompt.authenticate(cancellationSignal, executor, biometricPrompt$AuthenticationCallback);
        }
        
        static android.hardware.biometrics.BiometricPrompt buildPrompt(final BiometricPrompt$Builder biometricPrompt$Builder) {
            return biometricPrompt$Builder.build();
        }
        
        static BiometricPrompt$Builder createPromptBuilder(final Context context) {
            return new BiometricPrompt$Builder(context);
        }
        
        static void setDescription(final BiometricPrompt$Builder biometricPrompt$Builder, final CharSequence description) {
            biometricPrompt$Builder.setDescription(description);
        }
        
        static void setNegativeButton(final BiometricPrompt$Builder biometricPrompt$Builder, final CharSequence charSequence, final Executor executor, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
            biometricPrompt$Builder.setNegativeButton(charSequence, executor, dialogInterface$OnClickListener);
        }
        
        static void setSubtitle(final BiometricPrompt$Builder biometricPrompt$Builder, final CharSequence subtitle) {
            biometricPrompt$Builder.setSubtitle(subtitle);
        }
        
        static void setTitle(final BiometricPrompt$Builder biometricPrompt$Builder, final CharSequence title) {
            biometricPrompt$Builder.setTitle(title);
        }
    }
    
    private static class Api29Impl
    {
        static void setConfirmationRequired(final BiometricPrompt$Builder biometricPrompt$Builder, final boolean confirmationRequired) {
            biometricPrompt$Builder.setConfirmationRequired(confirmationRequired);
        }
        
        static void setDeviceCredentialAllowed(final BiometricPrompt$Builder biometricPrompt$Builder, final boolean deviceCredentialAllowed) {
            biometricPrompt$Builder.setDeviceCredentialAllowed(deviceCredentialAllowed);
        }
    }
    
    private static class Api30Impl
    {
        static void setAllowedAuthenticators(final BiometricPrompt$Builder biometricPrompt$Builder, final int allowedAuthenticators) {
            biometricPrompt$Builder.setAllowedAuthenticators(allowedAuthenticators);
        }
    }
    
    private static class PromptExecutor implements Executor
    {
        private final Handler mPromptHandler;
        
        PromptExecutor() {
            this.mPromptHandler = new Handler(Looper.getMainLooper());
        }
        
        @Override
        public void execute(final Runnable runnable) {
            this.mPromptHandler.post(runnable);
        }
    }
    
    private static class ShowPromptForAuthenticationRunnable implements Runnable
    {
        private final WeakReference<BiometricFragment> mFragmentRef;
        
        ShowPromptForAuthenticationRunnable(final BiometricFragment referent) {
            this.mFragmentRef = new WeakReference<BiometricFragment>(referent);
        }
        
        @Override
        public void run() {
            if (this.mFragmentRef.get() != null) {
                this.mFragmentRef.get().showPromptForAuthentication();
            }
        }
    }
    
    private static class StopDelayingPromptRunnable implements Runnable
    {
        private final WeakReference<BiometricViewModel> mViewModelRef;
        
        StopDelayingPromptRunnable(final BiometricViewModel referent) {
            this.mViewModelRef = new WeakReference<BiometricViewModel>(referent);
        }
        
        @Override
        public void run() {
            if (this.mViewModelRef.get() != null) {
                this.mViewModelRef.get().setDelayingPrompt(false);
            }
        }
    }
    
    private static class StopIgnoringCancelRunnable implements Runnable
    {
        private final WeakReference<BiometricViewModel> mViewModelRef;
        
        StopIgnoringCancelRunnable(final BiometricViewModel referent) {
            this.mViewModelRef = new WeakReference<BiometricViewModel>(referent);
        }
        
        @Override
        public void run() {
            if (this.mViewModelRef.get() != null) {
                this.mViewModelRef.get().setIgnoringCancel(false);
            }
        }
    }
}

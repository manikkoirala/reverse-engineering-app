// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import java.util.Arrays;

class BiometricErrorData
{
    private final int mErrorCode;
    private final CharSequence mErrorMessage;
    
    BiometricErrorData(final int mErrorCode, final CharSequence mErrorMessage) {
        this.mErrorCode = mErrorCode;
        this.mErrorMessage = mErrorMessage;
    }
    
    private static String convertToString(final CharSequence charSequence) {
        String string;
        if (charSequence != null) {
            string = charSequence.toString();
        }
        else {
            string = null;
        }
        return string;
    }
    
    private boolean isErrorMessageEqualTo(final CharSequence charSequence) {
        final String convertToString = convertToString(this.mErrorMessage);
        final String convertToString2 = convertToString(charSequence);
        return (convertToString == null && convertToString2 == null) || (convertToString != null && convertToString.equals(convertToString2));
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof BiometricErrorData;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final BiometricErrorData biometricErrorData = (BiometricErrorData)o;
            b3 = b2;
            if (this.mErrorCode == biometricErrorData.mErrorCode) {
                b3 = b2;
                if (this.isErrorMessageEqualTo(biometricErrorData.mErrorMessage)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    int getErrorCode() {
        return this.mErrorCode;
    }
    
    CharSequence getErrorMessage() {
        return this.mErrorMessage;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.mErrorCode, convertToString(this.mErrorMessage) });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.util.Log;
import android.os.Build$VERSION;
import android.os.CancellationSignal;

class CancellationSignalProvider
{
    private static final String TAG = "CancelSignalProvider";
    private CancellationSignal mBiometricCancellationSignal;
    private androidx.core.os.CancellationSignal mFingerprintCancellationSignal;
    private final Injector mInjector;
    
    CancellationSignalProvider() {
        this.mInjector = (Injector)new Injector(this) {
            final CancellationSignalProvider this$0;
            
            @Override
            public CancellationSignal getBiometricCancellationSignal() {
                return Api16Impl.create();
            }
            
            @Override
            public androidx.core.os.CancellationSignal getFingerprintCancellationSignal() {
                return new androidx.core.os.CancellationSignal();
            }
        };
    }
    
    CancellationSignalProvider(final Injector mInjector) {
        this.mInjector = mInjector;
    }
    
    void cancel() {
        if (Build$VERSION.SDK_INT >= 16) {
            final CancellationSignal mBiometricCancellationSignal = this.mBiometricCancellationSignal;
            if (mBiometricCancellationSignal != null) {
                try {
                    Api16Impl.cancel(mBiometricCancellationSignal);
                }
                catch (final NullPointerException ex) {
                    Log.e("CancelSignalProvider", "Got NPE while canceling biometric authentication.", (Throwable)ex);
                }
                this.mBiometricCancellationSignal = null;
            }
        }
        final androidx.core.os.CancellationSignal mFingerprintCancellationSignal = this.mFingerprintCancellationSignal;
        if (mFingerprintCancellationSignal != null) {
            try {
                mFingerprintCancellationSignal.cancel();
            }
            catch (final NullPointerException ex2) {
                Log.e("CancelSignalProvider", "Got NPE while canceling fingerprint authentication.", (Throwable)ex2);
            }
            this.mFingerprintCancellationSignal = null;
        }
    }
    
    CancellationSignal getBiometricCancellationSignal() {
        if (this.mBiometricCancellationSignal == null) {
            this.mBiometricCancellationSignal = this.mInjector.getBiometricCancellationSignal();
        }
        return this.mBiometricCancellationSignal;
    }
    
    androidx.core.os.CancellationSignal getFingerprintCancellationSignal() {
        if (this.mFingerprintCancellationSignal == null) {
            this.mFingerprintCancellationSignal = this.mInjector.getFingerprintCancellationSignal();
        }
        return this.mFingerprintCancellationSignal;
    }
    
    private static class Api16Impl
    {
        static void cancel(final CancellationSignal cancellationSignal) {
            cancellationSignal.cancel();
        }
        
        static CancellationSignal create() {
            return new CancellationSignal();
        }
    }
    
    interface Injector
    {
        CancellationSignal getBiometricCancellationSignal();
        
        androidx.core.os.CancellationSignal getFingerprintCancellationSignal();
    }
}

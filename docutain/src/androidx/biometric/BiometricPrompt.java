// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import java.lang.ref.WeakReference;
import android.text.TextUtils;
import java.security.Signature;
import javax.crypto.Mac;
import android.security.identity.IdentityCredential;
import javax.crypto.Cipher;
import android.os.Build$VERSION;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.ViewModelProvider;
import android.util.Log;
import androidx.lifecycle.LifecycleObserver;
import androidx.fragment.app.FragmentActivity;
import java.util.concurrent.Executor;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class BiometricPrompt
{
    public static final int AUTHENTICATION_RESULT_TYPE_BIOMETRIC = 2;
    public static final int AUTHENTICATION_RESULT_TYPE_DEVICE_CREDENTIAL = 1;
    public static final int AUTHENTICATION_RESULT_TYPE_UNKNOWN = -1;
    private static final String BIOMETRIC_FRAGMENT_TAG = "androidx.biometric.BiometricFragment";
    static final int BIOMETRIC_SUCCESS = 0;
    public static final int ERROR_CANCELED = 5;
    public static final int ERROR_HW_NOT_PRESENT = 12;
    public static final int ERROR_HW_UNAVAILABLE = 1;
    public static final int ERROR_LOCKOUT = 7;
    public static final int ERROR_LOCKOUT_PERMANENT = 9;
    public static final int ERROR_NEGATIVE_BUTTON = 13;
    public static final int ERROR_NO_BIOMETRICS = 11;
    public static final int ERROR_NO_DEVICE_CREDENTIAL = 14;
    public static final int ERROR_NO_SPACE = 4;
    public static final int ERROR_SECURITY_UPDATE_REQUIRED = 15;
    public static final int ERROR_TIMEOUT = 3;
    public static final int ERROR_UNABLE_TO_PROCESS = 2;
    public static final int ERROR_USER_CANCELED = 10;
    public static final int ERROR_VENDOR = 8;
    private static final String TAG = "BiometricPromptCompat";
    private FragmentManager mClientFragmentManager;
    
    public BiometricPrompt(final Fragment fragment, final AuthenticationCallback authenticationCallback) {
        if (fragment == null) {
            throw new IllegalArgumentException("Fragment must not be null.");
        }
        if (authenticationCallback != null) {
            final FragmentActivity activity = fragment.getActivity();
            final FragmentManager childFragmentManager = fragment.getChildFragmentManager();
            final BiometricViewModel viewModel = getViewModel(activity);
            addObservers(fragment, viewModel);
            this.init(childFragmentManager, viewModel, null, authenticationCallback);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }
    
    public BiometricPrompt(final Fragment fragment, final Executor executor, final AuthenticationCallback authenticationCallback) {
        if (fragment == null) {
            throw new IllegalArgumentException("Fragment must not be null.");
        }
        if (executor == null) {
            throw new IllegalArgumentException("Executor must not be null.");
        }
        if (authenticationCallback != null) {
            final FragmentActivity activity = fragment.getActivity();
            final FragmentManager childFragmentManager = fragment.getChildFragmentManager();
            final BiometricViewModel viewModel = getViewModel(activity);
            addObservers(fragment, viewModel);
            this.init(childFragmentManager, viewModel, executor, authenticationCallback);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }
    
    public BiometricPrompt(final FragmentActivity fragmentActivity, final AuthenticationCallback authenticationCallback) {
        if (fragmentActivity == null) {
            throw new IllegalArgumentException("FragmentActivity must not be null.");
        }
        if (authenticationCallback != null) {
            this.init(fragmentActivity.getSupportFragmentManager(), getViewModel(fragmentActivity), null, authenticationCallback);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }
    
    public BiometricPrompt(final FragmentActivity fragmentActivity, final Executor executor, final AuthenticationCallback authenticationCallback) {
        if (fragmentActivity == null) {
            throw new IllegalArgumentException("FragmentActivity must not be null.");
        }
        if (executor == null) {
            throw new IllegalArgumentException("Executor must not be null.");
        }
        if (authenticationCallback != null) {
            this.init(fragmentActivity.getSupportFragmentManager(), getViewModel(fragmentActivity), executor, authenticationCallback);
            return;
        }
        throw new IllegalArgumentException("AuthenticationCallback must not be null.");
    }
    
    private static void addObservers(final Fragment fragment, final BiometricViewModel biometricViewModel) {
        if (biometricViewModel != null) {
            fragment.getLifecycle().addObserver(new ResetCallbackObserver(biometricViewModel));
        }
    }
    
    private void authenticateInternal(final PromptInfo promptInfo, final CryptoObject cryptoObject) {
        final FragmentManager mClientFragmentManager = this.mClientFragmentManager;
        if (mClientFragmentManager == null) {
            Log.e("BiometricPromptCompat", "Unable to start authentication. Client fragment manager was null.");
            return;
        }
        if (mClientFragmentManager.isStateSaved()) {
            Log.e("BiometricPromptCompat", "Unable to start authentication. Called after onSaveInstanceState().");
            return;
        }
        findOrAddBiometricFragment(this.mClientFragmentManager).authenticate(promptInfo, cryptoObject);
    }
    
    private static BiometricFragment findBiometricFragment(final FragmentManager fragmentManager) {
        return (BiometricFragment)fragmentManager.findFragmentByTag("androidx.biometric.BiometricFragment");
    }
    
    private static BiometricFragment findOrAddBiometricFragment(final FragmentManager fragmentManager) {
        BiometricFragment biometricFragment;
        if ((biometricFragment = findBiometricFragment(fragmentManager)) == null) {
            biometricFragment = BiometricFragment.newInstance();
            fragmentManager.beginTransaction().add(biometricFragment, "androidx.biometric.BiometricFragment").commitAllowingStateLoss();
            fragmentManager.executePendingTransactions();
        }
        return biometricFragment;
    }
    
    private static BiometricViewModel getViewModel(final FragmentActivity fragmentActivity) {
        BiometricViewModel biometricViewModel;
        if (fragmentActivity != null) {
            biometricViewModel = new ViewModelProvider(fragmentActivity).get(BiometricViewModel.class);
        }
        else {
            biometricViewModel = null;
        }
        return biometricViewModel;
    }
    
    private void init(final FragmentManager mClientFragmentManager, final BiometricViewModel biometricViewModel, final Executor clientExecutor, final AuthenticationCallback clientCallback) {
        this.mClientFragmentManager = mClientFragmentManager;
        if (biometricViewModel != null) {
            if (clientExecutor != null) {
                biometricViewModel.setClientExecutor(clientExecutor);
            }
            biometricViewModel.setClientCallback(clientCallback);
        }
    }
    
    public void authenticate(final PromptInfo promptInfo) {
        if (promptInfo != null) {
            this.authenticateInternal(promptInfo, null);
            return;
        }
        throw new IllegalArgumentException("PromptInfo cannot be null.");
    }
    
    public void authenticate(final PromptInfo promptInfo, final CryptoObject cryptoObject) {
        if (promptInfo == null) {
            throw new IllegalArgumentException("PromptInfo cannot be null.");
        }
        if (cryptoObject == null) {
            throw new IllegalArgumentException("CryptoObject cannot be null.");
        }
        final int consolidatedAuthenticators = AuthenticatorUtils.getConsolidatedAuthenticators(promptInfo, cryptoObject);
        if (AuthenticatorUtils.isWeakBiometricAllowed(consolidatedAuthenticators)) {
            throw new IllegalArgumentException("Crypto-based authentication is not supported for Class 2 (Weak) biometrics.");
        }
        if (Build$VERSION.SDK_INT < 30 && AuthenticatorUtils.isDeviceCredentialAllowed(consolidatedAuthenticators)) {
            throw new IllegalArgumentException("Crypto-based authentication is not supported for device credential prior to API 30.");
        }
        this.authenticateInternal(promptInfo, cryptoObject);
    }
    
    public void cancelAuthentication() {
        final FragmentManager mClientFragmentManager = this.mClientFragmentManager;
        if (mClientFragmentManager == null) {
            Log.e("BiometricPromptCompat", "Unable to start authentication. Client fragment manager was null.");
            return;
        }
        final BiometricFragment biometricFragment = findBiometricFragment(mClientFragmentManager);
        if (biometricFragment == null) {
            Log.e("BiometricPromptCompat", "Unable to cancel authentication. BiometricFragment not found.");
            return;
        }
        biometricFragment.cancelAuthentication(3);
    }
    
    public abstract static class AuthenticationCallback
    {
        public void onAuthenticationError(final int n, final CharSequence charSequence) {
        }
        
        public void onAuthenticationFailed() {
        }
        
        public void onAuthenticationSucceeded(final AuthenticationResult authenticationResult) {
        }
    }
    
    public static class AuthenticationResult
    {
        private final int mAuthenticationType;
        private final CryptoObject mCryptoObject;
        
        AuthenticationResult(final CryptoObject mCryptoObject, final int mAuthenticationType) {
            this.mCryptoObject = mCryptoObject;
            this.mAuthenticationType = mAuthenticationType;
        }
        
        public int getAuthenticationType() {
            return this.mAuthenticationType;
        }
        
        public CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }
    
    public static class CryptoObject
    {
        private final Cipher mCipher;
        private final IdentityCredential mIdentityCredential;
        private final Mac mMac;
        private final Signature mSignature;
        
        public CryptoObject(final IdentityCredential mIdentityCredential) {
            this.mSignature = null;
            this.mCipher = null;
            this.mMac = null;
            this.mIdentityCredential = mIdentityCredential;
        }
        
        public CryptoObject(final Signature mSignature) {
            this.mSignature = mSignature;
            this.mCipher = null;
            this.mMac = null;
            this.mIdentityCredential = null;
        }
        
        public CryptoObject(final Cipher mCipher) {
            this.mSignature = null;
            this.mCipher = mCipher;
            this.mMac = null;
            this.mIdentityCredential = null;
        }
        
        public CryptoObject(final Mac mMac) {
            this.mSignature = null;
            this.mCipher = null;
            this.mMac = mMac;
            this.mIdentityCredential = null;
        }
        
        public Cipher getCipher() {
            return this.mCipher;
        }
        
        public IdentityCredential getIdentityCredential() {
            return this.mIdentityCredential;
        }
        
        public Mac getMac() {
            return this.mMac;
        }
        
        public Signature getSignature() {
            return this.mSignature;
        }
    }
    
    public static class PromptInfo
    {
        private final int mAllowedAuthenticators;
        private final CharSequence mDescription;
        private final boolean mIsConfirmationRequired;
        private final boolean mIsDeviceCredentialAllowed;
        private final CharSequence mNegativeButtonText;
        private final CharSequence mSubtitle;
        private final CharSequence mTitle;
        
        PromptInfo(final CharSequence mTitle, final CharSequence mSubtitle, final CharSequence mDescription, final CharSequence mNegativeButtonText, final boolean mIsConfirmationRequired, final boolean mIsDeviceCredentialAllowed, final int mAllowedAuthenticators) {
            this.mTitle = mTitle;
            this.mSubtitle = mSubtitle;
            this.mDescription = mDescription;
            this.mNegativeButtonText = mNegativeButtonText;
            this.mIsConfirmationRequired = mIsConfirmationRequired;
            this.mIsDeviceCredentialAllowed = mIsDeviceCredentialAllowed;
            this.mAllowedAuthenticators = mAllowedAuthenticators;
        }
        
        public int getAllowedAuthenticators() {
            return this.mAllowedAuthenticators;
        }
        
        public CharSequence getDescription() {
            return this.mDescription;
        }
        
        public CharSequence getNegativeButtonText() {
            CharSequence mNegativeButtonText = this.mNegativeButtonText;
            if (mNegativeButtonText == null) {
                mNegativeButtonText = "";
            }
            return mNegativeButtonText;
        }
        
        public CharSequence getSubtitle() {
            return this.mSubtitle;
        }
        
        public CharSequence getTitle() {
            return this.mTitle;
        }
        
        public boolean isConfirmationRequired() {
            return this.mIsConfirmationRequired;
        }
        
        @Deprecated
        public boolean isDeviceCredentialAllowed() {
            return this.mIsDeviceCredentialAllowed;
        }
        
        public static class Builder
        {
            private int mAllowedAuthenticators;
            private CharSequence mDescription;
            private boolean mIsConfirmationRequired;
            private boolean mIsDeviceCredentialAllowed;
            private CharSequence mNegativeButtonText;
            private CharSequence mSubtitle;
            private CharSequence mTitle;
            
            public Builder() {
                this.mTitle = null;
                this.mSubtitle = null;
                this.mDescription = null;
                this.mNegativeButtonText = null;
                this.mIsConfirmationRequired = true;
                this.mIsDeviceCredentialAllowed = false;
                this.mAllowedAuthenticators = 0;
            }
            
            public PromptInfo build() {
                if (TextUtils.isEmpty(this.mTitle)) {
                    throw new IllegalArgumentException("Title must be set and non-empty.");
                }
                if (!AuthenticatorUtils.isSupportedCombination(this.mAllowedAuthenticators)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Authenticator combination is unsupported on API ");
                    sb.append(Build$VERSION.SDK_INT);
                    sb.append(": ");
                    sb.append(AuthenticatorUtils.convertToString(this.mAllowedAuthenticators));
                    throw new IllegalArgumentException(sb.toString());
                }
                final int mAllowedAuthenticators = this.mAllowedAuthenticators;
                boolean b;
                if (mAllowedAuthenticators != 0) {
                    b = AuthenticatorUtils.isDeviceCredentialAllowed(mAllowedAuthenticators);
                }
                else {
                    b = this.mIsDeviceCredentialAllowed;
                }
                if (TextUtils.isEmpty(this.mNegativeButtonText) && !b) {
                    throw new IllegalArgumentException("Negative text must be set and non-empty.");
                }
                if (!TextUtils.isEmpty(this.mNegativeButtonText) && b) {
                    throw new IllegalArgumentException("Negative text must not be set if device credential authentication is allowed.");
                }
                return new PromptInfo(this.mTitle, this.mSubtitle, this.mDescription, this.mNegativeButtonText, this.mIsConfirmationRequired, this.mIsDeviceCredentialAllowed, this.mAllowedAuthenticators);
            }
            
            public Builder setAllowedAuthenticators(final int mAllowedAuthenticators) {
                this.mAllowedAuthenticators = mAllowedAuthenticators;
                return this;
            }
            
            public Builder setConfirmationRequired(final boolean mIsConfirmationRequired) {
                this.mIsConfirmationRequired = mIsConfirmationRequired;
                return this;
            }
            
            public Builder setDescription(final CharSequence mDescription) {
                this.mDescription = mDescription;
                return this;
            }
            
            @Deprecated
            public Builder setDeviceCredentialAllowed(final boolean mIsDeviceCredentialAllowed) {
                this.mIsDeviceCredentialAllowed = mIsDeviceCredentialAllowed;
                return this;
            }
            
            public Builder setNegativeButtonText(final CharSequence mNegativeButtonText) {
                this.mNegativeButtonText = mNegativeButtonText;
                return this;
            }
            
            public Builder setSubtitle(final CharSequence mSubtitle) {
                this.mSubtitle = mSubtitle;
                return this;
            }
            
            public Builder setTitle(final CharSequence mTitle) {
                this.mTitle = mTitle;
                return this;
            }
        }
    }
    
    private static class ResetCallbackObserver implements LifecycleObserver
    {
        private final WeakReference<BiometricViewModel> mViewModelRef;
        
        ResetCallbackObserver(final BiometricViewModel referent) {
            this.mViewModelRef = new WeakReference<BiometricViewModel>(referent);
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void resetCallback() {
            if (this.mViewModelRef.get() != null) {
                this.mViewModelRef.get().resetClientCallback();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.os.Build;
import android.hardware.biometrics.BiometricPrompt$CryptoObject;
import android.content.Context;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.os.Build$VERSION;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

public class BiometricManager
{
    public static final int BIOMETRIC_ERROR_HW_UNAVAILABLE = 1;
    public static final int BIOMETRIC_ERROR_NONE_ENROLLED = 11;
    public static final int BIOMETRIC_ERROR_NO_HARDWARE = 12;
    public static final int BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED = 15;
    public static final int BIOMETRIC_ERROR_UNSUPPORTED = -2;
    public static final int BIOMETRIC_STATUS_UNKNOWN = -1;
    public static final int BIOMETRIC_SUCCESS = 0;
    private static final String TAG = "BiometricManager";
    private final android.hardware.biometrics.BiometricManager mBiometricManager;
    private final FingerprintManagerCompat mFingerprintManager;
    private final Injector mInjector;
    
    BiometricManager(final Injector mInjector) {
        this.mInjector = mInjector;
        final int sdk_INT = Build$VERSION.SDK_INT;
        final FingerprintManagerCompat fingerprintManagerCompat = null;
        android.hardware.biometrics.BiometricManager biometricManager;
        if (sdk_INT >= 29) {
            biometricManager = mInjector.getBiometricManager();
        }
        else {
            biometricManager = null;
        }
        this.mBiometricManager = biometricManager;
        FingerprintManagerCompat fingerprintManager = fingerprintManagerCompat;
        if (Build$VERSION.SDK_INT <= 29) {
            fingerprintManager = mInjector.getFingerprintManager();
        }
        this.mFingerprintManager = fingerprintManager;
    }
    
    private int canAuthenticateCompat(int n) {
        if (!AuthenticatorUtils.isSupportedCombination(n)) {
            return -2;
        }
        final int n2 = 12;
        if (n == 0) {
            return 12;
        }
        if (!this.mInjector.isDeviceSecurable()) {
            return 12;
        }
        if (AuthenticatorUtils.isDeviceCredentialAllowed(n)) {
            if (this.mInjector.isDeviceSecuredWithCredential()) {
                n = 0;
            }
            else {
                n = 11;
            }
            return n;
        }
        if (Build$VERSION.SDK_INT == 29) {
            if (AuthenticatorUtils.isWeakBiometricAllowed(n)) {
                n = this.canAuthenticateWithWeakBiometricOnApi29();
            }
            else {
                n = this.canAuthenticateWithStrongBiometricOnApi29();
            }
            return n;
        }
        if (Build$VERSION.SDK_INT == 28) {
            n = n2;
            if (this.mInjector.isFingerprintHardwarePresent()) {
                n = this.canAuthenticateWithFingerprintOrUnknownBiometric();
            }
            return n;
        }
        return this.canAuthenticateWithFingerprint();
    }
    
    private int canAuthenticateWithFingerprint() {
        final FingerprintManagerCompat mFingerprintManager = this.mFingerprintManager;
        if (mFingerprintManager == null) {
            Log.e("BiometricManager", "Failure in canAuthenticate(). FingerprintManager was null.");
            return 1;
        }
        if (!mFingerprintManager.isHardwareDetected()) {
            return 12;
        }
        if (!this.mFingerprintManager.hasEnrolledFingerprints()) {
            return 11;
        }
        return 0;
    }
    
    private int canAuthenticateWithFingerprintOrUnknownBiometric() {
        if (!this.mInjector.isDeviceSecuredWithCredential()) {
            return this.canAuthenticateWithFingerprint();
        }
        int n;
        if (this.canAuthenticateWithFingerprint() == 0) {
            n = 0;
        }
        else {
            n = -1;
        }
        return n;
    }
    
    private int canAuthenticateWithStrongBiometricOnApi29() {
        final Method canAuthenticateWithCryptoMethod = Api29Impl.getCanAuthenticateWithCryptoMethod();
        Label_0083: {
            if (canAuthenticateWithCryptoMethod != null) {
                Object o = CryptoObjectUtils.wrapForBiometricPrompt(CryptoObjectUtils.createFakeCryptoObject());
                if (o != null) {
                    try {
                        o = canAuthenticateWithCryptoMethod.invoke(this.mBiometricManager, o);
                        if (o instanceof Integer) {
                            return (int)o;
                        }
                        Log.w("BiometricManager", "Invalid return type for canAuthenticate(CryptoObject).");
                        break Label_0083;
                    }
                    catch (final InvocationTargetException o) {}
                    catch (final IllegalArgumentException o) {}
                    catch (final IllegalAccessException ex) {}
                    Log.w("BiometricManager", "Failed to invoke canAuthenticate(CryptoObject).", (Throwable)o);
                }
            }
        }
        int n2;
        final int n = n2 = this.canAuthenticateWithWeakBiometricOnApi29();
        if (!this.mInjector.isStrongBiometricGuaranteed()) {
            if (n != 0) {
                n2 = n;
            }
            else {
                n2 = this.canAuthenticateWithFingerprintOrUnknownBiometric();
            }
        }
        return n2;
    }
    
    private int canAuthenticateWithWeakBiometricOnApi29() {
        final android.hardware.biometrics.BiometricManager mBiometricManager = this.mBiometricManager;
        if (mBiometricManager == null) {
            Log.e("BiometricManager", "Failure in canAuthenticate(). BiometricManager was null.");
            return 1;
        }
        return Api29Impl.canAuthenticate(mBiometricManager);
    }
    
    public static BiometricManager from(final Context context) {
        return new BiometricManager((Injector)new DefaultInjector(context));
    }
    
    @Deprecated
    public int canAuthenticate() {
        return this.canAuthenticate(255);
    }
    
    public int canAuthenticate(final int n) {
        if (Build$VERSION.SDK_INT < 30) {
            return this.canAuthenticateCompat(n);
        }
        final android.hardware.biometrics.BiometricManager mBiometricManager = this.mBiometricManager;
        if (mBiometricManager == null) {
            Log.e("BiometricManager", "Failure in canAuthenticate(). BiometricManager was null.");
            return 1;
        }
        return Api30Impl.canAuthenticate(mBiometricManager, n);
    }
    
    private static class Api29Impl
    {
        static int canAuthenticate(final android.hardware.biometrics.BiometricManager biometricManager) {
            return biometricManager.canAuthenticate();
        }
        
        static android.hardware.biometrics.BiometricManager create(final Context context) {
            return (android.hardware.biometrics.BiometricManager)context.getSystemService((Class)android.hardware.biometrics.BiometricManager.class);
        }
        
        static Method getCanAuthenticateWithCryptoMethod() {
            try {
                return android.hardware.biometrics.BiometricManager.class.getMethod("canAuthenticate", BiometricPrompt$CryptoObject.class);
            }
            catch (final NoSuchMethodException ex) {
                return null;
            }
        }
    }
    
    private static class Api30Impl
    {
        static int canAuthenticate(final android.hardware.biometrics.BiometricManager biometricManager, final int n) {
            return biometricManager.canAuthenticate(n);
        }
    }
    
    public interface Authenticators
    {
        public static final int BIOMETRIC_STRONG = 15;
        public static final int BIOMETRIC_WEAK = 255;
        public static final int DEVICE_CREDENTIAL = 32768;
    }
    
    private static class DefaultInjector implements Injector
    {
        private final Context mContext;
        
        DefaultInjector(final Context context) {
            this.mContext = context.getApplicationContext();
        }
        
        @Override
        public android.hardware.biometrics.BiometricManager getBiometricManager() {
            return Api29Impl.create(this.mContext);
        }
        
        @Override
        public FingerprintManagerCompat getFingerprintManager() {
            return FingerprintManagerCompat.from(this.mContext);
        }
        
        @Override
        public boolean isDeviceSecurable() {
            return KeyguardUtils.getKeyguardManager(this.mContext) != null;
        }
        
        @Override
        public boolean isDeviceSecuredWithCredential() {
            return KeyguardUtils.isDeviceSecuredWithCredential(this.mContext);
        }
        
        @Override
        public boolean isFingerprintHardwarePresent() {
            return PackageUtils.hasSystemFeatureFingerprint(this.mContext);
        }
        
        @Override
        public boolean isStrongBiometricGuaranteed() {
            return DeviceUtils.canAssumeStrongBiometrics(this.mContext, Build.MODEL);
        }
    }
    
    interface Injector
    {
        android.hardware.biometrics.BiometricManager getBiometricManager();
        
        FingerprintManagerCompat getFingerprintManager();
        
        boolean isDeviceSecurable();
        
        boolean isDeviceSecuredWithCredential();
        
        boolean isFingerprintHardwarePresent();
        
        boolean isStrongBiometricGuaranteed();
    }
}

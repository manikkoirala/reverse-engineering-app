// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.os.Build$VERSION;
import android.app.KeyguardManager;
import android.content.Context;

class KeyguardUtils
{
    private KeyguardUtils() {
    }
    
    static KeyguardManager getKeyguardManager(final Context context) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getKeyguardManager(context);
        }
        final Object systemService = context.getSystemService("keyguard");
        KeyguardManager keyguardManager;
        if (systemService instanceof KeyguardManager) {
            keyguardManager = (KeyguardManager)systemService;
        }
        else {
            keyguardManager = null;
        }
        return keyguardManager;
    }
    
    static boolean isDeviceSecuredWithCredential(final Context context) {
        final KeyguardManager keyguardManager = getKeyguardManager(context);
        if (keyguardManager == null) {
            return false;
        }
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.isDeviceSecure(keyguardManager);
        }
        return Build$VERSION.SDK_INT >= 16 && Api16Impl.isKeyguardSecure(keyguardManager);
    }
    
    private static class Api16Impl
    {
        static boolean isKeyguardSecure(final KeyguardManager keyguardManager) {
            return keyguardManager.isKeyguardSecure();
        }
    }
    
    private static class Api23Impl
    {
        static KeyguardManager getKeyguardManager(final Context context) {
            return (KeyguardManager)context.getSystemService((Class)KeyguardManager.class);
        }
        
        static boolean isDeviceSecure(final KeyguardManager keyguardManager) {
            return keyguardManager.isDeviceSecure();
        }
    }
}

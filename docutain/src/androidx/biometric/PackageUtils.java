// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.content.pm.PackageManager;
import android.os.Build$VERSION;
import android.content.Context;

class PackageUtils
{
    private PackageUtils() {
    }
    
    static boolean hasSystemFeatureFingerprint(final Context context) {
        return Build$VERSION.SDK_INT >= 23 && context != null && context.getPackageManager() != null && Api23Impl.hasSystemFeatureFingerprint(context.getPackageManager());
    }
    
    private static class Api23Impl
    {
        static boolean hasSystemFeatureFingerprint(final PackageManager packageManager) {
            return packageManager.hasSystemFeature("android.hardware.fingerprint");
        }
    }
}

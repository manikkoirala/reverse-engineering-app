// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import java.security.spec.AlgorithmParameterSpec;
import android.security.keystore.KeyGenParameterSpec;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import android.security.identity.IdentityCredential;
import javax.crypto.Mac;
import java.security.Signature;
import android.os.Build$VERSION;
import android.hardware.biometrics.BiometricPrompt$CryptoObject;
import android.security.keystore.KeyGenParameterSpec$Builder;
import android.util.Log;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.KeyStoreException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.UnrecoverableKeyException;
import java.io.IOException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import java.security.Key;
import javax.crypto.KeyGenerator;
import java.security.KeyStore;

class CryptoObjectUtils
{
    private static final String FAKE_KEY_NAME = "androidxBiometric";
    private static final String KEYSTORE_INSTANCE = "AndroidKeyStore";
    private static final String TAG = "CryptoObjectUtils";
    
    private CryptoObjectUtils() {
    }
    
    static BiometricPrompt.CryptoObject createFakeCryptoObject() {
        Object instance2 = null;
        try {
            final KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
            instance.load(null);
            final KeyGenParameterSpec$Builder keyGenParameterSpecBuilder = Api23Impl.createKeyGenParameterSpecBuilder("androidxBiometric", 3);
            Api23Impl.setBlockModeCBC(keyGenParameterSpecBuilder);
            Api23Impl.setEncryptionPaddingPKCS7(keyGenParameterSpecBuilder);
            instance2 = KeyGenerator.getInstance("AES", "AndroidKeyStore");
            Api23Impl.initKeyGenerator((KeyGenerator)instance2, Api23Impl.buildKeyGenParameterSpec(keyGenParameterSpecBuilder));
            ((KeyGenerator)instance2).generateKey();
            instance2 = instance.getKey("androidxBiometric", null);
            final Cipher instance3 = Cipher.getInstance("AES/CBC/PKCS7Padding");
            instance3.init(1, (Key)instance2);
            instance2 = new BiometricPrompt.CryptoObject(instance3);
            return (BiometricPrompt.CryptoObject)instance2;
        }
        catch (final NoSuchProviderException instance2) {}
        catch (final IOException instance2) {}
        catch (final UnrecoverableKeyException instance2) {}
        catch (final InvalidAlgorithmParameterException instance2) {}
        catch (final InvalidKeyException instance2) {}
        catch (final KeyStoreException instance2) {}
        catch (final CertificateException instance2) {}
        catch (final NoSuchAlgorithmException instance2) {}
        catch (final NoSuchPaddingException ex) {}
        Log.w("CryptoObjectUtils", "Failed to create fake crypto object.", (Throwable)instance2);
        return null;
    }
    
    static BiometricPrompt.CryptoObject unwrapFromBiometricPrompt(final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject) {
        final BiometricPrompt.CryptoObject cryptoObject = null;
        if (biometricPrompt$CryptoObject == null) {
            return null;
        }
        final Cipher cipher = Api28Impl.getCipher(biometricPrompt$CryptoObject);
        if (cipher != null) {
            return new BiometricPrompt.CryptoObject(cipher);
        }
        final Signature signature = Api28Impl.getSignature(biometricPrompt$CryptoObject);
        if (signature != null) {
            return new BiometricPrompt.CryptoObject(signature);
        }
        final Mac mac = Api28Impl.getMac(biometricPrompt$CryptoObject);
        if (mac != null) {
            return new BiometricPrompt.CryptoObject(mac);
        }
        BiometricPrompt.CryptoObject cryptoObject2 = cryptoObject;
        if (Build$VERSION.SDK_INT >= 30) {
            final IdentityCredential identityCredential = Api30Impl.getIdentityCredential(biometricPrompt$CryptoObject);
            cryptoObject2 = cryptoObject;
            if (identityCredential != null) {
                cryptoObject2 = new BiometricPrompt.CryptoObject(identityCredential);
            }
        }
        return cryptoObject2;
    }
    
    static BiometricPrompt.CryptoObject unwrapFromFingerprintManager(final FingerprintManagerCompat.CryptoObject cryptoObject) {
        final BiometricPrompt.CryptoObject cryptoObject2 = null;
        if (cryptoObject == null) {
            return null;
        }
        final Cipher cipher = cryptoObject.getCipher();
        if (cipher != null) {
            return new BiometricPrompt.CryptoObject(cipher);
        }
        final Signature signature = cryptoObject.getSignature();
        if (signature != null) {
            return new BiometricPrompt.CryptoObject(signature);
        }
        final Mac mac = cryptoObject.getMac();
        Object o = cryptoObject2;
        if (mac != null) {
            o = new BiometricPrompt.CryptoObject(mac);
        }
        return (BiometricPrompt.CryptoObject)o;
    }
    
    static BiometricPrompt$CryptoObject wrapForBiometricPrompt(final BiometricPrompt.CryptoObject cryptoObject) {
        if (cryptoObject == null) {
            return null;
        }
        final Cipher cipher = cryptoObject.getCipher();
        if (cipher != null) {
            return Api28Impl.create(cipher);
        }
        final Signature signature = cryptoObject.getSignature();
        if (signature != null) {
            return Api28Impl.create(signature);
        }
        final Mac mac = cryptoObject.getMac();
        if (mac != null) {
            return Api28Impl.create(mac);
        }
        if (Build$VERSION.SDK_INT >= 30) {
            final IdentityCredential identityCredential = cryptoObject.getIdentityCredential();
            if (identityCredential != null) {
                return Api30Impl.create(identityCredential);
            }
        }
        return null;
    }
    
    static FingerprintManagerCompat.CryptoObject wrapForFingerprintManager(final BiometricPrompt.CryptoObject cryptoObject) {
        if (cryptoObject == null) {
            return null;
        }
        final Cipher cipher = cryptoObject.getCipher();
        if (cipher != null) {
            return new FingerprintManagerCompat.CryptoObject(cipher);
        }
        final Signature signature = cryptoObject.getSignature();
        if (signature != null) {
            return new FingerprintManagerCompat.CryptoObject(signature);
        }
        final Mac mac = cryptoObject.getMac();
        if (mac != null) {
            return new FingerprintManagerCompat.CryptoObject(mac);
        }
        if (Build$VERSION.SDK_INT >= 30 && cryptoObject.getIdentityCredential() != null) {
            Log.e("CryptoObjectUtils", "Identity credential is not supported by FingerprintManager.");
        }
        return null;
    }
    
    private static class Api23Impl
    {
        static KeyGenParameterSpec buildKeyGenParameterSpec(final KeyGenParameterSpec$Builder keyGenParameterSpec$Builder) {
            return keyGenParameterSpec$Builder.build();
        }
        
        static KeyGenParameterSpec$Builder createKeyGenParameterSpecBuilder(final String s, final int n) {
            return new KeyGenParameterSpec$Builder(s, n);
        }
        
        static void initKeyGenerator(final KeyGenerator keyGenerator, final KeyGenParameterSpec params) throws InvalidAlgorithmParameterException {
            keyGenerator.init((AlgorithmParameterSpec)params);
        }
        
        static void setBlockModeCBC(final KeyGenParameterSpec$Builder keyGenParameterSpec$Builder) {
            keyGenParameterSpec$Builder.setBlockModes(new String[] { "CBC" });
        }
        
        static void setEncryptionPaddingPKCS7(final KeyGenParameterSpec$Builder keyGenParameterSpec$Builder) {
            keyGenParameterSpec$Builder.setEncryptionPaddings(new String[] { "PKCS7Padding" });
        }
    }
    
    private static class Api28Impl
    {
        static BiometricPrompt$CryptoObject create(final Signature signature) {
            return new BiometricPrompt$CryptoObject(signature);
        }
        
        static BiometricPrompt$CryptoObject create(final Cipher cipher) {
            return new BiometricPrompt$CryptoObject(cipher);
        }
        
        static BiometricPrompt$CryptoObject create(final Mac mac) {
            return new BiometricPrompt$CryptoObject(mac);
        }
        
        static Cipher getCipher(final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject) {
            return biometricPrompt$CryptoObject.getCipher();
        }
        
        static Mac getMac(final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject) {
            return biometricPrompt$CryptoObject.getMac();
        }
        
        static Signature getSignature(final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject) {
            return biometricPrompt$CryptoObject.getSignature();
        }
    }
    
    private static class Api30Impl
    {
        static BiometricPrompt$CryptoObject create(final IdentityCredential identityCredential) {
            return new BiometricPrompt$CryptoObject(identityCredential);
        }
        
        static IdentityCredential getIdentityCredential(final BiometricPrompt$CryptoObject biometricPrompt$CryptoObject) {
            return biometricPrompt$CryptoObject.getIdentityCredential();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.util.Log;
import android.content.Context;

class ErrorUtils
{
    private ErrorUtils() {
    }
    
    static String getFingerprintErrorString(final Context context, final int i) {
        if (context == null) {
            return "";
        }
        if (i != 1) {
            if (i != 7) {
                switch (i) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unknown error code: ");
                        sb.append(i);
                        Log.e("BiometricUtils", sb.toString());
                        return context.getString(R.string.default_error_msg);
                    }
                    case 12: {
                        return context.getString(R.string.fingerprint_error_hw_not_present);
                    }
                    case 11: {
                        return context.getString(R.string.fingerprint_error_no_fingerprints);
                    }
                    case 10: {
                        return context.getString(R.string.fingerprint_error_user_canceled);
                    }
                    case 9: {
                        break;
                    }
                }
            }
            return context.getString(R.string.fingerprint_error_lockout);
        }
        return context.getString(R.string.fingerprint_error_hw_not_available);
    }
    
    static boolean isKnownError(final int n) {
        switch (n) {
            default: {
                return false;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15: {
                return true;
            }
        }
    }
    
    static boolean isLockoutError(final int n) {
        return n == 7 || n == 9;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.os.Build$VERSION;
import android.hardware.biometrics.BiometricPrompt$AuthenticationResult;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import android.hardware.biometrics.BiometricPrompt$AuthenticationCallback;

class AuthenticationCallbackProvider
{
    private BiometricPrompt$AuthenticationCallback mBiometricCallback;
    private FingerprintManagerCompat.AuthenticationCallback mFingerprintCallback;
    final Listener mListener;
    
    AuthenticationCallbackProvider(final Listener mListener) {
        this.mListener = mListener;
    }
    
    BiometricPrompt$AuthenticationCallback getBiometricCallback() {
        if (this.mBiometricCallback == null) {
            this.mBiometricCallback = Api28Impl.createCallback(this.mListener);
        }
        return this.mBiometricCallback;
    }
    
    FingerprintManagerCompat.AuthenticationCallback getFingerprintCallback() {
        if (this.mFingerprintCallback == null) {
            this.mFingerprintCallback = new FingerprintManagerCompat.AuthenticationCallback(this) {
                final AuthenticationCallbackProvider this$0;
                
                @Override
                public void onAuthenticationError(final int n, final CharSequence charSequence) {
                    this.this$0.mListener.onError(n, charSequence);
                }
                
                @Override
                public void onAuthenticationFailed() {
                    this.this$0.mListener.onFailure();
                }
                
                @Override
                public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                    this.this$0.mListener.onHelp(charSequence);
                }
                
                @Override
                public void onAuthenticationSucceeded(final AuthenticationResult authenticationResult) {
                    BiometricPrompt.CryptoObject unwrapFromFingerprintManager;
                    if (authenticationResult != null) {
                        unwrapFromFingerprintManager = CryptoObjectUtils.unwrapFromFingerprintManager(authenticationResult.getCryptoObject());
                    }
                    else {
                        unwrapFromFingerprintManager = null;
                    }
                    this.this$0.mListener.onSuccess(new BiometricPrompt.AuthenticationResult(unwrapFromFingerprintManager, 2));
                }
            };
        }
        return this.mFingerprintCallback;
    }
    
    private static class Api28Impl
    {
        static BiometricPrompt$AuthenticationCallback createCallback(final Listener listener) {
            return new BiometricPrompt$AuthenticationCallback(listener) {
                final Listener val$listener;
                
                public void onAuthenticationError(final int n, final CharSequence charSequence) {
                    this.val$listener.onError(n, charSequence);
                }
                
                public void onAuthenticationFailed() {
                    this.val$listener.onFailure();
                }
                
                public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                }
                
                public void onAuthenticationSucceeded(final BiometricPrompt$AuthenticationResult biometricPrompt$AuthenticationResult) {
                    BiometricPrompt.CryptoObject unwrapFromBiometricPrompt;
                    if (biometricPrompt$AuthenticationResult != null) {
                        unwrapFromBiometricPrompt = CryptoObjectUtils.unwrapFromBiometricPrompt(biometricPrompt$AuthenticationResult.getCryptoObject());
                    }
                    else {
                        unwrapFromBiometricPrompt = null;
                    }
                    final int sdk_INT = Build$VERSION.SDK_INT;
                    int authenticationType = -1;
                    if (sdk_INT >= 30) {
                        if (biometricPrompt$AuthenticationResult != null) {
                            authenticationType = Api30Impl.getAuthenticationType(biometricPrompt$AuthenticationResult);
                        }
                    }
                    else if (Build$VERSION.SDK_INT != 29) {
                        authenticationType = 2;
                    }
                    this.val$listener.onSuccess(new BiometricPrompt.AuthenticationResult(unwrapFromBiometricPrompt, authenticationType));
                }
            };
        }
    }
    
    private static class Api30Impl
    {
        static int getAuthenticationType(final BiometricPrompt$AuthenticationResult biometricPrompt$AuthenticationResult) {
            return biometricPrompt$AuthenticationResult.getAuthenticationType();
        }
    }
    
    static class Listener
    {
        void onError(final int n, final CharSequence charSequence) {
        }
        
        void onFailure() {
        }
        
        void onHelp(final CharSequence charSequence) {
        }
        
        void onSuccess(final BiometricPrompt.AuthenticationResult authenticationResult) {
        }
    }
}

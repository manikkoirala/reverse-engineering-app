// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.content.DialogInterface;
import android.os.Handler;
import java.lang.ref.WeakReference;
import androidx.lifecycle.LiveData;
import android.os.Looper;
import android.content.DialogInterface$OnClickListener;
import java.util.concurrent.Executor;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BiometricViewModel extends ViewModel
{
    private AuthenticationCallbackProvider mAuthenticationCallbackProvider;
    private MutableLiveData<BiometricErrorData> mAuthenticationError;
    private MutableLiveData<CharSequence> mAuthenticationHelpMessage;
    private MutableLiveData<BiometricPrompt.AuthenticationResult> mAuthenticationResult;
    private int mCanceledFrom;
    private CancellationSignalProvider mCancellationSignalProvider;
    private BiometricPrompt.AuthenticationCallback mClientCallback;
    private Executor mClientExecutor;
    private BiometricPrompt.CryptoObject mCryptoObject;
    private MutableLiveData<CharSequence> mFingerprintDialogHelpMessage;
    private int mFingerprintDialogPreviousState;
    private MutableLiveData<Integer> mFingerprintDialogState;
    private MutableLiveData<Boolean> mIsAuthenticationFailurePending;
    private boolean mIsAwaitingResult;
    private boolean mIsConfirmingDeviceCredential;
    private boolean mIsDelayingPrompt;
    private MutableLiveData<Boolean> mIsFingerprintDialogCancelPending;
    private boolean mIsFingerprintDialogDismissedInstantly;
    private boolean mIsIgnoringCancel;
    private MutableLiveData<Boolean> mIsNegativeButtonPressPending;
    private boolean mIsPromptShowing;
    private DialogInterface$OnClickListener mNegativeButtonListener;
    private CharSequence mNegativeButtonTextOverride;
    private BiometricPrompt.PromptInfo mPromptInfo;
    
    public BiometricViewModel() {
        this.mCanceledFrom = 0;
        this.mIsFingerprintDialogDismissedInstantly = true;
        this.mFingerprintDialogPreviousState = 0;
    }
    
    private static <T> void updateValue(final MutableLiveData<T> mutableLiveData, final T value) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            mutableLiveData.setValue(value);
        }
        else {
            mutableLiveData.postValue(value);
        }
    }
    
    int getAllowedAuthenticators() {
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        int consolidatedAuthenticators;
        if (mPromptInfo != null) {
            consolidatedAuthenticators = AuthenticatorUtils.getConsolidatedAuthenticators(mPromptInfo, this.mCryptoObject);
        }
        else {
            consolidatedAuthenticators = 0;
        }
        return consolidatedAuthenticators;
    }
    
    AuthenticationCallbackProvider getAuthenticationCallbackProvider() {
        if (this.mAuthenticationCallbackProvider == null) {
            this.mAuthenticationCallbackProvider = new AuthenticationCallbackProvider((AuthenticationCallbackProvider.Listener)new CallbackListener(this));
        }
        return this.mAuthenticationCallbackProvider;
    }
    
    MutableLiveData<BiometricErrorData> getAuthenticationError() {
        if (this.mAuthenticationError == null) {
            this.mAuthenticationError = new MutableLiveData<BiometricErrorData>();
        }
        return this.mAuthenticationError;
    }
    
    LiveData<CharSequence> getAuthenticationHelpMessage() {
        if (this.mAuthenticationHelpMessage == null) {
            this.mAuthenticationHelpMessage = new MutableLiveData<CharSequence>();
        }
        return this.mAuthenticationHelpMessage;
    }
    
    LiveData<BiometricPrompt.AuthenticationResult> getAuthenticationResult() {
        if (this.mAuthenticationResult == null) {
            this.mAuthenticationResult = new MutableLiveData<BiometricPrompt.AuthenticationResult>();
        }
        return this.mAuthenticationResult;
    }
    
    int getCanceledFrom() {
        return this.mCanceledFrom;
    }
    
    CancellationSignalProvider getCancellationSignalProvider() {
        if (this.mCancellationSignalProvider == null) {
            this.mCancellationSignalProvider = new CancellationSignalProvider();
        }
        return this.mCancellationSignalProvider;
    }
    
    BiometricPrompt.AuthenticationCallback getClientCallback() {
        if (this.mClientCallback == null) {
            this.mClientCallback = new BiometricPrompt.AuthenticationCallback(this) {
                final BiometricViewModel this$0;
            };
        }
        return this.mClientCallback;
    }
    
    Executor getClientExecutor() {
        Executor mClientExecutor = this.mClientExecutor;
        if (mClientExecutor == null) {
            mClientExecutor = new DefaultExecutor();
        }
        return mClientExecutor;
    }
    
    BiometricPrompt.CryptoObject getCryptoObject() {
        return this.mCryptoObject;
    }
    
    CharSequence getDescription() {
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        CharSequence description;
        if (mPromptInfo != null) {
            description = mPromptInfo.getDescription();
        }
        else {
            description = null;
        }
        return description;
    }
    
    LiveData<CharSequence> getFingerprintDialogHelpMessage() {
        if (this.mFingerprintDialogHelpMessage == null) {
            this.mFingerprintDialogHelpMessage = new MutableLiveData<CharSequence>();
        }
        return this.mFingerprintDialogHelpMessage;
    }
    
    int getFingerprintDialogPreviousState() {
        return this.mFingerprintDialogPreviousState;
    }
    
    LiveData<Integer> getFingerprintDialogState() {
        if (this.mFingerprintDialogState == null) {
            this.mFingerprintDialogState = new MutableLiveData<Integer>();
        }
        return this.mFingerprintDialogState;
    }
    
    int getInferredAuthenticationResultType() {
        final int allowedAuthenticators = this.getAllowedAuthenticators();
        if (AuthenticatorUtils.isSomeBiometricAllowed(allowedAuthenticators) && !AuthenticatorUtils.isDeviceCredentialAllowed(allowedAuthenticators)) {
            return 2;
        }
        return -1;
    }
    
    DialogInterface$OnClickListener getNegativeButtonListener() {
        if (this.mNegativeButtonListener == null) {
            this.mNegativeButtonListener = (DialogInterface$OnClickListener)new NegativeButtonListener(this);
        }
        return this.mNegativeButtonListener;
    }
    
    CharSequence getNegativeButtonText() {
        final CharSequence mNegativeButtonTextOverride = this.mNegativeButtonTextOverride;
        if (mNegativeButtonTextOverride != null) {
            return mNegativeButtonTextOverride;
        }
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        if (mPromptInfo != null) {
            return mPromptInfo.getNegativeButtonText();
        }
        return null;
    }
    
    CharSequence getSubtitle() {
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        CharSequence subtitle;
        if (mPromptInfo != null) {
            subtitle = mPromptInfo.getSubtitle();
        }
        else {
            subtitle = null;
        }
        return subtitle;
    }
    
    CharSequence getTitle() {
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        CharSequence title;
        if (mPromptInfo != null) {
            title = mPromptInfo.getTitle();
        }
        else {
            title = null;
        }
        return title;
    }
    
    LiveData<Boolean> isAuthenticationFailurePending() {
        if (this.mIsAuthenticationFailurePending == null) {
            this.mIsAuthenticationFailurePending = new MutableLiveData<Boolean>();
        }
        return this.mIsAuthenticationFailurePending;
    }
    
    boolean isAwaitingResult() {
        return this.mIsAwaitingResult;
    }
    
    boolean isConfirmationRequired() {
        final BiometricPrompt.PromptInfo mPromptInfo = this.mPromptInfo;
        return mPromptInfo == null || mPromptInfo.isConfirmationRequired();
    }
    
    boolean isConfirmingDeviceCredential() {
        return this.mIsConfirmingDeviceCredential;
    }
    
    boolean isDelayingPrompt() {
        return this.mIsDelayingPrompt;
    }
    
    LiveData<Boolean> isFingerprintDialogCancelPending() {
        if (this.mIsFingerprintDialogCancelPending == null) {
            this.mIsFingerprintDialogCancelPending = new MutableLiveData<Boolean>();
        }
        return this.mIsFingerprintDialogCancelPending;
    }
    
    boolean isFingerprintDialogDismissedInstantly() {
        return this.mIsFingerprintDialogDismissedInstantly;
    }
    
    boolean isIgnoringCancel() {
        return this.mIsIgnoringCancel;
    }
    
    LiveData<Boolean> isNegativeButtonPressPending() {
        if (this.mIsNegativeButtonPressPending == null) {
            this.mIsNegativeButtonPressPending = new MutableLiveData<Boolean>();
        }
        return this.mIsNegativeButtonPressPending;
    }
    
    boolean isPromptShowing() {
        return this.mIsPromptShowing;
    }
    
    void resetClientCallback() {
        this.mClientCallback = null;
    }
    
    void setAuthenticationError(final BiometricErrorData biometricErrorData) {
        if (this.mAuthenticationError == null) {
            this.mAuthenticationError = new MutableLiveData<BiometricErrorData>();
        }
        updateValue(this.mAuthenticationError, biometricErrorData);
    }
    
    void setAuthenticationFailurePending(final boolean b) {
        if (this.mIsAuthenticationFailurePending == null) {
            this.mIsAuthenticationFailurePending = new MutableLiveData<Boolean>();
        }
        updateValue(this.mIsAuthenticationFailurePending, b);
    }
    
    void setAuthenticationHelpMessage(final CharSequence charSequence) {
        if (this.mAuthenticationHelpMessage == null) {
            this.mAuthenticationHelpMessage = new MutableLiveData<CharSequence>();
        }
        updateValue(this.mAuthenticationHelpMessage, charSequence);
    }
    
    void setAuthenticationResult(final BiometricPrompt.AuthenticationResult authenticationResult) {
        if (this.mAuthenticationResult == null) {
            this.mAuthenticationResult = new MutableLiveData<BiometricPrompt.AuthenticationResult>();
        }
        updateValue(this.mAuthenticationResult, authenticationResult);
    }
    
    void setAwaitingResult(final boolean mIsAwaitingResult) {
        this.mIsAwaitingResult = mIsAwaitingResult;
    }
    
    void setCanceledFrom(final int mCanceledFrom) {
        this.mCanceledFrom = mCanceledFrom;
    }
    
    void setClientCallback(final BiometricPrompt.AuthenticationCallback mClientCallback) {
        this.mClientCallback = mClientCallback;
    }
    
    void setClientExecutor(final Executor mClientExecutor) {
        this.mClientExecutor = mClientExecutor;
    }
    
    void setConfirmingDeviceCredential(final boolean mIsConfirmingDeviceCredential) {
        this.mIsConfirmingDeviceCredential = mIsConfirmingDeviceCredential;
    }
    
    void setCryptoObject(final BiometricPrompt.CryptoObject mCryptoObject) {
        this.mCryptoObject = mCryptoObject;
    }
    
    void setDelayingPrompt(final boolean mIsDelayingPrompt) {
        this.mIsDelayingPrompt = mIsDelayingPrompt;
    }
    
    void setFingerprintDialogCancelPending(final boolean b) {
        if (this.mIsFingerprintDialogCancelPending == null) {
            this.mIsFingerprintDialogCancelPending = new MutableLiveData<Boolean>();
        }
        updateValue(this.mIsFingerprintDialogCancelPending, b);
    }
    
    void setFingerprintDialogDismissedInstantly(final boolean mIsFingerprintDialogDismissedInstantly) {
        this.mIsFingerprintDialogDismissedInstantly = mIsFingerprintDialogDismissedInstantly;
    }
    
    void setFingerprintDialogHelpMessage(final CharSequence charSequence) {
        if (this.mFingerprintDialogHelpMessage == null) {
            this.mFingerprintDialogHelpMessage = new MutableLiveData<CharSequence>();
        }
        updateValue(this.mFingerprintDialogHelpMessage, charSequence);
    }
    
    void setFingerprintDialogPreviousState(final int mFingerprintDialogPreviousState) {
        this.mFingerprintDialogPreviousState = mFingerprintDialogPreviousState;
    }
    
    void setFingerprintDialogState(final int i) {
        if (this.mFingerprintDialogState == null) {
            this.mFingerprintDialogState = new MutableLiveData<Integer>();
        }
        updateValue(this.mFingerprintDialogState, i);
    }
    
    void setIgnoringCancel(final boolean mIsIgnoringCancel) {
        this.mIsIgnoringCancel = mIsIgnoringCancel;
    }
    
    void setNegativeButtonPressPending(final boolean b) {
        if (this.mIsNegativeButtonPressPending == null) {
            this.mIsNegativeButtonPressPending = new MutableLiveData<Boolean>();
        }
        updateValue(this.mIsNegativeButtonPressPending, b);
    }
    
    void setNegativeButtonTextOverride(final CharSequence mNegativeButtonTextOverride) {
        this.mNegativeButtonTextOverride = mNegativeButtonTextOverride;
    }
    
    void setPromptInfo(final BiometricPrompt.PromptInfo mPromptInfo) {
        this.mPromptInfo = mPromptInfo;
    }
    
    void setPromptShowing(final boolean mIsPromptShowing) {
        this.mIsPromptShowing = mIsPromptShowing;
    }
    
    private static final class CallbackListener extends Listener
    {
        private final WeakReference<BiometricViewModel> mViewModelRef;
        
        CallbackListener(final BiometricViewModel referent) {
            this.mViewModelRef = new WeakReference<BiometricViewModel>(referent);
        }
        
        @Override
        void onError(final int n, final CharSequence charSequence) {
            if (this.mViewModelRef.get() != null && !this.mViewModelRef.get().isConfirmingDeviceCredential() && this.mViewModelRef.get().isAwaitingResult()) {
                this.mViewModelRef.get().setAuthenticationError(new BiometricErrorData(n, charSequence));
            }
        }
        
        @Override
        void onFailure() {
            if (this.mViewModelRef.get() != null && this.mViewModelRef.get().isAwaitingResult()) {
                this.mViewModelRef.get().setAuthenticationFailurePending(true);
            }
        }
        
        @Override
        void onHelp(final CharSequence authenticationHelpMessage) {
            if (this.mViewModelRef.get() != null) {
                this.mViewModelRef.get().setAuthenticationHelpMessage(authenticationHelpMessage);
            }
        }
        
        @Override
        void onSuccess(final BiometricPrompt.AuthenticationResult authenticationResult) {
            if (this.mViewModelRef.get() != null && this.mViewModelRef.get().isAwaitingResult()) {
                BiometricPrompt.AuthenticationResult authenticationResult2 = authenticationResult;
                if (authenticationResult.getAuthenticationType() == -1) {
                    authenticationResult2 = new BiometricPrompt.AuthenticationResult(authenticationResult.getCryptoObject(), this.mViewModelRef.get().getInferredAuthenticationResultType());
                }
                this.mViewModelRef.get().setAuthenticationResult(authenticationResult2);
            }
        }
    }
    
    private static class DefaultExecutor implements Executor
    {
        private final Handler mHandler;
        
        DefaultExecutor() {
            this.mHandler = new Handler(Looper.getMainLooper());
        }
        
        @Override
        public void execute(final Runnable runnable) {
            this.mHandler.post(runnable);
        }
    }
    
    private static class NegativeButtonListener implements DialogInterface$OnClickListener
    {
        private final WeakReference<BiometricViewModel> mViewModelRef;
        
        NegativeButtonListener(final BiometricViewModel referent) {
            this.mViewModelRef = new WeakReference<BiometricViewModel>(referent);
        }
        
        public void onClick(final DialogInterface dialogInterface, final int n) {
            if (this.mViewModelRef.get() != null) {
                this.mViewModelRef.get().setNegativeButtonPressPending(true);
            }
        }
    }
}

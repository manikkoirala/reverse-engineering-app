// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.view.View;
import android.content.DialogInterface$OnClickListener;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.appcompat.app.AlertDialog;
import android.app.Dialog;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.graphics.drawable.Drawable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.ViewModelProvider;
import android.os.Looper;
import android.widget.TextView;
import android.os.Handler;
import android.widget.ImageView;
import androidx.fragment.app.DialogFragment;

public class FingerprintDialogFragment extends DialogFragment
{
    private static final int MESSAGE_DISPLAY_TIME_MS = 2000;
    static final int STATE_FINGERPRINT = 1;
    static final int STATE_FINGERPRINT_AUTHENTICATED = 3;
    static final int STATE_FINGERPRINT_ERROR = 2;
    static final int STATE_NONE = 0;
    private static final String TAG = "FingerprintFragment";
    private int mErrorTextColor;
    private ImageView mFingerprintIcon;
    final Handler mHandler;
    TextView mHelpMessageView;
    private int mNormalTextColor;
    final Runnable mResetDialogRunnable;
    BiometricViewModel mViewModel;
    
    public FingerprintDialogFragment() {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mResetDialogRunnable = new Runnable() {
            final FingerprintDialogFragment this$0;
            
            @Override
            public void run() {
                this.this$0.resetDialog();
            }
        };
    }
    
    private void connectViewModel() {
        final FragmentActivity activity = this.getActivity();
        if (activity == null) {
            return;
        }
        final BiometricViewModel mViewModel = new ViewModelProvider(activity).get(BiometricViewModel.class);
        this.mViewModel = mViewModel;
        mViewModel.getFingerprintDialogState().observe(this, new Observer<Integer>(this) {
            final FingerprintDialogFragment this$0;
            
            @Override
            public void onChanged(final Integer n) {
                this.this$0.mHandler.removeCallbacks(this.this$0.mResetDialogRunnable);
                this.this$0.updateFingerprintIcon(n);
                this.this$0.updateHelpMessageColor(n);
                this.this$0.mHandler.postDelayed(this.this$0.mResetDialogRunnable, 2000L);
            }
        });
        this.mViewModel.getFingerprintDialogHelpMessage().observe(this, new Observer<CharSequence>(this) {
            final FingerprintDialogFragment this$0;
            
            @Override
            public void onChanged(final CharSequence charSequence) {
                this.this$0.mHandler.removeCallbacks(this.this$0.mResetDialogRunnable);
                this.this$0.updateHelpMessageText(charSequence);
                this.this$0.mHandler.postDelayed(this.this$0.mResetDialogRunnable, 2000L);
            }
        });
    }
    
    private Drawable getAssetForTransition(int n, final int n2) {
        final Context context = this.getContext();
        if (context == null) {
            Log.w("FingerprintFragment", "Unable to get asset. Context is null.");
            return null;
        }
        if (n == 0 && n2 == 1) {
            n = R.drawable.fingerprint_dialog_fp_icon;
        }
        else if (n == 1 && n2 == 2) {
            n = R.drawable.fingerprint_dialog_error;
        }
        else if (n == 2 && n2 == 1) {
            n = R.drawable.fingerprint_dialog_fp_icon;
        }
        else {
            if (n != 1 || n2 != 3) {
                return null;
            }
            n = R.drawable.fingerprint_dialog_fp_icon;
        }
        return ContextCompat.getDrawable(context, n);
    }
    
    private int getThemedColorFor(int color) {
        final Context context = this.getContext();
        final FragmentActivity activity = this.getActivity();
        if (context != null && activity != null) {
            final TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(color, typedValue, true);
            final TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(typedValue.data, new int[] { color });
            color = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
            return color;
        }
        Log.w("FingerprintFragment", "Unable to get themed color. Context or activity is null.");
        return 0;
    }
    
    static FingerprintDialogFragment newInstance() {
        return new FingerprintDialogFragment();
    }
    
    private boolean shouldAnimateForTransition(final int n, final int n2) {
        return (n != 0 || n2 != 1) && ((n == 1 && n2 == 2) || (n == 2 && n2 == 1));
    }
    
    @Override
    public void onCancel(final DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        this.mViewModel.setFingerprintDialogCancelPending(true);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.connectViewModel();
        if (Build$VERSION.SDK_INT >= 26) {
            this.mErrorTextColor = this.getThemedColorFor(Api26Impl.getColorErrorAttr());
        }
        else {
            final Context context = this.getContext();
            int color;
            if (context != null) {
                color = ContextCompat.getColor(context, R.color.biometric_error_color);
            }
            else {
                color = 0;
            }
            this.mErrorTextColor = color;
        }
        this.mNormalTextColor = this.getThemedColorFor(16842808);
    }
    
    @Override
    public Dialog onCreateDialog(final Bundle bundle) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.requireContext());
        builder.setTitle(this.mViewModel.getTitle());
        final View inflate = LayoutInflater.from(builder.getContext()).inflate(R.layout.fingerprint_dialog_layout, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(R.id.fingerprint_subtitle);
        if (textView != null) {
            final CharSequence subtitle = this.mViewModel.getSubtitle();
            if (TextUtils.isEmpty(subtitle)) {
                textView.setVisibility(8);
            }
            else {
                textView.setVisibility(0);
                textView.setText(subtitle);
            }
        }
        final TextView textView2 = (TextView)inflate.findViewById(R.id.fingerprint_description);
        if (textView2 != null) {
            final CharSequence description = this.mViewModel.getDescription();
            if (TextUtils.isEmpty(description)) {
                textView2.setVisibility(8);
            }
            else {
                textView2.setVisibility(0);
                textView2.setText(description);
            }
        }
        this.mFingerprintIcon = (ImageView)inflate.findViewById(R.id.fingerprint_icon);
        this.mHelpMessageView = (TextView)inflate.findViewById(R.id.fingerprint_error);
        CharSequence charSequence;
        if (AuthenticatorUtils.isDeviceCredentialAllowed(this.mViewModel.getAllowedAuthenticators())) {
            charSequence = this.getString(R.string.confirm_device_credential_password);
        }
        else {
            charSequence = this.mViewModel.getNegativeButtonText();
        }
        builder.setNegativeButton(charSequence, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            final FingerprintDialogFragment this$0;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.this$0.mViewModel.setNegativeButtonPressPending(true);
            }
        });
        builder.setView(inflate);
        final AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(false);
        return create;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.mHandler.removeCallbacksAndMessages((Object)null);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.mViewModel.setFingerprintDialogPreviousState(0);
        this.mViewModel.setFingerprintDialogState(1);
        this.mViewModel.setFingerprintDialogHelpMessage(this.getString(R.string.fingerprint_dialog_touch_sensor));
    }
    
    void resetDialog() {
        final Context context = this.getContext();
        if (context == null) {
            Log.w("FingerprintFragment", "Not resetting the dialog. Context is null.");
            return;
        }
        this.mViewModel.setFingerprintDialogState(1);
        this.mViewModel.setFingerprintDialogHelpMessage(context.getString(R.string.fingerprint_dialog_touch_sensor));
    }
    
    void updateFingerprintIcon(final int fingerprintDialogPreviousState) {
        if (this.mFingerprintIcon == null) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 23) {
            final int fingerprintDialogPreviousState2 = this.mViewModel.getFingerprintDialogPreviousState();
            final Drawable assetForTransition = this.getAssetForTransition(fingerprintDialogPreviousState2, fingerprintDialogPreviousState);
            if (assetForTransition == null) {
                return;
            }
            this.mFingerprintIcon.setImageDrawable(assetForTransition);
            if (this.shouldAnimateForTransition(fingerprintDialogPreviousState2, fingerprintDialogPreviousState)) {
                Api21Impl.startAnimation(assetForTransition);
            }
            this.mViewModel.setFingerprintDialogPreviousState(fingerprintDialogPreviousState);
        }
    }
    
    void updateHelpMessageColor(int textColor) {
        final TextView mHelpMessageView = this.mHelpMessageView;
        if (mHelpMessageView != null) {
            if (textColor == 2) {
                textColor = 1;
            }
            else {
                textColor = 0;
            }
            if (textColor != 0) {
                textColor = this.mErrorTextColor;
            }
            else {
                textColor = this.mNormalTextColor;
            }
            mHelpMessageView.setTextColor(textColor);
        }
    }
    
    void updateHelpMessageText(final CharSequence text) {
        final TextView mHelpMessageView = this.mHelpMessageView;
        if (mHelpMessageView != null) {
            mHelpMessageView.setText(text);
        }
    }
    
    private static class Api21Impl
    {
        static void startAnimation(final Drawable drawable) {
            if (drawable instanceof AnimatedVectorDrawable) {
                ((AnimatedVectorDrawable)drawable).start();
            }
        }
    }
    
    private static class Api26Impl
    {
        static int getColorErrorAttr() {
            return R.attr.colorError;
        }
    }
}

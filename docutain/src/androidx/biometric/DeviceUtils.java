// 
// Decompiled by Procyon v0.6.0
// 

package androidx.biometric;

import android.os.Build$VERSION;
import android.content.Context;

class DeviceUtils
{
    private DeviceUtils() {
    }
    
    static boolean canAssumeStrongBiometrics(final Context context, final String s) {
        return Build$VERSION.SDK_INT < 30 && isModelInList(context, s, R.array.assume_strong_biometrics_models);
    }
    
    private static boolean isModelInList(final Context context, final String s, int i) {
        if (s == null) {
            return false;
        }
        final String[] stringArray = context.getResources().getStringArray(i);
        int length;
        for (length = stringArray.length, i = 0; i < length; ++i) {
            if (s.equals(stringArray[i])) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean isModelInPrefixList(final Context context, final String s, int i) {
        if (s == null) {
            return false;
        }
        final String[] stringArray = context.getResources().getStringArray(i);
        int length;
        for (length = stringArray.length, i = 0; i < length; ++i) {
            if (s.startsWith(stringArray[i])) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean isVendorInList(final Context context, final String s, int i) {
        if (s == null) {
            return false;
        }
        final String[] stringArray = context.getResources().getStringArray(i);
        int length;
        for (length = stringArray.length, i = 0; i < length; ++i) {
            if (s.equalsIgnoreCase(stringArray[i])) {
                return true;
            }
        }
        return false;
    }
    
    static boolean shouldDelayShowingPrompt(final Context context, final String s) {
        return Build$VERSION.SDK_INT == 29 && isModelInList(context, s, R.array.delay_showing_prompt_models);
    }
    
    static boolean shouldHideFingerprintDialog(final Context context, final String s) {
        return Build$VERSION.SDK_INT == 28 && isModelInPrefixList(context, s, R.array.hide_fingerprint_instantly_prefixes);
    }
    
    static boolean shouldUseFingerprintForCrypto(final Context context, final String s, final String s2) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT != 28) {
            return false;
        }
        if (isVendorInList(context, s, R.array.crypto_fingerprint_fallback_vendors) || isModelInPrefixList(context, s2, R.array.crypto_fingerprint_fallback_prefixes)) {
            b = true;
        }
        return b;
    }
}

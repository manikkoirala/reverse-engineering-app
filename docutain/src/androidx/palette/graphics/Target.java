// 
// Decompiled by Procyon v0.6.0
// 

package androidx.palette.graphics;

public final class Target
{
    public static final Target DARK_MUTED;
    public static final Target DARK_VIBRANT;
    static final int INDEX_MAX = 2;
    static final int INDEX_MIN = 0;
    static final int INDEX_TARGET = 1;
    static final int INDEX_WEIGHT_LUMA = 1;
    static final int INDEX_WEIGHT_POP = 2;
    static final int INDEX_WEIGHT_SAT = 0;
    public static final Target LIGHT_MUTED;
    public static final Target LIGHT_VIBRANT;
    private static final float MAX_DARK_LUMA = 0.45f;
    private static final float MAX_MUTED_SATURATION = 0.4f;
    private static final float MAX_NORMAL_LUMA = 0.7f;
    private static final float MIN_LIGHT_LUMA = 0.55f;
    private static final float MIN_NORMAL_LUMA = 0.3f;
    private static final float MIN_VIBRANT_SATURATION = 0.35f;
    public static final Target MUTED;
    private static final float TARGET_DARK_LUMA = 0.26f;
    private static final float TARGET_LIGHT_LUMA = 0.74f;
    private static final float TARGET_MUTED_SATURATION = 0.3f;
    private static final float TARGET_NORMAL_LUMA = 0.5f;
    private static final float TARGET_VIBRANT_SATURATION = 1.0f;
    public static final Target VIBRANT;
    private static final float WEIGHT_LUMA = 0.52f;
    private static final float WEIGHT_POPULATION = 0.24f;
    private static final float WEIGHT_SATURATION = 0.24f;
    boolean mIsExclusive;
    final float[] mLightnessTargets;
    final float[] mSaturationTargets;
    final float[] mWeights;
    
    static {
        final Target target = new Target();
        setDefaultLightLightnessValues(LIGHT_VIBRANT = target);
        setDefaultVibrantSaturationValues(target);
        final Target target2 = new Target();
        setDefaultNormalLightnessValues(VIBRANT = target2);
        setDefaultVibrantSaturationValues(target2);
        final Target target3 = new Target();
        setDefaultDarkLightnessValues(DARK_VIBRANT = target3);
        setDefaultVibrantSaturationValues(target3);
        final Target target4 = new Target();
        setDefaultLightLightnessValues(LIGHT_MUTED = target4);
        setDefaultMutedSaturationValues(target4);
        final Target target5 = new Target();
        setDefaultNormalLightnessValues(MUTED = target5);
        setDefaultMutedSaturationValues(target5);
        final Target target6 = new Target();
        setDefaultDarkLightnessValues(DARK_MUTED = target6);
        setDefaultMutedSaturationValues(target6);
    }
    
    Target() {
        final float[] array = new float[3];
        this.mSaturationTargets = array;
        final float[] array2 = new float[3];
        this.mLightnessTargets = array2;
        this.mWeights = new float[3];
        this.mIsExclusive = true;
        setTargetDefaultValues(array);
        setTargetDefaultValues(array2);
        this.setDefaultWeights();
    }
    
    Target(final Target target) {
        final float[] mSaturationTargets = new float[3];
        this.mSaturationTargets = mSaturationTargets;
        final float[] mLightnessTargets = new float[3];
        this.mLightnessTargets = mLightnessTargets;
        final float[] mWeights = new float[3];
        this.mWeights = mWeights;
        this.mIsExclusive = true;
        System.arraycopy(target.mSaturationTargets, 0, mSaturationTargets, 0, mSaturationTargets.length);
        System.arraycopy(target.mLightnessTargets, 0, mLightnessTargets, 0, mLightnessTargets.length);
        System.arraycopy(target.mWeights, 0, mWeights, 0, mWeights.length);
    }
    
    private static void setDefaultDarkLightnessValues(final Target target) {
        final float[] mLightnessTargets = target.mLightnessTargets;
        mLightnessTargets[1] = 0.26f;
        mLightnessTargets[2] = 0.45f;
    }
    
    private static void setDefaultLightLightnessValues(final Target target) {
        final float[] mLightnessTargets = target.mLightnessTargets;
        mLightnessTargets[0] = 0.55f;
        mLightnessTargets[1] = 0.74f;
    }
    
    private static void setDefaultMutedSaturationValues(final Target target) {
        final float[] mSaturationTargets = target.mSaturationTargets;
        mSaturationTargets[1] = 0.3f;
        mSaturationTargets[2] = 0.4f;
    }
    
    private static void setDefaultNormalLightnessValues(final Target target) {
        final float[] mLightnessTargets = target.mLightnessTargets;
        mLightnessTargets[0] = 0.3f;
        mLightnessTargets[1] = 0.5f;
        mLightnessTargets[2] = 0.7f;
    }
    
    private static void setDefaultVibrantSaturationValues(final Target target) {
        final float[] mSaturationTargets = target.mSaturationTargets;
        mSaturationTargets[0] = 0.35f;
        mSaturationTargets[1] = 1.0f;
    }
    
    private void setDefaultWeights() {
        final float[] mWeights = this.mWeights;
        mWeights[0] = 0.24f;
        mWeights[1] = 0.52f;
        mWeights[2] = 0.24f;
    }
    
    private static void setTargetDefaultValues(final float[] array) {
        array[0] = 0.0f;
        array[1] = 0.5f;
        array[2] = 1.0f;
    }
    
    public float getLightnessWeight() {
        return this.mWeights[1];
    }
    
    public float getMaximumLightness() {
        return this.mLightnessTargets[2];
    }
    
    public float getMaximumSaturation() {
        return this.mSaturationTargets[2];
    }
    
    public float getMinimumLightness() {
        return this.mLightnessTargets[0];
    }
    
    public float getMinimumSaturation() {
        return this.mSaturationTargets[0];
    }
    
    public float getPopulationWeight() {
        return this.mWeights[2];
    }
    
    public float getSaturationWeight() {
        return this.mWeights[0];
    }
    
    public float getTargetLightness() {
        return this.mLightnessTargets[1];
    }
    
    public float getTargetSaturation() {
        return this.mSaturationTargets[1];
    }
    
    public boolean isExclusive() {
        return this.mIsExclusive;
    }
    
    void normalizeWeights() {
        final int length = this.mWeights.length;
        final int n = 0;
        int i = 0;
        float n2 = 0.0f;
        while (i < length) {
            final float n3 = this.mWeights[i];
            float n4 = n2;
            if (n3 > 0.0f) {
                n4 = n2 + n3;
            }
            ++i;
            n2 = n4;
        }
        if (n2 != 0.0f) {
            for (int length2 = this.mWeights.length, j = n; j < length2; ++j) {
                final float[] mWeights = this.mWeights;
                final float n5 = mWeights[j];
                if (n5 > 0.0f) {
                    mWeights[j] = n5 / n2;
                }
            }
        }
    }
    
    public static final class Builder
    {
        private final Target mTarget;
        
        public Builder() {
            this.mTarget = new Target();
        }
        
        public Builder(final Target target) {
            this.mTarget = new Target(target);
        }
        
        public Target build() {
            return this.mTarget;
        }
        
        public Builder setExclusive(final boolean mIsExclusive) {
            this.mTarget.mIsExclusive = mIsExclusive;
            return this;
        }
        
        public Builder setLightnessWeight(final float n) {
            this.mTarget.mWeights[1] = n;
            return this;
        }
        
        public Builder setMaximumLightness(final float n) {
            this.mTarget.mLightnessTargets[2] = n;
            return this;
        }
        
        public Builder setMaximumSaturation(final float n) {
            this.mTarget.mSaturationTargets[2] = n;
            return this;
        }
        
        public Builder setMinimumLightness(final float n) {
            this.mTarget.mLightnessTargets[0] = n;
            return this;
        }
        
        public Builder setMinimumSaturation(final float n) {
            this.mTarget.mSaturationTargets[0] = n;
            return this;
        }
        
        public Builder setPopulationWeight(final float n) {
            this.mTarget.mWeights[2] = n;
            return this;
        }
        
        public Builder setSaturationWeight(final float n) {
            this.mTarget.mWeights[0] = n;
            return this;
        }
        
        public Builder setTargetLightness(final float n) {
            this.mTarget.mLightnessTargets[1] = n;
            return this;
        }
        
        public Builder setTargetSaturation(final float n) {
            this.mTarget.mSaturationTargets[1] = n;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.vectordrawable.graphics.drawable;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.Animatable2$AnimationCallback;
import android.graphics.drawable.Animatable;

public interface Animatable2Compat extends Animatable
{
    void clearAnimationCallbacks();
    
    void registerAnimationCallback(final AnimationCallback p0);
    
    boolean unregisterAnimationCallback(final AnimationCallback p0);
    
    public abstract static class AnimationCallback
    {
        Animatable2$AnimationCallback mPlatformCallback;
        
        Animatable2$AnimationCallback getPlatformCallback() {
            if (this.mPlatformCallback == null) {
                this.mPlatformCallback = new Animatable2$AnimationCallback(this) {
                    final AnimationCallback this$0;
                    
                    public void onAnimationEnd(final Drawable drawable) {
                        this.this$0.onAnimationEnd(drawable);
                    }
                    
                    public void onAnimationStart(final Drawable drawable) {
                        this.this$0.onAnimationStart(drawable);
                    }
                };
            }
            return this.mPlatformCallback;
        }
        
        public void onAnimationEnd(final Drawable drawable) {
        }
        
        public void onAnimationStart(final Drawable drawable) {
        }
    }
}

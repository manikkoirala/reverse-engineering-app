// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Result$Companion;
import kotlin.Result;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import java.util.ArrayList;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0011\u0010\r\u001a\u00020\bH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eJ\u0006\u0010\u000f\u001a\u00020\bJ\u0006\u0010\u0010\u001a\u00020\bJ%\u0010\u0011\u001a\u0002H\u0012\"\u0004\b\u0000\u0010\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0014H\u0086\b\u00f8\u0001\u0001¢\u0006\u0002\u0010\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u009920\u0001¨\u0006\u0016" }, d2 = { "Landroidx/compose/runtime/Latch;", "", "()V", "_isOpen", "", "awaiters", "", "Lkotlin/coroutines/Continuation;", "", "isOpen", "()Z", "lock", "spareList", "await", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "closeLatch", "openLatch", "withClosed", "R", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Latch
{
    private boolean _isOpen;
    private List<Continuation<Unit>> awaiters;
    private final Object lock;
    private List<Continuation<Unit>> spareList;
    
    public Latch() {
        this.lock = new Object();
        this.awaiters = new ArrayList<Continuation<Unit>>();
        this.spareList = new ArrayList<Continuation<Unit>>();
        this._isOpen = true;
    }
    
    public static final /* synthetic */ List access$getAwaiters$p(final Latch latch) {
        return latch.awaiters;
    }
    
    public static final /* synthetic */ Object access$getLock$p(final Latch latch) {
        return latch.lock;
    }
    
    public final Object await(final Continuation<? super Unit> continuation) {
        if (this.isOpen()) {
            return Unit.INSTANCE;
        }
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        Object o = access$getLock$p(this);
        synchronized (o) {
            access$getAwaiters$p(this).add(cancellableContinuation);
            monitorexit(o);
            cancellableContinuation.invokeOnCancellation((Function1)new Latch$await$2.Latch$await$2$2(this, cancellableContinuation));
            o = cancellableContinuationImpl.getResult();
            if (o == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
            }
            if (o == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                return o;
            }
            return Unit.INSTANCE;
        }
    }
    
    public final void closeLatch() {
        synchronized (this.lock) {
            this._isOpen = false;
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final boolean isOpen() {
        synchronized (this.lock) {
            return this._isOpen;
        }
    }
    
    public final void openLatch() {
        synchronized (this.lock) {
            if (this.isOpen()) {
                return;
            }
            final List<Continuation<Unit>> awaiters = this.awaiters;
            this.awaiters = this.spareList;
            this.spareList = awaiters;
            this._isOpen = true;
            for (int i = 0; i < awaiters.size(); ++i) {
                final Continuation continuation = awaiters.get(i);
                final Result$Companion companion = Result.Companion;
                continuation.resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
            }
            awaiters.clear();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final <R> R withClosed(final Function0<? extends R> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        this.closeLatch();
        try {
            return (R)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            this.openLatch();
            InlineMarker.finallyEnd(1);
        }
    }
}

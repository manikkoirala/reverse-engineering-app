// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Unit;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\nJ:\u0010\f\u001a\u0002H\r\"\u0004\b\u0000\u0010\r2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u0002H\r0\u000fH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u00058F¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0015" }, d2 = { "Landroidx/compose/runtime/PausableMonotonicFrameClock;", "Landroidx/compose/runtime/MonotonicFrameClock;", "frameClock", "(Landroidx/compose/runtime/MonotonicFrameClock;)V", "isPaused", "", "()Z", "latch", "Landroidx/compose/runtime/Latch;", "pause", "", "resume", "withFrameNanos", "R", "onFrame", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "frameTimeNanos", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PausableMonotonicFrameClock implements MonotonicFrameClock
{
    public static final int $stable = 8;
    private final MonotonicFrameClock frameClock;
    private final Latch latch;
    
    public PausableMonotonicFrameClock(final MonotonicFrameClock frameClock) {
        Intrinsics.checkNotNullParameter((Object)frameClock, "frameClock");
        this.frameClock = frameClock;
        this.latch = new Latch();
    }
    
    public <R> R fold(final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
        return DefaultImpls.fold(this, r, function2);
    }
    
    public <E extends CoroutineContext$Element> E get(final CoroutineContext$Key<E> coroutineContext$Key) {
        return DefaultImpls.get(this, coroutineContext$Key);
    }
    
    public final boolean isPaused() {
        return this.latch.isOpen() ^ true;
    }
    
    public CoroutineContext minusKey(final CoroutineContext$Key<?> coroutineContext$Key) {
        return DefaultImpls.minusKey(this, coroutineContext$Key);
    }
    
    public final void pause() {
        this.latch.closeLatch();
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return DefaultImpls.plus(this, coroutineContext);
    }
    
    public final void resume() {
        this.latch.openLatch();
    }
    
    @Override
    public <R> Object withFrameNanos(Function1<? super Long, ? extends R> l$1, final Continuation<? super R> continuation) {
        Object o = null;
        Label_0053: {
            if (continuation instanceof PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1) {
                final PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1 pausableMonotonicFrameClock$withFrameNanos$1 = (PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)continuation;
                if ((pausableMonotonicFrameClock$withFrameNanos$1.label & Integer.MIN_VALUE) != 0x0) {
                    pausableMonotonicFrameClock$withFrameNanos$1.label += Integer.MIN_VALUE;
                    o = pausableMonotonicFrameClock$withFrameNanos$1;
                    break Label_0053;
                }
            }
            o = new PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1(this, (Continuation)continuation);
        }
        final Object result = ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).label;
        PausableMonotonicFrameClock pausableMonotonicFrameClock2;
        if (label != 0) {
            if (label != 1) {
                if (label == 2) {
                    ResultKt.throwOnFailure(result);
                    return result;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            else {
                l$1 = (Function1)((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$1;
                final PausableMonotonicFrameClock pausableMonotonicFrameClock = (PausableMonotonicFrameClock)((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$0;
                ResultKt.throwOnFailure(result);
                pausableMonotonicFrameClock2 = pausableMonotonicFrameClock;
            }
        }
        else {
            ResultKt.throwOnFailure(result);
            final Latch latch = this.latch;
            ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$0 = this;
            ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$1 = l$1;
            ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).label = 1;
            if (latch.await((Continuation<? super Unit>)o) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
            pausableMonotonicFrameClock2 = this;
        }
        final MonotonicFrameClock frameClock = pausableMonotonicFrameClock2.frameClock;
        ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$0 = null;
        ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).L$1 = null;
        ((PausableMonotonicFrameClock$withFrameNanos.PausableMonotonicFrameClock$withFrameNanos$1)o).label = 2;
        Object withFrameNanos;
        if ((withFrameNanos = frameClock.withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)l$1, (kotlin.coroutines.Continuation<? super Object>)o)) == coroutine_SUSPENDED) {
            return coroutine_SUSPENDED;
        }
        return withFrameNanos;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import kotlin.jvm.internal.TypeIntrinsics;
import java.util.Map;
import java.util.ConcurrentModificationException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableIterator;
import java.util.Iterator;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010)\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\b\u0012\u0004\u0012\u0002H\u00030\u00042\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005B9\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007\u0012\u001e\u0010\b\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\n0\t¢\u0006\u0002\u0010\u000bJ\b\u0010\u0012\u001a\u00020\u0013H\u0002J\b\u0010\u0014\u001a\u00020\u0013H\u0002J\u000e\u0010\u0015\u001a\u00028\u0002H\u0096\u0002¢\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u00020\u0013H\u0016J5\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\r2\u000e\u0010\u001a\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u001b2\u0006\u0010\u001c\u001a\u00028\u00002\u0006\u0010\u001d\u001a\u00020\rH\u0002¢\u0006\u0002\u0010\u001eJ\u001b\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u001c\u001a\u00028\u00002\u0006\u0010 \u001a\u00028\u0001¢\u0006\u0002\u0010!R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00018\u0000X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\"" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilderBaseIterator;", "K", "V", "T", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBaseIterator;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "path", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;[Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;)V", "expectedModCount", "", "lastIteratedKey", "Ljava/lang/Object;", "nextWasInvoked", "", "checkForComodification", "", "checkNextWasInvoked", "next", "()Ljava/lang/Object;", "remove", "resetPath", "keyHash", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "key", "pathIndex", "(ILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;Ljava/lang/Object;I)V", "setValue", "newValue", "(Ljava/lang/Object;Ljava/lang/Object;)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class PersistentHashMapBuilderBaseIterator<K, V, T> extends PersistentHashMapBaseIterator<K, V, T> implements Iterator<T>, KMutableIterator
{
    private final PersistentHashMapBuilder<K, V> builder;
    private int expectedModCount;
    private K lastIteratedKey;
    private boolean nextWasInvoked;
    
    public PersistentHashMapBuilderBaseIterator(final PersistentHashMapBuilder<K, V> builder, final TrieNodeBaseIterator<K, V, T>[] array) {
        Intrinsics.checkNotNullParameter((Object)builder, "builder");
        Intrinsics.checkNotNullParameter((Object)array, "path");
        super(builder.getNode$runtime_release(), array);
        this.builder = builder;
        this.expectedModCount = builder.getModCount$runtime_release();
    }
    
    private final void checkForComodification() {
        if (this.builder.getModCount$runtime_release() == this.expectedModCount) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    private final void checkNextWasInvoked() {
        if (this.nextWasInvoked) {
            return;
        }
        throw new IllegalStateException();
    }
    
    private final void resetPath(int entryKeyIndex$runtime_release, final TrieNode<?, ?> trieNode, final K k, final int n) {
        final int n2 = n * 5;
        if (n2 > 30) {
            this.getPath()[n].reset(trieNode.getBuffer$runtime_release(), trieNode.getBuffer$runtime_release().length, 0);
            while (!Intrinsics.areEqual((Object)this.getPath()[n].currentKey(), (Object)k)) {
                this.getPath()[n].moveToNextKey();
            }
            this.setPathLastIndex(n);
            return;
        }
        final int n3 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n2);
        if (trieNode.hasEntryAt$runtime_release(n3)) {
            entryKeyIndex$runtime_release = trieNode.entryKeyIndex$runtime_release(n3);
            this.getPath()[n].reset(trieNode.getBuffer$runtime_release(), trieNode.entryCount$runtime_release() * 2, entryKeyIndex$runtime_release);
            this.setPathLastIndex(n);
            return;
        }
        final int nodeIndex$runtime_release = trieNode.nodeIndex$runtime_release(n3);
        final TrieNode nodeAtIndex$runtime_release = trieNode.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
        this.getPath()[n].reset(trieNode.getBuffer$runtime_release(), trieNode.entryCount$runtime_release() * 2, nodeIndex$runtime_release);
        this.resetPath(entryKeyIndex$runtime_release, nodeAtIndex$runtime_release, k, n + 1);
    }
    
    @Override
    public T next() {
        this.checkForComodification();
        this.lastIteratedKey = this.currentKey();
        this.nextWasInvoked = true;
        return super.next();
    }
    
    @Override
    public void remove() {
        this.checkNextWasInvoked();
        if (this.hasNext()) {
            final K currentKey = this.currentKey();
            TypeIntrinsics.asMutableMap((Object)this.builder).remove(this.lastIteratedKey);
            int hashCode;
            if (currentKey != null) {
                hashCode = currentKey.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.resetPath(hashCode, this.builder.getNode$runtime_release(), currentKey, 0);
        }
        else {
            TypeIntrinsics.asMutableMap((Object)this.builder).remove(this.lastIteratedKey);
        }
        this.lastIteratedKey = null;
        this.nextWasInvoked = false;
        this.expectedModCount = this.builder.getModCount$runtime_release();
    }
    
    public final void setValue(final K k, final V v) {
        if (!this.builder.containsKey(k)) {
            return;
        }
        if (this.hasNext()) {
            final K currentKey = this.currentKey();
            this.builder.put(k, v);
            int hashCode;
            if (currentKey != null) {
                hashCode = currentKey.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.resetPath(hashCode, this.builder.getNode$runtime_release(), currentKey, 0);
        }
        else {
            this.builder.put(k, v);
        }
        this.expectedModCount = this.builder.getModCount$runtime_release();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.internal;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\b\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00d6\u0001J\u0011\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0003H\u0086\u0002J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\u0013" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/DeltaCounter;", "", "count", "", "(I)V", "getCount", "()I", "setCount", "component1", "copy", "equals", "", "other", "hashCode", "plusAssign", "", "that", "toString", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class DeltaCounter
{
    private int count;
    
    public DeltaCounter() {
        this(0, 1, null);
    }
    
    public DeltaCounter(final int count) {
        this.count = count;
    }
    
    public final int component1() {
        return this.count;
    }
    
    public final DeltaCounter copy(final int n) {
        return new DeltaCounter(n);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof DeltaCounter && this.count == ((DeltaCounter)o).count);
    }
    
    public final int getCount() {
        return this.count;
    }
    
    @Override
    public int hashCode() {
        return this.count;
    }
    
    public final void plusAssign(final int n) {
        this.count += n;
    }
    
    public final void setCount(final int count) {
        this.count = count;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DeltaCounter(count=");
        sb.append(this.count);
        sb.append(')');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.EndOfChain;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.collections.AbstractMutableMap;

@Metadata(d1 = { "\u0000^\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0010'\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u001f\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004B\u0019\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006¢\u0006\u0002\u0010\u0007J\u0014\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010#H\u0016J\b\u0010$\u001a\u00020%H\u0016J\u0015\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010)J\u0018\u0010*\u001a\u0004\u0018\u00018\u00012\u0006\u0010(\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010+J\u001f\u0010,\u001a\u0004\u0018\u00018\u00012\u0006\u0010(\u001a\u00028\u00002\u0006\u0010-\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010.J\u0017\u0010/\u001a\u0004\u0018\u00018\u00012\u0006\u0010(\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010+J\u001b\u0010/\u001a\u00020'2\u0006\u0010(\u001a\u00028\u00002\u0006\u0010-\u001a\u00028\u0001¢\u0006\u0002\u00100R&\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\"\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\u000e@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R&\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\u00140\u0013X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\fR\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u00020\u001b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00010\u001f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b \u0010!¨\u00061" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMapBuilder;", "K", "V", "Lkotlin/collections/AbstractMutableMap;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap$Builder;", "map", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;)V", "entries", "", "", "getEntries", "()Ljava/util/Set;", "<set-?>", "", "firstKey", "getFirstKey$runtime_release", "()Ljava/lang/Object;", "hashMapBuilder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/LinkedValue;", "getHashMapBuilder$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "keys", "getKeys", "lastKey", "size", "", "getSize", "()I", "values", "", "getValues", "()Ljava/util/Collection;", "build", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "clear", "", "containsKey", "", "key", "(Ljava/lang/Object;)Z", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "put", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "remove", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentOrderedMapBuilder<K, V> extends AbstractMutableMap<K, V> implements Builder<K, V>
{
    private Object firstKey;
    private final PersistentHashMapBuilder<K, LinkedValue<V>> hashMapBuilder;
    private Object lastKey;
    private PersistentOrderedMap<K, V> map;
    
    public PersistentOrderedMapBuilder(final PersistentOrderedMap<K, V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "map");
        this.map = map;
        this.firstKey = map.getFirstKey$runtime_release();
        this.lastKey = this.map.getLastKey$runtime_release();
        this.hashMapBuilder = this.map.getHashMap$runtime_release().builder();
    }
    
    public PersistentMap<K, V> build() {
        final PersistentHashMap<K, LinkedValue<V>> build = this.hashMapBuilder.build();
        PersistentOrderedMap<K, V> map;
        if (build == this.map.getHashMap$runtime_release()) {
            final Object firstKey = this.firstKey;
            final Object firstKey$runtime_release = this.map.getFirstKey$runtime_release();
            final boolean b = true;
            CommonFunctionsKt.assert(firstKey == firstKey$runtime_release);
            CommonFunctionsKt.assert(this.lastKey == this.map.getLastKey$runtime_release() && b);
            map = this.map;
        }
        else {
            map = new PersistentOrderedMap<K, V>(this.firstKey, this.lastKey, build);
        }
        this.map = map;
        return map;
    }
    
    public void clear() {
        this.hashMapBuilder.clear();
        this.firstKey = EndOfChain.INSTANCE;
        this.lastKey = EndOfChain.INSTANCE;
    }
    
    public boolean containsKey(final Object o) {
        return this.hashMapBuilder.containsKey(o);
    }
    
    public V get(Object value) {
        final LinkedValue linkedValue = this.hashMapBuilder.get(value);
        if (linkedValue != null) {
            value = linkedValue.getValue();
        }
        else {
            value = null;
        }
        return (V)value;
    }
    
    public Set<Entry<K, V>> getEntries() {
        return (Set<Entry<K, V>>)new PersistentOrderedMapBuilderEntries((PersistentOrderedMapBuilder<Object, Object>)this);
    }
    
    public final Object getFirstKey$runtime_release() {
        return this.firstKey;
    }
    
    public final PersistentHashMapBuilder<K, LinkedValue<V>> getHashMapBuilder$runtime_release() {
        return this.hashMapBuilder;
    }
    
    public Set<K> getKeys() {
        return new PersistentOrderedMapBuilderKeys<K, Object>(this);
    }
    
    public int getSize() {
        return this.hashMapBuilder.size();
    }
    
    public Collection<V> getValues() {
        return new PersistentOrderedMapBuilderValues<Object, V>(this);
    }
    
    public V put(final K lastKey, final V v) {
        final LinkedValue linkedValue = this.hashMapBuilder.get(lastKey);
        if (linkedValue != null) {
            if (linkedValue.getValue() == v) {
                return v;
            }
            this.hashMapBuilder.put(lastKey, linkedValue.withValue(v));
            return (V)linkedValue.getValue();
        }
        else {
            if (this.isEmpty()) {
                this.firstKey = lastKey;
                this.lastKey = lastKey;
                this.hashMapBuilder.put(lastKey, new LinkedValue(v));
                return null;
            }
            final Object lastKey2 = this.lastKey;
            final LinkedValue<V> value = this.hashMapBuilder.get(lastKey2);
            Intrinsics.checkNotNull((Object)value);
            final LinkedValue<V> linkedValue2 = value;
            CommonFunctionsKt.assert(linkedValue2.getHasNext() ^ true);
            this.hashMapBuilder.put(lastKey2, linkedValue2.withNext(lastKey));
            this.hashMapBuilder.put(lastKey, new LinkedValue(v, lastKey2));
            this.lastKey = lastKey;
            return null;
        }
    }
    
    public V remove(final Object o) {
        final LinkedValue linkedValue = this.hashMapBuilder.remove(o);
        if (linkedValue == null) {
            return null;
        }
        if (linkedValue.getHasPrevious()) {
            final Object value = this.hashMapBuilder.get(linkedValue.getPrevious());
            Intrinsics.checkNotNull(value);
            this.hashMapBuilder.put(linkedValue.getPrevious(), ((LinkedValue)value).withNext(linkedValue.getNext()));
        }
        else {
            this.firstKey = linkedValue.getNext();
        }
        if (linkedValue.getHasNext()) {
            final Object value2 = this.hashMapBuilder.get(linkedValue.getNext());
            Intrinsics.checkNotNull(value2);
            this.hashMapBuilder.put(linkedValue.getNext(), ((LinkedValue)value2).withPrevious(linkedValue.getPrevious()));
        }
        else {
            this.lastKey = linkedValue.getPrevious();
        }
        return (V)linkedValue.getValue();
    }
    
    public final boolean remove(final Object o, final Object o2) {
        final LinkedValue linkedValue = this.hashMapBuilder.get(o);
        boolean b = false;
        if (linkedValue == null) {
            return false;
        }
        if (Intrinsics.areEqual(linkedValue.getValue(), o2)) {
            this.remove(o);
            b = true;
        }
        return b;
    }
}

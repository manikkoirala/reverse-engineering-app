// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import java.util.NoSuchElementException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\b\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B-\u0012\u000e\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007¢\u0006\u0002\u0010\nJ\r\u0010\u000f\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\u0010J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0007H\u0002J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0007H\u0002J\u000e\u0010\u0016\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0010J\r\u0010\u0017\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010J7\u0010\u0018\u001a\u00020\u00122\u000e\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0000¢\u0006\u0004\b\u0019\u0010\nR\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0018\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u000e¨\u0006\u001a" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/TrieIterator;", "E", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractListIterator;", "root", "", "", "index", "", "size", "height", "([Ljava/lang/Object;III)V", "isInRightEdge", "", "path", "[Ljava/lang/Object;", "elementAtCurrentIndex", "()Ljava/lang/Object;", "fillPath", "", "startLevel", "fillPathIfNeeded", "indexPredicate", "next", "previous", "reset", "reset$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TrieIterator<E> extends AbstractListIterator<E>
{
    private int height;
    private boolean isInRightEdge;
    private Object[] path;
    
    public TrieIterator(final Object[] array, final int n, int isInRightEdge, final int height) {
        Intrinsics.checkNotNullParameter((Object)array, "root");
        super(n, isInRightEdge);
        this.height = height;
        final Object[] path = new Object[height];
        this.path = path;
        if (n == isInRightEdge) {
            isInRightEdge = 1;
        }
        else {
            isInRightEdge = 0;
        }
        this.isInRightEdge = (isInRightEdge != 0);
        path[0] = array;
        this.fillPath(n - isInRightEdge, 1);
    }
    
    private final E elementAtCurrentIndex() {
        final int index = this.getIndex();
        final Object o = this.path[this.height - 1];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList.TrieIterator>");
        return (E)((Object[])o)[index & 0x1F];
    }
    
    private final void fillPath(final int n, int i) {
        int n2 = (this.height - i) * 5;
        while (i < this.height) {
            final Object[] path = this.path;
            final Object o = path[i - 1];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            path[i] = ((Object[])o)[UtilsKt.indexSegment(n, n2)];
            n2 -= 5;
            ++i;
        }
    }
    
    private final void fillPathIfNeeded(int height) {
        int n;
        for (n = 0; UtilsKt.indexSegment(this.getIndex(), n) == height; n += 5) {}
        if (n > 0) {
            height = this.height;
            this.fillPath(this.getIndex(), height - 1 - n / 5 + 1);
        }
    }
    
    @Override
    public E next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        final E elementAtCurrentIndex = this.elementAtCurrentIndex();
        this.setIndex(this.getIndex() + 1);
        if (this.getIndex() == this.getSize()) {
            this.isInRightEdge = true;
            return elementAtCurrentIndex;
        }
        this.fillPathIfNeeded(0);
        return elementAtCurrentIndex;
    }
    
    @Override
    public E previous() {
        if (!this.hasPrevious()) {
            throw new NoSuchElementException();
        }
        this.setIndex(this.getIndex() - 1);
        if (this.isInRightEdge) {
            this.isInRightEdge = false;
            return this.elementAtCurrentIndex();
        }
        this.fillPathIfNeeded(31);
        return this.elementAtCurrentIndex();
    }
    
    public final void reset$runtime_release(final Object[] array, final int index, final int size, int n) {
        Intrinsics.checkNotNullParameter((Object)array, "root");
        this.setIndex(index);
        this.setSize(size);
        this.height = n;
        if (this.path.length < n) {
            this.path = new Object[n];
        }
        final Object[] path = this.path;
        n = 0;
        path[0] = array;
        if (index == size) {
            n = 1;
        }
        this.fillPath(index - ((this.isInRightEdge = (n != 0)) ? 1 : 0), 1);
    }
}

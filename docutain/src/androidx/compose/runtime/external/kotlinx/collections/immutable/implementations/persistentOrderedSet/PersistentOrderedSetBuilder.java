// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedSet;

import java.util.Iterator;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.EndOfChain;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentSet;
import kotlin.collections.AbstractMutableSet;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010)\n\u0002\b\u0002\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001aJ\u000e\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\u0016\u0010\u001f\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u001aJ\u000f\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000!H\u0096\u0002J\u0015\u0010\"\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001aR\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR \u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000eX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00148VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016¨\u0006#" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSetBuilder;", "E", "Lkotlin/collections/AbstractMutableSet;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet$Builder;", "set", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSet;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSet;)V", "firstElement", "", "getFirstElement$runtime_release", "()Ljava/lang/Object;", "setFirstElement$runtime_release", "(Ljava/lang/Object;)V", "hashMapBuilder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/Links;", "getHashMapBuilder$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "lastElement", "size", "", "getSize", "()I", "add", "", "element", "(Ljava/lang/Object;)Z", "build", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "clear", "", "contains", "iterator", "", "remove", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentOrderedSetBuilder<E> extends AbstractMutableSet<E> implements Builder<E>
{
    private Object firstElement;
    private final PersistentHashMapBuilder<E, Links> hashMapBuilder;
    private Object lastElement;
    private PersistentOrderedSet<E> set;
    
    public PersistentOrderedSetBuilder(final PersistentOrderedSet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "set");
        this.set = set;
        this.firstElement = set.getFirstElement$runtime_release();
        this.lastElement = this.set.getLastElement$runtime_release();
        this.hashMapBuilder = this.set.getHashMap$runtime_release().builder();
    }
    
    public boolean add(final E lastElement) {
        if (this.hashMapBuilder.containsKey(lastElement)) {
            return false;
        }
        if (this.isEmpty()) {
            this.firstElement = lastElement;
            this.lastElement = lastElement;
            this.hashMapBuilder.put(lastElement, new Links());
            return true;
        }
        final Links value = this.hashMapBuilder.get(this.lastElement);
        Intrinsics.checkNotNull((Object)value);
        this.hashMapBuilder.put(this.lastElement, value.withNext(lastElement));
        this.hashMapBuilder.put(lastElement, new Links(this.lastElement));
        this.lastElement = lastElement;
        return true;
    }
    
    public PersistentSet<E> build() {
        final PersistentHashMap<E, Links> build = this.hashMapBuilder.build();
        PersistentOrderedSet<E> set;
        if (build == this.set.getHashMap$runtime_release()) {
            final Object firstElement = this.firstElement;
            final Object firstElement$runtime_release = this.set.getFirstElement$runtime_release();
            final boolean b = true;
            CommonFunctionsKt.assert(firstElement == firstElement$runtime_release);
            CommonFunctionsKt.assert(this.lastElement == this.set.getLastElement$runtime_release() && b);
            set = this.set;
        }
        else {
            set = new PersistentOrderedSet<E>(this.firstElement, this.lastElement, build);
        }
        this.set = set;
        return set;
    }
    
    public void clear() {
        this.hashMapBuilder.clear();
        this.firstElement = EndOfChain.INSTANCE;
        this.lastElement = EndOfChain.INSTANCE;
    }
    
    public boolean contains(final Object o) {
        return this.hashMapBuilder.containsKey(o);
    }
    
    public final Object getFirstElement$runtime_release() {
        return this.firstElement;
    }
    
    public final PersistentHashMapBuilder<E, Links> getHashMapBuilder$runtime_release() {
        return this.hashMapBuilder;
    }
    
    public int getSize() {
        return this.hashMapBuilder.size();
    }
    
    public Iterator<E> iterator() {
        return new PersistentOrderedSetMutableIterator<E>(this);
    }
    
    public boolean remove(final Object o) {
        final Links links = this.hashMapBuilder.remove(o);
        if (links == null) {
            return false;
        }
        if (links.getHasPrevious()) {
            final Object value = this.hashMapBuilder.get(links.getPrevious());
            Intrinsics.checkNotNull(value);
            this.hashMapBuilder.put(links.getPrevious(), ((Links)value).withNext(links.getNext()));
        }
        else {
            this.firstElement = links.getNext();
        }
        if (links.getHasNext()) {
            final Object value2 = this.hashMapBuilder.get(links.getNext());
            Intrinsics.checkNotNull(value2);
            this.hashMapBuilder.put(links.getNext(), ((Links)value2).withPrevious(links.getPrevious()));
        }
        else {
            this.lastElement = links.getPrevious();
        }
        return true;
    }
    
    public final void setFirstElement$runtime_release(final Object firstElement) {
        this.firstElement = firstElement;
    }
}

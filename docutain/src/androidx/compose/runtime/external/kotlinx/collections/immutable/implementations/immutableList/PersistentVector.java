// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import kotlin.jvm.functions.Function1;
import java.util.ListIterator;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.ListImplementation;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import kotlin.collections.ArraysKt;
import java.util.Arrays;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import kotlin.ranges.RangesKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentList;

@Metadata(d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010*\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\t\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B5\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005\u0012\u000e\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0002\u0010\u000bJ\u001b\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0010\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0011J#\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0013J\u001d\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\u0012\u001a\u00020\tH\u0002¢\u0006\u0002\u0010\u0015J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u0017H\u0016J\u0016\u0010\u0018\u001a\u00028\u00002\u0006\u0010\u0012\u001a\u00020\tH\u0096\u0002¢\u0006\u0002\u0010\u0019JG\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\t2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u001c\u001a\u00020\u001dH\u0002¢\u0006\u0002\u0010\u001eJ5\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010 \u001a\u00020\t2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0002\u0010!J\u0016\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#2\u0006\u0010\u0012\u001a\u00020\tH\u0016J?\u0010$\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0006\u0018\u00010\u00052\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\t2\u0006\u0010%\u001a\u00020\u001dH\u0002¢\u0006\u0002\u0010&J3\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010(\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\tH\u0002¢\u0006\u0002\u0010)JC\u0010*\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010+\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010,\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005H\u0002¢\u0006\u0002\u0010-J?\u0010.\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0010\u0010\u0004\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0006\u0018\u00010\u00052\u0006\u0010\u001b\u001a\u00020\t2\u000e\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005H\u0002¢\u0006\u0002\u0010/J\"\u00100\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020302H\u0016J\u0016\u00104\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0012\u001a\u00020\tH\u0016J=\u00105\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\t2\u0006\u0010%\u001a\u00020\u001dH\u0002¢\u0006\u0002\u0010&J;\u00106\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010(\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\tH\u0002¢\u0006\u0002\u00107J\b\u0010(\u001a\u00020\tH\u0002J#\u00108\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0013J?\u00109\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\t2\b\u0010:\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0002\u0010;R\u0018\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\fR\u000e\u0010\n\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u00020\tX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0018\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\f¨\u0006<" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVector;", "E", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractPersistentList;", "root", "", "", "tail", "size", "", "rootShift", "([Ljava/lang/Object;[Ljava/lang/Object;II)V", "[Ljava/lang/Object;", "getSize", "()I", "add", "element", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "index", "(ILjava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "bufferFor", "(I)[Ljava/lang/Object;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorBuilder;", "get", "(I)Ljava/lang/Object;", "insertIntoRoot", "shift", "elementCarry", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;", "([Ljava/lang/Object;IILjava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;)[Ljava/lang/Object;", "insertIntoTail", "tailIndex", "([Ljava/lang/Object;ILjava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVector;", "listIterator", "", "pullLastBuffer", "tailCarry", "([Ljava/lang/Object;IILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;)[Ljava/lang/Object;", "pullLastBufferFromRoot", "rootSize", "([Ljava/lang/Object;II)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "pushFilledTail", "filledTail", "newTail", "([Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVector;", "pushTail", "([Ljava/lang/Object;I[Ljava/lang/Object;)[Ljava/lang/Object;", "removeAll", "predicate", "Lkotlin/Function1;", "", "removeAt", "removeFromRootAt", "removeFromTailAt", "([Ljava/lang/Object;III)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "set", "setInRoot", "e", "([Ljava/lang/Object;IILjava/lang/Object;)[Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentVector<E> extends AbstractPersistentList<E> implements PersistentList<E>
{
    private final Object[] root;
    private final int rootShift;
    private final int size;
    private final Object[] tail;
    
    public PersistentVector(final Object[] root, final Object[] tail, int size, final int rootShift) {
        Intrinsics.checkNotNullParameter((Object)root, "root");
        Intrinsics.checkNotNullParameter((Object)tail, "tail");
        this.root = root;
        this.tail = tail;
        this.size = size;
        this.rootShift = rootShift;
        size = this.size();
        boolean b = true;
        if (size > 32) {
            size = 1;
        }
        else {
            size = 0;
        }
        if (size != 0) {
            if (this.size() - UtilsKt.rootSize(this.size()) > RangesKt.coerceAtMost(tail.length, 32)) {
                b = false;
            }
            CommonFunctionsKt.assert(b);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Trie-based persistent vector should have at least 33 elements, got ");
        sb.append(this.size());
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private final Object[] bufferFor(final int n) {
        if (this.rootSize() <= n) {
            return this.tail;
        }
        Object[] root = this.root;
        for (int i = this.rootShift; i > 0; i -= 5) {
            final Object o = root[UtilsKt.indexSegment(n, i)];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            root = (Object[])o;
        }
        return root;
    }
    
    private final Object[] insertIntoRoot(final Object[] array, int n, final int n2, final Object o, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        if (n == 0) {
            Object[] copy;
            if (indexSegment == 0) {
                copy = new Object[32];
            }
            else {
                copy = Arrays.copyOf(array, 32);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            }
            ArraysKt.copyInto(array, copy, indexSegment + 1, indexSegment, 31);
            objectRef.setValue(array[31]);
            copy[indexSegment] = o;
            return copy;
        }
        final Object[] copy2 = Arrays.copyOf(array, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
        final int n3 = n - 5;
        final Object o2 = array[indexSegment];
        final String s = "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>";
        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        copy2[indexSegment] = this.insertIntoRoot((Object[])o2, n3, n2, o, objectRef);
        Object[] array2;
        Object o3;
        for (n = indexSegment + 1, array2 = copy2; n < 32 && array2[n] != null; ++n) {
            o3 = array[n];
            Intrinsics.checkNotNull(o3, s);
            array2[n] = this.insertIntoRoot((Object[])o3, n3, 0, objectRef.getValue(), objectRef);
        }
        return array2;
    }
    
    private final PersistentVector<E> insertIntoTail(final Object[] array, final int n, final Object o) {
        final int n2 = this.size() - this.rootSize();
        final Object[] copy = Arrays.copyOf(this.tail, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        if (n2 < 32) {
            ArraysKt.copyInto(this.tail, copy, n + 1, n, n2);
            copy[n] = o;
            return new PersistentVector<E>(array, copy, this.size() + 1, this.rootShift);
        }
        final Object[] tail = this.tail;
        final Object o2 = tail[31];
        ArraysKt.copyInto(tail, copy, n + 1, n, n2 - 1);
        copy[n] = o;
        return this.pushFilledTail(array, copy, UtilsKt.presizedBufferWith(o2));
    }
    
    private final Object[] pullLastBuffer(Object[] copy, final int n, final int n2, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        Object pullLastBuffer;
        if (n == 5) {
            objectRef.setValue(copy[indexSegment]);
            pullLastBuffer = null;
        }
        else {
            final Object o = copy[indexSegment];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            pullLastBuffer = this.pullLastBuffer((Object[])o, n - 5, n2, objectRef);
        }
        if (pullLastBuffer == null && indexSegment == 0) {
            return null;
        }
        copy = Arrays.copyOf(copy, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        copy[indexSegment] = pullLastBuffer;
        return copy;
    }
    
    private final PersistentList<E> pullLastBufferFromRoot(Object[] pullLastBuffer, final int n, final int n2) {
        if (n2 == 0) {
            Object[] copy = pullLastBuffer;
            if (pullLastBuffer.length == 33) {
                copy = Arrays.copyOf(pullLastBuffer, 32);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            }
            return new SmallPersistentVector<E>(copy);
        }
        final ObjectRef objectRef = new ObjectRef(null);
        pullLastBuffer = this.pullLastBuffer(pullLastBuffer, n2, n - 1, objectRef);
        Intrinsics.checkNotNull((Object)pullLastBuffer);
        final Object value = objectRef.getValue();
        Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        final Object[] array = (Object[])value;
        if (pullLastBuffer[1] == null) {
            final Object o = pullLastBuffer[0];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            return new PersistentVector((Object[])o, array, n, n2 - 5);
        }
        return new PersistentVector(pullLastBuffer, array, n, n2);
    }
    
    private final PersistentVector<E> pushFilledTail(Object[] presizedBufferWith, final Object[] array, final Object[] array2) {
        final int size = this.size();
        final int rootShift = this.rootShift;
        if (size >> 5 > 1 << rootShift) {
            presizedBufferWith = UtilsKt.presizedBufferWith(presizedBufferWith);
            final int n = this.rootShift + 5;
            return new PersistentVector<E>(this.pushTail(presizedBufferWith, n, array), array2, this.size() + 1, n);
        }
        return new PersistentVector<E>(this.pushTail(presizedBufferWith, rootShift, array), array2, this.size() + 1, this.rootShift);
    }
    
    private final Object[] pushTail(Object[] original, final int n, final Object[] array) {
        final int indexSegment = UtilsKt.indexSegment(this.size() - 1, n);
        Label_0045: {
            if (original != null) {
                final Object[] copy = Arrays.copyOf(original, 32);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                if ((original = copy) != null) {
                    break Label_0045;
                }
            }
            original = new Object[32];
        }
        if (n == 5) {
            original[indexSegment] = array;
        }
        else {
            original[indexSegment] = this.pushTail((Object[])original[indexSegment], n - 5, array);
        }
        return original;
    }
    
    private final Object[] removeFromRootAt(Object[] copy, int n, final int n2, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        int indexSegment2 = 31;
        if (n == 0) {
            Object[] copy2;
            if (indexSegment == 0) {
                copy2 = new Object[32];
            }
            else {
                copy2 = Arrays.copyOf(copy, 32);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
            }
            ArraysKt.copyInto(copy, copy2, indexSegment, indexSegment + 1, 32);
            copy2[31] = objectRef.getValue();
            objectRef.setValue(copy[indexSegment]);
            return copy2;
        }
        if (copy[31] == null) {
            indexSegment2 = UtilsKt.indexSegment(this.rootSize() - 1, n);
        }
        copy = Arrays.copyOf(copy, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        n -= 5;
        final int n3 = indexSegment + 1;
        if (n3 <= indexSegment2) {
            while (true) {
                final Object o = copy[indexSegment2];
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
                copy[indexSegment2] = this.removeFromRootAt((Object[])o, n, 0, objectRef);
                if (indexSegment2 == n3) {
                    break;
                }
                --indexSegment2;
            }
        }
        final Object o2 = copy[indexSegment];
        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        copy[indexSegment] = this.removeFromRootAt((Object[])o2, n, n2, objectRef);
        return copy;
    }
    
    private final PersistentList<E> removeFromTailAt(final Object[] array, final int n, final int n2, final int n3) {
        final int n4 = this.size() - n;
        CommonFunctionsKt.assert(n3 < n4);
        if (n4 == 1) {
            return this.pullLastBufferFromRoot(array, n, n2);
        }
        final Object[] copy = Arrays.copyOf(this.tail, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        final int n5 = n4 - 1;
        if (n3 < n5) {
            ArraysKt.copyInto(this.tail, copy, n3, n3 + 1, n4);
        }
        copy[n5] = null;
        return new PersistentVector(array, copy, n + n4 - 1, n2);
    }
    
    private final int rootSize() {
        return UtilsKt.rootSize(this.size());
    }
    
    private final Object[] setInRoot(final Object[] original, final int n, final int n2, final Object o) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        final Object[] copy = Arrays.copyOf(original, 32);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        if (n == 0) {
            copy[indexSegment] = o;
        }
        else {
            final Object o2 = copy[indexSegment];
            Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            copy[indexSegment] = this.setInRoot((Object[])o2, n - 5, n2, o);
        }
        return copy;
    }
    
    @Override
    public PersistentList<E> add(final int n, final E e) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        if (n == this.size()) {
            return this.add(e);
        }
        final int rootSize = this.rootSize();
        if (n >= rootSize) {
            return this.insertIntoTail(this.root, n - rootSize, e);
        }
        final ObjectRef objectRef = new ObjectRef(null);
        return this.insertIntoTail(this.insertIntoRoot(this.root, this.rootShift, n, e, objectRef), 0, objectRef.getValue());
    }
    
    @Override
    public PersistentList<E> add(final E e) {
        final int n = this.size() - this.rootSize();
        if (n < 32) {
            final Object[] copy = Arrays.copyOf(this.tail, 32);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            copy[n] = e;
            return new PersistentVector(this.root, copy, this.size() + 1, this.rootShift);
        }
        return this.pushFilledTail(this.root, this.tail, UtilsKt.presizedBufferWith(e));
    }
    
    public PersistentVectorBuilder<E> builder() {
        return new PersistentVectorBuilder<E>((PersistentList<? extends E>)this, this.root, this.tail, this.rootShift);
    }
    
    public E get(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        return (E)this.bufferFor(n)[n & 0x1F];
    }
    
    public int getSize() {
        return this.size;
    }
    
    public ListIterator<E> listIterator(final int n) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        return new PersistentVectorIterator<E>(this.root, (E[])this.tail, n, this.size(), this.rootShift / 5 + 1);
    }
    
    @Override
    public PersistentList<E> removeAll(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final PersistentVectorBuilder<E> builder = this.builder();
        builder.removeAllWithPredicate(function1);
        return builder.build();
    }
    
    @Override
    public PersistentList<E> removeAt(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        final int rootSize = this.rootSize();
        if (n >= rootSize) {
            return this.removeFromTailAt(this.root, rootSize, this.rootShift, n - rootSize);
        }
        return this.removeFromTailAt(this.removeFromRootAt(this.root, this.rootShift, n, new ObjectRef(this.tail[0])), rootSize, this.rootShift, 0);
    }
    
    @Override
    public PersistentList<E> set(final int n, final E e) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        if (this.rootSize() <= n) {
            final Object[] copy = Arrays.copyOf(this.tail, 32);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            copy[n & 0x1F] = e;
            return new PersistentVector(this.root, copy, this.size(), this.rootShift);
        }
        return new PersistentVector(this.setInRoot(this.root, this.rootShift, n, e), this.tail, this.size(), this.rootShift);
    }
}

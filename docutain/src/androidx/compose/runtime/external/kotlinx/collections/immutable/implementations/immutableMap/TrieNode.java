// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import kotlin.jvm.functions.Function1;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.DeltaCounter;
import java.util.Arrays;
import kotlin.ranges.RangesKt;
import kotlin.ranges.IntProgression;
import kotlin.Unit;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.MutabilityOwnership;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b*\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b(\b\u0000\u0018\u0000 }*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003:\u0002}~B'\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u000e\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b¢\u0006\u0002\u0010\tB/\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u000e\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\u0091\u0001\u0010\u0011\u001a\u00020\u00122\u0081\u0001\u0010\u0013\u001a}\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0006\u0012\u0004\u0012\u00020\u00120\u0014H\u0000¢\u0006\u0002\b\u001aJ\u009c\u0001\u0010\u0011\u001a\u00020\u00122\u0081\u0001\u0010\u0013\u001a}\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0006\u0012\u0004\u0012\u00020\u00120\u00142\u0006\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0005H\u0002J\u0014\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001cH\u0002J\u0014\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001cH\u0002JO\u0010\u001e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b2\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00028\u00002\u0006\u0010#\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u00052\b\u0010$\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0002\u0010%J\b\u0010&\u001a\u00020\u0005H\u0002J\u0015\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010*J\u0017\u0010+\u001a\u0004\u0018\u00018\u00012\u0006\u0010)\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010,J+\u0010-\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u001c2\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u0001H\u0002¢\u0006\u0002\u0010/J#\u00100\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010)\u001a\u00028\u0000H\u0002¢\u0006\u0002\u00101J+\u00100\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u0001H\u0002¢\u0006\u0002\u00102J\u001e\u00103\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00104\u001a\u00020\u0005H\u0002J#\u00105\u001a\u00020(2\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u00107J\u001c\u00108\u001a\u00020(2\u0012\u00109\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000H\u0002J\r\u0010:\u001a\u00020\u0005H\u0000¢\u0006\u0002\b;J\u0015\u0010<\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u0005H\u0000¢\u0006\u0002\b=J%\u0010>\u001a\u0004\u0018\u00018\u00012\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010?J\u0015\u0010@\u001a\u00020(2\u0006\u0010 \u001a\u00020\u0005H\u0000¢\u0006\u0002\bAJ\u0010\u0010B\u001a\u00020(2\u0006\u0010 \u001a\u00020\u0005H\u0002J1\u0010C\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010 \u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u0001H\u0002¢\u0006\u0002\u0010DJ\u0015\u0010E\u001a\u00028\u00002\u0006\u0010\u001f\u001a\u00020\u0005H\u0002¢\u0006\u0002\u0010FJ[\u0010G\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010H\u001a\u00020\u00052\u0006\u0010I\u001a\u00028\u00002\u0006\u0010J\u001a\u00028\u00012\u0006\u0010K\u001a\u00020\u00052\u0006\u0010L\u001a\u00028\u00002\u0006\u0010M\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u00052\b\u0010$\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0002\u0010NJI\u0010O\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00028\u00002\u0006\u0010#\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u0005H\u0002¢\u0006\u0002\u0010PJ=\u0010Q\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002¢\u0006\u0002\u0010TJ8\u0010U\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0012\u00109\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010V\u001a\u00020W2\u0006\u0010$\u001a\u00020\u000bH\u0002J?\u0010X\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002¢\u0006\u0002\u0010TJ7\u0010X\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010)\u001a\u00028\u00002\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002¢\u0006\u0002\u0010YJ2\u0010Z\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00104\u001a\u00020\u00052\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002J9\u0010[\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010 \u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0006\u0010$\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010\\JQ\u0010]\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00028\u00002\u0006\u0010#\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u00052\u0006\u0010$\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010^JK\u0010_\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u00052\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010S¢\u0006\u0002\u0010`JJ\u0010a\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0012\u00109\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u0018\u001a\u00020\u00052\u0006\u0010V\u001a\u00020W2\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SJT\u0010b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0012\u00109\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010 \u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00052\u0006\u0010V\u001a\u00020W2\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002JM\u0010c\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u00052\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010S¢\u0006\u0002\u0010`JE\u0010c\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00020\u00052\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010S¢\u0006\u0002\u0010dJ:\u0010e\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002J.\u0010f\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010$\u001a\u00020\u000bH\u0002JX\u0010h\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0012\u0010i\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0014\u0010j\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0006\u0010$\u001a\u00020\u000bH\u0002J8\u0010k\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0012\u0010j\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010$\u001a\u00020\u000bH\u0002J=\u0010l\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010.\u001a\u00028\u00012\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010SH\u0002¢\u0006\u0002\u0010mJ!\u0010n\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010g\u001a\u00020\u0005H\u0000¢\u0006\u0002\boJ\u0015\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u0005H\u0000¢\u0006\u0002\bpJ9\u0010q\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u001c2\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010rJ9\u0010s\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010.\u001a\u00028\u00012\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010tJ1\u0010s\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u00106\u001a\u00020\u00052\u0006\u0010)\u001a\u00028\u00002\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010uJ&\u0010v\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u0005H\u0002J&\u0010w\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u0005H\u0002JP\u0010x\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0012\u0010i\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0014\u0010j\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u0005H\u0002J8\u0010y\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010g\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u00052\u0012\u0010j\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000H\u0002J)\u0010z\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010.\u001a\u00028\u0001H\u0002¢\u0006\u0002\u0010{J\u0015\u0010|\u001a\u00028\u00012\u0006\u0010\u001f\u001a\u00020\u0005H\u0002¢\u0006\u0002\u0010FR0\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b2\u000e\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b@BX\u0080\u000e¢\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u007f" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "K", "V", "", "dataMap", "", "nodeMap", "buffer", "", "(II[Ljava/lang/Object;)V", "ownedBy", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "(II[Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)V", "<set-?>", "getBuffer$runtime_release", "()[Ljava/lang/Object;", "[Ljava/lang/Object;", "accept", "", "visitor", "Lkotlin/Function5;", "Lkotlin/ParameterName;", "name", "node", "shift", "hash", "accept$runtime_release", "asInsertResult", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode$ModificationResult;", "asUpdateResult", "bufferMoveEntryToNode", "keyIndex", "positionMask", "newKeyHash", "newKey", "newValue", "owner", "(IIILjava/lang/Object;Ljava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)[Ljava/lang/Object;", "calculateSize", "collisionContainsKey", "", "key", "(Ljava/lang/Object;)Z", "collisionGet", "(Ljava/lang/Object;)Ljava/lang/Object;", "collisionPut", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode$ModificationResult;", "collisionRemove", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "(Ljava/lang/Object;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "collisionRemoveEntryAtIndex", "i", "containsKey", "keyHash", "(ILjava/lang/Object;I)Z", "elementsIdentityEquals", "otherNode", "entryCount", "entryCount$runtime_release", "entryKeyIndex", "entryKeyIndex$runtime_release", "get", "(ILjava/lang/Object;I)Ljava/lang/Object;", "hasEntryAt", "hasEntryAt$runtime_release", "hasNodeAt", "insertEntryAt", "(ILjava/lang/Object;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "keyAtIndex", "(I)Ljava/lang/Object;", "makeNode", "keyHash1", "key1", "value1", "keyHash2", "key2", "value2", "(ILjava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "moveEntryToNode", "(IIILjava/lang/Object;Ljava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutableCollisionPut", "mutator", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "(Ljava/lang/Object;Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutableCollisionPutAll", "intersectionCounter", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/DeltaCounter;", "mutableCollisionRemove", "(Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutableCollisionRemoveEntryAtIndex", "mutableInsertEntryAt", "(ILjava/lang/Object;Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutableMoveEntryToNode", "(IIILjava/lang/Object;Ljava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutablePut", "(ILjava/lang/Object;Ljava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutablePutAll", "mutablePutAllFromOtherNodeCell", "mutableRemove", "(ILjava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "mutableRemoveEntryAtIndex", "mutableRemoveNodeAtIndex", "nodeIndex", "mutableReplaceNode", "targetNode", "newNode", "mutableUpdateNodeAtIndex", "mutableUpdateValueAtIndex", "(ILjava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "nodeAtIndex", "nodeAtIndex$runtime_release", "nodeIndex$runtime_release", "put", "(ILjava/lang/Object;Ljava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode$ModificationResult;", "remove", "(ILjava/lang/Object;Ljava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "(ILjava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "removeEntryAtIndex", "removeNodeAtIndex", "replaceNode", "updateNodeAtIndex", "updateValueAtIndex", "(ILjava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "valueAtKeyIndex", "Companion", "ModificationResult", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TrieNode<K, V>
{
    public static final Companion Companion;
    private static final TrieNode EMPTY;
    private Object[] buffer;
    private int dataMap;
    private int nodeMap;
    private final MutabilityOwnership ownedBy;
    
    static {
        Companion = new Companion(null);
        EMPTY = new TrieNode(0, 0, new Object[0]);
    }
    
    public TrieNode(final int n, final int n2, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "buffer");
        this(n, n2, array, null);
    }
    
    public TrieNode(final int dataMap, final int nodeMap, final Object[] buffer, final MutabilityOwnership ownedBy) {
        Intrinsics.checkNotNullParameter((Object)buffer, "buffer");
        this.dataMap = dataMap;
        this.nodeMap = nodeMap;
        this.ownedBy = ownedBy;
        this.buffer = buffer;
    }
    
    private final void accept(final Function5<? super TrieNode<K, V>, ? super Integer, ? super Integer, ? super Integer, ? super Integer, Unit> function5, final int i, final int j) {
        function5.invoke((Object)this, (Object)j, (Object)i, (Object)this.dataMap, (Object)this.nodeMap);
        int lowestOneBit;
        for (int k = this.nodeMap; k != 0; k -= lowestOneBit) {
            lowestOneBit = Integer.lowestOneBit(k);
            this.nodeAtIndex$runtime_release(this.nodeIndex$runtime_release(lowestOneBit)).accept(function5, (Integer.numberOfTrailingZeros(lowestOneBit) << j) + i, j + 5);
        }
    }
    
    public static final /* synthetic */ TrieNode access$getEMPTY$cp() {
        return TrieNode.EMPTY;
    }
    
    private final ModificationResult<K, V> asInsertResult() {
        return new ModificationResult<K, V>(this, 1);
    }
    
    private final ModificationResult<K, V> asUpdateResult() {
        return new ModificationResult<K, V>(this, 0);
    }
    
    private final Object[] bufferMoveEntryToNode(final int n, int nodeIndex$runtime_release, final int n2, final K k, final V v, final int n3, final MutabilityOwnership mutabilityOwnership) {
        final K keyAtIndex = this.keyAtIndex(n);
        int hashCode;
        if (keyAtIndex != null) {
            hashCode = keyAtIndex.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode<K, V> node = this.makeNode(hashCode, keyAtIndex, this.valueAtKeyIndex(n), n2, k, v, n3 + 5, mutabilityOwnership);
        nodeIndex$runtime_release = this.nodeIndex$runtime_release(nodeIndex$runtime_release);
        return TrieNodeKt.access$replaceEntryWithNode(this.buffer, n, nodeIndex$runtime_release + 1, node);
    }
    
    private final int calculateSize() {
        if (this.nodeMap == 0) {
            return this.buffer.length / 2;
        }
        int bitCount = Integer.bitCount(this.dataMap);
        for (int i = bitCount * 2; i < this.buffer.length; ++i) {
            bitCount += this.nodeAtIndex$runtime_release(i).calculateSize();
        }
        return bitCount;
    }
    
    private final boolean collisionContainsKey(final K k) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return false;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, this.buffer[n])) {
            if (n == last) {
                return false;
            }
            n += step2;
        }
        return true;
    }
    
    private final V collisionGet(final K k) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return null;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n))) {
            if (n == last) {
                return null;
            }
            n += step2;
        }
        return this.valueAtKeyIndex(n);
    }
    
    private final ModificationResult<K, V> collisionPut(final K k, final V v) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return new TrieNode(0, 0, TrieNodeKt.access$insertEntryAtIndex(this.buffer, 0, k, v)).asInsertResult();
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n))) {
            if (n == last) {
                return new TrieNode(0, 0, TrieNodeKt.access$insertEntryAtIndex(this.buffer, 0, k, v)).asInsertResult();
            }
            n += step2;
        }
        if (v == this.valueAtKeyIndex(n)) {
            return null;
        }
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n + 1] = v;
        return new TrieNode(0, 0, copy).asUpdateResult();
    }
    
    private final TrieNode<K, V> collisionRemove(final K k) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return this;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n))) {
            if (n == last) {
                return this;
            }
            n += step2;
        }
        return this.collisionRemoveEntryAtIndex(n);
    }
    
    private final TrieNode<K, V> collisionRemove(final K k, final V v) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return this;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n)) || !Intrinsics.areEqual((Object)v, (Object)this.valueAtKeyIndex(n))) {
            if (n == last) {
                return this;
            }
            n += step2;
        }
        return this.collisionRemoveEntryAtIndex(n);
    }
    
    private final TrieNode<K, V> collisionRemoveEntryAtIndex(final int n) {
        final Object[] buffer = this.buffer;
        if (buffer.length == 2) {
            return null;
        }
        return new TrieNode<K, V>(0, 0, TrieNodeKt.access$removeEntryAtIndex(buffer, n));
    }
    
    private final boolean elementsIdentityEquals(final TrieNode<K, V> trieNode) {
        if (this == trieNode) {
            return true;
        }
        if (this.nodeMap != trieNode.nodeMap) {
            return false;
        }
        if (this.dataMap != trieNode.dataMap) {
            return false;
        }
        for (int length = this.buffer.length, i = 0; i < length; ++i) {
            if (this.buffer[i] != trieNode.buffer[i]) {
                return false;
            }
        }
        return true;
    }
    
    private final boolean hasNodeAt(final int n) {
        return (n & this.nodeMap) != 0x0;
    }
    
    private final TrieNode<K, V> insertEntryAt(final int n, final K k, final V v) {
        return new TrieNode<K, V>(n | this.dataMap, this.nodeMap, TrieNodeKt.access$insertEntryAtIndex(this.buffer, this.entryKeyIndex$runtime_release(n), k, v));
    }
    
    private final K keyAtIndex(final int n) {
        return (K)this.buffer[n];
    }
    
    private final TrieNode<K, V> makeNode(final int n, final K k, final V v, final int n2, final K i, final V v2, final int n3, final MutabilityOwnership mutabilityOwnership) {
        if (n3 > 30) {
            return new TrieNode<K, V>(0, 0, new Object[] { k, v, i, v2 }, mutabilityOwnership);
        }
        final int indexSegment = TrieNodeKt.indexSegment(n, n3);
        final int indexSegment2 = TrieNodeKt.indexSegment(n2, n3);
        if (indexSegment != indexSegment2) {
            Object[] array;
            if (indexSegment < indexSegment2) {
                array = new Object[] { k, v, i, v2 };
            }
            else {
                array = new Object[] { i, v2, k, v };
            }
            return new TrieNode<K, V>(1 << indexSegment | 1 << indexSegment2, 0, array, mutabilityOwnership);
        }
        return new TrieNode<K, V>(0, 1 << indexSegment, new Object[] { this.makeNode(n, k, v, n2, i, v2, n3 + 5, mutabilityOwnership) }, mutabilityOwnership);
    }
    
    private final TrieNode<K, V> moveEntryToNode(final int n, final int n2, final int n3, final K k, final V v, final int n4) {
        return new TrieNode<K, V>(this.dataMap ^ n2, n2 | this.nodeMap, this.bufferMoveEntryToNode(n, n2, n3, k, v, n4, null));
    }
    
    private final TrieNode<K, V> mutableCollisionPut(final K k, final V v, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        Label_0185: {
            int n;
            if (step2 <= 0 || (n = first) > last) {
                if (step2 >= 0 || last > first) {
                    break Label_0185;
                }
                n = first;
            }
            while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n))) {
                if (n == last) {
                    break Label_0185;
                }
                n += step2;
            }
            persistentHashMapBuilder.setOperationResult$runtime_release(this.valueAtKeyIndex(n));
            if (this.ownedBy == persistentHashMapBuilder.getOwnership$runtime_release()) {
                this.buffer[n + 1] = v;
                return this;
            }
            persistentHashMapBuilder.setModCount$runtime_release(persistentHashMapBuilder.getModCount$runtime_release() + 1);
            final Object[] buffer = this.buffer;
            final Object[] copy = Arrays.copyOf(buffer, buffer.length);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
            copy[n + 1] = v;
            return new TrieNode<K, V>(0, 0, copy, persistentHashMapBuilder.getOwnership$runtime_release());
        }
        persistentHashMapBuilder.setSize(persistentHashMapBuilder.size() + 1);
        return new TrieNode<K, V>(0, 0, TrieNodeKt.access$insertEntryAtIndex(this.buffer, 0, k, v), persistentHashMapBuilder.getOwnership$runtime_release());
    }
    
    private final TrieNode<K, V> mutableCollisionPutAll(TrieNode<K, V> trieNode, final DeltaCounter deltaCounter, final MutabilityOwnership mutabilityOwnership) {
        CommonFunctionsKt.assert(this.nodeMap == 0);
        CommonFunctionsKt.assert(this.dataMap == 0);
        CommonFunctionsKt.assert(trieNode.nodeMap == 0);
        CommonFunctionsKt.assert(trieNode.dataMap == 0);
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length + trieNode.buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        final int length = this.buffer.length;
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, trieNode.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int newLength = 0;
    Label_0289:
        while (true) {
            Label_0180: {
                if (step2 <= 0) {
                    break Label_0180;
                }
                int n = length;
                int n2;
                if ((n2 = first) > last) {
                    break Label_0180;
                }
                while (true) {
                    if (!this.collisionContainsKey(trieNode.buffer[n2])) {
                        final Object[] buffer2 = trieNode.buffer;
                        copy[n] = buffer2[n2];
                        copy[n + 1] = buffer2[n2 + 1];
                        n += 2;
                    }
                    else {
                        deltaCounter.setCount(deltaCounter.getCount() + 1);
                    }
                    newLength = n;
                    if (n2 == last) {
                        break Label_0289;
                    }
                    n2 += step2;
                }
            }
            newLength = length;
            if (step2 < 0) {
                newLength = length;
                if (last <= first) {
                    final int n2 = first;
                    final int n = length;
                    continue;
                }
            }
            break;
        }
        if (newLength == this.buffer.length) {
            trieNode = this;
        }
        else if (newLength != trieNode.buffer.length) {
            if (newLength == copy.length) {
                trieNode = new TrieNode<K, V>(0, 0, copy, mutabilityOwnership);
            }
            else {
                final Object[] copy2 = Arrays.copyOf(copy, newLength);
                Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
                trieNode = new TrieNode<K, V>(0, 0, copy2, mutabilityOwnership);
            }
        }
        return trieNode;
    }
    
    private final TrieNode<K, V> mutableCollisionRemove(final K k, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return this;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n))) {
            if (n == last) {
                return this;
            }
            n += step2;
        }
        return this.mutableCollisionRemoveEntryAtIndex(n, persistentHashMapBuilder);
    }
    
    private final TrieNode<K, V> mutableCollisionRemove(final K k, final V v, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        final IntProgression step = RangesKt.step((IntProgression)RangesKt.until(0, this.buffer.length), 2);
        final int first = step.getFirst();
        final int last = step.getLast();
        final int step2 = step.getStep();
        int n;
        if (step2 <= 0 || (n = first) > last) {
            if (step2 >= 0 || last > first) {
                return this;
            }
            n = first;
        }
        while (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(n)) || !Intrinsics.areEqual((Object)v, (Object)this.valueAtKeyIndex(n))) {
            if (n == last) {
                return this;
            }
            n += step2;
        }
        return this.mutableCollisionRemoveEntryAtIndex(n, persistentHashMapBuilder);
    }
    
    private final TrieNode<K, V> mutableCollisionRemoveEntryAtIndex(final int n, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        persistentHashMapBuilder.setSize(persistentHashMapBuilder.size() - 1);
        persistentHashMapBuilder.setOperationResult$runtime_release(this.valueAtKeyIndex(n));
        if (this.buffer.length == 2) {
            return null;
        }
        if (this.ownedBy == persistentHashMapBuilder.getOwnership$runtime_release()) {
            this.buffer = TrieNodeKt.access$removeEntryAtIndex(this.buffer, n);
            return this;
        }
        return new TrieNode<K, V>(0, 0, TrieNodeKt.access$removeEntryAtIndex(this.buffer, n), persistentHashMapBuilder.getOwnership$runtime_release());
    }
    
    private final TrieNode<K, V> mutableInsertEntryAt(final int n, final K k, final V v, final MutabilityOwnership mutabilityOwnership) {
        final int entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n);
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = TrieNodeKt.access$insertEntryAtIndex(this.buffer, entryKeyIndex$runtime_release, k, v);
            this.dataMap |= n;
            return this;
        }
        return new TrieNode<K, V>(n | this.dataMap, this.nodeMap, TrieNodeKt.access$insertEntryAtIndex(this.buffer, entryKeyIndex$runtime_release, k, v), mutabilityOwnership);
    }
    
    private final TrieNode<K, V> mutableMoveEntryToNode(final int n, final int n2, final int n3, final K k, final V v, final int n4, final MutabilityOwnership mutabilityOwnership) {
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = this.bufferMoveEntryToNode(n, n2, n3, k, v, n4, mutabilityOwnership);
            this.dataMap ^= n2;
            this.nodeMap |= n2;
            return this;
        }
        return new TrieNode<K, V>(this.dataMap ^ n2, n2 | this.nodeMap, this.bufferMoveEntryToNode(n, n2, n3, k, v, n4, mutabilityOwnership), mutabilityOwnership);
    }
    
    private final TrieNode<K, V> mutablePutAllFromOtherNodeCell(final TrieNode<K, V> trieNode, int n, int n2, final DeltaCounter deltaCounter, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        final boolean hasNode = this.hasNodeAt(n);
        int hashCode = 0;
        Object o;
        if (hasNode) {
            final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(this.nodeIndex$runtime_release(n));
            if (trieNode.hasNodeAt(n)) {
                o = nodeAtIndex$runtime_release.mutablePutAll(trieNode.nodeAtIndex$runtime_release(trieNode.nodeIndex$runtime_release(n)), n2 + 5, deltaCounter, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
            }
            else {
                o = nodeAtIndex$runtime_release;
                if (trieNode.hasEntryAt$runtime_release(n)) {
                    n = trieNode.entryKeyIndex$runtime_release(n);
                    final Object keyAtIndex = trieNode.keyAtIndex(n);
                    final Object valueAtKeyIndex = trieNode.valueAtKeyIndex(n);
                    final int size = persistentHashMapBuilder.size();
                    if (keyAtIndex != null) {
                        n = keyAtIndex.hashCode();
                    }
                    else {
                        n = 0;
                    }
                    final TrieNode<Object, Object> trieNode2 = (TrieNode<Object, Object>)(o = nodeAtIndex$runtime_release.mutablePut(n, keyAtIndex, valueAtKeyIndex, n2 + 5, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder));
                    if (persistentHashMapBuilder.size() == size) {
                        deltaCounter.setCount(deltaCounter.getCount() + 1);
                        o = trieNode2;
                    }
                }
            }
        }
        else if (trieNode.hasNodeAt(n)) {
            o = trieNode.nodeAtIndex$runtime_release(trieNode.nodeIndex$runtime_release(n));
            if (this.hasEntryAt$runtime_release(n)) {
                final int entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n);
                final K keyAtIndex2 = this.keyAtIndex(entryKeyIndex$runtime_release);
                if (keyAtIndex2 != null) {
                    n = keyAtIndex2.hashCode();
                }
                else {
                    n = 0;
                }
                n2 += 5;
                if (((TrieNode<Object, Object>)o).containsKey(n, keyAtIndex2, n2)) {
                    deltaCounter.setCount(deltaCounter.getCount() + 1);
                }
                else {
                    final V valueAtKeyIndex2 = this.valueAtKeyIndex(entryKeyIndex$runtime_release);
                    if (keyAtIndex2 != null) {
                        n = keyAtIndex2.hashCode();
                    }
                    else {
                        n = 0;
                    }
                    o = ((TrieNode<Object, Object>)o).mutablePut(n, keyAtIndex2, valueAtKeyIndex2, n2, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
                }
            }
        }
        else {
            final int entryKeyIndex$runtime_release2 = this.entryKeyIndex$runtime_release(n);
            final K keyAtIndex3 = this.keyAtIndex(entryKeyIndex$runtime_release2);
            final V valueAtKeyIndex3 = this.valueAtKeyIndex(entryKeyIndex$runtime_release2);
            n = trieNode.entryKeyIndex$runtime_release(n);
            final Object keyAtIndex4 = trieNode.keyAtIndex(n);
            final Object valueAtKeyIndex4 = trieNode.valueAtKeyIndex(n);
            if (keyAtIndex3 != null) {
                n = keyAtIndex3.hashCode();
            }
            else {
                n = 0;
            }
            if (keyAtIndex4 != null) {
                hashCode = keyAtIndex4.hashCode();
            }
            o = this.makeNode(n, keyAtIndex3, valueAtKeyIndex3, hashCode, keyAtIndex4, valueAtKeyIndex4, n2 + 5, persistentHashMapBuilder.getOwnership$runtime_release());
        }
        return (TrieNode<K, V>)o;
    }
    
    private final TrieNode<K, V> mutableRemoveEntryAtIndex(final int n, final int n2, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        persistentHashMapBuilder.setSize(persistentHashMapBuilder.size() - 1);
        persistentHashMapBuilder.setOperationResult$runtime_release(this.valueAtKeyIndex(n));
        if (this.buffer.length == 2) {
            return null;
        }
        if (this.ownedBy == persistentHashMapBuilder.getOwnership$runtime_release()) {
            this.buffer = TrieNodeKt.access$removeEntryAtIndex(this.buffer, n);
            this.dataMap ^= n2;
            return this;
        }
        return new TrieNode<K, V>(n2 ^ this.dataMap, this.nodeMap, TrieNodeKt.access$removeEntryAtIndex(this.buffer, n), persistentHashMapBuilder.getOwnership$runtime_release());
    }
    
    private final TrieNode<K, V> mutableRemoveNodeAtIndex(final int n, final int n2, final MutabilityOwnership mutabilityOwnership) {
        final Object[] buffer = this.buffer;
        if (buffer.length == 1) {
            return null;
        }
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = TrieNodeKt.access$removeNodeAtIndex(buffer, n);
            this.nodeMap ^= n2;
            return this;
        }
        return new TrieNode<K, V>(this.dataMap, n2 ^ this.nodeMap, TrieNodeKt.access$removeNodeAtIndex(buffer, n), mutabilityOwnership);
    }
    
    private final TrieNode<K, V> mutableReplaceNode(final TrieNode<K, V> trieNode, final TrieNode<K, V> trieNode2, final int n, final int n2, final MutabilityOwnership mutabilityOwnership) {
        TrieNode<K, V> trieNode3;
        if (trieNode2 == null) {
            trieNode3 = this.mutableRemoveNodeAtIndex(n, n2, mutabilityOwnership);
        }
        else if (this.ownedBy != mutabilityOwnership && trieNode == trieNode2) {
            trieNode3 = this;
        }
        else {
            trieNode3 = this.mutableUpdateNodeAtIndex(n, trieNode2, mutabilityOwnership);
        }
        return trieNode3;
    }
    
    private final TrieNode<K, V> mutableUpdateNodeAtIndex(final int n, final TrieNode<K, V> trieNode, final MutabilityOwnership mutabilityOwnership) {
        final Object[] buffer = this.buffer;
        if (buffer.length == 1 && trieNode.buffer.length == 2 && trieNode.nodeMap == 0) {
            trieNode.dataMap = this.nodeMap;
            return trieNode;
        }
        if (this.ownedBy == mutabilityOwnership) {
            buffer[n] = trieNode;
            return this;
        }
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = trieNode;
        return new TrieNode<K, V>(this.dataMap, this.nodeMap, copy, mutabilityOwnership);
    }
    
    private final TrieNode<K, V> mutableUpdateValueAtIndex(final int n, final V v, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        if (this.ownedBy == persistentHashMapBuilder.getOwnership$runtime_release()) {
            this.buffer[n + 1] = v;
            return this;
        }
        persistentHashMapBuilder.setModCount$runtime_release(persistentHashMapBuilder.getModCount$runtime_release() + 1);
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n + 1] = v;
        return new TrieNode<K, V>(this.dataMap, this.nodeMap, copy, persistentHashMapBuilder.getOwnership$runtime_release());
    }
    
    private final TrieNode<K, V> removeEntryAtIndex(final int n, final int n2) {
        final Object[] buffer = this.buffer;
        if (buffer.length == 2) {
            return null;
        }
        return new TrieNode<K, V>(n2 ^ this.dataMap, this.nodeMap, TrieNodeKt.access$removeEntryAtIndex(buffer, n));
    }
    
    private final TrieNode<K, V> removeNodeAtIndex(final int n, final int n2) {
        final Object[] buffer = this.buffer;
        if (buffer.length == 1) {
            return null;
        }
        return new TrieNode<K, V>(this.dataMap, n2 ^ this.nodeMap, TrieNodeKt.access$removeNodeAtIndex(buffer, n));
    }
    
    private final TrieNode<K, V> replaceNode(final TrieNode<K, V> trieNode, final TrieNode<K, V> trieNode2, final int n, final int n2) {
        TrieNode<K, V> trieNode3;
        if (trieNode2 == null) {
            trieNode3 = this.removeNodeAtIndex(n, n2);
        }
        else if (trieNode != trieNode2) {
            trieNode3 = this.updateNodeAtIndex(n, n2, trieNode2);
        }
        else {
            trieNode3 = this;
        }
        return trieNode3;
    }
    
    private final TrieNode<K, V> updateNodeAtIndex(final int n, final int n2, final TrieNode<K, V> trieNode) {
        final Object[] buffer = trieNode.buffer;
        if (buffer.length != 2 || trieNode.nodeMap != 0) {
            final Object[] buffer2 = this.buffer;
            final Object[] copy = Arrays.copyOf(buffer2, buffer2.length);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            copy[n] = trieNode;
            return new TrieNode<K, V>(this.dataMap, this.nodeMap, copy);
        }
        if (this.buffer.length == 1) {
            trieNode.dataMap = this.nodeMap;
            return trieNode;
        }
        return new TrieNode<K, V>(this.dataMap ^ n2, n2 ^ this.nodeMap, TrieNodeKt.access$replaceNodeWithEntry(this.buffer, n, this.entryKeyIndex$runtime_release(n2), buffer[0], buffer[1]));
    }
    
    private final TrieNode<K, V> updateValueAtIndex(final int n, final V v) {
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n + 1] = v;
        return new TrieNode<K, V>(this.dataMap, this.nodeMap, copy);
    }
    
    private final V valueAtKeyIndex(final int n) {
        return (V)this.buffer[n + 1];
    }
    
    public final void accept$runtime_release(final Function5<? super TrieNode<K, V>, ? super Integer, ? super Integer, ? super Integer, ? super Integer, Unit> function5) {
        Intrinsics.checkNotNullParameter((Object)function5, "visitor");
        this.accept(function5, 0, 0);
    }
    
    public final boolean containsKey(final int n, final K k, final int n2) {
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasEntryAt$runtime_release(n3)) {
            return Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(this.entryKeyIndex$runtime_release(n3)));
        }
        if (!this.hasNodeAt(n3)) {
            return false;
        }
        final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(this.nodeIndex$runtime_release(n3));
        if (n2 == 30) {
            return nodeAtIndex$runtime_release.collisionContainsKey((Object)k);
        }
        return nodeAtIndex$runtime_release.containsKey(n, (Object)k, n2 + 5);
    }
    
    public final int entryCount$runtime_release() {
        return Integer.bitCount(this.dataMap);
    }
    
    public final int entryKeyIndex$runtime_release(final int n) {
        return Integer.bitCount(n - 1 & this.dataMap) * 2;
    }
    
    public final V get(int entryKeyIndex$runtime_release, final K k, final int n) {
        final int n2 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n);
        if (this.hasEntryAt$runtime_release(n2)) {
            entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n2);
            if (Intrinsics.areEqual((Object)k, this.keyAtIndex(entryKeyIndex$runtime_release))) {
                return this.valueAtKeyIndex(entryKeyIndex$runtime_release);
            }
            return null;
        }
        else {
            if (!this.hasNodeAt(n2)) {
                return null;
            }
            final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(this.nodeIndex$runtime_release(n2));
            if (n == 30) {
                return nodeAtIndex$runtime_release.collisionGet((Object)k);
            }
            return nodeAtIndex$runtime_release.get(entryKeyIndex$runtime_release, (Object)k, n + 5);
        }
    }
    
    public final Object[] getBuffer$runtime_release() {
        return this.buffer;
    }
    
    public final boolean hasEntryAt$runtime_release(final int n) {
        return (n & this.dataMap) != 0x0;
    }
    
    public final TrieNode<K, V> mutablePut(final int n, final K k, final V v, final int n2, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        Intrinsics.checkNotNullParameter((Object)persistentHashMapBuilder, "mutator");
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasEntryAt$runtime_release(n3)) {
            final int entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n3);
            if (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(entryKeyIndex$runtime_release))) {
                persistentHashMapBuilder.setSize(persistentHashMapBuilder.size() + 1);
                return this.mutableMoveEntryToNode(entryKeyIndex$runtime_release, n3, n, k, v, n2, persistentHashMapBuilder.getOwnership$runtime_release());
            }
            persistentHashMapBuilder.setOperationResult$runtime_release(this.valueAtKeyIndex(entryKeyIndex$runtime_release));
            if (this.valueAtKeyIndex(entryKeyIndex$runtime_release) == v) {
                return this;
            }
            return this.mutableUpdateValueAtIndex(entryKeyIndex$runtime_release, v, persistentHashMapBuilder);
        }
        else {
            if (!this.hasNodeAt(n3)) {
                persistentHashMapBuilder.setSize(persistentHashMapBuilder.size() + 1);
                return this.mutableInsertEntryAt(n3, k, v, persistentHashMapBuilder.getOwnership$runtime_release());
            }
            final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n3);
            final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
            TrieNode<Object, Object> trieNode;
            if (n2 == 30) {
                trieNode = nodeAtIndex$runtime_release.mutableCollisionPut((Object)k, (Object)v, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
            }
            else {
                trieNode = nodeAtIndex$runtime_release.mutablePut(n, (Object)k, (Object)v, n2 + 5, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
            }
            if (nodeAtIndex$runtime_release == trieNode) {
                return this;
            }
            return this.mutableUpdateNodeAtIndex(nodeIndex$runtime_release, (TrieNode<K, V>)trieNode, persistentHashMapBuilder.getOwnership$runtime_release());
        }
    }
    
    public final TrieNode<K, V> mutablePutAll(final TrieNode<K, V> trieNode, int lowestOneBit, final DeltaCounter deltaCounter, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "otherNode");
        Intrinsics.checkNotNullParameter((Object)deltaCounter, "intersectionCounter");
        Intrinsics.checkNotNullParameter((Object)persistentHashMapBuilder, "mutator");
        if (this == trieNode) {
            deltaCounter.plusAssign(this.calculateSize());
            return this;
        }
        if (lowestOneBit > 30) {
            return this.mutableCollisionPutAll(trieNode, deltaCounter, persistentHashMapBuilder.getOwnership$runtime_release());
        }
        int i = this.nodeMap | trieNode.nodeMap;
        final int dataMap = this.dataMap;
        final int dataMap2 = trieNode.dataMap;
        int j = dataMap & dataMap2;
        int k = (dataMap ^ dataMap2) & ~i;
        while (j != 0) {
            final int lowestOneBit2 = Integer.lowestOneBit(j);
            if (Intrinsics.areEqual((Object)this.keyAtIndex(this.entryKeyIndex$runtime_release(lowestOneBit2)), trieNode.keyAtIndex(trieNode.entryKeyIndex$runtime_release(lowestOneBit2)))) {
                k |= lowestOneBit2;
            }
            else {
                i |= lowestOneBit2;
            }
            j ^= lowestOneBit2;
        }
        final int n = 0;
        if ((i & k) == 0x0) {
            TrieNode trieNode2;
            if (Intrinsics.areEqual((Object)this.ownedBy, (Object)persistentHashMapBuilder.getOwnership$runtime_release()) && this.dataMap == k && this.nodeMap == i) {
                trieNode2 = this;
            }
            else {
                trieNode2 = new TrieNode(k, i, new Object[Integer.bitCount(k) * 2 + Integer.bitCount(i)]);
            }
            int n2 = 0;
            int l = i;
            int m;
            int n3;
            while (true) {
                m = k;
                n3 = n;
                if (l == 0) {
                    break;
                }
                final int lowestOneBit3 = Integer.lowestOneBit(l);
                final Object[] buffer = trieNode2.buffer;
                buffer[buffer.length - 1 - n2] = this.mutablePutAllFromOtherNodeCell(trieNode, lowestOneBit3, lowestOneBit, deltaCounter, persistentHashMapBuilder);
                ++n2;
                l ^= lowestOneBit3;
            }
            while (m != 0) {
                lowestOneBit = Integer.lowestOneBit(m);
                final int n4 = n3 * 2;
                if (!trieNode.hasEntryAt$runtime_release(lowestOneBit)) {
                    final int entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(lowestOneBit);
                    trieNode2.buffer[n4] = this.keyAtIndex(entryKeyIndex$runtime_release);
                    trieNode2.buffer[n4 + 1] = this.valueAtKeyIndex(entryKeyIndex$runtime_release);
                }
                else {
                    final int entryKeyIndex$runtime_release2 = trieNode.entryKeyIndex$runtime_release(lowestOneBit);
                    trieNode2.buffer[n4] = trieNode.keyAtIndex(entryKeyIndex$runtime_release2);
                    trieNode2.buffer[n4 + 1] = trieNode.valueAtKeyIndex(entryKeyIndex$runtime_release2);
                    if (this.hasEntryAt$runtime_release(lowestOneBit)) {
                        deltaCounter.setCount(deltaCounter.getCount() + 1);
                    }
                }
                ++n3;
                m ^= lowestOneBit;
            }
            TrieNode trieNode3;
            if (this.elementsIdentityEquals(trieNode2)) {
                trieNode3 = this;
            }
            else {
                trieNode3 = trieNode2;
                if (trieNode.elementsIdentityEquals(trieNode2)) {
                    trieNode3 = trieNode;
                }
            }
            return trieNode3;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
    
    public final TrieNode<K, V> mutableRemove(int entryKeyIndex$runtime_release, final K k, final int n, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        Intrinsics.checkNotNullParameter((Object)persistentHashMapBuilder, "mutator");
        final int n2 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n);
        if (this.hasEntryAt$runtime_release(n2)) {
            entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n2);
            if (Intrinsics.areEqual((Object)k, this.keyAtIndex(entryKeyIndex$runtime_release))) {
                return this.mutableRemoveEntryAtIndex(entryKeyIndex$runtime_release, n2, persistentHashMapBuilder);
            }
            return this;
        }
        else {
            if (this.hasNodeAt(n2)) {
                final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n2);
                final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
                TrieNode<Object, V> trieNode;
                if (n == 30) {
                    trieNode = nodeAtIndex$runtime_release.mutableCollisionRemove((Object)k, (PersistentHashMapBuilder<Object, V>)persistentHashMapBuilder);
                }
                else {
                    trieNode = nodeAtIndex$runtime_release.mutableRemove(entryKeyIndex$runtime_release, (Object)k, n + 5, (PersistentHashMapBuilder<Object, V>)persistentHashMapBuilder);
                }
                return this.mutableReplaceNode(nodeAtIndex$runtime_release, (TrieNode<K, V>)trieNode, nodeIndex$runtime_release, n2, persistentHashMapBuilder.getOwnership$runtime_release());
            }
            return this;
        }
    }
    
    public final TrieNode<K, V> mutableRemove(int entryKeyIndex$runtime_release, final K k, final V v, final int n, final PersistentHashMapBuilder<K, V> persistentHashMapBuilder) {
        Intrinsics.checkNotNullParameter((Object)persistentHashMapBuilder, "mutator");
        final int n2 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n);
        if (this.hasEntryAt$runtime_release(n2)) {
            entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n2);
            if (Intrinsics.areEqual((Object)k, this.keyAtIndex(entryKeyIndex$runtime_release)) && Intrinsics.areEqual((Object)v, this.valueAtKeyIndex(entryKeyIndex$runtime_release))) {
                return this.mutableRemoveEntryAtIndex(entryKeyIndex$runtime_release, n2, persistentHashMapBuilder);
            }
            return this;
        }
        else {
            if (this.hasNodeAt(n2)) {
                final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n2);
                final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
                TrieNode<Object, Object> trieNode;
                if (n == 30) {
                    trieNode = nodeAtIndex$runtime_release.mutableCollisionRemove((Object)k, (Object)v, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
                }
                else {
                    trieNode = nodeAtIndex$runtime_release.mutableRemove(entryKeyIndex$runtime_release, (Object)k, (Object)v, n + 5, (PersistentHashMapBuilder<Object, Object>)persistentHashMapBuilder);
                }
                return this.mutableReplaceNode(nodeAtIndex$runtime_release, (TrieNode<K, V>)trieNode, nodeIndex$runtime_release, n2, persistentHashMapBuilder.getOwnership$runtime_release());
            }
            return this;
        }
    }
    
    public final TrieNode<K, V> nodeAtIndex$runtime_release(final int n) {
        final Object o = this.buffer[n];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode>");
        return (TrieNode<K, V>)o;
    }
    
    public final int nodeIndex$runtime_release(final int n) {
        return this.buffer.length - 1 - Integer.bitCount(n - 1 & this.nodeMap);
    }
    
    public final ModificationResult<K, V> put(final int n, final K k, final V v, final int n2) {
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasEntryAt$runtime_release(n3)) {
            final int entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n3);
            if (!Intrinsics.areEqual((Object)k, (Object)this.keyAtIndex(entryKeyIndex$runtime_release))) {
                return this.moveEntryToNode(entryKeyIndex$runtime_release, n3, n, k, v, n2).asInsertResult();
            }
            if (this.valueAtKeyIndex(entryKeyIndex$runtime_release) == v) {
                return null;
            }
            return this.updateValueAtIndex(entryKeyIndex$runtime_release, v).asUpdateResult();
        }
        else {
            if (this.hasNodeAt(n3)) {
                final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n3);
                final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
                ModificationResult<Object, Object> modificationResult;
                if (n2 == 30) {
                    if ((modificationResult = nodeAtIndex$runtime_release.collisionPut((Object)k, (Object)v)) == null) {
                        return null;
                    }
                }
                else if ((modificationResult = nodeAtIndex$runtime_release.put(n, (Object)k, (Object)v, n2 + 5)) == null) {
                    return null;
                }
                modificationResult.setNode(this.updateNodeAtIndex(nodeIndex$runtime_release, n3, (TrieNode<Object, Object>)modificationResult.getNode()));
                return (ModificationResult<K, V>)modificationResult;
            }
            return this.insertEntryAt(n3, k, v).asInsertResult();
        }
    }
    
    public final TrieNode<K, V> remove(int entryKeyIndex$runtime_release, final K k, final int n) {
        final int n2 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n);
        if (this.hasEntryAt$runtime_release(n2)) {
            entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n2);
            if (Intrinsics.areEqual((Object)k, this.keyAtIndex(entryKeyIndex$runtime_release))) {
                return this.removeEntryAtIndex(entryKeyIndex$runtime_release, n2);
            }
            return this;
        }
        else {
            if (this.hasNodeAt(n2)) {
                final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n2);
                final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
                TrieNode<Object, V> trieNode;
                if (n == 30) {
                    trieNode = nodeAtIndex$runtime_release.collisionRemove((Object)k);
                }
                else {
                    trieNode = nodeAtIndex$runtime_release.remove(entryKeyIndex$runtime_release, (Object)k, n + 5);
                }
                return this.replaceNode(nodeAtIndex$runtime_release, (TrieNode<K, V>)trieNode, nodeIndex$runtime_release, n2);
            }
            return this;
        }
    }
    
    public final TrieNode<K, V> remove(int entryKeyIndex$runtime_release, final K k, final V v, final int n) {
        final int n2 = 1 << TrieNodeKt.indexSegment(entryKeyIndex$runtime_release, n);
        if (this.hasEntryAt$runtime_release(n2)) {
            entryKeyIndex$runtime_release = this.entryKeyIndex$runtime_release(n2);
            if (Intrinsics.areEqual((Object)k, this.keyAtIndex(entryKeyIndex$runtime_release)) && Intrinsics.areEqual((Object)v, this.valueAtKeyIndex(entryKeyIndex$runtime_release))) {
                return this.removeEntryAtIndex(entryKeyIndex$runtime_release, n2);
            }
            return this;
        }
        else {
            if (this.hasNodeAt(n2)) {
                final int nodeIndex$runtime_release = this.nodeIndex$runtime_release(n2);
                final TrieNode<K, V> nodeAtIndex$runtime_release = this.nodeAtIndex$runtime_release(nodeIndex$runtime_release);
                TrieNode<Object, Object> trieNode;
                if (n == 30) {
                    trieNode = nodeAtIndex$runtime_release.collisionRemove((Object)k, (Object)v);
                }
                else {
                    trieNode = nodeAtIndex$runtime_release.remove(entryKeyIndex$runtime_release, (Object)k, (Object)v, n + 5);
                }
                return this.replaceNode(nodeAtIndex$runtime_release, (TrieNode<K, V>)trieNode, nodeIndex$runtime_release, n2);
            }
            return this;
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "", "getEMPTY$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final TrieNode getEMPTY$runtime_release() {
            return TrieNode.access$getEMPTY$cp();
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u0000*\u0004\b\u0002\u0010\u0001*\u0004\b\u0003\u0010\u00022\u00020\u0003B!\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJD\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00002*\u0010\u0010\u001a&\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00050\u0011H\u0086\b\u00f8\u0001\u0000R&\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u0012" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode$ModificationResult;", "K", "V", "", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "sizeDelta", "", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;I)V", "getNode", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "setNode", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;)V", "getSizeDelta", "()I", "replaceNode", "operation", "Lkotlin/Function1;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class ModificationResult<K, V>
    {
        private TrieNode<K, V> node;
        private final int sizeDelta;
        
        public ModificationResult(final TrieNode<K, V> node, final int sizeDelta) {
            Intrinsics.checkNotNullParameter((Object)node, "node");
            this.node = node;
            this.sizeDelta = sizeDelta;
        }
        
        public final TrieNode<K, V> getNode() {
            return this.node;
        }
        
        public final int getSizeDelta() {
            return this.sizeDelta;
        }
        
        public final ModificationResult<K, V> replaceNode(final Function1<? super TrieNode<K, V>, TrieNode<K, V>> function1) {
            Intrinsics.checkNotNullParameter((Object)function1, "operation");
            final ModificationResult modificationResult = this;
            this.setNode((TrieNode<K, V>)function1.invoke((Object)this.getNode()));
            return this;
        }
        
        public final void setNode(final TrieNode<K, V> node) {
            Intrinsics.checkNotNullParameter((Object)node, "<set-?>");
            this.node = node;
        }
    }
}

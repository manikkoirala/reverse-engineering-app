// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import java.util.Arrays;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import java.util.ArrayList;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.ArrayIteratorKt;
import kotlin.ranges.RangesKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.ListImplementation;
import java.util.ListIterator;
import kotlin.collections.ArraysKt;
import java.util.Collection;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.MutabilityOwnership;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentList;
import kotlin.collections.AbstractMutableList;

@Metadata(d1 = { "\u0000~\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0007\n\u0002\u0010(\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010)\n\u0000\n\u0002\u0010*\n\u0000\n\u0002\u0010+\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\b\u0018\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B=\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0010\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007\u0012\u000e\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0015\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001fJ\u001d\u0010\u001c\u001a\u00020 2\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\"J\u001e\u0010#\u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\u000b2\f\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000%H\u0016J\u0016\u0010#\u001a\u00020\u001d2\f\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000%H\u0016J\u001d\u0010&\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010!\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010'J\u000e\u0010(\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005H\u0016J=\u0010)\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010*\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010+\u001a\u00020\u000b2\u000e\u0010,\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0-H\u0002¢\u0006\u0002\u0010.J\u0016\u0010/\u001a\u00028\u00002\u0006\u0010!\u001a\u00020\u000bH\u0096\u0002¢\u0006\u0002\u00100J\r\u00101\u001a\u00020\u000bH\u0000¢\u0006\u0002\b2JG\u00103\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u00104\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u000b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\u0006\u00105\u001a\u000206H\u0002¢\u0006\u0002\u00107J[\u00103\u001a\u00020 2\f\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000%2\u0006\u0010!\u001a\u00020\u000b2\u0006\u00108\u001a\u00020\u000b2\u0016\u00109\u001a\u0012\u0012\u000e\u0012\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00070\u00072\u0006\u0010:\u001a\u00020\u000b2\u000e\u0010;\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0002\u0010<J/\u0010=\u001a\u00020 2\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010>J\u001d\u0010?\u001a\u00020\u001d2\u000e\u0010*\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0002\u0010@J\u000f\u0010A\u001a\b\u0012\u0004\u0012\u00028\u00000BH\u0096\u0002J\u001e\u0010C\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00070D2\u0006\u0010!\u001a\u00020\u000bH\u0002J\u000e\u0010E\u001a\b\u0012\u0004\u0012\u00028\u00000FH\u0016J\u0016\u0010E\u001a\b\u0012\u0004\u0012\u00028\u00000F2\u0006\u0010!\u001a\u00020\u000bH\u0016J'\u0010G\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0010\u0010*\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007H\u0002¢\u0006\u0002\u0010HJ-\u0010I\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010*\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010J\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010KJ\u0015\u0010L\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0002\u0010\u0012J\u001f\u0010M\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\b\u0010\u001e\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0002\u0010NJ5\u0010O\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010!\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010PJ?\u0010Q\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u00104\u001a\u00020\u000b2\u0006\u0010R\u001a\u00020\u000b2\u0006\u0010S\u001a\u000206H\u0002¢\u0006\u0002\u0010TJ/\u0010U\u001a\u00020 2\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0006\u0010R\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010VJM\u0010W\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0006\u0010R\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u000b2\u0014\u0010X\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00070-H\u0002¢\u0006\u0002\u0010YJE\u0010Z\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0006\u0010R\u001a\u00020\u000b2\u0014\u00109\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00070\u0007H\u0002¢\u0006\u0002\u0010[J?\u0010\\\u001a\u00020 2\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u000e\u0010]\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010^\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0002\u0010_J?\u0010`\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u000e\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u00104\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010aJu\u0010b\u001a\u00020\u000b2\u0012\u0010c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001d0d2\u000e\u0010*\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010e\u001a\u00020\u000b2\u0006\u0010f\u001a\u00020\u000b2\u0006\u0010g\u001a\u0002062\u0014\u0010h\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00070i2\u0014\u00109\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00070iH\u0002¢\u0006\u0002\u0010jJ\u001c\u0010k\u001a\u00020\u001d2\u0012\u0010c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001d0dH\u0002JA\u0010k\u001a\u00020\u000b2\u0012\u0010c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001d0d2\u000e\u0010*\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010e\u001a\u00020\u000b2\u0006\u0010g\u001a\u000206H\u0002¢\u0006\u0002\u0010lJ\u0016\u0010k\u001a\u00020\u001d2\f\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000%H\u0016J,\u0010m\u001a\u00020\u000b2\u0012\u0010c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001d0d2\u0006\u0010n\u001a\u00020\u000b2\u0006\u0010g\u001a\u000206H\u0002J\u001a\u0010o\u001a\u00020\u001d2\u0012\u0010c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001d0dJ\u0015\u0010p\u001a\u00028\u00002\u0006\u0010!\u001a\u00020\u000bH\u0016¢\u0006\u0002\u00100J=\u0010q\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u00104\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010S\u001a\u000206H\u0002¢\u0006\u0002\u0010TJ9\u0010r\u001a\u0004\u0018\u00010\b2\u0010\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0006\u0010R\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010sJ/\u0010t\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010\u0018\u001a\u00020\u000bH\u0002¢\u0006\u0002\u0010KJ\b\u0010R\u001a\u00020\u000bH\u0002J\u001e\u0010u\u001a\u00028\u00002\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010vJE\u0010w\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u00104\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010x\u001a\u00028\u00002\u0006\u0010y\u001a\u000206H\u0002¢\u0006\u0002\u00107JU\u0010z\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010{\u001a\u00020\u000b2\u0006\u00108\u001a\u00020\u000b2\u0016\u00109\u001a\u0012\u0012\u000e\u0012\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00070\u00072\u0006\u0010:\u001a\u00020\u000b2\u000e\u0010;\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0002\u0010|Jl\u0010}\u001a\u00020 2\f\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000%2\u0006\u0010!\u001a\u00020\u000b2\u000e\u0010~\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010\u007f\u001a\u00020\u000b2\u0016\u00109\u001a\u0012\u0012\u000e\u0012\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00070\u00072\u0006\u0010:\u001a\u00020\u000b2\u000e\u0010;\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007H\u0002¢\u0006\u0003\u0010\u0080\u0001J\b\u0010n\u001a\u00020\u000bH\u0002J\u0010\u0010n\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u000bH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R4\u0010\u0010\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00072\u0010\u0010\u000f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007@BX\u0080\u000e¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012R\u001a\u0010\n\u001a\u00020\u000bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001e\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b@RX\u0096\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R0\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u000e\u0010\u000f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007@BX\u0080\u000e¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u001b\u0010\u0012R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u0013R\u0018\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u0013¨\u0006\u0081\u0001" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorBuilder;", "E", "Lkotlin/collections/AbstractMutableList;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList$Builder;", "vector", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "vectorRoot", "", "", "vectorTail", "rootShift", "", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;[Ljava/lang/Object;[Ljava/lang/Object;I)V", "ownership", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "<set-?>", "root", "getRoot$runtime_release", "()[Ljava/lang/Object;", "[Ljava/lang/Object;", "getRootShift$runtime_release", "()I", "setRootShift$runtime_release", "(I)V", "size", "getSize", "tail", "getTail$runtime_release", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "", "bufferFor", "(I)[Ljava/lang/Object;", "build", "copyToBuffer", "buffer", "bufferIndex", "sourceIterator", "", "([Ljava/lang/Object;ILjava/util/Iterator;)[Ljava/lang/Object;", "get", "(I)Ljava/lang/Object;", "getModCount", "getModCount$runtime_release", "insertIntoRoot", "shift", "elementCarry", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;", "([Ljava/lang/Object;IILjava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;)[Ljava/lang/Object;", "rightShift", "buffers", "nullBuffers", "nextBuffer", "(Ljava/util/Collection;II[[Ljava/lang/Object;I[Ljava/lang/Object;)V", "insertIntoTail", "([Ljava/lang/Object;ILjava/lang/Object;)V", "isMutable", "([Ljava/lang/Object;)Z", "iterator", "", "leafBufferIterator", "", "listIterator", "", "makeMutable", "([Ljava/lang/Object;)[Ljava/lang/Object;", "makeMutableShiftingRight", "distance", "([Ljava/lang/Object;I)[Ljava/lang/Object;", "mutableBuffer", "mutableBufferWith", "(Ljava/lang/Object;)[Ljava/lang/Object;", "nullifyAfter", "([Ljava/lang/Object;II)[Ljava/lang/Object;", "pullLastBuffer", "rootSize", "tailCarry", "([Ljava/lang/Object;IILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;)[Ljava/lang/Object;", "pullLastBufferFromRoot", "([Ljava/lang/Object;II)V", "pushBuffers", "buffersIterator", "([Ljava/lang/Object;IILjava/util/Iterator;)[Ljava/lang/Object;", "pushBuffersIncreasingHeightIfNeeded", "([Ljava/lang/Object;I[[Ljava/lang/Object;)[Ljava/lang/Object;", "pushFilledTail", "filledTail", "newTail", "([Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)V", "pushTail", "([Ljava/lang/Object;[Ljava/lang/Object;I)[Ljava/lang/Object;", "recyclableRemoveAll", "predicate", "Lkotlin/Function1;", "bufferSize", "toBufferSize", "bufferRef", "recyclableBuffers", "", "(Lkotlin/jvm/functions/Function1;[Ljava/lang/Object;IILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;Ljava/util/List;Ljava/util/List;)I", "removeAll", "(Lkotlin/jvm/functions/Function1;[Ljava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/ObjectRef;)I", "removeAllFromTail", "tailSize", "removeAllWithPredicate", "removeAt", "removeFromRootAt", "removeFromTailAt", "([Ljava/lang/Object;III)Ljava/lang/Object;", "retainFirst", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "setInRoot", "e", "oldElementCarry", "shiftLeafBuffers", "startLeafIndex", "(II[[Ljava/lang/Object;I[Ljava/lang/Object;)[Ljava/lang/Object;", "splitToBuffers", "startBuffer", "startBufferSize", "(Ljava/util/Collection;I[Ljava/lang/Object;I[[Ljava/lang/Object;I[Ljava/lang/Object;)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentVectorBuilder<E> extends AbstractMutableList<E> implements Builder<E>
{
    private MutabilityOwnership ownership;
    private Object[] root;
    private int rootShift;
    private int size;
    private Object[] tail;
    private PersistentList<? extends E> vector;
    private Object[] vectorRoot;
    private Object[] vectorTail;
    
    public PersistentVectorBuilder(final PersistentList<? extends E> vector, final Object[] vectorRoot, final Object[] vectorTail, final int rootShift) {
        Intrinsics.checkNotNullParameter((Object)vector, "vector");
        Intrinsics.checkNotNullParameter((Object)vectorTail, "vectorTail");
        this.vector = vector;
        this.vectorRoot = vectorRoot;
        this.vectorTail = vectorTail;
        this.rootShift = rootShift;
        this.ownership = new MutabilityOwnership();
        this.root = this.vectorRoot;
        this.tail = this.vectorTail;
        this.size = this.vector.size();
    }
    
    private final Object[] bufferFor(final int n) {
        if (this.rootSize() <= n) {
            return this.tail;
        }
        Object[] root = this.root;
        Intrinsics.checkNotNull((Object)root);
        for (int i = this.rootShift; i > 0; i -= 5) {
            final Object o = root[UtilsKt.indexSegment(n, i)];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            root = (Object[])o;
        }
        return root;
    }
    
    private final Object[] copyToBuffer(final Object[] array, int n, final Iterator<?> iterator) {
        while (n < 32 && iterator.hasNext()) {
            array[n] = iterator.next();
            ++n;
        }
        return array;
    }
    
    private final void insertIntoRoot(final Collection<? extends E> collection, final int n, int n2, final Object[][] array, final int n3, Object[] array2) {
        if (this.root != null) {
            final int n4 = n >> 5;
            final Object[] shiftLeafBuffers = this.shiftLeafBuffers(n4, n2, array, n3, array2);
            n2 = n3 - ((this.rootSize() >> 5) - 1 - n4);
            if (n2 < n3) {
                array2 = array[n2];
                Intrinsics.checkNotNull((Object)array2);
            }
            this.splitToBuffers(collection, n, shiftLeafBuffers, 32, array, n2, array2);
            return;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    private final Object[] insertIntoRoot(Object[] array, int n, final int n2, Object o, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        if (n == 0) {
            objectRef.setValue(array[31]);
            array = ArraysKt.copyInto(array, this.makeMutable(array), indexSegment + 1, indexSegment, 31);
            array[indexSegment] = o;
            return array;
        }
        array = this.makeMutable(array);
        final int n3 = n - 5;
        final Object o2 = array[indexSegment];
        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        array[indexSegment] = this.insertIntoRoot((Object[])o2, n3, n2, o, objectRef);
        n = indexSegment;
        while (++n < 32) {
            o = array[n];
            if (o == null) {
                break;
            }
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            array[n] = this.insertIntoRoot((Object[])o, n3, 0, objectRef.getValue(), objectRef);
        }
        return array;
    }
    
    private final void insertIntoTail(final Object[] root, final int n, final E e) {
        final int tailSize = this.tailSize();
        final Object[] mutable = this.makeMutable(this.tail);
        if (tailSize < 32) {
            ArraysKt.copyInto(this.tail, mutable, n + 1, n, tailSize);
            mutable[n] = e;
            this.root = root;
            this.tail = mutable;
            this.size = this.size() + 1;
        }
        else {
            final Object[] tail = this.tail;
            final Object o = tail[31];
            ArraysKt.copyInto(tail, mutable, n + 1, n, 31);
            mutable[n] = e;
            this.pushFilledTail(root, mutable, this.mutableBufferWith(o));
        }
    }
    
    private final boolean isMutable(final Object[] array) {
        return array.length == 33 && array[32] == this.ownership;
    }
    
    private final ListIterator<Object[]> leafBufferIterator(final int n) {
        if (this.root == null) {
            throw new IllegalStateException("Required value was null.".toString());
        }
        final int n2 = this.rootSize() >> 5;
        ListImplementation.checkPositionIndex$runtime_release(n, n2);
        final int rootShift = this.rootShift;
        if (rootShift == 0) {
            final Object[] root = this.root;
            Intrinsics.checkNotNull((Object)root);
            return new SingleElementListIterator<Object[]>(root, n);
        }
        final int n3 = rootShift / 5;
        final Object[] root2 = this.root;
        Intrinsics.checkNotNull((Object)root2);
        return new TrieIterator<Object[]>(root2, n, n2, n3);
    }
    
    private final Object[] makeMutable(final Object[] array) {
        if (array == null) {
            return this.mutableBuffer();
        }
        if (this.isMutable(array)) {
            return array;
        }
        return ArraysKt.copyInto$default(array, this.mutableBuffer(), 0, 0, RangesKt.coerceAtMost(array.length, 32), 6, (Object)null);
    }
    
    private final Object[] makeMutableShiftingRight(final Object[] array, final int n) {
        if (this.isMutable(array)) {
            return ArraysKt.copyInto(array, array, n, 0, 32 - n);
        }
        return ArraysKt.copyInto(array, this.mutableBuffer(), n, 0, 32 - n);
    }
    
    private final Object[] mutableBuffer() {
        return new Object[] { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this.ownership };
    }
    
    private final Object[] mutableBufferWith(final Object o) {
        return new Object[] { o, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this.ownership };
    }
    
    private final Object[] nullifyAfter(Object[] mutable, int n, final int n2) {
        if (n2 < 0) {
            throw new IllegalStateException("Check failed.".toString());
        }
        if (n2 == 0) {
            return mutable;
        }
        final int indexSegment = UtilsKt.indexSegment(n, n2);
        final Object o = mutable[indexSegment];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        final Object[] nullifyAfter = this.nullifyAfter((Object[])o, n, n2 - 5);
        Object[] copyInto = mutable;
        if (indexSegment < 31) {
            n = indexSegment + 1;
            copyInto = mutable;
            if (mutable[n] != null) {
                if (this.isMutable(mutable)) {
                    ArraysKt.fill(mutable, (Object)null, n, 32);
                }
                copyInto = ArraysKt.copyInto(mutable, this.mutableBuffer(), 0, 0, n);
            }
        }
        mutable = copyInto;
        if (nullifyAfter != copyInto[indexSegment]) {
            mutable = this.makeMutable(copyInto);
            mutable[indexSegment] = nullifyAfter;
        }
        return mutable;
    }
    
    private final Object[] pullLastBuffer(Object[] mutable, final int n, final int n2, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2 - 1, n);
        Object pullLastBuffer;
        if (n == 5) {
            objectRef.setValue(mutable[indexSegment]);
            pullLastBuffer = null;
        }
        else {
            final Object o = mutable[indexSegment];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            pullLastBuffer = this.pullLastBuffer((Object[])o, n - 5, n2, objectRef);
        }
        if (pullLastBuffer == null && indexSegment == 0) {
            return null;
        }
        mutable = this.makeMutable(mutable);
        mutable[indexSegment] = pullLastBuffer;
        return mutable;
    }
    
    private final void pullLastBufferFromRoot(Object[] pullLastBuffer, final int n, final int n2) {
        if (n2 == 0) {
            this.root = null;
            Object[] tail;
            if ((tail = pullLastBuffer) == null) {
                tail = new Object[0];
            }
            this.tail = tail;
            this.size = n;
            this.rootShift = n2;
            return;
        }
        final ObjectRef objectRef = new ObjectRef(null);
        Intrinsics.checkNotNull((Object)pullLastBuffer);
        pullLastBuffer = this.pullLastBuffer(pullLastBuffer, n2, n, objectRef);
        Intrinsics.checkNotNull((Object)pullLastBuffer);
        final Object value = objectRef.getValue();
        Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        this.tail = (Object[])value;
        this.size = n;
        if (pullLastBuffer[1] == null) {
            this.root = (Object[])pullLastBuffer[0];
            this.rootShift = n2 - 5;
        }
        else {
            this.root = pullLastBuffer;
            this.rootShift = n2;
        }
    }
    
    private final Object[] pushBuffers(Object[] array, int n, int n2, final Iterator<Object[]> iterator) {
        if (!iterator.hasNext()) {
            throw new IllegalStateException("Check failed.".toString());
        }
        if (n2 < 0) {
            throw new IllegalStateException("Check failed.".toString());
        }
        if (n2 == 0) {
            return iterator.next();
        }
        final Object[] mutable = this.makeMutable(array);
        final int indexSegment = UtilsKt.indexSegment(n, n2);
        array = (Object[])mutable[indexSegment];
        n2 -= 5;
        mutable[indexSegment] = this.pushBuffers(array, n, n2, iterator);
        n = indexSegment;
        while (++n < 32 && iterator.hasNext()) {
            mutable[n] = this.pushBuffers((Object[])mutable[n], 0, n2, iterator);
        }
        return mutable;
    }
    
    private final Object[] pushBuffersIncreasingHeightIfNeeded(Object[] array, int rootShift, final Object[][] array2) {
        final Iterator iterator = ArrayIteratorKt.iterator((Object[])array2);
        final int rootShift2 = this.rootShift;
        if (rootShift >> 5 < 1 << rootShift2) {
            array = this.pushBuffers(array, rootShift, rootShift2, iterator);
        }
        else {
            array = this.makeMutable(array);
        }
        while (iterator.hasNext()) {
            this.rootShift += 5;
            array = this.mutableBufferWith(array);
            rootShift = this.rootShift;
            this.pushBuffers(array, 1 << rootShift, rootShift, iterator);
        }
        return array;
    }
    
    private final void pushFilledTail(final Object[] array, final Object[] root, final Object[] tail) {
        final int size = this.size();
        final int rootShift = this.rootShift;
        if (size >> 5 > 1 << rootShift) {
            this.root = this.pushTail(this.mutableBufferWith(array), root, this.rootShift + 5);
            this.tail = tail;
            this.rootShift += 5;
            this.size = this.size() + 1;
        }
        else if (array == null) {
            this.root = root;
            this.tail = tail;
            this.size = this.size() + 1;
        }
        else {
            this.root = this.pushTail(array, root, rootShift);
            this.tail = tail;
            this.size = this.size() + 1;
        }
    }
    
    private final Object[] pushTail(Object[] mutable, final Object[] array, final int n) {
        final int indexSegment = UtilsKt.indexSegment(this.size() - 1, n);
        mutable = this.makeMutable(mutable);
        if (n == 5) {
            mutable[indexSegment] = array;
        }
        else {
            mutable[indexSegment] = this.pushTail((Object[])mutable[indexSegment], array, n - 5);
        }
        return mutable;
    }
    
    private final int recyclableRemoveAll(final Function1<? super E, Boolean> function1, final Object[] array, final int n, int n2, final ObjectRef objectRef, final List<Object[]> list, final List<Object[]> list2) {
        if (this.isMutable(array)) {
            list.add(array);
        }
        final Object value = objectRef.getValue();
        Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        Object[] mutableBuffer;
        final Object[] array2 = mutableBuffer = (Object[])value;
        Object[] array3;
        int n3;
        for (int i = 0; i < n; ++i, mutableBuffer = array3, n2 = n3) {
            final Object o = array[i];
            array3 = mutableBuffer;
            n3 = n2;
            if (!(boolean)function1.invoke(o)) {
                if ((n3 = n2) == 32) {
                    if (list.isEmpty() ^ true) {
                        mutableBuffer = list.remove(list.size() - 1);
                    }
                    else {
                        mutableBuffer = this.mutableBuffer();
                    }
                    n3 = 0;
                }
                mutableBuffer[n3] = o;
                ++n3;
                array3 = mutableBuffer;
            }
        }
        objectRef.setValue(mutableBuffer);
        if (array2 != objectRef.getValue()) {
            list2.add(array2);
        }
        return n2;
    }
    
    private final int removeAll(final Function1<? super E, Boolean> function1, final Object[] array, final int n, final ObjectRef objectRef) {
        int i = 0;
        Object[] value = array;
        int n2 = n;
        int n3 = 0;
        while (i < n) {
            final Object o = array[i];
            int n4;
            Object[] mutable;
            int n5;
            if (function1.invoke(o)) {
                n4 = n3;
                mutable = value;
                n5 = n2;
                if (n3 == 0) {
                    mutable = this.makeMutable(array);
                    n4 = 1;
                    n5 = i;
                }
            }
            else {
                n4 = n3;
                mutable = value;
                n5 = n2;
                if (n3 != 0) {
                    value[n2] = o;
                    n5 = n2 + 1;
                    mutable = value;
                    n4 = n3;
                }
            }
            ++i;
            n3 = n4;
            value = mutable;
            n2 = n5;
        }
        objectRef.setValue(value);
        return n2;
    }
    
    private final boolean removeAll(final Function1<? super E, Boolean> function1) {
        final int tailSize = this.tailSize();
        final ObjectRef objectRef = new ObjectRef(null);
        final Object[] root = this.root;
        boolean b = false;
        final boolean b2 = false;
        if (root == null) {
            boolean b3 = b2;
            if (this.removeAllFromTail(function1, tailSize, objectRef) != tailSize) {
                b3 = true;
            }
            return b3;
        }
        ListIterator<Object[]> leafBufferIterator;
        int removeAll;
        for (leafBufferIterator = this.leafBufferIterator(0), removeAll = 32; removeAll == 32 && leafBufferIterator.hasNext(); removeAll = this.removeAll(function1, leafBufferIterator.next(), 32, objectRef)) {}
        if (removeAll == 32) {
            CommonFunctionsKt.assert(leafBufferIterator.hasNext() ^ true);
            final int removeAllFromTail = this.removeAllFromTail(function1, tailSize, objectRef);
            if (removeAllFromTail == 0) {
                this.pullLastBufferFromRoot(this.root, this.size(), this.rootShift);
            }
            if (removeAllFromTail != tailSize) {
                b = true;
            }
            return b;
        }
        final int n = leafBufferIterator.previousIndex() << 5;
        final List list = new ArrayList();
        final List list2 = new ArrayList();
        int recyclableRemoveAll = removeAll;
        final int n2 = n;
        while (leafBufferIterator.hasNext()) {
            recyclableRemoveAll = this.recyclableRemoveAll(function1, leafBufferIterator.next(), 32, recyclableRemoveAll, objectRef, list2, list);
        }
        final int recyclableRemoveAll2 = this.recyclableRemoveAll(function1, this.tail, tailSize, recyclableRemoveAll, objectRef, list2, list);
        final Object value = objectRef.getValue();
        Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        final Object[] tail = (Object[])value;
        ArraysKt.fill(tail, (Object)null, recyclableRemoveAll2, 32);
        Object[] array;
        if (list.isEmpty()) {
            array = this.root;
            Intrinsics.checkNotNull((Object)array);
        }
        else {
            array = this.pushBuffers(this.root, n2, this.rootShift, list.iterator());
        }
        final int n3 = n2 + (list.size() << 5);
        this.root = this.retainFirst(array, n3);
        this.tail = tail;
        this.size = n3 + recyclableRemoveAll2;
        return true;
    }
    
    private final int removeAllFromTail(final Function1<? super E, Boolean> function1, final int n, final ObjectRef objectRef) {
        final int removeAll = this.removeAll(function1, this.tail, n, objectRef);
        if (removeAll == n) {
            CommonFunctionsKt.assert(objectRef.getValue() == this.tail);
            return n;
        }
        final Object value = objectRef.getValue();
        Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        final Object[] tail = (Object[])value;
        ArraysKt.fill(tail, (Object)null, removeAll, n);
        this.tail = tail;
        this.size = this.size() - (n - removeAll);
        return removeAll;
    }
    
    private final Object[] removeFromRootAt(Object[] array, int n, final int n2, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        int indexSegment2 = 31;
        if (n == 0) {
            final Object value = array[indexSegment];
            array = ArraysKt.copyInto(array, this.makeMutable(array), indexSegment, indexSegment + 1, 32);
            array[31] = objectRef.getValue();
            objectRef.setValue(value);
            return array;
        }
        if (array[31] == null) {
            indexSegment2 = UtilsKt.indexSegment(this.rootSize() - 1, n);
        }
        array = this.makeMutable(array);
        final int n3 = n - 5;
        n = indexSegment + 1;
        if (n <= indexSegment2) {
            while (true) {
                final Object o = array[indexSegment2];
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
                array[indexSegment2] = this.removeFromRootAt((Object[])o, n3, 0, objectRef);
                if (indexSegment2 == n) {
                    break;
                }
                --indexSegment2;
            }
        }
        final Object o2 = array[indexSegment];
        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        array[indexSegment] = this.removeFromRootAt((Object[])o2, n3, n2, objectRef);
        return array;
    }
    
    private final Object removeFromTailAt(final Object[] root, final int n, final int rootShift, final int n2) {
        final int n3 = this.size() - n;
        CommonFunctionsKt.assert(n2 < n3);
        Object o2;
        if (n3 == 1) {
            final Object o = this.tail[0];
            this.pullLastBufferFromRoot(root, n, rootShift);
            o2 = o;
        }
        else {
            final Object[] tail = this.tail;
            final Object o3 = tail[n2];
            final Object[] copyInto = ArraysKt.copyInto(tail, this.makeMutable(tail), n2, n2 + 1, n3);
            copyInto[n3 - 1] = null;
            this.root = root;
            this.tail = copyInto;
            this.size = n + n3 - 1;
            this.rootShift = rootShift;
            o2 = o3;
        }
        return o2;
    }
    
    private final Object[] retainFirst(Object[] array, int rootShift) {
        if ((rootShift & 0x1F) != 0x0) {
            throw new IllegalStateException("Check failed.".toString());
        }
        if (rootShift == 0) {
            this.rootShift = 0;
            return null;
        }
        final int n = rootShift - 1;
        while (true) {
            rootShift = this.rootShift;
            if (n >> rootShift != 0) {
                break;
            }
            this.rootShift = rootShift - 5;
            final Object o = array[0];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            array = (Object[])o;
        }
        return this.nullifyAfter(array, n, rootShift);
    }
    
    private final int rootSize() {
        if (this.size() <= 32) {
            return 0;
        }
        return UtilsKt.rootSize(this.size());
    }
    
    private final Object[] setInRoot(final Object[] array, final int n, final int n2, final E e, final ObjectRef objectRef) {
        final int indexSegment = UtilsKt.indexSegment(n2, n);
        final Object[] mutable = this.makeMutable(array);
        if (n == 0) {
            if (mutable != array) {
                ++this.modCount;
            }
            objectRef.setValue(mutable[indexSegment]);
            mutable[indexSegment] = e;
            return mutable;
        }
        final Object o = mutable[indexSegment];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        mutable[indexSegment] = this.setInRoot((Object[])o, n - 5, n2, e, objectRef);
        return mutable;
    }
    
    private final Object[] shiftLeafBuffers(final int n, final int n2, final Object[][] array, int n3, Object[] mutableShiftingRight) {
        if (this.root != null) {
            final ListIterator<Object[]> leafBufferIterator = this.leafBufferIterator(this.rootSize() >> 5);
            while (leafBufferIterator.previousIndex() != n) {
                final Object[] array2 = leafBufferIterator.previous();
                ArraysKt.copyInto(array2, mutableShiftingRight, 0, 32 - n2, 32);
                mutableShiftingRight = this.makeMutableShiftingRight(array2, n2);
                --n3;
                array[n3] = mutableShiftingRight;
            }
            return leafBufferIterator.previous();
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    private final void splitToBuffers(final Collection<? extends E> collection, int i, Object[] mutableBuffer, final int n, final Object[][] array, int n2, Object[] array2) {
        final int n3 = 1;
        if (n2 >= 1) {
            final Object[] mutable = this.makeMutable(mutableBuffer);
            array[0] = mutable;
            final int n4 = i & 0x1F;
            i = (i + collection.size() - 1 & 0x1F);
            final int n5 = n - n4 + i;
            if (n5 < 32) {
                ArraysKt.copyInto(mutable, array2, i + 1, n4, n);
            }
            else {
                if (n2 == 1) {
                    mutableBuffer = mutable;
                }
                else {
                    mutableBuffer = this.mutableBuffer();
                    --n2;
                    array[n2] = mutableBuffer;
                }
                final int n6 = n - (n5 - 32 + 1);
                ArraysKt.copyInto(mutable, array2, 0, n6, n);
                ArraysKt.copyInto(mutable, mutableBuffer, i + 1, n4, n6);
                array2 = mutableBuffer;
            }
            final Iterator iterator = collection.iterator();
            this.copyToBuffer(mutable, n4, iterator);
            for (i = n3; i < n2; ++i) {
                array[i] = this.copyToBuffer(this.mutableBuffer(), 0, iterator);
            }
            this.copyToBuffer(array2, 0, iterator);
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
    
    private final int tailSize() {
        return this.tailSize(this.size());
    }
    
    private final int tailSize(final int n) {
        if (n <= 32) {
            return n;
        }
        return n - UtilsKt.rootSize(n);
    }
    
    public void add(final int n, final E e) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        if (n == this.size()) {
            this.add(e);
            return;
        }
        ++this.modCount;
        final int rootSize = this.rootSize();
        if (n >= rootSize) {
            this.insertIntoTail(this.root, n - rootSize, e);
            return;
        }
        final ObjectRef objectRef = new ObjectRef(null);
        final Object[] root = this.root;
        Intrinsics.checkNotNull((Object)root);
        this.insertIntoTail(this.insertIntoRoot(root, this.rootShift, n, e, objectRef), 0, objectRef.getValue());
    }
    
    public boolean add(final E e) {
        ++this.modCount;
        final int tailSize = this.tailSize();
        if (tailSize < 32) {
            final Object[] mutable = this.makeMutable(this.tail);
            mutable[tailSize] = e;
            this.tail = mutable;
            this.size = this.size() + 1;
        }
        else {
            this.pushFilledTail(this.root, this.tail, this.mutableBufferWith(e));
        }
        return true;
    }
    
    public boolean addAll(final int n, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        if (n == this.size()) {
            return this.addAll(collection);
        }
        final boolean empty = collection.isEmpty();
        boolean b = false;
        if (empty) {
            return false;
        }
        ++this.modCount;
        final int n2 = n >> 5 << 5;
        int n3 = (this.size() - n2 + collection.size() - 1) / 32;
        if (n3 == 0) {
            if (n >= this.rootSize()) {
                b = true;
            }
            CommonFunctionsKt.assert(b);
            final int n4 = n & 0x1F;
            final int size = collection.size();
            final Object[] tail = this.tail;
            final Object[] copyInto = ArraysKt.copyInto(tail, this.makeMutable(tail), (n + size - 1 & 0x1F) + 1, n4, this.tailSize());
            this.copyToBuffer(copyInto, n4, collection.iterator());
            this.tail = copyInto;
            this.size = this.size() + collection.size();
            return true;
        }
        final Object[][] array = new Object[n3][];
        final int tailSize = this.tailSize();
        final int tailSize2 = this.tailSize(this.size() + collection.size());
        Object[] tail2;
        if (n >= this.rootSize()) {
            tail2 = this.mutableBuffer();
            this.splitToBuffers(collection, n, this.tail, tailSize, array, n3, tail2);
        }
        else if (tailSize2 > tailSize) {
            final int n5 = tailSize2 - tailSize;
            tail2 = this.makeMutableShiftingRight(this.tail, n5);
            this.insertIntoRoot(collection, n, n5, array, n3, tail2);
        }
        else {
            final Object[] tail3 = this.tail;
            final Object[] mutableBuffer = this.mutableBuffer();
            final int n6 = tailSize - tailSize2;
            tail2 = ArraysKt.copyInto(tail3, mutableBuffer, 0, n6, tailSize);
            final int n7 = 32 - n6;
            final Object[] mutableShiftingRight = this.makeMutableShiftingRight(this.tail, n7);
            --n3;
            this.insertIntoRoot(collection, n, n7, array, n3, array[n3] = mutableShiftingRight);
        }
        this.root = this.pushBuffersIncreasingHeightIfNeeded(this.root, n2, array);
        this.tail = tail2;
        this.size = this.size() + collection.size();
        return true;
    }
    
    public boolean addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        if (collection.isEmpty()) {
            return false;
        }
        ++this.modCount;
        final int tailSize = this.tailSize();
        final Iterator iterator = collection.iterator();
        if (32 - tailSize >= collection.size()) {
            this.tail = this.copyToBuffer(this.makeMutable(this.tail), tailSize, iterator);
            this.size = this.size() + collection.size();
        }
        else {
            final int n = (collection.size() + tailSize - 1) / 32;
            final Object[][] array = new Object[n][];
            array[0] = this.copyToBuffer(this.makeMutable(this.tail), tailSize, iterator);
            for (int i = 1; i < n; ++i) {
                array[i] = this.copyToBuffer(this.mutableBuffer(), 0, iterator);
            }
            this.root = this.pushBuffersIncreasingHeightIfNeeded(this.root, this.rootSize(), array);
            this.tail = this.copyToBuffer(this.mutableBuffer(), 0, iterator);
            this.size = this.size() + collection.size();
        }
        return true;
    }
    
    public PersistentList<E> build() {
        Object vector;
        if (this.root == this.vectorRoot && this.tail == this.vectorTail) {
            vector = this.vector;
        }
        else {
            this.ownership = new MutabilityOwnership();
            final Object[] root = this.root;
            this.vectorRoot = root;
            final Object[] tail = this.tail;
            this.vectorTail = tail;
            if (root == null) {
                if (tail.length == 0) {
                    vector = UtilsKt.persistentVectorOf();
                }
                else {
                    final Object[] copy = Arrays.copyOf(this.tail, this.size());
                    Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                    vector = new SmallPersistentVector<E>(copy);
                }
            }
            else {
                final Object[] root2 = this.root;
                Intrinsics.checkNotNull((Object)root2);
                vector = new PersistentVector<E>(root2, this.tail, this.size(), this.rootShift);
            }
        }
        return (PersistentList<E>)(this.vector = (PersistentList<? extends E>)vector);
    }
    
    public E get(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        return (E)this.bufferFor(n)[n & 0x1F];
    }
    
    public final int getModCount$runtime_release() {
        return this.modCount;
    }
    
    public final Object[] getRoot$runtime_release() {
        return this.root;
    }
    
    public final int getRootShift$runtime_release() {
        return this.rootShift;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public final Object[] getTail$runtime_release() {
        return this.tail;
    }
    
    public Iterator<E> iterator() {
        return this.listIterator();
    }
    
    public ListIterator<E> listIterator() {
        return this.listIterator(0);
    }
    
    public ListIterator<E> listIterator(final int n) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        return new PersistentVectorMutableIterator<E>(this, n);
    }
    
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.removeAllWithPredicate((Function1<? super E, Boolean>)new PersistentVectorBuilder$removeAll.PersistentVectorBuilder$removeAll$1((Collection)collection));
    }
    
    public final boolean removeAllWithPredicate(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final boolean removeAll = this.removeAll(function1);
        if (removeAll) {
            ++this.modCount;
        }
        return removeAll;
    }
    
    public E removeAt(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        ++this.modCount;
        final int rootSize = this.rootSize();
        if (n >= rootSize) {
            return (E)this.removeFromTailAt(this.root, rootSize, this.rootShift, n - rootSize);
        }
        final ObjectRef objectRef = new ObjectRef(this.tail[0]);
        final Object[] root = this.root;
        Intrinsics.checkNotNull((Object)root);
        this.removeFromTailAt(this.removeFromRootAt(root, this.rootShift, n, objectRef), rootSize, this.rootShift, 0);
        return (E)objectRef.getValue();
    }
    
    public E set(int n, final E e) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        if (this.rootSize() <= n) {
            final Object[] mutable = this.makeMutable(this.tail);
            if (mutable != this.tail) {
                ++this.modCount;
            }
            n &= 0x1F;
            final Object o = mutable[n];
            mutable[n] = e;
            this.tail = mutable;
            return (E)o;
        }
        final ObjectRef objectRef = new ObjectRef(null);
        final Object[] root = this.root;
        Intrinsics.checkNotNull((Object)root);
        this.root = this.setInRoot(root, this.rootShift, n, e, objectRef);
        return (E)objectRef.getValue();
    }
    
    public final void setRootShift$runtime_release(final int rootShift) {
        this.rootShift = rootShift;
    }
}

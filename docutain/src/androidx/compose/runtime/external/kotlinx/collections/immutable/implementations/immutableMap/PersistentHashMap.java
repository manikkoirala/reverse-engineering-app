// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import java.util.Collection;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableCollection;
import java.util.Set;
import java.util.Map;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableSet;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.collections.AbstractMap;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\b\u0004\b\u0000\u0018\u0000 ,*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004:\u0001,B!\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0014\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001aH\u0016J\u0014\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0004H\u0016J\u0015\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001fJ\u001a\u0010 \u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\f0\u000bH\u0002J\u0018\u0010!\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u001e\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\"J\u001a\u0010\r\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\f0#H\u0001J)\u0010$\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001e\u001a\u00028\u00002\u0006\u0010%\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010&J*\u0010'\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00042\u0014\u0010(\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010)H\u0016J!\u0010*\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010+J)\u0010*\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010\u001e\u001a\u00028\u00002\u0006\u0010%\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010&R&\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000eR \u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00010\u00168VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018¨\u0006-" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "K", "V", "Lkotlin/collections/AbstractMap;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "size", "", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;I)V", "entries", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "", "getEntries", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "keys", "getKeys", "getNode$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "getSize", "()I", "values", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableCollection;", "getValues", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableCollection;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "clear", "containsKey", "", "key", "(Ljava/lang/Object;)Z", "createEntries", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "", "put", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "putAll", "m", "", "remove", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentHashMap<K, V> extends AbstractMap<K, V> implements PersistentMap<K, V>
{
    public static final Companion Companion;
    private static final PersistentHashMap EMPTY;
    private final TrieNode<K, V> node;
    private final int size;
    
    static {
        Companion = new Companion(null);
        EMPTY = new PersistentHashMap(TrieNode.Companion.getEMPTY$runtime_release(), 0);
    }
    
    public PersistentHashMap(final TrieNode<K, V> node, final int size) {
        Intrinsics.checkNotNullParameter((Object)node, "node");
        this.node = node;
        this.size = size;
    }
    
    public static final /* synthetic */ PersistentHashMap access$getEMPTY$cp() {
        return PersistentHashMap.EMPTY;
    }
    
    private final ImmutableSet<Entry<K, V>> createEntries() {
        return (ImmutableSet)new PersistentHashMapEntries((PersistentHashMap<Object, Object>)this);
    }
    
    public PersistentHashMapBuilder<K, V> builder() {
        return new PersistentHashMapBuilder<K, V>(this);
    }
    
    public PersistentMap<K, V> clear() {
        return (PersistentMap)PersistentHashMap.Companion.emptyOf$runtime_release();
    }
    
    public boolean containsKey(final Object o) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.containsKey(hashCode, (K)o, 0);
    }
    
    public final /* bridge */ ImmutableSet<Entry<K, V>> entrySet() {
        return this.getEntries();
    }
    
    public V get(final Object o) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.get(hashCode, (K)o, 0);
    }
    
    public ImmutableSet<Entry<K, V>> getEntries() {
        return this.createEntries();
    }
    
    public final Set<Entry<K, V>> getEntries() {
        return this.createEntries();
    }
    
    public ImmutableSet<K> getKeys() {
        return new PersistentHashMapKeys<K, Object>(this);
    }
    
    public final TrieNode<K, V> getNode$runtime_release() {
        return this.node;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public ImmutableCollection<V> getValues() {
        return new PersistentHashMapValues<Object, V>(this);
    }
    
    public final /* bridge */ ImmutableSet<K> keySet() {
        return this.getKeys();
    }
    
    public PersistentHashMap<K, V> put(final K k, final V v) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (k != null) {
            hashCode = k.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode.ModificationResult<K, V> put = node.put(hashCode, k, v, 0);
        if (put == null) {
            return this;
        }
        return new PersistentHashMap<K, V>(put.getNode(), this.size() + put.getSizeDelta());
    }
    
    public PersistentMap<K, V> putAll(final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "m");
        final PersistentMap persistentMap = this;
        Intrinsics.checkNotNull((Object)persistentMap, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate, V of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate>");
        final Builder builder = persistentMap.builder();
        builder.putAll(map);
        return builder.build();
    }
    
    public PersistentHashMap<K, V> remove(final K k) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (k != null) {
            hashCode = k.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode<K, V> remove = node.remove(hashCode, k, 0);
        if (this.node == remove) {
            return this;
        }
        if (remove == null) {
            return PersistentHashMap.Companion.emptyOf$runtime_release();
        }
        return new PersistentHashMap<K, V>((TrieNode<Object, Object>)remove, this.size() - 1);
    }
    
    public PersistentHashMap<K, V> remove(final K k, final V v) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (k != null) {
            hashCode = k.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode<K, V> remove = node.remove(hashCode, k, v, 0);
        if (this.node == remove) {
            return this;
        }
        if (remove == null) {
            return PersistentHashMap.Companion.emptyOf$runtime_release();
        }
        return new PersistentHashMap<K, V>((TrieNode<Object, Object>)remove, this.size() - 1);
    }
    
    public final /* bridge */ ImmutableCollection<V> values() {
        return this.getValues();
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0005\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J%\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\b0\u0004\"\u0004\b\u0002\u0010\u0007\"\u0004\b\u0003\u0010\bH\u0000¢\u0006\u0002\b\tR\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "", "emptyOf", "K", "V", "emptyOf$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <K, V> PersistentHashMap<K, V> emptyOf$runtime_release() {
            final PersistentHashMap<Object, Object> access$getEMPTY$cp = PersistentHashMap.access$getEMPTY$cp();
            Intrinsics.checkNotNull((Object)access$getEMPTY$cp, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap.Companion.emptyOf, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap.Companion.emptyOf>");
            return (PersistentHashMap<K, V>)access$getEMPTY$cp;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import java.util.NoSuchElementException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u000e\u0010\n\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u000bJ\r\u0010\f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000bR\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0088\u0004¢\u0006\u0004\n\u0002\u0010\t¨\u0006\r" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/BufferIterator;", "T", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractListIterator;", "buffer", "", "index", "", "size", "([Ljava/lang/Object;II)V", "[Ljava/lang/Object;", "next", "()Ljava/lang/Object;", "previous", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class BufferIterator<T> extends AbstractListIterator<T>
{
    private final T[] buffer;
    
    public BufferIterator(final T[] buffer, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)buffer, "buffer");
        super(n, n2);
        this.buffer = buffer;
    }
    
    @Override
    public T next() {
        if (this.hasNext()) {
            final T[] buffer = this.buffer;
            final int index = this.getIndex();
            this.setIndex(index + 1);
            return buffer[index];
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public T previous() {
        if (this.hasPrevious()) {
            final T[] buffer = this.buffer;
            this.setIndex(this.getIndex() - 1);
            return buffer[this.getIndex()];
        }
        throw new NoSuchElementException();
    }
}

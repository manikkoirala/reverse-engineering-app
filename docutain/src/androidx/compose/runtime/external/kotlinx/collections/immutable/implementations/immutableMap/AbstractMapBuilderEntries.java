// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.collections.AbstractMutableSet;
import java.util.Map;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010&\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\b \u0018\u0000*\u0014\b\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002*\u0004\b\u0001\u0010\u0003*\u0004\b\u0002\u0010\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010\nJ\u001c\u0010\u000b\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0002H&J\u0013\u0010\f\u001a\u00020\b2\u0006\u0010\t\u001a\u00028\u0000¢\u0006\u0002\u0010\nJ\u001c\u0010\r\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0002H&¨\u0006\u000e" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/AbstractMapBuilderEntries;", "E", "", "K", "V", "Lkotlin/collections/AbstractMutableSet;", "()V", "contains", "", "element", "(Ljava/util/Map$Entry;)Z", "containsEntry", "remove", "removeEntry", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class AbstractMapBuilderEntries<E extends Map.Entry<? extends K, ? extends V>, K, V> extends AbstractMutableSet<E>
{
    public final /* bridge */ boolean contains(final Object o) {
        return o instanceof Map.Entry && this.contains((E)o);
    }
    
    public final boolean contains(final E e) {
        Intrinsics.checkNotNullParameter((Object)e, "element");
        if (e instanceof Object) {
            final Map.Entry<? extends K, ? extends V> entry = e;
        }
        else {
            final Map.Entry<? extends K, ? extends V> entry = null;
        }
        Map.Entry<? extends K, ? extends V> entry;
        return entry instanceof Map.Entry && this.containsEntry(e);
    }
    
    public abstract boolean containsEntry(final Map.Entry<? extends K, ? extends V> p0);
    
    public final /* bridge */ boolean remove(final Object o) {
        return o instanceof Map.Entry && this.remove((E)o);
    }
    
    public final boolean remove(final E e) {
        Intrinsics.checkNotNullParameter((Object)e, "element");
        if (e instanceof Object) {
            final Map.Entry<? extends K, ? extends V> entry = e;
        }
        else {
            final Map.Entry<? extends K, ? extends V> entry = null;
        }
        Map.Entry<? extends K, ? extends V> entry;
        return entry instanceof Map.Entry && this.removeEntry(e);
    }
    
    public abstract boolean removeEntry(final Map.Entry<? extends K, ? extends V> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap;

import java.util.Collection;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableCollection;
import java.util.Set;
import java.util.Map;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableSet;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.EndOfChain;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.collections.AbstractMap;

@Metadata(d1 = { "\u0000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\b\u0004\b\u0000\u0018\u0000 3*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004:\u00013B3\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\u0018\u0010\b\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\n0\t¢\u0006\u0002\u0010\u000bJ\u0014\u0010 \u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010!H\u0016J\u0014\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0004H\u0016J\u0015\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010&J\u001a\u0010'\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000e0\rH\u0002J\u0018\u0010(\u001a\u0004\u0018\u00018\u00012\u0006\u0010%\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010)J\u001a\u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000e0*H\u0001J)\u0010+\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010%\u001a\u00028\u00002\u0006\u0010,\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010-J*\u0010.\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00042\u0014\u0010/\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u000100H\u0016J!\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010%\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00102J)\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010%\u001a\u00028\u00002\u0006\u0010,\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010-R&\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000e0\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R&\u0010\b\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\n0\tX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0010R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u0014\u0010\u0018\u001a\u00020\u00198VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00010\u001d8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001f¨\u00064" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;", "K", "V", "Lkotlin/collections/AbstractMap;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "firstKey", "", "lastKey", "hashMap", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/LinkedValue;", "(Ljava/lang/Object;Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;)V", "entries", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "", "getEntries", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "getFirstKey$runtime_release", "()Ljava/lang/Object;", "getHashMap$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "keys", "getKeys", "getLastKey$runtime_release", "size", "", "getSize", "()I", "values", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableCollection;", "getValues", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableCollection;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap$Builder;", "clear", "containsKey", "", "key", "(Ljava/lang/Object;)Z", "createEntries", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "", "put", "value", "(Ljava/lang/Object;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;", "putAll", "m", "", "remove", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentOrderedMap<K, V> extends AbstractMap<K, V> implements PersistentMap<K, V>
{
    public static final Companion Companion;
    private static final PersistentOrderedMap EMPTY;
    private final Object firstKey;
    private final PersistentHashMap<K, LinkedValue<V>> hashMap;
    private final Object lastKey;
    
    static {
        Companion = new Companion(null);
        EMPTY = new PersistentOrderedMap(EndOfChain.INSTANCE, EndOfChain.INSTANCE, PersistentHashMap.Companion.emptyOf$runtime_release());
    }
    
    public PersistentOrderedMap(final Object firstKey, final Object lastKey, final PersistentHashMap<K, LinkedValue<V>> hashMap) {
        Intrinsics.checkNotNullParameter((Object)hashMap, "hashMap");
        this.firstKey = firstKey;
        this.lastKey = lastKey;
        this.hashMap = hashMap;
    }
    
    public static final /* synthetic */ PersistentOrderedMap access$getEMPTY$cp() {
        return PersistentOrderedMap.EMPTY;
    }
    
    private final ImmutableSet<Entry<K, V>> createEntries() {
        return (ImmutableSet)new PersistentOrderedMapEntries((PersistentOrderedMap<Object, Object>)this);
    }
    
    public Builder<K, V> builder() {
        return (Builder)new PersistentOrderedMapBuilder(this);
    }
    
    public PersistentMap<K, V> clear() {
        return (PersistentMap)PersistentOrderedMap.Companion.emptyOf$runtime_release();
    }
    
    public boolean containsKey(final Object o) {
        return this.hashMap.containsKey(o);
    }
    
    public final /* bridge */ ImmutableSet<Entry<K, V>> entrySet() {
        return this.getEntries();
    }
    
    public V get(Object value) {
        final LinkedValue linkedValue = this.hashMap.get(value);
        if (linkedValue != null) {
            value = linkedValue.getValue();
        }
        else {
            value = null;
        }
        return (V)value;
    }
    
    public ImmutableSet<Entry<K, V>> getEntries() {
        return this.createEntries();
    }
    
    public final Set<Entry<K, V>> getEntries() {
        return this.createEntries();
    }
    
    public final Object getFirstKey$runtime_release() {
        return this.firstKey;
    }
    
    public final PersistentHashMap<K, LinkedValue<V>> getHashMap$runtime_release() {
        return this.hashMap;
    }
    
    public ImmutableSet<K> getKeys() {
        return new PersistentOrderedMapKeys<K, Object>(this);
    }
    
    public final Object getLastKey$runtime_release() {
        return this.lastKey;
    }
    
    public int getSize() {
        return this.hashMap.size();
    }
    
    public ImmutableCollection<V> getValues() {
        return new PersistentOrderedMapValues<Object, V>(this);
    }
    
    public final /* bridge */ ImmutableSet<K> keySet() {
        return this.getKeys();
    }
    
    public PersistentOrderedMap<K, V> put(final K k, final V v) {
        if (this.isEmpty()) {
            return new PersistentOrderedMap<K, V>(k, k, this.hashMap.put(k, new LinkedValue<V>(v)));
        }
        final LinkedValue linkedValue = this.hashMap.get(k);
        if (linkedValue == null) {
            final Object lastKey = this.lastKey;
            final LinkedValue<V> value = this.hashMap.get(lastKey);
            Intrinsics.checkNotNull((Object)value);
            return new PersistentOrderedMap<K, V>(this.firstKey, k, (PersistentHashMap<Object, LinkedValue<Object>>)this.hashMap.put((K)lastKey, value.withNext(k)).put(k, new LinkedValue<V>(v, lastKey)));
        }
        if (linkedValue.getValue() == v) {
            return this;
        }
        return new PersistentOrderedMap<K, V>(this.firstKey, this.lastKey, (PersistentHashMap<Object, LinkedValue<Object>>)this.hashMap.put(k, linkedValue.withValue(v)));
    }
    
    public PersistentMap<K, V> putAll(final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "m");
        final PersistentMap persistentMap = this;
        Intrinsics.checkNotNull((Object)persistentMap, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate, V of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate>");
        final Builder builder = persistentMap.builder();
        builder.putAll(map);
        return builder.build();
    }
    
    public PersistentOrderedMap<K, V> remove(final K k) {
        final LinkedValue linkedValue = this.hashMap.get(k);
        if (linkedValue == null) {
            return this;
        }
        PersistentMap<K, V> persistentMap;
        final PersistentHashMap<K, LinkedValue<V>> persistentHashMap = (PersistentHashMap<K, LinkedValue<V>>)(persistentMap = (PersistentMap<K, V>)this.hashMap.remove(k));
        if (linkedValue.getHasPrevious()) {
            final LinkedValue value = ((Map<K, LinkedValue>)persistentHashMap).get(linkedValue.getPrevious());
            Intrinsics.checkNotNull((Object)value);
            persistentMap = (PersistentMap<K, V>)persistentHashMap.put((K)linkedValue.getPrevious(), value.withNext(linkedValue.getNext()));
        }
        Object put = persistentMap;
        if (linkedValue.getHasNext()) {
            final LinkedValue value2 = ((Map<K, LinkedValue>)persistentMap).get(linkedValue.getNext());
            Intrinsics.checkNotNull((Object)value2);
            put = ((PersistentHashMap<Object, LinkedValue>)persistentMap).put(linkedValue.getNext(), value2.withPrevious(linkedValue.getPrevious()));
        }
        Object o;
        if (!linkedValue.getHasPrevious()) {
            o = linkedValue.getNext();
        }
        else {
            o = this.firstKey;
        }
        Object o2;
        if (!linkedValue.getHasNext()) {
            o2 = linkedValue.getPrevious();
        }
        else {
            o2 = this.lastKey;
        }
        return new PersistentOrderedMap<K, V>(o, o2, (PersistentHashMap<Object, LinkedValue<Object>>)put);
    }
    
    public PersistentOrderedMap<K, V> remove(final K k, final V v) {
        final LinkedValue linkedValue = this.hashMap.get(k);
        if (linkedValue == null) {
            return this;
        }
        PersistentOrderedMap<K, V> remove;
        if (Intrinsics.areEqual(linkedValue.getValue(), (Object)v)) {
            remove = this.remove(k);
        }
        else {
            remove = this;
        }
        return remove;
    }
    
    public final /* bridge */ ImmutableCollection<V> values() {
        return this.getValues();
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0005\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J%\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\b0\u0004\"\u0004\b\u0002\u0010\u0007\"\u0004\b\u0003\u0010\bH\u0000¢\u0006\u0002\b\tR\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMap;", "", "emptyOf", "K", "V", "emptyOf$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <K, V> PersistentOrderedMap<K, V> emptyOf$runtime_release() {
            final PersistentOrderedMap<Object, Object> access$getEMPTY$cp = PersistentOrderedMap.access$getEMPTY$cp();
            Intrinsics.checkNotNull((Object)access$getEMPTY$cp, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap.PersistentOrderedMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap.PersistentOrderedMap.Companion.emptyOf, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap.PersistentOrderedMap.Companion.emptyOf>");
            return (PersistentOrderedMap<K, V>)access$getEMPTY$cp;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet;

import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentSet;
import kotlin.collections.AbstractSet;

@Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010(\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 !*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001!B\u001b\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u001b\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fJ\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0016J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u0014H\u0016J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003H\u0016J\u0016\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000e\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0018J\u0016\u0010\u0019\u001a\u00020\u00172\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0016J\u000f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u001bH\u0096\u0002J\u001b\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fJ\"\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00170\u001fH\u0016J\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0016J\u001c\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\"" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSet;", "E", "Lkotlin/collections/AbstractSet;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "size", "", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;I)V", "getNode$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "getSize", "()I", "add", "element", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "addAll", "elements", "", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet$Builder;", "clear", "contains", "", "(Ljava/lang/Object;)Z", "containsAll", "iterator", "", "remove", "removeAll", "predicate", "Lkotlin/Function1;", "retainAll", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentHashSet<E> extends AbstractSet<E> implements PersistentSet<E>
{
    public static final Companion Companion;
    private static final PersistentHashSet EMPTY;
    private final TrieNode<E> node;
    private final int size;
    
    static {
        Companion = new Companion(null);
        EMPTY = new PersistentHashSet(TrieNode.Companion.getEMPTY$runtime_release(), 0);
    }
    
    public PersistentHashSet(final TrieNode<E> node, final int size) {
        Intrinsics.checkNotNullParameter((Object)node, "node");
        this.node = node;
        this.size = size;
    }
    
    public static final /* synthetic */ PersistentHashSet access$getEMPTY$cp() {
        return PersistentHashSet.EMPTY;
    }
    
    public PersistentSet<E> add(final E e) {
        final TrieNode<E> node = this.node;
        int hashCode;
        if (e != null) {
            hashCode = e.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode<E> add = node.add(hashCode, e, 0);
        if (this.node == add) {
            return this;
        }
        return new PersistentHashSet((TrieNode<Object>)add, this.size() + 1);
    }
    
    public PersistentSet<E> addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.addAll(collection);
        return builder.build();
    }
    
    public Builder<E> builder() {
        return (Builder)new PersistentHashSetBuilder(this);
    }
    
    public PersistentSet<E> clear() {
        return PersistentHashSet.Companion.emptyOf$runtime_release();
    }
    
    public boolean contains(final Object o) {
        final TrieNode<E> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.contains(hashCode, (E)o, 0);
    }
    
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        if (collection instanceof PersistentHashSet) {
            return this.node.containsAll(((PersistentHashSet)collection).node, 0);
        }
        if (collection instanceof PersistentHashSetBuilder) {
            return this.node.containsAll(((PersistentHashSetBuilder)collection).getNode$runtime_release(), 0);
        }
        return super.containsAll((Collection)collection);
    }
    
    public final TrieNode<E> getNode$runtime_release() {
        return this.node;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public Iterator<E> iterator() {
        return new PersistentHashSetIterator<E>(this.node);
    }
    
    public PersistentSet<E> remove(final E e) {
        final TrieNode<E> node = this.node;
        int hashCode;
        if (e != null) {
            hashCode = e.hashCode();
        }
        else {
            hashCode = 0;
        }
        final TrieNode<E> remove = node.remove(hashCode, e, 0);
        if (this.node == remove) {
            return this;
        }
        return new PersistentHashSet((TrieNode<Object>)remove, this.size() - 1);
    }
    
    public PersistentSet<E> removeAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.removeAll(collection);
        return builder.build();
    }
    
    public PersistentSet<E> removeAll(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final Builder<E> builder = this.builder();
        CollectionsKt.removeAll((Iterable)builder, (Function1)function1);
        return builder.build();
    }
    
    public PersistentSet<E> retainAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.retainAll(collection);
        return builder.build();
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\b0\u0007\"\u0004\b\u0001\u0010\bH\u0000¢\u0006\u0002\b\tR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSet$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSet;", "", "emptyOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "E", "emptyOf$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <E> PersistentSet<E> emptyOf$runtime_release() {
            return (PersistentSet)PersistentHashSet.access$getEMPTY$cp();
        }
    }
}

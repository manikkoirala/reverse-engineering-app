// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.DeltaCounter;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.MutabilityOwnership;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.collections.AbstractMutableMap;

@Metadata(d1 = { "\u0000\\\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0010'\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u001f\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010$\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004B\u0019\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006¢\u0006\u0002\u0010\u0007J\u0014\u0010.\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0016J\b\u0010/\u001a\u000200H\u0016J\u0015\u00101\u001a\u0002022\u0006\u00103\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00104J\u0018\u00105\u001a\u0004\u0018\u00018\u00012\u0006\u00103\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u00106J\u001f\u00107\u001a\u0004\u0018\u00018\u00012\u0006\u00103\u001a\u00028\u00002\u0006\u0010&\u001a\u00028\u0001H\u0016¢\u0006\u0002\u00108J\u001e\u00109\u001a\u0002002\u0014\u0010:\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010;H\u0016J\u0017\u0010<\u001a\u0004\u0018\u00018\u00012\u0006\u00103\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00106J\u001b\u0010<\u001a\u0002022\u0006\u00103\u001a\u00028\u00002\u0006\u0010&\u001a\u00028\u0001¢\u0006\u0002\u0010=R&\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\fR\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u00020\u0010X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R&\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0016X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001e\u0010\u001b\u001a\u0004\u0018\u00018\u0001X\u0080\u000e¢\u0006\u0010\n\u0002\u0010 \u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001e\u0010#\u001a\u00020\"2\u0006\u0010!\u001a\u00020\"@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R$\u0010'\u001a\u00020\u00102\u0006\u0010&\u001a\u00020\u0010@VX\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0012\"\u0004\b)\u0010\u0014R\u001a\u0010*\u001a\b\u0012\u0004\u0012\u00028\u00010+8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b,\u0010-¨\u0006>" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBuilder;", "K", "V", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap$Builder;", "Lkotlin/collections/AbstractMutableMap;", "map", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;)V", "entries", "", "", "getEntries", "()Ljava/util/Set;", "keys", "getKeys", "modCount", "", "getModCount$runtime_release", "()I", "setModCount$runtime_release", "(I)V", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "getNode$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "setNode$runtime_release", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;)V", "operationResult", "getOperationResult$runtime_release", "()Ljava/lang/Object;", "setOperationResult$runtime_release", "(Ljava/lang/Object;)V", "Ljava/lang/Object;", "<set-?>", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "ownership", "getOwnership$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "value", "size", "getSize", "setSize", "values", "", "getValues", "()Ljava/util/Collection;", "build", "clear", "", "containsKey", "", "key", "(Ljava/lang/Object;)Z", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "putAll", "from", "", "remove", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentHashMapBuilder<K, V> extends AbstractMutableMap<K, V> implements Builder<K, V>
{
    private PersistentHashMap<K, V> map;
    private int modCount;
    private TrieNode<K, V> node;
    private V operationResult;
    private MutabilityOwnership ownership;
    private int size;
    
    public PersistentHashMapBuilder(final PersistentHashMap<K, V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "map");
        this.map = map;
        this.ownership = new MutabilityOwnership();
        this.node = this.map.getNode$runtime_release();
        this.size = this.map.size();
    }
    
    public PersistentHashMap<K, V> build() {
        PersistentHashMap<K, V> map;
        if (this.node == this.map.getNode$runtime_release()) {
            map = this.map;
        }
        else {
            this.ownership = new MutabilityOwnership();
            map = new PersistentHashMap<K, V>(this.node, this.size());
        }
        return this.map = map;
    }
    
    public void clear() {
        final TrieNode empty$runtime_release = TrieNode.Companion.getEMPTY$runtime_release();
        Intrinsics.checkNotNull((Object)empty$runtime_release, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder>");
        this.node = empty$runtime_release;
        this.setSize(0);
    }
    
    public boolean containsKey(final Object o) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.containsKey(hashCode, (K)o, 0);
    }
    
    public V get(final Object o) {
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.get(hashCode, (K)o, 0);
    }
    
    public Set<Entry<K, V>> getEntries() {
        return (Set<Entry<K, V>>)new PersistentHashMapBuilderEntries((PersistentHashMapBuilder<Object, Object>)this);
    }
    
    public Set<K> getKeys() {
        return new PersistentHashMapBuilderKeys<K, Object>(this);
    }
    
    public final int getModCount$runtime_release() {
        return this.modCount;
    }
    
    public final TrieNode<K, V> getNode$runtime_release() {
        return this.node;
    }
    
    public final V getOperationResult$runtime_release() {
        return this.operationResult;
    }
    
    public final MutabilityOwnership getOwnership$runtime_release() {
        return this.ownership;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public Collection<V> getValues() {
        return new PersistentHashMapBuilderValues<Object, V>(this);
    }
    
    public V put(final K k, final V v) {
        this.operationResult = null;
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (k != null) {
            hashCode = k.hashCode();
        }
        else {
            hashCode = 0;
        }
        this.node = node.mutablePut(hashCode, k, v, 0, this);
        return this.operationResult;
    }
    
    public void putAll(final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "from");
        PersistentHashMap persistentHashMap;
        if (map instanceof PersistentHashMap) {
            persistentHashMap = (PersistentHashMap)map;
        }
        else {
            persistentHashMap = null;
        }
        PersistentHashMap build = persistentHashMap;
        if (persistentHashMap == null) {
            PersistentHashMapBuilder persistentHashMapBuilder;
            if (map instanceof PersistentHashMapBuilder) {
                persistentHashMapBuilder = (PersistentHashMapBuilder)map;
            }
            else {
                persistentHashMapBuilder = null;
            }
            if (persistentHashMapBuilder != null) {
                build = persistentHashMapBuilder.build();
            }
            else {
                build = null;
            }
        }
        if (build != null) {
            final DeltaCounter deltaCounter = new DeltaCounter(0, 1, null);
            final int size = this.size();
            final TrieNode<K, V> node = this.node;
            final TrieNode node$runtime_release = build.getNode$runtime_release();
            Intrinsics.checkNotNull((Object)node$runtime_release, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder>");
            this.node = node.mutablePutAll(node$runtime_release, 0, deltaCounter, this);
            final int size2 = build.size() + size - deltaCounter.getCount();
            if (size != size2) {
                this.setSize(size2);
            }
        }
        else {
            super.putAll((Map)map);
        }
    }
    
    public V remove(final Object o) {
        this.operationResult = null;
        final TrieNode<K, V> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        TrieNode<K, V> node2;
        if ((node2 = node.mutableRemove(hashCode, (K)o, 0, this)) == null) {
            node2 = TrieNode.Companion.getEMPTY$runtime_release();
            Intrinsics.checkNotNull((Object)node2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder>");
        }
        this.node = node2;
        return this.operationResult;
    }
    
    public final boolean remove(final Object o, final Object o2) {
        final int size = this.size();
        final TrieNode<K, V> node = this.node;
        boolean b = false;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        TrieNode<K, V> node2;
        if ((node2 = node.mutableRemove(hashCode, (K)o, (V)o2, 0, this)) == null) {
            node2 = TrieNode.Companion.getEMPTY$runtime_release();
            Intrinsics.checkNotNull((Object)node2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.TrieNode<K of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder, V of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder>");
        }
        this.node = node2;
        if (size != this.size()) {
            b = true;
        }
        return b;
    }
    
    public final void setModCount$runtime_release(final int modCount) {
        this.modCount = modCount;
    }
    
    public final void setNode$runtime_release(final TrieNode<K, V> node) {
        Intrinsics.checkNotNullParameter((Object)node, "<set-?>");
        this.node = node;
    }
    
    public final void setOperationResult$runtime_release(final V operationResult) {
        this.operationResult = operationResult;
    }
    
    public void setSize(final int size) {
        this.size = size;
        ++this.modCount;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.adapters;

import kotlin.jvm.internal.CollectionToArray;
import java.util.Comparator;
import java.util.function.UnaryOperator;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.List;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableList;

@Metadata(d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010*\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003¢\u0006\u0002\u0010\u0005J\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0000H\u0096\u0003¢\u0006\u0002\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0010H\u0096\u0001J\u0013\u0010\u0011\u001a\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0096\u0002J\u0016\u0010\u0014\u001a\u00028\u00002\u0006\u0010\u0015\u001a\u00020\u0007H\u0096\u0003¢\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u00020\u0007H\u0016J\u0016\u0010\u0018\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0000H\u0096\u0001¢\u0006\u0002\u0010\u0019J\t\u0010\u001a\u001a\u00020\u000bH\u0096\u0001J\u000f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u001cH\u0096\u0003J\u0016\u0010\u001d\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0000H\u0096\u0001¢\u0006\u0002\u0010\u0019J\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0096\u0001J\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001f2\u0006\u0010\u0015\u001a\u00020\u0007H\u0096\u0001J\u001e\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u0007H\u0016J\b\u0010#\u001a\u00020$H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u00020\u0007X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006%" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/adapters/ImmutableListAdapter;", "E", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableList;", "", "impl", "(Ljava/util/List;)V", "size", "", "getSize", "()I", "contains", "", "element", "(Ljava/lang/Object;)Z", "containsAll", "elements", "", "equals", "other", "", "get", "index", "(I)Ljava/lang/Object;", "hashCode", "indexOf", "(Ljava/lang/Object;)I", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "subList", "fromIndex", "toIndex", "toString", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ImmutableListAdapter<E> implements ImmutableList<E>, List<E>, KMappedMarker
{
    private final List<E> impl;
    
    public ImmutableListAdapter(final List<? extends E> impl) {
        Intrinsics.checkNotNullParameter((Object)impl, "impl");
        this.impl = (List<E>)impl;
    }
    
    @Override
    public void add(final int n, final E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean add(final E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean addAll(final int n, final Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean addAll(final Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.impl.contains(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.impl.containsAll(collection);
    }
    
    @Override
    public boolean equals(final Object obj) {
        return this.impl.equals(obj);
    }
    
    @Override
    public E get(final int n) {
        return this.impl.get(n);
    }
    
    public int getSize() {
        return this.impl.size();
    }
    
    @Override
    public int hashCode() {
        return this.impl.hashCode();
    }
    
    @Override
    public int indexOf(final Object o) {
        return this.impl.indexOf(o);
    }
    
    @Override
    public boolean isEmpty() {
        return this.impl.isEmpty();
    }
    
    @Override
    public Iterator<E> iterator() {
        return this.impl.iterator();
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        return this.impl.lastIndexOf(o);
    }
    
    @Override
    public ListIterator<E> listIterator() {
        return this.impl.listIterator();
    }
    
    @Override
    public ListIterator<E> listIterator(final int n) {
        return this.impl.listIterator(n);
    }
    
    @Override
    public E remove(final int n) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public void replaceAll(final UnaryOperator<E> unaryOperator) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public E set(final int n, final E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public void sort(final Comparator<? super E> comparator) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public ImmutableList<E> subList(final int n, final int n2) {
        return new ImmutableListAdapter(this.impl.subList(n, n2));
    }
    
    @Override
    public Object[] toArray() {
        return CollectionToArray.toArray((Collection)this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "array");
        return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
    }
    
    @Override
    public String toString() {
        return this.impl.toString();
    }
}

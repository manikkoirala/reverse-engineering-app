// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedSet;

import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.EndOfChain;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentSet;
import kotlin.collections.AbstractSet;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010(\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 '*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001'B-\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\nJ\u001b\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0016J\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019H\u0016J\u000e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u001bH\u0016J\u000e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003H\u0016J\u0016\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0015\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u001fJ\u000f\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000!H\u0096\u0002J\u001b\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0016J\"\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001e0%H\u0016J\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019H\u0016J\u001c\u0010&\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019H\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR \u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\bX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\fR\u0014\u0010\u0010\u001a\u00020\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013¨\u0006(" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSet;", "E", "Lkotlin/collections/AbstractSet;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "firstElement", "", "lastElement", "hashMap", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/Links;", "(Ljava/lang/Object;Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;)V", "getFirstElement$runtime_release", "()Ljava/lang/Object;", "getHashMap$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMap;", "getLastElement$runtime_release", "size", "", "getSize", "()I", "add", "element", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "addAll", "elements", "", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet$Builder;", "clear", "contains", "", "(Ljava/lang/Object;)Z", "iterator", "", "remove", "removeAll", "predicate", "Lkotlin/Function1;", "retainAll", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentOrderedSet<E> extends AbstractSet<E> implements PersistentSet<E>
{
    public static final Companion Companion;
    private static final PersistentOrderedSet EMPTY;
    private final Object firstElement;
    private final PersistentHashMap<E, Links> hashMap;
    private final Object lastElement;
    
    static {
        Companion = new Companion(null);
        EMPTY = new PersistentOrderedSet(EndOfChain.INSTANCE, EndOfChain.INSTANCE, PersistentHashMap.Companion.emptyOf$runtime_release());
    }
    
    public PersistentOrderedSet(final Object firstElement, final Object lastElement, final PersistentHashMap<E, Links> hashMap) {
        Intrinsics.checkNotNullParameter((Object)hashMap, "hashMap");
        this.firstElement = firstElement;
        this.lastElement = lastElement;
        this.hashMap = hashMap;
    }
    
    public static final /* synthetic */ PersistentOrderedSet access$getEMPTY$cp() {
        return PersistentOrderedSet.EMPTY;
    }
    
    public PersistentSet<E> add(final E e) {
        if (this.hashMap.containsKey(e)) {
            return this;
        }
        if (this.isEmpty()) {
            return new PersistentOrderedSet(e, e, (PersistentHashMap<Object, Links>)this.hashMap.put(e, new Links()));
        }
        final Object lastElement = this.lastElement;
        final Links value = this.hashMap.get(lastElement);
        Intrinsics.checkNotNull((Object)value);
        return new PersistentOrderedSet(this.firstElement, e, (PersistentHashMap<Object, Links>)this.hashMap.put((E)lastElement, value.withNext(e)).put(e, new Links(lastElement)));
    }
    
    public PersistentSet<E> addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.addAll(collection);
        return builder.build();
    }
    
    public Builder<E> builder() {
        return (Builder)new PersistentOrderedSetBuilder(this);
    }
    
    public PersistentSet<E> clear() {
        return PersistentOrderedSet.Companion.emptyOf$runtime_release();
    }
    
    public boolean contains(final Object o) {
        return this.hashMap.containsKey(o);
    }
    
    public final Object getFirstElement$runtime_release() {
        return this.firstElement;
    }
    
    public final PersistentHashMap<E, Links> getHashMap$runtime_release() {
        return this.hashMap;
    }
    
    public final Object getLastElement$runtime_release() {
        return this.lastElement;
    }
    
    public int getSize() {
        return this.hashMap.size();
    }
    
    public Iterator<E> iterator() {
        return new PersistentOrderedSetIterator<E>(this.firstElement, this.hashMap);
    }
    
    public PersistentSet<E> remove(final E e) {
        final Links links = this.hashMap.get(e);
        if (links == null) {
            return this;
        }
        PersistentHashMap<E, Links> persistentHashMap2;
        final PersistentHashMap<E, Links> persistentHashMap = persistentHashMap2 = this.hashMap.remove(e);
        if (links.getHasPrevious()) {
            final Object value = persistentHashMap.get(links.getPrevious());
            Intrinsics.checkNotNull(value);
            persistentHashMap2 = persistentHashMap.put((E)links.getPrevious(), ((Links)value).withNext(links.getNext()));
        }
        PersistentHashMap<E, Links> put = persistentHashMap2;
        if (links.getHasNext()) {
            final Object value2 = persistentHashMap2.get(links.getNext());
            Intrinsics.checkNotNull(value2);
            put = (PersistentHashMap<E, Links>)persistentHashMap2.put(links.getNext(), ((Links)value2).withPrevious(links.getPrevious()));
        }
        Object o;
        if (!links.getHasPrevious()) {
            o = links.getNext();
        }
        else {
            o = this.firstElement;
        }
        Object o2;
        if (!links.getHasNext()) {
            o2 = links.getPrevious();
        }
        else {
            o2 = this.lastElement;
        }
        return new PersistentOrderedSet(o, o2, (PersistentHashMap<Object, Links>)put);
    }
    
    public PersistentSet<E> removeAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.removeAll(collection);
        return builder.build();
    }
    
    public PersistentSet<E> removeAll(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final Builder<E> builder = this.builder();
        CollectionsKt.removeAll((Iterable)builder, (Function1)function1);
        return builder.build();
    }
    
    public PersistentSet<E> retainAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Builder<E> builder = this.builder();
        builder.retainAll(collection);
        return builder.build();
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\b0\u0007\"\u0004\b\u0001\u0010\bH\u0000¢\u0006\u0002\b\tR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSet$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedSet/PersistentOrderedSet;", "", "emptyOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "E", "emptyOf$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <E> PersistentSet<E> emptyOf$runtime_release() {
            return (PersistentSet)PersistentOrderedSet.access$getEMPTY$cp();
        }
    }
}

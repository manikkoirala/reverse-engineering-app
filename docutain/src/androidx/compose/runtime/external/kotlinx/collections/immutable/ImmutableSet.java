// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable;

import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Set;

@Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\b`\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0004\u00c0\u0006\u0001" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "E", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableCollection;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface ImmutableSet<E> extends Set<E>, ImmutableCollection<E>, KMappedMarker
{
}

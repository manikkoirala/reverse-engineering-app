// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet;

import java.util.NoSuchElementException;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Iterator;

@Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010(\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\u0002\u0010\u0005J\r\u0010\u0015\u001a\u00028\u0000H\u0004¢\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\t\u0010\u0006\u001a\u00020\u0007H\u0096\u0002J\u0010\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0002J\u000e\u0010\u001b\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0016R\u0018\u0010\u0006\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\b\n\u0000\u0012\u0004\b\b\u0010\tR \u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\f0\u000bX\u0084\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006\u001c" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSetIterator;", "E", "", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;)V", "hasNext", "", "getHasNext$annotations", "()V", "path", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNodeIterator;", "getPath", "()Ljava/util/List;", "pathLastIndex", "", "getPathLastIndex", "()I", "setPathLastIndex", "(I)V", "currentElement", "()Ljava/lang/Object;", "ensureNextElementIsReady", "", "moveToNextNodeWithData", "pathIndex", "next", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class PersistentHashSetIterator<E> implements Iterator<E>, KMappedMarker
{
    private boolean hasNext;
    private final List<TrieNodeIterator<E>> path;
    private int pathLastIndex;
    
    public PersistentHashSetIterator(final TrieNode<E> trieNode) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "node");
        final List mutableList = CollectionsKt.mutableListOf((Object[])new TrieNodeIterator[] { new TrieNodeIterator() });
        this.path = mutableList;
        this.hasNext = true;
        TrieNodeIterator.reset$default((TrieNodeIterator<Object>)mutableList.get(0), trieNode.getBuffer(), 0, 2, null);
        this.pathLastIndex = 0;
        this.ensureNextElementIsReady();
    }
    
    private final void ensureNextElementIsReady() {
        if (this.path.get(this.pathLastIndex).hasNextElement()) {
            return;
        }
        for (int pathLastIndex = this.pathLastIndex; -1 < pathLastIndex; --pathLastIndex) {
            final int moveToNextNodeWithData = this.moveToNextNodeWithData(pathLastIndex);
            int moveToNextNodeWithData2;
            if ((moveToNextNodeWithData2 = moveToNextNodeWithData) == -1) {
                moveToNextNodeWithData2 = moveToNextNodeWithData;
                if (this.path.get(pathLastIndex).hasNextCell()) {
                    this.path.get(pathLastIndex).moveToNextCell();
                    moveToNextNodeWithData2 = this.moveToNextNodeWithData(pathLastIndex);
                }
            }
            if (moveToNextNodeWithData2 != -1) {
                this.pathLastIndex = moveToNextNodeWithData2;
                return;
            }
            if (pathLastIndex > 0) {
                this.path.get(pathLastIndex - 1).moveToNextCell();
            }
            this.path.get(pathLastIndex).reset(TrieNode.Companion.getEMPTY$runtime_release().getBuffer(), 0);
        }
        this.hasNext = false;
    }
    
    private final int moveToNextNodeWithData(int n) {
        if (this.path.get(n).hasNextElement()) {
            return n;
        }
        if (this.path.get(n).hasNextNode()) {
            final TrieNode currentNode = this.path.get(n).currentNode();
            if (++n == this.path.size()) {
                this.path.add(new TrieNodeIterator<E>());
            }
            TrieNodeIterator.reset$default((TrieNodeIterator<Object>)this.path.get(n), currentNode.getBuffer(), 0, 2, null);
            return this.moveToNextNodeWithData(n);
        }
        return -1;
    }
    
    protected final E currentElement() {
        CommonFunctionsKt.assert(this.hasNext());
        return this.path.get(this.pathLastIndex).currentElement();
    }
    
    protected final List<TrieNodeIterator<E>> getPath() {
        return this.path;
    }
    
    protected final int getPathLastIndex() {
        return this.pathLastIndex;
    }
    
    @Override
    public boolean hasNext() {
        return this.hasNext;
    }
    
    @Override
    public E next() {
        if (this.hasNext) {
            final E nextElement = this.path.get(this.pathLastIndex).nextElement();
            this.ensureNextElementIsReady();
            return nextElement;
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    protected final void setPathLastIndex(final int pathLastIndex) {
        this.pathLastIndex = pathLastIndex;
    }
}

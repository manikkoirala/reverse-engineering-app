// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import kotlin.jvm.internal.Intrinsics;
import java.util.Arrays;
import kotlin.collections.ArraysKt;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u0018\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\u0001H\u0000\u001aE\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n\"\u0004\b\u0000\u0010\f\"\u0004\b\u0001\u0010\r*\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n2\u0006\u0010\u000e\u001a\u00020\u00012\u0006\u0010\u000f\u001a\u0002H\f2\u0006\u0010\u0010\u001a\u0002H\rH\u0002¢\u0006\u0002\u0010\u0011\u001a)\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n*\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n2\u0006\u0010\u000e\u001a\u00020\u0001H\u0002¢\u0006\u0002\u0010\u0013\u001a)\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n*\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n2\u0006\u0010\u0015\u001a\u00020\u0001H\u0002¢\u0006\u0002\u0010\u0013\u001aA\u0010\u0016\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n*\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n2\u0006\u0010\u000e\u001a\u00020\u00012\u0006\u0010\u0015\u001a\u00020\u00012\u000e\u0010\u0017\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u0018H\u0002¢\u0006\u0002\u0010\u0019\u001aM\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n\"\u0004\b\u0000\u0010\f\"\u0004\b\u0001\u0010\r*\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n2\u0006\u0010\u0015\u001a\u00020\u00012\u0006\u0010\u000e\u001a\u00020\u00012\u0006\u0010\u000f\u001a\u0002H\f2\u0006\u0010\u0010\u001a\u0002H\rH\u0002¢\u0006\u0002\u0010\u001b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000¨\u0006\u001c" }, d2 = { "ENTRY_SIZE", "", "LOG_MAX_BRANCHING_FACTOR", "MAX_BRANCHING_FACTOR", "MAX_BRANCHING_FACTOR_MINUS_ONE", "MAX_SHIFT", "indexSegment", "index", "shift", "insertEntryAtIndex", "", "", "K", "V", "keyIndex", "key", "value", "([Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;", "removeEntryAtIndex", "([Ljava/lang/Object;I)[Ljava/lang/Object;", "removeNodeAtIndex", "nodeIndex", "replaceEntryWithNode", "newNode", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "([Ljava/lang/Object;IILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;)[Ljava/lang/Object;", "replaceNodeWithEntry", "([Ljava/lang/Object;IILjava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class TrieNodeKt
{
    public static final int ENTRY_SIZE = 2;
    public static final int LOG_MAX_BRANCHING_FACTOR = 5;
    public static final int MAX_BRANCHING_FACTOR = 32;
    public static final int MAX_BRANCHING_FACTOR_MINUS_ONE = 31;
    public static final int MAX_SHIFT = 30;
    
    public static final int indexSegment(final int n, final int n2) {
        return n >> n2 & 0x1F;
    }
    
    private static final <K, V> Object[] insertEntryAtIndex(final Object[] array, final int n, final K k, final V v) {
        final Object[] array2 = new Object[array.length + 2];
        ArraysKt.copyInto$default(array, array2, 0, 0, n, 6, (Object)null);
        ArraysKt.copyInto(array, array2, n + 2, n, array.length);
        array2[n] = k;
        array2[n + 1] = v;
        return array2;
    }
    
    private static final Object[] removeEntryAtIndex(final Object[] array, final int n) {
        final Object[] array2 = new Object[array.length - 2];
        ArraysKt.copyInto$default(array, array2, 0, 0, n, 6, (Object)null);
        ArraysKt.copyInto(array, array2, n, n + 2, array.length);
        return array2;
    }
    
    private static final Object[] removeNodeAtIndex(final Object[] array, final int n) {
        final Object[] array2 = new Object[array.length - 1];
        ArraysKt.copyInto$default(array, array2, 0, 0, n, 6, (Object)null);
        ArraysKt.copyInto(array, array2, n, n + 1, array.length);
        return array2;
    }
    
    private static final Object[] replaceEntryWithNode(final Object[] array, final int n, final int n2, final TrieNode<?, ?> trieNode) {
        final int n3 = n2 - 2;
        final Object[] array2 = new Object[array.length - 2 + 1];
        ArraysKt.copyInto$default(array, array2, 0, 0, n, 6, (Object)null);
        ArraysKt.copyInto(array, array2, n, n + 2, n2);
        array2[n3] = trieNode;
        ArraysKt.copyInto(array, array2, n3 + 1, n2, array.length);
        return array2;
    }
    
    private static final <K, V> Object[] replaceNodeWithEntry(final Object[] original, final int n, final int n2, final K k, final V v) {
        final Object[] copy = Arrays.copyOf(original, original.length + 1);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        ArraysKt.copyInto(copy, copy, n + 2, n + 1, original.length);
        ArraysKt.copyInto(copy, copy, n2 + 2, n2, n);
        copy[n2] = k;
        copy[n2 + 1] = v;
        return copy;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet;

import kotlin.Unit;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.DeltaCounter;
import java.util.Arrays;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.MutabilityOwnership;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u001f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0018\b\u0000\u0018\u0000 _*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001_B\u001f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0006¢\u0006\u0002\u0010\u0007B'\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ)\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u0004¢\u0006\u0002\u0010\u001cJ#\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\u001fJ\b\u0010 \u001a\u00020\u0004H\u0002J\u001b\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001a\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\"J\u0015\u0010#\u001a\u00020$2\u0006\u0010\u001a\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010%J\u001b\u0010&\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001a\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\"J\u0016\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010(\u001a\u00020\u0004H\u0002J#\u0010)\u001a\u00020$2\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u0004¢\u0006\u0002\u0010*J\u001c\u0010+\u001a\u00020$2\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001b\u001a\u00020\u0004J\u0015\u0010-\u001a\u00028\u00002\u0006\u0010.\u001a\u00020\u0004H\u0002¢\u0006\u0002\u0010/J\u0016\u00100\u001a\u00020$2\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0002J\u0010\u00101\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J\u0015\u00102\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u0004H\u0000¢\u0006\u0002\b3JE\u00104\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u00105\u001a\u00020\u00042\u0006\u00106\u001a\u00028\u00002\u0006\u00107\u001a\u00020\u00042\u0006\u00108\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u00042\b\u00109\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0002\u0010:J=\u0010;\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010<\u001a\u00020\u00042\u0006\u0010=\u001a\u00020\u00042\u0006\u0010>\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u00042\b\u00109\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0002\u0010?J3\u0010@\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010<\u001a\u00020\u00042\u0006\u0010=\u001a\u00020\u00042\u0006\u0010>\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0002\u0010AJ5\u0010B\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u00042\n\u0010C\u001a\u0006\u0012\u0002\b\u00030D¢\u0006\u0002\u0010EJ6\u0010F\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010G\u001a\u00020H2\n\u0010C\u001a\u0006\u0012\u0002\b\u00030DJ+\u0010I\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u00109\u001a\u00020\tH\u0002¢\u0006\u0002\u0010JJ'\u0010K\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001a\u001a\u00028\u00002\n\u0010C\u001a\u0006\u0012\u0002\b\u00030DH\u0002¢\u0006\u0002\u0010LJ,\u0010M\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010G\u001a\u00020H2\u0006\u00109\u001a\u00020\tH\u0002J'\u0010N\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001a\u001a\u00028\u00002\n\u0010C\u001a\u0006\u0012\u0002\b\u00030DH\u0002¢\u0006\u0002\u0010LJ(\u0010O\u001a\u0004\u0018\u00010\u00022\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010G\u001a\u00020H2\u0006\u00109\u001a\u00020\tH\u0002J\u001e\u0010P\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010(\u001a\u00020\u00042\u0006\u00109\u001a\u00020\tH\u0002J(\u0010Q\u001a\u0004\u0018\u00010\u00022\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010G\u001a\u00020H2\u0006\u00109\u001a\u00020\tH\u0002J;\u0010R\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010<\u001a\u00020\u00042\u0006\u0010=\u001a\u00020\u00042\u0006\u0010>\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u00109\u001a\u00020\tH\u0002¢\u0006\u0002\u0010?J5\u0010S\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u00042\n\u0010C\u001a\u0006\u0012\u0002\b\u00030D¢\u0006\u0002\u0010EJ2\u0010T\u001a\u0004\u0018\u00010\u00022\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010G\u001a\u00020H2\n\u0010C\u001a\u0006\u0012\u0002\b\u00030DJ&\u0010U\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010V\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u00109\u001a\u00020\tH\u0002J2\u0010W\u001a\u0004\u0018\u00010\u00022\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010G\u001a\u00020H2\n\u0010C\u001a\u0006\u0012\u0002\b\u00030DJ,\u0010X\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010Y\u001a\u00020\u00042\f\u0010Z\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u00109\u001a\u00020\tH\u0002J\u0016\u0010[\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010.\u001a\u00020\u0004H\u0002J)\u0010\\\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u0004¢\u0006\u0002\u0010\u001cJ\u001e\u0010]\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010V\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J$\u0010^\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010Y\u001a\u00020\u00042\f\u0010Z\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR$\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0006X\u0086\u000e¢\u0006\u0010\n\u0002\u0010\u0013\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017¨\u0006`" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "E", "", "bitmap", "", "buffer", "", "(I[Ljava/lang/Object;)V", "ownedBy", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "(I[Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)V", "getBitmap", "()I", "setBitmap", "(I)V", "getBuffer", "()[Ljava/lang/Object;", "setBuffer", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "getOwnedBy", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "setOwnedBy", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)V", "add", "elementHash", "element", "shift", "(ILjava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "addElementAt", "positionMask", "(ILjava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "calculateSize", "collisionAdd", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "collisionContainsElement", "", "(Ljava/lang/Object;)Z", "collisionRemove", "collisionRemoveElementAtIndex", "i", "contains", "(ILjava/lang/Object;I)Z", "containsAll", "otherNode", "elementAtIndex", "index", "(I)Ljava/lang/Object;", "elementsIdentityEquals", "hasNoCellAt", "indexOfCellAt", "indexOfCellAt$runtime_release", "makeNode", "elementHash1", "element1", "elementHash2", "element2", "owner", "(ILjava/lang/Object;ILjava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "makeNodeAtIndex", "elementIndex", "newElementHash", "newElement", "(IILjava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "moveElementToNode", "(IILjava/lang/Object;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "mutableAdd", "mutator", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSetBuilder;", "(ILjava/lang/Object;ILandroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSetBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "mutableAddAll", "intersectionSizeRef", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/DeltaCounter;", "mutableAddElementAt", "(ILjava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "mutableCollisionAdd", "(Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSetBuilder;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "mutableCollisionAddAll", "mutableCollisionRemove", "mutableCollisionRemoveAll", "mutableCollisionRemoveElementAtIndex", "mutableCollisionRetainAll", "mutableMoveElementToNode", "mutableRemove", "mutableRemoveAll", "mutableRemoveCellAtIndex", "cellIndex", "mutableRetainAll", "mutableUpdateNodeAtIndex", "nodeIndex", "newNode", "nodeAtIndex", "remove", "removeCellAtIndex", "updateNodeAtIndex", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TrieNode<E>
{
    public static final Companion Companion;
    private static final TrieNode EMPTY;
    private int bitmap;
    private Object[] buffer;
    private MutabilityOwnership ownedBy;
    
    static {
        Companion = new Companion(null);
        EMPTY = new TrieNode(0, new Object[0]);
    }
    
    public TrieNode(final int n, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "buffer");
        this(n, array, null);
    }
    
    public TrieNode(final int bitmap, final Object[] buffer, final MutabilityOwnership ownedBy) {
        Intrinsics.checkNotNullParameter((Object)buffer, "buffer");
        this.bitmap = bitmap;
        this.buffer = buffer;
        this.ownedBy = ownedBy;
    }
    
    public static final /* synthetic */ TrieNode access$getEMPTY$cp() {
        return TrieNode.EMPTY;
    }
    
    private final TrieNode<E> addElementAt(final int n, final E e) {
        return new TrieNode<E>(n | this.bitmap, TrieNodeKt.access$addElementAtIndex(this.buffer, this.indexOfCellAt$runtime_release(n), e));
    }
    
    private final int calculateSize() {
        if (this.bitmap == 0) {
            return this.buffer.length;
        }
        final Object[] buffer = this.buffer;
        final int length = buffer.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final Object o = buffer[i];
            int calculateSize;
            if (o instanceof TrieNode) {
                calculateSize = ((TrieNode)o).calculateSize();
            }
            else {
                calculateSize = 1;
            }
            n += calculateSize;
            ++i;
        }
        return n;
    }
    
    private final TrieNode<E> collisionAdd(final E e) {
        if (this.collisionContainsElement(e)) {
            return this;
        }
        return new TrieNode<E>(0, TrieNodeKt.access$addElementAtIndex(this.buffer, 0, e));
    }
    
    private final boolean collisionContainsElement(final E e) {
        return ArraysKt.contains(this.buffer, (Object)e);
    }
    
    private final TrieNode<E> collisionRemove(final E e) {
        final int index = ArraysKt.indexOf(this.buffer, (Object)e);
        if (index != -1) {
            return this.collisionRemoveElementAtIndex(index);
        }
        return this;
    }
    
    private final TrieNode<E> collisionRemoveElementAtIndex(final int n) {
        return new TrieNode<E>(0, TrieNodeKt.access$removeCellAtIndex(this.buffer, n));
    }
    
    private final E elementAtIndex(final int n) {
        return (E)this.buffer[n];
    }
    
    private final boolean elementsIdentityEquals(final TrieNode<E> trieNode) {
        if (this == trieNode) {
            return true;
        }
        if (this.bitmap != trieNode.bitmap) {
            return false;
        }
        for (int length = this.buffer.length, i = 0; i < length; ++i) {
            if (this.buffer[i] != trieNode.buffer[i]) {
                return false;
            }
        }
        return true;
    }
    
    private final boolean hasNoCellAt(final int n) {
        return (n & this.bitmap) == 0x0;
    }
    
    private final TrieNode<E> makeNode(final int n, final E e, final int n2, final E e2, final int n3, final MutabilityOwnership mutabilityOwnership) {
        if (n3 > 30) {
            return new TrieNode<E>(0, new Object[] { e, e2 }, mutabilityOwnership);
        }
        final int indexSegment = TrieNodeKt.indexSegment(n, n3);
        final int indexSegment2 = TrieNodeKt.indexSegment(n2, n3);
        if (indexSegment != indexSegment2) {
            Object[] array;
            if (indexSegment < indexSegment2) {
                array = new Object[] { e, e2 };
            }
            else {
                array = new Object[] { e2, e };
            }
            return new TrieNode<E>(1 << indexSegment | 1 << indexSegment2, array, mutabilityOwnership);
        }
        return new TrieNode<E>(1 << indexSegment, new Object[] { this.makeNode(n, e, n2, e2, n3 + 5, mutabilityOwnership) }, mutabilityOwnership);
    }
    
    private final TrieNode<E> makeNodeAtIndex(int hashCode, final int n, final E e, final int n2, final MutabilityOwnership mutabilityOwnership) {
        final E elementAtIndex = this.elementAtIndex(hashCode);
        if (elementAtIndex != null) {
            hashCode = elementAtIndex.hashCode();
        }
        else {
            hashCode = 0;
        }
        return this.makeNode(hashCode, elementAtIndex, n, e, n2 + 5, mutabilityOwnership);
    }
    
    private final TrieNode<E> moveElementToNode(final int n, final int n2, final E e, final int n3) {
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = this.makeNodeAtIndex(n, n2, e, n3, null);
        return new TrieNode<E>(this.bitmap, copy);
    }
    
    private final TrieNode<E> mutableAddElementAt(final int n, final E e, final MutabilityOwnership mutabilityOwnership) {
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n);
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = TrieNodeKt.access$addElementAtIndex(this.buffer, indexOfCellAt$runtime_release, e);
            this.bitmap |= n;
            return this;
        }
        return new TrieNode<E>(n | this.bitmap, TrieNodeKt.access$addElementAtIndex(this.buffer, indexOfCellAt$runtime_release, e), mutabilityOwnership);
    }
    
    private final TrieNode<E> mutableCollisionAdd(final E e, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        if (this.collisionContainsElement(e)) {
            return this;
        }
        persistentHashSetBuilder.setSize(persistentHashSetBuilder.size() + 1);
        if (this.ownedBy == persistentHashSetBuilder.getOwnership$runtime_release()) {
            this.buffer = TrieNodeKt.access$addElementAtIndex(this.buffer, 0, e);
            return this;
        }
        return new TrieNode<E>(0, TrieNodeKt.access$addElementAtIndex(this.buffer, 0, e), persistentHashSetBuilder.getOwnership$runtime_release());
    }
    
    private final TrieNode<E> mutableCollisionAddAll(final TrieNode<E> trieNode, final DeltaCounter deltaCounter, final MutabilityOwnership mutabilityOwnership) {
        if (this == trieNode) {
            deltaCounter.plusAssign(this.buffer.length);
            return this;
        }
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length + trieNode.buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        final Object[] buffer2 = trieNode.buffer;
        final int length = this.buffer.length;
        int i = 0;
        int n = 0;
        while (i < buffer2.length) {
            final boolean b = true;
            CommonFunctionsKt.assert(n <= i);
            int n2 = n;
            if (this.collisionContainsElement(buffer2[i]) ^ true) {
                copy[length + n] = buffer2[i];
                n2 = n + 1;
                CommonFunctionsKt.assert(length + n2 <= copy.length && b);
            }
            ++i;
            n = n2;
        }
        final int newLength = n + this.buffer.length;
        deltaCounter.plusAssign(copy.length - newLength);
        if (newLength == this.buffer.length) {
            return this;
        }
        if (newLength == trieNode.buffer.length) {
            return trieNode;
        }
        Object[] copy2;
        if (newLength == copy.length) {
            copy2 = copy;
        }
        else {
            copy2 = Arrays.copyOf(copy, newLength);
            Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
        }
        TrieNode trieNode2;
        if (Intrinsics.areEqual((Object)this.ownedBy, (Object)mutabilityOwnership)) {
            this.buffer = copy2;
            trieNode2 = this;
        }
        else {
            trieNode2 = new TrieNode(0, copy2, mutabilityOwnership);
        }
        return trieNode2;
    }
    
    private final TrieNode<E> mutableCollisionRemove(final E e, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        final int index = ArraysKt.indexOf(this.buffer, (Object)e);
        if (index != -1) {
            persistentHashSetBuilder.setSize(persistentHashSetBuilder.size() - 1);
            return this.mutableCollisionRemoveElementAtIndex(index, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        return this;
    }
    
    private final Object mutableCollisionRemoveAll(final TrieNode<E> trieNode, final DeltaCounter deltaCounter, final MutabilityOwnership mutabilityOwnership) {
        if (this == trieNode) {
            deltaCounter.plusAssign(this.buffer.length);
            return TrieNode.EMPTY;
        }
        Object[] buffer;
        if (Intrinsics.areEqual((Object)mutabilityOwnership, (Object)this.ownedBy)) {
            buffer = this.buffer;
        }
        else {
            buffer = new Object[this.buffer.length];
        }
        final Object[] buffer2 = this.buffer;
        int n = 0;
        int newLength = 0;
        while (true) {
            final int length = buffer2.length;
            final boolean b = true;
            if (n >= length) {
                break;
            }
            CommonFunctionsKt.assert(newLength <= n);
            int n2 = newLength;
            if (trieNode.collisionContainsElement(buffer2[n]) ^ true) {
                buffer[0 + newLength] = buffer2[n];
                n2 = newLength + 1;
                CommonFunctionsKt.assert(0 + n2 <= buffer.length && b);
            }
            ++n;
            newLength = n2;
        }
        deltaCounter.plusAssign(this.buffer.length - newLength);
        Object empty;
        if (newLength == 0) {
            empty = TrieNode.EMPTY;
        }
        else if (newLength == 1) {
            empty = buffer[0];
        }
        else if (newLength == this.buffer.length) {
            empty = this;
        }
        else if (newLength == buffer.length) {
            empty = new TrieNode(0, buffer, mutabilityOwnership);
        }
        else {
            final Object[] copy = Arrays.copyOf(buffer, newLength);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            empty = new TrieNode(0, copy, mutabilityOwnership);
        }
        return empty;
    }
    
    private final TrieNode<E> mutableCollisionRemoveElementAtIndex(final int n, final MutabilityOwnership mutabilityOwnership) {
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = TrieNodeKt.access$removeCellAtIndex(this.buffer, n);
            return this;
        }
        return new TrieNode<E>(0, TrieNodeKt.access$removeCellAtIndex(this.buffer, n), mutabilityOwnership);
    }
    
    private final Object mutableCollisionRetainAll(TrieNode<E> empty, final DeltaCounter deltaCounter, final MutabilityOwnership mutabilityOwnership) {
        if (this == empty) {
            deltaCounter.plusAssign(this.buffer.length);
            return this;
        }
        Object[] buffer;
        if (Intrinsics.areEqual((Object)mutabilityOwnership, (Object)this.ownedBy)) {
            buffer = this.buffer;
        }
        else {
            buffer = new Object[Math.min(this.buffer.length, ((TrieNode)empty).buffer.length)];
        }
        final Object[] buffer2 = this.buffer;
        int n = 0;
        int newLength = 0;
        while (true) {
            final int length = buffer2.length;
            final boolean b = true;
            if (n >= length) {
                break;
            }
            CommonFunctionsKt.assert(newLength <= n);
            int n2 = newLength;
            if (((TrieNode<Object>)empty).collisionContainsElement(buffer2[n])) {
                buffer[0 + newLength] = buffer2[n];
                n2 = newLength + 1;
                CommonFunctionsKt.assert(0 + n2 <= buffer.length && b);
            }
            ++n;
            newLength = n2;
        }
        deltaCounter.plusAssign(newLength);
        if (newLength == 0) {
            empty = TrieNode.EMPTY;
        }
        else if (newLength == 1) {
            empty = buffer[0];
        }
        else if (newLength == this.buffer.length) {
            empty = this;
        }
        else if (newLength != ((TrieNode)empty).buffer.length) {
            if (newLength == buffer.length) {
                empty = new TrieNode<Object>(0, buffer, mutabilityOwnership);
            }
            else {
                final Object[] copy = Arrays.copyOf(buffer, newLength);
                Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
                empty = new TrieNode<Object>(0, copy, mutabilityOwnership);
            }
        }
        return empty;
    }
    
    private final TrieNode<E> mutableMoveElementToNode(final int n, final int n2, final E e, final int n3, final MutabilityOwnership mutabilityOwnership) {
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer[n] = this.makeNodeAtIndex(n, n2, e, n3, mutabilityOwnership);
            return this;
        }
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = this.makeNodeAtIndex(n, n2, e, n3, mutabilityOwnership);
        return new TrieNode<E>(this.bitmap, copy, mutabilityOwnership);
    }
    
    private final TrieNode<E> mutableRemoveCellAtIndex(final int n, final int n2, final MutabilityOwnership mutabilityOwnership) {
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer = TrieNodeKt.access$removeCellAtIndex(this.buffer, n);
            this.bitmap ^= n2;
            return this;
        }
        return new TrieNode<E>(n2 ^ this.bitmap, TrieNodeKt.access$removeCellAtIndex(this.buffer, n), mutabilityOwnership);
    }
    
    private final TrieNode<E> mutableUpdateNodeAtIndex(final int n, final TrieNode<E> trieNode, final MutabilityOwnership mutabilityOwnership) {
        final Object[] buffer = trieNode.buffer;
        TrieNode<E> trieNode2 = trieNode;
        if (buffer.length == 1) {
            final Object o = buffer[0];
            trieNode2 = trieNode;
            if (!(o instanceof TrieNode)) {
                if (this.buffer.length == 1) {
                    trieNode.bitmap = this.bitmap;
                    return trieNode;
                }
                trieNode2 = (TrieNode<E>)o;
            }
        }
        if (this.ownedBy == mutabilityOwnership) {
            this.buffer[n] = trieNode2;
            return this;
        }
        final Object[] buffer2 = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer2, buffer2.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = trieNode2;
        return new TrieNode<E>(this.bitmap, copy, mutabilityOwnership);
    }
    
    private final TrieNode<E> nodeAtIndex(final int n) {
        final Object o = this.buffer[n];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode>");
        return (TrieNode<E>)o;
    }
    
    private final TrieNode<E> removeCellAtIndex(final int n, final int n2) {
        return new TrieNode<E>(n2 ^ this.bitmap, TrieNodeKt.access$removeCellAtIndex(this.buffer, n));
    }
    
    private final TrieNode<E> updateNodeAtIndex(final int n, final TrieNode<E> trieNode) {
        final Object[] buffer = trieNode.buffer;
        TrieNode<E> trieNode2 = trieNode;
        if (buffer.length == 1) {
            final Object o = buffer[0];
            trieNode2 = trieNode;
            if (!(o instanceof TrieNode)) {
                if (this.buffer.length == 1) {
                    trieNode.bitmap = this.bitmap;
                    return trieNode;
                }
                trieNode2 = (TrieNode<E>)o;
            }
        }
        final Object[] buffer2 = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer2, buffer2.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = trieNode2;
        return new TrieNode<E>(this.bitmap, copy);
    }
    
    public final TrieNode<E> add(final int n, final E e, final int n2) {
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasNoCellAt(n3)) {
            return this.addElementAt(n3, e);
        }
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n3);
        final Object o = this.buffer[indexOfCellAt$runtime_release];
        if (o instanceof TrieNode) {
            final TrieNode<E> nodeAtIndex = this.nodeAtIndex(indexOfCellAt$runtime_release);
            TrieNode<Object> trieNode;
            if (n2 == 30) {
                trieNode = nodeAtIndex.collisionAdd((Object)e);
            }
            else {
                trieNode = nodeAtIndex.add(n, (Object)e, n2 + 5);
            }
            if (nodeAtIndex == trieNode) {
                return this;
            }
            return this.updateNodeAtIndex(indexOfCellAt$runtime_release, (TrieNode<E>)trieNode);
        }
        else {
            if (Intrinsics.areEqual((Object)e, o)) {
                return this;
            }
            return this.moveElementToNode(indexOfCellAt$runtime_release, n, e, n2);
        }
    }
    
    public final boolean contains(final int n, final E e, final int n2) {
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasNoCellAt(n3)) {
            return false;
        }
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n3);
        final Object o = this.buffer[indexOfCellAt$runtime_release];
        if (!(o instanceof TrieNode)) {
            return Intrinsics.areEqual((Object)e, o);
        }
        final TrieNode<E> nodeAtIndex = this.nodeAtIndex(indexOfCellAt$runtime_release);
        if (n2 == 30) {
            return nodeAtIndex.collisionContainsElement((Object)e);
        }
        return nodeAtIndex.contains(n, (Object)e, n2 + 5);
    }
    
    public final boolean containsAll(final TrieNode<E> trieNode, int n) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "otherNode");
        final boolean b = true;
        if (this == trieNode) {
            return true;
        }
        if (n > 30) {
            final Object[] buffer = trieNode.buffer;
            final int length = buffer.length;
            n = 0;
            boolean b2;
            while (true) {
                b2 = b;
                if (n >= length) {
                    break;
                }
                if (!ArraysKt.contains(this.buffer, buffer[n])) {
                    b2 = false;
                    break;
                }
                ++n;
            }
            return b2;
        }
        final int bitmap = this.bitmap;
        final int bitmap2 = trieNode.bitmap;
        int i;
        if ((i = (bitmap & bitmap2)) != bitmap2) {
            return false;
        }
        while (i != 0) {
            final int lowestOneBit = Integer.lowestOneBit(i);
            final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(lowestOneBit);
            final int indexOfCellAt$runtime_release2 = trieNode.indexOfCellAt$runtime_release(lowestOneBit);
            final Object o = this.buffer[indexOfCellAt$runtime_release];
            final Object o2 = trieNode.buffer[indexOfCellAt$runtime_release2];
            final boolean b3 = o instanceof TrieNode;
            final boolean b4 = o2 instanceof TrieNode;
            if (b3 && b4) {
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.containsAll$lambda$13>");
                final TrieNode trieNode2 = (TrieNode)o;
                Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.containsAll$lambda$13>");
                if (!trieNode2.containsAll((TrieNode)o2, n + 5)) {
                    return false;
                }
            }
            else if (b3) {
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.containsAll$lambda$13>");
                final TrieNode trieNode3 = (TrieNode)o;
                int hashCode;
                if (o2 != null) {
                    hashCode = o2.hashCode();
                }
                else {
                    hashCode = 0;
                }
                if (!trieNode3.contains(hashCode, o2, n + 5)) {
                    return false;
                }
            }
            else {
                if (b4) {
                    return false;
                }
                if (!Intrinsics.areEqual(o, o2)) {
                    return false;
                }
            }
            i ^= lowestOneBit;
        }
        return true;
    }
    
    public final int getBitmap() {
        return this.bitmap;
    }
    
    public final Object[] getBuffer() {
        return this.buffer;
    }
    
    public final MutabilityOwnership getOwnedBy() {
        return this.ownedBy;
    }
    
    public final int indexOfCellAt$runtime_release(final int n) {
        return Integer.bitCount(n - 1 & this.bitmap);
    }
    
    public final TrieNode<E> mutableAdd(final int n, final E e, final int n2, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        Intrinsics.checkNotNullParameter((Object)persistentHashSetBuilder, "mutator");
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasNoCellAt(n3)) {
            persistentHashSetBuilder.setSize(persistentHashSetBuilder.size() + 1);
            return this.mutableAddElementAt(n3, e, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n3);
        final Object o = this.buffer[indexOfCellAt$runtime_release];
        if (o instanceof TrieNode) {
            final TrieNode<E> nodeAtIndex = this.nodeAtIndex(indexOfCellAt$runtime_release);
            TrieNode<Object> trieNode;
            if (n2 == 30) {
                trieNode = nodeAtIndex.mutableCollisionAdd((Object)e, persistentHashSetBuilder);
            }
            else {
                trieNode = nodeAtIndex.mutableAdd(n, (Object)e, n2 + 5, persistentHashSetBuilder);
            }
            if (nodeAtIndex == trieNode) {
                return this;
            }
            return this.mutableUpdateNodeAtIndex(indexOfCellAt$runtime_release, (TrieNode<E>)trieNode, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        else {
            if (Intrinsics.areEqual((Object)e, o)) {
                return this;
            }
            persistentHashSetBuilder.setSize(persistentHashSetBuilder.size() + 1);
            return this.mutableMoveElementToNode(indexOfCellAt$runtime_release, n, e, n2, persistentHashSetBuilder.getOwnership$runtime_release());
        }
    }
    
    public final TrieNode<E> mutableAddAll(final TrieNode<E> trieNode, final int n, final DeltaCounter deltaCounter, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "otherNode");
        Intrinsics.checkNotNullParameter((Object)deltaCounter, "intersectionSizeRef");
        Intrinsics.checkNotNullParameter((Object)persistentHashSetBuilder, "mutator");
        if (this == trieNode) {
            deltaCounter.setCount(deltaCounter.getCount() + this.calculateSize());
            return this;
        }
        if (n > 30) {
            return this.mutableCollisionAddAll(trieNode, deltaCounter, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        final int bitmap = this.bitmap;
        int i = trieNode.bitmap | bitmap;
        TrieNode trieNode2;
        if (i == bitmap && Intrinsics.areEqual((Object)this.ownedBy, (Object)persistentHashSetBuilder.getOwnership$runtime_release())) {
            trieNode2 = this;
        }
        else {
            trieNode2 = new TrieNode(i, new Object[Integer.bitCount(i)], persistentHashSetBuilder.getOwnership$runtime_release());
        }
        int n2 = 0;
        while (i != 0) {
            final int lowestOneBit = Integer.lowestOneBit(i);
            final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(lowestOneBit);
            final int indexOfCellAt$runtime_release2 = trieNode.indexOfCellAt$runtime_release(lowestOneBit);
            final Object[] buffer = trieNode2.buffer;
            Object o;
            if (this.hasNoCellAt(lowestOneBit)) {
                o = trieNode.buffer[indexOfCellAt$runtime_release2];
            }
            else if (trieNode.hasNoCellAt(lowestOneBit)) {
                o = this.buffer[indexOfCellAt$runtime_release];
            }
            else {
                o = this.buffer[indexOfCellAt$runtime_release];
                final Object o2 = trieNode.buffer[indexOfCellAt$runtime_release2];
                final boolean b = o instanceof TrieNode;
                final boolean b2 = o2 instanceof TrieNode;
                if (b && b2) {
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableAddAll$lambda$6>");
                    final TrieNode trieNode3 = (TrieNode)o;
                    Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableAddAll$lambda$6>");
                    o = trieNode3.mutableAddAll((TrieNode)o2, n + 5, deltaCounter, persistentHashSetBuilder);
                }
                else if (b) {
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableAddAll$lambda$6>");
                    final TrieNode trieNode4 = (TrieNode)o;
                    final int size = persistentHashSetBuilder.size();
                    int hashCode;
                    if (o2 != null) {
                        hashCode = o2.hashCode();
                    }
                    else {
                        hashCode = 0;
                    }
                    o = trieNode4.mutableAdd(hashCode, o2, n + 5, persistentHashSetBuilder);
                    if (persistentHashSetBuilder.size() == size) {
                        deltaCounter.setCount(deltaCounter.getCount() + 1);
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                else if (b2) {
                    Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableAddAll$lambda$6>");
                    final TrieNode trieNode5 = (TrieNode)o2;
                    final int size2 = persistentHashSetBuilder.size();
                    int hashCode2;
                    if (o != null) {
                        hashCode2 = o.hashCode();
                    }
                    else {
                        hashCode2 = 0;
                    }
                    o = trieNode5.mutableAdd(hashCode2, o, n + 5, persistentHashSetBuilder);
                    if (persistentHashSetBuilder.size() == size2) {
                        deltaCounter.setCount(deltaCounter.getCount() + 1);
                    }
                    final Unit instance2 = Unit.INSTANCE;
                }
                else if (Intrinsics.areEqual(o, o2)) {
                    deltaCounter.setCount(deltaCounter.getCount() + 1);
                    final Unit instance3 = Unit.INSTANCE;
                }
                else {
                    int hashCode3;
                    if (o != null) {
                        hashCode3 = o.hashCode();
                    }
                    else {
                        hashCode3 = 0;
                    }
                    int hashCode4;
                    if (o2 != null) {
                        hashCode4 = o2.hashCode();
                    }
                    else {
                        hashCode4 = 0;
                    }
                    o = this.makeNode(hashCode3, (E)o, hashCode4, (E)o2, n + 5, persistentHashSetBuilder.getOwnership$runtime_release());
                }
            }
            buffer[n2] = o;
            ++n2;
            i ^= lowestOneBit;
        }
        TrieNode trieNode6;
        if (this.elementsIdentityEquals(trieNode2)) {
            trieNode6 = this;
        }
        else {
            trieNode6 = trieNode2;
            if (trieNode.elementsIdentityEquals(trieNode2)) {
                trieNode6 = trieNode;
            }
        }
        return trieNode6;
    }
    
    public final TrieNode<E> mutableRemove(final int n, final E e, final int n2, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        Intrinsics.checkNotNullParameter((Object)persistentHashSetBuilder, "mutator");
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasNoCellAt(n3)) {
            return this;
        }
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n3);
        final Object o = this.buffer[indexOfCellAt$runtime_release];
        if (o instanceof TrieNode) {
            final TrieNode<E> nodeAtIndex = this.nodeAtIndex(indexOfCellAt$runtime_release);
            TrieNode<Object> trieNode;
            if (n2 == 30) {
                trieNode = nodeAtIndex.mutableCollisionRemove((Object)e, persistentHashSetBuilder);
            }
            else {
                trieNode = nodeAtIndex.mutableRemove(n, (Object)e, n2 + 5, persistentHashSetBuilder);
            }
            if (this.ownedBy != persistentHashSetBuilder.getOwnership$runtime_release() && nodeAtIndex == trieNode) {
                return this;
            }
            return this.mutableUpdateNodeAtIndex(indexOfCellAt$runtime_release, (TrieNode<E>)trieNode, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        else {
            if (Intrinsics.areEqual((Object)e, o)) {
                persistentHashSetBuilder.setSize(persistentHashSetBuilder.size() - 1);
                return this.mutableRemoveCellAtIndex(indexOfCellAt$runtime_release, n3, persistentHashSetBuilder.getOwnership$runtime_release());
            }
            return this;
        }
    }
    
    public final Object mutableRemoveAll(final TrieNode<E> trieNode, int i, final DeltaCounter deltaCounter, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "otherNode");
        Intrinsics.checkNotNullParameter((Object)deltaCounter, "intersectionSizeRef");
        Intrinsics.checkNotNullParameter((Object)persistentHashSetBuilder, "mutator");
        if (this == trieNode) {
            deltaCounter.plusAssign(this.calculateSize());
            return TrieNode.EMPTY;
        }
        if (i > 30) {
            return this.mutableCollisionRemoveAll(trieNode, deltaCounter, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        int j = this.bitmap & trieNode.bitmap;
        if (j == 0) {
            return this;
        }
        TrieNode trieNode2;
        if (Intrinsics.areEqual((Object)this.ownedBy, (Object)persistentHashSetBuilder.getOwnership$runtime_release())) {
            trieNode2 = this;
        }
        else {
            final int bitmap = this.bitmap;
            final Object[] buffer = this.buffer;
            final Object[] copy = Arrays.copyOf(buffer, buffer.length);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
            trieNode2 = new TrieNode(bitmap, copy, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        int bitmap2 = this.bitmap;
        while (j != 0) {
            final int lowestOneBit = Integer.lowestOneBit(j);
            final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(lowestOneBit);
            final int indexOfCellAt$runtime_release2 = trieNode.indexOfCellAt$runtime_release(lowestOneBit);
            final TrieNode trieNode3 = this;
            final Object o = this.buffer[indexOfCellAt$runtime_release];
            final Object o2 = trieNode.buffer[indexOfCellAt$runtime_release2];
            final boolean b = o instanceof TrieNode;
            final boolean b2 = o2 instanceof TrieNode;
            Object o3 = null;
            Label_0481: {
                if (b && b2) {
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRemoveAll$lambda$11$lambda$10>");
                    final TrieNode trieNode4 = (TrieNode)o;
                    Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRemoveAll$lambda$11$lambda$10>");
                    o3 = trieNode4.mutableRemoveAll((TrieNode)o2, i + 5, deltaCounter, persistentHashSetBuilder);
                }
                else if (b) {
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRemoveAll$lambda$11$lambda$10>");
                    final TrieNode trieNode5 = (TrieNode)o;
                    final int size = persistentHashSetBuilder.size();
                    int hashCode;
                    if (o2 != null) {
                        hashCode = o2.hashCode();
                    }
                    else {
                        hashCode = 0;
                    }
                    final TrieNode<TrieNode> mutableRemove = trieNode5.mutableRemove(hashCode, o2, i + 5, persistentHashSetBuilder);
                    o3 = o;
                    if (size != persistentHashSetBuilder.size()) {
                        deltaCounter.plusAssign(1);
                        final Object[] buffer2 = mutableRemove.buffer;
                        if (buffer2.length == 1) {
                            o3 = buffer2[0];
                            if (!(o3 instanceof TrieNode)) {
                                break Label_0481;
                            }
                        }
                        o3 = mutableRemove;
                    }
                }
                else if (b2) {
                    Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRemoveAll$lambda$11$lambda$10>");
                    final TrieNode trieNode6 = (TrieNode)o2;
                    int hashCode2;
                    if (o != null) {
                        hashCode2 = o.hashCode();
                    }
                    else {
                        hashCode2 = 0;
                    }
                    o3 = o;
                    if (trieNode6.contains(hashCode2, o, i + 5)) {
                        deltaCounter.plusAssign(1);
                        o3 = TrieNode.EMPTY;
                    }
                }
                else {
                    o3 = o;
                    if (Intrinsics.areEqual(o, o2)) {
                        deltaCounter.plusAssign(1);
                        o3 = TrieNode.EMPTY;
                    }
                }
            }
            int n = bitmap2;
            if (o3 == TrieNode.EMPTY) {
                n = (bitmap2 ^ lowestOneBit);
            }
            trieNode2.buffer[indexOfCellAt$runtime_release] = o3;
            j ^= lowestOneBit;
            bitmap2 = n;
        }
        final int bitCount = Integer.bitCount(bitmap2);
        Object empty;
        if (bitmap2 == 0) {
            empty = TrieNode.EMPTY;
        }
        else if (bitmap2 == this.bitmap) {
            empty = trieNode2;
            if (trieNode2.elementsIdentityEquals(this)) {
                empty = this;
            }
        }
        else if (bitCount == 1 && i != 0) {
            final Object o4 = empty = trieNode2.buffer[trieNode2.indexOfCellAt$runtime_release(bitmap2)];
            if (o4 instanceof TrieNode) {
                empty = new TrieNode(bitmap2, new Object[] { o4 }, persistentHashSetBuilder.getOwnership$runtime_release());
            }
        }
        else {
            final Object[] array = new Object[bitCount];
            final Object[] buffer3 = trieNode2.buffer;
            i = 0;
            int n2 = 0;
            while (i < buffer3.length) {
                CommonFunctionsKt.assert(n2 <= i);
                if (buffer3[i] != TrieNode.Companion.getEMPTY$runtime_release()) {
                    array[0 + n2] = buffer3[i];
                    ++n2;
                    CommonFunctionsKt.assert(0 + n2 <= bitCount);
                }
                ++i;
            }
            empty = new TrieNode(bitmap2, array, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        return empty;
    }
    
    public final Object mutableRetainAll(TrieNode<E> empty, int i, final DeltaCounter deltaCounter, final PersistentHashSetBuilder<?> persistentHashSetBuilder) {
        final TrieNode trieNode = empty;
        Intrinsics.checkNotNullParameter((Object)trieNode, "otherNode");
        Intrinsics.checkNotNullParameter((Object)deltaCounter, "intersectionSizeRef");
        Intrinsics.checkNotNullParameter((Object)persistentHashSetBuilder, "mutator");
        if (this == trieNode) {
            deltaCounter.plusAssign(this.calculateSize());
            return this;
        }
        if (i > 30) {
            return this.mutableCollisionRetainAll(trieNode, deltaCounter, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        final int j = this.bitmap & trieNode.bitmap;
        if (j == 0) {
            return TrieNode.EMPTY;
        }
        TrieNode trieNode2;
        if (Intrinsics.areEqual((Object)this.ownedBy, (Object)persistentHashSetBuilder.getOwnership$runtime_release()) && j == this.bitmap) {
            trieNode2 = this;
        }
        else {
            trieNode2 = new TrieNode(j, new Object[Integer.bitCount(j)], persistentHashSetBuilder.getOwnership$runtime_release());
        }
        int k = j;
        int l = 0;
        int n = 0;
        while (k != 0) {
            final int lowestOneBit = Integer.lowestOneBit(k);
            final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(lowestOneBit);
            final int indexOfCellAt$runtime_release2 = trieNode.indexOfCellAt$runtime_release(lowestOneBit);
            final TrieNode trieNode3 = this;
            final Object o = this.buffer[indexOfCellAt$runtime_release];
            Object o2 = trieNode.buffer[indexOfCellAt$runtime_release2];
            final boolean b = o instanceof TrieNode;
            final boolean b2 = o2 instanceof TrieNode;
            if (b && b2) {
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRetainAll$lambda$9$lambda$8>");
                final TrieNode trieNode4 = (TrieNode)o;
                Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRetainAll$lambda$9$lambda$8>");
                o2 = trieNode4.mutableRetainAll((TrieNode)o2, i + 5, deltaCounter, persistentHashSetBuilder);
            }
            else if (b) {
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRetainAll$lambda$9$lambda$8>");
                final TrieNode trieNode5 = (TrieNode)o;
                int hashCode;
                if (o2 != null) {
                    hashCode = o2.hashCode();
                }
                else {
                    hashCode = 0;
                }
                if (trieNode5.contains(hashCode, o2, i + 5)) {
                    deltaCounter.plusAssign(1);
                }
                else {
                    o2 = TrieNode.EMPTY;
                }
            }
            else if (b2) {
                Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode.mutableRetainAll$lambda$9$lambda$8>");
                final TrieNode trieNode6 = (TrieNode)o2;
                int hashCode2;
                if (o != null) {
                    hashCode2 = o.hashCode();
                }
                else {
                    hashCode2 = 0;
                }
                if (trieNode6.contains(hashCode2, o, i + 5)) {
                    deltaCounter.plusAssign(1);
                    o2 = o;
                }
                else {
                    o2 = TrieNode.EMPTY;
                }
            }
            else if (Intrinsics.areEqual(o, o2)) {
                deltaCounter.plusAssign(1);
                o2 = o;
            }
            else {
                o2 = TrieNode.EMPTY;
            }
            int n2 = l;
            if (o2 != TrieNode.EMPTY) {
                n2 = (l | lowestOneBit);
            }
            trieNode2.buffer[n] = o2;
            ++n;
            k ^= lowestOneBit;
            l = n2;
        }
        final int bitCount = Integer.bitCount(l);
        if (l == 0) {
            empty = TrieNode.EMPTY;
        }
        else if (l == j) {
            if (trieNode2.elementsIdentityEquals(this)) {
                empty = this;
            }
            else if (trieNode2.elementsIdentityEquals(trieNode)) {
                empty = trieNode;
            }
            else {
                empty = trieNode2;
            }
        }
        else if (bitCount == 1 && i != 0) {
            final Object o3 = empty = (TrieNode)trieNode2.buffer[trieNode2.indexOfCellAt$runtime_release(l)];
            if (o3 instanceof TrieNode) {
                empty = new TrieNode<E>(l, new Object[] { o3 }, persistentHashSetBuilder.getOwnership$runtime_release());
            }
        }
        else {
            final Object[] array = new Object[bitCount];
            final Object[] buffer = trieNode2.buffer;
            i = 0;
            int n3 = 0;
            while (i < buffer.length) {
                CommonFunctionsKt.assert(n3 <= i);
                if (buffer[i] != TrieNode.Companion.getEMPTY$runtime_release()) {
                    array[0 + n3] = buffer[i];
                    ++n3;
                    CommonFunctionsKt.assert(0 + n3 <= bitCount);
                }
                ++i;
            }
            empty = new TrieNode<E>(l, array, persistentHashSetBuilder.getOwnership$runtime_release());
        }
        return empty;
    }
    
    public final TrieNode<E> remove(final int n, final E e, final int n2) {
        final int n3 = 1 << TrieNodeKt.indexSegment(n, n2);
        if (this.hasNoCellAt(n3)) {
            return this;
        }
        final int indexOfCellAt$runtime_release = this.indexOfCellAt$runtime_release(n3);
        final Object o = this.buffer[indexOfCellAt$runtime_release];
        if (o instanceof TrieNode) {
            final TrieNode<E> nodeAtIndex = this.nodeAtIndex(indexOfCellAt$runtime_release);
            TrieNode<Object> trieNode;
            if (n2 == 30) {
                trieNode = nodeAtIndex.collisionRemove((Object)e);
            }
            else {
                trieNode = nodeAtIndex.remove(n, (Object)e, n2 + 5);
            }
            if (nodeAtIndex == trieNode) {
                return this;
            }
            return this.updateNodeAtIndex(indexOfCellAt$runtime_release, (TrieNode<E>)trieNode);
        }
        else {
            if (Intrinsics.areEqual((Object)e, o)) {
                return this.removeCellAtIndex(indexOfCellAt$runtime_release, n3);
            }
            return this;
        }
    }
    
    public final void setBitmap(final int bitmap) {
        this.bitmap = bitmap;
    }
    
    public final void setBuffer(final Object[] buffer) {
        Intrinsics.checkNotNullParameter((Object)buffer, "<set-?>");
        this.buffer = buffer;
    }
    
    public final void setOwnedBy(final MutabilityOwnership ownedBy) {
        this.ownedBy = ownedBy;
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "", "getEMPTY$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final TrieNode getEMPTY$runtime_release() {
            return TrieNode.access$getEMPTY$cp();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap;

import java.util.NoSuchElementException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Iterator;

@Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0007\b \u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\b\u0012\u0004\u0012\u0002H\u00030\u0004B9\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u0012\u001e\u0010\u0007\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t0\b¢\u0006\u0002\u0010\nJ\b\u0010\u0018\u001a\u00020\u0019H\u0002J\r\u0010\u001a\u001a\u00028\u0000H\u0004¢\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u0019H\u0002J\t\u0010\u000b\u001a\u00020\fH\u0096\u0002J\u0010\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0013H\u0002J\u000e\u0010\u001f\u001a\u00028\u0002H\u0096\u0002¢\u0006\u0002\u0010\u001bR\u0018\u0010\u000b\u001a\u00020\f8\u0002@\u0002X\u0083\u000e¢\u0006\b\n\u0000\u0012\u0004\b\r\u0010\u000eR.\u0010\u0007\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t0\bX\u0084\u0004¢\u0006\n\n\u0002\u0010\u0011\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0012\u001a\u00020\u0013X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017¨\u0006 " }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/PersistentHashMapBaseIterator;", "K", "V", "T", "", "node", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;", "path", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNode;[Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;)V", "hasNext", "", "getHasNext$annotations", "()V", "getPath", "()[Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;", "[Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableMap/TrieNodeBaseIterator;", "pathLastIndex", "", "getPathLastIndex", "()I", "setPathLastIndex", "(I)V", "checkHasNext", "", "currentKey", "()Ljava/lang/Object;", "ensureNextEntryIsReady", "moveToNextNodeWithData", "pathIndex", "next", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class PersistentHashMapBaseIterator<K, V, T> implements Iterator<T>, KMappedMarker
{
    private boolean hasNext;
    private final TrieNodeBaseIterator<K, V, T>[] path;
    private int pathLastIndex;
    
    public PersistentHashMapBaseIterator(final TrieNode<K, V> trieNode, final TrieNodeBaseIterator<K, V, T>[] path) {
        Intrinsics.checkNotNullParameter((Object)trieNode, "node");
        Intrinsics.checkNotNullParameter((Object)path, "path");
        this.path = path;
        this.hasNext = true;
        path[0].reset(trieNode.getBuffer$runtime_release(), trieNode.entryCount$runtime_release() * 2);
        this.pathLastIndex = 0;
        this.ensureNextEntryIsReady();
    }
    
    private final void checkHasNext() {
        if (this.hasNext()) {
            return;
        }
        throw new NoSuchElementException();
    }
    
    private final void ensureNextEntryIsReady() {
        if (this.path[this.pathLastIndex].hasNextKey()) {
            return;
        }
        for (int pathLastIndex = this.pathLastIndex; -1 < pathLastIndex; --pathLastIndex) {
            final int moveToNextNodeWithData = this.moveToNextNodeWithData(pathLastIndex);
            int moveToNextNodeWithData2;
            if ((moveToNextNodeWithData2 = moveToNextNodeWithData) == -1) {
                moveToNextNodeWithData2 = moveToNextNodeWithData;
                if (this.path[pathLastIndex].hasNextNode()) {
                    this.path[pathLastIndex].moveToNextNode();
                    moveToNextNodeWithData2 = this.moveToNextNodeWithData(pathLastIndex);
                }
            }
            if (moveToNextNodeWithData2 != -1) {
                this.pathLastIndex = moveToNextNodeWithData2;
                return;
            }
            if (pathLastIndex > 0) {
                this.path[pathLastIndex - 1].moveToNextNode();
            }
            this.path[pathLastIndex].reset(TrieNode.Companion.getEMPTY$runtime_release().getBuffer$runtime_release(), 0);
        }
        this.hasNext = false;
    }
    
    private final int moveToNextNodeWithData(final int n) {
        if (this.path[n].hasNextKey()) {
            return n;
        }
        if (this.path[n].hasNextNode()) {
            final TrieNode<? extends K, ? extends V> currentNode = this.path[n].currentNode();
            if (n == 6) {
                this.path[n + 1].reset(currentNode.getBuffer$runtime_release(), currentNode.getBuffer$runtime_release().length);
            }
            else {
                this.path[n + 1].reset(currentNode.getBuffer$runtime_release(), currentNode.entryCount$runtime_release() * 2);
            }
            return this.moveToNextNodeWithData(n + 1);
        }
        return -1;
    }
    
    protected final K currentKey() {
        this.checkHasNext();
        return this.path[this.pathLastIndex].currentKey();
    }
    
    protected final TrieNodeBaseIterator<K, V, T>[] getPath() {
        return this.path;
    }
    
    protected final int getPathLastIndex() {
        return this.pathLastIndex;
    }
    
    @Override
    public boolean hasNext() {
        return this.hasNext;
    }
    
    @Override
    public T next() {
        this.checkHasNext();
        final T next = this.path[this.pathLastIndex].next();
        this.ensureNextEntryIsReady();
        return next;
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    protected final void setPathLastIndex(final int pathLastIndex) {
        this.pathLastIndex = pathLastIndex;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet;

import java.util.Iterator;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.DeltaCounter;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.MutabilityOwnership;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentSet;
import kotlin.collections.AbstractMutableSet;

@Metadata(d1 = { "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010)\n\u0002\b\u0004\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001cJ\u0016\u0010\u001d\u001a\u00020\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0016J\u000e\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u0005H\u0016J\b\u0010!\u001a\u00020\"H\u0016J\u0016\u0010#\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u001cJ\u0016\u0010$\u001a\u00020\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0016J\u000f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000&H\u0096\u0002J\u0015\u0010'\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001cJ\u0016\u0010(\u001a\u00020\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0016J\u0016\u0010)\u001a\u00020\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0016R\u001e\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR*\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\f2\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\f@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0007\u001a\u00020\u0010@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R$\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\b@VX\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000b\"\u0004\b\u0017\u0010\u0018¨\u0006*" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSetBuilder;", "E", "Lkotlin/collections/AbstractMutableSet;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet$Builder;", "set", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSet;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/PersistentHashSet;)V", "<set-?>", "", "modCount", "getModCount$runtime_release", "()I", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "node", "getNode$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "ownership", "getOwnership$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/MutabilityOwnership;", "value", "size", "getSize", "setSize", "(I)V", "add", "", "element", "(Ljava/lang/Object;)Z", "addAll", "elements", "", "build", "clear", "", "contains", "containsAll", "iterator", "", "remove", "removeAll", "retainAll", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentHashSetBuilder<E> extends AbstractMutableSet<E> implements Builder<E>
{
    private int modCount;
    private TrieNode<E> node;
    private MutabilityOwnership ownership;
    private PersistentHashSet<E> set;
    private int size;
    
    public PersistentHashSetBuilder(final PersistentHashSet<E> set) {
        Intrinsics.checkNotNullParameter((Object)set, "set");
        this.set = set;
        this.ownership = new MutabilityOwnership();
        this.node = this.set.getNode$runtime_release();
        this.size = this.set.size();
    }
    
    public boolean add(final E e) {
        final int size = this.size();
        final TrieNode<E> node = this.node;
        boolean b = false;
        int hashCode;
        if (e != null) {
            hashCode = e.hashCode();
        }
        else {
            hashCode = 0;
        }
        this.node = node.mutableAdd(hashCode, e, 0, this);
        if (size != this.size()) {
            b = true;
        }
        return b;
    }
    
    public boolean addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        PersistentHashSet set;
        if (collection instanceof PersistentHashSet) {
            set = (PersistentHashSet)collection;
        }
        else {
            set = null;
        }
        PersistentHashSet build = set;
        if (set == null) {
            PersistentHashSetBuilder persistentHashSetBuilder;
            if (collection instanceof PersistentHashSetBuilder) {
                persistentHashSetBuilder = (PersistentHashSetBuilder)collection;
            }
            else {
                persistentHashSetBuilder = null;
            }
            if (persistentHashSetBuilder != null) {
                build = persistentHashSetBuilder.build();
            }
            else {
                build = null;
            }
        }
        if (build != null) {
            boolean b = false;
            final DeltaCounter deltaCounter = new DeltaCounter(0, 1, null);
            final int size = this.size();
            final TrieNode<E> mutableAddAll = this.node.mutableAddAll(build.getNode$runtime_release(), 0, deltaCounter, this);
            final int size2 = collection.size() + size - deltaCounter.getCount();
            if (size != size2) {
                this.node = mutableAddAll;
                this.setSize(size2);
            }
            if (size != this.size()) {
                b = true;
            }
            return b;
        }
        return super.addAll((Collection)collection);
    }
    
    public PersistentHashSet<E> build() {
        PersistentHashSet<E> set;
        if (this.node == this.set.getNode$runtime_release()) {
            set = this.set;
        }
        else {
            this.ownership = new MutabilityOwnership();
            set = new PersistentHashSet<E>(this.node, this.size());
        }
        return this.set = set;
    }
    
    public void clear() {
        final TrieNode empty$runtime_release = TrieNode.Companion.getEMPTY$runtime_release();
        Intrinsics.checkNotNull((Object)empty$runtime_release, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.PersistentHashSetBuilder>");
        this.node = empty$runtime_release;
        this.setSize(0);
    }
    
    public boolean contains(final Object o) {
        final TrieNode<E> node = this.node;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return node.contains(hashCode, (E)o, 0);
    }
    
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        if (collection instanceof PersistentHashSet) {
            return this.node.containsAll(((PersistentHashSet<E>)collection).getNode$runtime_release(), 0);
        }
        if (collection instanceof PersistentHashSetBuilder) {
            return this.node.containsAll(((PersistentHashSetBuilder)collection).node, 0);
        }
        return super.containsAll((Collection)collection);
    }
    
    public final int getModCount$runtime_release() {
        return this.modCount;
    }
    
    public final TrieNode<E> getNode$runtime_release() {
        return this.node;
    }
    
    public final MutabilityOwnership getOwnership$runtime_release() {
        return this.ownership;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public Iterator<E> iterator() {
        return new PersistentHashSetMutableIterator<E>(this);
    }
    
    public boolean remove(final Object o) {
        final int size = this.size();
        final TrieNode<E> node = this.node;
        boolean b = false;
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        this.node = node.mutableRemove(hashCode, (E)o, 0, this);
        if (size != this.size()) {
            b = true;
        }
        return b;
    }
    
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        PersistentHashSet set;
        if (collection instanceof PersistentHashSet) {
            set = (PersistentHashSet)collection;
        }
        else {
            set = null;
        }
        PersistentHashSet build = set;
        if (set == null) {
            PersistentHashSetBuilder persistentHashSetBuilder;
            if (collection instanceof PersistentHashSetBuilder) {
                persistentHashSetBuilder = (PersistentHashSetBuilder)collection;
            }
            else {
                persistentHashSetBuilder = null;
            }
            if (persistentHashSetBuilder != null) {
                build = persistentHashSetBuilder.build();
            }
            else {
                build = null;
            }
        }
        if (build != null) {
            boolean b = false;
            final DeltaCounter deltaCounter = new DeltaCounter(0, 1, null);
            final int size = this.size();
            final Object mutableRemoveAll = this.node.mutableRemoveAll(build.getNode$runtime_release(), 0, deltaCounter, this);
            final int size2 = size - deltaCounter.getCount();
            if (size2 == 0) {
                this.clear();
            }
            else if (size2 != size) {
                Intrinsics.checkNotNull(mutableRemoveAll, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.PersistentHashSetBuilder>");
                this.node = (TrieNode<E>)mutableRemoveAll;
                this.setSize(size2);
            }
            if (size != this.size()) {
                b = true;
            }
            return b;
        }
        return super.removeAll((Collection)collection);
    }
    
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        PersistentHashSet set;
        if (collection instanceof PersistentHashSet) {
            set = (PersistentHashSet)collection;
        }
        else {
            set = null;
        }
        PersistentHashSet build = set;
        if (set == null) {
            PersistentHashSetBuilder persistentHashSetBuilder;
            if (collection instanceof PersistentHashSetBuilder) {
                persistentHashSetBuilder = (PersistentHashSetBuilder)collection;
            }
            else {
                persistentHashSetBuilder = null;
            }
            if (persistentHashSetBuilder != null) {
                build = persistentHashSetBuilder.build();
            }
            else {
                build = null;
            }
        }
        if (build != null) {
            boolean b = false;
            final DeltaCounter deltaCounter = new DeltaCounter(0, 1, null);
            final int size = this.size();
            final Object mutableRetainAll = this.node.mutableRetainAll(build.getNode$runtime_release(), 0, deltaCounter, this);
            final int count = deltaCounter.getCount();
            if (count == 0) {
                this.clear();
            }
            else if (count != size) {
                Intrinsics.checkNotNull(mutableRetainAll, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.PersistentHashSetBuilder>");
                this.node = (TrieNode<E>)mutableRetainAll;
                this.setSize(count);
            }
            if (size != this.size()) {
                b = true;
            }
            return b;
        }
        return super.retainAll((Collection)collection);
    }
    
    public void setSize(final int size) {
        this.size = size;
        ++this.modCount;
    }
}

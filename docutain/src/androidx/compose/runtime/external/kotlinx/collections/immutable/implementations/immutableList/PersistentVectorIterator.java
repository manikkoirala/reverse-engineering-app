// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import kotlin.ranges.RangesKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B;\u0012\u000e\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\b¢\u0006\u0002\u0010\u000bJ\u000e\u0010\u000f\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0010J\r\u0010\u0011\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010R\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0088\u0004¢\u0006\u0004\n\u0002\u0010\fR\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u000eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorIterator;", "T", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractListIterator;", "root", "", "", "tail", "index", "", "size", "trieHeight", "([Ljava/lang/Object;[Ljava/lang/Object;III)V", "[Ljava/lang/Object;", "trieIterator", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/TrieIterator;", "next", "()Ljava/lang/Object;", "previous", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentVectorIterator<T> extends AbstractListIterator<T>
{
    private final T[] tail;
    private final TrieIterator<T> trieIterator;
    
    public PersistentVectorIterator(final Object[] array, final T[] tail, final int n, int rootSize, final int n2) {
        Intrinsics.checkNotNullParameter((Object)array, "root");
        Intrinsics.checkNotNullParameter((Object)tail, "tail");
        super(n, rootSize);
        this.tail = tail;
        rootSize = UtilsKt.rootSize(rootSize);
        this.trieIterator = new TrieIterator<T>(array, RangesKt.coerceAtMost(n, rootSize), rootSize, n2);
    }
    
    @Override
    public T next() {
        this.checkHasNext$runtime_release();
        if (this.trieIterator.hasNext()) {
            this.setIndex(this.getIndex() + 1);
            return this.trieIterator.next();
        }
        final T[] tail = this.tail;
        final int index = this.getIndex();
        this.setIndex(index + 1);
        return tail[index - this.trieIterator.getSize()];
    }
    
    @Override
    public T previous() {
        this.checkHasPrevious$runtime_release();
        if (this.getIndex() > this.trieIterator.getSize()) {
            final T[] tail = this.tail;
            this.setIndex(this.getIndex() - 1);
            return tail[this.getIndex() - this.trieIterator.getSize()];
        }
        this.setIndex(this.getIndex() - 1);
        return this.trieIterator.previous();
    }
}

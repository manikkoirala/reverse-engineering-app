// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import kotlin.jvm.functions.Function1;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.util.Arrays;
import kotlin.collections.ArraysKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.ListImplementation;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentList;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentCollection;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ImmutableList;

@Metadata(d1 = { "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010*\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0000\u0018\u0000 (*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001(B\u0015\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005¢\u0006\u0002\u0010\u0007J\u001b\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010J#\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0012J$\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0006\u0010\u0011\u001a\u00020\n2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015H\u0016J\u001c\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015H\u0016J\u001d\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u0006\u0010\t\u001a\u00020\nH\u0002¢\u0006\u0002\u0010\u0018J\u000e\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\u0016\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u0011\u001a\u00020\nH\u0096\u0002¢\u0006\u0002\u0010\u001cJ\u0015\u0010\u001d\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001eJ\u0015\u0010\u001f\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001eJ\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000!2\u0006\u0010\u0011\u001a\u00020\nH\u0016J\"\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020%0$H\u0016J\u0016\u0010&\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0006\u0010\u0011\u001a\u00020\nH\u0016J#\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0012R\u0018\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006)" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/SmallPersistentVector;", "E", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableList;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractPersistentList;", "buffer", "", "", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "size", "", "getSize", "()I", "add", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "element", "(Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "index", "(ILjava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "addAll", "c", "", "elements", "bufferOfSize", "(I)[Ljava/lang/Object;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList$Builder;", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "lastIndexOf", "listIterator", "", "removeAll", "predicate", "Lkotlin/Function1;", "", "removeAt", "set", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SmallPersistentVector<E> extends AbstractPersistentList<E> implements ImmutableList<E>
{
    public static final Companion Companion;
    private static final SmallPersistentVector EMPTY;
    private final Object[] buffer;
    
    static {
        Companion = new Companion(null);
        EMPTY = new SmallPersistentVector(new Object[0]);
    }
    
    public SmallPersistentVector(final Object[] buffer) {
        Intrinsics.checkNotNullParameter((Object)buffer, "buffer");
        this.buffer = buffer;
        CommonFunctionsKt.assert(buffer.length <= 32);
    }
    
    public static final /* synthetic */ SmallPersistentVector access$getEMPTY$cp() {
        return SmallPersistentVector.EMPTY;
    }
    
    private final Object[] bufferOfSize(final int n) {
        return new Object[n];
    }
    
    public PersistentList<E> add(final int n, final E e) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        if (n == this.size()) {
            return this.add(e);
        }
        if (this.size() < 32) {
            final Object[] bufferOfSize = this.bufferOfSize(this.size() + 1);
            ArraysKt.copyInto$default(this.buffer, bufferOfSize, 0, 0, n, 6, (Object)null);
            ArraysKt.copyInto(this.buffer, bufferOfSize, n + 1, n, this.size());
            bufferOfSize[n] = e;
            return new SmallPersistentVector(bufferOfSize);
        }
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        ArraysKt.copyInto(this.buffer, copy, n + 1, n, this.size() - 1);
        copy[n] = e;
        return new PersistentVector<E>(copy, UtilsKt.presizedBufferWith(this.buffer[31]), this.size() + 1, 0);
    }
    
    public PersistentList<E> add(final E e) {
        if (this.size() < 32) {
            final Object[] copy = Arrays.copyOf(this.buffer, this.size() + 1);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            copy[this.size()] = e;
            return new SmallPersistentVector(copy);
        }
        return new PersistentVector<E>(this.buffer, UtilsKt.presizedBufferWith(e), this.size() + 1, 0);
    }
    
    @Override
    public PersistentList<E> addAll(int n, final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "c");
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        if (this.size() + collection.size() <= 32) {
            final Object[] bufferOfSize = this.bufferOfSize(this.size() + collection.size());
            ArraysKt.copyInto$default(this.buffer, bufferOfSize, 0, 0, n, 6, (Object)null);
            ArraysKt.copyInto(this.buffer, bufferOfSize, collection.size() + n, n, this.size());
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                bufferOfSize[n] = iterator.next();
                ++n;
            }
            return new SmallPersistentVector(bufferOfSize);
        }
        final PersistentList.Builder<E> builder = this.builder();
        builder.addAll(n, collection);
        return builder.build();
    }
    
    @Override
    public PersistentList<E> addAll(final Collection<? extends E> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        if (this.size() + collection.size() <= 32) {
            final Object[] copy = Arrays.copyOf(this.buffer, this.size() + collection.size());
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            int size = this.size();
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                copy[size] = iterator.next();
                ++size;
            }
            return new SmallPersistentVector(copy);
        }
        final PersistentList.Builder<E> builder = this.builder();
        builder.addAll(collection);
        return builder.build();
    }
    
    public Builder<E> builder() {
        return (Builder)new PersistentVectorBuilder(this, null, this.buffer, 0);
    }
    
    public E get(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        return (E)this.buffer[n];
    }
    
    public int getSize() {
        return this.buffer.length;
    }
    
    public int indexOf(final Object o) {
        return ArraysKt.indexOf(this.buffer, o);
    }
    
    public int lastIndexOf(final Object o) {
        return ArraysKt.lastIndexOf(this.buffer, o);
    }
    
    public ListIterator<E> listIterator(final int n) {
        ListImplementation.checkPositionIndex$runtime_release(n, this.size());
        return new BufferIterator<E>((E[])this.buffer, n, this.size());
    }
    
    public PersistentList<E> removeAll(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        Object[] buffer = this.buffer;
        int size = this.size();
        final int size2 = this.size();
        int i = 0;
        int n = 0;
        while (i < size2) {
            final Object o = this.buffer[i];
            Object[] copy;
            int n2;
            int n3;
            if (function1.invoke(o)) {
                copy = buffer;
                n2 = size;
                if ((n3 = n) == 0) {
                    final Object[] buffer2 = this.buffer;
                    copy = Arrays.copyOf(buffer2, buffer2.length);
                    Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
                    n3 = 1;
                    n2 = i;
                }
            }
            else {
                copy = buffer;
                n2 = size;
                if ((n3 = n) != 0) {
                    buffer[size] = o;
                    n2 = size + 1;
                    n3 = n;
                    copy = buffer;
                }
            }
            ++i;
            buffer = copy;
            size = n2;
            n = n3;
        }
        PersistentList list;
        if (size == this.size()) {
            list = this;
        }
        else if (size == 0) {
            list = SmallPersistentVector.EMPTY;
        }
        else {
            list = new SmallPersistentVector(ArraysKt.copyOfRange(buffer, 0, size));
        }
        return list;
    }
    
    public PersistentList<E> removeAt(final int n) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        if (this.size() == 1) {
            return SmallPersistentVector.EMPTY;
        }
        final Object[] copy = Arrays.copyOf(this.buffer, this.size() - 1);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        ArraysKt.copyInto(this.buffer, copy, n, n + 1, this.size());
        return new SmallPersistentVector(copy);
    }
    
    public PersistentList<E> set(final int n, final E e) {
        ListImplementation.checkElementIndex$runtime_release(n, this.size());
        final Object[] buffer = this.buffer;
        final Object[] copy = Arrays.copyOf(buffer, buffer.length);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, size)");
        copy[n] = e;
        return new SmallPersistentVector(copy);
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/SmallPersistentVector$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/SmallPersistentVector;", "", "getEMPTY", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/SmallPersistentVector;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final SmallPersistentVector getEMPTY() {
            return SmallPersistentVector.access$getEMPTY$cp();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.internal;

import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.Collection;
import kotlin.jvm.JvmStatic;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0005\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0001¢\u0006\u0002\b\bJ\u001d\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0001¢\u0006\u0002\b\nJ%\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0001¢\u0006\u0002\b\u000eJ%\u0010\u000f\u001a\u00020\u00102\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u00122\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u0012H\u0001¢\u0006\u0002\b\u0014J\u0019\u0010\u0015\u001a\u00020\u00062\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u0012H\u0001¢\u0006\u0002\b\u0016¨\u0006\u0017" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/internal/ListImplementation;", "", "()V", "checkElementIndex", "", "index", "", "size", "checkElementIndex$runtime_release", "checkPositionIndex", "checkPositionIndex$runtime_release", "checkRangeIndexes", "fromIndex", "toIndex", "checkRangeIndexes$runtime_release", "orderedEquals", "", "c", "", "other", "orderedEquals$runtime_release", "orderedHashCode", "orderedHashCode$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ListImplementation
{
    public static final ListImplementation INSTANCE;
    
    static {
        INSTANCE = new ListImplementation();
    }
    
    private ListImplementation() {
    }
    
    @JvmStatic
    public static final void checkElementIndex$runtime_release(final int i, final int j) {
        if (i >= 0 && i < j) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("index: ");
        sb.append(i);
        sb.append(", size: ");
        sb.append(j);
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    @JvmStatic
    public static final void checkPositionIndex$runtime_release(final int i, final int j) {
        if (i >= 0 && i <= j) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("index: ");
        sb.append(i);
        sb.append(", size: ");
        sb.append(j);
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    @JvmStatic
    public static final void checkRangeIndexes$runtime_release(final int n, final int n2, final int i) {
        if (n < 0 || n2 > i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("fromIndex: ");
            sb.append(n);
            sb.append(", toIndex: ");
            sb.append(n2);
            sb.append(", size: ");
            sb.append(i);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        if (n <= n2) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("fromIndex: ");
        sb2.append(n);
        sb2.append(" > toIndex: ");
        sb2.append(n2);
        throw new IllegalArgumentException(sb2.toString());
    }
    
    @JvmStatic
    public static final boolean orderedEquals$runtime_release(final Collection<?> collection, final Collection<?> collection2) {
        Intrinsics.checkNotNullParameter((Object)collection, "c");
        Intrinsics.checkNotNullParameter((Object)collection2, "other");
        if (collection.size() != collection2.size()) {
            return false;
        }
        final Iterator iterator = collection2.iterator();
        final Iterator iterator2 = collection.iterator();
        while (iterator2.hasNext()) {
            if (!Intrinsics.areEqual(iterator2.next(), iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    @JvmStatic
    public static final int orderedHashCode$runtime_release(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "c");
        final Iterator<?> iterator = collection.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            int hashCode;
            if (next != null) {
                hashCode = next.hashCode();
            }
            else {
                hashCode = 0;
            }
            n = n * 31 + hashCode;
        }
        return n;
    }
}

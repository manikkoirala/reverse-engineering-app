// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet;

import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.CommonFunctionsKt;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u000b\u0010\t\u001a\u00028\u0000¢\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\fJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eJ\u0006\u0010\u0010\u001a\u00020\u000eJ\u0006\u0010\u0011\u001a\u00020\u0012J\u000b\u0010\u0013\u001a\u00028\u0000¢\u0006\u0002\u0010\nJ%\u0010\u0014\u001a\u00020\u00122\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\u0015R\u0018\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0016" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNodeIterator;", "E", "", "()V", "buffer", "", "[Ljava/lang/Object;", "index", "", "currentElement", "()Ljava/lang/Object;", "currentNode", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableSet/TrieNode;", "hasNextCell", "", "hasNextElement", "hasNextNode", "moveToNextCell", "", "nextElement", "reset", "([Ljava/lang/Object;I)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TrieNodeIterator<E>
{
    private Object[] buffer;
    private int index;
    
    public TrieNodeIterator() {
        this.buffer = TrieNode.Companion.getEMPTY$runtime_release().getBuffer();
    }
    
    public final E currentElement() {
        CommonFunctionsKt.assert(this.hasNextElement());
        return (E)this.buffer[this.index];
    }
    
    public final TrieNode<? extends E> currentNode() {
        CommonFunctionsKt.assert(this.hasNextNode());
        final Object o = this.buffer[this.index];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNode<E of androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.TrieNodeIterator>");
        return (TrieNode<? extends E>)o;
    }
    
    public final boolean hasNextCell() {
        return this.index < this.buffer.length;
    }
    
    public final boolean hasNextElement() {
        return this.hasNextCell() && !(this.buffer[this.index] instanceof TrieNode);
    }
    
    public final boolean hasNextNode() {
        return this.hasNextCell() && this.buffer[this.index] instanceof TrieNode;
    }
    
    public final void moveToNextCell() {
        CommonFunctionsKt.assert(this.hasNextCell());
        ++this.index;
    }
    
    public final E nextElement() {
        CommonFunctionsKt.assert(this.hasNextElement());
        return (E)this.buffer[this.index++];
    }
    
    public final void reset(final Object[] buffer, final int index) {
        Intrinsics.checkNotNullParameter((Object)buffer, "buffer");
        this.buffer = buffer;
        this.index = index;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable;

import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedSet.PersistentOrderedSetBuilder;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap.PersistentOrderedMapBuilder;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.PersistentHashSetBuilder;
import kotlin.text.StringsKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMapBuilder;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedSet.PersistentOrderedSet;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap.PersistentOrderedMap;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList.UtilsKt;
import kotlin.collections.ArraysKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableSet.PersistentHashSet;
import kotlin.collections.MapsKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableMap.PersistentHashMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.Map;
import java.util.List;
import kotlin.sequences.Sequence;
import kotlin.collections.CollectionsKt;
import java.util.Set;
import java.util.Collection;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000v\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0010\u0002\n\u0002\u0010%\n\u0002\u0010#\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\f\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u001aQ\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032*\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005\"\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006H\u0001¢\u0006\u0002\u0010\u0007\u001a-\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0001¢\u0006\u0002\u0010\f\u001a\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\nH\u0001\u001a-\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0001¢\u0006\u0002\u0010\u000f\u001aQ\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032*\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005\"\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006H\u0001¢\u0006\u0002\u0010\u0007\u001a\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\nH\u0001\u001a-\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0001¢\u0006\u0002\u0010\f\u001a \u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003H\u0000\u001aQ\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032*\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005\"\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006H\u0000¢\u0006\u0002\u0010\u0007\u001a\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\nH\u0000\u001a-\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0000¢\u0006\u0002\u0010\f\u001a\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\nH\u0000\u001a-\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0000¢\u0006\u0002\u0010\u000f\u001a \u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003H\u0000\u001aQ\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032*\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005\"\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006H\u0000¢\u0006\u0002\u0010\u0007\u001a\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\nH\u0000\u001a-\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n2\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005\"\u0002H\nH\u0000¢\u0006\u0002\u0010\f\u001a-\u0010\u0017\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0004\u001a-\u0010\u0017\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0004\u001a,\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010\u001c\u001a4\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010\u001d\u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001a,\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010\u001f\u001a4\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010 \u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001a@\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0006\u0010!\u001a\u0002H\u0002H\u0080\u0002¢\u0006\u0002\u0010\"\u001aH\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u000e\u0010#\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0005H\u0080\u0002¢\u0006\u0002\u0010$\u001aA\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\f\u0010#\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0019H\u0080\u0002\u001aA\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\f\u0010#\u001a\b\u0012\u0004\u0012\u0002H\u00020\u001eH\u0080\u0002\u001a,\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010%\u001a4\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010&\u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001a<\u0010'\u001a\b\u0012\u0004\u0012\u0002H(0\u000e\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u000e2\u0018\u0010)\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H(0+\u0012\u0004\u0012\u00020,0*H\u0080\b\u00f8\u0001\u0000\u001aV\u0010'\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u001e\u0010)\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030-\u0012\u0004\u0012\u00020,0*H\u0080\b\u00f8\u0001\u0000\u001a<\u0010'\u001a\b\u0012\u0004\u0012\u0002H(0\t\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\t2\u0018\u0010)\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H(0.\u0012\u0004\u0012\u00020,0*H\u0080\b\u00f8\u0001\u0000\u001a,\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010\u001c\u001a4\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010\u001d\u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u0018\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u00182\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001a,\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010\u001f\u001a4\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010 \u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\u000e\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\u000e2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001aT\u0010/\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u001a\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005H\u0080\n¢\u0006\u0002\u00100\u001aG\u0010/\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006H\u0080\n\u001aM\u0010/\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0019H\u0080\n\u001aI\u0010/\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0014\u00102\u001a\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u000303H\u0080\n\u001aM\u0010/\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u001eH\u0080\n\u001a,\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\u0006\u0010\u001b\u001a\u0002H\nH\u0080\n¢\u0006\u0002\u0010%\u001a4\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\n0\u0005H\u0080\u0002¢\u0006\u0002\u0010&\u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u0019H\u0080\u0002\u001a-\u0010/\u001a\b\u0012\u0004\u0012\u0002H\n0\t\"\u0004\b\u0000\u0010\n*\b\u0012\u0004\u0012\u0002H\n0\t2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\u001eH\u0080\u0002\u001aS\u00104\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u001a\u0010\u0004\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0005H\u0000¢\u0006\u0002\u00100\u001aL\u00104\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u0019H\u0000\u001aH\u00104\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0014\u00102\u001a\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u000303H\u0000\u001aL\u00104\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00060\u001eH\u0000\u001a\u0012\u00105\u001a\b\u0012\u0004\u0012\u00020706*\u000208H\u0000\u001a\u001e\u00105\u001a\b\u0012\u0004\u0012\u0002H(06\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u0019H\u0000\u001a\u001e\u00105\u001a\b\u0012\u0004\u0012\u0002H(06\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u001eH\u0000\u001a0\u00109\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030:\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u000303H\u0000\u001a\u0012\u0010;\u001a\b\u0012\u0004\u0012\u0002070\t*\u000208H\u0000\u001a\u001e\u0010;\u001a\b\u0012\u0004\u0012\u0002H(0<\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u0019H\u0000\u001a\u001e\u0010;\u001a\b\u0012\u0004\u0012\u0002H(0<\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u001eH\u0000\u001a0\u0010=\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u000303H\u0000\u001a\u0012\u0010>\u001a\b\u0012\u0004\u0012\u0002070\t*\u000208H\u0000\u001a\u001e\u0010>\u001a\b\u0012\u0004\u0012\u0002H(0\t\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u0019H\u0000\u001a\u001e\u0010>\u001a\b\u0012\u0004\u0012\u0002H(0\t\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u001eH\u0000\u001a\u0012\u0010?\u001a\b\u0012\u0004\u0012\u0002070\u000e*\u000208H\u0000\u001a\u001e\u0010?\u001a\b\u0012\u0004\u0012\u0002H(0\u000e\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u0019H\u0000\u001a\u001e\u0010?\u001a\b\u0012\u0004\u0012\u0002H(0\u000e\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u001eH\u0000\u001a0\u0010@\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u000303H\u0000\u001a\u0012\u0010A\u001a\b\u0012\u0004\u0012\u0002070\t*\u000208H\u0000\u001a\u001e\u0010A\u001a\b\u0012\u0004\u0012\u0002H(0\t\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u0019H\u0000\u001a\u001e\u0010A\u001a\b\u0012\u0004\u0012\u0002H(0\t\"\u0004\b\u0000\u0010(*\b\u0012\u0004\u0012\u0002H(0\u001eH\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006B" }, d2 = { "immutableHashMapOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "K", "V", "pairs", "", "Lkotlin/Pair;", "([Lkotlin/Pair;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "immutableHashSetOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "E", "elements", "([Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "immutableListOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "([Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "immutableMapOf", "immutableSetOf", "persistentHashMapOf", "persistentHashSetOf", "persistentListOf", "persistentMapOf", "persistentSetOf", "intersect", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentCollection;", "", "minus", "element", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentCollection;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentCollection;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentCollection;[Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentCollection;", "Lkotlin/sequences/Sequence;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;[Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "key", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "keys", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;[Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;[Ljava/lang/Object;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "mutate", "T", "mutator", "Lkotlin/Function1;", "", "", "", "", "plus", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;[Lkotlin/Pair;)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "pair", "map", "", "putAll", "toImmutableList", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableList;", "", "", "toImmutableMap", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableMap;", "toImmutableSet", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/ImmutableSet;", "toPersistentHashMap", "toPersistentHashSet", "toPersistentList", "toPersistentMap", "toPersistentSet", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ExtensionsKt
{
    @Deprecated(message = "Use persistentHashMapOf instead.", replaceWith = @ReplaceWith(expression = "persistentHashMapOf(*pairs)", imports = {}))
    public static final <K, V> PersistentMap<K, V> immutableHashMapOf(final Pair<? extends K, ? extends V>... original) {
        Intrinsics.checkNotNullParameter((Object)original, "pairs");
        return (PersistentMap<K, V>)persistentHashMapOf((kotlin.Pair<?, ?>[])Arrays.copyOf((Pair<? extends K, ? extends V>[])original, original.length));
    }
    
    @Deprecated(message = "Use persistentHashSetOf instead.", replaceWith = @ReplaceWith(expression = "persistentHashSetOf(*elements)", imports = {}))
    public static final <E> PersistentSet<E> immutableHashSetOf(final E... original) {
        Intrinsics.checkNotNullParameter((Object)original, "elements");
        return (PersistentSet<E>)persistentHashSetOf((Object[])Arrays.copyOf((E[])original, original.length));
    }
    
    @Deprecated(message = "Use persistentListOf instead.", replaceWith = @ReplaceWith(expression = "persistentListOf()", imports = {}))
    public static final <E> PersistentList<E> immutableListOf() {
        return (PersistentList<E>)persistentListOf();
    }
    
    @Deprecated(message = "Use persistentListOf instead.", replaceWith = @ReplaceWith(expression = "persistentListOf(*elements)", imports = {}))
    public static final <E> PersistentList<E> immutableListOf(final E... original) {
        Intrinsics.checkNotNullParameter((Object)original, "elements");
        return (PersistentList<E>)persistentListOf((Object[])Arrays.copyOf((E[])original, original.length));
    }
    
    @Deprecated(message = "Use persistentMapOf instead.", replaceWith = @ReplaceWith(expression = "persistentMapOf(*pairs)", imports = {}))
    public static final <K, V> PersistentMap<K, V> immutableMapOf(final Pair<? extends K, ? extends V>... original) {
        Intrinsics.checkNotNullParameter((Object)original, "pairs");
        return (PersistentMap<K, V>)persistentMapOf((kotlin.Pair<?, ?>[])Arrays.copyOf((Pair<? extends K, ? extends V>[])original, original.length));
    }
    
    @Deprecated(message = "Use persistentSetOf instead.", replaceWith = @ReplaceWith(expression = "persistentSetOf()", imports = {}))
    public static final <E> PersistentSet<E> immutableSetOf() {
        return (PersistentSet<E>)persistentSetOf();
    }
    
    @Deprecated(message = "Use persistentSetOf instead.", replaceWith = @ReplaceWith(expression = "persistentSetOf(*elements)", imports = {}))
    public static final <E> PersistentSet<E> immutableSetOf(final E... original) {
        Intrinsics.checkNotNullParameter((Object)original, "elements");
        return (PersistentSet<E>)persistentSetOf((Object[])Arrays.copyOf((E[])original, original.length));
    }
    
    public static final <E> PersistentSet<E> intersect(final PersistentCollection<? extends E> collection, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        return intersect(toPersistentSet((Iterable<? extends E>)collection), iterable);
    }
    
    public static final <E> PersistentSet<E> intersect(final PersistentSet<? extends E> set, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentSet<? extends E> set2;
        if (iterable instanceof Collection) {
            set2 = set.retainAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentSet.Builder<? extends E> builder = set.builder();
            CollectionsKt.retainAll((Collection)builder, (Iterable)iterable);
            set2 = builder.build();
        }
        return (PersistentSet<E>)set2;
    }
    
    public static final <E> PersistentCollection<E> minus(final PersistentCollection<? extends E> collection, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentCollection<? extends E> collection2;
        if (iterable instanceof Collection) {
            collection2 = collection.removeAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentCollection.Builder<? extends E> builder = collection.builder();
            CollectionsKt.removeAll((Collection)builder, (Iterable)iterable);
            collection2 = builder.build();
        }
        return (PersistentCollection<E>)collection2;
    }
    
    public static final <E> PersistentCollection<E> minus(final PersistentCollection<? extends E> collection, final E e) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        return (PersistentCollection<E>)collection.remove((E)e);
    }
    
    public static final <E> PersistentCollection<E> minus(final PersistentCollection<? extends E> collection, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentCollection.Builder<? extends E> builder = collection.builder();
        CollectionsKt.removeAll((Collection)builder, (Sequence)sequence);
        return (PersistentCollection<E>)builder.build();
    }
    
    public static final <E> PersistentCollection<E> minus(final PersistentCollection<? extends E> collection, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentCollection.Builder<? extends E> builder = collection.builder();
        CollectionsKt.removeAll((Collection)builder, (Object[])array);
        return (PersistentCollection<E>)builder.build();
    }
    
    public static final <E> PersistentList<E> minus(final PersistentList<? extends E> list, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentList<? extends E> list2;
        if (iterable instanceof Collection) {
            list2 = list.removeAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentList.Builder<? extends E> builder = list.builder();
            CollectionsKt.removeAll((Collection)builder, (Iterable)iterable);
            list2 = builder.build();
        }
        return (PersistentList<E>)list2;
    }
    
    public static final <E> PersistentList<E> minus(final PersistentList<? extends E> list, final E e) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        return (PersistentList<E>)list.remove((E)e);
    }
    
    public static final <E> PersistentList<E> minus(final PersistentList<? extends E> list, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentList.Builder<? extends E> builder = list.builder();
        CollectionsKt.removeAll((Collection)builder, (Sequence)sequence);
        return (PersistentList<E>)builder.build();
    }
    
    public static final <E> PersistentList<E> minus(final PersistentList<? extends E> list, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentList.Builder<? extends E> builder = list.builder();
        CollectionsKt.removeAll((Collection)builder, (Object[])array);
        return (PersistentList<E>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> minus(final PersistentMap<? extends K, ? extends V> persistentMap, final Iterable<? extends K> iterable) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "keys");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        CollectionsKt.removeAll((Collection)builder.keySet(), (Iterable)iterable);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> minus(final PersistentMap<? extends K, ? extends V> persistentMap, final K k) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        return (PersistentMap<K, V>)persistentMap.remove((K)k);
    }
    
    public static final <K, V> PersistentMap<K, V> minus(final PersistentMap<? extends K, ? extends V> persistentMap, final Sequence<? extends K> sequence) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "keys");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        CollectionsKt.removeAll((Collection)builder.keySet(), (Sequence)sequence);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> minus(final PersistentMap<? extends K, ? extends V> persistentMap, final K[] array) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "keys");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        CollectionsKt.removeAll((Collection)builder.keySet(), (Object[])array);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <E> PersistentSet<E> minus(final PersistentSet<? extends E> set, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentSet<? extends E> set2;
        if (iterable instanceof Collection) {
            set2 = set.removeAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentSet.Builder<? extends E> builder = set.builder();
            CollectionsKt.removeAll((Collection)builder, (Iterable)iterable);
            set2 = builder.build();
        }
        return (PersistentSet<E>)set2;
    }
    
    public static final <E> PersistentSet<E> minus(final PersistentSet<? extends E> set, final E e) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return (PersistentSet<E>)set.remove((E)e);
    }
    
    public static final <E> PersistentSet<E> minus(final PersistentSet<? extends E> set, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentSet.Builder<? extends E> builder = set.builder();
        CollectionsKt.removeAll((Collection)builder, (Sequence)sequence);
        return (PersistentSet<E>)builder.build();
    }
    
    public static final <E> PersistentSet<E> minus(final PersistentSet<? extends E> set, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentSet.Builder<? extends E> builder = set.builder();
        CollectionsKt.removeAll((Collection)builder, (Object[])array);
        return (PersistentSet<E>)builder.build();
    }
    
    public static final <T> PersistentList<T> mutate(final PersistentList<? extends T> list, final Function1<? super List<T>, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "mutator");
        final PersistentList.Builder<? extends T> builder = list.builder();
        function1.invoke((Object)builder);
        return (PersistentList<T>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> mutate(final PersistentMap<? extends K, ? extends V> persistentMap, final Function1<? super Map<K, V>, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "mutator");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        function1.invoke((Object)builder);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <T> PersistentSet<T> mutate(final PersistentSet<? extends T> set, final Function1<? super Set<T>, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "mutator");
        final PersistentSet.Builder<? extends T> builder = set.builder();
        function1.invoke((Object)builder);
        return (PersistentSet<T>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> persistentHashMapOf() {
        return (PersistentMap)PersistentHashMap.Companion.emptyOf$runtime_release();
    }
    
    public static final <K, V> PersistentMap<K, V> persistentHashMapOf(final Pair<? extends K, ? extends V>... array) {
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final PersistentMap persistentMap = PersistentHashMap.Companion.emptyOf$runtime_release();
        Intrinsics.checkNotNull((Object)persistentMap, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate, V of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate>");
        final PersistentMap.Builder builder = persistentMap.builder();
        MapsKt.putAll((Map)builder, (Pair[])array);
        return builder.build();
    }
    
    public static final <E> PersistentSet<E> persistentHashSetOf() {
        return PersistentHashSet.Companion.emptyOf$runtime_release();
    }
    
    public static final <E> PersistentSet<E> persistentHashSetOf(final E... array) {
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        return PersistentHashSet.Companion.emptyOf$runtime_release().addAll((Collection<? extends E>)ArraysKt.asList((Object[])array));
    }
    
    public static final <E> PersistentList<E> persistentListOf() {
        return UtilsKt.persistentVectorOf();
    }
    
    public static final <E> PersistentList<E> persistentListOf(final E... array) {
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        return UtilsKt.persistentVectorOf().addAll((Collection<? extends E>)ArraysKt.asList((Object[])array));
    }
    
    public static final <K, V> PersistentMap<K, V> persistentMapOf() {
        return (PersistentMap)PersistentOrderedMap.Companion.emptyOf$runtime_release();
    }
    
    public static final <K, V> PersistentMap<K, V> persistentMapOf(final Pair<? extends K, ? extends V>... array) {
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final PersistentMap persistentMap = PersistentOrderedMap.Companion.emptyOf$runtime_release();
        Intrinsics.checkNotNull((Object)persistentMap, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<K of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate, V of androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt.mutate>");
        final PersistentMap.Builder builder = persistentMap.builder();
        MapsKt.putAll((Map)builder, (Pair[])array);
        return builder.build();
    }
    
    public static final <E> PersistentSet<E> persistentSetOf() {
        return PersistentOrderedSet.Companion.emptyOf$runtime_release();
    }
    
    public static final <E> PersistentSet<E> persistentSetOf(final E... array) {
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        return PersistentOrderedSet.Companion.emptyOf$runtime_release().addAll((Collection<? extends E>)ArraysKt.asList((Object[])array));
    }
    
    public static final <E> PersistentCollection<E> plus(final PersistentCollection<? extends E> collection, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentCollection<? extends E> collection2;
        if (iterable instanceof Collection) {
            collection2 = collection.addAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentCollection.Builder<? extends E> builder = collection.builder();
            CollectionsKt.addAll((Collection)builder, (Iterable)iterable);
            collection2 = builder.build();
        }
        return (PersistentCollection<E>)collection2;
    }
    
    public static final <E> PersistentCollection<E> plus(final PersistentCollection<? extends E> collection, final E e) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        return (PersistentCollection<E>)collection.add((E)e);
    }
    
    public static final <E> PersistentCollection<E> plus(final PersistentCollection<? extends E> collection, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentCollection.Builder<? extends E> builder = collection.builder();
        CollectionsKt.addAll((Collection)builder, (Sequence)sequence);
        return (PersistentCollection<E>)builder.build();
    }
    
    public static final <E> PersistentCollection<E> plus(final PersistentCollection<? extends E> collection, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)collection, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentCollection.Builder<? extends E> builder = collection.builder();
        CollectionsKt.addAll((Collection)builder, (Object[])array);
        return (PersistentCollection<E>)builder.build();
    }
    
    public static final <E> PersistentList<E> plus(final PersistentList<? extends E> list, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentList<? extends E> list2;
        if (iterable instanceof Collection) {
            list2 = list.addAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentList.Builder<? extends E> builder = list.builder();
            CollectionsKt.addAll((Collection)builder, (Iterable)iterable);
            list2 = builder.build();
        }
        return (PersistentList<E>)list2;
    }
    
    public static final <E> PersistentList<E> plus(final PersistentList<? extends E> list, final E e) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        return (PersistentList<E>)list.add((E)e);
    }
    
    public static final <E> PersistentList<E> plus(final PersistentList<? extends E> list, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentList.Builder<? extends E> builder = list.builder();
        CollectionsKt.addAll((Collection)builder, (Sequence)sequence);
        return (PersistentList<E>)builder.build();
    }
    
    public static final <E> PersistentList<E> plus(final PersistentList<? extends E> list, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)list, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentList.Builder<? extends E> builder = list.builder();
        CollectionsKt.addAll((Collection)builder, (Object[])array);
        return (PersistentList<E>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> plus(final PersistentMap<? extends K, ? extends V> persistentMap, final Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "pairs");
        return (PersistentMap<K, V>)putAll((PersistentMap<?, ?>)persistentMap, (Iterable<? extends kotlin.Pair<?, ?>>)iterable);
    }
    
    public static final <K, V> PersistentMap<K, V> plus(final PersistentMap<? extends K, ? extends V> persistentMap, final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)map, "map");
        return (PersistentMap<K, V>)putAll((PersistentMap<?, ?>)persistentMap, (Map<?, ?>)map);
    }
    
    public static final <K, V> PersistentMap<K, V> plus(final PersistentMap<? extends K, ? extends V> persistentMap, final Pair<? extends K, ? extends V> pair) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)pair, "pair");
        return (PersistentMap<K, V>)persistentMap.put((K)pair.getFirst(), (V)pair.getSecond());
    }
    
    public static final <K, V> PersistentMap<K, V> plus(final PersistentMap<? extends K, ? extends V> persistentMap, final Sequence<? extends Pair<? extends K, ? extends V>> sequence) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "pairs");
        return (PersistentMap<K, V>)putAll((PersistentMap<?, ?>)persistentMap, (kotlin.sequences.Sequence<? extends kotlin.Pair<?, ?>>)sequence);
    }
    
    public static final <K, V> PersistentMap<K, V> plus(final PersistentMap<? extends K, ? extends V> persistentMap, final Pair<? extends K, ? extends V>[] array) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        return (PersistentMap<K, V>)putAll((PersistentMap<?, ?>)persistentMap, (kotlin.Pair<?, ?>[])array);
    }
    
    public static final <E> PersistentSet<E> plus(final PersistentSet<? extends E> set, final Iterable<? extends E> iterable) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "elements");
        PersistentSet<? extends E> set2;
        if (iterable instanceof Collection) {
            set2 = set.addAll((Collection<? extends E>)iterable);
        }
        else {
            final PersistentSet.Builder<? extends E> builder = set.builder();
            CollectionsKt.addAll((Collection)builder, (Iterable)iterable);
            set2 = builder.build();
        }
        return (PersistentSet<E>)set2;
    }
    
    public static final <E> PersistentSet<E> plus(final PersistentSet<? extends E> set, final E e) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        return (PersistentSet<E>)set.add((E)e);
    }
    
    public static final <E> PersistentSet<E> plus(final PersistentSet<? extends E> set, final Sequence<? extends E> sequence) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "elements");
        final PersistentSet.Builder<? extends E> builder = set.builder();
        CollectionsKt.addAll((Collection)builder, (Sequence)sequence);
        return (PersistentSet<E>)builder.build();
    }
    
    public static final <E> PersistentSet<E> plus(final PersistentSet<? extends E> set, final E[] array) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        final PersistentSet.Builder<? extends E> builder = set.builder();
        CollectionsKt.addAll((Collection)builder, (Object[])array);
        return (PersistentSet<E>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> putAll(final PersistentMap<? extends K, ? extends V> persistentMap, final Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)iterable, "pairs");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        MapsKt.putAll((Map)builder, (Iterable)iterable);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> putAll(final PersistentMap<? extends K, ? extends V> persistentMap, final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)map, "map");
        return (PersistentMap<K, V>)persistentMap.putAll(map);
    }
    
    public static final <K, V> PersistentMap<K, V> putAll(final PersistentMap<? extends K, ? extends V> persistentMap, final Sequence<? extends Pair<? extends K, ? extends V>> sequence) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)sequence, "pairs");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        MapsKt.putAll((Map)builder, (Sequence)sequence);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final <K, V> PersistentMap<K, V> putAll(final PersistentMap<? extends K, ? extends V> persistentMap, final Pair<? extends K, ? extends V>[] array) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final PersistentMap.Builder<? extends K, ? extends V> builder = persistentMap.builder();
        MapsKt.putAll((Map)builder, (Pair[])array);
        return (PersistentMap<K, V>)builder.build();
    }
    
    public static final ImmutableList<Character> toImmutableList(final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        return toPersistentList(charSequence);
    }
    
    public static final <T> ImmutableList<T> toImmutableList(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "<this>");
        ImmutableList list;
        if (iterable instanceof ImmutableList) {
            list = (ImmutableList)iterable;
        }
        else {
            list = null;
        }
        ImmutableList list2 = list;
        if (list == null) {
            list2 = toPersistentList((Iterable<?>)iterable);
        }
        return list2;
    }
    
    public static final <T> ImmutableList<T> toImmutableList(final Sequence<? extends T> sequence) {
        Intrinsics.checkNotNullParameter((Object)sequence, "<this>");
        return (ImmutableList)toPersistentList((kotlin.sequences.Sequence<?>)sequence);
    }
    
    public static final <K, V> ImmutableMap<K, V> toImmutableMap(final Map<K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "<this>");
        final boolean b = map instanceof ImmutableMap;
        final PersistentMap persistentMap = null;
        ImmutableMap immutableMap;
        if (b) {
            immutableMap = (ImmutableMap)map;
        }
        else {
            immutableMap = null;
        }
        ImmutableMap immutableMap2 = immutableMap;
        if (immutableMap == null) {
            PersistentMap.Builder builder;
            if (map instanceof PersistentMap.Builder) {
                builder = (PersistentMap.Builder)map;
            }
            else {
                builder = null;
            }
            PersistentMap build = persistentMap;
            if (builder != null) {
                build = builder.build();
            }
            if (build != null) {
                immutableMap2 = build;
            }
            else {
                immutableMap2 = persistentMapOf().putAll((Map<?, ?>)map);
            }
        }
        return immutableMap2;
    }
    
    public static final <T> ImmutableSet<T> toImmutableSet(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "<this>");
        final boolean b = iterable instanceof ImmutableSet;
        final PersistentSet set = null;
        ImmutableSet set2;
        if (b) {
            set2 = (ImmutableSet)iterable;
        }
        else {
            set2 = null;
        }
        ImmutableSet set3 = set2;
        if (set2 == null) {
            PersistentSet.Builder builder;
            if (iterable instanceof PersistentSet.Builder) {
                builder = (PersistentSet.Builder)iterable;
            }
            else {
                builder = null;
            }
            PersistentSet build = set;
            if (builder != null) {
                build = builder.build();
            }
            if (build != null) {
                set3 = build;
            }
            else {
                set3 = plus(persistentSetOf(), (Iterable<?>)iterable);
            }
        }
        return set3;
    }
    
    public static final <T> ImmutableSet<T> toImmutableSet(final Sequence<? extends T> sequence) {
        Intrinsics.checkNotNullParameter((Object)sequence, "<this>");
        return (ImmutableSet)toPersistentSet((kotlin.sequences.Sequence<?>)sequence);
    }
    
    public static final PersistentSet<Character> toImmutableSet(final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        return toPersistentSet(charSequence);
    }
    
    public static final <K, V> PersistentMap<K, V> toPersistentHashMap(final Map<K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "<this>");
        final boolean b = map instanceof PersistentHashMap;
        PersistentHashMap build = null;
        PersistentHashMap persistentHashMap;
        if (b) {
            persistentHashMap = (PersistentHashMap)map;
        }
        else {
            persistentHashMap = null;
        }
        PersistentMap<Object, Object> putAll;
        if (persistentHashMap != null) {
            putAll = persistentHashMap;
        }
        else {
            PersistentHashMapBuilder persistentHashMapBuilder;
            if (map instanceof PersistentHashMapBuilder) {
                persistentHashMapBuilder = (PersistentHashMapBuilder)map;
            }
            else {
                persistentHashMapBuilder = null;
            }
            if (persistentHashMapBuilder != null) {
                build = persistentHashMapBuilder.build();
            }
            if (build != null) {
                putAll = build;
            }
            else {
                putAll = PersistentHashMap.Companion.emptyOf$runtime_release().putAll(map);
            }
        }
        return (PersistentMap<K, V>)putAll;
    }
    
    public static final PersistentSet<Character> toPersistentHashSet(final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        final PersistentSet.Builder<Character> builder = persistentHashSetOf().builder();
        StringsKt.toCollection(charSequence, (Collection)builder);
        return builder.build();
    }
    
    public static final <T> PersistentSet<T> toPersistentHashSet(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "<this>");
        final boolean b = iterable instanceof PersistentHashSet;
        PersistentHashSet build = null;
        PersistentHashSet set;
        if (b) {
            set = (PersistentHashSet)iterable;
        }
        else {
            set = null;
        }
        PersistentSet<Object> plus;
        if (set != null) {
            plus = set;
        }
        else {
            PersistentHashSetBuilder persistentHashSetBuilder;
            if (iterable instanceof PersistentHashSetBuilder) {
                persistentHashSetBuilder = (PersistentHashSetBuilder)iterable;
            }
            else {
                persistentHashSetBuilder = null;
            }
            if (persistentHashSetBuilder != null) {
                build = persistentHashSetBuilder.build();
            }
            if (build != null) {
                plus = build;
            }
            else {
                plus = plus(PersistentHashSet.Companion.emptyOf$runtime_release(), (Iterable<?>)iterable);
            }
        }
        return (PersistentSet<T>)plus;
    }
    
    public static final <T> PersistentSet<T> toPersistentHashSet(final Sequence<? extends T> sequence) {
        Intrinsics.checkNotNullParameter((Object)sequence, "<this>");
        return plus(persistentHashSetOf(), sequence);
    }
    
    public static final PersistentList<Character> toPersistentList(final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        final PersistentList.Builder<Character> builder = persistentListOf().builder();
        StringsKt.toCollection(charSequence, (Collection)builder);
        return builder.build();
    }
    
    public static final <T> PersistentList<T> toPersistentList(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "<this>");
        final boolean b = iterable instanceof PersistentList;
        final PersistentList list = null;
        PersistentList list2;
        if (b) {
            list2 = (PersistentList)iterable;
        }
        else {
            list2 = null;
        }
        PersistentList<Object> list3 = list2;
        if (list2 == null) {
            PersistentList.Builder builder;
            if (iterable instanceof PersistentList.Builder) {
                builder = (PersistentList.Builder)iterable;
            }
            else {
                builder = null;
            }
            list3 = list;
            if (builder != null) {
                list3 = builder.build();
            }
            if (list3 == null) {
                list3 = (PersistentList<Object>)plus(persistentListOf(), iterable);
            }
        }
        return (PersistentList<T>)list3;
    }
    
    public static final <T> PersistentList<T> toPersistentList(final Sequence<? extends T> sequence) {
        Intrinsics.checkNotNullParameter((Object)sequence, "<this>");
        return plus(persistentListOf(), sequence);
    }
    
    public static final <K, V> PersistentMap<K, V> toPersistentMap(final Map<K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "<this>");
        final boolean b = map instanceof PersistentOrderedMap;
        final PersistentMap persistentMap = null;
        PersistentOrderedMap persistentOrderedMap;
        if (b) {
            persistentOrderedMap = (PersistentOrderedMap)map;
        }
        else {
            persistentOrderedMap = null;
        }
        PersistentMap<Object, Object> putAll;
        if (persistentOrderedMap != null) {
            putAll = persistentOrderedMap;
        }
        else {
            PersistentOrderedMapBuilder persistentOrderedMapBuilder;
            if (map instanceof PersistentOrderedMapBuilder) {
                persistentOrderedMapBuilder = (PersistentOrderedMapBuilder)map;
            }
            else {
                persistentOrderedMapBuilder = null;
            }
            PersistentMap build = persistentMap;
            if (persistentOrderedMapBuilder != null) {
                build = persistentOrderedMapBuilder.build();
            }
            if (build == null) {
                putAll = PersistentOrderedMap.Companion.emptyOf$runtime_release().putAll(map);
            }
            else {
                putAll = build;
            }
        }
        return (PersistentMap<K, V>)putAll;
    }
    
    public static final PersistentSet<Character> toPersistentSet(final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        final PersistentSet.Builder<Character> builder = persistentSetOf().builder();
        StringsKt.toCollection(charSequence, (Collection)builder);
        return builder.build();
    }
    
    public static final <T> PersistentSet<T> toPersistentSet(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "<this>");
        final boolean b = iterable instanceof PersistentOrderedSet;
        final PersistentSet set = null;
        PersistentOrderedSet set2;
        if (b) {
            set2 = (PersistentOrderedSet)iterable;
        }
        else {
            set2 = null;
        }
        PersistentSet<Object> plus;
        if (set2 != null) {
            plus = set2;
        }
        else {
            PersistentOrderedSetBuilder persistentOrderedSetBuilder;
            if (iterable instanceof PersistentOrderedSetBuilder) {
                persistentOrderedSetBuilder = (PersistentOrderedSetBuilder)iterable;
            }
            else {
                persistentOrderedSetBuilder = null;
            }
            PersistentSet build = set;
            if (persistentOrderedSetBuilder != null) {
                build = persistentOrderedSetBuilder.build();
            }
            if (build == null) {
                plus = plus(PersistentOrderedSet.Companion.emptyOf$runtime_release(), (Iterable<?>)iterable);
            }
            else {
                plus = build;
            }
        }
        return (PersistentSet<T>)plus;
    }
    
    public static final <T> PersistentSet<T> toPersistentSet(final Sequence<? extends T> sequence) {
        Intrinsics.checkNotNullParameter((Object)sequence, "<this>");
        return plus(persistentSetOf(), sequence);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.persistentOrderedMap;

import kotlin.jvm.internal.TypeIntrinsics;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import androidx.compose.runtime.external.kotlinx.collections.immutable.internal.EndOfChain;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableIterator;
import java.util.Iterator;

@Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010)\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00040\u0003B#\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\b¢\u0006\u0002\u0010\tJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010\u001c\u001a\u00020\u001bH\u0002J\b\u0010\u001d\u001a\u00020\u001bH\u0002J\t\u0010\u001e\u001a\u00020\u0019H\u0096\u0002J\u000f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004H\u0096\u0002J\b\u0010 \u001a\u00020\u001bH\u0016R \u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\bX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u00020\rX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0006X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006!" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMapBuilderLinksIterator;", "K", "V", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/LinkedValue;", "nextKey", "", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMapBuilder;", "(Ljava/lang/Object;Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMapBuilder;)V", "getBuilder$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/persistentOrderedMap/PersistentOrderedMapBuilder;", "expectedModCount", "", "index", "getIndex$runtime_release", "()I", "setIndex$runtime_release", "(I)V", "lastIteratedKey", "getLastIteratedKey$runtime_release", "()Ljava/lang/Object;", "setLastIteratedKey$runtime_release", "(Ljava/lang/Object;)V", "nextWasInvoked", "", "checkForComodification", "", "checkHasNext", "checkNextWasInvoked", "hasNext", "next", "remove", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class PersistentOrderedMapBuilderLinksIterator<K, V> implements Iterator<LinkedValue<V>>, KMutableIterator
{
    private final PersistentOrderedMapBuilder<K, V> builder;
    private int expectedModCount;
    private int index;
    private Object lastIteratedKey;
    private Object nextKey;
    private boolean nextWasInvoked;
    
    public PersistentOrderedMapBuilderLinksIterator(final Object nextKey, final PersistentOrderedMapBuilder<K, V> builder) {
        Intrinsics.checkNotNullParameter((Object)builder, "builder");
        this.nextKey = nextKey;
        this.builder = builder;
        this.lastIteratedKey = EndOfChain.INSTANCE;
        this.expectedModCount = builder.getHashMapBuilder$runtime_release().getModCount$runtime_release();
    }
    
    private final void checkForComodification() {
        if (this.builder.getHashMapBuilder$runtime_release().getModCount$runtime_release() == this.expectedModCount) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    private final void checkHasNext() {
        if (this.hasNext()) {
            return;
        }
        throw new NoSuchElementException();
    }
    
    private final void checkNextWasInvoked() {
        if (this.nextWasInvoked) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public final PersistentOrderedMapBuilder<K, V> getBuilder$runtime_release() {
        return this.builder;
    }
    
    public final int getIndex$runtime_release() {
        return this.index;
    }
    
    public final Object getLastIteratedKey$runtime_release() {
        return this.lastIteratedKey;
    }
    
    @Override
    public boolean hasNext() {
        return this.index < this.builder.size();
    }
    
    @Override
    public LinkedValue<V> next() {
        this.checkForComodification();
        this.checkHasNext();
        this.lastIteratedKey = this.nextKey;
        this.nextWasInvoked = true;
        ++this.index;
        final LinkedValue value = this.builder.getHashMapBuilder$runtime_release().get(this.nextKey);
        if (value != null) {
            final LinkedValue linkedValue = value;
            this.nextKey = linkedValue.getNext();
            return linkedValue;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Hash code of a key (");
        sb.append(this.nextKey);
        sb.append(") has changed after it was added to the persistent map.");
        throw new ConcurrentModificationException(sb.toString());
    }
    
    @Override
    public void remove() {
        this.checkNextWasInvoked();
        TypeIntrinsics.asMutableMap((Object)this.builder).remove(this.lastIteratedKey);
        this.lastIteratedKey = null;
        this.nextWasInvoked = false;
        this.expectedModCount = this.builder.getHashMapBuilder$runtime_release().getModCount$runtime_release();
        --this.index;
    }
    
    public final void setIndex$runtime_release(final int index) {
        this.index = index;
    }
    
    public final void setLastIteratedKey$runtime_release(final Object lastIteratedKey) {
        this.lastIteratedKey = lastIteratedKey;
    }
}

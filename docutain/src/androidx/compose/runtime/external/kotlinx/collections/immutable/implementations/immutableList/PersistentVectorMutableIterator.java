// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList;

import kotlin.ranges.RangesKt;
import java.util.ConcurrentModificationException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableListIterator;
import java.util.ListIterator;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010+\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\f\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u001b\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u00020\u000eH\u0002J\b\u0010\u0012\u001a\u00020\u000eH\u0002J\u000e\u0010\u0013\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0014J\r\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\b\u0010\u0016\u001a\u00020\u000eH\u0016J\b\u0010\u0017\u001a\u00020\u000eH\u0002J\u0015\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010J\b\u0010\u0019\u001a\u00020\u000eH\u0002R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u001a" }, d2 = { "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorMutableIterator;", "T", "", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/AbstractListIterator;", "builder", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorBuilder;", "index", "", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/PersistentVectorBuilder;I)V", "expectedModCount", "lastIteratedIndex", "trieIterator", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/implementations/immutableList/TrieIterator;", "add", "", "element", "(Ljava/lang/Object;)V", "checkForComodification", "checkHasIterated", "next", "()Ljava/lang/Object;", "previous", "remove", "reset", "set", "setupTrieIterator", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PersistentVectorMutableIterator<T> extends AbstractListIterator<T> implements ListIterator<T>, KMutableListIterator
{
    private final PersistentVectorBuilder<T> builder;
    private int expectedModCount;
    private int lastIteratedIndex;
    private TrieIterator<? extends T> trieIterator;
    
    public PersistentVectorMutableIterator(final PersistentVectorBuilder<T> builder, final int n) {
        Intrinsics.checkNotNullParameter((Object)builder, "builder");
        super(n, builder.size());
        this.builder = builder;
        this.expectedModCount = builder.getModCount$runtime_release();
        this.lastIteratedIndex = -1;
        this.setupTrieIterator();
    }
    
    private final void checkForComodification() {
        if (this.expectedModCount == this.builder.getModCount$runtime_release()) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    private final void checkHasIterated() {
        if (this.lastIteratedIndex != -1) {
            return;
        }
        throw new IllegalStateException();
    }
    
    private final void reset() {
        this.setSize(this.builder.size());
        this.expectedModCount = this.builder.getModCount$runtime_release();
        this.lastIteratedIndex = -1;
        this.setupTrieIterator();
    }
    
    private final void setupTrieIterator() {
        final Object[] root$runtime_release = this.builder.getRoot$runtime_release();
        if (root$runtime_release == null) {
            this.trieIterator = null;
            return;
        }
        final int rootSize = UtilsKt.rootSize(this.builder.size());
        final int coerceAtMost = RangesKt.coerceAtMost(this.getIndex(), rootSize);
        final int n = this.builder.getRootShift$runtime_release() / 5 + 1;
        final TrieIterator<? extends T> trieIterator = this.trieIterator;
        if (trieIterator == null) {
            this.trieIterator = (TrieIterator<? extends T>)new TrieIterator<T>(root$runtime_release, coerceAtMost, rootSize, n);
        }
        else {
            Intrinsics.checkNotNull((Object)trieIterator);
            trieIterator.reset$runtime_release(root$runtime_release, coerceAtMost, rootSize, n);
        }
    }
    
    @Override
    public void add(final T t) {
        this.checkForComodification();
        this.builder.add(this.getIndex(), t);
        this.setIndex(this.getIndex() + 1);
        this.reset();
    }
    
    @Override
    public T next() {
        this.checkForComodification();
        this.checkHasNext$runtime_release();
        this.lastIteratedIndex = this.getIndex();
        final TrieIterator<? extends T> trieIterator = this.trieIterator;
        if (trieIterator == null) {
            final Object[] tail$runtime_release = this.builder.getTail$runtime_release();
            final int index = this.getIndex();
            this.setIndex(index + 1);
            return (T)tail$runtime_release[index];
        }
        if (trieIterator.hasNext()) {
            this.setIndex(this.getIndex() + 1);
            return (T)trieIterator.next();
        }
        final Object[] tail$runtime_release2 = this.builder.getTail$runtime_release();
        final int index2 = this.getIndex();
        this.setIndex(index2 + 1);
        return (T)tail$runtime_release2[index2 - trieIterator.getSize()];
    }
    
    @Override
    public T previous() {
        this.checkForComodification();
        this.checkHasPrevious$runtime_release();
        this.lastIteratedIndex = this.getIndex() - 1;
        final TrieIterator<? extends T> trieIterator = this.trieIterator;
        if (trieIterator == null) {
            final Object[] tail$runtime_release = this.builder.getTail$runtime_release();
            this.setIndex(this.getIndex() - 1);
            return (T)tail$runtime_release[this.getIndex()];
        }
        if (this.getIndex() > trieIterator.getSize()) {
            final Object[] tail$runtime_release2 = this.builder.getTail$runtime_release();
            this.setIndex(this.getIndex() - 1);
            return (T)tail$runtime_release2[this.getIndex() - trieIterator.getSize()];
        }
        this.setIndex(this.getIndex() - 1);
        return trieIterator.previous();
    }
    
    @Override
    public void remove() {
        this.checkForComodification();
        this.checkHasIterated();
        this.builder.remove(this.lastIteratedIndex);
        if (this.lastIteratedIndex < this.getIndex()) {
            this.setIndex(this.lastIteratedIndex);
        }
        this.reset();
    }
    
    @Override
    public void set(final T t) {
        this.checkForComodification();
        this.checkHasIterated();
        this.builder.set(this.lastIteratedIndex, t);
        this.expectedModCount = this.builder.getModCount$runtime_release();
        this.setupTrieIterator();
    }
}

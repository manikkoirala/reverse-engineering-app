// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Iterator;
import java.util.Collection;
import androidx.compose.runtime.collection.IdentityArraySet;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.collection.IdentityArrayIntMap;
import androidx.compose.runtime.collection.IdentityArrayMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000e\u00107\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u00108\u001a\u00020\u00102\u0006\u00109\u001a\u00020\u000eJ\u001c\u0010:\u001a\u0010\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020\u0010\u0018\u00010;2\u0006\u0010=\u001a\u00020\u000fJ\b\u0010>\u001a\u00020\u0010H\u0016J\u0010\u0010?\u001a\u00020@2\b\u0010\u0019\u001a\u0004\u0018\u00010/J\u0016\u0010A\u001a\u00020\u00122\u000e\u0010B\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010CJ\u000e\u0010D\u001a\u00020\u00102\u0006\u0010E\u001a\u00020/J\u0006\u0010F\u001a\u00020\u0010J\u0006\u0010G\u001a\u00020\u0010J\u0006\u0010H\u001a\u00020\u0010J\u000e\u0010I\u001a\u00020\u00102\u0006\u0010=\u001a\u00020\u000fJ\"\u0010J\u001a\u00020\u00102\u0018\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\rH\u0016R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\"\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R$\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001b\u0010\u0014\"\u0004\b\u001c\u0010\u001dR$\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001f\u0010\u0014\"\u0004\b \u0010\u001dR\u000e\u0010!\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\"\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b\"\u0010\u0014R$\u0010#\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b$\u0010\u0014\"\u0004\b%\u0010\u001dR$\u0010&\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128B@BX\u0082\u000e¢\u0006\f\u001a\u0004\b'\u0010\u0014\"\u0004\b(\u0010\u001dR$\u0010)\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128@@BX\u0080\u000e¢\u0006\f\u001a\u0004\b*\u0010\u0014\"\u0004\b+\u0010\u001dR\"\u0010,\u001a\u0016\u0012\b\u0012\u0006\u0012\u0002\b\u00030.\u0012\u0006\u0012\u0004\u0018\u00010/\u0018\u00010-X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u000101X\u0082\u000e¢\u0006\u0002\n\u0000R$\u00102\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00128F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b3\u0010\u0014\"\u0004\b4\u0010\u001dR\u0011\u00105\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b6\u0010\u0014¨\u0006K" }, d2 = { "Landroidx/compose/runtime/RecomposeScopeImpl;", "Landroidx/compose/runtime/ScopeUpdateScope;", "Landroidx/compose/runtime/RecomposeScope;", "composition", "Landroidx/compose/runtime/CompositionImpl;", "(Landroidx/compose/runtime/CompositionImpl;)V", "anchor", "Landroidx/compose/runtime/Anchor;", "getAnchor", "()Landroidx/compose/runtime/Anchor;", "setAnchor", "(Landroidx/compose/runtime/Anchor;)V", "block", "Lkotlin/Function2;", "Landroidx/compose/runtime/Composer;", "", "", "canRecompose", "", "getCanRecompose", "()Z", "<set-?>", "getComposition", "()Landroidx/compose/runtime/CompositionImpl;", "currentToken", "value", "defaultsInScope", "getDefaultsInScope", "setDefaultsInScope", "(Z)V", "defaultsInvalid", "getDefaultsInvalid", "setDefaultsInvalid", "flags", "isConditional", "requiresRecompose", "getRequiresRecompose", "setRequiresRecompose", "rereading", "getRereading", "setRereading", "skipped", "getSkipped$runtime_release", "setSkipped", "trackedDependencies", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Landroidx/compose/runtime/DerivedState;", "", "trackedInstances", "Landroidx/compose/runtime/collection/IdentityArrayIntMap;", "used", "getUsed", "setUsed", "valid", "getValid", "adoptedBy", "compose", "composer", "end", "Lkotlin/Function1;", "Landroidx/compose/runtime/Composition;", "token", "invalidate", "invalidateForResult", "Landroidx/compose/runtime/InvalidationResult;", "isInvalidFor", "instances", "Landroidx/compose/runtime/collection/IdentityArraySet;", "recordRead", "instance", "release", "rereadTrackedInstances", "scopeSkipped", "start", "updateScope", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class RecomposeScopeImpl implements ScopeUpdateScope, RecomposeScope
{
    private Anchor anchor;
    private Function2<? super Composer, ? super Integer, Unit> block;
    private CompositionImpl composition;
    private int currentToken;
    private int flags;
    private IdentityArrayMap<DerivedState<?>, Object> trackedDependencies;
    private IdentityArrayIntMap trackedInstances;
    
    public RecomposeScopeImpl(final CompositionImpl composition) {
        this.composition = composition;
    }
    
    private final boolean getRereading() {
        return (this.flags & 0x20) != 0x0;
    }
    
    private final void setRereading(final boolean b) {
        if (b) {
            this.flags |= 0x20;
        }
        else {
            this.flags &= 0xFFFFFFDF;
        }
    }
    
    private final void setSkipped(final boolean b) {
        if (b) {
            this.flags |= 0x10;
        }
        else {
            this.flags &= 0xFFFFFFEF;
        }
    }
    
    public final void adoptedBy(final CompositionImpl composition) {
        Intrinsics.checkNotNullParameter((Object)composition, "composition");
        this.composition = composition;
    }
    
    public final void compose(final Composer composer) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        final Function2<? super Composer, ? super Integer, Unit> block = this.block;
        Unit instance;
        if (block != null) {
            block.invoke((Object)composer, (Object)1);
            instance = Unit.INSTANCE;
        }
        else {
            instance = null;
        }
        if (instance != null) {
            return;
        }
        throw new IllegalStateException("Invalid restart scope".toString());
    }
    
    public final Function1<Composition, Unit> end(final int n) {
        final IdentityArrayIntMap trackedInstances = this.trackedInstances;
        Function1 function2;
        final Function1 function1 = function2 = null;
        if (trackedInstances != null) {
            function2 = function1;
            if (!this.getSkipped$runtime_release()) {
                final int size = trackedInstances.getSize();
                final int n2 = 0;
                int n3 = 0;
                int n4;
                while (true) {
                    n4 = n2;
                    if (n3 >= size) {
                        break;
                    }
                    Intrinsics.checkNotNull(trackedInstances.getKeys()[n3], "null cannot be cast to non-null type kotlin.Any");
                    if (trackedInstances.getValues()[n3] != n) {
                        n4 = 1;
                        break;
                    }
                    ++n3;
                }
                function2 = function1;
                if (n4 != 0) {
                    function2 = (Function1)new RecomposeScopeImpl$end$1.RecomposeScopeImpl$end$1$2(this, n, trackedInstances);
                }
            }
        }
        return (Function1<Composition, Unit>)function2;
    }
    
    public final Anchor getAnchor() {
        return this.anchor;
    }
    
    public final boolean getCanRecompose() {
        return this.block != null;
    }
    
    public final CompositionImpl getComposition() {
        return this.composition;
    }
    
    public final boolean getDefaultsInScope() {
        return (this.flags & 0x2) != 0x0;
    }
    
    public final boolean getDefaultsInvalid() {
        return (this.flags & 0x4) != 0x0;
    }
    
    public final boolean getRequiresRecompose() {
        return (this.flags & 0x8) != 0x0;
    }
    
    public final boolean getSkipped$runtime_release() {
        return (this.flags & 0x10) != 0x0;
    }
    
    public final boolean getUsed() {
        final int flags = this.flags;
        boolean b = true;
        if ((flags & 0x1) == 0x0) {
            b = false;
        }
        return b;
    }
    
    public final boolean getValid() {
        final CompositionImpl composition = this.composition;
        boolean b = false;
        if (composition != null) {
            final Anchor anchor = this.anchor;
            final boolean b2 = anchor != null && anchor.getValid();
            b = b;
            if (b2) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public void invalidate() {
        final CompositionImpl composition = this.composition;
        if (composition != null) {
            composition.invalidate(this, null);
        }
    }
    
    public final InvalidationResult invalidateForResult(final Object o) {
        final CompositionImpl composition = this.composition;
        InvalidationResult invalidationResult;
        if (composition == null || (invalidationResult = composition.invalidate(this, o)) == null) {
            invalidationResult = InvalidationResult.IGNORED;
        }
        return invalidationResult;
    }
    
    public final boolean isConditional() {
        return this.trackedDependencies != null;
    }
    
    public final boolean isInvalidFor(final IdentityArraySet<Object> set) {
        if (set == null) {
            return true;
        }
        final IdentityArrayMap<DerivedState<?>, Object> trackedDependencies = this.trackedDependencies;
        if (trackedDependencies == null) {
            return true;
        }
        if (set.isNotEmpty()) {
            final Iterable iterable = set;
            boolean b2 = false;
            Label_0148: {
                if (!(iterable instanceof Collection) || !((Collection)iterable).isEmpty()) {
                    for (final Object next : iterable) {
                        boolean b = false;
                        Label_0142: {
                            if (next instanceof DerivedState) {
                                final DerivedState derivedState = (DerivedState)next;
                                SnapshotMutationPolicy<Object> snapshotMutationPolicy;
                                if ((snapshotMutationPolicy = derivedState.getPolicy()) == null) {
                                    snapshotMutationPolicy = SnapshotStateKt.structuralEqualityPolicy();
                                }
                                if (snapshotMutationPolicy.equivalent(derivedState.getCurrentValue(), trackedDependencies.get(derivedState))) {
                                    b = true;
                                    break Label_0142;
                                }
                            }
                            b = false;
                        }
                        if (!b) {
                            b2 = false;
                            break Label_0148;
                        }
                    }
                }
                b2 = true;
            }
            if (b2) {
                return false;
            }
        }
        return true;
    }
    
    public final void recordRead(final Object o) {
        Intrinsics.checkNotNullParameter(o, "instance");
        if (this.getRereading()) {
            return;
        }
        IdentityArrayIntMap trackedInstances;
        if ((trackedInstances = this.trackedInstances) == null) {
            trackedInstances = new IdentityArrayIntMap();
            this.trackedInstances = trackedInstances;
        }
        trackedInstances.add(o, this.currentToken);
        if (o instanceof DerivedState) {
            IdentityArrayMap<DerivedState<?>, Object> trackedDependencies;
            if ((trackedDependencies = this.trackedDependencies) == null) {
                trackedDependencies = new IdentityArrayMap<DerivedState<?>, Object>(0, 1, null);
                this.trackedDependencies = trackedDependencies;
            }
            trackedDependencies.set((DerivedState<?>)o, ((DerivedState)o).getCurrentValue());
        }
    }
    
    public final void release() {
        this.composition = null;
        this.trackedInstances = null;
        this.trackedDependencies = null;
    }
    
    public final void rereadTrackedInstances() {
        final CompositionImpl composition = this.composition;
        if (composition != null) {
            final IdentityArrayIntMap trackedInstances = this.trackedInstances;
            if (trackedInstances != null) {
                this.setRereading(true);
                try {
                    for (int size = trackedInstances.getSize(), i = 0; i < size; ++i) {
                        final Object o = trackedInstances.getKeys()[i];
                        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Any");
                        final int n = trackedInstances.getValues()[i];
                        composition.recordReadOf(o);
                    }
                }
                finally {
                    this.setRereading(false);
                }
            }
        }
    }
    
    public final void scopeSkipped() {
        this.setSkipped(true);
    }
    
    public final void setAnchor(final Anchor anchor) {
        this.anchor = anchor;
    }
    
    public final void setDefaultsInScope(final boolean b) {
        if (b) {
            this.flags |= 0x2;
        }
        else {
            this.flags &= 0xFFFFFFFD;
        }
    }
    
    public final void setDefaultsInvalid(final boolean b) {
        if (b) {
            this.flags |= 0x4;
        }
        else {
            this.flags &= 0xFFFFFFFB;
        }
    }
    
    public final void setRequiresRecompose(final boolean b) {
        if (b) {
            this.flags |= 0x8;
        }
        else {
            this.flags &= 0xFFFFFFF7;
        }
    }
    
    public final void setUsed(final boolean b) {
        if (b) {
            this.flags |= 0x1;
        }
        else {
            this.flags &= 0xFFFFFFFE;
        }
    }
    
    public final void start(final int currentToken) {
        this.currentToken = currentToken;
        this.setSkipped(false);
    }
    
    @Override
    public void updateScope(final Function2<? super Composer, ? super Integer, Unit> block) {
        Intrinsics.checkNotNullParameter((Object)block, "block");
        this.block = block;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import androidx.camera.view.PreviewView$1$$ExternalSyntheticBackportWithForwarding0;
import kotlin.Pair;
import kotlin.collections.CollectionsKt;
import kotlin.collections.ArraysKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.functions.Function0;
import kotlin.KotlinNothingValueException;
import java.util.Iterator;
import java.util.Collection;
import kotlin.jvm.internal.Ref$ObjectRef;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicReference;
import androidx.compose.runtime.collection.IdentityArraySet;
import androidx.compose.runtime.collection.IdentityArrayMap;
import androidx.compose.runtime.collection.IdentityScopeMap;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import java.util.List;
import java.util.HashSet;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u00e2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0015\b\u0000\u0018\u00002\u00020\u0001:\u0002\u0091\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\b\u0010T\u001a\u00020\u001bH\u0002J\u001e\u0010U\u001a\u00020\u001b2\f\u0010V\u001a\b\u0012\u0004\u0012\u00020.0W2\u0006\u0010X\u001a\u00020\u000fH\u0002J\b\u0010Y\u001a\u00020\u001bH\u0016Jc\u0010Z\u001a\u00020\u001b2Y\u0010\u0012\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\u0017¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0019¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u001a\u0012\u0004\u0012\u00020\u001b0\u0014j\u0002`\u001c0\u0013H\u0002J\b\u0010[\u001a\u00020\u001bH\u0016J\b\u0010\\\u001a\u00020\u001bH\u0016J\b\u0010]\u001a\u00020\u001bH\u0002J \u0010^\u001a\u00020\u001b2\u0011\u0010_\u001a\r\u0012\u0004\u0012\u00020\u001b0\u001e¢\u0006\u0002\b\u001fH\u0016¢\u0006\u0002\u0010#J3\u0010`\u001a\u0002Ha\"\u0004\b\u0000\u0010a2\b\u0010b\u001a\u0004\u0018\u00010\u00012\u0006\u0010c\u001a\u00020:2\f\u0010d\u001a\b\u0012\u0004\u0012\u0002Ha0\u001eH\u0016¢\u0006\u0002\u0010eJ\b\u0010f\u001a\u00020\u001bH\u0016J\u0010\u0010g\u001a\u00020\u001b2\u0006\u0010h\u001a\u00020iH\u0016J\b\u0010j\u001a\u00020\u001bH\u0002J\b\u0010k\u001a\u00020\u001bH\u0002J\"\u0010l\u001a\u0002Hm\"\u0004\b\u0000\u0010m2\f\u0010d\u001a\b\u0012\u0004\u0012\u0002Hm0\u001eH\u0082\b¢\u0006\u0002\u0010nJK\u0010o\u001a\u0002Hm\"\u0004\b\u0000\u0010m25\u0010d\u001a1\u0012'\u0012%\u0012\u0004\u0012\u00020)\u0012\f\u0012\n\u0012\u0004\u0012\u00020.\u0018\u00010=0<¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u0002Hm0pH\u0082\b¢\u0006\u0002\u0010qJ$\u0010r\u001a\u00020\u001b2\u001a\u0010s\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020u\u0012\u0006\u0012\u0004\u0018\u00010u0t0(H\u0016J\u0018\u0010v\u001a\u00020w2\u0006\u0010x\u001a\u00020)2\b\u0010y\u001a\u0004\u0018\u00010.J\b\u0010z\u001a\u00020\u001bH\u0016J\"\u0010{\u001a\u00020w2\u0006\u0010x\u001a\u00020)2\u0006\u0010|\u001a\u00020}2\b\u0010y\u001a\u0004\u0018\u00010.H\u0002J\u000e\u0010~\u001a\u00020\u001b2\u0006\u0010\u007f\u001a\u00020:J\u0012\u0010\u0080\u0001\u001a\u00020\u001b2\u0007\u0010\u0081\u0001\u001a\u00020.H\u0002J\u0017\u0010\u0082\u0001\u001a\u00020\u000f2\f\u0010V\u001a\b\u0012\u0004\u0012\u00020.0WH\u0016J\u0017\u0010\u0083\u0001\u001a\u00020\u001b2\f\u0010d\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001eH\u0016J\t\u0010\u0084\u0001\u001a\u00020\u000fH\u0016J\u0017\u0010\u0085\u0001\u001a\u00020\u001b2\f\u0010V\u001a\b\u0012\u0004\u0012\u00020.0WH\u0016J\u0012\u0010\u0086\u0001\u001a\u00020\u001b2\u0007\u0010\u0081\u0001\u001a\u00020.H\u0016J\u0012\u0010\u0087\u0001\u001a\u00020\u001b2\u0007\u0010\u0081\u0001\u001a\u00020.H\u0016J\u001b\u0010\u0088\u0001\u001a\u00020\u001b2\n\u0010h\u001a\u0006\u0012\u0002\b\u000302H\u0000¢\u0006\u0003\b\u0089\u0001J\u001f\u0010\u008a\u0001\u001a\u00020\u001b2\u0006\u0010y\u001a\u00020.2\u0006\u0010x\u001a\u00020)H\u0000¢\u0006\u0003\b\u008b\u0001J!\u0010\u008c\u0001\u001a\u00020\u001b2\u0011\u0010_\u001a\r\u0012\u0004\u0012\u00020\u001b0\u001e¢\u0006\u0002\b\u001fH\u0016¢\u0006\u0002\u0010#J\u001d\u0010\u008d\u0001\u001a\u0016\u0012\u0004\u0012\u00020)\u0012\f\u0012\n\u0012\u0004\u0012\u00020.\u0018\u00010=0<H\u0002J#\u0010\u008e\u0001\u001a\u0002Hm\"\u0004\b\u0000\u0010m2\f\u0010d\u001a\b\u0012\u0004\u0012\u0002Hm0\u001eH\u0082\b¢\u0006\u0002\u0010nJ\u0011\u0010\u008f\u0001\u001a\u00020\u001b2\u0006\u0010P\u001a\u00020QH\u0002J\t\u0010\u0090\u0001\u001a\u00020\u001bH\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011Ra\u0010\u0012\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\u0017¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0019¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u001a\u0012\u0004\u0012\u00020\u001b0\u0014j\u0002`\u001c0\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R'\u0010\u001d\u001a\r\u0012\u0004\u0012\u00020\u001b0\u001e¢\u0006\u0002\b\u001fX\u0086\u000e¢\u0006\u0010\n\u0002\u0010$\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u000e\u0010%\u001a\u00020&X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b*\u0010+R\u001e\u0010,\u001a\u0012\u0012\u0004\u0012\u00020)0\u000bj\b\u0012\u0004\u0012\u00020)`\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010-\u001a\b\u0012\u0004\u0012\u00020.0(8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b/\u0010+R\u0018\u00100\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030201X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u00104\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b5\u0010\u0011R\u0014\u00106\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b7\u0010\u0011R\u0010\u00108\u001a\u0004\u0018\u00010\u0000X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020:X\u0082\u000e¢\u0006\u0002\n\u0000R\"\u0010;\u001a\u0016\u0012\u0004\u0012\u00020)\u0012\f\u0012\n\u0012\u0004\u0012\u00020.\u0018\u00010=0<X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010>\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b>\u0010\u0011R\u0014\u0010?\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b?\u0010\u0011R\u0011\u0010@\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b@\u0010\u0011Ra\u0010A\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0005¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\u0017¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0019¢\u0006\f\b\u0015\u0012\b\b\u0016\u0012\u0004\b\b(\u001a\u0012\u0004\u0012\u00020\u001b0\u0014j\u0002`\u001c0\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020.X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010C\u001a\b\u0012\u0004\u0012\u00020)01X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010D\u001a\b\u0012\u0004\u0012\u00020)01X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010E\u001a\b\u0012\u0004\u0012\u00020.0(8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\bF\u0010+R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010G\u001a\u00020\u000fX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bH\u0010\u0011\"\u0004\bI\u0010JR\"\u0010K\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010.0Lj\n\u0012\u0006\u0012\u0004\u0018\u00010.`MX\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u00078F¢\u0006\u0006\u001a\u0004\bN\u0010OR\u0014\u0010P\u001a\u00020QX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\bR\u0010S¨\u0006\u0092\u0001" }, d2 = { "Landroidx/compose/runtime/CompositionImpl;", "Landroidx/compose/runtime/ControlledComposition;", "parent", "Landroidx/compose/runtime/CompositionContext;", "applier", "Landroidx/compose/runtime/Applier;", "recomposeContext", "Lkotlin/coroutines/CoroutineContext;", "(Landroidx/compose/runtime/CompositionContext;Landroidx/compose/runtime/Applier;Lkotlin/coroutines/CoroutineContext;)V", "_recomposeContext", "abandonSet", "Ljava/util/HashSet;", "Landroidx/compose/runtime/RememberObserver;", "Lkotlin/collections/HashSet;", "areChildrenComposing", "", "getAreChildrenComposing", "()Z", "changes", "", "Lkotlin/Function3;", "Lkotlin/ParameterName;", "name", "Landroidx/compose/runtime/SlotWriter;", "slots", "Landroidx/compose/runtime/RememberManager;", "rememberManager", "", "Landroidx/compose/runtime/Change;", "composable", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "getComposable", "()Lkotlin/jvm/functions/Function2;", "setComposable", "(Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function2;", "composer", "Landroidx/compose/runtime/ComposerImpl;", "conditionalScopes", "", "Landroidx/compose/runtime/RecomposeScopeImpl;", "getConditionalScopes$runtime_release", "()Ljava/util/List;", "conditionallyInvalidatedScopes", "derivedStateDependencies", "", "getDerivedStateDependencies$runtime_release", "derivedStates", "Landroidx/compose/runtime/collection/IdentityScopeMap;", "Landroidx/compose/runtime/DerivedState;", "disposed", "hasInvalidations", "getHasInvalidations", "hasPendingChanges", "getHasPendingChanges", "invalidationDelegate", "invalidationDelegateGroup", "", "invalidations", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Landroidx/compose/runtime/collection/IdentityArraySet;", "isComposing", "isDisposed", "isRoot", "lateChanges", "lock", "observations", "observationsProcessed", "observedObjects", "getObservedObjects$runtime_release", "pendingInvalidScopes", "getPendingInvalidScopes$runtime_release", "setPendingInvalidScopes$runtime_release", "(Z)V", "pendingModifications", "Ljava/util/concurrent/atomic/AtomicReference;", "Landroidx/compose/runtime/AtomicReference;", "getRecomposeContext", "()Lkotlin/coroutines/CoroutineContext;", "slotTable", "Landroidx/compose/runtime/SlotTable;", "getSlotTable$runtime_release", "()Landroidx/compose/runtime/SlotTable;", "abandonChanges", "addPendingInvalidationsLocked", "values", "", "forgetConditionalScopes", "applyChanges", "applyChangesInLocked", "applyLateChanges", "changesApplied", "cleanUpDerivedStateObservations", "composeContent", "content", "delegateInvalidations", "R", "to", "groupIndex", "block", "(Landroidx/compose/runtime/ControlledComposition;ILkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "dispose", "disposeUnusedMovableContent", "state", "Landroidx/compose/runtime/MovableContentState;", "drainPendingModificationsForCompositionLocked", "drainPendingModificationsLocked", "guardChanges", "T", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "guardInvalidationsLocked", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "insertMovableContent", "references", "Lkotlin/Pair;", "Landroidx/compose/runtime/MovableContentStateReference;", "invalidate", "Landroidx/compose/runtime/InvalidationResult;", "scope", "instance", "invalidateAll", "invalidateChecked", "anchor", "Landroidx/compose/runtime/Anchor;", "invalidateGroupsWithKey", "key", "invalidateScopeOfLocked", "value", "observesAnyOf", "prepareCompose", "recompose", "recordModificationsOf", "recordReadOf", "recordWriteOf", "removeDerivedStateObservation", "removeDerivedStateObservation$runtime_release", "removeObservation", "removeObservation$runtime_release", "setContent", "takeInvalidations", "trackAbandonedValues", "validateRecomposeScopeAnchors", "verifyConsistent", "RememberEventDispatcher", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class CompositionImpl implements ControlledComposition
{
    private final CoroutineContext _recomposeContext;
    private final HashSet<RememberObserver> abandonSet;
    private final Applier<?> applier;
    private final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> changes;
    private Function2<? super Composer, ? super Integer, Unit> composable;
    private final ComposerImpl composer;
    private final HashSet<RecomposeScopeImpl> conditionallyInvalidatedScopes;
    private final IdentityScopeMap<DerivedState<?>> derivedStates;
    private boolean disposed;
    private CompositionImpl invalidationDelegate;
    private int invalidationDelegateGroup;
    private IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> invalidations;
    private final boolean isRoot;
    private final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> lateChanges;
    private final Object lock;
    private final IdentityScopeMap<RecomposeScopeImpl> observations;
    private final IdentityScopeMap<RecomposeScopeImpl> observationsProcessed;
    private final CompositionContext parent;
    private boolean pendingInvalidScopes;
    private final AtomicReference<Object> pendingModifications;
    private final SlotTable slotTable;
    
    public CompositionImpl(final CompositionContext parent, final Applier<?> applier, final CoroutineContext recomposeContext) {
        Intrinsics.checkNotNullParameter((Object)parent, "parent");
        Intrinsics.checkNotNullParameter((Object)applier, "applier");
        this.parent = parent;
        this.applier = applier;
        this.pendingModifications = new AtomicReference<Object>(null);
        this.lock = new Object();
        final HashSet abandonSet = new HashSet();
        this.abandonSet = abandonSet;
        final SlotTable slotTable = new SlotTable();
        this.slotTable = slotTable;
        this.observations = new IdentityScopeMap<RecomposeScopeImpl>();
        this.conditionallyInvalidatedScopes = new HashSet<RecomposeScopeImpl>();
        this.derivedStates = new IdentityScopeMap<DerivedState<?>>();
        final List changes = new ArrayList();
        this.changes = changes;
        final List lateChanges = new ArrayList();
        this.lateChanges = lateChanges;
        this.observationsProcessed = new IdentityScopeMap<RecomposeScopeImpl>();
        this.invalidations = new IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>>(0, 1, null);
        final ComposerImpl composer = new ComposerImpl(applier, parent, slotTable, abandonSet, changes, lateChanges, this);
        parent.registerComposer$runtime_release(composer);
        this.composer = composer;
        this._recomposeContext = recomposeContext;
        this.isRoot = (parent instanceof Recomposer);
        this.composable = ComposableSingletons$CompositionKt.INSTANCE.getLambda-1$runtime_release();
    }
    
    private final void abandonChanges() {
        this.pendingModifications.set(null);
        this.changes.clear();
        this.lateChanges.clear();
        this.abandonSet.clear();
    }
    
    private final void addPendingInvalidationsLocked(final Set<?> set, final boolean b) {
        final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        for (final Object next : set) {
            if (next instanceof RecomposeScopeImpl) {
                ((RecomposeScopeImpl)next).invalidateForResult(null);
            }
            else {
                addPendingInvalidationsLocked$invalidate(this, b, (Ref$ObjectRef<HashSet<RecomposeScopeImpl>>)ref$ObjectRef, next);
                final IdentityScopeMap<DerivedState<?>> derivedStates = this.derivedStates;
                final int access$find = IdentityScopeMap.access$find((IdentityScopeMap<Object>)derivedStates, next);
                if (access$find < 0) {
                    continue;
                }
                final IdentityArraySet access$scopeSet = IdentityScopeMap.access$scopeSetAt((IdentityScopeMap<Object>)derivedStates, access$find);
                for (int size = access$scopeSet.size(), i = 0; i < size; ++i) {
                    addPendingInvalidationsLocked$invalidate(this, b, (Ref$ObjectRef<HashSet<RecomposeScopeImpl>>)ref$ObjectRef, access$scopeSet.get(i));
                }
            }
        }
        if (b && (this.conditionallyInvalidatedScopes.isEmpty() ^ true)) {
            final IdentityScopeMap<RecomposeScopeImpl> observations = this.observations;
            final int size2 = observations.getSize();
            int j = 0;
            int size3 = 0;
            while (j < size2) {
                final int n = observations.getValueOrder()[j];
                final IdentityArraySet set2 = observations.getScopeSets()[n];
                Intrinsics.checkNotNull((Object)set2);
                final int size4 = set2.size();
                int k = 0;
                int size5 = 0;
                while (k < size4) {
                    final Object o = set2.getValues()[k];
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
                    final RecomposeScopeImpl recomposeScopeImpl = (RecomposeScopeImpl)o;
                    boolean b2 = false;
                    Label_0303: {
                        if (!this.conditionallyInvalidatedScopes.contains(recomposeScopeImpl)) {
                            final HashSet set3 = (HashSet)ref$ObjectRef.element;
                            if (set3 == null || !set3.contains(recomposeScopeImpl)) {
                                b2 = false;
                                break Label_0303;
                            }
                        }
                        b2 = true;
                    }
                    int n2 = size5;
                    if (!b2) {
                        if (size5 != k) {
                            set2.getValues()[size5] = o;
                        }
                        n2 = size5 + 1;
                    }
                    ++k;
                    size5 = n2;
                }
                for (int size6 = set2.size(), l = size5; l < size6; ++l) {
                    set2.getValues()[l] = null;
                }
                set2.setSize(size5);
                int n3 = size3;
                if (set2.size() > 0) {
                    if (size3 != j) {
                        final int n4 = observations.getValueOrder()[size3];
                        observations.getValueOrder()[size3] = n;
                        observations.getValueOrder()[j] = n4;
                    }
                    n3 = size3 + 1;
                }
                ++j;
                size3 = n3;
            }
            for (int size7 = observations.getSize(), n5 = size3; n5 < size7; ++n5) {
                observations.getValues()[observations.getValueOrder()[n5]] = null;
            }
            observations.setSize(size3);
            this.cleanUpDerivedStateObservations();
            this.conditionallyInvalidatedScopes.clear();
        }
        else {
            final HashSet set4 = (HashSet)ref$ObjectRef.element;
            if (set4 != null) {
                final IdentityScopeMap<RecomposeScopeImpl> observations2 = this.observations;
                final int size8 = observations2.getSize();
                int n6 = 0;
                int size9 = 0;
                while (n6 < size8) {
                    final int n7 = observations2.getValueOrder()[n6];
                    final IdentityArraySet set5 = observations2.getScopeSets()[n7];
                    Intrinsics.checkNotNull((Object)set5);
                    final int size10 = set5.size();
                    int n8 = 0;
                    int size11 = 0;
                    while (n8 < size10) {
                        final Object o2 = set5.getValues()[n8];
                        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
                        int n9 = size11;
                        if (!set4.contains(o2)) {
                            if (size11 != n8) {
                                set5.getValues()[size11] = o2;
                            }
                            n9 = size11 + 1;
                        }
                        ++n8;
                        size11 = n9;
                    }
                    for (int size12 = set5.size(), n10 = size11; n10 < size12; ++n10) {
                        set5.getValues()[n10] = null;
                    }
                    set5.setSize(size11);
                    int n11 = size9;
                    if (set5.size() > 0) {
                        if (size9 != n6) {
                            final int n12 = observations2.getValueOrder()[size9];
                            observations2.getValueOrder()[size9] = n7;
                            observations2.getValueOrder()[n6] = n12;
                        }
                        n11 = size9 + 1;
                    }
                    ++n6;
                    size9 = n11;
                }
                for (int size13 = observations2.getSize(), n13 = size9; n13 < size13; ++n13) {
                    observations2.getValues()[observations2.getValueOrder()[n13]] = null;
                }
                observations2.setSize(size9);
                this.cleanUpDerivedStateObservations();
            }
        }
    }
    
    private static final void addPendingInvalidationsLocked$invalidate(final CompositionImpl compositionImpl, final boolean b, final Ref$ObjectRef<HashSet<RecomposeScopeImpl>> ref$ObjectRef, final Object o) {
        final IdentityScopeMap<RecomposeScopeImpl> observations = compositionImpl.observations;
        final int access$find = IdentityScopeMap.access$find((IdentityScopeMap<Object>)observations, o);
        if (access$find >= 0) {
            final IdentityArraySet access$scopeSet = IdentityScopeMap.access$scopeSetAt((IdentityScopeMap<Object>)observations, access$find);
            for (int i = 0; i < access$scopeSet.size(); ++i) {
                final RecomposeScopeImpl recomposeScopeImpl = access$scopeSet.get(i);
                if (!compositionImpl.observationsProcessed.remove(o, recomposeScopeImpl) && recomposeScopeImpl.invalidateForResult(o) != InvalidationResult.IGNORED) {
                    if (recomposeScopeImpl.isConditional() && !b) {
                        compositionImpl.conditionallyInvalidatedScopes.add(recomposeScopeImpl);
                    }
                    else {
                        HashSet element;
                        if ((element = (HashSet)ref$ObjectRef.element) == null) {
                            element = new HashSet();
                            ref$ObjectRef.element = element;
                        }
                        element.add(recomposeScopeImpl);
                    }
                }
            }
        }
    }
    
    private final void applyChangesInLocked(List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> beginSection) {
        final RememberEventDispatcher rememberEventDispatcher = new RememberEventDispatcher(this.abandonSet);
        try {
            if (((List)beginSection).isEmpty()) {
                return;
            }
            final Object beginSection2 = Trace.INSTANCE.beginSection("Compose:applyChanges");
            try {
                this.applier.onBeginChanges();
                Object openWriter = this.slotTable.openWriter();
                try {
                    final Applier<?> applier = this.applier;
                    for (int size = ((List)beginSection).size(), i = 0; i < size; ++i) {
                        ((Function3)((List)beginSection).get(i)).invoke((Object)applier, openWriter, (Object)rememberEventDispatcher);
                    }
                    ((List)beginSection).clear();
                    final Unit instance = Unit.INSTANCE;
                    ((SlotWriter)openWriter).close();
                    this.applier.onEndChanges();
                    final Unit instance2 = Unit.INSTANCE;
                    Trace.INSTANCE.endSection(beginSection2);
                    rememberEventDispatcher.dispatchRememberObservers();
                    rememberEventDispatcher.dispatchNodeCallbacks();
                    rememberEventDispatcher.dispatchSideEffects();
                    if (this.pendingInvalidScopes) {
                        beginSection = Trace.INSTANCE.beginSection("Compose:unobserve");
                        try {
                            this.pendingInvalidScopes = false;
                            final IdentityScopeMap<RecomposeScopeImpl> observations = this.observations;
                            final int size2 = observations.getSize();
                            int j = 0;
                            int size3 = 0;
                            while (j < size2) {
                                final int n = observations.getValueOrder()[j];
                                final IdentityArraySet set = observations.getScopeSets()[n];
                                Intrinsics.checkNotNull((Object)set);
                                final int size4 = set.size();
                                int k = 0;
                                int size5 = 0;
                                while (k < size4) {
                                    openWriter = set.getValues()[k];
                                    Intrinsics.checkNotNull(openWriter, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
                                    int n2 = size5;
                                    if (!(((RecomposeScopeImpl)openWriter).getValid() ^ true)) {
                                        if (size5 != k) {
                                            set.getValues()[size5] = openWriter;
                                        }
                                        n2 = size5 + 1;
                                    }
                                    ++k;
                                    size5 = n2;
                                }
                                for (int size6 = set.size(), l = size5; l < size6; ++l) {
                                    set.getValues()[l] = null;
                                }
                                set.setSize(size5);
                                int n3 = size3;
                                if (set.size() > 0) {
                                    if (size3 != j) {
                                        final int n4 = observations.getValueOrder()[size3];
                                        observations.getValueOrder()[size3] = n;
                                        observations.getValueOrder()[j] = n4;
                                    }
                                    n3 = size3 + 1;
                                }
                                ++j;
                                size3 = n3;
                            }
                            for (int size7 = observations.getSize(), n5 = size3; n5 < size7; ++n5) {
                                observations.getValues()[observations.getValueOrder()[n5]] = null;
                            }
                            observations.setSize(size3);
                            this.cleanUpDerivedStateObservations();
                            final Unit instance3 = Unit.INSTANCE;
                            Trace.INSTANCE.endSection(beginSection);
                        }
                        finally {}
                    }
                }
                finally {
                    ((SlotWriter)openWriter).close();
                }
            }
            finally {
                Trace.INSTANCE.endSection(beginSection2);
            }
        }
        finally {
            if (this.lateChanges.isEmpty()) {
                rememberEventDispatcher.dispatchAbandons();
            }
        }
    }
    
    private final void cleanUpDerivedStateObservations() {
        final IdentityScopeMap<DerivedState<?>> derivedStates = this.derivedStates;
        final int size = derivedStates.getSize();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final int n = derivedStates.getValueOrder()[i];
            final IdentityArraySet set = derivedStates.getScopeSets()[n];
            Intrinsics.checkNotNull((Object)set);
            final int size3 = set.size();
            int j = 0;
            int size4 = 0;
            while (j < size3) {
                final Object o = set.getValues()[j];
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
                final DerivedState derivedState = (DerivedState)o;
                int n2 = size4;
                if (!(this.observations.contains(derivedState) ^ true)) {
                    if (size4 != j) {
                        set.getValues()[size4] = o;
                    }
                    n2 = size4 + 1;
                }
                ++j;
                size4 = n2;
            }
            for (int size5 = set.size(), k = size4; k < size5; ++k) {
                set.getValues()[k] = null;
            }
            set.setSize(size4);
            int n3 = size2;
            if (set.size() > 0) {
                if (size2 != i) {
                    final int n4 = derivedStates.getValueOrder()[size2];
                    derivedStates.getValueOrder()[size2] = n;
                    derivedStates.getValueOrder()[i] = n4;
                }
                n3 = size2 + 1;
            }
            ++i;
            size2 = n3;
        }
        for (int size6 = derivedStates.getSize(), l = size2; l < size6; ++l) {
            derivedStates.getValues()[derivedStates.getValueOrder()[l]] = null;
        }
        derivedStates.setSize(size2);
        final Iterator<RecomposeScopeImpl> iterator = this.conditionallyInvalidatedScopes.iterator();
        Intrinsics.checkNotNullExpressionValue((Object)iterator, "iterator()");
        while (iterator.hasNext()) {
            if (iterator.next().isConditional() ^ true) {
                iterator.remove();
            }
        }
    }
    
    private final void drainPendingModificationsForCompositionLocked() {
        final Set[] andSet = this.pendingModifications.getAndSet(CompositionKt.access$getPendingApplyNoModifications$p());
        if (andSet != null) {
            if (Intrinsics.areEqual((Object)andSet, CompositionKt.access$getPendingApplyNoModifications$p())) {
                ComposerKt.composeRuntimeError("pending composition has not been applied");
                throw new KotlinNothingValueException();
            }
            if (andSet instanceof Set) {
                this.addPendingInvalidationsLocked((Set<?>)(Object)andSet, true);
            }
            else {
                if (!(andSet instanceof Object[])) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("corrupt pendingModifications drain: ");
                    sb.append(this.pendingModifications);
                    ComposerKt.composeRuntimeError(sb.toString());
                    throw new KotlinNothingValueException();
                }
                final Set[] array = andSet;
                for (int i = 0; i < array.length; ++i) {
                    this.addPendingInvalidationsLocked(array[i], true);
                }
            }
        }
    }
    
    private final void drainPendingModificationsLocked() {
        final Set[] andSet = this.pendingModifications.getAndSet(null);
        if (!Intrinsics.areEqual((Object)andSet, CompositionKt.access$getPendingApplyNoModifications$p())) {
            if (andSet instanceof Set) {
                this.addPendingInvalidationsLocked((Set<?>)(Object)andSet, false);
            }
            else if (andSet instanceof Object[]) {
                final Set[] array = andSet;
                for (int length = array.length, i = 0; i < length; ++i) {
                    this.addPendingInvalidationsLocked(array[i], false);
                }
            }
            else {
                if (andSet == null) {
                    ComposerKt.composeRuntimeError("calling recordModificationsOf and applyChanges concurrently is not supported");
                    throw new KotlinNothingValueException();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("corrupt pendingModifications drain: ");
                sb.append(this.pendingModifications);
                ComposerKt.composeRuntimeError(sb.toString());
                throw new KotlinNothingValueException();
            }
        }
    }
    
    private final boolean getAreChildrenComposing() {
        return this.composer.getAreChildrenComposing$runtime_release();
    }
    
    private final <T> T guardChanges(Function0<? extends T> invoke) {
        try {
            invoke = ((Function0)invoke).invoke();
            try {
                InlineMarker.finallyStart(1);
                InlineMarker.finallyEnd(1);
                return (T)invoke;
            }
            catch (final Exception invoke) {}
        }
        finally {
            InlineMarker.finallyStart(1);
            if (this.abandonSet.isEmpty() ^ true) {
                invoke = new RememberEventDispatcher(this.abandonSet);
                ((RememberEventDispatcher)invoke).dispatchAbandons();
            }
            InlineMarker.finallyEnd(1);
        }
        this.abandonChanges();
        throw invoke;
    }
    
    private final <T> T guardInvalidationsLocked(final Function1<? super IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>>, ? extends T> function1) {
        final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> takeInvalidations = this.takeInvalidations();
        try {
            return (T)function1.invoke((Object)takeInvalidations);
        }
        catch (final Exception ex) {
            this.invalidations = takeInvalidations;
            throw ex;
        }
    }
    
    private final InvalidationResult invalidateChecked(final RecomposeScopeImpl recomposeScopeImpl, final Anchor anchor, final Object o) {
        synchronized (this.lock) {
            CompositionImpl invalidationDelegate = this.invalidationDelegate;
            if (invalidationDelegate == null || !this.slotTable.groupContainsAnchor(this.invalidationDelegateGroup, anchor)) {
                invalidationDelegate = null;
            }
            if (invalidationDelegate == null) {
                if (this.isComposing() && this.composer.tryImminentInvalidation$runtime_release(recomposeScopeImpl, o)) {
                    return InvalidationResult.IMMINENT;
                }
                if (o == null) {
                    this.invalidations.set(recomposeScopeImpl, null);
                }
                else {
                    CompositionKt.access$addValue(this.invalidations, recomposeScopeImpl, o);
                }
            }
            monitorexit(this.lock);
            if (invalidationDelegate != null) {
                return invalidationDelegate.invalidateChecked(recomposeScopeImpl, anchor, o);
            }
            this.parent.invalidate$runtime_release(this);
            InvalidationResult invalidationResult;
            if (this.isComposing()) {
                invalidationResult = InvalidationResult.DEFERRED;
            }
            else {
                invalidationResult = InvalidationResult.SCHEDULED;
            }
            return invalidationResult;
        }
    }
    
    private final void invalidateScopeOfLocked(final Object o) {
        final IdentityScopeMap<RecomposeScopeImpl> observations = this.observations;
        final int access$find = IdentityScopeMap.access$find((IdentityScopeMap<Object>)observations, o);
        if (access$find >= 0) {
            final IdentityArraySet access$scopeSet = IdentityScopeMap.access$scopeSetAt((IdentityScopeMap<Object>)observations, access$find);
            for (int i = 0; i < access$scopeSet.size(); ++i) {
                final RecomposeScopeImpl recomposeScopeImpl = access$scopeSet.get(i);
                if (recomposeScopeImpl.invalidateForResult(o) == InvalidationResult.IMMINENT) {
                    this.observationsProcessed.add(o, recomposeScopeImpl);
                }
            }
        }
    }
    
    private final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> takeInvalidations() {
        final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> invalidations = this.invalidations;
        this.invalidations = new IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>>(0, 1, null);
        return invalidations;
    }
    
    private final <T> T trackAbandonedValues(final Function0<? extends T> function0) {
        try {
            final Object invoke = function0.invoke();
            InlineMarker.finallyStart(1);
            InlineMarker.finallyEnd(1);
            return (T)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            if (this.abandonSet.isEmpty() ^ true) {
                new RememberEventDispatcher(this.abandonSet).dispatchAbandons();
            }
            InlineMarker.finallyEnd(1);
        }
    }
    
    private final void validateRecomposeScopeAnchors(final SlotTable slotTable) {
        final Object[] slots = slotTable.getSlots();
        final Collection collection = new ArrayList();
        final int length = slots.length;
        final int n = 0;
        for (final Object o : slots) {
            RecomposeScopeImpl recomposeScopeImpl;
            if (o instanceof RecomposeScopeImpl) {
                recomposeScopeImpl = (RecomposeScopeImpl)o;
            }
            else {
                recomposeScopeImpl = null;
            }
            if (recomposeScopeImpl != null) {
                collection.add(recomposeScopeImpl);
            }
        }
        final List list = (List)collection;
        for (int size = list.size(), j = n; j < size; ++j) {
            final RecomposeScopeImpl obj = list.get(j);
            final Anchor anchor = obj.getAnchor();
            if (anchor != null && !slotTable.slotsOf$runtime_release(anchor.toIndexFor(slotTable)).contains(obj)) {
                final int index = ArraysKt.indexOf(slotTable.getSlots(), (Object)obj);
                final StringBuilder sb = new StringBuilder();
                sb.append("Misaligned anchor ");
                sb.append(anchor);
                sb.append(" in scope ");
                sb.append(obj);
                sb.append(" encountered, scope found at ");
                sb.append(index);
                throw new IllegalStateException(sb.toString().toString());
            }
        }
    }
    
    @Override
    public void applyChanges() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/compose/runtime/CompositionImpl.lock:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: aload_0        
        //     9: getfield        androidx/compose/runtime/CompositionImpl.changes:Ljava/util/List;
        //    12: invokespecial   androidx/compose/runtime/CompositionImpl.applyChangesInLocked:(Ljava/util/List;)V
        //    15: aload_0        
        //    16: invokespecial   androidx/compose/runtime/CompositionImpl.drainPendingModificationsLocked:()V
        //    19: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    22: astore_2       
        //    23: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    26: astore_2       
        //    27: aload_1        
        //    28: monitorexit    
        //    29: return         
        //    30: astore_2       
        //    31: aload_0        
        //    32: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    35: checkcast       Ljava/util/Collection;
        //    38: invokeinterface java/util/Collection.isEmpty:()Z
        //    43: iconst_1       
        //    44: ixor           
        //    45: ifeq            67
        //    48: new             Landroidx/compose/runtime/CompositionImpl$RememberEventDispatcher;
        //    51: astore_3       
        //    52: aload_3        
        //    53: aload_0        
        //    54: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    57: checkcast       Ljava/util/Set;
        //    60: invokespecial   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.<init>:(Ljava/util/Set;)V
        //    63: aload_3        
        //    64: invokevirtual   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.dispatchAbandons:()V
        //    67: aload_2        
        //    68: athrow         
        //    69: astore_2       
        //    70: goto            80
        //    73: astore_2       
        //    74: aload_0        
        //    75: invokespecial   androidx/compose/runtime/CompositionImpl.abandonChanges:()V
        //    78: aload_2        
        //    79: athrow         
        //    80: aload_1        
        //    81: monitorexit    
        //    82: aload_2        
        //    83: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      23     30     80     Any
        //  23     27     69     73     Any
        //  31     67     73     80     Ljava/lang/Exception;
        //  31     67     69     73     Any
        //  67     69     73     80     Ljava/lang/Exception;
        //  67     69     69     73     Any
        //  74     80     69     73     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0067:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void applyLateChanges() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/compose/runtime/CompositionImpl.lock:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        androidx/compose/runtime/CompositionImpl.lateChanges:Ljava/util/List;
        //    11: checkcast       Ljava/util/Collection;
        //    14: invokeinterface java/util/Collection.isEmpty:()Z
        //    19: iconst_1       
        //    20: ixor           
        //    21: ifeq            32
        //    24: aload_0        
        //    25: aload_0        
        //    26: getfield        androidx/compose/runtime/CompositionImpl.lateChanges:Ljava/util/List;
        //    29: invokespecial   androidx/compose/runtime/CompositionImpl.applyChangesInLocked:(Ljava/util/List;)V
        //    32: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    35: astore_2       
        //    36: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    39: astore_2       
        //    40: aload_1        
        //    41: monitorexit    
        //    42: return         
        //    43: astore_2       
        //    44: aload_0        
        //    45: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    48: checkcast       Ljava/util/Collection;
        //    51: invokeinterface java/util/Collection.isEmpty:()Z
        //    56: iconst_1       
        //    57: ixor           
        //    58: ifeq            80
        //    61: new             Landroidx/compose/runtime/CompositionImpl$RememberEventDispatcher;
        //    64: astore_3       
        //    65: aload_3        
        //    66: aload_0        
        //    67: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    70: checkcast       Ljava/util/Set;
        //    73: invokespecial   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.<init>:(Ljava/util/Set;)V
        //    76: aload_3        
        //    77: invokevirtual   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.dispatchAbandons:()V
        //    80: aload_2        
        //    81: athrow         
        //    82: astore_2       
        //    83: goto            93
        //    86: astore_2       
        //    87: aload_0        
        //    88: invokespecial   androidx/compose/runtime/CompositionImpl.abandonChanges:()V
        //    91: aload_2        
        //    92: athrow         
        //    93: aload_1        
        //    94: monitorexit    
        //    95: aload_2        
        //    96: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      32     43     93     Any
        //  32     36     43     93     Any
        //  36     40     82     86     Any
        //  44     80     86     93     Ljava/lang/Exception;
        //  44     80     82     86     Any
        //  80     82     86     93     Ljava/lang/Exception;
        //  80     82     82     86     Any
        //  87     93     82     86     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0080:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void changesApplied() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/compose/runtime/CompositionImpl.lock:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        androidx/compose/runtime/CompositionImpl.composer:Landroidx/compose/runtime/ComposerImpl;
        //    11: invokevirtual   androidx/compose/runtime/ComposerImpl.changesApplied$runtime_release:()V
        //    14: aload_0        
        //    15: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    18: checkcast       Ljava/util/Collection;
        //    21: invokeinterface java/util/Collection.isEmpty:()Z
        //    26: iconst_1       
        //    27: ixor           
        //    28: ifeq            50
        //    31: new             Landroidx/compose/runtime/CompositionImpl$RememberEventDispatcher;
        //    34: astore_2       
        //    35: aload_2        
        //    36: aload_0        
        //    37: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    40: checkcast       Ljava/util/Set;
        //    43: invokespecial   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.<init>:(Ljava/util/Set;)V
        //    46: aload_2        
        //    47: invokevirtual   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.dispatchAbandons:()V
        //    50: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    53: astore_2       
        //    54: getstatic       kotlin/Unit.INSTANCE:Lkotlin/Unit;
        //    57: astore_2       
        //    58: aload_1        
        //    59: monitorexit    
        //    60: return         
        //    61: astore_2       
        //    62: aload_0        
        //    63: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    66: checkcast       Ljava/util/Collection;
        //    69: invokeinterface java/util/Collection.isEmpty:()Z
        //    74: iconst_1       
        //    75: ixor           
        //    76: ifeq            98
        //    79: new             Landroidx/compose/runtime/CompositionImpl$RememberEventDispatcher;
        //    82: astore_3       
        //    83: aload_3        
        //    84: aload_0        
        //    85: getfield        androidx/compose/runtime/CompositionImpl.abandonSet:Ljava/util/HashSet;
        //    88: checkcast       Ljava/util/Set;
        //    91: invokespecial   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.<init>:(Ljava/util/Set;)V
        //    94: aload_3        
        //    95: invokevirtual   androidx/compose/runtime/CompositionImpl$RememberEventDispatcher.dispatchAbandons:()V
        //    98: aload_2        
        //    99: athrow         
        //   100: astore_2       
        //   101: goto            111
        //   104: astore_2       
        //   105: aload_0        
        //   106: invokespecial   androidx/compose/runtime/CompositionImpl.abandonChanges:()V
        //   109: aload_2        
        //   110: athrow         
        //   111: aload_1        
        //   112: monitorexit    
        //   113: aload_2        
        //   114: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      50     61     111    Any
        //  50     54     61     111    Any
        //  54     58     100    104    Any
        //  62     98     104    111    Ljava/lang/Exception;
        //  62     98     100    104    Any
        //  98     100    104    111    Ljava/lang/Exception;
        //  98     100    100    104    Any
        //  105    111    100    104    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0098:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void composeContent(final Function2<? super Composer, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "content");
        try {
            synchronized (this.lock) {
                this.drainPendingModificationsForCompositionLocked();
                final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> takeInvalidations = this.takeInvalidations();
                try {
                    this.composer.composeContent$runtime_release(takeInvalidations, function2);
                    final Unit instance = Unit.INSTANCE;
                    final Unit instance2 = Unit.INSTANCE;
                    monitorexit(this.lock);
                    final Unit instance3 = Unit.INSTANCE;
                }
                catch (final Exception ex) {
                    this.invalidations = takeInvalidations;
                    throw ex;
                }
            }
        }
        finally {
            try {
                if (this.abandonSet.isEmpty() ^ true) {
                    new RememberEventDispatcher(this.abandonSet).dispatchAbandons();
                }
            }
            catch (final Exception ex2) {
                this.abandonChanges();
                throw ex2;
            }
        }
    }
    
    @Override
    public <R> R delegateInvalidations(final ControlledComposition controlledComposition, final int invalidationDelegateGroup, final Function0<? extends R> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        if (controlledComposition != null && !Intrinsics.areEqual((Object)controlledComposition, (Object)this) && invalidationDelegateGroup >= 0) {
            this.invalidationDelegate = (CompositionImpl)controlledComposition;
            this.invalidationDelegateGroup = invalidationDelegateGroup;
            try {
                function0.invoke();
                final Object invoke;
                return (R)invoke;
            }
            finally {
                this.invalidationDelegate = null;
                this.invalidationDelegateGroup = 0;
            }
        }
        final Object invoke = function0.invoke();
        return (R)invoke;
    }
    
    @Override
    public void dispose() {
        synchronized (this.lock) {
            if (!this.disposed) {
                this.disposed = true;
                this.composable = ComposableSingletons$CompositionKt.INSTANCE.getLambda-2$runtime_release();
                final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> deferredChanges$runtime_release = this.composer.getDeferredChanges$runtime_release();
                if (deferredChanges$runtime_release != null) {
                    this.applyChangesInLocked(deferredChanges$runtime_release);
                }
                final boolean b = this.slotTable.getGroupsSize() > 0;
                if (b || (true ^ this.abandonSet.isEmpty())) {
                    final RememberEventDispatcher rememberEventDispatcher = new RememberEventDispatcher(this.abandonSet);
                    if (b) {
                        final SlotWriter openWriter = this.slotTable.openWriter();
                        try {
                            ComposerKt.removeCurrentGroup(openWriter, rememberEventDispatcher);
                            final Unit instance = Unit.INSTANCE;
                            openWriter.close();
                            this.applier.clear();
                            rememberEventDispatcher.dispatchRememberObservers();
                            rememberEventDispatcher.dispatchNodeCallbacks();
                        }
                        finally {
                            openWriter.close();
                        }
                    }
                    rememberEventDispatcher.dispatchAbandons();
                }
                this.composer.dispose$runtime_release();
            }
            final Unit instance2 = Unit.INSTANCE;
            monitorexit(this.lock);
            this.parent.unregisterComposition$runtime_release(this);
        }
    }
    
    @Override
    public void disposeUnusedMovableContent(MovableContentState openWriter) {
        Intrinsics.checkNotNullParameter((Object)openWriter, "state");
        final RememberEventDispatcher rememberEventDispatcher = new RememberEventDispatcher(this.abandonSet);
        openWriter = (MovableContentState)openWriter.getSlotTable$runtime_release().openWriter();
        try {
            ComposerKt.removeCurrentGroup((SlotWriter)openWriter, rememberEventDispatcher);
            final Unit instance = Unit.INSTANCE;
            ((SlotWriter)openWriter).close();
            rememberEventDispatcher.dispatchRememberObservers();
            rememberEventDispatcher.dispatchNodeCallbacks();
        }
        finally {
            ((SlotWriter)openWriter).close();
        }
    }
    
    public final Function2<Composer, Integer, Unit> getComposable() {
        return (Function2<Composer, Integer, Unit>)this.composable;
    }
    
    public final List<RecomposeScopeImpl> getConditionalScopes$runtime_release() {
        return CollectionsKt.toList((Iterable)this.conditionallyInvalidatedScopes);
    }
    
    public final List<Object> getDerivedStateDependencies$runtime_release() {
        return ArraysKt.filterNotNull(this.derivedStates.getValues());
    }
    
    @Override
    public boolean getHasInvalidations() {
        synchronized (this.lock) {
            return this.invalidations.getSize$runtime_release() > 0;
        }
    }
    
    @Override
    public boolean getHasPendingChanges() {
        synchronized (this.lock) {
            return this.composer.getHasPendingChanges$runtime_release();
        }
    }
    
    public final List<Object> getObservedObjects$runtime_release() {
        return ArraysKt.filterNotNull(this.observations.getValues());
    }
    
    public final boolean getPendingInvalidScopes$runtime_release() {
        return this.pendingInvalidScopes;
    }
    
    public final CoroutineContext getRecomposeContext() {
        CoroutineContext coroutineContext;
        if ((coroutineContext = this._recomposeContext) == null) {
            coroutineContext = this.parent.getRecomposeCoroutineContext$runtime_release();
        }
        return coroutineContext;
    }
    
    public final SlotTable getSlotTable$runtime_release() {
        return this.slotTable;
    }
    
    @Override
    public void insertMovableContent(final List<Pair<MovableContentStateReference, MovableContentStateReference>> list) {
        Intrinsics.checkNotNullParameter((Object)list, "references");
        final int size = list.size();
        boolean b = false;
        int i = 0;
        while (true) {
            while (i < size) {
                if (!Intrinsics.areEqual((Object)((MovableContentStateReference)((Pair)list.get(i)).getFirst()).getComposition$runtime_release(), (Object)this)) {
                    ComposerKt.runtimeCheck(b);
                    try {
                        this.composer.insertMovableContentReferences(list);
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    finally {
                        try {
                            if (this.abandonSet.isEmpty() ^ true) {
                                new RememberEventDispatcher(this.abandonSet).dispatchAbandons();
                            }
                        }
                        catch (final Exception ex) {
                            this.abandonChanges();
                            throw ex;
                        }
                    }
                }
                else {
                    ++i;
                }
            }
            b = true;
            continue;
        }
    }
    
    public final InvalidationResult invalidate(final RecomposeScopeImpl recomposeScopeImpl, final Object o) {
        Intrinsics.checkNotNullParameter((Object)recomposeScopeImpl, "scope");
        if (recomposeScopeImpl.getDefaultsInScope()) {
            recomposeScopeImpl.setDefaultsInvalid(true);
        }
        final Anchor anchor = recomposeScopeImpl.getAnchor();
        if (anchor == null || !this.slotTable.ownsAnchor(anchor) || !anchor.getValid()) {
            return InvalidationResult.IGNORED;
        }
        if (!anchor.getValid()) {
            return InvalidationResult.IGNORED;
        }
        if (!recomposeScopeImpl.getCanRecompose()) {
            return InvalidationResult.IGNORED;
        }
        return this.invalidateChecked(recomposeScopeImpl, anchor, o);
    }
    
    @Override
    public void invalidateAll() {
        synchronized (this.lock) {
            final Object[] slots = this.slotTable.getSlots();
            for (int i = 0; i < slots.length; ++i) {
                final Object o = slots[i];
                RecomposeScopeImpl recomposeScopeImpl;
                if (o instanceof RecomposeScopeImpl) {
                    recomposeScopeImpl = (RecomposeScopeImpl)o;
                }
                else {
                    recomposeScopeImpl = null;
                }
                if (recomposeScopeImpl != null) {
                    recomposeScopeImpl.invalidate();
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void invalidateGroupsWithKey(int i) {
        synchronized (this.lock) {
            final List<RecomposeScopeImpl> invalidateGroupsWithKey$runtime_release = this.slotTable.invalidateGroupsWithKey$runtime_release(i);
            monitorexit(this.lock);
            final int n = 0;
            int n2 = 0;
            Label_0099: {
                Label_0097: {
                    if (invalidateGroupsWithKey$runtime_release != null) {
                        final int size = invalidateGroupsWithKey$runtime_release.size();
                        i = 0;
                        while (true) {
                            while (i < size) {
                                if (((RecomposeScopeImpl)invalidateGroupsWithKey$runtime_release.get(i)).invalidateForResult(null) == InvalidationResult.IGNORED) {
                                    i = 1;
                                    n2 = n;
                                    if (i != 0) {
                                        break Label_0097;
                                    }
                                    break Label_0099;
                                }
                                else {
                                    ++i;
                                }
                            }
                            i = 0;
                            continue;
                        }
                    }
                }
                n2 = 1;
            }
            if (n2 != 0 && this.composer.forceRecomposeScopes$runtime_release()) {
                this.parent.invalidate$runtime_release(this);
            }
        }
    }
    
    @Override
    public boolean isComposing() {
        return this.composer.isComposing$runtime_release();
    }
    
    @Override
    public boolean isDisposed() {
        return this.disposed;
    }
    
    public final boolean isRoot() {
        return this.isRoot;
    }
    
    @Override
    public boolean observesAnyOf(final Set<?> set) {
        Intrinsics.checkNotNullParameter((Object)set, "values");
        for (final Object next : set) {
            if (this.observations.contains(next) || this.derivedStates.contains(next)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void prepareCompose(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        this.composer.prepareCompose$runtime_release(function0);
    }
    
    @Override
    public boolean recompose() {
        synchronized (this.lock) {
            this.drainPendingModificationsForCompositionLocked();
            try {
                final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> takeInvalidations = this.takeInvalidations();
                try {
                    final boolean recompose$runtime_release = this.composer.recompose$runtime_release(takeInvalidations);
                    if (!recompose$runtime_release) {
                        this.drainPendingModificationsLocked();
                    }
                    return recompose$runtime_release;
                }
                catch (final Exception ex) {
                    this.invalidations = takeInvalidations;
                    throw ex;
                }
            }
            finally {
                try {
                    if (this.abandonSet.isEmpty() ^ true) {
                        new RememberEventDispatcher(this.abandonSet).dispatchAbandons();
                    }
                }
                catch (final Exception ex2) {
                    this.abandonChanges();
                }
            }
        }
    }
    
    @Override
    public void recordModificationsOf(final Set<?> set) {
        Intrinsics.checkNotNullParameter((Object)set, "values");
        Object[] value;
        Object plus;
        do {
            value = this.pendingModifications.get();
            if (value == null || Intrinsics.areEqual((Object)value, CompositionKt.access$getPendingApplyNoModifications$p())) {
                plus = set;
            }
            else if (value instanceof Set) {
                plus = new Set[] { (Set)value, set };
            }
            else {
                if (!(value instanceof Object[])) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("corrupt pendingModifications: ");
                    sb.append(this.pendingModifications);
                    throw new IllegalStateException(sb.toString().toString());
                }
                Intrinsics.checkNotNull((Object)value, "null cannot be cast to non-null type kotlin.Array<kotlin.collections.Set<kotlin.Any>>");
                plus = ArraysKt.plus((Object[])value, (Object)set);
            }
        } while (!PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(this.pendingModifications, value, plus));
        if (value == null) {
            synchronized (this.lock) {
                this.drainPendingModificationsLocked();
                final Unit instance = Unit.INSTANCE;
            }
        }
    }
    
    @Override
    public void recordReadOf(final Object o) {
        Intrinsics.checkNotNullParameter(o, "value");
        if (!this.getAreChildrenComposing()) {
            final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.composer.getCurrentRecomposeScope$runtime_release();
            if (currentRecomposeScope$runtime_release != null) {
                currentRecomposeScope$runtime_release.setUsed(true);
                this.observations.add(o, currentRecomposeScope$runtime_release);
                if (o instanceof DerivedState) {
                    this.derivedStates.removeScope((DerivedState<?>)o);
                    final Object[] dependencies = ((DerivedState)o).getDependencies();
                    for (int i = 0; i < dependencies.length; ++i) {
                        final Object o2 = dependencies[i];
                        if (o2 == null) {
                            break;
                        }
                        this.derivedStates.add(o2, (DerivedState<?>)o);
                    }
                }
                currentRecomposeScope$runtime_release.recordRead(o);
            }
        }
    }
    
    @Override
    public void recordWriteOf(final Object o) {
        Intrinsics.checkNotNullParameter(o, "value");
        synchronized (this.lock) {
            this.invalidateScopeOfLocked(o);
            final IdentityScopeMap<DerivedState<?>> derivedStates = this.derivedStates;
            final int access$find = IdentityScopeMap.access$find((IdentityScopeMap<Object>)derivedStates, o);
            if (access$find >= 0) {
                final IdentityArraySet access$scopeSet = IdentityScopeMap.access$scopeSetAt((IdentityScopeMap<Object>)derivedStates, access$find);
                for (int i = 0; i < access$scopeSet.size(); ++i) {
                    this.invalidateScopeOfLocked(access$scopeSet.get(i));
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void removeDerivedStateObservation$runtime_release(final DerivedState<?> derivedState) {
        Intrinsics.checkNotNullParameter((Object)derivedState, "state");
        if (!this.observations.contains(derivedState)) {
            this.derivedStates.removeScope(derivedState);
        }
    }
    
    public final void removeObservation$runtime_release(final Object o, final RecomposeScopeImpl recomposeScopeImpl) {
        Intrinsics.checkNotNullParameter(o, "instance");
        Intrinsics.checkNotNullParameter((Object)recomposeScopeImpl, "scope");
        this.observations.remove(o, recomposeScopeImpl);
    }
    
    public final void setComposable(final Function2<? super Composer, ? super Integer, Unit> composable) {
        Intrinsics.checkNotNullParameter((Object)composable, "<set-?>");
        this.composable = composable;
    }
    
    @Override
    public void setContent(final Function2<? super Composer, ? super Integer, Unit> composable) {
        Intrinsics.checkNotNullParameter((Object)composable, "content");
        if (this.disposed ^ true) {
            this.composable = composable;
            this.parent.composeInitial$runtime_release(this, composable);
            return;
        }
        throw new IllegalStateException("The composition is disposed".toString());
    }
    
    public final void setPendingInvalidScopes$runtime_release(final boolean pendingInvalidScopes) {
        this.pendingInvalidScopes = pendingInvalidScopes;
    }
    
    @Override
    public void verifyConsistent() {
        synchronized (this.lock) {
            if (!this.isComposing()) {
                this.composer.verifyConsistent$runtime_release();
                this.slotTable.verifyWellFormed();
                this.validateRecomposeScopeAnchors(this.slotTable);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\bH\u0016J\u0006\u0010\u0010\u001a\u00020\u000eJ\u0006\u0010\u0011\u001a\u00020\u000eJ\u0006\u0010\u0012\u001a\u00020\u000eJ\u0006\u0010\u0013\u001a\u00020\u000eJ\u0010\u0010\t\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0004H\u0016J\u0010\u0010\n\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\bH\u0016J\u0010\u0010\u000b\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0004H\u0016J\u0016\u0010\u0014\u001a\u00020\u000e2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u0016R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016" }, d2 = { "Landroidx/compose/runtime/CompositionImpl$RememberEventDispatcher;", "Landroidx/compose/runtime/RememberManager;", "abandoning", "", "Landroidx/compose/runtime/RememberObserver;", "(Ljava/util/Set;)V", "deactivating", "", "Landroidx/compose/runtime/ComposeNodeLifecycleCallback;", "forgetting", "releasing", "remembering", "sideEffects", "Lkotlin/Function0;", "", "instance", "dispatchAbandons", "dispatchNodeCallbacks", "dispatchRememberObservers", "dispatchSideEffects", "sideEffect", "effect", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class RememberEventDispatcher implements RememberManager
    {
        private final Set<RememberObserver> abandoning;
        private List<ComposeNodeLifecycleCallback> deactivating;
        private final List<RememberObserver> forgetting;
        private List<ComposeNodeLifecycleCallback> releasing;
        private final List<RememberObserver> remembering;
        private final List<Function0<Unit>> sideEffects;
        
        public RememberEventDispatcher(final Set<RememberObserver> abandoning) {
            Intrinsics.checkNotNullParameter((Object)abandoning, "abandoning");
            this.abandoning = abandoning;
            this.remembering = new ArrayList<RememberObserver>();
            this.forgetting = new ArrayList<RememberObserver>();
            this.sideEffects = new ArrayList<Function0<Unit>>();
        }
        
        @Override
        public void deactivating(final ComposeNodeLifecycleCallback composeNodeLifecycleCallback) {
            Intrinsics.checkNotNullParameter((Object)composeNodeLifecycleCallback, "instance");
            List<ComposeNodeLifecycleCallback> deactivating;
            if ((deactivating = this.deactivating) == null) {
                deactivating = new ArrayList<ComposeNodeLifecycleCallback>();
                this.deactivating = deactivating;
            }
            deactivating.add(composeNodeLifecycleCallback);
        }
        
        public final void dispatchAbandons() {
            if (this.abandoning.isEmpty() ^ true) {
                final Object beginSection = Trace.INSTANCE.beginSection("Compose:abandons");
                try {
                    final Iterator<RememberObserver> iterator = this.abandoning.iterator();
                    while (iterator.hasNext()) {
                        final RememberObserver rememberObserver = iterator.next();
                        iterator.remove();
                        rememberObserver.onAbandoned();
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection);
                }
            }
        }
        
        public final void dispatchNodeCallbacks() {
            final List<ComposeNodeLifecycleCallback> deactivating = this.deactivating;
            final Collection collection = deactivating;
            final int n = 0;
            if (collection != null && !collection.isEmpty()) {
                final Object beginSection = Trace.INSTANCE.beginSection("Compose:deactivations");
                try {
                    for (int n2 = deactivating.size() - 1; -1 < n2; --n2) {
                        ((ComposeNodeLifecycleCallback)deactivating.get(n2)).onDeactivate();
                    }
                    final Unit instance = Unit.INSTANCE;
                    Trace.INSTANCE.endSection(beginSection);
                    deactivating.clear();
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection);
                }
            }
            final List<ComposeNodeLifecycleCallback> releasing = this.releasing;
            final Collection collection2 = releasing;
            int n3 = 0;
            Label_0151: {
                if (collection2 != null) {
                    n3 = n;
                    if (!collection2.isEmpty()) {
                        break Label_0151;
                    }
                }
                n3 = 1;
            }
            if (n3 == 0) {
                final Object beginSection2 = Trace.INSTANCE.beginSection("Compose:releases");
                try {
                    for (int n4 = releasing.size() - 1; -1 < n4; --n4) {
                        ((ComposeNodeLifecycleCallback)releasing.get(n4)).onRelease();
                    }
                    final Unit instance2 = Unit.INSTANCE;
                    Trace.INSTANCE.endSection(beginSection2);
                    releasing.clear();
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection2);
                }
            }
        }
        
        public final void dispatchRememberObservers() {
            if (this.forgetting.isEmpty() ^ true) {
                final Object beginSection = Trace.INSTANCE.beginSection("Compose:onForgotten");
                try {
                    for (int n = this.forgetting.size() - 1; -1 < n; --n) {
                        final RememberObserver rememberObserver = this.forgetting.get(n);
                        if (!this.abandoning.contains(rememberObserver)) {
                            rememberObserver.onForgotten();
                        }
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection);
                }
            }
            if (this.remembering.isEmpty() ^ true) {
                final Object beginSection2 = Trace.INSTANCE.beginSection("Compose:onRemembered");
                try {
                    final List<RememberObserver> remembering = this.remembering;
                    for (int i = 0; i < remembering.size(); ++i) {
                        final RememberObserver rememberObserver2 = remembering.get(i);
                        this.abandoning.remove(rememberObserver2);
                        rememberObserver2.onRemembered();
                    }
                    final Unit instance2 = Unit.INSTANCE;
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection2);
                }
            }
        }
        
        public final void dispatchSideEffects() {
            if (this.sideEffects.isEmpty() ^ true) {
                final Object beginSection = Trace.INSTANCE.beginSection("Compose:sideeffects");
                try {
                    final List<Function0<Unit>> sideEffects = this.sideEffects;
                    for (int i = 0; i < sideEffects.size(); ++i) {
                        ((Function0)sideEffects.get(i)).invoke();
                    }
                    this.sideEffects.clear();
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    Trace.INSTANCE.endSection(beginSection);
                }
            }
        }
        
        @Override
        public void forgetting(final RememberObserver rememberObserver) {
            Intrinsics.checkNotNullParameter((Object)rememberObserver, "instance");
            final int lastIndex = this.remembering.lastIndexOf(rememberObserver);
            if (lastIndex >= 0) {
                this.remembering.remove(lastIndex);
                this.abandoning.remove(rememberObserver);
            }
            else {
                this.forgetting.add(rememberObserver);
            }
        }
        
        @Override
        public void releasing(final ComposeNodeLifecycleCallback composeNodeLifecycleCallback) {
            Intrinsics.checkNotNullParameter((Object)composeNodeLifecycleCallback, "instance");
            List<ComposeNodeLifecycleCallback> releasing;
            if ((releasing = this.releasing) == null) {
                releasing = new ArrayList<ComposeNodeLifecycleCallback>();
                this.releasing = releasing;
            }
            releasing.add(composeNodeLifecycleCallback);
        }
        
        @Override
        public void remembering(final RememberObserver rememberObserver) {
            Intrinsics.checkNotNullParameter((Object)rememberObserver, "instance");
            final int lastIndex = this.forgetting.lastIndexOf(rememberObserver);
            if (lastIndex >= 0) {
                this.forgetting.remove(lastIndex);
                this.abandoning.remove(rememberObserver);
            }
            else {
                this.remembering.add(rememberObserver);
            }
        }
        
        @Override
        public void sideEffect(final Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "effect");
            this.sideEffects.add(function0);
        }
    }
}

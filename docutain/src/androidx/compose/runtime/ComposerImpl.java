// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.TypeIntrinsics;
import java.util.HashSet;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.InlineMarker;
import kotlin.collections.ArraysKt;
import java.util.Map;
import androidx.compose.runtime.tooling.CompositionData;
import androidx.compose.runtime.tooling.InspectionTablesKt;
import java.util.Iterator;
import kotlin.TuplesKt;
import androidx.compose.runtime.internal.ComposableLambdaKt;
import java.util.Collection;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.Pair;
import java.util.LinkedHashSet;
import androidx.compose.runtime.snapshots.ListUtilsKt;
import kotlin.KotlinNothingValueException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import java.util.Comparator;
import kotlin.jvm.functions.Function2;
import androidx.compose.runtime.collection.IdentityArraySet;
import androidx.compose.runtime.collection.IdentityArrayMap;
import androidx.compose.runtime.snapshots.SnapshotKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.snapshots.Snapshot;
import androidx.compose.runtime.collection.IntMap;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import java.util.HashMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u00ea\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0005\n\u0002\u0010\f\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0011\n\u0002\b7\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b)\b\u0000\u0018\u00002\u00020\u0001:\u0004\u00ef\u0002\u00f0\u0002B\u00ed\u0001\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012Y\u0010\u000b\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\f\u0012Y\u0010\u0016\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\f\u0012\u0006\u0010\u0017\u001a\u00020\u0018¢\u0006\u0002\u0010\u0019J\t\u0010\u0097\u0001\u001a\u00020\u0014H\u0002J\t\u0010\u0098\u0001\u001a\u00020\u0014H\u0002JK\u0010\u0099\u0001\u001a\u00020\u0014\"\u0005\b\u0000\u0010\u009a\u0001\"\u0005\b\u0001\u0010\u009b\u00012\b\u0010\u009c\u0001\u001a\u0003H\u009a\u00012\"\u0010\u009d\u0001\u001a\u001d\u0012\u0005\u0012\u0003H\u009b\u0001\u0012\u0005\u0012\u0003H\u009a\u0001\u0012\u0004\u0012\u00020\u00140\u009e\u0001¢\u0006\u0003\b\u009f\u0001H\u0016¢\u0006\u0003\u0010 \u0001J\t\u0010¡\u0001\u001a\u00020\u0005H\u0016J5\u0010¢\u0001\u001a\u0003H\u009b\u0001\"\u0005\b\u0000\u0010\u009b\u00012\u0007\u0010£\u0001\u001a\u00020!2\u000f\u0010\u009d\u0001\u001a\n\u0012\u0005\u0012\u0003H\u009b\u00010¤\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0003\u0010¥\u0001J\u0014\u0010¦\u0001\u001a\u00020!2\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010EH\u0017J\u0012\u0010¦\u0001\u001a\u00020!2\u0007\u0010\u009c\u0001\u001a\u00020!H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030§\u0001H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030¨\u0001H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030©\u0001H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030ª\u0001H\u0017J\u0012\u0010¦\u0001\u001a\u00020!2\u0007\u0010\u009c\u0001\u001a\u00020%H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030«\u0001H\u0017J\u0013\u0010¦\u0001\u001a\u00020!2\b\u0010\u009c\u0001\u001a\u00030¬\u0001H\u0017J\u0014\u0010\u00ad\u0001\u001a\u00020!2\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010EH\u0017J\u000f\u0010®\u0001\u001a\u00020\u0014H\u0000¢\u0006\u0003\b¯\u0001J\t\u0010°\u0001\u001a\u00020\u0014H\u0002J\t\u0010±\u0001\u001a\u00020\u0014H\u0002J\t\u0010²\u0001\u001a\u00020\u0014H\u0016JG\u0010³\u0001\u001a\u00020\u00142\u001d\u0010´\u0001\u001a\u0018\u0012\u0004\u0012\u000208\u0012\r\u0012\u000b\u0012\u0004\u0012\u00020E\u0018\u00010¶\u00010µ\u00012\u0014\u0010·\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u00140¤\u0001¢\u0006\u0003\b¸\u0001H\u0000¢\u0006\u0006\b¹\u0001\u0010º\u0001J$\u0010»\u0001\u001a\u00020%2\u0007\u0010¼\u0001\u001a\u00020%2\u0007\u0010½\u0001\u001a\u00020%2\u0007\u0010¾\u0001\u001a\u00020%H\u0002J'\u0010¿\u0001\u001a\u0003H\u009b\u0001\"\u0005\b\u0000\u0010\u009b\u00012\u000e\u0010\u00c0\u0001\u001a\t\u0012\u0005\u0012\u0003H\u009b\u00010nH\u0017¢\u0006\u0003\u0010\u00c1\u0001J\t\u0010\u00c2\u0001\u001a\u00020\u0014H\u0002J!\u0010\u00c3\u0001\u001a\u00020\u0014\"\u0005\b\u0000\u0010\u009b\u00012\u000f\u0010\u00c4\u0001\u001a\n\u0012\u0005\u0012\u0003H\u009b\u00010¤\u0001H\u0016J)\u0010\u00c5\u0001\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`pH\u0002J2\u0010\u00c5\u0001\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`p2\u0007\u0010¼\u0001\u001a\u00020%H\u0002J\u0012\u0010\u00c6\u0001\u001a\u00020\u00142\u0007\u0010¦\u0001\u001a\u00020!H\u0017J\t\u0010\u00c7\u0001\u001a\u00020\u0014H\u0016J\t\u0010\u00c8\u0001\u001a\u00020\u0014H\u0016J\u000f\u0010\u00c9\u0001\u001a\u00020\u0014H\u0000¢\u0006\u0003\b\u00ca\u0001JF\u0010\u00cb\u0001\u001a\u00020\u00142\u001d\u0010´\u0001\u001a\u0018\u0012\u0004\u0012\u000208\u0012\r\u0012\u000b\u0012\u0004\u0012\u00020E\u0018\u00010¶\u00010µ\u00012\u0016\u0010·\u0001\u001a\u0011\u0012\u0004\u0012\u00020\u0014\u0018\u00010¤\u0001¢\u0006\u0003\b¸\u0001H\u0002¢\u0006\u0003\u0010º\u0001J\u001b\u0010\u00cc\u0001\u001a\u00020\u00142\u0007\u0010¼\u0001\u001a\u00020%2\u0007\u0010\u00cd\u0001\u001a\u00020%H\u0002J\t\u0010\u00ce\u0001\u001a\u00020\u0014H\u0016J\u0012\u0010\u00cf\u0001\u001a\u00020\u00142\u0007\u0010\u00d0\u0001\u001a\u00020!H\u0002J\t\u0010\u00d1\u0001\u001a\u00020\u0014H\u0017J\t\u0010\u00d2\u0001\u001a\u00020\u0014H\u0002J\t\u0010\u00d3\u0001\u001a\u00020\u0014H\u0017J\t\u0010\u00d4\u0001\u001a\u00020\u0014H\u0016J\t\u0010\u00d5\u0001\u001a\u00020\u0014H\u0017J\t\u0010\u00d6\u0001\u001a\u00020\u0014H\u0017J\f\u0010\u00d7\u0001\u001a\u0005\u0018\u00010\u00d8\u0001H\u0017J\t\u0010\u00d9\u0001\u001a\u00020\u0014H\u0016J\t\u0010\u00da\u0001\u001a\u00020\u0014H\u0002J\u0012\u0010\u00db\u0001\u001a\u00020\u00142\u0007\u0010\u00dc\u0001\u001a\u00020%H\u0016J\t\u0010\u00dd\u0001\u001a\u00020\u0014H\u0002J\u001d\u0010\u00de\u0001\u001a\u00020\u00142\u0007\u0010\u00d0\u0001\u001a\u00020!2\t\u0010\u00df\u0001\u001a\u0004\u0018\u00010rH\u0002J\u001a\u0010\u00e0\u0001\u001a\u00020\u00142\u0007\u0010\u00e1\u0001\u001a\u00020%2\u0006\u0010Z\u001a\u00020!H\u0002J\t\u0010\u00e2\u0001\u001a\u00020\u0014H\u0002J\u000e\u0010H\u001a\u00020!H\u0000¢\u0006\u0003\b\u00e3\u0001J\"\u0010\u00e4\u0001\u001a\u00020\u00142\f\u0010\u009c\u0001\u001a\u0007\u0012\u0002\b\u00030\u00e5\u00012\t\u0010\u00e6\u0001\u001a\u0004\u0018\u00010EH\u0017J*\u0010\u00e7\u0001\u001a\u00020\u00142\u001f\u0010\u00e8\u0001\u001a\u001a\u0012\u0015\u0012\u0013\u0012\u0005\u0012\u00030\u00eb\u0001\u0012\u0007\u0012\u0005\u0018\u00010\u00eb\u00010\u00ea\u00010\u00e9\u0001H\u0002J*\u0010\u00ec\u0001\u001a\u00020\u00142\u001f\u0010\u00e8\u0001\u001a\u001a\u0012\u0015\u0012\u0013\u0012\u0005\u0012\u00030\u00eb\u0001\u0012\u0007\u0012\u0005\u0018\u00010\u00eb\u00010\u00ea\u00010\u00e9\u0001H\u0017J\u0012\u0010\u00ed\u0001\u001a\u00020%2\u0007\u0010\u00ee\u0001\u001a\u00020%H\u0002JX\u0010\u00ef\u0001\u001a\u00020\u00142\u0010\u0010·\u0001\u001a\u000b\u0012\u0006\u0012\u0004\u0018\u00010E0\u00e5\u00012'\u0010\u00f0\u0001\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`p2\t\u0010\u00e6\u0001\u001a\u0004\u0018\u00010E2\u0007\u0010\u00f1\u0001\u001a\u00020!H\u0002J\u001f\u0010\u00f2\u0001\u001a\u00020E2\t\u0010\u00f3\u0001\u001a\u0004\u0018\u00010E2\t\u0010\u00f4\u0001\u001a\u0004\u0018\u00010EH\u0017J\u000b\u0010\u00f5\u0001\u001a\u0004\u0018\u00010EH\u0001J-\u0010\u00f6\u0001\u001a\u00020%2\u0007\u0010\u00f7\u0001\u001a\u00020%2\u0007\u0010¼\u0001\u001a\u00020%2\u0007\u0010½\u0001\u001a\u00020%2\u0007\u0010\u00f8\u0001\u001a\u00020%H\u0002J\u000f\u0010\u00f9\u0001\u001a\u00020%H\u0000¢\u0006\u0003\b\u00fa\u0001J\u001f\u0010\u00fb\u0001\u001a\u00020\u00142\u000e\u0010\u009d\u0001\u001a\t\u0012\u0004\u0012\u00020\u00140¤\u0001H\u0000¢\u0006\u0003\b\u00fc\u0001J\t\u0010\u00fd\u0001\u001a\u00020\u0014H\u0002J!\u0010\u00fd\u0001\u001a\u00020\u00142\u0010\u0010\u00fe\u0001\u001a\u000b\u0012\u0006\u0012\u0004\u0018\u00010E0\u00ff\u0001H\u0002¢\u0006\u0003\u0010\u0080\u0002J\t\u0010\u0081\u0002\u001a\u00020\u0014H\u0002J\u0014\u0010\u0082\u0002\u001a\u00020\u00142\t\b\u0002\u0010\u0083\u0002\u001a\u00020!H\u0002J\t\u0010\u0084\u0002\u001a\u00020\u0014H\u0002J.\u0010\u0085\u0002\u001a\u00020!2\u001d\u0010´\u0001\u001a\u0018\u0012\u0004\u0012\u000208\u0012\r\u0012\u000b\u0012\u0004\u0012\u00020E\u0018\u00010¶\u00010µ\u0001H\u0000¢\u0006\u0003\b\u0086\u0002Jv\u0010\u0087\u0002\u001a\u0003H\u0088\u0002\"\u0005\b\u0000\u0010\u0088\u00022\u000b\b\u0002\u0010\u0089\u0002\u001a\u0004\u0018\u00010\u00182\u000b\b\u0002\u0010\u008a\u0002\u001a\u0004\u0018\u00010\u00182\u000b\b\u0002\u0010\u00ee\u0001\u001a\u0004\u0018\u00010%2%\b\u0002\u0010^\u001a\u001f\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u000208\u0012\r\u0012\u000b\u0012\u0004\u0012\u00020E\u0018\u00010¶\u00010\u00ea\u00010\u00e9\u00012\u000f\u0010\u009d\u0001\u001a\n\u0012\u0005\u0012\u0003H\u0088\u00020¤\u0001H\u0002¢\u0006\u0003\u0010\u008b\u0002J\t\u0010\u008c\u0002\u001a\u00020\u0014H\u0002J_\u0010\u008d\u0002\u001a\u00020\u00142T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002J_\u0010\u008f\u0002\u001a\u00020\u00142T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002J\t\u0010\u0090\u0002\u001a\u00020\u0014H\u0002J\u0014\u0010\u0091\u0002\u001a\u00020\u00142\t\u0010\u0094\u0001\u001a\u0004\u0018\u00010EH\u0002J\t\u0010\u0092\u0002\u001a\u00020\u0014H\u0002J\t\u0010\u0093\u0002\u001a\u00020\u0014H\u0002J_\u0010\u0094\u0002\u001a\u00020\u00142T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002J\u0012\u0010\u0095\u0002\u001a\u00020\u00142\u0007\u0010\u0096\u0002\u001a\u00020RH\u0002J_\u0010\u0097\u0002\u001a\u00020\u00142T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002J$\u0010\u0098\u0002\u001a\u00020\u00142\u0007\u0010\u0089\u0002\u001a\u00020%2\u0007\u0010\u008a\u0002\u001a\u00020%2\u0007\u0010\u0099\u0002\u001a\u00020%H\u0002J\u0012\u0010\u009a\u0002\u001a\u00020\u00142\u0007\u0010\u009b\u0002\u001a\u00020%H\u0002J\u001a\u0010\u009c\u0002\u001a\u00020\u00142\u0006\u0010j\u001a\u00020%2\u0007\u0010\u0099\u0002\u001a\u00020%H\u0002J\u0019\u0010\u009d\u0002\u001a\u00020\u00142\u000e\u0010\u009e\u0002\u001a\t\u0012\u0004\u0012\u00020\u00140¤\u0001H\u0016J\t\u0010\u009f\u0002\u001a\u00020\u0014H\u0002J_\u0010 \u0002\u001a\u00020\u00142T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002Jj\u0010¡\u0002\u001a\u00020\u00142\t\b\u0002\u0010\u0083\u0002\u001a\u00020!2T\u0010\u008e\u0002\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015H\u0002J\t\u0010¢\u0002\u001a\u00020\u0014H\u0002J$\u0010£\u0002\u001a\u00020\u00142\u0007\u0010¤\u0002\u001a\u00020%2\u0007\u0010¥\u0002\u001a\u00020%2\u0007\u0010¦\u0002\u001a\u00020%H\u0002J\u0013\u0010§\u0002\u001a\u00020\u00142\b\u0010¨\u0002\u001a\u00030\u0081\u0001H\u0016J\t\u0010©\u0002\u001a\u00020\u0014H\u0002J\u001b\u0010ª\u0002\u001a\u00020\u00142\b\u0010«\u0002\u001a\u00030\u00eb\u00012\u0006\u0010\u0011\u001a\u00020\u0010H\u0002J\u000b\u0010¬\u0002\u001a\u0004\u0018\u00010EH\u0016J\t\u0010\u00ad\u0002\u001a\u00020\u0014H\u0002J\u0012\u0010®\u0002\u001a\u00020\u00142\u0007\u0010¯\u0002\u001a\u00020%H\u0002JP\u0010°\u0002\u001a\u0003H\u009b\u0001\"\u0005\b\u0000\u0010\u009b\u00012\u000e\u0010\u00c0\u0001\u001a\t\u0012\u0005\u0012\u0003H\u009b\u00010n2'\u0010¨\u0002\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`pH\u0002¢\u0006\u0003\u0010±\u0002J\t\u0010²\u0002\u001a\u00020\u0014H\u0017J\t\u0010³\u0002\u001a\u00020\u0014H\u0002J\t\u0010´\u0002\u001a\u00020\u0014H\u0002J\t\u0010µ\u0002\u001a\u00020\u0014H\u0017J\u0013\u0010¶\u0002\u001a\u00020\u00142\b\u0010¶\u0002\u001a\u00030·\u0002H\u0017J\t\u0010¸\u0002\u001a\u00020\u0014H\u0017J\u001c\u0010¹\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%2\b\u0010¶\u0002\u001a\u00030·\u0002H\u0017JA\u0010º\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%2\t\u0010»\u0002\u001a\u0004\u0018\u00010E2\b\u0010¼\u0002\u001a\u00030½\u00022\t\u0010¾\u0002\u001a\u0004\u0018\u00010EH\u0002\u00f8\u0001\u0001\u00f8\u0001\u0002¢\u0006\u0006\b¿\u0002\u0010\u00c0\u0002J\t\u0010\u00c1\u0002\u001a\u00020\u0014H\u0017J\u0012\u0010\u00c2\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%H\u0002J\u001d\u0010\u00c2\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%2\t\u0010\u00c3\u0002\u001a\u0004\u0018\u00010EH\u0002J\u001d\u0010\u00c4\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%2\t\u0010\u00c3\u0002\u001a\u0004\u0018\u00010EH\u0017J\t\u0010\u00c5\u0002\u001a\u00020\u0014H\u0016J&\u0010\u00c6\u0002\u001a\u00020\u00142\u0015\u0010\u00c7\u0002\u001a\u0010\u0012\u000b\b\u0001\u0012\u0007\u0012\u0002\b\u00030\u00c8\u00020\u00ff\u0001H\u0017¢\u0006\u0003\u0010\u00c9\u0002J\u001d\u0010\u00ca\u0002\u001a\u00020\u00142\u0007\u0010\u00d0\u0001\u001a\u00020!2\t\u0010¾\u0002\u001a\u0004\u0018\u00010EH\u0002J\u0012\u0010\u00cb\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%H\u0017J\u0012\u0010\u00cc\u0002\u001a\u00020\u00012\u0007\u0010\u00c0\u0001\u001a\u00020%H\u0017J\u001d\u0010\u00cd\u0002\u001a\u00020\u00142\u0007\u0010\u00c0\u0001\u001a\u00020%2\t\u0010\u00c3\u0002\u001a\u0004\u0018\u00010EH\u0016J\t\u0010\u00ce\u0002\u001a\u00020\u0014H\u0016J\t\u0010\u00cf\u0002\u001a\u00020\u0014H\u0002J#\u0010\u00d0\u0002\u001a\u00020!2\u0007\u0010¨\u0002\u001a\u0002082\t\u0010\u00d1\u0002\u001a\u0004\u0018\u00010EH\u0000¢\u0006\u0003\b\u00d2\u0002J\u0014\u0010\u00d3\u0002\u001a\u00020\u00142\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010EH\u0001J(\u0010\u00d4\u0002\u001a\u00020\u00142\u0007\u0010\u00d5\u0002\u001a\u00020%2\t\u0010\u00c3\u0002\u001a\u0004\u0018\u00010E2\t\u0010¾\u0002\u001a\u0004\u0018\u00010EH\u0002J\u0012\u0010\u00d6\u0002\u001a\u00020\u00142\u0007\u0010\u00d7\u0002\u001a\u00020%H\u0002J(\u0010\u00d8\u0002\u001a\u00020\u00142\u0007\u0010\u00d5\u0002\u001a\u00020%2\t\u0010\u00c3\u0002\u001a\u0004\u0018\u00010E2\t\u0010¾\u0002\u001a\u0004\u0018\u00010EH\u0002J\u0012\u0010\u00d9\u0002\u001a\u00020\u00142\u0007\u0010\u00d5\u0002\u001a\u00020%H\u0002J\u001b\u0010\u00da\u0002\u001a\u00020\u00142\u0007\u0010¼\u0001\u001a\u00020%2\u0007\u0010\u0099\u0002\u001a\u00020%H\u0002J\u001b\u0010\u00db\u0002\u001a\u00020\u00142\u0007\u0010¼\u0001\u001a\u00020%2\u0007\u0010\u00dc\u0002\u001a\u00020%H\u0002J{\u0010\u00dd\u0002\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`p2'\u0010\u00de\u0002\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`p2'\u0010\u00df\u0002\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`pH\u0002J\u0014\u0010\u00e0\u0002\u001a\u00020\u00142\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010EH\u0016J\u0014\u0010\u00e1\u0002\u001a\u00020\u00142\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010EH\u0001J\u0012\u0010\u00e2\u0002\u001a\u00020%2\u0007\u0010¼\u0001\u001a\u00020%H\u0002J\t\u0010\u00e3\u0002\u001a\u00020\u0014H\u0016J\t\u0010\u00e4\u0002\u001a\u00020\u0014H\u0002J\t\u0010\u00e5\u0002\u001a\u00020\u0014H\u0002J\u000f\u0010\u00e6\u0002\u001a\u00020\u0014H\u0000¢\u0006\u0003\b\u00e7\u0002J\u0085\u0001\u0010\u00e8\u0002\u001a\u0003H\u0088\u0002\"\u0005\b\u0000\u0010\u0088\u00022Z\u0010\u00e9\u0002\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\f2\u000f\u0010\u009d\u0001\u001a\n\u0012\u0005\u0012\u0003H\u0088\u00020¤\u0001H\u0082\b¢\u0006\u0003\u0010\u00ea\u0002J1\u0010\u00eb\u0002\u001a\u0003H\u0088\u0002\"\u0005\b\u0000\u0010\u0088\u00022\u0006\u0010~\u001a\u00020\u007f2\u000f\u0010\u009d\u0001\u001a\n\u0012\u0005\u0012\u0003H\u0088\u00020¤\u0001H\u0082\b¢\u0006\u0003\u0010\u00ec\u0002J\u0016\u0010\u00ed\u0002\u001a\u00020%*\u00020\u007f2\u0007\u0010¼\u0001\u001a\u00020%H\u0002J\u0018\u0010\u00ee\u0002\u001a\u0004\u0018\u00010E*\u00020\u007f2\u0007\u0010\u00ee\u0001\u001a\u00020%H\u0002R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001d8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020!8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\"\u0010#R\u0014\u0010$\u001a\u00020%8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b&\u0010'Ra\u0010\u000b\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u0018X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0014\u0010+\u001a\u00020,8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b-\u0010.R\u000e\u0010/\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R&\u00101\u001a\u00020%2\u0006\u00100\u001a\u00020%8\u0016@RX\u0097\u000e¢\u0006\u000e\n\u0000\u0012\u0004\b2\u00103\u001a\u0004\b4\u0010'R\u0014\u00105\u001a\u00020%8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b6\u0010'R\u0016\u00107\u001a\u0004\u0018\u0001088@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b9\u0010:R\u001a\u0010;\u001a\u00020!8VX\u0097\u0004¢\u0006\f\u0012\u0004\b<\u00103\u001a\u0004\b=\u0010#Ro\u0010>\u001aW\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u0015\u0018\u00010\fX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b?\u0010@\"\u0004\bA\u0010BR\u0016\u0010C\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010E0DX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020GX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010H\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010I\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020GX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010L\u001a\u00020!8F¢\u0006\u0006\u001a\u0004\bM\u0010#R\u0014\u0010N\u001a\u00020!8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\bO\u0010#R\u000e\u0010P\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010Q\u001a\u00020RX\u0082\u000e¢\u0006\u0002\n\u0000Ra\u0010S\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010T\u001a\u00020\u0007X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bU\u0010V\"\u0004\bW\u0010XRa\u0010Y\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150DX\u0082\u0004¢\u0006\u0002\n\u0000R&\u0010Z\u001a\u00020!2\u0006\u00100\u001a\u00020!8\u0016@RX\u0097\u000e¢\u0006\u000e\n\u0000\u0012\u0004\b[\u00103\u001a\u0004\b\\\u0010#R\u0014\u0010]\u001a\b\u0012\u0004\u0012\u0002080DX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010^\u001a\b\u0012\u0004\u0012\u00020_0\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010`\u001a\u00020!2\u0006\u00100\u001a\u00020!@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\ba\u0010#R\u001e\u0010b\u001a\u00020!2\u0006\u00100\u001a\u00020!@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\bc\u0010#Ra\u0010\u0016\u001aU\u0012Q\u0012O\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u00140\rj\u0002`\u00150\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010d\u001a\u0004\u0018\u00010eX\u0082\u000e¢\u0006\u0002\n\u0000R.\u0010f\u001a\"\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020%\u0018\u00010gj\u0010\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020%\u0018\u0001`hX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010i\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010j\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010k\u001a\u00020GX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R.\u0010l\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`pX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010q\u001a\u0004\u0018\u00010rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010s\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010r0DX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010t\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010u\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010v\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010w\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010x\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R2\u0010y\u001a&\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o\u0018\u00010mj\u0004\u0018\u0001`pX\u0082\u000e¢\u0006\u0002\n\u0000R4\u0010z\u001a(\u0012$\u0012\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0n\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010E0o0mj\u0002`p0{X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010|\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010}\u001a\u00020GX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010~\u001a\u00020\u007fX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0080\u0001\u001a\u0005\u0018\u00010\u0081\u00018VX\u0096\u0004¢\u0006\b\u001a\u0006\b\u0082\u0001\u0010\u0083\u0001R\u0019\u0010\u0084\u0001\u001a\u0004\u0018\u00010E8VX\u0096\u0004¢\u0006\b\u001a\u0006\b\u0085\u0001\u0010\u0086\u0001R\u000f\u0010\u0087\u0001\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u0088\u0001\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u001d\u0010\u0089\u0001\u001a\u00020!8VX\u0097\u0004¢\u0006\u000e\u0012\u0005\b\u008a\u0001\u00103\u001a\u0005\b\u008b\u0001\u0010#R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u008c\u0001\u001a\u00030\u008d\u0001X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u008e\u0001\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u008f\u0001\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u0090\u0001\u001a\u00020GX\u0082\u0004¢\u0006\u0002\n\u0000R\u000f\u0010\u0091\u0001\u001a\u00020\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u0092\u0001\u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010\u0093\u0001\u001a\u00020%X\u0082\u000e¢\u0006\u0002\n\u0000R\u001d\u0010\u0094\u0001\u001a\u0004\u0018\u00010E*\u00020\u007f8BX\u0082\u0004¢\u0006\b\u001a\u0006\b\u0095\u0001\u0010\u0096\u0001\u0082\u0002\u0012\n\u0005\b\u009920\u0001\n\u0005\b¡\u001e0\u0001\n\u0002\b\u0019¨\u0006\u00f1\u0002" }, d2 = { "Landroidx/compose/runtime/ComposerImpl;", "Landroidx/compose/runtime/Composer;", "applier", "Landroidx/compose/runtime/Applier;", "parentContext", "Landroidx/compose/runtime/CompositionContext;", "slotTable", "Landroidx/compose/runtime/SlotTable;", "abandonSet", "", "Landroidx/compose/runtime/RememberObserver;", "changes", "", "Lkotlin/Function3;", "Lkotlin/ParameterName;", "name", "Landroidx/compose/runtime/SlotWriter;", "slots", "Landroidx/compose/runtime/RememberManager;", "rememberManager", "", "Landroidx/compose/runtime/Change;", "lateChanges", "composition", "Landroidx/compose/runtime/ControlledComposition;", "(Landroidx/compose/runtime/Applier;Landroidx/compose/runtime/CompositionContext;Landroidx/compose/runtime/SlotTable;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Landroidx/compose/runtime/ControlledComposition;)V", "getApplier", "()Landroidx/compose/runtime/Applier;", "applyCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "getApplyCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "areChildrenComposing", "", "getAreChildrenComposing$runtime_release", "()Z", "changeCount", "", "getChangeCount$runtime_release", "()I", "childrenComposing", "getComposition", "()Landroidx/compose/runtime/ControlledComposition;", "compositionData", "Landroidx/compose/runtime/tooling/CompositionData;", "getCompositionData", "()Landroidx/compose/runtime/tooling/CompositionData;", "compositionToken", "<set-?>", "compoundKeyHash", "getCompoundKeyHash$annotations", "()V", "getCompoundKeyHash", "currentMarker", "getCurrentMarker", "currentRecomposeScope", "Landroidx/compose/runtime/RecomposeScopeImpl;", "getCurrentRecomposeScope$runtime_release", "()Landroidx/compose/runtime/RecomposeScopeImpl;", "defaultsInvalid", "getDefaultsInvalid$annotations", "getDefaultsInvalid", "deferredChanges", "getDeferredChanges$runtime_release", "()Ljava/util/List;", "setDeferredChanges$runtime_release", "(Ljava/util/List;)V", "downNodes", "Landroidx/compose/runtime/Stack;", "", "entersStack", "Landroidx/compose/runtime/IntStack;", "forceRecomposeScopes", "forciblyRecompose", "groupNodeCount", "groupNodeCountStack", "hasInvalidations", "getHasInvalidations", "hasPendingChanges", "getHasPendingChanges$runtime_release", "implicitRootStart", "insertAnchor", "Landroidx/compose/runtime/Anchor;", "insertFixups", "insertTable", "getInsertTable$runtime_release", "()Landroidx/compose/runtime/SlotTable;", "setInsertTable$runtime_release", "(Landroidx/compose/runtime/SlotTable;)V", "insertUpFixups", "inserting", "getInserting$annotations", "getInserting", "invalidateStack", "invalidations", "Landroidx/compose/runtime/Invalidation;", "isComposing", "isComposing$runtime_release", "isDisposed", "isDisposed$runtime_release", "nodeCountOverrides", "", "nodeCountVirtualOverrides", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "nodeExpected", "nodeIndex", "nodeIndexStack", "parentProvider", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "Landroidx/compose/runtime/CompositionLocal;", "Landroidx/compose/runtime/State;", "Landroidx/compose/runtime/CompositionLocalMap;", "pending", "Landroidx/compose/runtime/Pending;", "pendingStack", "pendingUps", "previousCount", "previousMoveFrom", "previousMoveTo", "previousRemove", "providerCache", "providerUpdates", "Landroidx/compose/runtime/collection/IntMap;", "providersInvalid", "providersInvalidStack", "reader", "Landroidx/compose/runtime/SlotReader;", "recomposeScope", "Landroidx/compose/runtime/RecomposeScope;", "getRecomposeScope", "()Landroidx/compose/runtime/RecomposeScope;", "recomposeScopeIdentity", "getRecomposeScopeIdentity", "()Ljava/lang/Object;", "reusing", "reusingGroup", "skipping", "getSkipping$annotations", "getSkipping", "snapshot", "Landroidx/compose/runtime/snapshots/Snapshot;", "sourceInformationEnabled", "startedGroup", "startedGroups", "writer", "writerHasAProvider", "writersReaderDelta", "node", "getNode", "(Landroidx/compose/runtime/SlotReader;)Ljava/lang/Object;", "abortRoot", "addRecomposeScope", "apply", "V", "T", "value", "block", "Lkotlin/Function2;", "Lkotlin/ExtensionFunctionType;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "buildContext", "cache", "invalid", "Lkotlin/Function0;", "(ZLkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "changed", "", "", "", "", "", "", "changedInstance", "changesApplied", "changesApplied$runtime_release", "cleanUpCompose", "clearUpdatedNodeCounts", "collectParameterInformation", "composeContent", "invalidationsRequested", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Landroidx/compose/runtime/collection/IdentityArraySet;", "content", "Landroidx/compose/runtime/Composable;", "composeContent$runtime_release", "(Landroidx/compose/runtime/collection/IdentityArrayMap;Lkotlin/jvm/functions/Function2;)V", "compoundKeyOf", "group", "recomposeGroup", "recomposeKey", "consume", "key", "(Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;", "createFreshInsertTable", "createNode", "factory", "currentCompositionLocalScope", "deactivateToEndGroup", "disableReusing", "disableSourceInformation", "dispose", "dispose$runtime_release", "doCompose", "doRecordDownsFor", "nearestCommonRoot", "enableReusing", "end", "isNode", "endDefaults", "endGroup", "endMovableGroup", "endNode", "endProviders", "endReplaceableGroup", "endRestartGroup", "Landroidx/compose/runtime/ScopeUpdateScope;", "endReusableGroup", "endRoot", "endToMarker", "marker", "ensureWriter", "enterGroup", "newPending", "exitGroup", "expectedNodeCount", "finalizeCompose", "forceRecomposeScopes$runtime_release", "insertMovableContent", "Landroidx/compose/runtime/MovableContent;", "parameter", "insertMovableContentGuarded", "references", "", "Lkotlin/Pair;", "Landroidx/compose/runtime/MovableContentStateReference;", "insertMovableContentReferences", "insertedGroupVirtualIndex", "index", "invokeMovableContentLambda", "locals", "force", "joinKey", "left", "right", "nextSlot", "nodeIndexOf", "groupLocation", "recomposeIndex", "parentKey", "parentKey$runtime_release", "prepareCompose", "prepareCompose$runtime_release", "realizeDowns", "nodes", "", "([Ljava/lang/Object;)V", "realizeMovement", "realizeOperationLocation", "forParent", "realizeUps", "recompose", "recompose$runtime_release", "recomposeMovableContent", "R", "from", "to", "(Landroidx/compose/runtime/ControlledComposition;Landroidx/compose/runtime/ControlledComposition;Ljava/lang/Integer;Ljava/util/List;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "recomposeToGroupEnd", "record", "change", "recordApplierOperation", "recordDelete", "recordDown", "recordEndGroup", "recordEndRoot", "recordFixup", "recordInsert", "anchor", "recordInsertUpFixup", "recordMoveNode", "count", "recordReaderMoving", "location", "recordRemoveNode", "recordSideEffect", "effect", "recordSlotEditing", "recordSlotEditingOperation", "recordSlotTableOperation", "recordUp", "recordUpsAndDowns", "oldGroup", "newGroup", "commonRoot", "recordUsed", "scope", "registerInsertUpFixup", "releaseMovableGroupAtCurrent", "reference", "rememberedValue", "reportAllMovableContent", "reportFreeMovableContent", "groupBeingRemoved", "resolveCompositionLocal", "(Landroidx/compose/runtime/CompositionLocal;Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;)Ljava/lang/Object;", "skipCurrentGroup", "skipGroup", "skipReaderToGroupEnd", "skipToGroupEnd", "sourceInformation", "", "sourceInformationMarkerEnd", "sourceInformationMarkerStart", "start", "objectKey", "kind", "Landroidx/compose/runtime/GroupKind;", "data", "start-BaiHCIY", "(ILjava/lang/Object;ILjava/lang/Object;)V", "startDefaults", "startGroup", "dataKey", "startMovableGroup", "startNode", "startProviders", "values", "Landroidx/compose/runtime/ProvidedValue;", "([Landroidx/compose/runtime/ProvidedValue;)V", "startReaderGroup", "startReplaceableGroup", "startRestartGroup", "startReusableGroup", "startReusableNode", "startRoot", "tryImminentInvalidation", "instance", "tryImminentInvalidation$runtime_release", "updateCachedValue", "updateCompoundKeyWhenWeEnterGroup", "groupKey", "updateCompoundKeyWhenWeEnterGroupKeyHash", "keyHash", "updateCompoundKeyWhenWeExitGroup", "updateCompoundKeyWhenWeExitGroupKeyHash", "updateNodeCount", "updateNodeCountOverrides", "newCount", "updateProviderMapGroup", "parentScope", "currentProviders", "updateRememberedValue", "updateValue", "updatedNodeCount", "useNode", "validateNodeExpected", "validateNodeNotExpected", "verifyConsistent", "verifyConsistent$runtime_release", "withChanges", "newChanges", "(Ljava/util/List;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "withReader", "(Landroidx/compose/runtime/SlotReader;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "groupCompoundKeyPart", "nodeAt", "CompositionContextHolder", "CompositionContextImpl", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposerImpl implements Composer
{
    private final Set<RememberObserver> abandonSet;
    private final Applier<?> applier;
    private List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> changes;
    private int childrenComposing;
    private final ControlledComposition composition;
    private int compositionToken;
    private int compoundKeyHash;
    private List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> deferredChanges;
    private Stack<Object> downNodes;
    private final IntStack entersStack;
    private boolean forceRecomposeScopes;
    private boolean forciblyRecompose;
    private int groupNodeCount;
    private IntStack groupNodeCountStack;
    private boolean implicitRootStart;
    private Anchor insertAnchor;
    private final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> insertFixups;
    private SlotTable insertTable;
    private final Stack<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> insertUpFixups;
    private boolean inserting;
    private final Stack<RecomposeScopeImpl> invalidateStack;
    private final List<Invalidation> invalidations;
    private boolean isComposing;
    private boolean isDisposed;
    private List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> lateChanges;
    private int[] nodeCountOverrides;
    private HashMap<Integer, Integer> nodeCountVirtualOverrides;
    private boolean nodeExpected;
    private int nodeIndex;
    private IntStack nodeIndexStack;
    private final CompositionContext parentContext;
    private PersistentMap<CompositionLocal<Object>, ? extends State<?>> parentProvider;
    private Pending pending;
    private final Stack<Pending> pendingStack;
    private int pendingUps;
    private int previousCount;
    private int previousMoveFrom;
    private int previousMoveTo;
    private int previousRemove;
    private PersistentMap<CompositionLocal<Object>, ? extends State<?>> providerCache;
    private final IntMap<PersistentMap<CompositionLocal<Object>, State<Object>>> providerUpdates;
    private boolean providersInvalid;
    private final IntStack providersInvalidStack;
    private SlotReader reader;
    private boolean reusing;
    private int reusingGroup;
    private final SlotTable slotTable;
    private Snapshot snapshot;
    private boolean sourceInformationEnabled;
    private boolean startedGroup;
    private final IntStack startedGroups;
    private SlotWriter writer;
    private boolean writerHasAProvider;
    private int writersReaderDelta;
    
    public ComposerImpl(Applier<?> openReader, final CompositionContext parentContext, final SlotTable slotTable, final Set<RememberObserver> abandonSet, final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> changes, final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> lateChanges, final ControlledComposition composition) {
        Intrinsics.checkNotNullParameter((Object)openReader, "applier");
        Intrinsics.checkNotNullParameter((Object)parentContext, "parentContext");
        Intrinsics.checkNotNullParameter((Object)slotTable, "slotTable");
        Intrinsics.checkNotNullParameter((Object)abandonSet, "abandonSet");
        Intrinsics.checkNotNullParameter((Object)changes, "changes");
        Intrinsics.checkNotNullParameter((Object)lateChanges, "lateChanges");
        Intrinsics.checkNotNullParameter((Object)composition, "composition");
        this.applier = (Applier<?>)openReader;
        this.parentContext = parentContext;
        this.slotTable = slotTable;
        this.abandonSet = abandonSet;
        this.changes = changes;
        this.lateChanges = lateChanges;
        this.composition = composition;
        this.pendingStack = new Stack<Pending>();
        this.nodeIndexStack = new IntStack();
        this.groupNodeCountStack = new IntStack();
        this.invalidations = new ArrayList<Invalidation>();
        this.entersStack = new IntStack();
        this.parentProvider = ExtensionsKt.persistentHashMapOf();
        this.providerUpdates = new IntMap<PersistentMap<CompositionLocal<Object>, State<Object>>>(0, 1, null);
        this.providersInvalidStack = new IntStack();
        this.reusingGroup = -1;
        this.snapshot = SnapshotKt.currentSnapshot();
        this.sourceInformationEnabled = true;
        this.invalidateStack = new Stack<RecomposeScopeImpl>();
        final SlotReader openReader2 = slotTable.openReader();
        openReader2.close();
        this.reader = openReader2;
        final SlotTable insertTable = new SlotTable();
        this.insertTable = insertTable;
        final SlotWriter openWriter = insertTable.openWriter();
        openWriter.close();
        this.writer = openWriter;
        openReader = this.insertTable.openReader();
        try {
            final Anchor anchor = openReader.anchor(0);
            openReader.close();
            this.insertAnchor = anchor;
            this.insertFixups = new ArrayList<Function3<Applier<?>, SlotWriter, RememberManager, Unit>>();
            this.downNodes = new Stack<Object>();
            this.implicitRootStart = true;
            this.startedGroups = new IntStack();
            this.insertUpFixups = new Stack<Function3<Applier<?>, SlotWriter, RememberManager, Unit>>();
            this.previousRemove = -1;
            this.previousMoveFrom = -1;
            this.previousMoveTo = -1;
        }
        finally {
            openReader.close();
        }
    }
    
    private final void abortRoot() {
        this.cleanUpCompose();
        this.pendingStack.clear();
        this.nodeIndexStack.clear();
        this.groupNodeCountStack.clear();
        this.entersStack.clear();
        this.providersInvalidStack.clear();
        this.providerUpdates.clear();
        if (!this.reader.getClosed()) {
            this.reader.close();
        }
        if (!this.writer.getClosed()) {
            this.writer.close();
        }
        this.createFreshInsertTable();
        this.compoundKeyHash = 0;
        this.childrenComposing = 0;
        this.nodeExpected = false;
        this.inserting = false;
        this.reusing = false;
        this.isComposing = false;
        this.forciblyRecompose = false;
    }
    
    public static final /* synthetic */ List access$getChanges$p(final ComposerImpl composerImpl) {
        return composerImpl.changes;
    }
    
    public static final /* synthetic */ int access$getChildrenComposing$p(final ComposerImpl composerImpl) {
        return composerImpl.childrenComposing;
    }
    
    public static final /* synthetic */ int[] access$getNodeCountOverrides$p(final ComposerImpl composerImpl) {
        return composerImpl.nodeCountOverrides;
    }
    
    public static final /* synthetic */ CompositionContext access$getParentContext$p(final ComposerImpl composerImpl) {
        return composerImpl.parentContext;
    }
    
    public static final /* synthetic */ SlotReader access$getReader$p(final ComposerImpl composerImpl) {
        return composerImpl.reader;
    }
    
    public static final /* synthetic */ SlotTable access$getSlotTable$p(final ComposerImpl composerImpl) {
        return composerImpl.slotTable;
    }
    
    public static final /* synthetic */ void access$setChanges$p(final ComposerImpl composerImpl, final List changes) {
        composerImpl.changes = changes;
    }
    
    public static final /* synthetic */ void access$setChildrenComposing$p(final ComposerImpl composerImpl, final int childrenComposing) {
        composerImpl.childrenComposing = childrenComposing;
    }
    
    public static final /* synthetic */ void access$setNodeCountOverrides$p(final ComposerImpl composerImpl, final int[] nodeCountOverrides) {
        composerImpl.nodeCountOverrides = nodeCountOverrides;
    }
    
    public static final /* synthetic */ void access$setReader$p(final ComposerImpl composerImpl, final SlotReader reader) {
        composerImpl.reader = reader;
    }
    
    private final void addRecomposeScope() {
        if (this.getInserting()) {
            final ControlledComposition composition = this.getComposition();
            Intrinsics.checkNotNull((Object)composition, "null cannot be cast to non-null type androidx.compose.runtime.CompositionImpl");
            final RecomposeScopeImpl recomposeScopeImpl = new RecomposeScopeImpl((CompositionImpl)composition);
            this.invalidateStack.push(recomposeScopeImpl);
            this.updateValue(recomposeScopeImpl);
            recomposeScopeImpl.start(this.compositionToken);
        }
        else {
            final Invalidation access$removeLocation = ComposerKt.access$removeLocation(this.invalidations, this.reader.getParent());
            final Object next = this.reader.next();
            RecomposeScopeImpl recomposeScopeImpl2;
            if (Intrinsics.areEqual(next, Composer.Companion.getEmpty())) {
                final ControlledComposition composition2 = this.getComposition();
                Intrinsics.checkNotNull((Object)composition2, "null cannot be cast to non-null type androidx.compose.runtime.CompositionImpl");
                recomposeScopeImpl2 = new RecomposeScopeImpl((CompositionImpl)composition2);
                this.updateValue(recomposeScopeImpl2);
            }
            else {
                Intrinsics.checkNotNull(next, "null cannot be cast to non-null type androidx.compose.runtime.RecomposeScopeImpl");
                recomposeScopeImpl2 = (RecomposeScopeImpl)next;
            }
            recomposeScopeImpl2.setRequiresRecompose(access$removeLocation != null);
            this.invalidateStack.push(recomposeScopeImpl2);
            recomposeScopeImpl2.start(this.compositionToken);
        }
    }
    
    private final void cleanUpCompose() {
        this.pending = null;
        this.nodeIndex = 0;
        this.groupNodeCount = 0;
        this.writersReaderDelta = 0;
        this.compoundKeyHash = 0;
        this.nodeExpected = false;
        this.startedGroup = false;
        this.startedGroups.clear();
        this.invalidateStack.clear();
        this.clearUpdatedNodeCounts();
    }
    
    private final void clearUpdatedNodeCounts() {
        this.nodeCountOverrides = null;
        this.nodeCountVirtualOverrides = null;
    }
    
    private final int compoundKeyOf(final int n, final int n2, int n3) {
        if (n != n2) {
            final ComposerImpl composerImpl = this;
            final int groupCompoundKeyPart = this.groupCompoundKeyPart(this.reader, n);
            if (groupCompoundKeyPart == 126665345) {
                n3 = groupCompoundKeyPart;
            }
            else {
                n3 = (Integer.rotateLeft(this.compoundKeyOf(this.reader.parent(n), n2, n3), 3) ^ groupCompoundKeyPart);
            }
        }
        return n3;
    }
    
    private final void createFreshInsertTable() {
        ComposerKt.runtimeCheck(this.writer.getClosed());
        final SlotTable insertTable = new SlotTable();
        this.insertTable = insertTable;
        final SlotWriter openWriter = insertTable.openWriter();
        openWriter.close();
        this.writer = openWriter;
    }
    
    private final PersistentMap<CompositionLocal<Object>, State<Object>> currentCompositionLocalScope() {
        final PersistentMap<CompositionLocal<Object>, ? extends State<?>> providerCache = this.providerCache;
        if (providerCache != null) {
            return (PersistentMap<CompositionLocal<Object>, State<Object>>)providerCache;
        }
        return this.currentCompositionLocalScope(this.reader.getParent());
    }
    
    private final PersistentMap<CompositionLocal<Object>, State<Object>> currentCompositionLocalScope(int i) {
        if (this.getInserting() && this.writerHasAProvider) {
            for (int j = this.writer.getParent(); j > 0; j = this.writer.parent(j)) {
                if (this.writer.groupKey(j) == 202 && Intrinsics.areEqual(this.writer.groupObjectKey(j), ComposerKt.getCompositionLocalMap())) {
                    final Object groupAux = this.writer.groupAux(j);
                    Intrinsics.checkNotNull(groupAux, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<androidx.compose.runtime.CompositionLocal<kotlin.Any?>, androidx.compose.runtime.State<kotlin.Any?>>{ androidx.compose.runtime.ComposerKt.CompositionLocalMap }");
                    return (PersistentMap<CompositionLocal<Object>, State<Object>>)(this.providerCache = (PersistentMap<CompositionLocal<Object>, ? extends State<?>>)groupAux);
                }
            }
        }
        if (this.reader.getSize() > 0) {
            while (i > 0) {
                if (this.reader.groupKey(i) == 202 && Intrinsics.areEqual(this.reader.groupObjectKey(i), ComposerKt.getCompositionLocalMap())) {
                    PersistentMap providerCache;
                    if ((providerCache = this.providerUpdates.get(i)) == null) {
                        final Object groupAux2 = this.reader.groupAux(i);
                        Intrinsics.checkNotNull(groupAux2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<androidx.compose.runtime.CompositionLocal<kotlin.Any?>, androidx.compose.runtime.State<kotlin.Any?>>{ androidx.compose.runtime.ComposerKt.CompositionLocalMap }");
                        providerCache = (PersistentMap)groupAux2;
                    }
                    return (PersistentMap<CompositionLocal<Object>, State<Object>>)(this.providerCache = providerCache);
                }
                i = this.reader.parent(i);
            }
        }
        return (PersistentMap<CompositionLocal<Object>, State<Object>>)(this.providerCache = this.parentProvider);
    }
    
    private final void doCompose(final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> identityArrayMap, final Function2<? super Composer, ? super Integer, Unit> function2) {
        if (this.isComposing ^ true) {
            final Object beginSection = Trace.INSTANCE.beginSection("Compose:recompose");
            try {
                final Snapshot currentSnapshot = SnapshotKt.currentSnapshot();
                this.snapshot = currentSnapshot;
                this.compositionToken = currentSnapshot.getId();
                this.providerUpdates.clear();
                for (int size$runtime_release = identityArrayMap.getSize$runtime_release(), i = 0; i < size$runtime_release; ++i) {
                    final Object o = identityArrayMap.getKeys$runtime_release()[i];
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
                    final IdentityArraySet set = (IdentityArraySet)identityArrayMap.getValues$runtime_release()[i];
                    final RecomposeScopeImpl recomposeScopeImpl = (RecomposeScopeImpl)o;
                    final Anchor anchor = recomposeScopeImpl.getAnchor();
                    if (anchor == null) {
                        return;
                    }
                    this.invalidations.add(new Invalidation(recomposeScopeImpl, anchor.getLocation$runtime_release(), set));
                }
                final List<Invalidation> invalidations = this.invalidations;
                if (invalidations.size() > 1) {
                    CollectionsKt.sortWith((List)invalidations, (Comparator)new ComposerImpl$doCompose$lambda$37$$inlined$sortBy.ComposerImpl$doCompose$lambda$37$$inlined$sortBy$1());
                }
                this.nodeIndex = 0;
                this.isComposing = true;
                try {
                    this.startRoot();
                    final Object nextSlot = this.nextSlot();
                    if (nextSlot != function2 && function2 != null) {
                        this.updateValue(function2);
                    }
                    SnapshotStateKt.observeDerivedStateRecalculations((Function1<? super State<?>, Unit>)new ComposerImpl$doCompose$2.ComposerImpl$doCompose$2$3(this), (Function1<? super State<?>, Unit>)new ComposerImpl$doCompose$2.ComposerImpl$doCompose$2$4(this), (kotlin.jvm.functions.Function0<?>)new ComposerImpl$doCompose$2.ComposerImpl$doCompose$2$5((Function2)function2, this, nextSlot));
                    this.endRoot();
                    this.isComposing = false;
                    this.invalidations.clear();
                    final Unit instance = Unit.INSTANCE;
                    return;
                }
                finally {
                    this.isComposing = false;
                    this.invalidations.clear();
                    this.abortRoot();
                }
            }
            finally {
                Trace.INSTANCE.endSection(beginSection);
            }
        }
        ComposerKt.composeRuntimeError("Reentrant composition is not supported".toString());
        throw new KotlinNothingValueException();
    }
    
    private final void doRecordDownsFor(final int n, final int n2) {
        if (n > 0 && n != n2) {
            this.doRecordDownsFor(this.reader.parent(n), n2);
            if (this.reader.isNode(n)) {
                this.recordDown(this.nodeAt(this.reader, n));
            }
        }
    }
    
    private final void end(final boolean b) {
        if (this.getInserting()) {
            final int parent = this.writer.getParent();
            this.updateCompoundKeyWhenWeExitGroup(this.writer.groupKey(parent), this.writer.groupObjectKey(parent), this.writer.groupAux(parent));
        }
        else {
            final int parent2 = this.reader.getParent();
            this.updateCompoundKeyWhenWeExitGroup(this.reader.groupKey(parent2), this.reader.groupObjectKey(parent2), this.reader.groupAux(parent2));
        }
        final int groupNodeCount = this.groupNodeCount;
        final Pending pending = this.pending;
        if (pending != null && pending.getKeyInfos().size() > 0) {
            final List<KeyInfo> keyInfos = pending.getKeyInfos();
            List<KeyInfo> used = pending.getUsed();
            final Set<Object> fastToSet = ListUtilsKt.fastToSet((List<?>)used);
            final Set set = new LinkedHashSet();
            final int size = used.size();
            final int size2 = keyInfos.size();
            int i = 0;
            int n = 0;
            int n2 = 0;
            while (i < size2) {
                final KeyInfo keyInfo = keyInfos.get(i);
                List list = null;
                int n3 = 0;
                int n4 = 0;
                int n5 = 0;
                Label_0315: {
                    if (!fastToSet.contains(keyInfo)) {
                        this.recordRemoveNode(pending.nodePositionOf(keyInfo) + pending.getStartIndex(), keyInfo.getNodes());
                        pending.updateNodeCount(keyInfo.getLocation(), 0);
                        this.recordReaderMoving(keyInfo.getLocation());
                        this.reader.reposition(keyInfo.getLocation());
                        this.recordDelete();
                        this.reader.skipGroup();
                        ComposerKt.access$removeRange(this.invalidations, keyInfo.getLocation(), keyInfo.getLocation() + this.reader.groupSize(keyInfo.getLocation()));
                    }
                    else if (!set.contains(keyInfo)) {
                        list = used;
                        n3 = i;
                        n4 = n;
                        n5 = n2;
                        if (n < size) {
                            final KeyInfo keyInfo2 = used.get(n);
                            if (keyInfo2 != keyInfo) {
                                final int nodePosition = pending.nodePositionOf(keyInfo2);
                                set.add(keyInfo2);
                                if (nodePosition != n2) {
                                    final int updatedNodeCount = pending.updatedNodeCountOf(keyInfo2);
                                    this.recordMoveNode(pending.getStartIndex() + nodePosition, n2 + pending.getStartIndex(), updatedNodeCount);
                                    pending.registerMoveNode(nodePosition, n2, updatedNodeCount);
                                }
                            }
                            else {
                                ++i;
                            }
                            n4 = n + 1;
                            n5 = n2 + pending.updatedNodeCountOf(keyInfo2);
                            list = used;
                            n3 = i;
                        }
                        break Label_0315;
                    }
                    n3 = i + 1;
                    n5 = n2;
                    n4 = n;
                    list = used;
                }
                used = list;
                i = n3;
                n = n4;
                n2 = n5;
            }
            this.realizeMovement();
            if (keyInfos.size() > 0) {
                this.recordReaderMoving(this.reader.getGroupEnd());
                this.reader.skipToGroupEnd();
            }
        }
        final int nodeIndex = this.nodeIndex;
        while (!this.reader.isGroupEnd()) {
            final int currentGroup = this.reader.getCurrentGroup();
            this.recordDelete();
            this.recordRemoveNode(nodeIndex, this.reader.skipGroup());
            ComposerKt.access$removeRange(this.invalidations, currentGroup, this.reader.getCurrentGroup());
        }
        final boolean inserting = this.getInserting();
        int n7;
        if (inserting) {
            int n6 = groupNodeCount;
            if (b) {
                this.registerInsertUpFixup();
                n6 = 1;
            }
            this.reader.endEmpty();
            final int parent3 = this.writer.getParent();
            this.writer.endGroup();
            n7 = n6;
            if (!this.reader.getInEmpty()) {
                final int insertedGroupVirtualIndex = this.insertedGroupVirtualIndex(parent3);
                this.writer.endInsert();
                this.writer.close();
                this.recordInsert(this.insertAnchor);
                this.inserting = false;
                n7 = n6;
                if (!this.slotTable.isEmpty()) {
                    this.updateNodeCount(insertedGroupVirtualIndex, 0);
                    this.updateNodeCountOverrides(insertedGroupVirtualIndex, n6);
                    n7 = n6;
                }
            }
        }
        else {
            if (b) {
                this.recordUp();
            }
            this.recordEndGroup();
            final int parent4 = this.reader.getParent();
            if (groupNodeCount != this.updatedNodeCount(parent4)) {
                this.updateNodeCountOverrides(parent4, groupNodeCount);
            }
            int n8 = groupNodeCount;
            if (b) {
                n8 = 1;
            }
            this.reader.endGroup();
            this.realizeMovement();
            n7 = n8;
        }
        this.exitGroup(n7, inserting);
    }
    
    private final void endGroup() {
        this.end(false);
    }
    
    private final void endRoot() {
        this.endGroup();
        this.parentContext.doneComposing$runtime_release();
        this.endGroup();
        this.recordEndRoot();
        this.finalizeCompose();
        this.reader.close();
        this.forciblyRecompose = false;
    }
    
    private final void ensureWriter() {
        if (this.writer.getClosed()) {
            (this.writer = this.insertTable.openWriter()).skipToGroupEnd();
            this.writerHasAProvider = false;
            this.providerCache = null;
        }
    }
    
    private final void enterGroup(final boolean b, final Pending pending) {
        this.pendingStack.push(this.pending);
        this.pending = pending;
        this.nodeIndexStack.push(this.nodeIndex);
        if (b) {
            this.nodeIndex = 0;
        }
        this.groupNodeCountStack.push(this.groupNodeCount);
        this.groupNodeCount = 0;
    }
    
    private final void exitGroup(final int n, final boolean b) {
        final Pending pending = this.pendingStack.pop();
        if (pending != null && !b) {
            pending.setGroupIndex(pending.getGroupIndex() + 1);
        }
        this.pending = pending;
        this.nodeIndex = this.nodeIndexStack.pop() + n;
        this.groupNodeCount = this.groupNodeCountStack.pop() + n;
    }
    
    private final void finalizeCompose() {
        this.realizeUps();
        if (!this.pendingStack.isEmpty()) {
            ComposerKt.composeRuntimeError("Start/end imbalance".toString());
            throw new KotlinNothingValueException();
        }
        if (this.startedGroups.isEmpty()) {
            this.cleanUpCompose();
            return;
        }
        ComposerKt.composeRuntimeError("Missed recording an endGroup()".toString());
        throw new KotlinNothingValueException();
    }
    
    private final Object getNode(final SlotReader slotReader) {
        return slotReader.node(slotReader.getParent());
    }
    
    private final int groupCompoundKeyPart(final SlotReader slotReader, int n) {
        if (slotReader.hasObjectKey(n)) {
            final Object groupObjectKey = slotReader.groupObjectKey(n);
            if (groupObjectKey != null) {
                if (groupObjectKey instanceof Enum) {
                    n = ((Enum)groupObjectKey).ordinal();
                }
                else if (groupObjectKey instanceof MovableContent) {
                    n = 126665345;
                }
                else {
                    n = groupObjectKey.hashCode();
                }
            }
            else {
                n = 0;
            }
        }
        else {
            final int groupKey = slotReader.groupKey(n);
            int hashCode;
            if ((hashCode = groupKey) == 207) {
                final Object groupAux = slotReader.groupAux(n);
                hashCode = groupKey;
                if (groupAux != null) {
                    if (Intrinsics.areEqual(groupAux, Composer.Companion.getEmpty())) {
                        hashCode = groupKey;
                    }
                    else {
                        hashCode = groupAux.hashCode();
                    }
                }
            }
            n = hashCode;
        }
        return n;
    }
    
    private final void insertMovableContentGuarded(final List<Pair<MovableContentStateReference, MovableContentStateReference>> list) {
        final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> lateChanges = this.lateChanges;
        final List access$getChanges$p = access$getChanges$p(this);
        try {
            access$setChanges$p(this, lateChanges);
            this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerKt.access$getResetSlotsInstance$p());
            final int size = list.size();
            int i = 0;
            while (i < size) {
                final Pair pair = list.get(i);
                final MovableContentStateReference movableContentStateReference = (MovableContentStateReference)pair.component1();
                final MovableContentStateReference movableContentStateReference2 = (MovableContentStateReference)pair.component2();
                Object o = movableContentStateReference.getAnchor$runtime_release();
                final int anchorIndex = movableContentStateReference.getSlotTable$runtime_release().anchorIndex((Anchor)o);
                final Ref$IntRef ref$IntRef = new Ref$IntRef();
                this.realizeUps();
                this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$insertMovableContentGuarded$1$1.ComposerImpl$insertMovableContentGuarded$1$1$1(ref$IntRef, (Anchor)o));
                Label_0706: {
                    if (movableContentStateReference2 == null) {
                        if (Intrinsics.areEqual((Object)movableContentStateReference.getSlotTable$runtime_release(), (Object)this.insertTable)) {
                            this.createFreshInsertTable();
                        }
                        final Object o2 = movableContentStateReference.getSlotTable$runtime_release().openReader();
                        try {
                            ((SlotReader)o2).reposition(anchorIndex);
                            this.writersReaderDelta = anchorIndex;
                            final List list2 = new ArrayList();
                            Object openReader = new ComposerImpl$insertMovableContentGuarded$1$1$2.ComposerImpl$insertMovableContentGuarded$1$1$2$1(this, list2, (SlotReader)o2, movableContentStateReference);
                            recomposeMovableContent$default(this, null, null, null, null, (Function0)openReader, 15, null);
                            if (list2.isEmpty() ^ true) {
                                openReader = new ComposerImpl$insertMovableContentGuarded$1$1$2.ComposerImpl$insertMovableContentGuarded$1$1$2$2(ref$IntRef, list2);
                                this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)openReader);
                            }
                            final Unit instance = Unit.INSTANCE;
                            break Label_0706;
                        }
                        finally {
                            ((SlotReader)o2).close();
                        }
                    }
                    Object o3 = this.parentContext.movableContentStateResolve$runtime_release(movableContentStateReference2);
                    SlotTable slotTable;
                    if (o3 == null || (slotTable = ((MovableContentState)o3).getSlotTable$runtime_release()) == null) {
                        slotTable = movableContentStateReference2.getSlotTable$runtime_release();
                    }
                    Anchor anchor = null;
                    Label_0364: {
                        if (o3 != null) {
                            final SlotTable slotTable$runtime_release = ((MovableContentState)o3).getSlotTable$runtime_release();
                            if (slotTable$runtime_release != null && (anchor = slotTable$runtime_release.anchor(0)) != null) {
                                break Label_0364;
                            }
                        }
                        anchor = movableContentStateReference2.getAnchor$runtime_release();
                    }
                    final List access$collectNodes = ComposerKt.access$collectNodesFrom(slotTable, anchor);
                    if (access$collectNodes.isEmpty() ^ true) {
                        this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$insertMovableContentGuarded$1$1.ComposerImpl$insertMovableContentGuarded$1$1$3(ref$IntRef, access$collectNodes));
                        if (Intrinsics.areEqual((Object)movableContentStateReference.getSlotTable$runtime_release(), (Object)this.slotTable)) {
                            final int anchorIndex2 = this.slotTable.anchorIndex((Anchor)o);
                            this.updateNodeCount(anchorIndex2, this.updatedNodeCount(anchorIndex2) + access$collectNodes.size());
                        }
                    }
                    this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$insertMovableContentGuarded$1$1.ComposerImpl$insertMovableContentGuarded$1$1$4((MovableContentState)o3, this, movableContentStateReference2, movableContentStateReference));
                    Object openReader = slotTable.openReader();
                    try {
                        o = access$getReader$p(this);
                        o3 = access$getNodeCountOverrides$p(this);
                        access$setNodeCountOverrides$p(this, null);
                        try {
                            access$setReader$p(this, (SlotReader)openReader);
                            final int anchorIndex3 = slotTable.anchorIndex(anchor);
                            ((SlotReader)openReader).reposition(anchorIndex3);
                            this.writersReaderDelta = anchorIndex3;
                            final List list3 = new ArrayList();
                            Object o2 = access$getChanges$p(this);
                            try {
                                access$setChanges$p(this, list3);
                                final ControlledComposition composition$runtime_release = movableContentStateReference2.getComposition$runtime_release();
                                final ControlledComposition composition$runtime_release2 = movableContentStateReference.getComposition$runtime_release();
                                final int currentGroup = ((SlotReader)openReader).getCurrentGroup();
                                final List<Pair<RecomposeScopeImpl, IdentityArraySet<Object>>> invalidations$runtime_release = movableContentStateReference2.getInvalidations$runtime_release();
                                final Function0 function0 = (Function0)new ComposerImpl$insertMovableContentGuarded$1$1$5$1$1.ComposerImpl$insertMovableContentGuarded$1$1$5$1$1$1(this, movableContentStateReference);
                                try {
                                    this.recomposeMovableContent(composition$runtime_release, composition$runtime_release2, Integer.valueOf(currentGroup), invalidations$runtime_release, (kotlin.jvm.functions.Function0<?>)function0);
                                    final Unit instance2 = Unit.INSTANCE;
                                    access$setChanges$p(this, (List)o2);
                                    if (list3.isEmpty() ^ true) {
                                        o2 = new ComposerImpl$insertMovableContentGuarded$1$1$5$1.ComposerImpl$insertMovableContentGuarded$1$1$5$1$2(ref$IntRef, list3);
                                        this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)o2);
                                    }
                                    o2 = Unit.INSTANCE;
                                    access$setReader$p(this, (SlotReader)o);
                                    access$setNodeCountOverrides$p(this, (int[])o3);
                                    o2 = Unit.INSTANCE;
                                    ((SlotReader)openReader).close();
                                    this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerKt.access$getSkipToGroupEndInstance$p());
                                    ++i;
                                }
                                finally {}
                            }
                            finally {}
                            access$setChanges$p(this, (List)o2);
                        }
                        finally {
                            access$setReader$p(this, (SlotReader)o);
                            access$setNodeCountOverrides$p(this, (int[])o3);
                        }
                    }
                    finally {
                        ((SlotReader)openReader).close();
                    }
                }
                break;
            }
            this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerImpl$insertMovableContentGuarded$1.ComposerImpl$insertMovableContentGuarded$1$2.INSTANCE);
            this.writersReaderDelta = 0;
            final Unit instance3 = Unit.INSTANCE;
        }
        finally {
            access$setChanges$p(this, access$getChanges$p);
        }
    }
    
    private static final int insertMovableContentGuarded$currentNodeIndex(final SlotWriter slotWriter) {
        final int currentGroup = slotWriter.getCurrentGroup();
        int i;
        for (i = slotWriter.getParent(); i >= 0 && !slotWriter.isNode(i); i = slotWriter.parent(i)) {}
        ++i;
        int n = 0;
        while (i < currentGroup) {
            if (slotWriter.indexInGroup(currentGroup, i)) {
                if (slotWriter.isNode(i)) {
                    n = 0;
                }
                ++i;
            }
            else {
                int nodeCount;
                if (slotWriter.isNode(i)) {
                    nodeCount = 1;
                }
                else {
                    nodeCount = slotWriter.nodeCount(i);
                }
                n += nodeCount;
                i += slotWriter.groupSize(i);
            }
        }
        return n;
    }
    
    private static final int insertMovableContentGuarded$positionToInsert(final SlotWriter slotWriter, final Anchor anchor, final Applier<Object> applier) {
        final int anchorIndex = slotWriter.anchorIndex(anchor);
        final int currentGroup = slotWriter.getCurrentGroup();
        final boolean b = true;
        ComposerKt.runtimeCheck(currentGroup < anchorIndex);
        insertMovableContentGuarded$positionToParentOf(slotWriter, applier, anchorIndex);
        int insertMovableContentGuarded$currentNodeIndex = insertMovableContentGuarded$currentNodeIndex(slotWriter);
        while (slotWriter.getCurrentGroup() < anchorIndex) {
            if (slotWriter.indexInCurrentGroup(anchorIndex)) {
                if (slotWriter.isNode()) {
                    applier.down(slotWriter.node(slotWriter.getCurrentGroup()));
                    insertMovableContentGuarded$currentNodeIndex = 0;
                }
                slotWriter.startGroup();
            }
            else {
                insertMovableContentGuarded$currentNodeIndex += slotWriter.skipGroup();
            }
        }
        ComposerKt.runtimeCheck(slotWriter.getCurrentGroup() == anchorIndex && b);
        return insertMovableContentGuarded$currentNodeIndex;
    }
    
    private static final void insertMovableContentGuarded$positionToParentOf(final SlotWriter slotWriter, final Applier<Object> applier, final int n) {
        while (!slotWriter.indexInParent(n)) {
            slotWriter.skipToGroupEnd();
            if (slotWriter.isNode(slotWriter.getParent())) {
                applier.up();
            }
            slotWriter.endGroup();
        }
    }
    
    private final int insertedGroupVirtualIndex(final int n) {
        return -2 - n;
    }
    
    private final void invokeMovableContentLambda(final MovableContent<Object> movableContent, final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final Object o, final boolean b) {
        this.startMovableGroup(126665345, movableContent);
        this.changed(o);
        final int compoundKeyHash = this.getCompoundKeyHash();
        try {
            this.compoundKeyHash = 126665345;
            final boolean inserting = this.getInserting();
            boolean providersInvalid = false;
            if (inserting) {
                SlotWriter.markGroup$default(this.writer, 0, 1, null);
            }
            if (!this.getInserting()) {
                if (!Intrinsics.areEqual(this.reader.getGroupAux(), (Object)persistentMap)) {
                    providersInvalid = true;
                }
            }
            if (providersInvalid) {
                this.providerUpdates.set(this.reader.getCurrentGroup(), (PersistentMap<CompositionLocal<Object>, State<Object>>)persistentMap);
            }
            this.start-BaiHCIY(202, ComposerKt.getCompositionLocalMap(), GroupKind.Companion.getGroup-ULZAiWs(), persistentMap);
            if (this.getInserting() && !b) {
                this.writerHasAProvider = true;
                this.providerCache = null;
                final SlotWriter writer = this.writer;
                this.parentContext.insertMovableContent$runtime_release(new MovableContentStateReference(movableContent, o, this.getComposition(), this.insertTable, writer.anchor(writer.parent(writer.getParent())), CollectionsKt.emptyList(), this.currentCompositionLocalScope()));
            }
            else {
                final boolean providersInvalid2 = this.providersInvalid;
                this.providersInvalid = providersInvalid;
                ActualJvm_jvmKt.invokeComposable(this, (Function2<? super Composer, ? super Integer, Unit>)ComposableLambdaKt.composableLambdaInstance(694380496, true, new ComposerImpl$invokeMovableContentLambda.ComposerImpl$invokeMovableContentLambda$1((MovableContent)movableContent, o)));
                this.providersInvalid = providersInvalid2;
            }
        }
        finally {
            this.endGroup();
            this.compoundKeyHash = compoundKeyHash;
            this.endMovableGroup();
        }
    }
    
    private final Object nodeAt(final SlotReader slotReader, final int n) {
        return slotReader.node(n);
    }
    
    private final int nodeIndexOf(final int n, int n2, int n3, int n4) {
        int n5;
        for (n5 = this.reader.parent(n2); n5 != n3 && !this.reader.isNode(n5); n5 = this.reader.parent(n5)) {}
        if (this.reader.isNode(n5)) {
            n4 = 0;
        }
        if (n5 == n2) {
            return n4;
        }
        final int updatedNodeCount = this.updatedNodeCount(n5);
        final int nodeCount = this.reader.nodeCount(n2);
        int n6 = n4;
        int n7 = 0;
    Label_0166:
        while (true) {
            n2 = n6;
            if ((n7 = n2) >= updatedNodeCount - nodeCount + n4) {
                break;
            }
            n7 = n2;
            if (n5 == n) {
                break;
            }
            n3 = n5 + 1;
            while (true) {
                n7 = n2;
                if (n3 >= n) {
                    break Label_0166;
                }
                final int n8 = this.reader.groupSize(n3) + n3;
                n5 = n3;
                n6 = n2;
                if (n < n8) {
                    break;
                }
                n2 += this.updatedNodeCount(n3);
                n3 = n8;
            }
        }
        return n7;
    }
    
    private final void realizeDowns() {
        if (this.downNodes.isNotEmpty()) {
            this.realizeDowns(this.downNodes.toArray());
            this.downNodes.clear();
        }
    }
    
    private final void realizeDowns(final Object[] array) {
        this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$realizeDowns.ComposerImpl$realizeDowns$1(array));
    }
    
    private final void realizeMovement() {
        final int previousCount = this.previousCount;
        this.previousCount = 0;
        if (previousCount > 0) {
            final int previousRemove = this.previousRemove;
            if (previousRemove >= 0) {
                this.previousRemove = -1;
                this.recordApplierOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$realizeMovement.ComposerImpl$realizeMovement$1(previousRemove, previousCount));
            }
            else {
                final int previousMoveFrom = this.previousMoveFrom;
                this.previousMoveFrom = -1;
                final int previousMoveTo = this.previousMoveTo;
                this.previousMoveTo = -1;
                this.recordApplierOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$realizeMovement.ComposerImpl$realizeMovement$2(previousMoveFrom, previousMoveTo, previousCount));
            }
        }
    }
    
    private final void realizeOperationLocation(final boolean b) {
        int writersReaderDelta;
        if (b) {
            writersReaderDelta = this.reader.getParent();
        }
        else {
            writersReaderDelta = this.reader.getCurrentGroup();
        }
        final int n = writersReaderDelta - this.writersReaderDelta;
        if (n >= 0) {
            if (n > 0) {
                this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$realizeOperationLocation.ComposerImpl$realizeOperationLocation$2(n));
                this.writersReaderDelta = writersReaderDelta;
            }
            return;
        }
        ComposerKt.composeRuntimeError("Tried to seek backward".toString());
        throw new KotlinNothingValueException();
    }
    
    static /* synthetic */ void realizeOperationLocation$default(final ComposerImpl composerImpl, boolean b, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            b = false;
        }
        composerImpl.realizeOperationLocation(b);
    }
    
    private final void realizeUps() {
        final int pendingUps = this.pendingUps;
        if (pendingUps > 0) {
            this.pendingUps = 0;
            this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$realizeUps.ComposerImpl$realizeUps$1(pendingUps));
        }
    }
    
    private final <R> R recomposeMovableContent(final ControlledComposition controlledComposition, final ControlledComposition controlledComposition2, final Integer n, final List<Pair<RecomposeScopeImpl, IdentityArraySet<Object>>> list, final Function0<? extends R> function0) {
        final boolean implicitRootStart = this.implicitRootStart;
        final boolean isComposing = this.isComposing;
        final int nodeIndex = this.nodeIndex;
        try {
            this.implicitRootStart = false;
            this.isComposing = true;
            this.nodeIndex = 0;
            for (int size = list.size(), i = 0; i < size; ++i) {
                final Pair pair = list.get(i);
                final RecomposeScopeImpl recomposeScopeImpl = (RecomposeScopeImpl)pair.component1();
                final IdentityArraySet set = (IdentityArraySet)pair.component2();
                if (set != null) {
                    for (int size2 = set.size(), j = 0; j < size2; ++j) {
                        this.tryImminentInvalidation$runtime_release(recomposeScopeImpl, set.get(j));
                    }
                }
                else {
                    this.tryImminentInvalidation$runtime_release(recomposeScopeImpl, null);
                }
            }
            if (controlledComposition != null) {
                int intValue;
                if (n != null) {
                    intValue = n;
                }
                else {
                    intValue = -1;
                }
                final Object o;
                if ((o = controlledComposition.delegateInvalidations(controlledComposition2, intValue, (kotlin.jvm.functions.Function0<?>)function0)) != null) {
                    return (R)o;
                }
            }
            Object o = function0.invoke();
            return (R)o;
        }
        finally {
            this.implicitRootStart = implicitRootStart;
            this.isComposing = isComposing;
            this.nodeIndex = nodeIndex;
        }
    }
    
    static /* synthetic */ Object recomposeMovableContent$default(final ComposerImpl composerImpl, ControlledComposition controlledComposition, ControlledComposition controlledComposition2, Integer n, List emptyList, final Function0 function0, final int n2, final Object o) {
        if ((n2 & 0x1) != 0x0) {
            controlledComposition = null;
        }
        if ((n2 & 0x2) != 0x0) {
            controlledComposition2 = null;
        }
        if ((n2 & 0x4) != 0x0) {
            n = null;
        }
        if ((n2 & 0x8) != 0x0) {
            emptyList = CollectionsKt.emptyList();
        }
        return composerImpl.recomposeMovableContent(controlledComposition, controlledComposition2, n, (List<Pair<RecomposeScopeImpl, IdentityArraySet<Object>>>)emptyList, (kotlin.jvm.functions.Function0<?>)function0);
    }
    
    private final void recomposeToGroupEnd() {
        final boolean isComposing = this.isComposing;
        this.isComposing = true;
        final int parent = this.reader.getParent();
        final int n = this.reader.groupSize(parent) + parent;
        final int nodeIndex = this.nodeIndex;
        final int compoundKeyHash = this.getCompoundKeyHash();
        final int groupNodeCount = this.groupNodeCount;
        Invalidation invalidation = ComposerKt.access$firstInRange(this.invalidations, this.reader.getCurrentGroup(), n);
        boolean b = false;
        int n2 = parent;
        while (invalidation != null) {
            final int location = invalidation.getLocation();
            ComposerKt.access$removeLocation(this.invalidations, location);
            if (invalidation.isInvalid()) {
                this.reader.reposition(location);
                final int currentGroup = this.reader.getCurrentGroup();
                this.recordUpsAndDowns(n2, currentGroup, parent);
                this.nodeIndex = this.nodeIndexOf(location, currentGroup, parent, nodeIndex);
                this.compoundKeyHash = this.compoundKeyOf(this.reader.parent(currentGroup), parent, compoundKeyHash);
                this.providerCache = null;
                invalidation.getScope().compose(this);
                this.providerCache = null;
                this.reader.restoreParent(parent);
                n2 = currentGroup;
                b = true;
            }
            else {
                this.invalidateStack.push(invalidation.getScope());
                invalidation.getScope().rereadTrackedInstances();
                this.invalidateStack.pop();
            }
            invalidation = ComposerKt.access$firstInRange(this.invalidations, this.reader.getCurrentGroup(), n);
        }
        if (b) {
            this.recordUpsAndDowns(n2, parent, parent);
            this.reader.skipToGroupEnd();
            final int updatedNodeCount = this.updatedNodeCount(parent);
            this.nodeIndex = nodeIndex + updatedNodeCount;
            this.groupNodeCount = groupNodeCount + updatedNodeCount;
        }
        else {
            this.skipReaderToGroupEnd();
        }
        this.compoundKeyHash = compoundKeyHash;
        this.isComposing = isComposing;
    }
    
    private final void record(final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        this.changes.add((Function3<Applier<?>, SlotWriter, RememberManager, Unit>)function3);
    }
    
    private final void recordApplierOperation(final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        this.realizeUps();
        this.realizeDowns();
        this.record(function3);
    }
    
    private final void recordDelete() {
        this.reportFreeMovableContent(this.reader.getCurrentGroup());
        this.recordSlotEditingOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerKt.access$getRemoveCurrentGroupInstance$p());
        this.writersReaderDelta += this.reader.getGroupSize();
    }
    
    private final void recordDown(final Object o) {
        this.downNodes.push(o);
    }
    
    private final void recordEndGroup() {
        final int parent = this.reader.getParent();
        if (this.startedGroups.peekOr(-1) <= parent) {
            if (this.startedGroups.peekOr(-1) == parent) {
                this.startedGroups.pop();
                recordSlotTableOperation$default(this, false, ComposerKt.access$getEndGroupInstance$p(), 1, null);
            }
            return;
        }
        ComposerKt.composeRuntimeError("Missed recording an endGroup".toString());
        throw new KotlinNothingValueException();
    }
    
    private final void recordEndRoot() {
        if (this.startedGroup) {
            recordSlotTableOperation$default(this, false, ComposerKt.access$getEndGroupInstance$p(), 1, null);
            this.startedGroup = false;
        }
    }
    
    private final void recordFixup(final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        this.insertFixups.add((Function3<Applier<?>, SlotWriter, RememberManager, Unit>)function3);
    }
    
    private final void recordInsert(final Anchor anchor) {
        if (this.insertFixups.isEmpty()) {
            this.recordSlotEditingOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$recordInsert.ComposerImpl$recordInsert$1(this.insertTable, anchor));
        }
        else {
            final List mutableList = CollectionsKt.toMutableList((Collection)this.insertFixups);
            this.insertFixups.clear();
            this.realizeUps();
            this.realizeDowns();
            this.recordSlotEditingOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$recordInsert.ComposerImpl$recordInsert$2(this.insertTable, anchor, mutableList));
        }
    }
    
    private final void recordInsertUpFixup(final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        this.insertUpFixups.push((Function3<Applier<?>, SlotWriter, RememberManager, Unit>)function3);
    }
    
    private final void recordMoveNode(final int previousMoveFrom, final int previousMoveTo, final int previousCount) {
        if (previousCount > 0) {
            final int previousCount2 = this.previousCount;
            if (previousCount2 > 0 && this.previousMoveFrom == previousMoveFrom - previousCount2 && this.previousMoveTo == previousMoveTo - previousCount2) {
                this.previousCount = previousCount2 + previousCount;
            }
            else {
                this.realizeMovement();
                this.previousMoveFrom = previousMoveFrom;
                this.previousMoveTo = previousMoveTo;
                this.previousCount = previousCount;
            }
        }
    }
    
    private final void recordReaderMoving(final int n) {
        this.writersReaderDelta = n - (this.reader.getCurrentGroup() - this.writersReaderDelta);
    }
    
    private final void recordRemoveNode(final int n, final int previousCount) {
        if (previousCount > 0) {
            if (n < 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid remove index ");
                sb.append(n);
                ComposerKt.composeRuntimeError(sb.toString().toString());
                throw new KotlinNothingValueException();
            }
            if (this.previousRemove == n) {
                this.previousCount += previousCount;
            }
            else {
                this.realizeMovement();
                this.previousRemove = n;
                this.previousCount = previousCount;
            }
        }
    }
    
    private final void recordSlotEditing() {
        if (this.reader.getSize() > 0) {
            final SlotReader reader = this.reader;
            final int parent = reader.getParent();
            if (this.startedGroups.peekOr(-2) != parent) {
                if (!this.startedGroup && this.implicitRootStart) {
                    recordSlotTableOperation$default(this, false, ComposerKt.access$getStartRootGroup$p(), 1, null);
                    this.startedGroup = true;
                }
                if (parent > 0) {
                    final Anchor anchor = reader.anchor(parent);
                    this.startedGroups.push(parent);
                    recordSlotTableOperation$default(this, false, (Function3)new ComposerImpl$recordSlotEditing.ComposerImpl$recordSlotEditing$1(anchor), 1, null);
                }
            }
        }
    }
    
    private final void recordSlotEditingOperation(final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        realizeOperationLocation$default(this, false, 1, null);
        this.recordSlotEditing();
        this.record(function3);
    }
    
    private final void recordSlotTableOperation(final boolean b, final Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit> function3) {
        this.realizeOperationLocation(b);
        this.record(function3);
    }
    
    static /* synthetic */ void recordSlotTableOperation$default(final ComposerImpl composerImpl, boolean b, final Function3 function3, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            b = false;
        }
        composerImpl.recordSlotTableOperation(b, (Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)function3);
    }
    
    private final void recordUp() {
        if (this.downNodes.isNotEmpty()) {
            this.downNodes.pop();
        }
        else {
            ++this.pendingUps;
        }
    }
    
    private final void recordUpsAndDowns(int parent, final int n, int access$nearestCommonRoot) {
        SlotReader reader;
        for (reader = this.reader, access$nearestCommonRoot = ComposerKt.access$nearestCommonRootOf(reader, parent, n, access$nearestCommonRoot); parent > 0 && parent != access$nearestCommonRoot; parent = reader.parent(parent)) {
            if (reader.isNode(parent)) {
                this.recordUp();
            }
        }
        this.doRecordDownsFor(n, access$nearestCommonRoot);
    }
    
    private final void registerInsertUpFixup() {
        this.insertFixups.add(this.insertUpFixups.pop());
    }
    
    private final void releaseMovableGroupAtCurrent(final MovableContentStateReference movableContentStateReference, final SlotWriter slotWriter) {
        final SlotTable slotTable = new SlotTable();
        final SlotWriter openWriter = slotTable.openWriter();
        try {
            openWriter.beginInsert();
            openWriter.startGroup(126665345, movableContentStateReference.getContent$runtime_release());
            SlotWriter.markGroup$default(openWriter, 0, 1, null);
            openWriter.update(movableContentStateReference.getParameter$runtime_release());
            slotWriter.moveTo(movableContentStateReference.getAnchor$runtime_release(), 1, openWriter);
            openWriter.skipGroup();
            openWriter.endGroup();
            openWriter.endInsert();
            final Unit instance = Unit.INSTANCE;
            openWriter.close();
            this.parentContext.movableContentStateReleased$runtime_release(movableContentStateReference, new MovableContentState(slotTable));
        }
        finally {
            openWriter.close();
        }
    }
    
    private final void reportAllMovableContent() {
        if (this.slotTable.containsMark()) {
            final List deferredChanges = new ArrayList();
            this.deferredChanges = deferredChanges;
            final SlotReader openReader = this.slotTable.openReader();
            try {
                this.reader = openReader;
                Object o = access$getChanges$p(this);
                try {
                    access$setChanges$p(this, deferredChanges);
                    this.reportFreeMovableContent(0);
                    this.realizeUps();
                    if (this.startedGroup) {
                        this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerKt.access$getSkipToGroupEndInstance$p());
                        this.recordEndRoot();
                    }
                    final Unit instance = Unit.INSTANCE;
                    access$setChanges$p(this, (List)o);
                    o = Unit.INSTANCE;
                }
                finally {
                    access$setChanges$p(this, (List)o);
                }
            }
            finally {
                openReader.close();
            }
        }
    }
    
    private final void reportFreeMovableContent(final int n) {
        reportFreeMovableContent$reportGroup(this, n, false, 0);
        this.realizeMovement();
    }
    
    private static final int reportFreeMovableContent$reportGroup(final ComposerImpl composerImpl, int n, final boolean b, final int n2) {
        final boolean hasMark = composerImpl.reader.hasMark(n);
        final int n3 = 1;
        final int n4 = 0;
        if (hasMark) {
            final int groupKey = composerImpl.reader.groupKey(n);
            final Object groupObjectKey = composerImpl.reader.groupObjectKey(n);
            if (groupKey == 126665345 && groupObjectKey instanceof MovableContent) {
                final MovableContent movableContent = (MovableContent)groupObjectKey;
                final Object groupGet = composerImpl.reader.groupGet(n, 0);
                final Anchor anchor = composerImpl.reader.anchor(n);
                final List access$filterToRange = ComposerKt.access$filterToRange(composerImpl.invalidations, n, composerImpl.reader.groupSize(n) + n);
                final ArrayList list = new ArrayList(access$filterToRange.size());
                for (int size = access$filterToRange.size(), i = 0; i < size; ++i) {
                    final Object value = access$filterToRange.get(i);
                    final Collection collection = list;
                    final Invalidation invalidation = (Invalidation)value;
                    collection.add(TuplesKt.to((Object)invalidation.getScope(), (Object)invalidation.getInstances()));
                }
                final MovableContentStateReference movableContentStateReference = new MovableContentStateReference(movableContent, groupGet, composerImpl.getComposition(), composerImpl.slotTable, anchor, (List<Pair<RecomposeScopeImpl, IdentityArraySet<Object>>>)list, composerImpl.currentCompositionLocalScope(n));
                composerImpl.parentContext.deletedMovableContent$runtime_release(movableContentStateReference);
                composerImpl.recordSlotEditing();
                composerImpl.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$reportFreeMovableContent$reportGroup.ComposerImpl$reportFreeMovableContent$reportGroup$1(composerImpl, movableContentStateReference));
                if (b) {
                    composerImpl.realizeMovement();
                    composerImpl.realizeUps();
                    composerImpl.realizeDowns();
                    int nodeCount;
                    if (composerImpl.reader.isNode(n)) {
                        nodeCount = n3;
                    }
                    else {
                        nodeCount = composerImpl.reader.nodeCount(n);
                    }
                    n = n4;
                    if (nodeCount > 0) {
                        composerImpl.recordRemoveNode(n2, nodeCount);
                        n = n4;
                    }
                }
                else {
                    n = composerImpl.reader.nodeCount(n);
                }
            }
            else if (groupKey == 206 && Intrinsics.areEqual(groupObjectKey, ComposerKt.getReference())) {
                final Object groupGet2 = composerImpl.reader.groupGet(n, 0);
                CompositionContextHolder compositionContextHolder;
                if (groupGet2 instanceof CompositionContextHolder) {
                    compositionContextHolder = (CompositionContextHolder)groupGet2;
                }
                else {
                    compositionContextHolder = null;
                }
                if (compositionContextHolder != null) {
                    final Iterator iterator = compositionContextHolder.getRef().getComposers().iterator();
                    while (iterator.hasNext()) {
                        ((ComposerImpl)iterator.next()).reportAllMovableContent();
                    }
                }
                n = composerImpl.reader.nodeCount(n);
            }
            else {
                n = composerImpl.reader.nodeCount(n);
            }
        }
        else if (composerImpl.reader.containsMark(n)) {
            final int groupSize = composerImpl.reader.groupSize(n);
            int j = n + 1;
            int n5 = 0;
            while (j < groupSize + n) {
                final boolean node = composerImpl.reader.isNode(j);
                if (node) {
                    composerImpl.realizeMovement();
                    composerImpl.recordDown(composerImpl.reader.node(j));
                }
                final boolean b2 = node || b;
                int n6;
                if (node) {
                    n6 = 0;
                }
                else {
                    n6 = n2 + n5;
                }
                n5 += reportFreeMovableContent$reportGroup(composerImpl, j, b2, n6);
                if (node) {
                    composerImpl.realizeMovement();
                    composerImpl.recordUp();
                }
                j += composerImpl.reader.groupSize(j);
            }
            n = n5;
        }
        else {
            n = composerImpl.reader.nodeCount(n);
        }
        return n;
    }
    
    private final <T> T resolveCompositionLocal(final CompositionLocal<T> compositionLocal, final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap) {
        T t;
        if (ComposerKt.contains(persistentMap, compositionLocal)) {
            t = ComposerKt.getValueOf(persistentMap, compositionLocal);
        }
        else {
            t = compositionLocal.getDefaultValueHolder$runtime_release().getValue();
        }
        return t;
    }
    
    private final void skipGroup() {
        this.groupNodeCount += this.reader.skipGroup();
    }
    
    private final void skipReaderToGroupEnd() {
        this.groupNodeCount = this.reader.getParentNodes();
        this.reader.skipToGroupEnd();
    }
    
    private final void start-BaiHCIY(int n, Object o, int n2, Object o2) {
        final Object o3 = o;
        this.validateNodeNotExpected();
        this.updateCompoundKeyWhenWeEnterGroup(n, o, o2);
        final int group-ULZAiWs = GroupKind.Companion.getGroup-ULZAiWs();
        final int n3 = 0;
        final boolean b = n2 != group-ULZAiWs;
        final boolean inserting = this.getInserting();
        final Pending pending = null;
        if (inserting) {
            this.reader.beginEmpty();
            n2 = this.writer.getCurrentGroup();
            if (b) {
                this.writer.startNode(n, Composer.Companion.getEmpty());
            }
            else if (o2 != null) {
                final SlotWriter writer = this.writer;
                if ((o = o3) == null) {
                    o = Composer.Companion.getEmpty();
                }
                writer.startData(n, o, o2);
            }
            else {
                final SlotWriter writer2 = this.writer;
                if ((o = o3) == null) {
                    o = Composer.Companion.getEmpty();
                }
                writer2.startGroup(n, o);
            }
            final Pending pending2 = this.pending;
            if (pending2 != null) {
                o2 = new KeyInfo(n, -1, this.insertedGroupVirtualIndex(n2), -1, 0);
                pending2.registerInsert((KeyInfo)o2, this.nodeIndex - pending2.getStartIndex());
                pending2.recordUsed((KeyInfo)o2);
            }
            this.enterGroup(b, null);
            return;
        }
        if (n2 != GroupKind.Companion.getNode-ULZAiWs()) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if (n2 == 0 && this.reusing) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if (this.pending == null) {
            final int groupKey = this.reader.getGroupKey();
            if (n2 == 0 && groupKey == n && Intrinsics.areEqual(o, this.reader.getGroupObjectKey())) {
                this.startReaderGroup(b, o2);
            }
            else {
                this.pending = new Pending(this.reader.extractKeys(), this.nodeIndex);
            }
        }
        final Pending pending3 = this.pending;
        Pending pending4 = pending;
        if (pending3 != null) {
            final KeyInfo next = pending3.getNext(n, o);
            if (n2 == 0 && next != null) {
                pending3.recordUsed(next);
                n2 = next.getLocation();
                this.nodeIndex = pending3.nodePositionOf(next) + pending3.getStartIndex();
                n = pending3.slotPositionOf(next);
                final int n4 = n - pending3.getGroupIndex();
                pending3.registerMoveSlot(n, pending3.getGroupIndex());
                this.recordReaderMoving(n2);
                this.reader.reposition(n2);
                if (n4 > 0) {
                    this.recordSlotEditingOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$start.ComposerImpl$start$2(n4));
                }
                this.startReaderGroup(b, o2);
                pending4 = pending;
            }
            else {
                this.reader.beginEmpty();
                this.inserting = true;
                this.providerCache = null;
                this.ensureWriter();
                this.writer.beginInsert();
                n2 = this.writer.getCurrentGroup();
                if (b) {
                    this.writer.startNode(n, Composer.Companion.getEmpty());
                }
                else if (o2 != null) {
                    final SlotWriter writer3 = this.writer;
                    if ((o = o3) == null) {
                        o = Composer.Companion.getEmpty();
                    }
                    writer3.startData(n, o, o2);
                }
                else {
                    final SlotWriter writer4 = this.writer;
                    if ((o = o3) == null) {
                        o = Composer.Companion.getEmpty();
                    }
                    writer4.startGroup(n, o);
                }
                this.insertAnchor = this.writer.anchor(n2);
                o = new KeyInfo(n, -1, this.insertedGroupVirtualIndex(n2), -1, 0);
                pending3.registerInsert((KeyInfo)o, this.nodeIndex - pending3.getStartIndex());
                pending3.recordUsed((KeyInfo)o);
                final List list = new ArrayList();
                if (b) {
                    n = n3;
                }
                else {
                    n = this.nodeIndex;
                }
                pending4 = new Pending(list, n);
            }
        }
        this.enterGroup(b, pending4);
    }
    
    private final void startGroup(final int n) {
        this.start-BaiHCIY(n, null, GroupKind.Companion.getGroup-ULZAiWs(), null);
    }
    
    private final void startGroup(final int n, final Object o) {
        this.start-BaiHCIY(n, o, GroupKind.Companion.getGroup-ULZAiWs(), null);
    }
    
    private final void startReaderGroup(final boolean b, final Object o) {
        if (b) {
            this.reader.startNode();
        }
        else {
            if (o != null && this.reader.getGroupAux() != o) {
                recordSlotTableOperation$default(this, false, (Function3)new ComposerImpl$startReaderGroup.ComposerImpl$startReaderGroup$1(o), 1, null);
            }
            this.reader.startGroup();
        }
    }
    
    private final void startRoot() {
        this.reader = this.slotTable.openReader();
        this.startGroup(100);
        this.parentContext.startComposing$runtime_release();
        this.parentProvider = this.parentContext.getCompositionLocalScope$runtime_release();
        this.providersInvalidStack.push(ComposerKt.access$asInt(this.providersInvalid));
        this.providersInvalid = this.changed(this.parentProvider);
        this.providerCache = null;
        if (!this.forceRecomposeScopes) {
            this.forceRecomposeScopes = this.parentContext.getCollectingParameterInformation$runtime_release();
        }
        final Set set = this.resolveCompositionLocal((CompositionLocal<Set>)InspectionTablesKt.getLocalInspectionTables(), this.parentProvider);
        if (set != null) {
            set.add(this.slotTable);
            this.parentContext.recordInspectionTable$runtime_release(set);
        }
        this.startGroup(this.parentContext.getCompoundHashKey$runtime_release());
    }
    
    private final void updateCompoundKeyWhenWeEnterGroup(final int n, final Object o, final Object o2) {
        if (o == null) {
            if (o2 != null && n == 207 && !Intrinsics.areEqual(o2, Composer.Companion.getEmpty())) {
                this.updateCompoundKeyWhenWeEnterGroupKeyHash(o2.hashCode());
            }
            else {
                this.updateCompoundKeyWhenWeEnterGroupKeyHash(n);
            }
        }
        else if (o instanceof Enum) {
            this.updateCompoundKeyWhenWeEnterGroupKeyHash(((Enum)o).ordinal());
        }
        else {
            this.updateCompoundKeyWhenWeEnterGroupKeyHash(o.hashCode());
        }
    }
    
    private final void updateCompoundKeyWhenWeEnterGroupKeyHash(final int n) {
        this.compoundKeyHash = (n ^ Integer.rotateLeft(this.getCompoundKeyHash(), 3));
    }
    
    private final void updateCompoundKeyWhenWeExitGroup(final int n, final Object o, final Object o2) {
        if (o == null) {
            if (o2 != null && n == 207 && !Intrinsics.areEqual(o2, Composer.Companion.getEmpty())) {
                this.updateCompoundKeyWhenWeExitGroupKeyHash(o2.hashCode());
            }
            else {
                this.updateCompoundKeyWhenWeExitGroupKeyHash(n);
            }
        }
        else if (o instanceof Enum) {
            this.updateCompoundKeyWhenWeExitGroupKeyHash(((Enum)o).ordinal());
        }
        else {
            this.updateCompoundKeyWhenWeExitGroupKeyHash(o.hashCode());
        }
    }
    
    private final void updateCompoundKeyWhenWeExitGroupKeyHash(final int n) {
        this.compoundKeyHash = Integer.rotateRight(n ^ this.getCompoundKeyHash(), 3);
    }
    
    private final void updateNodeCount(final int i, final int j) {
        if (this.updatedNodeCount(i) != j) {
            if (i < 0) {
                HashMap<Integer, Integer> nodeCountVirtualOverrides;
                if ((nodeCountVirtualOverrides = this.nodeCountVirtualOverrides) == null) {
                    final ComposerImpl composerImpl = this;
                    nodeCountVirtualOverrides = new HashMap<Integer, Integer>();
                    this.nodeCountVirtualOverrides = nodeCountVirtualOverrides;
                }
                nodeCountVirtualOverrides.put(i, j);
            }
            else {
                int[] nodeCountOverrides;
                if ((nodeCountOverrides = this.nodeCountOverrides) == null) {
                    final ComposerImpl composerImpl2 = this;
                    nodeCountOverrides = new int[this.reader.getSize()];
                    ArraysKt.fill$default(nodeCountOverrides, -1, 0, 0, 6, (Object)null);
                    this.nodeCountOverrides = nodeCountOverrides;
                }
                nodeCountOverrides[i] = j;
            }
        }
    }
    
    private final void updateNodeCountOverrides(int i, final int n) {
        final int updatedNodeCount = this.updatedNodeCount(i);
        if (updatedNodeCount != n) {
            int n2 = this.pendingStack.getSize() - 1;
            while (i != -1) {
                final int n3 = this.updatedNodeCount(i) + (n - updatedNodeCount);
                this.updateNodeCount(i, n3);
                int n4 = n2;
                int n5;
                while (true) {
                    n5 = n2;
                    if (-1 >= n4) {
                        break;
                    }
                    final Pending pending = this.pendingStack.peek(n4);
                    if (pending != null && pending.updateNodeCount(i, n3)) {
                        n5 = n4 - 1;
                        break;
                    }
                    --n4;
                }
                if (i < 0) {
                    i = this.reader.getParent();
                    n2 = n5;
                }
                else {
                    if (this.reader.isNode(i)) {
                        break;
                    }
                    i = this.reader.parent(i);
                    n2 = n5;
                }
            }
        }
    }
    
    private final PersistentMap<CompositionLocal<Object>, State<Object>> updateProviderMapGroup(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap2) {
        final PersistentMap.Builder<CompositionLocal<Object>, ? extends State<?>> builder = persistentMap.builder();
        builder.putAll(persistentMap2);
        final PersistentMap<CompositionLocal<Object>, ? extends State<?>> build = builder.build();
        this.startGroup(204, ComposerKt.getProviderMaps());
        this.changed(build);
        this.changed(persistentMap2);
        this.endGroup();
        return (PersistentMap<CompositionLocal<Object>, State<Object>>)build;
    }
    
    private final int updatedNodeCount(int intValue) {
        if (intValue < 0) {
            final HashMap<Integer, Integer> nodeCountVirtualOverrides = this.nodeCountVirtualOverrides;
            if (nodeCountVirtualOverrides != null) {
                final Integer n = nodeCountVirtualOverrides.get(intValue);
                if (n != null) {
                    intValue = n;
                    return intValue;
                }
            }
            intValue = 0;
            return intValue;
        }
        final int[] nodeCountOverrides = this.nodeCountOverrides;
        if (nodeCountOverrides != null) {
            final int n2 = nodeCountOverrides[intValue];
            if (n2 >= 0) {
                return n2;
            }
        }
        return this.reader.nodeCount(intValue);
    }
    
    private final void validateNodeExpected() {
        if (this.nodeExpected) {
            this.nodeExpected = false;
            return;
        }
        ComposerKt.composeRuntimeError("A call to createNode(), emitNode() or useNode() expected was not expected".toString());
        throw new KotlinNothingValueException();
    }
    
    private final void validateNodeNotExpected() {
        if (this.nodeExpected ^ true) {
            return;
        }
        ComposerKt.composeRuntimeError("A call to createNode(), emitNode() or useNode() expected".toString());
        throw new KotlinNothingValueException();
    }
    
    private final <R> R withChanges(final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> list, final Function0<? extends R> function0) {
        final List access$getChanges$p = access$getChanges$p(this);
        try {
            access$setChanges$p(this, list);
            return (R)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            access$setChanges$p(this, access$getChanges$p);
            InlineMarker.finallyEnd(1);
        }
    }
    
    private final <R> R withReader(final SlotReader slotReader, final Function0<? extends R> function0) {
        final SlotReader access$getReader$p = access$getReader$p(this);
        final int[] access$getNodeCountOverrides$p = access$getNodeCountOverrides$p(this);
        access$setNodeCountOverrides$p(this, null);
        try {
            access$setReader$p(this, slotReader);
            return (R)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            access$setReader$p(this, access$getReader$p);
            access$setNodeCountOverrides$p(this, access$getNodeCountOverrides$p);
            InlineMarker.finallyEnd(1);
        }
    }
    
    @Override
    public <V, T> void apply(final V v, final Function2<? super T, ? super V, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final Function3 function3 = (Function3)new ComposerImpl$apply$operation.ComposerImpl$apply$operation$1((Function2)function2, (Object)v);
        if (this.getInserting()) {
            this.recordFixup((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)function3);
        }
        else {
            this.recordApplierOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)function3);
        }
    }
    
    @Override
    public CompositionContext buildContext() {
        this.startGroup(206, ComposerKt.getReference());
        final boolean inserting = this.getInserting();
        CompositionContextHolder compositionContextHolder = null;
        if (inserting) {
            SlotWriter.markGroup$default(this.writer, 0, 1, null);
        }
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof CompositionContextHolder) {
            compositionContextHolder = (CompositionContextHolder)nextSlot;
        }
        CompositionContextHolder compositionContextHolder2;
        if ((compositionContextHolder2 = compositionContextHolder) == null) {
            compositionContextHolder2 = new CompositionContextHolder(new CompositionContextImpl(this.getCompoundKeyHash(), this.forceRecomposeScopes));
            this.updateValue(compositionContextHolder2);
        }
        compositionContextHolder2.getRef().updateCompositionLocalScope(this.currentCompositionLocalScope());
        this.endGroup();
        return compositionContextHolder2.getRef();
    }
    
    @ComposeCompilerApi
    public final <T> T cache(final boolean b, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        Object o = this.nextSlot();
        if (o == Composer.Companion.getEmpty() || b) {
            o = function0.invoke();
            this.updateValue(o);
        }
        return (T)o;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final byte b) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Byte && b == ((Number)nextSlot).byteValue()) {
            return false;
        }
        this.updateValue(b);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final char c) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Character && c == (char)nextSlot) {
            return false;
        }
        this.updateValue(c);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final double d) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Double && d == ((Number)nextSlot).doubleValue()) {
            return false;
        }
        this.updateValue(d);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final float f) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Float && f == ((Number)nextSlot).floatValue()) {
            return false;
        }
        this.updateValue(f);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final int i) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Integer && i == ((Number)nextSlot).intValue()) {
            return false;
        }
        this.updateValue(i);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final long l) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Long && l == ((Number)nextSlot).longValue()) {
            return false;
        }
        this.updateValue(l);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final Object o) {
        boolean b;
        if (!Intrinsics.areEqual(this.nextSlot(), o)) {
            this.updateValue(o);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final short s) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Short && s == ((Number)nextSlot).shortValue()) {
            return false;
        }
        this.updateValue(s);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changed(final boolean b) {
        final Object nextSlot = this.nextSlot();
        if (nextSlot instanceof Boolean && b == (boolean)nextSlot) {
            return false;
        }
        this.updateValue(b);
        return true;
    }
    
    @ComposeCompilerApi
    @Override
    public boolean changedInstance(final Object o) {
        boolean b;
        if (this.nextSlot() != o) {
            this.updateValue(o);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public final void changesApplied$runtime_release() {
        this.providerUpdates.clear();
    }
    
    @Override
    public void collectParameterInformation() {
        this.forceRecomposeScopes = true;
    }
    
    public final void composeContent$runtime_release(final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> identityArrayMap, final Function2<? super Composer, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)identityArrayMap, "invalidationsRequested");
        Intrinsics.checkNotNullParameter((Object)function2, "content");
        if (this.changes.isEmpty()) {
            this.doCompose(identityArrayMap, function2);
            return;
        }
        ComposerKt.composeRuntimeError("Expected applyChanges() to have been called".toString());
        throw new KotlinNothingValueException();
    }
    
    @Override
    public <T> T consume(final CompositionLocal<T> compositionLocal) {
        Intrinsics.checkNotNullParameter((Object)compositionLocal, "key");
        return this.resolveCompositionLocal(compositionLocal, this.currentCompositionLocalScope());
    }
    
    @Override
    public <T> void createNode(final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "factory");
        this.validateNodeExpected();
        if (this.getInserting()) {
            final int peek = this.nodeIndexStack.peek();
            final SlotWriter writer = this.writer;
            final Anchor anchor = writer.anchor(writer.getParent());
            ++this.groupNodeCount;
            this.recordFixup((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$createNode.ComposerImpl$createNode$2((Function0)function0, anchor, peek));
            this.recordInsertUpFixup((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$createNode.ComposerImpl$createNode$3(anchor, peek));
            return;
        }
        ComposerKt.composeRuntimeError("createNode() can only be called when inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    @ComposeCompilerApi
    @Override
    public void deactivateToEndGroup(final boolean b) {
        if (this.groupNodeCount == 0) {
            if (!this.getInserting()) {
                if (!b) {
                    this.skipReaderToGroupEnd();
                    return;
                }
                final int currentGroup = this.reader.getCurrentGroup();
                final int currentEnd = this.reader.getCurrentEnd();
                for (int i = currentGroup; i < currentEnd; ++i) {
                    if (this.reader.isNode(i)) {
                        final Object node = this.reader.node(i);
                        if (node instanceof ComposeNodeLifecycleCallback) {
                            this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$deactivateToEndGroup.ComposerImpl$deactivateToEndGroup$2(node));
                        }
                    }
                    this.reader.forEachData$runtime_release(i, (Function2<? super Integer, Object, Unit>)new ComposerImpl$deactivateToEndGroup.ComposerImpl$deactivateToEndGroup$3(this, i));
                }
                ComposerKt.access$removeRange(this.invalidations, currentGroup, currentEnd);
                this.reader.reposition(currentGroup);
                this.reader.skipToGroupEnd();
            }
            return;
        }
        ComposerKt.composeRuntimeError("No nodes can be emitted before calling dactivateToEndGroup".toString());
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void disableReusing() {
        this.reusing = false;
    }
    
    @Override
    public void disableSourceInformation() {
        this.sourceInformationEnabled = false;
    }
    
    public final void dispose$runtime_release() {
        final Object beginSection = Trace.INSTANCE.beginSection("Compose:Composer.dispose");
        try {
            this.parentContext.unregisterComposer$runtime_release(this);
            this.invalidateStack.clear();
            this.invalidations.clear();
            this.changes.clear();
            this.providerUpdates.clear();
            this.getApplier().clear();
            this.isDisposed = true;
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            Trace.INSTANCE.endSection(beginSection);
        }
    }
    
    @Override
    public void enableReusing() {
        this.reusing = (this.reusingGroup >= 0);
    }
    
    @ComposeCompilerApi
    @Override
    public void endDefaults() {
        this.endGroup();
        final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.getCurrentRecomposeScope$runtime_release();
        if (currentRecomposeScope$runtime_release != null && currentRecomposeScope$runtime_release.getUsed()) {
            currentRecomposeScope$runtime_release.setDefaultsInScope(true);
        }
    }
    
    @ComposeCompilerApi
    @Override
    public void endMovableGroup() {
        this.endGroup();
    }
    
    @Override
    public void endNode() {
        this.end(true);
    }
    
    @Override
    public void endProviders() {
        this.endGroup();
        this.endGroup();
        this.providersInvalid = ComposerKt.access$asBool(this.providersInvalidStack.pop());
        this.providerCache = null;
    }
    
    @ComposeCompilerApi
    @Override
    public void endReplaceableGroup() {
        this.endGroup();
    }
    
    @ComposeCompilerApi
    @Override
    public ScopeUpdateScope endRestartGroup() {
        final boolean notEmpty = this.invalidateStack.isNotEmpty();
        final ScopeUpdateScope scopeUpdateScope = null;
        RecomposeScopeImpl recomposeScopeImpl;
        if (notEmpty) {
            recomposeScopeImpl = this.invalidateStack.pop();
        }
        else {
            recomposeScopeImpl = null;
        }
        if (recomposeScopeImpl != null) {
            recomposeScopeImpl.setRequiresRecompose(false);
        }
        if (recomposeScopeImpl != null) {
            final Function1<Composition, Unit> end = recomposeScopeImpl.end(this.compositionToken);
            if (end != null) {
                this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$endRestartGroup$1.ComposerImpl$endRestartGroup$1$1((Function1)end, this));
            }
        }
        ScopeUpdateScope scopeUpdateScope2 = scopeUpdateScope;
        Label_0167: {
            if (recomposeScopeImpl != null) {
                scopeUpdateScope2 = scopeUpdateScope;
                if (!recomposeScopeImpl.getSkipped$runtime_release()) {
                    if (!recomposeScopeImpl.getUsed()) {
                        scopeUpdateScope2 = scopeUpdateScope;
                        if (!this.forceRecomposeScopes) {
                            break Label_0167;
                        }
                    }
                    if (recomposeScopeImpl.getAnchor() == null) {
                        Anchor anchor;
                        if (this.getInserting()) {
                            final SlotWriter writer = this.writer;
                            anchor = writer.anchor(writer.getParent());
                        }
                        else {
                            final SlotReader reader = this.reader;
                            anchor = reader.anchor(reader.getParent());
                        }
                        recomposeScopeImpl.setAnchor(anchor);
                    }
                    recomposeScopeImpl.setDefaultsInvalid(false);
                    scopeUpdateScope2 = recomposeScopeImpl;
                }
            }
        }
        this.end(false);
        return scopeUpdateScope2;
    }
    
    @Override
    public void endReusableGroup() {
        if (this.reusing && this.reader.getParent() == this.reusingGroup) {
            this.reusingGroup = -1;
            this.reusing = false;
        }
        this.end(false);
    }
    
    @Override
    public void endToMarker(int n) {
        if (n < 0) {
            n = -n;
            final SlotWriter writer = this.writer;
            while (true) {
                final int parent = writer.getParent();
                if (parent <= n) {
                    break;
                }
                this.end(writer.isNode(parent));
            }
        }
        else {
            if (this.getInserting()) {
                final SlotWriter writer2 = this.writer;
                while (this.getInserting()) {
                    this.end(writer2.isNode(writer2.getParent()));
                }
            }
            final SlotReader reader = this.reader;
            while (true) {
                final int parent2 = reader.getParent();
                if (parent2 <= n) {
                    break;
                }
                this.end(reader.isNode(parent2));
            }
        }
    }
    
    public final boolean forceRecomposeScopes$runtime_release() {
        final boolean forceRecomposeScopes = this.forceRecomposeScopes;
        boolean b = true;
        if (!forceRecomposeScopes) {
            this.forceRecomposeScopes = true;
            this.forciblyRecompose = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    @Override
    public Applier<?> getApplier() {
        return this.applier;
    }
    
    @Override
    public CoroutineContext getApplyCoroutineContext() {
        return this.parentContext.getEffectCoroutineContext$runtime_release();
    }
    
    public final boolean getAreChildrenComposing$runtime_release() {
        return this.childrenComposing > 0;
    }
    
    public final int getChangeCount$runtime_release() {
        return this.changes.size();
    }
    
    @Override
    public ControlledComposition getComposition() {
        return this.composition;
    }
    
    @Override
    public CompositionData getCompositionData() {
        return this.slotTable;
    }
    
    @Override
    public int getCompoundKeyHash() {
        return this.compoundKeyHash;
    }
    
    @Override
    public int getCurrentMarker() {
        int parent;
        if (this.getInserting()) {
            parent = -this.writer.getParent();
        }
        else {
            parent = this.reader.getParent();
        }
        return parent;
    }
    
    public final RecomposeScopeImpl getCurrentRecomposeScope$runtime_release() {
        final Stack<RecomposeScopeImpl> invalidateStack = this.invalidateStack;
        RecomposeScopeImpl recomposeScopeImpl;
        if (this.childrenComposing == 0 && invalidateStack.isNotEmpty()) {
            recomposeScopeImpl = invalidateStack.peek();
        }
        else {
            recomposeScopeImpl = null;
        }
        return recomposeScopeImpl;
    }
    
    @Override
    public boolean getDefaultsInvalid() {
        final boolean providersInvalid = this.providersInvalid;
        boolean b = false;
        if (!providersInvalid) {
            final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.getCurrentRecomposeScope$runtime_release();
            if (currentRecomposeScope$runtime_release == null || !currentRecomposeScope$runtime_release.getDefaultsInvalid()) {
                return b;
            }
        }
        b = true;
        return b;
    }
    
    public final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> getDeferredChanges$runtime_release() {
        return this.deferredChanges;
    }
    
    public final boolean getHasInvalidations() {
        return this.invalidations.isEmpty() ^ true;
    }
    
    public final boolean getHasPendingChanges$runtime_release() {
        return this.changes.isEmpty() ^ true;
    }
    
    public final SlotTable getInsertTable$runtime_release() {
        return this.insertTable;
    }
    
    @Override
    public boolean getInserting() {
        return this.inserting;
    }
    
    @Override
    public RecomposeScope getRecomposeScope() {
        return this.getCurrentRecomposeScope$runtime_release();
    }
    
    @Override
    public Object getRecomposeScopeIdentity() {
        final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.getCurrentRecomposeScope$runtime_release();
        Anchor anchor;
        if (currentRecomposeScope$runtime_release != null) {
            anchor = currentRecomposeScope$runtime_release.getAnchor();
        }
        else {
            anchor = null;
        }
        return anchor;
    }
    
    @Override
    public boolean getSkipping() {
        final boolean inserting = this.getInserting();
        boolean b = true;
        if (!inserting && !this.reusing && !this.providersInvalid) {
            final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.getCurrentRecomposeScope$runtime_release();
            if (currentRecomposeScope$runtime_release != null && !currentRecomposeScope$runtime_release.getRequiresRecompose() && !this.forciblyRecompose) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    @Override
    public void insertMovableContent(final MovableContent<?> movableContent, final Object o) {
        Intrinsics.checkNotNullParameter((Object)movableContent, "value");
        this.invokeMovableContentLambda((MovableContent<Object>)movableContent, this.currentCompositionLocalScope(), o, false);
    }
    
    @Override
    public void insertMovableContentReferences(final List<Pair<MovableContentStateReference, MovableContentStateReference>> list) {
        Intrinsics.checkNotNullParameter((Object)list, "references");
        try {
            this.insertMovableContentGuarded(list);
            this.cleanUpCompose();
        }
        finally {
            this.abortRoot();
        }
    }
    
    public final boolean isComposing$runtime_release() {
        return this.isComposing;
    }
    
    public final boolean isDisposed$runtime_release() {
        return this.isDisposed;
    }
    
    @ComposeCompilerApi
    @Override
    public Object joinKey(final Object o, final Object o2) {
        Object access$getKey;
        if ((access$getKey = ComposerKt.access$getKey(this.reader.getGroupObjectKey(), o, o2)) == null) {
            access$getKey = new JoinedKey(o, o2);
        }
        return access$getKey;
    }
    
    public final Object nextSlot() {
        Object o;
        if (this.getInserting()) {
            this.validateNodeNotExpected();
            o = Composer.Companion.getEmpty();
        }
        else {
            o = this.reader.next();
            if (this.reusing) {
                o = Composer.Companion.getEmpty();
            }
        }
        return o;
    }
    
    public final int parentKey$runtime_release() {
        int n;
        if (this.getInserting()) {
            final SlotWriter writer = this.writer;
            n = writer.groupKey(writer.getParent());
        }
        else {
            final SlotReader reader = this.reader;
            n = reader.groupKey(reader.getParent());
        }
        return n;
    }
    
    public final void prepareCompose$runtime_release(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        if (this.isComposing ^ true) {
            this.isComposing = true;
            try {
                function0.invoke();
                return;
            }
            finally {
                this.isComposing = false;
            }
        }
        ComposerKt.composeRuntimeError("Preparing a composition while composing is not supported".toString());
        throw new KotlinNothingValueException();
    }
    
    public final boolean recompose$runtime_release(final IdentityArrayMap<RecomposeScopeImpl, IdentityArraySet<Object>> identityArrayMap) {
        Intrinsics.checkNotNullParameter((Object)identityArrayMap, "invalidationsRequested");
        if (!this.changes.isEmpty()) {
            ComposerKt.composeRuntimeError("Expected applyChanges() to have been called".toString());
            throw new KotlinNothingValueException();
        }
        if (!identityArrayMap.isNotEmpty() && !(this.invalidations.isEmpty() ^ true) && !this.forciblyRecompose) {
            return false;
        }
        this.doCompose(identityArrayMap, null);
        return this.changes.isEmpty() ^ true;
    }
    
    @Override
    public void recordSideEffect(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "effect");
        this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$recordSideEffect.ComposerImpl$recordSideEffect$1((Function0)function0));
    }
    
    @Override
    public void recordUsed(final RecomposeScope recomposeScope) {
        Intrinsics.checkNotNullParameter((Object)recomposeScope, "scope");
        RecomposeScopeImpl recomposeScopeImpl;
        if (recomposeScope instanceof RecomposeScopeImpl) {
            recomposeScopeImpl = (RecomposeScopeImpl)recomposeScope;
        }
        else {
            recomposeScopeImpl = null;
        }
        if (recomposeScopeImpl != null) {
            recomposeScopeImpl.setUsed(true);
        }
    }
    
    @Override
    public Object rememberedValue() {
        return this.nextSlot();
    }
    
    public final void setDeferredChanges$runtime_release(final List<Function3<Applier<?>, SlotWriter, RememberManager, Unit>> deferredChanges) {
        this.deferredChanges = deferredChanges;
    }
    
    public final void setInsertTable$runtime_release(final SlotTable insertTable) {
        Intrinsics.checkNotNullParameter((Object)insertTable, "<set-?>");
        this.insertTable = insertTable;
    }
    
    @ComposeCompilerApi
    @Override
    public void skipCurrentGroup() {
        if (this.invalidations.isEmpty()) {
            this.skipGroup();
        }
        else {
            final SlotReader reader = this.reader;
            final int groupKey = reader.getGroupKey();
            final Object groupObjectKey = reader.getGroupObjectKey();
            final Object groupAux = reader.getGroupAux();
            this.updateCompoundKeyWhenWeEnterGroup(groupKey, groupObjectKey, groupAux);
            this.startReaderGroup(reader.isNode(), null);
            this.recomposeToGroupEnd();
            reader.endGroup();
            this.updateCompoundKeyWhenWeExitGroup(groupKey, groupObjectKey, groupAux);
        }
    }
    
    @ComposeCompilerApi
    @Override
    public void skipToGroupEnd() {
        if (this.groupNodeCount == 0) {
            final RecomposeScopeImpl currentRecomposeScope$runtime_release = this.getCurrentRecomposeScope$runtime_release();
            if (currentRecomposeScope$runtime_release != null) {
                currentRecomposeScope$runtime_release.scopeSkipped();
            }
            if (this.invalidations.isEmpty()) {
                this.skipReaderToGroupEnd();
            }
            else {
                this.recomposeToGroupEnd();
            }
            return;
        }
        ComposerKt.composeRuntimeError("No nodes can be emitted before calling skipAndEndGroup".toString());
        throw new KotlinNothingValueException();
    }
    
    @ComposeCompilerApi
    @Override
    public void sourceInformation(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sourceInformation");
        if (this.getInserting() && this.sourceInformationEnabled) {
            this.writer.insertAux(s);
        }
    }
    
    @ComposeCompilerApi
    @Override
    public void sourceInformationMarkerEnd() {
        if (this.sourceInformationEnabled) {
            this.end(false);
        }
    }
    
    @ComposeCompilerApi
    @Override
    public void sourceInformationMarkerStart(final int n, final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sourceInformation");
        if (this.sourceInformationEnabled) {
            this.start-BaiHCIY(n, null, GroupKind.Companion.getGroup-ULZAiWs(), s);
        }
    }
    
    @ComposeCompilerApi
    @Override
    public void startDefaults() {
        this.start-BaiHCIY(-127, null, GroupKind.Companion.getGroup-ULZAiWs(), null);
    }
    
    @ComposeCompilerApi
    @Override
    public void startMovableGroup(final int n, final Object o) {
        this.start-BaiHCIY(n, o, GroupKind.Companion.getGroup-ULZAiWs(), null);
    }
    
    @Override
    public void startNode() {
        this.start-BaiHCIY(125, null, GroupKind.Companion.getNode-ULZAiWs(), null);
        this.nodeExpected = true;
    }
    
    @Override
    public void startProviders(final ProvidedValue<?>[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "values");
        final PersistentMap<CompositionLocal<Object>, State<Object>> currentCompositionLocalScope = this.currentCompositionLocalScope();
        this.startGroup(201, ComposerKt.getProvider());
        this.startGroup(203, ComposerKt.getProviderValues());
        final PersistentMap persistentMap = ActualJvm_jvmKt.invokeComposableForResult((Composer)this, (kotlin.jvm.functions.Function2<? super Composer, ? super Integer, ? extends PersistentMap>)new ComposerImpl$startProviders$currentProviders.ComposerImpl$startProviders$currentProviders$1((ProvidedValue[])array, (PersistentMap)currentCompositionLocalScope));
        this.endGroup();
        final boolean inserting = this.getInserting();
        boolean providersInvalid = false;
        PersistentMap<CompositionLocal<Object>, State<Object>> updateProviderMapGroup;
        if (inserting) {
            updateProviderMapGroup = this.updateProviderMapGroup(currentCompositionLocalScope, persistentMap);
            this.writerHasAProvider = true;
        }
        else {
            final Object groupGet = this.reader.groupGet(0);
            Intrinsics.checkNotNull(groupGet, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<androidx.compose.runtime.CompositionLocal<kotlin.Any?>, androidx.compose.runtime.State<kotlin.Any?>>{ androidx.compose.runtime.ComposerKt.CompositionLocalMap }");
            updateProviderMapGroup = (PersistentMap<CompositionLocal<Object>, State<Object>>)groupGet;
            final Object groupGet2 = this.reader.groupGet(1);
            Intrinsics.checkNotNull(groupGet2, "null cannot be cast to non-null type androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap<androidx.compose.runtime.CompositionLocal<kotlin.Any?>, androidx.compose.runtime.State<kotlin.Any?>>{ androidx.compose.runtime.ComposerKt.CompositionLocalMap }");
            final PersistentMap persistentMap2 = (PersistentMap)groupGet2;
            if (this.getSkipping() && Intrinsics.areEqual((Object)persistentMap2, (Object)persistentMap)) {
                this.skipGroup();
            }
            else {
                final PersistentMap<CompositionLocal<Object>, State<Object>> updateProviderMapGroup2 = this.updateProviderMapGroup(currentCompositionLocalScope, persistentMap);
                providersInvalid = (Intrinsics.areEqual((Object)updateProviderMapGroup2, (Object)updateProviderMapGroup) ^ true);
                updateProviderMapGroup = updateProviderMapGroup2;
            }
        }
        if (providersInvalid && !this.getInserting()) {
            this.providerUpdates.set(this.reader.getCurrentGroup(), updateProviderMapGroup);
        }
        this.providersInvalidStack.push(ComposerKt.access$asInt(this.providersInvalid));
        this.providersInvalid = providersInvalid;
        this.providerCache = updateProviderMapGroup;
        this.start-BaiHCIY(202, ComposerKt.getCompositionLocalMap(), GroupKind.Companion.getGroup-ULZAiWs(), updateProviderMapGroup);
    }
    
    @ComposeCompilerApi
    @Override
    public void startReplaceableGroup(final int n) {
        this.start-BaiHCIY(n, null, GroupKind.Companion.getGroup-ULZAiWs(), null);
    }
    
    @ComposeCompilerApi
    @Override
    public Composer startRestartGroup(final int n) {
        this.start-BaiHCIY(n, null, GroupKind.Companion.getGroup-ULZAiWs(), null);
        this.addRecomposeScope();
        return this;
    }
    
    @Override
    public void startReusableGroup(final int n, final Object o) {
        if (this.reader.getGroupKey() == n && !Intrinsics.areEqual(this.reader.getGroupAux(), o) && this.reusingGroup < 0) {
            this.reusingGroup = this.reader.getCurrentGroup();
            this.reusing = true;
        }
        this.start-BaiHCIY(n, null, GroupKind.Companion.getGroup-ULZAiWs(), o);
    }
    
    @Override
    public void startReusableNode() {
        this.start-BaiHCIY(125, null, GroupKind.Companion.getReusableNode-ULZAiWs(), null);
        this.nodeExpected = true;
    }
    
    public final boolean tryImminentInvalidation$runtime_release(final RecomposeScopeImpl recomposeScopeImpl, final Object o) {
        Intrinsics.checkNotNullParameter((Object)recomposeScopeImpl, "scope");
        final Anchor anchor = recomposeScopeImpl.getAnchor();
        if (anchor == null) {
            return false;
        }
        final int index = anchor.toIndexFor(this.slotTable);
        if (this.isComposing && index >= this.reader.getCurrentGroup()) {
            ComposerKt.access$insertIfMissing(this.invalidations, index, recomposeScopeImpl, o);
            return true;
        }
        return false;
    }
    
    public final void updateCachedValue(final Object o) {
        this.updateValue(o);
    }
    
    @Override
    public void updateRememberedValue(final Object o) {
        this.updateValue(o);
    }
    
    public final void updateValue(final Object o) {
        if (this.getInserting()) {
            this.writer.update(o);
            if (o instanceof RememberObserver) {
                this.record((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$updateValue.ComposerImpl$updateValue$1(o));
                this.abandonSet.add((RememberObserver)o);
            }
        }
        else {
            final int groupSlotIndex = this.reader.getGroupSlotIndex();
            if (o instanceof RememberObserver) {
                this.abandonSet.add((RememberObserver)o);
            }
            this.recordSlotTableOperation(true, (Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)new ComposerImpl$updateValue.ComposerImpl$updateValue$2(o, groupSlotIndex - 1));
        }
    }
    
    @Override
    public void useNode() {
        this.validateNodeExpected();
        if (this.getInserting() ^ true) {
            final Object node = this.getNode(this.reader);
            this.recordDown(node);
            if (this.reusing && node instanceof ComposeNodeLifecycleCallback) {
                this.recordApplierOperation((Function3<? super Applier<?>, ? super SlotWriter, ? super RememberManager, Unit>)ComposerImpl$useNode.ComposerImpl$useNode$2.INSTANCE);
            }
            return;
        }
        ComposerKt.composeRuntimeError("useNode() called while inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void verifyConsistent$runtime_release() {
        this.insertTable.verifyWellFormed();
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0010\u0002\u001a\u00060\u0003R\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0016J\b\u0010\u000b\u001a\u00020\tH\u0016R\u0015\u0010\u0002\u001a\u00060\u0003R\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\f" }, d2 = { "Landroidx/compose/runtime/ComposerImpl$CompositionContextHolder;", "Landroidx/compose/runtime/RememberObserver;", "ref", "Landroidx/compose/runtime/ComposerImpl$CompositionContextImpl;", "Landroidx/compose/runtime/ComposerImpl;", "(Landroidx/compose/runtime/ComposerImpl$CompositionContextImpl;)V", "getRef", "()Landroidx/compose/runtime/ComposerImpl$CompositionContextImpl;", "onAbandoned", "", "onForgotten", "onRemembered", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class CompositionContextHolder implements RememberObserver
    {
        private final CompositionContextImpl ref;
        
        public CompositionContextHolder(final CompositionContextImpl ref) {
            Intrinsics.checkNotNullParameter((Object)ref, "ref");
            this.ref = ref;
        }
        
        public final CompositionContextImpl getRef() {
            return this.ref;
        }
        
        @Override
        public void onAbandoned() {
            this.ref.dispose();
        }
        
        @Override
        public void onForgotten() {
            this.ref.dispose();
        }
        
        @Override
        public void onRemembered() {
        }
    }
    
    @Metadata(d1 = { "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J*\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0011\u0010.\u001a\r\u0012\u0004\u0012\u00020+0/¢\u0006\u0002\b0H\u0010¢\u0006\u0004\b1\u00102J\u0015\u00103\u001a\u00020+2\u0006\u00104\u001a\u000205H\u0010¢\u0006\u0002\b6J\u0006\u00107\u001a\u00020+J\r\u00108\u001a\u00020+H\u0010¢\u0006\u0002\b9J-\u0010\u0015\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u00120\u000fj\u0002`\u0013H\u0010¢\u0006\u0002\b:J\u0015\u0010;\u001a\u00020+2\u0006\u00104\u001a\u000205H\u0010¢\u0006\u0002\b<J\u0015\u0010=\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0010¢\u0006\u0002\b>J\u0015\u0010?\u001a\u00020+2\u0006\u0010@\u001a\u00020AH\u0010¢\u0006\u0002\bBJ\u001d\u0010C\u001a\u00020+2\u0006\u00104\u001a\u0002052\u0006\u0010D\u001a\u00020EH\u0010¢\u0006\u0002\bFJ\u0017\u0010G\u001a\u0004\u0018\u00010E2\u0006\u00104\u001a\u000205H\u0010¢\u0006\u0002\bHJ\u001b\u0010I\u001a\u00020+2\f\u0010J\u001a\b\u0012\u0004\u0012\u00020\"0\nH\u0010¢\u0006\u0002\bKJ\u0015\u0010L\u001a\u00020+2\u0006\u0010M\u001a\u00020NH\u0010¢\u0006\u0002\bOJ\u0015\u0010P\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0010¢\u0006\u0002\bQJ\r\u0010R\u001a\u00020+H\u0010¢\u0006\u0002\bSJ\u0015\u0010T\u001a\u00020+2\u0006\u0010M\u001a\u00020NH\u0010¢\u0006\u0002\bUJ\u0015\u0010V\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0010¢\u0006\u0002\bWJ.\u0010X\u001a\u00020+2&\u0010@\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u00120\u000fj\u0002`\u0013R\u0014\u0010\u0004\u001a\u00020\u0005X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rRk\u0010\u0014\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u00120\u000fj\u0002`\u00132&\u0010\u000e\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u00120\u000fj\u0002`\u00138B@BX\u0082\u008e\u0002¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0002\u001a\u00020\u0003X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001e8PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R(\u0010!\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\"0\n\u0018\u00010\nX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\r\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020\u001e8PX\u0090\u0004¢\u0006\f\u0012\u0004\b'\u0010(\u001a\u0004\b)\u0010 ¨\u0006Y" }, d2 = { "Landroidx/compose/runtime/ComposerImpl$CompositionContextImpl;", "Landroidx/compose/runtime/CompositionContext;", "compoundHashKey", "", "collectingParameterInformation", "", "(Landroidx/compose/runtime/ComposerImpl;IZ)V", "getCollectingParameterInformation$runtime_release", "()Z", "composers", "", "Landroidx/compose/runtime/ComposerImpl;", "getComposers", "()Ljava/util/Set;", "<set-?>", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "Landroidx/compose/runtime/CompositionLocal;", "", "Landroidx/compose/runtime/State;", "Landroidx/compose/runtime/CompositionLocalMap;", "compositionLocalScope", "getCompositionLocalScope", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "setCompositionLocalScope", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;)V", "compositionLocalScope$delegate", "Landroidx/compose/runtime/MutableState;", "getCompoundHashKey$runtime_release", "()I", "effectCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "getEffectCoroutineContext$runtime_release", "()Lkotlin/coroutines/CoroutineContext;", "inspectionTables", "Landroidx/compose/runtime/tooling/CompositionData;", "getInspectionTables", "setInspectionTables", "(Ljava/util/Set;)V", "recomposeCoroutineContext", "getRecomposeCoroutineContext$runtime_release$annotations", "()V", "getRecomposeCoroutineContext$runtime_release", "composeInitial", "", "composition", "Landroidx/compose/runtime/ControlledComposition;", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "composeInitial$runtime_release", "(Landroidx/compose/runtime/ControlledComposition;Lkotlin/jvm/functions/Function2;)V", "deletedMovableContent", "reference", "Landroidx/compose/runtime/MovableContentStateReference;", "deletedMovableContent$runtime_release", "dispose", "doneComposing", "doneComposing$runtime_release", "getCompositionLocalScope$runtime_release", "insertMovableContent", "insertMovableContent$runtime_release", "invalidate", "invalidate$runtime_release", "invalidateScope", "scope", "Landroidx/compose/runtime/RecomposeScopeImpl;", "invalidateScope$runtime_release", "movableContentStateReleased", "data", "Landroidx/compose/runtime/MovableContentState;", "movableContentStateReleased$runtime_release", "movableContentStateResolve", "movableContentStateResolve$runtime_release", "recordInspectionTable", "table", "recordInspectionTable$runtime_release", "registerComposer", "composer", "Landroidx/compose/runtime/Composer;", "registerComposer$runtime_release", "registerComposition", "registerComposition$runtime_release", "startComposing", "startComposing$runtime_release", "unregisterComposer", "unregisterComposer$runtime_release", "unregisterComposition", "unregisterComposition$runtime_release", "updateCompositionLocalScope", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class CompositionContextImpl extends CompositionContext
    {
        private final boolean collectingParameterInformation;
        private final Set<ComposerImpl> composers;
        private final MutableState compositionLocalScope$delegate;
        private final int compoundHashKey;
        private Set<Set<CompositionData>> inspectionTables;
        final ComposerImpl this$0;
        
        public CompositionContextImpl(final ComposerImpl this$0, final int compoundHashKey, final boolean collectingParameterInformation) {
            this.this$0 = this$0;
            this.compoundHashKey = compoundHashKey;
            this.collectingParameterInformation = collectingParameterInformation;
            this.composers = new LinkedHashSet<ComposerImpl>();
            this.compositionLocalScope$delegate = SnapshotStateKt.mutableStateOf$default(ExtensionsKt.persistentHashMapOf(), null, 2, null);
        }
        
        private final PersistentMap<CompositionLocal<Object>, State<Object>> getCompositionLocalScope() {
            return this.compositionLocalScope$delegate.getValue();
        }
        
        private final void setCompositionLocalScope(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> value) {
            this.compositionLocalScope$delegate.setValue(value);
        }
        
        @Override
        public void composeInitial$runtime_release(final ControlledComposition controlledComposition, final Function2<? super Composer, ? super Integer, Unit> function2) {
            Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
            Intrinsics.checkNotNullParameter((Object)function2, "content");
            ComposerImpl.access$getParentContext$p(this.this$0).composeInitial$runtime_release(controlledComposition, function2);
        }
        
        @Override
        public void deletedMovableContent$runtime_release(final MovableContentStateReference movableContentStateReference) {
            Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
            ComposerImpl.access$getParentContext$p(this.this$0).deletedMovableContent$runtime_release(movableContentStateReference);
        }
        
        public final void dispose() {
            if (this.composers.isEmpty() ^ true) {
                final Set<Set<CompositionData>> inspectionTables = this.inspectionTables;
                if (inspectionTables != null) {
                    for (final ComposerImpl composerImpl : this.composers) {
                        final Iterator<Set<CompositionData>> iterator2 = inspectionTables.iterator();
                        while (iterator2.hasNext()) {
                            iterator2.next().remove(ComposerImpl.access$getSlotTable$p(composerImpl));
                        }
                    }
                }
                this.composers.clear();
            }
        }
        
        @Override
        public void doneComposing$runtime_release() {
            final ComposerImpl this$0 = this.this$0;
            ComposerImpl.access$setChildrenComposing$p(this$0, ComposerImpl.access$getChildrenComposing$p(this$0) - 1);
        }
        
        @Override
        public boolean getCollectingParameterInformation$runtime_release() {
            return this.collectingParameterInformation;
        }
        
        public final Set<ComposerImpl> getComposers() {
            return this.composers;
        }
        
        @Override
        public PersistentMap<CompositionLocal<Object>, State<Object>> getCompositionLocalScope$runtime_release() {
            return this.getCompositionLocalScope();
        }
        
        @Override
        public int getCompoundHashKey$runtime_release() {
            return this.compoundHashKey;
        }
        
        @Override
        public CoroutineContext getEffectCoroutineContext$runtime_release() {
            return ComposerImpl.access$getParentContext$p(this.this$0).getEffectCoroutineContext$runtime_release();
        }
        
        public final Set<Set<CompositionData>> getInspectionTables() {
            return this.inspectionTables;
        }
        
        @Override
        public CoroutineContext getRecomposeCoroutineContext$runtime_release() {
            return CompositionKt.getRecomposeCoroutineContext(this.this$0.getComposition());
        }
        
        @Override
        public void insertMovableContent$runtime_release(final MovableContentStateReference movableContentStateReference) {
            Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
            ComposerImpl.access$getParentContext$p(this.this$0).insertMovableContent$runtime_release(movableContentStateReference);
        }
        
        @Override
        public void invalidate$runtime_release(final ControlledComposition controlledComposition) {
            Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
            ComposerImpl.access$getParentContext$p(this.this$0).invalidate$runtime_release(this.this$0.getComposition());
            ComposerImpl.access$getParentContext$p(this.this$0).invalidate$runtime_release(controlledComposition);
        }
        
        @Override
        public void invalidateScope$runtime_release(final RecomposeScopeImpl recomposeScopeImpl) {
            Intrinsics.checkNotNullParameter((Object)recomposeScopeImpl, "scope");
            ComposerImpl.access$getParentContext$p(this.this$0).invalidateScope$runtime_release(recomposeScopeImpl);
        }
        
        @Override
        public void movableContentStateReleased$runtime_release(final MovableContentStateReference movableContentStateReference, final MovableContentState movableContentState) {
            Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
            Intrinsics.checkNotNullParameter((Object)movableContentState, "data");
            ComposerImpl.access$getParentContext$p(this.this$0).movableContentStateReleased$runtime_release(movableContentStateReference, movableContentState);
        }
        
        @Override
        public MovableContentState movableContentStateResolve$runtime_release(final MovableContentStateReference movableContentStateReference) {
            Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
            return ComposerImpl.access$getParentContext$p(this.this$0).movableContentStateResolve$runtime_release(movableContentStateReference);
        }
        
        @Override
        public void recordInspectionTable$runtime_release(final Set<CompositionData> set) {
            Intrinsics.checkNotNullParameter((Object)set, "table");
            Set<Set<CompositionData>> inspectionTables;
            if ((inspectionTables = this.inspectionTables) == null) {
                inspectionTables = new HashSet<Set<CompositionData>>();
                this.inspectionTables = inspectionTables;
            }
            inspectionTables.add(set);
        }
        
        @Override
        public void registerComposer$runtime_release(final Composer composer) {
            Intrinsics.checkNotNullParameter((Object)composer, "composer");
            super.registerComposer$runtime_release(composer);
            this.composers.add((ComposerImpl)composer);
        }
        
        @Override
        public void registerComposition$runtime_release(final ControlledComposition controlledComposition) {
            Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
            ComposerImpl.access$getParentContext$p(this.this$0).registerComposition$runtime_release(controlledComposition);
        }
        
        public final void setInspectionTables(final Set<Set<CompositionData>> inspectionTables) {
            this.inspectionTables = inspectionTables;
        }
        
        @Override
        public void startComposing$runtime_release() {
            final ComposerImpl this$0 = this.this$0;
            ComposerImpl.access$setChildrenComposing$p(this$0, ComposerImpl.access$getChildrenComposing$p(this$0) + 1);
        }
        
        @Override
        public void unregisterComposer$runtime_release(final Composer composer) {
            Intrinsics.checkNotNullParameter((Object)composer, "composer");
            final Set<Set<CompositionData>> inspectionTables = this.inspectionTables;
            if (inspectionTables != null) {
                final Iterator iterator = inspectionTables.iterator();
                while (iterator.hasNext()) {
                    ((Set)iterator.next()).remove(ComposerImpl.access$getSlotTable$p((ComposerImpl)composer));
                }
            }
            TypeIntrinsics.asMutableCollection((Object)this.composers).remove(composer);
        }
        
        @Override
        public void unregisterComposition$runtime_release(final ControlledComposition controlledComposition) {
            Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
            ComposerImpl.access$getParentContext$p(this.this$0).unregisterComposition$runtime_release(controlledComposition);
        }
        
        public final void updateCompositionLocalScope(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> compositionLocalScope) {
            Intrinsics.checkNotNullParameter((Object)compositionLocalScope, "scope");
            this.setCompositionLocalScope(compositionLocalScope);
        }
    }
}

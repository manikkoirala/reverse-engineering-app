// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0007\u001a\u00020\bH&J \u0010\t\u001a\u00020\b2\u0011\u0010\n\u001a\r\u0012\u0004\u0012\u00020\b0\u000b¢\u0006\u0002\b\fH&¢\u0006\u0002\u0010\rR\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0005\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u000e\u00c0\u0006\u0001" }, d2 = { "Landroidx/compose/runtime/Composition;", "", "hasInvalidations", "", "getHasInvalidations", "()Z", "isDisposed", "dispose", "", "setContent", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "(Lkotlin/jvm/functions/Function2;)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface Composition
{
    void dispose();
    
    boolean getHasInvalidations();
    
    boolean isDisposed();
    
    void setContent(final Function2<? super Composer, ? super Integer, Unit> p0);
}

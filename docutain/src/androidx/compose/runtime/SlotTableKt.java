// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import java.util.Iterator;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.ranges.RangesKt;
import kotlin.ranges.IntProgression;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0000\n\u0002\u0010\b\n\u0002\b\u0014\n\u0002\u0010\u0002\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u001c\n\u0002\b\n\u001a\u0010\u0010\u0013\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u0015\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u0019\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u001a\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u001c\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u001d\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\b\b\u0002\u0010 \u001a\u00020\u0001H\u0002\u001a;\u0010!\u001a\u00020\"*\u0012\u0012\u0004\u0012\u00020\"0#j\b\u0012\u0004\u0012\u00020\"`$2\u0006\u0010%\u001a\u00020\u00012\u0006\u0010&\u001a\u00020\u00012\f\u0010'\u001a\b\u0012\u0004\u0012\u00020\"0(H\u0082\b\u001a\u0014\u0010)\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010*\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\b\b\u0002\u0010 \u001a\u00020\u0001H\u0002\u001a\u0014\u0010,\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010-\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010.\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001aD\u0010/\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u00100\u001a\u00020\u00012\u0006\u00101\u001a\u00020\u001b2\u0006\u00102\u001a\u00020\u001b2\u0006\u00103\u001a\u00020\u001b2\u0006\u00104\u001a\u00020\u00012\u0006\u0010\u001d\u001a\u00020\u0001H\u0002\u001a\u0014\u00101\u001a\u00020\u001b*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u00100\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u00105\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\b\b\u0002\u0010 \u001a\u00020\u0001H\u0002\u001a,\u00106\u001a\u00020\u0001*\u0012\u0012\u0004\u0012\u00020\"0#j\b\u0012\u0004\u0012\u00020\"`$2\u0006\u0010%\u001a\u00020\u00012\u0006\u0010&\u001a\u00020\u0001H\u0002\u001a\u0014\u00107\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u00108\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\b\b\u0002\u0010 \u001a\u00020\u0001H\u0002\u001a\u0014\u00109\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u0010:\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u0014\u00104\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u0010;\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\b\b\u0002\u0010 \u001a\u00020\u0001H\u0002\u001a,\u0010<\u001a\u00020\u0001*\u0012\u0012\u0004\u0012\u00020\"0#j\b\u0012\u0004\u0012\u00020\"`$2\u0006\u0010=\u001a\u00020\u00012\u0006\u0010&\u001a\u00020\u0001H\u0002\u001a \u0010>\u001a\b\u0012\u0004\u0012\u00020\u00010\u001f*\u00020\u00172\f\u0010?\u001a\b\u0012\u0004\u0012\u00020\u00010@H\u0002\u001a\u0014\u0010A\u001a\u00020\u0001*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0001H\u0002\u001a\u001c\u0010B\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u001bH\u0002\u001a\u001c\u0010C\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010D\u001a\u00020\u0001H\u0002\u001a\u001c\u0010E\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u00100\u001a\u00020\u0001H\u0002\u001a\u001c\u0010F\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u0001H\u0002\u001a\u001c\u0010G\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u001bH\u0002\u001a\u001c\u0010H\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u0001H\u0002\u001a\u001c\u0010I\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000¨\u0006J" }, d2 = { "Aux_Mask", "", "Aux_Shift", "ContainsMark_Mask", "DataAnchor_Offset", "GroupInfo_Offset", "Group_Fields_Size", "Key_Offset", "Mark_Mask", "MinGroupGrowthSize", "MinSlotsGrowthSize", "NodeBit_Mask", "NodeCount_Mask", "ObjectKey_Mask", "ObjectKey_Shift", "ParentAnchor_Offset", "Size_Offset", "Slots_Shift", "parentAnchorPivot", "countOneBits", "value", "addAux", "", "", "address", "auxIndex", "containsAnyMark", "", "containsMark", "dataAnchor", "dataAnchors", "", "len", "getOrAdd", "Landroidx/compose/runtime/Anchor;", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "index", "effectiveSize", "block", "Lkotlin/Function0;", "groupInfo", "groupSize", "groupSizes", "hasAux", "hasMark", "hasObjectKey", "initGroup", "key", "isNode", "hasDataKey", "hasData", "parentAnchor", "keys", "locationOf", "nodeCount", "nodeCounts", "nodeIndex", "objectKeyIndex", "parentAnchors", "search", "location", "slice", "indices", "", "slotAnchor", "updateContainsMark", "updateDataAnchor", "anchor", "updateGroupKey", "updateGroupSize", "updateMark", "updateNodeCount", "updateParentAnchor", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SlotTableKt
{
    private static final int Aux_Mask = 268435456;
    private static final int Aux_Shift = 28;
    private static final int ContainsMark_Mask = 67108864;
    private static final int DataAnchor_Offset = 4;
    private static final int GroupInfo_Offset = 1;
    private static final int Group_Fields_Size = 5;
    private static final int Key_Offset = 0;
    private static final int Mark_Mask = 134217728;
    private static final int MinGroupGrowthSize = 32;
    private static final int MinSlotsGrowthSize = 32;
    private static final int NodeBit_Mask = 1073741824;
    private static final int NodeCount_Mask = 67108863;
    private static final int ObjectKey_Mask = 536870912;
    private static final int ObjectKey_Shift = 29;
    private static final int ParentAnchor_Offset = 2;
    private static final int Size_Offset = 3;
    private static final int Slots_Shift = 28;
    private static final int parentAnchorPivot = -2;
    
    private static final void addAux(final int[] array, int n) {
        n = n * 5 + 1;
        array[n] |= 0x10000000;
    }
    
    private static final int auxIndex(final int[] array, int length) {
        length *= 5;
        if (length >= array.length) {
            length = array.length;
        }
        else {
            length = countOneBits(array[length + 1] >> 29) + array[length + 4];
        }
        return length;
    }
    
    private static final boolean containsAnyMark(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0xC000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final boolean containsMark(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0x4000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final int countOneBits(final int n) {
        int n2 = 2;
        switch (n) {
            default: {
                n2 = 3;
                return n2;
            }
            case 3:
            case 5:
            case 6: {
                return n2;
            }
            case 1:
            case 2:
            case 4: {
                n2 = 1;
                return n2;
            }
            case 0: {
                n2 = 0;
                return n2;
            }
        }
    }
    
    private static final int dataAnchor(final int[] array, final int n) {
        return array[n * 5 + 4];
    }
    
    private static final List<Integer> dataAnchors(final int[] array, final int n) {
        return slice(array, (Iterable<Integer>)RangesKt.step((IntProgression)RangesKt.until(4, n), 5));
    }
    
    private static final Anchor getOrAdd(final ArrayList<Anchor> list, int access$search, final int n, final Function0<Anchor> function0) {
        access$search = search(list, access$search, n);
        Anchor anchor;
        if (access$search < 0) {
            final Anchor element = (Anchor)function0.invoke();
            list.add(-(access$search + 1), element);
            anchor = element;
        }
        else {
            final Anchor value = list.get(access$search);
            Intrinsics.checkNotNullExpressionValue((Object)value, "get(location)");
            anchor = value;
        }
        return anchor;
    }
    
    private static final int groupInfo(final int[] array, final int n) {
        return array[n * 5 + 1];
    }
    
    private static final int groupSize(final int[] array, final int n) {
        return array[n * 5 + 3];
    }
    
    private static final List<Integer> groupSizes(final int[] array, final int n) {
        return slice(array, (Iterable<Integer>)RangesKt.step((IntProgression)RangesKt.until(3, n), 5));
    }
    
    private static final boolean hasAux(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0x10000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final boolean hasMark(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0x8000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final boolean hasObjectKey(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0x20000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final void initGroup(final int[] array, int n, final int n2, final boolean b, final boolean b2, final boolean b3, final int n3, final int n4) {
        int n5;
        if (b) {
            n5 = 1073741824;
        }
        else {
            n5 = 0;
        }
        int n6;
        if (b2) {
            n6 = 536870912;
        }
        else {
            n6 = 0;
        }
        int n7;
        if (b3) {
            n7 = 268435456;
        }
        else {
            n7 = 0;
        }
        n *= 5;
        array[n + 0] = n2;
        array[n + 1] = (n5 | n6 | n7);
        array[n + 2] = n3;
        array[n + 3] = 0;
        array[n + 4] = n4;
    }
    
    private static final boolean isNode(final int[] array, final int n) {
        boolean b = true;
        if ((array[n * 5 + 1] & 0x40000000) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private static final int key(final int[] array, final int n) {
        return array[n * 5];
    }
    
    private static final List<Integer> keys(final int[] array, final int n) {
        return slice(array, (Iterable<Integer>)RangesKt.step((IntProgression)RangesKt.until(0, n), 5));
    }
    
    private static final int locationOf(final ArrayList<Anchor> list, int search, final int n) {
        search = search(list, search, n);
        if (search < 0) {
            search = -(search + 1);
        }
        return search;
    }
    
    private static final int nodeCount(final int[] array, final int n) {
        return array[n * 5 + 1] & 0x3FFFFFF;
    }
    
    private static final List<Integer> nodeCounts(final int[] array, int i) {
        final List<Integer> slice = slice(array, (Iterable<Integer>)RangesKt.step((IntProgression)RangesKt.until(1, i), 5));
        final ArrayList list = new ArrayList<Integer>(slice.size());
        int size;
        for (size = slice.size(), i = 0; i < size; ++i) {
            list.add(Integer.valueOf(slice.get(i).intValue() & 0x3FFFFFF));
        }
        return (List)list;
    }
    
    private static final int nodeIndex(final int[] array, final int n) {
        return array[n * 5 + 4];
    }
    
    private static final int objectKeyIndex(final int[] array, int n) {
        n *= 5;
        return array[n + 4] + countOneBits(array[n + 1] >> 30);
    }
    
    private static final int parentAnchor(final int[] array, final int n) {
        return array[n * 5 + 2];
    }
    
    private static final List<Integer> parentAnchors(final int[] array, final int n) {
        return slice(array, (Iterable<Integer>)RangesKt.step((IntProgression)RangesKt.until(2, n), 5));
    }
    
    private static final int search(final ArrayList<Anchor> list, final int n, final int n2) {
        int n3 = list.size() - 1;
        int i = 0;
        while (i <= n3) {
            final int index = i + n3 >>> 1;
            final int location$runtime_release = list.get(index).getLocation$runtime_release();
            int n4;
            if ((n4 = location$runtime_release) < 0) {
                n4 = location$runtime_release + n2;
            }
            final int compare = Intrinsics.compare(n4, n);
            if (compare < 0) {
                i = index + 1;
            }
            else {
                if (compare <= 0) {
                    return index;
                }
                n3 = index - 1;
            }
        }
        return -(i + 1);
    }
    
    private static final List<Integer> slice(final int[] array, final Iterable<Integer> iterable) {
        final List list = new ArrayList();
        final Iterator<Integer> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            list.add(array[iterator.next().intValue()]);
        }
        return list;
    }
    
    private static final int slotAnchor(final int[] array, int n) {
        n *= 5;
        return array[n + 4] + countOneBits(array[n + 1] >> 28);
    }
    
    private static final void updateContainsMark(final int[] array, int n, final boolean b) {
        n = n * 5 + 1;
        if (b) {
            array[n] |= 0x4000000;
        }
        else {
            array[n] &= 0xFBFFFFFF;
        }
    }
    
    private static final void updateDataAnchor(final int[] array, final int n, final int n2) {
        array[n * 5 + 4] = n2;
    }
    
    private static final void updateGroupKey(final int[] array, final int n, final int n2) {
        array[n * 5 + 0] = n2;
    }
    
    private static final void updateGroupSize(final int[] array, final int n, final int n2) {
        ComposerKt.runtimeCheck(n2 >= 0);
        array[n * 5 + 3] = n2;
    }
    
    private static final void updateMark(final int[] array, int n, final boolean b) {
        n = n * 5 + 1;
        if (b) {
            array[n] |= 0x8000000;
        }
        else {
            array[n] &= 0xF7FFFFFF;
        }
    }
    
    private static final void updateNodeCount(final int[] array, int n, final int n2) {
        ComposerKt.runtimeCheck(n2 >= 0 && n2 < 67108863);
        n = n * 5 + 1;
        array[n] = (n2 | (array[n] & 0xFC000000));
    }
    
    private static final void updateParentAnchor(final int[] array, final int n, final int n2) {
        array[n * 5 + 2] = n2;
    }
}

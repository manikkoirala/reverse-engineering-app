// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J:\u0010\u0005\u001a\u0002H\u0006\"\u0004\b\u0000\u0010\u00062!\u0010\u0007\u001a\u001d\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u0002H\u00060\bH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\r" }, d2 = { "Landroidx/compose/runtime/SdkStubsFallbackFrameClock;", "Landroidx/compose/runtime/MonotonicFrameClock;", "()V", "DefaultFrameDelay", "", "withFrameNanos", "R", "onFrame", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "frameTimeNanos", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SdkStubsFallbackFrameClock implements MonotonicFrameClock
{
    private static final long DefaultFrameDelay = 16L;
    public static final SdkStubsFallbackFrameClock INSTANCE;
    
    static {
        INSTANCE = new SdkStubsFallbackFrameClock();
    }
    
    private SdkStubsFallbackFrameClock() {
    }
    
    public <R> R fold(final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
        return DefaultImpls.fold(this, r, function2);
    }
    
    public <E extends CoroutineContext$Element> E get(final CoroutineContext$Key<E> coroutineContext$Key) {
        return DefaultImpls.get(this, coroutineContext$Key);
    }
    
    public CoroutineContext minusKey(final CoroutineContext$Key<?> coroutineContext$Key) {
        return DefaultImpls.minusKey(this, coroutineContext$Key);
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return DefaultImpls.plus(this, coroutineContext);
    }
    
    @Override
    public <R> Object withFrameNanos(final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        return BuildersKt.withContext((CoroutineContext)Dispatchers.getMain(), (Function2)new SdkStubsFallbackFrameClock$withFrameNanos.SdkStubsFallbackFrameClock$withFrameNanos$2((Function1)function1, (Continuation)null), (Continuation)continuation);
    }
}

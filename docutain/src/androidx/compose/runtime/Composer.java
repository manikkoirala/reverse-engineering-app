// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import java.util.List;
import androidx.compose.runtime.tooling.CompositionData;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u00c2\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0005\n\u0002\u0010\f\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\bv\u0018\u0000 \u007f2\u00020\u0001:\u0001\u007fJ@\u00101\u001a\u000202\"\u0004\b\u0000\u00103\"\u0004\b\u0001\u001042\u0006\u00105\u001a\u0002H32\u001d\u00106\u001a\u0019\u0012\u0004\u0012\u0002H4\u0012\u0004\u0012\u0002H3\u0012\u0004\u0012\u00020207¢\u0006\u0002\b8H'¢\u0006\u0002\u00109J\b\u0010:\u001a\u00020;H'J\u0012\u0010<\u001a\u00020\u001e2\b\u00105\u001a\u0004\u0018\u00010\u0001H'J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020\u001eH\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020=H\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020>H\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020?H\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020@H\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020\u0016H\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020AH\u0017J\u0010\u0010<\u001a\u00020\u001e2\u0006\u00105\u001a\u00020BH\u0017J\u0012\u0010C\u001a\u00020\u001e2\b\u00105\u001a\u0004\u0018\u00010\u0001H\u0017J\b\u0010D\u001a\u000202H&J!\u0010E\u001a\u0002H4\"\u0004\b\u0000\u001042\f\u0010F\u001a\b\u0012\u0004\u0012\u0002H40GH'¢\u0006\u0002\u0010HJ\u001c\u0010I\u001a\u000202\"\u0004\b\u0000\u001042\f\u0010J\u001a\b\u0012\u0004\u0012\u0002H40KH'J\u0010\u0010L\u001a\u0002022\u0006\u0010<\u001a\u00020\u001eH'J\b\u0010M\u001a\u000202H'J\b\u0010N\u001a\u000202H&J\b\u0010O\u001a\u000202H'J\b\u0010P\u001a\u000202H'J\b\u0010Q\u001a\u000202H'J\b\u0010R\u001a\u000202H'J\b\u0010S\u001a\u000202H'J\b\u0010T\u001a\u000202H'J\n\u0010U\u001a\u0004\u0018\u00010VH'J\b\u0010W\u001a\u000202H'J\u0010\u0010X\u001a\u0002022\u0006\u0010Y\u001a\u00020\u0016H'J\u001e\u0010Z\u001a\u0002022\n\u00105\u001a\u0006\u0012\u0002\b\u00030[2\b\u0010\\\u001a\u0004\u0018\u00010\u0001H'J$\u0010]\u001a\u0002022\u001a\u0010^\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020a\u0012\u0006\u0012\u0004\u0018\u00010a0`0_H'J\u001c\u0010b\u001a\u00020\u00012\b\u0010c\u001a\u0004\u0018\u00010\u00012\b\u0010d\u001a\u0004\u0018\u00010\u0001H'J\u0016\u0010e\u001a\u0002022\f\u0010f\u001a\b\u0012\u0004\u0012\u0002020KH'J\u0010\u0010g\u001a\u0002022\u0006\u0010h\u001a\u00020&H'J\n\u0010i\u001a\u0004\u0018\u00010\u0001H'J\b\u0010j\u001a\u000202H'J\b\u0010k\u001a\u000202H'J\u0010\u0010l\u001a\u0002022\u0006\u0010l\u001a\u00020mH&J\b\u0010n\u001a\u000202H&J\u0018\u0010o\u001a\u0002022\u0006\u0010F\u001a\u00020\u00162\u0006\u0010l\u001a\u00020mH&J\b\u0010p\u001a\u000202H'J\u001a\u0010q\u001a\u0002022\u0006\u0010F\u001a\u00020\u00162\b\u0010r\u001a\u0004\u0018\u00010\u0001H'J\b\u0010s\u001a\u000202H'J!\u0010t\u001a\u0002022\u0012\u0010u\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030w0vH'¢\u0006\u0002\u0010xJ\u0010\u0010y\u001a\u0002022\u0006\u0010F\u001a\u00020\u0016H'J\u0010\u0010z\u001a\u00020\u00002\u0006\u0010F\u001a\u00020\u0016H'J\u001a\u0010{\u001a\u0002022\u0006\u0010F\u001a\u00020\u00162\b\u0010r\u001a\u0004\u0018\u00010\u0001H'J\b\u0010|\u001a\u000202H'J\u0012\u0010}\u001a\u0002022\b\u00105\u001a\u0004\u0018\u00010\u0001H'J\b\u0010~\u001a\u000202H'R\u001e\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u00038&X§\u0004¢\u0006\f\u0012\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\u00020\t8fX§\u0004¢\u0006\f\u0012\u0004\b\n\u0010\u0005\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000e8fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u0012X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u00168&X§\u0004¢\u0006\f\u0012\u0004\b\u0017\u0010\u0005\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u00020\u00168&X§\u0004¢\u0006\f\u0012\u0004\b\u001b\u0010\u0005\u001a\u0004\b\u001c\u0010\u0019R\u001a\u0010\u001d\u001a\u00020\u001e8&X§\u0004¢\u0006\f\u0012\u0004\b\u001f\u0010\u0005\u001a\u0004\b \u0010!R\u001a\u0010\"\u001a\u00020\u001e8&X§\u0004¢\u0006\f\u0012\u0004\b#\u0010\u0005\u001a\u0004\b$\u0010!R\u001c\u0010%\u001a\u0004\u0018\u00010&8&X§\u0004¢\u0006\f\u0012\u0004\b'\u0010\u0005\u001a\u0004\b(\u0010)R\u001c\u0010*\u001a\u0004\u0018\u00010\u00018&X§\u0004¢\u0006\f\u0012\u0004\b+\u0010\u0005\u001a\u0004\b,\u0010-R\u001a\u0010.\u001a\u00020\u001e8&X§\u0004¢\u0006\f\u0012\u0004\b/\u0010\u0005\u001a\u0004\b0\u0010!\u0082\u0001\u0002\u0080\u0001\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0081\u0001\u00c0\u0006\u0001" }, d2 = { "Landroidx/compose/runtime/Composer;", "", "applier", "Landroidx/compose/runtime/Applier;", "getApplier$annotations", "()V", "getApplier", "()Landroidx/compose/runtime/Applier;", "applyCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "getApplyCoroutineContext$annotations", "getApplyCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "composition", "Landroidx/compose/runtime/ControlledComposition;", "getComposition", "()Landroidx/compose/runtime/ControlledComposition;", "compositionData", "Landroidx/compose/runtime/tooling/CompositionData;", "getCompositionData", "()Landroidx/compose/runtime/tooling/CompositionData;", "compoundKeyHash", "", "getCompoundKeyHash$annotations", "getCompoundKeyHash", "()I", "currentMarker", "getCurrentMarker$annotations", "getCurrentMarker", "defaultsInvalid", "", "getDefaultsInvalid$annotations", "getDefaultsInvalid", "()Z", "inserting", "getInserting$annotations", "getInserting", "recomposeScope", "Landroidx/compose/runtime/RecomposeScope;", "getRecomposeScope$annotations", "getRecomposeScope", "()Landroidx/compose/runtime/RecomposeScope;", "recomposeScopeIdentity", "getRecomposeScopeIdentity$annotations", "getRecomposeScopeIdentity", "()Ljava/lang/Object;", "skipping", "getSkipping$annotations", "getSkipping", "apply", "", "V", "T", "value", "block", "Lkotlin/Function2;", "Lkotlin/ExtensionFunctionType;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "buildContext", "Landroidx/compose/runtime/CompositionContext;", "changed", "", "", "", "", "", "", "changedInstance", "collectParameterInformation", "consume", "key", "Landroidx/compose/runtime/CompositionLocal;", "(Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;", "createNode", "factory", "Lkotlin/Function0;", "deactivateToEndGroup", "disableReusing", "disableSourceInformation", "enableReusing", "endDefaults", "endMovableGroup", "endNode", "endProviders", "endReplaceableGroup", "endRestartGroup", "Landroidx/compose/runtime/ScopeUpdateScope;", "endReusableGroup", "endToMarker", "marker", "insertMovableContent", "Landroidx/compose/runtime/MovableContent;", "parameter", "insertMovableContentReferences", "references", "", "Lkotlin/Pair;", "Landroidx/compose/runtime/MovableContentStateReference;", "joinKey", "left", "right", "recordSideEffect", "effect", "recordUsed", "scope", "rememberedValue", "skipCurrentGroup", "skipToGroupEnd", "sourceInformation", "", "sourceInformationMarkerEnd", "sourceInformationMarkerStart", "startDefaults", "startMovableGroup", "dataKey", "startNode", "startProviders", "values", "", "Landroidx/compose/runtime/ProvidedValue;", "([Landroidx/compose/runtime/ProvidedValue;)V", "startReplaceableGroup", "startRestartGroup", "startReusableGroup", "startReusableNode", "updateRememberedValue", "useNode", "Companion", "Landroidx/compose/runtime/ComposerImpl;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface Composer
{
    public static final Companion Companion = Composer.Companion.$$INSTANCE;
    
    @ComposeCompilerApi
     <V, T> void apply(final V p0, final Function2<? super T, ? super V, Unit> p1);
    
    CompositionContext buildContext();
    
    @ComposeCompilerApi
    boolean changed(final byte p0);
    
    @ComposeCompilerApi
    boolean changed(final char p0);
    
    @ComposeCompilerApi
    boolean changed(final double p0);
    
    @ComposeCompilerApi
    boolean changed(final float p0);
    
    @ComposeCompilerApi
    boolean changed(final int p0);
    
    @ComposeCompilerApi
    boolean changed(final long p0);
    
    @ComposeCompilerApi
    boolean changed(final Object p0);
    
    @ComposeCompilerApi
    boolean changed(final short p0);
    
    @ComposeCompilerApi
    boolean changed(final boolean p0);
    
    @ComposeCompilerApi
    boolean changedInstance(final Object p0);
    
    void collectParameterInformation();
    
     <T> T consume(final CompositionLocal<T> p0);
    
    @ComposeCompilerApi
     <T> void createNode(final Function0<? extends T> p0);
    
    @ComposeCompilerApi
    void deactivateToEndGroup(final boolean p0);
    
    @ComposeCompilerApi
    void disableReusing();
    
    void disableSourceInformation();
    
    @ComposeCompilerApi
    void enableReusing();
    
    @ComposeCompilerApi
    void endDefaults();
    
    @ComposeCompilerApi
    void endMovableGroup();
    
    @ComposeCompilerApi
    void endNode();
    
    void endProviders();
    
    @ComposeCompilerApi
    void endReplaceableGroup();
    
    @ComposeCompilerApi
    ScopeUpdateScope endRestartGroup();
    
    @ComposeCompilerApi
    void endReusableGroup();
    
    @ComposeCompilerApi
    void endToMarker(final int p0);
    
    Applier<?> getApplier();
    
    CoroutineContext getApplyCoroutineContext();
    
    ControlledComposition getComposition();
    
    CompositionData getCompositionData();
    
    int getCompoundKeyHash();
    
    int getCurrentMarker();
    
    boolean getDefaultsInvalid();
    
    boolean getInserting();
    
    RecomposeScope getRecomposeScope();
    
    Object getRecomposeScopeIdentity();
    
    boolean getSkipping();
    
    void insertMovableContent(final MovableContent<?> p0, final Object p1);
    
    void insertMovableContentReferences(final List<Pair<MovableContentStateReference, MovableContentStateReference>> p0);
    
    @ComposeCompilerApi
    Object joinKey(final Object p0, final Object p1);
    
    void recordSideEffect(final Function0<Unit> p0);
    
    void recordUsed(final RecomposeScope p0);
    
    @ComposeCompilerApi
    Object rememberedValue();
    
    @ComposeCompilerApi
    void skipCurrentGroup();
    
    @ComposeCompilerApi
    void skipToGroupEnd();
    
    void sourceInformation(final String p0);
    
    void sourceInformationMarkerEnd();
    
    void sourceInformationMarkerStart(final int p0, final String p1);
    
    @ComposeCompilerApi
    void startDefaults();
    
    @ComposeCompilerApi
    void startMovableGroup(final int p0, final Object p1);
    
    @ComposeCompilerApi
    void startNode();
    
    void startProviders(final ProvidedValue<?>[] p0);
    
    @ComposeCompilerApi
    void startReplaceableGroup(final int p0);
    
    @ComposeCompilerApi
    Composer startRestartGroup(final int p0);
    
    @ComposeCompilerApi
    void startReusableGroup(final int p0, final Object p1);
    
    @ComposeCompilerApi
    void startReusableNode();
    
    @ComposeCompilerApi
    void updateRememberedValue(final Object p0);
    
    @ComposeCompilerApi
    void useNode();
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007R\u0011\u0010\u0003\u001a\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005¨\u0006\n" }, d2 = { "Landroidx/compose/runtime/Composer$Companion;", "", "()V", "Empty", "getEmpty", "()Ljava/lang/Object;", "setTracer", "", "tracer", "Landroidx/compose/runtime/CompositionTracer;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        static final Companion $$INSTANCE;
        private static final Object Empty;
        
        static {
            $$INSTANCE = new Companion();
            Empty = new Composer$Companion$Empty.Composer$Companion$Empty$1();
        }
        
        private Companion() {
        }
        
        public final Object getEmpty() {
            return Companion.Empty;
        }
        
        public final void setTracer(final CompositionTracer compositionTracer) {
            Intrinsics.checkNotNullParameter((Object)compositionTracer, "tracer");
            ComposerKt.access$setCompositionTracer$p(compositionTracer);
        }
    }
}

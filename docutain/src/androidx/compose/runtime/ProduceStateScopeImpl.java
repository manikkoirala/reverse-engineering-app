// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.functions.Function1;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.KotlinNothingValueException;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u001b\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001f\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0014J\u000e\u0010\u0015\u001a\u00028\u0000H\u0096\u0003¢\u0006\u0002\u0010\fJ\u0015\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00130\u0017H\u0096\u0003R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0018\u0010\n\u001a\u00028\u0000X\u0096\u000f¢\u0006\f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0018" }, d2 = { "Landroidx/compose/runtime/ProduceStateScopeImpl;", "T", "Landroidx/compose/runtime/ProduceStateScope;", "Landroidx/compose/runtime/MutableState;", "state", "coroutineContext", "Lkotlin/coroutines/CoroutineContext;", "(Landroidx/compose/runtime/MutableState;Lkotlin/coroutines/CoroutineContext;)V", "getCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "value", "getValue", "()Ljava/lang/Object;", "setValue", "(Ljava/lang/Object;)V", "awaitDispose", "", "onDispose", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "component1", "component2", "Lkotlin/Function1;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ProduceStateScopeImpl<T> implements ProduceStateScope<T>, MutableState<T>
{
    private final MutableState<T> $$delegate_0;
    private final CoroutineContext coroutineContext;
    
    public ProduceStateScopeImpl(final MutableState<T> $$delegate_0, final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)$$delegate_0, "state");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "coroutineContext");
        this.coroutineContext = coroutineContext;
        this.$$delegate_0 = $$delegate_0;
    }
    
    @Override
    public Object awaitDispose(Function0<Unit> l$0, Continuation<?> function0) {
        ProduceStateScopeImpl$awaitDispose.ProduceStateScopeImpl$awaitDispose$1 produceStateScopeImpl$awaitDispose$1 = null;
        Label_0051: {
            if (function0 instanceof ProduceStateScopeImpl$awaitDispose.ProduceStateScopeImpl$awaitDispose$1) {
                produceStateScopeImpl$awaitDispose$1 = (ProduceStateScopeImpl$awaitDispose.ProduceStateScopeImpl$awaitDispose$1)function0;
                if ((produceStateScopeImpl$awaitDispose$1.label & Integer.MIN_VALUE) != 0x0) {
                    produceStateScopeImpl$awaitDispose$1.label += Integer.MIN_VALUE;
                    break Label_0051;
                }
            }
            produceStateScopeImpl$awaitDispose$1 = new ProduceStateScopeImpl$awaitDispose.ProduceStateScopeImpl$awaitDispose$1(this, (Continuation)function0);
        }
        final Object result = produceStateScopeImpl$awaitDispose$1.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = produceStateScopeImpl$awaitDispose$1.label;
        Label_0107: {
            if (label == 0) {
                break Label_0107;
            }
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            l$0 = (function0 = (Function0)produceStateScopeImpl$awaitDispose$1.L$0);
            try {
                ResultKt.throwOnFailure(result);
                Label_0200: {
                    function0 = l$0;
                }
                function0 = l$0;
                final KotlinNothingValueException ex = new KotlinNothingValueException();
                function0 = l$0;
                throw ex;
                ResultKt.throwOnFailure(result);
                function0 = l$0;
                produceStateScopeImpl$awaitDispose$1.L$0 = l$0;
                function0 = l$0;
                produceStateScopeImpl$awaitDispose$1.label = 1;
                function0 = l$0;
                function0 = l$0;
                final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)produceStateScopeImpl$awaitDispose$1), 1);
                function0 = l$0;
                cancellableContinuationImpl.initCancellability();
                function0 = l$0;
                final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
                function0 = l$0;
                final Object result2 = cancellableContinuationImpl.getResult();
                function0 = l$0;
                iftrue(Label_0190:)(result2 != IntrinsicsKt.getCOROUTINE_SUSPENDED());
                function0 = l$0;
                DebugProbesKt.probeCoroutineSuspended((Continuation)produceStateScopeImpl$awaitDispose$1);
                Label_0190:
                iftrue(Label_0200:)(result2 != coroutine_SUSPENDED);
                return coroutine_SUSPENDED;
            }
            finally {
                function0.invoke();
            }
        }
    }
    
    @Override
    public T component1() {
        return this.$$delegate_0.component1();
    }
    
    @Override
    public Function1<T, Unit> component2() {
        return this.$$delegate_0.component2();
    }
    
    public CoroutineContext getCoroutineContext() {
        return this.coroutineContext;
    }
    
    @Override
    public T getValue() {
        return this.$$delegate_0.getValue();
    }
    
    @Override
    public void setValue(final T value) {
        this.$$delegate_0.setValue(value);
    }
}

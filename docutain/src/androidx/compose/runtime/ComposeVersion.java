// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/compose/runtime/ComposeVersion;", "", "()V", "version", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposeVersion
{
    public static final ComposeVersion INSTANCE;
    public static final int version = 9705;
    
    static {
        INSTANCE = new ComposeVersion();
    }
    
    private ComposeVersion() {
    }
}

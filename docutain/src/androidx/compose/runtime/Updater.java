// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmInline;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0087@\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0012\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006J\u001a\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001¢\u0006\u0004\b\u0010\u0010\u0011J&\u0010\u0012\u001a\u00020\u00132\u0017\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00130\u0015¢\u0006\u0002\b\u0016¢\u0006\u0004\b\u0017\u0010\u0018J&\u0010\u0019\u001a\u00020\u00132\u0017\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00130\u0015¢\u0006\u0002\b\u0016¢\u0006\u0004\b\u001a\u0010\u0018JI\u0010\u001b\u001a\u00020\u0013\"\u0004\b\u0001\u0010\u001c2\u0006\u0010\u001d\u001a\u0002H\u001c2,\u0010\u0014\u001a(\u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u0011H\u001c¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u001d\u0012\u0004\u0012\u00020\u00130\u001e¢\u0006\u0002\b\u0016¢\u0006\u0004\b!\u0010\"JK\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u000f2.\b\b\u0010\u0014\u001a(\u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u00110\u000f¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u001d\u0012\u0004\u0012\u00020\u00130\u001e¢\u0006\u0002\b\u0016H\u0086\b\u00f8\u0001\u0001¢\u0006\u0004\b!\u0010#J\u0010\u0010$\u001a\u00020%H\u00d6\u0001¢\u0006\u0004\b&\u0010'JI\u0010(\u001a\u00020\u0013\"\u0004\b\u0001\u0010\u001c2\u0006\u0010\u001d\u001a\u0002H\u001c2,\u0010\u0014\u001a(\u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u0011H\u001c¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u001d\u0012\u0004\u0012\u00020\u00130\u001e¢\u0006\u0002\b\u0016¢\u0006\u0004\b)\u0010\"JK\u0010(\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u000f2.\b\b\u0010\u0014\u001a(\u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u00110\u000f¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u001d\u0012\u0004\u0012\u00020\u00130\u001e¢\u0006\u0002\b\u0016H\u0086\b\u00f8\u0001\u0001¢\u0006\u0004\b)\u0010#R\u0016\u0010\u0003\u001a\u00020\u00048\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\b\u0088\u0001\u0003\u0092\u0001\u00020\u0004\u00f8\u0001\u0000\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u009920\u0001¨\u0006*" }, d2 = { "Landroidx/compose/runtime/Updater;", "T", "", "composer", "Landroidx/compose/runtime/Composer;", "constructor-impl", "(Landroidx/compose/runtime/Composer;)Landroidx/compose/runtime/Composer;", "getComposer$annotations", "()V", "equals", "", "other", "equals-impl", "(Landroidx/compose/runtime/Composer;Ljava/lang/Object;)Z", "hashCode", "", "hashCode-impl", "(Landroidx/compose/runtime/Composer;)I", "init", "", "block", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "init-impl", "(Landroidx/compose/runtime/Composer;Lkotlin/jvm/functions/Function1;)V", "reconcile", "reconcile-impl", "set", "V", "value", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "set-impl", "(Landroidx/compose/runtime/Composer;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "(Landroidx/compose/runtime/Composer;ILkotlin/jvm/functions/Function2;)V", "toString", "", "toString-impl", "(Landroidx/compose/runtime/Composer;)Ljava/lang/String;", "update", "update-impl", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
@JvmInline
public final class Updater<T>
{
    private final Composer composer = composer;
    
    public static <T> Composer constructor-impl(final Composer composer) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        return composer;
    }
    
    public static boolean equals-impl(final Composer composer, final Object o) {
        return o instanceof Updater && Intrinsics.areEqual((Object)composer, (Object)((Updater)o).unbox-impl());
    }
    
    public static final boolean equals-impl0(final Composer composer, final Composer composer2) {
        return Intrinsics.areEqual((Object)composer, (Object)composer2);
    }
    
    public static int hashCode-impl(final Composer composer) {
        return composer.hashCode();
    }
    
    public static final void init-impl(final Composer composer, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        if (composer.getInserting()) {
            composer.apply(Unit.INSTANCE, (kotlin.jvm.functions.Function2<? super Object, ? super Unit, Unit>)new Updater$init.Updater$init$1((Function1)function1));
        }
    }
    
    public static final void reconcile-impl(final Composer composer, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        composer.apply(Unit.INSTANCE, (kotlin.jvm.functions.Function2<? super Object, ? super Unit, Unit>)new Updater$reconcile.Updater$reconcile$1((Function1)function1));
    }
    
    public static final void set-impl(final Composer composer, final int i, final Function2<? super T, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        if (composer.getInserting() || !Intrinsics.areEqual(composer.rememberedValue(), (Object)i)) {
            composer.updateRememberedValue(i);
            composer.apply(i, (kotlin.jvm.functions.Function2<? super Object, ? super Integer, Unit>)function2);
        }
    }
    
    public static final <V> void set-impl(final Composer composer, final V v, final Function2<? super T, ? super V, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        if (composer.getInserting() || !Intrinsics.areEqual(composer.rememberedValue(), (Object)v)) {
            composer.updateRememberedValue(v);
            composer.apply(v, (kotlin.jvm.functions.Function2<? super Object, ? super V, Unit>)function2);
        }
    }
    
    public static String toString-impl(final Composer obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Updater(composer=");
        sb.append(obj);
        sb.append(')');
        return sb.toString();
    }
    
    public static final void update-impl(final Composer composer, final int i, final Function2<? super T, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final boolean inserting = composer.getInserting();
        if (inserting || !Intrinsics.areEqual(composer.rememberedValue(), (Object)i)) {
            composer.updateRememberedValue(i);
            if (!inserting) {
                composer.apply(i, (kotlin.jvm.functions.Function2<? super Object, ? super Integer, Unit>)function2);
            }
        }
    }
    
    public static final <V> void update-impl(final Composer composer, final V v, final Function2<? super T, ? super V, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final boolean inserting = composer.getInserting();
        if (inserting || !Intrinsics.areEqual(composer.rememberedValue(), (Object)v)) {
            composer.updateRememberedValue(v);
            if (!inserting) {
                composer.apply(v, (kotlin.jvm.functions.Function2<? super Object, ? super V, Unit>)function2);
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.composer, o);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.composer);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.composer);
    }
    
    public final /* synthetic */ Composer unbox-impl() {
        return this.composer;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlinx.coroutines.CompletableJob;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.JobKt;
import kotlinx.coroutines.Job;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.jvm.functions.Function0;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import java.util.Arrays;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000d\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a&\u0010\u0005\u001a\u00020\u00062\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\nH\u0007¢\u0006\u0002\u0010\u000b\u001a0\u0010\u0005\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\nH\u0007¢\u0006\u0002\u0010\u000e\u001a:\u0010\u0005\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\nH\u0007¢\u0006\u0002\u0010\u0010\u001aD\u0010\u0005\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2\b\u0010\u0011\u001a\u0004\u0018\u00010\r2\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\nH\u0007¢\u0006\u0002\u0010\u0012\u001a>\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0013\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\r0\u0014\"\u0004\u0018\u00010\r2\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\nH\u0007¢\u0006\u0002\u0010\u0015\u001aW\u0010\u0016\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2\b\u0010\u0011\u001a\u0004\u0018\u00010\r2'\u0010\u0017\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u001a\u0012\u0006\u0012\u0004\u0018\u00010\r0\u0018¢\u0006\u0002\b\nH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001b\u001aM\u0010\u0016\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2'\u0010\u0017\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u001a\u0012\u0006\u0012\u0004\u0018\u00010\r0\u0018¢\u0006\u0002\b\nH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001c\u001aC\u0010\u0016\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\r2'\u0010\u0017\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u001a\u0012\u0006\u0012\u0004\u0018\u00010\r0\u0018¢\u0006\u0002\b\nH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001d\u001aQ\u0010\u0016\u001a\u00020\u00062\u0016\u0010\u0013\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\r0\u0014\"\u0004\u0018\u00010\r2'\u0010\u0017\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u001a\u0012\u0006\u0012\u0004\u0018\u00010\r0\u0018¢\u0006\u0002\b\nH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001e\u001a9\u0010\u0016\u001a\u00020\u00062'\u0010\u0017\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u001a\u0012\u0006\u0012\u0004\u0018\u00010\r0\u0018¢\u0006\u0002\b\nH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001f\u001a\u001b\u0010 \u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060!H\u0007¢\u0006\u0002\u0010\"\u001a\u0018\u0010#\u001a\u00020\u00192\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0001\u001a&\u0010(\u001a\u00020\u00192\u0013\b\u0006\u0010)\u001a\r\u0012\u0004\u0012\u00020%0!¢\u0006\u0002\b*H\u0087\b\u00f8\u0001\u0001¢\u0006\u0002\u0010+\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u009920\u0001¨\u0006," }, d2 = { "DisposableEffectNoParamError", "", "InternalDisposableEffectScope", "Landroidx/compose/runtime/DisposableEffectScope;", "LaunchedEffectNoParamError", "DisposableEffect", "", "effect", "Lkotlin/Function1;", "Landroidx/compose/runtime/DisposableEffectResult;", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;I)V", "key1", "", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;I)V", "key2", "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;I)V", "key3", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;I)V", "keys", "", "([Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/Composer;I)V", "LaunchedEffect", "block", "Lkotlin/Function2;", "Lkotlinx/coroutines/CoroutineScope;", "Lkotlin/coroutines/Continuation;", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "([Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "(Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "SideEffect", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;Landroidx/compose/runtime/Composer;I)V", "createCompositionCoroutineScope", "coroutineContext", "Lkotlin/coroutines/CoroutineContext;", "composer", "Landroidx/compose/runtime/Composer;", "rememberCoroutineScope", "getContext", "Landroidx/compose/runtime/DisallowComposableCalls;", "(Lkotlin/jvm/functions/Function0;Landroidx/compose/runtime/Composer;II)Lkotlinx/coroutines/CoroutineScope;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class EffectsKt
{
    private static final String DisposableEffectNoParamError = "DisposableEffect must provide one or more 'key' parameters that define the identity of the DisposableEffect and determine when its previous effect should be disposed and a new effect started for the new key.";
    private static final DisposableEffectScope InternalDisposableEffectScope;
    private static final String LaunchedEffectNoParamError = "LaunchedEffect must provide one or more 'key' parameters that define the identity of the LaunchedEffect and determine when its previous effect coroutine should be cancelled and a new effect launched for the new key.";
    
    static {
        InternalDisposableEffectScope = new DisposableEffectScope();
    }
    
    public static final void DisposableEffect(Object rememberedValue, final Object o, final Object o2, final Function1<? super DisposableEffectScope, ? extends DisposableEffectResult> function1, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function1, "effect");
        composer.startReplaceableGroup(-1239538271);
        ComposerKt.sourceInformation(composer, "C(DisposableEffect)P(1,2,3)235@9981L59:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1239538271, n, -1, "androidx.compose.runtime.DisposableEffect (Effects.kt:229)");
        }
        composer.startReplaceableGroup(1618982084);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1,2,3):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        final boolean changed2 = composer.changed(o);
        final boolean changed3 = composer.changed(o2);
        rememberedValue = composer.rememberedValue();
        if ((changed | changed2 | changed3) || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new DisposableEffectImpl(function1));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void DisposableEffect(Object rememberedValue, final Object o, final Function1<? super DisposableEffectScope, ? extends DisposableEffectResult> function1, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function1, "effect");
        composer.startReplaceableGroup(1429097729);
        ComposerKt.sourceInformation(composer, "C(DisposableEffect)P(1,2)194@8057L53:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(1429097729, n, -1, "androidx.compose.runtime.DisposableEffect (Effects.kt:189)");
        }
        composer.startReplaceableGroup(511388516);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1,2):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        final boolean changed2 = composer.changed(o);
        rememberedValue = composer.rememberedValue();
        if ((changed | changed2) || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new DisposableEffectImpl(function1));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void DisposableEffect(Object rememberedValue, final Function1<? super DisposableEffectScope, ? extends DisposableEffectResult> function1, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function1, "effect");
        composer.startReplaceableGroup(-1371986847);
        ComposerKt.sourceInformation(composer, "C(DisposableEffect)P(1)154@6171L47:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1371986847, n, -1, "androidx.compose.runtime.DisposableEffect (Effects.kt:150)");
        }
        composer.startReplaceableGroup(1157296644);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        rememberedValue = composer.rememberedValue();
        if (changed || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new DisposableEffectImpl(function1));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "DisposableEffect must provide one or more 'key' parameters that define the identity of the DisposableEffect and determine when its previous effect should be disposed and a new effect started for the new key.")
    public static final void DisposableEffect(final Function1<? super DisposableEffectScope, ? extends DisposableEffectResult> function1, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function1, "effect");
        composer.startReplaceableGroup(-904483903);
        ComposerKt.sourceInformation(composer, "C(DisposableEffect):Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-904483903, n, -1, "androidx.compose.runtime.DisposableEffect (Effects.kt:115)");
        }
        throw new IllegalStateException("DisposableEffect must provide one or more 'key' parameters that define the identity of the DisposableEffect and determine when its previous effect should be disposed and a new effect started for the new key.".toString());
    }
    
    public static final void DisposableEffect(Object[] copy, final Function1<? super DisposableEffectScope, ? extends DisposableEffectResult> function1, final Composer composer, int i) {
        Intrinsics.checkNotNullParameter((Object)copy, "keys");
        Intrinsics.checkNotNullParameter((Object)function1, "effect");
        composer.startReplaceableGroup(-1307627122);
        ComposerKt.sourceInformation(composer, "C(DisposableEffect)P(1)275@11877L48:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1307627122, i, -1, "androidx.compose.runtime.DisposableEffect (Effects.kt:271)");
        }
        copy = Arrays.copyOf(copy, copy.length);
        composer.startReplaceableGroup(-568225417);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1):Composables.kt#9igjgp");
        final int length = copy.length;
        i = 0;
        boolean b = false;
        while (i < length) {
            b |= composer.changed(copy[i]);
            ++i;
        }
        final Object rememberedValue = composer.rememberedValue();
        if (b || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new DisposableEffectImpl(function1));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void LaunchedEffect(Object rememberedValue, final Object o, final Object o2, final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        composer.startReplaceableGroup(-54093371);
        ComposerKt.sourceInformation(composer, "C(LaunchedEffect)P(1,2,3)383@16147L70:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-54093371, n, -1, "androidx.compose.runtime.LaunchedEffect (Effects.kt:376)");
        }
        final CoroutineContext applyCoroutineContext = composer.getApplyCoroutineContext();
        composer.startReplaceableGroup(1618982084);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1,2,3):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        final boolean changed2 = composer.changed(o);
        final boolean changed3 = composer.changed(o2);
        rememberedValue = composer.rememberedValue();
        if ((changed | changed2 | changed3) || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new LaunchedEffectImpl(applyCoroutineContext, function2));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void LaunchedEffect(Object rememberedValue, final Object o, final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        composer.startReplaceableGroup(590241125);
        ComposerKt.sourceInformation(composer, "C(LaunchedEffect)P(1,2)359@15109L64:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(590241125, n, -1, "androidx.compose.runtime.LaunchedEffect (Effects.kt:353)");
        }
        final CoroutineContext applyCoroutineContext = composer.getApplyCoroutineContext();
        composer.startReplaceableGroup(511388516);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1,2):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        final boolean changed2 = composer.changed(o);
        rememberedValue = composer.rememberedValue();
        if ((changed | changed2) || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new LaunchedEffectImpl(applyCoroutineContext, function2));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void LaunchedEffect(Object rememberedValue, final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        composer.startReplaceableGroup(1179185413);
        ComposerKt.sourceInformation(composer, "C(LaunchedEffect)P(1)336@14101L58:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(1179185413, n, -1, "androidx.compose.runtime.LaunchedEffect (Effects.kt:331)");
        }
        final CoroutineContext applyCoroutineContext = composer.getApplyCoroutineContext();
        composer.startReplaceableGroup(1157296644);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1):Composables.kt#9igjgp");
        final boolean changed = composer.changed(rememberedValue);
        rememberedValue = composer.rememberedValue();
        if (changed || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new LaunchedEffectImpl(applyCoroutineContext, function2));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "LaunchedEffect must provide one or more 'key' parameters that define the identity of the LaunchedEffect and determine when its previous effect coroutine should be cancelled and a new effect launched for the new key.")
    public static final void LaunchedEffect(final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        startRestartGroup = startRestartGroup.startRestartGroup(-805415771);
        ComposerKt.sourceInformation(startRestartGroup, "C(LaunchedEffect):Effects.kt#9igjgp");
        if ((n & 0x1) == 0x0 && startRestartGroup.getSkipping()) {
            startRestartGroup.skipToGroupEnd();
            final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
            if (endRestartGroup != null) {
                endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new EffectsKt$LaunchedEffect.EffectsKt$LaunchedEffect$1((Function2)function2, n));
            }
            return;
        }
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-805415771, n, -1, "androidx.compose.runtime.LaunchedEffect (Effects.kt:313)");
        }
        throw new IllegalStateException("LaunchedEffect must provide one or more 'key' parameters that define the identity of the LaunchedEffect and determine when its previous effect coroutine should be cancelled and a new effect launched for the new key.".toString());
    }
    
    public static final void LaunchedEffect(Object[] copy, final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, final Composer composer, int n) {
        Intrinsics.checkNotNullParameter((Object)copy, "keys");
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        composer.startReplaceableGroup(-139560008);
        ComposerKt.sourceInformation(composer, "C(LaunchedEffect)P(1)406@17175L59:Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-139560008, n, -1, "androidx.compose.runtime.LaunchedEffect (Effects.kt:401)");
        }
        final CoroutineContext applyCoroutineContext = composer.getApplyCoroutineContext();
        copy = Arrays.copyOf(copy, copy.length);
        composer.startReplaceableGroup(-568225417);
        ComposerKt.sourceInformation(composer, "CC(remember)P(1):Composables.kt#9igjgp");
        final int length = copy.length;
        int i = 0;
        n = 0;
        while (i < length) {
            n |= (composer.changed(copy[i]) ? 1 : 0);
            ++i;
        }
        final Object rememberedValue = composer.rememberedValue();
        if (n != 0 || rememberedValue == Composer.Companion.getEmpty()) {
            composer.updateRememberedValue(new LaunchedEffectImpl(applyCoroutineContext, function2));
        }
        composer.endReplaceableGroup();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final void SideEffect(final Function0<Unit> function0, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)function0, "effect");
        composer.startReplaceableGroup(-1288466761);
        ComposerKt.sourceInformation(composer, "C(SideEffect):Effects.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1288466761, n, -1, "androidx.compose.runtime.SideEffect (Effects.kt:44)");
        }
        composer.recordSideEffect(function0);
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
    }
    
    public static final CoroutineScope createCompositionCoroutineScope(final CoroutineContext coroutineContext, final Composer composer) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "coroutineContext");
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        CoroutineScope coroutineScope;
        if (coroutineContext.get((CoroutineContext$Key)Job.Key) != null) {
            final CompletableJob job$default = JobKt.Job$default((Job)null, 1, (Object)null);
            job$default.completeExceptionally((Throwable)new IllegalArgumentException("CoroutineContext supplied to rememberCoroutineScope may not include a parent job"));
            coroutineScope = CoroutineScopeKt.CoroutineScope((CoroutineContext)job$default);
        }
        else {
            final CoroutineContext applyCoroutineContext = composer.getApplyCoroutineContext();
            coroutineScope = CoroutineScopeKt.CoroutineScope(applyCoroutineContext.plus((CoroutineContext)JobKt.Job((Job)applyCoroutineContext.get((CoroutineContext$Key)Job.Key))).plus(coroutineContext));
        }
        return coroutineScope;
    }
    
    public static final CoroutineScope rememberCoroutineScope(Function0<? extends CoroutineContext> function0, final Composer composer, final int n, final int n2) {
        composer.startReplaceableGroup(773894976);
        ComposerKt.sourceInformation(composer, "CC(rememberCoroutineScope)476@19869L144:Effects.kt#9igjgp");
        if ((n2 & 0x1) != 0x0) {
            function0 = (Function0)EffectsKt$rememberCoroutineScope.EffectsKt$rememberCoroutineScope$1.INSTANCE;
        }
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object rememberedValue;
        if ((rememberedValue = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            rememberedValue = new CompositionScopedCoroutineScopeCanceller(createCompositionCoroutineScope((CoroutineContext)function0.invoke(), composer));
            composer.updateRememberedValue(rememberedValue);
        }
        composer.endReplaceableGroup();
        final CoroutineScope coroutineScope = ((CompositionScopedCoroutineScopeCanceller)rememberedValue).getCoroutineScope();
        composer.endReplaceableGroup();
        return coroutineScope;
    }
}

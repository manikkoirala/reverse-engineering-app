// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.ReplaceWith;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.KotlinNothingValueException;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import androidx.compose.runtime.collection.IdentityArraySet;
import java.util.Map;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import java.util.LinkedHashSet;
import java.util.HashMap;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u00e4\u0001\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b#\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\b\u0010\u001a\u0010\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020BH\u0000\u001ai\u0010C\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`G2\u0012\u0010H\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030J0I2&\u0010K\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`GH\u0003¢\u0006\u0002\u0010L\u001a(\u0010M\u001a\u0004\u0018\u00010\u00012\b\u0010N\u001a\u0004\u0018\u00010\u00012\b\u0010O\u001a\u0004\u0018\u00010\u00012\b\u0010P\u001a\u0004\u0018\u00010\u0001H\u0002\u001a\b\u0010Q\u001a\u00020RH\u0007\u001aP\u0010S\u001a>\u0012\u0004\u0012\u0002HU\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002HW0Vj\b\u0012\u0004\u0012\u0002HW`X0Tj\u001e\u0012\u0004\u0012\u0002HU\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002HW0Vj\b\u0012\u0004\u0012\u0002HW`X`Y\"\u0004\b\u0000\u0010U\"\u0004\b\u0001\u0010WH\u0002\u001a\u0010\u0010Z\u001a\u00020\u00172\u0006\u0010N\u001a\u00020RH\u0000\u001a\"\u0010Z\u001a\u00020\u00172\u0006\u0010N\u001a\u00020R2\f\u0010[\u001a\b\u0012\u0004\u0012\u00020\u00010\\H\u0080\b\u00f8\u0001\u0000\u001a\u0018\u0010]\u001a\u00020\u00172\u0006\u0010^\u001a\u00020_2\u0006\u0010]\u001a\u00020BH\u0007\u001a\u0010\u0010`\u001a\u00020\u00172\u0006\u0010^\u001a\u00020_H\u0007\u001a \u0010a\u001a\u00020\u00172\u0006\u0010^\u001a\u00020_2\u0006\u0010b\u001a\u00020\u00072\u0006\u0010]\u001a\u00020BH\u0007\u001a\b\u0010c\u001a\u00020\u0017H\u0007\u001a(\u0010d\u001a\u00020\u00172\u0006\u0010b\u001a\u00020\u00072\u0006\u0010e\u001a\u00020\u00072\u0006\u0010f\u001a\u00020\u00072\u0006\u0010g\u001a\u00020BH\u0007\u001a\u0018\u0010d\u001a\u00020\u00172\u0006\u0010b\u001a\u00020\u00072\u0006\u0010g\u001a\u00020BH\u0007\u001a\f\u0010h\u001a\u00020R*\u00020\u0007H\u0002\u001a\f\u0010i\u001a\u00020\u0007*\u00020RH\u0002\u001a6\u0010j\u001a\u0002Hk\"\u0004\b\u0000\u0010k*\u00020_2\u0006\u0010l\u001a\u00020R2\u0011\u0010m\u001a\r\u0012\u0004\u0012\u0002Hk0\\¢\u0006\u0002\bnH\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010o\u001a\u001c\u0010p\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010q*\u00020r2\u0006\u0010s\u001a\u00020tH\u0002\u001a@\u0010u\u001a\u00020R\"\u0004\b\u0000\u0010k*\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`G2\f\u0010b\u001a\b\u0012\u0004\u0012\u0002Hk0EH\u0000\u001a\u001c\u0010v\u001a\u00020\u0007*\u00020w2\u0006\u0010x\u001a\u00020\u00072\u0006\u0010y\u001a\u00020\u0007H\u0002\u001a(\u0010z\u001a\b\u0012\u0004\u0012\u00020|0{*\b\u0012\u0004\u0012\u00020|0{2\u0006\u0010}\u001a\u00020\u00072\u0006\u0010~\u001a\u00020\u0007H\u0002\u001a\u001b\u0010\u007f\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020|0{2\u0007\u0010\u0080\u0001\u001a\u00020\u0007H\u0002\u001a\u001c\u0010\u0081\u0001\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020|0{2\u0007\u0010\u0080\u0001\u001a\u00020\u0007H\u0002\u001a%\u0010\u0082\u0001\u001a\u0004\u0018\u00010|*\b\u0012\u0004\u0012\u00020|0{2\u0006\u0010}\u001a\u00020\u00072\u0006\u0010~\u001a\u00020\u0007H\u0002\u001aG\u0010\u0083\u0001\u001a\u0002Hk\"\u0004\b\u0000\u0010k*\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`G2\f\u0010b\u001a\b\u0012\u0004\u0012\u0002Hk0EH\u0000¢\u0006\u0003\u0010\u0084\u0001\u001a1\u0010\u0085\u0001\u001a\u00020\u0017*\b\u0012\u0004\u0012\u00020|0{2\u0007\u0010\u0080\u0001\u001a\u00020\u00072\b\u0010\u0086\u0001\u001a\u00030\u0087\u00012\t\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u0001H\u0002\u001a\u0084\u0001\u0010\u0089\u0001\u001a\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`G*\"\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0Dj\u0002`G21\u0010\u008a\u0001\u001a,\u0012!\u0012\u001f\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0\u008c\u0001\u0012\u0004\u0012\u00020\u00170\u008b\u0001H\u0080\b\u00f8\u0001\u0000\u001a(\u0010\u008d\u0001\u001a\u00020\u0007*\u00020w2\u0007\u0010\u008e\u0001\u001a\u00020\u00072\u0007\u0010\u008f\u0001\u001a\u00020\u00072\u0007\u0010\u0090\u0001\u001a\u00020\u0007H\u0002\u001a[\u0010\u0091\u0001\u001a\u0004\u0018\u0001HW\"\u0004\b\u0000\u0010U\"\u0004\b\u0001\u0010W*4\u0012\u0004\u0012\u0002HU\u0012\n\u0012\b\u0012\u0004\u0012\u0002HW0V0Tj\u001e\u0012\u0004\u0012\u0002HU\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002HW0Vj\b\u0012\u0004\u0012\u0002HW`X`Y2\u0006\u0010b\u001a\u0002HUH\u0002¢\u0006\u0003\u0010\u0092\u0001\u001aa\u0010\u0093\u0001\u001a\u00020R\"\u0004\b\u0000\u0010U\"\u0004\b\u0001\u0010W*4\u0012\u0004\u0012\u0002HU\u0012\n\u0012\b\u0012\u0004\u0012\u0002HW0V0Tj\u001e\u0012\u0004\u0012\u0002HU\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002HW0Vj\b\u0012\u0004\u0012\u0002HW`X`Y2\u0006\u0010b\u001a\u0002HU2\u0006\u0010N\u001a\u0002HWH\u0002¢\u0006\u0003\u0010\u0094\u0001\u001ac\u0010\u0095\u0001\u001a\u0004\u0018\u00010\u0017\"\u0004\b\u0000\u0010U\"\u0004\b\u0001\u0010W*4\u0012\u0004\u0012\u0002HU\u0012\n\u0012\b\u0012\u0004\u0012\u0002HW0V0Tj\u001e\u0012\u0004\u0012\u0002HU\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002HW0Vj\b\u0012\u0004\u0012\u0002HW`X`Y2\u0006\u0010b\u001a\u0002HU2\u0006\u0010N\u001a\u0002HWH\u0002¢\u0006\u0003\u0010\u0096\u0001\u001a\u0015\u0010\u0097\u0001\u001a\u00020\u0017*\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0015H\u0000\u001a\u001e\u0010\u0098\u0001\u001a\u0004\u0018\u00010|*\b\u0012\u0004\u0012\u00020|0{2\u0007\u0010\u0080\u0001\u001a\u00020\u0007H\u0002\u001a#\u0010\u0099\u0001\u001a\u00020\u0017*\b\u0012\u0004\u0012\u00020|0{2\u0006\u0010}\u001a\u00020\u00072\u0006\u0010~\u001a\u00020\u0007H\u0002\"\u001c\u0010\u0000\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0002\u0010\u0003\u001a\u0004\b\u0004\u0010\u0005\"\u0016\u0010\u0006\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\b\u0010\u0003\"\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0003\"\u000e\u0010\f\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000\"[\u0010\r\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000ej\u0002`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000\"\u001c\u0010\u001a\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u001b\u0010\u0003\u001a\u0004\b\u001c\u0010\u0005\"\u0016\u0010\u001d\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u001e\u0010\u0003\"\u000e\u0010\u001f\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000\"\u001c\u0010 \u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b!\u0010\u0003\u001a\u0004\b\"\u0010\u0005\"\u0016\u0010#\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b$\u0010\u0003\"\u001c\u0010%\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b&\u0010\u0003\u001a\u0004\b'\u0010\u0005\"\u0016\u0010(\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b)\u0010\u0003\"\u001c\u0010*\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b+\u0010\u0003\u001a\u0004\b,\u0010\u0005\"\u0016\u0010-\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b.\u0010\u0003\"\u001c\u0010/\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b0\u0010\u0003\u001a\u0004\b1\u0010\u0005\"\u0016\u00102\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b3\u0010\u0003\"[\u00104\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000ej\u0002`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000\"[\u00105\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000ej\u0002`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0016\u00106\u001a\u00020\u00078\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b7\u0010\u0003\"\u000e\u00108\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000\"[\u00109\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000ej\u0002`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000\"[\u0010:\u001aO\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000ej\u0002`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0018\u0010;\u001a\u00020\u0001*\u00020<8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b=\u0010>*\u009f\u0001\b\u0000\u0010\u009a\u0001\"K\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000e2K\u0012\u0017\u0012\u0015\u0012\u0002\b\u00030\u000f¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00170\u000e*E\b\u0000\u0010\u009b\u0001\"\u001e\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0D2\u001e\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010E\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010F0D\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u009c\u0001" }, d2 = { "compositionLocalMap", "", "getCompositionLocalMap$annotations", "()V", "getCompositionLocalMap", "()Ljava/lang/Object;", "compositionLocalMapKey", "", "getCompositionLocalMapKey$annotations", "compositionTracer", "Landroidx/compose/runtime/CompositionTracer;", "getCompositionTracer$annotations", "defaultsKey", "endGroupInstance", "Lkotlin/Function3;", "Landroidx/compose/runtime/Applier;", "Lkotlin/ParameterName;", "name", "applier", "Landroidx/compose/runtime/SlotWriter;", "slots", "Landroidx/compose/runtime/RememberManager;", "rememberManager", "", "Landroidx/compose/runtime/Change;", "invalidGroupLocation", "invocation", "getInvocation$annotations", "getInvocation", "invocationKey", "getInvocationKey$annotations", "nodeKey", "provider", "getProvider$annotations", "getProvider", "providerKey", "getProviderKey$annotations", "providerMaps", "getProviderMaps$annotations", "getProviderMaps", "providerMapsKey", "getProviderMapsKey$annotations", "providerValues", "getProviderValues$annotations", "getProviderValues", "providerValuesKey", "getProviderValuesKey$annotations", "reference", "getReference$annotations", "getReference", "referenceKey", "getReferenceKey$annotations", "removeCurrentGroupInstance", "resetSlotsInstance", "reuseKey", "getReuseKey$annotations", "rootKey", "skipToGroupEndInstance", "startRootGroup", "joinedKey", "Landroidx/compose/runtime/KeyInfo;", "getJoinedKey", "(Landroidx/compose/runtime/KeyInfo;)Ljava/lang/Object;", "composeRuntimeError", "", "message", "", "compositionLocalMapOf", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "Landroidx/compose/runtime/CompositionLocal;", "Landroidx/compose/runtime/State;", "Landroidx/compose/runtime/CompositionLocalMap;", "values", "", "Landroidx/compose/runtime/ProvidedValue;", "parentScope", "([Landroidx/compose/runtime/ProvidedValue;Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "getKey", "value", "left", "right", "isTraceInProgress", "", "multiMap", "Ljava/util/HashMap;", "K", "Ljava/util/LinkedHashSet;", "V", "Lkotlin/collections/LinkedHashSet;", "Lkotlin/collections/HashMap;", "runtimeCheck", "lazyMessage", "Lkotlin/Function0;", "sourceInformation", "composer", "Landroidx/compose/runtime/Composer;", "sourceInformationMarkerEnd", "sourceInformationMarkerStart", "key", "traceEventEnd", "traceEventStart", "dirty1", "dirty2", "info", "asBool", "asInt", "cache", "T", "invalid", "block", "Landroidx/compose/runtime/DisallowComposableCalls;", "(Landroidx/compose/runtime/Composer;ZLkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "collectNodesFrom", "", "Landroidx/compose/runtime/SlotTable;", "anchor", "Landroidx/compose/runtime/Anchor;", "contains", "distanceFrom", "Landroidx/compose/runtime/SlotReader;", "index", "root", "filterToRange", "", "Landroidx/compose/runtime/Invalidation;", "start", "end", "findInsertLocation", "location", "findLocation", "firstInRange", "getValueOf", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;Landroidx/compose/runtime/CompositionLocal;)Ljava/lang/Object;", "insertIfMissing", "scope", "Landroidx/compose/runtime/RecomposeScopeImpl;", "instance", "mutate", "mutator", "Lkotlin/Function1;", "", "nearestCommonRootOf", "a", "b", "common", "pop", "(Ljava/util/HashMap;Ljava/lang/Object;)Ljava/lang/Object;", "put", "(Ljava/util/HashMap;Ljava/lang/Object;Ljava/lang/Object;)Z", "remove", "(Ljava/util/HashMap;Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Unit;", "removeCurrentGroup", "removeLocation", "removeRange", "Change", "CompositionLocalMap", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ComposerKt
{
    private static final Object compositionLocalMap;
    public static final int compositionLocalMapKey = 202;
    private static CompositionTracer compositionTracer;
    private static final int defaultsKey = -127;
    private static final Function3<Applier<?>, SlotWriter, RememberManager, Unit> endGroupInstance;
    private static final int invalidGroupLocation = -2;
    private static final Object invocation;
    public static final int invocationKey = 200;
    private static final int nodeKey = 125;
    private static final Object provider;
    public static final int providerKey = 201;
    private static final Object providerMaps;
    public static final int providerMapsKey = 204;
    private static final Object providerValues;
    public static final int providerValuesKey = 203;
    private static final Object reference;
    public static final int referenceKey = 206;
    private static final Function3<Applier<?>, SlotWriter, RememberManager, Unit> removeCurrentGroupInstance;
    private static final Function3<Applier<?>, SlotWriter, RememberManager, Unit> resetSlotsInstance;
    public static final int reuseKey = 207;
    private static final int rootKey = 100;
    private static final Function3<Applier<?>, SlotWriter, RememberManager, Unit> skipToGroupEndInstance;
    private static final Function3<Applier<?>, SlotWriter, RememberManager, Unit> startRootGroup;
    
    static {
        removeCurrentGroupInstance = (Function3)ComposerKt$removeCurrentGroupInstance.ComposerKt$removeCurrentGroupInstance$1.INSTANCE;
        skipToGroupEndInstance = (Function3)ComposerKt$skipToGroupEndInstance.ComposerKt$skipToGroupEndInstance$1.INSTANCE;
        endGroupInstance = (Function3)ComposerKt$endGroupInstance.ComposerKt$endGroupInstance$1.INSTANCE;
        startRootGroup = (Function3)ComposerKt$startRootGroup.ComposerKt$startRootGroup$1.INSTANCE;
        resetSlotsInstance = (Function3)ComposerKt$resetSlotsInstance.ComposerKt$resetSlotsInstance$1.INSTANCE;
        invocation = new OpaqueKey("provider");
        provider = new OpaqueKey("provider");
        compositionLocalMap = new OpaqueKey("compositionLocalMap");
        providerValues = new OpaqueKey("providerValues");
        providerMaps = new OpaqueKey("providers");
        reference = new OpaqueKey("reference");
    }
    
    private static final boolean asBool(final int n) {
        return n != 0;
    }
    
    private static final int asInt(final boolean b) {
        return b ? 1 : 0;
    }
    
    @ComposeCompilerApi
    public static final <T> T cache(final Composer composer, final boolean b, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)composer, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        final Object rememberedValue = composer.rememberedValue();
        Object invoke;
        if (b || (invoke = rememberedValue) == Composer.Companion.getEmpty()) {
            invoke = function0.invoke();
            composer.updateRememberedValue(invoke);
        }
        return (T)invoke;
    }
    
    private static final List<Object> collectNodesFrom(final SlotTable slotTable, final Anchor anchor) {
        final List list = new ArrayList();
        final SlotReader openReader = slotTable.openReader();
        try {
            collectNodesFrom$lambda$10$collectFromGroup(openReader, list, slotTable.anchorIndex(anchor));
            final Unit instance = Unit.INSTANCE;
            return list;
        }
        finally {
            openReader.close();
        }
    }
    
    private static final void collectNodesFrom$lambda$10$collectFromGroup(final SlotReader slotReader, final List<Object> list, final int n) {
        if (slotReader.isNode(n)) {
            list.add(slotReader.node(n));
        }
        else {
            for (int i = n + 1; i < n + slotReader.groupSize(n); i += slotReader.groupSize(i)) {
                collectNodesFrom$lambda$10$collectFromGroup(slotReader, list, i);
            }
        }
    }
    
    public static final Void composeRuntimeError(final String str) {
        Intrinsics.checkNotNullParameter((Object)str, "message");
        final StringBuilder sb = new StringBuilder();
        sb.append("Compose Runtime internal error. Unexpected or incorrect use of the Compose internal runtime API (");
        sb.append(str);
        sb.append("). Please report to Google or use https://goo.gle/compose-feedback");
        throw new ComposeRuntimeError(sb.toString());
    }
    
    private static final PersistentMap<CompositionLocal<Object>, State<Object>> compositionLocalMapOf(final ProvidedValue<?>[] array, final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final Composer composer, int i) {
        composer.startReplaceableGroup(721128344);
        sourceInformation(composer, "C(compositionLocalMapOf)P(1):Composer.kt#9igjgp");
        if (isTraceInProgress()) {
            traceEventStart(721128344, i, -1, "androidx.compose.runtime.compositionLocalMapOf (Composer.kt:319)");
        }
        final PersistentMap.Builder<Object, Object> builder = ExtensionsKt.persistentHashMapOf().builder();
        final Map map = builder;
        ProvidedValue<?> providedValue;
        CompositionLocal compositionLocal;
        for (i = 0; i < array.length; ++i) {
            providedValue = array[i];
            composer.startReplaceableGroup(680853375);
            sourceInformation(composer, "*329@11982L24");
            if (providedValue.getCanOverride() || !contains(persistentMap, providedValue.getCompositionLocal())) {
                compositionLocal = providedValue.getCompositionLocal();
                Intrinsics.checkNotNull((Object)compositionLocal, "null cannot be cast to non-null type androidx.compose.runtime.CompositionLocal<kotlin.Any?>");
                map.put(compositionLocal, providedValue.getCompositionLocal().provided$runtime_release(providedValue.getValue(), composer, 8));
            }
            composer.endReplaceableGroup();
        }
        final PersistentMap<Object, Object> build = builder.build();
        if (isTraceInProgress()) {
            traceEventEnd();
        }
        composer.endReplaceableGroup();
        return (PersistentMap<CompositionLocal<Object>, State<Object>>)build;
    }
    
    public static final <T> boolean contains(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final CompositionLocal<T> compositionLocal) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)compositionLocal, "key");
        return persistentMap.containsKey(compositionLocal);
    }
    
    private static final int distanceFrom(final SlotReader slotReader, int parent, final int n) {
        int n2;
        for (n2 = 0; parent > 0 && parent != n; parent = slotReader.parent(parent), ++n2) {}
        return n2;
    }
    
    private static final List<Invalidation> filterToRange(final List<Invalidation> list, int i, final int n) {
        final List list2 = new ArrayList();
        Invalidation invalidation;
        for (i = findInsertLocation(list, i); i < list.size(); ++i) {
            invalidation = list.get(i);
            if (invalidation.getLocation() >= n) {
                break;
            }
            list2.add(invalidation);
        }
        return list2;
    }
    
    private static final int findInsertLocation(final List<Invalidation> list, int location) {
        final int n = location = findLocation(list, location);
        if (n < 0) {
            location = -(n + 1);
        }
        return location;
    }
    
    private static final int findLocation(final List<Invalidation> list, final int n) {
        int n2 = list.size() - 1;
        int i = 0;
        while (i <= n2) {
            final int n3 = i + n2 >>> 1;
            final int compare = Intrinsics.compare(list.get(n3).getLocation(), n);
            if (compare < 0) {
                i = n3 + 1;
            }
            else {
                if (compare <= 0) {
                    return n3;
                }
                n2 = n3 - 1;
            }
        }
        return -(i + 1);
    }
    
    private static final Invalidation firstInRange(final List<Invalidation> list, int insertLocation, final int n) {
        insertLocation = findInsertLocation(list, insertLocation);
        if (insertLocation < list.size()) {
            final Invalidation invalidation = list.get(insertLocation);
            if (invalidation.getLocation() < n) {
                return invalidation;
            }
        }
        return null;
    }
    
    public static final Object getCompositionLocalMap() {
        return ComposerKt.compositionLocalMap;
    }
    
    public static final Object getInvocation() {
        return ComposerKt.invocation;
    }
    
    private static final Object getJoinedKey(final KeyInfo keyInfo) {
        Object value;
        if (keyInfo.getObjectKey() != null) {
            value = new JoinedKey(keyInfo.getKey(), keyInfo.getObjectKey());
        }
        else {
            value = keyInfo.getKey();
        }
        return value;
    }
    
    private static final Object getKey(Object o, final Object o2, final Object o3) {
        final boolean b = o instanceof JoinedKey;
        Object o4 = null;
        JoinedKey joinedKey;
        if (b) {
            joinedKey = (JoinedKey)o;
        }
        else {
            joinedKey = null;
        }
        if (joinedKey != null) {
            if (!Intrinsics.areEqual(joinedKey.getLeft(), o2) || !Intrinsics.areEqual(joinedKey.getRight(), o3)) {
                if ((o = getKey(joinedKey.getLeft(), o2, o3)) == null) {
                    o = getKey(joinedKey.getRight(), o2, o3);
                }
            }
            o4 = o;
        }
        return o4;
    }
    
    public static final Object getProvider() {
        return ComposerKt.provider;
    }
    
    public static final Object getProviderMaps() {
        return ComposerKt.providerMaps;
    }
    
    public static final Object getProviderValues() {
        return ComposerKt.providerValues;
    }
    
    public static final Object getReference() {
        return ComposerKt.reference;
    }
    
    public static final <T> T getValueOf(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final CompositionLocal<T> compositionLocal) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)compositionLocal, "key");
        final State state = persistentMap.get(compositionLocal);
        Object value;
        if (state != null) {
            value = state.getValue();
        }
        else {
            value = null;
        }
        return (T)value;
    }
    
    private static final void insertIfMissing(final List<Invalidation> list, final int n, final RecomposeScopeImpl recomposeScopeImpl, final Object o) {
        final int location = findLocation(list, n);
        IdentityArraySet<Object> set = null;
        if (location < 0) {
            final int n2 = -(location + 1);
            if (o != null) {
                set = new IdentityArraySet<Object>();
                set.add(o);
            }
            list.add(n2, new Invalidation(recomposeScopeImpl, n, set));
        }
        else if (o == null) {
            list.get(location).setInstances(null);
        }
        else {
            final IdentityArraySet<Object> instances = list.get(location).getInstances();
            if (instances != null) {
                instances.add(o);
            }
        }
    }
    
    @ComposeCompilerApi
    public static final boolean isTraceInProgress() {
        final CompositionTracer compositionTracer = ComposerKt.compositionTracer;
        return compositionTracer != null && compositionTracer.isTraceInProgress();
    }
    
    private static final <K, V> HashMap<K, LinkedHashSet<V>> multiMap() {
        return new HashMap<K, LinkedHashSet<V>>();
    }
    
    public static final PersistentMap<CompositionLocal<Object>, State<Object>> mutate(final PersistentMap<CompositionLocal<Object>, ? extends State<?>> persistentMap, final Function1<? super Map<CompositionLocal<Object>, State<Object>>, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)persistentMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "mutator");
        final PersistentMap.Builder<CompositionLocal<Object>, ? extends State<?>> builder = persistentMap.builder();
        function1.invoke((Object)builder);
        return (PersistentMap<CompositionLocal<Object>, State<Object>>)builder.build();
    }
    
    private static final int nearestCommonRootOf(final SlotReader slotReader, int parent, int parent2, int i) {
        if (parent == parent2) {
            return parent;
        }
        if (parent == i || parent2 == i) {
            return i;
        }
        if (slotReader.parent(parent) == parent2) {
            return parent2;
        }
        if (slotReader.parent(parent2) == parent) {
            return parent;
        }
        if (slotReader.parent(parent) == slotReader.parent(parent2)) {
            return slotReader.parent(parent);
        }
        final int distance = distanceFrom(slotReader, parent, i);
        final int distance2 = distanceFrom(slotReader, parent2, i);
        int n = 0;
        for (i = 0; i < distance - distance2; ++i) {
            parent = slotReader.parent(parent);
        }
        int j;
        while (true) {
            j = parent;
            i = parent2;
            if (n >= distance2 - distance) {
                break;
            }
            parent2 = slotReader.parent(parent2);
            ++n;
        }
        while (j != i) {
            j = slotReader.parent(j);
            i = slotReader.parent(i);
        }
        return j;
    }
    
    private static final <K, V> V pop(final HashMap<K, LinkedHashSet<V>> hashMap, final K key) {
        final LinkedHashSet set = hashMap.get(key);
        if (set != null) {
            final Object firstOrNull = CollectionsKt.firstOrNull((Iterable)set);
            if (firstOrNull != null) {
                remove(hashMap, key, (V)firstOrNull);
                return (V)firstOrNull;
            }
        }
        return null;
    }
    
    private static final <K, V> boolean put(final HashMap<K, LinkedHashSet<V>> hashMap, final K k, final V e) {
        final Map map = hashMap;
        Object value;
        if ((value = map.get(k)) == null) {
            value = new LinkedHashSet();
            map.put(k, value);
        }
        return ((LinkedHashSet)value).add(e);
    }
    
    private static final <K, V> Unit remove(final HashMap<K, LinkedHashSet<V>> hashMap, final K k, final V o) {
        final LinkedHashSet set = hashMap.get(k);
        Unit instance;
        if (set != null) {
            set.remove(o);
            if (set.isEmpty()) {
                hashMap.remove(k);
            }
            instance = Unit.INSTANCE;
        }
        else {
            instance = null;
        }
        return instance;
    }
    
    public static final void removeCurrentGroup(final SlotWriter slotWriter, final RememberManager rememberManager) {
        Intrinsics.checkNotNullParameter((Object)slotWriter, "<this>");
        Intrinsics.checkNotNullParameter((Object)rememberManager, "rememberManager");
        final Iterator<Object> groupSlots = slotWriter.groupSlots();
        while (groupSlots.hasNext()) {
            final RecomposeScopeImpl next = groupSlots.next();
            if (next instanceof ComposeNodeLifecycleCallback) {
                rememberManager.releasing((ComposeNodeLifecycleCallback)next);
            }
            if (next instanceof RememberObserver) {
                rememberManager.forgetting((RememberObserver)next);
            }
            if (next instanceof RecomposeScopeImpl) {
                final RecomposeScopeImpl recomposeScopeImpl = next;
                final CompositionImpl composition = recomposeScopeImpl.getComposition();
                if (composition == null) {
                    continue;
                }
                composition.setPendingInvalidScopes$runtime_release(true);
                recomposeScopeImpl.release();
            }
        }
        slotWriter.removeGroup();
    }
    
    private static final Invalidation removeLocation(final List<Invalidation> list, int location) {
        location = findLocation(list, location);
        Invalidation invalidation;
        if (location >= 0) {
            invalidation = list.remove(location);
        }
        else {
            invalidation = null;
        }
        return invalidation;
    }
    
    private static final void removeRange(final List<Invalidation> list, int insertLocation, final int n) {
        insertLocation = findInsertLocation(list, insertLocation);
        while (insertLocation < list.size() && list.get(insertLocation).getLocation() < n) {
            list.remove(insertLocation);
        }
    }
    
    public static final void runtimeCheck(final boolean b) {
        if (b) {
            return;
        }
        composeRuntimeError("Check failed".toString());
        throw new KotlinNothingValueException();
    }
    
    public static final void runtimeCheck(final boolean b, final Function0<?> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "lazyMessage");
        if (b) {
            return;
        }
        composeRuntimeError(function0.invoke().toString());
        throw new KotlinNothingValueException();
    }
    
    @ComposeCompilerApi
    public static final void sourceInformation(final Composer composer, final String s) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter((Object)s, "sourceInformation");
        composer.sourceInformation(s);
    }
    
    @ComposeCompilerApi
    public static final void sourceInformationMarkerEnd(final Composer composer) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        composer.sourceInformationMarkerEnd();
    }
    
    @ComposeCompilerApi
    public static final void sourceInformationMarkerStart(final Composer composer, final int n, final String s) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter((Object)s, "sourceInformation");
        composer.sourceInformationMarkerStart(n, s);
    }
    
    @ComposeCompilerApi
    public static final void traceEventEnd() {
        final CompositionTracer compositionTracer = ComposerKt.compositionTracer;
        if (compositionTracer != null) {
            compositionTracer.traceEventEnd();
        }
    }
    
    @ComposeCompilerApi
    public static final void traceEventStart(final int n, final int n2, final int n3, final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "info");
        final CompositionTracer compositionTracer = ComposerKt.compositionTracer;
        if (compositionTracer != null) {
            compositionTracer.traceEventStart(n, n2, n3, s);
        }
    }
}

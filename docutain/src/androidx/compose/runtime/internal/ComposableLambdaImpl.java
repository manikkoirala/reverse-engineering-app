// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.internal;

import kotlin.jvm.functions.Function21;
import kotlin.jvm.functions.Function20;
import kotlin.jvm.functions.Function19;
import kotlin.jvm.functions.Function18;
import kotlin.jvm.functions.Function17;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function15;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.functions.Function13;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.functions.Function9;
import kotlin.jvm.functions.Function8;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function3;
import androidx.compose.runtime.ScopeUpdateScope;
import kotlin.Unit;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayList;
import androidx.compose.runtime.Composer;
import java.util.List;
import androidx.compose.runtime.RecomposeScope;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0001\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001b\u0010\u000f\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002J%\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002J/\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002J9\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002JC\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002JM\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002JW\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002Ja\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002Jk\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002Ju\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0096\u0002J\u0087\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u0091\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u009b\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J¥\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J¯\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J¹\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\b2\b\u0010\"\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u00c3\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\b2\b\u0010\"\u001a\u0004\u0018\u00010\b2\b\u0010#\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u00cd\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\b2\b\u0010\"\u001a\u0004\u0018\u00010\b2\b\u0010#\u001a\u0004\u0018\u00010\b2\b\u0010$\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u00d7\u0001\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\b2\b\u0010 \u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\b2\b\u0010\"\u001a\u0004\u0018\u00010\b2\b\u0010#\u001a\u0004\u0018\u00010\b2\b\u0010$\u001a\u0004\u0018\u00010\b2\b\u0010%\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0003H\u0096\u0002J\u0010\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020\u0011H\u0002J\b\u0010)\u001a\u00020'H\u0002J\u000e\u0010*\u001a\u00020'2\u0006\u0010+\u001a\u00020\bR\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006," }, d2 = { "Landroidx/compose/runtime/internal/ComposableLambdaImpl;", "Landroidx/compose/runtime/internal/ComposableLambda;", "key", "", "tracked", "", "(IZ)V", "_block", "", "getKey", "()I", "scope", "Landroidx/compose/runtime/RecomposeScope;", "scopes", "", "invoke", "c", "Landroidx/compose/runtime/Composer;", "changed", "p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9", "p10", "changed1", "p11", "p12", "p13", "p14", "p15", "p16", "p17", "p18", "trackRead", "", "composer", "trackWrite", "update", "block", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableLambdaImpl implements ComposableLambda
{
    private Object _block;
    private final int key;
    private RecomposeScope scope;
    private List<RecomposeScope> scopes;
    private final boolean tracked;
    
    public ComposableLambdaImpl(final int key, final boolean tracked) {
        this.key = key;
        this.tracked = tracked;
    }
    
    private final void trackRead(final Composer composer) {
        if (this.tracked) {
            final RecomposeScope recomposeScope = composer.getRecomposeScope();
            if (recomposeScope != null) {
                composer.recordUsed(recomposeScope);
                if (ComposableLambdaKt.replacableWith(this.scope, recomposeScope)) {
                    this.scope = recomposeScope;
                }
                else {
                    final List<RecomposeScope> scopes = this.scopes;
                    if (scopes == null) {
                        (this.scopes = new ArrayList<RecomposeScope>()).add(recomposeScope);
                    }
                    else {
                        for (int i = 0; i < scopes.size(); ++i) {
                            if (ComposableLambdaKt.replacableWith((RecomposeScope)scopes.get(i), recomposeScope)) {
                                scopes.set(i, recomposeScope);
                                return;
                            }
                        }
                        scopes.add(recomposeScope);
                    }
                }
            }
        }
    }
    
    private final void trackWrite() {
        if (this.tracked) {
            final RecomposeScope scope = this.scope;
            if (scope != null) {
                scope.invalidate();
                this.scope = null;
            }
            final List<RecomposeScope> scopes = this.scopes;
            if (scopes != null) {
                for (int i = 0; i < scopes.size(); ++i) {
                    ((RecomposeScope)scopes.get(i)).invalidate();
                }
                scopes.clear();
            }
        }
    }
    
    public final int getKey() {
        return this.key;
    }
    
    public Object invoke(final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(0);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(0);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function2<@[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 2)).invoke((Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            Intrinsics.checkNotNull((Object)this, "null cannot be cast to non-null type kotlin.Function2<androidx.compose.runtime.Composer, kotlin.Int, kotlin.Unit>");
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)TypeIntrinsics.beforeCheckcastToFunctionOfArity((Object)this, 2));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(1);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(1);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function3<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function3)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 3)).invoke(o, (Object)startRestartGroup, (Object)(n2 | n));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$1(this, o, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(2);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(2);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function4<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function4)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 4)).invoke(o, o2, (Object)startRestartGroup, (Object)(n2 | n));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$2(this, o, o2, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(3);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(3);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function5<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function5)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 5)).invoke(o, o2, o3, (Object)startRestartGroup, (Object)(n2 | n));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$3(this, o, o2, o3, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(4);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(4);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function6<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function6)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 6)).invoke(o, o2, o3, o4, (Object)startRestartGroup, (Object)(n2 | n));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$4(this, o, o2, o3, o4, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(5);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(5);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function7<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function7)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 7)).invoke(o, o2, o3, o4, o5, (Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$5(this, o, o2, o3, o4, o5, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(6);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(6);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function8<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function8)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 8)).invoke(o, o2, o3, o4, o5, o6, (Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$6(this, o, o2, o3, o4, o5, o6, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(7);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(7);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function9<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function9)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 9)).invoke(o, o2, o3, o4, o5, o6, o7, (Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$7(this, o, o2, o3, o4, o5, o6, o7, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(8);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(8);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function10<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function10)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 10)).invoke(o, o2, o3, o4, o5, o6, o7, o8, (Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$8(this, o, o2, o3, o4, o5, o6, o7, o8, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(9);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(9);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function11<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function11)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 11)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, (Object)startRestartGroup, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$9(this, o, o2, o3, o4, o5, o6, o7, o8, o9, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, Composer startRestartGroup, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(10);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(10);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function13<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function13)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 13)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$10(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, i));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Composer composer, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(11);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(11);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function14<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function14)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 14)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$11(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, Composer startRestartGroup, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(12);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(12);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function15<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function15)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 15)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$12(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Composer composer, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(13);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(13);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function16<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function16)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 16)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$13(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Object o14, final Composer composer, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(14);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(14);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function17<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'p14')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function17)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 17)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$14(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Object o14, final Object o15, final Composer composer, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(15);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(15);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function18<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'p14')] kotlin.Any?, @[ParameterName(name = 'p15')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function18)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 18)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$15(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Object o14, final Object o15, final Object o16, Composer startRestartGroup, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(16);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(16);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function19<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'p14')] kotlin.Any?, @[ParameterName(name = 'p15')] kotlin.Any?, @[ParameterName(name = 'p16')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function19)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 19)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$16(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Object o14, final Object o15, final Object o16, final Object o17, Composer startRestartGroup, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)startRestartGroup, "c");
        startRestartGroup = startRestartGroup.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(17);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(17);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function20<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'p14')] kotlin.Any?, @[ParameterName(name = 'p15')] kotlin.Any?, @[ParameterName(name = 'p16')] kotlin.Any?, @[ParameterName(name = 'p17')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function20)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 20)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$17(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, i, n));
        }
        return invoke;
    }
    
    public Object invoke(final Object o, final Object o2, final Object o3, final Object o4, final Object o5, final Object o6, final Object o7, final Object o8, final Object o9, final Object o10, final Object o11, final Object o12, final Object o13, final Object o14, final Object o15, final Object o16, final Object o17, final Object o18, final Composer composer, final int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)composer, "c");
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n2;
        if (startRestartGroup.changed(this)) {
            n2 = ComposableLambdaKt.differentBits(18);
        }
        else {
            n2 = ComposableLambdaKt.sameBits(18);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.Function21<@[ParameterName(name = 'p1')] kotlin.Any?, @[ParameterName(name = 'p2')] kotlin.Any?, @[ParameterName(name = 'p3')] kotlin.Any?, @[ParameterName(name = 'p4')] kotlin.Any?, @[ParameterName(name = 'p5')] kotlin.Any?, @[ParameterName(name = 'p6')] kotlin.Any?, @[ParameterName(name = 'p7')] kotlin.Any?, @[ParameterName(name = 'p8')] kotlin.Any?, @[ParameterName(name = 'p9')] kotlin.Any?, @[ParameterName(name = 'p10')] kotlin.Any?, @[ParameterName(name = 'p11')] kotlin.Any?, @[ParameterName(name = 'p12')] kotlin.Any?, @[ParameterName(name = 'p13')] kotlin.Any?, @[ParameterName(name = 'p14')] kotlin.Any?, @[ParameterName(name = 'p15')] kotlin.Any?, @[ParameterName(name = 'p16')] kotlin.Any?, @[ParameterName(name = 'p17')] kotlin.Any?, @[ParameterName(name = 'p18')] kotlin.Any?, @[ParameterName(name = 'c')] androidx.compose.runtime.Composer, @[ParameterName(name = 'changed')] kotlin.Int, @[ParameterName(name = 'changed1')] kotlin.Int, kotlin.Any?>");
        final Object invoke = ((Function21)TypeIntrinsics.beforeCheckcastToFunctionOfArity(block, 21)).invoke(o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, (Object)startRestartGroup, (Object)i, (Object)(n | n2));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaImpl$invoke.ComposableLambdaImpl$invoke$18(this, o, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, i, n));
        }
        return invoke;
    }
    
    public final void update(final Object block) {
        Intrinsics.checkNotNullParameter(block, "block");
        if (!Intrinsics.areEqual(this._block, block)) {
            final boolean b = this._block == null;
            this._block = block;
            if (!b) {
                this.trackWrite();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.internal;

import androidx.compose.runtime.RecomposeScopeImpl;
import androidx.compose.runtime.RecomposeScope;
import androidx.compose.runtime.ComposeCompilerApi;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.Composer;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0000\u001a(\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007\u001a \u0010\u000f\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007\u001a\u0010\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0000\u001a\u0010\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0000\u001a\u0016\u0010\u0012\u001a\u00020\f*\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "BITS_PER_SLOT", "", "SLOTS_PER_INT", "bitsForSlot", "bits", "slot", "composableLambda", "Landroidx/compose/runtime/internal/ComposableLambda;", "composer", "Landroidx/compose/runtime/Composer;", "key", "tracked", "", "block", "", "composableLambdaInstance", "differentBits", "sameBits", "replacableWith", "Landroidx/compose/runtime/RecomposeScope;", "other", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableLambdaKt
{
    private static final int BITS_PER_SLOT = 3;
    public static final int SLOTS_PER_INT = 10;
    
    public static final int bitsForSlot(final int n, final int n2) {
        return n << n2 % 10 * 3 + 1;
    }
    
    @ComposeCompilerApi
    public static final ComposableLambda composableLambda(final Composer composer, final int n, final boolean b, final Object o) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter(o, "block");
        composer.startReplaceableGroup(n);
        final Object rememberedValue = composer.rememberedValue();
        ComposableLambdaImpl composableLambdaImpl;
        if (rememberedValue == Composer.Companion.getEmpty()) {
            composableLambdaImpl = new ComposableLambdaImpl(n, b);
            composer.updateRememberedValue(composableLambdaImpl);
        }
        else {
            Intrinsics.checkNotNull(rememberedValue, "null cannot be cast to non-null type androidx.compose.runtime.internal.ComposableLambdaImpl");
            composableLambdaImpl = (ComposableLambdaImpl)rememberedValue;
        }
        composableLambdaImpl.update(o);
        composer.endReplaceableGroup();
        return composableLambdaImpl;
    }
    
    @ComposeCompilerApi
    public static final ComposableLambda composableLambdaInstance(final int n, final boolean b, final Object o) {
        Intrinsics.checkNotNullParameter(o, "block");
        final ComposableLambdaImpl composableLambdaImpl = new ComposableLambdaImpl(n, b);
        composableLambdaImpl.update(o);
        return composableLambdaImpl;
    }
    
    public static final int differentBits(final int n) {
        return bitsForSlot(2, n);
    }
    
    public static final boolean replacableWith(final RecomposeScope recomposeScope, final RecomposeScope recomposeScope2) {
        Intrinsics.checkNotNullParameter((Object)recomposeScope2, "other");
        if (recomposeScope != null) {
            if (recomposeScope instanceof RecomposeScopeImpl && recomposeScope2 instanceof RecomposeScopeImpl) {
                final RecomposeScopeImpl recomposeScopeImpl = (RecomposeScopeImpl)recomposeScope;
                if (!recomposeScopeImpl.getValid() || Intrinsics.areEqual((Object)recomposeScope, (Object)recomposeScope2)) {
                    return true;
                }
                if (Intrinsics.areEqual((Object)recomposeScopeImpl.getAnchor(), (Object)((RecomposeScopeImpl)recomposeScope2).getAnchor())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public static final int sameBits(final int n) {
        return bitsForSlot(1, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0016\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\b\u0000\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u0010\r\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u000b\u001a\u00020\fJ\u0018\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0007X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\t¨\u0006\u0012" }, d2 = { "Landroidx/compose/runtime/internal/ThreadMap;", "", "size", "", "keys", "", "values", "", "(I[J[Ljava/lang/Object;)V", "[Ljava/lang/Object;", "find", "key", "", "get", "newWith", "value", "trySet", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ThreadMap
{
    private final long[] keys;
    private final int size;
    private final Object[] values;
    
    public ThreadMap(final int size, final long[] keys, final Object[] values) {
        Intrinsics.checkNotNullParameter((Object)keys, "keys");
        Intrinsics.checkNotNullParameter((Object)values, "values");
        this.size = size;
        this.keys = keys;
        this.values = values;
    }
    
    private final int find(final long n) {
        int n2 = this.size - 1;
        int n3 = -1;
        if (n2 != -1) {
            int i = 0;
            if (n2 != 0) {
                while (i <= n2) {
                    final int n4 = i + n2 >>> 1;
                    final long n5 = lcmp(this.keys[n4] - n, 0L);
                    if (n5 < 0) {
                        i = n4 + 1;
                    }
                    else {
                        if (n5 <= 0) {
                            return n4;
                        }
                        n2 = n4 - 1;
                    }
                }
                return -(i + 1);
            }
            final long n6 = this.keys[0];
            if (n6 == n) {
                n3 = 0;
            }
            else {
                n3 = n3;
                if (n6 > n) {
                    n3 = -2;
                }
            }
        }
        return n3;
    }
    
    public final Object get(final long n) {
        final int find = this.find(n);
        Object o;
        if (find >= 0) {
            o = this.values[find];
        }
        else {
            o = null;
        }
        return o;
    }
    
    public final ThreadMap newWith(long n, Object o) {
        final int size = this.size;
        final Object[] values = this.values;
        final int length = values.length;
        final int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        while (true) {
            boolean b = true;
            if (n3 >= length) {
                break;
            }
            if (values[n3] == null) {
                b = false;
            }
            int n5 = n4;
            if (b) {
                n5 = n4 + 1;
            }
            ++n3;
            n4 = n5;
        }
        final int n6 = n4 + 1;
        final long[] array = new long[n6];
        final Object[] array2 = new Object[n6];
        if (n6 > 1) {
            int n7 = 0;
            int n8 = n2;
            int i;
            while (true) {
                i = n8;
                if (n8 >= n6) {
                    break;
                }
                i = n8;
                if (n7 >= size) {
                    break;
                }
                final long n9 = this.keys[n7];
                final Object o2 = this.values[n7];
                if (n9 > n) {
                    array[n8] = n;
                    array2[n8] = o;
                    i = n8 + 1;
                    break;
                }
                int n10 = n8;
                if (o2 != null) {
                    array[n8] = n9;
                    array2[n8] = o2;
                    n10 = n8 + 1;
                }
                ++n7;
                n8 = n10;
            }
            int n11;
            if ((n11 = n7) == size) {
                final int n12 = n6 - 1;
                array[n12] = n;
                array2[n12] = o;
            }
            else {
                while (i < n6) {
                    n = this.keys[n11];
                    o = this.values[n11];
                    int n13 = i;
                    if (o != null) {
                        array[i] = n;
                        array2[i] = o;
                        n13 = i + 1;
                    }
                    ++n11;
                    i = n13;
                }
            }
        }
        else {
            array[0] = n;
            array2[0] = o;
        }
        return new ThreadMap(n6, array, array2);
    }
    
    public final boolean trySet(final long n, final Object o) {
        final int find = this.find(n);
        if (find < 0) {
            return false;
        }
        this.values[find] = o;
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.internal;

import androidx.compose.runtime.ComposeCompilerApi;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.Composer;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a0\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH\u0007\u001a(\u0010\u000b\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\f" }, d2 = { "composableLambdaN", "Landroidx/compose/runtime/internal/ComposableLambdaN;", "composer", "Landroidx/compose/runtime/Composer;", "key", "", "tracked", "", "arity", "block", "", "composableLambdaNInstance", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableLambdaN_jvmKt
{
    @ComposeCompilerApi
    public static final ComposableLambdaN composableLambdaN(final Composer composer, final int n, final boolean b, final int n2, final Object o) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter(o, "block");
        composer.startReplaceableGroup(n);
        final Object rememberedValue = composer.rememberedValue();
        ComposableLambdaNImpl composableLambdaNImpl;
        if (rememberedValue == Composer.Companion.getEmpty()) {
            composableLambdaNImpl = new ComposableLambdaNImpl(n, b, n2);
            composer.updateRememberedValue(composableLambdaNImpl);
        }
        else {
            Intrinsics.checkNotNull(rememberedValue, "null cannot be cast to non-null type androidx.compose.runtime.internal.ComposableLambdaNImpl");
            composableLambdaNImpl = (ComposableLambdaNImpl)rememberedValue;
        }
        composableLambdaNImpl.update(o);
        composer.endReplaceableGroup();
        return composableLambdaNImpl;
    }
    
    @ComposeCompilerApi
    public static final ComposableLambdaN composableLambdaNInstance(final int n, final boolean b, final int n2, final Object o) {
        Intrinsics.checkNotNullParameter(o, "block");
        final ComposableLambdaNImpl composableLambdaNImpl = new ComposableLambdaNImpl(n, b, n2);
        composableLambdaNImpl.update(o);
        return composableLambdaNImpl;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.internal;

import androidx.compose.runtime.ScopeUpdateScope;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.SpreadBuilder;
import kotlin.jvm.functions.FunctionN;
import kotlin.collections.ArraysKt;
import kotlin.ranges.RangesKt;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayList;
import androidx.compose.runtime.Composer;
import java.util.List;
import androidx.compose.runtime.RecomposeScope;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J(\u0010\u0011\u001a\u0004\u0018\u00010\t2\u0016\u0010\u0012\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\t0\u0013\"\u0004\u0018\u00010\tH\u0096\u0002¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0003H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u0018H\u0002J\u000e\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\tR\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Landroidx/compose/runtime/internal/ComposableLambdaNImpl;", "Landroidx/compose/runtime/internal/ComposableLambdaN;", "key", "", "tracked", "", "arity", "(IZI)V", "_block", "", "getArity", "()I", "getKey", "scope", "Landroidx/compose/runtime/RecomposeScope;", "scopes", "", "invoke", "args", "", "([Ljava/lang/Object;)Ljava/lang/Object;", "realParamCount", "params", "trackRead", "", "composer", "Landroidx/compose/runtime/Composer;", "trackWrite", "update", "block", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableLambdaNImpl implements ComposableLambdaN
{
    private Object _block;
    private final int arity;
    private final int key;
    private RecomposeScope scope;
    private List<RecomposeScope> scopes;
    private final boolean tracked;
    
    public ComposableLambdaNImpl(final int key, final boolean tracked, final int arity) {
        this.key = key;
        this.tracked = tracked;
        this.arity = arity;
    }
    
    private final int realParamCount(int n) {
        int n2;
        for (n2 = n - 1 - 1, n = 1; n * 10 < n2; --n2, ++n) {}
        return n2;
    }
    
    private final void trackRead(final Composer composer) {
        if (this.tracked) {
            final RecomposeScope recomposeScope = composer.getRecomposeScope();
            if (recomposeScope != null) {
                composer.recordUsed(recomposeScope);
                if (ComposableLambdaKt.replacableWith(this.scope, recomposeScope)) {
                    this.scope = recomposeScope;
                }
                else {
                    final List<RecomposeScope> scopes = this.scopes;
                    if (scopes == null) {
                        (this.scopes = new ArrayList<RecomposeScope>()).add(recomposeScope);
                    }
                    else {
                        for (int i = 0; i < scopes.size(); ++i) {
                            if (ComposableLambdaKt.replacableWith((RecomposeScope)scopes.get(i), recomposeScope)) {
                                scopes.set(i, recomposeScope);
                                return;
                            }
                        }
                        scopes.add(recomposeScope);
                    }
                }
            }
        }
    }
    
    private final void trackWrite() {
        if (this.tracked) {
            final RecomposeScope scope = this.scope;
            if (scope != null) {
                scope.invalidate();
                this.scope = null;
            }
            final List<RecomposeScope> scopes = this.scopes;
            if (scopes != null) {
                for (int i = 0; i < scopes.size(); ++i) {
                    ((RecomposeScope)scopes.get(i)).invalidate();
                }
                scopes.clear();
            }
        }
    }
    
    public int getArity() {
        return this.arity;
    }
    
    public final int getKey() {
        return this.key;
    }
    
    public Object invoke(final Object... array) {
        Intrinsics.checkNotNullParameter((Object)array, "args");
        final int realParamCount = this.realParamCount(array.length);
        final Object o = array[realParamCount];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.Composer");
        final Composer composer = (Composer)o;
        final Object[] array2 = ArraysKt.slice(array, RangesKt.until(0, array.length - 1)).toArray(new Object[0]);
        final Object o2 = array[array.length - 1];
        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Int");
        final int intValue = (int)o2;
        final Composer startRestartGroup = composer.startRestartGroup(this.key);
        this.trackRead(startRestartGroup);
        int n;
        if (startRestartGroup.changed(this)) {
            n = ComposableLambdaKt.differentBits(realParamCount);
        }
        else {
            n = ComposableLambdaKt.sameBits(realParamCount);
        }
        final Object block = this._block;
        Intrinsics.checkNotNull(block, "null cannot be cast to non-null type kotlin.jvm.functions.FunctionN<*>");
        final FunctionN functionN = (FunctionN)block;
        final SpreadBuilder spreadBuilder = new SpreadBuilder(2);
        spreadBuilder.addSpread((Object)array2);
        spreadBuilder.add((Object)(intValue | n));
        final Object invoke = functionN.invoke(spreadBuilder.toArray(new Object[spreadBuilder.size()]));
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new ComposableLambdaNImpl$invoke.ComposableLambdaNImpl$invoke$1(array, realParamCount, this));
        }
        return invoke;
    }
    
    public final void update(final Object o) {
        Intrinsics.checkNotNullParameter(o, "block");
        if (!Intrinsics.areEqual(o, this._block)) {
            final boolean b = this._block == null;
            this._block = o;
            if (!b) {
                this.trackWrite();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.tooling;

import androidx.compose.runtime.CompositionLocalKt;
import kotlin.jvm.functions.Function0;
import java.util.Set;
import androidx.compose.runtime.ProvidableCompositionLocal;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u001f\u0010\u0000\u001a\u0010\u0012\f\u0012\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0006" }, d2 = { "LocalInspectionTables", "Landroidx/compose/runtime/ProvidableCompositionLocal;", "", "Landroidx/compose/runtime/tooling/CompositionData;", "getLocalInspectionTables", "()Landroidx/compose/runtime/ProvidableCompositionLocal;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class InspectionTablesKt
{
    private static final ProvidableCompositionLocal<Set<CompositionData>> LocalInspectionTables;
    
    static {
        LocalInspectionTables = CompositionLocalKt.staticCompositionLocalOf((kotlin.jvm.functions.Function0<? extends Set<CompositionData>>)InspectionTablesKt$LocalInspectionTables.InspectionTablesKt$LocalInspectionTables$1.INSTANCE);
    }
    
    public static final ProvidableCompositionLocal<Set<CompositionData>> getLocalInspectionTables() {
        return InspectionTablesKt.LocalInspectionTables;
    }
}

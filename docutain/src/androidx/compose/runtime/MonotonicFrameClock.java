// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$Element$DefaultImpls;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.Metadata;
import kotlin.coroutines.CoroutineContext$Element;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fJ:\u0010\u0006\u001a\u0002H\u0007\"\u0004\b\u0000\u0010\u00072!\u0010\b\u001a\u001d\u0012\u0013\u0012\u00110\n¢\u0006\f\b\u000b\u0012\b\b\f\u0012\u0004\b\b(\r\u0012\u0004\u0012\u0002H\u00070\tH¦@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eR\u0018\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00f8\u0001\u0001\u0082\u0002\n\n\u0002\b\u0019\n\u0004\b!0\u0001¨\u0006\u0010\u00c0\u0006\u0003" }, d2 = { "Landroidx/compose/runtime/MonotonicFrameClock;", "Lkotlin/coroutines/CoroutineContext$Element;", "key", "Lkotlin/coroutines/CoroutineContext$Key;", "getKey", "()Lkotlin/coroutines/CoroutineContext$Key;", "withFrameNanos", "R", "onFrame", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "frameTimeNanos", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Key", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface MonotonicFrameClock extends CoroutineContext$Element
{
    public static final Key Key = MonotonicFrameClock.Key.$$INSTANCE;
    
    CoroutineContext$Key<?> getKey();
    
     <R> Object withFrameNanos(final Function1<? super Long, ? extends R> p0, final Continuation<? super R> p1);
    
    @Metadata(k = 3, mv = { 1, 8, 0 }, xi = 48)
    public static final class DefaultImpls
    {
        public static <R> R fold(final MonotonicFrameClock monotonicFrameClock, final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
            Intrinsics.checkNotNullParameter((Object)function2, "operation");
            return (R)CoroutineContext$Element$DefaultImpls.fold((CoroutineContext$Element)monotonicFrameClock, (Object)r, (Function2)function2);
        }
        
        public static <E extends CoroutineContext$Element> E get(final MonotonicFrameClock monotonicFrameClock, final CoroutineContext$Key<E> coroutineContext$Key) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext$Key, "key");
            return (E)CoroutineContext$Element$DefaultImpls.get((CoroutineContext$Element)monotonicFrameClock, (CoroutineContext$Key)coroutineContext$Key);
        }
        
        @Deprecated
        public static CoroutineContext$Key<?> getKey(final MonotonicFrameClock monotonicFrameClock) {
            return (CoroutineContext$Key<?>)MonotonicFrameClock$_CC.access$getKey$jd(monotonicFrameClock);
        }
        
        public static CoroutineContext minusKey(final MonotonicFrameClock monotonicFrameClock, final CoroutineContext$Key<?> coroutineContext$Key) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext$Key, "key");
            return CoroutineContext$Element$DefaultImpls.minusKey((CoroutineContext$Element)monotonicFrameClock, (CoroutineContext$Key)coroutineContext$Key);
        }
        
        public static CoroutineContext plus(final MonotonicFrameClock monotonicFrameClock, final CoroutineContext coroutineContext) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
            return CoroutineContext$Element$DefaultImpls.plus((CoroutineContext$Element)monotonicFrameClock, coroutineContext);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Landroidx/compose/runtime/MonotonicFrameClock$Key;", "Lkotlin/coroutines/CoroutineContext$Key;", "Landroidx/compose/runtime/MonotonicFrameClock;", "()V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Key implements CoroutineContext$Key<MonotonicFrameClock>
    {
        static final Key $$INSTANCE;
        
        static {
            $$INSTANCE = new Key();
        }
        
        private Key() {
        }
    }
}

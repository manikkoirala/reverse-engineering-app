// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.functions.Function0;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a(\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0011\u0010\u0004\u001a\r\u0012\u0004\u0012\u00020\u00010\u0005¢\u0006\u0002\b\u0006H\u0007¢\u0006\u0002\u0010\u0007\u001a<\u0010\u0000\u001a\u00020\u00012\u001a\u0010\b\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030\n0\t\"\u0006\u0012\u0002\b\u00030\n2\u0011\u0010\u0004\u001a\r\u0012\u0004\u0012\u00020\u00010\u0005¢\u0006\u0002\b\u0006H\u0007¢\u0006\u0002\u0010\u000b\u001a0\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000e0\r\"\u0004\b\u0000\u0010\u000e2\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0005\u001a \u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u000e0\r\"\u0004\b\u0000\u0010\u000e2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0005¨\u0006\u0013" }, d2 = { "CompositionLocalProvider", "", "context", "Landroidx/compose/runtime/CompositionLocalContext;", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "(Landroidx/compose/runtime/CompositionLocalContext;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "values", "", "Landroidx/compose/runtime/ProvidedValue;", "([Landroidx/compose/runtime/ProvidedValue;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)V", "compositionLocalOf", "Landroidx/compose/runtime/ProvidableCompositionLocal;", "T", "policy", "Landroidx/compose/runtime/SnapshotMutationPolicy;", "defaultFactory", "staticCompositionLocalOf", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class CompositionLocalKt
{
    public static final void CompositionLocalProvider(final CompositionLocalContext compositionLocalContext, final Function2<? super Composer, ? super Integer, Unit> function2, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)compositionLocalContext, "context");
        Intrinsics.checkNotNullParameter((Object)function2, "content");
        startRestartGroup = startRestartGroup.startRestartGroup(1853897736);
        ComposerKt.sourceInformation(startRestartGroup, "C(CompositionLocalProvider)P(1)247@10697L209:CompositionLocal.kt#9igjgp");
        int n3;
        if ((n & 0xE) == 0x0) {
            int n2;
            if (startRestartGroup.changed(compositionLocalContext)) {
                n2 = 4;
            }
            else {
                n2 = 2;
            }
            n3 = (n2 | n);
        }
        else {
            n3 = n;
        }
        int n4 = n3;
        if ((n & 0x70) == 0x0) {
            int n5;
            if (startRestartGroup.changedInstance(function2)) {
                n5 = 32;
            }
            else {
                n5 = 16;
            }
            n4 = (n3 | n5);
        }
        if ((n4 & 0x5B) == 0x12 && startRestartGroup.getSkipping()) {
            startRestartGroup.skipToGroupEnd();
        }
        else {
            if (ComposerKt.isTraceInProgress()) {
                ComposerKt.traceEventStart(1853897736, n4, -1, "androidx.compose.runtime.CompositionLocalProvider (CompositionLocal.kt:246)");
            }
            final Map map = compositionLocalContext.getCompositionLocals$runtime_release();
            final Collection collection = new ArrayList(map.size());
            for (final Map.Entry<Object, V> entry : map.entrySet()) {
                final ProvidableCompositionLocal key = entry.getKey();
                Intrinsics.checkNotNull((Object)key, "null cannot be cast to non-null type androidx.compose.runtime.ProvidableCompositionLocal<kotlin.Any?>");
                collection.add(key.provides(((State)entry.getValue()).getValue()));
            }
            final ProvidedValue[] original = ((Collection)(List<E>)collection).toArray(new ProvidedValue[0]);
            CompositionLocalProvider(Arrays.copyOf(original, original.length), function2, startRestartGroup, (n4 & 0x70) | 0x8);
            if (ComposerKt.isTraceInProgress()) {
                ComposerKt.traceEventEnd();
            }
        }
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new CompositionLocalKt$CompositionLocalProvider.CompositionLocalKt$CompositionLocalProvider$3(compositionLocalContext, (Function2)function2, n));
        }
    }
    
    public static final void CompositionLocalProvider(final ProvidedValue<?>[] array, final Function2<? super Composer, ? super Integer, Unit> function2, Composer startRestartGroup, final int n) {
        Intrinsics.checkNotNullParameter((Object)array, "values");
        Intrinsics.checkNotNullParameter((Object)function2, "content");
        startRestartGroup = startRestartGroup.startRestartGroup(-1390796515);
        ComposerKt.sourceInformation(startRestartGroup, "C(CompositionLocalProvider)P(1)227@9992L9:CompositionLocal.kt#9igjgp");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1390796515, n, -1, "androidx.compose.runtime.CompositionLocalProvider (CompositionLocal.kt:225)");
        }
        startRestartGroup.startProviders(array);
        function2.invoke((Object)startRestartGroup, (Object)(n >> 3 & 0xE));
        startRestartGroup.endProviders();
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        final ScopeUpdateScope endRestartGroup = startRestartGroup.endRestartGroup();
        if (endRestartGroup != null) {
            endRestartGroup.updateScope((Function2<? super Composer, ? super Integer, Unit>)new CompositionLocalKt$CompositionLocalProvider.CompositionLocalKt$CompositionLocalProvider$1((ProvidedValue[])array, (Function2)function2, n));
        }
    }
    
    public static final <T> ProvidableCompositionLocal<T> compositionLocalOf(final SnapshotMutationPolicy<T> snapshotMutationPolicy, final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)snapshotMutationPolicy, "policy");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultFactory");
        return new DynamicProvidableCompositionLocal<T>(snapshotMutationPolicy, function0);
    }
    
    public static final <T> ProvidableCompositionLocal<T> staticCompositionLocalOf(final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "defaultFactory");
        return new StaticProvidableCompositionLocal<T>(function0);
    }
}

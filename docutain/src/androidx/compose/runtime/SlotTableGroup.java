// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import java.util.Iterator;
import java.util.ConcurrentModificationException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import androidx.compose.runtime.tooling.CompositionGroup;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010(\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00010\u0002B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0012\u0010&\u001a\u0004\u0018\u00010\u00012\u0006\u0010'\u001a\u00020\rH\u0016J\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00010)H\u0096\u0002J\b\u0010*\u001a\u00020+H\u0002R\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u0014\u0010\u0013\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u00178VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u0015R\u0016\u0010\u001b\u001a\u0004\u0018\u00010\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0015R\u0014\u0010\u001d\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0010R\u0016\u0010\u001f\u001a\u0004\u0018\u00010 8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0010¨\u0006," }, d2 = { "Landroidx/compose/runtime/SlotTableGroup;", "Landroidx/compose/runtime/tooling/CompositionGroup;", "", "table", "Landroidx/compose/runtime/SlotTable;", "group", "", "version", "(Landroidx/compose/runtime/SlotTable;II)V", "compositionGroups", "getCompositionGroups", "()Ljava/lang/Iterable;", "data", "", "getData", "getGroup", "()I", "groupSize", "getGroupSize", "identity", "getIdentity", "()Ljava/lang/Object;", "isEmpty", "", "()Z", "key", "getKey", "node", "getNode", "slotsSize", "getSlotsSize", "sourceInfo", "", "getSourceInfo", "()Ljava/lang/String;", "getTable", "()Landroidx/compose/runtime/SlotTable;", "getVersion", "find", "identityToFind", "iterator", "", "validateRead", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SlotTableGroup implements CompositionGroup, Iterable<CompositionGroup>, KMappedMarker
{
    private final int group;
    private final SlotTable table;
    private final int version;
    
    public SlotTableGroup(final SlotTable table, final int group, final int version) {
        Intrinsics.checkNotNullParameter((Object)table, "table");
        this.table = table;
        this.group = group;
        this.version = version;
    }
    
    private final void validateRead() {
        if (this.table.getVersion$runtime_release() == this.version) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public CompositionGroup find(final Object o) {
        Intrinsics.checkNotNullParameter(o, "identityToFind");
        final boolean b = o instanceof Anchor;
        final CompositionGroup compositionGroup = null;
        Anchor anchor;
        if (b) {
            anchor = (Anchor)o;
        }
        else {
            anchor = null;
        }
        CompositionGroup compositionGroup2 = compositionGroup;
        if (anchor != null) {
            compositionGroup2 = compositionGroup;
            if (this.table.ownsAnchor(anchor)) {
                final int anchorIndex = this.table.anchorIndex(anchor);
                final int group = this.group;
                compositionGroup2 = compositionGroup;
                if (anchorIndex >= group) {
                    compositionGroup2 = compositionGroup;
                    if (anchorIndex - group < SlotTableKt.access$groupSize(this.table.getGroups(), this.group)) {
                        compositionGroup2 = new SlotTableGroup(this.table, anchorIndex, this.version);
                    }
                }
            }
        }
        return compositionGroup2;
    }
    
    public Iterable<CompositionGroup> getCompositionGroups() {
        return this;
    }
    
    @Override
    public Iterable<Object> getData() {
        return new DataIterator(this.table, this.group);
    }
    
    public final int getGroup() {
        return this.group;
    }
    
    @Override
    public int getGroupSize() {
        return SlotTableKt.access$groupSize(this.table.getGroups(), this.group);
    }
    
    @Override
    public Object getIdentity() {
        this.validateRead();
        final SlotReader openReader = this.table.openReader();
        try {
            return openReader.anchor(this.group);
        }
        finally {
            openReader.close();
        }
    }
    
    @Override
    public Object getKey() {
        Object value;
        if (SlotTableKt.access$hasObjectKey(this.table.getGroups(), this.group)) {
            value = this.table.getSlots()[SlotTableKt.access$objectKeyIndex(this.table.getGroups(), this.group)];
            Intrinsics.checkNotNull(value);
        }
        else {
            value = SlotTableKt.access$key(this.table.getGroups(), this.group);
        }
        return value;
    }
    
    @Override
    public Object getNode() {
        Object o;
        if (SlotTableKt.access$isNode(this.table.getGroups(), this.group)) {
            o = this.table.getSlots()[SlotTableKt.access$nodeIndex(this.table.getGroups(), this.group)];
        }
        else {
            o = null;
        }
        return o;
    }
    
    @Override
    public int getSlotsSize() {
        final int n = this.group + this.getGroupSize();
        int n2;
        if (n < this.table.getGroupsSize()) {
            n2 = SlotTableKt.access$dataAnchor(this.table.getGroups(), n);
        }
        else {
            n2 = this.table.getSlotsSize();
        }
        return n2 - SlotTableKt.access$dataAnchor(this.table.getGroups(), this.group);
    }
    
    @Override
    public String getSourceInfo() {
        final boolean access$hasAux = SlotTableKt.access$hasAux(this.table.getGroups(), this.group);
        String s = null;
        if (access$hasAux) {
            final Object o = this.table.getSlots()[SlotTableKt.access$auxIndex(this.table.getGroups(), this.group)];
            s = s;
            if (o instanceof String) {
                s = (String)o;
            }
        }
        return s;
    }
    
    public final SlotTable getTable() {
        return this.table;
    }
    
    public final int getVersion() {
        return this.version;
    }
    
    public boolean isEmpty() {
        return SlotTableKt.access$groupSize(this.table.getGroups(), this.group) == 0;
    }
    
    @Override
    public Iterator<CompositionGroup> iterator() {
        this.validateRead();
        final SlotTable table = this.table;
        final int group = this.group;
        return new GroupIterator(table, group + 1, group + SlotTableKt.access$groupSize(table.getGroups(), this.group));
    }
}

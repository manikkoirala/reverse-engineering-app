// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.rxjava3;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Flowable;
import kotlin.jvm.internal.Intrinsics;
import io.reactivex.rxjava3.core.Completable;
import androidx.compose.runtime.DisposableEffectResult;
import androidx.compose.runtime.DisposableEffectScope;
import androidx.compose.runtime.EffectsKt;
import androidx.compose.runtime.MutableState;
import androidx.compose.runtime.SnapshotMutationPolicy;
import androidx.compose.runtime.SnapshotStateKt;
import androidx.compose.runtime.ComposerKt;
import androidx.compose.runtime.State;
import androidx.compose.runtime.Composer;
import io.reactivex.rxjava3.disposables.Disposable;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000H\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001aY\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0002H\u00032\u0006\u0010\u0004\u001a\u0002H\u00022+\b\u0004\u0010\u0005\u001a%\u0012\u0004\u0012\u0002H\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0004\u0012\u00020\t0\u0006¢\u0006\u0002\b\nH\u0083\b¢\u0006\u0002\u0010\u000b\u001a\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0001*\u00020\u000eH\u0007¢\u0006\u0002\u0010\u000f\u001a5\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00100\u0001\"\u0004\b\u0000\u0010\u0010\"\b\b\u0001\u0010\u0002*\u0002H\u0010*\b\u0012\u0004\u0012\u0002H\u00020\u00112\u0006\u0010\u0004\u001a\u0002H\u0010H\u0007¢\u0006\u0002\u0010\u0012\u001a5\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00100\u0001\"\u0004\b\u0000\u0010\u0010\"\b\b\u0001\u0010\u0002*\u0002H\u0010*\b\u0012\u0004\u0012\u0002H\u00020\u00132\u0006\u0010\u0004\u001a\u0002H\u0010H\u0007¢\u0006\u0002\u0010\u0014\u001a5\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00100\u0001\"\u0004\b\u0000\u0010\u0010\"\b\b\u0001\u0010\u0002*\u0002H\u0010*\b\u0012\u0004\u0012\u0002H\u00020\u00152\u0006\u0010\u0004\u001a\u0002H\u0010H\u0007¢\u0006\u0002\u0010\u0016\u001a5\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00100\u0001\"\u0004\b\u0000\u0010\u0010\"\b\b\u0001\u0010\u0002*\u0002H\u0010*\b\u0012\u0004\u0012\u0002H\u00020\u00172\u0006\u0010\u0004\u001a\u0002H\u0010H\u0007¢\u0006\u0002\u0010\u0018¨\u0006\u0019" }, d2 = { "asState", "Landroidx/compose/runtime/State;", "T", "S", "initial", "subscribe", "Lkotlin/Function2;", "Lkotlin/Function1;", "", "Lio/reactivex/rxjava3/disposables/Disposable;", "Lkotlin/ExtensionFunctionType;", "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "subscribeAsState", "", "Lio/reactivex/rxjava3/core/Completable;", "(Lio/reactivex/rxjava3/core/Completable;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "R", "Lio/reactivex/rxjava3/core/Flowable;", "(Lio/reactivex/rxjava3/core/Flowable;Ljava/lang/Object;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "Lio/reactivex/rxjava3/core/Maybe;", "(Lio/reactivex/rxjava3/core/Maybe;Ljava/lang/Object;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "Lio/reactivex/rxjava3/core/Observable;", "(Lio/reactivex/rxjava3/core/Observable;Ljava/lang/Object;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "Lio/reactivex/rxjava3/core/Single;", "(Lio/reactivex/rxjava3/core/Single;Ljava/lang/Object;Landroidx/compose/runtime/Composer;I)Landroidx/compose/runtime/State;", "runtime-rxjava3_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class RxJava3AdapterKt
{
    private static final <T, S> State<T> asState(final S n, final T t, final Function2<? super S, ? super Function1<? super T, Unit>, ? extends Disposable> function2, final Composer composer, final int n2) {
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(t, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(n, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$asState.RxJava3AdapterKt$asState$1((Function2)function2, (Object)n, mutableState), composer, n2 & 0xE);
        composer.endReplaceableGroup();
        return mutableState;
    }
    
    public static final State<Boolean> subscribeAsState(final Completable completable, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)completable, "<this>");
        composer.startReplaceableGroup(1334238354);
        ComposerKt.sourceInformation(composer, "C(subscribeAsState)129@5633L59:RxJava3Adapter.kt#wvx964");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(1334238354, n, -1, "androidx.compose.runtime.rxjava3.subscribeAsState (RxJava3Adapter.kt:128)");
        }
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(false, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(completable, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$subscribeAsState$$inlined$asState.RxJava3AdapterKt$subscribeAsState$$inlined$asState$5((Object)completable, mutableState), composer, 8);
        composer.endReplaceableGroup();
        final State state = mutableState;
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
        return state;
    }
    
    public static final <R, T extends R> State<R> subscribeAsState(final Flowable<T> flowable, final R r, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)flowable, "<this>");
        composer.startReplaceableGroup(-1952109204);
        ComposerKt.sourceInformation(composer, "C(subscribeAsState)71@3053L34:RxJava3Adapter.kt#wvx964");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-1952109204, n, -1, "androidx.compose.runtime.rxjava3.subscribeAsState (RxJava3Adapter.kt:70)");
        }
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(r, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(flowable, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$subscribeAsState$$inlined$asState.RxJava3AdapterKt$subscribeAsState$$inlined$asState$2((Object)flowable, mutableState), composer, ((n & 0x70) | ((n >> 3 & 0x8) << 3 | 0x8)) & 0xE);
        composer.endReplaceableGroup();
        final State state = mutableState;
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
        return state;
    }
    
    public static final <R, T extends R> State<R> subscribeAsState(final Maybe<T> maybe, final R r, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)maybe, "<this>");
        composer.startReplaceableGroup(1243760040);
        ComposerKt.sourceInformation(composer, "C(subscribeAsState)112@4876L34:RxJava3Adapter.kt#wvx964");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(1243760040, n, -1, "androidx.compose.runtime.rxjava3.subscribeAsState (RxJava3Adapter.kt:111)");
        }
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(r, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(maybe, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$subscribeAsState$$inlined$asState.RxJava3AdapterKt$subscribeAsState$$inlined$asState$4((Object)maybe, mutableState), composer, ((n & 0x70) | ((n >> 3 & 0x8) << 3 | 0x8)) & 0xE);
        composer.endReplaceableGroup();
        final State state = mutableState;
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
        return state;
    }
    
    public static final <R, T extends R> State<R> subscribeAsState(final Observable<T> observable, final R r, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)observable, "<this>");
        composer.startReplaceableGroup(-845703663);
        ComposerKt.sourceInformation(composer, "C(subscribeAsState)50@2109L34:RxJava3Adapter.kt#wvx964");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(-845703663, n, -1, "androidx.compose.runtime.rxjava3.subscribeAsState (RxJava3Adapter.kt:49)");
        }
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(r, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(observable, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$subscribeAsState$$inlined$asState.RxJava3AdapterKt$subscribeAsState$$inlined$asState$1((Object)observable, mutableState), composer, ((n & 0x70) | ((n >> 3 & 0x8) << 3 | 0x8)) & 0xE);
        composer.endReplaceableGroup();
        final State state = mutableState;
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
        return state;
    }
    
    public static final <R, T extends R> State<R> subscribeAsState(final Single<T> single, final R r, final Composer composer, final int n) {
        Intrinsics.checkNotNullParameter((Object)single, "<this>");
        composer.startReplaceableGroup(919948588);
        ComposerKt.sourceInformation(composer, "C(subscribeAsState)92@3972L34:RxJava3Adapter.kt#wvx964");
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventStart(919948588, n, -1, "androidx.compose.runtime.rxjava3.subscribeAsState (RxJava3Adapter.kt:91)");
        }
        composer.startReplaceableGroup(-675894395);
        ComposerKt.sourceInformation(composer, "CC(asState)136@5846L36,137@5887L149:RxJava3Adapter.kt#wvx964");
        composer.startReplaceableGroup(-492369756);
        ComposerKt.sourceInformation(composer, "CC(remember):Composables.kt#9igjgp");
        Object o;
        if ((o = composer.rememberedValue()) == Composer.Companion.getEmpty()) {
            o = SnapshotStateKt.mutableStateOf$default(r, null, 2, null);
            composer.updateRememberedValue(o);
        }
        composer.endReplaceableGroup();
        final MutableState mutableState = (MutableState)o;
        EffectsKt.DisposableEffect(single, (Function1<? super DisposableEffectScope, ? extends DisposableEffectResult>)new RxJava3AdapterKt$subscribeAsState$$inlined$asState.RxJava3AdapterKt$subscribeAsState$$inlined$asState$3((Object)single, mutableState), composer, ((n & 0x70) | ((n >> 3 & 0x8) << 3 | 0x8)) & 0xE);
        composer.endReplaceableGroup();
        final State state = mutableState;
        if (ComposerKt.isTraceInProgress()) {
            ComposerKt.traceEventEnd();
        }
        composer.endReplaceableGroup();
        return state;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext;
import java.util.Collection;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Result$Companion;
import kotlin.coroutines.Continuation;
import kotlin.ResultKt;
import kotlin.Result;
import java.util.concurrent.CancellationException;
import java.util.ArrayList;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001:\u0001 B\u0017\u0012\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0014\u0010\u0012\u001a\u00020\u00042\f\b\u0002\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015J\u0010\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\nH\u0002J\u000e\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u001aJ+\u0010\u001b\u001a\u0002H\u001c\"\u0004\b\u0000\u0010\u001c2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u0002H\u001c0\u001eH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001fR\u0018\u0010\u0006\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b0\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u000b\u001a\u00020\f8F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0011\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b0\u0007X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006!" }, d2 = { "Landroidx/compose/runtime/BroadcastFrameClock;", "Landroidx/compose/runtime/MonotonicFrameClock;", "onNewAwaiters", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "awaiters", "", "Landroidx/compose/runtime/BroadcastFrameClock$FrameAwaiter;", "failureCause", "", "hasAwaiters", "", "getHasAwaiters", "()Z", "lock", "", "spareList", "cancel", "cancellationException", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "fail", "cause", "sendFrame", "timeNanos", "", "withFrameNanos", "R", "onFrame", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "FrameAwaiter", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class BroadcastFrameClock implements MonotonicFrameClock
{
    public static final int $stable = 8;
    private List<FrameAwaiter<?>> awaiters;
    private Throwable failureCause;
    private final Object lock;
    private final Function0<Unit> onNewAwaiters;
    private List<FrameAwaiter<?>> spareList;
    
    public BroadcastFrameClock() {
        this(null, 1, null);
    }
    
    public BroadcastFrameClock(final Function0<Unit> onNewAwaiters) {
        this.onNewAwaiters = onNewAwaiters;
        this.lock = new Object();
        this.awaiters = new ArrayList<FrameAwaiter<?>>();
        this.spareList = new ArrayList<FrameAwaiter<?>>();
    }
    
    public static final /* synthetic */ List access$getAwaiters$p(final BroadcastFrameClock broadcastFrameClock) {
        return broadcastFrameClock.awaiters;
    }
    
    public static final /* synthetic */ Throwable access$getFailureCause$p(final BroadcastFrameClock broadcastFrameClock) {
        return broadcastFrameClock.failureCause;
    }
    
    public static final /* synthetic */ Object access$getLock$p(final BroadcastFrameClock broadcastFrameClock) {
        return broadcastFrameClock.lock;
    }
    
    public static final /* synthetic */ Function0 access$getOnNewAwaiters$p(final BroadcastFrameClock broadcastFrameClock) {
        return broadcastFrameClock.onNewAwaiters;
    }
    
    private final void fail(final Throwable failureCause) {
        synchronized (this.lock) {
            if (this.failureCause != null) {
                return;
            }
            this.failureCause = failureCause;
            final List<FrameAwaiter<?>> awaiters = this.awaiters;
            for (int i = 0; i < awaiters.size(); ++i) {
                final Continuation continuation = awaiters.get(i).getContinuation();
                final Result$Companion companion = Result.Companion;
                continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(failureCause)));
            }
            this.awaiters.clear();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void cancel(final CancellationException ex) {
        Intrinsics.checkNotNullParameter((Object)ex, "cancellationException");
        this.fail(ex);
    }
    
    public <R> R fold(final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
        return DefaultImpls.fold(this, r, function2);
    }
    
    public <E extends CoroutineContext$Element> E get(final CoroutineContext$Key<E> coroutineContext$Key) {
        return DefaultImpls.get(this, coroutineContext$Key);
    }
    
    public final boolean getHasAwaiters() {
        synchronized (this.lock) {
            final boolean empty = this.awaiters.isEmpty();
            monitorexit(this.lock);
            return empty ^ true;
        }
    }
    
    public CoroutineContext minusKey(final CoroutineContext$Key<?> coroutineContext$Key) {
        return DefaultImpls.minusKey(this, coroutineContext$Key);
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return DefaultImpls.plus(this, coroutineContext);
    }
    
    public final void sendFrame(final long n) {
        synchronized (this.lock) {
            final List<FrameAwaiter<?>> awaiters = this.awaiters;
            this.awaiters = this.spareList;
            this.spareList = awaiters;
            for (int i = 0; i < awaiters.size(); ++i) {
                ((FrameAwaiter)awaiters.get(i)).resume(n);
            }
            awaiters.clear();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Override
    public <R> Object withFrameNanos(final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        synchronized (access$getLock$p(this)) {
            final Throwable access$getFailureCause$p = access$getFailureCause$p(this);
            if (access$getFailureCause$p != null) {
                final Continuation continuation2 = (Continuation)cancellableContinuation;
                final Result$Companion companion = Result.Companion;
                continuation2.resumeWith(Result.constructor-impl(ResultKt.createFailure(access$getFailureCause$p)));
                monitorexit(access$getLock$p(this));
            }
            else {
                ref$ObjectRef.element = new FrameAwaiter(function1, (kotlin.coroutines.Continuation<? super Object>)cancellableContinuation);
                final boolean b = !access$getAwaiters$p(this).isEmpty();
                final List access$getAwaiters$p = access$getAwaiters$p(this);
                Object o;
                if (ref$ObjectRef.element == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("awaiter");
                    o = null;
                }
                else {
                    o = ref$ObjectRef.element;
                }
                access$getAwaiters$p.add(o);
                monitorexit(access$getLock$p(this));
                cancellableContinuation.invokeOnCancellation((Function1)new BroadcastFrameClock$withFrameNanos$2.BroadcastFrameClock$withFrameNanos$2$1(this, ref$ObjectRef));
                if ((b ^ true) && access$getOnNewAwaiters$p(this) != null) {
                    try {
                        access$getOnNewAwaiters$p(this).invoke();
                    }
                    finally {
                        final Throwable t;
                        this.fail(t);
                    }
                }
            }
            final Object result = cancellableContinuationImpl.getResult();
            if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
            }
            return result;
        }
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B'\u0012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u00000\u0004\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0005R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0010" }, d2 = { "Landroidx/compose/runtime/BroadcastFrameClock$FrameAwaiter;", "R", "", "onFrame", "Lkotlin/Function1;", "", "continuation", "Lkotlin/coroutines/Continuation;", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V", "getContinuation", "()Lkotlin/coroutines/Continuation;", "getOnFrame", "()Lkotlin/jvm/functions/Function1;", "resume", "", "timeNanos", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class FrameAwaiter<R>
    {
        private final Continuation<R> continuation;
        private final Function1<Long, R> onFrame;
        
        public FrameAwaiter(final Function1<? super Long, ? extends R> onFrame, final Continuation<? super R> continuation) {
            Intrinsics.checkNotNullParameter((Object)onFrame, "onFrame");
            Intrinsics.checkNotNullParameter((Object)continuation, "continuation");
            this.onFrame = (Function1<Long, R>)onFrame;
            this.continuation = (Continuation<R>)continuation;
        }
        
        public final Continuation<R> getContinuation() {
            return this.continuation;
        }
        
        public final Function1<Long, R> getOnFrame() {
            return this.onFrame;
        }
        
        public final void resume(final long l) {
            final Continuation<R> continuation = this.continuation;
            Object constructor-impl;
            try {
                final Result$Companion companion = Result.Companion;
                final FrameAwaiter frameAwaiter = this;
                Result.constructor-impl(this.onFrame.invoke((Object)l));
            }
            finally {
                final Result$Companion companion2 = Result.Companion;
                final Throwable t;
                constructor-impl = Result.constructor-impl(ResultKt.createFailure(t));
            }
            continuation.resumeWith(constructor-impl);
        }
    }
}

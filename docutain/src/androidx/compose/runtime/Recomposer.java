// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import androidx.compose.runtime.tooling.CompositionData;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.flow.StateFlow;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import kotlinx.coroutines.flow.FlowKt;
import kotlinx.coroutines.flow.Flow;
import kotlin.ResultKt;
import kotlinx.coroutines.BuildersKt;
import kotlin.jvm.functions.Function2;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlinx.coroutines.CancellableContinuation$DefaultImpls;
import kotlin.jvm.internal.InlineMarker;
import androidx.compose.runtime.snapshots.Snapshot;
import kotlin.Result$Companion;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.Result;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import androidx.compose.runtime.snapshots.SnapshotApplyResult;
import androidx.compose.runtime.snapshots.MutableSnapshot;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function3;
import androidx.compose.runtime.collection.IdentityArraySet;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.JobKt;
import kotlin.coroutines.CoroutineContext$Key;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.flow.StateFlowKt;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import java.util.Set;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.CompletableJob;
import kotlin.coroutines.CoroutineContext;
import java.util.Map;
import java.util.List;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentSet;
import kotlinx.coroutines.flow.MutableStateFlow;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0098\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\r\b\u0007\u0018\u0000 ¦\u00012\u00020\u0001:\n¦\u0001§\u0001¨\u0001©\u0001ª\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010O\u001a\u00020N2\u0006\u0010P\u001a\u00020QH\u0002J\u0006\u0010R\u001a\u00020SJ\u0011\u0010T\u001a\u00020NH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010UJ\u0011\u0010V\u001a\u00020NH\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010UJ\u0006\u0010W\u001a\u00020NJ\u0006\u0010X\u001a\u00020NJ*\u0010Y\u001a\u00020N2\u0006\u0010Z\u001a\u00020\u00172\u0011\u0010[\u001a\r\u0012\u0004\u0012\u00020N0\\¢\u0006\u0002\b]H\u0010¢\u0006\u0004\b^\u0010_J:\u0010`\u001a\u0002Ha\"\u0004\b\u0000\u0010a2\u0006\u0010Z\u001a\u00020\u00172\u000e\u0010b\u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010c2\f\u0010d\u001a\b\u0012\u0004\u0012\u0002Ha0\\H\u0082\b¢\u0006\u0002\u0010eJ\u0015\u0010f\u001a\u00020N2\u0006\u0010g\u001a\u00020\u001aH\u0010¢\u0006\u0002\bhJ\u0010\u0010i\u001a\n\u0012\u0004\u0012\u00020N\u0018\u00010MH\u0002J\b\u0010j\u001a\u00020NH\u0002J\u0015\u0010k\u001a\u00020N2\u0006\u0010g\u001a\u00020\u001aH\u0010¢\u0006\u0002\blJ\u0015\u0010m\u001a\u00020N2\u0006\u0010Z\u001a\u00020\u0017H\u0010¢\u0006\u0002\bnJ\u0015\u0010o\u001a\u00020N2\u0006\u0010p\u001a\u00020qH\u0010¢\u0006\u0002\brJ\u0011\u0010s\u001a\u00020NH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010UJ\u001d\u0010t\u001a\u00020N2\u0006\u0010g\u001a\u00020\u001a2\u0006\u0010u\u001a\u00020\u001bH\u0010¢\u0006\u0002\bvJ\u0017\u0010w\u001a\u0004\u0018\u00010\u001b2\u0006\u0010g\u001a\u00020\u001aH\u0010¢\u0006\u0002\bxJ\u0010\u0010y\u001a\u00020N2\u0006\u0010Z\u001a\u00020\u0017H\u0002J,\u0010z\u001a\b\u0012\u0004\u0012\u00020\u00170{2\f\u0010|\u001a\b\u0012\u0004\u0012\u00020\u001a0{2\u000e\u0010b\u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010cH\u0002J\"\u0010}\u001a\u0004\u0018\u00010\u00172\u0006\u0010Z\u001a\u00020\u00172\u000e\u0010b\u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010cH\u0002J.\u0010~\u001a\u00020N2\f\u0010\u007f\u001a\b0\u0080\u0001j\u0003`\u0081\u00012\u000b\b\u0002\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00172\t\b\u0002\u0010\u0083\u0001\u001a\u00020\u0012H\u0002J\u001e\u0010\u0084\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020N0\u0085\u00012\u0006\u0010Z\u001a\u00020\u0017H\u0002JY\u0010\u0086\u0001\u001a\u00020N2D\u0010d\u001a@\b\u0001\u0012\u0005\u0012\u00030\u0088\u0001\u0012\u0017\u0012\u00150\u0089\u0001¢\u0006\u000f\b\u008a\u0001\u0012\n\b\u008b\u0001\u0012\u0005\b\b(\u008c\u0001\u0012\u000b\u0012\t\u0012\u0004\u0012\u00020N0\u008d\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u0087\u0001¢\u0006\u0003\b\u008e\u0001H\u0082@\u00f8\u0001\u0000¢\u0006\u0003\u0010\u008f\u0001J\t\u0010\u0090\u0001\u001a\u00020NH\u0002J \u0010\u0090\u0001\u001a\u00020N2\u0014\u0010\u0091\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020N0\u0085\u0001H\u0082\bJ\u001f\u0010\u0092\u0001\u001a\u00020N2\u000e\u0010\u0093\u0001\u001a\t\u0012\u0005\u0012\u00030\u0094\u00010DH\u0010¢\u0006\u0003\b\u0095\u0001J\u0017\u0010\u0096\u0001\u001a\u00020N2\u0006\u0010Z\u001a\u00020\u0017H\u0010¢\u0006\u0003\b\u0097\u0001J\u0012\u0010\u0098\u0001\u001a\u00020N2\u0007\u0010\u0099\u0001\u001a\u00020@H\u0002J\u000b\u0010\u009a\u0001\u001a\u0004\u0018\u00010/H\u0002J\t\u0010\u009b\u0001\u001a\u00020NH\u0002J'\u0010\u009c\u0001\u001a\u00020N2\b\u0010\u008c\u0001\u001a\u00030\u0089\u00012\b\u0010\u009d\u0001\u001a\u00030\u009e\u0001H\u0082@\u00f8\u0001\u0000¢\u0006\u0003\u0010\u009f\u0001J\u0012\u0010 \u0001\u001a\u00020NH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010UJ\u001b\u0010¡\u0001\u001a\u00020N2\u0006\u0010;\u001a\u00020\u0003H\u0087@\u00f8\u0001\u0000¢\u0006\u0003\u0010¢\u0001J\u0017\u0010£\u0001\u001a\u00020N2\u0006\u0010Z\u001a\u00020\u0017H\u0010¢\u0006\u0003\b¤\u0001J.\u0010¥\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020N0\u0085\u00012\u0006\u0010Z\u001a\u00020\u00172\u000e\u0010b\u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010cH\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u00020\u00128PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R(\u0010\u001d\u001a\u001c\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001a0\u00160\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010!\u001a\u00020\"8PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$R\u000e\u0010%\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00070'8F¢\u0006\u0006\u001a\u0004\b(\u0010)R\u0014\u0010\u0002\u001a\u00020\u0003X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u000e\u0010,\u001a\u00020-X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010/X\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u00100\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u00101\u001a\u00020\u00128BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b2\u0010\u0014R\u0014\u00103\u001a\u00020\u00128BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b4\u0010\u0014R\u0011\u00105\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b6\u0010\u0014R\u0014\u00107\u001a\u00020\u00128BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b8\u0010\u0014R\u000e\u00109\u001a\u00020\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010;\u001a\u00020\u00038PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b<\u0010+R\u0012\u0010=\u001a\u00060>R\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010?\u001a\u0004\u0018\u00010@X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010A\u001a\u00020\u00128BX\u0082\u0004¢\u0006\u0006\u001a\u0004\bB\u0010\u0014R\u0014\u0010C\u001a\b\u0012\u0004\u0012\u00020\u001f0DX\u0082\u000e¢\u0006\u0002\n\u0000R \u0010E\u001a\b\u0012\u0004\u0012\u00020\u00070F8FX\u0087\u0004¢\u0006\f\u0012\u0004\bG\u0010H\u001a\u0004\bI\u0010JR\u000e\u0010K\u001a\u00020\u001fX\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010L\u001a\n\u0012\u0004\u0012\u00020N\u0018\u00010MX\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006«\u0001" }, d2 = { "Landroidx/compose/runtime/Recomposer;", "Landroidx/compose/runtime/CompositionContext;", "effectCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "(Lkotlin/coroutines/CoroutineContext;)V", "_state", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Landroidx/compose/runtime/Recomposer$State;", "broadcastFrameClock", "Landroidx/compose/runtime/BroadcastFrameClock;", "<set-?>", "", "changeCount", "getChangeCount", "()J", "closeCause", "", "collectingParameterInformation", "", "getCollectingParameterInformation$runtime_release", "()Z", "compositionInvalidations", "", "Landroidx/compose/runtime/ControlledComposition;", "compositionValueStatesAvailable", "", "Landroidx/compose/runtime/MovableContentStateReference;", "Landroidx/compose/runtime/MovableContentState;", "compositionValuesAwaitingInsert", "compositionValuesRemoved", "Landroidx/compose/runtime/MovableContent;", "", "compositionsAwaitingApply", "compoundHashKey", "", "getCompoundHashKey$runtime_release", "()I", "concurrentCompositionsOutstanding", "currentState", "Lkotlinx/coroutines/flow/StateFlow;", "getCurrentState", "()Lkotlinx/coroutines/flow/StateFlow;", "getEffectCoroutineContext$runtime_release", "()Lkotlin/coroutines/CoroutineContext;", "effectJob", "Lkotlinx/coroutines/CompletableJob;", "errorState", "Landroidx/compose/runtime/Recomposer$RecomposerErrorState;", "failedCompositions", "hasConcurrentFrameWorkLocked", "getHasConcurrentFrameWorkLocked", "hasFrameWorkLocked", "getHasFrameWorkLocked", "hasPendingWork", "getHasPendingWork", "hasSchedulingWork", "getHasSchedulingWork", "isClosed", "knownCompositions", "recomposeCoroutineContext", "getRecomposeCoroutineContext$runtime_release", "recomposerInfo", "Landroidx/compose/runtime/Recomposer$RecomposerInfoImpl;", "runnerJob", "Lkotlinx/coroutines/Job;", "shouldKeepRecomposing", "getShouldKeepRecomposing", "snapshotInvalidations", "", "state", "Lkotlinx/coroutines/flow/Flow;", "getState$annotations", "()V", "getState", "()Lkotlinx/coroutines/flow/Flow;", "stateLock", "workContinuation", "Lkotlinx/coroutines/CancellableContinuation;", "", "applyAndCheck", "snapshot", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "asRecomposerInfo", "Landroidx/compose/runtime/RecomposerInfo;", "awaitIdle", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "awaitWorkAvailable", "cancel", "close", "composeInitial", "composition", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "composeInitial$runtime_release", "(Landroidx/compose/runtime/ControlledComposition;Lkotlin/jvm/functions/Function2;)V", "composing", "T", "modifiedValues", "Landroidx/compose/runtime/collection/IdentityArraySet;", "block", "(Landroidx/compose/runtime/ControlledComposition;Landroidx/compose/runtime/collection/IdentityArraySet;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "deletedMovableContent", "reference", "deletedMovableContent$runtime_release", "deriveStateLocked", "discardUnusedValues", "insertMovableContent", "insertMovableContent$runtime_release", "invalidate", "invalidate$runtime_release", "invalidateScope", "scope", "Landroidx/compose/runtime/RecomposeScopeImpl;", "invalidateScope$runtime_release", "join", "movableContentStateReleased", "data", "movableContentStateReleased$runtime_release", "movableContentStateResolve", "movableContentStateResolve$runtime_release", "performInitialMovableContentInserts", "performInsertValues", "", "references", "performRecompose", "processCompositionError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "failedInitialComposition", "recoverable", "readObserverOf", "Lkotlin/Function1;", "recompositionRunner", "Lkotlin/Function3;", "Lkotlinx/coroutines/CoroutineScope;", "Landroidx/compose/runtime/MonotonicFrameClock;", "Lkotlin/ParameterName;", "name", "parentFrameClock", "Lkotlin/coroutines/Continuation;", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/jvm/functions/Function3;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "recordComposerModificationsLocked", "onEachInvalidComposition", "recordInspectionTable", "table", "Landroidx/compose/runtime/tooling/CompositionData;", "recordInspectionTable$runtime_release", "registerComposition", "registerComposition$runtime_release", "registerRunnerJob", "callingJob", "resetErrorState", "retryFailedCompositions", "runFrameLoop", "frameSignal", "Landroidx/compose/runtime/ProduceFrameSignal;", "(Landroidx/compose/runtime/MonotonicFrameClock;Landroidx/compose/runtime/ProduceFrameSignal;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runRecomposeAndApplyChanges", "runRecomposeConcurrentlyAndApplyChanges", "(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "unregisterComposition", "unregisterComposition$runtime_release", "writeObserverOf", "Companion", "HotReloadable", "RecomposerErrorState", "RecomposerInfoImpl", "State", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Recomposer extends CompositionContext
{
    public static final int $stable;
    public static final Companion Companion;
    private static final AtomicReference<Boolean> _hotReloadEnabled;
    private static final MutableStateFlow<PersistentSet<RecomposerInfoImpl>> _runningRecomposers;
    private final MutableStateFlow<State> _state;
    private final BroadcastFrameClock broadcastFrameClock;
    private long changeCount;
    private Throwable closeCause;
    private final List<ControlledComposition> compositionInvalidations;
    private final Map<MovableContentStateReference, MovableContentState> compositionValueStatesAvailable;
    private final List<MovableContentStateReference> compositionValuesAwaitingInsert;
    private final Map<MovableContent<Object>, List<MovableContentStateReference>> compositionValuesRemoved;
    private final List<ControlledComposition> compositionsAwaitingApply;
    private int concurrentCompositionsOutstanding;
    private final CoroutineContext effectCoroutineContext;
    private final CompletableJob effectJob;
    private RecomposerErrorState errorState;
    private List<ControlledComposition> failedCompositions;
    private boolean isClosed;
    private final List<ControlledComposition> knownCompositions;
    private final RecomposerInfoImpl recomposerInfo;
    private Job runnerJob;
    private Set<Object> snapshotInvalidations;
    private final Object stateLock;
    private CancellableContinuation<? super Unit> workContinuation;
    
    static {
        Companion = new Companion(null);
        $stable = 8;
        _runningRecomposers = StateFlowKt.MutableStateFlow((Object)ExtensionsKt.persistentSetOf());
        _hotReloadEnabled = new AtomicReference<Boolean>(false);
    }
    
    public Recomposer(final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "effectCoroutineContext");
        final BroadcastFrameClock broadcastFrameClock = new BroadcastFrameClock((Function0<Unit>)new Recomposer$broadcastFrameClock.Recomposer$broadcastFrameClock$1(this));
        this.broadcastFrameClock = broadcastFrameClock;
        this.stateLock = new Object();
        this.knownCompositions = new ArrayList<ControlledComposition>();
        this.snapshotInvalidations = new LinkedHashSet<Object>();
        this.compositionInvalidations = new ArrayList<ControlledComposition>();
        this.compositionsAwaitingApply = new ArrayList<ControlledComposition>();
        this.compositionValuesAwaitingInsert = new ArrayList<MovableContentStateReference>();
        this.compositionValuesRemoved = new LinkedHashMap<MovableContent<Object>, List<MovableContentStateReference>>();
        this.compositionValueStatesAvailable = new LinkedHashMap<MovableContentStateReference, MovableContentState>();
        this._state = (MutableStateFlow<State>)StateFlowKt.MutableStateFlow((Object)State.Inactive);
        final CompletableJob job = JobKt.Job((Job)coroutineContext.get((CoroutineContext$Key)Job.Key));
        job.invokeOnCompletion((Function1)new Recomposer$effectJob$1.Recomposer$effectJob$1$1(this));
        this.effectJob = job;
        this.effectCoroutineContext = coroutineContext.plus((CoroutineContext)broadcastFrameClock).plus((CoroutineContext)job);
        this.recomposerInfo = new RecomposerInfoImpl();
    }
    
    public static final /* synthetic */ List access$getCompositionInvalidations$p(final Recomposer recomposer) {
        return recomposer.compositionInvalidations;
    }
    
    public static final /* synthetic */ RecomposerErrorState access$getErrorState$p(final Recomposer recomposer) {
        return recomposer.errorState;
    }
    
    public static final /* synthetic */ List access$getKnownCompositions$p(final Recomposer recomposer) {
        return recomposer.knownCompositions;
    }
    
    public static final /* synthetic */ Set access$getSnapshotInvalidations$p(final Recomposer recomposer) {
        return recomposer.snapshotInvalidations;
    }
    
    public static final /* synthetic */ Object access$getStateLock$p(final Recomposer recomposer) {
        return recomposer.stateLock;
    }
    
    public static final /* synthetic */ AtomicReference access$get_hotReloadEnabled$cp() {
        return Recomposer._hotReloadEnabled;
    }
    
    public static final /* synthetic */ MutableStateFlow access$get_runningRecomposers$cp() {
        return Recomposer._runningRecomposers;
    }
    
    public static final /* synthetic */ void access$setSnapshotInvalidations$p(final Recomposer recomposer, final Set snapshotInvalidations) {
        recomposer.snapshotInvalidations = snapshotInvalidations;
    }
    
    public static final /* synthetic */ void access$setWorkContinuation$p(final Recomposer recomposer, final CancellableContinuation workContinuation) {
        recomposer.workContinuation = (CancellableContinuation<? super Unit>)workContinuation;
    }
    
    private final void applyAndCheck(final MutableSnapshot mutableSnapshot) {
        try {
            if (!(mutableSnapshot.apply() instanceof SnapshotApplyResult.Failure)) {
                return;
            }
            throw new IllegalStateException("Unsupported concurrent change during composition. A state object was modified by composition as well as being modified outside composition.".toString());
        }
        finally {
            mutableSnapshot.dispose();
        }
    }
    
    private final Object awaitWorkAvailable(final Continuation<? super Unit> continuation) {
        if (!this.getHasSchedulingWork()) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
            cancellableContinuationImpl.initCancellability();
            final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
            Object o = access$getStateLock$p(this);
            synchronized (o) {
                if (this.getHasSchedulingWork()) {
                    final Continuation continuation2 = (Continuation)cancellableContinuation;
                    final Result$Companion companion = Result.Companion;
                    continuation2.resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
                }
                else {
                    access$setWorkContinuation$p(this, cancellableContinuation);
                }
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                o = cancellableContinuationImpl.getResult();
                if (o == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
                }
                if (o == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    return o;
                }
                return Unit.INSTANCE;
            }
        }
        return Unit.INSTANCE;
    }
    
    private final <T> T composing(ControlledComposition takeMutableSnapshot, IdentityArraySet<Object> current, final Function0<? extends T> function0) {
        takeMutableSnapshot = (ControlledComposition)Snapshot.Companion.takeMutableSnapshot(this.readObserverOf(takeMutableSnapshot), this.writeObserverOf(takeMutableSnapshot, (IdentityArraySet<Object>)current));
        try {
            final Snapshot snapshot = (Snapshot)takeMutableSnapshot;
            current = snapshot.makeCurrent();
            try {
                return (T)function0.invoke();
            }
            finally {
                InlineMarker.finallyStart(1);
                snapshot.restoreCurrent(current);
                InlineMarker.finallyEnd(1);
            }
        }
        finally {
            InlineMarker.finallyStart(1);
            this.applyAndCheck((MutableSnapshot)takeMutableSnapshot);
            InlineMarker.finallyEnd(1);
        }
    }
    
    private final CancellableContinuation<Unit> deriveStateLocked() {
        final int compareTo = ((State)this._state.getValue()).compareTo((State)State.ShuttingDown);
        CancellableContinuation<? super Unit> workContinuation = null;
        if (compareTo <= 0) {
            this.knownCompositions.clear();
            this.snapshotInvalidations = new LinkedHashSet<Object>();
            this.compositionInvalidations.clear();
            this.compositionsAwaitingApply.clear();
            this.compositionValuesAwaitingInsert.clear();
            this.failedCompositions = null;
            final CancellableContinuation<? super Unit> workContinuation2 = this.workContinuation;
            if (workContinuation2 != null) {
                CancellableContinuation$DefaultImpls.cancel$default((CancellableContinuation)workContinuation2, (Throwable)null, 1, (Object)null);
            }
            this.workContinuation = null;
            this.errorState = null;
            return null;
        }
        State value;
        if (this.errorState != null) {
            value = State.Inactive;
        }
        else if (this.runnerJob == null) {
            this.snapshotInvalidations = new LinkedHashSet<Object>();
            this.compositionInvalidations.clear();
            if (this.broadcastFrameClock.getHasAwaiters()) {
                value = State.InactivePendingWork;
            }
            else {
                value = State.Inactive;
            }
        }
        else if (!(this.compositionInvalidations.isEmpty() ^ true) && !(this.snapshotInvalidations.isEmpty() ^ true) && !(this.compositionsAwaitingApply.isEmpty() ^ true) && !(this.compositionValuesAwaitingInsert.isEmpty() ^ true) && this.concurrentCompositionsOutstanding <= 0 && !this.broadcastFrameClock.getHasAwaiters()) {
            value = State.Idle;
        }
        else {
            value = State.PendingWork;
        }
        this._state.setValue((Object)value);
        if (value == State.PendingWork) {
            workContinuation = this.workContinuation;
            this.workContinuation = null;
        }
        return (CancellableContinuation<Unit>)workContinuation;
    }
    
    private final void discardUnusedValues() {
        Object stateLock = this.stateLock;
        synchronized (stateLock) {
            final boolean empty = this.compositionValuesRemoved.isEmpty();
            final int n = 0;
            List emptyList;
            if (empty ^ true) {
                final List flatten = CollectionsKt.flatten((Iterable)this.compositionValuesRemoved.values());
                this.compositionValuesRemoved.clear();
                final ArrayList list = new ArrayList(flatten.size());
                for (int size = flatten.size(), i = 0; i < size; ++i) {
                    final Object value = flatten.get(i);
                    final Collection collection = list;
                    final MovableContentStateReference movableContentStateReference = (MovableContentStateReference)value;
                    collection.add(TuplesKt.to((Object)movableContentStateReference, (Object)this.compositionValueStatesAvailable.get(movableContentStateReference)));
                }
                emptyList = list;
                this.compositionValueStatesAvailable.clear();
            }
            else {
                emptyList = CollectionsKt.emptyList();
            }
            monitorexit(stateLock);
            for (int size2 = emptyList.size(), j = n; j < size2; ++j) {
                final Pair pair = emptyList.get(j);
                stateLock = pair.component1();
                final MovableContentState movableContentState = (MovableContentState)pair.component2();
                if (movableContentState != null) {
                    ((MovableContentStateReference)stateLock).getComposition$runtime_release().disposeUnusedMovableContent(movableContentState);
                }
            }
        }
    }
    
    private final boolean getHasConcurrentFrameWorkLocked() {
        final boolean empty = this.compositionsAwaitingApply.isEmpty();
        boolean b = true;
        if (!(empty ^ true)) {
            b = (this.broadcastFrameClock.getHasAwaiters() && b);
        }
        return b;
    }
    
    private final boolean getHasFrameWorkLocked() {
        final boolean empty = this.compositionInvalidations.isEmpty();
        boolean b = true;
        if (!(empty ^ true)) {
            b = (this.broadcastFrameClock.getHasAwaiters() && b);
        }
        return b;
    }
    
    private final boolean getHasSchedulingWork() {
        synchronized (this.stateLock) {
            final boolean empty = this.snapshotInvalidations.isEmpty();
            boolean b2;
            final boolean b = b2 = true;
            if (!(empty ^ true)) {
                b2 = b;
                if (!(this.compositionInvalidations.isEmpty() ^ true)) {
                    b2 = (this.broadcastFrameClock.getHasAwaiters() && b);
                }
            }
            return b2;
        }
    }
    
    private final boolean getShouldKeepRecomposing() {
        Object o = this.stateLock;
        synchronized (o) {
            final boolean isClosed = this.isClosed;
            final boolean b = true;
            monitorexit(o);
            boolean b2 = b;
            if (!(isClosed ^ true)) {
                o = this.effectJob.getChildren().iterator();
                while (((Iterator)o).hasNext()) {
                    if (((Job)((Iterator)o).next()).isActive()) {
                        final boolean b3 = true;
                        b2 = (b3 && b);
                        return b2;
                    }
                }
                final boolean b3 = false;
                return b3 && b;
            }
            return b2;
        }
    }
    
    private final void performInitialMovableContentInserts(final ControlledComposition controlledComposition) {
        Object stateLock = this.stateLock;
        synchronized (stateLock) {
            final List<MovableContentStateReference> compositionValuesAwaitingInsert = this.compositionValuesAwaitingInsert;
            final int size = compositionValuesAwaitingInsert.size();
            final int n = 0;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= size) {
                    break;
                }
                if (Intrinsics.areEqual((Object)((MovableContentStateReference)compositionValuesAwaitingInsert.get(n2)).getComposition$runtime_release(), (Object)controlledComposition)) {
                    n3 = 1;
                    break;
                }
                ++n2;
            }
            if (n3 == 0) {
                return;
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(stateLock);
            stateLock = new ArrayList<Object>();
            performInitialMovableContentInserts$fillToInsert((List<MovableContentStateReference>)stateLock, this, controlledComposition);
            while (((Collection)stateLock).isEmpty() ^ true) {
                this.performInsertValues((List<MovableContentStateReference>)stateLock, null);
                performInitialMovableContentInserts$fillToInsert((List<MovableContentStateReference>)stateLock, this, controlledComposition);
            }
        }
    }
    
    private static final void performInitialMovableContentInserts$fillToInsert(final List<MovableContentStateReference> list, final Recomposer recomposer, final ControlledComposition controlledComposition) {
        list.clear();
        synchronized (recomposer.stateLock) {
            final Iterator<MovableContentStateReference> iterator = recomposer.compositionValuesAwaitingInsert.iterator();
            while (iterator.hasNext()) {
                final MovableContentStateReference movableContentStateReference = iterator.next();
                if (Intrinsics.areEqual((Object)movableContentStateReference.getComposition$runtime_release(), (Object)controlledComposition)) {
                    list.add(movableContentStateReference);
                    iterator.remove();
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    private final List<ControlledComposition> performInsertValues(final List<MovableContentStateReference> list, final IdentityArraySet<Object> set) {
        final HashMap hashMap = new HashMap(list.size());
        for (int size = list.size(), i = 0; i < size; ++i) {
            final MovableContentStateReference value = list.get(i);
            final ControlledComposition composition$runtime_release = value.getComposition$runtime_release();
            final Map map = hashMap;
            Object value2;
            if ((value2 = map.get(composition$runtime_release)) == null) {
                value2 = new ArrayList();
                map.put(composition$runtime_release, value2);
            }
            ((ArrayList)value2).add(value);
        }
        final Map map2 = hashMap;
        for (Map.Entry<ControlledComposition, V> entry : map2.entrySet()) {
            final ControlledComposition controlledComposition = entry.getKey();
            final List list2 = (List)entry.getValue();
            ComposerKt.runtimeCheck(controlledComposition.isComposing() ^ true);
            final MutableSnapshot takeMutableSnapshot = Snapshot.Companion.takeMutableSnapshot(this.readObserverOf(controlledComposition), this.writeObserverOf(controlledComposition, set));
            try {
                final Snapshot snapshot = takeMutableSnapshot;
                final Snapshot current = snapshot.makeCurrent();
                try {
                    synchronized (this.stateLock) {
                        final ArrayList list3 = new ArrayList(list2.size());
                        for (int size2 = list2.size(), j = 0; j < size2; ++j) {
                            final Object value3 = list2.get(j);
                            final Collection collection = list3;
                            final MovableContentStateReference movableContentStateReference = (MovableContentStateReference)value3;
                            collection.add(TuplesKt.to((Object)movableContentStateReference, (Object)RecomposerKt.removeLastMultiValue(this.compositionValuesRemoved, movableContentStateReference.getContent$runtime_release())));
                        }
                        final List list4 = list3;
                        monitorexit(this.stateLock);
                        controlledComposition.insertMovableContent(list4);
                        final Unit instance = Unit.INSTANCE;
                    }
                }
                finally {
                    snapshot.restoreCurrent(current);
                }
            }
            finally {
                this.applyAndCheck(takeMutableSnapshot);
            }
            break;
        }
        return CollectionsKt.toList((Iterable)map2.keySet());
    }
    
    private final ControlledComposition performRecompose(ControlledComposition controlledComposition, final IdentityArraySet<Object> set) {
        if (!controlledComposition.isComposing()) {
            if (!controlledComposition.isDisposed()) {
                final MutableSnapshot takeMutableSnapshot = Snapshot.Companion.takeMutableSnapshot(this.readObserverOf(controlledComposition), this.writeObserverOf(controlledComposition, set));
                try {
                    final Snapshot snapshot = takeMutableSnapshot;
                    final Snapshot current = snapshot.makeCurrent();
                    boolean b = true;
                    Label_0077: {
                        Label_0075: {
                            if (set != null) {
                                Label_0137: {
                                    try {
                                        if (set.isNotEmpty()) {
                                            break Label_0077;
                                        }
                                    }
                                    finally {
                                        break Label_0137;
                                    }
                                    break Label_0075;
                                }
                                snapshot.restoreCurrent(current);
                            }
                        }
                        b = false;
                    }
                    if (b) {
                        controlledComposition.prepareCompose((Function0<Unit>)new Recomposer$performRecompose$1.Recomposer$performRecompose$1$1((IdentityArraySet)set, controlledComposition));
                    }
                    final boolean recompose = controlledComposition.recompose();
                    snapshot.restoreCurrent(current);
                    this.applyAndCheck(takeMutableSnapshot);
                    if (!recompose) {
                        controlledComposition = null;
                    }
                    return controlledComposition;
                }
                finally {
                    this.applyAndCheck(takeMutableSnapshot);
                }
            }
        }
        return null;
    }
    
    private final void processCompositionError(final Exception ex, final ControlledComposition controlledComposition, final boolean b) {
        final Boolean value = Recomposer._hotReloadEnabled.get();
        Intrinsics.checkNotNullExpressionValue((Object)value, "_hotReloadEnabled.get()");
        if (value && !(ex instanceof ComposeRuntimeError)) {
            synchronized (this.stateLock) {
                ActualAndroid_androidKt.logError("Error was captured in composition while live edit was enabled.", ex);
                this.compositionsAwaitingApply.clear();
                this.compositionInvalidations.clear();
                this.snapshotInvalidations = new LinkedHashSet<Object>();
                this.compositionValuesAwaitingInsert.clear();
                this.compositionValuesRemoved.clear();
                this.compositionValueStatesAvailable.clear();
                this.errorState = new RecomposerErrorState(b, ex);
                if (controlledComposition != null) {
                    List<ControlledComposition> failedCompositions;
                    if ((failedCompositions = this.failedCompositions) == null) {
                        failedCompositions = new ArrayList<ControlledComposition>();
                        this.failedCompositions = failedCompositions;
                    }
                    if (!failedCompositions.contains(controlledComposition)) {
                        failedCompositions.add(controlledComposition);
                    }
                    this.knownCompositions.remove(controlledComposition);
                }
                this.deriveStateLocked();
                return;
            }
        }
        throw ex;
    }
    
    static /* synthetic */ void processCompositionError$default(final Recomposer recomposer, final Exception ex, ControlledComposition controlledComposition, boolean b, final int n, final Object o) {
        if ((n & 0x2) != 0x0) {
            controlledComposition = null;
        }
        if ((n & 0x4) != 0x0) {
            b = false;
        }
        recomposer.processCompositionError(ex, controlledComposition, b);
    }
    
    private final Function1<Object, Unit> readObserverOf(final ControlledComposition controlledComposition) {
        return (Function1<Object, Unit>)new Recomposer$readObserverOf.Recomposer$readObserverOf$1(controlledComposition);
    }
    
    private final Object recompositionRunner(final Function3<? super CoroutineScope, ? super MonotonicFrameClock, ? super Continuation<? super Unit>, ?> function3, final Continuation<? super Unit> continuation) {
        final Object withContext = BuildersKt.withContext((CoroutineContext)this.broadcastFrameClock, (Function2)new Recomposer$recompositionRunner.Recomposer$recompositionRunner$2(this, (Function3)function3, MonotonicFrameClockKt.getMonotonicFrameClock(continuation.getContext()), (Continuation)null), (Continuation)continuation);
        if (withContext == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return withContext;
        }
        return Unit.INSTANCE;
    }
    
    private final void recordComposerModificationsLocked() {
        final Set<Object> snapshotInvalidations = this.snapshotInvalidations;
        if (snapshotInvalidations.isEmpty() ^ true) {
            final Recomposer recomposer = this;
            final List<ControlledComposition> knownCompositions = this.knownCompositions;
            for (int i = 0; i < knownCompositions.size(); ++i) {
                ((ControlledComposition)knownCompositions.get(i)).recordModificationsOf(snapshotInvalidations);
                if (((State)this._state.getValue()).compareTo((State)State.ShuttingDown) <= 0) {
                    break;
                }
            }
            this.snapshotInvalidations = new LinkedHashSet<Object>();
            if (this.deriveStateLocked() != null) {
                throw new IllegalStateException("called outside of runRecomposeAndApplyChanges".toString());
            }
        }
    }
    
    private final void recordComposerModificationsLocked(final Function1<? super ControlledComposition, Unit> function1) {
        final Set access$getSnapshotInvalidations$p = access$getSnapshotInvalidations$p(this);
        final boolean empty = access$getSnapshotInvalidations$p.isEmpty();
        final int n = 0;
        if (empty ^ true) {
            final List access$getKnownCompositions$p = access$getKnownCompositions$p(this);
            for (int size = access$getKnownCompositions$p.size(), i = 0; i < size; ++i) {
                ((ControlledComposition)access$getKnownCompositions$p.get(i)).recordModificationsOf(access$getSnapshotInvalidations$p);
            }
            access$setSnapshotInvalidations$p(this, new LinkedHashSet());
        }
        final List access$getCompositionInvalidations$p = access$getCompositionInvalidations$p(this);
        for (int size2 = access$getCompositionInvalidations$p.size(), j = n; j < size2; ++j) {
            function1.invoke(access$getCompositionInvalidations$p.get(j));
        }
        access$getCompositionInvalidations$p(this).clear();
        if (this.deriveStateLocked() == null) {
            return;
        }
        throw new IllegalStateException("called outside of runRecomposeAndApplyChanges".toString());
    }
    
    private final void registerRunnerJob(final Job runnerJob) {
        synchronized (this.stateLock) {
            final Throwable closeCause = this.closeCause;
            if (closeCause != null) {
                throw closeCause;
            }
            if (((State)this._state.getValue()).compareTo((State)State.ShuttingDown) <= 0) {
                throw new IllegalStateException("Recomposer shut down".toString());
            }
            if (this.runnerJob == null) {
                this.runnerJob = runnerJob;
                this.deriveStateLocked();
                return;
            }
            throw new IllegalStateException("Recomposer already running".toString());
        }
    }
    
    private final RecomposerErrorState resetErrorState() {
        synchronized (this.stateLock) {
            final RecomposerErrorState errorState = this.errorState;
            if (errorState != null) {
                this.errorState = null;
                this.deriveStateLocked();
            }
            return errorState;
        }
    }
    
    private final void retryFailedCompositions() {
        synchronized (this.stateLock) {
            final List<ControlledComposition> failedCompositions = this.failedCompositions;
            if (failedCompositions == null) {
                return;
            }
            while (failedCompositions.isEmpty() ^ true) {
                final ControlledComposition controlledComposition = (ControlledComposition)CollectionsKt.removeLast((List)failedCompositions);
                if (controlledComposition instanceof CompositionImpl) {
                    controlledComposition.invalidateAll();
                    controlledComposition.setContent(((CompositionImpl)controlledComposition).getComposable());
                    if (this.errorState != null) {
                        break;
                    }
                    continue;
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    private final Object runFrameLoop(final MonotonicFrameClock monotonicFrameClock, ProduceFrameSignal l$2, final Continuation<? super Unit> continuation) {
        Object o = null;
        Label_0055: {
            if (continuation instanceof Recomposer$runFrameLoop.Recomposer$runFrameLoop$1) {
                final Recomposer$runFrameLoop.Recomposer$runFrameLoop$1 recomposer$runFrameLoop$1 = (Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)continuation;
                if ((recomposer$runFrameLoop$1.label & Integer.MIN_VALUE) != 0x0) {
                    recomposer$runFrameLoop$1.label += Integer.MIN_VALUE;
                    o = recomposer$runFrameLoop$1;
                    break Label_0055;
                }
            }
            o = new Recomposer$runFrameLoop.Recomposer$runFrameLoop$1(this, (Continuation)continuation);
        }
        final Object result = ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).label;
        while (true) {
            List l$7 = null;
            List l$8 = null;
            ProduceFrameSignal l$9 = null;
            MonotonicFrameClock l$10 = null;
            Recomposer l$11 = null;
            Object o2 = null;
            Label_0343: {
                List l$3 = null;
                Recomposer l$4 = null;
                MonotonicFrameClock l$5 = null;
                List l$6 = null;
                Label_0263: {
                    if (label == 0) {
                        ResultKt.throwOnFailure(result);
                        l$3 = new ArrayList();
                        final List list = new ArrayList();
                        l$4 = this;
                        l$5 = monotonicFrameClock;
                        l$6 = list;
                        break Label_0263;
                    }
                    if (label == 1) {
                        l$7 = (List)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$4;
                        l$8 = (List)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$3;
                        l$9 = (ProduceFrameSignal)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$2;
                        l$10 = (MonotonicFrameClock)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$1;
                        l$11 = (Recomposer)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$0;
                        ResultKt.throwOnFailure(result);
                        o2 = o;
                        break Label_0343;
                    }
                    if (label != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    l$7 = (List)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$4;
                    l$8 = (List)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$3;
                    l$9 = (ProduceFrameSignal)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$2;
                    l$5 = (MonotonicFrameClock)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$1;
                    l$11 = (Recomposer)((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$0;
                    ResultKt.throwOnFailure(result);
                    final List list2 = l$8;
                    l$2 = l$9;
                    l$6 = l$7;
                    l$4 = l$11;
                    l$3 = list2;
                }
                final Object stateLock = l$4.stateLock;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$0 = l$4;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$1 = l$5;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$2 = l$2;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$3 = l$3;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).L$4 = l$6;
                ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o).label = 1;
                if (l$2.awaitFrameRequest(stateLock, (Continuation<? super Unit>)o) == coroutine_SUSPENDED) {
                    return coroutine_SUSPENDED;
                }
                final Recomposer recomposer = l$4;
                l$7 = l$6;
                final ProduceFrameSignal produceFrameSignal = l$2;
                l$8 = l$3;
                l$11 = recomposer;
                l$10 = l$5;
                l$9 = produceFrameSignal;
                o2 = o;
            }
            final Function1 function1 = (Function1)new Recomposer$runFrameLoop.Recomposer$runFrameLoop$2(l$11, l$8, l$7, l$9);
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).L$0 = l$11;
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).L$1 = l$10;
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).L$2 = l$9;
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).L$3 = l$8;
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).L$4 = l$7;
            ((Recomposer$runFrameLoop.Recomposer$runFrameLoop$1)o2).label = 2;
            o = o2;
            MonotonicFrameClock l$5 = l$10;
            if (l$10.withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)function1, (kotlin.coroutines.Continuation<? super Object>)o2) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
            continue;
        }
    }
    
    private final Function1<Object, Unit> writeObserverOf(final ControlledComposition controlledComposition, final IdentityArraySet<Object> set) {
        return (Function1<Object, Unit>)new Recomposer$writeObserverOf.Recomposer$writeObserverOf$1(controlledComposition, (IdentityArraySet)set);
    }
    
    public final RecomposerInfo asRecomposerInfo() {
        return this.recomposerInfo;
    }
    
    public final Object awaitIdle(final Continuation<? super Unit> continuation) {
        final Object collect = FlowKt.collect(FlowKt.takeWhile((Flow)this.getCurrentState(), (Function2)new Recomposer$awaitIdle.Recomposer$awaitIdle$2((Continuation)null)), (Continuation)continuation);
        if (collect == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return collect;
        }
        return Unit.INSTANCE;
    }
    
    public final void cancel() {
        synchronized (this.stateLock) {
            if (((State)this._state.getValue()).compareTo((State)State.Idle) >= 0) {
                this._state.setValue((Object)State.ShuttingDown);
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(this.stateLock);
            Job$DefaultImpls.cancel$default((Job)this.effectJob, (CancellationException)null, 1, (Object)null);
        }
    }
    
    public final void close() {
        if (this.effectJob.complete()) {
            synchronized (this.stateLock) {
                this.isClosed = true;
                final Unit instance = Unit.INSTANCE;
            }
        }
    }
    
    @Override
    public void composeInitial$runtime_release(final ControlledComposition ex, Function2<? super Composer, ? super Integer, Unit> stateLock) {
        Intrinsics.checkNotNullParameter((Object)ex, "composition");
        Intrinsics.checkNotNullParameter(stateLock, "content");
        final boolean composing = ((ControlledComposition)ex).isComposing();
        try {
            Object o = Snapshot.Companion.takeMutableSnapshot(this.readObserverOf((ControlledComposition)ex), this.writeObserverOf((ControlledComposition)ex, null));
            try {
                final Snapshot snapshot = (Snapshot)o;
                final Snapshot current = snapshot.makeCurrent();
                try {
                    ((ControlledComposition)ex).composeContent((Function2<? super Composer, ? super Integer, Unit>)stateLock);
                    final Unit instance = Unit.INSTANCE;
                    snapshot.restoreCurrent(current);
                    this.applyAndCheck((MutableSnapshot)o);
                    if (!composing) {
                        Snapshot.Companion.notifyObjectsInitialized();
                    }
                    stateLock = this.stateLock;
                    synchronized (stateLock) {
                        if (((State)this._state.getValue()).compareTo((State)State.ShuttingDown) > 0 && !this.knownCompositions.contains(ex)) {
                            this.knownCompositions.add(ex);
                        }
                        o = Unit.INSTANCE;
                        monitorexit(stateLock);
                        try {
                            this.performInitialMovableContentInserts((ControlledComposition)ex);
                            try {
                                ((ControlledComposition)ex).applyChanges();
                                ((ControlledComposition)ex).applyLateChanges();
                                if (!composing) {
                                    Snapshot.Companion.notifyObjectsInitialized();
                                }
                            }
                            catch (final Exception ex) {
                                processCompositionError$default(this, ex, null, false, 6, null);
                            }
                        }
                        catch (final Exception stateLock) {
                            this.processCompositionError((Exception)stateLock, (ControlledComposition)ex, true);
                        }
                    }
                }
                finally {
                    snapshot.restoreCurrent(current);
                }
            }
            finally {
                this.applyAndCheck((MutableSnapshot)o);
            }
        }
        catch (final Exception ex2) {
            this.processCompositionError(ex2, (ControlledComposition)ex, true);
        }
    }
    
    @Override
    public void deletedMovableContent$runtime_release(final MovableContentStateReference movableContentStateReference) {
        Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
        synchronized (this.stateLock) {
            RecomposerKt.addMultiValue(this.compositionValuesRemoved, movableContentStateReference.getContent$runtime_release(), movableContentStateReference);
        }
    }
    
    public final long getChangeCount() {
        return this.changeCount;
    }
    
    @Override
    public boolean getCollectingParameterInformation$runtime_release() {
        return false;
    }
    
    @Override
    public int getCompoundHashKey$runtime_release() {
        return 1000;
    }
    
    public final StateFlow<State> getCurrentState() {
        return (StateFlow<State>)this._state;
    }
    
    @Override
    public CoroutineContext getEffectCoroutineContext$runtime_release() {
        return this.effectCoroutineContext;
    }
    
    public final boolean getHasPendingWork() {
        synchronized (this.stateLock) {
            final boolean empty = this.snapshotInvalidations.isEmpty();
            boolean b2;
            final boolean b = b2 = true;
            if (!(empty ^ true)) {
                b2 = b;
                if (!(this.compositionInvalidations.isEmpty() ^ true)) {
                    b2 = b;
                    if (this.concurrentCompositionsOutstanding <= 0) {
                        b2 = b;
                        if (!(this.compositionsAwaitingApply.isEmpty() ^ true)) {
                            b2 = (this.broadcastFrameClock.getHasAwaiters() && b);
                        }
                    }
                }
            }
            return b2;
        }
    }
    
    @Override
    public CoroutineContext getRecomposeCoroutineContext$runtime_release() {
        return (CoroutineContext)EmptyCoroutineContext.INSTANCE;
    }
    
    public final Flow<State> getState() {
        return (Flow<State>)this.getCurrentState();
    }
    
    @Override
    public void insertMovableContent$runtime_release(final MovableContentStateReference movableContentStateReference) {
        Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
        synchronized (this.stateLock) {
            this.compositionValuesAwaitingInsert.add(movableContentStateReference);
            final CancellableContinuation<Unit> deriveStateLocked = this.deriveStateLocked();
            monitorexit(this.stateLock);
            if (deriveStateLocked != null) {
                final Continuation continuation = (Continuation)deriveStateLocked;
                final Result$Companion companion = Result.Companion;
                continuation.resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
            }
        }
    }
    
    @Override
    public void invalidate$runtime_release(final ControlledComposition controlledComposition) {
        Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
        Object stateLock = this.stateLock;
        synchronized (stateLock) {
            Object deriveStateLocked;
            if (!this.compositionInvalidations.contains(controlledComposition)) {
                this.compositionInvalidations.add(controlledComposition);
                deriveStateLocked = this.deriveStateLocked();
            }
            else {
                deriveStateLocked = null;
            }
            monitorexit(stateLock);
            if (deriveStateLocked != null) {
                stateLock = deriveStateLocked;
                final Result$Companion companion = Result.Companion;
                ((Continuation)stateLock).resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
            }
        }
    }
    
    @Override
    public void invalidateScope$runtime_release(final RecomposeScopeImpl recomposeScopeImpl) {
        Intrinsics.checkNotNullParameter((Object)recomposeScopeImpl, "scope");
        synchronized (this.stateLock) {
            this.snapshotInvalidations.add(recomposeScopeImpl);
            final CancellableContinuation<Unit> deriveStateLocked = this.deriveStateLocked();
            monitorexit(this.stateLock);
            if (deriveStateLocked != null) {
                final Continuation continuation = (Continuation)deriveStateLocked;
                final Result$Companion companion = Result.Companion;
                continuation.resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
            }
        }
    }
    
    public final Object join(final Continuation<? super Unit> continuation) {
        final Object first = FlowKt.first((Flow)this.getCurrentState(), (Function2)new Recomposer$join.Recomposer$join$2((Continuation)null), (Continuation)continuation);
        if (first == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return first;
        }
        return Unit.INSTANCE;
    }
    
    @Override
    public void movableContentStateReleased$runtime_release(final MovableContentStateReference movableContentStateReference, final MovableContentState movableContentState) {
        Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
        Intrinsics.checkNotNullParameter((Object)movableContentState, "data");
        synchronized (this.stateLock) {
            this.compositionValueStatesAvailable.put(movableContentStateReference, movableContentState);
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Override
    public MovableContentState movableContentStateResolve$runtime_release(final MovableContentStateReference movableContentStateReference) {
        Intrinsics.checkNotNullParameter((Object)movableContentStateReference, "reference");
        synchronized (this.stateLock) {
            return this.compositionValueStatesAvailable.remove(movableContentStateReference);
        }
    }
    
    @Override
    public void recordInspectionTable$runtime_release(final Set<CompositionData> set) {
        Intrinsics.checkNotNullParameter((Object)set, "table");
    }
    
    @Override
    public void registerComposition$runtime_release(final ControlledComposition controlledComposition) {
        Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
    }
    
    public final Object runRecomposeAndApplyChanges(final Continuation<? super Unit> continuation) {
        final Object recompositionRunner = this.recompositionRunner((Function3<? super CoroutineScope, ? super MonotonicFrameClock, ? super Continuation<? super Unit>, ?>)new Recomposer$runRecomposeAndApplyChanges.Recomposer$runRecomposeAndApplyChanges$2(this, (Continuation)null), continuation);
        if (recompositionRunner == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return recompositionRunner;
        }
        return Unit.INSTANCE;
    }
    
    public final Object runRecomposeConcurrentlyAndApplyChanges(final CoroutineContext coroutineContext, final Continuation<? super Unit> continuation) {
        final Object recompositionRunner = this.recompositionRunner((Function3<? super CoroutineScope, ? super MonotonicFrameClock, ? super Continuation<? super Unit>, ?>)new Recomposer$runRecomposeConcurrentlyAndApplyChanges.Recomposer$runRecomposeConcurrentlyAndApplyChanges$2(coroutineContext, this, (Continuation)null), continuation);
        if (recompositionRunner == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return recompositionRunner;
        }
        return Unit.INSTANCE;
    }
    
    @Override
    public void unregisterComposition$runtime_release(final ControlledComposition controlledComposition) {
        Intrinsics.checkNotNullParameter((Object)controlledComposition, "composition");
        synchronized (this.stateLock) {
            this.knownCompositions.remove(controlledComposition);
            this.compositionInvalidations.remove(controlledComposition);
            this.compositionsAwaitingApply.remove(controlledComposition);
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Metadata(d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000b\b\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0013\u001a\u00020\u00142\n\u0010\u0015\u001a\u00060\u000bR\u00020\fH\u0002J\r\u0010\u0016\u001a\u00020\u0014H\u0000¢\u0006\u0002\b\u0017J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H\u0000¢\u0006\u0002\b\u001bJ\u0015\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0000¢\u0006\u0002\b\u001fJ\u0015\u0010 \u001a\u00020\u00142\u0006\u0010!\u001a\u00020\u0001H\u0000¢\u0006\u0002\b\"J\u0014\u0010#\u001a\u00020\u00142\n\u0010\u0015\u001a\u00060\u000bR\u00020\fH\u0002J\r\u0010$\u001a\u00020\u0001H\u0000¢\u0006\u0002\b%J\u0015\u0010&\u001a\u00020\u00142\u0006\u0010'\u001a\u00020\u0005H\u0000¢\u0006\u0002\b(R.\u0010\u0003\u001a\"\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004j\u0010\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005`\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000bR\u00020\f0\n0\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e8F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012¨\u0006)" }, d2 = { "Landroidx/compose/runtime/Recomposer$Companion;", "", "()V", "_hotReloadEnabled", "Ljava/util/concurrent/atomic/AtomicReference;", "", "kotlin.jvm.PlatformType", "Landroidx/compose/runtime/AtomicReference;", "_runningRecomposers", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentSet;", "Landroidx/compose/runtime/Recomposer$RecomposerInfoImpl;", "Landroidx/compose/runtime/Recomposer;", "runningRecomposers", "Lkotlinx/coroutines/flow/StateFlow;", "", "Landroidx/compose/runtime/RecomposerInfo;", "getRunningRecomposers", "()Lkotlinx/coroutines/flow/StateFlow;", "addRunning", "", "info", "clearErrors", "clearErrors$runtime_release", "getCurrentErrors", "", "Landroidx/compose/runtime/RecomposerErrorInfo;", "getCurrentErrors$runtime_release", "invalidateGroupsWithKey", "key", "", "invalidateGroupsWithKey$runtime_release", "loadStateAndComposeForHotReload", "token", "loadStateAndComposeForHotReload$runtime_release", "removeRunning", "saveStateAndDisposeForHotReload", "saveStateAndDisposeForHotReload$runtime_release", "setHotReloadEnabled", "value", "setHotReloadEnabled$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final void addRunning(final RecomposerInfoImpl recomposerInfoImpl) {
            PersistentSet set;
            PersistentSet<RecomposerInfoImpl> add;
            do {
                set = (PersistentSet)Recomposer.access$get_runningRecomposers$cp().getValue();
                add = set.add(recomposerInfoImpl);
            } while (set != add && !Recomposer.access$get_runningRecomposers$cp().compareAndSet((Object)set, (Object)add));
        }
        
        private final void removeRunning(final RecomposerInfoImpl recomposerInfoImpl) {
            PersistentSet set;
            PersistentSet<RecomposerInfoImpl> remove;
            do {
                set = (PersistentSet)Recomposer.access$get_runningRecomposers$cp().getValue();
                remove = set.remove(recomposerInfoImpl);
            } while (set != remove && !Recomposer.access$get_runningRecomposers$cp().compareAndSet((Object)set, (Object)remove));
        }
        
        public final void clearErrors$runtime_release() {
            final Iterable iterable = (Iterable)Recomposer.access$get_runningRecomposers$cp().getValue();
            final Collection collection = new ArrayList();
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                final RecomposerErrorState resetErrorState = ((RecomposerInfoImpl)iterator.next()).resetErrorState();
                if (resetErrorState != null) {
                    collection.add(resetErrorState);
                }
            }
            final List list = (List)collection;
        }
        
        public final List<RecomposerErrorInfo> getCurrentErrors$runtime_release() {
            final Iterable iterable = (Iterable)Recomposer.access$get_runningRecomposers$cp().getValue();
            final Collection collection = new ArrayList();
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                final RecomposerErrorInfo currentError = ((RecomposerInfoImpl)iterator.next()).getCurrentError();
                if (currentError != null) {
                    collection.add(currentError);
                }
            }
            return (List<RecomposerErrorInfo>)collection;
        }
        
        public final StateFlow<Set<RecomposerInfo>> getRunningRecomposers() {
            return (StateFlow<Set<RecomposerInfo>>)Recomposer.access$get_runningRecomposers$cp();
        }
        
        public final void invalidateGroupsWithKey$runtime_release(final int n) {
            Recomposer.access$get_hotReloadEnabled$cp().set(true);
            for (final RecomposerInfoImpl recomposerInfoImpl : (Iterable)Recomposer.access$get_runningRecomposers$cp().getValue()) {
                final RecomposerErrorInfo currentError = recomposerInfoImpl.getCurrentError();
                int n2 = 0;
                if (currentError != null) {
                    n2 = n2;
                    if (!currentError.getRecoverable()) {
                        n2 = 1;
                    }
                }
                if (n2 != 0) {
                    continue;
                }
                recomposerInfoImpl.resetErrorState();
                recomposerInfoImpl.invalidateGroupsWithKey(n);
                recomposerInfoImpl.retryFailedCompositions();
            }
        }
        
        public final void loadStateAndComposeForHotReload$runtime_release(final Object o) {
            Intrinsics.checkNotNullParameter(o, "token");
            Recomposer.access$get_hotReloadEnabled$cp().set(true);
            final Iterator iterator = ((Iterable)Recomposer.access$get_runningRecomposers$cp().getValue()).iterator();
            while (iterator.hasNext()) {
                ((RecomposerInfoImpl)iterator.next()).resetErrorState();
            }
            final List list = (List)o;
            final int size = list.size();
            final int n = 0;
            for (int i = 0; i < size; ++i) {
                ((HotReloadable)list.get(i)).resetContent();
            }
            for (int size2 = list.size(), j = n; j < size2; ++j) {
                ((HotReloadable)list.get(j)).recompose();
            }
            final Iterator iterator2 = ((Iterable)Recomposer.access$get_runningRecomposers$cp().getValue()).iterator();
            while (iterator2.hasNext()) {
                ((RecomposerInfoImpl)iterator2.next()).retryFailedCompositions();
            }
        }
        
        public final Object saveStateAndDisposeForHotReload$runtime_release() {
            Recomposer.access$get_hotReloadEnabled$cp().set(true);
            final Iterable iterable = (Iterable)Recomposer.access$get_runningRecomposers$cp().getValue();
            final Collection collection = new ArrayList();
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                CollectionsKt.addAll(collection, (Iterable)((RecomposerInfoImpl)iterator.next()).saveStateAndDisposeForHotReload());
            }
            return (List<?>)collection;
        }
        
        public final void setHotReloadEnabled$runtime_release(final boolean b) {
            Recomposer.access$get_hotReloadEnabled$cp().set(b);
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\n\u001a\u00020\u0007J\u0006\u0010\u000b\u001a\u00020\u0007J\u0006\u0010\f\u001a\u00020\u0007R\u001b\u0010\u0005\u001a\r\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\b\bX\u0082\u000e¢\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/compose/runtime/Recomposer$HotReloadable;", "", "composition", "Landroidx/compose/runtime/CompositionImpl;", "(Landroidx/compose/runtime/CompositionImpl;)V", "composable", "Lkotlin/Function0;", "", "Landroidx/compose/runtime/Composable;", "Lkotlin/jvm/functions/Function2;", "clearContent", "recompose", "resetContent", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class HotReloadable
    {
        private Function2<? super Composer, ? super Integer, Unit> composable;
        private final CompositionImpl composition;
        
        public HotReloadable(final CompositionImpl composition) {
            Intrinsics.checkNotNullParameter((Object)composition, "composition");
            this.composition = composition;
            this.composable = composition.getComposable();
        }
        
        public final void clearContent() {
            if (this.composition.isRoot()) {
                this.composition.setContent(ComposableSingletons$RecomposerKt.INSTANCE.getLambda-1$runtime_release());
            }
        }
        
        public final void recompose() {
            if (this.composition.isRoot()) {
                this.composition.setContent(this.composable);
            }
        }
        
        public final void resetContent() {
            this.composition.setComposable(this.composable);
        }
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0002\u0010\u0007R\u0018\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f" }, d2 = { "Landroidx/compose/runtime/Recomposer$RecomposerErrorState;", "Landroidx/compose/runtime/RecomposerErrorInfo;", "recoverable", "", "cause", "Ljava/lang/Exception;", "Lkotlin/Exception;", "(ZLjava/lang/Exception;)V", "getCause", "()Ljava/lang/Exception;", "getRecoverable", "()Z", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class RecomposerErrorState implements RecomposerErrorInfo
    {
        private final Exception cause;
        private final boolean recoverable;
        
        public RecomposerErrorState(final boolean recoverable, final Exception cause) {
            Intrinsics.checkNotNullParameter((Object)cause, "cause");
            this.recoverable = recoverable;
            this.cause = cause;
        }
        
        @Override
        public Exception getCause() {
            return this.cause;
        }
        
        @Override
        public boolean getRecoverable() {
            return this.recoverable;
        }
    }
    
    @Metadata(d1 = { "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019J\u0006\u0010\u001a\u001a\u00020\u0015J\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001cR\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b8F¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u001e" }, d2 = { "Landroidx/compose/runtime/Recomposer$RecomposerInfoImpl;", "Landroidx/compose/runtime/RecomposerInfo;", "(Landroidx/compose/runtime/Recomposer;)V", "changeCount", "", "getChangeCount", "()J", "currentError", "Landroidx/compose/runtime/RecomposerErrorInfo;", "getCurrentError", "()Landroidx/compose/runtime/RecomposerErrorInfo;", "hasPendingWork", "", "getHasPendingWork", "()Z", "state", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/compose/runtime/Recomposer$State;", "getState", "()Lkotlinx/coroutines/flow/Flow;", "invalidateGroupsWithKey", "", "key", "", "resetErrorState", "Landroidx/compose/runtime/Recomposer$RecomposerErrorState;", "retryFailedCompositions", "saveStateAndDisposeForHotReload", "", "Landroidx/compose/runtime/Recomposer$HotReloadable;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class RecomposerInfoImpl implements RecomposerInfo
    {
        final Recomposer this$0;
        
        public RecomposerInfoImpl(final Recomposer this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public long getChangeCount() {
            return this.this$0.getChangeCount();
        }
        
        public final RecomposerErrorInfo getCurrentError() {
            final Object access$getStateLock$p = Recomposer.access$getStateLock$p(this.this$0);
            final Recomposer this$0 = this.this$0;
            synchronized (access$getStateLock$p) {
                return Recomposer.access$getErrorState$p(this$0);
            }
        }
        
        @Override
        public boolean getHasPendingWork() {
            return this.this$0.getHasPendingWork();
        }
        
        @Override
        public Flow<State> getState() {
            return (Flow<State>)this.this$0.getCurrentState();
        }
        
        public final void invalidateGroupsWithKey(final int n) {
            Object access$getStateLock$p = Recomposer.access$getStateLock$p(this.this$0);
            final Recomposer this$0 = this.this$0;
            synchronized (access$getStateLock$p) {
                final List mutableList = CollectionsKt.toMutableList((Collection)Recomposer.access$getKnownCompositions$p(this$0));
                monitorexit(access$getStateLock$p);
                final ArrayList list = new ArrayList<CompositionImpl>(mutableList.size());
                final int size = mutableList.size();
                final int n2 = 0;
                for (int i = 0; i < size; ++i) {
                    access$getStateLock$p = mutableList.get(i);
                    if (access$getStateLock$p instanceof CompositionImpl) {
                        access$getStateLock$p = access$getStateLock$p;
                    }
                    else {
                        access$getStateLock$p = null;
                    }
                    if (access$getStateLock$p != null) {
                        list.add((CompositionImpl)access$getStateLock$p);
                    }
                }
                access$getStateLock$p = list;
                for (int size2 = ((List)access$getStateLock$p).size(), j = n2; j < size2; ++j) {
                    ((CompositionImpl)((List)access$getStateLock$p).get(j)).invalidateGroupsWithKey(n);
                }
            }
        }
        
        public final RecomposerErrorState resetErrorState() {
            return this.this$0.resetErrorState();
        }
        
        public final void retryFailedCompositions() {
            this.this$0.retryFailedCompositions();
        }
        
        public final List<HotReloadable> saveStateAndDisposeForHotReload() {
            Object access$getStateLock$p = Recomposer.access$getStateLock$p(this.this$0);
            final Recomposer this$0 = this.this$0;
            synchronized (access$getStateLock$p) {
                final List mutableList = CollectionsKt.toMutableList((Collection)Recomposer.access$getKnownCompositions$p(this$0));
                monitorexit(access$getStateLock$p);
                final ArrayList list = new ArrayList<CompositionImpl>(mutableList.size());
                final int size = mutableList.size();
                final int n = 0;
                for (int i = 0; i < size; ++i) {
                    access$getStateLock$p = mutableList.get(i);
                    if (access$getStateLock$p instanceof CompositionImpl) {
                        access$getStateLock$p = access$getStateLock$p;
                    }
                    else {
                        access$getStateLock$p = null;
                    }
                    if (access$getStateLock$p != null) {
                        list.add((CompositionImpl)access$getStateLock$p);
                    }
                }
                final List list2 = list;
                final ArrayList list3 = new ArrayList<HotReloadable>(list2.size());
                for (int size2 = list2.size(), j = n; j < size2; ++j) {
                    final Object value = list2.get(j);
                    access$getStateLock$p = list3;
                    final HotReloadable hotReloadable = new HotReloadable((CompositionImpl)value);
                    hotReloadable.clearContent();
                    ((Collection<HotReloadable>)access$getStateLock$p).add(hotReloadable);
                }
                return (List<HotReloadable>)list3;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t" }, d2 = { "Landroidx/compose/runtime/Recomposer$State;", "", "(Ljava/lang/String;I)V", "ShutDown", "ShuttingDown", "Inactive", "InactivePendingWork", "Idle", "PendingWork", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public enum State
    {
        private static final State[] $VALUES;
        
        Idle, 
        Inactive, 
        InactivePendingWork, 
        PendingWork, 
        ShutDown, 
        ShuttingDown;
        
        private static final /* synthetic */ State[] $values() {
            return new State[] { State.ShutDown, State.ShuttingDown, State.Inactive, State.InactivePendingWork, State.Idle, State.PendingWork };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

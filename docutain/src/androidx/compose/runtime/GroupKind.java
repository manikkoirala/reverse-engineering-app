// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.JvmInline;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0083@\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0014\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\u000e\u001a\u00020\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001¢\u0006\u0004\b\u0013\u0010\u0005J\u0010\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001¢\u0006\u0004\b\u0016\u0010\u0017R\u0012\u0010\u0006\u001a\u00020\u00078\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u00078\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r\u0088\u0001\u0002\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0019" }, d2 = { "Landroidx/compose/runtime/GroupKind;", "", "value", "", "constructor-impl", "(I)I", "isNode", "", "isNode-impl", "(I)Z", "isReusable", "isReusable-impl", "getValue", "()I", "equals", "other", "equals-impl", "(ILjava/lang/Object;)Z", "hashCode", "hashCode-impl", "toString", "", "toString-impl", "(I)Ljava/lang/String;", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
@JvmInline
final class GroupKind
{
    public static final Companion Companion;
    private static final int Group;
    private static final int Node;
    private static final int ReusableNode;
    private final int value = value;
    
    static {
        Companion = new Companion(null);
        Group = constructor-impl(0);
        Node = constructor-impl(1);
        ReusableNode = constructor-impl(2);
    }
    
    public static final /* synthetic */ int access$getGroup$cp() {
        return GroupKind.Group;
    }
    
    public static final /* synthetic */ int access$getNode$cp() {
        return GroupKind.Node;
    }
    
    public static final /* synthetic */ int access$getReusableNode$cp() {
        return GroupKind.ReusableNode;
    }
    
    private static int constructor-impl(final int n) {
        return n;
    }
    
    public static boolean equals-impl(final int n, final Object o) {
        return o instanceof GroupKind && n == ((GroupKind)o).unbox-impl();
    }
    
    public static final boolean equals-impl0(final int n, final int n2) {
        return n == n2;
    }
    
    public static int hashCode-impl(final int n) {
        return n;
    }
    
    public static final boolean isNode-impl(final int n) {
        return n != GroupKind.Companion.getGroup-ULZAiWs();
    }
    
    public static final boolean isReusable-impl(final int n) {
        return n != GroupKind.Companion.getNode-ULZAiWs();
    }
    
    public static String toString-impl(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("GroupKind(value=");
        sb.append(i);
        sb.append(')');
        return sb.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.value, o);
    }
    
    public final int getValue() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.value);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.value);
    }
    
    public final /* synthetic */ int unbox-impl() {
        return this.value;
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u0004\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\t\u0010\u0006R\u001c\u0010\n\u001a\u00020\u0004\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u000b\u0010\u0006\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b¡\u001e0\u0001\n\u0002\b!¨\u0006\f" }, d2 = { "Landroidx/compose/runtime/GroupKind$Companion;", "", "()V", "Group", "Landroidx/compose/runtime/GroupKind;", "getGroup-ULZAiWs", "()I", "I", "Node", "getNode-ULZAiWs", "ReusableNode", "getReusableNode-ULZAiWs", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final int getGroup-ULZAiWs() {
            return GroupKind.access$getGroup$cp();
        }
        
        public final int getNode-ULZAiWs() {
            return GroupKind.access$getNode$cp();
        }
        
        public final int getReusableNode-ULZAiWs() {
            return GroupKind.access$getReusableNode$cp();
        }
    }
}

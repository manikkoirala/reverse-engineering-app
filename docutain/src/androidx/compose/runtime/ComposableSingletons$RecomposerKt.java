// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import androidx.compose.runtime.internal.ComposableLambdaKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(k = 3, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableSingletons$RecomposerKt
{
    public static final ComposableSingletons$RecomposerKt INSTANCE;
    public static Function2<Composer, Integer, Unit> lambda-1;
    
    static {
        INSTANCE = new ComposableSingletons$RecomposerKt();
        ComposableSingletons$RecomposerKt.lambda-1 = (Function2<Composer, Integer, Unit>)ComposableLambdaKt.composableLambdaInstance(-1091980426, false, ComposableSingletons$RecomposerKt$lambda_1.ComposableSingletons$RecomposerKt$lambda_1$1.INSTANCE);
    }
    
    public final Function2<Composer, Integer, Unit> getLambda-1$runtime_release() {
        return ComposableSingletons$RecomposerKt.lambda-1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.KotlinNothingValueException;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0004J\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\nJ\u0006\u0010\f\u001a\u00020\u0004J\u0006\u0010\r\u001a\u00020\u0004J\u0006\u0010\u000e\u001a\u00020\u0007R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/compose/runtime/PrioritySet;", "", "list", "", "", "(Ljava/util/List;)V", "add", "", "value", "isEmpty", "", "isNotEmpty", "peek", "takeMax", "validateHeap", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PrioritySet
{
    private final List<Integer> list;
    
    public PrioritySet() {
        this(null, 1, null);
    }
    
    public PrioritySet(final List<Integer> list) {
        Intrinsics.checkNotNullParameter((Object)list, "list");
        this.list = list;
    }
    
    public final void add(final int n) {
        Label_0070: {
            if (this.list.isEmpty() ^ true) {
                if (this.list.get(0).intValue() != n) {
                    final List<Integer> list = this.list;
                    if (((Number)list.get(list.size() - 1)).intValue() != n) {
                        break Label_0070;
                    }
                }
                return;
            }
        }
        int i = this.list.size();
        this.list.add(n);
        while (i > 0) {
            final int n2 = (i + 1 >>> 1) - 1;
            final int intValue = this.list.get(n2).intValue();
            if (n <= intValue) {
                break;
            }
            this.list.set(i, intValue);
            i = n2;
        }
        this.list.set(i, n);
    }
    
    public final boolean isEmpty() {
        return this.list.isEmpty();
    }
    
    public final boolean isNotEmpty() {
        return this.list.isEmpty() ^ true;
    }
    
    public final int peek() {
        return ((Number)CollectionsKt.first((List)this.list)).intValue();
    }
    
    public final int takeMax() {
        if (this.list.size() > 0) {
            final int intValue = this.list.get(0).intValue();
            while ((this.list.isEmpty() ^ true) && this.list.get(0).intValue() == intValue) {
                final List<Integer> list = this.list;
                list.set(0, (Integer)CollectionsKt.last((List)list));
                final List<Integer> list2 = this.list;
                list2.remove(list2.size() - 1);
                final int size = this.list.size();
                final int size2 = this.list.size();
                int i = 0;
                while (i < size2 >>> 1) {
                    final int intValue2 = this.list.get(i).intValue();
                    final int n = (i + 1) * 2;
                    final int n2 = n - 1;
                    final int intValue3 = this.list.get(n2).intValue();
                    if (n < size) {
                        final int intValue4 = this.list.get(n).intValue();
                        if (intValue4 > intValue3) {
                            if (intValue4 > intValue2) {
                                this.list.set(i, intValue4);
                                this.list.set(n, intValue2);
                                i = n;
                                continue;
                            }
                            break;
                        }
                    }
                    if (intValue3 <= intValue2) {
                        break;
                    }
                    this.list.set(i, intValue3);
                    this.list.set(n2, intValue2);
                    i = n2;
                }
            }
            return intValue;
        }
        ComposerKt.composeRuntimeError("Set is empty".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void validateHeap() {
        final int size = this.list.size();
        int n2;
        for (int n = size / 2, i = 0; i < n; i = n2) {
            n2 = i + 1;
            final int n3 = n2 * 2;
            final int intValue = this.list.get(i).intValue();
            final int intValue2 = this.list.get(n3 - 1).intValue();
            final boolean b = true;
            if (intValue < intValue2) {
                throw new IllegalStateException("Check failed.".toString());
            }
            int n4 = b ? 1 : 0;
            if (n3 < size) {
                if (this.list.get(i).intValue() >= this.list.get(n3).intValue()) {
                    n4 = (b ? 1 : 0);
                }
                else {
                    n4 = 0;
                }
            }
            if (n4 == 0) {
                throw new IllegalStateException("Check failed.".toString());
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import java.util.Iterator;
import kotlin.KotlinNothingValueException;
import kotlin.collections.ArraysKt;
import kotlin.collections.CollectionsKt;
import kotlin.ranges.RangesKt;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.ArrayList;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u001f\n\u0002\u0010(\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010 \n\u0002\b8\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u009d\u00012\u00020\u0001:\u0002\u009d\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0016\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\n2\u0006\u00104\u001a\u00020\nJ\u000e\u00105\u001a\u0002022\u0006\u00104\u001a\u00020\nJ\u0010\u00106\u001a\u00020\u00072\b\b\u0002\u00107\u001a\u00020\nJ\u000e\u00108\u001a\u00020\n2\u0006\u00106\u001a\u00020\u0007J\r\u00109\u001a\u000202H\u0000¢\u0006\u0002\b:J\u0006\u0010;\u001a\u000202J\u0010\u0010<\u001a\u00020\u000e2\u0006\u0010=\u001a\u00020\nH\u0002J\u0006\u0010>\u001a\u000202J\u0010\u0010?\u001a\u00020\u000e2\u0006\u0010=\u001a\u00020\nH\u0002J\u0010\u0010@\u001a\u00020\u000e2\u0006\u0010=\u001a\u00020\nH\u0002J \u0010A\u001a\u00020\n2\u0006\u00106\u001a\u00020\n2\u0006\u0010B\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010C\u001a\u00020\n2\u0006\u00107\u001a\u00020\nH\u0002J\u0010\u0010D\u001a\u00020\n2\u0006\u0010C\u001a\u00020\nH\u0002J(\u0010E\u001a\u00020\n2\u0006\u00107\u001a\u00020\n2\u0006\u0010F\u001a\u00020\n2\u0006\u0010B\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0006\u0010G\u001a\u00020\nJ\u0006\u0010H\u001a\u000202J\u000e\u0010I\u001a\u0002022\u0006\u00106\u001a\u00020\u0007J\u000e\u0010I\u001a\u0002022\u0006\u00107\u001a\u00020\nJ \u0010J\u001a\u0002022\u0006\u0010\"\u001a\u00020\n2\u0006\u0010G\u001a\u00020\n2\u0006\u0010K\u001a\u00020\nH\u0002J\u0010\u0010L\u001a\u0004\u0018\u00010\u00012\u0006\u00107\u001a\u00020\nJ\u0010\u0010M\u001a\u00020\n2\u0006\u00107\u001a\u00020\nH\u0002J\u000e\u0010N\u001a\u00020\n2\u0006\u00107\u001a\u00020\nJ\u0010\u0010O\u001a\u0004\u0018\u00010\u00012\u0006\u00107\u001a\u00020\nJ\u000e\u0010P\u001a\u00020\n2\u0006\u00107\u001a\u00020\nJ\u000e\u0010Q\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010RJ\u0006\u0010S\u001a\u00020TJ\u000e\u0010U\u001a\u00020\u000e2\u0006\u00107\u001a\u00020\nJ\u0016\u0010V\u001a\u00020\u000e2\u0006\u00107\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010W\u001a\u00020\u000e2\u0006\u00107\u001a\u00020\nJ\u0010\u0010X\u001a\u0002022\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u0010\u0010Z\u001a\u0002022\u0006\u0010&\u001a\u00020\nH\u0002J\u000e\u0010[\u001a\u0002022\u0006\u0010\\\u001a\u00020\nJ\u0018\u0010]\u001a\u0002022\u0006\u0010&\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nH\u0002J\u000e\u0010\u001f\u001a\u00020\u000e2\u0006\u00107\u001a\u00020\nJ\u000e\u0010^\u001a\b\u0012\u0004\u0012\u00020\n0_H\u0002J\u0010\u0010`\u001a\u0002022\b\b\u0002\u0010=\u001a\u00020\nJ \u0010a\u001a\u0002022\u0006\u0010b\u001a\u00020\n2\u0006\u0010c\u001a\u00020\n2\u0006\u0010&\u001a\u00020\nH\u0002J\u001c\u0010d\u001a\b\u0012\u0004\u0012\u00020\u00070_2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u00107\u001a\u00020\nJ\u000e\u0010e\u001a\u0002022\u0006\u0010f\u001a\u00020\nJ\u0010\u0010g\u001a\u0002022\u0006\u00107\u001a\u00020\nH\u0002J$\u0010h\u001a\b\u0012\u0004\u0012\u00020\u00070_2\u0006\u0010f\u001a\u00020\n2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u00107\u001a\u00020\nJ\u0018\u0010i\u001a\u0002022\u0006\u00107\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nH\u0002J$\u0010j\u001a\b\u0012\u0004\u0012\u00020\u00070_2\u0006\u00106\u001a\u00020\u00072\u0006\u0010f\u001a\u00020\n2\u0006\u0010k\u001a\u00020\u0000J\u0010\u0010l\u001a\u0004\u0018\u00010\u00012\u0006\u00106\u001a\u00020\u0007J\u0010\u0010l\u001a\u0004\u0018\u00010\u00012\u0006\u00107\u001a\u00020\nJ\u000e\u0010 \u001a\u00020\n2\u0006\u00107\u001a\u00020\nJ\u000e\u0010\"\u001a\u00020\n2\u0006\u00106\u001a\u00020\u0007J\u000e\u0010\"\u001a\u00020\n2\u0006\u00107\u001a\u00020\nJ\u0010\u0010m\u001a\u00020\n2\u0006\u00107\u001a\u00020\nH\u0002J\u0018\u0010n\u001a\u00020\n2\u0006\u00107\u001a\u00020\n2\u0006\u0010F\u001a\u00020\nH\u0002J\b\u0010o\u001a\u000202H\u0002J\u0018\u0010p\u001a\u00020\u000e2\u0006\u0010F\u001a\u00020\n2\u0006\u0010&\u001a\u00020\nH\u0002J\u0006\u0010q\u001a\u00020\u000eJ\u0018\u0010r\u001a\u00020\u000e2\u0006\u0010s\u001a\u00020\n2\u0006\u0010t\u001a\u00020\nH\u0002J \u0010u\u001a\u0002022\u0006\u0010s\u001a\u00020\n2\u0006\u0010t\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nH\u0002J\u0006\u0010v\u001a\u000202J\b\u0010w\u001a\u00020\nH\u0002J\b\u0010x\u001a\u000202H\u0002J\u000e\u0010y\u001a\u0002022\u0006\u00106\u001a\u00020\u0007J\u0010\u0010z\u001a\u0002022\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u001a\u0010z\u001a\u0004\u0018\u00010\u00012\u0006\u00107\u001a\u00020\n2\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\b\u0010{\u001a\u0004\u0018\u00010\u0001J\u0006\u0010|\u001a\u00020\nJ\u0006\u0010}\u001a\u000202J\u0018\u0010~\u001a\u0004\u0018\u00010\u00012\u0006\u00106\u001a\u00020\u00072\u0006\u00107\u001a\u00020\nJ\u0018\u0010~\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u007f\u001a\u00020\n2\u0006\u00107\u001a\u00020\nJ\u001a\u0010\u0080\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\u0001J%\u0010\u0080\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00012\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\u0001J\u0007\u0010\u0083\u0001\u001a\u000202J\u000f\u0010\u0083\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\nJ\u001a\u0010\u0083\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u0001J/\u0010\u0083\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u001f\u001a\u00020\u000e2\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\u0001H\u0002J\u001a\u0010\u0085\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u0001J$\u0010\u0085\u0001\u001a\u0002022\u0006\u0010\\\u001a\u00020\n2\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00012\b\u0010l\u001a\u0004\u0018\u00010\u0001J\t\u0010\u0086\u0001\u001a\u00020TH\u0016J\u0013\u0010\u0087\u0001\u001a\u0004\u0018\u00010\u00012\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u001b\u0010\u0088\u0001\u001a\u0002022\u0007\u0010\u0089\u0001\u001a\u00020\n2\u0007\u0010\u008a\u0001\u001a\u00020\nH\u0002J\u0011\u0010\u008b\u0001\u001a\u0002022\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u0011\u0010\u008c\u0001\u001a\u0002022\u0006\u0010=\u001a\u00020\nH\u0002J\u0019\u0010\u008d\u0001\u001a\u0002022\u0006\u0010=\u001a\u00020\n2\u0006\u0010z\u001a\u00020%H\u0002J\u0019\u0010\u008e\u0001\u001a\u0002022\u0006\u00106\u001a\u00020\u00072\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u0011\u0010\u008e\u0001\u001a\u0002022\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u001b\u0010\u008f\u0001\u001a\u0002022\u0006\u00107\u001a\u00020\n2\b\u0010Y\u001a\u0004\u0018\u00010\u0001H\u0002J\u0011\u0010\u0090\u0001\u001a\u0002022\b\u0010Y\u001a\u0004\u0018\u00010\u0001J\u000f\u0010\u0091\u0001\u001a\u000202H\u0000¢\u0006\u0003\b\u0092\u0001J\u000f\u0010\u0093\u0001\u001a\u000202H\u0000¢\u0006\u0003\b\u0094\u0001J\u0015\u0010\u0095\u0001\u001a\u00020\n*\u00020\u001c2\u0006\u00103\u001a\u00020\nH\u0002J\u0014\u0010C\u001a\u00020\n*\u00020\u001c2\u0006\u00103\u001a\u00020\nH\u0002J\u0013\u0010\u0096\u0001\u001a\b\u0012\u0004\u0012\u00020\n0_*\u00020\u001cH\u0002J\u001b\u0010\u0097\u0001\u001a\u000202*\b0\u0098\u0001j\u0003`\u0099\u00012\u0006\u00107\u001a\u00020\nH\u0002J\u0015\u0010\u009a\u0001\u001a\u00020\n*\u00020\u001c2\u0006\u00103\u001a\u00020\nH\u0002J\u0014\u0010\"\u001a\u00020\n*\u00020\u001c2\u0006\u00107\u001a\u00020\nH\u0002J\u0015\u0010\u009b\u0001\u001a\u00020\n*\u00020\u001c2\u0006\u00103\u001a\u00020\nH\u0002J\u001d\u0010\u009c\u0001\u001a\u000202*\u00020\u001c2\u0006\u00103\u001a\u00020\n2\u0006\u0010C\u001a\u00020\nH\u0002R\u001e\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\u000e@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001e\u0010\u0012\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\n@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\fR\u000e\u0010\u0014\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u001e\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0011R\u0011\u0010\u001f\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u001f\u0010\u0011R\u000e\u0010 \u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\"\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\n@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\fR\u0010\u0010$\u001a\u0004\u0018\u00010%X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u00020\n8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b'\u0010\fR\u0018\u0010(\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010)X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u000e\u0010+\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b/\u00100¨\u0006\u009e\u0001" }, d2 = { "Landroidx/compose/runtime/SlotWriter;", "", "table", "Landroidx/compose/runtime/SlotTable;", "(Landroidx/compose/runtime/SlotTable;)V", "anchors", "Ljava/util/ArrayList;", "Landroidx/compose/runtime/Anchor;", "Lkotlin/collections/ArrayList;", "capacity", "", "getCapacity", "()I", "<set-?>", "", "closed", "getClosed", "()Z", "currentGroup", "getCurrentGroup", "currentGroupEnd", "currentSlot", "currentSlotEnd", "endStack", "Landroidx/compose/runtime/IntStack;", "groupGapLen", "groupGapStart", "groups", "", "insertCount", "isGroupEnd", "isNode", "nodeCount", "nodeCountStack", "parent", "getParent", "pendingRecalculateMarks", "Landroidx/compose/runtime/PrioritySet;", "size", "getSize$runtime_release", "slots", "", "[Ljava/lang/Object;", "slotsGapLen", "slotsGapOwner", "slotsGapStart", "startStack", "getTable$runtime_release", "()Landroidx/compose/runtime/SlotTable;", "addToGroupSizeAlongSpine", "", "address", "amount", "advanceBy", "anchor", "index", "anchorIndex", "bashGroup", "bashGroup$runtime_release", "beginInsert", "childContainsAnyMarks", "group", "close", "containsAnyGroupMarks", "containsGroupMark", "dataAnchorToDataIndex", "gapLen", "dataIndex", "dataIndexToDataAddress", "dataIndexToDataAnchor", "gapStart", "endGroup", "endInsert", "ensureStarted", "fixParentAnchorsFor", "firstChild", "groupAux", "groupIndexToAddress", "groupKey", "groupObjectKey", "groupSize", "groupSlots", "", "groupsAsString", "", "indexInCurrentGroup", "indexInGroup", "indexInParent", "insertAux", "value", "insertGroups", "insertParentGroup", "key", "insertSlots", "keys", "", "markGroup", "moveAnchors", "originalLocation", "newLocation", "moveFrom", "moveGroup", "offset", "moveGroupGapTo", "moveIntoGroupFrom", "moveSlotGapTo", "moveTo", "writer", "node", "parentAnchorToIndex", "parentIndexToAnchor", "recalculateMarks", "removeAnchors", "removeGroup", "removeGroups", "start", "len", "removeSlots", "reset", "restoreCurrentGroupEnd", "saveCurrentGroupEnd", "seek", "set", "skip", "skipGroup", "skipToGroupEnd", "slot", "groupIndex", "startData", "aux", "objectKey", "startGroup", "dataKey", "startNode", "toString", "update", "updateAnchors", "previousGapStart", "newGapStart", "updateAux", "updateContainsMark", "updateContainsMarkNow", "updateNode", "updateNodeOfGroup", "updateParentNode", "verifyDataAnchors", "verifyDataAnchors$runtime_release", "verifyParentAnchors", "verifyParentAnchors$runtime_release", "auxIndex", "dataIndexes", "groupAsString", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "nodeIndex", "slotIndex", "updateDataIndex", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SlotWriter
{
    public static final Companion Companion;
    private ArrayList<Anchor> anchors;
    private boolean closed;
    private int currentGroup;
    private int currentGroupEnd;
    private int currentSlot;
    private int currentSlotEnd;
    private final IntStack endStack;
    private int groupGapLen;
    private int groupGapStart;
    private int[] groups;
    private int insertCount;
    private int nodeCount;
    private final IntStack nodeCountStack;
    private int parent;
    private PrioritySet pendingRecalculateMarks;
    private Object[] slots;
    private int slotsGapLen;
    private int slotsGapOwner;
    private int slotsGapStart;
    private final IntStack startStack;
    private final SlotTable table;
    
    static {
        Companion = new Companion(null);
    }
    
    public SlotWriter(final SlotTable table) {
        Intrinsics.checkNotNullParameter((Object)table, "table");
        this.table = table;
        this.groups = table.getGroups();
        this.slots = table.getSlots();
        this.anchors = table.getAnchors$runtime_release();
        this.groupGapStart = table.getGroupsSize();
        this.groupGapLen = this.groups.length / 5 - table.getGroupsSize();
        this.currentGroupEnd = table.getGroupsSize();
        this.slotsGapStart = table.getSlotsSize();
        this.slotsGapLen = this.slots.length - table.getSlotsSize();
        this.slotsGapOwner = table.getGroupsSize();
        this.startStack = new IntStack();
        this.endStack = new IntStack();
        this.nodeCountStack = new IntStack();
        this.parent = -1;
    }
    
    public static final /* synthetic */ ArrayList access$getAnchors$p(final SlotWriter slotWriter) {
        return slotWriter.anchors;
    }
    
    public static final /* synthetic */ int access$getCurrentSlot$p(final SlotWriter slotWriter) {
        return slotWriter.currentSlot;
    }
    
    public static final /* synthetic */ int access$getGroupGapStart$p(final SlotWriter slotWriter) {
        return slotWriter.groupGapStart;
    }
    
    public static final /* synthetic */ int[] access$getGroups$p(final SlotWriter slotWriter) {
        return slotWriter.groups;
    }
    
    public static final /* synthetic */ int access$getNodeCount$p(final SlotWriter slotWriter) {
        return slotWriter.nodeCount;
    }
    
    public static final /* synthetic */ Object[] access$getSlots$p(final SlotWriter slotWriter) {
        return slotWriter.slots;
    }
    
    public static final /* synthetic */ int access$getSlotsGapLen$p(final SlotWriter slotWriter) {
        return slotWriter.slotsGapLen;
    }
    
    public static final /* synthetic */ int access$getSlotsGapOwner$p(final SlotWriter slotWriter) {
        return slotWriter.slotsGapOwner;
    }
    
    public static final /* synthetic */ int access$getSlotsGapStart$p(final SlotWriter slotWriter) {
        return slotWriter.slotsGapStart;
    }
    
    public static final /* synthetic */ void access$setCurrentGroup$p(final SlotWriter slotWriter, final int currentGroup) {
        slotWriter.currentGroup = currentGroup;
    }
    
    public static final /* synthetic */ void access$setCurrentSlot$p(final SlotWriter slotWriter, final int currentSlot) {
        slotWriter.currentSlot = currentSlot;
    }
    
    public static final /* synthetic */ void access$setNodeCount$p(final SlotWriter slotWriter, final int nodeCount) {
        slotWriter.nodeCount = nodeCount;
    }
    
    public static final /* synthetic */ void access$setSlotsGapOwner$p(final SlotWriter slotWriter, final int slotsGapOwner) {
        slotWriter.slotsGapOwner = slotsGapOwner;
    }
    
    private final int auxIndex(final int[] array, final int n) {
        return this.dataIndex(array, n) + SlotTableKt.access$countOneBits(SlotTableKt.access$groupInfo(array, n) >> 29);
    }
    
    private final boolean childContainsAnyMarks(final int n) {
        for (int i = n + 1; i < n + this.groupSize(n); i += this.groupSize(i)) {
            if (SlotTableKt.access$containsAnyMark(this.groups, this.groupIndexToAddress(i))) {
                return true;
            }
        }
        return false;
    }
    
    private final boolean containsAnyGroupMarks(final int n) {
        return n >= 0 && SlotTableKt.access$containsAnyMark(this.groups, this.groupIndexToAddress(n));
    }
    
    private final boolean containsGroupMark(final int n) {
        return n >= 0 && SlotTableKt.access$containsMark(this.groups, this.groupIndexToAddress(n));
    }
    
    private final int dataAnchorToDataIndex(final int n, final int n2, final int n3) {
        int n4 = n;
        if (n < 0) {
            n4 = n3 - n2 + n + 1;
        }
        return n4;
    }
    
    private final int dataIndex(final int n) {
        return this.dataIndex(this.groups, this.groupIndexToAddress(n));
    }
    
    private final int dataIndex(final int[] array, int dataAnchorToDataIndex) {
        if (dataAnchorToDataIndex >= this.getCapacity()) {
            dataAnchorToDataIndex = this.slots.length - this.slotsGapLen;
        }
        else {
            dataAnchorToDataIndex = this.dataAnchorToDataIndex(SlotTableKt.access$dataAnchor(array, dataAnchorToDataIndex), this.slotsGapLen, this.slots.length);
        }
        return dataAnchorToDataIndex;
    }
    
    private final int dataIndexToDataAddress(int n) {
        if (n >= this.slotsGapStart) {
            n += this.slotsGapLen;
        }
        return n;
    }
    
    private final int dataIndexToDataAnchor(final int n, final int n2, final int n3, final int n4) {
        int n5 = n;
        if (n > n2) {
            n5 = -(n4 - n3 - n + 1);
        }
        return n5;
    }
    
    private final List<Integer> dataIndexes(final int[] array) {
        final int[] groups = this.groups;
        int i = 0;
        final List dataAnchors$default = SlotTableKt.dataAnchors$default(groups, 0, 1, null);
        final List plus = CollectionsKt.plus((Collection)CollectionsKt.slice(dataAnchors$default, RangesKt.until(0, this.groupGapStart)), (Iterable)CollectionsKt.slice(dataAnchors$default, RangesKt.until(this.groupGapStart + this.groupGapLen, array.length / 5)));
        final ArrayList list = new ArrayList<Integer>(plus.size());
        while (i < plus.size()) {
            list.add(Integer.valueOf(this.dataAnchorToDataIndex(plus.get(i).intValue(), this.slotsGapLen, this.slots.length)));
            ++i;
        }
        return (List)list;
    }
    
    private final void fixParentAnchorsFor(int n, final int n2, int i) {
        final int parentIndexToAnchor = this.parentIndexToAnchor(n, this.groupGapStart);
        while (i < n2) {
            updateParentAnchor(this.groups, this.groupIndexToAddress(i), parentIndexToAnchor);
            n = groupSize(this.groups, this.groupIndexToAddress(i)) + i;
            this.fixParentAnchorsFor(i, n, i + 1);
            i = n;
        }
    }
    
    private final int getCapacity() {
        return this.groups.length / 5;
    }
    
    private final void groupAsString(final StringBuilder sb, int i) {
        final int groupIndexToAddress = this.groupIndexToAddress(i);
        sb.append("Group(");
        if (i < 10) {
            sb.append(' ');
        }
        if (i < 100) {
            sb.append(' ');
        }
        if (i < 1000) {
            sb.append(' ');
        }
        sb.append(i);
        if (groupIndexToAddress != i) {
            sb.append("(");
            sb.append(groupIndexToAddress);
            sb.append(")");
        }
        sb.append('#');
        sb.append(groupSize(this.groups, groupIndexToAddress));
        final boolean groupAsString$isStarted = groupAsString$isStarted(this, i);
        if (groupAsString$isStarted) {
            sb.append('?');
        }
        sb.append('^');
        sb.append(this.parentAnchorToIndex(parentAnchor(this.groups, groupIndexToAddress)));
        sb.append(": key=");
        sb.append(key(this.groups, groupIndexToAddress));
        sb.append(", nodes=");
        sb.append(nodeCount(this.groups, groupIndexToAddress));
        if (groupAsString$isStarted) {
            sb.append('?');
        }
        sb.append(", dataAnchor=");
        sb.append(dataAnchor(this.groups, groupIndexToAddress));
        sb.append(", parentAnchor=");
        sb.append(parentAnchor(this.groups, groupIndexToAddress));
        if (isNode(this.groups, groupIndexToAddress)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(", node=");
            sb2.append(this.slots[this.dataIndexToDataAddress(this.nodeIndex(this.groups, groupIndexToAddress))]);
            sb.append(sb2.toString());
        }
        final int slotIndex = this.slotIndex(this.groups, groupIndexToAddress);
        final int dataIndex = this.dataIndex(this.groups, groupIndexToAddress + 1);
        if (dataIndex > slotIndex) {
            sb.append(", [");
            for (i = slotIndex; i < dataIndex; ++i) {
                if (i != slotIndex) {
                    sb.append(", ");
                }
                sb.append(String.valueOf(this.slots[this.dataIndexToDataAddress(i)]));
            }
            sb.append(']');
        }
        sb.append(")");
    }
    
    private static final boolean groupAsString$isStarted(final SlotWriter slotWriter, final int n) {
        return n < slotWriter.currentGroup && (n == slotWriter.parent || slotWriter.startStack.indexOf(n) >= 0 || groupAsString$isStarted(slotWriter, slotWriter.parent(n)));
    }
    
    private final int groupIndexToAddress(int n) {
        if (n >= this.groupGapStart) {
            n += this.groupGapLen;
        }
        return n;
    }
    
    private final void insertGroups(final int n) {
        if (n > 0) {
            final int currentGroup = this.currentGroup;
            this.moveGroupGapTo(currentGroup);
            final int groupGapStart = this.groupGapStart;
            final int groupGapLen = this.groupGapLen;
            final int[] groups = this.groups;
            final int n2 = groups.length / 5;
            final int n3 = n2 - groupGapLen;
            int slotsGapStart = 0;
            int n4;
            if ((n4 = groupGapLen) < n) {
                final int max = Math.max(Math.max(n2 * 2, n3 + n), 32);
                final int[] groups2 = new int[max * 5];
                n4 = max - n3;
                ArraysKt.copyInto(groups, groups2, 0, 0, groupGapStart * 5);
                ArraysKt.copyInto(groups, groups2, (groupGapStart + n4) * 5, (groupGapLen + groupGapStart) * 5, n2 * 5);
                this.groups = groups2;
            }
            final int currentGroupEnd = this.currentGroupEnd;
            if (currentGroupEnd >= groupGapStart) {
                this.currentGroupEnd = currentGroupEnd + n;
            }
            final int groupGapStart2 = groupGapStart + n;
            this.groupGapStart = groupGapStart2;
            this.groupGapLen = n4 - n;
            int dataIndex;
            if (n3 > 0) {
                dataIndex = this.dataIndex(currentGroup + n);
            }
            else {
                dataIndex = 0;
            }
            if (this.slotsGapOwner >= groupGapStart) {
                slotsGapStart = this.slotsGapStart;
            }
            final int dataIndexToDataAnchor = this.dataIndexToDataAnchor(dataIndex, slotsGapStart, this.slotsGapLen, this.slots.length);
            for (int i = groupGapStart; i < groupGapStart2; ++i) {
                updateDataAnchor(this.groups, i, dataIndexToDataAnchor);
            }
            final int slotsGapOwner = this.slotsGapOwner;
            if (slotsGapOwner >= groupGapStart) {
                this.slotsGapOwner = slotsGapOwner + n;
            }
        }
    }
    
    private final void insertSlots(final int n, int i) {
        if (n > 0) {
            this.moveSlotGapTo(this.currentSlot, i);
            final int slotsGapStart = this.slotsGapStart;
            final int slotsGapLen = this.slotsGapLen;
            if ((i = slotsGapLen) < n) {
                final Object[] slots = this.slots;
                final int length = slots.length;
                final int n2 = length - slotsGapLen;
                final int max = Math.max(Math.max(length * 2, n2 + n), 32);
                final Object[] slots2 = new Object[max];
                for (i = 0; i < max; ++i) {
                    slots2[i] = null;
                }
                i = max - n2;
                ArraysKt.copyInto(slots, slots2, 0, 0, slotsGapStart);
                ArraysKt.copyInto(slots, slots2, slotsGapStart + i, slotsGapLen + slotsGapStart, length);
                this.slots = slots2;
            }
            final int currentSlotEnd = this.currentSlotEnd;
            if (currentSlotEnd >= slotsGapStart) {
                this.currentSlotEnd = currentSlotEnd + n;
            }
            this.slotsGapStart = slotsGapStart + n;
            this.slotsGapLen = i - n;
        }
    }
    
    private final List<Integer> keys() {
        final List keys$default = SlotTableKt.keys$default(this.groups, 0, 1, null);
        final ArrayList list = new ArrayList<Number>(keys$default.size());
        for (int size = keys$default.size(), i = 0; i < size; ++i) {
            final Object value = keys$default.get(i);
            ((Number)value).intValue();
            final int groupGapStart = this.groupGapStart;
            if (i < groupGapStart || i >= groupGapStart + this.groupGapLen) {
                list.add((Number)value);
            }
        }
        return (List)list;
    }
    
    private final void moveAnchors(final int n, final int n2, int i) {
        final int size$runtime_release = this.getSize$runtime_release();
        final int j = locationOf(this.anchors, n, size$runtime_release);
        final List list = new ArrayList();
        if (j >= 0) {
            while (j < this.anchors.size()) {
                final Anchor value = this.anchors.get(j);
                Intrinsics.checkNotNullExpressionValue((Object)value, "anchors[index]");
                final Anchor anchor = value;
                final int anchorIndex = this.anchorIndex(anchor);
                if (anchorIndex < n || anchorIndex >= i + n) {
                    break;
                }
                list.add(anchor);
                this.anchors.remove(j);
            }
        }
        Anchor element;
        int location$runtime_release;
        for (i = 0; i < list.size(); ++i) {
            element = list.get(i);
            location$runtime_release = this.anchorIndex(element) + (n2 - n);
            if (location$runtime_release >= this.groupGapStart) {
                element.setLocation$runtime_release(-(size$runtime_release - location$runtime_release));
            }
            else {
                element.setLocation$runtime_release(location$runtime_release);
            }
            this.anchors.add(locationOf(this.anchors, location$runtime_release, size$runtime_release), element);
        }
    }
    
    private final void moveGroupGapTo(final int groupGapStart) {
        final int groupGapLen = this.groupGapLen;
        final int groupGapStart2 = this.groupGapStart;
        if (groupGapStart2 != groupGapStart) {
            final boolean empty = this.anchors.isEmpty();
            boolean b = true;
            if (empty ^ true) {
                this.updateAnchors(groupGapStart2, groupGapStart);
            }
            if (groupGapLen > 0) {
                final int[] groups = this.groups;
                final int n = groupGapStart * 5;
                final int n2 = groupGapLen * 5;
                final int n3 = groupGapStart2 * 5;
                if (groupGapStart < groupGapStart2) {
                    ArraysKt.copyInto(groups, groups, n2 + n, n, n3);
                }
                else {
                    ArraysKt.copyInto(groups, groups, n3, n3 + n2, n + n2);
                }
            }
            int i;
            if (groupGapStart < (i = groupGapStart2)) {
                i = groupGapStart + groupGapLen;
            }
            final int capacity = this.getCapacity();
            if (i >= capacity) {
                b = false;
            }
            ComposerKt.runtimeCheck(b);
            while (i < capacity) {
                final int access$parentAnchor = parentAnchor(this.groups, i);
                final int parentIndexToAnchor = this.parentIndexToAnchor(this.parentAnchorToIndex(access$parentAnchor), groupGapStart);
                if (parentIndexToAnchor != access$parentAnchor) {
                    updateParentAnchor(this.groups, i, parentIndexToAnchor);
                }
                final int n4 = i + 1;
                if ((i = n4) == groupGapStart) {
                    i = n4 + groupGapLen;
                }
            }
        }
        this.groupGapStart = groupGapStart;
    }
    
    private final void moveSlotGapTo(final int slotsGapStart, int i) {
        final int slotsGapLen = this.slotsGapLen;
        final int slotsGapStart2 = this.slotsGapStart;
        final int slotsGapOwner = this.slotsGapOwner;
        if (slotsGapStart2 != slotsGapStart) {
            final Object[] slots = this.slots;
            if (slotsGapStart < slotsGapStart2) {
                ArraysKt.copyInto(slots, slots, slotsGapStart + slotsGapLen, slotsGapStart, slotsGapStart2);
            }
            else {
                ArraysKt.copyInto(slots, slots, slotsGapStart2, slotsGapStart2 + slotsGapLen, slotsGapStart + slotsGapLen);
            }
            ArraysKt.fill(slots, (Object)null, slotsGapStart, slotsGapStart + slotsGapLen);
        }
        final int min = Math.min(i + 1, this.getSize$runtime_release());
        if (slotsGapOwner != min) {
            final int n = this.slots.length - slotsGapLen;
            if (min < slotsGapOwner) {
                i = this.groupIndexToAddress(min);
                final int groupIndexToAddress = this.groupIndexToAddress(slotsGapOwner);
                final int groupGapStart = this.groupGapStart;
                while (i < groupIndexToAddress) {
                    final int access$dataAnchor = dataAnchor(this.groups, i);
                    if (access$dataAnchor < 0) {
                        ComposerKt.composeRuntimeError("Unexpected anchor value, expected a positive anchor".toString());
                        throw new KotlinNothingValueException();
                    }
                    updateDataAnchor(this.groups, i, -(n - access$dataAnchor + 1));
                    final int n2 = i + 1;
                    if ((i = n2) != groupGapStart) {
                        continue;
                    }
                    i = n2 + this.groupGapLen;
                }
            }
            else {
                i = this.groupIndexToAddress(slotsGapOwner);
                while (i < this.groupIndexToAddress(min)) {
                    final int access$dataAnchor2 = dataAnchor(this.groups, i);
                    if (access$dataAnchor2 >= 0) {
                        ComposerKt.composeRuntimeError("Unexpected anchor value, expected a negative anchor".toString());
                        throw new KotlinNothingValueException();
                    }
                    updateDataAnchor(this.groups, i, access$dataAnchor2 + n + 1);
                    final int n3 = i + 1;
                    if ((i = n3) != this.groupGapStart) {
                        continue;
                    }
                    i = n3 + this.groupGapLen;
                }
            }
            this.slotsGapOwner = min;
        }
        this.slotsGapStart = slotsGapStart;
    }
    
    private final int nodeIndex(final int[] array, final int n) {
        return this.dataIndex(array, n);
    }
    
    private final int parent(final int[] array, final int n) {
        return this.parentAnchorToIndex(parentAnchor(array, this.groupIndexToAddress(n)));
    }
    
    private final int parentAnchorToIndex(int n) {
        if (n <= -2) {
            n = this.getSize$runtime_release() + n + 2;
        }
        return n;
    }
    
    private final int parentIndexToAnchor(int n, final int n2) {
        if (n >= n2) {
            n = -(this.getSize$runtime_release() - n + 2);
        }
        return n;
    }
    
    private final void recalculateMarks() {
        final PrioritySet pendingRecalculateMarks = this.pendingRecalculateMarks;
        if (pendingRecalculateMarks != null) {
            while (pendingRecalculateMarks.isNotEmpty()) {
                this.updateContainsMarkNow(pendingRecalculateMarks.takeMax(), pendingRecalculateMarks);
            }
        }
    }
    
    private final boolean removeAnchors(final int n, int capacity) {
        final int groupGapLen = this.groupGapLen;
        final int n2 = capacity + n;
        capacity = this.getCapacity();
        int i;
        capacity = (i = locationOf(this.anchors, n2, capacity - groupGapLen));
        if (capacity >= this.anchors.size()) {
            i = capacity - 1;
        }
        int fromIndex = i + 1;
        boolean b = false;
        capacity = 0;
        while (i >= 0) {
            final Anchor value = this.anchors.get(i);
            Intrinsics.checkNotNullExpressionValue((Object)value, "anchors[index]");
            final Anchor anchor = value;
            final int anchorIndex = this.anchorIndex(anchor);
            if (anchorIndex < n) {
                break;
            }
            int n3 = capacity;
            if (anchorIndex < n2) {
                anchor.setLocation$runtime_release(Integer.MIN_VALUE);
                if ((n3 = capacity) == 0) {
                    n3 = i + 1;
                }
                fromIndex = i;
            }
            --i;
            capacity = n3;
        }
        if (fromIndex < capacity) {
            b = true;
        }
        if (b) {
            this.anchors.subList(fromIndex, capacity).clear();
        }
        return b;
    }
    
    private final boolean removeGroups(int currentGroupEnd, final int n) {
        boolean b = false;
        boolean removeAnchors = false;
        if (n > 0) {
            final ArrayList<Anchor> anchors = this.anchors;
            this.moveGroupGapTo(currentGroupEnd);
            if (anchors.isEmpty() ^ true) {
                removeAnchors = this.removeAnchors(currentGroupEnd, n);
            }
            this.groupGapStart = currentGroupEnd;
            this.groupGapLen += n;
            final int slotsGapOwner = this.slotsGapOwner;
            if (slotsGapOwner > currentGroupEnd) {
                this.slotsGapOwner = Math.max(currentGroupEnd, slotsGapOwner - n);
            }
            currentGroupEnd = this.currentGroupEnd;
            if (currentGroupEnd >= this.groupGapStart) {
                this.currentGroupEnd = currentGroupEnd - n;
            }
            b = removeAnchors;
            if (this.containsGroupMark(this.parent)) {
                this.updateContainsMark(this.parent);
                b = removeAnchors;
            }
        }
        return b;
    }
    
    private final void removeSlots(final int slotsGapStart, final int n, int currentSlotEnd) {
        if (n > 0) {
            final int slotsGapLen = this.slotsGapLen;
            final int n2 = slotsGapStart + n;
            this.moveSlotGapTo(n2, currentSlotEnd);
            this.slotsGapStart = slotsGapStart;
            this.slotsGapLen = slotsGapLen + n;
            ArraysKt.fill(this.slots, (Object)null, slotsGapStart, n2);
            currentSlotEnd = this.currentSlotEnd;
            if (currentSlotEnd >= slotsGapStart) {
                this.currentSlotEnd = currentSlotEnd - n;
            }
        }
    }
    
    private final int restoreCurrentGroupEnd() {
        return this.currentGroupEnd = this.getCapacity() - this.groupGapLen - this.endStack.pop();
    }
    
    private final void saveCurrentGroupEnd() {
        this.endStack.push(this.getCapacity() - this.groupGapLen - this.currentGroupEnd);
    }
    
    private final int slotIndex(final int[] array, int dataAnchorToDataIndex) {
        if (dataAnchorToDataIndex >= this.getCapacity()) {
            dataAnchorToDataIndex = this.slots.length - this.slotsGapLen;
        }
        else {
            dataAnchorToDataIndex = this.dataAnchorToDataIndex(slotAnchor(array, dataAnchorToDataIndex), this.slotsGapLen, this.slots.length);
        }
        return dataAnchorToDataIndex;
    }
    
    private final void startGroup(int currentGroupEnd, final Object o, final boolean b, final Object o2) {
        final boolean b2 = this.insertCount > 0;
        this.nodeCountStack.push(this.nodeCount);
        if (b2) {
            this.insertGroups(1);
            final int currentGroup = this.currentGroup;
            final int groupIndexToAddress = this.groupIndexToAddress(currentGroup);
            int n;
            if (o != Composer.Companion.getEmpty()) {
                n = 1;
            }
            else {
                n = 0;
            }
            int n2;
            if (!b && o2 != Composer.Companion.getEmpty()) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            initGroup(this.groups, groupIndexToAddress, currentGroupEnd, b, (boolean)(n != 0), (boolean)(n2 != 0), this.parent, this.currentSlot);
            this.currentSlotEnd = this.currentSlot;
            currentGroupEnd = (b ? 1 : 0) + n + n2;
            if (currentGroupEnd > 0) {
                this.insertSlots(currentGroupEnd, currentGroup);
                final Object[] slots = this.slots;
                final int n3 = currentGroupEnd = this.currentSlot;
                if ((b ? 1 : 0) != 0) {
                    slots[n3] = o2;
                    currentGroupEnd = n3 + 1;
                }
                int n4 = currentGroupEnd;
                if (n != 0) {
                    slots[currentGroupEnd] = o;
                    n4 = currentGroupEnd + 1;
                }
                currentGroupEnd = n4;
                if (n2 != 0) {
                    slots[n4] = o2;
                    currentGroupEnd = n4 + 1;
                }
                this.currentSlot = currentGroupEnd;
            }
            this.nodeCount = 0;
            currentGroupEnd = currentGroup + 1;
            this.parent = currentGroup;
            this.currentGroup = currentGroupEnd;
        }
        else {
            currentGroupEnd = this.parent;
            this.startStack.push(currentGroupEnd);
            this.saveCurrentGroupEnd();
            final int currentGroup2 = this.currentGroup;
            currentGroupEnd = this.groupIndexToAddress(currentGroup2);
            if (!Intrinsics.areEqual(o2, Composer.Companion.getEmpty())) {
                if ((b ? 1 : 0) != 0) {
                    this.updateNode(o2);
                }
                else {
                    this.updateAux(o2);
                }
            }
            this.currentSlot = this.slotIndex(this.groups, currentGroupEnd);
            this.currentSlotEnd = this.dataIndex(this.groups, this.groupIndexToAddress(this.currentGroup + 1));
            this.nodeCount = nodeCount(this.groups, currentGroupEnd);
            this.parent = currentGroup2;
            this.currentGroup = currentGroup2 + 1;
            currentGroupEnd = currentGroup2 + groupSize(this.groups, currentGroupEnd);
        }
        this.currentGroupEnd = currentGroupEnd;
    }
    
    private final void updateAnchors(int i, int location$runtime_release) {
        final int n = this.getCapacity() - this.groupGapLen;
        if (i < location$runtime_release) {
            Anchor value;
            Anchor anchor;
            int location$runtime_release2;
            int location$runtime_release3;
            for (i = locationOf(this.anchors, i, n); i < this.anchors.size(); ++i) {
                value = this.anchors.get(i);
                Intrinsics.checkNotNullExpressionValue((Object)value, "anchors[index]");
                anchor = value;
                location$runtime_release2 = anchor.getLocation$runtime_release();
                if (location$runtime_release2 >= 0) {
                    break;
                }
                location$runtime_release3 = location$runtime_release2 + n;
                if (location$runtime_release3 >= location$runtime_release) {
                    break;
                }
                anchor.setLocation$runtime_release(location$runtime_release3);
            }
        }
        else {
            Anchor value2;
            Anchor anchor2;
            for (i = locationOf(this.anchors, location$runtime_release, n); i < this.anchors.size(); ++i) {
                value2 = this.anchors.get(i);
                Intrinsics.checkNotNullExpressionValue((Object)value2, "anchors[index]");
                anchor2 = value2;
                location$runtime_release = anchor2.getLocation$runtime_release();
                if (location$runtime_release < 0) {
                    break;
                }
                anchor2.setLocation$runtime_release(-(n - location$runtime_release));
            }
        }
    }
    
    private final void updateContainsMark(final int n) {
        if (n >= 0) {
            PrioritySet pendingRecalculateMarks;
            if ((pendingRecalculateMarks = this.pendingRecalculateMarks) == null) {
                pendingRecalculateMarks = new PrioritySet(null, 1, null);
                this.pendingRecalculateMarks = pendingRecalculateMarks;
            }
            pendingRecalculateMarks.add(n);
        }
    }
    
    private final void updateContainsMarkNow(int parent, final PrioritySet set) {
        final int groupIndexToAddress = this.groupIndexToAddress(parent);
        final boolean childContainsAnyMarks = this.childContainsAnyMarks(parent);
        if (containsMark(this.groups, groupIndexToAddress) != childContainsAnyMarks) {
            updateContainsMark(this.groups, groupIndexToAddress, childContainsAnyMarks);
            parent = this.parent(parent);
            if (parent >= 0) {
                set.add(parent);
            }
        }
    }
    
    private final void updateDataIndex(final int[] array, final int n, final int n2) {
        updateDataAnchor(array, n, this.dataIndexToDataAnchor(n2, this.slotsGapStart, this.slotsGapLen, this.slots.length));
    }
    
    private final void updateNodeOfGroup(final int i, Object o) {
        final int groupIndexToAddress = this.groupIndexToAddress(i);
        final int[] groups = this.groups;
        if (groupIndexToAddress < groups.length && isNode(groups, groupIndexToAddress)) {
            this.slots[this.dataIndexToDataAddress(this.nodeIndex(this.groups, groupIndexToAddress))] = o;
            return;
        }
        o = new StringBuilder();
        ((StringBuilder)o).append("Updating the node of a group at ");
        ((StringBuilder)o).append(i);
        ((StringBuilder)o).append(" that was not created with as a node group");
        ComposerKt.composeRuntimeError(((StringBuilder)o).toString().toString());
        throw new KotlinNothingValueException();
    }
    
    public final void addToGroupSizeAlongSpine(int i, final int n) {
        while (i > 0) {
            final int[] groups = this.groups;
            updateGroupSize(groups, i, groupSize(groups, i) + n);
            i = this.groupIndexToAddress(this.parentAnchorToIndex(parentAnchor(this.groups, i)));
        }
    }
    
    public final void advanceBy(int dataIndex) {
        final int n = 1;
        if (dataIndex < 0) {
            ComposerKt.composeRuntimeError("Cannot seek backwards".toString());
            throw new KotlinNothingValueException();
        }
        if (this.insertCount > 0) {
            throw new IllegalStateException("Cannot call seek() while inserting".toString());
        }
        if (dataIndex == 0) {
            return;
        }
        final int currentGroup = this.currentGroup + dataIndex;
        if (currentGroup >= this.parent && currentGroup <= this.currentGroupEnd) {
            dataIndex = n;
        }
        else {
            dataIndex = 0;
        }
        if (dataIndex != 0) {
            this.currentGroup = currentGroup;
            dataIndex = this.dataIndex(this.groups, this.groupIndexToAddress(currentGroup));
            this.currentSlot = dataIndex;
            this.currentSlotEnd = dataIndex;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot seek outside the current group (");
        sb.append(this.parent);
        sb.append('-');
        sb.append(this.currentGroupEnd);
        sb.append(')');
        ComposerKt.composeRuntimeError(sb.toString().toString());
        throw new KotlinNothingValueException();
    }
    
    public final Anchor anchor(int n) {
        final ArrayList<Anchor> anchors = this.anchors;
        final int access$search = search(anchors, n, this.getSize$runtime_release());
        Anchor element;
        if (access$search < 0) {
            if (n > this.groupGapStart) {
                n = -(this.getSize$runtime_release() - n);
            }
            element = new Anchor(n);
            anchors.add(-(access$search + 1), element);
        }
        else {
            final Anchor value = anchors.get(access$search);
            Intrinsics.checkNotNullExpressionValue((Object)value, "get(location)");
            element = value;
        }
        return element;
    }
    
    public final int anchorIndex(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        int location$runtime_release;
        final int n = location$runtime_release = anchor.getLocation$runtime_release();
        if (n < 0) {
            location$runtime_release = n + this.getSize$runtime_release();
        }
        return location$runtime_release;
    }
    
    public final void bashGroup$runtime_release() {
        this.startGroup();
        while (!this.isGroupEnd()) {
            this.insertParentGroup(-3);
            this.skipGroup();
        }
        this.endGroup();
    }
    
    public final void beginInsert() {
        if (this.insertCount++ == 0) {
            this.saveCurrentGroupEnd();
        }
    }
    
    public final void close() {
        this.closed = true;
        if (this.startStack.isEmpty()) {
            this.moveGroupGapTo(this.getSize$runtime_release());
            this.moveSlotGapTo(this.slots.length - this.slotsGapLen, this.groupGapStart);
            this.recalculateMarks();
        }
        this.table.close$runtime_release(this, this.groups, this.groupGapStart, this.slots, this.slotsGapStart, this.anchors);
    }
    
    public final int endGroup() {
        final int insertCount = this.insertCount;
        final int n = 1;
        final int n2 = 1;
        final int n3 = 0;
        final boolean b = insertCount > 0;
        final int currentGroup = this.currentGroup;
        final int currentGroupEnd = this.currentGroupEnd;
        final int parent = this.parent;
        final int groupIndexToAddress = this.groupIndexToAddress(parent);
        final int nodeCount = this.nodeCount;
        final int n4 = currentGroup - parent;
        final boolean access$isNode = isNode(this.groups, groupIndexToAddress);
        if (b) {
            updateGroupSize(this.groups, groupIndexToAddress, n4);
            updateNodeCount(this.groups, groupIndexToAddress, nodeCount);
            final int pop = this.nodeCountStack.pop();
            int n5;
            if (access$isNode) {
                n5 = n2;
            }
            else {
                n5 = nodeCount;
            }
            this.nodeCount = pop + n5;
            this.parent = this.parent(this.groups, parent);
        }
        else {
            int n6;
            if (currentGroup == currentGroupEnd) {
                n6 = n;
            }
            else {
                n6 = 0;
            }
            if (n6 == 0) {
                ComposerKt.composeRuntimeError("Expected to be at the end of a group".toString());
                throw new KotlinNothingValueException();
            }
            final int access$groupSize = groupSize(this.groups, groupIndexToAddress);
            final int access$nodeCount = nodeCount(this.groups, groupIndexToAddress);
            updateGroupSize(this.groups, groupIndexToAddress, n4);
            updateNodeCount(this.groups, groupIndexToAddress, nodeCount);
            final int pop2 = this.startStack.pop();
            this.restoreCurrentGroupEnd();
            this.parent = pop2;
            final int parent2 = this.parent(this.groups, parent);
            final int pop3 = this.nodeCountStack.pop();
            this.nodeCount = pop3;
            if (parent2 == pop2) {
                int n7;
                if (access$isNode) {
                    n7 = n3;
                }
                else {
                    n7 = nodeCount - access$nodeCount;
                }
                this.nodeCount = pop3 + n7;
            }
            else {
                final int n8 = n4 - access$groupSize;
                int n9;
                if (access$isNode) {
                    n9 = 0;
                }
                else {
                    n9 = nodeCount - access$nodeCount;
                }
                int n10 = n9;
                int parent3 = parent2;
                int n11 = 0;
                Label_0439: {
                    if (n8 == 0) {
                        if ((n11 = n9) == 0) {
                            break Label_0439;
                        }
                        parent3 = parent2;
                        n10 = n9;
                    }
                    while (true) {
                        n11 = n10;
                        if (parent3 == 0) {
                            break;
                        }
                        n11 = n10;
                        if (parent3 == pop2) {
                            break;
                        }
                        if (n10 == 0) {
                            n11 = n10;
                            if (n8 == 0) {
                                break;
                            }
                        }
                        final int groupIndexToAddress2 = this.groupIndexToAddress(parent3);
                        if (n8 != 0) {
                            updateGroupSize(this.groups, groupIndexToAddress2, groupSize(this.groups, groupIndexToAddress2) + n8);
                        }
                        if (n10 != 0) {
                            final int[] groups = this.groups;
                            updateNodeCount(groups, groupIndexToAddress2, nodeCount(groups, groupIndexToAddress2) + n10);
                        }
                        if (isNode(this.groups, groupIndexToAddress2)) {
                            n10 = 0;
                        }
                        parent3 = this.parent(this.groups, parent3);
                    }
                }
                this.nodeCount += n11;
            }
        }
        return nodeCount;
    }
    
    public final void endInsert() {
        final int insertCount = this.insertCount;
        final int n = 1;
        if (insertCount > 0) {
            if ((this.insertCount = insertCount - 1) == 0) {
                int n2;
                if (this.nodeCountStack.getSize() == this.startStack.getSize()) {
                    n2 = n;
                }
                else {
                    n2 = 0;
                }
                if (n2 == 0) {
                    ComposerKt.composeRuntimeError("startGroup/endGroup mismatch while inserting".toString());
                    throw new KotlinNothingValueException();
                }
                this.restoreCurrentGroupEnd();
            }
            return;
        }
        throw new IllegalStateException("Unbalanced begin/end insert".toString());
    }
    
    public final void ensureStarted(final int n) {
        final int insertCount = this.insertCount;
        final int n2 = 1;
        if (insertCount <= 0) {
            final int parent = this.parent;
            if (parent != n) {
                int n3;
                if (n >= parent && n < this.currentGroupEnd) {
                    n3 = n2;
                }
                else {
                    n3 = 0;
                }
                if (n3 == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Started group at ");
                    sb.append(n);
                    sb.append(" must be a subgroup of the group at ");
                    sb.append(parent);
                    ComposerKt.composeRuntimeError(sb.toString().toString());
                    throw new KotlinNothingValueException();
                }
                final int currentGroup = this.currentGroup;
                final int currentSlot = this.currentSlot;
                final int currentSlotEnd = this.currentSlotEnd;
                this.currentGroup = n;
                this.startGroup();
                this.currentGroup = currentGroup;
                this.currentSlot = currentSlot;
                this.currentSlotEnd = currentSlotEnd;
            }
            return;
        }
        ComposerKt.composeRuntimeError("Cannot call ensureStarted() while inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void ensureStarted(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        this.ensureStarted(anchor.toIndexFor(this));
    }
    
    public final boolean getClosed() {
        return this.closed;
    }
    
    public final int getCurrentGroup() {
        return this.currentGroup;
    }
    
    public final int getParent() {
        return this.parent;
    }
    
    public final int getSize$runtime_release() {
        return this.getCapacity() - this.groupGapLen;
    }
    
    public final SlotTable getTable$runtime_release() {
        return this.table;
    }
    
    public final Object groupAux(int groupIndexToAddress) {
        groupIndexToAddress = this.groupIndexToAddress(groupIndexToAddress);
        Object empty;
        if (hasAux(this.groups, groupIndexToAddress)) {
            empty = this.slots[this.auxIndex(this.groups, groupIndexToAddress)];
        }
        else {
            empty = Composer.Companion.getEmpty();
        }
        return empty;
    }
    
    public final int groupKey(final int n) {
        return key(this.groups, this.groupIndexToAddress(n));
    }
    
    public final Object groupObjectKey(int groupIndexToAddress) {
        groupIndexToAddress = this.groupIndexToAddress(groupIndexToAddress);
        Object o;
        if (hasObjectKey(this.groups, groupIndexToAddress)) {
            o = this.slots[objectKeyIndex(this.groups, groupIndexToAddress)];
        }
        else {
            o = null;
        }
        return o;
    }
    
    public final int groupSize(final int n) {
        return groupSize(this.groups, this.groupIndexToAddress(n));
    }
    
    public final Iterator<Object> groupSlots() {
        final int dataIndex = this.dataIndex(this.groups, this.groupIndexToAddress(this.currentGroup));
        final int[] groups = this.groups;
        final int currentGroup = this.currentGroup;
        return (Iterator<Object>)new SlotWriter$groupSlots.SlotWriter$groupSlots$1(dataIndex, this.dataIndex(groups, this.groupIndexToAddress(currentGroup + this.groupSize(currentGroup))), this);
    }
    
    public final String groupsAsString() {
        final StringBuilder sb = new StringBuilder();
        for (int size$runtime_release = this.getSize$runtime_release(), i = 0; i < size$runtime_release; ++i) {
            this.groupAsString(sb, i);
            sb.append('\n');
        }
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
        return string;
    }
    
    public final boolean indexInCurrentGroup(final int n) {
        return this.indexInGroup(n, this.currentGroup);
    }
    
    public final boolean indexInGroup(final int n, final int n2) {
        final int parent = this.parent;
        final boolean b = false;
        int currentGroupEnd = 0;
        Label_0087: {
            if (n2 == parent) {
                currentGroupEnd = this.currentGroupEnd;
            }
            else {
                int n3;
                if (n2 > this.startStack.peekOr(0)) {
                    n3 = this.groupSize(n2);
                }
                else {
                    final int index = this.startStack.indexOf(n2);
                    if (index >= 0) {
                        currentGroupEnd = this.getCapacity() - this.groupGapLen - this.endStack.peek(index);
                        break Label_0087;
                    }
                    n3 = this.groupSize(n2);
                }
                currentGroupEnd = n3 + n2;
            }
        }
        boolean b2 = b;
        if (n > n2) {
            b2 = b;
            if (n < currentGroupEnd) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public final boolean indexInParent(final int n) {
        final int parent = this.parent;
        return (n > parent && n < this.currentGroupEnd) || (parent == 0 && n == 0);
    }
    
    public final void insertAux(final Object o) {
        final int insertCount = this.insertCount;
        final int n = 0;
        if (insertCount < 0) {
            ComposerKt.composeRuntimeError("Cannot insert auxiliary data when not inserting".toString());
            throw new KotlinNothingValueException();
        }
        final int parent = this.parent;
        final int groupIndexToAddress = this.groupIndexToAddress(parent);
        if (hasAux(this.groups, groupIndexToAddress) ^ true) {
            this.insertSlots(1, parent);
            final int auxIndex = this.auxIndex(this.groups, groupIndexToAddress);
            final int dataIndexToDataAddress = this.dataIndexToDataAddress(auxIndex);
            final int currentSlot = this.currentSlot;
            if (currentSlot > auxIndex) {
                final int n2 = currentSlot - auxIndex;
                int n3 = n;
                if (n2 < 3) {
                    n3 = 1;
                }
                if (n3 == 0) {
                    throw new IllegalStateException("Moving more than two slot not supported".toString());
                }
                if (n2 > 1) {
                    final Object[] slots = this.slots;
                    slots[dataIndexToDataAddress + 2] = slots[dataIndexToDataAddress + 1];
                }
                final Object[] slots2 = this.slots;
                slots2[dataIndexToDataAddress + 1] = slots2[dataIndexToDataAddress];
            }
            addAux(this.groups, groupIndexToAddress);
            this.slots[dataIndexToDataAddress] = o;
            ++this.currentSlot;
            return;
        }
        ComposerKt.composeRuntimeError("Group already has auxiliary data".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void insertParentGroup(final int n) {
        final int insertCount = this.insertCount;
        final int n2 = 0;
        if (insertCount == 0) {
            if (this.isGroupEnd()) {
                this.beginInsert();
                this.startGroup(n);
                this.endGroup();
                this.endInsert();
            }
            else {
                final int currentGroup = this.currentGroup;
                final int parent = this.parent(this.groups, currentGroup);
                final int currentGroup2 = parent + this.groupSize(parent);
                int i = currentGroup;
                int n3 = n2;
                while (i < currentGroup2) {
                    final int groupIndexToAddress = this.groupIndexToAddress(i);
                    n3 += nodeCount(this.groups, groupIndexToAddress);
                    i += groupSize(this.groups, groupIndexToAddress);
                }
                final int access$dataAnchor = dataAnchor(this.groups, this.groupIndexToAddress(currentGroup));
                this.beginInsert();
                this.insertGroups(1);
                this.endInsert();
                final int groupIndexToAddress2 = this.groupIndexToAddress(currentGroup);
                initGroup(this.groups, groupIndexToAddress2, n, false, false, false, parent, access$dataAnchor);
                updateGroupSize(this.groups, groupIndexToAddress2, currentGroup2 - currentGroup + 1);
                updateNodeCount(this.groups, groupIndexToAddress2, n3);
                this.addToGroupSizeAlongSpine(this.groupIndexToAddress(parent), 1);
                this.fixParentAnchorsFor(parent, currentGroup2, currentGroup);
                this.currentGroup = currentGroup2;
            }
            return;
        }
        ComposerKt.composeRuntimeError("Writer cannot be inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final boolean isGroupEnd() {
        return this.currentGroup == this.currentGroupEnd;
    }
    
    public final boolean isNode() {
        final int currentGroup = this.currentGroup;
        return currentGroup < this.currentGroupEnd && isNode(this.groups, this.groupIndexToAddress(currentGroup));
    }
    
    public final boolean isNode(final int n) {
        return isNode(this.groups, this.groupIndexToAddress(n));
    }
    
    public final void markGroup(final int n) {
        final int groupIndexToAddress = this.groupIndexToAddress(n);
        if (!hasMark(this.groups, groupIndexToAddress)) {
            updateMark(this.groups, groupIndexToAddress, true);
            if (!containsMark(this.groups, groupIndexToAddress)) {
                this.updateContainsMark(this.parent(n));
            }
        }
    }
    
    public final List<Anchor> moveFrom(SlotTable openWriter, int groupsSize) {
        Intrinsics.checkNotNullParameter((Object)openWriter, "table");
        ComposerKt.runtimeCheck(this.insertCount > 0);
        if (groupsSize == 0 && this.currentGroup == 0 && this.table.getGroupsSize() == 0) {
            final int[] groups = this.groups;
            final Object[] slots = this.slots;
            final ArrayList<Anchor> anchors = this.anchors;
            final int[] groups2 = openWriter.getGroups();
            groupsSize = openWriter.getGroupsSize();
            final Object[] slots2 = openWriter.getSlots();
            final int slotsSize = openWriter.getSlotsSize();
            this.groups = groups2;
            this.slots = slots2;
            this.anchors = openWriter.getAnchors$runtime_release();
            this.groupGapStart = groupsSize;
            this.groupGapLen = groups2.length / 5 - groupsSize;
            this.slotsGapStart = slotsSize;
            this.slotsGapLen = slots2.length - slotsSize;
            this.slotsGapOwner = groupsSize;
            openWriter.setTo$runtime_release(groups, 0, slots, 0, anchors);
            return this.anchors;
        }
        openWriter = (SlotTable)openWriter.openWriter();
        try {
            return SlotWriter.Companion.moveGroup((SlotWriter)openWriter, groupsSize, this, true, true);
        }
        finally {
            ((SlotWriter)openWriter).close();
        }
    }
    
    public final void moveGroup(int i) {
        if (this.insertCount != 0) {
            ComposerKt.composeRuntimeError("Cannot move a group while inserting".toString());
            throw new KotlinNothingValueException();
        }
        if (i < 0) {
            ComposerKt.composeRuntimeError("Parameter offset is out of bounds".toString());
            throw new KotlinNothingValueException();
        }
        if (i == 0) {
            return;
        }
        final int currentGroup = this.currentGroup;
        final int parent = this.parent;
        final int currentGroupEnd = this.currentGroupEnd;
        int n = currentGroup;
        while (i > 0) {
            final int n2 = n + groupSize(this.groups, this.groupIndexToAddress(n));
            if (n2 > currentGroupEnd) {
                ComposerKt.composeRuntimeError("Parameter offset is out of bounds".toString());
                throw new KotlinNothingValueException();
            }
            --i;
            n = n2;
        }
        final int access$groupSize = groupSize(this.groups, this.groupIndexToAddress(n));
        i = this.currentSlot;
        final int dataIndex = this.dataIndex(this.groups, this.groupIndexToAddress(n));
        final int[] groups = this.groups;
        final int n3 = n + access$groupSize;
        final int dataIndex2 = this.dataIndex(groups, this.groupIndexToAddress(n3));
        final int n4 = dataIndex2 - dataIndex;
        this.insertSlots(n4, Math.max(this.currentGroup - 1, 0));
        this.insertGroups(access$groupSize);
        final int[] groups2 = this.groups;
        final int n5 = this.groupIndexToAddress(n3) * 5;
        ArraysKt.copyInto(groups2, groups2, this.groupIndexToAddress(currentGroup) * 5, n5, access$groupSize * 5 + n5);
        if (n4 > 0) {
            final Object[] slots = this.slots;
            ArraysKt.copyInto(slots, slots, i, this.dataIndexToDataAddress(dataIndex + n4), this.dataIndexToDataAddress(dataIndex2 + n4));
        }
        final int n6 = dataIndex + n4;
        final int n7 = n6 - i;
        i = this.slotsGapStart;
        final int slotsGapLen = this.slotsGapLen;
        final int length = this.slots.length;
        final int slotsGapOwner = this.slotsGapOwner;
        for (int j = currentGroup; j < currentGroup + access$groupSize; ++j) {
            final int groupIndexToAddress = this.groupIndexToAddress(j);
            final int dataIndex3 = this.dataIndex(groups2, groupIndexToAddress);
            int n8;
            if (slotsGapOwner < groupIndexToAddress) {
                n8 = 0;
            }
            else {
                n8 = i;
            }
            this.updateDataIndex(groups2, groupIndexToAddress, this.dataIndexToDataAnchor(dataIndex3 - n7, n8, slotsGapLen, length));
        }
        this.moveAnchors(n3, currentGroup, access$groupSize);
        if (this.removeGroups(n3, access$groupSize) ^ true) {
            this.fixParentAnchorsFor(parent, this.currentGroupEnd, currentGroup);
            if (n4 > 0) {
                this.removeSlots(n6, n4, n3 - 1);
            }
            return;
        }
        ComposerKt.composeRuntimeError("Unexpectedly removed anchors".toString());
        throw new KotlinNothingValueException();
    }
    
    public final List<Anchor> moveIntoGroupFrom(final int n, SlotTable openWriter, final int n2) {
        Intrinsics.checkNotNullParameter((Object)openWriter, "table");
        final int insertCount = this.insertCount;
        boolean b = true;
        if (insertCount > 0 || this.groupSize(this.currentGroup + n) != 1) {
            b = false;
        }
        ComposerKt.runtimeCheck(b);
        final int currentGroup = this.currentGroup;
        final int currentSlot = this.currentSlot;
        final int currentSlotEnd = this.currentSlotEnd;
        this.advanceBy(n);
        this.startGroup();
        this.beginInsert();
        openWriter = (SlotTable)openWriter.openWriter();
        try {
            final List access$moveGroup = SlotWriter.Companion.moveGroup((SlotWriter)openWriter, n2, this, false, true);
            ((SlotWriter)openWriter).close();
            this.endInsert();
            this.endGroup();
            this.currentGroup = currentGroup;
            this.currentSlot = currentSlot;
            this.currentSlotEnd = currentSlotEnd;
            return access$moveGroup;
        }
        finally {
            ((SlotWriter)openWriter).close();
        }
    }
    
    public final List<Anchor> moveTo(final Anchor anchor, int n, final SlotWriter slotWriter) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        Intrinsics.checkNotNullParameter((Object)slotWriter, "writer");
        final int insertCount = slotWriter.insertCount;
        final boolean b = true;
        ComposerKt.runtimeCheck(insertCount > 0);
        ComposerKt.runtimeCheck(this.insertCount == 0);
        ComposerKt.runtimeCheck(anchor.getValid());
        n += this.anchorIndex(anchor);
        final int currentGroup = this.currentGroup;
        ComposerKt.runtimeCheck(currentGroup <= n && n < this.currentGroupEnd);
        int i = this.parent(n);
        final int groupSize = this.groupSize(n);
        int nodeCount;
        if (this.isNode(n)) {
            nodeCount = 1;
        }
        else {
            nodeCount = this.nodeCount(n);
        }
        final List access$moveGroup = SlotWriter.Companion.moveGroup(this, n, slotWriter, false, false);
        this.updateContainsMark(i);
        if (nodeCount > 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        while (i >= currentGroup) {
            final int groupIndexToAddress = this.groupIndexToAddress(i);
            final int[] groups = this.groups;
            updateGroupSize(groups, groupIndexToAddress, groupSize(groups, groupIndexToAddress) - groupSize);
            int n2;
            if ((n2 = n) != 0) {
                if (isNode(this.groups, groupIndexToAddress)) {
                    n2 = 0;
                }
                else {
                    final int[] groups2 = this.groups;
                    updateNodeCount(groups2, groupIndexToAddress, nodeCount(groups2, groupIndexToAddress) - nodeCount);
                    n2 = n;
                }
            }
            i = this.parent(i);
            n = n2;
        }
        if (n != 0) {
            ComposerKt.runtimeCheck(this.nodeCount >= nodeCount && b);
            this.nodeCount -= nodeCount;
        }
        return access$moveGroup;
    }
    
    public final Object node(int groupIndexToAddress) {
        groupIndexToAddress = this.groupIndexToAddress(groupIndexToAddress);
        Object o;
        if (isNode(this.groups, groupIndexToAddress)) {
            o = this.slots[this.dataIndexToDataAddress(this.nodeIndex(this.groups, groupIndexToAddress))];
        }
        else {
            o = null;
        }
        return o;
    }
    
    public final Object node(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        return this.node(anchor.toIndexFor(this));
    }
    
    public final int nodeCount(final int n) {
        return nodeCount(this.groups, this.groupIndexToAddress(n));
    }
    
    public final int parent(final int n) {
        return this.parent(this.groups, n);
    }
    
    public final int parent(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        int parent;
        if (anchor.getValid()) {
            parent = this.parent(this.groups, this.anchorIndex(anchor));
        }
        else {
            parent = -1;
        }
        return parent;
    }
    
    public final boolean removeGroup() {
        if (this.insertCount == 0) {
            final int currentGroup = this.currentGroup;
            final int currentSlot = this.currentSlot;
            final int skipGroup = this.skipGroup();
            final PrioritySet pendingRecalculateMarks = this.pendingRecalculateMarks;
            if (pendingRecalculateMarks != null) {
                while (pendingRecalculateMarks.isNotEmpty() && pendingRecalculateMarks.peek() >= currentGroup) {
                    pendingRecalculateMarks.takeMax();
                }
            }
            final boolean removeGroups = this.removeGroups(currentGroup, this.currentGroup - currentGroup);
            this.removeSlots(currentSlot, this.currentSlot - currentSlot, currentGroup - 1);
            this.currentGroup = currentGroup;
            this.currentSlot = currentSlot;
            this.nodeCount -= skipGroup;
            return removeGroups;
        }
        ComposerKt.composeRuntimeError("Cannot remove group while inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void reset() {
        if (this.insertCount == 0) {
            this.recalculateMarks();
            this.currentGroup = 0;
            this.currentGroupEnd = this.getCapacity() - this.groupGapLen;
            this.currentSlot = 0;
            this.currentSlotEnd = 0;
            this.nodeCount = 0;
            return;
        }
        ComposerKt.composeRuntimeError("Cannot reset when inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void seek(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        this.advanceBy(anchor.toIndexFor(this) - this.currentGroup);
    }
    
    public final Object set(int dataIndexToDataAddress, Object o) {
        final int slotIndex = this.slotIndex(this.groups, this.groupIndexToAddress(this.currentGroup));
        final int[] groups = this.groups;
        final int currentGroup = this.currentGroup;
        boolean b = true;
        final int dataIndex = this.dataIndex(groups, this.groupIndexToAddress(currentGroup + 1));
        final int n = slotIndex + dataIndexToDataAddress;
        if (n < slotIndex || n >= dataIndex) {
            b = false;
        }
        if (b) {
            dataIndexToDataAddress = this.dataIndexToDataAddress(n);
            final Object[] slots = this.slots;
            final Object o2 = slots[dataIndexToDataAddress];
            slots[dataIndexToDataAddress] = o;
            return o2;
        }
        o = new StringBuilder();
        ((StringBuilder)o).append("Write to an invalid slot index ");
        ((StringBuilder)o).append(dataIndexToDataAddress);
        ((StringBuilder)o).append(" for group ");
        ((StringBuilder)o).append(this.currentGroup);
        ComposerKt.composeRuntimeError(((StringBuilder)o).toString().toString());
        throw new KotlinNothingValueException();
    }
    
    public final void set(final Object o) {
        final int currentSlot = this.currentSlot;
        if (currentSlot <= this.currentSlotEnd) {
            this.slots[this.dataIndexToDataAddress(currentSlot - 1)] = o;
            return;
        }
        ComposerKt.composeRuntimeError("Writing to an invalid slot".toString());
        throw new KotlinNothingValueException();
    }
    
    public final Object skip() {
        if (this.insertCount > 0) {
            this.insertSlots(1, this.parent);
        }
        return this.slots[this.dataIndexToDataAddress(this.currentSlot++)];
    }
    
    public final int skipGroup() {
        final int groupIndexToAddress = this.groupIndexToAddress(this.currentGroup);
        final int currentGroup = this.currentGroup + groupSize(this.groups, groupIndexToAddress);
        this.currentGroup = currentGroup;
        this.currentSlot = this.dataIndex(this.groups, this.groupIndexToAddress(currentGroup));
        int access$nodeCount;
        if (isNode(this.groups, groupIndexToAddress)) {
            access$nodeCount = 1;
        }
        else {
            access$nodeCount = nodeCount(this.groups, groupIndexToAddress);
        }
        return access$nodeCount;
    }
    
    public final void skipToGroupEnd() {
        final int currentGroupEnd = this.currentGroupEnd;
        this.currentGroup = currentGroupEnd;
        this.currentSlot = this.dataIndex(this.groups, this.groupIndexToAddress(currentGroupEnd));
    }
    
    public final Object slot(int n, int n2) {
        final int slotIndex = this.slotIndex(this.groups, this.groupIndexToAddress(n));
        final int[] groups = this.groups;
        final int n3 = 1;
        n = this.dataIndex(groups, this.groupIndexToAddress(n + 1));
        n2 += slotIndex;
        if (slotIndex <= n2 && n2 < n) {
            n = n3;
        }
        else {
            n = 0;
        }
        if (n == 0) {
            return Composer.Companion.getEmpty();
        }
        n = this.dataIndexToDataAddress(n2);
        return this.slots[n];
    }
    
    public final Object slot(final Anchor anchor, final int n) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        return this.slot(this.anchorIndex(anchor), n);
    }
    
    public final void startData(final int n, final Object o) {
        this.startGroup(n, Composer.Companion.getEmpty(), false, o);
    }
    
    public final void startData(final int n, final Object o, final Object o2) {
        this.startGroup(n, o, false, o2);
    }
    
    public final void startGroup() {
        if (this.insertCount == 0) {
            this.startGroup(0, Composer.Companion.getEmpty(), false, Composer.Companion.getEmpty());
            return;
        }
        ComposerKt.composeRuntimeError("Key must be supplied when inserting".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void startGroup(final int n) {
        this.startGroup(n, Composer.Companion.getEmpty(), false, Composer.Companion.getEmpty());
    }
    
    public final void startGroup(final int n, final Object o) {
        this.startGroup(n, o, false, Composer.Companion.getEmpty());
    }
    
    public final void startNode(final int n, final Object o) {
        this.startGroup(n, o, true, Composer.Companion.getEmpty());
    }
    
    public final void startNode(final int n, final Object o, final Object o2) {
        this.startGroup(n, o, true, o2);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SlotWriter(current = ");
        sb.append(this.currentGroup);
        sb.append(" end=");
        sb.append(this.currentGroupEnd);
        sb.append(" size = ");
        sb.append(this.getSize$runtime_release());
        sb.append(" gap=");
        sb.append(this.groupGapStart);
        sb.append('-');
        sb.append(this.groupGapStart + this.groupGapLen);
        sb.append(')');
        return sb.toString();
    }
    
    public final Object update(final Object o) {
        final Object skip = this.skip();
        this.set(o);
        return skip;
    }
    
    public final void updateAux(final Object o) {
        final int groupIndexToAddress = this.groupIndexToAddress(this.currentGroup);
        if (hasAux(this.groups, groupIndexToAddress)) {
            this.slots[this.dataIndexToDataAddress(this.auxIndex(this.groups, groupIndexToAddress))] = o;
            return;
        }
        ComposerKt.composeRuntimeError("Updating the data of a group that was not created with a data slot".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void updateNode(final Anchor anchor, final Object o) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        this.updateNodeOfGroup(anchor.toIndexFor(this), o);
    }
    
    public final void updateNode(final Object o) {
        this.updateNodeOfGroup(this.currentGroup, o);
    }
    
    public final void updateParentNode(final Object o) {
        this.updateNodeOfGroup(this.parent, o);
    }
    
    public final void verifyDataAnchors$runtime_release() {
        final int slotsGapOwner = this.slotsGapOwner;
        final int length = this.slots.length;
        final int slotsGapLen = this.slotsGapLen;
        final int size$runtime_release = this.getSize$runtime_release();
        int i = 0;
        int j = 0;
        int n = 0;
        while (i < size$runtime_release) {
            final int groupIndexToAddress = this.groupIndexToAddress(i);
            final int access$dataAnchor = dataAnchor(this.groups, groupIndexToAddress);
            final int dataIndex = this.dataIndex(this.groups, groupIndexToAddress);
            if (dataIndex < j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Data index out of order at ");
                sb.append(i);
                sb.append(", previous = ");
                sb.append(j);
                sb.append(", current = ");
                sb.append(dataIndex);
                throw new IllegalStateException(sb.toString().toString());
            }
            if (dataIndex > length - slotsGapLen) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Data index, ");
                sb2.append(dataIndex);
                sb2.append(", out of bound at ");
                sb2.append(i);
                throw new IllegalStateException(sb2.toString().toString());
            }
            int n2 = n;
            if (access$dataAnchor < 0 && (n2 = n) == 0) {
                if (slotsGapOwner != i) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Expected the slot gap owner to be ");
                    sb3.append(slotsGapOwner);
                    sb3.append(" found gap at ");
                    sb3.append(i);
                    throw new IllegalStateException(sb3.toString().toString());
                }
                n2 = 1;
            }
            ++i;
            j = dataIndex;
            n = n2;
        }
    }
    
    public final void verifyParentAnchors$runtime_release() {
        final int groupGapStart = this.groupGapStart;
        final int groupGapLen = this.groupGapLen;
        final int capacity = this.getCapacity();
        int i = 0;
        while (true) {
            boolean b = true;
            if (i >= groupGapStart) {
                for (int j = groupGapLen + groupGapStart; j < capacity; ++j) {
                    final int access$parentAnchor = parentAnchor(this.groups, j);
                    if (this.parentAnchorToIndex(access$parentAnchor) < groupGapStart) {
                        if (access$parentAnchor <= -2) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Expected a start relative anchor at ");
                            sb.append(j);
                            throw new IllegalStateException(sb.toString().toString());
                        }
                    }
                    else if (access$parentAnchor > -2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Expected an end relative anchor at ");
                        sb2.append(j);
                        throw new IllegalStateException(sb2.toString().toString());
                    }
                }
                return;
            }
            if (parentAnchor(this.groups, i) <= -2) {
                b = false;
            }
            if (!b) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Expected a start relative anchor at ");
                sb3.append(i);
                throw new IllegalStateException(sb3.toString().toString());
            }
            ++i;
        }
    }
    
    @Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J6\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u0002¨\u0006\u000e" }, d2 = { "Landroidx/compose/runtime/SlotWriter$Companion;", "", "()V", "moveGroup", "", "Landroidx/compose/runtime/Anchor;", "fromWriter", "Landroidx/compose/runtime/SlotWriter;", "fromIndex", "", "toWriter", "updateFromCursor", "", "updateToCursor", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final List<Anchor> moveGroup(final SlotWriter slotWriter, int access$nodeCount, final SlotWriter slotWriter2, final boolean b, final boolean b2) {
            final int groupSize = slotWriter.groupSize(access$nodeCount);
            final int n = access$nodeCount + groupSize;
            final int access$dataIndex = slotWriter.dataIndex(access$nodeCount);
            final int access$dataIndex2 = slotWriter.dataIndex(n);
            final int n2 = access$dataIndex2 - access$dataIndex;
            final boolean access$containsAnyGroupMarks = slotWriter.containsAnyGroupMarks(access$nodeCount);
            slotWriter2.insertGroups(groupSize);
            slotWriter2.insertSlots(n2, slotWriter2.getCurrentGroup());
            if (SlotWriter.access$getGroupGapStart$p(slotWriter) < n) {
                slotWriter.moveGroupGapTo(n);
            }
            if (SlotWriter.access$getSlotsGapStart$p(slotWriter) < access$dataIndex2) {
                slotWriter.moveSlotGapTo(access$dataIndex2, n);
            }
            final int[] access$getGroups$p = SlotWriter.access$getGroups$p(slotWriter2);
            final int currentGroup = slotWriter2.getCurrentGroup();
            ArraysKt.copyInto(SlotWriter.access$getGroups$p(slotWriter), access$getGroups$p, currentGroup * 5, access$nodeCount * 5, n * 5);
            final Object[] access$getSlots$p = SlotWriter.access$getSlots$p(slotWriter2);
            final int access$getCurrentSlot$p = SlotWriter.access$getCurrentSlot$p(slotWriter2);
            ArraysKt.copyInto(SlotWriter.access$getSlots$p(slotWriter), access$getSlots$p, access$getCurrentSlot$p, access$dataIndex, access$dataIndex2);
            final int parent = slotWriter2.getParent();
            updateParentAnchor(access$getGroups$p, currentGroup, parent);
            final int n3 = currentGroup - access$nodeCount;
            final int n4 = currentGroup + groupSize;
            final int n5 = access$getCurrentSlot$p - slotWriter2.dataIndex(access$getGroups$p, currentGroup);
            int access$getSlotsGapOwner$p = SlotWriter.access$getSlotsGapOwner$p(slotWriter2);
            final int access$getSlotsGapLen$p = SlotWriter.access$getSlotsGapLen$p(slotWriter2);
            final int length = access$getSlots$p.length;
            int n6 = currentGroup;
            int n7;
            while (true) {
                n7 = 0;
                if (n6 >= n4) {
                    break;
                }
                if (n6 != currentGroup) {
                    updateParentAnchor(access$getGroups$p, n6, parentAnchor(access$getGroups$p, n6) + n3);
                }
                final int access$dataIndex3 = slotWriter2.dataIndex(access$getGroups$p, n6);
                int access$getSlotsGapStart$p;
                if (access$getSlotsGapOwner$p < n6) {
                    access$getSlotsGapStart$p = 0;
                }
                else {
                    access$getSlotsGapStart$p = SlotWriter.access$getSlotsGapStart$p(slotWriter2);
                }
                updateDataAnchor(access$getGroups$p, n6, slotWriter2.dataIndexToDataAnchor(access$dataIndex3 + n5, access$getSlotsGapStart$p, access$getSlotsGapLen$p, length));
                int n8 = access$getSlotsGapOwner$p;
                if (n6 == access$getSlotsGapOwner$p) {
                    n8 = access$getSlotsGapOwner$p + 1;
                }
                ++n6;
                access$getSlotsGapOwner$p = n8;
            }
            SlotWriter.access$setSlotsGapOwner$p(slotWriter2, access$getSlotsGapOwner$p);
            final int access$location = locationOf(SlotWriter.access$getAnchors$p(slotWriter), access$nodeCount, slotWriter.getSize$runtime_release());
            final int access$location2 = locationOf(SlotWriter.access$getAnchors$p(slotWriter), n, slotWriter.getSize$runtime_release());
            List emptyList;
            if (access$location < access$location2) {
                final ArrayList access$getAnchors$p = SlotWriter.access$getAnchors$p(slotWriter);
                final ArrayList list = new ArrayList<Anchor>(access$location2 - access$location);
                for (int i = access$location; i < access$location2; ++i) {
                    final Object value = access$getAnchors$p.get(i);
                    Intrinsics.checkNotNullExpressionValue(value, "sourceAnchors[anchorIndex]");
                    final Anchor e = (Anchor)value;
                    e.setLocation$runtime_release(e.getLocation$runtime_release() + n3);
                    list.add(e);
                }
                SlotWriter.access$getAnchors$p(slotWriter2).addAll(locationOf(SlotWriter.access$getAnchors$p(slotWriter2), slotWriter2.getCurrentGroup(), slotWriter2.getSize$runtime_release()), list);
                access$getAnchors$p.subList(access$location, access$location2).clear();
                emptyList = list;
            }
            else {
                emptyList = CollectionsKt.emptyList();
            }
            final int parent2 = slotWriter.parent(access$nodeCount);
            final int n9 = 1;
            int n11;
            if (b) {
                int n10 = n7;
                if (parent2 >= 0) {
                    n10 = 1;
                }
                if (n10 != 0) {
                    slotWriter.startGroup();
                    slotWriter.advanceBy(parent2 - slotWriter.getCurrentGroup());
                    slotWriter.startGroup();
                }
                slotWriter.advanceBy(access$nodeCount - slotWriter.getCurrentGroup());
                n11 = (slotWriter.removeGroup() ? 1 : 0);
                if (n10 != 0) {
                    slotWriter.skipToGroupEnd();
                    slotWriter.endGroup();
                    slotWriter.skipToGroupEnd();
                    slotWriter.endGroup();
                    n11 = n11;
                }
            }
            else {
                n11 = (slotWriter.removeGroups(access$nodeCount, groupSize) ? 1 : 0);
                slotWriter.removeSlots(access$dataIndex, n2, access$nodeCount - 1);
            }
            if ((n11 ^ 0x1) != 0x0) {
                final int access$getNodeCount$p = SlotWriter.access$getNodeCount$p(slotWriter2);
                if (isNode(access$getGroups$p, currentGroup)) {
                    access$nodeCount = n9;
                }
                else {
                    access$nodeCount = nodeCount(access$getGroups$p, currentGroup);
                }
                SlotWriter.access$setNodeCount$p(slotWriter2, access$getNodeCount$p + access$nodeCount);
                if (b2) {
                    SlotWriter.access$setCurrentGroup$p(slotWriter2, n4);
                    SlotWriter.access$setCurrentSlot$p(slotWriter2, access$getCurrentSlot$p + n2);
                }
                if (access$containsAnyGroupMarks) {
                    slotWriter2.updateContainsMark(parent);
                }
                return emptyList;
            }
            ComposerKt.composeRuntimeError("Unexpectedly removed anchors".toString());
            throw new KotlinNothingValueException();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import androidx.compose.runtime.snapshots.StateObject$_CC;
import androidx.compose.runtime.snapshots.StateRecord;
import androidx.compose.runtime.snapshots.SnapshotKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.collection.IdentityArrayMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Pair;
import androidx.compose.runtime.collection.MutableVector;
import androidx.compose.runtime.snapshots.Snapshot;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;
import androidx.compose.runtime.snapshots.StateObject;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001+B#\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!J:\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000\u00162\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u00162\u0006\u0010 \u001a\u00020!2\u0006\u0010$\u001a\u00020%2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005H\u0002J\b\u0010&\u001a\u00020'H\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010\u001d\u001a\u00020\u0018H\u0016J\b\u0010*\u001a\u00020'H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00028\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u0004\u0018\u00018\u00008G¢\u0006\f\u0012\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u000bR\u001c\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u0016X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u001c\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00028\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u000b¨\u0006," }, d2 = { "Landroidx/compose/runtime/DerivedSnapshotState;", "T", "Landroidx/compose/runtime/snapshots/StateObject;", "Landroidx/compose/runtime/DerivedState;", "calculation", "Lkotlin/Function0;", "policy", "Landroidx/compose/runtime/SnapshotMutationPolicy;", "(Lkotlin/jvm/functions/Function0;Landroidx/compose/runtime/SnapshotMutationPolicy;)V", "currentValue", "getCurrentValue", "()Ljava/lang/Object;", "debuggerDisplayValue", "getDebuggerDisplayValue$annotations", "()V", "getDebuggerDisplayValue", "dependencies", "", "", "getDependencies", "()[Ljava/lang/Object;", "first", "Landroidx/compose/runtime/DerivedSnapshotState$ResultRecord;", "firstStateRecord", "Landroidx/compose/runtime/snapshots/StateRecord;", "getFirstStateRecord", "()Landroidx/compose/runtime/snapshots/StateRecord;", "getPolicy", "()Landroidx/compose/runtime/SnapshotMutationPolicy;", "value", "getValue", "current", "snapshot", "Landroidx/compose/runtime/snapshots/Snapshot;", "currentRecord", "readable", "forceDependencyReads", "", "displayValue", "", "prependStateRecord", "", "toString", "ResultRecord", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class DerivedSnapshotState<T> implements StateObject, DerivedState<T>
{
    private final Function0<T> calculation;
    private ResultRecord<T> first;
    private final SnapshotMutationPolicy<T> policy;
    
    public DerivedSnapshotState(final Function0<? extends T> calculation, final SnapshotMutationPolicy<T> policy) {
        Intrinsics.checkNotNullParameter((Object)calculation, "calculation");
        this.calculation = (Function0<T>)calculation;
        this.policy = policy;
        this.first = new ResultRecord<T>();
    }
    
    private final ResultRecord<T> currentRecord(ResultRecord<T> resultRecord, Snapshot content, final boolean b, Function0<? extends T> mutableVector) {
        Object policy = this;
        final boolean valid = resultRecord.isValid((DerivedState)policy, content);
        final int n = 1;
        int size = 0;
        final int n2 = 0;
        final int n3 = 0;
        if (valid) {
            if (b) {
                if ((mutableVector = SnapshotStateKt__DerivedStateKt.access$getDerivedStateObservers$p().get()) == null) {
                    mutableVector = new MutableVector<Object>(new Pair[0], 0);
                }
                final int size2 = mutableVector.getSize();
                if (size2 > 0) {
                    final Object[] content2 = mutableVector.getContent();
                    int n4 = 0;
                    do {
                        ((Function1)((Pair)content2[n4]).component1()).invoke(policy);
                    } while (++n4 < size2);
                }
                try {
                    final IdentityArrayMap dependencies = resultRecord.getDependencies();
                    final Integer n5 = SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().get();
                    int intValue;
                    if (n5 != null) {
                        intValue = n5;
                    }
                    else {
                        intValue = 0;
                    }
                    if (dependencies != null) {
                        for (int size$runtime_release = dependencies.getSize$runtime_release(), i = 0; i < size$runtime_release; ++i) {
                            final Object o = dependencies.getKeys$runtime_release()[i];
                            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
                            final int intValue2 = ((Number)dependencies.getValues$runtime_release()[i]).intValue();
                            final StateObject stateObject = (StateObject)o;
                            SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().set(intValue2 + intValue);
                            final Function1<Object, Unit> readObserver$runtime_release = content.getReadObserver$runtime_release();
                            if (readObserver$runtime_release != null) {
                                readObserver$runtime_release.invoke((Object)stateObject);
                            }
                        }
                    }
                    SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().set(intValue);
                    final Unit instance = Unit.INSTANCE;
                    size = mutableVector.getSize();
                    if (size > 0) {
                        final Object[] content3 = mutableVector.getContent();
                        int n6 = n3;
                        do {
                            ((Function1)((Pair)content3[n6]).component2()).invoke(policy);
                        } while (++n6 < size);
                    }
                }
                finally {
                    final int size3 = mutableVector.getSize();
                    if (size3 > 0) {
                        final Object[] content4 = mutableVector.getContent();
                        int n7 = size;
                        do {
                            ((Function1)((Pair)content4[n7]).component2()).invoke(policy);
                        } while (++n7 < size3);
                    }
                }
            }
            return resultRecord;
        }
        final Integer n8 = SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().get();
        int intValue3;
        if (n8 != null) {
            intValue3 = n8;
        }
        else {
            intValue3 = 0;
        }
        final IdentityArrayMap identityArrayMap = new IdentityArrayMap(0, 1, null);
        if ((content = (Snapshot)SnapshotStateKt__DerivedStateKt.access$getDerivedStateObservers$p().get()) == null) {
            content = (Snapshot)new MutableVector(new Pair[0], 0);
        }
        final int size4 = ((MutableVector)content).getSize();
        if (size4 > 0) {
            final Object[] content5 = ((MutableVector<Object>)content).getContent();
            int n9 = 0;
            do {
                ((Function1)((Pair)content5[n9]).component1()).invoke(policy);
            } while (++n9 < size4);
        }
        try {
            SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().set(intValue3 + 1);
            final Object observe = Snapshot.Companion.observe((Function1<Object, Unit>)new DerivedSnapshotState$currentRecord$result$1$result.DerivedSnapshotState$currentRecord$result$1$result$1(this, (IdentityArrayMap)identityArrayMap, intValue3), (Function1<Object, Unit>)null, (kotlin.jvm.functions.Function0<?>)mutableVector);
            SnapshotStateKt__DerivedStateKt.access$getCalculationBlockNestedLevel$p().set(intValue3);
            final int size5 = ((MutableVector)content).getSize();
            if (size5 > 0) {
                content = (Snapshot)((MutableVector<Object>)content).getContent();
                int n10 = 0;
                do {
                    ((Function1)((Pair)content[n10]).component2()).invoke(policy);
                } while (++n10 < size5);
            }
            synchronized (SnapshotKt.getLock()) {
                final Snapshot current = Snapshot.Companion.getCurrent();
                Label_0792: {
                    if (resultRecord.getResult() != ResultRecord.Companion.getUnset()) {
                        policy = this.getPolicy();
                        int n11;
                        if (policy != null && ((SnapshotMutationPolicy<Object>)policy).equivalent(observe, resultRecord.getResult())) {
                            n11 = n;
                        }
                        else {
                            n11 = 0;
                        }
                        if (n11 != 0) {
                            resultRecord.setDependencies(identityArrayMap);
                            resultRecord.setResultHash(resultRecord.readableHash(this, current));
                            break Label_0792;
                        }
                    }
                    resultRecord = SnapshotKt.newWritableRecord(this.first, this, current);
                    resultRecord.setDependencies(identityArrayMap);
                    resultRecord.setResultHash(resultRecord.readableHash(this, current));
                    resultRecord.setResult(observe);
                }
                monitorexit(SnapshotKt.getLock());
                if (intValue3 == 0) {
                    Snapshot.Companion.notifyObjectsInitialized();
                }
                return resultRecord;
            }
        }
        finally {
            final int size6 = ((MutableVector)content).getSize();
            if (size6 > 0) {
                final Object[] content6 = ((MutableVector<Object>)content).getContent();
                int n12 = n2;
                do {
                    ((Function1)((Pair)content6[n12]).component2()).invoke(policy);
                } while (++n12 < size6);
            }
        }
    }
    
    private final String displayValue() {
        final ResultRecord resultRecord = SnapshotKt.current(this.first);
        if (resultRecord.isValid(this, Snapshot.Companion.getCurrent())) {
            return String.valueOf(resultRecord.getResult());
        }
        return "<Not calculated>";
    }
    
    public final StateRecord current(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        return this.currentRecord(SnapshotKt.current(this.first, snapshot), snapshot, false, (Function0<? extends T>)this.calculation);
    }
    
    @Override
    public T getCurrentValue() {
        return (T)this.currentRecord(SnapshotKt.current(this.first), Snapshot.Companion.getCurrent(), false, (Function0<? extends T>)this.calculation).getResult();
    }
    
    public final T getDebuggerDisplayValue() {
        final ResultRecord resultRecord = SnapshotKt.current(this.first);
        Object result;
        if (resultRecord.isValid(this, Snapshot.Companion.getCurrent())) {
            result = resultRecord.getResult();
        }
        else {
            result = null;
        }
        return (T)result;
    }
    
    @Override
    public Object[] getDependencies() {
        final IdentityArrayMap<StateObject, Integer> dependencies = this.currentRecord(SnapshotKt.current(this.first), Snapshot.Companion.getCurrent(), false, (Function0<? extends T>)this.calculation).getDependencies();
        Object[] keys$runtime_release;
        if (dependencies == null || (keys$runtime_release = dependencies.getKeys$runtime_release()) == null) {
            keys$runtime_release = new Object[0];
        }
        return keys$runtime_release;
    }
    
    @Override
    public StateRecord getFirstStateRecord() {
        return this.first;
    }
    
    @Override
    public SnapshotMutationPolicy<T> getPolicy() {
        return this.policy;
    }
    
    @Override
    public T getValue() {
        final Function1<Object, Unit> readObserver$runtime_release = Snapshot.Companion.getCurrent().getReadObserver$runtime_release();
        if (readObserver$runtime_release != null) {
            readObserver$runtime_release.invoke((Object)this);
        }
        return (T)this.currentRecord(SnapshotKt.current(this.first), Snapshot.Companion.getCurrent(), true, (Function0<? extends T>)this.calculation).getResult();
    }
    
    @Override
    public void prependStateRecord(final StateRecord stateRecord) {
        Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
        this.first = (ResultRecord)stateRecord;
    }
    
    @Override
    public String toString() {
        final ResultRecord resultRecord = SnapshotKt.current(this.first);
        final StringBuilder sb = new StringBuilder();
        sb.append("DerivedState(value=");
        sb.append(this.displayValue());
        sb.append(")@");
        sb.append(this.hashCode());
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u0000 \"*\u0004\b\u0001\u0010\u00012\u00020\u0002:\u0001\"B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0016J\b\u0010\u001a\u001a\u00020\u0002H\u0016J\u001a\u0010\u001b\u001a\u00020\u001c2\n\u0010\u001d\u001a\u0006\u0012\u0002\b\u00030\u001e2\u0006\u0010\u001f\u001a\u00020 J\u001a\u0010!\u001a\u00020\u00072\n\u0010\u001d\u001a\u0006\u0012\u0002\b\u00030\u001e2\u0006\u0010\u001f\u001a\u00020 R(\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006#" }, d2 = { "Landroidx/compose/runtime/DerivedSnapshotState$ResultRecord;", "T", "Landroidx/compose/runtime/snapshots/StateRecord;", "()V", "dependencies", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Landroidx/compose/runtime/snapshots/StateObject;", "", "getDependencies", "()Landroidx/compose/runtime/collection/IdentityArrayMap;", "setDependencies", "(Landroidx/compose/runtime/collection/IdentityArrayMap;)V", "result", "", "getResult", "()Ljava/lang/Object;", "setResult", "(Ljava/lang/Object;)V", "resultHash", "getResultHash", "()I", "setResultHash", "(I)V", "assign", "", "value", "create", "isValid", "", "derivedState", "Landroidx/compose/runtime/DerivedState;", "snapshot", "Landroidx/compose/runtime/snapshots/Snapshot;", "readableHash", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class ResultRecord<T> extends StateRecord
    {
        public static final int $stable;
        public static final Companion Companion;
        private static final Object Unset;
        private IdentityArrayMap<StateObject, Integer> dependencies;
        private Object result;
        private int resultHash;
        
        static {
            Companion = new Companion(null);
            $stable = 8;
            Unset = new Object();
        }
        
        public ResultRecord() {
            this.result = ResultRecord.Unset;
        }
        
        public static final /* synthetic */ Object access$getUnset$cp() {
            return ResultRecord.Unset;
        }
        
        @Override
        public void assign(final StateRecord stateRecord) {
            Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
            final ResultRecord resultRecord = (ResultRecord)stateRecord;
            this.dependencies = resultRecord.dependencies;
            this.result = resultRecord.result;
            this.resultHash = resultRecord.resultHash;
        }
        
        @Override
        public StateRecord create() {
            return new ResultRecord<Object>();
        }
        
        public final IdentityArrayMap<StateObject, Integer> getDependencies() {
            return this.dependencies;
        }
        
        public final Object getResult() {
            return this.result;
        }
        
        public final int getResultHash() {
            return this.resultHash;
        }
        
        public final boolean isValid(final DerivedState<?> derivedState, final Snapshot snapshot) {
            Intrinsics.checkNotNullParameter((Object)derivedState, "derivedState");
            Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
            return this.result != ResultRecord.Unset && this.resultHash == this.readableHash(derivedState, snapshot);
        }
        
        public final int readableHash(final DerivedState<?> derivedState, final Snapshot snapshot) {
            Intrinsics.checkNotNullParameter((Object)derivedState, "derivedState");
            Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
            Object o = SnapshotKt.getLock();
            synchronized (o) {
                final IdentityArrayMap<StateObject, Integer> dependencies = this.dependencies;
                monitorexit(o);
                int size;
                int n = size = 7;
                if (dependencies != null) {
                    final MutableVector mutableVector = SnapshotStateKt__DerivedStateKt.access$getDerivedStateObservers$p().get();
                    int size2 = 0;
                    final int n2 = 0;
                    if ((o = mutableVector) == null) {
                        o = new MutableVector(new Pair[0], 0);
                    }
                    final int size3 = ((MutableVector)o).getSize();
                    if (size3 > 0) {
                        final Object[] content = ((MutableVector)o).getContent();
                        int n3 = 0;
                        do {
                            ((Function1)((Pair)content[n3]).component1()).invoke((Object)derivedState);
                        } while (++n3 < size3);
                    }
                    try {
                        for (int size$runtime_release = dependencies.getSize$runtime_release(), i = 0; i < size$runtime_release; ++i) {
                            final Object o2 = dependencies.getKeys$runtime_release()[i];
                            Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
                            final int intValue = ((Number)dependencies.getValues$runtime_release()[i]).intValue();
                            final StateObject stateObject = (StateObject)o2;
                            if (intValue == 1) {
                                StateRecord stateRecord;
                                if (stateObject instanceof DerivedSnapshotState) {
                                    stateRecord = ((DerivedSnapshotState)stateObject).current(snapshot);
                                }
                                else {
                                    stateRecord = SnapshotKt.current(stateObject.getFirstStateRecord(), snapshot);
                                }
                                n = (n * 31 + ActualJvm_jvmKt.identityHashCode(stateRecord)) * 31 + stateRecord.getSnapshotId$runtime_release();
                            }
                        }
                        final Unit instance = Unit.INSTANCE;
                        size2 = ((MutableVector)o).getSize();
                        if (size2 > 0) {
                            final Object[] content2 = ((MutableVector)o).getContent();
                            int n4 = n2;
                            do {
                                ((Function1)((Pair)content2[n4]).component2()).invoke((Object)derivedState);
                            } while (++n4 < size2);
                        }
                    }
                    finally {
                        size = ((MutableVector)o).getSize();
                        if (size > 0) {
                            o = ((MutableVector)o).getContent();
                            int n5 = size2;
                            do {
                                ((Function1)((Pair)o[n5]).component2()).invoke((Object)derivedState);
                            } while (++n5 < size);
                        }
                    }
                }
                return size;
            }
        }
        
        public final void setDependencies(final IdentityArrayMap<StateObject, Integer> dependencies) {
            this.dependencies = dependencies;
        }
        
        public final void setResult(final Object result) {
            this.result = result;
        }
        
        public final void setResultHash(final int resultHash) {
            this.resultHash = resultHash;
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0006" }, d2 = { "Landroidx/compose/runtime/DerivedSnapshotState$ResultRecord$Companion;", "", "()V", "Unset", "getUnset", "()Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final Object getUnset() {
                return ResultRecord.access$getUnset$cp();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0006\u00c0\u0006\u0001" }, d2 = { "Landroidx/compose/runtime/ComposeNodeLifecycleCallback;", "", "onDeactivate", "", "onRelease", "onReuse", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface ComposeNodeLifecycleCallback
{
    void onDeactivate();
    
    void onRelease();
    
    void onReuse();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007" }, d2 = { "Landroidx/compose/runtime/MovableContentState;", "", "slotTable", "Landroidx/compose/runtime/SlotTable;", "(Landroidx/compose/runtime/SlotTable;)V", "getSlotTable$runtime_release", "()Landroidx/compose/runtime/SlotTable;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class MovableContentState
{
    public static final int $stable = 8;
    private final SlotTable slotTable;
    
    public MovableContentState(final SlotTable slotTable) {
        Intrinsics.checkNotNullParameter((Object)slotTable, "slotTable");
        this.slotTable = slotTable;
    }
    
    public final SlotTable getSlotTable$runtime_release() {
        return this.slotTable;
    }
}

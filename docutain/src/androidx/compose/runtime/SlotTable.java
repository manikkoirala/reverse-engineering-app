// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.functions.Function1;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.KotlinNothingValueException;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$BooleanRef;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import androidx.compose.runtime.tooling.CompositionGroup;
import androidx.compose.runtime.tooling.CompositionData;

@Metadata(d1 = { "\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010(\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B\u0005¢\u0006\u0002\u0010\u0004J\u000e\u0010+\u001a\u00020\u00072\u0006\u0010,\u001a\u00020\u0015J\u000e\u0010-\u001a\u00020\u00152\u0006\u0010+\u001a\u00020\u0007J\u0006\u0010.\u001a\u00020/J\u0015\u00100\u001a\u0002012\u0006\u00102\u001a\u000203H\u0000¢\u0006\u0002\b4JW\u00100\u001a\u0002012\u0006\u0010)\u001a\u0002052\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00152\u000e\u0010\u001f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0\u001d2\u0006\u0010#\u001a\u00020\u00152\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u0000¢\u0006\u0004\b4\u00106J\u0006\u00107\u001a\u00020\u001aJ\u000e\u00108\u001a\b\u0012\u0004\u0012\u00020\u001509H\u0002J\u0012\u0010:\u001a\u0004\u0018\u00010\u00032\u0006\u0010;\u001a\u00020\u001eH\u0016J\u0012\u0010<\u001a\u0004\u0018\u00010=2\u0006\u0010>\u001a\u00020\u0015H\u0002J\u0016\u0010?\u001a\u00020\u001a2\u0006\u0010@\u001a\u00020\u00152\u0006\u0010+\u001a\u00020\u0007J\u000e\u0010A\u001a\b\u0012\u0004\u0012\u00020\u001509H\u0002J\u0010\u0010B\u001a\u00020\u001a2\u0006\u0010>\u001a\u00020\u0015H\u0002J\u001d\u0010C\u001a\n\u0012\u0004\u0012\u00020=\u0018\u0001092\u0006\u0010D\u001a\u00020\u0015H\u0000¢\u0006\u0002\bEJ\u000f\u0010F\u001a\b\u0012\u0004\u0012\u00020\u00030GH\u0096\u0002J\u000e\u0010H\u001a\b\u0012\u0004\u0012\u00020\u001509H\u0002J\u000e\u0010I\u001a\b\u0012\u0004\u0012\u00020\u001509H\u0002J\u0006\u0010J\u001a\u000203J\u0006\u0010K\u001a\u000205J\u000e\u0010L\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020\u0007J\u000e\u0010M\u001a\b\u0012\u0004\u0012\u00020\u001509H\u0002J:\u0010N\u001a\u0002HO\"\u0004\b\u0000\u0010O2!\u0010P\u001a\u001d\u0012\u0013\u0012\u001103¢\u0006\f\bR\u0012\b\bS\u0012\u0004\b\b(2\u0012\u0004\u0012\u0002HO0QH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010TJO\u0010U\u001a\u0002012\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00152\u000e\u0010\u001f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0\u001d2\u0006\u0010#\u001a\u00020\u00152\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u0000¢\u0006\u0004\bV\u0010WJ\u001d\u0010X\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e092\u0006\u0010>\u001a\u00020\u0015H\u0000¢\u0006\u0002\bYJ\u0006\u0010Z\u001a\u000201J:\u0010[\u001a\u0002HO\"\u0004\b\u0000\u0010O2!\u0010P\u001a\u001d\u0012\u0013\u0012\u001105¢\u0006\f\bR\u0012\b\bS\u0012\u0004\b\b()\u0012\u0004\u0012\u0002HO0QH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010TJ \u0010\\\u001a\u00020\u0015*\u00060]j\u0002`^2\u0006\u0010,\u001a\u00020\u00152\u0006\u0010_\u001a\u00020\u0015H\u0002R*\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u0011@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0015@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\u001a8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R0\u0010\u001f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0\u001d2\u000e\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0\u001d@BX\u0086\u000e¢\u0006\n\n\u0002\u0010\"\u001a\u0004\b \u0010!R\u001e\u0010#\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0015@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u001a\u0010%\u001a\u00020\u0015X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0018\"\u0004\b'\u0010(R\u001e\u0010)\u001a\u00020\u001a2\u0006\u0010\u0010\u001a\u00020\u001a@BX\u0080\u000e¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001b\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006`" }, d2 = { "Landroidx/compose/runtime/SlotTable;", "Landroidx/compose/runtime/tooling/CompositionData;", "", "Landroidx/compose/runtime/tooling/CompositionGroup;", "()V", "anchors", "Ljava/util/ArrayList;", "Landroidx/compose/runtime/Anchor;", "Lkotlin/collections/ArrayList;", "getAnchors$runtime_release", "()Ljava/util/ArrayList;", "setAnchors$runtime_release", "(Ljava/util/ArrayList;)V", "compositionGroups", "getCompositionGroups", "()Ljava/lang/Iterable;", "<set-?>", "", "groups", "getGroups", "()[I", "", "groupsSize", "getGroupsSize", "()I", "isEmpty", "", "()Z", "readers", "", "", "slots", "getSlots", "()[Ljava/lang/Object;", "[Ljava/lang/Object;", "slotsSize", "getSlotsSize", "version", "getVersion$runtime_release", "setVersion$runtime_release", "(I)V", "writer", "getWriter$runtime_release", "anchor", "index", "anchorIndex", "asString", "", "close", "", "reader", "Landroidx/compose/runtime/SlotReader;", "close$runtime_release", "Landroidx/compose/runtime/SlotWriter;", "(Landroidx/compose/runtime/SlotWriter;[II[Ljava/lang/Object;ILjava/util/ArrayList;)V", "containsMark", "dataIndexes", "", "find", "identityToFind", "findEffectiveRecomposeScope", "Landroidx/compose/runtime/RecomposeScopeImpl;", "group", "groupContainsAnchor", "groupIndex", "groupSizes", "invalidateGroup", "invalidateGroupsWithKey", "target", "invalidateGroupsWithKey$runtime_release", "iterator", "", "keys", "nodes", "openReader", "openWriter", "ownsAnchor", "parentIndexes", "read", "T", "block", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "setTo", "setTo$runtime_release", "([II[Ljava/lang/Object;ILjava/util/ArrayList;)V", "slotsOf", "slotsOf$runtime_release", "verifyWellFormed", "write", "emitGroup", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "level", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SlotTable implements CompositionData, Iterable<CompositionGroup>, KMappedMarker
{
    private ArrayList<Anchor> anchors;
    private int[] groups;
    private int groupsSize;
    private int readers;
    private Object[] slots;
    private int slotsSize;
    private int version;
    private boolean writer;
    
    public SlotTable() {
        this.groups = new int[0];
        this.slots = new Object[0];
        this.anchors = new ArrayList<Anchor>();
    }
    
    private final List<Integer> dataIndexes() {
        return SlotTableKt.access$dataAnchors(this.groups, this.groupsSize * 5);
    }
    
    private final int emitGroup(final StringBuilder sb, final int i, final int n) {
        final int n2 = 0;
        for (int j = 0; j < n; ++j) {
            sb.append(' ');
        }
        sb.append("Group(");
        sb.append(i);
        sb.append(") key=");
        sb.append(SlotTableKt.access$key(this.groups, i));
        final int access$groupSize = SlotTableKt.access$groupSize(this.groups, i);
        sb.append(", nodes=");
        sb.append(SlotTableKt.access$nodeCount(this.groups, i));
        sb.append(", size=");
        sb.append(access$groupSize);
        if (SlotTableKt.access$hasMark(this.groups, i)) {
            sb.append(", mark");
        }
        if (SlotTableKt.access$containsMark(this.groups, i)) {
            sb.append(", contains mark");
        }
        final int emitGroup$dataIndex = emitGroup$dataIndex(this, i);
        final int n3 = i + 1;
        final int emitGroup$dataIndex2 = emitGroup$dataIndex(this, n3);
        int n4 = n2;
        if (emitGroup$dataIndex >= 0) {
            n4 = n2;
            if (emitGroup$dataIndex <= emitGroup$dataIndex2) {
                n4 = 1;
            }
        }
        if (n4 != 0 && emitGroup$dataIndex2 <= this.slotsSize) {
            if (SlotTableKt.access$hasObjectKey(this.groups, i)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" objectKey=");
                sb2.append(this.slots[SlotTableKt.access$objectKeyIndex(this.groups, i)]);
                sb.append(sb2.toString());
            }
            if (SlotTableKt.access$isNode(this.groups, i)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" node=");
                sb3.append(this.slots[SlotTableKt.access$nodeIndex(this.groups, i)]);
                sb.append(sb3.toString());
            }
            if (SlotTableKt.access$hasAux(this.groups, i)) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(" aux=");
                sb4.append(this.slots[SlotTableKt.access$auxIndex(this.groups, i)]);
                sb.append(sb4.toString());
            }
            final int access$slotAnchor = SlotTableKt.access$slotAnchor(this.groups, i);
            if (access$slotAnchor < emitGroup$dataIndex2) {
                sb.append(", slots=[");
                sb.append(access$slotAnchor);
                sb.append(": ");
                for (int k = access$slotAnchor; k < emitGroup$dataIndex2; ++k) {
                    if (k != access$slotAnchor) {
                        sb.append(", ");
                    }
                    sb.append(String.valueOf(this.slots[k]));
                }
                sb.append("]");
            }
        }
        else {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(", *invalid data offsets ");
            sb5.append(emitGroup$dataIndex);
            sb5.append('-');
            sb5.append(emitGroup$dataIndex2);
            sb5.append('*');
            sb.append(sb5.toString());
        }
        sb.append('\n');
        for (int l = n3; l < i + access$groupSize; l += this.emitGroup(sb, l, n + 1)) {}
        return access$groupSize;
    }
    
    private static final int emitGroup$dataIndex(final SlotTable slotTable, int n) {
        if (n >= slotTable.groupsSize) {
            n = slotTable.slotsSize;
        }
        else {
            n = SlotTableKt.access$dataAnchor(slotTable.groups, n);
        }
        return n;
    }
    
    private final RecomposeScopeImpl findEffectiveRecomposeScope(int i) {
        while (i > 0) {
            for (final RecomposeScopeImpl next : new DataIterator(this, i)) {
                if (next instanceof RecomposeScopeImpl) {
                    return next;
                }
            }
            i = SlotTableKt.access$parentAnchor(this.groups, i);
        }
        return null;
    }
    
    private final List<Integer> groupSizes() {
        return SlotTableKt.access$groupSizes(this.groups, this.groupsSize * 5);
    }
    
    private final boolean invalidateGroup(int access$parentAnchor) {
        while (true) {
            boolean b = false;
            if (access$parentAnchor < 0) {
                return false;
            }
            for (final RecomposeScopeImpl next : new DataIterator(this, access$parentAnchor)) {
                if (next instanceof RecomposeScopeImpl) {
                    final RecomposeScopeImpl recomposeScopeImpl = next;
                    recomposeScopeImpl.setRequiresRecompose(true);
                    if (recomposeScopeImpl.invalidateForResult(null) != InvalidationResult.IGNORED) {
                        b = true;
                    }
                    return b;
                }
            }
            access$parentAnchor = SlotTableKt.access$parentAnchor(this.groups, access$parentAnchor);
        }
    }
    
    private static final void invalidateGroupsWithKey$lambda$14$scanGroup(final SlotReader slotReader, final int n, final List<Anchor> list, final Ref$BooleanRef ref$BooleanRef, final SlotTable slotTable, final List<RecomposeScopeImpl> list2) {
        if (slotReader.getGroupKey() == n) {
            list.add(SlotReader.anchor$default(slotReader, 0, 1, null));
            if (ref$BooleanRef.element) {
                final RecomposeScopeImpl effectiveRecomposeScope = slotTable.findEffectiveRecomposeScope(slotReader.getCurrentGroup());
                if (effectiveRecomposeScope != null) {
                    list2.add(effectiveRecomposeScope);
                }
                else {
                    ref$BooleanRef.element = false;
                    list2.clear();
                }
            }
            slotReader.skipGroup();
            return;
        }
        slotReader.startGroup();
        while (!slotReader.isGroupEnd()) {
            invalidateGroupsWithKey$lambda$14$scanGroup(slotReader, n, list, ref$BooleanRef, slotTable, list2);
        }
        slotReader.endGroup();
    }
    
    private final List<Integer> keys() {
        return SlotTableKt.access$keys(this.groups, this.groupsSize * 5);
    }
    
    private final List<Integer> nodes() {
        return SlotTableKt.access$nodeCounts(this.groups, this.groupsSize * 5);
    }
    
    private final List<Integer> parentIndexes() {
        return SlotTableKt.access$parentAnchors(this.groups, this.groupsSize * 5);
    }
    
    private static final int verifyWellFormed$validateGroup(final Ref$IntRef ref$IntRef, final SlotTable slotTable, final int n, int i) {
        final int element = ref$IntRef.element;
        final int element2 = element + 1;
        ref$IntRef.element = element2;
        final int access$parentAnchor = SlotTableKt.access$parentAnchor(slotTable.groups, element);
        final int n2 = 0;
        final int n3 = 1;
        if (access$parentAnchor != n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid parent index detected at ");
            sb.append(element);
            sb.append(", expected parent index to be ");
            sb.append(n);
            sb.append(" found ");
            sb.append(access$parentAnchor);
            throw new IllegalStateException(sb.toString().toString());
        }
        final int n4 = SlotTableKt.access$groupSize(slotTable.groups, element) + element;
        if (n4 > slotTable.groupsSize) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("A group extends past the end of the table at ");
            sb2.append(element);
            throw new IllegalStateException(sb2.toString().toString());
        }
        if (n4 <= i) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (i == 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("A group extends past its parent group at ");
            sb3.append(element);
            throw new IllegalStateException(sb3.toString().toString());
        }
        final int access$dataAnchor = SlotTableKt.access$dataAnchor(slotTable.groups, element);
        if (element >= slotTable.groupsSize - 1) {
            i = slotTable.slotsSize;
        }
        else {
            i = SlotTableKt.access$dataAnchor(slotTable.groups, element2);
        }
        if (i > slotTable.slots.length) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Slots for ");
            sb4.append(element);
            sb4.append(" extend past the end of the slot table");
            throw new IllegalStateException(sb4.toString().toString());
        }
        if (access$dataAnchor > i) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Invalid data anchor at ");
            sb5.append(element);
            throw new IllegalStateException(sb5.toString().toString());
        }
        if (SlotTableKt.access$slotAnchor(slotTable.groups, element) > i) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Slots start out of range at ");
            sb6.append(element);
            throw new IllegalStateException(sb6.toString().toString());
        }
        if (i - access$dataAnchor >= ((SlotTableKt.access$isNode(slotTable.groups, element) + SlotTableKt.access$hasObjectKey(slotTable.groups, element) + SlotTableKt.access$hasAux(slotTable.groups, element)) ? 1 : 0)) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (i == 0) {
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("Not enough slots added for group ");
            sb7.append(element);
            throw new IllegalStateException(sb7.toString().toString());
        }
        final boolean access$isNode = SlotTableKt.access$isNode(slotTable.groups, element);
        if (access$isNode && slotTable.slots[SlotTableKt.access$nodeIndex(slotTable.groups, element)] == null) {
            i = 0;
        }
        else {
            i = 1;
        }
        if (i == 0) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append("No node recorded for a node group at ");
            sb8.append(element);
            throw new IllegalStateException(sb8.toString().toString());
        }
        i = 0;
        while (ref$IntRef.element < n4) {
            i += verifyWellFormed$validateGroup(ref$IntRef, slotTable, element, n4);
        }
        final int access$nodeCount = SlotTableKt.access$nodeCount(slotTable.groups, element);
        final int access$groupSize = SlotTableKt.access$groupSize(slotTable.groups, element);
        if (access$nodeCount != i) {
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("Incorrect node count detected at ");
            sb9.append(element);
            sb9.append(", expected ");
            sb9.append(access$nodeCount);
            sb9.append(", received ");
            sb9.append(i);
            throw new IllegalStateException(sb9.toString().toString());
        }
        final int j = ref$IntRef.element - element;
        if (access$groupSize == j) {
            if (SlotTableKt.access$containsAnyMark(slotTable.groups, element)) {
                int n5 = 0;
                Label_0446: {
                    if (element > 0) {
                        n5 = n2;
                        if (!SlotTableKt.access$containsMark(slotTable.groups, n)) {
                            break Label_0446;
                        }
                    }
                    n5 = 1;
                }
                if (n5 == 0) {
                    final StringBuilder sb10 = new StringBuilder();
                    sb10.append("Expected group ");
                    sb10.append(n);
                    sb10.append(" to record it contains a mark because ");
                    sb10.append(element);
                    sb10.append(" does");
                    throw new IllegalStateException(sb10.toString().toString());
                }
            }
            if (access$isNode) {
                i = n3;
            }
            return i;
        }
        final StringBuilder sb11 = new StringBuilder();
        sb11.append("Incorrect slot count detected at ");
        sb11.append(element);
        sb11.append(", expected ");
        sb11.append(access$groupSize);
        sb11.append(", received ");
        sb11.append(j);
        throw new IllegalStateException(sb11.toString().toString());
    }
    
    public final Anchor anchor(final int n) {
        if (!(this.writer ^ true)) {
            ComposerKt.composeRuntimeError("use active SlotWriter to create an anchor location instead ".toString());
            throw new KotlinNothingValueException();
        }
        int n2 = 0;
        if (n >= 0) {
            n2 = n2;
            if (n < this.groupsSize) {
                n2 = 1;
            }
        }
        if (n2 != 0) {
            final ArrayList<Anchor> anchors = this.anchors;
            final int access$search = SlotTableKt.access$search(anchors, n, this.groupsSize);
            Anchor element;
            if (access$search < 0) {
                element = new Anchor(n);
                anchors.add(-(access$search + 1), element);
            }
            else {
                final Anchor value = anchors.get(access$search);
                Intrinsics.checkNotNullExpressionValue((Object)value, "get(location)");
                element = value;
            }
            return element;
        }
        throw new IllegalArgumentException("Parameter index is out of range".toString());
    }
    
    public final int anchorIndex(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        if (!(this.writer ^ true)) {
            ComposerKt.composeRuntimeError("Use active SlotWriter to determine anchor location instead".toString());
            throw new KotlinNothingValueException();
        }
        if (anchor.getValid()) {
            return anchor.getLocation$runtime_release();
        }
        throw new IllegalArgumentException("Anchor refers to a group that was removed".toString());
    }
    
    public final String asString() {
        String s;
        if (this.writer) {
            s = super.toString();
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append('\n');
            final int groupsSize = this.groupsSize;
            if (groupsSize > 0) {
                for (int i = 0; i < groupsSize; i += this.emitGroup(sb, i, 0)) {}
            }
            else {
                sb.append("<EMPTY>");
            }
            s = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)s, "StringBuilder().apply(builderAction).toString()");
        }
        return s;
    }
    
    public final void close$runtime_release(final SlotReader slotReader) {
        Intrinsics.checkNotNullParameter((Object)slotReader, "reader");
        if (slotReader.getTable$runtime_release() == this && this.readers > 0) {
            --this.readers;
            return;
        }
        ComposerKt.composeRuntimeError("Unexpected reader close()".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void close$runtime_release(final SlotWriter slotWriter, final int[] array, final int n, final Object[] array2, final int n2, final ArrayList<Anchor> list) {
        Intrinsics.checkNotNullParameter((Object)slotWriter, "writer");
        Intrinsics.checkNotNullParameter((Object)array, "groups");
        Intrinsics.checkNotNullParameter((Object)array2, "slots");
        Intrinsics.checkNotNullParameter((Object)list, "anchors");
        if (slotWriter.getTable$runtime_release() == this && this.writer) {
            this.writer = false;
            this.setTo$runtime_release(array, n, array2, n2, list);
            return;
        }
        throw new IllegalArgumentException("Unexpected writer close()".toString());
    }
    
    public final boolean containsMark() {
        final int groupsSize = this.groupsSize;
        boolean b = false;
        if (groupsSize > 0) {
            b = b;
            if (SlotTableKt.access$containsMark(this.groups, 0)) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public CompositionGroup find(final Object o) {
        Intrinsics.checkNotNullParameter(o, "identityToFind");
        return new SlotTableGroup(this, 0, 0, 4, null).find(o);
    }
    
    public final ArrayList<Anchor> getAnchors$runtime_release() {
        return this.anchors;
    }
    
    @Override
    public Iterable<CompositionGroup> getCompositionGroups() {
        return this;
    }
    
    public final int[] getGroups() {
        return this.groups;
    }
    
    public final int getGroupsSize() {
        return this.groupsSize;
    }
    
    public final Object[] getSlots() {
        return this.slots;
    }
    
    public final int getSlotsSize() {
        return this.slotsSize;
    }
    
    public final int getVersion$runtime_release() {
        return this.version;
    }
    
    public final boolean getWriter$runtime_release() {
        return this.writer;
    }
    
    public final boolean groupContainsAnchor(int n, final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        final boolean writer = this.writer;
        boolean b = true;
        if (!(writer ^ true)) {
            ComposerKt.composeRuntimeError("Writer is active".toString());
            throw new KotlinNothingValueException();
        }
        if (n >= 0 && n < this.groupsSize) {
            if (this.ownsAnchor(anchor)) {
                final int access$groupSize = SlotTableKt.access$groupSize(this.groups, n);
                final int location$runtime_release = anchor.getLocation$runtime_release();
                if (n <= location$runtime_release && location$runtime_release < access$groupSize + n) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                if (n != 0) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        ComposerKt.composeRuntimeError("Invalid group index".toString());
        throw new KotlinNothingValueException();
    }
    
    public final List<RecomposeScopeImpl> invalidateGroupsWithKey$runtime_release(int i) {
        final List list = new ArrayList();
        List list2 = new ArrayList();
        final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = true;
        Object o = this.openReader();
        try {
            invalidateGroupsWithKey$lambda$14$scanGroup((SlotReader)o, i, list, ref$BooleanRef, this, list2);
            final Unit instance = Unit.INSTANCE;
            ((SlotReader)o).close();
            o = this.openWriter();
            try {
                ((SlotWriter)o).startGroup();
                Anchor anchor;
                for (i = 0; i < list.size(); ++i) {
                    anchor = list.get(i);
                    if (anchor.toIndexFor((SlotWriter)o) >= ((SlotWriter)o).getCurrentGroup()) {
                        ((SlotWriter)o).seek(anchor);
                        ((SlotWriter)o).bashGroup$runtime_release();
                    }
                }
                ((SlotWriter)o).skipToGroupEnd();
                ((SlotWriter)o).endGroup();
                ((SlotWriter)o).close();
                if (!ref$BooleanRef.element) {
                    list2 = null;
                }
                return list2;
            }
            finally {
                ((SlotWriter)o).close();
            }
        }
        finally {
            ((SlotReader)o).close();
        }
    }
    
    @Override
    public boolean isEmpty() {
        return this.groupsSize == 0;
    }
    
    @Override
    public Iterator<CompositionGroup> iterator() {
        return new GroupIterator(this, 0, this.groupsSize);
    }
    
    public final SlotReader openReader() {
        if (!this.writer) {
            ++this.readers;
            return new SlotReader(this);
        }
        throw new IllegalStateException("Cannot read while a writer is pending".toString());
    }
    
    public final SlotWriter openWriter() {
        if (!(this.writer ^ true)) {
            ComposerKt.composeRuntimeError("Cannot start a writer when another writer is pending".toString());
            throw new KotlinNothingValueException();
        }
        if (this.readers <= 0) {
            this.writer = true;
            ++this.version;
            return new SlotWriter(this);
        }
        ComposerKt.composeRuntimeError("Cannot start a writer when a reader is pending".toString());
        throw new KotlinNothingValueException();
    }
    
    public final boolean ownsAnchor(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        final boolean valid = anchor.getValid();
        boolean b = true;
        if (valid) {
            final int access$search = SlotTableKt.access$search(this.anchors, anchor.getLocation$runtime_release(), this.groupsSize);
            if (access$search >= 0 && Intrinsics.areEqual((Object)this.anchors.get(access$search), (Object)anchor)) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    public final <T> T read(final Function1<? super SlotReader, ? extends T> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final SlotReader openReader = this.openReader();
        try {
            return (T)function1.invoke((Object)openReader);
        }
        finally {
            InlineMarker.finallyStart(1);
            openReader.close();
            InlineMarker.finallyEnd(1);
        }
    }
    
    public final void setAnchors$runtime_release(final ArrayList<Anchor> anchors) {
        Intrinsics.checkNotNullParameter((Object)anchors, "<set-?>");
        this.anchors = anchors;
    }
    
    public final void setTo$runtime_release(final int[] groups, final int groupsSize, final Object[] slots, final int slotsSize, final ArrayList<Anchor> anchors) {
        Intrinsics.checkNotNullParameter((Object)groups, "groups");
        Intrinsics.checkNotNullParameter((Object)slots, "slots");
        Intrinsics.checkNotNullParameter((Object)anchors, "anchors");
        this.groups = groups;
        this.groupsSize = groupsSize;
        this.slots = slots;
        this.slotsSize = slotsSize;
        this.anchors = anchors;
    }
    
    public final void setVersion$runtime_release(final int version) {
        this.version = version;
    }
    
    public final List<Object> slotsOf$runtime_release(int n) {
        final int access$dataAnchor = SlotTableKt.access$dataAnchor(this.groups, n);
        if (++n < this.groupsSize) {
            n = SlotTableKt.access$dataAnchor(this.groups, n);
        }
        else {
            n = this.slots.length;
        }
        return ArraysKt.toList(this.slots).subList(access$dataAnchor, n);
    }
    
    public final void verifyWellFormed() {
        final Ref$IntRef ref$IntRef = new Ref$IntRef();
        final int groupsSize = this.groupsSize;
        int n = -1;
        if (groupsSize > 0) {
            while (ref$IntRef.element < this.groupsSize) {
                verifyWellFormed$validateGroup(ref$IntRef, this, -1, ref$IntRef.element + SlotTableKt.access$groupSize(this.groups, ref$IntRef.element));
            }
            if (ref$IntRef.element != this.groupsSize) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Incomplete group at root ");
                sb.append(ref$IntRef.element);
                sb.append(" expected to be ");
                sb.append(this.groupsSize);
                throw new IllegalStateException(sb.toString().toString());
            }
        }
        final List list = this.anchors;
        int index;
        for (int size = list.size(), i = 0; i < size; ++i, n = index) {
            index = list.get(i).toIndexFor(this);
            if (index < 0 || index > this.groupsSize) {
                throw new IllegalArgumentException("Invalid anchor, location out of bound".toString());
            }
            if (n >= index) {
                throw new IllegalArgumentException("Anchor is out of order".toString());
            }
        }
    }
    
    public final <T> T write(final Function1<? super SlotWriter, ? extends T> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final SlotWriter openWriter = this.openWriter();
        try {
            return (T)function1.invoke((Object)openWriter);
        }
        finally {
            InlineMarker.finallyStart(1);
            openWriter.close();
            InlineMarker.finallyEnd(1);
        }
    }
}

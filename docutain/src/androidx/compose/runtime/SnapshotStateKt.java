// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import java.util.Collection;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Pair;
import androidx.compose.runtime.snapshots.SnapshotStateMap;
import androidx.compose.runtime.snapshots.SnapshotStateList;
import kotlin.reflect.KProperty;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.flow.StateFlow;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.Flow;
import kotlin.Metadata;

@Metadata(d1 = { "androidx/compose/runtime/SnapshotStateKt__DerivedStateKt", "androidx/compose/runtime/SnapshotStateKt__ProduceStateKt", "androidx/compose/runtime/SnapshotStateKt__SnapshotFlowKt", "androidx/compose/runtime/SnapshotStateKt__SnapshotMutationPolicyKt", "androidx/compose/runtime/SnapshotStateKt__SnapshotStateKt" }, k = 4, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotStateKt
{
    public static final <T extends R, R> State<R> collectAsState(final Flow<? extends T> flow, final R r, final CoroutineContext coroutineContext, final Composer composer, final int n, final int n2) {
        return SnapshotStateKt__SnapshotFlowKt.collectAsState((kotlinx.coroutines.flow.Flow<?>)flow, r, coroutineContext, composer, n, n2);
    }
    
    public static final <T> State<T> collectAsState(final StateFlow<? extends T> stateFlow, final CoroutineContext coroutineContext, final Composer composer, final int n, final int n2) {
        return SnapshotStateKt__SnapshotFlowKt.collectAsState(stateFlow, coroutineContext, composer, n, n2);
    }
    
    public static final <T> State<T> derivedStateOf(final SnapshotMutationPolicy<T> snapshotMutationPolicy, final Function0<? extends T> function0) {
        return SnapshotStateKt__DerivedStateKt.derivedStateOf(snapshotMutationPolicy, function0);
    }
    
    public static final <T> State<T> derivedStateOf(final Function0<? extends T> function0) {
        return SnapshotStateKt__DerivedStateKt.derivedStateOf(function0);
    }
    
    public static final <T> T getValue(final State<? extends T> state, final Object o, final KProperty<?> kProperty) {
        return SnapshotStateKt__SnapshotStateKt.getValue(state, o, kProperty);
    }
    
    public static final <T> SnapshotStateList<T> mutableStateListOf() {
        return SnapshotStateKt__SnapshotStateKt.mutableStateListOf();
    }
    
    public static final <T> SnapshotStateList<T> mutableStateListOf(final T... array) {
        return SnapshotStateKt__SnapshotStateKt.mutableStateListOf(array);
    }
    
    public static final <K, V> SnapshotStateMap<K, V> mutableStateMapOf() {
        return SnapshotStateKt__SnapshotStateKt.mutableStateMapOf();
    }
    
    public static final <K, V> SnapshotStateMap<K, V> mutableStateMapOf(final Pair<? extends K, ? extends V>... array) {
        return SnapshotStateKt__SnapshotStateKt.mutableStateMapOf(array);
    }
    
    public static final <T> MutableState<T> mutableStateOf(final T t, final SnapshotMutationPolicy<T> snapshotMutationPolicy) {
        return SnapshotStateKt__SnapshotStateKt.mutableStateOf(t, snapshotMutationPolicy);
    }
    
    public static final <T> SnapshotMutationPolicy<T> neverEqualPolicy() {
        return SnapshotStateKt__SnapshotMutationPolicyKt.neverEqualPolicy();
    }
    
    public static final <R> void observeDerivedStateRecalculations(final Function1<? super State<?>, Unit> function1, final Function1<? super State<?>, Unit> function2, final Function0<? extends R> function3) {
        SnapshotStateKt__DerivedStateKt.observeDerivedStateRecalculations(function1, function2, (kotlin.jvm.functions.Function0<?>)function3);
    }
    
    public static final <T> State<T> produceState(final T t, final Object o, final Object o2, final Object o3, final Function2<? super ProduceStateScope<T>, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        return SnapshotStateKt__ProduceStateKt.produceState(t, o, o2, o3, function2, composer, n);
    }
    
    public static final <T> State<T> produceState(final T t, final Object o, final Object o2, final Function2<? super ProduceStateScope<T>, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        return SnapshotStateKt__ProduceStateKt.produceState(t, o, o2, function2, composer, n);
    }
    
    public static final <T> State<T> produceState(final T t, final Object o, final Function2<? super ProduceStateScope<T>, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        return SnapshotStateKt__ProduceStateKt.produceState(t, o, function2, composer, n);
    }
    
    public static final <T> State<T> produceState(final T t, final Function2<? super ProduceStateScope<T>, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        return SnapshotStateKt__ProduceStateKt.produceState(t, function2, composer, n);
    }
    
    public static final <T> State<T> produceState(final T t, final Object[] array, final Function2<? super ProduceStateScope<T>, ? super Continuation<? super Unit>, ?> function2, final Composer composer, final int n) {
        return SnapshotStateKt__ProduceStateKt.produceState(t, array, function2, composer, n);
    }
    
    public static final <T> SnapshotMutationPolicy<T> referentialEqualityPolicy() {
        return SnapshotStateKt__SnapshotMutationPolicyKt.referentialEqualityPolicy();
    }
    
    public static final <T> State<T> rememberUpdatedState(final T t, final Composer composer, final int n) {
        return SnapshotStateKt__SnapshotStateKt.rememberUpdatedState(t, composer, n);
    }
    
    public static final <T> void setValue(final MutableState<T> mutableState, final Object o, final KProperty<?> kProperty, final T t) {
        SnapshotStateKt__SnapshotStateKt.setValue(mutableState, o, kProperty, t);
    }
    
    public static final <T> Flow<T> snapshotFlow(final Function0<? extends T> function0) {
        return SnapshotStateKt__SnapshotFlowKt.snapshotFlow(function0);
    }
    
    public static final <T> SnapshotMutationPolicy<T> structuralEqualityPolicy() {
        return SnapshotStateKt__SnapshotMutationPolicyKt.structuralEqualityPolicy();
    }
    
    public static final <T> SnapshotStateList<T> toMutableStateList(final Collection<? extends T> collection) {
        return SnapshotStateKt__SnapshotStateKt.toMutableStateList(collection);
    }
    
    public static final <K, V> SnapshotStateMap<K, V> toMutableStateMap(final Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        return SnapshotStateKt__SnapshotStateKt.toMutableStateMap(iterable);
    }
}

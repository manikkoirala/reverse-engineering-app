// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001¢\u0006\u0002\u0010\u0004J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J!\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0001H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0012\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u0002J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0006¨\u0006\u0014" }, d2 = { "Landroidx/compose/runtime/JoinedKey;", "", "left", "right", "(Ljava/lang/Object;Ljava/lang/Object;)V", "getLeft", "()Ljava/lang/Object;", "getRight", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "hashCodeOf", "value", "toString", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class JoinedKey
{
    private final Object left;
    private final Object right;
    
    public JoinedKey(final Object left, final Object right) {
        this.left = left;
        this.right = right;
    }
    
    private final int hashCodeOf(final Object o) {
        int n;
        if (o instanceof Enum) {
            n = ((Enum)o).ordinal();
        }
        else if (o != null) {
            n = o.hashCode();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public final Object component1() {
        return this.left;
    }
    
    public final Object component2() {
        return this.right;
    }
    
    public final JoinedKey copy(final Object o, final Object o2) {
        return new JoinedKey(o, o2);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JoinedKey)) {
            return false;
        }
        final JoinedKey joinedKey = (JoinedKey)o;
        return Intrinsics.areEqual(this.left, joinedKey.left) && Intrinsics.areEqual(this.right, joinedKey.right);
    }
    
    public final Object getLeft() {
        return this.left;
    }
    
    public final Object getRight() {
        return this.right;
    }
    
    @Override
    public int hashCode() {
        return this.hashCodeOf(this.left) * 31 + this.hashCodeOf(this.right);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("JoinedKey(left=");
        sb.append(this.left);
        sb.append(", right=");
        sb.append(this.right);
        sb.append(')');
        return sb.toString();
    }
}

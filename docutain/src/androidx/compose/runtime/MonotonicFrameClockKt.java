// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\u001a:\u0010\u0007\u001a\u0002H\b\"\u0004\b\u0000\u0010\b2!\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\b0\nH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a:\u0010\u0010\u001a\u0002H\b\"\u0004\b\u0000\u0010\b2!\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u0002H\b0\nH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a@\u0010\u0007\u001a\u0002H\b\"\u0004\b\u0000\u0010\b*\u00020\u00012#\b\u0004\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\b0\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\"\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00028FX\u0087\u0004¢\u0006\f\u0012\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0013" }, d2 = { "monotonicFrameClock", "Landroidx/compose/runtime/MonotonicFrameClock;", "Lkotlin/coroutines/CoroutineContext;", "getMonotonicFrameClock$annotations", "(Lkotlin/coroutines/CoroutineContext;)V", "getMonotonicFrameClock", "(Lkotlin/coroutines/CoroutineContext;)Landroidx/compose/runtime/MonotonicFrameClock;", "withFrameMillis", "R", "onFrame", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "frameTimeMillis", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withFrameNanos", "frameTimeNanos", "(Landroidx/compose/runtime/MonotonicFrameClock;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class MonotonicFrameClockKt
{
    public static final MonotonicFrameClock getMonotonicFrameClock(final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "<this>");
        final MonotonicFrameClock monotonicFrameClock = (MonotonicFrameClock)coroutineContext.get((CoroutineContext$Key)MonotonicFrameClock.Key);
        if (monotonicFrameClock != null) {
            return monotonicFrameClock;
        }
        throw new IllegalStateException("A MonotonicFrameClock is not available in this CoroutineContext. Callers should supply an appropriate MonotonicFrameClock using withContext.".toString());
    }
    
    public static final <R> Object withFrameMillis(final MonotonicFrameClock monotonicFrameClock, final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        return monotonicFrameClock.withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)new MonotonicFrameClockKt$withFrameMillis.MonotonicFrameClockKt$withFrameMillis$2((Function1)function1), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withFrameMillis(final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        return getMonotonicFrameClock(continuation.getContext()).withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)new MonotonicFrameClockKt$withFrameMillis.MonotonicFrameClockKt$withFrameMillis$2((Function1)function1), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withFrameMillis$$forInline(final MonotonicFrameClock monotonicFrameClock, final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        final Function1 function2 = (Function1)new MonotonicFrameClockKt$withFrameMillis.MonotonicFrameClockKt$withFrameMillis$2((Function1)function1);
        InlineMarker.mark(0);
        final Object withFrameNanos = monotonicFrameClock.withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
        InlineMarker.mark(1);
        return withFrameNanos;
    }
    
    public static final <R> Object withFrameNanos(final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        return getMonotonicFrameClock(continuation.getContext()).withFrameNanos((kotlin.jvm.functions.Function1<? super Long, ?>)function1, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
}
